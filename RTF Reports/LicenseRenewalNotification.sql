[Main]
select j.jobid Jobid,
       j.ExternalFileNum ExternalFileNum,
       j.JobDescription  JobDescription,
       to_char( j.CreatedDate, 'fmMonth DD, YYYY' ) CreatedDate,
       j.CreatedByUser CreatedByUser
from    query.j_abc_batchrenewalnotification j 
where  j.ObjectId = :PosseObjectId

[LicenseContent]
select a.LicenseObectId,
       a.EstablishmentAddress,
       a.LicenseNumber,
       a.LicenseType,
       a.Licensee,
       a.Establishment,
       a.ExpirationDate,
       a.MailingAddress,
       a.PreferredContactMethod,
       a.ReliefRequired1218,
       a.ReliefRequired1239
  from (
          select   l.ObjectId                               LicenseObectId,
                   l.EstablishmentLocationAddress           EstablishmentAddress,
                   l.LicenseNumber                          LicenseNumber, 
                   l.LicenseType                            LicenseType,
                   l.Licensee                               Licensee,
                   l.Establishment                          Establishment, 
                   to_char(l.ExpirationDate,'fmMonth DD, YYYY')   ExpirationDate,
                   le.MailingAddress                        MailingAddress,
                   le.PreferredContactMethod                PreferredContactMethod,
                   l.ReliefRequired1218                     ReliefRequired1218,
                   l.ReliefRequired1239                     ReliefRequired1239
          from     query.j_abc_batchrenewalnotification j
          join     abc.licensebatchgrace_xref_t rg on rg.renewalnotifbatchjobid = j.JobId
          join     query.o_abc_license l on l.ObjectId = rg.licenseobjectid
          join     query.r_ABC_LicenseLicenseeLE rle 
            on     rle.LicenseObjectId = l.objectid
          join     query.o_abc_legalentity le 
            on     le.objectid = rle.LegalEntityObjectId
          where    j.objectid = :[Main]JobId --18982152
            and    (le.PreferredContactMethod is null or
                    le.PreferredContactMethod is not null and le.PreferredContactMethod <> 'Email')
          UNION ALL
          select   l2.ObjectId                               LicenseObjectId,
                   l2.EstablishmentLocationAddress           EstablishmentAddress,
                   l2.LicenseNumber                          LicenseNumber, 
                   l2.LicenseType                            LicenseType,
                   l2.Licensee                               Licensee,
                   l2.Establishment                          Establishment, 
                   to_char(l2.ExpirationDate,'fmMonth DD, YYYY')   ExpirationDate,
                   le.MailingAddress                         MailingAddress,
                   le.PreferredContactMethod                 PreferredContactMethod,
                   l2.ReliefRequired1218                     ReliefRequired1218,
                   l2.ReliefRequired1239                     ReliefRequired1239
          from     query.j_abc_batchrenewalnotification j
          join     abc.licensebatch_xref_t rl on rl.renewalnotifbatchjobid = j.jobid
          join     query.o_abc_license l2 on l2.objectid = rl.licenseobjectid
          join     query.r_ABC_LicenseLicenseeLE rle 
            on     rle.LicenseObjectId = l2.objectid
          join     query.o_abc_legalentity le 
            on     le.objectid = rle.LegalEntityObjectId
          where    j.objectid = :[Main]JobId --18982152
            and    (le.PreferredContactMethod is null or
                    le.PreferredContactMethod is not null and le.PreferredContactMethod <> 'Email')
       ) a
  order by 3

[NJABCAddress]
select    ss.DepartmentAddress Address
from      query.o_systemsettings ss

[LicenseText]
select    s2.LicenseLetterText NotifyText,
          case when :[LicenseContent]ReliefRequired1218 = 'Y' then s2.ReliefRequiredText1218 else '' end ReliefRequired1218Text, 
          case when :[LicenseContent]ReliefRequired1239 = 'Y' then s2.ReliefRequiredText1239 else '' end ReliefRequired1239Text
from      query.o_systemsettings s2

[Document Info]
EndpointName=BatchRenewalLetter
DocumentTypeName=d_ElectronicDocument

[Document Bindings]
Description=Batch Renewal Notification - License

