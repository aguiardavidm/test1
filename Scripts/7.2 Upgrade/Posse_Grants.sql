grant select
on objmodelphys.alldomainlistofvalues
to extension;

grant select
on finance.fees
to feescheduleplus;

grant update
on finance.fees
to feescheduleplus;

grant select
on finance.transactions
to feescheduleplus;

grant update
on finance.transactions
to feescheduleplus;

grant select
on objmodelphys.expressioncolumndefs
to extension;

grant select
on objmodelphys.lookupcolumndefs
to extension;

grant select
on possedata.objectcolumndata
to extension;

grant select
on stage.responsibleobjects
to abc;

grant select
on stage.responsibleobjects
to extension;

grant select
on stage.responsibleobjects
to feescheduleplus;