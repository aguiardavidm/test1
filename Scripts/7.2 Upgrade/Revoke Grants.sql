--Ran during the upgrade
grant select on sys.dba_users to possedba with grant option;

grant select on sys.v_$dblink to possedba with grant option;

revoke select on sys.dba_data_files from possedba;
revoke select on sys.dba_data_files from app;
revoke select on sys.dba_free_space from possedba;
revoke select on sys.dba_free_space from app;
revoke select on sys.dba_segments from possedba;
revoke select on sys.dba_segments from app;
revoke select on sys.dba_tablespaces from possedba;
revoke select on sys.dba_tablespaces from app;
revoke select on sys.dba_temp_files from possedba;
revoke select on sys.dba_temp_files from app;
revoke select on sys.v_$sqlarea from possedba;
revoke select on sys.v_$sqlarea from app;
revoke select on sys.v_$sort_usage from possedba;
revoke select on sys.v_$sort_usage from app;
revoke select on sys.v_$system_parameter from possedba;

revoke select on sys.v_$system_parameter from app;
revoke select on sys.v_$temp_extent_pool from possedba;

revoke select on sys.v_$temp_extent_pool from app;