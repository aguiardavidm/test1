""" Clean up invalid widget properties.

X,Y on grids - sub-set of PPI007797

"""


import PosseOracle


def GetIdFromXml(xml, attribute):
    _, a = xml.split('    %s="' % attribute, 1)
    idStr, _ = a.split('"', 1)
    return int(idStr)


def FetchPaneItems(cursor, paneTypes):
    # Return Pane ItemGuids for the specified pane types

    # CapturableItemTypeId 56 - Panes
    cursor.execute("""
            select
              i.ItemGuid,
              x.ShortXml,
              x.LongXml
            from
              marshalregistry.Items_t i
              join marshalregistry.Versions_t v
                  on v.ItemGuid = i.ItemGuid
              join marshalregistry.Xml_t x
                  on x.VersionGuid = v.VersionGuid
            where i.CapturableItemTypeId = 56
            """)

    paneItems = []
    for itemGuid, shortXml, longXml in cursor:
        xml = shortXml or longXml.read()
        if GetIdFromXml(xml, "panetypeid") in paneTypes:
            paneItems.append(itemGuid)
    return paneItems


def GetXmlForWidgets(cursor, paneItemGuid, widgetTypes):
    # Return versionGuid and XML for widgets of the specified
    # widget types painted on the specified pane.

    # CapturableItemTypeId 94 - Widgets
    cursor.execute("""
            select
              d.VersionGuid,
              x.ShortXml,
              x.LongXml
            from
              marshalregistry.Dependencies_t d
              join marshalregistry.Versions_t v
                  on v.VersionGuid = d.VersionGuid
              join marshalregistry.Items_t i
                  on i.ItemGuid = v.ItemGuid
                 and i.CapturableItemTypeId = 94
              join marshalregistry.Xml_t x
                  on x.VersionGuid = d.VersionGuid
            where d.DependsOnItemGuid = :paneItemGuid
              and d.IsParent = 'Y'
            """, paneItemGuid = paneItemGuid)

    widgetXml = []
    for versionGuid, shortXml, longXml in cursor:
        xml = shortXml or longXml.read()
        widgetTypeId = GetIdFromXml(xml, "widgettypeid")
        if widgetTypeId in widgetTypes:
            widgetXml.append((versionGuid, xml, widgetTypeId))
    return widgetXml


def RemoveTag(xml, attrName):
    # Remove the attrName tag from the XML
    startTag = "<%s>" % attrName
    endTag = "</%s>" % attrName
    t = xml.split("\n      %s" % startTag, 1)
    if len(t) != 2:
        return xml

    _, a = t[1].split("     %s" % endTag, 1)
    return t[0] + a


def UpdateVersionXml(cursor, versionGuid, xml):
    if len(xml) <= 4000:
       shortXml = xml
       longXml = None
    else:
       shortXml = None
       longXml = xml
    clobVar = cursor.clob(longXml)
    cursor.setinputsizes(
        versionGuid = PosseOracle.GUIDSIZE,
        shortXml = 4000,
        longXml = clobVar)
    cursor.execute(None, versionGuid = versionGuid, shortXml = shortXml,
            longXml = longXml)


cursor = gConnection.cursor()
cursor.arraysize = 50
updateCursor = gConnection.cursor()
updateCursor.prepare("""
        update marshalregistry.Xml_t set
            ShortXml = :shortXml,
            LongXml = :longXml
        where VersionGuid = :versionGuid
        """)


#-------------------------------------------------------------------
# X,Y on grids - sub-set of PPI007797
#-------------------------------------------------------------------
# Widget Types: 10 Url, 13 Delete, 14 Link, 18 CalendarPopup, 19 LookupPopup
# PaneTypes: 8 RelatedObjectsGridStyle, 37 ConfigurableObjectList
# ClassId: 95 Widgets
# Property Defs: 9 X, 10 Y

numRows = 0
for paneItemGuid in FetchPaneItems(cursor, [8, 37, 42, 47]):
    for versionGuid, xml, widgetTypeId in \
            GetXmlForWidgets(cursor, paneItemGuid, [10, 13, 14, 18, 19, 9, 4, 3, 21, 2, 11, 15]):
        originalXml = xml
        xml = RemoveTag(xml, "x")
        xml = RemoveTag(xml, "y")
        if xml == originalXml:
            continue
        UpdateVersionXml(updateCursor, versionGuid, xml)
        numRows += 1

if numRows > 0:
    LogMessage("Removed X/Y properties for %s "
            "CalendarPopup, Delete, Link, LookupPopup and Url widgets on "
            "ConfigurableObjectList/RelatedObjectsGridStyle "
            "panes", numRows)

