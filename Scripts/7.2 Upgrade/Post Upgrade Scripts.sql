--Run as POSSEDATA
alter table WORKFLOWQUEUEITEMS_T allocate extent;

--Run as POSSECONFIG
alter table PYTHONMODULES_T allocate extent;
alter table REPORTREGISTRATIONS_T allocate extent;
alter table PROCESSOUTCOMESCRIPTS_T allocate extent;