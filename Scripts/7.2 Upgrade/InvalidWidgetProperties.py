#-------------------------------------------------------------------
# X,Y on grids - sub-set of PPI007797
#-------------------------------------------------------------------
# Widget Types: 10 Url, 13 Delete, 14 Link, 18 CalendarPopup, 19 LookupPopup
# PaneTypes: 8 RelatedObjectsGridStyle, 37 GridObjectList
# ClassId: 95 Widgets
# Property Defs: 9 X, 10 Y

gCursor.execute("""
        delete from commoncfg.PropertyValues_t
        where ClassId = 95
          and InstanceId in (
              select w.WidgetId
              from
                presentation.Panes_t p
                join presentation.Widgets_t w
                    on w.PaneId = p.PaneId
                   and w.WidgetTypeId in (10, 13, 14, 18, 19, 9, 4, 3, 21, 2, 11, 15)
              where p.PaneDefId in (8, 37, 42, 47))
          and PropertyDefId in (9, 10)
        """)

if gCursor.rowcount:
    LogMessage("Deleted %s X/Y property rows for "
            "CalendarPopup, Delete, Link, LookupPopup and Url widgets on "
            "ConfigurableObjectList/RelatedObjectsGridStyle "
            "panes", gCursor.rowcount)

