declare
  t_SysSettingsId                       number;
  t_ToCellPhoneNonProd                  number := 7205441270; -- Ben Albrecht's number
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  select max(s.objectid) 
    into t_SysSettingsId 
    from query.o_systemsettings s;
  api.pkg_columnupdate.RemoveValue(t_SysSettingsId, 'IsProduction');
  api.pkg_columnupdate.SetValue(t_SysSettingsId, 'ToCellPhoneNonProd', t_ToCellPhoneNonProd);
  api.pkg_columnupdate.SetValue(t_SysSettingsId, 'DoNotSendTextMessages', 'Y');
  api.pkg_columnupdate.SetValue(t_SysSettingsId, 'InternalWebsiteBaseURL', 'https://posseapptp.lps.state.nj.us/SB_ABCInternal/Default.aspx');
  api.pkg_columnupdate.SetValue(t_SysSettingsId, 'ExternalWebsiteBaseURL', 'https://posseapptp.lps.state.nj.us/SB_ABCPublic/Default.aspx');
  api.pkg_columnupdate.SetValue(t_SysSettingsId, 'MunicipalWebsiteBaseURL', 'https://posseapptp.lps.state.nj.us/SB_ABCMunicipal/Default.aspx');
  api.pkg_columnupdate.SetValue(t_SysSettingsId, 'TaxationDataExportPath', 'C:\Taxation\');
  api.pkg_columnupdate.SetValue(t_SysSettingsId, 'WordMergeURI', 'https://posseapptp.lps.state.nj.us/SB_RestWordInterface');
  api.pkg_columnupdate.SetValue(t_SysSettingsId, 'PaymentProviderWSDLURL', 'https://orion02.oit.state.nj.us/NJ_ENT_EPAY_WS_HOSTED_NICUSA/NJ_NICUSA_WSService?wsdl');
  api.pkg_logicaltransactionupdate.EndTransaction;
end;
/

alter user outridersys identified by &a_FromKeepass; 
alter user outridersys account unlock;

alter user abcwebguest identified by &a_FromKeepass;
alter user abcwebguest account unlock;

alter user abcwebguest identified by &a_FromKeepass;
alter user abcwebguest account unlock;

alter user abc identified by &a_FromKeepass;
alter user abc account unlock;

alter user abcrw identified by &a_FromKeepass;
alter user abcrw account unlock;

alter user investigations identified by &a_FromKeepass;
alter user investigations account unlock;

alter user extension identified by &a_FromKeepass;
alter user extension account unlock;

alter user pserversys identified by &a_FromKeepass;
alter user pserversys account unlock; 

alter user abc11p identified by &a_FromKeepass;
alter user abc11p account unlock;

alter user bcpdata identified by &a_FromKeepass;
alter user bcpdata account unlock;

alter user conversion identified by &a_FromKeepass;
alter user conversion account unlock;

alter user dashboard identified by &a_FromKeepass;
alter user dashboard account unlock;

alter user feescheduleplus identified by &a_FromKeepass;
alter user feescheduleplus account unlock;

alter user ereferral identified by &a_FromKeepass;
alter user ereferral account unlock;

alter user PosseAppServerSys identified by &a_FromKeepass;
alter user PosseAppServerSys account unlock;

alter user ReportServerSys identified by &a_FromKeepass;
alter user ReportServerSys account unlock;

alter user PosseSys identified by &a_FromKeepass;
alter user PosseSys account unlock;