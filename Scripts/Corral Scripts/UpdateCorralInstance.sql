/* Create a Corral instance with a single tablespace, schema and grantee (for POSSEEXTENSIONS).
  Add all the current tables and indexes to that schema, using the newly created tablespace, schema and grantees
  --This is intended to be run from sqlplus*/

declare
  t_InstanceId        number;
  t_IndexTablespaceId number;
  t_LogTransId        number;
  t_DataTablespaceId  number;
  t_SuperUser         number;
  t_possesys          number;
  t_TableId           number;
  t_ABCSchema         number;
  t_BCPSchema         number;
  t_ExtensionSchema   number;
  t_ABCREPORTINGGrantee number; --used for remote DBs
  t_PosseGrantee      number;
  t_ExtensionsGrantee number;
  t_DashboardGrantee  number;
  t_ABCRWGrantee      number;
  t_ABCRWAdHocGrantee number;
  t_IndexId           number;
  t_GrantId           number;
  --c_TNSEntry         varchar2(50) := '&TNSEntry';  --leave blank for a local Corral instance
  c_TNSEntry         varchar2(50) := null;  --leave blank for a local Corral instance
  t_ABCRWTables       extension.udst_stringlist; -- this is here because local and remote list is different
  t_DBInstance        varchar2(50); -- database instance
  t_Reporting         varchar2(100); -- for datamart.instance
begin
  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
  end if;
  select userid into t_possesys from api.users u where u.OracleLogonId='POSSESYS';

  select sys_context('USERENV','DB_NAME')
    into t_DBInstance
    from dual;
  if t_DBInstance = 'NJDV' then
     t_Reporting := 'Local NJ ABC Reporting';
  else
     t_Reporting := 'Reports';
  end if;

  admin.pkg_control.switchuser(t_possesys);

  --select * from  datamart.instances_t
  select ac.AccessGroupId
    into t_SuperUser
    from api.accessgroups ac
   where ac.Description = 'Super Users';

  --find the instance
  if c_TNSEntry is null then --c_TNSEntry is used for creating the "Remote" instance
    begin
      select instanceid
        into t_InstanceId
        from datamart.instances
       where description = t_Reporting;
    -- if not found, raise an error
    exception
      when no_data_found then
        raise_application_error(-20000, 'No Local NJ ABC Reporting instance found.');
    end;
  else
    begin
      select instanceid
        into t_InstanceId
        from datamart.instances
       where description = 'NJ ABC Reporting';
    --if not found, raise an error
    exception
      when no_data_found then
        raise_application_error(-20000, 'No NJ ABC Reporting instance found.');
    end;
  end if;

  --find the tablespaces
  begin
    select tablespaceid
      into t_IndexTableSpaceId
      from datamart.Tablespaces
     where Name = 'DATAMARTINDEXES'
     and instanceid= t_InstanceId;

  exception
    when no_data_found then
      -- Create the tablespace
      t_IndexTablespaceId := sheriff.pkg_tablespaceupdate.new(t_InstanceId,
                                                              'DATAMARTINDEXES');
  end;

  begin
    select tablespaceid
      into t_DataTablespaceId
      from datamart.Tablespaces
     where Name = 'DATAMARTDATA'
      and instanceid= t_InstanceId;

  exception
    when no_data_found then
      -- Create the tablespace
      t_DataTablespaceId := sheriff.pkg_tablespaceupdate.new(t_InstanceId,
                                                             'DATAMARTDATA');
  end;

  begin
  --Generate new schemas if they don't already exist
    select schemaid
     into t_ABCSchema
    from datamart.schemas
    where name = 'ABCRW'
    and instanceid = t_InstanceId;

  exception
     when no_data_found then
     t_ABCSchema := sheriff.pkg_schemaupdate.new(t_InstanceId,
                                               'ABCRW');
  end;
  if c_TNSEntry is null then -- we only want more than this schema for the local instance
    begin
      select schemaid
       into t_BCPSchema
      from datamart.schemas
      where name = 'BCPCORRAL'
       and instanceid = t_InstanceId;

    exception
       when no_data_found then
       t_BCPSchema := sheriff.pkg_schemaupdate.new(t_InstanceId,
                                                 'BCPCORRAL');
    end;

     begin
      select schemaid
       into t_ExtensionSchema
      from datamart.schemas
      where name = 'EXTENSIONCORRAL'
        and instanceid = t_InstanceId;

    exception
       when no_data_found then
       t_ExtensionSchema := sheriff.pkg_schemaupdate.new(t_InstanceId,
                                                 'EXTENSIONCORRAL');
    end;

    begin
      select granteeid
       into t_PosseGrantee
      from datamart.grantees
      where name = 'POSSEUSER'
      and instanceid = t_InstanceId;
    exception
      when no_data_found then
        t_PosseGrantee := sheriff.pkg_granteeupdate.new(t_InstanceId,
                                                 'POSSEUSER');
    end;

    begin
      select granteeid
       into t_ExtensionsGrantee
      from datamart.grantees
      where name = 'POSSEEXTENSIONS'
      and instanceid = t_InstanceId;
    exception
      when no_data_found then
        t_ExtensionsGrantee := sheriff.pkg_granteeupdate.new(t_InstanceId,
                                                 'POSSEEXTENSIONS');
    end;

    begin
      select granteeid
       into t_DashboardGrantee
      from datamart.grantees
      where name = 'DASHBOARD'
      and instanceid = t_InstanceId;
    exception
      when no_data_found then
        t_DashboardGrantee := sheriff.pkg_granteeupdate.new(t_InstanceId,
                                                 'DASHBOARD');
    end;

  begin
      select granteeid
       into t_ABCRWGrantee
      from datamart.grantees
      where name = 'ABCRW'
      and instanceid = t_InstanceId;
    exception
      when no_data_found then
        t_ABCRWGrantee := sheriff.pkg_granteeupdate.new(t_InstanceId,
                                                 'ABCRW');
    end;

  begin
      select granteeid
       into t_ABCRWAdHocGrantee
      from datamart.grantees
      where name = 'ABCRWADHOC'
      and instanceid = t_InstanceId;
    exception
      when no_data_found then
        t_ABCRWAdHocGrantee := sheriff.pkg_granteeupdate.new(t_InstanceId,
                                                 'ABCRWADHOC');
    end;


  else -- If we have a TNSEntry, the grantee will be ABC Reporting role
    begin
      select granteeid
       into t_ABCREPORTINGGrantee
      from datamart.grantees
      where name = 'ABCREPORTING'
      and instanceid = t_InstanceId;
    exception
      when no_data_found then
        t_ABCREPORTINGGrantee := sheriff.pkg_granteeupdate.new(t_InstanceId,
                                                 'ABCREPORTING');
    end;
  end if;

  /********************************************* ABCRW TABLES ******************************************/
 --get tableids for tables to create on the instance for ABCRW; NON-LOCAL
 t_ABCRWTables := extension.udst_stringlist();
 if c_TNSEntry is null then
    t_ABCRWTABLES.extend(239);
   select
      'ABC_PERMIT', 'ACCESSGROUP', 'ACCUSATIONCREATEINSPECTIONXREF', 'ACCUSATIONJOB', 'ADDRESS', 'ADDRESS_WARNING_REL',
      'ADDRESSONLINEENTRY', 'AMENDMENTAPPCANCELLATION', 'AMENDMENTJOB', 'AMENDMENTJOB_PROTESTANT_REL', 'AMENDMENTJOB_RESPONSE_REL', 'AMENDMENTTYPE',
      'AMENDMENTTYPE_DOCUMENTTYPE_REL', 'AMENDMENTTYPE_QUESTION_REL', 'AMENDTYPE_QUESTION_REL', 'APPEALJOB', 'BATCHRENEWALNOT_LICENSE_REL', 'BATCHRENEWALNOT_PERMIT_REL',
      'BATCHRENEWALNOTIFICATIONJOB', 'CONDITION', 'CONDITIONTYPE', 'CONDITIONTYPE_LICENSETYPE_REL', 'COOPMEMBER', 'COOPMEMBER_PERMITAPP_REL',
      'COUNTY', 'CPLSUBMISSIONJOB', 'CPLSUBMISSIONJOB_LICENSE_REL', 'CPLSUBMISSIONJOB_PERMIT_REL', 'CPLSUBMISSIONPERIOD', 'CTSREPRESENTATIVE',
      'DISTRIBUTOR', 'DOCUMENTTYPE', 'DOCUMENTTYPE_EDOCSAMPLE_REL', 'EMAIL_DATA', 'ESTABLISHMENT', 'ESTABLISHMENT_EDOCUMENT_REL',
      'ESTABLISHMENTHISTORY', 'ESTABLISHMENTTYPE', 'EVENTDATE', 'EVENTTYPE', 'EXPIRATIONJOB', 'FN_DAILYDEPOSITJOB',
      'FN_ECOM_ACCUSATIONJOB_REL', 'FN_ECOM_AMENDMENTJOB_REL', 'FN_ECOM_APPEALJOB_REL', 'FN_ECOM_BATCHRENEWALNOTJOB_REL', 'FN_ECOM_CPLSUBMISSIONJOB_REL', 'FN_ECOM_DAILYDEPOSITJOB_REL',
      'FN_ECOM_EXPIRATIONJOB_REL', 'FN_ECOM_FEE_REL', 'FN_ECOM_INSPECTIONJOB_REL', 'FN_ECOM_MISCREVENUEJOB_REL', 'FN_ECOM_NEWAPPLICATIONJOB_REL', 'FN_ECOM_PERMITAPPJOB_REL',
      'FN_ECOM_PERMITRENEWALJOB_REL', 'FN_ECOM_PETITIONJOB_REL', 'FN_ECOM_PR_AMENDMENTJOB_REL', 'FN_ECOM_PR_APPLICATIONJOB_REL', 'FN_ECOM_PR_RENEWALJOB_REL', 'FN_ECOM_REINSTATEMENTJOB_REL',
      'FN_ECOM_RENEWALJOB_REL', 'FN_ECOMTRANSACTION', 'FN_FEE', /*'FN_FEE_FEETRANS',*/ 'FN_FEETRANSACTION', 'FN_GLACCOUNT',
      'FN_GLACCOUNTCATEGORY', 'FN_MISCREVENUEJOB', 'FN_PAYMENT', 'FN_PAYMENT_DAILYDEPOSITJOB_REL', 'FN_PAYMENT_PAYMENTNEWADJ_REL', 'FN_PAYMENT_PAYMENTREFUND_REL',
      'FN_PAYMENT_PAYMENTREVERSAL_REL', 'FN_PAYMENTADJ_PAYMENTNEW_REL', 'FN_PAYMENTADJ_PAYMENTREV_REL', 'FN_PAYMENTADJUSTMENT',  'FN_PAYMENTDIST_PAYMENTADJ_REL', 'FN_PAYMENTDISTRIBUTION',
      'FN_PAYMENTMETHOD', 'FN_PAYMENTTRANSACTION', 'JOBCANCELLATION', 'JOBS', 'LEGALENTITY', 'LEGALENTITY_EDOCUMENT_REL',
      'LEGALENTITY_LECORPSTR_REL', 'LEGALENTITY_WARNING_REL', 'LEGALENTITYCORPSTRUCTURE', 'LEGALENTITYTYPE', 'LICENSE', 'LICENSE_ADDTL_WAREHOUSE_REL',
      'LICENSE_ADDTL_WAREHS_HIST_REL', 'LICENSE_CONDITION_REL', 'LICENSE_EVENTDATE_REL', 'LICENSE_LICENSETYPESEC_REL', 'LICENSE_LICENSEWARNING_REL', 'LICENSE_VEHICLE_REL',
      'LICENSEBATCHRENEWALNOTIFXREF', 'LICENSECANCELLATION', 'LICENSEEPUBLICUSERXREF', 'LICENSEESTABLISHMENTHIST', 'LICENSEJOB', 'LICENSETYPE',
      'LICENSETYPE_DOCUMENTTYPE_REL', 'LICENSETYPE_LICENSETYPESEC_REL', 'LICENSETYPE_QUESTIONAPP_REL', 'LICENSETYPE_QUESTIONRENEW_REL', 'LICENSEWAREHOUSEADDRESS', 'LICENSEWARNING',
      'LICENSEWARNINGTYPE', 'MUNICIPALITY', 'NEWAPPCANCELLATION', 'NEWAPPCREATESINSPECTIONXREF', 'NEWAPPJOB_OLCORPSTRUCTURE_REL', 'NEWAPPJOB_PROTESTANT_REL',
      'NEWAPPJOB_RESPONSE_REL', 'NEWAPPJOB_VEHICLE_REL', 'NEWAPPLICATIONJOB',  'OFFICE', 'ONLINECORPORATESTRUCTURE', 'OWNERONLINE',
      'OWNERONLINE_PERMITAPP_REL', 'OWNERONLINE_PERMITREN_REL', 'PERMIT', 'PERMIT_CTSREP_REL', 'PERMIT_EVENTDATE_REL', 'PERMIT_EVENTDATERAINDATE_REL',
      'PERMIT_GENERATIONALLICENSE_REL', 'PERMIT_HISTORICLICENSECOOP_REL', 'PERMIT_LEGALENTITYOWNER_REL', 'PERMIT_LICENSECOOPLICENSE_REL', 'PERMIT_PERMITSOLICITOR_REL', 'PERMIT_PRODUCT_REL',
      'PERMIT_SPECIALCONDITIONS_REL', 'PERMIT_VEHICLE_REL', 'PERMITAPPJOB_COOPMEMBER_REL', 'PERMITAPPJOB_CTSREP_REL', 'PERMITAPPJOB_EVENTDATE_REL', 'PERMITAPPJOB_OLCORPSTR_REL',
      'PERMITAPPJOB_OWNERONLINE_REL', 'PERMITAPPJOB_PRODUCTONLINE_REL', 'PERMITAPPJOB_RESPONSE_REL', 'PERMITAPPJOB_SOLICITOROL_REL', 'PERMITAPPJOB_VEHICLE_REL', 'PERMITAPPLICATIONJOB',
      'PERMITCANCELLATION', 'PERMITLEGALTEXT', 'PERMITRENEWALJOB', 'PERMITRENEWALJOB_RESPONSE_REL', 'PERMITRENEWALJOB_VEHICLE_REL', 'PERMITRENEWJOB_COOPMEMBER_REL',
      'PERMITSPECIALCONDITIONS', 'PERMITTYPE', 'PERMITTYPE_DOCUMENTTYPE_REL', 'PERMITTYPE_LICENSETYPE_REL', 'PERMITTYPE_LICENSETYPECOOP_REL',
      'PERMITTYPE_PERMITTYPEAPPL_REL', 'PETITIONJOB', 'PETITIONJOB_PETITIONTYPE_REL', 'PETITIONTERM', 'PR_AMENDJOB_DISTRIBUTOR_REL',
      'PR_AMENDMENTJOB', 'PR_AMENDMENTJOB_PRODUCT_REL', 'PR_APPJOB_DISTRIBUTOR_REL', 'PR_APPJOB_OLCORPSTRUCTURE_REL', 'PR_APPLICATIONJOB', 'PR_APPLICATIONJOB_PRODUCT_REL',
      'PR_NONRENEWALJOB', 'PR_NONRENEWALJOB_PRODUCT_REL', 'PR_RENEWALJOB', 'PR_RENEWALJOB_PRODUCTXREF_REL', 'PROCESS', 'PROCESSES',
      'PRODUCT', 'PRODUCT_LICDISTRIBUTOR_REL', 'PRODUCT_LICDISTRIBUTORHIST_REL', 'PRODUCT_PRODUCTREVOCATION_REL', 'PRODUCT_VINTAGE_REL', 'PRODUCTONLINE',
      'PRODUCTONLINE_PERMITAPP_REL', 'PRODUCTREGISTRATIONJOB', 'PRODUCTREVOCATION', 'PRODUCTTYPE', 'PRODUCTXREF', 'PRODUCTXREF_VINTAGE_REL',
      'PROTESTANT', 'QUESTION', 'REGIONOFFICEXREF', 'REINSTATEJOB_PROTESTANT_REL', 'REINSTATEMENTJOB', 'RENEWALJOB',
      'RENEWALJOB_PROTESTANT_REL', 'RENEWALJOB_RESPONSE_REL', 'RENEWALJOB_VEHICLE_REL', 'RESPONSE', 'ROLE', 'ROLE_ACCESSGROUP_REL',
      'SOLICITORONLINE', 'SOLICITORONLINE_PERMITAPP_REL', 'SOLICITORONLINE_PERMITREN_REL', 'STREETTYPE', 'USER_ACCESSGROUP_REL', 'USER_LEGALENTITY_REL',
      'USER_POLICEMUNICIPALITY_REL', 'USER_PROCESSEMAIL_REL', 'USER_ROLE_REL', 'USERS', 'VARIANCEANDAPPEALS', 'VEHICLE',
      'VINTAGE', 'WARNING', 'WARNINGBATCH1218', 'WARNINGBATCH1218_LICENSE_REL', 'WARNINGBATCH1218_WARNING_REL', 'WARNINGBATCH1239',
      'WARNINGBATCH1239_LICENSE_REL', 'WARNINGBATCH1239_WARNING_REL', 'MUNICIPALRESPONSE', 'POLICERESPONSE', 'JOBTYPE', 'ABC_SMS',
      'PRODUCTAMENDMENTTYPE', 'PR_APPLICATIONJOB_RESPONSE_REL', 'PR_AMENDMENTJOB_RESPONSE_REL', 'PR_RENEWALJOB_RESPONSE_REL', 'PR_AMENDTYPE_QUESTION_REL',
      'JOBTYPE_QUESTION_REL', 'PR_AMENDMENTTYPE_DOCTYPE_REL', 'JOBTYPE_DOCTYPE_REL', 'PR_AMENDMENTTYPE_QUESTION_REL', 'PETITIONTYPE', 'PETITIONAMENDMENTTYPE',
      'PERMITTYPE_QUESTIONAPP_REL','PERMITTYPE_QUESTIONMUNI_REL','PERMITTYPE_QUESTIONPOLICE_REL','PERMITTYPE_QUESTIONRENEW_REL','LICENSETYPE_QUESTIONMUNI_REL',
      'LICENSETYPE_QUESTIONPOLICE_REL', 'MUNIREVIEWPROC_RESPONSE_REL', 'SUBMITRESOPROC_RESPONSE_REL', 'POLICEREVIEWPROC_RESPONSE_REL'
      --this does not include 'LICENSE', 'LICENSETYPE',
     into t_ABCRWTables(1),  t_ABCRWTables(2),  t_ABCRWTables(3),  t_ABCRWTables(4),  t_ABCRWTables(5),  t_ABCRWTables(6),  t_ABCRWTables(7),
          t_ABCRWTables(8),  t_ABCRWTables(9),  t_ABCRWTables(10), t_ABCRWTables(11), t_ABCRWTables(12), t_ABCRWTables(13), t_ABCRWTables(14),
          t_ABCRWTables(15), t_ABCRWTables(16), t_ABCRWTables(17), t_ABCRWTables(18), t_ABCRWTables(19), t_ABCRWTables(20), t_ABCRWTables(21),
          t_ABCRWTables(22), t_ABCRWTables(23), t_ABCRWTables(24), t_ABCRWTables(25), t_ABCRWTables(26), t_ABCRWTables(27), t_ABCRWTables(28),
          t_ABCRWTables(29), t_ABCRWTables(30), t_ABCRWTables(31), t_ABCRWTables(32), t_ABCRWTables(33), t_ABCRWTables(34), t_ABCRWTables(35),
          t_ABCRWTables(36), t_ABCRWTables(37), t_ABCRWTables(38), t_ABCRWTables(39), t_ABCRWTables(40), t_ABCRWTables(41), t_ABCRWTables(42),
          t_ABCRWTables(43), t_ABCRWTables(44), t_ABCRWTables(45), t_ABCRWTables(46), t_ABCRWTables(47), t_ABCRWTables(48), t_ABCRWTables(49),
          t_ABCRWTables(50), t_ABCRWTables(51), t_ABCRWTables(52), t_ABCRWTables(53), t_ABCRWTables(54), t_ABCRWTables(55), t_ABCRWTables(56),
          t_ABCRWTables(57), t_ABCRWTables(58), t_ABCRWTables(59), t_ABCRWTables(60), t_ABCRWTables(61), t_ABCRWTables(62), t_ABCRWTables(63),
          t_ABCRWTables(64), t_ABCRWTables(65), t_ABCRWTables(66), t_ABCRWTables(67), t_ABCRWTables(68), t_ABCRWTables(69), t_ABCRWTables(70),
          t_ABCRWTables(71), t_ABCRWTables(72), t_ABCRWTables(73), t_ABCRWTables(74), t_ABCRWTables(75), t_ABCRWTables(76), t_ABCRWTables(77),
          t_ABCRWTables(78), t_ABCRWTables(79), t_ABCRWTables(80), t_ABCRWTables(81), t_ABCRWTables(82), t_ABCRWTables(83), t_ABCRWTables(84),
          t_ABCRWTables(85), t_ABCRWTables(86), t_ABCRWTables(87), t_ABCRWTables(88), t_ABCRWTables(89), t_ABCRWTables(90), t_ABCRWTables(91),
          t_ABCRWTables(92), t_ABCRWTables(93), t_ABCRWTables(94), t_ABCRWTables(95), t_ABCRWTables(96), t_ABCRWTables(97), t_ABCRWTables(98),
          t_ABCRWTables(99), t_ABCRWTables(100), t_ABCRWTables(101), t_ABCRWTables(102), t_ABCRWTables(103), t_ABCRWTables(104), t_ABCRWTables(105),
          t_ABCRWTables(106), t_ABCRWTables(107), t_ABCRWTables(108), t_ABCRWTables(109), t_ABCRWTables(110), t_ABCRWTables(111), t_ABCRWTables(112),
          t_ABCRWTables(113), t_ABCRWTables(114), t_ABCRWTables(115), t_ABCRWTables(116), t_ABCRWTables(117), t_ABCRWTables(118), t_ABCRWTables(119),
          t_ABCRWTables(120), t_ABCRWTables(121), t_ABCRWTables(122), t_ABCRWTables(123), t_ABCRWTables(124), t_ABCRWTables(125), t_ABCRWTables(126),
          t_ABCRWTables(127), t_ABCRWTables(128), t_ABCRWTables(129), t_ABCRWTables(130), t_ABCRWTables(131), t_ABCRWTables(132), t_ABCRWTables(133),
          t_ABCRWTables(134), t_ABCRWTables(135), t_ABCRWTables(136), t_ABCRWTables(137), t_ABCRWTables(138), t_ABCRWTables(139), t_ABCRWTables(140),
          t_ABCRWTables(141), t_ABCRWTables(142), t_ABCRWTables(143), t_ABCRWTables(144), t_ABCRWTables(145), t_ABCRWTables(146), t_ABCRWTables(147),
          t_ABCRWTables(148), t_ABCRWTables(149), t_ABCRWTables(150), t_ABCRWTables(151), t_ABCRWTables(152), t_ABCRWTables(153), t_ABCRWTables(154),
          t_ABCRWTables(155), t_ABCRWTables(156), t_ABCRWTables(157), t_ABCRWTables(158), t_ABCRWTables(159), t_ABCRWTables(160), t_ABCRWTables(161),
          t_ABCRWTables(162), t_ABCRWTables(163), t_ABCRWTables(164), t_ABCRWTables(165), t_ABCRWTables(166), t_ABCRWTables(167), t_ABCRWTables(168),
          t_ABCRWTables(169), t_ABCRWTables(170), t_ABCRWTables(171), t_ABCRWTables(172), t_ABCRWTables(173), t_ABCRWTables(174), t_ABCRWTables(175),
          t_ABCRWTables(176), t_ABCRWTables(177), t_ABCRWTables(178), t_ABCRWTables(179), t_ABCRWTables(180), t_ABCRWTables(181), t_ABCRWTables(182),
          t_ABCRWTables(183), t_ABCRWTables(184), t_ABCRWTables(185), t_ABCRWTables(186), t_ABCRWTables(187), t_ABCRWTables(188), t_ABCRWTables(189),
		      t_ABCRWTables(190), t_ABCRWTables(191), t_ABCRWTables(192), t_ABCRWTables(193), t_ABCRWTables(194), t_ABCRWTables(195), t_ABCRWTables(196),
          t_ABCRWTables(197), t_ABCRWTables(198), t_ABCRWTables(199), t_ABCRWTables(200), t_ABCRWTables(201), t_ABCRWTables(202), t_ABCRWTables(203),
          t_ABCRWTables(204), t_ABCRWTables(205), t_ABCRWTables(206), t_ABCRWTables(207), t_ABCRWTables(208), t_ABCRWTables(209), t_ABCRWTables(210),
          t_ABCRWTables(211), t_ABCRWTables(212), t_ABCRWTables(213), t_ABCRWTables(214), t_ABCRWTables(215), t_ABCRWTables(216), t_ABCRWTables(217),
          t_ABCRWTables(218), t_ABCRWTables(219), t_ABCRWTables(220), t_ABCRWTables(221), t_ABCRWTables(222), t_ABCRWTables(223), t_ABCRWTables(224),
          t_ABCRWTables(225), t_ABCRWTables(226), t_ABCRWTables(227), t_ABCRWTables(228), t_ABCRWTables(229), t_ABCRWTables(230), t_ABCRWTables(231),
          t_ABCRWTables(232), t_ABCRWTables(233), t_ABCRWTables(234), t_ABCRWTables(235), t_ABCRWTables(236), t_ABCRWTables(237), t_ABCRWTables(238),
          t_ABCRWTables(239)
     from dual;
  else
   t_ABCRWTABLES.extend(211);
   select 'ACCESSGROUP', 'ACCUSATIONJOB', 'ADDRESS', 'ADDRESS_WARNING_REL', 'ADDRESSONLINEENTRY', 'AMENDMENTJOB', 'AMENDMENTJOB_PROTESTANT_REL',
          'AMENDMENTJOB_RESPONSE_REL', 'AMENDMENTTYPE', 'AMENDMENTTYPE_DOCUMENTTYPE_REL', 'AMENDMENTTYPE_QUESTION_REL', 'AMENDTYPE_QUESTION_REL', 'APPEALJOB',
      'BATCHRENEWALNOT_LICENSE_REL', 'BATCHRENEWALNOT_PERMIT_REL', 'BATCHRENEWALNOTIFICATIONJOB', 'CONDITION', 'CONDITIONTYPE',
      'CONDITIONTYPE_LICENSETYPE_REL', 'COOPMEMBER', 'COOPMEMBER_PERMITAPP_REL', 'COUNTY', 'CPLSUBMISSIONJOB', 'CPLSUBMISSIONJOB_LICENSE_REL', 'CPLSUBMISSIONJOB_PERMIT_REL',
      'CPLSUBMISSIONPERIOD', 'CTSREPRESENTATIVE', 'DISTRIBUTOR', 'DOCUMENTTYPE', 'ESTABLISHMENT', 'ESTABLISHMENTHISTORY', 'ESTABLISHMENTTYPE', 'EVENTDATE', 'EVENTTYPE', 'EXPIRATIONJOB',
      'FN_DAILYDEPOSITJOB', 'FN_ECOM_ACCUSATIONJOB_REL', 'FN_ECOM_AMENDMENTJOB_REL', 'FN_ECOM_APPEALJOB_REL', 'FN_ECOM_BATCHRENEWALNOTJOB_REL',
      'FN_ECOM_CPLSUBMISSIONJOB_REL', 'FN_ECOM_DAILYDEPOSITJOB_REL', 'FN_ECOM_EXPIRATIONJOB_REL', 'FN_ECOM_FEE_REL',
      'FN_ECOM_MISCREVENUEJOB_REL', 'FN_ECOM_NEWAPPLICATIONJOB_REL', 'FN_ECOM_PERMITAPPJOB_REL', 'FN_ECOM_PERMITRENEWALJOB_REL',
      'FN_ECOM_PETITIONJOB_REL', 'FN_ECOM_PR_AMENDMENTJOB_REL', 'FN_ECOM_PR_APPLICATIONJOB_REL', 'FN_ECOM_PR_RENEWALJOB_REL',
      'FN_ECOM_REINSTATEMENTJOB_REL', 'FN_ECOM_RENEWALJOB_REL', 'FN_ECOMTRANSACTION', 'FN_FEE', 'FN_FEETRANSACTION', 'FN_GLACCOUNT',
      'FN_GLACCOUNTCATEGORY', 'FN_MISCREVENUEJOB', 'FN_PAYMENT', 'FN_PAYMENT_DAILYDEPOSITJOB_REL', 'FN_PAYMENT_PAYMENTNEWADJ_REL',
      'FN_PAYMENT_PAYMENTREFUND_REL', 'FN_PAYMENT_PAYMENTREVERSAL_REL', 'FN_PAYMENTADJ_PAYMENTNEW_REL', 'FN_PAYMENTADJ_PAYMENTREV_REL',
      'FN_PAYMENTADJUSTMENT', 'FN_PAYMENTDIST_PAYMENTADJ_REL', 'FN_PAYMENTDISTRIBUTION', 'FN_PAYMENTMETHOD', 'FN_PAYMENTTRANSACTION',
      'LEGALENTITY', 'LEGALENTITY_LECORPSTR_REL', 'LEGALENTITY_WARNING_REL', 'LEGALENTITYTYPE', 'LICENSE_ADDTL_WAREHOUSE_REL', 'LICENSE_ADDTL_WAREHS_HIST_REL', 'LICENSE_CONDITION_REL',
      'LICENSE_EVENTDATE_REL', 'LICENSE_LICENSETYPESEC_REL', 'LICENSE_LICENSEWARNING_REL', 'LICENSE_VEHICLE_REL', 'LICENSECANCELLATION',
      'LICENSETYPE_DOCUMENTTYPE_REL', 'LICENSETYPE_LICENSETYPESEC_REL', 'LICENSETYPE_QUESTIONAPP_REL', 'LICENSETYPE_QUESTIONRENEW_REL', 'LICENSEWARNING',
      'LICENSEWARNINGTYPE', 'LICENSEESTABLISHMENTHIST', 'MUNICIPALITY', 'NEWAPPJOB_OLCORPSTRUCTURE_REL', 'NEWAPPJOB_PROTESTANT_REL', 'NEWAPPJOB_RESPONSE_REL',
      'NEWAPPJOB_VEHICLE_REL', 'NEWAPPLICATIONJOB', 'ONLINECORPORATESTRUCTURE', 'OWNERONLINE', 'OWNERONLINE_PERMITAPP_REL', 'PERMIT',
      'PERMIT_CTSREP_REL', 'PERMIT_EVENTDATE_REL', 'PERMIT_EVENTDATERAINDATE_REL', 'PERMIT_GENERATIONALLICENSE_REL', 'PERMIT_HISTORICLICENSECOOP_REL', 'PERMIT_LEGALENTITYOWNER_REL',
      'PERMIT_LICENSECOOPLICENSE_REL', 'PERMIT_PERMITSOLICITOR_REL', 'PERMIT_PRODUCT_REL', 'PERMIT_SPECIALCONDITIONS_REL',
      'PERMIT_VEHICLE_REL', 'PERMITAPPJOB_COOPMEMBER_REL', 'PERMITAPPJOB_CTSREP_REL', 'PERMITAPPJOB_EVENTDATE_REL',
      'PERMITAPPJOB_OLCORPSTR_REL', 'PERMITAPPJOB_OWNERONLINE_REL', 'PERMITAPPJOB_PRODUCTONLINE_REL', 'PERMITAPPJOB_RESPONSE_REL',
      'PERMITAPPJOB_SOLICITOROL_REL', 'PERMITAPPJOB_VEHICLE_REL', 'PERMITAPPLICATIONJOB', 'PERMITCANCELLATION', 'PERMITLEGALTEXT',
      'PERMITRENEWALJOB', 'PERMITRENEWALJOB_RESPONSE_REL', 'PERMITRENEWALJOB_VEHICLE_REL', 'PERMITRENEWJOB_COOPMEMBER_REL',
      'PERMITSPECIALCONDITIONS', 'PERMITTYPE', 'PERMITTYPE_DOCUMENTTYPE_REL', 'PERMITTYPE_LICENSETYPE_REL', 'PERMITTYPE_LICENSETYPECOOP_REL', 'PERMITTYPE_PERMITTYPEAPPL_REL',
      'PETITIONJOB', 'PETITIONJOB_PETITIONTYPE_REL', 'PETITIONTERM', 'PR_AMENDJOB_DISTRIBUTOR_REL', 'PR_AMENDMENTJOB', 'PR_AMENDMENTJOB_PRODUCT_REL',
      'PR_APPJOB_DISTRIBUTOR_REL', 'PR_APPJOB_OLCORPSTRUCTURE_REL', 'PR_APPLICATIONJOB', 'PR_APPLICATIONJOB_PRODUCT_REL', 'PR_NONRENEWALJOB',
      'PR_NONRENEWALJOB_PRODUCT_REL', 'PR_RENEWALJOB', 'PR_RENEWALJOB_PRODUCTXREF_REL', 'PRODUCT', 'PRODUCT_LICDISTRIBUTOR_REL',
      'PRODUCT_LICDISTRIBUTORHIST_REL', 'PRODUCT_PRODUCTREVOCATION_REL', 'PRODUCT_VINTAGE_REL', 'PRODUCTONLINE', 'PRODUCTONLINE_PERMITAPP_REL', 'PRODUCTREVOCATION', 'PRODUCTTYPE', 'PRODUCTXREF',
      'PRODUCTXREF_VINTAGE_REL', 'PROTESTANT', 'QUESTION', 'REINSTATEJOB_PROTESTANT_REL', 'REINSTATEMENTJOB', 'RENEWALJOB',
      'RENEWALJOB_PROTESTANT_REL', 'RENEWALJOB_RESPONSE_REL', 'RENEWALJOB_VEHICLE_REL', 'RESPONSE', 'ROLE', 'ROLE_ACCESSGROUP_REL',
      'SOLICITORONLINE', 'SOLICITORONLINE_PERMITAPP_REL', 'STREETTYPE', 'USER_ACCESSGROUP_REL', 'USER_LEGALENTITY_REL',
      'USER_ROLE_REL', 'USERS', 'VEHICLE', 'VINTAGE', 'WARNING',
      'LICENSE', 'LICENSETYPE',
      'LICENSEJOB', 'PROCESSES', 'PROCESS', 'JOBS','LICENSEWAREHOUSEADDRESS','WARNINGBATCH1218','WARNINGBATCH1239','WARNINGBATCH1218_LICENSE_REL','WARNINGBATCH1239_LICENSE_REL',
      'WARNINGBATCH1218_WARNING_REL','WARNINGBATCH1239_WARNING_REL','OWNERONLINE_PERMITREN_REL','SOLICITORONLINE_PERMITREN_REL','EMAIL_DATA','USER_POLICEMUNICIPALITY_REL',
      'JOBCANCELLATION','AMENDMENTAPPCANCELLATION','NEWAPPCANCELLATION', 'PETITIONTYPE', 'PETITIONAMENDMENTTYPE',
      'PERMITTYPE_QUESTIONAPP_REL','PERMITTYPE_QUESTIONMUNI_REL','PERMITTYPE_QUESTIONPOLICE_REL','PERMITTYPE_QUESTIONRENEW_REL','LICENSETYPE_QUESTIONMUNI_REL',
      'LICENSETYPE_QUESTIONPOLICE_REL', 'MUNIREVIEWPROC_RESPONSE_REL', 'SUBMITRESOPROC_RESPONSE_REL', 'POLICEREVIEWPROC_RESPONSE_REL'
     into t_ABCRWTables(1),  t_ABCRWTables(2),  t_ABCRWTables(3),  t_ABCRWTables(4),  t_ABCRWTables(5),  t_ABCRWTables(6),  t_ABCRWTables(7),
          t_ABCRWTables(8),  t_ABCRWTables(9),  t_ABCRWTables(10), t_ABCRWTables(11), t_ABCRWTables(12), t_ABCRWTables(13), t_ABCRWTables(14),
          t_ABCRWTables(15), t_ABCRWTables(16), t_ABCRWTables(17), t_ABCRWTables(18), t_ABCRWTables(19), t_ABCRWTables(20), t_ABCRWTables(21),
          t_ABCRWTables(22), t_ABCRWTables(23), t_ABCRWTables(24), t_ABCRWTables(25), t_ABCRWTables(26), t_ABCRWTables(27), t_ABCRWTables(28),
          t_ABCRWTables(29), t_ABCRWTables(30), t_ABCRWTables(31), t_ABCRWTables(32), t_ABCRWTables(33), t_ABCRWTables(34), t_ABCRWTables(35),
          t_ABCRWTables(36), t_ABCRWTables(37), t_ABCRWTables(38), t_ABCRWTables(39), t_ABCRWTables(40), t_ABCRWTables(41), t_ABCRWTables(42),
          t_ABCRWTables(43), t_ABCRWTables(44), t_ABCRWTables(45), t_ABCRWTables(46), t_ABCRWTables(47), t_ABCRWTables(48), t_ABCRWTables(49),
          t_ABCRWTables(50), t_ABCRWTables(51), t_ABCRWTables(52), t_ABCRWTables(53), t_ABCRWTables(54), t_ABCRWTables(55), t_ABCRWTables(56),
          t_ABCRWTables(57), t_ABCRWTables(58), t_ABCRWTables(59), t_ABCRWTables(60), t_ABCRWTables(61), t_ABCRWTables(62), t_ABCRWTables(63),
          t_ABCRWTables(64), t_ABCRWTables(65), t_ABCRWTables(66), t_ABCRWTables(67), t_ABCRWTables(68), t_ABCRWTables(69), t_ABCRWTables(70),
          t_ABCRWTables(71), t_ABCRWTables(72), t_ABCRWTables(73), t_ABCRWTables(74), t_ABCRWTables(75), t_ABCRWTables(76), t_ABCRWTables(77),
          t_ABCRWTables(78), t_ABCRWTables(79), t_ABCRWTables(80), t_ABCRWTables(81), t_ABCRWTables(82), t_ABCRWTables(83), t_ABCRWTables(84),
          t_ABCRWTables(85), t_ABCRWTables(86), t_ABCRWTables(87), t_ABCRWTables(88), t_ABCRWTables(89), t_ABCRWTables(90), t_ABCRWTables(91),
          t_ABCRWTables(92), t_ABCRWTables(93), t_ABCRWTables(94), t_ABCRWTables(95), t_ABCRWTables(96), t_ABCRWTables(97), t_ABCRWTables(98),
          t_ABCRWTables(99), t_ABCRWTables(100), t_ABCRWTables(101), t_ABCRWTables(102), t_ABCRWTables(103), t_ABCRWTables(104), t_ABCRWTables(105),
          t_ABCRWTables(106), t_ABCRWTables(107), t_ABCRWTables(108), t_ABCRWTables(109), t_ABCRWTables(110), t_ABCRWTables(111), t_ABCRWTables(112),
          t_ABCRWTables(113), t_ABCRWTables(114), t_ABCRWTables(115), t_ABCRWTables(116), t_ABCRWTables(117), t_ABCRWTables(118), t_ABCRWTables(119),
          t_ABCRWTables(120), t_ABCRWTables(121), t_ABCRWTables(122), t_ABCRWTables(123), t_ABCRWTables(124), t_ABCRWTables(125), t_ABCRWTables(126),
          t_ABCRWTables(127), t_ABCRWTables(128), t_ABCRWTables(129), t_ABCRWTables(130), t_ABCRWTables(131), t_ABCRWTables(132), t_ABCRWTables(133),
          t_ABCRWTables(134), t_ABCRWTables(135), t_ABCRWTables(136), t_ABCRWTables(137), t_ABCRWTables(138), t_ABCRWTables(139), t_ABCRWTables(140),
          t_ABCRWTables(141), t_ABCRWTables(142), t_ABCRWTables(143), t_ABCRWTables(144), t_ABCRWTables(145), t_ABCRWTables(146), t_ABCRWTables(147),
          t_ABCRWTables(148), t_ABCRWTables(149), t_ABCRWTables(150), t_ABCRWTables(151), t_ABCRWTables(152), t_ABCRWTables(153), t_ABCRWTables(154),
          t_ABCRWTables(155), t_ABCRWTables(156), t_ABCRWTables(157), t_ABCRWTables(158), t_ABCRWTables(159), t_ABCRWTables(160), t_ABCRWTables(161),
          t_ABCRWTables(162), t_ABCRWTables(163), t_ABCRWTables(164), t_ABCRWTables(165), t_ABCRWTables(166), t_ABCRWTables(167), t_ABCRWTables(168),
          t_ABCRWTables(169), t_ABCRWTables(170), t_ABCRWTables(171), t_ABCRWTables(172), t_ABCRWTables(173), t_ABCRWTables(174), t_ABCRWTables(175),
          t_ABCRWTables(176), t_ABCRWTables(177), t_ABCRWTables(178), t_ABCRWTables(179), t_ABCRWTables(180), t_ABCRWTables(181), t_ABCRWTables(182),
          t_ABCRWTables(183), t_ABCRWTables(184), t_ABCRWTables(185), t_ABCRWTables(186), t_ABCRWTables(187), t_ABCRWTables(188), t_ABCRWTables(189),
          t_ABCRWTables(190), t_ABCRWTables(191), t_ABCRWTables(192), t_ABCRWTables(193), t_ABCRWTables(194), t_ABCRWTables(195), t_ABCRWTables(196),
          t_ABCRWTables(197), t_ABCRWTables(198), t_ABCRWTables(199), t_ABCRWTables(200), t_ABCRWTables(201), t_ABCRWTables(202), t_ABCRWTables(203),
          t_ABCRWTables(204), t_ABCRWTables(205), t_ABCRWTables(206), t_ABCRWTables(207), t_ABCRWTables(208), t_ABCRWTables(209), t_ABCRWTables(210),
          t_ABCRWTables(211)
     from dual;
  end if;
    for x in (select TableId
                from datamart.Tables t
                join table(cast(t_ABCRWTAbles as extension.udst_stringlist)) tabs
                on t.name = tabs.column_value) loop
     begin
       --First check to see if the table already exists
       select tableid
        into t_TableId
       from datamart.instancetables
       where tableid = x.tableid
       and instanceid = t_InstanceId;
     exception -- if not, add it to the abc schema
       when no_data_found then
       sheriff.pkg_instancetableupdate.new(t_InstanceId,
                                           x.tableid,
                                           t_ABCSchema,
                                           t_DataTablespaceId,
                                           t_IndexTablespaceId);
    end;
    if c_TNSEntry is null then
      /**************************************  ABCRW Grants ***********************************/
      /********** POSSEUSER **********/
      --We don't want to add grants twice
      begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_PosseGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
        sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                 x.tableid,
                                                 t_PosseGrantee,
                                                 'N');
      end;
      /********** POSSEEXTENSIONS ****/
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_ExtensionsGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
        sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                 x.tableid,
                                                 t_ExtensionsGrantee,
                                                 'N');
      end;
      /********** DASHBOARD **********/
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_DashboardGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
          sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                   x.tableid,
                                                   t_DashboardGrantee,
                                                   'N');
      end;
      /********** ABCRW **********/
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_ABCRWGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
          sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                   x.tableid,
                                                   t_ABCRWGrantee,
                                                   'N');
      end;
      /********** ABCRW Ad Hoc **********/
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_ABCRWAdHocGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
          sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                   x.tableid,
                                                   t_ABCRWAdHocGrantee,
                                                   'N');
      end;
    else -- if this is the remote instance, grant to ABCReporting
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_ABCREPORTINGGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
          sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                   x.tableid,
                                                   t_ABCREPORTINGGrantee,
                                                   'N');
      end;
    end if;
    /***************************** ABCRW INDEXES *********************************/
      for y in (select IndexId
                  from datamart.TableIndexes ti
                 where ti.TableId = x.tableid) loop
        begin  --Only add the index if it has not been already
          select i.IndexId
           into t_IndexId
          from datamart.instanceindexes i
          where i.IndexId = y.indexid
          and i.instanceid = t_InstanceId;
        exception
          when no_data_found then
          sheriff.pkg_instanceindexupdate.new(t_InstanceId,
                                              y.indexid,
                                              t_IndexTablespaceId);
        end;
      end loop;
    end loop;


 if c_TNSEntry is null then
   /**************************************************** BCP TABLES ******************************************************/
    /********** From BCP to ABCRW view tables **********/
      for z in (select t.TableId
                  from datamart.Tables t
                 where t.Name in (
                     'LICENSE',
                     'LICENSETYPE'
                     )) loop
       begin
        select g.GranteeId
          into t_GrantId
          from datamart.instancetablegrants g
         where g.tableid = z.tableid
       and g.GranteeId = t_ABCRWGrantee
           and instanceid = t_InstanceId;
       exception
        when no_data_found then
        sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                 z.tableid,
                                                 t_ABCRWGrantee,
                                                 'Y');
       end;
      end loop;

      /**************** Other Tables **************/
      for x in (select t.TableId, t.name
                  from datamart.Tables t
                 where t.Name in (
                     'LICENSE',
                     'LICENSEJOB',
                     'PROCESSES',
                     'LICENSETYPE'
                     )) loop
      begin
        select tableid
         into t_TableId
        from datamart.instancetables
        where tableid = x.tableid
        and instanceid = t_InstanceId;
      exception -- if not, add it to the BCP schema
        when no_data_found then
        sheriff.pkg_instancetableupdate.new(t_InstanceId,
                                            x.tableid,
                                            t_BCPSchema,
                                            t_DataTablespaceId,
                                            t_IndexTablespaceId);
      end;
       /**************************************  BCP Grants ***********************************/
      /********** POSSEUSER **********/
      --We don't want to add grants twice
      begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_PosseGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
        sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                 x.tableid,
                                                 t_PosseGrantee,
                                                 'N');
      end;
      /********** POSSEEXTENSIONS ****/
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_ExtensionsGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
        sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                 x.tableid,
                                                 t_ExtensionsGrantee,
                                                 'N');
      end;
      /********** DASHBOARD **********/
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_DashboardGrantee
        and instanceid = t_InstanceId;
      exception
        when no_data_found then
          sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                   x.tableid,
                                                   t_DashboardGrantee,
                                                   'N');
      end;

      /***************************** BCP INDEXES *********************************/
        for y in (select IndexId
                    from datamart.TableIndexes ti
                   where ti.TableId = x.tableid) loop
          begin  --Only add the index if it has not been already
            select i.IndexId
             into t_IndexId
            from datamart.instanceindexes i
            where i.IndexId = y.indexid
            and i.instanceid = t_InstanceId;
          exception
            when no_data_found then
            sheriff.pkg_instanceindexupdate.new(t_InstanceId,
                                                y.indexid,
                                                t_IndexTablespaceId);
          end;
        end loop;
      end loop;


  /***************************** EXTENSION TABLES ******************************/
       for x in (select TableId
                  from datamart.Tables t
                 where t.Name in (
                     'PROCESS',
                     'JOBS')) loop
      begin
        select tableid
         into t_TableId
        from datamart.instancetables
        where tableid = x.tableid and instanceid = t_InstanceId;
      exception -- if not, add it to the Extension schema
        when no_data_found then
        sheriff.pkg_instancetableupdate.new(t_InstanceId,
                                            x.tableid,
                                            t_ExtensionSchema,
                                            t_DataTablespaceId,
                                            t_IndexTablespaceId);
      end;
        /**************************************  EXTENSION Grants ***********************************/
      /********** POSSEUSER **********/
      --We don't want to add grants twice
      begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_PosseGrantee
      and instanceid = t_InstanceId;
      exception
        when no_data_found then
        sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                 x.tableid,
                                                 t_PosseGrantee,
                                                 'N');
      end;
      /********** POSSEEXTENSIONS ****/
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_ExtensionsGrantee
      and instanceid = t_InstanceId;
      exception
        when no_data_found then
        sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                 x.tableid,
                                                 t_ExtensionsGrantee,
                                                 'N');
      end;
      /********** DASHBOARD **********/
       begin
        select g.GranteeId
         into t_GrantId
        from datamart.instancetablegrants g
        where g.tableid = x.tableid and g.GranteeId = t_DashboardGrantee
      and instanceid = t_InstanceId;
      exception
        when no_data_found then
          sheriff.pkg_instancetablegrantupdate.new(t_InstanceId,
                                                   x.tableid,
                                                   t_DashboardGrantee,
                                                   'N');
      end;
      /***************************** EXTENSION INDEXES *********************************/
        for y in (select IndexId
                    from datamart.TableIndexes ti
                   where ti.TableId = x.tableid) loop
          begin  --Only add the index if it has not been already
            select i.IndexId
             into t_IndexId
            from datamart.instanceindexes i
            where i.IndexId = y.indexid
        and i.instanceid = t_InstanceId;
          exception
            when no_data_found then
            sheriff.pkg_instanceindexupdate.new(t_InstanceId,
                                                y.indexid,
                                                t_IndexTablespaceId);
          end;
        end loop;
      end loop;
    end if;

  --Commit the changes
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

end;
