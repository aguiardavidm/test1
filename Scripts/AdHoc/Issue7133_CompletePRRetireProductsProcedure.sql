-- Created on 01/07/2016 by MICHEL.SCHEFFERS
-- Set all non-complete p_ABC_PRRetireProducts to Complete with outcome Products Retired.
declare 
  -- Local variables here
  t_Outcome varchar2(100):= 'Products Retired'; -- Outcome
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  for p in (select p.ObjectId
              from query.p_ABC_PRRetireProducts p
             where p.ProcessStatus != 'Complete') loop
     api.pkg_processupdate.Complete(p.Objectid, t_Outcome);
     dbms_output.put_line('Completing process ' || p.objectid);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
