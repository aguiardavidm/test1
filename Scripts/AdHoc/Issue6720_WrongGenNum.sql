declare
  t_11997Lic number := 31432535;
  t_10368Lic number := 30775046;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  api.pkg_columnupdate.SetValue(t_11997Lic, 'State', 'Active');
  api.pkg_columnupdate.SetValue(t_11997Lic, 'LicenseNumber', '1220-33-011-005');
  api.pkg_columnupdate.SetValue(t_11997Lic, 'IsLatestVersion', 'Y');
  
  
  api.pkg_columnupdate.SetValue(t_10368Lic, 'EffectiveDate', to_date('01/15/2015', 'mm/dd/yyyy'));
  api.pkg_columnupdate.SetValue(t_10368Lic, 'ExpirationDate', to_date('06/30/2015', 'mm/dd/yyyy'));
  --deactivate the current license
  api.pkg_columnupdate.SetValue(33254765, 'State', 'Closed');
  api.pkg_columnupdate.SetValue(33254765, 'IsLatestVersion', 'N');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;