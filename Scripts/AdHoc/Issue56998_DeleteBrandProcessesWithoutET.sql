/*-----------------------------------------------------------------------------
 * Expected run time: <10 Sec in dev
 * Purpose: Remove all brands processes that dont have email templates
 *---------------------------------------------------------------------------*/
declare
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_ProcessIdList                       udt_IdList;
  
  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
  end;
  
  procedure deleteProcesses(
    t_IdList                            udt_IdList
  ) is
  begin 
    for i in 1..t_IdList.count() loop
      api.pkg_processupdate.Remove(t_IdList(i));
    end loop;
  end;

begin
  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  
  select objectid 
  bulk collect into t_ProcessIdList
  from query.p_abc_sendconfirmationletter p
  where p.outcome is null 
    and p.EmailTemplateObjectId is null;
  
  deleteProcesses(t_ProcessIdList);
  
  select objectid 
  bulk collect into t_ProcessIdList
  from query.p_abc_sendrejectnotification p
  where p.outcome is null 
    and p.EmailTemplateObjectId is null;
  
  deleteProcesses(t_ProcessIdList);
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  --commit;
end;
