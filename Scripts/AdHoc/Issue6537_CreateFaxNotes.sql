 create or replace package pkg_BoundaryRealignment_Utils as
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_EndPointInfo is rel.pkg_Definition.udt_EndPointInfo;
  subtype udt_EndPoint is rel.pkg_Definition.udt_EndPoint;
  subtype udt_ObjectInfo is objmodelphys.pkg_Definition.udt_ObjectInfo;

  ---------------------------------------------------------------------------
  -- New()
  ---------------------------------------------------------------------------
  function New (
    a_ToEndPointId      udt_Id,
    a_FromObject      udt_Id,
    a_ToObject        udt_Id,
    a_AsOfDate        date default sysdate
  ) return udt_Id;

  ---------------------------------------------------------------------------
  -- New()
  ---------------------------------------------------------------------------
  function New (
    a_RelationshipDefID        udt_Id,
    a_FromObjectID             udt_Id,
    a_ToObjectID               udt_Id,
    a_FromEndPointID           udt_Id,
    a_ToEndPointID             udt_Id,
    a_AsOfDate                 date default sysdate
  ) return udt_Id;

  ---------------------------------------------------------------------------
  -- New()
  ---------------------------------------------------------------------------
  function New (
    a_RelationshipDef           udt_Id,
    a_ToEndPoint                udt_EndPoint,
    a_FromObject                udt_Id,
    a_ToObject                  udt_Id,
    a_AsOfDate                  date
  ) return udt_Id;

  ---------------------------------------------------------------------------
  -- New()
  ---------------------------------------------------------------------------
  procedure New (
    a_FromObject                udt_Id,
    a_ToObject                  udt_Id,
    a_RelationshipDef           varchar2);

  ----------------------------------------------------------------------------
  -- DeleteRels()
  ----------------------------------------------------------------------------
  function DeleteRels (
    a_RelIds                            udt_IdList,
    a_FromIds                           udt_IdList,
    a_ToIds                             udt_IdList,
    a_RelDefIds                         udt_IdList,
    a_EPIds                             udt_IdList
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * SetColumnValue() -- PUBLIC
   *   Set the value of a stored column.
   *   Only handles char & number!
   *   Does not handle Audit!
   *-------------------------------------------------------------------------*/
  procedure SetColumnValue (
    a_ObjectId                          udt_Id,
    a_ColumnDefId                       udt_Id,
    a_Value                             varchar2
  );

  ----------------------------------------------------------------------------
  -- NewNote() -- PUBLIC
  ----------------------------------------------------------------------------
  function NewNote (
    a_ObjectId                          udt_Id,
    a_NoteDefId                         udt_Id,
    a_Locked                            boolean,
    a_Text                              varchar2
  ) return udt_Id;

end pkg_BoundaryRealignment_Utils;
/
create or replace package body pkg_BoundaryRealignment_Utils as

  ---------------------------------------------------------------------------
  -- Types
  ---------------------------------------------------------------------------
  type StringIndexedPLSList is table of pls_integer index by varchar2(100);
  subtype udt_StringIndexedPLSList is StringIndexedPLSList;
  subtype udt_ObjectDefInfo is objmodelphys.pkg_Definition.udt_ObjectDefInfo;
  subtype udt_StringList is api.pkg_definition.udt_StringList;

  ---------------------------------------------------------------------------
  -- Variables
  ---------------------------------------------------------------------------
  t_RelDefCache                         udt_StringIndexedPLSList;
  t_FromEndPointCache                   udt_StringIndexedPLSList;
  t_ToEndPointCache                     udt_StringIndexedPLSList;

  ---------------------------------------------------------------------------
  -- New()
  ---------------------------------------------------------------------------
  function New (
    a_RelationshipDefID                 udt_Id,
    a_FromObjectID                      udt_Id,
    a_ToObjectID                        udt_Id,
    a_FromEndPointID                    udt_Id,
    a_ToEndPointID                      udt_Id,
    a_AsOfDate                          date default sysdate
  ) return udt_ID is
    t_LogicalTransactionId              udt_Id;
    t_RelationshipID                    udt_Id;
  begin
    select possedata.ObjectId_s.nextval
      into t_RelationshipID from dual;

    t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();

    posseaudit.pkg_Update.EnsureAuditIsInitialized(a_FromObjectId);
    posseaudit.pkg_Update.EnsureAuditIsInitialized(a_ToObjectId);

    insert into possedata.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId
    ) values (
      t_LogicalTransactionId,
      t_LogicalTransactionId,
      t_RelationshipID,
      a_RelationshipDefID,
      4,
      4,
      a_RelationshipDefID,
      4,
      a_RelationshipDefID
    );

    posseaudit.pkg_Update.SetObjectInSync(t_RelationshipID,
        a_RelationshipDefID, 4);

    posseaudit.pkg_Update.ObjectNew(t_RelationshipID);
/*    posseaudit.pkg_Update.CorrectDateRange(t_RelationshipID, null,
        null, t_StoredColumns);*/

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipID,
      a_ToEndPointID,
      a_FromObjectID,
      a_ToObjectID
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipID,
      a_FromEndPointID,
      a_ToObjectID,
      a_FromObjectID
    );

    posseaudit.pkg_Update.StoredRelationshipNew(a_FromObjectId,
        a_ToEndPointID, t_RelationshipId, a_RelationshipDefID,
        a_ToObjectId);
    posseaudit.pkg_Update.StoredRelationshipNew(a_ToObjectId,
        a_FromEndPointID, t_RelationshipId, a_RelationshipDefID,
        a_FromObjectId);

   return t_RelationshipID;
 end;

  ---------------------------------------------------------------------------
  -- New()
  ---------------------------------------------------------------------------
  function New (
    a_RelationshipDef                   udt_Id,
    a_ToEndPoint                        udt_EndPoint,
    a_FromObject                        udt_Id,
    a_ToObject                          udt_Id,
    a_AsOfDate                          date
  ) return udt_Id is
    t_toendptid                         udt_Id;
    t_fromendptid                       udt_Id;
  begin
   select endpointid
     into t_toendptid
     from rel.endpoints
    where endpoint          = a_toendpoint
      and relationshipdefid = a_RelationshipDef;

   select endpointid
    into t_fromendptid
    from rel.endpoints
   where endpoint         <> a_toendpoint
     and relationshipdefid = a_RelationshipDef;
   return New(a_RelationshipDef, a_FromObject, a_ToObject, t_fromendptid, t_toendptid, trunc(sysdate));
 end;

  ---------------------------------------------------------------------------
  -- New()
  ---------------------------------------------------------------------------
  function New (
    a_ToEndPointId                      udt_Id,
    a_FromObject                        udt_Id,
    a_ToObject                          udt_Id,
    a_AsOfDate                          date default sysdate
  ) return udt_Id is
    t_RelDef                            udt_Id;
    t_FromEndPointId                    udt_Id;
  begin
    select RelationshipDefId
      into t_RelDef
      from rel.EndPoints
      where EndPointId = a_ToEndPointId
        and rownum = 1;

    select EndPointId
      into t_FromEndPointId
      from rel.EndPoints
      where RelationshipDefId = t_RelDef
        and EndPointId <> a_ToEndPointId;

    return New(t_RelDef, a_FromObject, a_ToObject, t_FromEndPointId,
        a_ToEndPointId, trunc(sysdate));
  end New;

  ---------------------------------------------------------------------------
  -- New()
  ---------------------------------------------------------------------------
  procedure New (
    a_FromObject                        udt_Id,
    a_ToObject                          udt_Id,
    a_RelationshipDef                   varchar2
  ) is
    t_RelId                             udt_Id;
    t_FromObjectDef                     udt_Id;
  begin
    if not t_RelDefCache.exists(a_RelationshipDef) then
      t_RelDefCache(a_RelationshipDef) := api.pkg_configquery.ObjectDefIdForName(a_RelationshipDef);

      select ObjectDefId
        into t_FromObjectDef
        from possedata.objects
       where objectid = a_FromObject;

      select min(EndPointId)
        into t_ToEndPointCache(a_RelationshipDef)
        from rel.EndPoints
       where RelationshipDefId = t_RelDefCache(a_RelationshipDef)
         and FromObjectDefId = t_FromObjectDef;

      select EndPointId
        into t_FromEndPointCache(a_RelationshipDef)
        from rel.EndPoints
       where RelationshipDefId = t_RelDefCache(a_RelationshipDef)
         and EndPointId <> t_ToEndPointCache(a_RelationshipDef);
    end if;

    t_RelId := New(t_RelDefCache(a_RelationshipDef), a_FromObject, a_ToObject,
        t_FromEndPointCache(a_RelationshipDef),
        t_ToEndPointCache(a_RelationshipDef), trunc(sysdate));
  end New;

  ----------------------------------------------------------------------------
  -- DeleteRels()
  ----------------------------------------------------------------------------
  function DeleteRels (
    a_RelIds                            udt_IdList,
    a_FromIds                           udt_IdList,
    a_ToIds                             udt_IdList,
    a_RelDefIds                         udt_IdList,
    a_EPIds                             udt_IdList
  ) return varchar2 is
    t_Output                            varchar2(500);
    t_OtherEP                           udt_Id;
  begin

    for i in 1..a_RelIds.count loop

      select e.EndPointId
        into t_OtherEP
        from rel.endpoints e
       where e.RelationshipDefId = a_RelDefids(i)
         and e.EndPointId <> a_EPIds(i);

      posseaudit.pkg_Update.EnsureAuditIsInitialized(a_RelIds(i),
          a_FromIds(i), a_ToIds(i));

      delete from rel.StoredRelationships
       where RelationshipId = a_RelIds(i);

      posseaudit.pkg_Update.StoredRelationshipRemove(a_FromIds(i),
          a_EPIds(i), a_RelIds(i), a_RelDefIds(i), a_ToIds(i));
      posseaudit.pkg_Update.StoredRelationshipRemove(a_ToIds(i),
          t_OtherEP, a_RelIds(i), a_RelDefIds(i), a_FromIds(i));
      posseaudit.pkg_Update.StoredRelObjectRemove(a_RelDefIds(i),
          a_RelIds(i));

      posseaudit.pkg_Update.ObjectRemove(a_RelIds(i));

      delete from possedata.objects
      where ObjectId = a_RelIds(i);

      datamart.pkg_Triggers.ObjectChanged(a_RelIds(i),
          a_RelDefIds(i), 4);

    end loop;

    t_Output := 'Rel count (Delete): ' || a_RelIds.count;
    return t_Output;

  end DeleteRels;

  /*---------------------------------------------------------------------------
   * SetColumnValue() -- PUBLIC
   *   Set the value of a stored column.
   *   Only handles char & number!
   *   Does not handle Audit! Not for System columns!
   *-------------------------------------------------------------------------*/
  procedure SetColumnValue (
    a_ObjectId                          udt_Id,
    a_ColumnDefId                       udt_Id,
    a_Value                             varchar2
  ) is
    t_ColumnDefInfo                     objmodelphys.pkg_definition.udt_ColumnDefInfo;
    t_LogicalTransactionId              udt_Id;
    t_ListValueId                       udt_Id;
    t_StartDate                         date;
    t_EndDate                           date;
    t_SearchValue                       search.pkg_definition.udt_SearchValue;
    t_NumericSearchValue                number;
  begin

    t_LogicalTransactionId := admin.pkg_Control.CurrentTransaction();
    t_ColumnDefInfo := search.pkg_ConfigCache.ColumnDefInfo(a_ColumnDefId);

    delete from possedata.ObjectColumnData
    where ObjectId = a_ObjectId
      and ColumnDefId = a_ColumnDefId;

    if a_Value is not null then
      if t_ColumnDefInfo.DataType = app.pkg_Definition.gc_Number then
        t_SearchValue := null;
        t_NumericSearchValue := to_number(a_Value);
      else
        t_SearchValue := lower(substrb(a_Value, 1,
            search.pkg_Definition.gc_SearchValueLength));
        t_NumericSearchValue := null;
      end if;

      insert into possedata.ObjectColumnData (
        LogicalTransactionId,
        ObjectId,
        ColumnDefId,
        AttributeValue,
        AttributeValueId,
        EffectiveStartDate,
        EffectiveEndDate,
        SearchValue,
        NumericSearchValue
      ) values (
        t_LogicalTransactionId,
        a_ObjectId,
        a_ColumnDefId,
        a_Value,
        t_ListValueId,
        t_StartDate,
        t_EndDate,
        t_SearchValue,
        t_NumericSearchValue
      );
    end if;

        /* perform unique key checking, if necessary */
/*        select count(*)
        into t_Count
        from
          objmodelphys.IndexDefColumns idc
          join objmodelphys.IndexDefs i
              on i.IndexDefId = idc.IndexDefId
              and i.IndexType in ('P', 'U')
        where idc.ColumnDefId = a_ColumnId;
        if t_Count > 0 then
          CheckUniqueKey(a_ObjectInfo.Id, a_ColumnId);
        end if;
*/

  end;

  /*---------------------------------------------------------------------------
   * IndexIfApplicable() -- PRIVATE
   *   Index the note text, if applicable.
   *-------------------------------------------------------------------------*/
  procedure IndexIfApplicable (
    a_NoteId                            udt_Id,
    a_Text                              long
  ) is
    t_Count                             number;
  begin

    select count(*)
    into t_Count
    from
      objmodelphys.Notes n
      join objmodelphys.KeywordIndexedNoteDefs nd
          on nd.ObjectDefNoteDefId = n.ObjectDefNoteDefId
    where n.NoteId = a_NoteId;

    if t_Count > 0 then
      possedata.pkg_KeywordUpdate.IndexNote(a_NoteId, a_Text);
    end if;

  end;

  /*---------------------------------------------------------------------------
   * NewObject() -- PRIVATE
   *-------------------------------------------------------------------------*/
  function NewObject (
    a_ObjectDefId                       udt_Id,
    a_StartDate                         date,
    a_EndDate                           date,
    a_Values                            udt_StringList,
    a_CreateExternalRow                 boolean
  ) return udt_Id is
    t_ConfigReadSecurityClassId         udt_Id;
    t_ConfigReadSecurityInstanceId      udt_Id;
    t_CreatingProcessId                 udt_Id;
    t_LogicalTransactionId              udt_Id;
    t_ObjectDefInfo                     udt_ObjectDefInfo;
    t_ObjectInfo                        udt_ObjectInfo;
    t_ObjectReadSecurityClassId         udt_Id;
    t_ObjectReadSecurityInstanceId      udt_Id;
    t_PossibleDeficiencyId              udt_Id;
    t_StoredColumns                     udt_IdList;
    t_IsObjDefNoteDef                   char(1) := 'Z';
  begin

    -- Initialization
    t_LogicalTransactionId := admin.pkg_Control.CurrentTransaction();
    t_ObjectDefInfo := search.pkg_ConfigCache.ObjectDefInfo(a_ObjectDefId);
    t_ObjectInfo.ObjectDef := a_ObjectDefId;
    t_ObjectInfo.ObjectDefType := t_ObjectDefInfo.ObjectDefTypeId;
    t_ObjectInfo.Class := t_ObjectDefInfo.ObjectDefTypeId;
    t_ObjectInfo.Instance := a_ObjectDefId;
    if t_ObjectDefInfo.EffectiveDated then
      t_ObjectInfo.StartDate := a_StartDate;
      t_ObjectInfo.EndDate := a_EndDate;
    end if;

    if t_ObjectDefInfo.ObjectDefTypeId = metadata.pkg_definition.gc_ObjectDefNoteDefs then
      select decode(max(o.ObjectDefNoteDefId), null, 'N', 'Y')
        into t_IsObjDefNoteDef
        from api.objectdefnotedefs o
       where o.ObjectDefNoteDefId = a_ObjectDefId;
    end if;

    if (t_ObjectDefInfo.ObjectDefTypeId <> metadata.pkg_definition.gc_ObjectTypes
        or t_ObjectDefInfo.IsExternal) and t_IsObjDefNoteDef <> 'Y' then
      api.pkg_errors.RaiseError(-20100, 'Script has only been updated to support stored Object Types.');
    end if;

      -- Set config read security, instance security stays null
      t_ConfigReadSecurityClassId := t_ObjectDefInfo.ObjectDefTypeId;
      t_ConfigReadSecurityInstanceId := a_ObjectDefId;

    -- Create the object
    pkg_Debug.PutLine('Creating new instance of ObjectDefId ' ||
        t_ObjectInfo.ObjectDef || ' in the Objects table...');

    insert into possedata.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      t_LogicalTransactionId,
      t_LogicalTransactionId,
      possedata.ObjectId_s.nextval,
      a_ObjectDefId,
      t_ObjectDefInfo.ObjectDefTypeId,
      t_ObjectDefInfo.ObjectDefTypeId,
      a_ObjectDefId,
      t_ObjectInfo.StartDate,
      t_ObjectInfo.EndDate,
      t_ConfigReadSecurityClassId,
      t_ConfigReadSecurityInstanceId,
      t_ObjectReadSecurityClassId,
      t_ObjectReadSecurityInstanceId
    ) returning ObjectId
      into t_ObjectInfo.Id;

    pkg_Debug.PutLine('Object ' || t_ObjectInfo.Id || ' (ObjectDefId ' ||
        t_ObjectInfo.ObjectDef || ') created in the Objects table.');

    posseaudit.pkg_Update.SetObjectInSync(t_ObjectInfo.Id,
        a_ObjectDefId, t_ObjectDefInfo.ObjectDefTypeId);

    -- Audit the creation of a new object
    case t_ObjectDefInfo.ObjectDefTypeId
      when metadata.pkg_Definition.gc_DeficiencyTypes then
        posseaudit.pkg_Update.DeficiencyNew(t_ObjectInfo.Id,
            t_CreatingProcessId, t_PossibleDeficiencyId);
      when metadata.pkg_Definition.gc_ObjectDefNoteDefs then
        null;  -- notes are handled elsewhere
      else
        -- Note that relationships are also handled here.
        posseaudit.pkg_Update.ObjectNew(t_ObjectInfo.Id);
        -- Though we are not really "correcting" the date range for the object
        -- per se, this procedure will audit the start and end date for the
        -- object and audit the columns
        posseaudit.pkg_Update.CorrectDateRange(t_ObjectInfo.Id, t_ObjectInfo.StartDate,
            t_ObjectInfo.EndDate, t_StoredColumns);
    end case;

    return t_ObjectInfo.Id;

  end;

  /*---------------------------------------------------------------------------
   * NewObject() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NewObject (
    a_ObjectDef                         udt_Id,
    a_AsOfDate                          date
  ) return udt_Id is
    t_Values                            udt_StringList;
  begin
/*    security.pkg_InstanceUpdate.FailIfNotGrantedPrivilege(
        metadata.pkg_Definition.gc_ObjectTypes, a_ObjectDef,
        metadata.pkg_Definition.gc_New);*/
    return NewObject(a_ObjectDef, trunc(a_AsOfDate),
        app.pkg_Definition.gc_MaxDate, t_Values, true);
  end;

  ----------------------------------------------------------------------------
  -- NewNote() -- PUBLIC
  ----------------------------------------------------------------------------
  function NewNote (
    a_ObjectId                          udt_Id,
    a_NoteDefId                         udt_Id,
    a_Locked                            boolean,
    a_Text                              varchar2
  ) return udt_Id is
    t_ObjectDefNoteDefId                udt_Id;
    t_NoteId                            udt_Id;
    t_Locked                            char(1);
  begin

    if a_Text is null then
      return null;
    end if;

    begin
      select nd.ObjectDefNoteDefId
        into t_ObjectDefNoteDefId
        from
          objmodelphys.Objects o
          join objmodelphys.ObjectDefNoteDefs nd
              on nd.ObjectDefId = o.ObjectDefId
        where o.ObjectId = a_ObjectId
          and nd.NoteDefId = a_NoteDefId;
    exception
    when no_data_found then
      raise_application_error(-20000, 'NoteDefId ' || a_NoteDefId
          || ' cannot be associated with ObjectId ' || a_ObjectId);
    end;

    t_NoteId := NewObject(t_ObjectDefNoteDefId, null);

    if a_Locked then
      t_Locked := 'Y';
    else
      t_Locked := 'N';
    end if;

    posseaudit.pkg_Update.NoteNew(t_NoteId, t_ObjectDefNoteDefId, a_ObjectId,
        t_Locked, a_Text);

    insert into objmodelphys.Notes (
      NoteId,
      ObjectId,
      ObjectDefNoteDefId,
      Locked,
      Text
    ) values (
      t_NoteId,
      a_ObjectId,
      t_ObjectDefNoteDefId,
      t_Locked,
      a_Text
    );

    IndexIfApplicable(t_NoteId, a_Text);

    return t_NoteId;
  end NewNote;

end pkg_BoundaryRealignment_Utils;
/

declare
  t_NoteCount      varchar2(40);
  t_RowsProcessed  number := 0;
  t_TotalRows      number := 0;
  t_NotesCreated   number := 0;
  t_NoteId         number;
  t_NoteDefId      number;
  t_ColumndefId    number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'ContactFaxNumber'); 
begin
                                             
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  select ND.NoteDefId
    into t_NoteDefId
    from api.notedefs nd
   where nd.Tag = 'GEN';
   
  for c in (select 'Fax Number: ' || le.FormattedLicenseeContactFaxNum NoteText, le.objectid
              from query.o_abc_legalentity le
             where le.FormattedLicenseeContactFaxNum is not null) loop
  --Create the note
    t_NoteId := possedba.pkg_boundaryrealignment_utils.NewNote(c.objectid, t_NoteDefId, true, c.NoteText);
    t_NotesCreated := t_NotesCreated + 1;
    t_RowsProcessed := t_RowsProcessed + 1;
    t_TotalRows := t_totalRows + 1;
    dbms_output.put_line(c.objectid);
    delete from possedata.objectcolumndata cd
     where cd.ObjectId = c.objectid
       and cd.ColumnDefId = t_ColumndefId;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
/
drop package pkg_BoundaryRealignment_Utils;