/*-----------------------------------------------------------------------------
 * Modifying Author: David A
 * Expected run time: < 5 sec
 * Purpose: To change the Job Status of Renewal Application 369511 to Cancelled
 *   No processes need to be removed.
 *
 *  We should always add a case note. Added code to add a case note with the issue number.
 *
 *  For Issue (not created yet), when running script in NJPROD, set...
 *     t_PermitNumber  := '369511'
 *---------------------------------------------------------------------------*/
declare
  t_ProcessId                           number;
  t_ProcessDefId                        number
    := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus'); -- DefId for Process to create
  t_Outcome                             varchar2(100)
    := 'Cancelled'; -- Outcome
  t_PermitId                            number;
  t_PermitNumber                        varchar2(100)
    := '369511'; -- # 369511 (Production Job to be Cancelled)
  t_NoteDefId                           pls_integer;
  t_NoteId                              pls_integer;
  t_NoteTag                             varchar2(6) := 'GEN';
  t_NoteText                            varchar2(4000);
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select r.objectid
  into t_PermitId
  from query.j_ABC_RenewalApplication r
  where r.ExternalFileNum = t_PermitNumber;

  -- Cancel job
  dbms_output.put_line('Cancelling job number ('|| t_PermitNumber ||').');
  t_ProcessId := api.pkg_processupdate.New(t_PermitId, t_ProcessDefId, 'Change Status', sysdate, sysdate, sysdate);
  api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange', 'Cancelled per Issue 56254.');
  api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);

  -- Create Note and Populate Text
  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;

  t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
        'Cancelling renewal by NJABC request for issue 56254. See job #369510.';
  t_NoteText := t_NoteText;
  t_NoteId := api.pkg_NoteUpdate.New(t_PermitId, t_NoteDefId, 'Y', t_NoteText);

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
