-- Created on 7/22/2016 by MICHEL.SCHEFFERS 
-- Apply Instance Security on CPL Online Documents
-- Issue 8293
declare 
  -- Local variables here
  t_Jobs integer := 0;

begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all CPL Online Documents
  for i in (select ced.DocumentId
              from query.r_ABC_OnlineCPLeDocument ced
            ) loop
    -- Apply Instance Security for each found document
    t_Jobs := t_Jobs + 1;
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.Documentid, sysdate);
  end loop;
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbms_output.put_line('CPL Documents updated: ' || t_Jobs);
end;
