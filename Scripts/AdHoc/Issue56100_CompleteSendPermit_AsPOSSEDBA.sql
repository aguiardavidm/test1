/*-----------------------------------------------------------------------------
 * Author: Benjamin Albrecht
 * Expected run time: 2+ seconds in cxnjdev for 2 rows.
 * Purpose: Complete the apx 319 outsanding Send Permit processes to help assist NJ in processing.
 *
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_IssueNumber                         varchar2(6) := '56100';
  t_LogicalTransactionId                pls_Integer;
  t_ObjectId                            pls_Integer;
  t_ObjectList                          udt_IdList;
  t_NoteText                            varchar2(4000);
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_TimeEnd                             number(15);
  t_TimeStart                           number(15);
  t_ProcessTypeId                       udt_Id := api.pkg_configquery.ObjectDefIdForName('p_ABC_SendPermit');
  t_UpdatedCount                        pls_Integer := 0;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

  procedure createCaseNote (
    a_ObjectId                          pls_integer,
    a_NoteText                          varchar2
  ) is
    t_NoteDefId                         pls_integer;
    t_NoteId                            pls_integer;
    t_NoteTag                           varchar2(6) := 'GEN';
  begin

  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;

  t_NoteId := api.pkg_NoteUpdate.New(a_ObjectId, t_NoteDefId, 'Y', a_NoteText);

  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;
  
  -- Populate Case Note Text & create on object
  t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
      'Script Completed Send Permit Process. See Issue ' || t_IssueNumber || '.';
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);
  
  -- Loop through the jobs to update
  for i in (
      --Populate processes to complete  
      select 
              p.processid,
              p.jobid
              from 
                api.processes p 
            join api.jobs pa on pa.JobId = p.JobId
            join api.relationships r on r.fromobjectid = pa.jobid
            join api.endpoints ep on ep.endpointid = r.endpointid
            join query.o_abc_permittype pt on pt.ObjectId = r.toobjectid
            where pt.Code in ('TI', 'TLI', 'LTI', 'LTC')
      --      and pa.CreatedDate >= to_date('04/01/2020', 'MM/DD/YYYY')
      --      and pa.JobStatus = 'NOTIF'
            and ep.Name in ('PermitType', 'PermitRenewal', 'OnlinePermitType', 'RenewToPermit')
            and p.ProcessTypeId = api.pkg_configquery.ObjectDefIdForName('p_ABC_SendPermit')--t_ProcessTypeId
            and p.outcome is null
      union all
      select 
              p.processid,
              p.jobid
              from 
                api.processes p 
            join api.jobs pa on pa.JobId = p.JobId
            join api.relationships r on r.fromobjectid = pa.jobid
            join api.endpoints ep on ep.endpointid = r.endpointid
            join api.relationships r2 on r2.FromObjectId = r.toobjectid
            join api.endpoints ep2 on ep2.endpointid = r2.endpointid
            join query.o_abc_permittype pt on pt.ObjectId = r2.toobjectid
            where pt.Code in ('TI', 'TLI', 'LTI', 'LTC')
      --      and pa.CreatedDate >= to_date('04/01/2020', 'MM/DD/YYYY')
      --      and pa.JobStatus = 'NOTIF'
            and ep.Name in ('RenewToPermit')
            and ep2.name in ('PermitType')
            and p.ProcessTypeId = api.pkg_configquery.ObjectDefIdForName('p_ABC_SendPermit')--t_ProcessTypeId
            and p.outcome is null
  ) loop
    begin
      --Complete Process
      api.pkg_processupdate.Complete(i.processid, 'Sent');

      createCaseNote(i.jobid, t_NoteText);
      t_UpdatedCount := t_UpdatedCount + 1;
    exception when others then
      dbms_output.put_line('Processid: ' || i.processid || ', jobid: ' || i.jobid || ', error: ' || sqlerrm);
      raise;
    end;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  dbug('Updated Object Count: ' || t_UpdatedCount);

  --commit;

end;
