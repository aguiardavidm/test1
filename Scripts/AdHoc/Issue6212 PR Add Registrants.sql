-- Created on 9/17/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
    t_AmendRegistrantEndPoint  number := api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment', 'RegistrantEndPoint');
    t_AmendJobEndPoint         number := api.pkg_configquery.OtherEndPointIdForName('j_ABC_PRAmendment', 'RegistrantEndPoint');
    t_RelationshipDef          number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PRAmendToRegistrant');
    t_PubAmendRegRelId         number;
    t_PubAmendRegId            number;
    t_RowsProccessesd          number :=0;
    t_RelCount                 number :=0;
    
    
begin
  -- Test statements here
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for c in (select pra.ObjectId JobId,
                   pra.OnlineUseLegalEntityObjectId  ,
                   pra.ExternalFileNum    
              from query.j_Abc_Pramendment pra
             where pra.EnteredOnline = 'Y') loop
    
  begin
        
        select r.RelationshipId, r.ToObjectId
          into t_PubAmendRegRelId, t_PubAmendRegId
          from api.relationships r
         where r.FromObjectId = c.JobId
           and r.EndPointId = t_AmendRegistrantEndPoint;
         
        /* if t_PubAmendRegId != c.OnlineUseLegalEntityObjectId then
           api.pkg_relationshipupdate.Remove(t_PubAmendRegRelId);
           t_PubAmendRegRelId := api.pkg_relationshipupdate.New(t_AmendRegistrantEndPoint, c.JobId, c.OnlineUseLegalEntityObjectId );
         end if;  */
         
         dbms_output.put_line('No Change: ' || c.Externalfilenum);
         
         exception when no_data_found then --Create the relationship
           if c.OnlineUseLegalEntityObjectId is not null then
              --t_PubAmendRegRelId := api.pkg_relationshipupdate.New(t_AmendRegistrantEndPoint, c.JobId, c.OnlineUseLegalEntityObjectId );
              
              select possedata.ObjectId_s.nextval
              into t_PubAmendRegRelId 
             from dual;

            --Insert into ObjModelPhys.Objects
            insert into objmodelphys.Objects (
                  LogicalTransactionId,
                  CreatedLogicalTransactionId,
                  ObjectId,
                  ObjectDefId,
                  ObjectDefTypeId,
                  ClassId,
                  InstanceId,
                  EffectiveStartDate,
                  EffectiveEndDate,
                  ConfigReadSecurityClassId,
                  ConfigReadSecurityInstanceId,
                  ObjectReadSecurityClassId,
                  ObjectReadSecurityInstanceId
                ) values (
                  1, -- Hard coded logical Transaction to 1
                  1, -- Hard coded logical Transaction to 1
                  t_PubAmendRegRelId,
                  t_RelationshipDef,
                  4,
                  4,
                  t_RelationshipDef,
                  null,
                  null,
                  4,
                  t_RelationshipDef,
                  null,
                  null
                );

             
            --Insert Into Rel.StoredRelationships    

                insert into rel.StoredRelationships (
                  RelationshipId,
                  EndPointId,
                  FromObjectId,
                  ToObjectId
                ) values (
                  t_PubAmendRegRelId,
                  t_AmendRegistrantEndPoint,
                  c.JobId,
                  c.OnlineUseLegalEntityObjectId
                );

                insert into rel.StoredRelationships (
                  RelationshipId,
                  EndPointId,
                  FromObjectId,
                  ToObjectId
                ) values (
                  t_PubAmendRegRelId,
                  t_AmendJobEndPoint,
                  c.OnlineUseLegalEntityObjectId,
                  c.JobId
                );
              
              dbms_output.put_line('New Relationship: ' || c.Externalfilenum);
           t_RelCount := t_RelCount +1;   
           end if;
       end;
  t_RowsProccessesd := t_RowsProccessesd+1;
  end loop;
  
 dbms_output.put_line('Rows Processed: ' || t_RowsProccessesd || ' Releationships Created: ' || t_RelCount);
  
end;
