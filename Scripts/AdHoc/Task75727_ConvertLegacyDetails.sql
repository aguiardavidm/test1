-- Script - Convert Legacy Details
-- Est PROD run time: 1-2 hours
--  This script will convert the pre-existing brand question details into 
--   the new details, and create a case note containing the previous values.
--  This script will also generate all of the questions on existing draft 
--   PR Applications.
declare
  t_PRAppDef                        api.pkg_definition.udt_Id
      := api.pkg_configquery.ObjectDefIdForName('j_Abc_PRApplication');
  t_NoteDef                         api.pkg_definition.udt_Id;
  t_NoteText                        varchar2(4000); 
  t_OnlineIsTheRegistrantBasedInNJ  varchar2(4000);
  t_OnlineIsProdOwnerOrAuthAgent    varchar2(4000);
  t_OnlineHasLetterOfAuthorization  varchar2(4000);
  t_IsRegistrantBasedNewJersey      varchar2(4000);
  t_IsProdOwnerOrAuthorizedAgent    varchar2(4000);
  t_HasLetterOfAuthorization        varchar2(4000);
  t_PRApplications                  api.pkg_definition.udt_IdList;
  t_NoteId                          api.pkg_definition.udt_Id;
  t_LegacyCount                     number := 0;
  t_GenerateQuestCount              number := 0;
  
  -- API BREAK - Define our own column update procedure to go beneath api for performance reasons
  procedure AuditUpdate (
    a_ObjectId                      api.pkg_definition.udt_Id,
    a_ColumnDefId                   api.pkg_definition.udt_Id,
    a_Value                         varchar2
  ) is
    begin
      insert into posseaudit.ObjectColumnData (
       LogicalTransactionId,
       ObjectId,
       SourceLogicalTransactionId,
       ColumnDefId,
       SeqNum,
       Value,
       Text
      ) values (
       admin.pkg_Control.CurrentTransaction(),
       a_ObjectId,
       admin.pkg_Control.CurrentTransaction(),
       a_ColumnDefId,
       posseaudit.AuditSeqNum_s.nextval,
       a_Value,
       null
      );
    end;

  procedure ColumnUpdate (
    a_ObjectId                      api.pkg_definition.udt_Id,
    a_ColumnName                    varchar2,
    a_Value                         varchar2
  ) is
    t_ColumnDefId                   api.pkg_definition.udt_Id;
    t_DomainId                      api.pkg_definition.udt_Id;
    t_Value                         varchar2(4000);
    t_AttributeId                   api.pkg_definition.udt_Id;
    t_ObjectDefId                   api.pkg_definition.udt_Id
        := api.pkg_configquery.ObjectDefIdForName('j_Abc_PRApplication');
  begin
    -- Determine the domain value if column uses a domain, otherwise use the provided value
    --  Technically for the purposes of this script we are always dealing with a domain value...
    --  But I had already written out the logic for setting a non-domain value so I left it in
    --  case we need it later.
    select cd.ColumnDefId, cd.DomainId
      into t_ColumnDefId, t_DomainId
      from stage.columndefs cd
     where cd.ObjectDefId = t_ObjectDefId
       and cd.Name = a_ColumnName;
    if t_DomainId is not null then
      select dlv.ListValueId
        into t_AttributeId
        from stage.domainlistofvalues dlv
       where dlv.DomainId = t_DomainId
         and dlv.Value = a_Value;
    else
      t_Value := a_Value;
    end if;
    t_ColumnDefId := api.pkg_configquery.ColumnDefIdForName('j_Abc_PRApplication', a_ColumnName);
    -- If the column already has a value, update the existing row.
    --  Otherwise, inert a new row into ObjectColumnData
    if api.pkg_columnquery.value(a_ObjectId, a_ColumnName) is null then
      insert into possedata.objectcolumndata ocd (
        logicaltransactionid,
        objectid,
        columndefid,
        attributevalue,
        searchvalue,
        attributevalueid
        )
        values
        (
        api.pkg_logicaltransactionupdate.CurrentTransaction,
        a_ObjectID,
        t_ColumnDefId,
        t_Value,
        lower(t_Value),
        t_AttributeId
        );
    else
      update possedata.objectcolumndata ocd
         set ocd.LogicalTransactionId = api.pkg_logicaltransactionupdate.CurrentTransaction,
             ocd.AttributeValue = t_Value,
             ocd.SearchValue = lower(t_Value),
             ocd.AttributeValueId = t_AttributeId
       where ocd.ObjectId = a_ObjectId
         and ocd.ColumnDefId = t_ColumnDefId;
    end if;

    AuditUpdate(a_ObjectId, t_ColumnDefId, a_Value);
  end;
  
begin
  if user != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20001, 'Script must be run as POSSEDBA');
  end if;
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  select j.JobId 
    bulk collect into t_PRApplications
    from api.jobs j
   where j.JobTypeId = t_PRAppDef;
   
  select nd.NoteDefId
    into t_NoteDef
    from api.notedefs nd
   where nd.Tag = 'GEN';
  for i in 1..t_PRApplications.count loop
    -- Generate questions on the job
    if  api.pkg_columnquery.Value(t_PRApplications(i), 'StatusName') = 'NEW' then
      abc.pkg_ABC_Workflow.CopyPRApplicationQuestions(t_PRApplications(i), sysdate);
      t_GenerateQuestCount := t_GenerateQuestCount + 1;
    end if;
  
    -- Run the Legacy detail updates
    t_NoteText := 'Legacy Detail Maintenance - ' || to_char(sysdate, 'Mon dd, yyyy hh:mi:ss');
    t_OnlineIsTheRegistrantBasedInNJ  := api.pkg_columnquery.Value(t_PRApplications(i), 'OnlineIsTheRegistrantBasedInNJ');
    t_OnlineIsProdOwnerOrAuthAgent    := api.pkg_columnquery.Value(t_PRApplications(i), 'OnlineIsProdOwnerOrAuthAgent');
    t_OnlineHasLetterOfAuthorization  := api.pkg_columnquery.Value(t_PRApplications(i), 'OnlineHasLetterOfAuthorization');
    t_IsRegistrantBasedNewJersey      := api.pkg_columnquery.Value(t_PRApplications(i), 'IsRegistrantBasedNewJersey');
    t_IsProdOwnerOrAuthorizedAgent    := api.pkg_columnquery.Value(t_PRApplications(i), 'IsProdOwnerOrAuthorizedAgent');
    t_HasLetterOfAuthorization        := api.pkg_columnquery.Value(t_PRApplications(i), 'HasLetterOfAuthorization');
    
    -- If none of the legacy details are set, skip this job.
    -- Using length instead of null comparison because of issues with empty but non-null values
    if length(t_OnlineIsTheRegistrantBasedInNJ || t_OnlineIsProdOwnerOrAuthAgent ||
        t_OnlineHasLetterOfAuthorization || t_IsRegistrantBasedNewJersey || 
        t_IsProdOwnerOrAuthorizedAgent || t_HasLetterOfAuthorization) > 0 then
      -- At least one of these details is populated, continue normally
      if api.pkg_columnquery.Value(t_PRApplications(i), 'StatusName') != 'NEW' then
        -- Set details based on business logic
        if t_OnlineIsTheRegistrantBasedInNJ = 'Yes'
            or t_IsRegistrantBasedNewJersey = 'Yes' then
          if api.pkg_columnquery.Value(t_PRApplications(i), 'LicenseNumber') is not null then
            ColumnUpdate(t_PRApplications(i), 'OnlineRegistrantHasNJLicense', 'Yes');
            ColumnUpdate(t_PRApplications(i), 'RegistrantHasNJLicense', 'Yes');
          elsif api.pkg_columnquery.Value(t_PRApplications(i), 'PermitNumber') is not null then
            ColumnUpdate(t_PRApplications(i), 'OnlineRegistrantHasNJLicense', 'No');
            ColumnUpdate(t_PRApplications(i), 'OnlineRegistrantHasNJTAPPermit', 'Yes');
            ColumnUpdate(t_PRApplications(i), 'RegistrantHasNJLicense', 'No');
            ColumnUpdate(t_PRApplications(i), 'RegistrantHasNJTAPPermit', 'Yes');
          else
            api.pkg_columnupdate.RemoveValue(t_PRApplications(i), 'OnlineRegistrantHasNJLicense');
            api.pkg_columnupdate.RemoveValue(t_PRApplications(i), 'RegistrantHasNJLicense');
          end if;
        else
          ColumnUpdate(t_PRApplications(i), 'OnlineRegistrantHasNJLicense', 'No');
          ColumnUpdate(t_PRApplications(i), 'OnlineRegistrantHasNJTAPPermit', 'No');
          ColumnUpdate(t_PRApplications(i), 'OnlineRegistrantHasNJPresence', 'No');
          ColumnUpdate(t_PRApplications(i), 'OnlineRegistrantSolicitingInNJ', 'No');
          ColumnUpdate(t_PRApplications(i), 'RegistrantHasNJLicense', 'No');
          ColumnUpdate(t_PRApplications(i), 'RegistrantHasNJTAPPermit', 'No');
          ColumnUpdate(t_PRApplications(i), 'RegistrantHasNJPresence', 'No');
          ColumnUpdate(t_PRApplications(i), 'RegistrantSolicitingInNJ', 'No');
        end if;
      end if;
      -- Add legacy values to the note and then remove the values from posse
      -- Based in NJ
      if length(t_OnlineIsTheRegistrantBasedInNJ) > 0 then
        t_NoteText := t_NoteText || chr(10) || chr(13) || '(Online) Is the Registant based in NJ?: '
            || t_OnlineIsTheRegistrantBasedInNJ;
        api.pkg_columnupdate.RemoveValue(t_PRApplications(i), 'OnlineIsTheRegistrantBasedInNJ');
      end if;
      if length(t_IsRegistrantBasedNewJersey) > 0 then
        t_NoteText := t_NoteText || chr(10) || chr(13) || 'Is the Registant based in NJ?: '
            || t_IsRegistrantBasedNewJersey;
        api.pkg_columnupdate.RemoveValue(t_PRApplications(i), 'IsRegistrantBasedNewJersey');
      end if;
      -- Owner or Agent
      if length(t_OnlineIsProdOwnerOrAuthAgent) > 0 then
        t_NoteText := t_NoteText || chr(10) || chr(13) || '(Online) Is the Registrant Product Owner or an Authorized Agent?: '
            || t_OnlineIsProdOwnerOrAuthAgent;
        api.pkg_columnupdate.RemoveValue(t_PRApplications(i), 'OnlineIsProdOwnerOrAuthAgent');
      end if;
      if length(t_IsProdOwnerOrAuthorizedAgent) > 0 then
        t_NoteText := t_NoteText || chr(10) || chr(13) || 'Is the Registrant Product Owner or an Authorized Agent?: '
            || t_IsProdOwnerOrAuthorizedAgent;
        api.pkg_columnupdate.RemoveValue(t_PRApplications(i), 'IsProdOwnerOrAuthorizedAgent');
      end if;
      -- Has Letter of Authorization
      if length(t_OnlineHasLetterOfAuthorization) > 0 then
        t_NoteText := t_NoteText || chr(10) || chr(13) || '(Online) Does the Registrant have a letter of Authorization from the Product Owner?: '
            || t_OnlineHasLetterOfAuthorization;
        api.pkg_columnupdate.RemoveValue(t_PRApplications(i), 'OnlineHasLetterOfAuthorization');
      end if;
      if length(t_HasLetterOfAuthorization) > 0 then
        t_NoteText := t_NoteText || chr(10) || chr(13) || 'Does the Registrant have a letter of Authorization from the Product Owner?: '
            || t_HasLetterOfAuthorization;
        api.pkg_columnupdate.RemoveValue(t_PRApplications(i), 'HasLetterOfAuthorization');
      end if;
        
      -- Create the conversion note
      -- NOT done beneath API as that it's a little too risky for me.
      --  Instead we are suppressing the post verify stuff below.
      t_NoteId := api.pkg_noteupdate.New(t_PRApplications(i), t_NoteDef, 'N', t_NoteText);
      t_NoteText := '';
      
      t_LegacyCount := t_LegacyCount + 1;
      -- Commit every 100 rows (product team recommendation)
      if mod(t_LegacyCount, 100) = 0 then
        -- API BREAK - Skip post verify because it takes too long
        objmodelphys.pkg_eventqueue.endsession('N');
        api.pkg_logicaltransactionupdate.EndTransaction;
        commit;
        api.pkg_logicaltransactionupdate.ResetTransaction;
      end if;
    end if;
  end loop;
  -- API BREAK - Skip post verify because it takes too long
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
dbms_output.put_line('Generate-questions triggered on jobs: ' || t_GenerateQuestCount);
dbms_output.put_line('Updated Legacy details on jobs: ' || t_LegacyCount);
end;
