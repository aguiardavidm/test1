
-- Create table
--DROP table possedba.LOG_Docs_LastModified;
create table possedba.LOG_Docs_LastModified
(
  documentId                           NUMBER,
  documentType                         VARCHAR2(100),
  oldLogigalTrans                      NUMBER,
  newLogigalTrans                      NUMBER,
  new_LastUpdateByUserId               NUMBER,
  new_LastUpdateByUserName             VARCHAR2(100),
  new_LastUpdateByUserOracle           VARCHAR2(100)
);


declare

t_docType   VARCHAR2(100);
p           INTEGER := 0;                      

begin

for i in (
  select d.DocumentId, l.LogicalTransactionId newLT, pdo.LogicalTransactionId oldLT,l.CreatedBy LastUpdateByUserId, api.pkg_columnquery.Value(l.CreatedBy,'FormattedName') LastUpdateByUserName, 
  api.pkg_columnquery.Value(l.CreatedBy,'OracleLogonId') LastUpdateByUserOracleLogon, l.CreatedDate
  from   doc.documents d
  join   api.objects o on o.ObjectId = d.DocumentId
  join   doc.documentrevisions dr on dr.DocumentId = d.DocumentId
  join   api.logicaltransactions l on l.LogicalTransactionId = dr.LogicalTransactionId
  join   possedata.objects pdo on pdo.ObjectId = d.DocumentId
  where  d.DocumentId IN (select o.ObjectId
                            from api.objects o
                            join api.objectdefs od
                              on o.ObjectDefId = od.ObjectDefId
                           where od.ObjectDefTypeId = 5
                             and od.Name not in ('d_AdieuComponentDef',
                                                 'd_BuildingPermitDocument',
                                                 'd_CertificateOfOccupancyDoc',
                                                 'd_ContractorBillDocument',
                                                 'd_CSVDocument',
                                                 'd_DepositSlipDocument',
                                                 'd_ElectronicSignature',
                                                 'd_FinancialSummaryDocument',
                                                 'd_GeneralPermitDocument',
                                                 'd_InspectionDetailDocument',
                                                 'd_InspectionSummaryDocument',
                                                 'd_InspectionTicketDocument',
                                                 'd_NoticetoContractorDocument',
                                                 'd_PythonScript',
                                                 'd_ReportLogo',
                                                 'd_TCODocument',
                                                 'd_TradePermitDocument',
                                                 'd_UnpaidBillsDocument',
                                                 'd_UtilityConnectsDocument',
                                                 'd_WebImage',
                                                 'd_WordInterfaceTemplate',
                                                 'd_WordMergeMacros',
                                                 'd_WordReport',
                                                 'd_WordTemplate'
                                                )
                      )
  AND api.pkg_ColumnQuery.Value(d.DocumentId, 'LastUpdateByUserId') = 1
  AND l.CreatedBy != 1
)
LOOP
--  p := p + 1;
--  if p > 5 
--  THEN EXIT;
--  END IF;
  
  t_docType := api.pkg_ColumnQuery.Value(i.DocumentId, 'ObjectDefName');
 -- SELECT b.LogicalTransactionId FROM  possedata.objects b
  --WHERE b.ObjectId = 40314804
  
  UPDATE possedata.objects o
  SET o.LogicalTransactionId = i.newLT
  WHERE o.ObjectId = i.DocumentId;
  
   
      INSERT INTO possedba.LOG_Docs_LastModified
      (documentId, oldLogigalTrans, newLogigalTrans, documentType, new_LastUpdateByUserId, new_LastUpdateByUserName, new_LastUpdateByUserOracle)
      VALUES
      (i.documentid,
       i.oldlt,
       i.newlt,
       t_docType,
       i.LastUpdateByUserId,
       i.LastUpdateByUserName,
       i.LastUpdateByUserOracleLogon
      );


END LOOP;
commit;
END;

