/*-----------------------------------------------------------------------------
 * Creating Author: Ben
 * Expected run time:
 * Purpose: After removing performing a rescue process on a already complete job 
 * we failed to create the Monitor Review process and when the MC and PC processes
 * were completed manually they did not create the next step, Admin Review. This
 * script recreates and completes the Monitor Review process to move the job
 * forward in workflow.
 *
 * Set the t_JobId to the application you are fixing.
 *---------------------------------------------------------------------------*/
declare
  t_JobId         api.pkg_definition.udt_id := ;
  t_ProcessTypeId api.pkg_definition.udt_id := api.pkg_configquery.ObjectDefIdForName('p_ABC_MonitorReview');
  t_NewProcessID  api.pkg_definition.udt_id;


begin
  
  --Start Transaction
  api.pkg_logicaltransactionupdate.ResetTransaction();

  --Recreate the missing 'Monitor Review' process
  t_NewProcessID := api.pkg_processupdate.New(t_jobid, t_processtypeid, null, null, null, null);
  
  --Complete the process with an outcome of 'Completed' to move the job forward.
  api.pkg_processupdate.Complete(t_NewProcessID, 'Completed');

  api.pkg_logicaltransactionupdate.EndTransaction();
  --Manually Commit After Confirming Pending Changes from the DBMS Output.
end;