/*-----------------------------------------------------------------------------
 * Expected run time: .25 sec
 * Purpose:Set the public permit search instructional text
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;

  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_SystemSettingsID                    udt_Id;

begin
  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;
  
  --find system settings detail
  select objectid 
    into t_SystemSettingsID
    from query.o_systemsettings s
   where rownum < 2;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  
  api.pkg_columnupdate.SetValue(t_SystemSettingsID, 'InternalLEPermitSearchInstText', 
  'When there are more than 200 records; please refine your search by selecting the criteria below or click Save as Excel to export all results for your criteria.');
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
