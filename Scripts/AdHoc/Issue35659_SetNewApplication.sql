  /*---------------------------------------------------------------------------
   * Issue 35659 - Consistency of documents with in amendment jobs and new 
   * application jobs 
   *   Pre-populate NewApplication on r_OLLicenseTypeDocumentType and
   * r_ABC_PermitTypeDocumentType with 'Y'.
   * Run time < 1 minute
   * ------------------------------------------------------------------------*/
declare
  -- Local variables here
  t_Count                               integer := 0;
begin

  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
  end if;

  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);

  for a in (select r.RelationshipId
            from query.r_OLLicenseTypeDocumentType r
           ) loop
    t_Count := t_Count + 1;
    api.pkg_columnupdate.SetValue(a.relationshipid, 'NewApplication', 'Y');
  end loop;
  for a in (select r.RelationshipId
            from query.r_ABC_PermitTypeDocumentType r
           ) loop
    t_Count := t_Count + 1;
    api.pkg_columnupdate.SetValue(a.relationshipid, 'NewApplication', 'Y');
  end loop;

  dbms_output.put_line (t_Count || ' Checklist Template records updated.');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
