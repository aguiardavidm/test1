rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- Created by: JOSHUA.LENON 
-- Created Date: 5/12/2016
-- Purpose: Fix process server scheduled processes for Product Registration Non Renewals
--          that were scheduled a year after the intended date.
declare 
  -- Get the Review Status of Non Renewal process type id
  t_ProcessDef number := api.pkg_configquery.ObjectDefIdForName('p_ABC_ReviewStatusOfNonRenewal');
begin
  -- Begin Transaction
  api.pkg_logicaltransactionupdate.ResetTransaction();
    
    -- loop through affected rows and set the scheduled date to the current date so that the incorrect data will run immediately
    for r in 
      (select s.schedulekey,
              s.scheduledate
         from scheduler.sysschedule s
        where s.description like '%' || t_ProcessDef || '%'
          and s.scheduledate > sysdate)loop
        
        dbms_output.put_line('Create Process Type: ' || t_ProcessDef);
        dbms_output.put_line('Schedule: ' || r.schedulekey);
        dbms_output.put_line('Schedule Date: ' || r.ScheduleDate);
        
        -- Update ScheduledDate
        update scheduler.sysschedule su
           set su.scheduledate = trunc(sysdate)
         where su.schedulekey = r.schedulekey;
        
    end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  
  commit;
        
end;
/
