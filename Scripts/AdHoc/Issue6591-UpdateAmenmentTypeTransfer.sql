-- Created on 01/18/2016 by MICHEL.SCHEFFERS 
-- Update TransferPermits and TransferCoOpMemberships for each Amendment Type.
declare 
  -- Local variables here
  i                  integer := 0;
  
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  for a in (select at.ObjectId
              from query.o_abc_amendmenttype at
           ) loop
     dbms_output.put_line('Updating Amendment Type ' || api.pkg_columnquery.Value(a.objectid,'Name'));
     api.pkg_columnupdate.SetValue (a.objectid,'TransferPermits','Y');
     api.pkg_columnupdate.SetValue (a.objectid,'TransferCoOpMemberships','Y');
     i := i + 1;
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

  dbms_output.put_line('AmendmentType records updated : ' || i);
end;
