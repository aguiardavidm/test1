-- Created by Joshua Lenon (CXUSA)
-- Created on 9/27/2015
-- Time to run in NJCONV:  mins
declare
  t_ColumnDefId      number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'dup_ExpDatePlusGracePeriod');
  t_CurrTransaction  number;
  t_CommitCount      number := 0;
begin
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  t_CurrTransaction := api.pkg_LogicalTransactionUpdate.CurrentTransaction;
  -- for every permit in the system pull back the value for the ObjectId and the Location
  for i in (select p.objectid, to_char(p.ExpDatePlusGracePeriod, 'YYYY-MM-DD HH24:MI:SS') FormattedDate, p.dup_ExpDatePlusGracePeriod dup_FormattedDate
              from query.o_ABC_Permit p) loop

    -- if the count is zero there is no value and we need to insert one
    if i.dup_FormattedDate is null  and i.FormattedDate is not null then
      insert into possedata.objectcolumndata_t cd
      values (t_CurrTransaction,
             i.ObjectId,
             t_ColumnDefId,
             to_date(null),
             to_date(null),
             i.FormattedDate,
             to_number(null),
             lower(substr(i.FormattedDate, 1, 20)), --the search field is limited to 20 characters
             to_number(null));

    -- otherwise update the existing row in objectcolumndata_t
    elsif i.FormattedDate is not null then
      update possedata.objectcolumndata_t cd
         set cd.AttributeValue = i.FormattedDate,
             cd.SearchValue = lower(substr(i.FormattedDate, 1, 20)) --the search field is limited to 20 characters
       where cd.objectid = i.objectid
         and cd.columndefid = t_ColumnDefId;
    end if;

    if mod(t_CommitCount, 10000) = 0 then
      t_CommitCount := t_CommitCount + 1;
      api.pkg_LogicalTransactionUpdate.EndTransaction;
      commit;
      api.pkg_LogicalTransactionUpdate.ResetTransaction;
    end if;
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;
