/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 min (in prod)
 * Purpose: This script updates dup_FormattedPermitteeObjectId on o_ABC_Permit
 *   objects. The dbms_output should be copied into a .csv file to show the
 *   difference between dup_FormattedPermitteeObjectId and
 *   FormattedPermitteeObjectId.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_ColumnDefId                         number;
  t_Count                               number := 0;
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  -- CSV column headers
  dbug('PermitObjectId,Permit Number,dup_FormattedPermitteeObjectId,dup_Permittee,' ||
      'FormattedPermitteeObjectId,Permittee');

  t_ColumnDefId := api.pkg_configquery.ColumnDefIdForName(
      'o_ABC_Permit', 'dup_FormattedPermitteeObjectId');

  for i in (
      select
        p.objectid,
        p.FormattedPermitteeObjectId,
        p.dup_FormattedPermitteeObjectId
      from query.o_ABC_Permit p
      where p.objectdeftypeid = 1
      ) loop
    -- exit if dup and non-dup values are equal or both are null
    if (i.FormattedPermitteeObjectId = i.dup_FormattedPermitteeObjectId)
        or (i.FormattedPermitteeObjectId is null and i.dup_FormattedPermitteeObjectId is null)
        or (i.FormattedPermitteeObjectId is null and i.dup_FormattedpermitteeObjectId is not null)
        then
      continue;
    end if;

    -- CSV data
    dbug(i.ObjectId || ',' ||
        api.pkg_ColumnQuery.Value(i.ObjectId, 'PermitNumber') || ',' ||
        i.dup_FormattedPermitteeObjectId || ',"' ||
        api.pkg_ColumnQuery.Value(i.dup_FormattedpermitteeObjectId, 'dup_FormattedName') || '",' ||
        i.FormattedPermitteeObjectId || ',"' ||
        api.pkg_ColumnQuery.Value(i.FormattedpermitteeObjectId, 'dup_FormattedName') ||'"');

    -- The dup value is not set
    if i.FormattedPermitteeObjectId is not null and i.dup_FormattedPermitteeObjectId is null then
      insert into possedata.objectcolumndata
      (
      logicaltransactionid,
      objectid,
      columndefid,
      attributevalue,
      numericsearchvalue
      )
      values
      (
      api.pkg_logicaltransactionupdate.CurrentTransaction,
      i.ObjectId,
      t_ColumnDefId,
      i.FormattedPermitteeObjectId,
      i.FormattedPermitteeObjectId
      );

    -- Both fields are populated but inconsistent
    elsif (i.FormattedPermitteeObjectId is not null and i.dup_FormattedPermitteeObjectId is not null)
        and (i.FormattedPermitteeObjectId != i.dup_FormattedPermitteeObjectId) then
      update possedata.objectcolumndata cd
         set cd.LogicalTransactionId = api.pkg_logicaltransactionupdate.CurrentTransaction,
             cd.AttributeValue = i.FormattedPermitteeObjectId,
             cd.NumericSearchValue = i.FormattedPermitteeObjectId
      where cd.ObjectId = i.objectid
        and cd.ColumnDefId = t_ColumnDefId;
    end if;

    t_Count := t_Count + 1;
    if mod(t_count, 1000) = 0 then
      api.pkg_logicaltransactionupdate.EndTransaction;
      commit;
      api.pkg_logicaltransactionupdate.ResetTransaction;
    end if;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;

  dbug('Number of Updated Objects: ' || t_Count || ',,,,,,');
  commit;

end;
