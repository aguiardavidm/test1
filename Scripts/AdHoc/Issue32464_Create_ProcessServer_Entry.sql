-- RUN AS POSSEDBA
-- Issue 32464 - Data Warehouse synchronization failure
-- Created on 06/15/2018
-- DESCRIPTION: This script inserts an hourly job (every 4 hours) to check for
-- Data Warehouse sync errors
  declare
    t_ScheduleId                        number;
    t_Frequency                         api.pkg_Definition.udt_Frequency;
  begin

    if user != 'POSSEDBA' then
      api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
    end if;

    t_Frequency := api.pkg_Definition.gc_Every;
    api.pkg_LogicalTransactionUpdate.ResetTransaction;

    begin
      select s.ScheduleKey
      into t_ScheduleId
      from procsrvr.sysschedule s
      where s.Description = 'Check Data Warehouse sync failure';
      api.pkg_ProcessServer.RemoveScheduledItem(t_ScheduleId);
    exception
      when no_data_found then
        t_ScheduleId := null;
    end;
    if t_ScheduleId is not null then
      api.Pkg_Processserver.RemoveScheduledItem(t_ScheduleId);
    end if;
    t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure(
        'abc.pkg_ABC_Common.CheckDataWarehouseFailure',
                     --Name
        'Check Data Warehouse sync failure',
                     --Description
        t_Frequency, --Frequency
        4,           --IncrementBy
        2,           --Unit
        null,        --Arguments
        'N',         --SkipBacklog
        'Y',         --DisableOnError
        'Y',         --Sunday
        'Y',         --Monday
        'Y',         --Tuesday
        'Y',         --Wednesday
        'Y',         --Thursday
        'Y',         --Friday
        'Y',         --Saturday
        'N',         --MonthEnd
        trunc(sysdate) + 1,
                     --Start at midnight
        null,        --EndDate
        null,        --Email
        null,        --ServerId
        api.pkg_Definition.gc_normal)
                     --Priority
        ;
    api.pkg_LogicalTransactionUpdate.EndTransaction;
    commit;

  end;
