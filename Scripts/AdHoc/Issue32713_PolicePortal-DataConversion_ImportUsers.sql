-- Created on 8/9/2017 by MICHEL.SCHEFFERS 
-- Issue 32713 - Police Portal - Data Conversion - Import Users
-- Run time depending on number of users. Initial run less than 1 minute for 190 users.
-- Be sure to check the DBMS Output and table for any errors!!
declare 
  -- Local variables here
  t_Count                integer;
  t_AuthenticationName   varchar(40);
  t_UserId               integer;
  t_AuthenticationId     integer;
  t_Name                 varchar(4000);
  t_Password             varchar(4000) := abc.Pkg_Abc_Users.Encrypt('Dog$1234');
  t_ABCPoliceAccessGroup integer;
  t_MunicipalityList     api.pkg_Definition.udt_StringList;  
  t_CountyMuni           varchar(4);
  t_Municipality         integer;
  t_RelId                integer;
  t_EndPointId           integer := api.pkg_configquery.EndPointIdForName('u_Users', 'MunicipalityPolice');
  t_UsersRead            integer := 0;
  t_UsersCreated         integer := 0;
  t_Objects              api.udt_ObjectList;
  t_AllObjects           api.pkg_Definition.udt_IdList;
begin
  -- Test statements here
  if user != 'POSSESYS' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSESYS');
  end if;

  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);

  select accessgroupid
  into   t_ABCPoliceAccessGroup
  from   api.accessgroups
  where  description = 'Police Users';
  
  for p in (select ip.*, ip.rowid
            from   possedba.issue32713_police ip
            where  ip.userid is null
           ) loop
     t_Name := trim (p.firstname || ' ' || p.lastname);
     t_UsersRead := t_UsersRead + 1;
     dbms_output.put_line ('Processing ' || t_Name);

     -- Check Mandatory Fields
     if p.ranktitle     is null or
        p.firstname     is null or
        p.Lastname      is null or
        p.emailaddress  is null or
        p.businessphone is null then
        dbms_output.put_line ('           ' || t_Name || ' (Rowid: ' || p.rowid || ') is missing required information');
        update possedba.issue32713_police p1
        set    p1.Error = t_Name || ' (Rowid: ' || p.rowid || ') is missing required information'
        where  p1.rowid = p.rowid;
        continue;
     end if;

     -- Check First/Last Name
/*     select count(*) 
     into   t_Count
     from   api.users
     where  upper(name) = upper(t_name);
     if t_Count > 0 then
        dbms_output.put_line ('           ' || t_Name || ' already exists in the system. Please change the name.  Eg: Add middle initials');
        update possedba.issue32713_police p1
        set    p1.Error = t_Name || ' already exists in the system. Please change the name.  Eg: Add middle initials'
        where  p1.rowid = p.rowid;
        continue;
     end if;*/
     
     -- Check Email: no email for Police Users, if Muni User with same email then de-activate it
     select count(*)
     into   t_Count
     from   table(cast(api.pkg_simplesearch.castableobjectsbyindex('u_Users', 'EmailAddress', p.EmailAddress) as api.udt_ObjectList)) x
     join   query.u_users u on u.ObjectId = x.objectid
     where  u.usertype in ('Public', 'Police');
     if t_Count > 0 then
        dbms_output.put_line ('           Email address ' || p.emailaddress || ' has already been used.');
        update possedba.issue32713_police p1
        set    p1.Error = 'Email address ' || p.emailaddress || ' has already been used'
        where  p1.rowid = p.rowid;
        continue;
     end if;
     for u1 in (select u.ObjectId
               from   table(cast(api.pkg_simplesearch.castableobjectsbyindex('u_Users', 'EmailAddress', p.EmailAddress) as api.udt_ObjectList)) x
               join   query.u_users u on u.ObjectId = x.objectid
               where  u.usertype = 'Municipal'
              ) loop
        dbms_output.put_line ('           Deactivating Municipal User ' || api.pkg_columnquery.Value(u1.objectid, 'FormattedName') || '; Municipality ' || api.pkg_columnquery.Value(u1.objectid, 'MunicipalityName'));
        api.pkg_columnupdate.SetValue(u1.objectid, 'Active', 'N');
     end loop;

     -- Create new Police User
     loop
       select dbms_random.value(10000000, 99999999)
       into   t_AuthenticationId
       from   dual;
       t_AuthenticationName := upper(substr(p.FirstName, 0, 1)) ||
                               upper(substr(p.LastName, 0, 1))  || t_AuthenticationId;
       select count (*)
       into   t_Count
       from   api.users u
       where  u.AuthenticationName = t_AuthenticationName;                             
       if t_Count = 0 then
          exit;
       end if;
     end loop;
     dbms_output.put_line ('           Adding Police User ' || t_Name || '. AuthenticationName ' || t_AuthenticationName || ' (' ||
         substr(trim(t_Name), 0, 50) || t_AuthenticationId || '_P)');
     t_UserId := api.pkg_UserUpdate.New(a_UserManagementScheme => 'PublicInternetUsers',
                                        a_Name                 => substr(trim(t_Name), 0, 50) || t_AuthenticationId || '_P',
                                        a_ShortName            => t_AuthenticationName,
                                        a_AuthenticationName   => t_AuthenticationName,
                                        a_Password             => t_Password);
     api.pkg_ColumnUpdate.SetValue(t_UserId, 'RankTitle', p.Ranktitle);
     api.pkg_ColumnUpdate.SetValue(t_UserId, 'FirstName', p.Firstname);
     api.pkg_ColumnUpdate.SetValue(t_UserId, 'LastName', p.Lastname);
     api.pkg_ColumnUpdate.SetValue(t_UserId, 'PhoneNumber', p.businessphone);
     api.pkg_ColumnUpdate.SetValue(t_UserId, 'ExtensionNumber', p.phoneextension);
     api.pkg_ColumnUpdate.SetValue(t_UserId, 'BusinessPhone', p.cellphone);
     api.pkg_ColumnUpdate.SetValue(t_UserId, 'EmailAddress', p.EmailAddress);
     api.pkg_ColumnUpdate.SetValue(t_UserId, 'UserType', 'Police');
     api.pkg_columnUpdate.setValue(t_UserId, 'OSUserId', t_AuthenticationName);
     api.pkg_columnUpdate.SetValue(t_UserId, 'PasswordWeb' ,t_Password);
     t_UsersCreated := t_UsersCreated + 1;
     
     -- Relate Municipalities
     t_MunicipalityList := extension.pkg_utils.split(replace(p.municipality, ', ', ','), ',');
     for i in 1..t_MunicipalityList.count loop
        t_CountyMuni := substr(t_MunicipalityList(i), 0, 4);
        t_Objects := api.udt_ObjectList();
        extension.pkg_CxProceduralSearch.InitializeSearch('o_ABC_Office');
        extension.pkg_CxProceduralSearch.SearchByIndex('dup_MunicipalityCountyCode', t_CountyMuni, null, true);
        extension.pkg_CxProceduralSearch.PerformSearch(t_Objects, 'and');
        if t_Objects.count = 0 then
          dbms_output.put_line ('           No Municipality record found for ' || t_MunicipalityList (i));   
        else  
          dbms_output.put_line ('           Relating Municipality ' || t_MunicipalityList (i));   
          t_RelId := api.pkg_relationshipupdate.New(a_EndPoint   => t_EndPointId,
                                                    a_FromObject => t_UserId,
                                                    a_ToObject   => t_Objects(1).ObjectId,
                                                    a_AsOfDate   => sysdate);
        end if;
     end loop;

     -- Add Police User to correct Access Group
     api.pkg_UserUpdate.AddToAccessGroup(t_UserId, t_ABCPoliceAccessGroup);

     -- Update the temporary table
     update possedba.issue32713_police p1
     set    p1.dateentered        = sysdate,
            p1.AuthenticationName = t_AuthenticationName,
            p1.userid             = t_UserId
     where  p1.rowid              = p.rowid;
                                           
  end loop;
  
  dbms_output.put_line (t_UsersRead || ' Police Users selected.');
  dbms_output.put_line (t_UsersCreated || ' New Police Users created.');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
