-- Created on 6/2/2017 by Michel.Scheffers
-- Complete processes p_ABC_DraftNonRenewalNotice for Non-Renewal jobs created on 3/1/2017 or 5/6/2017

declare
    t_ProcessTypeId       integer;
    p                     integer := 0;
    t_Outcome             varchar2(100):= 'Sent';
    t_SysId               integer;
    t_NonReviewMonthNew   varchar2(50) := 'June';
    t_NonReviewDayNew     number := 23;
    t_NonReviewMonth      varchar2(50);
    t_NonReviewDay        number;
begin
   dbms_output.enable(null);
   api.pkg_logicaltransactionupdate.ResetTransaction();

   select s.ObjectId, s.PRNonRenewalReviewDay, s.PRNonRenewalReviewMonth
   into   t_SysId, t_NonReviewDay, t_NonReviewMonth
   from   query.o_systemsettings s;
   api.pkg_columnupdate.SetValue(t_SysId, 'PRNonRenewalReviewDay', t_NonReviewDayNew);
   api.pkg_columnupdate.SetValue(t_SysId, 'PRNonRenewalReviewMonth', t_NonReviewMonthNew);
   dbms_output.put_line('Setting date to ' || t_NonReviewMonthNew || ' ' || t_NonReviewDayNew);   

   select pt.ProcessTypeId
   into   t_ProcessTypeId
   from   api.processtypes pt
   where  pt.Name = 'p_ABC_DraftNonRenewalNotice';

   dbms_output.put_line('Complete processes p_ABC_DraftNonRenewalNotice:');   
   FOR i IN (select j.ExternalFileNum, p.ProcessId
             from   query.j_abc_prnonrenewal j
             join   api.processes p on p.JobId = j.ObjectId
             join   api.processtypes pt on pt.ProcessTypeId = p.ProcessTypeId
             where  to_date (trunc (j.CreatedDate)) = to_date('05/06/2017','mm/dd/yyyy')
             and    p.Outcome is null
             and    p.ProcessTypeId = t_ProcessTypeId
            ) LOOP                                        
     p := p + 1;
     dbms_output.put_line (i.Processid || ' for job ' || i.externalfilenum);
     api.pkg_processupdate.Complete(i.ProcessId, t_Outcome);       
   END LOOP;
    
   api.pkg_columnupdate.SetValue(t_SysId, 'PRNonRenewalReviewDay', t_NonReviewDay);
   api.pkg_columnupdate.SetValue(t_SysId, 'PRNonRenewalReviewMonth', t_NonReviewMonth);
   api.pkg_logicaltransactionupdate.EndTransaction();
   commit;
   dbms_output.put_line (p || ' Processes updated');
   dbms_output.put_line('Setting date back to ' || t_NonReviewMonth || ' ' || t_NonReviewDay);   

end;
