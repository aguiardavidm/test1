  -- Running Document fix for Permit Number 21268.
  -- 08/05/2015
declare 
  t_RelationshipId      number (9);
  t_PermitObjectId      number (9) := 31703442; -- ObjectId for Permit 21268
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();

  
  -- Find correct Relationship Id for the Permit - Certificate
  select min (r.RelationshipId)
  into t_RelationshipId
  from api.relationships r
  join api.objects o on o.ObjectId = r.ToObjectId
  join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
 where r.FromObjectId = t_PermitObjectId
   and od.Name = 'd_PosseReportLetter';

  -- Use the found RelationshipId as parameter:   
  api.pkg_relationshipupdate.Remove(t_RelationshipId);
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;
