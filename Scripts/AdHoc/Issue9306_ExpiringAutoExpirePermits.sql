-- Created on 8/15/2016 by MICHEL.SCHEFFERS 
-- Issue 9306 - Expiring permits with expiration date before today and are set to auto-expire

declare 
  -- Local variables here
  i            integer := 0;
  t_JobId      integer;
  t_RelId      integer;
  t_EndpointId integer := api.pkg_configquery.EndPointIdForName('j_ABC_Expiration', 'Permit');
  t_JobtypeId  integer := api.pkg_ConfigQuery.ObjectDefIdForName('j_ABC_Expiration');
  t_PermitType integer;
begin
  -- Test statements here
  dbms_output.enable(null);
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for pt1 in (select pt.ObjectId
              from   query.o_abc_permittype pt
			       ) loop
     if api.pkg_columnquery.Value (pt1.ObjectId,'AutoExpire') = 'Y' then   
        dbms_output.put_line ('Expiring permit type ' || api.pkg_columnquery.Value(pt1.objectid,'Name') || ':');
        for pe in (select ppt.PermitId, api.pkg_columnquery.DateValue(ppt.PermitId,'ExpirationDate') ExpirationDate
                   from   query.r_abc_permitpermittype ppt
                   where  ppt.PermitTypeId = pt1.objectid
                   and    api.pkg_columnquery.Value(ppt.PermitId,'State') = 'Active'
                   and    trunc(api.pkg_columnquery.DateValue(ppt.PermitId,'ExpirationDate')) < trunc (sysdate)
                   order  by ppt.PermitId          
				          ) loop
           dbms_output.put_line ('     Permit Number ' || api.pkg_columnquery.Value(pe.permitid,'PermitNumber') || '; Expiration Date ' || pe.expirationdate);
           i := i + 1;
           t_JobId := api.pkg_jobupdate.New(t_JobtypeId, 'Expiring Older AutoExpire Permits', null);
	         t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_JobId, pe.permitid, sysdate);
        end loop;
	   end if;
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  dbms_output.put_line ('Expired ' || i || ' permits');
end;
