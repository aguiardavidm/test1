-- Create table
create table possedba.LOG1218LICENSEWARNINGS
(
  licenseid      NUMBER,
  licensenumber  VARCHAR2(40),
  expirationdate DATE,
  batchdate      DATE
);
/
--12.18
declare
  t_LicenseList         api.pkg_definition.udt_IdList;
  t_BatchDate           date;
  t_JobId               api.pkg_definition.udt_IdList;
  t_LicenseWarningCount number;
  t_Petitions           api.udt_objectlist;
  t_PetitionExists      varchar2(2);
  t_PetitionCount       number;
  t_ConvertedPetition   number;
  t_AlreadyProcessed    number;
  t_RenewalCount        number;
  t_LicWarningId        api.pkg_definition.udt_Id;
  t_RelId               api.pkg_definition.udt_Id;
  t_LicWarningType      api.pkg_definition.udt_Id := api.pkg_simplesearch.ObjectByIndex('o_ABC_LicenseWarningType', 'LicenseWarningType', '12.18 Restriction');
  t_LicWarningTypeEP    api.pkg_definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_LicenseWarning', 'LicenseWarning');
  t_LicLicWarningEP     api.pkg_definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseWarning');
  t_LicenseWarningDef   api.pkg_definition.udt_Id := api.pkg_configquery.ObjectDefIdForName('o_ABC_LicenseWarning');

begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_LicenseList := api.pkg_simplesearch.ObjectsByIndex('o_ABC_License', 'State', 'Active');
  t_BatchDate := trunc(sysdate);
  for i in 1 .. t_LicenseList.count loop
    --Make sure to skip licenses marked off in Joann's list
    if api.pkg_columnquery.Value(t_LicenseList(i), 'LicenseNumber') not in ('0102-33-008-008','0102-44-212-003','0117-33-011-012','0231-33-027-005','0266-33-002-007','0304-36-001-003','0310-31-009-001','0318-33-002-006','0319-33-007-006','0320-33-013-001',
                                                                            '0339-33-006-006','0408-33-062-007','0408-33-167-008','0408-33-182-007','0411-33-001-010','0425-33-006-009','0436-33-008-002','0610-36-027-003','0614-33-019-007','0701-33-006-011',
                                                                            '0701-33-043-009','0702-33-036-012','0705-32-024-005','0705-44-016-004','0709-33-013-003','0709-33-060-003','0711-33-006-005','0711-33-007-006','0711-44-018-006','0714-31-934-002',
                                                                            '0714-32-046-007','0714-32-335-002','0714-32-698-006','0714-33-068-008','0714-33-217-006','0714-33-248-002','0714-33-280-002','0714-33-303-010','0714-33-363-004','0714-33-409-006',
                                                                            '0714-33-476-003','0714-33-498-005','0714-33-508-005','0714-33-569-008','0714-33-626-007','0714-33-655-008','0714-33-663-004','0714-44-598-007','0714-44-734-006','0717-33-071-006',
                                                                            '0719-33-001-005','0722-33-071-003','0901-33-104-006','0903-33-027-005','0904-44-052-008','0905-33-081-003','0906-33-162-003','0906-33-163-006','0906-33-212-006','0906-33-229-012',
                                                                            '0906-33-282-008','0906-33-403-004','0906-33-493-003','0908-33-098-006','0909-33-032-007','0910-33-063-005','0910-33-093-005','0910-33-100-010','0910-33-148-007','0910-33-177-006',
                                                                            '1201-33-003-004','1305-34-001-004','1306-34-003-004','1306-34-011-008','1325-34-029-012','1348-34-007-005','1350-33-012-006','1412-33-006-004','1425-33-004-006','1514-33-024-006',
                                                                            '1520-33-001-005','1601-33-003-004','1608-33-037-003','1608-33-238-006','1608-33-278-004','1608-33-315-014','1608-44-040-006','1613-33-002-005','1703-33-001-005','1707-33-007-005',
                                                                            '1710-44-007-001','1812-33-002-005','1912-33-002-009','1913-33-001-006','2004-33-082-004','2004-33-244-005','2004-33-246-006','2007-32-013-008','2009-33-014-007','2009-33-038-002',
                                                                            '2012-33-026-004','2013-33-011-014','2013-33-034-007','2019-33-070-008','2107-33-005-003','2115-33-002-006','2119-33-006-004','2120-33-004-002'
                                                                           ) then
      t_PetitionExists := 'N';
      --Make sure Inactivity Start Date is entered and 2 terms have passed
      if api.pkg_columnquery.DateValue(t_LicenseList(i), 'ExpirationDate') < trunc(t_BatchDate) then
        if api.pkg_columnquery.Value(t_LicenseList(i), 'IssuingAuthority') = 'Municipality' then
          --Check for an existing current warning on the license
          select count(1)
            into t_LicenseWarningCount
            from query.r_WarningLicense r
           where r.LicenseId = t_LicenseList(i)
             and api.pkg_columnquery.Value(r.LicenseWarningId, 'WarningTypeActive') = 'Y'
             and (t_BatchDate < nvl(api.pkg_columnquery.DateValue(r.LicenseWarningId, 'EndDate'), to_date(3999, 'yyyy')));
          if t_LicenseWarningCount = 0 then
            --Check for an existing petition over the stored rel.
            select count(1)
              into t_PetitionCount
              from query.r_abc_LicensePetition j
              join query.r_ABC_PetitionPetitiontype r on r.PetitionId = j.PetitionId
              left outer join dataconv.j_abc_petition d on d.objectid = j.PetitionId
             where j.LicenseId = t_LicenseList(i)
               and (api.pkg_columnquery.Value(j.PetitionId, 'StatusDescription') = 'Approved' or d.statustag = 'COMP')
               and api.pkg_columnquery.Value(r.PetitionTypeId, 'PetitionType') = '12.18'
               and t_BatchDate between api.pkg_columnquery.DateValue(r.PetitionTypeId, 'StartDate') and api.pkg_columnquery.DateValue(r.PetitionTypeId, 'EndDate');
             --Check for petitions over the procedural rel
            t_Petitions := api.udt_objectlist();
            abc.pkg_ABC_ProceduralRels.PetitionJobsForLicense(t_LicenseList(i), null, t_Petitions);
            for j in 1 .. t_Petitions.count() loop
              if api.pkg_columnquery.Value(t_Petitions(j).objectid, 'StatusDescription') in ('Approved', 'Completed') then
                for pt in (select r.PetitionTypeId, api.pkg_columnquery.Value(t_Petitions(j).objectid, 'StatusDescription') Status
                             from query.r_abc_petitionpetitiontype r
                            where r.PetitionId = t_Petitions(j).objectid
                              and api.pkg_columnquery.Value(r.petitiontypeid, 'PetitionType') = '12.18'
                              and t_BatchDate between api.pkg_columnquery.DateValue(r.petitiontypeid, 'StartDate') 
                                                      and api.pkg_columnquery.DateValue(r.petitiontypeid, 'EndDate')) loop
                  if pt.status = 'Approved' then
                    t_PetitionExists := 'Y';
                  elsif pt.status = 'Completed' then
                    select count(*)
                      into t_ConvertedPetition
                      from dataconv.j_abc_petition j
                     where j.objectid = t_Petitions(j).objectid
                       and j.statustag = 'COMP';
                    if t_ConvertedPetition > 0 then
                      t_PetitionExists := 'Y';
                    end if;
                  end if;
                  exit;
                end loop; -- Petition Type
              end if; -- Petition Approved
              if t_PetitionExists = 'Y' then
                exit;
              end if;
            end loop; -- Petition
            if t_Petitionexists = 'N' and t_PetitionCount = 0 then
              --Check for an in progress renewal
              select count(*)
                into t_RenewalCount
                from query.r_ABC_LicenseToRenewJobLicense ra
               where ra.LicenseObjectId = t_LicenseList(i)
                 and api.pkg_columnquery.Value(ra.RenewJobId, 'StatusName') not in ('CANCEL', 'REJECT');
              if t_RenewalCount = 0 then
                --Create Warning and relate to license
                t_LicWarningId := api.pkg_objectupdate.New(t_LicenseWarningDef);
                t_RelId := api.pkg_relationshipupdate.New(t_LicWarningTypeEP, t_LicWarningId, t_LicWarningType);
                api.pkg_columnupdate.SetValue(t_LicWarningId, 'StartDate', trunc(t_BatchDate));
                api.pkg_columnupdate.SetValue(t_LicWarningId, 'Comments', '12.18 RULING ISSUED FOR ' ||
                                                                          (to_char(t_BatchDate, 'yyyy')-1) || '-' ||
                                                                          to_char(t_BatchDate, 'yyyy') || ' ON ' ||
                                                                          to_char(t_BatchDate, 'MON dd, yyyy'));
                t_RelId := api.pkg_relationshipupdate.New(t_LicLicWarningEP, t_LicenseList(i), t_LicWarningId);
                --log it
                insert into possedba.log1218licensewarnings
                (licenseid, licensenumber, expirationdate, batchdate)
                values
                (t_LicenseList(i),
                api.pkg_columnquery.value(t_LicenseList(i), 'LicenseNumber'),
                api.pkg_columnquery.DateValue(t_LicenseList(i), 'ExpirationDate'),
                t_BatchDate
                );
              end if; -- Renewal Doesn't Exist
            end if; -- Petition Doesn't Exist
          end if; -- Warning Doesn't exist
        end if; -- Municipality Issued
      end if; -- Valid Inactivity Start Date
    end if;
  end loop; -- License loop
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
/

select *
  from possedba.log1218licensewarnings;