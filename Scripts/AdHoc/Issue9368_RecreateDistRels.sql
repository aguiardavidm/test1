--drop table possedba.LogRecreatedDistRels;
--Create logging table
create table possedba.LogRecreatedDistRels as (
select distinct o.RelationshipId, o.RelationshipDefId, o.EndpointId,
                o.Action, o.FromObjectId,
                cl.LicenseObjectId ToObjectId,
                'N' Processed, cast(null as date) ProcessedDate
              from query.p_abc_prretireproducts r
              join api.processes p
                on r.processid = p.processid
              join api.logicaltransactions t
                on p.CompletedByLogicalTransId = t.LogicalTransactionId
              join api.storedrelationshipsaudit o
                on t.LogicalTransactionId = o.LogicalTransactionId
              join query.r_abc_masterlicenselicense ml
                on ml.LicenseObjectId = o.ToObjectId
              join query.r_abc_masterlicenselicense cl 
                on cl.MasterLicenseObjectId = ml.MasterLicenseObjectId
             where o.RelationshipDefId = 1531320
               and o.action = 'Delete'
               and o.EndpointId = 1531321
               and api.pkg_columnquery.value(cl.LicenseObjectId, 'IsLatestVersion') = 'Y'
               and api.pkg_columnquery.Value(cl.LicenseObjectId, 'State') != 'Pending');
/

-- Created on 9/17/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
    t_NewRelId                 number;
    t_RelCount                 number := 0;
    t_CurrTransaction          number;
    t_RelExists                number;

begin
  -- Test statements here

  api.pkg_logicaltransactionupdate.ResetTransaction();
  t_CurrTransaction := api.pkg_logicaltransactionupdate.CurrentTransaction;

  for c in (select *
              from possedba.LogRecreatedDistRels dr
             where dr.Processed = 'N') loop
    --Check to make sure the rel doesn't exist
    select count(1)
      into t_RelExists
      from api.relationships pd
     where pd.FromObjectId = c.fromobjectid
       and pd.ToObjectId = c.toobjectid
       and pd.EndPointId = c.endpointid;
    if t_RelExists = 0 then

      t_NewRelId := possedata.ObjectId_s.nextval;

      --Create the object
      insert into objmodelphys.Objects (
            LogicalTransactionId,
            CreatedLogicalTransactionId,
            ObjectId,
            ObjectDefId,
            ObjectDefTypeId,
            ClassId,
            InstanceId,
            EffectiveStartDate,
            EffectiveEndDate,
            ConfigReadSecurityClassId,
            ConfigReadSecurityInstanceId,
            ObjectReadSecurityClassId,
            ObjectReadSecurityInstanceId
          ) values (
            t_CurrTransaction,
            t_CurrTransaction,
            t_NewRelId,
            c.RelationshipDefId,
            4,
            4,
            c.RelationshipDefId,
            null,
            null,
            4,
            c.RelationshipDefId,
            null,
            null);

      --Create both sides of the relationship
      insert into rel.StoredRelationships (
        RelationshipId,
        EndPointId,
        FromObjectId,
        ToObjectId
      ) values (
        t_NewRelId,
        c.endpointid,
        c.Fromobjectid,
        c.Toobjectid
      );

      insert into rel.StoredRelationships (
        RelationshipId,
        EndPointId,
        FromObjectId,
        ToObjectId
      ) values (
        t_NewRelId,
        c.endpointid+1, --I know that our c.endpointid is 1531321 and that the other endpointid for the rel is 1531322
        c.Toobjectid,
        c.Fromobjectid
      );
      --Log the successful creation.
      update possedba.LogRecreatedDistRels
         set Processed = 'Y', ProcessedDate = sysdate
       where RelationshipId = c.RelationshipId;
      t_RelCount := t_RelCount +1;
    end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  dbms_output.put_line('Releationships Created: ' || t_RelCount);
  commit;
end;