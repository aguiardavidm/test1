-- Created on 4/23/2020 by BENJAMIN
declare 
  -- Local variables here
  t_Count                  integer := 0;
  t_PermitIds              api.Pkg_Definition.udt_IdList;
  t_ConsolidateToPermitId  number;
  t_PermitId               number;
  t_RelId                  number;
  t_FeeCount           number := 0;
  t_NoteDefId              number;
  t_NoteId                 number;
  t_Transactionid          number;
begin
  select nd.NoteDefId
    into t_NoteDefId
    from api.notedefs nd
   where nd.Tag ='GEN';
  dbms_output.put_line('JobId, JobNumber, FeeId, TransactionId');
  -- Loop through all permits valid for consolidation
  for i in (
    select
      (f.amount + f.PaidAmount + f.adjustedamount) TotalOwing,
      f.Description,
      j.jobid,
      j.externalfilenum JobNumber,
      f.TransactionDate,
      f.feeid
    from api.fees f
      join api.jobs j
          on f.JobId = j.JobId
    where j.externalfilenum in (
    	'338834','337859','336747','334342','333113','332170','334247','334214','333123','331010','330786','329243','326276','326522','326422',
		'325743','325694','325388','323722','323604','323017','322998','338919','337193','337025','335530','335529','334897','334813','334372',
		'334103','328995','331033','328996','329250','328997','338888','337979','337922','337678','337673','337204','336895','334956','333146',
		'331790','329035','326510','326053','325518','325399','324663','324450','322598','330081','328457','333020','339993','323706','332853',
		'331007','330389','328211','332965','313782','332870','333143','339744','332904','332044','332036','331141','330412','329572','335549',
		'332849','334314','329096','323599','329473','329413','338664','338295','304749','322829','322826','338615','322547','329213','312838',
		'332989','339829','333432','336665','333429','331667','332972','336257','331143','328081','336234','330075','323882','336178','331766',
		'332802','332803','325734','332372','332374','329668','322364','339734','329139','339956','333275','337813','339977','334592','337075',
		'331146','335769','338064','325972','334633','336598','336999','336253','333409','336098','336221','337429','335912','328378','334330',
		'330429','335429','338384','332301','333224','338608','331012','338032','330431','337822','335698','335890','335457','332105','333026',
		'334976','325894','332099','332339','332860','332861','323977','331439','334196','329036','329177','338356','331435','331957','330807',
		'331653','328380','329643','336842','323873','325555','329490','330090','331123','333422','335935','330300','321066','324353','326612',
		'331523','334650','337963','326545','339757','330577','336601','326320','328393','330321','330828','332319','332373','333249','333402',
		'334120','334374','334403','336111','340368','340375','340379','340382','330567','334375')
    order by f.TransactionDate asc
    ) loop
    begin
      if i.TotalOwing = 0 then
        continue;
      end if;
      t_FeeCount :=0;
      api.pkg_logicaltransactionupdate.ResetTransaction;
      
      --Adjust the Fees to 0
      t_Transactionid := api.pkg_feeupdate.Adjust(i.feeid, i.totalowing * -1, 'System Error Adjustment', sysdate);
      
      -- Add Note to Job for system adjusted fees
      t_NoteId := api.pkg_noteupdate.New(i.jobid, t_NoteDefId, 'Y', to_char(sysdate, 'Mon dd, yyyy HH:MI:SS PM') || '
Issue #54931 - COVID-19 Support
System Fee Adjustment due to Issue #49650');
      
      api.pkg_logicaltransactionupdate.EndTransaction;
      rollback;--commit;
    exception when others then
      --rollback;
      dbms_output.put_line(i.jobid || ', Job Number: ' || i.jobnumber || ', Feeid: ' || i.feeid || ', TransactionId: ' || t_Transactionid || ', Failed: ' || sqlerrm);
      continue;
    end;
    dbms_output.put_line(i.jobid || ', ' || i.jobnumber || ', ' || i.feeid || ', ' || t_Transactionid );
    
    t_Count := t_Count+1;
  end loop;

end;