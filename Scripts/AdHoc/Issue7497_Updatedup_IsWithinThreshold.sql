-- Created on 02/26/2016 by MICHEL.SCHEFFERS 
-- Issue7497: 	Vintage load time from public end
-- Update all dup_IsWithinThreshold for Vintages
declare 
  -- Local variables here
  d                          number := 0;
  t_VintageIds               api.pkg_definition.udt_IdList;
  t_ColumnDefId              number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Vintage', 'dup_IsWithinThreshold');
      
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select v.ObjectId
    bulk collect into t_VintageIds
    from query.o_abc_Vintage v;
  for i in 1 .. t_VintageIds.count loop
     api.pkg_columnupdate.SetValue (t_VintageIds(i), 'dup_IsWithinThreshold', api.pkg_columnquery.Value(t_VintageIds(i), 'IsWithinThreshold'));
     d := d + 1;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
  dbms_output.put_line('Vintage records updated : ' || d);

end;
