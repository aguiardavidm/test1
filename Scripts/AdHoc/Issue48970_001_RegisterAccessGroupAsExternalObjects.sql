/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 1 second
 * Purpose: Register CPL Notes access groups as external object.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_AccessGroupId                       udt_Id;
  t_LogicalTransactionId                udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  select ag.AccessGroupId
  into t_AccessGroupId
  from api.AccessGroups ag
  where ag.Description = 'CPL Notes';

  api.pkg_ObjectUpdate.RegisterExternalObject('o_AccessGroup', t_AccessGroupId);

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;