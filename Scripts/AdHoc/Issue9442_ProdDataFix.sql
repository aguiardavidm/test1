declare
  t_CancelObjId api.pkg_definition.udt_id;
  t_PermitId    api.pkg_definition.udt_id = 49812376; --49779333
  t_EndpointId  api.pkg_definition.udt_id = api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                                                                                   'PermitCancellation');

  t_relId           api.pkg_definition.udt_id;
  t_jobid           api.pkg_definition.udt_Id;
  t_JobEndpointId   api.pkg_definition.udt_Id = api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                                                                                       'PermitToRenew');
  t_ChangeStatus    api.pkg_definition.udt_Id = api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus');
  t_StatusProcessId api.pkg_definition.udt_Id;
  t_SendPermit      api.pkg_definition.udt_Id = api.pkg_configQuery.ObjectDefIdForName('p_ABC_SendPermit');
  t_SendPermitId    api.pkg_definition.udt_Id;
begin
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  select r.ToObjectId
    into t_CancelObjId
    from api.relationships r
   where r.fromobjectid = t_Permitid
     and r.EndPointId = t_EndpointId;
  select re.toobjectid
    into t_jobid
    from api.relationships re
   where re.FromObjectId = t_permitid
     and re.endpointid = t_jobEndpointid;

  api.pkg_objectupdate.Remove(t_CancelObjId);
  api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Active');
  api.pkg_columnupdate.SetValue(t_PermitId,
                                'IssueDate',
                                to_date('08-17-2018', 'MM-DD-YYYY'));
  api.pkg_columnupdate.SetValue(t_PermitId,
                                'EffectiveDate',
                                to_date('08-17-2018', 'MM-DD-YYYY'));
  api.pkg_columnupdate.SetValue(t_PermitId,
                                'ExpirationDate',
                                api.pkg_columnquery.DateValue(t_PermitId,
                                                              'CalculatedExpirationDate'));
  api.pkg_columnupdate.SetValue(t_PermitId,
                                'SystemCancelled',
                                'N');
  update workflow.jobs j set statusid = 1662805 where j.JobId = t_jobid;
  t_sendPermitId = api.pkg_processupdate.New(t_JobId,
                                              t_SendPermit,
                                              null,
                                              null,
                                              null,
                                              null);

  api.pkg_LogicalTransactionUpdate.EndTransaction();
   commit;
end;
