-- Created on 03/23/2017 by Dennis.OConnor 
-- Retire products with expirationdate of 2015 and are not part of any

-- Create table
--drop table possedba.LOG_PRODUCT_RETIREMENT;
create table possedba.LOG_PRODUCT_RETIREMENT
(
  objectId        NUMBER,
  productid       VARCHAR2(20),
  expiration_date DATE,
  current_state   VARCHAR2(20),
  modified_state  VARCHAR2(20)
);

declare 
  current_product_state VARCHAR2(20);

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(NULL);

  for p in (SELECT pp.ObjectId
            FROM query.o_abc_product pp 
            WHERE pp.ExpirationDate = to_date('12/31/2015', 'mm/dd/yyyy')
            AND pp.State = 'Current'
            MINUS
            SELECT DISTINCT (prp.ProductId) 
            FROM query.r_abc_productrenewaltoproduct prp) loop

     current_product_state := api.pkg_columnquery.value(p.objectid, 'State');

     api.pkg_columnupdate.SetValue (p.objectid, 'State', 'Historical');
     api.pkg_columnupdate.SetValue(p.objectid,'DirectExpire','Y');
     dbms_output.put_line ('Updating State for ' || api.pkg_columnquery.Value (p.objectid,'RegistrationNumber') || '-' || api.pkg_columnquery.Value (p.objectid,'Name'));

     insert into possedba.LOG_PRODUCT_RETIREMENT
      (objectId, productid, expiration_date, current_state, modified_state)
      values
      (p.objectid,
       api.pkg_columnquery.value(p.objectid, 'RegistrationNumber'),
       api.pkg_columnquery.DateValue(p.objectid, 'ExpirationDate'),
       current_product_state,
       api.pkg_columnquery.value(p.objectid, 'State') 
      );
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
 -- commit;  
end;