-- Created on 02/09/2016 by MICHEL.SCHEFFERS 
-- Issue7391: Blanket Employement Permit cannot be edited
-- Set number of Individuals for Blanket Employment to 1 when it is null. Permit cannot be edited.
declare 
  -- Local variables here
  t_PermitObjectId     api.pkg_Definition.udt_idlist;
  t_Updated            number := 0;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  select p.ObjectId
    bulk collect into t_PermitObjectId
    from query.o_Abc_Permit p
   where p.State = 'Active'
     and p.NumberOfIndividuals is null
     and p.PermitNumber in ('15006739', '15007115', '15007117', '15007217', '15007342', '15007343', '15007801', '15007802', '15007943', '15008018', '15008034', '15008349', '15009220', '15009967');
  
  for x in 1..t_PermitObjectId.count loop
     dbms_output.put_line('Updating Permit ' || api.Pkg_Columnquery.Value(t_PermitObjectId(x),'PermitNumber'));
     t_Updated := t_Updated + 1;
     api.Pkg_Columnupdate.SetValue(t_PermitObjectId(x), 'NumberOfIndividuals', 1);
  end loop;
     
  dbms_output.put_line ('Permits updated: ' || t_Updated);  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
