-- Created on 8/21/2017 by Michel Scheffers
-- Issue 31533 - License 1205-43-071 
declare 
  -- Local variables here
  t_LicenseObjectId     integer;
  t_LicenseTypeObjectId integer;
  t_LicenseNumber       varchar2(25) := '1205-43-071-001';
  t_NewLicenseNumber    varchar2(25) := '1205-44-071-001';
  t_EndpointId          integer := api.pkg_configquery.EndPointIdForName('o_ABC_License' , 'LicenseType');
  t_RelId               integer;
  t_ReportId            integer;
  t_ProcessId           integer;
  t_JobId               integer;
  t_AppSummDefId        number := api.pkg_configquery.ObjectDefIdForName('p_ABC_GenerateAppSummaryReport'); -- DefId for Process
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;

-- Collect various objects  
  select l.ObjectId
  into   t_LicenseObjectId
  from   query.o_abc_license l
  where  l.LicenseNumber = t_LicenseNumber
  and    l.State = 'Active';

  select lt.ObjectId
  into   t_LicenseTypeObjectId
  from   query.o_abc_licensetype lt
  where  lt.Code = '44';
  
  select llt.RelationshipId
  into   t_RelId
  from   query.r_Abc_Licenselicensetype llt
  where  llt.LicenseObjectId = t_LicenseObjectId;

-- Sever relationship from License to Old License Type and create from License to New License Type
  api.pkg_relationshipupdate.Remove (t_RelId);
  t_RelId := api.pkg_relationshipupdate.New(t_EndpointId, t_LicenseObjectId, t_LicenseTypeObjectId);
  
-- Set new License Number
  api.pkg_columnupdate.SetValue(t_LicenseObjectId, 'LicenseNumber', t_NewLicenseNumber);
  
-- Collect License Certificate info
  select r1.RelationshipId, sl.ProcessId
  into   t_RelId, t_ProcessId
  from   query.r_ABC_NewApplicationLicense r
  join   query.p_abc_sendlicense sl on sl.JobId = r.NewApplicationJobId
  join   query.r_SendLicense_Certificate r1 on r1.ProcessId = sl.ProcessId
  where  r.LicenseObjectId =  t_LicenseObjectId;
  
-- Sever relationship from License to Old License Certificate and create from License to New License Certificate
  delete from rel.StoredRelationships
  where  RelationshipId = t_RelId;
  delete from objmodelphys.Objects
  where  ObjectId = t_RelId;
  t_ReportId := extension.pkg_processserver.ScheduleRTFReport('LicenseCertificate', t_ProcessId, t_ProcessId);
  
-- Collect Application Summary Report info from License
  select r2.RelationshipId, r.NewApplicationJobId
  into   t_RelId, t_JobId
  from   query.r_ABC_NewApplicationLicense r
  join   query.r_NewAppSummary r2 on r2.JobId = r.NewApplicationJobId
  where  r.LicenseObjectId = t_LicenseObjectId;
-- Sever relationship from License to Application Summary Report
  delete from rel.StoredRelationships
  where  RelationshipId = t_RelId;
  delete from objmodelphys.Objects
  where  ObjectId = t_RelId;
  
-- Collect Application Summary Report info from Job
  select r.RelationshipId
  into   t_RelId
  from   api.processes p
  join   query.r_RenwalAppSummary r on r.ProcessId = p.ProcessId
  where  p.ProcessTypeId = t_AppSummDefId
  and    p.JobId = t_JobId;
-- Sever relationship from Job to Application Summary Report
  delete from rel.StoredRelationships
  where  RelationshipId = t_RelId;
  delete from objmodelphys.Objects
  where  ObjectId = t_RelId;
-- Create new Application Summary Report
  toolbox.pkg_workflow.CreateProcessesFromCheckBoxes(t_JobId,
                                                     sysdate,
                                                     'VALUE:Y<Complete,,,,Today>p_ABC_GenerateAppSummaryReport',
                                                     'JOB');
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;  
end;