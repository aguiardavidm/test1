-- Created on 7/24/2017 by MICHEL.SCHEFFERS 
-- Issue 5513 - Register new process
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select processtypeid, p.Name
            from   api.processtypes p 
            where  not exists (select 1 
                               from   query.o_processtype pp 
                               where  pp.processtypeid =  p.processtypeid
                              )
            and    p.Name = 'p_ABC_ReceiptNewInformation')
           ) loop  
    api.pkg_objectupdate.RegisterExternalObject('o_ProcessType', i.processtypeid);
    dbms_output.put_line ('Registering ' || i.Name);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;

end;