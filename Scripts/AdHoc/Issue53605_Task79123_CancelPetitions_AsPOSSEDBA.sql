/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 sec
 * Purpose: This script cancels Petition jobs specified by NJ ABC on issue
 *   53605.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_ChangeStatusProcTypeId              udt_Id;
  t_UpdatedCount                        pls_Integer := 0;
  t_JobIds                              udt_IdList;
  t_ProcessId                           udt_Id;
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_TimeEnd                             number(15);
  t_TimeStart                           number(15);

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  t_TimeStart := dbms_utility.get_time();
  t_ChangeStatusProcTypeId := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');

  select j.JobId
  bulk collect into t_JobIds
  from
    api.jobs j
    join api.jobtypes jt
        on j.JobTypeId = jt.JobTypeId
  where jt.Name = 'j_ABC_Petition'
    and j.ExternalFileNum in (
        '418', '1783', '1133', '1641', '4347', '4399', '4476', '4518', '4866', '4924', '4718',
        '4743', '4752', '4763', '3910', '4050', '4223', '4242', '4272', '4275', '4315', '3456',
        '3720', '3736', '3284', '6678', '7222', '6326', '6329', '6339', '6342', '6361', '6408',
        '6412', '6439', '6448', '6505', '6507', '6509', '6532', '6551', '6552', '6572', '5998',
        '6050', '6091', '6174', '6189', '4987', '4994', '5078', '5338', '5518', '8880', '8893',
        '8895', '8933', '7733', '8971', '8977', '8984', '8987', '8988', '8997', '9017', '9020',
        '9021', '9026', '9030', '7809', '9032', '9033', '9034', '9035', '9036', '9037', '9038',
        '7894', '7912', '7959', '8029', '8030', '8143', '8201', '8280', '8406', '7302', '7394',
        '8665', '8666', '8702', '8703', '8704', '8744', '8789', '8820', '8853', '8854'
        );

  -- If you are looping through a sizable list, the following code is a good way to commit as you go
  for i in 1..t_JobIds.count() loop
    t_ProcessId := api.pkg_ProcessUpdate.New(t_JobIds(i), t_ChangeStatusProcTypeId, null, null,
        null, null);

    api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ReasonForChange', 'Job must be cancelled.');
    api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Cancelled');

    t_UpdatedCount := t_UpdatedCount + 1;
  end loop;

  objmodelphys.pkg_eventqueue.endsession('N');
  
  t_TimeEnd := dbms_utility.get_time();
  dbug('Time Elapsed: ' || (t_TimeEnd - t_TimeStart) / 100.0);

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  dbug('Updated Object Count: ' || t_UpdatedCount);
  commit;

end;

