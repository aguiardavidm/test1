/*-----------------------------------------------------------------------------
 * Creating Author: Ben
 * Expected run time:
 * Purpose: Email with the subject line of 'POSSE ISSUE' was sent from Jit on 10/22
 *  this issue is similar to 51503 and was introduced due to Delivery 51. 
 *  We modified the scripts previously used for 51503 and re-ran this script in Prod
 *  to rescue job 337716.
 *
 * Set the t_JobId to the application you are fixing.
 *---------------------------------------------------------------------------*/
declare
  t_JobId               api.pkg_definition.udt_id := ;
  t_ProcessTypeId       api.pkg_definition.udt_id := api.pkg_configquery.ObjectDefIdForName('p_ABC_MonitorReview');
  t_ChangeStatusTypeId  api.pkg_definition.udt_id := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');
  t_AdminReviewTypeId   api.pkg_definition.udt_id := api.pkg_ConfigQuery.ObjectDefIdForName('p_ABC_AdministrativePermReview');
  t_ProcessToRemoveId   api.pkg_definition.udt_id;
  t_NewProcessID        api.pkg_definition.udt_id;


begin
  
  --Start Transaction
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  -- ProcessId of the Administrative Review process
  select
    p.ProcessId
  into
    t_ProcessToRemoveId
  from
    api.processes p
  where p.JobId = t_Jobid
    and p.ProcessTypeId = t_AdminReviewTypeId;
  
  -- Remove Administrative Review Process
  api.pkg_ProcessUpdate.Remove(t_ProcessToRemoveId);
  
  -- Change Job Status to Municipal Review
  t_NewProcessID := api.pkg_ProcessUpdate.New(t_JobId, t_ChangeStatusTypeId, null, null, null, null);
  api.pkg_ColumnUpdate.SetValue(t_NewProcessID, 'ReasonForChange', 'Job Completed with wrong outcome.');
  api.pkg_ProcessUpdate.Complete(t_NewProcessID, 'Municipal Review');

  --Recreate the missing 'Monitor Review' process
  t_NewProcessID := api.pkg_processupdate.New(t_jobid, t_processtypeid, null, null, null, null);
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  --Manually Commit After Confirming Pending Changes from the DBMS Output if any.
end;