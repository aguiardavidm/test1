-- Created on 2/7/2019 by ADMINISTRATOR 
declare
  -- Local variables here
  t_SearchResults        api.pkg_definition.udt_SearchResults;
  t_Objects              api.pkg_definition.udt_IdList;
  t_AssociatedPermit     integer;
  t_Reason               varchar2(4000);
  t_ObjectId             integer;
  t_CancelEndpointId     integer := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                                                                          'PermitCancellation');
  t_ObjectDefId          integer := api.pkg_configquery.ObjectDefIdForName('o_ABC_PermitCancellation');
  t_RelId                integer;
  t_PermitNumber         varchar2(50);
  t_PermitTypeId         integer;
  t_ActivePermits        api.pkg_definition.udt_idList;
  t_ConsolidatedPermitId integer;
  t_ToEndPointId         integer;
  t_EndPointId           integer;
  t_VehicleId            integer;
  t_VehicleDefId         integer := api.pkg_configquery.ObjectDefIdForName('o_ABC_Vehicle');
  t_Cancelled            integer := 0;
  t_Consolidated         integer := 0;
  t_ConsolidatePermits   varchar2(01);
begin

  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute script as POSSEDBA');
  end if;

  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction;

  select p.ObjectId
  bulk collect
  into t_objects
  from query.o_abc_permit p
  where p.PermitNumber in ('44161',
                           '44784',
                           '47040',
                           '49094',
                           '49638',
                           '51586',
                           '51719',
                           '51929',
                           '51943',
                           '52330',
                           '59971',
                           '60474',
                           '60998',
                           '61006',
                           '61454',
                           '61507',
                           '63296',
                           '63301',
                           '63324',
                           '63341',
                           '63342',
                           '63594',
                           '63651',
                           '63665',
                           '63677',
                           '64219',
                           '64238',
                           '64366',
                           '64367',
                           '64373',
                           '64842',
                           '65309',
                           '65727',
                           '65731',
                           '66194',
                           '66208',
                           '66209',
                           '66294')
      and p.State = 'Active';
  dbms_output.put_line('Permits ' || t_Objects.Count);

  for p in 1 .. t_Objects.Count loop
    -- Associated Permit
    if api.pkg_columnquery.Value(t_Objects(p), 'AssociatedPermitObjectId') is not null then
      t_AssociatedPermit := api.pkg_columnquery.Value(t_Objects(p), 'AssociatedPermitObjectId');
      -- Check for Insignia Permits to consolidate
      t_PermitNumber := api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
      t_PermitTypeId := api.pkg_columnquery.value(t_Objects(p), 'PermitTypeObjectId');
    
      select r.PrimaryPermitObjectId
      bulk collect
      into t_ActivePermits
      from query.r_ABC_AssociatedPermit r
      where r.AsscPermitObjectId = t_AssociatedPermit
          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'State') = 'Active'
          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitNumber') != t_PermitNumber
          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitTypeObjectId') = t_PermitTypeId;
    
      for r in 1 .. t_ActivePermits.count loop
        t_ConsolidatedPermitId := t_ActivePermits(r);
        --Copy Vehicles to Active Permit to Consolidate the Vehicles
        t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Vehicle');
        dbms_output.put_line('    Copying vehicle(s) from ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber') ||
                             ' to ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
        for i in (select *
                  from query.r_abc_permitvehicle rpv
                  where rpv.PermitObjectId = t_ConsolidatedPermitId) loop
          t_VehicleId := api.pkg_objectupdate.New(t_VehicleDefId);
          t_RelId     := api.Pkg_Relationshipupdate.New(t_ToEndPointId, t_Objects(p), t_VehicleId);
          api.pkg_columnupdate.setvalue(t_VehicleId, 'PreviousInsigniaNumber', 
              api.pkg_columnquery.Value(i.vehicleobjectid, 'PreviousInsigniaNumber'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'InsigniaNumber',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'InsigniaNumber'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'MakeModelYear',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'makemodelyear'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'OwnedOrLeasedLimousine',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'ownedorleasedlimousine'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'VIN',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'vin'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateregistration',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateregistration'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateofregistration',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateofregistration'));
        end loop;
        -- Set the permit as Cancelled
        t_Consolidated := t_Consolidated + 1;
        api.pkg_columnupdate.SetValue(t_ConsolidatedPermitId, 'State', 'Cancelled');
        api.pkg_columnupdate.SetValue(t_ConsolidatedPermitId, 'Edit', 'N');
        api.pkg_columnupdate.setValue(t_ConsolidatedPermitId, 'SystemCancelled', 'Y');
        t_ObjectId := api.pkg_objectupdate.New(t_ObjectDefId, sysdate);
        t_Reason   := 'Consolidated with Permit ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
        api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation', sysdate);
        api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
        t_RelId := api.pkg_relationshipupdate.New(t_CancelEndpointId, t_ConsolidatedPermitId, t_ObjectId, sysdate);
        dbms_output.put_line('    ' || t_Reason);
      end loop;
    end if;
  
    -- Associated TAP
    if api.pkg_columnquery.Value(t_Objects(p), 'TAPPermitObjectId') is not null then
      t_AssociatedPermit := api.pkg_columnquery.Value(t_Objects(p), 'TAPPermitObjectId');
      -- Check for Insignia Permits to consolidate
      t_PermitNumber := api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
      t_PermitTypeId := api.pkg_columnquery.value(t_Objects(p), 'PermitTypeObjectId');
      select r.PermitObjectId
      bulk collect
      into t_ActivePermits
      from query.r_ABC_TAPPermit r
      where r.TAPPermitObjectId = t_AssociatedPermit
          and api.pkg_columnquery.Value(r.PermitObjectId, 'State') = 'Active'
          and api.pkg_columnquery.Value(r.PermitObjectId, 'PermitNumber') != t_PermitNumber
          and api.pkg_columnquery.Value(r.PermitObjectId, 'PermitTypeObjectId') = t_PermitTypeId;
      for c in 1 .. t_ActivePermits.count loop
        t_ConsolidatedPermitId := t_ActivePermits(c);
        --Copy Vehicles to Active Permit to Consolidate the Vehicles
        t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Vehicle');
        dbms_output.put_line('    Copying vehicle(s) from ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber') ||
                             ' to ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
        for i in (select *
                  from query.r_abc_permitvehicle rpv
                  where rpv.PermitObjectId = t_ConsolidatedPermitId) loop
          null;
          t_VehicleId := api.pkg_objectupdate.New(t_VehicleDefId);
          t_RelId     := api.Pkg_Relationshipupdate.New(t_ToEndPointId, t_Objects(p), t_VehicleId);
        api.pkg_columnupdate.setvalue(t_VehicleId, 'PreviousInsigniaNumber', 
            api.pkg_columnquery.Value(i.vehicleobjectid, 'PreviousInsigniaNumber'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'InsigniaNumber',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'InsigniaNumber'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'MakeModelYear',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'makemodelyear'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'OwnedOrLeasedLimousine',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'ownedorleasedlimousine'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'VIN',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'vin'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateregistration',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateregistration'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateofregistration',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateofregistration'));
        end loop;
        -- Set the permit as Cancelled
        t_Consolidated := t_Consolidated + 1;
        api.pkg_columnupdate.SetValue(t_ConsolidatedPermitId, 'State', 'Cancelled');
        api.pkg_columnupdate.SetValue(t_ConsolidatedPermitId, 'Edit', 'N');
        api.pkg_columnupdate.setValue(t_ConsolidatedPermitId, 'SystemCancelled', 'Y');
        t_ObjectId := api.pkg_objectupdate.New(t_ObjectDefId, sysdate);
        t_Reason   := 'Consolidated with Permit ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
        api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation', sysdate);
        api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
        t_RelId := api.pkg_relationshipupdate.New(t_CancelEndpointId, t_ConsolidatedPermitId, t_ObjectId, sysdate);
        dbms_output.put_line('    ' || t_Reason);
      end loop;
    end if;
  
    -- Associated License
    if api.pkg_columnquery.Value(t_Objects(p), 'LicenseObjectId') is not null then
      t_AssociatedPermit := api.pkg_columnquery.Value(t_Objects(p), 'LicenseObjectId');
      -- Check for Insignia Permits to consolidate
      t_PermitNumber := api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
      t_PermitTypeId := api.pkg_columnquery.value(t_Objects(p), 'PermitTypeObjectId');
      select r.PermitObjectId
      bulk collect
      into t_ActivePermits
      from query.r_PermitLicense r
      where r.LicenseObjectId = t_AssociatedPermit
          and api.pkg_columnquery.Value(r.PermitObjectId, 'State') = 'Active'
          and api.pkg_columnquery.Value(r.PermitObjectId, 'PermitNumber') != t_PermitNumber
          and api.pkg_columnquery.Value(r.PermitObjectId, 'PermitTypeObjectId') = t_PermitTypeId;
      for s in 1 .. t_ActivePermits.count loop
        t_ConsolidatedPermitId := t_ActivePermits(s);
        --Copy Vehicles to Active Permit to Consolidate the Vehicles
        t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Vehicle');
        dbms_output.put_line('    Copying vehicle(s) from ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber') ||
                             ' to ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
        for i in (select *
                  from query.r_abc_permitvehicle rpv
                  where rpv.PermitObjectId = t_ConsolidatedPermitId) loop
          t_VehicleId := api.pkg_objectupdate.New(t_VehicleDefId);
          t_RelId     := api.Pkg_Relationshipupdate.New(t_ToEndPointId, t_Objects(p), t_VehicleId);
          api.pkg_columnupdate.setvalue(t_VehicleId, 'PreviousInsigniaNumber', 
              api.pkg_columnquery.Value(i.vehicleobjectid, 'PreviousInsigniaNumber'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'InsigniaNumber',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'InsigniaNumber'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'MakeModelYear',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'makemodelyear'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'OwnedOrLeasedLimousine',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'ownedorleasedlimousine'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'VIN',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'vin'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateregistration',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateregistration'));
          api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateofregistration',
              api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateofregistration'));
        end loop;
        -- Set the permit as Cancelled
        t_Consolidated := t_Consolidated + 1;
        api.pkg_columnupdate.SetValue(t_ConsolidatedPermitId, 'State', 'Cancelled');
        api.pkg_columnupdate.SetValue(t_ConsolidatedPermitId, 'Edit', 'N');
        api.pkg_columnupdate.setValue(t_ConsolidatedPermitId, 'SystemCancelled', 'Y');
        t_ObjectId := api.pkg_objectupdate.New(t_ObjectDefId, sysdate);
        t_Reason   := 'Consolidated with Permit ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
        api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation', sysdate);
        api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
        t_RelId := api.pkg_relationshipupdate.New(t_CancelEndpointId, t_ConsolidatedPermitId, t_ObjectId, sysdate);
        dbms_output.put_line('    ' || t_Reason);
      end loop;
    end if;

  end loop;
  dbms_output.put_line('Permits cancelled due to consolidation:                         ' || t_Consolidated);
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
