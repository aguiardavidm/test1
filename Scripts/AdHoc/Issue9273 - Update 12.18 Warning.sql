drop   table possedba.delete1218warning;
create table possedba.delete1218warning
(
   licenseid     number,
   licensenumber varchar2(40),
   oldwarning    varchar2(80),
   newwarning    varchar2(80)
);
insert into possedba.delete1218warning
select l.ObjectId, l.LicenseNumber, null, null
from   query.o_abc_license l
where  l.LicenseNumber in
(
'0106-33-003-006',
'0112-33-027-006',
'0212-33-022-010',
'0414-33-001-012',
'0419-33-001-010',
'0712-33-018-001',
'0714-33-246-009',
'0714-33-408-003',
'0714-33-508-005',
'0714-33-673-011',
'0714-33-790-010',
'0716-33-017-010',
'0716-33-027-011',
'0717-33-007-010',
'0809-36-007-001',
'0814-32-010-005',
'0906-44-140-008',
'1111-33-005-005',
'1205-33-011-009',
'1214-33-054-003',
'1216-33-032-002',
'1216-33-045-009',
'1222-33-003-007',
'1225-33-065-002',
'1412-33-011-009',
'1505-33-004-008',
'1505-33-015-007',
'1506-32-002-005',
'1507-31-035-002',
'1507-33-018-011',
'1608-33-206-008',
'1608-33-246-007',
'1608-33-311-009',
'1608-33-315-014',
'1608-44-040-006',
'1608-44-247-007',
'1616-33-012-009',
'1710-44-007-001',
'1712-33-004-005',
'1803-33-009-002',
'1803-44-008-010',
'1806-44-038-004',
'2009-33-014-008',
'2009-33-038-002',
'2009-33-051-007',
'2013-31-049-001',
'2019-33-050-008',
'2021-44-001-008',
'2115-33-002-006'
);

drop   table possedba.update1218warning;
create table possedba.update1218warning
(
   licenseid     number,
   licensenumber varchar2(40),
   oldwarning    varchar2(80),
   newwarning    varchar2(80)
);
insert into possedba.update1218warning
select l.ObjectId, l.LicenseNumber, null, null
from   query.o_abc_license l
where  l.LicenseNumber in
(
'0102-33-001-005',
'0103-33-005-006',
'0107-33-017-007',
'0108-33-014-008',
'0108-33-021-004',
'0121-33-014-006',
'0201-44-005-004',
'0206-33-016-004',
'0206-44-041-006',
'0218-33-021-008',
'0219-44-023-005',
'0223-31-073-001',
'0235-44-001-005',
'0237-33-008-008',
'0245-44-031-005',
'0251-33-006-001',
'0260-44-003-006',
'0265-33-015-009',
'0265-33-031-003',
'0304-36-001-003',
'0330-33-011-003',
'0339-33-002-005',
'0408-33-065-011',
'0409-36-064-005',
'0414-33-005-004',
'0414-33-014-002',
'0414-33-020-010',
'0425-33-006-009',
'0435-33-009-006',
'0436-33-016-002',
'0614-33-007-010',
'0705-44-016-004',
'0709-33-041-003',
'0714-31-922-001',
'0714-31-937-001',
'0714-33-009-008',
'0714-33-013-005',
'0714-33-133-005',
'0714-33-217-006',
'0714-33-290-003',
'0714-33-293-005',
'0714-33-384-007',
'0714-33-421-006',
'0714-33-434-008',
'0714-33-454-008',
'0714-33-486-002',
'0714-33-489-007',
'0714-33-552-006',
'0714-33-579-002',
'0714-33-600-003',
'0714-33-607-009',
'0714-33-612-008',
'0714-33-648-002',
'0714-33-659-007',
'0714-33-795-004',
'0714-44-412-002',
'0714-44-417-007',
'0716-31-038-001',
'0716-33-026-005',
'0717-33-011-004',
'0717-33-055-003',
'0719-31-020-001',
'0814-33-016-012',
'0818-33-017-005',
'0901-33-004-001',
'0901-33-007-008',
'0901-33-016-004',
'0901-33-020-007',
'0901-33-060-008',
'0905-44-050-004',
'0906-33-008-009',
'0906-33-047-008',
'0906-33-114-002',
'0906-33-191-010',
'0906-33-203-004',
'0906-33-304-002',
'0906-33-317-005',
'0906-33-360-002',
'0906-33-379-003',
'0906-33-382-007',
'0906-33-430-005',
'0906-33-462-003',
'0906-44-093-011',
'0906-44-504-006',
'0908-31-112-001',
'0908-33-032-007',
'0908-33-073-006',
'0908-33-106-003',
'0908-43-037-002',
'0909-36-052-002',
'0910-33-115-008',
'0910-43-005-002',
'0910-43-158-001',
'0911-44-024-007',
'1019-33-002-005',
'1019-33-007-001',
'1111-33-031-003',
'1111-33-032-010',
'1111-33-065-007',
'1111-33-092-009',
'1111-33-198-008',
'1111-33-251-008',
'1111-33-256-005',
'1216-32-100-007',
'1216-33-075-004',
'1220-31-047-001',
'1220-33-037-005',
'1223-33-002-009',
'1323-43-001-002',
'1341-44-003-002',
'1411-33-005-004',
'1435-33-010-006',
'1436-36-026-001',
'1512-33-002-009',
'1601-33-007-009',
'1602-33-047-008',
'1602-33-056-005',
'1602-33-151-009',
'1602-33-156-009',
'1602-44-055-005',
'1603-31-027-001',
'1603-33-013-007',
'1608-33-007-007',
'1608-33-065-005',
'1608-33-079-012',
'1608-33-095-009',
'1608-33-122-011',
'1608-33-193-005',
'1608-33-207-004',
'1608-33-223-005',
'1608-33-277-004',
'1608-33-281-005',
'1608-33-312-004',
'1614-31-055-001',
'1615-33-001-010',
'1615-33-029-007',
'1615-33-032-010',
'1616-33-007-012',
'1707-31-022-002',
'1707-33-013-005',
'1708-33-006-003',
'1810-44-004-005',
'1811-32-021-006',
'1815-33-005-012',
'1816-33-012-009',
'1916-32-004-004',
'2004-33-029-003',
'2004-33-104-006',
'2004-33-191-008',
'2004-44-119-008',
'2007-33-003-006',
'2009-33-037-007',
'2009-33-040-011',
'2013-33-011-014',
'2014-33-005-005',
'2015-33-006-011',
'2016-33-004-005',
'2018-31-025-001',
'2019-33-052-007',
'2019-43-005-008',
'2121-33-007-002'
);
/
declare
   t_LicWarningId        api.pkg_definition.udt_Id;
   t_RelId               api.pkg_definition.udt_Id;
   t_LicWarningType      api.pkg_definition.udt_Id := api.pkg_simplesearch.ObjectByIndex('o_ABC_LicenseWarningType', 'LicenseWarningType', '12.18 Restriction');
   t_LicWarningTypeEP    api.pkg_definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_LicenseWarning', 'LicenseWarning');
   t_LicLicWarningEP     api.pkg_definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseWarning');
   t_LicenseWarningDef   api.pkg_definition.udt_Id := api.pkg_configquery.ObjectDefIdForName('o_ABC_LicenseWarning');
   t_Comment             varchar2(100)             := '12.18 RULING REQUIRED FOR 2016-2017 ON AUG 02, 2016';
   t_WarningUpdated      varchar2(01)              := 'N';
   t_BatchDate           date                      := to_date ('2016/08/02', 'yyyy/mm/dd');   

begin
   dbms_output.enable(null);
   api.pkg_logicaltransactionupdate.ResetTransaction;
   for l in (select *
             from   possedba.delete1218warning
            )
   loop            
      for l2 in (select wl.RelationshipId, wl.LicenseWarningId, lw.Comments
                 from   query.r_warninglicense wl
                 join   query.o_abc_licensewarning lw on lw.ObjectId = wl.LicenseWarningId
                 where  wl.LicenseId = l.licenseid
                 and    lw.Active1218WarningType = 'Y'
                )
      loop
         dbms_output.put_line('Deleting warning for license ' || l.licensenumber || ' from ' || l2.Comments);
         -- Delete old Warning and remove relationship to license
         api.pkg_relationshipupdate.Remove(l2.relationshipid);
         api.pkg_objectupdate.Remove(l2.licensewarningid);
         -- Log it
         update possedba.delete1218warning dw
         set    dw.oldwarning = l2.Comments
         where  dw.licenseid = l.licenseid;
      end loop;
   end loop;
   api.pkg_logicaltransactionupdate.EndTransaction;
   commit;

   api.pkg_logicaltransactionupdate.ResetTransaction;
   for l in (select *
             from   possedba.update1218warning
            )
   loop    
      t_WarningUpdated := 'N';        
      for l2 in (select wl.RelationshipId, wl.LicenseWarningId, lw.Comments
                 from   query.r_warninglicense wl
                 join   query.o_abc_licensewarning lw on lw.ObjectId = wl.LicenseWarningId
                 where  wl.LicenseId = l.licenseid
                 and    lw.Active1218WarningType = 'Y'
                )
      loop
         t_WarningUpdated := 'Y';
         dbms_output.put_line('Updating warning for license ' || l.licensenumber || ' from ' || l2.Comments || ' to ' || t_Comment);
         api.pkg_columnupdate.SetValue(l2.LicenseWarningId, 'StartDate', trunc(t_BatchDate));
         api.pkg_columnupdate.SetValue(l2.LicenseWarningId, 'Comments', t_Comment);
         -- Log it
         update possedba.update1218warning dw
         set    dw.oldwarning = l2.Comments,
                dw.newwarning = t_Comment
         where  dw.licenseid = l.licenseid;
      end loop;
      if t_WarningUpdated = 'N' then
         dbms_output.put_line('Updating warning for license ' || l.licensenumber || ' from .. to ' || t_Comment);
         -- Create new Warning and relate to license
         t_LicWarningId := api.pkg_objectupdate.New(t_LicenseWarningDef);
         t_RelId := api.pkg_relationshipupdate.New(t_LicWarningTypeEP, t_LicWarningId, t_LicWarningType);
         api.pkg_columnupdate.SetValue(t_LicWarningId, 'StartDate', trunc(t_BatchDate));
         api.pkg_columnupdate.SetValue(t_LicWarningId, 'Comments', t_Comment);
         t_RelId := api.pkg_relationshipupdate.New(t_LicLicWarningEP, l.licenseid, t_LicWarningId);
         -- Log it
         update possedba.update1218warning dw
         set    dw.oldwarning = null,
                dw.newwarning = t_Comment
         where  dw.licenseid = l.licenseid;
      end if;
   end loop;
   api.pkg_logicaltransactionupdate.EndTransaction;
   commit;
end;   
