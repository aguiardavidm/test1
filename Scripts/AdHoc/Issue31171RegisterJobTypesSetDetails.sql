/*-----------------------------------------------------------------------------
 * Author: Jae Holderby / Maria Knigge
 * Expected run time: 1.5 minutes
 * Purpose: This script registers jobs types that have been created as external
 *  objects, sets IsABCJob, IsPermitJob, and IsLicenseJob for applicable job
 *  types and defaults the Automatic Cancellation Period for all Job, License
 *  and Permit Types to 180 days. Note: this script is designed to be reused.
 *---------------------------------------------------------------------------*/
declare
  t_JobTypeIds                          api.pkg_definition.udt_IdList;
  t_JobTypeObjectIds                    api.pkg_definition.udt_IdList;
  t_LicenceJobTypeIds                   api.pkg_definition.udt_IdList;
  t_PermitJobTypeIds                    api.pkg_definition.udt_IdList;
  t_TypeIds                             api.pkg_definition.udt_IdList;
begin

  api.pkg_LogicalTransactionUpdate.ResetTransaction();

  --register all j_ABC job types
  select jt.JobTypeId
  bulk collect into t_JobTypeIds
  from api.jobtypes jt
  where jt.Name in (
      'j_ABC_1218WarningBatch',
      'j_ABC_1239WarningBatch',
      'j_ABC_Accusation',
      'j_ABC_AmendmentApplication',
      'j_ABC_Appeal',
      'j_ABC_BatchRenewalNotification',
      'j_ABC_BatchRenewalNotifRequest',
      'j_ABC_Complaint',
      'j_ABC_CPLSubmissions',
      'j_ABC_DailyDeposit',
      'j_ABC_Expiration',
      'j_ABC_Inspection',
      'j_ABC_MiscellaneousRevenue',
      'j_ABC_NewApplication',
      'j_ABC_PaymentAdjustment',
      'j_ABC_PermitApplication',
      'j_ABC_PermitRenewal',
      'j_ABC_Petition',
      'j_ABC_PRAmendment',
      'j_ABC_PRApplication',
      'j_ABC_PRNonRenewal',
      'j_ABC_PRRenewal',
      'j_ABC_Reinstatement',
      'j_ABC_RenewalApplication')
  minus
  select JobTypeId
  from query.o_jobtypes;

  for i in 1..t_JobTypeIds.count() loop
    api.pkg_Objectupdate.RegisterExternalObject('o_JobTypes', t_JobTypeIds(i));
  end loop;

  -- Set IsABCJob for applicable types. Currently j_ABC_Inspection, j_ABC_Complaint,
  -- j_ABC_Accusation do not apply for the Job Types Search as they are not yet implemented
  select jt.ObjectId
  bulk collect into t_JobTypeObjectIds
  from query.o_jobtypes jt
  where jt.Name in (
      'j_ABC_1218WarningBatch',
      'j_ABC_1239WarningBatch',
      'j_ABC_AmendmentApplication',
      'j_ABC_Appeal',
      'j_ABC_BatchRenewalNotification',
      'j_ABC_BatchRenewalNotifRequest',
      'j_ABC_CPLSubmissions',
      'j_ABC_DailyDeposit',
      'j_ABC_Expiration',
      'j_ABC_MiscellaneousRevenue',
      'j_ABC_NewApplication',
      'j_ABC_PaymentAdjustment',
      'j_ABC_PermitApplication',
      'j_ABC_PermitRenewal',
      'j_ABC_Petition',
      'j_ABC_PRAmendment',
      'j_ABC_PRApplication',
      'j_ABC_PRNonRenewal',
      'j_ABC_PRRenewal',
      'j_ABC_Reinstatement',
      'j_ABC_RenewalApplication')
     and jt.IsABCJob = 'N';

  for j in 1..t_JobTypeObjectIds.count() loop
    api.pkg_columnupdate.SetValue(t_JobTypeObjectIds(j), 'IsABCJob', 'Y');
  end loop;

  select jt.ObjectId
  bulk collect into t_PermitJobTypeIds
  from query.o_jobtypes jt
  where jt.Name in (
      'j_ABC_PermitApplication',
      'j_ABC_PermitRenewal')
    and jt.IsPermitJob = 'N';

  for k in 1..t_PermitJobTypeIds.count() loop
    api.pkg_columnupdate.SetValue(t_PermitJobTypeIds(k), 'IsPermitJob', 'Y');
  end loop;

  select jt.ObjectId
  bulk collect into t_LicenceJobTypeIds
  from query.o_jobtypes jt
  where jt.Name in (
      'j_ABC_AmendmentApplication',
      'j_ABC_NewApplication',
      'j_ABC_RenewalApplication')
   and jt.IsLicenseJob = 'N';

  for k in 1..t_LicenceJobTypeIds.count() loop
    api.pkg_columnupdate.SetValue(t_LicenceJobTypeIds(k), 'IsLicenseJob', 'Y');
  end loop;

  -- Set default 180 days on Automatic Cancellation Period for Job, License and Permit Types that
  -- do not already have a cancellation period set.
  select jt.ObjectId
  bulk collect into t_TypeIds
  from query.o_jobtypes jt
  where jt.AutomaticCancellationPeriod is null
  union all
  select lt.ObjectId
  from query.o_abc_licensetype lt
  where lt.AutomaticCancellationPeriod is null
  union all
  select pt.ObjectId
  from query.o_abc_permittype pt
  where pt.AutomaticCancellationPeriod is null;

  for l in 1..t_TypeIds.count() loop
    api.pkg_columnupdate.SetValue(t_TypeIds(l), 'AutomaticCancellationPeriod', 180);
  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction();

  commit;

end;
