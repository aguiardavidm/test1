declare
  t_count                number;
  t_TargetLegalEntityId  number;
  t_BrandsToMove         api.pkg_definition.udt_IdList;
  t_LicensesToMove       api.pkg_definition.udt_IdList;
  t_PermitsToMove        api.pkg_definition.udt_IdList;
  t_LEProductEP          number := api.pkg_configquery.EndPointIdForName('o_ABC_LegalEntity', 'Product');
  t_LELicenseEP          number := api.pkg_configquery.EndPointIdForName('o_ABC_LegalEntity', 'License');
  t_LEPermitEP           number := api.pkg_configquery.EndPointIdForName('o_ABC_LegalEntity', 'Permittee_Permit');
  t_RelId                number;
  t_NoteDefId            number;
  t_NoteId               number;
  t_CountObjs            number := 0;
begin 
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *
             from possedba.Issue33989Consolidate t
            where t.target = 'N'
              and t.processed is null
           order by groupnum) loop
    -- Get the Id for the legal entity we are going to be moving everything to
    select c.legalentityid
      into t_TargetLegalEntityId
      from possedba.Issue33989Consolidate c
     where c.groupnum = i.groupnum
       and c.target = 'Y';
    
    -- Move Brands
    for b in (select r.relationshipid, r.ToObjectId
                from api.relationships r
               where r.FromObjectId = i.legalentityid
                 and r.EndPointId = t_LEProductEP) loop
      if i.legalentityid = 33013370 and api.pkg_columnquery.value(b.toobjectid, 'State') != 'Current' then
        continue;
      end if;
      t_CountObjs := t_CountObjs + 1;
      api.pkg_relationshipupdate.Remove(b.relationshipid);
      t_RelId := api.pkg_relationshipupdate.new(t_LEProductEP, t_TargetLegalEntityId, b.toobjectid);
    end loop;
    
    if i.legalentityid != 33013370 then
      -- Move Licenses
      for l in (select r.relationshipid, r.ToObjectId
                  from api.relationships r
                 where r.FromObjectId = i.legalentityid
                   and r.EndPointId = t_LELicenseEP) loop
        if i.legalentityid = 27766096 and api.pkg_columnquery.value(l.toobjectid, 'State') != 'Active' then
          continue;
        end if;
        t_CountObjs := t_CountObjs + 1;
        api.pkg_relationshipupdate.Remove(l.relationshipid);
        t_RelId := api.pkg_relationshipupdate.new(t_LELicenseEP, t_TargetLegalEntityId, l.toobjectid);
      end loop;
      if i.legalentityid != 27766096 then
        -- Move Permits
        for p in (select r.relationshipid, r.ToObjectId
                    from api.relationships r
                   where r.FromObjectId = i.legalentityid
                     and r.EndPointId = t_LEPermitEP) loop
          t_CountObjs := t_CountObjs + 1;
          api.pkg_relationshipupdate.Remove(p.relationshipid);
          t_RelId := api.pkg_relationshipupdate.new(t_LEPermitEP, t_TargetLegalEntityId, p.toobjectid);
        end loop;
      end if;
    end if;
    -- Add a note for the inactive LE
    select d.NoteDefId
      into t_NoteDefId
      from api.notedefs d
     where d.Tag = 'GEN';
    t_NoteId := api.pkg_noteupdate.New(i.legalentityid, t_NoteDefId, 'N', ('Consolidated with ' || i.legalname || ' as part of ISSUE #6845'));
    api.pkg_columnupdate.SetValue(i.legalentityid, 'Active', 'N');
    update possedba.issue33989consolidate x
       set x.processed = 'Y', x.processeddate = sysdate
     where x.legalentityid = i.legalentityid;
       
  end loop;
  dbms_output.put_line(t_CountObjs);
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
