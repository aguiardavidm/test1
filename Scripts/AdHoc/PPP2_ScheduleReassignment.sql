declare
  t_ScheduleId   number;
  t_Frequency    api.pkg_Definition.udt_Frequency;
begin
  t_Frequency := api.pkg_Definition.gc_Daily;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure('abc.pkg_abc_permitapplication.reassignreviewprocesses', --Name
                                                                 'Reassign Review Process', --Description
                                                                 t_Frequency, --Frequency
                                                                 null,        --IncrementBy
                                                                 null,        --Unit
                                                                 null,        --Arguments
                                                                 'N',        --SkipBacklog
                                                                 'Y',        --DisableOnError
                                                                 'Y',         --Sunday
                                                                 'Y',         --Monday
                                                                 'Y',         --Tuesday
                                                                 'Y',         --Wednesday
                                                                 'Y',         --Thursday
                                                                 'Y',         --Friday
                                                                 'Y',         --Saturday
                                                                 'N',        --MonthEnd
                                                                 --Run at 10 PM nightly
                                                                 (trunc(sysdate) + (22/24)), --StartDate
                                                                 null,        --EndDate
                                                                 null,        --Email
                                                                 null,        --ServerId
                                                                 api.pkg_Definition.gc_normal);       --Priority
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;