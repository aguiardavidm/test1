-- Created on 02/03/2016 by MICHEL.SCHEFFERS 
-- Remove process p_ABC_PRPaymentProcessing from Appeal and PR jobs since they are no longer used in Workflow.
-- This script is only run in NJDev, since none of these processes occur in UAT/Delivery/Prod.
-- Script was created to clean up the workflow on the Appeal and PR jobs in Stage. Needed to remove data first.
declare 
  -- Local variables here
  d1                         number := 0;
  d2                         number := 0;
  d3                         number := 0;
  d4                         number := 0;
  t_ProcessId                api.pkg_definition.udt_IdList;
      
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();

-- Appeal Job
  select pp.ProcessId
    bulk collect into t_ProcessId
    from query.p_abc_prpaymentprocessing pp
    join query.j_abc_appeal a on a.ObjectId = pp.JobId
    ;    
  for i in 1 .. t_ProcessId.count loop
     if api.pkg_columnquery.Value(t_ProcessId(i),'Outcome') is not null then
        update workflow.processes p set
               Outcome = null,
               DateCompleted = null,
               CompletedByLogicalTransId = null,
               CompletedByUserId = null
         where p.ProcessId = t_ProcessId(i);       
     end if;
     api.pkg_ProcessUpdate.Remove(t_ProcessId(i));  
     d1 := d1 + 1;
  end loop;
  dbms_output.put_line('p_ABC_PRPaymentProcessing records removed from Appeal job:         ' || d1);

-- PR Application Job
  select pp.ProcessId
    bulk collect into t_ProcessId
    from query.p_abc_prpaymentprocessing pp
    join query.j_abc_prapplication a on a.ObjectId = pp.JobId
    ;    
  for i in 1 .. t_ProcessId.count loop
     if api.pkg_columnquery.Value(t_ProcessId(i),'Outcome') is not null then
        update workflow.processes p set
               Outcome = null,
               DateCompleted = null,
               CompletedByLogicalTransId = null,
               CompletedByUserId = null
         where p.ProcessId = t_ProcessId(i);       
     end if;
     api.pkg_ProcessUpdate.Remove(t_ProcessId(i));  
     d2 := d2 + 1;
  end loop;
  dbms_output.put_line('p_ABC_PRPaymentProcessing records removed from PR Application job: ' || d2);
-- PR Renewal Job
  select pp.ProcessId
    bulk collect into t_ProcessId
    from query.p_abc_prpaymentprocessing pp
    join query.j_abc_prrenewal a on a.ObjectId = pp.JobId
    ;    
  for i in 1 .. t_ProcessId.count loop
     if api.pkg_columnquery.Value(t_ProcessId(i),'Outcome') is not null then
        update workflow.processes p set
               Outcome = null,
               DateCompleted = null,
               CompletedByLogicalTransId = null,
               CompletedByUserId = null
         where p.ProcessId = t_ProcessId(i);       
     end if;
     api.pkg_ProcessUpdate.Remove(t_ProcessId(i));  
     d3 := d3 + 1;
  end loop;
  dbms_output.put_line('p_ABC_PRPaymentProcessing records removed from PR Renewal job:     ' || d3);

-- PR Amendment Job
  select pp.ProcessId
    bulk collect into t_ProcessId
    from query.p_abc_prpaymentprocessing pp
    join query.j_abc_pramendment a on a.ObjectId = pp.JobId
    ;    
  for i in 1 .. t_ProcessId.count loop
     if api.pkg_columnquery.Value(t_ProcessId(i),'Outcome') is not null then
        update workflow.processes p set
               Outcome = null,
               DateCompleted = null,
               CompletedByLogicalTransId = null,
               CompletedByUserId = null
         where p.ProcessId = t_ProcessId(i);       
     end if;
     api.pkg_ProcessUpdate.Remove(t_ProcessId(i));  
     d4 := d4 + 1;
  end loop;
  dbms_output.put_line('p_ABC_PRPaymentProcessing records removed from PR Amendment job:   ' || d4);
  dbms_output.put_line('Total p_ABC_PRPaymentProcessing records removed from jobs:         ' || (d1+d2+d3+d4));

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;
