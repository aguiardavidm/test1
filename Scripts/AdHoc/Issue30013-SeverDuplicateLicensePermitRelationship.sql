-- Created on 5/22/2017 by MICHEL.SCHEFFERS 
-- Issue 30013 - Sever duplicate License-Permit relationship

-- <<!! *** NEED TO ADD "RETURN" STATEMENT IN ABC.PKG_ABC_PERMIT.VALIDATEPERMITDATA BEFORE RUNNING THIS *** !!>>

drop   table possedba.issue30013_duprels;
create table possedba.issue30013_duprels
(
   GenRelId         number,
   GenPermitId      number,
   GenLicenseId     number,
   PrimRelId        number,
   PrimPermitId     number,
   PrimLicenseId    number
);
/
declare
   p                     number := 0;

begin
   dbms_output.enable(null);
   api.pkg_logicaltransactionupdate.ResetTransaction;
   for lp in (select pgl.RelationshipId GenRelId, pgl.PermitObjectId GenPermitId, pgl.LicenseObjectId GenLicenseId,
                     pl.RelationshipId PrimRelId, pl.PermitObjectId PrimPermitId, pl.LicenseObjectId PrimLicenseId
              from   query.r_permitlicense pl
              join   query.r_abc_permitgenerationlicense pgl on pgl.PermitObjectId = pl.PermitObjectId
                                                            and pgl.LicenseObjectId = pl.LicenseObjectId
             )
   loop
      insert into possedba.issue30013_duprels
             (GenRelId, GenPermitId, GenLicenseId, PrimRelId, PrimPermitId, PrimLicenseId)
      values
             (lp.genrelid, lp.genpermitid, lp.genlicenseid, lp.primrelid, lp.primpermitid, lp.primlicenseid);
      -- Remove Generational Permit License relationship
      dbms_output.put_line ('Removing Generational Permit-License relationship for ' || lp.genpermitid || '-' || lp.genlicenseid);
      api.pkg_relationshipupdate.Remove(lp.genrelid);
      p := p + 1;      
   end loop;
   
   dbms_output.put_line('Permit Generational License relationships removed: ' || p);
   api.pkg_logicaltransactionupdate.EndTransaction;
   commit;

end;
