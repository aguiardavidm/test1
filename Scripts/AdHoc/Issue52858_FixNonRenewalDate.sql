declare
  t_Products               api.pkg_definition.udt_IdList;
  t_NonRenewalNoticeDates  api.pkg_definition.udt_StringList;
  begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  select p.objectid, p.NonRenewalNoticeDate
    bulk collect into t_Products, t_NonRenewalNoticeDates
    from (api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Product', 'State', 'Current')) x
   join query.o_abc_product p on p.ObjectId = x.objectid
   where p.NonRenewalNoticeDate < p.ExpirationDate;
 
  for i in 1 .. t_Products.count  loop
    api.Pkg_Columnupdate.SetValue(t_Products(i), 'NonRenewalNoticeDate', add_months(t_NonRenewalNoticeDates(i), 12));
  end loop;
 api.pkg_logicaltransactionupdate.EndTransaction;
 commit;
end;
