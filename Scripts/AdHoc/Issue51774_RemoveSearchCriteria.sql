/*-----------------------------------------------------------------------------
 * Expected run time: < 2 sec
 * Purpose:
 * Removes all FIL_ Values from all Legal Entities
 *---------------------------------------------------------------------------*/
declare

  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_UpdatedCount                        pls_Integer := 1; -- Indexed by 1 so that the mod doesnt break
  t_CheckedCount                        pls_Integer := 0;
  t_LogicalTransactionId                pls_Integer;
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_TimeEnd                             number(15);
  t_TimeStart                           number(15);

  procedure dbug(
    a_Text varchar2
    ) is
  begin
    dbms_output.put_line(a_Text);
  end dbug;

  function rem_col_LE(
    a_ObjectId number,
    a_ColumnName varchar2
    ) return number is
    t_Count number := 0;
  begin
    if api.pkg_columnquery.Value(a_ObjectId,a_ColumnName) is not null then
       api.pkg_ColumnUpdate.RemoveValue(a_objectid, a_ColumnName);
       t_Count := t_Count + 1;
    end if;
    t_CheckedCount := t_CheckedCount + 1;
    return t_Count;
  end rem_col_LE;

begin
  
  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();

  t_TimeStart := dbms_utility.get_time();

  for i in (
    select l.objectid, c.Name
    from query.o_Abc_Legalentity l
    join possedata.objectcolumndata_t o
      on l.objectid = o.objectid
    join api.columndefs c 
      on c.ColumnDefId = o.columndefid
    where c.name in ('FIL_FromExpirationDate','FIL_FromRegistrationDate','FIL_ProductName','FIL_RegistrationNumber',
      'FIL_State', 'FIL_ToExpirationDate','FIL_ToRegistrationDate','FIL_VehInsigniaNumber',
      'FIL_VehMakeModelYear','FIL_VehOwnedOrLeased','FIL_VehPermitNumber','FIL_VehPermitState',
      'FIL_VehStateOfRegistration','FIL_VehStateRegistration','FIL_VehVIN')
  ) loop
    t_UpdatedCount := t_UpdatedCount + rem_col_LE(i.objectid, i.name);
    if mod(t_UpdatedCount, 500) = 0 then
      dbms_output.put_line(t_UpdatedCount);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

  t_TimeEnd := dbms_utility.get_time();
  dbug('Time Elapsed: ' || (t_TimeEnd - t_TimeStart) / 100);

  dbug(chr(13) || chr(10) || '---AFTER---');

  t_UpdatedCount := t_UpdatedCount-1;
  dbug('Updated Object Count: ' || t_CheckedCount);
  dbug('Updated Object Count: ' || t_UpdatedCount);

end;
/
