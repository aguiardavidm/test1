declare
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  t_IdList udt_IdList;
begin
  api.Pkg_Logicaltransactionupdate.ResetTransaction();

  select g.ProcessId
  bulk collect into t_IDList
  from query.p_abc_generatelicense g
  join conversion.Temp_LicCertJobs t
    on g.jobid = t.JobID
  where not exists (select 1 from query.r_GenerateLicense_Certificate r where g.ProcessId = r.ProcessId)
  --and rownum < 2
  ;

  for i in 1..t_IdList.count loop
    dbms_output.put_Line(t_IdList(i));
    abc.pkg_abc_Workflow.GenerateLicense(t_IdList(i),sysdate);
  end loop;

  api.Pkg_Logicaltransactionupdate.EndTransaction();
  -- commit;
end;