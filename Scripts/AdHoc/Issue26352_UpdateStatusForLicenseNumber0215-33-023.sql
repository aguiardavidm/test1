-- Created on 12/28/2016 by MICHEL.SCHEFFERS 
-- Issue 26352 - License number is at the wrong generation because of job sequence processing (0218-33-023)
declare 
  -- Local variables here
  t_LicenseId_007 number := 0;
  t_LicenseId_008 number := 0;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
-- Find correct object ids for each license
  begin
    select l.ObjectId
    into   t_LicenseId_007
    from   query.o_abc_license l
    where  l.LicenseNumber = '0218-33-023-007'
    and    l.State = 'Active';
  exception
    when others then
      t_LicenseId_007 := 0;
  end;
  begin
    select l.ObjectId
    into   t_LicenseId_008
    from   query.o_abc_license l
    where  l.LicenseNumber = '0218-33-023-008'
    and    l.State = 'Closed';
  exception
    when others then
      t_LicenseId_008 := 0;
  end;
  
-- Activate license 0218-33-023-008
  if t_LicenseId_008 > 0 then
    dbms_output.put_line('Activating license 0218-33-023-008 (' || t_LicenseId_008 || ').');
    api.pkg_columnupdate.SetValue(t_LicenseId_008, 'State', 'Active');
    api.pkg_columnupdate.SetValue(t_LicenseId_008, 'IsLatestVersion', 'Y');
  end if;

-- Close license 0218-33-023-007
  if t_LicenseId_007 > 0 then
    dbms_output.put_line('Closing license 0218-33-023-007 (' || t_LicenseId_007 || ').');
    api.pkg_columnupdate.SetValue(t_LicenseId_007, 'State', 'Closed');
    api.pkg_columnupdate.SetValue(t_LicenseId_007, 'IsLatestVersion', 'N');
  end if;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;