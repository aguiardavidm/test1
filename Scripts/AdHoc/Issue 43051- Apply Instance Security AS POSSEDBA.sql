/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 minutes
 * Purpose: Apply Instance Security to all PR Amendment, PR Application and PR
 *  Renewal jobs, so Modify privilege is given to Users with Licensing Review.
 *  Additionally, the Instance Secured Modify privilege must be given to all
 *  documents currently on all PR jobs--Amendments, Applications, Non-Renewals
 *  and Renewals.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_Count                               pls_integer := 0;
  t_DocObjectIds                        udt_IdList;
  t_InstanceSecureModify                varchar2(4000);
  t_InstanceSecureRead                  varchar2(4000);
  t_JobIds                              udt_IdList;
  t_LogicalTransactionId                number;
  t_PRAmendDocEndpointId                number := api.pkg_ConfigQuery.EndpointIdForName(
      'j_ABC_PRAmendment', 'Document');
  t_PRAppDocEndpointId                  number := api.pkg_ConfigQuery.EndpointIdForName(
      'j_ABC_PRApplication', 'Document');
  t_PRNonRenewalDocEndpointId           number := api.pkg_ConfigQuery.EndpointIdForName(
      'j_ABC_PRNonRenewal', 'Document');
  t_PRRenewalDocEndpointId              number := api.pkg_ConfigQuery.EndpointIdForName(
      'j_ABC_PRRenewal', 'Document');

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  dbug(chr(13) || chr(10) || '---BEFORE---');

  -- Get all PR Amendment, PR Application and PR Renewal jobs
  select j.JobId
  bulk collect into t_JobIds
  from
    api.jobs j
    join api.jobtypes jt
        on j.JobTypeId = jt.JobTypeId
  where jt.Name in ('j_ABC_PRAmendment', 'j_ABC_PRApplication', 'j_ABC_PRRenewal');
  dbug('TOTAL PR Amendment, PR Application and PR Renewal jobs: ' || t_JobIds.count());

  -- Get all documents related to PR Amendment, PR Application, PR NonRenewal and PR
  -- Renewal jobs
  select r.ToObjectId
  bulk collect into t_DocObjectIds
  from
    api.relationships r
    join query.d_ElectronicDocument e
        on r.ToObjectId = e.ObjectId
    join (
        select j.JobId
        from
          api.jobs j
          join api.jobtypes jt
              on j.JobTypeId = jt.JobTypeId
        where jt.Name in (
            'j_ABC_PRAmendment',
            'j_ABC_PRApplication',
            'j_ABC_PRNonRenewal',
            'j_ABC_PRRenewal')
        ) j
        on r.FromObjectId = j.JobId
  where r.EndpointId in (
      t_PRAmendDocEndpointId,
      t_PRAppDocEndpointId,
      t_PRNonRenewalDocEndpointId,
      t_PRRenewalDocEndpointId);
  dbug('TOTAL Documents on PR Amendment, PR Application, PR Non-Renewal and PR Renewal jobs: '
      || t_DocObjectIds.count());

  -- Update Modify Instance Security on Jobs
  for i in 1..t_JobIds.count() loop
    t_InstanceSecureModify := api.pkg_ColumnQuery.Value(t_JobIds(i), 'InstanceSecureModifyGroups');
    t_InstanceSecureRead := api.pkg_ColumnQuery.Value(t_JobIds(i), 'InstanceSecureReadGroups');
    abc.pkg_InstanceSecurity.AdjustByColumn(t_JobIds(i), sysdate, t_InstanceSecureRead,
        t_InstanceSecureModify);
  end loop;

  -- Update Modify Instance Security on Documents
  for i in 1..t_DocObjectIds.count() loop
    t_InstanceSecureModify := api.pkg_ColumnQuery.Value(t_DocObjectIds(i),
        'InstanceSecureModifyGroups');
    t_InstanceSecureRead := api.pkg_ColumnQuery.Value(t_DocObjectIds(i),
        'InstanceSecureReadGroups');
    abc.pkg_InstanceSecurity.AdjustByColumn(t_DocObjectIds(i), sysdate, t_InstanceSecureRead,
        t_InstanceSecureModify);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

  dbug(chr(13) || chr(10) || '---AFTER---');
  select count(1)
  into t_Count
  from
    api.Logicaltransactions l
    join api.objects o
        on o.LogicalTransactionId = l.LogicalTransactionId
    join api.jobs j
        on o.ObjectId = j.JobId
  where l.LogicalTransactionId = t_LogicalTransactionId;
  dbug(t_Count || ' jobs were updated.');

  select count(1)
  into t_Count
  from
    api.Logicaltransactions l
    join api.objects o
        on o.LogicalTransactionId = l.LogicalTransactionId
    join query.d_ElectronicDocument d
        on o.ObjectId = d.ObjectId
  where l.LogicalTransactionId = t_LogicalTransactionId;
    dbug(t_Count || ' documents were updated.');
  commit;
end;
