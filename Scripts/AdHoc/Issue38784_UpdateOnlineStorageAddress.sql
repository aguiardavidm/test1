/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 1 min
 * Purpose: This script updates the OnlineStorageAddress for Vehicle records,
 * using FormattedDisplay from the related Address Online Entry Object.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_OnlineAddresses                     udt_StringList;
  t_VehicleIds                          udt_IdList;
  t_VehicleCount                        number := 0;
  t_LogicalTransactionId                udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction;

  dbug('---BEFORE---');
  dbug('Logical Transaction Id: ' || t_LogicalTransactionId);
  select
    aoe.FormattedDisplay,
    v.ObjectId
  bulk collect into t_OnlineAddresses, t_VehicleIds
  from
    query.o_ABC_Vehicle v
    join api.relationships r
        on v.ObjectId = r.ToObjectId
    join query.o_ABC_AddressOnlineEntry aoe
        on r.FromObjectId = aoe.ObjectId
  where r.EndpointId = api.pkg_ConfigQuery.EndpointIdForName('o_ABC_AddressOnlineEntry',
      'Vehicle');

  dbug('There are ' || t_VehicleIds.count || ' Vehicles related to Address Online Entry Objects.'
      || chr(13));

  for i in 1..t_VehicleIds.count loop
    api.pkg_ColumnUpdate.SetValue(t_VehicleIds(i), 'OnlineStorageAddress', t_OnlineAddresses(i));
  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;

  dbug('---AFTER---');
  select count(1)
  into t_VehicleCount
  from
    api.Logicaltransactions l
    join api.objects o
        on o.LogicalTransactionId = l.LogicalTransactionId
    join query.o_ABC_Vehicle v
        on o.ObjectId = v.ObjectId
  where l.LogicalTransactionId = t_LogicalTransactionId;

  dbug(t_VehicleCount || ' Vehicle Objects have been updated.');
  commit;
end;

