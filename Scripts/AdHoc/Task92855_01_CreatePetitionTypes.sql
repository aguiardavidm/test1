/*******************************************************************************************
 * Description: Creates the initial petition types and petition amendment types
 * Author: Jada Quon
 * Last Modified: Dec 22, 2020
 * Run-time: < 1 s
 * Task Number: 92855
 ********************************************************************************************/
declare
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_definition.udt_StringList;

  t_NameofScript                        varchar2(4000) := 'Task92855_01_CreatePetitionTypes.sql';
  t_Schema                              varchar2(4000) := 'POSSEDBA';
  t_DatabaseName                        varchar2(4000);
  t_PrintDebug                          boolean := True;
  t_RowsProcessed                       pls_integer := 0;
  t_TimeStart                           timestamp := systimestamp;
  t_TimeEnd                             timestamp;
  t_TimeDiff                            float;

  t_PetitionTypeDefId                   udt_Id;
  t_PetitionAmendTypeDefId              udt_Id;
  t_LicenseWarnTypeEndPointId           udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    if t_PrintDebug then
      dbms_output.put_line(a_Text);
    end if;

    -- pipe debug
    pkg_debug.putline(a_Text);
  end dbug;

  procedure CreatePetitionType (
    a_Name                              varchar2,
    a_TermStartMethod                   varchar2,
    a_TermStartNumber                   number,
    a_AvailableOnline                   varchar2,
    a_AdminReview                       varchar2,
    a_MaxTerms                          number,
    a_LicenseWarningTypeDetail          varchar2
  ) is
    t_LicenseWarnTypeId                 udt_Id;
    t_PetitionTypeId                    udt_Id;
    t_RelationshipId                    udt_Id;
  begin
    t_PetitionTypeId := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionType', 'Name', a_Name);
    if t_PetitionTypeId is null then
      t_PetitionTypeId := api.pkg_ObjectUpdate.New(t_PetitionTypeDefId);
      api.pkg_ColumnUpdate.SetValue(t_PetitionTypeId, 'Name', a_Name);
      api.pkg_ColumnUpdate.SetValue(t_PetitionTypeId, 'PetitionTermStartYear', a_TermStartMethod);
      api.pkg_ColumnUpdate.SetValue(t_PetitionTypeId, 'PetitionTermStartYearNumber', a_TermStartNumber);
      api.pkg_ColumnUpdate.SetValue(t_PetitionTypeId, 'IsAvailableOnline', a_AvailableOnline);
      api.pkg_ColumnUpdate.SetValue(t_PetitionTypeId, 'IsAdministrativeReviewRequired', a_AdminReview);
      api.pkg_ColumnUpdate.SetValue(t_PetitionTypeId, 'MaxTermsPerApplication', a_MaxTerms);

      t_LicenseWarnTypeId := api.pkg_SimpleSearch.ObjectByIndex(
          'o_ABC_LicenseWarningType', a_LicenseWarningTypeDetail, 'Y');
      if t_LicenseWarnTypeId is not null then
        t_RelationshipId := api.pkg_RelationshipUpdate.New(
            t_LicenseWarnTypeEndPointId, t_PetitionTypeId, t_LicenseWarnTypeId);
      end if;
      t_RowsProcessed := t_RowsProcessed + 1;
      dbug('Created new Petition Type: ' || a_Name);
    end if;
  end CreatePetitionType;

  procedure CreatePetitionAmendType (
    a_Name                              varchar2,
    a_AvailableOnline                   varchar2,
    a_CorrectTermPeriod                 varchar2,
    a_CancelTerm                        varchar2,
    a_AddTerms                          varchar2
  ) is
    t_PetitionAmendTypeId                    udt_Id;
  begin
    t_PetitionAmendTypeId := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionAmendmentType', 'Name', a_Name);
    if t_PetitionAmendTypeId is null then
      t_PetitionAmendTypeId := api.pkg_ObjectUpdate.New(t_PetitionAmendTypeDefId);
      api.pkg_ColumnUpdate.SetValue(t_PetitionAmendTypeId, 'Name', a_Name);
      api.pkg_ColumnUpdate.SetValue(t_PetitionAmendTypeId, 'IsAvailableOnline', a_AvailableOnline);
      api.pkg_ColumnUpdate.SetValue(t_PetitionAmendTypeId, 'CanCorrectTermPeriod', a_CorrectTermPeriod);
      api.pkg_ColumnUpdate.SetValue(t_PetitionAmendTypeId, 'CanCancelTerm', a_CancelTerm);
      api.pkg_ColumnUpdate.SetValue(t_PetitionAmendTypeId, 'CanAddTerms', a_AddTerms);
      t_RowsProcessed := t_RowsProcessed + 1;
      dbug('Created new Petition Amendment Type: ' || a_Name);
    end if;
  end CreatePetitionAmendType;

begin
  dbug('Starting Script: ' || t_NameOfScript || ' at: ' || to_char(t_TimeStart));

  if user != t_Schema then
    raise_application_error(-20000, 'Script must be run as ' || t_Schema || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  t_PetitionTypeDefId := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_PetitionType');
  t_LicenseWarnTypeEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
      t_PetitionTypeDefId, 'LicenseWarningType');
  CreatePetitionType('12.18', 'Terms After Expiration Date', 0, 'N', 'Y', 2, 'WarningType');
  CreatePetitionType('12.39', 'Terms After Inactivity Start Date', 2, 'N', 'Y', 2, 'WarningType1239');

  t_PetitionAmendTypeDefId := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_PetitionAmendmentType');
  CreatePetitionAmendType('Term Correction', 'Y', 'Y', 'N', 'N');
  CreatePetitionAmendType('Administrative Amendment', 'N', 'Y', 'Y', 'Y');

  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

  t_TimeEnd := systimestamp;
  t_TimeDiff := (EXTRACT (DAY FROM t_TimeEnd-t_TimeStart) * 24 * 60 * 60
                      + EXTRACT (HOUR FROM t_TimeEnd-t_TimeStart) * 60 * 60
                      + EXTRACT (MINUTE FROM t_TimeEnd-t_TimeStart) * 60
                      + EXTRACT (SECOND FROM t_TimeEnd-t_TimeStart));
  dbug('');
  dbug('Rows Processed:  ' || t_RowsProcessed);
  dbug('Ending Script:   ' || t_NameOfScript || ' at: ' || to_char(t_TimeEnd));
  dbug('Run Time:        ' || t_TimeDiff || 's');
end;
/
