-- Created on 9/12/2017 by MICHEL.SCHEFFERS 
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for d in (
            select r1.ElectronicDocId
            from   query.r_NewAppAdditionalInfoDocs r1
            union
            select r2.ElectronicDocId
            from   query.r_AmendmentAdditionalInfoDocs r2
           ) loop
     api.pkg_columnupdate.SetValue(d.electronicdocid, 'DocumentSubmitted', 'Y');
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
  
end;