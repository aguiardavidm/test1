  -- Running Document fix for multiple Receipts.
  -- 19/05/2016 - Issue 8538
declare 
  t_RelationshipId      number (9);
begin

  api.pkg_logicaltransactionupdate.ResetTransaction();

  -- Find Payments with more than 1 Receipt Document
  for r in (select pr.PaymentObjectId
              from query.r_PaymentReceiptReport pr 
             group by pr.PaymentObjectId
            having count(*) > 1) loop                               
     dbms_output.put_line ('Updating documents for Payment ' || r.PaymentObjectId);  
  -- Find correct Document Relationship Id for the Payment
     select min (r.RelationshipId)
     into t_RelationshipId
     from api.relationships r
     join api.objects o on o.ObjectId = r.ToObjectId
     join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
    where r.FromObjectId = r.PaymentObjectId
      and od.Name = 'd_ReceiptDocument';
  -- Use the found RelationshipId as parameter:   
     api.pkg_relationshipupdate.Remove(t_RelationshipId);  
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;