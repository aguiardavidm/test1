-- Created on 01/21/2016 by MICHEL.SCHEFFERS 
-- Update all dup_ExpDatePlusGracePeriod for Permits where it is not equal to the newly calculated ExpDatePlusGracePeriod.
declare 
  -- Local variables here
  d                          number := 0;
  t_PermitIds                api.pkg_definition.udt_IdList;
  t_ExpDatePlusGracePeriod   date;
  t_ColumnDefId              number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'dup_ExpDatePlusGracePeriod');
      
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select p.ObjectId
    bulk collect into t_PermitIds
    from query.o_abc_Permit p
   where p.dup_ExpDatePlusGracePeriod != p.ExpDatePlusGracePeriod
  ;
  for i in 1 .. t_PermitIds.count loop
    t_ExpDatePlusGracePeriod := api.pkg_columnquery.DateValue(t_PermitIds(i), 'ExpDatePlusGracePeriod');
    update possedata.objectcolumndata_t cd
       set cd.AttributeValue = t_ExpDatePlusGracePeriod,
           cd.Searchvalue = lower(substr(t_ExpDatePlusGracePeriod, 1, 20)) --the search field is limited to 20 characters
     where cd.ObjectId = t_PermitIds(i)
       and cd.ColumnDefId = t_ColumnDefId;
     d := d + 1;
    commit;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

  dbms_output.put_line('Permit records updated : ' || d);
end;
