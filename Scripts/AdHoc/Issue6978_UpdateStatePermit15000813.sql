-- Created on 12/08/2015 by MICHEL.SCHEFFERS 
-- Issue6978: Cannot open Permit 15000813
-- Need to set the State since in Production State is null
declare 
  t_PermitId     integer := api.pkg_search.ObjectByColumnValue('o_ABC_Permit','PermitNumber','15000813');
  -- Local variables here
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Expired');
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
