-- Created on 2/28/2017 by MICHEL.SCHEFFERS 
-- Issue 8539 Batch renewal Retail Licenses - Group by municipality

alter table abc.licensebatchjobgrace_gt
rename      column MunicipalityObjectId to CountyObjectId;
alter table abc.licensebatchjobgrace_gt
rename      column Municipality to County;

alter table abc.licensebatchjob_gt
rename      column MunicipalityObjectId to CountyObjectId;
alter table abc.licensebatchjob_gt
rename      column Municipality to County;
