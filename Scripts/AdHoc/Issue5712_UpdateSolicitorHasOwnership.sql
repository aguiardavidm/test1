-- Created on 8/5/2015 by MICHEL.SCHEFFERS 
-- Runtime up to 5 minutes.
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  for p in (select p.ObjectId, p.PermitNumber
              from query.o_abc_permit p
             where p.PermitTypeCode = 'CTW'
               and p.State = 'Active'
               and p.SolicitorHasOwnership is null
               and (p.ExpirationDate > sysdate or
                    p.ExpirationDate is null)
           )
  loop
    dbms_output.put_line('Found Permit ' || p.PermitNumber || '. ObjectId ' || p.ObjectId);
    api.pkg_columnupdate.SetValue (p.objectid, 'SolicitorHasOwnership', 'Yes');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;