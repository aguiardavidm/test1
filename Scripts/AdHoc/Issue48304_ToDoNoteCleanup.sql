PL/SQL Developer Test script 3.0
40
-- Created on 6/26/2019 by JAE.HOLDERBY 
/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: Clean Up To Do Note Data setting the Job Id for the existing To Do 
 * Notes that were previously set using the Process Ids.
 *---------------------------------------------------------------------------*/
declare 
  t_NoteObjectId          api.Pkg_Definition.udt_IdList;
  t_JobId                 api.Pkg_Definition.udt_IdList;  
  t_Counter               integer := 0;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  select 
    tn.ObjectId, 
    p.JobId
  bulk collect into 
    t_NoteObjectId,
    t_JobId
  from query.o_abc_todonote tn
    join api.processes p 
      on p.ProcessId = tn.ProcessId
  where tn.JobId is null;
      
  for i in 1..t_NoteObjectId.count loop
    api.pkg_columnupdate.SetValue(t_NoteObjectId(i), 'JobId', t_JobId(i));
    t_Counter := t_Counter +1;
    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;
  
  dbms_output.put_line(t_Counter);
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  
end;
0
0
