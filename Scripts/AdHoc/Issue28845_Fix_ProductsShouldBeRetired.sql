-- Created on 07/21/2017 by Dennis.OConnor 
-- Modify products state to Historical for products that have a Exp in the past, and are in a Active State

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(NULL);

  for p in (SELECT  p.ObjectId
			FROM query.o_abc_product p
			WHERE p.ExpirationDate < sysdate 
			AND p.IsProductActive = 'Y')
        loop
        api.pkg_columnupdate.SetValue(p.objectid,'State','Historical');
        api.pkg_columnupdate.SetValue(p.objectid,'DirectExpire','Y');
      dbms_output.put_line ('Updating State to Historical for ' || api.pkg_columnquery.Value (p.objectid,'RegistrationNumber') || '-' || api.pkg_columnquery.Value (p.objectid,'Name'));
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;