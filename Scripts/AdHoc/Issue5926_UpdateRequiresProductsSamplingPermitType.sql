-- Created on 12/08/2015 by MICHEL.SCHEFFERS 
-- Issue5926: Close Out Permit: need fields for Size and Vintage
-- Need to reset the RequiresProducts flag on Sampling Permit Type because it has its own grid for Products
declare 
  t_PermitTypeId     integer := api.pkg_search.ObjectByColumnValue('o_ABC_PermitType','Code','SP');
  -- Local variables here
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  api.pkg_columnupdate.SetValue(t_PermitTypeId, 'RequiresProducts', 'N');
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
