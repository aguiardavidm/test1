begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.ObjectId
              from query.p_abc_takeexpirationactions p
             where p.Outcome is null) loop
    api.pkg_processupdate.Complete(i.ObjectId, 'Complete');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;