create table possedba.LogIssue7775Rels as 
select p.ObjectId PermitId, p.PermitNumber,  r.LicenseObjectId SellersLicenseId, p.SellersLicenseNumber, p.LicenseNumber, p.EventLocAddressDescription, r.RelationshipId
              from query.o_abc_permit p
              join query.r_SellersLicense r on r.PermitObjectId = p.ObjectId
             where p.PermitTypeCode = 'SA';
/

declare
  t_EventLocColDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'EventLocAddressDescription');
  t_CurrentTransId   number;

begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_CurrentTransId := api.pkg_logicaltransactionupdate.CurrentTransaction;

  for i in (select *
              from possedba.LogIssue7775Rels) loop
    if i.licensenumber = i.sellerslicensenumber then
    delete from rel.storedrelationships r 
     where r.RelationshipId = i.relationshipid;
    delete from possedata.objects o
     where o.ObjectId = i.relationshipid;

    elsif i.licensenumber is null or i.LicenseNumber != i.SellersLicenseNumber then
      delete from rel.storedrelationships r 
       where r.RelationshipId = i.relationshipid;
      delete from possedata.objects o
       where o.ObjectId = i.relationshipid;

      if i.eventlocaddressdescription is null then
        insert into possedata.objectcolumndata
        (
        logicaltransactionid ,
        objectid ,
        columndefid ,
        attributevalue ,
        searchvalue 
        )
        values
        (
        t_CurrentTransId,
        i.permitid,
        t_EventLocColDefId,
        i.sellerslicensenumber,
        lower(substrb(i.sellerslicensenumber, 1, 20))
        );

      else
        update possedata.objectcolumndata_t
           set logicaltransactionid = t_CurrentTransId,
               attributevalue = i.EventLocAddressDescription || ' - ' || i.sellerslicensenumber, 
               searchvalue = lower(substrb(i.EventLocAddressDescription || ' - ' || i.sellerslicensenumber, 1, 20))
         where objectid = i.permitid
           and columndefid = t_CurrentTransId;

      end if;
    end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;