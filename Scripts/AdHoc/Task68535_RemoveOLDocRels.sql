PL/SQL Developer Test script 3.0
40
-- Created on 6/11/2019 by JAE.HOLDERBY 
/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: remove online documents from the internal relationships 
 *
 * INSTRUCTIONS: execute this script then check the DBMS Output. If you see the 
 * following Error; ORA-20000: Resubmissions are not allowed 
 * after the Submission Deadline Date.
 * You have sucessfully run this script and may commit.
 * If you see any other errors evaluate the errors and send them to your
 * Tech Lead as a log.
 *---------------------------------------------------------------------------*/
declare 
  -- Local variables here
  t_RelationshipIdList                   api.Pkg_Definition.udt_IdList;
  t_Counter                              integer := 0;
begin
 
  select r.RelationshipId
  bulk collect into t_RelationshipIdList
  from query.r_ABC_CPLDocument r
  where exists ( select *
               from query.r_ABC_OnlineCPLeDocument od
               where r.DocumentId = od.DocumentId
               and r.JobId = od.JobId);

  for i in 1.. t_RelationshipIdList.count loop
    begin 
      api.pkg_logicaltransactionupdate.ResetTransaction();
      api.pkg_relationshipupdate.Remove(t_RelationshipIdList(i));
      t_Counter := t_Counter +1;
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
   exception when others then
     dbms_output.put_line(sqlerrm);
     rollback; 
   end;
  end loop;
end;
0
0
