drop table possedba.AG_AvailableAccessGroups;
create table possedba.AG_AvailableAccessGroups
(
   AG_EmailAddress  varchar2(1000),
   AG_Run_Date      date,
   AG_Available     integer,
   AG_Description   varchar2(2000)
);

drop table possedba.AG_Email;
create table possedba.AG_Email
(
   AG_EmailAddress  varchar2(1000)
);

insert into possedba.AG_Email
values ('michel.scheffers@computronix.com');

grant select, insert on possedba.ag_availableaccessgroups to abc;
grant select, insert on possedba.ag_email to abc;
