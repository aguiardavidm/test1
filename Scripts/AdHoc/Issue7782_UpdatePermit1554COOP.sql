rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- Created on 4/19/2016 by MICHEL.SCHEFFERS 
-- Close old permit 1554COOP and set correct expiration date for new 1554COOP
declare 
  -- Local variables here
  i integer;
  
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();

  for p in (select p.objectid, p.effectivedate, p.calculatedexpirationdate, p.expirationdate
              from query.o_abc_permit p
             where p.permitnumber = '1554COOP') loop
     if p.expirationdate is null then
        dbms_output.put_line('Updating expiration date for ' || p.objectid || ' to ' || p.calculatedexpirationdate);
        api.pkg_columnupdate.SetValue (p.objectid, 'ExpirationDate', p.calculatedexpirationdate);
     end if;
     if p.expirationdate < sysdate then
        dbms_output.put_line('Closing permit ' || p.objectid);
        api.pkg_ColumnUpdate.SetValue(p.objectid, 'State', 'Closed');
     end if;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
/
