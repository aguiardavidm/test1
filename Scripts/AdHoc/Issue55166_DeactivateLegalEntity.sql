declare
  t_LegalEntityId    number := 27362942;
  t_ProductIds       api.pkg_Definition.udt_IdList;
  t_RelId            number;
  t_NoteDefId        number;
  t_NoteText         varchar2(4000);
  t_ProdCount        number := 0;
  t_DistCount        number := 0;
  t_NoteId           number;
begin
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20000, 'Must be run as POSSEDBA');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Get the Case Note Def Id
  select nd.NoteDefId
    into t_NoteDefId
    from api.notedefs nd
   where nd.Tag = 'GEN';
  
  -- Get the Product Ids for our LE
  select pr.ProductObjectId
    bulk collect into t_ProductIds
    from query.r_abc_productregistrant pr
   where pr.RegistrantObjectId = t_LegalEntityId
     and api.pkg_ColumnQuery.value(pr.ProductObjectId, 'State') = 'Current';
  for i in 1..t_ProductIds.count() loop
    -- Make Distributor Licenses Historic
    for d in (
      select ld.LicenseId DistributorId
        from query.r_abcproduct_distributor ld
       where ld.ProductId = t_ProductIds(i)
       union all
      select td.PermitId DistributorId
        from query.r_abc_producttapdistributor td
       where td.ProductId = t_ProductIds(i)
    ) loop
      -- This mimics the actions on the "Make Historical" button on the distributors grid
      api.pkg_ColumnUpdate.SetValue(d.DistributorId, 'ProductObjectId', t_ProductIds(i));
      t_DistCount := t_DistCount+1;
    end loop;
    -- Make the actual product Historical
    t_ProdCount := t_ProdCount+1;
    api.pkg_ColumnUpdate.SetValue(t_ProductIds(i), 'State', 'Historical');
    --Create Case Note
    t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) || 'System Made Brand historic for issue 55166';
    t_NoteId := api.pkg_NoteUpdate.New(t_ProductIds(i), t_NoteDefId, 'N', t_NoteText);
  end loop;
  
  --Create Case Note
  t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) || 'System Deactivated Legal Entity and made Brands historic for issue 55166';
  t_NoteId := api.pkg_NoteUpdate.New(t_LegalEntityId, t_NoteDefId, 'N', t_NoteText);
  -- Deactivate the LE
  api.pkg_ColumnUpdate.SetValue(t_LegalEntityId, 'Active', 'N');
  
  dbms_output.put_line('Deactivated Legal Entity: ' || t_LegalEntityId || '. ' || api.pkg_ColumnQuery.value(t_LegalEntityId, 'dup_FormattedName'));
  dbms_output.put_line('Products made Historical: ' || t_ProdCount);
  dbms_output.put_line('Distributors Made Historical: ' || t_DistCount);
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
--  rollback;
end;
