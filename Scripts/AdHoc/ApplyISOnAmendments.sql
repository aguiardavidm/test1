begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all Amendment jobs
  for i in (select ObjectId
              from query.j_ABC_AmendmentApplication
            ) loop
    -- Apply Instance Security for each job
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.ObjectId, sysdate);
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;
