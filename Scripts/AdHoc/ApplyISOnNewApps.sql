begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all New Application jobs
  for i in (select ObjectId
              from query.j_ABC_NewApplication) loop
    -- Apply Instance Security for each job
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.ObjectId, sysdate);
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;
