-- Created on 8/9/2017 by MICHEL.SCHEFFERS 
-- Issue 31491 - New Jobs More Than 6 Months Old

drop   table possedba.issue31491_newjobs;
create table possedba.issue31491_newjobs
(
   JobId           integer,
   JobNumber       varchar2(40),
   JobName         varchar2(400),
   CreatedBy       varchar2(400),
   CreatedOn       date,
   EnteredPublic   varchar2(01),
   FeeDescription  varchar2(400),
   FeeAmount       varchar2(40),
   FeePaid         varchar2(40),
   Balance         varchar2(40),
   PaidOn          date,
   CancelDate	   date
);
/

declare 
  -- Local variables here
  i integer;
  t_Balance number;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for j in (select j1.JobId, j1.CreatedDate, j1.ExternalFileNum
            from   api.jobs j1
            where  j1.JobStatus = 'NEW'
            and    api.pkg_columnquery.Value(j1.JobId, 'ObjectDefDescription') = 'Miscellaneous Revenue Transaction'
           ) loop
     if trunc (j.CreatedDate) < (to_date(sysdate - 180))
        and api.pkg_columnquery.Value(j.JobId, 'ObjectDefDescription') not in ('Registration', 'eCom', 'Payment Adjustment', 'Daily Deposit', 'Referral User Registration', 'Proposed Payment') 
          then
        for f in (select f1.Description, f1.Amount, f1.PaidAmount, lt.CreatedDate--, api.pkg_columnquery.DateValue(p1.PaymentId, 'PaymentDate'), pt.PaymentId
                  from   api.jobs j1
                  full   join api.fees f1 on f1.JobId = j.jobid
                  full   join api.feetransactions ft on ft.FeeId = f1.FeeId
                  full   join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
                  full   join api.payments p1 on p1.PaymentId = pt.PaymentId
                  full   join api.logicaltransactions lt on lt.LogicalTransactionId = p1.LogicalTransactionId
                  where  j1.JobId = j.jobid
                 ) loop
           t_Balance := null;
           if f.amount is not null then
              t_Balance := f.amount + f.paidamount;
           end if;
           insert into possedba.issue31491_newjobs
           values (j.jobid,
                   j.ExternalFileNum, 
                   api.pkg_columnquery.Value(j.JobId, 'ObjectDefDescription'),
                   api.pkg_columnquery.Value(j.JobId, 'CreatedByUserName'),
                   trunc(j.CreatedDate),
                   nvl(api.pkg_columnquery.Value(j.JobId, 'EnteredOnline'), 'N'),
                   f.Description,
                   to_char(f.Amount,'99999999.99'),
                   to_char((f.PaidAmount * -1),'99999999.99'),
                   to_char(t_Balance,'99999999.99'),
                   trunc (f.createddate)
                  );
        end loop;
     end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
