  -- Update path for Municipal Email Header image in System Settings. Issue 6818
  -- 11/12/2015
declare 
  t_ExternalWebsiteBaseURL varchar2(100);
  t_SSObjectId             number (9);
begin

  api.pkg_logicaltransactionupdate.ResetTransaction();

  begin
    select ss.ObjectId, substr(ss.ExternalWebsiteBaseURL,1,(INSTR(ss.ExternalWebsiteBaseURL,'/', -1, 1)))
      into t_SSObjectId, t_ExternalWebsiteBaseURL
      from query.o_systemsettings ss;
  exception
      when no_data_found then
        t_ExternalWebsiteBaseURL := '';
  end;
  -- Find Payments with more than 1 Receipt Document
  t_ExternalWebsiteBaseURL := t_ExternalWebsiteBaseURL || 'images/EmailHeader-Muni.jpg';
  dbms_output.put_line ('Updating Email Header path to ' || t_ExternalWebsiteBaseURL);  
  api.pkg_columnupdate.SetValue(t_SSObjectId,'MuniNotiEmailHeader',t_ExternalWebsiteBaseURL);

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;
