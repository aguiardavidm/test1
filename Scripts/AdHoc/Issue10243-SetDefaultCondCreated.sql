-- Created on 3/10/2017 by MICHEL.SCHEFFERS 
-- Issue 10243 - Temporary Permit Conditions (TAP) & other permits
-- Set DefaultCondCreated on all existing Permits to Y so they don't inadvertently get conditions copied when a change is made to the Permit
-- *** NOTE: May need to add "Return null" in the beginning of abc.pkg_ABC_Permit.RaisePermitError to avoid errors. ***

declare 
  -- Local variables here
  d                          number := 0;
  t_Dummy                    integer := 0;
  t_PermitIds                api.pkg_definition.udt_IdList;
  t_ExpDatePlusGracePeriod   date;
  t_ColumnDefId              number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'DefaultCondCreated');
      
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select p.ObjectId
  bulk   collect into t_PermitIds
  from   query.o_abc_Permit p;
  
  for i in 1 .. t_PermitIds.count loop    
     begin
        select 1
        into   t_Dummy
        from   possedata.Objectcolumndata ocd
        where  ocd.columndefid = t_columndefid
        and    ocd.ObjectId = t_PermitIds(i);
        update possedata.objectcolumndata_t cd
           set cd.AttributeValue = 'Y',
               cd.Searchvalue = 'y'
         where cd.ObjectId = t_PermitIds(i)
           and cd.ColumnDefId = t_ColumnDefId;
     exception
        when no_data_found then
           insert into possedata.objectcolumndata_t cd
                  (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue, cd.searchvalue)
                  Values (api.pkg_logicaltransactionupdate.CurrentTransaction, t_PermitIds(i), t_columndefid,'Y', 'y');
     end;
      d := d + 1;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
  dbms_output.put_line(d || ' Permit records updated');
end;
