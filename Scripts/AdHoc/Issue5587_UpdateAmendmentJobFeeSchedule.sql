/* Author: Michel Scheffers
   Description: Create License Type 24 entry for ABC Fee Schedule
   Issue 
   Execution time: < 5 minutes
*/
declare
  subtype udt_Id is api.pkg_definition.udt_Id;
-- Object IDs  
  t_FSObjId      udt_Id; -- FeeSchedule Object Id
  t_FDObjId      udt_Id; -- FeeDefinition Object Id
  t_GLObjId      udt_Id; -- GLAccount Object Id
  t_DSObjIdAppl  udt_id; -- Application Job Object Id
  t_DSObjIdRnw   udt_Id; -- Renewal Job Object Id
  t_DSObjIdAm    udt_Id; -- Amend Job Object Id
  t_ODObjIdAppl  udt_id; -- Application Job ObjectDef Object Id
  t_ODObjIdRnw   udt_Id; -- Renewal Job ObjectDef Object Id
  t_ODObjIdAm    udt_Id; -- Amend Job ObjectDef Object Id
-- Relationship/Endpoint IDs
  t_FSEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeSchedule', 'FeeDefinition');
  t_FDEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeSchedule', 'DataSource');
  t_DSEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeDefinition', 'DataSource');
  t_EDEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'DataSource');  
  t_EFEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'FeeSchedule');  
  t_RelationshipId  udt_id;
  t_FeeElementId    udt_id;
  t_FeeScheduleId   udt_id;
  t_DataSourceId    udt_id;
  t_FeeDefinitionId udt_id;
-- Counters
  t_FDRecords number := 0;
  t_FERecords number := 0;

-- PROCEDURES --
procedure dbug (
    a_Text    varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
end;

procedure CreateFD(a_Amount                     varchar2,
                   a_Condition                  varchar2,
                   a_EffectiveDateColumnName    varchar2,
                   a_FeeDescription             varchar2,
                   a_Description                varchar2,
                   a_ODObjId                    udt_id,
                   a_DSObjId                    udt_id,
                   a_FSObjId                    udt_id,
                   a_GLObjId                    udt_id) is
  t_RelationshipId udt_Id; 
  
begin
  begin
    select fd.objectid
      into t_FDObjId
      from query.o_FeeDefinition fd
      join query.r_Feedef_Feeschedule fdfs on fdfs.FeeDefinitionObjectId = fd.objectid
     where fd.FeeDescription  = a_FeeDescription
       and fdfs.FeeScheduleId = a_FSObjId;
     exception
       when no_data_found then
         t_FDObjId := null;
  end;
  if t_FDObjId is null then
  --Create new object of FeeDefinition
     feescheduleplus.pkg_PosseExternalRegistration.RegisterFeeDefinition (t_FeeDefinitionId,
                                                                          a_Description,
                                                                          a_Condition,
                                                                          a_Amount,
                                                                          a_EffectiveDateColumnName,
                                                                          a_FeeDescription,
                                                                          sysdate,
                                                                          'POSSEDBA',
                                                                          sysdate,
                                                                          'POSSEDBA',
                                                                          api.pkg_columnquery.NumericValue(a_FSObjId,'FeeScheduleId'),
                                                                          api.pkg_columnquery.NumericValue(a_DSObjId,'DataSourceId'),
                                                                          null,
                                                                          '={GLAccountCode}',
                                                                          'Default',
                                                                          null
                                                                          );
     t_FDObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeDefinition',t_FeeDefinitionId);
     dbug ('Added FeeDefinition ' || t_FDObjId || ' - ' || a_FeeDescription);
     t_FDRecords := t_FDRecords + 1;
  --Create relationship from FeeSchedule to FeeDefinition
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FSEndpointId, a_FSObjId, t_FDObjId);
  --Create relationship from FeeDefinition to DataSource
     t_RelationshipId := api.pkg_relationshipupdate.New(t_DSEndpointId, t_FDObjId, a_DSObjId);
  end if;
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Amount', a_Amount);
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Condition', a_Condition);
  api.pkg_columnupdate.SetValue(t_FDObjId, 'EffectiveDateColumnName', a_EffectiveDateColumnName); 
  api.pkg_columnupdate.SetValue(t_FDObjId, 'FeeDescription', a_FeeDescription); 
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Description', a_Description); 

end;

-- MAIN PROCESS --
begin
  dbug('Start Creating Fee script');
  api.pkg_logicaltransactionupdate.ResetTransaction();
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  select objectdefid 
  into t_ODObjIdAppl
  from api.objectdefs 
  where Name = 'j_ABC_NewApplication';

  select objectdefid 
  into t_ODObjIdRnw
  from api.objectdefs 
  where Name = 'j_ABC_RenewalApplication';

  select objectdefid 
  into t_ODObjIdAm
  from api.objectdefs 
  where Name = 'j_ABC_AmendmentApplication';

  t_GLObjId      := api.pkg_search.ObjectByColumnValue('o_glaccount', 'Code', '24');
  if t_GLObjId is null then
  --Error, GL Account should be present
      api.pkg_Errors.RaiseError(-20000, 'G/L Account - 24 should be present before you can continue');
  end if;
  
  t_FSObjId := api.pkg_search.ObjectByColumnValue('o_FeeSchedule', 'Description', 'ABC Fee Schedule');
  dbug(t_FSObjId);
  if t_FSObjId is null then
  --Create a new object of FeeSchedule
     feescheduleplus.pkg_posseexternalregistration.RegisterFeeSchedule(t_FeeScheduleId,
                                                                       'ABC Fee Schedule',
                                                                       to_date ('20110414','yyyymmdd'),
                                                                       to_date ('39990415','yyyymmdd'),
                                                                       sysdate,
                                                                       'POSSEDBA',
                                                                       sysdate,
                                                                       'POSSEDBA'
                                                                       );
     t_FSObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeSchedule',t_FeeScheduleId);
     dbug ('Added FeeSchedule ' || t_FSObjId || ' - ABC Fee Schedule');
  end if;

  -- Checking DataSource  
  --Application
  begin
     select ds.objectid 
       into t_DSObjIdAppl
       from query.r_DataSource_FeeSchedule dsfs 
       join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
      where dsfs.FeeScheduleId = t_FSObjId
        and ds.PosseViewName = 'j_ABC_NewApplication';
     exception
       when no_data_found then
         t_DSObjIdAppl := null;
  end;  
  if t_DSObjIdAppl is null then  
     feescheduleplus.pkg_posseexternalregistration.RegisterDataSource (
                                                                       t_DataSourceId,
                                                                       'New Application Job',
                                                                       'j_ABC_NewApplication',
                                                                       api.pkg_columnquery.NumericValue(t_FSObjId,'FeeScheduleId')
                                                                       );
     t_DSObjIdAppl := api.pkg_objectupdate.RegisterExternalObject('o_DataSource',t_DataSourceId);
     dbug ('Added DataSource ' || t_DSObjIdAppl || ' - Application');
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FDEndpointId, t_FSObjId, t_DSObjIdAppl);
  end if;
  
  --Renewal
  begin
     select ds.objectid 
       into t_DSObjIdRnw
       from query.r_DataSource_FeeSchedule dsfs 
       join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
      where dsfs.FeeScheduleId = t_FSObjId
        and ds.PosseViewName = 'j_ABC_RenewalApplication';
     exception
       when no_data_found then
         t_DSObjIdRnw := null;
  end;  
  if t_DSObjIdRnw is null then  
     feescheduleplus.pkg_posseexternalregistration.RegisterDataSource (
                                                                       t_DataSourceId,
                                                                       'Renewal Job',
                                                                       'j_ABC_RenewalApplication',
                                                                       api.pkg_columnquery.NumericValue(t_FSObjId,'FeeScheduleId')
                                                                       );
     t_DSObjIdRnw := api.pkg_objectupdate.RegisterExternalObject('o_DataSource',t_DataSourceId);
     dbug ('Added DataSource ' || t_DSObjIdRnw || ' - Renewal');
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FDEndpointId, t_FSObjId, t_DSObjIdRnw);
  end if;

  --Amendment
  begin
     select ds.objectid 
       into t_DSObjIdAm
       from query.r_DataSource_FeeSchedule dsfs 
       join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
      where dsfs.FeeScheduleId = t_FSObjId
        and ds.PosseViewName = 'j_ABC_AmendmentApplication';
     exception
       when no_data_found then
         t_DSObjIdAm := null;
  end;  
  if t_DSObjIdAm is null then  
     feescheduleplus.pkg_posseexternalregistration.RegisterDataSource (
                                                                       t_DataSourceId,
                                                                       'Amendment Job',
                                                                       'j_ABC_AmendmentApplication',
                                                                       api.pkg_columnquery.NumericValue(t_FSObjId,'FeeScheduleId')
                                                                       );
     t_DSObjIdAm := api.pkg_objectupdate.RegisterExternalObject('o_DataSource',t_DataSourceId);
     dbug ('Added DataSource ' || t_DSObjIdAm || ' - Amendment');
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FDEndpointId, t_FSObjId, t_DSObjIdAm);
  end if;
     
  -- Additional Warehouse Jobs
  CreateFD ('={AdditionalWarehouseSalesroomFe} * 0.25',
            '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''24'' and {AdditionalWarehouseSalesroomFe} > 0',
            'CreatedDate',
            'Additional Warehouse Application Fee ',
            'Additional Warehouse Application Fee ',
            t_ODObjIdAppl,
            t_DSObjIdAppl,
            t_FSObjId,
            t_GLObjId);
  CreateFD ('={AdditionalWarehouseSalesroomFe} * 0.25',
            '={LicenseTypeCode} = ''24'' and {AdditionalWarehouseSalesroomFe} > 0',
            'CreatedDate',
            'Additional Warehouse Renewal Fee',
            'Additional Warehouse Renewal Fee',
            t_ODObjIdRnw,
            t_DSObjIdRnw,
            t_FSObjId,
            t_GLObjId);  
  CreateFD ('= case {AmendmentTypeCode}
             when ''PersonToPerson'' then ({AdditionalWarehouseSalesroomFe} * 0.25) * 0.10
             when ''PlaceToPlace''   then ({AdditionalWarehouseSalesroomFe} * 0.25) * 0.10
             when ''PersonAndPlace'' then ({AdditionalWarehouseSalesroomFe} * 0.25) * 0.20
			       end case',
            '={LicenseTypeCode} = ''24'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'') and {AdditionalWarehouseSalesroomFe} > 0',
            'CreatedDate',
            'Additional Warehouse Amendment Fee',
            'Additional Warehouse Amendment Fee',
            t_ODObjIdAm,
            t_DSObjIdAm,
            t_FSObjId,
            t_GLObjId);

  dbug ('FeeElement records added:    ' || t_FERecords);              
  dbug ('FeeDefinition records added: ' || t_FDRecords);              

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  dbug('End Creating Fee script');
end;