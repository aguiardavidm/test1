-- Created on 4/23/2020 by BENJAMIN
declare 
  -- Local variables here
  t_Count                  integer := 0;
  t_PermitIds              api.Pkg_Definition.udt_IdList;
  t_ConsolidateToPermitId  number;
  t_PermitId               number;
  t_RelId                  number;
  t_FeeCount           number := 0;
  t_NoteDefId              number;
  t_NoteId                 number;
  t_Transactionid          number;
begin
  select nd.NoteDefId
    into t_NoteDefId
    from api.notedefs nd
   where nd.Tag ='GEN';
  dbms_output.put_line('JobId, JobNumber, FeeId, TransactionId');
  -- Loop through all permits valid for consolidation
  for i in (
    select
      (f.amount + f.PaidAmount + f.adjustedamount) TotalOwing,
      api.pkg_columnquery.value(r.ToObjectId, 'dup_FormattedName') dup_FormattedName,
      f.Description,
      j.jobid,
      j.externalfilenum JobNumber,
      f.TransactionDate,
      f.feeid
    from
      api.relationships r
      join api.fees f
          on r.ToObjectId = f.ResponsibleObjectId
      join api.jobs j
          on f.JobId = j.JobId
      join api.jobstatuses js
          on j.StatusId = js.StatusId
         and j.JobTypeId = js.JobTypeId
    where r.FromObjectId = 45405693 --{ObjectId}--ghagen@allied
--    where r.toobjectid = 27747759
      and r.EndpointId = api.pkg_ConfigQuery.EndpointIdForName('u_Users', 'LegalEntity')
      and js.StatusType in ('I', 'O', 'C')
      and api.pkg_columnquery.value(f.JobId, 'ObjectDefName') in (
          'j_ABC_Accusation',
          'j_ABC_AmendmentApplication',
          'j_ABC_NewApplication',
          'j_ABC_Reinstatement',
          'j_ABC_RenewalApplication',
          'j_ABC_PRApplication',
          'j_ABC_PRRenewal',
          'j_ABC_PRAmendment',
          'j_ABC_PermitApplication',
          'j_ABC_PermitRenewal',
          'j_ABC_Petition',
          'j_ABC_MiscellaneousRevenue')
    and (f.amount + f.PaidAmount + f.adjustedamount) > 0
    and f.Description = 'Transit Insignia Application Fee'
    order by f.TransactionDate asc
    ) loop
    begin
      t_FeeCount :=0;
      api.pkg_logicaltransactionupdate.ResetTransaction;
      
      --Adjust the Fees to 0
      t_Transactionid := api.pkg_feeupdate.Adjust(i.feeid, i.totalowing * -1, 'System Error Adjustment', sysdate);
      
      -- Add Note to Job for system adjusted fees
      t_NoteId := api.pkg_noteupdate.New(i.jobid, t_NoteDefId, 'Y', to_char(sysdate, 'Mon dd, yyyy HH:MI:SS PM') || '
Issue #54931 - COVID-19 Support
System Fee Adjustment due to Issue #49650');
      
      api.pkg_logicaltransactionupdate.EndTransaction;
      rollback;--commit;
    exception when others then
      --rollback;
      dbms_output.put_line(i.jobid || ', Job Number: ' || i.jobnumber || ', Feeid: ' || i.feeid || ', TransactionId: ' || t_Transactionid || ', Failed: ' || sqlerrm);
      continue;
    end;
    dbms_output.put_line(i.jobid || ', ' || i.jobnumber || ', ' || i.feeid || ', ' || t_Transactionid );
    
    t_Count := t_Count+1;
  end loop;

end;