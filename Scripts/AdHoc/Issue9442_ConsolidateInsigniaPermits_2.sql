-- Created on 7/27/2018 by ADMINISTRATOR 
declare 
  -- Local variables here
    t_SearchResults                     api.pkg_definition.udt_SearchResults;
    t_Objects                           api.pkg_definition.udt_IdList;
    t_AssociatedPermit                  integer;
    t_NewAssociatedPermit               integer;
    t_Reason                            varchar2(4000);
    t_ObjectId                          integer;
    t_CancelEndpointId                  integer :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
            'PermitCancellation');
    t_ObjectDefId                       integer :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_PermitCancellation');
    t_RelId                             integer;
    t_PermitNumber                      varchar2(50);
    t_PermitTypeId                      integer;
    t_ActivePermits                     api.pkg_definition.udt_idList;
    t_ConsolidatedPermitId              integer;
    t_ToEndPointId                      integer;
    t_EndPointId                        integer;
    t_VehicleId                         integer;
    t_VehicleDefId                      integer :=
        api.pkg_configquery.ObjectDefIdForName ('o_ABC_Vehicle');
    t_Cancelled                         integer := 0;
    t_Consolidated                      integer := 0;
    t_ActiveRelated                     integer := 0;
    t_ExpirationDate                    date;
    t_RenewalCount                      integer;
    t_RenewEndpointId                   integer :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitRenewal');
    t_ConsolidatePermits                varchar2(01);
    t_testExpDate                       date;
  begin

    if user != 'POSSEDBA' then
      api.pkg_errors.raiseerror(-20000, 'Execute script as POSSEDBA');
    end if;

    dbms_output.enable(null);
    api.pkg_logicaltransactionupdate.ResetTransaction;
    t_SearchResults := api.pkg_Search.NewSearchResults();
    if not api.pkg_Search.IntersectByColumnValue('o_abc_permit', 'RequiresVehicle', 'Y', t_SearchResults) then
       return;
    end if;
    if not api.pkg_Search.IntersectByColumnValue('o_abc_permit', 'IsActive', 'Y', t_SearchResults) then
       return;
    end if;
    t_Objects := api.pkg_Search.SearchResultsToList(t_SearchResults);
    dbms_output.put_line('Permits ' || t_Objects.Count);
        
    for p in 1..t_Objects.Count loop
      t_TestExpDate := api.pkg_columnquery.DateValue(t_Objects(p),'ExpirationDate');
      if trunc(api.pkg_columnquery.DateValue(t_Objects(p),'ExpirationDate'),'YYYY') > trunc(sysdate, 'YYYY') then
      continue;
      end if;
      -- Associated Permit
      if api.pkg_columnquery.Value(t_Objects(p), 'AssociatedPermitObjectId') is not null then
        t_AssociatedPermit := api.pkg_columnquery.Value(t_Objects(p), 'AssociatedPermitObjectId');
        dbms_output.put_line('Finding Associated Permit ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'PermitNumber'));
        if api.pkg_columnquery.Value(t_AssociatedPermit, 'IsActive') = 'N' then
          dbms_output.put_line('Associated Permit ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'PermitNumber') ||
              ' for ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber') || ' is not Active');
          t_NewAssociatedPermit := abc.Pkg_Abc_Workflow.GetLatestPermit(t_Objects(p), t_AssociatedPermit);
          if t_NewAssociatedPermit = 0 then
            -- Cancelling Insignia Permit
            t_Cancelled := t_Cancelled + 1;
            dbms_output.put_line('    No active associated Permit found. Cancelling Permit ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
            t_Reason := 'Permit cancelled as part of the 9442 Consolidation for not having an Active Associated License or Permit: ' || 
                api.pkg_columnquery.Value(t_AssociatedPermit, 'PermitNumber');
            api.pkg_columnupdate.SetValue(t_Objects(p), 'State', 'Cancelled');
            api.pkg_columnupdate.SetValue(t_Objects(p), 'Edit', 'N');
            t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
            api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation',
                sysdate);
            api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
            t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, t_Objects(p),
                t_ObjectId, sysdate);
      continue;
          else
            t_ActiveRelated := t_ActiveRelated + 1;
            dbms_output.put_line('    Found active version of ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'PermitNumber') ||
                ' to associate with ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
            select pl.RelationshipId
            into t_RelId
            from query.r_ABC_AssociatedPermit pl
            where pl.PrimaryPermitObjectId = t_Objects(p)
            and pl.AsscPermitObjectId = t_AssociatedPermit;
            api.pkg_relationshipupdate.Remove(t_RelId);
            t_EndPointId := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit',
                'AssociatedPermit');
            t_RelId := api.pkg_relationshipupdate.New (t_EndPointId, t_Objects(p),
                t_NewAssociatedPermit);
          end if;
        end if;
        -- Check for Insignia Permits to consolidate
        t_PermitNumber := api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
        t_PermitTypeId := api.pkg_columnquery.value(t_Objects(p), 'PermitTypeObjectId');
        select r.PrimaryPermitObjectId
        bulk collect into t_ActivePermits
        from query.r_ABC_AssociatedPermit r
        where r.AsscPermitObjectId = t_AssociatedPermit
          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'State') = 'Active'
          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitNumber')
              != t_PermitNumber
          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitTypeObjectId') =
              t_PermitTypeId;
        t_ConsolidatePermits := 'N';

        if t_ActivePermits.count > 0 then
          t_ExpirationDate := api.pkg_columnquery.DateValue(t_Objects(p),
              'CalculatedExpirationDate');
          for c in 1..t_ActivePermits.count loop
            -- Check if the Other Active Permit has an expiration date greater than the new
            -- expiration date. If not, then don't consolidate.
            select (
              (select count(*) 
              from api.relationships r 
              join api.jobs j 
                on j.JobId = r.ToObjectId 
              where r.FromObjectId = t_ActivePermits(c)
                and r.EndPointId = t_RenewEndpointId
                and j.JobStatus in ('NEW','REVIEW'))
              + 
              (select count(*) 
              from api.relationships r 
              join api.jobs j 
                on j.JobId = r.ToObjectId 
              where r.FromObjectId = t_Objects(p)
                and r.EndPointId = t_RenewEndpointId
                and j.JobStatus in ('NEW','REVIEW'))
              )
            into t_RenewalCount
            from dual;
            if api.pkg_columnquery.Datevalue(t_ActivePermits(c), 'ExpirationDate') = 
                  t_ExpirationDate and t_RenewalCount = 0 then
              t_ConsolidatePermits := 'Y';
              t_ConsolidatedPermitId := t_ActivePermits(c);
              exit;
            end if;
          end loop;
        end if;  
        
        if t_ConsolidatePermits = 'Y' then
          t_ConsolidatedPermitId := t_ActivePermits(1);
          --Copy Vehicles to Active Permit to Consolidate the Vehicles
          t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
              'Vehicle');
          dbms_output.put_line('    Copying vehicle(s) from ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber')
              || ' to ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber'));
          for i in (select *
                    from query.r_abc_permitvehicle rpv
                    where rpv.PermitObjectId = t_Objects(p)) loop
                    null;
            t_VehicleId := api.pkg_objectupdate.New(t_VehicleDefId);
            t_RelId := api.Pkg_Relationshipupdate.New(t_ToEndPointId,
                t_ConsolidatedPermitId, t_VehicleId);
            api.pkg_columnupdate.setvalue(t_VehicleId, 'PreviousInsigniaNumber',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'PreviousInsigniaNumber'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'InsigniaNumber',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'InsigniaNumber'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'MakeModelYear',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'makemodelyear'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'OwnedOrLeasedLimousine',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'ownedorleasedlimousine'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'VIN',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'vin'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateregistration',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateregistration'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateofregistration',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateofregistration')); 
          end loop;
          -- Set the permit as Cancelled
          t_Consolidated := t_Consolidated + 1;
          api.pkg_columnupdate.SetValue(t_Objects(p), 'State', 'Cancelled');
          api.pkg_columnupdate.SetValue(t_Objects(p), 'Edit', 'N');
          api.pkg_columnupdate.setValue(t_Objects(p), 'SystemCancelled', 'Y');
          t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
          t_Reason := 'Consolidated with Permit ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber');
          api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation',
              sysdate);
          api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
          t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, t_Objects(p),
              t_ObjectId, sysdate);
          dbms_output.put_line('    ' || t_Reason);
        end if;
      end if;

      -- Associated TAP
      if api.pkg_columnquery.Value(t_Objects(p), 'TAPPermitObjectId') is not null then
        t_AssociatedPermit := api.pkg_columnquery.Value(t_Objects(p), 'TAPPermitObjectId');
        dbms_output.put_line('Finding Associated TAP Permit ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'PermitNumber'));
        if api.pkg_columnquery.Value(t_AssociatedPermit, 'IsActive') = 'N' then
          dbms_output.put_line('Associated TAP Permit ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'PermitNumber') ||
              ' for ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber') || ' is not Active');
          t_NewAssociatedPermit := abc.Pkg_Abc_Workflow.GetLatestPermit(t_Objects(p), t_AssociatedPermit);
          if t_NewAssociatedPermit = 0 then
            -- Cancelling Insignia Permit
            t_Cancelled := t_Cancelled + 1;
            dbms_output.put_line('    No active associated TAP Permit found. Cancelling Permit ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
            t_Reason := 'Permit cancelled as part of the 9442 Consolidation for not having an Active Associated License or Permit: ' || 
                api.pkg_columnquery.Value(t_AssociatedPermit, 'PermitNumber');
            api.pkg_columnupdate.SetValue(t_Objects(p), 'State', 'Cancelled');
            api.pkg_columnupdate.SetValue(t_Objects(p), 'Edit', 'N');
            t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
            api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation',
                sysdate);
            api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
            t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, t_Objects(p),
                t_ObjectId, sysdate);
      continue;
          else
            t_ActiveRelated := t_ActiveRelated + 1;
            dbms_output.put_line('    Found active version of ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'PermitNumber') ||
                ' to associate with ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
            select pl.RelationshipId
            into t_RelId
            from query.r_ABC_TAPPermit pl
            where pl.PermitObjectId = t_Objects(p)
            and pl.TAPPermitObjectId = t_AssociatedPermit;
            api.pkg_relationshipupdate.Remove(t_RelId);
            t_EndPointId := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit',
                'TAPPermit');
            t_RelId := api.pkg_relationshipupdate.New (t_EndPointId, t_Objects(p),
                t_NewAssociatedPermit);
          end if;
        end if;
        -- Check for Insignia Permits to consolidate
        t_PermitNumber := api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
        t_PermitTypeId := api.pkg_columnquery.value(t_Objects(p), 'PermitTypeObjectId');
        select r.PermitObjectId
        bulk collect into t_ActivePermits
        from query.r_ABC_TAPPermit r
        where r.TAPPermitObjectId = t_AssociatedPermit
          and api.pkg_columnquery.Value(r.PermitObjectId, 'State') = 'Active'
          and api.pkg_columnquery.Value(r.PermitObjectId, 'PermitNumber')
              != t_PermitNumber
          and api.pkg_columnquery.Value(r.PermitObjectId, 'PermitTypeObjectId') =
              t_PermitTypeId;
        t_ConsolidatePermits := 'N';

        if t_ActivePermits.count > 0 then
          t_ExpirationDate := api.pkg_columnquery.DateValue(t_Objects(p),
              'CalculatedExpirationDate');
          for c in 1..t_ActivePermits.count loop
            -- Check if the Other Active Permit has an expiration date greater than the new
            -- expiration date. If not, then don't consolidate.
            select (
              (select count(*) 
              from api.relationships r 
              join api.jobs j 
                on j.JobId = r.ToObjectId 
              where r.FromObjectId = t_ActivePermits(c)
                and r.EndPointId = t_RenewEndpointId
                and j.JobStatus in ('NEW','REVIEW'))
              + 
              (select count(*) 
              from api.relationships r 
              join api.jobs j 
                on j.JobId = r.ToObjectId 
              where r.FromObjectId = t_Objects(p)
                and r.EndPointId = t_RenewEndpointId
                and j.JobStatus in ('NEW','REVIEW'))
              )
            into t_RenewalCount
            from dual;
            if api.pkg_columnquery.Datevalue(t_ActivePermits(c), 'ExpirationDate') = 
                  t_ExpirationDate and t_RenewalCount = 0 then
              t_ConsolidatePermits := 'Y';
              t_ConsolidatedPermitId := t_ActivePermits(c);
              exit;
            end if;
          end loop;
        end if;  
        
        if t_ConsolidatePermits = 'Y' then
          t_ConsolidatedPermitId := t_ActivePermits(1);
          --Copy Vehicles to Active Permit to Consolidate the Vehicles
          t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
              'Vehicle');
          dbms_output.put_line('    Copying vehicle(s) from ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber')
              || ' to ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber'));
          for i in (select *
                    from query.r_abc_permitvehicle rpv
                    where rpv.PermitObjectId = t_Objects(p)) loop
                    null;
            t_VehicleId := api.pkg_objectupdate.New(t_VehicleDefId);
            t_RelId := api.Pkg_Relationshipupdate.New(t_ToEndPointId,
                t_ConsolidatedPermitId, t_VehicleId);
            api.pkg_columnupdate.setvalue(t_VehicleId, 'PreviousInsigniaNumber',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'PreviousInsigniaNumber'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'InsigniaNumber',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'InsigniaNumber'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'MakeModelYear',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'makemodelyear'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'OwnedOrLeasedLimousine',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'ownedorleasedlimousine'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'VIN',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'vin'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateregistration',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateregistration'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateofregistration',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateofregistration')); 
          end loop;
          -- Set the permit as Cancelled
          t_Consolidated := t_Consolidated + 1;
          api.pkg_columnupdate.SetValue(t_Objects(p), 'State', 'Cancelled');
          api.pkg_columnupdate.SetValue(t_Objects(p), 'Edit', 'N');
          api.pkg_columnupdate.setValue(t_Objects(p), 'SystemCancelled', 'Y');
          t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
          t_Reason := 'Consolidated with Permit ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber');
          api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation',
              sysdate);
          api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
          t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, t_Objects(p),
              t_ObjectId, sysdate);
          dbms_output.put_line('    ' || t_Reason);
        end if;
      end if;

      -- Associated License
      if api.pkg_columnquery.Value(t_Objects(p), 'LicenseObjectId') is not null then
        t_AssociatedPermit := api.pkg_columnquery.Value(t_Objects(p), 'LicenseObjectId');
        dbms_output.put_line('Finding Associated License ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'LicenseNumber'));
        if api.pkg_columnquery.Value(t_AssociatedPermit, 'IsActive') = 'N' then
          dbms_output.put_line('Associated License ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'LicenseNumber') ||
              ' for ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber') || ' is not Active');
          t_NewAssociatedPermit := abc.Pkg_Abc_Workflow.GetLatestLicense(t_AssociatedPermit, t_Objects(p));
          if t_NewAssociatedPermit = 0 then
            -- Cancelling Insignia Permit
            t_Cancelled := t_Cancelled + 1;
            dbms_output.put_line('    No active associated License found. Cancelling Permit ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
            t_Reason := 'Permit cancelled as part of the 9442 Consolidation for not having an Active Associated License or Permit: ' || 
                api.pkg_columnquery.Value(t_AssociatedPermit, 'LicenseNumber');
            api.pkg_columnupdate.SetValue(t_Objects(p), 'State', 'Cancelled');
            api.pkg_columnupdate.SetValue(t_Objects(p), 'Edit', 'N');
            t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
            api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation',
                sysdate);
            api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
            t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, t_Objects(p),
                t_ObjectId, sysdate);
      continue;
          else
            t_ActiveRelated := t_ActiveRelated + 1;
            dbms_output.put_line('    Found active version of ' || api.pkg_columnquery.Value(t_AssociatedPermit, 'LicenseNumber') ||
                ' to associate with ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber'));
            select pl.RelationshipId
            into t_RelId
            from query.r_PermitLicense pl
            where pl.PermitObjectId = t_Objects(p)
            and pl.LicenseObjectId = t_AssociatedPermit;
            api.pkg_relationshipupdate.Remove(t_RelId);
            t_EndPointId := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit',
                'License');
            t_RelId := api.pkg_relationshipupdate.New (t_EndPointId, t_Objects(p),
                t_NewAssociatedPermit);
          end if;
        end if;
        -- Check for Insignia Permits to consolidate
        t_PermitNumber := api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber');
        t_PermitTypeId := api.pkg_columnquery.value(t_Objects(p), 'PermitTypeObjectId');
        select r.PermitObjectId
        bulk collect into t_ActivePermits
        from query.r_PermitLicense r
        where r.LicenseObjectId = t_AssociatedPermit
          and api.pkg_columnquery.Value(r.PermitObjectId, 'State') = 'Active'
          and api.pkg_columnquery.Value(r.PermitObjectId, 'PermitNumber')
              != t_PermitNumber
          and api.pkg_columnquery.Value(r.PermitObjectId, 'PermitTypeObjectId') =
              t_PermitTypeId;
        t_ConsolidatePermits := 'N';

        if t_ActivePermits.count > 0 then
          t_ExpirationDate := api.pkg_columnquery.DateValue(t_Objects(p),
              'CalculatedExpirationDate');
          for c in 1..t_ActivePermits.count loop
            -- Check if the Other Active Permit has an expiration date greater than the new
            -- expiration date. If not, then don't consolidate.
            select (
              (select count(*) 
              from api.relationships r 
              join api.jobs j 
                on j.JobId = r.ToObjectId 
              where r.FromObjectId = t_ActivePermits(c)
                and r.EndPointId = t_RenewEndpointId
                and j.JobStatus in ('NEW','REVIEW'))
              + 
              (select count(*) 
              from api.relationships r 
              join api.jobs j 
                on j.JobId = r.ToObjectId 
              where r.FromObjectId = t_Objects(p)
                and r.EndPointId = t_RenewEndpointId
                and j.JobStatus in ('NEW','REVIEW'))
              )
            into t_RenewalCount
            from dual;
            if api.pkg_columnquery.Datevalue(t_ActivePermits(c), 'ExpirationDate') = 
                  t_ExpirationDate and t_RenewalCount = 0 then
              t_ConsolidatePermits := 'Y';
              t_ConsolidatedPermitId := t_ActivePermits(c);
              exit;
            end if;
          end loop;
        end if;  
        
        if t_ConsolidatePermits = 'Y' then
          t_ConsolidatedPermitId := t_ActivePermits(1);
          --Copy Vehicles to Active Permit to Consolidate the Vehicles
          t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
              'Vehicle');
          dbms_output.put_line('    Copying vehicle(s) from ' || api.pkg_columnquery.Value(t_Objects(p), 'PermitNumber')
              || ' to ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber'));
          for i in (select *
                    from query.r_abc_permitvehicle rpv
                    where rpv.PermitObjectId = t_Objects(p)) loop
                    null;
            t_VehicleId := api.pkg_objectupdate.New(t_VehicleDefId);
            t_RelId := api.Pkg_Relationshipupdate.New(t_ToEndPointId,
                t_ConsolidatedPermitId, t_VehicleId);
            api.pkg_columnupdate.setvalue(t_VehicleId, 'PreviousInsigniaNumber',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'PreviousInsigniaNumber'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'InsigniaNumber',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'InsigniaNumber'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'MakeModelYear',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'makemodelyear'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'OwnedOrLeasedLimousine',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'ownedorleasedlimousine'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'VIN',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'vin'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateregistration',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateregistration'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateofregistration',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateofregistration')); 
          end loop;
          -- Set the permit as Cancelled
          t_Consolidated := t_Consolidated + 1;
          api.pkg_columnupdate.SetValue(t_Objects(p), 'State', 'Cancelled');
          api.pkg_columnupdate.SetValue(t_Objects(p), 'Edit', 'N');
          api.pkg_columnupdate.setValue(t_Objects(p), 'SystemCancelled', 'Y');
          t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
          t_Reason := 'Consolidated with Permit ' || api.pkg_columnquery.Value(t_ConsolidatedPermitId, 'PermitNumber');
          api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation',
              sysdate);
          api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
          t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, t_Objects(p),
              t_ObjectId, sysdate);
          dbms_output.put_line('    ' || t_Reason);
        end if;
      end if;

    end loop;
    
    dbms_output.put_line ('Permits cancelled due to not having an Active Permit / License: ' || t_Cancelled);
    dbms_output.put_line ('Permits re-associated with an Active Permit / License:          ' || t_ActiveRelated);
    dbms_output.put_line ('Permits cancelled due to consolidation:                         ' || t_Consolidated);
    
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
    
end;
