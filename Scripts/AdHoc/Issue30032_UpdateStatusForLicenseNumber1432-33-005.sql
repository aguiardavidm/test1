-- Created on 05/11/2017 by MICHEL.SCHEFFERS 
-- Issue 30032 - License number is at the wrong generation because of amendment done to wrong License
declare 
  -- Local variables here
  t_LicenseId_006 number := 39516046;
  t_LicenseId_005 number := 36133361;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
-- Activate license 1432-33-005-005
  if t_LicenseId_005 > 0 then
    dbms_output.put_line('Activating license 1432-33-005-005 (' || t_LicenseId_005 || ').');
    api.pkg_columnupdate.SetValue(t_LicenseId_005, 'State', 'Active');
    api.pkg_columnupdate.SetValue(t_LicenseId_005, 'IsLatestVersion', 'Y');
  end if;

-- Close license 1432-33-005-006
  if t_LicenseId_006 > 0 then
    dbms_output.put_line('Closing license 1432-33-005-006 (' || t_LicenseId_006 || ').');
    api.pkg_columnupdate.SetValue(t_LicenseId_006, 'State', 'Closed');
    api.pkg_columnupdate.SetValue(t_LicenseId_006, 'IsLatestVersion', 'N');
  end if;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;