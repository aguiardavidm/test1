declare
  t_JobNumber varchar2(40);
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select a.objectid,
                   (case 
                     when a.CivicAddressType = 'Y' then
                       'Civic'
                     when a.OtherAddressType = 'Y' then
                       'Other / International'
                     when a.POBoxAddressType = 'Y' then
                       'PO Box'
                     when a.RuralRouteAddressType = 'Y' then
                       'Rural Route'
                     when a.GeneralDeliveryAddressType = 'Y' then
                       'General Delivery'
                   end) AddressType
            from query.o_abc_addressonlineentry a
           where a.AddressType is null) loop
    insert into possedata.objectcolumndata cd
    (
    logicaltransactionid ,
    objectid ,
    columndefid ,
    attributevalueid 
    )
    values
    (
    api.pkg_logicaltransactionupdate.CurrentTransaction,
    i.objectid,
    api.pkg_configquery.ColumnDefIdForName('o_abc_addressonlineentry', 'AddressType'),
    (select dl.DomainValueId
       from api.domainlistofvalues dl
      where dl.DomainId = 1397743
        and dl.AttributeValue = i.addresstype)
    );
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;