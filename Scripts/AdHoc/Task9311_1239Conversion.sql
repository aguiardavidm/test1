begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *
              from possedba.log1239licensewarnings l) loop
    begin
      api.pkg_objectupdate.Remove(i.warningid, sysdate);
    exception when others then
      null;
    end;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
/
begin
  execute immediate('drop table possedba.log1239licensewarnings');
exception when others then
  null;
end;
/
-- Create table
create table LOG1239LICENSEWARNINGS
(
  licenseid           NUMBER,
  licensenumber       VARCHAR2(40),
  inactivitystartdate DATE,
  batchdate           DATE,
  warningid           number
)
/
--12.39
declare
  t_LicenseList         api.pkg_definition.udt_IdList;
  t_WarningStartdate    date;
  t_BatchDate           date;
  t_JobId               api.pkg_definition.udt_IdList;
  t_LicenseWarningCount number;
  t_Petitions           api.udt_objectlist;
  t_PetitionCount       number;
  t_ConvertedPetition   number;
  t_AlreadyProcessed    number;
  t_RenewalCount        number;
  t_LicWarningId        api.pkg_definition.udt_Id;
  t_RelId               api.pkg_definition.udt_Id;
  t_LicWarningType      api.pkg_definition.udt_Id := api.pkg_simplesearch.ObjectByIndex('o_ABC_LicenseWarningType', 'LicenseWarningType', '12.39REQD');
  t_LicWarningTypeEP    api.pkg_definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_LicenseWarning', 'LicenseWarning');
  t_LicLicWarningEP     api.pkg_definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseWarning');
  t_LicenseWarningDef   api.pkg_definition.udt_Id := api.pkg_configquery.ObjectDefIdForName('o_ABC_LicenseWarning');
  t_PetitionStartDate   date;
  t_PetitionEndDate     date;
  t_WarningDate         date;

  --Function to get the Warning Start Date since we don't have the config yet
  function WarningStartDate(a_LicenseID number) return date is
                            t_WarningStartDate date;
    begin
      select
      (case when InactivityStartDate is not null then
        (case when ExpirationMethod = 'Manual' then trunc(add_months(ExpirationDate, 24))
              when ExpirationMethod = 'Number of Days' then (trunc((trunc(InactivityStartDate) + ExpirationNumber)) + ExpirationNumber)-1
              when ExpirationMethod = 'Number of Years' then (add_months(trunc((add_months(trunc(InactivityStartDate), ExpirationNumber * 12))-1), ExpirationNumber * 12))
              when ExpirationMethod = 'Number of Months' then (add_months(trunc((add_months(trunc(InactivityStartDate), ExpirationNumber))-1), ExpirationNumber))-1
              when ExpirationMethod = 'End of Specific Month' then add_months((case
                                                                        when (last_day(to_date((ExpirationMonth || ' 01, ' || (to_char(InactivityStartDate, 'YYYY'))),'Month DD, YYYY'))) >= InactivityStartDate then
                                                                          trunc(last_day(to_date((ExpirationMonth || ' 01, ' || (to_char(InactivityStartDate, 'YYYY'))),'Month DD, YYYY')))
                                                                        else
                                                                          trunc(last_day(to_date((ExpirationMonth || ' 01, ' || (to_char(InactivityStartDate, 'YYYY')+1)),'Month DD, YYYY')))
                                                                      end), 24)/* SEASONAL IS NOT HERE YET SO I CAN'T USE THIS
              when ExpirationMethod = 'Seasonal' then add_months((case
                                                          when trunc(InactivityStartDate) > 
                                                               trunc(to_date(ExpirationEndMonth || '/' || ExpirationEndDay || to_char(InactivityStartDate, 'yyyy'),'MM/DD/YYYY')) then
                                                               add_months(trunc(to_date(ExpirationEndMonth || '/' || ExpirationEndDay || to_char(InactivityStartDate, 'yyyy'),'MM/DD/YYYY')),12)
                                                          else
                                                               trunc(to_date(ExpirationEndMonth || '/' || l.ExpirationEndDay || to_char(InactivityStartDate, 'yyyy'),'MM/DD/YYYY'))  
                                                      end), 24)*/
          end)
      end)
    into t_WarningStartDate
    from query.o_abc_license l
   where l.objectid = a_LicenseId
     and l.objectdeftypeid = 1;
    return t_WarningStartDate;
  end;
      
    
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_LicenseList := api.pkg_simplesearch.ObjectsByIndex('o_ABC_License', 'State', 'Active');
  t_BatchDate := trunc(to_date('07012016', 'mmddyyyy'));
  for i in 1 .. t_LicenseList.count loop
    --Reset variables
    t_PetitionStartDate := null;
    t_PetitionEndDate   := null;
    --Make sure Inactivity Start Date is entered and 2 terms have passed
    t_WarningStartdate := trunc(WarningStartDate(t_LicenseList(i)));
    if api.pkg_columnquery.DateValue(t_LicenseList(i), 'InactivityStartDate') is not null 
        and t_WarningStartdate <= trunc(t_BatchDate) then
      if api.pkg_columnquery.Value(t_LicenseList(i), 'IssuingAuthority') = 'Municipality' then
        --Check for an existing current warning on the license
        select count(1)
          into t_LicenseWarningCount
          from query.r_WarningLicense r
         where r.LicenseId = t_LicenseList(i)
           and (api.pkg_columnquery.Value(r.LicenseWarningId, 'WarningType1239') = 'Y' or api.pkg_columnquery.Value(r.LicenseWarningId, 'LicenseWarningType') in ('12.39REQD', 
                                                                                                                                                                  '12.39 Restriction'))
           and (t_BatchDate < nvl(api.pkg_columnquery.DateValue(r.LicenseWarningId, 'EndDate'), to_date(3999, 'yyyy')));
        if t_LicenseWarningCount = 0 then
          --Check for an existing petition over the stored rel.
        select count(1), min(api.pkg_columnquery.DateValue(r.PetitionTypeId, 'StartDate')) StartDate, max(api.pkg_columnquery.DateValue(r.PetitionTypeId, 'EndDate')) EndDate
          into t_PetitionCount, t_PetitionStartDate, t_PetitionEndDate
          from query.r_abc_masterlicenselicense ml --go to our master license
          join query.r_abc_masterlicenselicense cl on cl.MasterLicenseObjectId = ml.MasterLicenseObjectId --get all of the child licenses, including ourself
          join query.r_abc_LicensePetition j on j.LicenseId = cl.LicenseObjectId --get petitions on each child license
          join query.r_ABC_PetitionPetitiontype r on r.PetitionId = j.PetitionId
          left outer join dataconv.j_abc_petition d on d.objectid = j.PetitionId
           where ml.LicenseObjectId = t_LicenseList(i)
             and (api.pkg_columnquery.Value(j.PetitionId, 'StatusDescription') = 'Approved' or d.statustag = 'COMP')
             and api.pkg_columnquery.Value(r.PetitionTypeId, 'PetitionType') = '12.39';
--             and t_WarningStartDate between api.pkg_columnquery.DateValue(r.PetitionTypeId, 'StartDate') and api.pkg_columnquery.DateValue(r.PetitionTypeId, 'EndDate');
          if t_PetitionCount = 0
            or (t_PetitionCount != 0 and to_date('07 01 2016', 'mm dd yyyy') not between t_PetitionStartDate and t_PetitionEndDate and(t_PetitionEndDate < to_date('07 01 2016', 'mm dd yyyy'))) then
            --Create Warning and relate to license
            t_WarningDate := greatest(nvl(t_PetitionEndDate+1, to_date('01011001', 'mmddyyyy')), to_date('0701' || to_char(t_WarningStartDate, 'yyyy'), 'mmddyyyy'));
            t_LicWarningId := api.pkg_objectupdate.New(t_LicenseWarningDef);
            t_RelId := api.pkg_relationshipupdate.New(t_LicWarningTypeEP, t_LicWarningId, t_LicWarningType);
            api.pkg_columnupdate.SetValue(t_LicWarningId, 'StartDate', trunc(t_WarningDate));
            api.pkg_columnupdate.SetValue(t_LicWarningId, 'Comments', '12.39 RULING ISSUED FOR ' ||
                                                                      to_char(t_WarningDate, 'yyyy') || '-' ||
                                                                      (to_char(t_WarningDate, 'yyyy')+1));
            t_RelId := api.pkg_relationshipupdate.New(t_LicLicWarningEP, t_LicenseList(i), t_LicWarningId);
            --log it
            insert into possedba.log1239licensewarnings
            (licenseid, licensenumber, inactivitystartdate, batchdate, warningid)
            values
            (t_LicenseList(i),
            api.pkg_columnquery.value(t_LicenseList(i), 'LicenseNumber'),
            api.pkg_columnquery.DateValue(t_LicenseList(i), 'InactivityStartDate'),
            t_WarningDate,
            t_LicWarningId
            );
          end if; -- Petition Doesn't Exist
        end if; -- Warning Doesn't exist
      end if; -- Municipality Issued
    end if; -- Valid Inactivity Start Date
  end loop; -- License loop
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
/
