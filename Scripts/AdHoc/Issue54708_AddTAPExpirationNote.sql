/*-----------------------------------------------------------------------------
 * Author: Paul
 * Expected run time: < 5 sec
 * Purpose: This script adds File Notes for Permits on issue
 *   54708.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  
  t_PermitIds                           udt_IdList;
  t_NoteDefId                           udt_Id;
  t_NoteId                              udt_Id;
  t_Text                                varchar2(100) := 'System updated due to Issue 54708 - TAP expiration date extending to 06/30/2020 - COVID-19';
  t_Tag                                 varchar2(50) := 'GEN';
  t_count                               number := 0;
  
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  select p.objectid
  bulk collect into t_PermitIds
  from
    query.o_abc_permit p
  where p.state = 'Active'
    and p.permitnumber in (
        '13006519','14001855','14001460','15007316','15007110','15007317','15008724','15008726','15008727','15001319','15007733','14007824',
        '15000218','15006516','22788','24169','25035','30105','31490','31738','32812','33181','33542','34332','34530','35321','35403','35548',
        '35549','36930','40943','41099','41488','41487','42337','43070','44259','44924','46477','48107','48241','48758','50814','57543','54254',
        '54253','54642','54958','55223','55537','56228','58541','58934','66084','64459','66969','67067','67339','67340','68083','68718','68716',
        '68901','71707','72689','74217','74283','74385','75880','76187','76158','76780','76781','77163','78389','78417','14008848','15008717',
        '15005327','20389','27196','43230','43589','45371','48759','50869','51279','55211','58876','59030','60009','60578','60783','61894','66722',
        '67960','67959','68748','68775','69302','69301','69300','69318','69640','69865','69997','70061','70145','70309','70315','71183','71857',
        '71856','72509','72840','74067','75407','76676','77025','77105','77265','77751','79599','78735','80026','80025');
  
  for i in 1..t_PermitIds.count() loop
      select nd.NoteDefId
      into t_NoteDefId
      from api.NoteDefs nd
      where nd.Tag = t_Tag;

      t_NoteId := api.pkg_NoteUpdate.New(t_PermitIds(i), t_NoteDefId, 'N', t_Text);
      t_count := t_count +1;
      dbms_output.put_line(t_PermitIds(i));
  end loop;
  dbms_output.put_line(t_count);
  api.pkg_logicaltransactionupdate.EndTransaction;
--  commit;  
end;
