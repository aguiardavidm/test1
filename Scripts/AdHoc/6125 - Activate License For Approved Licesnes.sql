PL/SQL Developer Test script 3.0
296
declare

a_ObjectId number := :a_ObjectId;
a_AsOfDate date := sysdate;

    t_EndpointId                        number := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'Secondary');
    t_JobDef                            varchar2(30);
    t_JobId                             number := a_ObjectId;
    t_PrimaryLicenseObjectId            number;
    t_RelId                             number;
    t_SecSourceLicenseObjectId          number;
    t_SecSourceLicenseState             varchar2(20);
    t_SourceLicenseObjectId             number;
    t_SourceLicenseState                varchar2(20);
    t_MunicipalityOrderOfIssuance       number;
    t_IssuingAuthority                  varchar2(20);
    t_GenerationNumber                  number;
    t_CountyCode                       varchar2(20);
    t_LicenseTypeCode                   varchar2(20);
    t_MunicipalityCode                  varchar2(20);
    t_MunicipalityCountyCode            varchar2(20);
    t_StateOrderOfIssuance              number;
    t_StateCode                         varchar2(20);
    t_OfficeId                          number := api.pkg_columnquery.numericValue(t_JobId, 'OfficeObjectId');
    t_RegionId                          number;
    t_LicenseId                         number;
    t_SystemSettingsId                  number;
    t_StartInactivity                   varchar2(01);
    t_SeqExists                         varchar2(1);
    a_JobId                             number;
    t_Licensee                          number;
    t_OnlineUser                        number;
    t_ExistingRelCount                  number;
    t_count                             number:=0;
    


  begin

api.pkg_logicaltransactionupdate.ResetTransaction();

for c in (select ra.ExternalFileNum, ra.LicenseNumber, ra.StatusDescription,  api.pkg_columnquery.Value(ra.LicenseObjectId, 'State') LICENSESTATE, ra.CompletedDate, ra.JobId
  from query.j_Abc_Renewalapplication ra
 where ra.StatusDescription in ('Distribute', 'Approved')
   and api.pkg_columnquery.Value(ra.LicenseObjectId, 'State') not in  ('Active', 'Closed')--ra.CompletedDate > to_date('07/29/2015','MM/DD/YYYY');
order by 1 ) loop

    a_ObjectId := c.jobid;
    t_JobId    := c.jobid;


    t_JobDef := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_PrimaryLicenseObjectId := api.pkg_ColumnQuery.Value(t_JobId, 'LicenseObjectId');
    t_SourceLicenseObjectId := api.pkg_ColumnQuery.NumericValue(t_PrimaryLicenseObjectId, 'SourceObjectId');
    t_SourceLicenseState := api.pkg_ColumnQuery.Value(t_SourceLicenseObjectId, 'State');
    t_IssuingAuthority := api.pkg_ColumnQuery.Value(t_JobId, 'IssuingAuthority');

    t_LicenseId := api.pkg_ColumnQuery.Value(t_JobId, 'LicenseObjectId');

    t_RegionId := api.pkg_columnquery.Value(t_JobId, 'RegionObjectId');
    t_CountyCode := api.pkg_columnquery.value(t_RegionId, 'CountyCode');

    t_LicenseTypeCode := api.pkg_columnquery.value(t_JobId,'LicenseTypeCode');
    
   --     api.pkg_errors.raiseerror(-20000, 'SS ID: ' || t_SystemSettingsId || ' State Code: ' || t_StateCode || ' License ID: ' || t_PrimaryLicenseObjectId || ' License Code: ' || api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'LicenseTypeCode'));
    -- Check License Specific Details
      if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'LicenseTypeCode')   = '13' then
         if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'RetailTransitType') = 'Limousine' then
            if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'VehiclesEntered') is null then
               api.pkg_errors.RaiseError(-20000, 'At least one Vehicle is required on the License.');
            end if;
            for v in (select vehicleid
                        from query.r_Abc_Licensevehicle
                       where licenseid = t_PrimaryLicenseObjectId) loop
                       if api.pkg_columnquery.Value(v.vehicleid, 'InsigniaNumber') is null then
                          api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an InsigniaNumber for each Limo on the License.');
                          exit;
                       end if;
            end loop;
         end if;
         if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'RetailTransitType') = 'Boat' then
            if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'VesselsEntered') is null then
               api.pkg_errors.RaiseError(-20000, 'At least one Vessel is required on the License.');
            end if;
         end if;
      end if;
    --first check for mandatories on the Primary License (unless the expiration method is Never)
      if api.pkg_ColumnQuery.DateValue(t_PrimaryLicenseObjectId, 'EffectiveDate') is null then
        api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Effective Date for the License.');
      end if;
    if api.pkg_ColumnQuery.Value(t_PrimaryLicenseObjectId, 'ExpirationMethod') != 'Never' then
      if api.pkg_ColumnQuery.DateValue(t_PrimaryLicenseObjectId, 'CalculatedExpirationDate') is null then
        api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Expiration Date for the License.');
      end if;
    end if;

    --if the License is currently suspended, we need to make sure the new one remains Suspended
    if t_SourceLicenseState = 'Suspended' then
      api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'State', 'Suspended');
      --find all related Accusation jobs and relate the new License to them
      for a in (select al.RelationshipId, al.AccusationJobId
                  from query.r_ABC_AccusationLicense al
                  where al.LicenseObjectId = t_SourceLicenseObjectId) loop

        api.pkg_RelationshipUpdate.Remove(a.relationshipid);
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_configquery.EndPointIdForName('j_ABC_Accusation', 'License'), a.accusationjobid, t_PrimaryLicenseObjectId);
      end loop;
    else
      api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'State', 'Active');
    end if;

    api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'IsLatestVersion', 'Y');
    api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'IssueDate', trunc(sysdate));
    api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'ExpirationDate', api.pkg_columnquery.DateValue(t_PrimaryLicenseObjectId, 'CalculatedExpirationDate'));

   --Generate the license number for a new application if the issuing authority is municipality.
    if t_JobDef = 'j_ABC_NewApplication' then
      if t_IssuingAuthority = 'Municipality' then
        t_MunicipalityCode := api.pkg_columnquery.value(t_OfficeId, 'MunicipalityCode');
        t_MunicipalitycountyCode := api.pkg_columnquery.value(t_OfficeId, 'MunicipalityCountyCode');
        begin
          execute immediate 'select abc.muniorderissue' || t_MunicipalitycountyCode || '_seq.nextval from dual'
          into t_MunicipalityOrderOfIssuance;
        exception when OTHERS then
          api.pkg_errors.raiseerror(-20100, 'Order of Issuance sequence does not exist for Municipality ' || t_MunicipalitycountyCode);
        end;
        api.pkg_ColumnUpdate.SetValue(t_OfficeId, 'OrderOfIssuance', t_MunicipalityOrderOfIssuance);
        t_GenerationNumber := 1;
        api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'LicenseNumber', lpad(t_CountyCode, 2, '00') || lpad(t_MunicipalityCode, 2, '00') || '-' || lpad(t_LicenseTypeCode, 2, '00') || '-' || lpad(t_MunicipalityOrderOfIssuance, 3, '000') || '-' || lpad(t_GenerationNumber, 3, '000'));
     --Generate the license number for a new application if the issuing authority is state.
      elsif t_IssuingAuthority = 'State' then
        select o.ObjectId
        into t_SystemSettingsId
        from query.o_systemsettings o
        where rownum < 2;
        begin
          t_StateOrderOfIssuance := abc.stateorderissue_seq.nextval;
        exception when others then
          api.pkg_errors.RaiseError(-20101, 'Order of Issuance sequence does not exist for State Issued');
        end;
        t_StateCode := api.pkg_columnquery.numericvalue(t_SystemSettingsId, 'StateCode');
        if t_StateOrderOfIssuance = '1' then
          api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'StateCode', t_StateCode + 1);
        end if;
        api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'OrderOfIssuance', t_StateOrderOfIssuance);
        t_GenerationNumber := 1;
        api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'LicenseNumber', lpad(t_StateCode, 4, '0000') || '-' || lpad(t_LicenseTypeCode, 2, '00') || '-' || lpad(t_StateOrderOfIssuance, 3, '000') || '-' || lpad(t_GenerationNumber, 3, '000'));
      end if;
    end if;

	if t_JobDef = 'j_ABC_AmendmentApplication' then
      select at.StartInactivity
        into t_StartInactivity
        from query.o_abc_amendmenttype at
       where at.Title = api.pkg_columnquery.Value (t_JobId , 'AmendmentType');
-- either set or clear the Inactivity Date based on the Amendment Type
      if t_StartInactivity = 'Y' then
         api.pkg_columnupdate.SetValue(t_LicenseId, 'InactivityStartDate', sysdate);
      else
         api.pkg_columnupdate.RemoveValue(t_LicenseId, 'InactivityStartDate');
      end if;
   end if;
	
   if t_JobDef in ('j_ABC_AmendmentApplication', 'j_ABC_RenewalApplication', 'j_ABC_Reinstatement') then
      --if this is an Amendment or Renewal or Reinstatement, we need to close the source License
      --if the source License was Revoked or Expired, we leave it in that state, otherwise we Close it
      if t_SourceLicenseState not in ('Revoked', 'Expired') then
        api.pkg_ColumnUpdate.SetValue(t_SourceLicenseObjectId, 'State', 'Closed');
      end if;
      --this License is no longer the latest version
      api.pkg_ColumnUpdate.SetValue(t_SourceLicenseObjectId, 'IsLatestVersion', 'N');

      --we also need to close any other Licenses that have been issued through concurrent amendments, renewals, re-instatements
      for j in (select r2.LicenseObjectId
                  from query.r_ABC_MasterLicenseLicense r1
                    join query.r_ABC_MasterLicenseLicense r2
                      on r2.MasterLicenseObjectId = r1.MasterLicenseObjectId
                    join query.o_abc_License l
                      on l.objectid = r2.LicenseObjectId
                  where r1.LicenseObjectId = t_PrimaryLicenseObjectId
                    and r2.LicenseObjectId not in (t_SourceLicenseObjectId, t_PrimaryLicenseObjectId)
                    and l.State = 'Active') loop
        api.pkg_ColumnUpdate.SetValue(j.LicenseObjectId, 'State', 'Closed');
        api.pkg_ColumnUpdate.SetValue(j.LicenseObjectId, 'IsLatestVersion', 'N');
      end loop;

      --if this is a secondary license, ensure the new version replaces the old on the primary license
      for l in (select r.RelationshipId, r.PrimaryLicenseObjectId
                  from query.r_ABC_PrimarySecondaryLicense r
                  where r.SecondaryLicenseObjectId = t_SourceLicenseObjectId) loop
        api.pkg_RelationshipUpdate.Remove(l.RelationshipId);
        t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, l.PrimaryLicenseObjectId, t_PrimaryLicenseObjectId);
      end loop;
    end if;

    --now the Secondaries...
    for i in (select r.SecondaryLicenseObjectId
                from query.r_ABC_PrimarySecondaryLicense r
                where r.PrimaryLicenseObjectId = t_PrimaryLicenseObjectId) loop
      t_SecSourceLicenseObjectId := api.pkg_ColumnQuery.NumericValue(i.SecondaryLicenseObjectId, 'SourceObjectId');
      t_SecSourceLicenseState := api.pkg_ColumnQuery.Value(t_SecSourceLicenseObjectId, 'State');

      --first check for mandatories on Secondary Licenses
        if api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'EffectiveDate') is null then
          api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Effective Date for all the Secondary Licenses.');
        end if;

      if api.pkg_ColumnQuery.Value(i.SecondaryLicenseObjectId, 'ExpirationMethod') != 'Never' then
        if api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'CalculatedExpirationDate') is null then
          api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Expiration Date for all the Secondary Licenses.');
        end if;
      end if;

      --if the License is suspended, we need to make sure the new one remains Suspended
      if t_SecSourceLicenseState = 'Suspended' then
        api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Suspended');
        --find all related Accusation jobs and relate the new License to them
        for a in (select al.RelationshipId, al.AccusationJobId
                    from query.r_ABC_AccusationLicense al
                    where al.LicenseObjectId = t_SecSourceLicenseObjectId) loop

          api.pkg_relationshipupdate.Remove(a.RelationshipId);
          t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndPointIdForName('j_ABC_Accusation', 'License'), a.AccusationJobId, i.SecondaryLicenseObjectId);
        end loop;
      else
        api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Active');
      end if;

      api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'IsLatestVersion', 'Y');
      api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'IssueDate', trunc(sysdate));
      api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'ExpirationDate', api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'CalculatedExpirationDate'));

      -- DM 2011-12-30 New Application or an amendment with a secondary license with a pending state
      if (t_JobDef = 'j_ABC_NewApplication' or (t_JobDef = 'j_ABC_AmendmentApplication' and t_SecSourceLicenseState = 'Pending')) then
        toolbox.pkg_counter.GenerateCounter(i.SecondaryLicenseObjectId, sysdate, '[9999999]', 'ABC License Number', 'LicenseNumber');
      elsif t_JobDef in ('j_ABC_AmendmentApplication', 'j_ABC_RenewalApplication', 'j_ABC_Reinstatement') then
        --if this is an Amendment or Renewal or Reinstatement, we need to close the source License
        --if the source License was Revoked or Expired, we leave it in that state, otherwise we Close it
        if t_SecSourceLicenseState not in ('Revoked', 'Expired') then
          api.pkg_ColumnUpdate.SetValue(t_SecSourceLicenseObjectId, 'State', 'Closed');
        end if;
        --this License is no longer the latest version
        api.pkg_ColumnUpdate.SetValue(t_SecSourceLicenseObjectId, 'IsLatestVersion', 'N');

        --we also need to close any other Licenses that have been issued through concurrent amendments, renewals, re-instatements
        for js in (select r2.LicenseObjectId
                    from query.r_ABC_MasterLicenseLicense r1
                      join query.r_ABC_MasterLicenseLicense r2
                        on r2.MasterLicenseObjectId = r1.MasterLicenseObjectId
                      join query.o_abc_License l
                        on l.objectid = r2.LicenseObjectId
                    where r1.LicenseObjectId = i.SecondaryLicenseObjectId
                      and r2.LicenseObjectId not in (t_SecSourceLicenseObjectId, i.SecondaryLicenseObjectId)
                      and l.State = 'Active') loop

          api.pkg_ColumnUpdate.SetValue(js.licenseobjectid, 'State', 'Closed');
        end loop;
      end if;
    end loop;
   -- abc.pkg_abc_workflow.CopyLicenseeToOnlineUser(t_JobId); Pulled code from Procedure to Run directly in Script
   a_JobId  := a_Objectid;
   t_JobDef := api.pkg_columnquery.Value(a_JobId, 'ObjectDefName');
   if t_JobDef = 'j_ABC_NewApplication' then
      if  api.pkg_ColumnQuery.Value(a_JobId,'EnteredOnline') = 'Y' then
        select api.pkg_ColumnQuery.NumericValue(j.LicenseObjectId, 'LicenseeObjectId'), j.OnlineUserObjectId
          into t_Licensee, t_OnlineUser
          from query.j_ABC_NewApplication j
         where j.JobId = a_JobId;
      end if;

      if t_OnlineUser is not null then -- no copy for guest users and users that already have this licensee
        select count(1)
          into t_ExistingRelCount
          from query.r_ABC_UserLegalEntity rul
         where rul.UserId = t_OnlineUser
           and rul.LegalEntityObjectId = t_Licensee;
        if t_ExistingRelCount = 0 then
          extension.pkg_RelationshipUpdate.New(
            t_OnlineUser,
            t_Licensee,
            'LegalEntity');
        end if;
      end if;
    end if;
    t_count := t_count +1;
    if mod(t_count, 10) = 0 then
    api.pkg_logicaltransactionupdate.EndTransaction();
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();    
    end if;
    
end loop;    
api.pkg_logicaltransactionupdate.EndTransaction();


  end;
1
a_ObjectId
0
5
0
