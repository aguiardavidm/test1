declare
  t_OldLicenseId number := 32892684; --1225-32-033-006
  t_NewLicenseId number := 32892379; --1225-32-033-007
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  api.pkg_columnupdate.SetValue(t_OldLicenseId, 'State', 'Closed');
  api.pkg_columnupdate.SetValue(t_OldLicenseId, 'IsLatestVersion', 'N');
  api.pkg_columnupdate.SetValue(t_NewLicenseId, 'State', 'Active');
  api.pkg_columnupdate.SetValue(t_NewLicenseId, 'IsLatestVersion', 'Y');
  api.pkg_logicaltransactionupdate.EndTransaction;
end;