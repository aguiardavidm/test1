  -- Running Document fix for multiple Receipts.
  -- 2018/01/09 - Issue 30934
  -- Run time < 1 minute
declare 
  t_RelationshipId      number (9);
  t_EndPointId          number (9) := api.pkg_configquery.EndPointIdForName('o_Payment' ,'Receipt');
  
  begin
  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
  end if;

  api.pkg_logicaltransactionupdate.ResetTransaction();

  -- Find Payments with more than 1 Receipt Document
  for r in (select pr.PaymentObjectId
              from query.r_PaymentReceiptReport pr 
             group by pr.PaymentObjectId
            having count(*) > 1
           ) loop                               
  -- Find correct Document Relationship Id for the Payment
     dbms_output.put_line ('Updating documents for Payment ' || r.PaymentObjectId);  
     select max (r.RelationshipId)
     into t_RelationshipId
     from api.relationships r
     join api.objects o on o.ObjectId = r.ToObjectId
     join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
    where r.FromObjectId = r.PaymentObjectId
      and od.Name = 'd_ReceiptDocument';
  -- Use the found RelationshipId as parameter:   
     for dp in (select r1.RelationshipId, r1.ToObjectId
                from   api.relationships r1
                where  r1.FromObjectId = r.PaymentObjectId
                and    r1.RelationshipId != t_RelationshipId
                and    r1.EndPointId = t_EndPointId
               ) loop
        dbms_output.put_line ('Removing relationship ' || dp.RelationshipId);
        api.pkg_relationshipupdate.Remove(dp.relationshipid);  
     end loop;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;