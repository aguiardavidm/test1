create table possedba.Log1218WarningDeletion as (
select lw.*, r.LicenseWarningId WarningId
              from possedba.log1218licensewarnings lw
              join query.o_abc_license l on l.ObjectId = lw.licenseid
              join query.r_WarningLicense r on r.LicenseId = lw.licenseid
              join query.o_abc_licensewarning w on w.ObjectId = r.LicenseWarningId
             where lw.expirationdate >= to_date('06302016', 'mmddyyyy')
               and w.WarningTypeActive = 'Y')
;
/
declare
begin
  for i in (select *
              from possedba.Log1218WarningDeletion
             order by 1) loop
    begin
      api.pkg_logicaltransactionupdate.ResetTransaction;
      api.pkg_objectupdate.Remove(i.WarningId);
      api.pkg_logicaltransactionupdate.EndTransaction;
      commit;
    exception when others then
      rollback;
      dbms_output.put_line(sqlerrm || '. LicenseId: ' || i.licenseid || '. LicenseNumber: ' || i.licensenumber);
    end;
  end loop;
end;