/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 1 minute
 * Purpose: This script sets details on System Settings, creates/updates email
 * templates for Password Reset (General, Municipal, Police) and Municipal/
 * Police Review Reminder emails, and sets details on Permit Types.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_Count                               pls_integer := 0;
  t_EmailText                           varchar2(32767);
  t_LogicalTransactionId                udt_Id;
  t_PermitTypeIds                       udt_IdList;
  t_SystemSettingsId                    udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

  procedure CreateUpdateEmailTemplate (
    a_ObjectId                          udt_Id,
    a_Tag                               varchar2,
    a_Text                              varchar2
  ) is
    t_NoteDefId                         udt_Id;
    t_NoteId                            udt_Id;
  begin

    select n.NoteId
    into t_NoteId
    from
      api.Notes n
      join api.NoteDefs nd
          on n.NoteDefId = nd.NoteDefId
    where n.ObjectId = a_ObjectId
      and nd.Tag = a_Tag;
    -- if there is an existing email body template in system settings, update it.
    api.pkg_NoteUpdate.Modify(t_NoteId, 'N', a_Text);

  exception
    -- Create a new email body template
    when no_data_found then
      select nd.NoteDefId
      into t_NoteDefId
      from api.NoteDefs nd
      where nd.Tag = a_Tag;
      t_NoteId := api.pkg_NoteUpdate.New(a_ObjectId, t_NoteDefId, 'N', a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  dbug('---BEGINNING TRANSACTION---');
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  select count(1)
  into t_Count
  from
    api.notes n
    join api.notedefs nd
        on n.NoteDefId = nd.NoteDefId
  where nd.Tag in ('PWREML', 'MRREML', 'PRREML', 'MPWREM', 'PPWREM');
  dbug('Existing Email Body Templates: ' || t_Count);
  dbug('Email Body Templates to be created: ' || (5 - t_Count));

  select count(1)
  into t_Count
  from query.o_ABC_PermitType;
  dbug('Total Permit Types: ' || t_Count);

  select s.ObjectId
  into t_SystemSettingsId
  from query.o_SystemSettings s
  where s.ObjectId = api.pkg_SimpleSearch.ObjectByIndex(
      'o_SystemSettings', 'SystemSettingsNumber', 1);

  -- Online Messages: set instructional text
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniPermitRevInstructionalText',
      'When responding, you can Save your response as a draft if the response is incomplete. '
      || 'When awaiting additional information from the applicant, a Hold response allows a '
      || 'follow-up response. The Deny response should only be used when the applicant can only '
      || 'reapply for an approval.');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniDocUploadInstructionalText',
      'Please upload a working document to allow the conditions to be copied / pasted. Word '
      || 'documents are preferred.');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniEndorsementAcknowledgement',
      'On behalf of the town the response provided is the official response for this permit '
      || 'application. The response of Approved, indicates that the town does not recommend '
      || 'withholding the ability of the permit to proceed. When providing special conditions in '
      || 'addition to the approval it is my responsibility to provide a clear special conditions '
      || 'document listing required conditions. If the response is a Denial of the permit '
      || 'application, I agree that I have provide all necessary information and clear response in '
      || 'which the application did not meet the regulations or provide the required information '
      || 'for approval; I have performed all reasonable and responsible activities to ensure the '
      || 'applicant received a valid chance to correct, provide additional information, or meet '
      || 'the requirements for the permit.');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliceAnnouncementText', 'Welcome to the '
      || 'Police Portal site, here you can respond to applications and renewals requiring approval'
      || ' Navigate to the Permitting Menu and click an Application or Renewal in the grid below '
      || 'to provide a response.');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PolicePermitRevInstructionText',
      'When responding, you can Save your response as a draft if the response is incomplete. '
      || 'When awaiting additional information from the applicant, a Hold response allows a '
      || 'follow-up response. The Deny response should only be used when the applicant can only '
      || 'reapply for an approval.');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliceDocUploadInstructionText',
      'Please upload a working document to allow the conditions to be copied / pasted. Word '
      || 'documents are preferred.');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliEndorsementAcknowledgement',
      'On behalf of the town the response provided is the official response for this permit '
      || 'application. The response of Approved, indicates that the town does not recommend '
      || 'withholding the ability of the permit to proceed. When providing special conditions '
      || 'in addition to the approval it is my responsibility to provide a clear special '
      || 'conditions document listing required conditions. If the response is a Denial of the '
      || 'permit application, I agree that I have provide all necessary information and clear '
      || 'response in which the application did not meet the regulations or provide the required '
      || 'information for approval; I have performed all reasonable and responsible activities to '
      || 'ensure the applicant received a valid chance to correct, provide additional information, '
      || 'or meet the requirements for the permit.');
  -- Online Emails: Set subject headers for Password Reset and Muni/Police Review Reminder Emails
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'ResetEmailSubject',
      'Online ABC - Password Reset');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniPwdResetEmailSubject',
      'Online ABC - Password Reset');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PolicePwdResetEmailSubject',
      'Online ABC - Password Reset');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniRevReminderEmailSubject',
      'Permit Reviews Pending');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliceRevReminderEmailSubject',
      'Permit Reviews Pending');
  --  Online Emails: Set Email Body Templates
  t_EmailText :=
      '<img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"><br>'
      || '<br><font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: 12px;">'
      || 'Dear {FirstName},<br>You have reset your password - to complete the process, please '
      || '[[click here]]</font>.<br><br><br><br><br><br><br><br><br><br>'
      || '<font face="Arial" size="1">Plain Text Link:<br>{RegistrationLink}</font><br>';
  CreateUpdateEmailTemplate(t_SystemSettingsId, 'PWREML', t_EmailText);
  CreateUpdateEmailTemplate(t_SystemSettingsId, 'MPWREM', t_EmailText);
  CreateUpdateEmailTemplate(t_SystemSettingsId, 'PPWREM', t_EmailText);
  t_EmailText :=
      '<img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader-muni.jpg">'
      || '<br><br><font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: '
      || '12px;">A permit application (Social Affair or Catering or Extension of Premise) requires'
      || ' review by Municipal Issuing Authorities.  At this time please review this EMAIL '
      || 'NOTIFICATION OF application.  If you have any concerns or objections, please contact the'
      || ' NJABC Permit Unit at 609-984-2830 as soon as possible.  We may issue this permit after'
      || ' seven (7) business days unless notified otherwise.<br/><br/>Thank you,<br/>NJABC Staff'
      || '<br/><br/>';
  CreateUpdateEmailTemplate(t_SystemSettingsId, 'MRREML', t_EmailText);
  CreateUpdateEmailTemplate(t_SystemSettingsId, 'PRREML', t_EmailText);
  -- Municipality Notifications
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniEmailSubject',
      'NJ ABC - Permit Application for an event in your municipality.');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniNotiEmailHeader',
      'https://abc.lps.nj.gov/ABCPublic/images/EmailHeader-Muni.jpg');
  t_EmailText := 'APPLICANTS:' || chr(13) || chr(10) || 'You are receiving a courtesy copy of your'
      || ' application.<br/>' || chr(13) || chr(10) || '<br/>' || chr(13) || chr(10)
      || 'ISSUING AUTHORITIES:' || chr(13) || chr(10) || 'A permit (Social Affair or Catering or '
      || 'Extension of Premise) has been applied for in your municipality.   ' || chr(13)
      || chr(10) || 'At this time please review this EMAIL NOTIFICATION OF application.  If you '
      || 'have any concerns or objections, please contact the NJABC Permit Unit at 609-984-2830 '
      || 'as soon as possible.  We may issue this permit after seven (7) business days unless '
      || 'notified otherwise.  If you require a copy of the issued permit, please request this '
      || 'directly from the applicant as they are supplied with an electronic permit certificate.'
      || chr(13) || chr(10) || 'PLEASE NOTE:  Future enhancements regarding this process will '
      || 'include your logging in and electronically endorsing permit applications in your town.'
      || '  Announcements will be forthcoming.' || chr(13) || chr(10) || '<br/>' || chr(13)
      || chr(10) || '<br/>';
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniEmailText', t_EmailText);
  t_EmailText := '<br/><br/>Thank you,<br/>NJABC Permit Unit';
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniEmailBodyFooter', t_EmailText);
  -- Police Notifications
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliceEmailSubject',
      'NJ ABC - Permit Application for an event in your jurisdiction.');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliceEmailHeaderPhotoURL',
      'https://abc.lps.nj.gov/ABCPublic/images/EmailHeader-Muni.jpg');
  t_EmailText := 'APPLICANTS:' || chr(13) || chr(10) || 'You are receiving a courtesy copy of your'
      || ' application.<br/>' || chr(13) || chr(10) || '<br/>' || chr(13) || chr(10)
      || 'ISSUING AUTHORITIES:' || chr(13) || chr(10) || 'A permit (Social Affair or Catering or '
      || 'Extension of Premise) has been applied for in your jurisdiction.   ' || chr(13)
      || chr(10) || 'At this time please review this EMAIL NOTIFICATION OF application.  If you '
      || 'have any concerns or objections, please contact the NJABC Permit Unit at 609-984-2830 '
      || 'as soon as possible.  We may issue this permit after seven (7) business days unless '
      || 'notified otherwise.  If you require a copy of the issued permit, please request this '
      || 'directly from the applicant as they are supplied with an electronic permit certificate.'
      || chr(13) || chr(10) || 'PLEASE NOTE:  Future enhancements regarding this process will '
      || 'include your logging in and electronically endorsing permit applications in your town.'
      || '  Announcements will be forthcoming.' || chr(13) || chr(10) || '<br/>' || chr(13)
      || chr(10) || '<br/>';
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliceEmailBodyHeader', t_EmailText);
  t_EmailText := '<br/><br/>Thank you,<br/>NJABC Permit Unit';
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliceEmailBodyFooter', t_EmailText);

  -- Get all Permit Types
  select pt.ObjectId
  bulk collect into t_PermitTypeIds
  from query.o_ABC_PermitType pt;

  -- Loop through Permit Types. Set Business Days to Respond and Muni Reminder Days after Submittal
  for i in 1..t_PermitTypeIds.count() loop
    api.pkg_ColumnUpdate.SetValue(t_PermitTypeIds(i), 'BusinessDaysToRespond', 7);
    api.pkg_ColumnUpdate.SetValue(t_PermitTypeIds(i), 'MunicipalityReminderDaysAfterS', 5);
  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
  dbug(chr(10) || '---TRANSACTION COMPLETE---');

  select count(1)
  into t_Count
  from
    api.LogicalTransactions l
    join api.Notes n
        on n.LogicalTransactionId = l.LogicalTransactionId
    join api.NoteDefs nd
        on n.NoteDefId = nd.NoteDefId
  where l.LogicalTransactionId = t_LogicalTransactionId
    and nd.Tag in ('PWREML', 'MRREML', 'PRREML', 'MPWREM', 'PPWREM');
  dbug('Email Body Templates Updated: ' || t_Count);

  select count(1)
  into t_Count
  from
    api.LogicalTransactions l
    join api.objects o
        on l.LogicalTransactionId = o.LogicalTransactionId
    join query.o_ABC_PermitType pt
        on o.ObjectId = pt.ObjectId
  where l.LogicalTransactionId = t_LogicalTransactionId;
  dbug('Permit Types Updated: ' || t_Count);

  commit;

end;
