-- Created on 7/12/2017 by MICHEL.SCHEFFERS
-- Issue 30900 - New G/L Account ABS for ALCOHOLIC BEVERAGE SEMINAR
--               Make G/L Account WS inactive
--               Associate Permit Type WS to new G/L Account ABS
declare 
  -- Local variables here
  t_GLAccountId  integer;
  t_GlAcctObjId  integer;
  t_GlAcctDefId  number := api.pkg_configquery.ObjectDefIdForName('o_GlAccount');  
  t_PermitTypeId number;
  t_RelId        number;
  t_EndPoint     number := api.pkg_configquery.EndPointIdForName('o_ABC_PermitType', 'GLAccount');

begin
    api.pkg_logicaltransactionupdate.ResetTransaction;
    
    -- Create new GL Account
    select gl.GLAccountId 
    into   t_GLAccountId
    from   api.GLAccounts gl
    where  gl.GLAccount = 'ABS';
    api.pkg_objectupdate.RegisterExternalObject('o_GLAccount', t_GLAccountId);
    Select reo.ObjectId
    into   t_GlAcctObjId
    from   api.registeredexternalobjects reo 
    where  reo.LinkValue = t_GLAccountId
    and    reo.ObjectDefId = t_GlAcctDefId;
    api.pkg_columnupdate.SetValue(t_GlAcctObjId, 'Active', 'Y');

    -- Make old GL Account inactive
    Select g.ObjectId
    into   t_GLAccountId
    from   query.o_glaccount g
    where  g.Code = 'WS';
    api.pkg_columnupdate.SetValue(t_GLAccountId, 'Active', 'N');

    -- Associate Permit Type WS to new GL Account
    Select p.ObjectId
    into   t_PermitTypeId
    from   query.o_abc_permittype p
    where  p.Code = 'WS';
    begin
       select r.RelationshipId
       into   t_RelId
       from   query.r_abc_permittypeglaccount r
       where  r.PermitTypeId = t_PermitTypeId;
       api.pkg_relationshipupdate.Remove(t_RelId);
    exception
       when no_data_found then
          null;
    end;
    api.pkg_columnupdate.SetValue(t_PermitTypeId, 'Code', 'ABS');
    t_RelId := api.pkg_relationshipupdate.New(t_EndPoint, t_PermitTypeId, t_GlAcctObjId);
    api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;