-- Created on 04/10/2019 by JAE.HOLDERBY
/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: To remove data from Expressions with ovveride before changing them
 * into Expression details.
 *---------------------------------------------------------------------------*/
declare
  -- Local variables here
  t_JobidList   api.Pkg_Definition.udt_IdList;
  t_Counter     integer := 0;
begin
  -- Test statements here

  api.pkg_logicaltransactionupdate.ResetTransaction();
  --Select the job Id.
  select cp.jobid
  bulk collect into
       t_JobidList
  from query.j_abc_cplsubmissions cp;

 --Cycle through Jobs and set the value of Submitteddate to the Completed Date from the processess.
  for i in 1.. t_JobidList.count loop
    api.pkg_columnupdate.RemoveValue(t_JobidList(i), 'IsLateSubmission');
    api.pkg_columnupdate.RemoveValue(t_JobidList(i), 'IsTimelySubmission');

    t_Counter := t_Counter +1;

    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  dbms_output.put_line(t_Counter);
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;