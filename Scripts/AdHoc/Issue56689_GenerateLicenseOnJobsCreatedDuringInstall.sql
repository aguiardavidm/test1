declare
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  t_IdList udt_IdList;
  t_ProcessId                           number;
  t_ChangeProcessDefId                  number
    := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus');
  t_GenerateProcessDefId                number
    := api.pkg_configquery.ObjectDefIdForName('p_ABC_GenerateLicense');
  t_NoteDefId                           pls_integer;
  t_NoteId                              pls_integer;
  t_NoteTag                             varchar2(6) := 'GEN';
  t_NoteText                            varchar2(4000);
begin
  api.Pkg_Logicaltransactionupdate.ResetTransaction();

  select j.jobid
  bulk collect into t_IdList
  from api.processes p
  join api.processtypes pt
    on pt.processtypeid = p.processtypeid
  join api.jobs j on j.jobid = p.jobid
  join api.jobtypes jt on jt.jobtypeid = j.jobtypeid
  where p.Outcome is not null
  and pt.name = 'p_ABC_SubmitResolution'
  and p.DateCompleted between to_Date('10/14/2020', 'MM/DD/YYYY') and to_Date('10/15/2020', 'MM/DD/YYYY')
  and jt.jobtypeid = 1401021
  and p.outcome = 'Submitted'
  and not exists
    (select 1
      from api.processes p
      join query.r_SendLicense_Certificate r
        on r.ProcessId = p.processid
      where p.jobid = j.jobid);

  /*select 24969325
  bulk collect into t_IdList
  from dual;*/

  -- Create Note and Populate Text
  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;

  for i in 1..t_IdList.count loop
    dbms_output.put_Line(t_IdList(i));

    -- Put job into Awaiting Resolution
    t_ProcessId := api.pkg_processupdate.New(t_IdList(i), t_ChangeProcessDefId, 'Change Status', sysdate, sysdate, sysdate);
    api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange', 'Generating license certificate Per Issue 56689');
    api.pkg_processupdate.Complete(t_ProcessId, 'Awaiting Resolution');

    -- Create and complete the generate license process
    t_ProcessId := api.pkg_processupdate.New(t_IdList(i), t_GenerateProcessDefId, 'Generate License Certificate', sysdate, sysdate, sysdate);
    api.pkg_processupdate.Complete(t_ProcessId, 'Generated');

    t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
          'Changing status to generate the license certificate';
    t_NoteText := t_NoteText;
    t_NoteId := api.pkg_NoteUpdate.New(t_IdList(i), t_NoteDefId, 'Y', t_NoteText);
  end loop;

  api.Pkg_Logicaltransactionupdate.EndTransaction();
  commit;
end;
