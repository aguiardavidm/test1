/* This script registers GL Accounts that have not been registered yet */
declare
  t_RegisteredGLAcct number;
begin
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  for i in (select gl.GLAccountId
              from api.glaccounts gl
            minus
            select q.GLAccountId
              from query.o_glaccount q) loop
    api.pkg_Objectupdate.RegisterExternalObject('o_GLAccount', i.GLAccountId);
    
    -- Set Active to Y on the newly registered object
    select g.objectid
      into t_RegisteredGLAcct
      from query.o_glaccount g
     where g.GLAccountId = i.glaccountid;
    api.pkg_columnupdate.SetValue(t_RegisteredGLAcct, 'Active', 'Y');
  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;
