/*Create table to pull info from & for logging.  Should include all Permit: Historic CoOp License rels 
  where License is active and the rel was created by the script run on 3/7, identified by CreatedLogicalTransactionId = 1*/
create table possedba.PermitActiveCoopMemberFix as 
(select api.pkg_columnquery.Value(r.permitobjectid, 'PermitNumber') PermitNumber,
        l.licensenumber,
        r.relationshipid,
        r.CoOpMemberEffectiveDate,
        r.PermitObjectId,
        r.CoOpMemberObjectId
   from query.r_ABC_PermitHistorCoOpLicenses r
   join query.o_abc_license l
     on r.CoOpMemberObjectId = l.ObjectId
   join api.objects o on o.ObjectId = r.RelationshipId
  where l.State = 'Active'
    and o.CreatedLogicalTransactionId = 1);
/
declare
  t_EndPointId number := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'CoOpLicense');
  t_RelId      number;
  t_RelCount   number := 0;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *
              from possedba.PermitActiveCoopMemberFix t) loop
    --If the current rel doesn't exist, create it
    select count(1)
      into t_RelCount
      from query.r_ABC_PermitCoOpLicenses r
     where r.PermitObjectId = i.permitobjectid
       and r.CoOpMemberObjectId = i.coopmemberobjectid;
    if t_RelCount = 0 then
      t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, i.permitobjectid, i.coopmemberobjectid);
      api.pkg_columnupdate.SetValue(t_RelId, 'CoOpMemberEffectiveDate', i.CoOpMemberEffectiveDate);
    end if;
    --Remove historic rel
    api.pkg_relationshipupdate.Remove(i.relationshipid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;