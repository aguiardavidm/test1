-- Created on 11/25/2015 by MICHEL.SCHEFFERS 
-- Issue6915: Update Vehicle Type where vehicles on a renewal job have no Vehicle Type.
declare 
  t_JobNumber                varchar2(100) := ' ';
  -- Local variables here
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for v in (select ra.ExternalFileNum, l.LicenseNumber, l.State, lv.VehicleId, api.pkg_columnquery.Value(lv.VehicleId,'VehicleType') VehicleType, api.pkg_columnquery.Value(lv.VehicleId,'MakeModelYear') Make
              from query.j_abc_renewalapplication ra
              join query.o_abc_license l on l.ObjectId = ra.LicenseObjectId
              join query.r_abc_licensevehicle lv on lv.LicenseId = l.ObjectId
             order by ra.ExternalFileNum
           )
           loop
              if t_JobNumber != v.externalfilenum then
                 t_JobNumber := v.Externalfilenum;
                 dbms_output.put_line ('Job Number ' || v.Externalfilenum || '. License ' || v.licensenumber);
              end if;
              if v.Vehicletype is null and
                 v.Make is not null then
                 dbms_output.put_line ('Updating Vehicle ' || v.Vehicleid || '-' || v.make);
                 api.pkg_columnupdate.SetValue(v.VehicleId, 'VehicleType', 'Limousine');
              end if;
           end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
