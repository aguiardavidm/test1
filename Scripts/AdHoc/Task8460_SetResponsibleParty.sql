begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select f.TransactionId, r.ApplicantObjectId
              from finance.fees f
              join query.j_Abc_Permitrenewal r on r.ObjectId = f.JobId
             where f.ResponsibleObjectId is null) loop
    update finance.fees
       set responsibleobjectid = i.applicantobjectid
     where TransactionId = i.transactionid;
  end loop;
  api.pkg_logicaltransactionupdate.endTransaction;
end;