drop table possedba.PermitDateFixR2_t;
/
/*Create Table if it does exist, else output table Already exists*/
begin
  begin 
    execute immediate 'create table possedba.PermitDateFixR2_t as (select p.PermitTypeObjectId PermitTypeId, 
                       p.PermitType,
                       p.permittypecode, 
                       p.IssueDate, 
                       p.PermitNumber, 
                       p.ObjectId PermitId,
                       p.State,
                       cast(null as varchar2(4000)) ErrorMessage, 
                       cast(null as varchar2(1)) ErroredUpdate,
                       ''N'' Processed
                  from query.o_Abc_permit p
                 where p.EffectiveDate is null
                   and p.PermitType is not null
                )';
  exception when others then
    dbms_output.put_line(sqlerrm);
  end;
end;
/
--Get rid of data we don't want, somehow faster than filtering the table creation.
delete from possedba.PermitDateFixR2_t t
 where t.State != 'Active'
    or t.PermitTypeCode not in ('AI','AE','AU','BE','BSP','CT','CO','CTS','CTW','EP','EP14','FEST','FP','BRW','WN','PC','LTP','LTC','MA2',
                                'MS','EMP','MISC','OPS','PI','REHAB','RR','SP','SA','SOL','TAP','TE','TI','TLI','WF','WS');
/
--Add missing issue numbers from spreadsheet on issue
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('6/2/2015', 'mm/dd/yyyy')
 where t.permitnumber = '29';
 
update possedba.PermitdateFixR2_t t
set t.IssueDate = to_date('7/9/2015', 'mm/dd/yyyy')
 where t.permitnumber = '20496';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('8/3/2015', 'mm/dd/yyyy')
 where t.permitnumber = '21283';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('8/5/2015', 'mm/dd/yyyy')
 where t.permitnumber = '21366';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('8/20/2015', 'mm/dd/yyyy')
 where t.permitnumber = '21875';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('8/20/2015', 'mm/dd/yyyy')
 where t.permitnumber = '21885';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('7/6/2015', 'mm/dd/yyyy')
 where t.permitnumber = '20301';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('7/24/2015', 'mm/dd/yyyy')
 where t.permitnumber = '21055';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('8/11/2015', 'mm/dd/yyyy')
 where t.permitnumber = '21598';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('8/12/2015', 'mm/dd/yyyy')
 where t.permitnumber = '21653';
 
update possedba.PermitdateFixR2_t t
   set t.IssueDate = to_date('7/28/2015', 'mm/dd/yyyy')
 where t.permitnumber = '21160';

update possedba.permitdatefixr2_t t
   set t.issuedate = to_date('Jul 1, 2005', 'Mon dd, yyyy')
 where t.permitnumber = '4005073';
commit;

update possedba.permitdatefixr2_t t
   set t.issuedate = to_date('09/23/2015', 'mm/dd/yyyy')
 where t.permitnumber = '26696';
commit;
/
 
-- Created on 10/13/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  t_RaiseError varchar2(1);
  t_AI number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','AI');
  t_AE number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','AE');
  t_AU number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','AU');
  t_BE number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','BE');
  t_BSP number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','BSP');
  t_CT number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','CT');
  t_CCL number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','CCL');
  t_CO number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','CO');
  t_CTS number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','CTS');
  t_CTW number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','CTW');
  t_COOP number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','COOP');
  t_EP number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','EP');
  t_EP14 number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','EP14');
  t_APRVL number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','APRVL');
  t_FEST number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','FEST');
  t_FP number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','FP');
  t_BRW number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','BRW');
  t_WN number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','WN');
  t_PC number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','PC');
  t_LTP number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','LTP');
  t_LTC number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','LTC');
  t_MA2 number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','MA2');
  t_MS number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','MS');
  t_EMP number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','EMP');
  t_MISC number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','MISC');
  t_OPS number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','OPS');
  t_APROS number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','APROS');
  t_APRV number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','APRV');
  t_PI number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','PI');
  t_REHAB number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','REHAB');
  t_RR number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','RR');
  t_SP number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','SP');
  t_SA number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','SA');
  t_SOL number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','SOL');
  t_TAP number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','TAP');
  t_TE number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','TE');
  t_TI number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','TI');
  t_TLI number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','TLI');
  t_WF number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','WF');
  t_WS number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType','Code','WS');


  t_PermitObjectDefid     number := api.pkg_configquery.ObjectDefIdForName('o_Abc_Permit');
  t_PermitNumberColDefid  number := api.pkg_configquery.ColumnDefIdForName('o_Abc_Permit','PermitNumber');
  t_EffectiveDate  date;
  t_ExpirationDate date;
  t_MaxRainDate    date;
  t_MaxEventDate   date;
  t_sqlerrm        varchar2(4000);
  t_ReturnDate     date;
  t_DupIsRenewDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'dup_IsRenewable');
  t_IsRenewable    varchar2(2);
  
  /*All Functions Used to return and set Expiration Date*/
  
  Procedure SetValue(a_ObjectId number, a_ColumnName varchar2, a_Value date) is
    t_ObjectDefId number := api.pkg_columnquery.value(a_ObjectId, 'ObjectDefId');
    t_ColumndefId number := api.pkg_configquery.ColumnDefIdForName(t_ObjectdefId, a_ColumnName);
    t_FormattedDate varchar2(400);
    begin
      if a_Value is not null then
        --format the date value properly. ex. 1950-11-15 00:00:00
        t_FormattedDate := to_char(a_value, 'YYYY-MM-DD HH:MM:SS');
        delete from possedata.objectcolumndata cd
         where cd.ObjectId = a_ObjectId
           and cd.ColumnDefId = t_ColumnDefId;
        insert into possedata.objectcolumndata_t cd
        (
        logicaltransactionid ,
        objectid ,
        columndefid ,
        attributevalue ,
        searchvalue 
        )
        values
        (
        api.pkg_logicaltransactionupdate.CurrentTransaction,
        a_ObjectId,
        t_ColumndefId,
        t_FormattedDate,
        t_FormattedDate
        );
--        dbms_output.put_line('Set ' || a_ColumnName || ' on Permit# ' || api.pkg_columnquery.value(a_ObjectId, 'PermitNumber') || ' to ' || t_FormattedDate);
      end if;
  end;
  
  Procedure SetValue(a_ObjectId number, a_ColumnName varchar2, a_Value varchar2) is
    t_ObjectDefId number := api.pkg_columnquery.value(a_ObjectId, 'ObjectDefId');
    t_ColumndefId number := api.pkg_configquery.ColumnDefIdForName(t_ObjectdefId, a_ColumnName);
    begin
      if a_Value is not null then
        delete from possedata.objectcolumndata cd
         where cd.ObjectId = a_ObjectId
           and cd.ColumnDefId = t_ColumnDefId;
        insert into possedata.objectcolumndata_t cd
        (
        logicaltransactionid ,
        objectid ,
        columndefid ,
        attributevalue ,
        searchvalue 
        )
        values
        (
        api.pkg_logicaltransactionupdate.CurrentTransaction,
        a_ObjectId,
        t_ColumndefId,
        a_Value,
        lower(a_Value)
        );
--        dbms_output.put_line('Set ' || a_ColumnName || ' on Permit# ' || api.pkg_columnquery.value(a_ObjectId, 'PermitNumber') || ' to ' || a_Value);
      end if;
  end;

  /*Add Days given days*/
  function Add_Days(a_PermitID number,
                    a_BeginDate date,
                    a_Days number) return date is
                    t_Newdate date;
    begin                
    t_NewDate := trunc(a_BeginDate) + a_Days;
    
    SetValue(a_PermitId, 'Expirationdate', t_NewDate);

    begin
    
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
    exception when others then
      t_sqlerrm := sqlerrm;
    rollback;
   --Log ExpirationDateFailed
      update possedba.PermitDateFix_t t
        set t.ErrorMessage = t_sqlerrm
           ,t.ErroredUpdate = 'Y'
       where t.permitid = a_PermitID; 
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();
    end;
    
    return t_NewDate;
  
  end;
  
  /*Add Months given days*/
  function Add_Mons(a_PermitID number,
                    a_BeginDate date,
                    a_Months number)
                    return date is
                    t_NewDate date;
    begin                
    t_NewDate := add_months(a_BeginDate, a_Months);
    
    SetValue(a_PermitId, 'Expirationdate', t_NewDate);

    begin
    
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
    exception when others then
      t_sqlerrm := sqlerrm;
      
    rollback;
   --Log ExpirationDateFailed
      update possedba.PermitDateFix_t t
        set t.ErrorMessage = t_sqlerrm
           ,t.ErroredUpdate = 'Y'
       where t.permitid = a_PermitID; 
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();
    end;
    
    return t_NewDate;
  
  end;
  
  /*Add Years given date*/
  function Add_Years(a_PermitID  number,
                     a_BeginDate date,
                     a_Years     number)
                     return date is                    
                    t_NewDate date;
    begin                 
    t_NewDate := add_months( a_BeginDate, (12*a_Years) )-1;
    
    SetValue(a_PermitId, 'Expirationdate', t_NewDate);
    
    begin
    
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
    exception when others then
      t_sqlerrm := sqlerrm;
    rollback;
   --Log ExpirationDateFailed
      update possedba.PermitDateFix_t t
        set t.ErrorMessage = t_sqlerrm
           ,t.ErroredUpdate = 'Y'
       where t.permitid = a_PermitID; 
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();
    end;
    
    return t_NewDate;  
  
  end;
  
  /*Same as Related License*/
  function RelatedLicense(a_PermitID  number)
                          return date is                    
                          t_NewDate date;
    begin                 
    
    t_NewDate := api.pkg_columnquery.datevalue(api.pkg_columnquery.value(a_PermitId, 'LicenseObjectId'), 'Expirationdate');
    
    SetValue(a_PermitId, 'Expirationdate', t_NewDate);
    
    begin
    
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
    exception when others then
      t_sqlerrm := sqlerrm;
    rollback;
   --Log ExpirationDateFailed
      update possedba.PermitDateFix_t t
        set t.ErrorMessage = t_sqlerrm
           ,t.ErroredUpdate = 'Y'
       where t.permitid = a_PermitID; 
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();
    end;
    
    return t_NewDate;  
  
  end;
  
  /*End of Month given month*/
  function EndOfMonth(a_PermitID number,
                      a_IssueDate date,
                      a_StartMonth varchar2)
                      return date is
                      t_NewDate date;
    begin                  
    if a_IssueDate is not null then
      -- Set the New Date to the Last day of the Month, and change the year to be the same as the issued date
      t_NewDate := Last_Day(to_date(a_StartMonth, 'Month'));
      t_NewDate := to_date(to_char(Last_Day(to_date(a_StartMonth, 'Month')), 'MM/DD/') || to_char(to_date(a_IssueDate), 'YYYY'), 'MM/DD/YYYY');
      --Last Day Returns Current Year if the Month has already passed add a year
      if t_NewDate < a_IssueDate then
      
        t_NewDate := add_months(t_NewDate,12);
      
      end if;
      
      SetValue(a_PermitId, 'Expirationdate', t_NewDate);
      
      begin
      
      api.pkg_logicaltransactionupdate.EndTransaction;
      commit;
      exception when others then
        t_sqlerrm := sqlerrm;
      rollback;
     --Log ExpirationDateFailed
        update possedba.PermitDateFix_t t
          set t.ErrorMessage = t_sqlerrm
             ,t.ErroredUpdate = 'Y'
         where t.permitid = a_PermitID; 
      commit;
      api.pkg_logicaltransactionupdate.ResetTransaction();
      end;
    end if;
    return t_NewDate; 
    
  end; 
  
  /*End of current month*/
  function EndOfCurrMonth(a_PermitID number,
                          a_IssueDate date)
                          return date is
                          t_NewDate date;
    begin                  
    -- Set the New Date to the Last day of the Month, and change the year to be the same as the issued date
    t_NewDate := trunc(Last_Day(a_Issuedate));
    
    SetValue(a_PermitId, 'Expirationdate', t_NewDate);
    
    begin
    
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
    exception when others then
      t_sqlerrm := sqlerrm;
    rollback;
   --Log ExpirationDateFailed
      update possedba.PermitDateFix_t t
        set t.ErrorMessage = t_sqlerrm
           ,t.ErroredUpdate = 'Y'
       where t.permitid = a_PermitID; 
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();
    end;
    
    return t_NewDate;  
  
  end;
  
   /*Max Event Date given PermitID*/
  function MaxEventDate(a_PermitID number) return date is
    begin
        --Grab Max Rain Date
        select max(api.pkg_columnquery.DateValue(r1.RainDateObjectId,'StartDate'))
          into t_MaxRainDate
          from query.r_ABC_PermitRainDate r1
         where r1.PermitObjectId = a_PermitID;
        
       --Grab Max Event Date 
         select max(api.pkg_columnquery.DateValue(r1.EventDateObjectId,'StartDate'))
          into t_MaxEventDate
          from query.r_ABC_PermitEventDate r1
         where r1.PermitObjectId = a_PermitID;
        
        if t_MaxRainDate is not null and t_MaxRainDate > nvl(t_MaxEventDate, to_date(0001, 'YYYY')) then
        --Set Expiration Date to Rain Date
        SetValue(a_PermitID,'ExpirationDate',t_MaxRainDate+1);  
        
        elsif t_MaxEventDate is not null and t_MaxEventDate > nvl(t_MaxRainDate, to_date(0001, 'YYYY')) then
         --Set Expiration Date to Rain Date
        SetValue(a_PermitID,'ExpirationDate',t_MaxEventDate+1);  
        else
          --Log No Event Date
          update possedba.PermitDateFix_t t
            set t.ErrorMessage = 'No Event Date Data'
               ,t.ErroredUpdate = 'Y'
           where t.permitid = a_PermitID;    
        end if;
    begin
    
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
    exception when others then
      t_sqlerrm := sqlerrm;
    rollback;
   --Log ExpirationDateFailed
      update possedba.PermitDateFix_t t
        set t.ErrorMessage = t_sqlerrm
           ,t.ErroredUpdate = 'Y'
       where t.permitid = a_PermitID; 
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();
    end;
    return nvl(t_MaxRainDate, t_MaxEventDate);
  end;
  
begin
  -- Loop through permits and set dates

api.pkg_logicaltransactionupdate.Resettransaction();
 
  for c in (select *
              from possedba.permitdatefixr2_t p
             where p.Processed = 'N') loop
    if c.issuedate is not null then
      if c.PermitTypeId = t_AE then --Administrator/Executor

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := add_years(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_AI then --Ad Interim 

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Mons(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_APROS then --Out-Of-State Winery Salesroom

        t_EffectiveDate := to_date('6/30/2015', 'MM/DD/YYYY');--c.IssueDate;
        if api.pkg_columnquery.value(c.permitid, 'IssueDate') is null then
          SetValue(c.permitid,'IssueDate',t_EffectiveDate);
        end if;
        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'June');

      end if;


      if c.PermitTypeId = t_APRV then --Plenary Winery Salesroom 

        t_EffectiveDate := to_date('6/30/2015', 'MM/DD/YYYY');--c.IssueDate;
        if api.pkg_columnquery.value(c.permitid, 'IssueDate') is null then
          SetValue(c.permitid,'IssueDate',t_EffectiveDate);
        end if;
        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'June');

      end if;


      if c.PermitTypeId = t_APRVL then --Farm Winery Salesroom

        t_EffectiveDate := to_date('6/30/2015', 'MM/DD/YYYY');--c.IssueDate;
        if api.pkg_columnquery.value(c.permitid, 'IssueDate') is null then
          SetValue(c.permitid,'IssueDate',t_EffectiveDate);
        end if;
        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'June');

      end if;


      if c.PermitTypeId = t_AU then --Auction 

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Years(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_BE then --Blanket Employment

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'March');

      end if;


      if c.PermitTypeId = t_BRW then --Home Brewer

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Years(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_BSP then --Bulk Sale 

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Mons(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_CO then --Close Out 

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_days(c.PermitId, t_EffectiveDate, 30);

      end if;


      if c.PermitTypeId = t_CT then --Catering 

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := MaxEventDate(c.PermitId);

      end if;


      if c.PermitTypeId = t_CTS then --Consumer Tasting - Suppliers

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'June');

      end if;


      if c.PermitTypeId = t_CTW then --Consumer Tasting - Wholesalers

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'June');

      end if;


      if c.PermitTypeId = t_EMP then --Minor Employment

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'March');

      end if;


      if c.PermitTypeId = t_EP then --Extension of  Premises

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Years(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_EP14 then --Special Event

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Years(c.PermitId, t_EffectiveDate, 2);

      end if;


      if c.PermitTypeId = t_FEST then --Festival

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := MaxEventdate(c.PermitId);

      end if;


      if c.PermitTypeId = t_FP then --Food and Pharmaceutical

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'June');

      end if;


      if c.PermitTypeId = t_LTC then --Limited Transportation Insignia

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'September');

      end if;


      if c.PermitTypeId = t_LTP then --Limited Transportation

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'September');

      end if;
      

      if c.PermitTypeId = t_MA2 then --Marketing Agent ID Card

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'June');

      end if;


      if c.PermitTypeId = t_MS then --Merchandising Show

        select min(api.pkg_columnquery.DateValue(r1.EventDateObjectId,'StartDate'))
          into t_EffectiveDate
          from query.r_ABC_PermitEventDate r1
         where r1.PermitObjectId = c.PermitId;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := MaxEventDate(c.PermitId);

      end if;


      if c.PermitTypeId = t_OPS then --Off Premise Storage of Records

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'June');

      end if;


      if c.PermitTypeId = t_PC then --Import for Personal Consumption

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Years(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_PI then --Product Introduction

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);
        SetValue(c.permitid,'IssueDate',t_EffectiveDate);

        t_ReturnDate := RelatedLicense(c.PermitId);

      end if;


      if c.PermitTypeId = t_REHAB then --Rehabilitation

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'December');

      end if;


      if c.PermitTypeId = t_RR then --Retailer to Retailer

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfCurrMonth(c.PermitId, t_EffectiveDate);

      end if;


      if c.PermitTypeId = t_SA then --Social Affair

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := MaxEventDate(c.PermitId);

      end if;


      if c.PermitTypeId = t_SOL then --Solicitor

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'August');

      end if;


      if c.PermitTypeId = t_SP then --Sampling or Display 

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Years(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_TAP then --Temporary Authorization to Operate

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Mons(c.PermitId, t_EffectiveDate, 3);

      end if;


      if c.PermitTypeId = t_TE then --Temporary Storage

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Mons(c.PermitId, t_EffectiveDate, 3);

      end if;


      if c.PermitTypeId = t_TI then --Transit Insignia

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := EndOfMonth(c.PermitId, t_EffectiveDate, 'August');

      end if;


      if c.PermitTypeId = t_TLI then --Transportation License Insignia

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := RelatedLicense(c.PermitId);

      end if;


      if c.PermitTypeId = t_WF then --Wine Festival

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := MaxEventDate(c.PermitId);

      end if;


      if c.PermitTypeId = t_WN then --Home Wine Making

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := Add_Years(c.PermitId, t_EffectiveDate, 1);

      end if;


      if c.PermitTypeId = t_WS then --Wine Seminar

        t_EffectiveDate := c.IssueDate;

        SetValue(c.permitid,'EffectiveDate',t_EffectiveDate);

        t_ReturnDate := MaxEventDate(c.PermitId);

      end if;
      --Set dup_Isrenewable
      t_IsRenewable := api.pkg_columnquery.value(c.permitid, 'IsRenewable');
      delete from possedata.objectcolumndata cd
       where cd.ObjectId = c.permitid
         and cd.ColumnDefId = t_DupIsRenewDefId;
      insert into possedata.objectcolumndata cd
      (
      logicaltransactionid ,
      objectid ,
      columndefid ,
      attributevalue ,
      searchvalue 
      )
      values
      (
      api.pkg_logicaltransactionupdate.CurrentTransaction,
      c.permitid,
      t_DupIsRenewDefId,
      t_IsRenewable,
      lower(t_IsRenewable)
      );
    elsif c.issuedate is null then
      if c.permitnumber in ('13005269','13005674','14000246','14002802','14006764','14007759','15009424','1003','3744','1004864','2002149','2002674','2006093','2006428',
                            '2007360','3000533','3001068','3001318','3001442','3001677','3003099','3003519','3007638','3008749','4001217','4001223','4001360','4002801',
                            '4005656','4005970','5000525','6008396','6008417','7003877','7007790','7008214','7008270','7008535','7008930','7009186','8000638','8001196',
                            '8002135','8002219','8002637','8007566','8007567','8010035','9000809','9001489','9002783','9002858','9003286','9003413','9004054','9008135',
                            '9009298','10000381','10004637','11007867','11009989','12000488','94003322','95003867','96000924','96001086','96004029','96005466',
                            '96006833','96007622','96007907','96007920','97001250','98000739','98002090','98006511','99000156') then
        SetValue(c.permitid, 'State', 'Expired');
      elsif c.permitnumber is not null then
        dbms_output.put_line(c.permitnumber || ' has no provided IssueDate');
      end if; --permit number in list
    end if; --Issue date not null
    update possedba.permitdatefixr2_t t
       set t.processed = 'Y'
     where t.permitid = c.permitid;
  end loop;
  api.pkg_logicaltransactionupdate.Endtransaction();
  commit;
end;
/
select p.permitnumber, p.permittype, t.PermitTypeCode, p.IssueDate, p.ExpirationDate, p.EffectiveDate, p.CalculatedExpirationDate, t.issuedate, t.state, p.state, t.processed, t.permitid
  from possedba.Permitdatefixr2_t t
  join query.o_abc_permit p on p.ObjectId = t.permitid --23743395
 where t.state = 'Active'
