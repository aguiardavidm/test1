declare
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  t_IdList udt_IdList;
  t_ProcessId                           number;
  t_ChangeProcessDefId                  number
    := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus');
  t_ProcessDefId                number
    := api.pkg_configquery.ObjectDefIdForName('p_ABC_SendConfirmationLetter');
  t_NoteDefId                           pls_integer;
  t_NoteId                              pls_integer;
  t_NoteTag                             varchar2(6) := 'GEN';
  t_NoteText                            varchar2(4000);
begin
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.Pkg_Logicaltransactionupdate.ResetTransaction();

  select j.jobid
  bulk collect into t_IdList
   from api.jobs j 
   join api.jobtypes jt on jt.jobtypeid = j.jobtypeid
  where jt.name = 'j_ABC_PRApplication'
    and not exists 
      (select 1 
        from api.processes p
       where p.jobid = j.jobid
         and p.ProcessTypeId = t_ProcessDefId)
    and j.jobstatus not in ('NEW', 'CANCEL', 'REVIEW', 'REJECT')
and j.jobid in ('59246057','64094758');

  -- Create Note and Populate Text
  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;

  for i in 1..t_IdList.count loop
    dbms_output.put_Line(t_IdList(i));

    -- Create the process
    t_ProcessId := api.pkg_processupdate.New(t_IdList(i), t_ProcessDefId, null, sysdate, sysdate, sysdate);

    t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
          'System recreated PR Send Confirmation Letter process. See issue 56998 for further information.';
    t_NoteText := t_NoteText;
    --t_NoteId := api.pkg_NoteUpdate.New(t_IdList(i), t_NoteDefId, 'Y', t_NoteText);--No Note this time.
  end loop;

  api.Pkg_Logicaltransactionupdate.EndTransaction();
  --commit; --Never commit these scripts, instead validate the output then commit in PL/SQL Developer.
end;
