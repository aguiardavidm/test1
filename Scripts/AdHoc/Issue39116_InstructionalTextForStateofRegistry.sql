/*-----------------------------------------------------------------------------
 * Expected run time:
 * Purpose: Update System setting Text for State of Registry
 * THIS SCRIPT IS NOT RERUNABLE. 
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_SysObjId                            udt_Id;
  t_GeneralText                         varchar2(4000)
    := 'Please list all vehicles to be used in the transportation of alcoholic beverages. 
If your Country or State/Province is not available, please select other and enter your country and State/Province in the fields provided.';
  t_VehicleRenewalInstructions          varchar2(4000);
  
  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  select objectid, VehicleRenewalInstructions
  into t_SysobjId, t_VehicleRenewalInstructions
  from query.o_Systemsettings;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();

  api.pkg_columnUpdate.setvalue(t_SysObjId, 'CountryOfIncorporationText', 'Please enter your Country and State/Province of incorporation. If you country or State/Province is not available, please select "Other" and enter your Country and State/Province in the fields provided.');
  api.pkg_columnUpdate.setvalue(t_SysObjId, 'PermitApplicationCORText', t_GeneralText);
  api.pkg_columnUpdate.setvalue(t_SysObjId, 'LicenseApplicationCORText', t_GeneralText);
  api.pkg_columnUpdate.setvalue(t_SysObjId, 'LicenseRenewalCORText', t_GeneralText);
  t_VehicleRenewalInstructions := t_VehicleRenewalInstructions ||
'<br/>ALL OF THE FOLLOWING INFORMATION/DATA MUST BE COMPLETED IN FULL TO PROCEED WITH YOUR RENEWAL.<br/>
APPLICATIONS THAT HAVE ANY MISSING DATA WILL BE REJECTED BY THE SYSTEM.<br/>
PLEASE COMPLETE ALL OF THE BLANK BOXES BELOW.<br/>' ||
t_GeneralText;
  api.pkg_columnUpdate.setvalue(t_SysObjId, 'VehicleRenewalInstructions', t_VehicleRenewalInstructions);

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
/
/*
select CountryOfIncorporationText, PermitApplicationCORText, LicenseApplicationCORText, LicenseRenewalCORText, VehicleRenewalInstructions
from query.o_Systemsettings;
*/
