-- Created on 06/26/2017 by MICHEL.SCHEFFERS 
-- Issue 31491 - New jobs more than 6 months old
declare 
  -- Local variables here
  t_ProcessId      number;
  t_ProcessDefId   number := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus'); -- DefId for Process to create
  t_Outcome        varchar2(100):= 'Cancelled'; -- Outcome
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(null);
  for j in (select *
            from   possedba.issue31491_newjobs
            order  by JobNumber
           ) loop
     if api.pkg_columnquery.value (j.Jobid, 'StatusName') = 'NEW' then
        dbms_output.put_line ('Cancelling job ' || j.jobnumber);
        t_ProcessId := api.pkg_processupdate.New (j.JobId, t_ProcessDefId, 'Change Status',sysdate,sysdate,sysdate);
        api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange', 'Cancelled per Issue 31491.'); 
        api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);  
        update possedba.issue31491_newjobs p
        set    p.canceldate = trunc(sysdate)
        where  p.jobid = j.jobid;
     else
        dbms_output.put_line ('Job ' || j.jobnumber || '-' || api.pkg_columnquery.value (j.JobId, 'StatusName'));
     end if;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;

46945824