  -- Update Event Location Description for Permits with Additional Privilege
  -- 10/28/2015
declare 
  t_Permits       number (9) := 0;
  t_ColumnDefId   number     := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'EventLocAddressDescription');
  t_Dummy         number;
begin

  DBMS_OUTPUT.ENABLE(100000000);
  api.pkg_logicaltransactionupdate.ResetTransaction();

  -- Find Permits with Additional Privileges in conversion table.
  for r in (select ap.location, ap.cnty_muni_cd || '-' || ap.lic_type_cd || '-' || ap.muni_ser_num || '-' || ap.gen_num || '-' || ap.priv_seq_num as appermit, p.objectid
              from abc11p.add_priv_mstr ap
              join dataconv.o_abc_permit p on p.legacykey = ap.cnty_muni_cd || '-' || ap.lic_type_cd || '-' || ap.muni_ser_num || '-' || ap.gen_num || '-' || ap.priv_seq_num 
           ) 
  loop  
     t_Permits := t_permits + 1;
     dbms_output.put_line ('Updating location for Permit ' || r.ObjectId || '-' || r.appermit);  
     begin   
        select 1
          into t_Dummy 
	      from possedata.objectcolumndata_t cd
	     where cd.objectid = r.objectid
	       and cd.columndefid = t_ColumnDefId;
	 exception
	    when no_data_found then
  -- Update the Event Location Description
           insert into possedata.objectcolumndata_t cd
                       (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue)
                       Values (api.pkg_logicaltransactionupdate.CurrentTransaction, r.objectid, t_columndefid,r.Location);
     end;             
  end loop;
  dbms_output.put_line ('Number of Permits: ' || t_Permits);
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;