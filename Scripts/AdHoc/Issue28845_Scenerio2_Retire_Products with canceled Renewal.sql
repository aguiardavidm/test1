-- Created on 03/23/2017 by Dennis.OConnor 
-- Retire products with expirationdate of 2015 and are not part of any

-- Create table
--drop table possedba.LOG_29212_SCENARIO2;
create table possedba.LOG_29212_Scenario2
(
  objectId        NUMBER,
  productid       VARCHAR2(20),
  expiration_date DATE,
  current_state   VARCHAR2(20),
  modified_state  VARCHAR2(20)
);

declare 
  current_product_state VARCHAR2(20);

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(NULL);

  for p in (SELECT p.ObjectId
            FROM query.j_abc_prrenewal pr
            JOIN query.r_abc_productrenewaltoproduct prp on pr.ObjectId = prp.ProductRenewalId
            JOIN query.o_abc_product p on p.ObjectId = prp.ProductId
            WHERE p.ExpirationDate = to_date('12/31/2015', 'mm/dd/yyyy')
            AND p.State = 'Current'
            AND pr.JobStatus = 'CANCEL') loop

     current_product_state := api.pkg_columnquery.value(p.objectid, 'State');

     api.pkg_columnupdate.SetValue (p.objectid, 'State', 'Historical');
     api.pkg_columnupdate.SetValue(p.objectid,'DirectExpire','Y');
     dbms_output.put_line ('Updating State for ' || api.pkg_columnquery.Value (p.objectid,'RegistrationNumber') || '-' || api.pkg_columnquery.Value (p.objectid,'Name'));

     insert into possedba.LOG_29212_SCENARIO2
      (objectId, productid, expiration_date, current_state, modified_state)
      values
      (p.objectid,
       api.pkg_columnquery.value(p.objectid, 'RegistrationNumber'),
       api.pkg_columnquery.DateValue(p.objectid, 'ExpirationDate'),
       current_product_state,
       api.pkg_columnquery.value(p.objectid, 'State') 
      );
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
 -- commit;  
end;