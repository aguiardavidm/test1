-- Created on 5/27/2020 by PAUL.ORSTAD 
-- Est runtime <5 minutes
declare 
  t_Count               number := 0;
  t_TransactionId       number;
  t_NoteId              number;
  t_NoteDefId           number;
begin
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20002, 'Must be run as POSSEDBA');
  end if;
  select nd.NoteDefId
    into t_NoteDefId
    from api.notedefs nd
   where nd.Tag = 'GEN';
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  dbms_output.put_line('Fees Adjusted on Jobs:');
  for i in (
      select
          j.jobid,
          f.feeid,
          f.Amount + f.AdjustedAmount + f.PaidAmount Balance
        from api.statuses s
        join api.jobstatuses js
            on js.StatusId = s.statusid
        join api.jobs j
            on j.JobTypeId = js.JobTypeId
        join api.fees f
            on f.JobId = j.jobid
       where s.Description = 'Cancelled'
         and j.StatusId = s.StatusId
         and f.amount + f.PaidAmount + f.adjustedamount > 0
         and f.ResponsibleObjectId is not null
         and f.SystemGenerated = 'Y'
         and f.PostedDate is null
     ) loop
     t_TransactionId := api.pkg_FeeUpdate.Adjust(i.FeeId, -i.Balance, 'Adjusted by system due to cancellation of job', sysdate);
     dbms_output.put_line(i.jobid);
     t_NoteId := api.pkg_NoteUpdate.New(i.jobid, t_NoteDefId,'N', to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) || 'System adjusted fees for issue 54054');
     t_count := t_count+1;
   end loop;
   dbms_output.put_line('Jobs Updated: ' || t_Count);
   api.pkg_LogicalTransactionUpdate.EndTransaction;
--   commit;
--   rollback;
end;
