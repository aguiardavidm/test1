-- Created on 7/22/2016 by MICHEL.SCHEFFERS 
-- Apply Instance Security on Petition Online Documents
-- Issue 8293
declare 
  -- Local variables here
  t_Jobs integer := 0;

begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all Petition Online Documents
  for i in (select ped.OLDocumentId
              from query.r_OLPetitionElectronicDocument ped
            ) loop
    -- Apply Instance Security for each found document
    t_Jobs := t_Jobs + 1;
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.Oldocumentid, sysdate);
  end loop;
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbms_output.put_line('Petition Documents updated: ' || t_Jobs);
end;
