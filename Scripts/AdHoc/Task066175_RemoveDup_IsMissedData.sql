-- Created on 04/15/2019 by JAE.HOLDERBY
/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: Remove dup_IsMissedCPL data from CPLSubmission Jobs
 *---------------------------------------------------------------------------*/
declare
  -- Local variables here
  t_CPLSubmissionsIdList     api.Pkg_Definition.udt_IdList;
  t_Counter                  integer := 0;

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select c.ObjectId
  bulk collect into t_CPLSubmissionsIdList
  from query.j_abc_cplsubmissions c
  where c.dup_IsMissedSubmission is not null;

  --Delete dup_IsMissedCPL from CPLSubmission Jobs
  for i in 1.. t_CPLSubmissionsIdList.count loop
    api.pkg_columnupdate.RemoveValue(t_CPLSubmissionsIdList(i), 'dup_IsMissedSubmission');
    t_Counter := t_Counter +1;
    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;
  dbms_output.put_line(t_Counter);
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
