-- Created on 05/18/2016 by MICHEL.SCHEFFERS 
-- Issue 8575 - OPS: Town transferred license without authority to transfer (0232-33-023)
declare 
  -- Local variables here
  t_ProcessId     number;
  t_ProcessDefId  number := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus'); -- DefId for Process to create
  t_Outcome       varchar2(100):= 'Cancelled'; -- Outcome
  t_NoteDefId     number;
  t_LicenseId_010 number := 31291453; -- ObjectId for closed License 0232-33-023-010
  t_LicenseId_011 number := 35062028; -- ObjectId for active License 0232-33-023-011
  t_JobId_53380   number := 35061328; -- JobId for job 53380
  t_JobId_53397   number := 35062022; -- JobId for job 53397
  t_JobId_68477   number := 36269039; -- JobId for job 68477
  n               number;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select n.NoteDefId
    into t_NoteDefId
    from api.notedefs n
   where n.Tag = 'GEN';

-- Cancel and create notes for jobs 53380 and 53397  
  dbms_output.put_line('Cancelling job 53380.');
  t_ProcessId := api.pkg_processupdate.New (t_JobId_53380, t_ProcessDefId, 'Change Status',sysdate,sysdate,sysdate);
  api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange', 'Cancelled per Issue 8575.'); 
  api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);  
  dbms_output.put_line('Creating note for 53380.');
  n := api.pkg_noteupdate.New(t_JobId_53380,
                              t_NoteDefId,
                              'N',
                              'Job cancelled per Issue 8575.');
                              
  dbms_output.put_line('Cancelling job 53397.');
  t_ProcessId := api.pkg_processupdate.New (t_JobId_53397, t_ProcessDefId, 'Change Status',sysdate,sysdate,sysdate);
  api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange', 'Cancelled per Issue 8575.'); 
  api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);  
  dbms_output.put_line('Creating note for 53397.');
  n := api.pkg_noteupdate.New(t_JobId_53397,
                              t_NoteDefId,
                              'N',
                              'Job cancelled per Issue 8575.');
  
-- Create note for job 68477
  dbms_output.put_line('Creating note for 68477');
  n := api.pkg_noteupdate.New(t_JobId_68477,
                              t_NoteDefId,
                              'N',
                              'Job cancelled per Issue 8575.');

-- Activate license 0232-33-023-010   
  dbms_output.put_line('Activating license 0232-33-023-010 (' || t_LicenseId_010 || ').');
  api.pkg_columnupdate.SetValue(t_LicenseId_010, 'State', 'Active');
  api.pkg_columnupdate.SetValue(t_LicenseId_010, 'IsLatestVersion', 'Y');

-- Close license 0232-33-023-011
  dbms_output.put_line('Closing license 0232-33-023-011 (' || t_LicenseId_011 || ').');
  api.pkg_columnupdate.SetValue(t_LicenseId_011, 'State', 'Closed');
  api.pkg_columnupdate.SetValue(t_LicenseId_011, 'IsLatestVersion', 'N');

-- No Products or Permits are related to -011, so no further action is needed.  

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
