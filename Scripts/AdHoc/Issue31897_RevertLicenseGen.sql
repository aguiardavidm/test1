-- Created on 9/13/2017 by Paul ORstad
-- Issue 31897 - License 0414-33-001-013
declare 
  -- Local variables here
  t_LicenseObjectId     integer;
  t_LicenseNumber       varchar2(25) := '0414-33-001-013';
  t_NewLicenseNumber    varchar2(25) := '0414-33-001-012';
  t_EndpointId          integer := api.pkg_configquery.EndPointIdForName('o_ABC_License' , 'LicenseType');
  t_RelId               integer;
  t_ReportId            integer;
  t_ProcessId           integer;
  t_JobId               integer;
  t_AppSummDefId        number := api.pkg_configquery.ObjectDefIdForName('p_ABC_GenerateAppSummaryReport'); -- DefId for Process
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;

-- Collect various objects  
  select l.ObjectId
  into   t_LicenseObjectId
  from   query.o_abc_license l
  where  l.LicenseNumber = t_LicenseNumber
  and    l.State = 'Active';
  
  -- Set new License Number
    api.pkg_columnupdate.SetValue(t_LicenseObjectId, 'LicenseNumber', t_NewLicenseNumber);
  
  -- Set License number on pending licenses
  for i in (select l.ObjectId
              from   query.o_abc_license l
              where  l.LicenseNumber = t_LicenseNumber
              and    l.State = 'Pending') loop
  
  -- Set new License Number
    api.pkg_columnupdate.SetValue(i.objectid, 'LicenseNumber', t_NewLicenseNumber);
  end loop;
  
  
-- Collect License Certificate info
  select r1.RelationshipId, sl.ProcessId
  into   t_RelId, t_ProcessId
  from   query.r_abc_amendappjoblicense r
  join   query.p_abc_sendlicense sl on sl.JobId = r.AmendAppJobId
  join   query.r_SendLicense_Certificate r1 on r1.ProcessId = sl.ProcessId
  where  r.LicenseObjectId = t_LicenseObjectId;
  
  
-- Sever relationship from License to Old License Certificate and create from License to New License Certificate
  delete from rel.StoredRelationships
  where  RelationshipId = t_RelId;
  delete from objmodelphys.Objects
  where  ObjectId = t_RelId;
  t_ReportId := extension.pkg_processserver.ScheduleRTFReport('LicenseCertificate', t_ProcessId, t_ProcessId);
  
-- Collect Application Summary Report info from License
  select r2.RelationshipId, r.AmendAppJobId
  into   t_RelId, t_JobId
  from   query.r_abc_amendappjoblicense r
  join   query.r_Amendappsummary r2 on r2.JobId = r.AmendAppJobId
  where  r.LicenseObjectId = t_LicenseObjectId;
-- Sever relationship from License to Application Summary Report
  delete from rel.StoredRelationships
  where  RelationshipId = t_RelId;
  delete from objmodelphys.Objects
  where  ObjectId = t_RelId;
  
-- Collect Application Summary Report info from Job
  select r.RelationshipId
  into   t_RelId
  from   api.processes p
  join   query.r_RenwalAppSummary r on r.ProcessId = p.ProcessId
  where  p.ProcessTypeId = t_AppSummDefId
  and    p.JobId = t_JobId;
-- Sever relationship from Job to Application Summary Report
  delete from rel.StoredRelationships
  where  RelationshipId = t_RelId;
  delete from objmodelphys.Objects
  where  ObjectId = t_RelId;
-- Create new Application Summary Report
  toolbox.pkg_workflow.CreateProcessesFromCheckBoxes(t_JobId,
                                                     sysdate,
                                                     'VALUE:Y<Complete,,,,Today>p_ABC_GenerateAppSummaryReport',
                                                     'JOB');
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;  
end;