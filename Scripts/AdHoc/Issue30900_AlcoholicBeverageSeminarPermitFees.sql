-- Created on 7/12/2017 by MICHEL.SCHEFFERS
-- Issue 30900 - New G/L Account ABS for ALCOHOLIC BEVERAGE SEMINAR
--               Update Fee Schedule to reflect new Permit Type Name and GL Account
declare 
  -- Local variables here
  t_FeeDefinitionId integer;

begin
    api.pkg_logicaltransactionupdate.ResetTransaction;
    
    -- Update Fee Schedule Entry
    select fd.ObjectId
    into   t_FeeDefinitionId
    from   query.o_feedefinition fd
    where  fd.GLAccountNumber = 'WS';
    api.pkg_columnupdate.SetValue(t_FeeDefinitionId, 'Description', 'Alcoholic Beverage Seminar Application Fee');
    api.pkg_columnupdate.SetValue(t_FeeDefinitionId, 'FeeDescription', 'Alcoholic Beverage Seminar Application Fee');
    api.pkg_columnupdate.SetValue(t_FeeDefinitionId, 'GLAccountNumber', 'ABS');
    api.pkg_columnupdate.SetValue(t_FeeDefinitionId, 'Condition', '=nvl({OnlinePermitTypeCode},{PermitTypeCode})=''ABS''');

    api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;