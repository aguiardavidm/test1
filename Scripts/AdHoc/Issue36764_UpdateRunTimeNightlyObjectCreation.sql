-- RUN AS POSSEDBA
-- Created on 06/07/2018
-- DESCRIPTION: This script updates the start time of the Nightly Object Creation job
declare 
  -- Local variables here
  t_JobId                               integer;
begin

  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
  end if;

  api.pkg_logicaltransactionupdate.EndTransaction;
  begin
    select j.JOB
    into t_JobId
    from dba_jobs j
    where j.what = 'Extension.pkg_NightlyObjectCreation.CreateObjects;';
    dbms_job.change(job => t_JobId, what => 'Extension.pkg_NightlyObjectCreation.CreateObjects;',
        next_date => trunc(sysdate) + 1 + 2/24, interval => 'trunc(sysdate) + 1 + 2/24');
  exception
    when no_data_found then
      null;
  end;

  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
  
end;
