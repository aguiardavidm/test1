-- Expire active permits where the expiration date plus grace period is less than todays date
declare 
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  t_IdList udt_IdList;
  t_NoteDefId                           pls_integer;
  t_NoteId                              pls_integer;
  t_NoteTag                             varchar2(6) := 'GEN';
  t_NoteText                            varchar2(4000);
begin
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  /*select p.Objectid
  bulk collect into t_IdList
  from query.o_abc_permit p
  join query.o_abc_permittype pt
    on p.PermitTypeObjectId = pt.ObjectId
  where p.state = 'Active'
  and (p.ExpirationDate + pt.GracePeriodDays) < sysdate
  and pt.AutoExpire = 'Y';*/
  
  select t.objectid
  bulk collect into t_IdList
	  from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_Permit', 'State', 'Active')) t
	  join query.o_abc_permittype pt
	      on api.pkg_ColumnQuery.value(t.objectid, 'PermitTypeObjectId') = pt.objectid
	 where api.pkg_columnquery.datevalue(t.objectid, 'ExpDatePlusGracePeriod') < sysdate
     and pt.AutoExpire = 'Y';
  
  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;
  
  api.Pkg_Logicaltransactionupdate.ResetTransaction();
  
  for i in 1..t_IdList.count loop 
    dbms_output.put_line(t_IdList(i) || ' - ' || api.pkg_columnquery.Value(t_IdList(I), 'PermitNumber'));
    api.pkg_ColumnUpdate.SetValue(t_IdList(i),'State', 'Expired');
    
    t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
          'Expired permit as it is set for auto expiration and is past its expiration date. See Issue #56714';
    t_NoteText := t_NoteText;
    t_NoteId := api.pkg_NoteUpdate.New(t_IdList(i), t_NoteDefId, 'Y', t_NoteText);
  end loop;
  
  api.Pkg_Logicaltransactionupdate.EndTransaction();
  --commit;
end;
