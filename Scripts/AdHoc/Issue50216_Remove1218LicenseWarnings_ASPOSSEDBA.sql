/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 minutes
 * Purpose: This script deletes the License Warnings created as a result of the
 *   12.18 Warning Batch script running on 07/30/2019 instead of 07/31/2019.
 *   This script outputs the License Numbers from which License Warnings were
 *   deleted.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_1218WarningBatchJobId               udt_Id;
  t_BatchToLicenseEndpointId            udt_Id
      := api.pkg_ConfigQuery.EndpointIdForName('j_ABC_1218WarningBatch', 'License');
  t_BatchToLicWarningEndpointId         udt_Id
      := api.pkg_ConfigQuery.EndpointIdForName('j_ABC_1218WarningBatch', 'LicenseWarning');
  t_LicenseNumbers                      udt_StringList;
  t_LicenseWarningIds                   udt_IdList;
  t_LicWarningToLicenseEndpointId       udt_Id
      := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_LicenseWarning', 'License');

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  dbug('---LOGGING---');
  select wb.ObjectId
  into t_1218WarningBatchJobId
  from query.j_ABC_1218WarningBatch wb
  where trunc(wb.CreatedDate) = trunc(sysdate);
  dbug('12.18 Warning Batch JobId: ' || t_1218WarningBatchJobId);

  select
    rr.FromObjectId, -- License Warning ObjectId
    l.LicenseNumber
  bulk collect into
    t_LicenseWarningIds,
    t_LicenseNumbers
  from
    api.relationships r -- from 12.18 Warning Batch Job to License Warning
    join api.relationships rr -- from License Warning to License
        on r.ToObjectId = rr.FromObjectId
    join query.o_ABC_License l
        on rr.ToObjectId = l.ObjectId
    join api.relationships rrr -- from 12.18 Warning Batch Job to License
        on r.FromObjectId = rrr.FromObjectId
  where r.FromObjectId = t_1218WarningBatchJobId
    and r.EndpointId = t_BatchToLicWarningEndpointId
    and rr.EndpointId = t_LicWarningToLicenseEndpointId
    and rr.ToObjectId = rrr.ToObjectId
    and rrr.EndpointId = t_BatchToLicenseEndpointId
  order by l.LicenseNumber;
  dbug('# of 12.18 Warnings Generated: ' || t_LicenseWarningIds.count());
  dbug('Affected License Numbers');

  for i in 1..t_LicenseWarningIds.count() loop
    dbug(t_LicenseNumbers(i));
    api.pkg_ObjectUpdate.Remove(t_LicenseWarningIds(i));
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  --commit;

end;

