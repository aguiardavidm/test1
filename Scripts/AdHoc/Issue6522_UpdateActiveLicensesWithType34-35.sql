-- Created on 03/02/2016 by MICHEL SCHEFFERS 
-- Issue 6522: Seasonal License Type changes
-- Update Licenses with License Types 34 and 35.
declare 
  -- Local variables here
  d                          number := 0;
  t_LicenseIds               api.pkg_definition.udt_IdList;
      
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select l.ObjectId
    bulk collect into t_LicenseIds
    from query.o_abc_license l
   where l.LicenseTypeCode in ('34', '35')
     and l.State = 'Active';
  
  for i in 1 .. t_LicenseIds.count loop
     dbms_output.put_line('Updating License ' || api.pkg_columnquery.Value (t_LicenseIds(i), 'LicenseNumber'));
     abc.pkg_ABC_License.PostVerifyWrapper(t_LicenseIds(i), trunc(sysdate));
     d := d + 1;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

  dbms_output.put_line('Licenses updated : ' || d);
end;
