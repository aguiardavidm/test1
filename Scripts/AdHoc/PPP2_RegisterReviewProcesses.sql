declare

begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select pt.ProcessTypeId
              from api.processtypes pt
              where pt.Name in ('p_ABC_PoliceReview',
                                'p_ABC_MunicipalityReview')
                and not exists (select 1
                                  from query.o_processtype q where q.ProcessTypeId = pt.ProcessTypeId)) loop
    api.pkg_objectupdate.RegisterExternalObject('o_ProcessType', i.processtypeid);
    dbms_output.put_line(i.processtypeid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;