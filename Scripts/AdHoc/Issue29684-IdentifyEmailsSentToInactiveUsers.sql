--drop   table possedba.InactiveUsers;
/*create table possedba.InactiveUsers
(
   ObjectId        number,
   Name            varchar2(1000),
   Email           varchar2(1000),
   Type            varchar2(40),
   Active          varchar2(01),
   SentDate        date
);
/*/
DECLARE
  I INTEGER;
  t_emails varchar2(4000) := ' ';
  TYPE T_ARRAY_OF_VARCHAR IS TABLE OF VARCHAR2(2000) INDEX BY BINARY_INTEGER;
  MY_ARRAY T_ARRAY_OF_VARCHAR;
  MY_STRING VARCHAR2(4000);
BEGIN
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for eml in (select e.ToAddress, e.SentDate
              from   extension.emails e
              where  trunc(e.SentDate) > trunc (sysdate) - 7
              order  by e.ToAddress
             ) loop
     if t_emails = eml.toaddress then
        continue;
     end if;
     t_emails := eml.toaddress;
     my_string := eml.toaddress;
     FOR CURRENT_ROW IN (
         with test as     
        (select MY_STRING from dual)
         select regexp_substr(MY_STRING, '[^,]+', 1, rownum) SPLIT
         from   test
         connect by level <= length (regexp_replace(MY_STRING, '[^,]+'))  + 1)
     LOOP
        for le1 in (select le.ObjectId, le.FormattedName Name, le.ContactEmailAddress Email, 'Legal Entity' UserType, le.Active
                    from   query.o_abc_legalentity le
                    where  le.ContactEmailAddress = CURRENT_ROW.SPLIT
                    and    le.ObjectDefTypeId = 1
                    union
                    select u.ObjectId, u.FormattedName Name, u.EmailAddress Email, u.UserType UserType, u.Active
                    from   query.u_users u
                    where  u.EmailAddress = CURRENT_ROW.SPLIT
                    and    u.ObjectDefTypeId = 24
                    union
                    select e.ObjectId, e.DoingBusinessAs Name, e.ContactEmailAddress Email, 'Establishment' UserType, e.Active
                    from   query.o_abc_establishment e
                    where  e.ContactEmailAddress = CURRENT_ROW.SPLIT
                    and    e.ObjectDefTypeId = 1
                    order  by 2
                   ) loop
           dbms_output.put_line ('User ' || le1.name || '(' || le1.usertype || ') with email ' || le1.email);
           if le1.active = 'N' then
              insert into possedba.InactiveUsers
                    (ObjectId, Name, Email, Type, Active, SentDate)
                 values
                    (le1.objectid, le1.name, le1.email, le1.usertype, le1.active, eml.sentdate);          
               dbms_output.put_line ('User ' || le1.name || '(' || le1.usertype || ') with email ' || le1.email || ' (sent on ' || eml.sentdate || ') is inactive.');
           end if;
        end loop; 
        MY_ARRAY(MY_ARRAY.COUNT) := CURRENT_ROW.SPLIT;
     END LOOP;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;

END;