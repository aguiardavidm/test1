-- Created on 10/14/2020 by PAUL.ORSTAD 
declare 
  t_AppealJobs  api.pkg_Definition.udt_IdList;
begin
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20000, 'Must be run as POSSEDBA');
  end if;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  select objectid
    bulk collect into t_AppealJobs
    from query.j_Abc_Appeal;
  for i in 1..t_AppealJobs.count loop
    abc.Pkg_Instancesecurity.ApplyInstanceSecurity(t_AppealJobs(i), sysdate);
  end loop;
  
  dbms_output.put_line('Applied security on ' || t_AppealJobs.count || ' Appeals.');
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  --commit;
end;
