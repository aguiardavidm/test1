-- Created on 4/13/2016 by Michel Scheffers
-- Issue 7855 - SEND appearing twice in processed
declare 
  -- Local variables here
  t_ChangeStatus    number := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus');
  t_ReasonForChange number := api.pkg_configquery.ColumnDefIdForName('p_ChangeStatus', 'ReasonForChange');
  t_Outcome         number := api.pkg_configquery.ColumnDefIdForName('p_ChangeStatus', 'Outcome');
  t_SendLicense     number := api.pkg_configquery.ObjectDefIdForName('p_ABC_SendLicense');
  t_ProcessId       number;
  t_count           number :=0;
  
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for c in (select j.JobId, p.ProcessId
              from api.jobs j
              join api.processes p on p.JobId = j.JobId
              join api.processtypes pt on pt.ProcessTypeId = p.ProcessTypeId
             where p.ProcessStatus = 'Scheduled'
               and pt.Name = 'p_ABC_SendLicense'
               and p.Description = 'Send License Process to Correct License Certificate for Un-Activated License'
             order by 1) loop
  
    api.pkg_processupdate.Remove (c.ProcessId);

    t_ProcessId := api.pkg_processupdate.New(c.JobId,
                                             t_ChangeStatus,
                                             'Change Status to correct script for Issue 6125',
                                             null,
                                             null,
                                             null);  
      api.pkg_columnupdate.SetValue(t_ProcessId, t_ReasonForChange,'Change Status to correct script for Issue 6125');  
    api.pkg_columnupdate.SetValue(t_ProcessId, t_Outcome, 'Approved');
                                           
    t_count := t_count +1;
    if mod(t_count, 10) = 0 then
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
      api.pkg_logicaltransactionupdate.ResetTransaction();    
    end if;                                                           
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  dbms_output.put_line (t_count || ' Processes updated');
  commit;
end;
