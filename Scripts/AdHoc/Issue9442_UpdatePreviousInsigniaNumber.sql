-- Created on 7/26/2018 by ADMINISTRATOR 
declare 
  -- Local variables here
begin
  -- Test statements here
  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute script as POSSEDBA');
  end if;

  api.pkg_logicaltransactionupdate.ResetTransaction;
  for p1 in (select p.oldvehicleid, p.vehicleid
             from possedba.issue9442_consolidatepermits p
             where api.pkg_columnquery.Value(p.oldvehicleid, 'PreviousInsigniaNumber') is not null
            ) loop
    dbms_output.put_line('Updating vehicle ' || p1.vehicleid || ' to ' || api.pkg_columnquery.Value(p1.oldvehicleid, 'PreviousInsigniaNumber'));
    api.pkg_columnupdate.SetValue(p1.Vehicleid, 'PreviousInsigniaNumber', api.pkg_columnquery.Value(p1.oldvehicleid, 'PreviousInsigniaNumber'));
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
  
end;