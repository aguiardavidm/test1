begin
  for i in (select pa.objectid 
              from query.j_abc_permitapplication pa
             union
            select pr.objectid
              from query.j_abc_permitrenewal pr) loop
    abc.pkg_instancesecurity.ApplyInstanceSecurity(i.objectid, sysdate);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;