-- Created on 8/31/2015 by Keaton
-- Correct Inactive dates 
declare 
  t_UpdateLicenseId  number;
  t_ExprDate     date;
begin
  
   t_ExprDate := to_date('6/30/2016', 'MM/DD/YYYY HH:MI:SS');
  
  --Transaction Handling
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  
  -- Find License Ids with license number 3404-41-281-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-281-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);

  -- Find License Ids with license number 3404-41-282-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-282-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-283-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-283-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-284-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-284-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-285-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-285-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-286-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-286-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-287-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-287-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-288-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-288-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-289-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-289-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-290-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-290-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
  
  -- Find License Ids with license number 3404-41-291-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-291-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
    
  -- Find License Ids with license number 3404-41-292-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-292-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
    
  -- Find License Ids with license number 3404-41-302-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-302-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
    
  -- Find License Ids with license number 3404-41-303-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-303-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
    
  -- Find License Ids with license number 3404-41-304-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-304-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
    
  -- Find License Ids with license number 3404-41-305-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-305-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
    
  -- Find License Ids with license number 3404-41-306-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-306-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);
    
  -- Find License Ids with license number 3404-41-307-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-307-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);

    
  -- Find License Ids with license number 3404-41-308-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-308-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);

    
  -- Find License Ids with license number 3404-41-309-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-309-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);  
  
    
  -- Find License Ids with license number 3404-41-311-001 – Set Expiration Date to 6/30/2016
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber = '3404-41-311-001'
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'ExpirationDate', t_ExprDate);    
  
   --Transaction Handling
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
