/*-----------------------------------------------------------------------------
 * Updates legal entite's state and country with existing metadata
 * Time: see each section
 * Current UAT Estimate is 25 minutes + 15 minutes
 *---------------------------------------------------------------------------*/
declare 
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_UpdatedCount                        pls_Integer := 0;
  t_LogicalTransactionId                pls_Integer;
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_TimeEnd                             number(15);
  t_TimeStart                           number(15);

  t_EndpointCountry                     udt_Id;
  t_EndpointState                       udt_Id;
  t_RelIdCountry                        udt_Id;
  t_RelIdState                          udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
  end;
  
  procedure ConvertLegalEntities is
  begin 
    -- Legal Entity Objects
    -- UAT: 47818 Rows at 32 rows a second 
    -- 47818/32/60 min est uat run = Est UAT 25 minutes
    -- Query takes 1.992 seconds
    t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
    dbug('Logical Transaction: ' || t_LogicalTransactionId);
    dbug(chr(13) || chr(10) || '---Legal Entity---');
    t_TimeStart := dbms_utility.get_time();

    t_EndpointCountry := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_LegalEntity','Country');
    t_EndpointState := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_LegalEntity','State');

    -- To make this go faster make this into a temp table
    for i in (
      select 
          le.ObjectId as LegalEntityID,
          le.StateObjectId as State,
          le.CountryObjectId as Country, 
          s.objectid as StateObjectId, 
          rcs.CountryId as CountryObjectID
      from query.o_Abc_Legalentity le
      left join query.o_Abc_State s
        on le.StateOfIncorporation = s.code
      join query.r_ABC_CountryState rcs 
        on rcs.StateId = s.ObjectId
      where le.StateOfIncorporation is not null
        and le.ObjectDefTypeId = 1
        and s.ObjectDefTypeId = 1) loop

      -- Delete rels if they exist
      if i.Country is not null then 
        select r.RelationshipId 
        into t_RelIDCountry 
        from api.relationships r 
        where r.EndPointId = t_EndpointCountry 
          and r.FromObjectId = i.LegalEntityID 
          and r.ToObjectId = i.Country;
        api.Pkg_Relationshipupdate.Remove(t_RelIDCountry);
      end if;

      if i.State is not null then
        select r.RelationshipId 
        into t_RelIDState 
        from api.relationships r 
        where r.EndPointId = t_EndpointState 
          and r.FromObjectId = i.LegalEntityID 
          and r.ToObjectId = i.State;
        api.Pkg_Relationshipupdate.Remove(t_RelIDState);
      end if;
      
      t_RelIDCountry := api.pkg_relationshipupdate.New(t_EndpointCountry, i.LegalEntityID, i.CountryObjectID);
      t_RelIdState := api.pkg_relationshipupdate.New(t_EndpointState, i.LegalEntityID, i.StateObjectId);
      t_UpdatedCount := t_UpdatedCount + 1;
      
      if mod(t_UpdatedCount, 500) = 0 then
        dbug(t_UpdatedCount);
        objmodelphys.pkg_eventqueue.endsession('N');
        api.pkg_logicaltransactionupdate.EndTransaction();
        commit;
      end if;
    end loop;
    
    objmodelphys.pkg_eventqueue.endsession('N');
    
    t_TimeEnd := dbms_utility.get_time();
    dbug('Time Elapsed Legal Entity: ' || (t_TimeEnd - t_TimeStart) / 100);

    dbug(t_UpdatedCount);
    api.pkg_logicaltransactionupdate.EndTransaction();
    commit;
  end ConvertLegalEntities;
  
  procedure ConvertLEJobs is
  begin 
    -- Legal Entity Jobs
    -- UAT: 8002 dev at 93 rows / 11.6 seconds 
    -- 8002*11.6/93/60 = UAT est 17 minutes 
    -- Query takes 11.065 seconds
    t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
    dbug('Logical Transaction: ' || t_LogicalTransactionId);
    dbug(chr(13) || chr(10) || '---Legal Entity Jobs---');
    t_TimeStart := dbms_utility.get_time();

    -- To make this go faster turn this query into a temp table
    for i in (
      select 
          j.objectid as JobID, 
          j.JobName, 
          j.StateObjectId as State,
          j.CountryObjectId as Country, 
          s.ObjectId StateObjectId, 
          rcs.CountryId CountryObjectId
      from (
        select 
            ObjectId, 
            onlinelestateofinc,
            'j_Abc_Newapplication' JobName,
            OnlineStateObjectId StateObjectId,
            OnlineCountryObjectId CountryObjectId
        from query.j_Abc_Newapplication na
        where OnlineLEStateOfInc is not null
          and ObjectDefTypeId = 2
        union all 
        select 
            ObjectId, 
            onlinelestateofinc,
            'j_ABC_PermitApplication' JobName,
            OnlineStateObjectId StateObjectId,
            OnlineCountryObjectId CountryObjectId
        from query.j_ABC_PermitApplication pa
        where OnlineLEStateOfInc is not null
          and ObjectDefTypeId = 2
        union all
        select 
            ObjectId,
            onlinelestateofinc,
            'j_ABC_PRApplication' JobName,
            OnlineStateObjectId StateObjectId,
            OnlineCountryObjectId CountryObjectId
        from query.j_ABC_PRApplication pra
        where OnlineLEStateOfInc is not null
          and ObjectDefTypeId = 2) j
      left join query.o_Abc_State s
        on j.onlinelestateofinc = s.code
      join query.r_ABC_CountryState rcs 
        on rcs.StateId = s.ObjectId
     where s.ObjectDefTypeId = 1) loop
        
      -- If this isnt fast enough this could be moved into 6 variables currently it seems fine
      t_EndpointCountry := api.pkg_ConfigQuery.EndPointIdForName(i.JobName,'OnlineCountry');
      t_EndpointState := api.pkg_ConfigQuery.EndPointIdForName(i.JobName,'OnlineState');

      -- Set the Endpoints based on JobID
      -- Delete rels if they exist
      if i.Country is not null then 
        select r.RelationshipId 
        into t_RelIDCountry 
        from api.relationships r 
        where r.EndPointId = t_EndpointCountry 
          and r.FromObjectId = i.JobID 
          and r.ToObjectId = i.Country;
        api.Pkg_Relationshipupdate.Remove(t_RelIDCountry);
      end if;

      if i.State is not null then
        select r.RelationshipId 
        into t_RelIDState 
        from api.relationships r 
        where r.EndPointId = t_EndpointState 
          and r.FromObjectId = i.JobID 
          and r.ToObjectId = i.State;
        api.Pkg_Relationshipupdate.Remove(t_RelIDState);
      end if;
      
      t_RelIDCountry := api.pkg_relationshipupdate.New(t_EndpointCountry, i.JobID, i.CountryObjectID);
      t_RelIdState := api.pkg_relationshipupdate.New(t_EndpointState, i.JobID, i.StateObjectId);
      t_UpdatedCount := t_UpdatedCount + 1;
      
      if mod(t_UpdatedCount, 500) = 0 then
        dbug(t_UpdatedCount);
        objmodelphys.pkg_eventqueue.endsession('N');
        api.pkg_logicaltransactionupdate.EndTransaction();
        commit;
      end if;
    end loop;
    
    objmodelphys.pkg_eventqueue.endsession('N');
    
    t_TimeEnd := dbms_utility.get_time();
    dbug('Time Elapsed Legal Entity Jobs: ' || (t_TimeEnd - t_TimeStart) / 100);

    dbug(t_UpdatedCount);
    api.pkg_logicaltransactionupdate.EndTransaction();
    commit;
  end;
  
  procedure DeleteSOIDetails is 
  begin
    t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
    dbug('Logical Transaction: ' || t_LogicalTransactionId);
    dbug(chr(13) || chr(10) || '---Delete SOI Details---');
    t_TimeStart := dbms_utility.get_time();

    -- Deleting old details for Jobs
    for i in (
        select ObjectId
        from query.j_Abc_Newapplication na
        where OnlineLEStateOfInc is not null
          and ObjectDefTypeId = 2
        union all 
        select ObjectId
        from query.j_ABC_PermitApplication pa
        where OnlineLEStateOfInc is not null
          and ObjectDefTypeId = 2
        union all
        select ObjectId
        from query.j_ABC_PRApplication pra
        where OnlineLEStateOfInc is not null
          and ObjectDefTypeId = 2) loop

      api.Pkg_Columnupdate.RemoveValue(i.Objectid,'onlinelestateofinc');
      t_UpdatedCount := t_UpdatedCount + 1;
      
      if mod(t_UpdatedCount, 500) = 0 then
        dbug(t_UpdatedCount);
        objmodelphys.pkg_eventqueue.endsession('N');
        api.pkg_logicaltransactionupdate.EndTransaction();
        commit;
      end if;
    end loop;
    
    -- Delete old detail for Legal Entity
    for i in (
      select ObjectId
      from query.o_Abc_Legalentity le
      where le.StateOfIncorporation is not null
        and ObjectDefTypeId = 1) loop

      api.Pkg_Columnupdate.RemoveValue(i.ObjectID,'StateOfIncorporation');
      t_UpdatedCount := t_UpdatedCount + 1;
      
      if mod(t_UpdatedCount, 500) = 0 then
        dbug(t_UpdatedCount);
        objmodelphys.pkg_eventqueue.endsession('N');
        api.pkg_logicaltransactionupdate.EndTransaction();
        commit;
      end if;
    end loop;

    objmodelphys.pkg_eventqueue.endsession('N');
    
    t_TimeEnd := dbms_utility.get_time();
    dbug('Deleting SOI Details: ' || (t_TimeEnd - t_TimeStart) / 100);

    dbug(t_UpdatedCount);
    api.pkg_logicaltransactionupdate.EndTransaction();
    commit;
  end;
  
begin 
  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  
  -- Current conversion script is rerunnable in dev
  -- To get true timings comment out the rel.new lines and run 
  -- as in production we will never hit the rel.deletes
  ConvertLegalEntities();    
  ConvertLEJobs();
  
  -- Procedures to delete the old details. 
  -- This maybe should have been done in 092886 but with context it was more efficent to do it here
  -- DeleteSOIDetails();
  
end; 

