--rerunable script
declare
  t_RunAsUser         varchar2(30) := 'POSSEDBA';
  t_newid             number;
  t_relid             number;
  t_jobtypeid         number;
  t_JTQEndpoint       number := api.pkg_configquery.EndPointIdForName('o_JobTypes','Question');
  t_jtQuestions       api.pkg_definition.udt_idlist;
  t_questions         api.pkg_definition.udt_stringlist;
begin
  -- verify run as user
  if sys_context('userenv', 'current_schema') != t_RunAsUser then
    raise_application_error(-20006, 'Please run script as ' || t_RunAsUser || '.');
  end if;
  
  --get new app jobtype (object)
  t_jobtypeid := api.pkg_simplesearch.ObjectByIndex('o_JobTypes','Name', 'j_ABC_PRApplication');
  
  dbms_output.put_line(t_jobtypeid);
  --get jobtype questions
  t_jtQuestions := api.pkg_objectquery.RelatedObjects(t_jobtypeid, t_JTQEndpoint);
      
  -- prep amnd types and procedure names list
  t_questions := extension.pkg_utils.Split('Is the Registrant the Product Owner?$Is the Registrant an Authorized Agent?$Does the Registrant have a Letter of Authorization from the Product Owner? (If the Registrant is the Product Owner, please answer "No")
  
By answering “yes” to this question, you acknowledge that, if requested, you will be required to provide the Letter(s) of Authorization within as few as 24 hours.', '$');
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  -- loop types list
  for i in 1 .. t_questions.count loop
    t_newid := null;
    for j in 1 .. t_jtQuestions.count loop
      if api.pkg_columnquery.Value(t_jtQuestions(j), 'Question') = t_questions(i) then
        t_newid := t_jtQuestions(j);
      end if;
    end loop;
    
    if t_newid is null then
      -- create new object if it doesn't exist
      t_newid := api.pkg_objectupdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_RenewalQuestion'));
        
      -- set question
      api.pkg_ColumnUpdate.SetValue(t_newid, 'Question', t_questions(i));
      
      -- create rel
      t_relid := api.pkg_relationshipupdate.New(t_JTQEndpoint, t_jobtypeid, t_newid);
    end if;
    
    --default values
    api.pkg_ColumnUpdate.SetValue(t_newid, 'SortOrder', i);
    api.pkg_ColumnUpdate.SetValue(t_newid, 'AcceptableAnswer', '');
    api.pkg_ColumnUpdate.SetValue(t_newid, 'NonacceptableText', ' '); --unfortunately mandatory
    api.pkg_ColumnUpdate.SetValue(t_newid, 'NonacceptableResponseType', 'Textbox');
    
  end loop;
  
  --save
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;
