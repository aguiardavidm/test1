-- Created on 10/5/2015 by JOSHUA.LENON 
-- Run time: < 10 seconds
declare 
  -- Local variables here
  a_GlobalName       varchar2(50);
  a_DataType         varchar2(10);
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
    for c in (select f.ObjectId, f.datatype
                from query.o_feeelement f
               where f.Globalname = :a_GlobalName)loop
         -- Set the data type of the fee element to a String. 
         api.pkg_columnupdate.SetValue(c.objectid, 'DataType', :a_DataType);
         
    end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
end;
