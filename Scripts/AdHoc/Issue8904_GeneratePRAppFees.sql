begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select pr.ExternalFileNum, pr.ObjectId, f.TotalOwing
              from query.j_Abc_Prapplication pr
              join query.o_fee f on f.JobId = pr.objectid
             where pr.LegalEntityObjectId is not null
               and f.TotalOwing != 0
               and f.ResponsibleObjectId is null) loop
    api.pkg_columnupdate.SetValue(i.objectid, 'GenerateFees', 'Y');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
