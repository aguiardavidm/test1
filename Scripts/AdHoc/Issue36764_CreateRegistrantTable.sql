-- Issue 36764-36866
-- Create table for Registrants that have non-renewed products.
-- 08/16/2018
drop   table possedba.Registrants_NonRenewed;
create table possedba.Registrants_NonRenewed
(
   RegistrantName                       varchar2(4000),
   RegistrantEmail                      varchar2(100),
   NumberOfProducts                     number,
   EmailSent                            date
);

grant insert on possedba.Registrants_NonRenewed to abc;
grant select on possedba.Registrants_NonRenewed to abc;
grant update on possedba.Registrants_NonRenewed to abc;
grant select on procsrvr.sysschedule to ABC;
grant execute on abc.pkg_abc_nonrenewal to posseextensions;
