-- Cancel a job
declare
  t_JobId number := &JobId;
  t_ProcessId number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  t_ProcessId := api.pkg_processupdate.New(t_JobId, api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus'), null, null, null, null);
  api.pkg_Columnupdate.SetValue(t_ProcessId, 'ReasonForChange', 'Test');
  api.pkg_processupdate.Complete(t_ProcessId, 'Cancelled');
  
  api.Pkg_Logicaltransactionupdate.EndTransaction;
  commit;
end;