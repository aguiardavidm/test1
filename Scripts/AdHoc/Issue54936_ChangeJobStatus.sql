/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 1 sec.
 * Purpose: This script changes the job status to "Cancelled" and generates a
 *   case note indicating the date and time of the change, as well as the
 *   purpose and the corresponding issue.
 *---------------------------------------------------------------------------*/
declare
  t_ChangeStatus                        pls_integer;
  t_IssueNumber                         varchar2(6) := '54936';
  t_JobId                               pls_integer;
  t_JobNumber                           varchar2(40) := '225103';
  t_JobStatus                           varchar2(60);
  t_JobType                             varchar2(60) := 'j_ABC_RenewalApplication';
  t_NoteDefId                           pls_integer;
  t_NoteId                              pls_integer;
  t_NoteTag                             varchar2(6) := 'GEN';
  t_NoteText                            varchar2(4000);
  t_ProcessId                           pls_integer;
begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  t_ChangeStatus := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');
  t_JobId := api.pkg_SimpleSearch.ObjectByIndex(t_JobType, 'ExternalFileNum', t_JobNumber);
  t_JobStatus := api.pkg_ColumnQuery.Value(t_JobId, 'StatusDescription');

  -- Create Note and Populate Text
  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;
  t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
        'System corrected Job status from ''' || t_JobStatus || ''' to ''';
  t_JobStatus := 'Cancelled';
  t_NoteText := t_NoteText || t_JobStatus || ''' by Issue ' || t_IssueNumber || '.';
  t_NoteId := api.pkg_NoteUpdate.New(t_JobId, t_NoteDefId, 'Y', t_NoteText);

  -- Change Job Status
  t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ChangeStatus, null, null, null, null);
  api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ReasonForChange',
      'Job #' || t_JobNumber || ' should be for the 19-20 term. See Issue ' || t_IssueNumber || '.');
  api.pkg_ProcessUpdate.Complete(t_ProcessId, t_JobStatus);

  api.pkg_logicaltransactionupdate.EndTransaction();

  commit;

end;
