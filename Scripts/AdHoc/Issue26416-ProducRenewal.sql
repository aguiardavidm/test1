-- Created on 12/14/2016 by M Scheffers
-- Issue 26416: Force a high-volume Product Renewal job to populate products.
declare 
  -- Local variables here
  t_RenewalId integer := 39053340; -- ObjectId of Product Renewal job 124166
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  api.pkg_columnupdate.SetValue (t_RenewalId, 'PopulateHighVolumeXrefs', 'Y');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
