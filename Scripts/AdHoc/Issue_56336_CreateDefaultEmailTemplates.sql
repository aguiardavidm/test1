/*-----------------------------------------------------------------------------
 * Expected run time:
 * Purpose: Create Default Email Templates
 *
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_ObjectID                            udt_Id;
  t_NewEmailID                          udt_Id;
  t_NoteDefID                           udt_Id;
  t_NoteID                              udt_Id;
  t_BadEmailTemplates                   udt_IdList;
  t_Count                               number;
  t_EmailDefID                          varchar2(50)
    := api.pkg_objectdefquery.IdForName('o_ABC_EmailTemplate');
  t_EmailHeader                         varchar2(4000);
  type array_t is varray(10) of varchar2(100);
  t_BusinessAreas array_t := array_t('License', 'Permit', 'Brands', 'CPL');
  t_EmailTypes array_t := array_t('Approval', 'Rejection');
  t_JobTypes array_t;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();

  t_EmailHeader := '<img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"><br><br>';

  select n.NoteDefId
    into t_NoteDefId
    from api.NoteDefs n
    where n.Description = 'E-Mail';

  select count(1)
  into t_Count
  from query.o_Abc_Emailtemplate
  where SubjectLine is null;

  dbms_output.put_line('There are ' || t_Count || ' System threatening email templates');

  select objectid
  bulk collect into t_BadEmailTemplates
  from query.o_Abc_Emailtemplate
  where SubjectLine is null;

  for i in 1..t_BadEmailTemplates.count loop
    api.pkg_objectupdate.remove(t_BadEmailTemplates(i), sysdate);
  end loop;

  t_JobTypes := array_t('New Application','Reinstatement Application', 'Renewal Application', 'Amendment Application');

  for b in 1..t_JobTypes.count loop
    for c in 1..t_EmailTypes.count loop
      select max(ObjectID)
      into t_ObjectID
      from query.o_abc_emailtemplate e
      where e.BusinessArea = 'License'
        and e.LicenseJobType = t_JobTypes(b)
        and e.EmailType = t_EmailTypes(c);
      if t_ObjectID is null then
        dbms_output.put_line('Creating: '||'License '||t_JobTypes(b) || ' ' || t_EmailTypes(c));
        t_NewEmailID := api.pkg_objectupdate.New(t_EmailDefID);
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'BusinessArea', 'License');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'LicenseJobType', t_JobTypes(b));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'EmailType', t_EmailTypes(c));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'SubjectLine', 'Default License '||t_JobTypes(b) || ' ' || t_EmailTypes(c));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'OnlineApplicantEmail', 'Y');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'AllLegalEntityOnlineUsers', 'Y');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'LegalEntityContactEmail', 'Y');
        t_NoteID := api.pkg_noteupdate.New(t_NewEmailID,t_NoteDefId,'N',t_EmailHeader||'Your '
            ||t_JobTypes(b)||' License has been completed for '||t_EmailTypes(c)||'.<br><br>{OnlineLink}');
      end if;
    end loop;
  end loop;

  t_JobTypes := array_t('Permit Application','Permit Renewal');

  for b in 1..t_JobTypes.count loop
    for c in 1..t_EmailTypes.count loop
      select max(ObjectID)
      into t_ObjectID
      from query.o_abc_emailtemplate e
      where e.BusinessArea = 'Permit'
        and e.PermitJobType = t_JobTypes(b)
        and e.EmailType = t_EmailTypes(c);
      if t_ObjectID is null then
        dbms_output.put_line('Creating: '||'Permit '||t_JobTypes(b) || ' ' || t_EmailTypes(c));
        t_NewEmailID := api.pkg_objectupdate.New(t_EmailDefID);
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'BusinessArea', 'Permit');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'PermitJobType', t_JobTypes(b));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'EmailType', t_EmailTypes(c));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'SubjectLine', 'Default Permit '||t_JobTypes(b) || ' ' || t_EmailTypes(c));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'OnlineApplicantEmail', 'Y');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'AllLegalEntityOnlineUsers', 'Y');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'LegalEntityContactEmail', 'Y');
        t_NoteID := api.pkg_noteupdate.New(t_NewEmailID,t_NoteDefId,'N',t_EmailHeader||'Your '
            ||t_JobTypes(b)||' has been completed for '||t_EmailTypes(c)||'.<br><br>{OnlineLink}');
      end if;
    end loop;
  end loop;

  t_JobTypes := array_t('Product Registration Application','Product Registration Amendment', 'Product Registration Renewal');

  for b in 1..t_JobTypes.count loop
    for c in 1..t_EmailTypes.count loop
      select max(ObjectID)
      into t_ObjectID
      from query.o_abc_emailtemplate e
      where e.BusinessArea = 'Brands'
        and e.BrandsJobType = t_JobTypes(b)
        and e.EmailType = t_EmailTypes(c);
      if t_ObjectID is null then
        dbms_output.put_line('Creating: '||'Brands '||t_JobTypes(b) || ' ' || t_EmailTypes(c));
        t_NewEmailID := api.pkg_objectupdate.New(t_EmailDefID);
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'BusinessArea', 'Brands');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'BrandsJobType', t_JobTypes(b));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'EmailType', t_EmailTypes(c));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'SubjectLine', 'Default Brands '||t_JobTypes(b) || ' ' || t_EmailTypes(c));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'OnlineApplicantEmail', 'Y');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'AllLegalEntityOnlineUsers', 'Y');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'LegalEntityContactEmail', 'Y');
        t_NoteID := api.pkg_noteupdate.New(t_NewEmailID,t_NoteDefId,'N',t_EmailHeader||'Your '
            ||t_JobTypes(b)||' has been completed for '||t_EmailTypes(c)||'.<br><br>{OnlineLink}');
      end if;
    end loop;
  end loop;

  t_JobTypes := array_t('CPL Submissions');

  for b in 1..t_JobTypes.count loop
    for c in 1..t_EmailTypes.count loop
      select max(ObjectID)
      into t_ObjectID
      from query.o_abc_emailtemplate e
      where e.BusinessArea = 'CPL'
        and e.CPLJobType = t_JobTypes(b)
        and e.EmailType = t_EmailTypes(c);
      if t_ObjectID is null then
        dbms_output.put_line('Creating: '||'CPL '||t_JobTypes(b) || ' ' || t_EmailTypes(c));
        t_NewEmailID := api.pkg_objectupdate.New(t_EmailDefID);
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'BusinessArea', 'CPL');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'CPLJobType', t_JobTypes(b));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'EmailType', t_EmailTypes(c));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'SubjectLine', 'Default CPL '||t_JobTypes(b) || ' ' || t_EmailTypes(c));
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'OnlineApplicantEmail', 'Y');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'AllLegalEntityOnlineUsers', 'Y');
        api.pkg_ColumnUpdate.SetValue(t_NewEmailID, 'LegalEntityContactEmail', 'Y');
        t_NoteID := api.pkg_noteupdate.New(t_NewEmailID,t_NoteDefId,'N',t_EmailHeader||'Your '
            ||t_JobTypes(b)||' CPL has been completed for '||t_EmailTypes(c)||'.<br><br>{OnlineLink}');
      end if;
    end loop;
  end loop;

  select count(1)
  into t_Count
  from query.o_Abc_Emailtemplate
  where SubjectLine is null;

  dbms_output.put_line('There are ' || t_Count || ' System threatening email templates');


  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
