-- Created on 8/16/2016 by MICHEL.SCHEFFERS 
-- Issue 5587 - Additional Warehouse. Removing stored values of AdditionalWarehouseSalesroomFee and let expression calculate it.
declare 
  -- Local variables here
  t_ColumnDefId      number;
  i                  number := 0;
begin
  -- Test statements here
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  begin
     select cd.ColumnDefId
     into   t_ColumnDefId
     from   api.columndefs cd
     where  lower (cd.name) = lower ('AdditionalWarehouseSalesroomFe')
     and    lower (cd.ObjectDefName) = lower ('o_ABC_License');
  exception
     when no_data_found then
        t_ColumnDefId := 0;
  end;
  
  for awf in (select ocd.ObjectId, ocd.AttributeValue
              from   possedata.objectcolumndata ocd
              where  ocd.ColumnDefId = t_ColumnDefId
              and    api.pkg_columnquery.Value(ocd.ObjectId,'LicenseTypeCode') = '24'
             ) loop
     dbms_output.put_line('Removing Salesroom Fee of '|| awf.attributevalue || ' for ' || awf.objectid || '(' || api.pkg_columnquery.Value(awf.objectid,'LicenseNumber') || ')');
     i := i + 1;
     api.pkg_columnupdate.RemoveValue(awf.objectid,'AdditionalWarehouseSalesroomFe');
  end loop;
  
  dbms_output.put_line('Removing Salesroom Fee for ' || i || ' licenses.');  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;