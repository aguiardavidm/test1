/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 1 sec
 * Purpose: This script registers New GL Accounts listed on the issue.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_GLAccountDefId                      number :=
      api.pkg_ConfigQuery.ObjectDefIdForName('o_GLAccount');
  t_GLAccountIds                        udt_IdList;
  t_GLAccountObjectIds                  udt_IdList;
  t_GLAccountObjects                    api.udt_ObjectList;
begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  rollback;

  select gl.GLAccountId
  bulk collect into t_GLAccountIds
  from api.GLAccounts gl
  where gl.GLAccount in ('GOP', 'ODP', 'NPF', 'NPPF', 'LFP', 'LPFP', 'SELB');

  for i in 1..t_GLAccountIds.count() loop
    api.pkg_objectupdate.RegisterExternalObject('o_GLAccount', t_GLAccountIds(i));
  end loop;

  t_GLAccountObjects := extension.pkg_utils.ConvertToObjectList(t_GLAccountIds);

  select reo.ObjectId
  bulk collect into t_GLAccountObjectIds
  from
    api.registeredexternalobjects reo
    join table(t_GLAccountObjects) gl
        on reo.LinkValue = gl.ObjectId
  where reo.ObjectDefId = t_GLAccountDefId;

  for j in 1..t_GLAccountObjectIds.count() loop
    api.pkg_columnupdate.SetValue(t_GLAccountObjectIds(j), 'Active', 'Y');
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;