-- Created on 10/9/2020 by CXADMIN
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_ProductID                           udt_Id;
  t_ProcIds                             udt_IdList;
  t_RelId                               udt_IdList;
  t_Count                               number := 0;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
  end;

begin
  select g.RelationshipId
  bulk collect into t_RelId
    from query.j_abc_renewalapplication r
    join query.p_abc_submitresolution s
        on s.JobId = r.jobid
       and s.Outcome = 'Submitted'
    join query.p_ABC_GenerateLicense p
      on r.JobId = p.JobID
    join query.r_generatelicense_certificate g
      on g.ProcessId = p.ProcessId
   where exists(select 1
                      from query.p_abc_administrativereview ar
                     where ar.JobId = r.jobid
                       and outcome is null
                     union all
                   select 1
                     from query.p_Abc_Reviewonlinerenewal ror
                     where ror.jobid = r.jobid
                       and outcome is null)
     and r.StatusDescription != 'Approved';

  api.pkg_LogicalTransactionUpdate.ResetTransaction();

  for i in 1..t_RelId.count() loop
      api.Pkg_Relationshipupdate.remove(t_RelId(i));
      t_Count := t_Count + 1;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();

  dbms_output.put_line(t_Count);

  --commit;
end;
