--run as POSSEDBA
-- NOTE: be sure to update the change set name passed into the CreateAccessGroups procedure at the bottom of the script 

declare
  subtype udt_Id is api.pkg_Definition.udt_Id;

  ----
  --  This procedure generates a block of access groups.  These access groups will have to
  --  be marshalled.
  ----
  procedure CreateAccessGroups (
    a_ChangeSet                varchar2,
    a_NumberToCreate           number,
    a_CreateChangeSet          varchar2 default 'Y',
    a_AccessGroupPrefix        varchar2 default 'www'
  ) is
    t_AccessGroupFolder        varchar2(100) := 'ABC Web Access Groups';
    t_AccessGroupId            udt_Id;
    t_AccessGroupName          varchar2(60);
    t_AdminAccessGroup         varchar2(30) := 'Super Users';
    t_AdminAccessGroupId       udt_Id;
    t_ChangeSetGUID            varchar2(38);
    t_ChangeSetName            varchar2(30);
    t_Comment                  varchar2(100);
    t_CurrentAccessGroup       number;
    t_FolderId                 udt_Id;
    t_StartingAccessGroup      number := 18001; --StartingAccessGroup; -- see NJ Wiki
    t_MaxRecord                number;
    t_SystemSettings           number;
    t_ProgrammaticallyAssigned boolean := true;
  begin

    rollback;
    api.pkg_LogicalTransactionUpdate.ResetTransaction();

    select FolderId
      into t_FolderId
      from presentation.Folders
      where Description = t_AccessGroupFolder;

    select AccessGroupId
      into t_AdminAccessGroupId
      from api.AccessGroups
      where Description = t_AdminAccessGroup;
     
   -- this came from production

    t_Comment := a_NumberToCreate || ' Access Group';
    if a_NumberToCreate <> 1 then
      t_Comment := t_Comment || 's';
    end if;
    t_Comment := t_Comment || ' created on ' || to_char(sysdate, 'FMMonth dd, yyyy') || '.';

    t_ChangeSetName := a_ChangeSet;

    if a_CreateChangeSet = 'Y' then
      posseCommonConfig.pkg_ChangeSetUpdate.New(t_ChangeSetName, t_Comment);
    end if;

    select cs.ChangeSetGuid
      into t_ChangeSetGUID
      from posseCommonConfig.ChangeSets cs
      where cs.Name = t_ChangeSetName;

    posseCommonConfig.pkg_ChangeSetUpdate.SetCurrentChangeSet(t_ChangeSetGUID, t_Comment);

    api.pkg_logicaltransactionupdate.EndTransaction;


    t_CurrentAccessGroup := t_StartingAccessGroup;
    t_MaxRecord := t_StartingAccessGroup + a_NumberToCreate;

    while t_currentAccessGroup < t_MaxRecord loop
      t_AccessGroupName := a_AccessGroupPrefix || trim(to_char(t_CurrentAccessGroup, '000000000'));
      stage.pkg_AccessGroupUpdate.New(t_AccessGroupName, t_FolderId, 'Y', t_AdminAccessGroupId);
      t_CurrentAccessGroup := t_CurrentAccessGroup + 1;

    end loop;

    api.pkg_LogicalTransactionUpdate.EndTransaction;
    COMMIT;
  end CreateAccessGroups;

begin

  CreateAccessGroups('NJ_P2_Issue39178', 2000, 'N'); -- see NJ Wiki
end;
/
