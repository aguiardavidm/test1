-- Created on 10/2/2015 by JOSHUA.LENON 
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
    for c in (select o.objectid, o.NightlyCreationObjectDefList
                from query.o_SystemSettings o)loop
                
         if c.nightlycreationobjectdeflist is null then
           api.pkg_columnupdate.setvalue(c.objectid, 'NightlyCreationObjectDefList', 'o_ABC_Permit');
         elsif instr(lower(c.nightlycreationobjectdeflist), 'o_abc_permit') = 0 then
           api.pkg_columnupdate.setvalue(c.objectid, 'NightlyCreationObjectDefList', c.nightlycreationobjectdeflist || ',o_ABC_Permit');
         elsif instr(lower(c.nightlycreationobjectdeflist), 'o_abc_permit') > 0 then
           null;
         end if;
    
    end loop;  
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
