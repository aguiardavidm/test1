-- rerunable script
declare
  t_RunAsUser   varchar2(30) := 'POSSEDBA';
  t_newid       number;
  t_types       api.pkg_definition.udt_stringlist;
  t_procnames   api.pkg_definition.udt_stringlist;
begin
  -- verify run as user
  if sys_context('userenv', 'current_schema') != t_RunAsUser then
    raise_application_error(-20006, 'Please run script as ' || t_RunAsUser || '.');
  end if;
  
  -- prep amnd types and procedure names list
  t_types := extension.pkg_utils.Split('Add Distributor,Administrative Amendment,Adjust Vintages', ',');
  t_procnames := extension.pkg_utils.Split('<Distributor Proc>,<Administrative Proc>,<Vintages Proc>', ',');
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  -- loop types list
  for i in 1 .. t_types.count loop
    t_newid := api.pkg_simplesearch.ObjectByIndex('o_ABC_ProductAmendmentType','Name', t_types(i));
    --dbms_output.put_line(t_types(i) || '  ' || t_procnames(i));
    if t_newid is null then
      --dbms_output.put_line(t_types(i));
      -- create new object if it doesn't exist
      t_newid := api.pkg_objectupdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_ProductAmendmentType'));
        
      -- set name
      api.pkg_ColumnUpdate.SetValue(t_newid, 'Name', t_types(i));
    end if;
    
    --default values
    api.pkg_ColumnUpdate.SetValue(t_newid, 'Active', 'Y');    
    api.pkg_ColumnUpdate.SetValue(t_newid, 'AvailableOnline', 'Y');
    api.pkg_ColumnUpdate.SetValue(t_newid, 'ProcedureName', t_procnames(i));

    if t_types(i) in ('Add Distributor','Administrative Amendment') then
      --dbms_output.put_line(t_types(i));
      api.pkg_ColumnUpdate.SetValue(t_newid, 'AdministrativeReviewRequired', 'Y');
    end if;
  end loop;
  
  --save
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;