-- RUN AS POSSESYS
-- Created by Michel Scheffers
-- Created on 12/15/2017
-- DESCRIPTION: This script inserts a daily job (03:00am) to remove any duplicate receipts that prevent the Daily Close from running

declare
  t_ScheduleId   number;
  t_Frequency    api.pkg_Definition.udt_Frequency;
begin
  if user != 'POSSESYS' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSESYS');
  end if;

  t_Frequency := api.pkg_Definition.gc_Daily;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure('abc.pkg_ABC_Common.RemoveDupReceipts', --Name
                                                                 'Remove Duplicate Receipts', --Description
                                                                 t_Frequency, --Frequency
                                                                 null,        --IncrementBy
                                                                 null,        --Unit
                                                                 null,        --Arguments
                                                                 'N',        --SkipBacklog
                                                                 'Y',        --DisableOnError
                                                                 'N',         --Sunday
                                                                 'Y',         --Monday
                                                                 'Y',         --Tuesday
                                                                 'Y',         --Wednesday
                                                                 'Y',         --Thursday
                                                                 'Y',         --Friday
                                                                 'Y',         --Saturday
                                                                 'N',        --MonthEnd
                                                                 --Run at 3:00 AM daily
                                                                 trunc(sysdate) + 3/24,
                                                                 null,        --EndDate
                                                                 null,        --Email
                                                                 null,        --ServerId
                                                                 api.pkg_Definition.gc_normal);       --Priority
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;