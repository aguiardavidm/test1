rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- Created on 02/23/2016 by MICHEL.SCHEFFERS 
-- Issue7027: Need to be able to reinstate licenses in states other than Revoked
-- Set IsLatestVersion on latest generation of License
-- In Conv: 17,192 licenses < 1 minutes
declare 
  -- Local variables here
  t_LicenseNumbers     api.pkg_definition.udt_StringList;
  t_ObjectIds          api.pkg_definition.udt_IdList;
  t_ActiveLicense      api.pkg_definition.udt_Id;
  t_ColumnDefId        api.pkg_definition.udt_Id := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'IsLatestVersion');
  t_ObjectId           api.pkg_definition.udt_Id;
  t_Updated            number := 0;
  t_Count              number;
  t_ActiveCount        number;
  t_LicenseNumber      varchar2(20);
  t_CurrentTransaction api.pkg_definition.Udt_Id;
  t_EffectiveDate      date;
  
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  t_CurrentTransaction := api.pkg_logicaltransactionupdate.CurrentTransaction;
  api.pkg_logicaltransactionupdate.EndTransaction();
  
  -- All unique LicenseNumbers (without Gen) from Conversion
  select distinct (substr(l.LicenseNumber, 1, 11))
    bulk collect into t_LicenseNumbers
    from query.o_abc_license l
   order by substr(l.LicenseNumber, 1, 11);
   
  for i in 1 .. t_LicenseNumbers.count loop
     select x.ObjectId
       bulk collect into t_ObjectIds
       from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', t_LicenseNumbers (i)) as api.udt_ObjectList)) x;
     t_ActiveCount := 0;  
     -- Find any active licenses
     for i1 in 1 .. t_ObjectIds.count loop
        if api.pkg_columnquery.Value(t_ObjectIds (i1), 'State') = 'Active' then
           t_ActiveLicense := t_ObjectIds(i1);
           t_ActiveCount := t_ActiveCount + 1;
           if t_ActiveCount > 1 then
              dbms_output.put_line ('License ' || t_LicenseNumbers (i) || ' has multiple Active versions.');
              exit;
           end if;
        end if;
     end loop;
     -- If an active found, set IsLatestVersion
     if t_ActiveCount = 1 then
        select count (*)
          into t_Count
          from possedata.ObjectColumnData d
         where d.ObjectId = t_ActiveLicense
           and d.ColumnDefId = t_ColumnDefId;
        if t_Count = 0 then 
           select count (cd.ObjectId)
             into t_Count
             from possedata.objectcolumndata cd
            where cd.objectid = t_ActiveLicense and
                  cd.columndefid = t_ColumnDefId;
           if t_Count = 0 then
              insert into possedata.objectcolumndata cd
                         (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue, cd.searchvalue)
                  Values (t_CurrentTransaction, t_ActiveLicense, t_ColumnDefId, 'Y', 'y'); 
              t_Updated := t_Updated + 1;
              dbms_output.put_line ('Updating Active License ' || t_LicenseNumbers (i));
           end if;
        end if;
     elsif t_ActiveCount = 0 then
        -- No Actives found. Find cancelled license       
        t_ObjectId := 0;
        for i1 in 1 .. t_ObjectIds.count loop
           if api.pkg_columnquery.Value(t_ObjectIds (i1), 'State') = 'Cancelled' then
              t_ObjectId := t_ObjectIds (i1);
              select count (cd.ObjectId)
                into t_Count
                from possedata.objectcolumndata cd
               where cd.objectid = t_ObjectId and
                     cd.columndefid = t_ColumnDefId;
              if t_Count = 0 then
                 dbms_output.put_line ('Updating Cancelled License ' || t_LicenseNumbers (i));
                 insert into possedata.objectcolumndata cd
                            (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue, cd.searchvalue)
                     Values (t_CurrentTransaction, t_ObjectId, t_ColumnDefId, 'Y', 'y');
                 t_Updated := t_Updated + 1;
              end if;
           end if;
        end loop;
        -- No Cancelled found, finding latest term non-pending
        if t_ObjectId = 0 then
           t_EffectiveDate := to_date ('01/01/1000','dd/mm/yyyy');
           for i1 in 1 .. t_ObjectIds.count loop
              if api.pkg_columnquery.Value(t_ObjectIds (i1), 'State') != 'Pending' then
                 if api.pkg_columnquery.DateValue(t_ObjectIds (i1), 'EffectiveDate') > t_EffectiveDate then
                    t_EffectiveDate := api.pkg_columnquery.DateValue(t_ObjectIds (i1), 'EffectiveDate');
                    t_ObjectId := t_ObjectIds(i1);
                 end if;
              end if;
           end loop;
           
           if t_ObjectId > 0 then
              select count (*)
                into t_Count
                from possedata.ObjectColumnData d
               where d.ObjectId = t_ObjectId
                 and d.ColumnDefId = t_ColumnDefId;
              if t_Count = 0 then 
                 dbms_output.put_line ('Updating Non-Active License ' || t_LicenseNumbers (i));
                 insert into possedata.objectcolumndata cd
                            (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue, cd.searchvalue)
                     Values (t_CurrentTransaction, t_ObjectId, t_ColumnDefId, 'Y', 'y');
                 t_Updated := t_Updated + 1;
              end if;
           end if;
        end if;
     end if;     
  end loop;

  dbms_output.put_line ('Licenses found   : ' || t_LicenseNumbers.count);  
  dbms_output.put_line ('Licenses updated : ' || t_Updated);  

  commit;  
end;
/
