-- Created on 8/5/2015 by MICHEL.SCHEFFERS 
-- Runtime less than 1 minutes.
declare 
  -- Local variables here
  t_PermitType integer;
begin
  -- Test statements here
  select objectid
    into t_PermitType
    from query.o_abc_permittype pt
   where pt.Code = 'CTW';
   
  for p in (Select x.ObjectId, x.PermitNumber
              from (select p.ObjectId, p.PermitNumber, p.SolicitorHasOwnership, trunc(p.IssueDate) IssueDateTrunc
                      from query.o_abc_permit p  
                      join query.r_abc_permitpermittype ppt  on ppt.PermitId = p.ObjectId
                     where ppt.PermitTypeId = t_PermitType) x
              where x.IssueDateTrunc > to_date('06/01/2015','mm/dd/yyyy') 
                and x.SolicitorHasOwnership is null
            )
  loop
    dbms_output.put_line('Found Permit ' || p.PermitNumber || '.');
    api.pkg_columnupdate.SetValue (p.objectid, 'SolicitorHasOwnership', 'Yes');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
