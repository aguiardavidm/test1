-------------------------------------------------------------------------------
-- Creates a copy from the ABCRW schema to the BCPCORRAL schema for license
-------------------------------------------------------------------------------
create or replace view ABCRW.LICENSE as
select 
	licenseid, 
	licensenumber, 
	effectivestartdate, 
	effectiveenddate, 
	expirationdate, 
	state, 
	issuedate, 
	licensetypeid, 
	licenseeid, 
	eventtypeid, 
	primarylicenseid, 
	effectivedate, 
	eventlocationaddressid, 
	operatorlegalentityid, 
	conflictofinterest, 
	federaltaxregistrationnumber, 
	warehousephonenumber, 
	inactivitystartdate, 
	countyid, 
	municipalityid, 
	criminalconviction, 
	establishmentid, 
	eventcontacts, 
	restrictedbreweryproduction, 
	limitedbreweryproduction, 
	supplementarylimiteddistillery, 
	farmwineryproduction, 
	farmwinerywholesaleprivilege, 
	plenarywineryproduction, 
	plenarywinerywholesaleprivileg, 
	outofstatewineryproduction, 
	outofstatewinerywholesaleprivi, 
	retailtransittype, 
	warehouseaddressid, 
	warehousename, 
	additionalwarehousesalesroomfe, 
	sportingfacilitycapacity, 
	islatestversion, 
	isaccusable, 
	isactive, 
	isactiveorsuspended, 
	isamendable, 
	isinspectable, 
	isrenewable
  from BCPCORRAL.LICENSE;

grant select on abcrw.license to dashboard, posseuser, posseextensions, abcrwadhoc;
  
-------------------------------------------------------------------------------
-- Creates a copy from the ABCRW schema to the BCPCORRAL schema for licensetype
-------------------------------------------------------------------------------
create or replace view ABCRW.LICENSETYPE as
select lt.LICENSETYPEID,
       lt.Code,
       lt.Name,
       lt.ObjectDefName,
       lt.Active,
       lt.IsSecondaryOnly,
       lt.IsSpecialEventLicense,
       lt.ExpirationMethod,
       lt.ExpirationMonth,
       lt.ExpirationNumber,
       lt.ExpirationReminderDays,
       lt.GracePeriodDays,
       lt.CPLSubmission,
       lt.EnableWarehouseAddressEntry,
       lt.issuingAuthority,
       lt.PremiseAddressRequired,
       lt.DefaultLicensingClerkId,
       lt.DefaultLicensingSupervisorId,
       lt.GLAccountId,
       lt.IncludeOnAddPrivilegeReport,
       lt.IsValidAsregistrantDistributor,
       lt.LicenseCertificateText,
       lt.AvailableOnline,
       lt.AvailableToPrintOnline,
       lt.CanRenewDaysPrior,
       lt.CanRenewExpired,
       lt.OnlineDescription,
       lt.RenewalAvailableOnline
  from BCPCORRAL.Licensetype lt;

grant select on abcrw.licensetype to dashboard, posseuser, posseextensions, abcrwadhoc;

-------------------------------------------------------------------------------
-- Creates a copy from the ABCRW schema to the BCPCORRAL schema for licensetype
-------------------------------------------------------------------------------
create or replace view ABCRW.LICENSEJOB as
select lj.JOBID,
       lj.LicenseObjectId,
       lj.JobNameDescription,
       lj.ObjectDefDescription,
       lj.ObjectDefName,
       lj.Region,
       lj.CompletedDate,
       lj.CreatedDate,
       lj.StatusDescription,
       lj.StatusId,
       lj.ExternalFileNum,
       lj.ObjectDefId,
       lj.StatusName,
       lj.LicenseType,
       lj.RegionObjectId
  from BCPCORRAL.LicenseJob lj;

grant select on abcrw.licensejob to dashboard, posseuser, posseextensions, abcrwadhoc;
