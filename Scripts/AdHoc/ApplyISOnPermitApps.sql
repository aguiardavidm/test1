begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all Permit Application jobs
  for i in (select ObjectId
              from query.j_ABC_PermitApplication
            ) loop
    -- Apply Instance Security for each job
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.ObjectId, sysdate);
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;
