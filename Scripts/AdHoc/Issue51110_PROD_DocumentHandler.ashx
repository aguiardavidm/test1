﻿<%@ WebHandler Language="C#" Class="Computronix.POSSE.DocumentHandler" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Computronix.Ext;

namespace Computronix.POSSE
{
    public class DocumentHandler : Computronix.Ext.HandlerBase
    {
        protected static Dictionary<string, string> Paths { get; set; }

        protected static bool StoreEncoded { get; set; }

        static DocumentHandler()
        {
            string path;
            string defaultPath = null;
            string[] keys = new string[]
                { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };

            DocumentHandler.StoreEncoded =
                    Sys.IsTrue(ConfigurationManager.AppSettings["StoreEncoded"]);
            DocumentHandler.Paths = new Dictionary<string, string>();

            foreach (string key in keys)
            {
                path = ConfigurationManager.AppSettings[string.Format("Path{0}", key)];

                if (!string.IsNullOrEmpty(path))
                {
                    if (!path.EndsWith("\\"))
                    {
                        path = string.Format("{0}\\", path);
                    }

                    defaultPath = path;
                }

                DocumentHandler.Paths.Add(key, defaultPath);
            }
        }

        public override void ProcessRequest()
        {
            string document;
            string documentKey = this["documentKey"];
            Stream stream;
            FileStream fileStream;

            switch (this["action"])
            {
                case "getBinary":
                    if (string.IsNullOrEmpty(documentKey))
                    {
                        throw new ApplicationException(
                            "The documentKey cannot be blank");
                    }
                    else
                    {
                        this.Response.ContentType = "application/octet-stream";

                        using (fileStream = File.OpenRead(this.GetFileName(documentKey)))
                        {
                            fileStream.CopyTo(this.Response.OutputStream);
                            this.Response.AddHeader("Content-Length", fileStream.Length.ToString());
                        }
                    }
                    break;
                case "get":
                    if (string.IsNullOrEmpty(documentKey))
                    {
                        throw new ApplicationException(
                            "The documentKey cannot be blank");
                    }
                    else
                    {
                        if (DocumentHandler.StoreEncoded)
                        {
                            this.Response.Write(string.Format(
                                "{{ \"documentKey\": \"{0}\",  \"document\": \"{1}\" }}",
                                documentKey, File.ReadAllText(this.GetFileName(documentKey))));
                        }
                        else
                        {
                            this.Response.Write(string.Format(
                                "{{ \"documentKey\": \"{0}\",  \"document\": \"{1}\" }}",
                                documentKey, Convert.ToBase64String(
                                File.ReadAllBytes(this.GetFileName(documentKey)))));
                        }
                    }
                    break;
                case "setBinary":
                    stream = this.Request.InputStream;

                    if (stream.Length == 0)
                    {
                        throw new ApplicationException(
                            "The document cannot be blank");
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(documentKey))
                        {
                            documentKey = Guid.NewGuid().ToString();
                        }

                        using (fileStream = File.Open(this.GetFileName(documentKey), FileMode.Create))
                        {
                            stream.CopyTo(fileStream);
                        }

                        this.Response.Write(string.Format("{{ \"documentKey\": \"{0}\" }}",
                        documentKey));
                    }
                    break;
                case "set":
                    document = this["document"];

                    if (string.IsNullOrEmpty(document))
                    {
                        throw new ApplicationException(
                            "The document cannot be blank");
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(documentKey))
                        {
                            documentKey = Guid.NewGuid().ToString();
                        }

                        if (DocumentHandler.StoreEncoded)
                        {
                            File.WriteAllText(this.GetFileName(documentKey), document);
                        }
                        else
                        {
                            File.WriteAllBytes(this.GetFileName(documentKey),
                                Convert.FromBase64String(document));
                        }

                        this.Response.Write(string.Format("{{ \"documentKey\": \"{0}\" }}",
                            documentKey));
                    }
                    break;
                case "delete":
                    if (string.IsNullOrEmpty(documentKey))
                    {
                        throw new ApplicationException(
                            "The documentKey cannot be blank");
                    }
                    else
                    {
                        /* The following line must be commented out in NON-PROD environments */
                        File.Delete(this.GetFileName(documentKey));

                        this.Response.Write(string.Format(
                            "{{ \"documentKey\": \"{0}\",  \"deleted\": \"true\" }}",
                            documentKey));
                    }
                    break;
                case "exists":
                    if (string.IsNullOrEmpty(documentKey))
                    {
                        throw new ApplicationException(
                            "The documentKey cannot be blank");
                    }
                    else
                    {
                        this.Response.Write(string.Format(
                            "{{ \"documentKey\": \"{0}\",  \"exists\": \"{1}\" }}",
                            documentKey, File.Exists(this.GetFileName(documentKey))));
                    }
                    break;
            }
        }

        protected string GetFileName(string documentKey)
        {
            return string.Format("{0}{1}.dat",
                DocumentHandler.Paths[documentKey.Substring(0, 1).ToUpper()],
                documentKey);
        }
    }
}