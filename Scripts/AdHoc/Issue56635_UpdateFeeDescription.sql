-- Update fee descriptions to avoid fee schedule bug that recalculates 
--  fees if the definition has changed.
--  update to finance.transactions approved by support: ISSUE027068
declare 
  -- Local variables here
  i integer;
begin
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (
      select f.feeId, pa.ObjectId, f.Description, pa.StatusDescription, f.Amount
        from api.fees f
        join query.j_Abc_Permitapplication pa
            on pa.ObjectId = f.JobId
       where f.Description = 'Transit License Insignia Application Fee'
         and pa.StatusDescription in  ('In Review', 'New')
      ) loop
      update finance.transactions_t
         set description = 'Transportation License Insignia Application Fee'
       where transactionid = i.feeid;
    dbms_output.put_line('Updated Fee ' || i.feeid || ' on Job ' || i.objectid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
--  commit;
end;