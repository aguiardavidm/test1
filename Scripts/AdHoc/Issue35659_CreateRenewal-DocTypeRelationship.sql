  /*---------------------------------------------------------------------------
   * Issue 35659 - Consistency of documents with in amendment jobs and new 
   * application jobs 
   *   Create RenewalApplication - QuestionDocumentType relationships to be
   * used on Documents tab of public renewal application
   * Run time < 1 minute
   * ------------------------------------------------------------------------*/
  declare 
  -- Local variables here
    t_EndPointId                        integer :=
        api.pkg_configquery.EndPointIdForName('j_ABC_RenewalApplication',
        'Response');
    t_DTEndpoint                        integer :=
        api.pkg_configquery.EndPointIdForName('j_ABC_RenewalApplication',
        'DocumentType');
    t_QARel                             integer;
  begin

    if user != 'POSSEDBA' then
      api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
    end if;

    api.pkg_logicaltransactionupdate.ResetTransaction;
    for d in (select
                r.FromObjectId,
                q.DocumentTypeObjectId,
                dt.Name
              from query.o_abc_qaresponse q
              join api.relationships r
                  on r.ToObjectId = q.ObjectId
              join query.o_abc_documenttype dt
                  on dt.ObjectId = q.DocumentTypeObjectId
              where r.EndPointId = t_EndPointId
             ) loop
      if d.DocumentTypeObjectId is not null then
        begin
          select r1.RelationshipId
          into t_QARel
          from query.r_ABC_RenewResponseDocType r1
          where r1.RenewalAppId = d.Fromobjectid
            and r1.DocTypeId = d.documenttypeobjectid;
        exception
          when no_data_found then
            t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, d.Fromobjectid,
                d.documenttypeobjectid);
            api.pkg_columnupdate.setvalue(t_QARel, 'Mandatory', 'Y');
        end;
      end if;
    end loop;

    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
  end;
