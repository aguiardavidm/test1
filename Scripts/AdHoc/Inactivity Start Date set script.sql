PL/SQL Developer Test script 3.0
11
begin
  -- logical transaction start
  api.pkg_logicaltransactionupdate.ResetTransaction();

  -- set some stored details
  api.pkg_columnupdate.SetValue(/*Job ID*/,'InactivityStartDate', to_date('1/1/2015', 'mm/dd/yyyy'));
  
  -- logical transaction end;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
0
0
