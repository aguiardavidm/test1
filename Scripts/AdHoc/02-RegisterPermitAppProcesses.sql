declare

begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select pt.ProcessTypeId
              from api.processtypes pt
              where pt.Name in ('p_ABC_OnlinePermitSubmittal',
                                'p_ABC_EnterPermitApplication',
                                'p_ABC_PrintPermit',
                                'p_ABC_AdministrativePermReview',
                                'p_ABC_SendPermit',
                                'p_ABC_SendPermitDenialNotif',
                                'p_ABC_RevokePermit',
                                'p_ABC_CancelPermit',
                                'p_ABC_SupervisorReview')
                and not exists (select 1
                                  from query.o_processtype q where q.ProcessTypeId = pt.ProcessTypeId)) loop
    api.pkg_objectupdate.RegisterExternalObject('o_ProcessType', i.processtypeid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;