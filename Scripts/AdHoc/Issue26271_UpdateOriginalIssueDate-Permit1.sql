-- Created on 10/31/2017 by MICHEL.SCHEFFERS 
-- Issue 26271 - Set Original Issue Date on Permit
declare 
  -- Local variables here
  t_OriginalIssueDate    date;
  t_IssueDate            date;
  t_IdList               api.pkg_Definition.udt_IdList;
  t_LicenseColumnDefId   number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'OriginalIssueDate');
  t_PermitColumnDefId    number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'OriginalIssueDate');
  t_CurrentTransaction   api.pkg_definition.Udt_Id;
  t_FormattedDate        varchar2(400);
  c1                     integer := 0;
  c2                     integer := 0;
  c3                     integer := 0;
  t_ObjectList           api.Udt_Objectlist;
begin
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_CurrentTransaction := api.pkg_logicaltransactionupdate.CurrentTransaction;  
  -- Update Original Issue Date for Permits
  dbms_output.put_line('Updating Original Issue Date for Permits');
  for p1 in (select distinct(p.PermitNumber)--, min(trunc(p.IssueDate)) IssueDate
             from   query.o_abc_permit p
            ) loop
     if p1.permitnumber is null then
       continue;
     end if;
     c1 := c1 + 1;

     t_ObjectList := api.Udt_Objectlist();
     extension.Pkg_Cxproceduralsearch.InitializeSearch('o_ABC_Permit');
     extension.Pkg_Cxproceduralsearch.SearchByIndex('PermitNumber', p1.permitnumber, p1.permitnumber,false);
     extension.pkg_cxproceduralsearch.PerformSearch(t_ObjectList, 'and');
     select ob.ObjectId
     bulk   collect into t_IdList
     from   (table(cast(t_ObjectList as api.udt_objectlist)) ob);

     t_OriginalIssueDate := to_date ('12/31/2099', 'mm/dd/yyyy');
     for i in 1..t_IdList.count loop
        t_IssueDate := api.pkg_columnquery.DateValue(t_IdList(i), 'IssueDate');
        if t_IssueDate < t_OriginalIssueDate then
           t_OriginalIssueDate := t_IssueDate;
        end if;
     end loop;
     if t_OriginalIssueDate = to_date ('12/31/2099', 'mm/dd/yyyy') then
       continue;
     end if;
     t_FormattedDate := to_char(t_OriginalIssueDate,'yyyy-mm-dd') || ' ' || '00:00:00';
     dbms_output.put_line('Original Issue Date for Permit ' || p1.permitnumber || '(' || c1 || '): ' || t_FormattedDate);

     for i in 1..t_IdList.count loop
        c2 := c2 + 1;
        if api.pkg_columnquery.Value(t_IdList(i), 'State') != 'Pending' and 
           api.pkg_columnquery.DateValue(t_IdList(i), 'OriginalIssueDate') is null then
           c3 := c3 + 1;
           dbms_output.put_line('   Updating Original Issue Date for ' || t_IdList(i) || ' to ' || t_FormattedDate);
           insert into possedata.objectcolumndata cd
                   (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue, cd.searchvalue)
            Values (t_CurrentTransaction, t_IdList(i), t_PermitColumnDefId, t_FormattedDate, t_FormattedDate);
        end if;
     end loop;
  end loop;
  dbms_output.put_line(c1 || ' Master Permits found');
  dbms_output.put_line(c2 || ' Permits Found');
  dbms_output.put_line(c3 || ' Permits Updated');
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
