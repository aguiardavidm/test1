-- Created on 6/23/2017 by MICHEL.SCHEFFERS 
-- Issue 9442 - Update Vehicles with null value in details

--drop   table possedba.issue9442_updatevehicles;
create table possedba.issue9442_updatevehicles
(
   VehicleId           number,
   MakeModel           varchar2(1000),
   StateRegistration   varchar2(1000),
   StateOfRegistration varchar2(2),
   VIN                 varchar2(50),
   OwnedOrLeased       varchar2(10),
   PermitNumber        varchar2(40)
);
/
declare 
  -- Local variables here
  i                           number  := 0;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);
  for v in (select *
             from   query.r_abc_permitvehicle pv
            ) loop
     if api.pkg_columnquery.Value(v.permitobjectid, 'RequiresVehicle' ) = 'Y' then            
       if (api.pkg_Columnquery.Value(v.vehicleobjectid, 'StateRegistration') is null or
          api.pkg_Columnquery.Value(v.vehicleobjectid, 'StateOfRegistration') is null or
          api.pkg_columnquery.Value(v.vehicleobjectid, 'MakeModelYear') is null or
          api.pkg_columnquery.Value(v.vehicleobjectid, 'VIN') is null or
          api.pkg_columnquery.Value(v.vehicleobjectid, 'OwnedOrLeasedLimousine') is null) and
          api.Pkg_Columnquery.Value(v.Permitobjectid, 'State') in ('Active', 'Expired') then
          i := i + 1;
          dbms_output.put_line('Updating Vehicle ' || v.vehicleobjectid || ' on Permit ' || api.pkg_columnquery.Value(v.permitobjectid, 'PermitNumber'));
          if api.pkg_Columnquery.Value(v.vehicleobjectid, 'StateRegistration') is null then
             dbms_output.put_line('         License Plate');
             api.pkg_columnupdate.setvalue(v.vehicleobjectid, 'Stateregistration', 'None');
          end if;
          if api.pkg_Columnquery.Value(v.vehicleobjectid, 'StateOfRegistration') is null then
             dbms_output.put_line('         State');
             api.pkg_columnupdate.setvalue(v.vehicleobjectid, 'StateOfRegistration', 'NJ');
          end if;
          if api.pkg_columnquery.Value(v.vehicleobjectid, 'MakeModelYear') is null then
             dbms_output.put_line('         Make/Model/Year');
             api.pkg_columnupdate.setvalue(v.vehicleobjectid, 'MakeModelYear', 'None');
          end if;
          if api.pkg_columnquery.Value(v.vehicleobjectid, 'VIN') is null then
             dbms_output.put_line('         VIN');
             api.pkg_columnupdate.setvalue(v.vehicleobjectid, 'VIN', 'None');
          end if;
          if api.pkg_columnquery.Value(v.vehicleobjectid, 'OwnedOrLeasedLimousine') is null then
             dbms_output.put_line('         Owned/Leased');
             api.pkg_columnupdate.setvalue(v.vehicleobjectid, 'OwnedOrLeasedLimousine', 'Owned');
          end if;
          insert into possedba.issue9442_updatevehicles
                      (VehicleId, MakeModel, StateRegistration, StateOfRegistration, VIN, OwnedOrLeased, PermitNumber)
                 values
                      (v.vehicleobjectid,
                       api.pkg_columnquery.Value(v.vehicleobjectid, 'MakeModelYear'),
                       api.pkg_columnquery.Value(v.vehicleobjectid, 'StateRegistration'),
                       api.pkg_columnquery.Value(v.vehicleobjectid, 'StateOfRegistration'),
                       api.pkg_columnquery.Value(v.vehicleobjectid, 'VIN'),
                       api.pkg_columnquery.Value(v.vehicleobjectid, 'OwnedOrLeasedLimousine'),
                       api.pkg_columnquery.Value(v.permitobjectid, 'PermitNumber'));
       end if;
     end if;
  end loop;
             
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
  dbms_output.put_line(i || ' Vehicles updated');
end;
