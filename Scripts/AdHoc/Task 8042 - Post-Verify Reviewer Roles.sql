begin
  for x in (select objectId from query.o_Role where  IsABCReviewerRole = 'Y')
    loop
      api.pkg_logicaltransactionupdate.ResetTransaction;
      api.pkg_objectupdate.Verify(x.objectid);
      api.pkg_logicaltransactionupdate.EndTransaction;
  end loop;
  commit;
end;