-- Run as CONVERSION
-- Created by Andy Patterson (CXUSA)
-- Created on 7/29/2015
-- Time to run in NJCONV:  mins
declare
  t_ColumnDefId      number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'dup_Location');
  t_CurrTransaction  number;
  t_CommitCount      number := 0;
begin
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  t_CurrTransaction := api.pkg_LogicalTransactionUpdate.CurrentTransaction;
  -- for every permit in the system pull back the value for the ObjectId and the Location
  for i in (select objectid, location, dup_Location
              from query.o_ABC_Permit) loop

    -- if the count is zero there is no value and we need to insert one
    if i.dup_Location is null  and i.Location is not null then
      insert into possedata.objectcolumndata_t cd
      values (t_CurrTransaction,
             i.ObjectId,
             t_ColumnDefId,
             to_date(null),
             to_date(null),
             i.Location,
             to_number(null),
             lower(substr(i.Location, 1, 20)), --the search field is limited to 20 characters
             to_number(null));

    -- otherwise update the existing row in objectcolumndata_t
    elsif i.Location is not null then
      update possedata.objectcolumndata_t cd
         set cd.AttributeValue = i.Location,
             cd.SearchValue = lower(substr(i.Location, 1, 20)) --the search field is limited to 20 characters
       where cd.objectid = i.objectid
         and cd.columndefid = t_ColumnDefId;
    end if;

    if mod(t_CommitCount, 10000) = 0 then
      t_CommitCount := t_CommitCount + 1;
      api.pkg_LogicalTransactionUpdate.EndTransaction;
      commit;
      api.pkg_LogicalTransactionUpdate.ResetTransaction;
    end if;
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;
