declare
  t_RelIds          api.pkg_definition.udt_IdList;
  t_EndPointId      number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_Product', 'DistributorLic');
  t_FromObjectIds   api.pkg_definition.udt_IdList;
  t_ToObjectIds     api.pkg_definition.udt_IdList;
begin
  select a.RelationshipId, a.FromObjectId, a.ToObjectId
    bulk collect into t_RelIds, t_FromObjectIds, t_ToObjectIds
    from (select count(*) count, r.RelationshipId
            from rel.storedrelationships r
            group by r.RelationshipId
            having count(1) != 2) r
    join api.relationships a on a.RelationshipId = r.RelationshipId
    join api.relationshipdefs rd on rd.RelationshipDefId = a.RelationshipDefId
    group by r.count, a.RelationshipId, rd.name, a.FromObjectId, a.ToObjectId;

  for i in 1 .. t_RelIds.count loop
    insert into rel.storedrelationships_t
    (
    relationshipid,
    endpointid,
    fromobjectid,
    toobjectid 
    )
    values
    (
    t_RelIds(i),
    t_EndPointId,
    t_ToObjectIds(i),
    t_FromObjectIds(i)
    );
  end loop;
  commit;
end;