PL/SQL Developer Test script 3.0
23
declare
  t_NoteId         pls_integer;
  t_NoteDefId      pls_integer;
  t_SystemObjectId pls_integer;

begin
  
  select n.NoteDefId
    into t_NoteDefId
    from api.NoteDefs n
    where n.Description = 'Municipal License Email';

  select s.ObjectId
    into t_SystemObjectId
    from query.o_systemsettings s;
    
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  t_NoteId := api.pkg_noteupdate.New(t_SystemObjectId,t_NoteDefId,'N','<img alt="Email Header" src="https://demo.computronix.com/abcdemo/pub/images/EmailHeader.jpg"><br><p><font face="arial">A new License has been approved in your municipality.<br></font><br><font face="verdana"><strong>License Information:</strong></font></p><table style="width: 600px; color: rgb(0, 0, 0); font-size: 12px;" border="1" cellspacing="2" cellpadding="2"><tbody><tr><td><font face="verdana"><font face="arial">Licensee:<br></font></font></td><td><font face="verdana"><strong><font color="#ff6600">{Licensee}</font></strong><br></font></td></tr><tr><td><font face="verdana"><font face="arial">License Number:<br></font></font></td><td><font face="verdana"><strong><font color="#ff6600">{LicenseNumber}</font></strong><br></font></td></tr><tr><td></td><td><br></td></tr></tbody></table>');
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  
end;
0
0
