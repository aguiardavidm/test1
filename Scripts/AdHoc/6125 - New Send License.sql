PL/SQL Developer Test script 3.0
53
-- Created on 7/31/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  t_ChangeStatus    number := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus');
  t_ReasonForChange number := api.pkg_configquery.ColumnDefIdForName('p_ChangeStatus', 'ReasonForChange');
  t_Outcome         number := api.pkg_configquery.ColumnDefIdForName('p_ChangeStatus', 'Outcome');
  t_ProcessId       number;
  t_SendLicense     number := api.pkg_configquery.ObjectDefIdForName('p_ABC_SendLicense');
  a_JobId           number := :a_JobId;
  t_count           number:=0;
  
begin
  -- Test statements here
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for c in (select ra.ExternalFileNum, ra.LicenseNumber, ra.StatusDescription,  api.pkg_columnquery.Value(ra.LicenseObjectId, 'State') LICENSESTATE, ra.CompletedDate, ra.JobId
              from query.j_Abc_Renewalapplication ra
             where ra.StatusDescription in ('Approved')
               and api.pkg_columnquery.Value(ra.LicenseObjectId, 'State') not in  ('Active', 'Closed')--ra.CompletedDate > to_date('07/29/2015','MM/DD/YYYY');
            order by 1) loop
  
  a_JobId := c.jobid;
  
  t_ProcessId := api.pkg_processupdate.New(a_JobId,
                                           t_ChangeStatus,
                                           'Change Status to Correct Un-Activated License',
                                           null,
                                           null,
                                           null);
  
  
  api.pkg_columnupdate.SetValue(t_ProcessId, t_ReasonForChange,'Change Status to Correct Un-Activated License');
  
  api.pkg_columnupdate.SetValue(t_ProcessId, t_Outcome, 'Distribute');
                            
  t_ProcessId := api.pkg_processupdate.New(a_JobId,
                                           t_SendLicense,
                                           'Send License Process to Correct License Certificate for Un-Activated License',
                                           null,
                                           null,
                                           null);     
                                           
  t_count := t_count +1;
    if mod(t_count, 10) = 0 then
    api.pkg_logicaltransactionupdate.EndTransaction();
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();    
    end if;                                                           
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  
end;
1
a_JobId
0
5
0
