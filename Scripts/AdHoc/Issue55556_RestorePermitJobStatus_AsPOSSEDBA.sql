/*-----------------------------------------------------------------------------
 * Modifying Author: Maria Knigge
 * Expected run time:
 * Purpose: To remove the open Send Denial Notification process, change the Job
 *  Status to In Review and add Administrative Review processes to Job in
 *  NJPROD.
 *  For Issue 55556, when running script in NJPROD, set...
 *     t_JobNumber := '370989'
 *     t_ProcessTypeToRemove t:= 'p_ABC_SendPermitDenialNotif'
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_ChangeStatus                        number;
  t_IssueNumber                         varchar2(6) := '55556';
  t_JobId                               number;
  t_JobNumber                           varchar2(40) := '370989';
  t_NoteDefId                           pls_integer;
  t_NoteId                              pls_integer;
  t_NoteTag                             varchar2(6) := 'GEN';
  t_NoteText                            varchar2(4000);
  t_ProcessId                           pls_integer;
  t_ProcessToRemoveId                   number;
  t_ProcessTypesToAdd                   udt_IdList;
  t_ProcessTypeToRemove                 varchar2(30) := 'p_ABC_SendPermitDenialNotif';
  t_ProcessTypeToRemoveId               number;
begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  t_ChangeStatus := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');

  -- Get ProcessTypeId for Processes to Add / Remove
  select pt.ProcessTypeId
  bulk collect into t_ProcessTypesToAdd
  from api.processtypes pt
  where pt.Name in ('p_ABC_AdministrativePermReview');

  select pt.ProcessTypeId
  into t_ProcessTypeToRemoveId
  from api.processtypes pt
  where pt.Name = t_ProcessTypeToRemove;

  -- Get JobId and ProcessId of the Process to be removed
  select
    x.ObjectId,
    p.ProcessId
  into
    t_JobId,
    t_ProcessToRemoveId
  from
    table(api.pkg_SimpleSearch.CastableObjectsByIndex('j_ABC_PermitApplication',
        'ExternalFileNum', t_JobNumber)) x
    join api.processes p
        on x.ObjectId = p.JobId
  where p.ProcessTypeId = t_ProcessTypeToRemoveId;

  dbms_output.put_line('JobId to be updated: ' || t_JobId);

  -- Create Note and Populate Text
  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;
  t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
      'Job Completed with wrong outcome. See Issue ' || t_IssueNumber || '.';
  t_NoteId := api.pkg_NoteUpdate.New(t_JobId, t_NoteDefId, 'Y', t_NoteText);

  -- Remove unwanted Process
  api.pkg_ProcessUpdate.Remove(t_ProcessToRemoveId);

  -- Change Job Status
  t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ChangeStatus, null, null, null, null);
  api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ReasonForChange', t_NoteText);
  api.pkg_ProcessUpdate.Complete(t_ProcessId, 'In Review');

  -- Add new Processes
  for i in 1..t_ProcessTypesToAdd.count() loop
    t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ProcessTypesToAdd(i), null, null, null, null);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();

  --commit;

end;
