-- Created on 4/1/2016 by MICHEL.SCHEFFERS 
-- Apply Instance Security on CPL Templates
-- Issue 8710
declare 
  -- Local variables here
  t_Documents integer := 0;

begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all CPL Templates
  for i in (select RelationshipId
              from query.r_ABC_WineSpiritsCPLTemplate
            union
            select RelationshipId
              from query.r_ABC_ComboCPLTemplate
            union
            select RelationshipId
              from query.r_Abc_Ripcpltemplate
            union
            select RelationshipId
              from query.r_Abc_Beercpltemplate
            ) loop
    -- Apply Instance Security for each template found
    t_Documents := t_Documents + 1;
    abc.pkg_ABC_CPL.CPLDocumentInstanceSecurity(i.RelationshipId, sysdate);
  end loop;
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbms_output.put_line('CPL Templates updated: ' || t_Documents);
end;
