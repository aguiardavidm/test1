-- Created on 03/23/2017 by Dennis.OConnor 
-- Modify products NonRenewalNoticeDate and NonRenewalReviewDate for Non Renewed Products with Expiration of  12/31/2015

-- Create table
--drop table possedba.LOG_Products_Update_NonRenewalNotice_Date;
create table possedba.LOG_Products_Update_NonRenewalNotice_Date
(
  objectId               NUMBER,
  productid              VARCHAR2(20),
  expiration_date        DATE,
  current_NonRenew_Date  DATE,
  new_NonRenew_Date      DATE,
  current_Review_Date    DATE,
  new_Review_Date        DATE
);

declare 
  current_NonRenew_Date DATE;
  current_Review_Date DATE;

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(NULL);

  for p in (SELECT pp.ObjectId
            FROM query.o_abc_product pp 
            WHERE pp.ExpirationDate = to_date('12/31/2015', 'mm/dd/yyyy')
            AND pp.State = 'Current'
            MINUS
            SELECT DISTINCT (prp.ProductId) 
            FROM query.r_abc_productrenewaltoproduct prp) loop

     current_NonRenew_Date := api.pkg_columnquery.DateValue(p.objectid, 'NonRenewalNoticeDate');
     current_Review_Date := api.pkg_columnquery.DateValue(p.objectid, 'NonRenewalreviewDate');

      api.pkg_columnupdate.SetValue (p.objectid, 'NonRenewalNoticeDate', to_date('03/01/2017','mm/dd/yyyy'));
      api.pkg_columnupdate.SetValue (p.objectid, 'NonRenewalreviewDate', to_date('03/01/2017','mm/dd/yyyy'));
      dbms_output.put_line ('Updating NonRenewalNoticeDate for ' || api.pkg_columnquery.Value (p.objectid,'RegistrationNumber') || '-' || api.pkg_columnquery.Value (p.objectid,'Name'));

     insert into possedba.LOG_PRODUCT_RETIREMENT
      (objectId, productid, expiration_date, current_NonRenew_Date, new_NonRenew_Date, current_Review_Date, new_Review_Date)
      values
      (p.objectid,
       api.pkg_columnquery.value(p.objectid, 'RegistrationNumber'),
       api.pkg_columnquery.DateValue(p.objectid, 'ExpirationDate'),
       current_NonRenew_Date,
       api.pkg_columnquery.DateValue(p.objectid, 'NonRenewalNoticeDate'),
       current_Review_Date,
       api.pkg_columnquery.DateValue(p.objectid, 'NonRenewalreviewDate')
      );
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
 -- commit;  
end;