begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select relationshipid
              from api.relationships r
             where r.FromObjectId = 29624582) loop
    delete from rel.storedrelationships r 
     where r.RelationshipId = i.relationshipid;
    delete from possedata.objects o
     where o.ObjectId = i.relationshipid;
  end loop;
    delete from objmodelphys.notes o
     where o.ObjectId = 29624582;
    delete from possedata.objects o
     where o.ObjectId = 29624582;
  
  api.pkg_columnupdate.SetValue(29623282, 'State', 'Active');
  api.pkg_columnupdate.SetValue(29623282, 'IsLatestVersion', 'Y');
  
  api.pkg_logicaltransactionupdate.EndTransaction;

end;
