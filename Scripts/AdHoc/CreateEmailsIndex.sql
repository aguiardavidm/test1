---------------------------------------------------------------------------------------------------
-- Author: MikeM
-- Purpose: Create an index on the emails_t table to improve performance. 
--
-- Run as the possedba user.
-- Run time: < 1 minute.
---------------------------------------------------------------------------------------------------
declare
  t_Count                               number;
  t_NextNumber                          number;
begin
  if user() != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20000, 'This script is to be run as the POSSEDBA user.');
  end if;
 
  select count(*)
  into t_Count
  from dba_ind_columns
  where table_owner = 'EXTENSION'
    and table_name = 'EMAILS_T'
    and column_name = 'CREATEDDATE';

  if t_Count = 0 then    
    select to_number(substr(max(index_name), -1, 1)) + 1
    into t_NextNumber
    from dba_indexes
    where table_owner = 'EXTENSION'
      and table_name = 'EMAILS_T'
      and index_name like 'EMAILS_IX__'; 

    execute immediate
        'create index extension.emails_ix_' || t_NextNumber || ' ' ||
        'on extension.emails_t ( ' ||
        '  createddate ' ||
        ') tablespace largeindexes';
  end if;
end;
/
