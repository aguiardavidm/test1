declare
  t_ObjectId number;
  t_RelId    number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_Role'), sysdate);
  api.pkg_columnupdate.SetValue(t_ObjectId, 'Name', 'CPL Reviewer');
  t_RelId := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName('o_Role', 'AccessGroup'),
                                            t_ObjectId,
                                            api.pkg_simplesearch.ObjectByIndex('o_AccessGroup', 'Description', 'CPL Reviewer'));
  t_RelId := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName('o_Role', 'AccessGroup'),
                                            t_ObjectId,
                                            api.pkg_simplesearch.ObjectByIndex('o_AccessGroup', 'Description', 'Licensing Read Only'));
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;