declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_PermitType', 'ReviewRequired');
  c integer := 0;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select v.ObjectId
              from query.o_ABC_PermitType v
              where v.ReviewRequired = 'N') loop
       c := c + 1;
    insert into possedata.objectcolumndata_t cd
           (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue)
           Values (api.pkg_logicaltransactionupdate.CurrentTransaction, i.objectid, t_columndefid,'Y')
              ;
  end loop;
  dbms_output.put_line('Records : ' || c);
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;