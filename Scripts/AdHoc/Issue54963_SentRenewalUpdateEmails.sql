-- Created on 5/1/2020 by BENJAMIN 
declare
  t_NewSubjectline varchar2(4000);
  t_NewBody        clob;
  t_NewMessage     clob;
  t_newToAddress   varchar2(4000);
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  -- Non-MAID cards
  for c in (
      select p.permittype, p.PermitTypeObjectId, t.*
      from extension.emails_t t
      join query.o_ABC_Permit p on p.objectid = t.relatedobjectid
      where t.subject = 'Permit Renewal Notification'
        and t.createddate >= trunc(sysdate)
        and p.PermitTypeObjectId != 22162921
  ) loop

    select 'benjamin.albrecht@computronix.com;paul.orstad@computronix.com' newtoAddress, --bentest --Remove for Prod
           replace(replace(replace(replace(e.Message, 'http://www.nj.gov/lps/abc/posse/images/EmailHeader.jpg', 'https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg') 
           ,'will expire on June 30, 2020.', 'indicates that it will expire on June 30, 2020 <b>(NOW SEPTEMBER 30, 2020).</b>'),
           'This is to notify you that your permit is about to expire. To renew, please sign on to our public site at <a href="https://abc.lps.nj.gov/ABCPublic/Login.aspx">https://abc.lps.nj.gov/ABCPublic/Login.aspx</a> or contact an ABC representative. <strong>DO NOT APPLY NEW</strong>.',
           'Please note that the <strong>ABC Director issued an Order (AO 2020-02) extending the 2019-2020 term and the renewal filing dates for the 2020-2021 term to September 30, 2020, due to Covid-19.</strong>  A copy of the Order may be found on our website: <a href="https://www.nj.gov/oag/abc/downloads/Order-extending-2019-20-municipal-and-state-liquor-licenses-and-permits-to-Sept-30-2020.pdf">https://www.nj.gov/oag/abc/downloads/Order-extending-2019-20-municipal-and-state-liquor-licenses-and-permits-to-Sept-30-2020.pdf</a>'),
           'The Division will not act upon the renewal of your permit until it has verified that your company has submitted their 2020-2021 license renewal.',
           'You may complete your permit renewal at any time prior to September 30, 2020, however the Division will not act upon the renewal application until it has verified that your company has submitted their 2020-2021 license renewal.  License renewals are expected to go out within the next few weeks.')
           NewMessage,
           replace(e.subject, 'Permit Renewal Notification', 'Permit Renewal Notification - UPDATE') NewSubject
    into
      t_newToAddress,--bentest --Remove for Prod
      t_NewMessage,
      t_NewSubjectLine
    from extension.emails_t e
    where e.objectid = c.objectid;

    extension.pkg_SendMail.SendEmail(c.relatedobjectid, c.fromaddress, c.toaddress,
        null, null, t_NewSubjectLine, t_NewMessage, null, 'Y', 'Y');
        
        
    dbms_output.put_line('Email Message updated and scheduled to resend: ' || c.objectid);
  end loop;

-- MAID Cards
  for i in (
      select p.permittype, p.PermitTypeObjectId, t.*
      from extension.emails_t t
      join query.o_ABC_Permit p on p.objectid = t.relatedobjectid
      where t.subject = 'Permit Renewal Notification'
        and t.createddate >= trunc(sysdate)
        and p.PermitTypeObjectId = 22162921
    ) loop

    select 'benjamin.albrecht@computronix.com;paul.orstad@computronix.com' newtoAddress, --bentest --Remove for Prod
           replace(replace(replace(replace(replace(e.Message, 'http://www.nj.gov/lps/abc/posse/images/EmailHeader.jpg', 'https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg') 
           ,'will expire on June 30, 2020.', 'indicates that it will expire on June 30, 2020 <b>(NOW SEPTEMBER 30, 2020).</b>'),
           'This is to notify you that your permit is about to expire. To renew, please sign on to our public site at <a href="https://abc.lps.nj.gov/ABCPublic/Login.aspx">https://abc.lps.nj.gov/ABCPublic/Login.aspx</a> or contact an ABC representative. <strong>DO NOT APPLY NEW</strong>.',
           'Please note that the <strong>ABC Director issued an Order (AO 2020-02) extending the 2019-2020 term and the renewal filing dates for the 2020-2021 term to September 30, 2020, due to Covid-19.</strong>  A copy of the Order may be found on our website: <a href="https://www.nj.gov/oag/abc/downloads/Order-extending-2019-20-municipal-and-state-liquor-licenses-and-permits-to-Sept-30-2020.pdf">https://www.nj.gov/oag/abc/downloads/Order-extending-2019-20-municipal-and-state-liquor-licenses-and-permits-to-Sept-30-2020.pdf</a>'),
           'The Division will not act upon the renewal of your permit until it has verified that your company has submitted their 2020-2021 license renewal.',
           '<strong>YOU MUST APPLY NEW. THE MARKETING AGENT ID CARD IS NOT RENEWABLE.</strong><br><br>You may complete your permit application at any time prior to September 30, 2020, however the Division will not act upon the permit application until it has verified that your company has submitted their 2020-2021 license renewal and 2020-2021 Marketing Agent permit renewal. License renewals are expected to go out within the next few weeks.'),
         '<strong>CONSUMER TASTING FOR WHOLESALE LICENSEES, AND OFF PREMISE STORAGE OF RECORDS:</strong> <p>* PLEASE NOTE THAT ADDITIONAL NOTIFICATIONS WILL BE SENT FOR PERMITS THAT EXPIRE ON LATER DATES (I.E. TRANSIT INSIGNIA) </p>', '')
           NewMessage,
           replace(e.subject, 'Permit Renewal Notification', 'Permit Renewal Notification - UPDATE - MARKETING AGENT ID CARD') NewSubject
    into
      t_newToAddress,--bentest --Remove for Prod
      t_NewMessage,
      t_NewSubjectLine
    from extension.emails_t e
    where e.objectid = i.objectid;

    extension.pkg_SendMail.SendEmail(i.relatedobjectid, i.fromaddress, i.toaddress,
        null, null, t_NewSubjectLine, t_NewMessage, null, 'Y', 'Y');
        
    dbms_output.put_line('Email Message updated and scheduled to resend: ' || i.objectid);
  end loop;

api.pkg_logicaltransactionupdate.EndTransaction;
end;
