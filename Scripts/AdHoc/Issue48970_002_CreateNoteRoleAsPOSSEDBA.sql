/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 seconds
 * Purpose: Create role for CPL Notes access group.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_LogicalTransactionId                number;
  t_RolesCreated                        udt_StringList;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

  procedure CreateRoleAndRel (
    a_AccessGroupName                   varchar2
  ) is
    t_AccessGroupId                     number;
    t_EndpointId                        number
        := api.pkg_configquery.EndPointIdForName('o_Role', 'AccessGroup');
    t_ObjectDefId                       number
        := api.pkg_ConfigQuery.ObjectDefIdForName('o_Role');
    t_ObjectId                          number;
    t_RelId                             number;
  begin

    t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId, sysdate);
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Name', a_AccessGroupName);
    t_AccessGroupId := api.pkg_SimpleSearch.ObjectByIndex('o_AccessGroup', 'Description', a_AccessGroupName);
    t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, t_ObjectId, t_AccessGroupId);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  CreateRoleAndRel('CPL Notes');

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

  select r.Name
  bulk collect into t_RolesCreated
  from
    api.LogicalTransactions lt
    join api.Objects o
        on lt.LogicalTransactionId = o.LogicalTransactionId
    join query.o_Role r
        on o.ObjectId = r.ObjectId
  where lt.LogicalTransactionId = t_LogicalTransactionId;

  dbug('The following Roles were created:');
  for i in 1..t_RolesCreated.count() loop
    dbug(t_RolesCreated(i));
  end loop;

  commit;

end;