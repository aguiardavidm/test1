  -- Remove vehicles from License 3402-13-095-001 (objectid ).
  -- Add vehicles to License 3402-13-095-001 (objectid ) from Renewal Job 35349 (objectid ).
  -- 01/22/2016
declare 
  t_LicenseId      number (9) := 33149469;-- objectid of new License 3402-13-095-001
  t_RenewalId      number (9) := 33148481;-- objectid of Renewal job 35349
  t_EndPointId     number (9) := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Vehicle');
  t_RelId          number (9);
  t_Count          integer := 0;
begin

  api.pkg_logicaltransactionupdate.ResetTransaction();

  -- Find Payments with more than 1 Receipt Document
  for r in (select lv.RelationshipId, lv.VehicleId
              from query.r_Abc_Licensevehicle lv
             where lv.LicenseId = t_LicenseId
           ) loop                               
  -- Use the found RelationshipId as parameter:
     dbms_output.put_line('Removing vehicle ' || r.vehicleid);
     api.pkg_relationshipupdate.Remove(r.RelationshipId);
     t_Count := t_Count + 1;
  end loop;
  dbms_output.put_line('Vehicles removed ' || t_Count);
  
  t_Count := 0;
  for r in (select rv.VehicleId
              from query.r_abc_renewalvehicle rv
             where rv.RenewalApplicationId = t_RenewalId
           ) loop
     t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_LicenseId, r.Vehicleid);
     dbms_output.put_line('https://posseapp.lps.state.nj.us/ABCInternal/Default.aspx?PossePresentation=NewVehicle&PosseObjectId=' || r.Vehicleid);
     t_Count := t_Count + 1;
  end loop;
  dbms_output.put_line('Vehicles added ' || t_Count);
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;
