-- Created on 05/26/2017 by MICHEL.SCHEFFERS 
-- Issue 30032 - License number is at the wrong generation because of amendment done to wrong License - Fix renewal job
declare 
  -- Local variables here
  t_LicenseId_006 number := 39516046; -- Same ID in UAT
  t_LicenseId_005 number := 36133361; -- Same ID in UAT
  t_RenewalJob    number := 41260143; -- UAT Job ID is 40382522
  t_EndpointId    number := api.pkg_configquery.EndPointIdForName('j_ABC_RenewalApplication', 'LicenseToRenew');
  t_RelId         number;
  t_LicenseId     number;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();

-- Remove license 1432-33-005-006 from Renewal job
  dbms_output.put_line('Removing license 1432-33-005-006 (' || t_LicenseId_006 || ') from Renewal job ' || t_RenewalJob);
  select r.RelationshipId
  into   t_RelId
  from   query.r_ABC_LicenseToRenewJobLicense r
  where  r.LicenseObjectId = t_LicenseId_006
  and    r.RenewJobId = t_RenewalJob;
  api.pkg_relationshipupdate.Remove(t_RelId);
-- Remove pending license 1432-33-005-006 from Renewal job
  dbms_output.put_line('Removing pending license 1432-33-005-006 from Renewal job ' || t_RenewalJob);
  select r.RelationshipId
  into   t_RelId
  from   query.r_ABC_RenewAppJobLicense r
  where  r.RenewAppJobId = t_RenewalJob;
  api.pkg_relationshipupdate.Remove(t_RelId);

-- Add license 1432-33-005-005 to Renewal job
  dbms_output.put_line('Adding license 1432-33-005-005 (' || t_LicenseId_005 || ') to Renewal job ' || t_RenewalJob);
  t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_RenewalJob, t_LicenseId_005);
-- Create pending license 1432-33-005-005 to Renewal job
  dbms_output.put_line('Creating pending license 1432-33-005-005 to Renewal job ' || t_RenewalJob);
  abc.pkg_abc_workflow.CopyLicense(t_RenewalJob, sysdate);
  select r.LicenseObjectId
  into   t_LicenseId
  from   query.r_ABC_RenewAppJobLicense r
  where  r.RenewAppJobId = t_RenewalJob;
  api.pkg_columnupdate.SetValue(t_LicenseId, 'State', 'Pending');

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;