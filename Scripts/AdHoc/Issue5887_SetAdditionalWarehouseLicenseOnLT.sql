-- Created on 4/20/2016 by MICHEL.SCHEFFERS 
-- Set detail Valid For Additional Warehouse License of License Type to true for certain License Types.
-- Issue 5887
declare 
  -- Local variables here
  t_Jobs integer := 0;

begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all License Types
  for i in (select lt.ObjectId
              from query.o_abc_licensetype lt
             where lt.Code in ('07','08','09','10','11','15','16','17','18','19','21','22','23','25','26','41')
            ) loop
    -- Apply Instance Security for each job
    t_Jobs := t_Jobs + 1;
    api.pkg_columnupdate.SetValue(i.objectid, 'ValidForAdditionalWarehouseLic', 'Y');
  end loop;
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbms_output.put_line('License Types updated: ' || t_Jobs);
end;
