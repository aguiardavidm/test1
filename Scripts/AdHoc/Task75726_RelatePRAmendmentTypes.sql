-- This script will relate an amendment type to all existing PR Amendments that do not already have a type.
--  Est Runtime: 20-30 minutes
declare
  t_PRAmendJobs           api.pkg_Definition.udt_Idlist;
  t_AddDistributorsType   api.pkg_definition.udt_Id;
  t_AdminAmendmentType    api.pkg_definition.udt_Id;
  t_AmendmentToTypeEP     api.pkg_definition.udt_Id;
  t_RelId                 api.pkg_definition.udt_Id;
  t_Count                 number := 0;
  t_ErrorMessage          varchar2(4000);
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20001, 'Must be run as POSSEDBA');
  end if;
  
  select objectid
    bulk collect into t_PRAmendJobs
    from api.objects o
   where o.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('j_ABC_PRAmendment');
   
  t_AddDistributorsType := api.pkg_search.ObjectByColumnValue('o_ABC_ProductAmendmentType', 'Name', 'Add Distributor');
  t_AdminAmendmentType  := api.pkg_search.ObjectByColumnValue('o_ABC_ProductAmendmentType', 'Name', 'Administrative Amendment');
  t_AmendmentToTypeEP   := api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment', 'ProductAmendmentType');
    
  for i in 1..t_PRAmendJobs.count loop
    begin
      if api.pkg_columnquery.value(t_PRAmendJobs(i), 'ProductAmendmentType') is null 
          and api.pkg_columnquery.value(t_PRAmendJobs(i), 'IsAmendment') = 'Y' then
        if api.pkg_columnquery.value(t_PRAmendJobs(i), 'EnteredOnline') = 'Y' then
          t_RelId := api.pkg_relationshipupdate.New(t_AmendmentToTypeEP, t_PRAmendJobs(i), t_AddDistributorsType);
          t_Count := t_Count + 1;
        else
          t_RelId := api.pkg_relationshipupdate.New(t_AmendmentToTypeEP, t_PRAmendJobs(i), t_AdminAmendmentType);
          t_Count := t_Count + 1;
        end if;
      end if;
      exception when others then
        t_ErrorMessage := sqlerrm;
        dbms_output.put_line('Error relating Amendment Type to jobid: ' || t_PRAmendJobs(i));
      end;
    if mod(t_Count, 100) = 0 then
      begin
        api.pkg_logicaltransactionupdate.EndTransaction;
        commit;
        exception when others then
          t_ErrorMessage := sqlerrm;
          dbms_output.put_line('Error commiting batch of jobs: ' || t_ErrorMessage);
          rollback;
          api.pkg_logicaltransactionupdate.ResetTransaction;
        end;
      api.pkg_logicaltransactionupdate.ResetTransaction;
    end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;

