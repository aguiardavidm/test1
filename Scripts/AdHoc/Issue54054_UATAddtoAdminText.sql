-- Created on 7/17/2020 by Benjamin
-- Est runtime <5 minutes
declare 
  t_systemsettingsid number(9);
begin
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20002, 'Must be run as POSSEDBA');
  end if;
  
  --find system settings detail
  select objectid 
    into t_systemSettingsId
    from query.o_systemsettings s
   where rownum < 2;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  
  dbms_output.put_line('Initialize Details');
  
  api.pkg_columnupdate.SetValue(t_systemSettingsId, 'OnlinePayNowButtonText', 'Pay Now '|| chr(38) ||' Submit Application');
  api.pkg_columnupdate.SetValue(t_systemSettingsId, 'OnlinePayLaterButtonText', 'Pay Later/Add to Pay All Fees');
  api.pkg_columnupdate.SetValue(t_systemSettingsId, 'OnlineRemoveFromPayLaterButton', 'Remove from Pay Later/Add to Pay All Fees');
  api.pkg_columnupdate.SetValue(t_systemSettingsId, 'OnlineSubmitNoFeeButtonText', 'Submit');
  
  
  api.pkg_columnupdate.SetValue(t_systemSettingsId, 'OnlinePayNowText', 'Press the Pay Now button to pay your fees and submit your application for processing');
  api.pkg_columnupdate.SetValue(t_systemSettingsId, 'OnlinePayLaterText', 'Press the Pay Later/Add to Pay All Fees below if you wish to make a payment and submit your application for processing along with additional pending fees at a later time using Pay All Fees from the main menu');
  api.pkg_columnupdate.SetValue(t_systemSettingsId, 'OnlineRemoveFromPayLaterText', 'By clicking below, the application will be removed from Pay All Fees and the application will be available in the draft section for further action.');
  api.pkg_columnupdate.SetValue(t_systemSettingsId, 'OnlineSubmitNoFeeText', 'Press the button below to submit your request for processing.');
    
  dbms_output.put_line('Details updated');
  api.pkg_LogicalTransactionUpdate.EndTransaction;
--   commit;
--   rollback;
end;
