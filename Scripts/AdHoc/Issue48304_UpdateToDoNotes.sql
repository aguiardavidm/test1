declare
  t_ToDoNoteIds    api.pkg_Definition.udt_IdList;
  t_JobIds         api.pkg_Definition.udt_IdList;
  t_Count          number := 0;
begin
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20000, 'Must be run as POSSEDBA');
  end if;

  api.pkg_LogicalTransactionUpdate.StartTransaction;

  select t.objectid, p.jobid
    bulk collect into t_ToDoNoteIds, t_ProcessIds
    from query.o_abc_todonote t
    join api.processes p
        on p.ProcessId = t.processid
   where p.outcome is null
     and t.jobid is null;

  for i in 1..t_ToDoNoteIds.count loop
    t_Count := t_Count+1;
    api.pkg_ColumnUpdate.setvalue(t_ToDoNoteIds(i), 'JobId', t_JobIds(i));
  end loop;
  dbms_output.put_line(t_Count || ' To Do Notes Updated.');
  api.pkg_LogicalTransactionUpdate.EndTransaction;
end;
