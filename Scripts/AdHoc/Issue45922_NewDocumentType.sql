declare
  t_DocObjId api.pkg_definition.udt_id;
  t_DocObjType api.pkg_definition.udt_id;

begin
  t_DocObjType := api.pkg_configquery.ObjectDefIdForName('o_ABC_DocumentType');
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  t_DocObjId := api.pkg_objectupdate.New(t_DocObjType);
  api.pkg_columnupdate.SetValue(t_DocObjId, 'Name', 'Issuing Authority Document');
  api.pkg_columnupdate.SetValue(t_DocObjId, 'Active', 'Y');
  api.pkg_LogicalTransactionUpdate.EndTransaction(); commit; 
end;