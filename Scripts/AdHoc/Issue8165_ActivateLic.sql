declare
  t_ActivateLic number := 31866534;
  t_DeactivateLic number := 31130727;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  api.pkg_columnupdate.SetValue(t_ActivateLic, 'State', 'Active');
  api.pkg_columnupdate.SetValue(t_ActivateLic, 'IsLatestVersion', 'Y');
  
  --deactivate the current license
  api.pkg_columnupdate.SetValue(t_DeactivateLic, 'State', 'Closed');
  api.pkg_columnupdate.SetValue(t_DeactivateLic, 'IsLatestVersion', 'N');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;