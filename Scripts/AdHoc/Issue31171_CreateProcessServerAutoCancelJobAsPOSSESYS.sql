/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 1 sec
 * Purpose: This script inserts a daily job (11:00 PM) that cancels all jobs
 *   with Automatic Cancellation enabled that have exceeded the cancellation
 *  period. Script must be run as POSSESYS.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Frequency is api.pkg_Definition.udt_Frequency;

  t_Frequency                           udt_Frequency;
  t_ScheduleId                          number;
begin

  if user != 'POSSESYS' then
    raise_application_error(-20000, 'Script must be run as POSSESYS.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  t_Frequency := api.pkg_Definition.gc_Daily;
  t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure(
      'abc.pkg_ABC_Jobs.AutoCancelJobs',
      'Auto Cancel Jobs',
      t_Frequency,
      null,
      null,
      null,
      'N',
      'Y',
      'Y',
      'Y',
      'Y',
      'Y',
      'Y',
      'Y',
      'Y',
      'N',
      -- Run at 11:00 PM daily
      trunc(sysdate) + 23/24,
      null,
      null,
      null,
      api.pkg_Definition.gc_normal);

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
