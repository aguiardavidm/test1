-- Script to set the License Tap Information Label on all Permit Types
declare
  t_IdList api.pkg_Definition.udt_IdList;
begin
  select objectid
  bulk collect into t_IdList
  from query.o_ABC_PermitType;
  
  api.pkg_logicaltransactionupdate.StartTransaction();
  
  for i in 1..t_IdList.Count() loop
    api.pkg_columnUpdate.SetValue(t_IdList(i), 'LicenseTapInformationLabel', 'Seller');
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
