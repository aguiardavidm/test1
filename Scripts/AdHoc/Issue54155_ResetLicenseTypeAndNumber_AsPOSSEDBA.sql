/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 sec.
 * Purpose: On License #0514-33-004-007 and Licenses 0514-38-004-004, -005,
 *   -006, set the License Type to '32' instead of '33' or '38', then set
 *   License Number to use '32' instead of '33' or '38'. This script also
 *   also creates a case note on the License Object to indicate the change
 *   made.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_Counter                             pls_integer := 0;
  t_EndpointId                          pls_integer;
  t_LicenseCode                         varchar2(20) := '32';
  t_LicenseIds                          udt_IdList;
  t_LicenseNumbers                      udt_StringList;
  t_LicenseTypeId                       pls_integer;
  t_NoteDefId                           pls_integer;
  t_NoteId                              pls_integer;
  t_NoteTag                             varchar2(6) := 'GEN';
  t_NoteText                            varchar2(4000);
  t_RelationshipId                      pls_integer;
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  t_EndpointId := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'LicenseType');

  t_LicenseNumbers(1) := '0514-33-004-007';
  t_LicenseNumbers(2) := '0514-38-004-004';
  t_LicenseNumbers(3) := '0514-38-004-005';
  t_LicenseNumbers(4) := '0514-38-004-006';

  for i in 1..t_LicenseNumbers.count() loop
    t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
        'System corrected License Type from ''' || substr(t_LicenseNumbers(i), 6, 2) || ''' to ''';
    t_Counter := 0;

    select l.ObjectId
    bulk collect into t_LicenseIds
    from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber',
        t_LicenseNumbers(i))) l;

    for j in 1..t_LicenseIds.count() loop
      t_RelationshipId := null;
      t_LicenseTypeId := extension.pkg_ObjectQuery.RelatedObject(t_LicenseIds(j), 'LicenseType');

      -- Get RelationshipId to incorrect License Type
      if t_LicenseTypeId is not null then
        select r.RelationshipId
        into t_RelationshipId
        from api.relationships r
        where r.FromObjectId = t_LicenseIds(j)
          and r.ToObjectId = t_LicenseTypeId
          and r.EndpointId = t_EndpointId;
      end if;

      if t_RelationshipId is not null then
        api.pkg_RelationshipUpdate.Remove(t_RelationshipId);
      end if;

      -- Get New License Type
      select lt.ObjectId
      into t_LicenseTypeId
      from query.o_ABC_LicenseType lt
      where lt.Code = t_LicenseCode;

      -- Calculate new License Number
      t_LicenseNumbers(i) := substr(t_LicenseNumbers(i), 0, 5) || t_LicenseCode ||
          substr(t_LicenseNumbers(i), 8);
      t_RelationshipId := api.pkg_RelationshipUpdate.New(t_EndpointId, t_LicenseIds(j),
          t_LicenseTypeId);
      api.pkg_ColumnUpdate.SetValue(t_LicenseIds(j), 'LicenseNumber', t_LicenseNumbers(i));
      t_Counter := t_Counter + 1;

      -- Insert Case Note on License to document change
      select nd.NoteDefId
      into t_NoteDefId
      from api.NoteDefs nd
      where nd.Tag = t_NoteTag;

      if t_Counter = 1 then
          t_NoteText := t_NoteText || t_LicenseCode || ''' by issue 54155.';
      end if;
      t_NoteId := api.pkg_NoteUpdate.New(t_LicenseIds(j), t_NoteDefId, 'Y', t_NoteText);
    end loop;
  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

end;

