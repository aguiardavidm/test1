-- Created on 7/30/2015 by MICHEL.SCHEFFERS 
-- Fix Duplicate Insignia Number issue for Permit 20036
declare 
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  -- Local variables here
  t_PrevInsigniaNumber     number (10);
  t_NextInsigniaNumber     number (10);
  t_PermitObjectId         number := 31421752; -- objectid for Permit 20036 in Production
  t_VehicleId              number;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  t_PrevInsigniaNumber := 0;
  select max(v.InsigniaNumber)
    into t_NextInsigniaNumber
    from query.o_abc_vehicle v 
    join query.r_abc_permitvehicle pv on pv.PermitObjectId = t_PermitObjectId
                                     and pv.VehicleObjectId = v.ObjectId;
  dbms_output.put_line ('Max Insignia Number ' || t_NextInsigniaNumber);
  for v1 in (select pv.VehicleObjectId, pv.PermitObjectId, v.InsigniaNumber, v.VIN
               from query.r_abc_permitvehicle pv
               join query.o_abc_vehicle v on v.ObjectId = pv.VehicleObjectId
              where pv.PermitObjectId = t_PermitObjectId
              order by v.InsigniaNumber
            )
            loop
              if t_PrevInsigniaNumber = v1.Insignianumber then
                 dbms_output.put_line ('VIN Number ' || v1.VIN);
                 dbms_output.put_line ('Old Insignia Number ' || v1.InsigniaNumber);
                 t_NextInsigniaNumber := t_NextInsigniaNumber + 1;
                 dbms_output.put_line ('New Insignia Number ' || t_NextInsigniaNumber);
                 api.pkg_columnupdate.SetValue(v1.Vehicleobjectid, 'InsigniaNumber', t_NextInsigniaNumber);
              end if;
              t_PrevInsigniaNumber := v1.InsigniaNumber;
            end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
