-- Created on 01/12/2016 by MICHEL.SCHEFFERS 
-- Create new relationship Permit - License. A permit will be associated with all non-pending Licenses with the same License Number.
declare 
  -- Local variables here
  i                  integer := 0;
  r                  integer := 0;
  t_RelCount         number;
  t_RelId            number;
  t_PrevLicNum       varchar2(100) := ' ';
  t_LicenseObjectId  api.pkg_Definition.udt_idlist;
  t_PermitLicRelDef  number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitGenerationLicense');
  t_PermitLicenseEP  number := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitGenerationalLicense');
  t_LicensePermitEP  number := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'GenerationalLicensePermit');  
  
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  for p in (select pl.PermitObjectId, pl.LicenseObjectId, api.pkg_columnquery.Value(pl.LicenseObjectId,'LicenseNumber') LicenseNumber
              from query.r_PermitLicense pl
             order by api.pkg_columnquery.Value(pl.LicenseObjectId,'LicenseNumber')
           ) loop
        r := r + 1;
        if t_PrevLicNum != p.licensenumber then
           select lic.objectid
             bulk collect into t_LicenseObjectId
             from query.o_abc_license lic
            where lic.LicenseNumber = p.licensenumber
              and lic.State != 'Pending';
           t_PrevLicNum := p.licensenumber;
        end if;
        
        for x in 1..t_LicenseObjectId.count loop
          if t_LicenseObjectId (x) != p.licenseobjectid then
             select count (*)
               into t_RelCount
               from query.r_ABC_PermitGenerationLicense pgl
              where pgl.PermitObjectId  = p.permitobjectid
                and pgl.LicenseObjectId = t_LicenseObjectId (x);
--    If the new Permit - License Relationship does not already exist, create it
             if t_RelCount = 0 then
                dbms_output.put_line ('Creating for License ' || api.pkg_columnquery.Value(t_LicenseObjectId (x),'LicenseNumber') ||'(' || api.pkg_columnquery.Value(t_LicenseObjectId (x),'State') || ')' || ' - Permit ' || api.pkg_columnquery.Value(p.permitobjectid,'PermitNumber') || ' (' || api.pkg_columnquery.Value(p.permitobjectid,'State') || ')');
--    Create the Permit - License Rel
--    Get the next sequence number
                select possedata.ObjectId_s.nextval
                  into t_RelId 
                  from dual;

--    Insert into ObjModelPhys.Objects
                insert into objmodelphys.Objects (
                                                 LogicalTransactionId,
                                                 CreatedLogicalTransactionId,
                                                 ObjectId,
                                                 ObjectDefId,
                                                 ObjectDefTypeId,
                                                 ClassId,
                                                 InstanceId,
                                                 EffectiveStartDate,
                                                 EffectiveEndDate,
                                                 ConfigReadSecurityClassId,
                                                 ConfigReadSecurityInstanceId,
                                                 ObjectReadSecurityClassId,
                                                 ObjectReadSecurityInstanceId
                                                 ) values 
                                                 (
                                                 1, -- Hard coded logical Transaction to 1
                                                 1, -- Hard coded logical Transaction to 1
                                                 t_RelId,
                                                 t_PermitLicRelDef,
                                                 4,
                                                 4,
                                                 t_PermitLicRelDef,
                                                 null,
                                                 null,
                                                 4,
                                                 t_PermitLicRelDef,
                                                 null,
                                                 null
                                                 );

--    Insert Into Rel.StoredRelationships    
                insert into rel.StoredRelationships (
                                                    RelationshipId,
                                                    EndPointId,
                                                    FromObjectId,
                                                    ToObjectId
                                                    ) values 
                                                    (
                                                    t_RelId,
                                                    t_PermitLicenseEP,
                                                    p.permitobjectid,
                                                    t_LicenseObjectId (x) 
                                                    );

                insert into rel.StoredRelationships (
                                                    RelationshipId,
                                                    EndPointId,
                                                    FromObjectId,
                                                    ToObjectId
                                                    ) values 
                                                    (
                                                    t_RelId,
                                                    t_LicensePermitEP,
                                                    t_LicenseObjectId (x),
                                                    p.permitobjectid
                                                    );
                i := i + 1;
             end if;
          end if;
        end loop;
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

  dbms_output.put_line('PermitLicense Records found         : ' || r);
  dbms_output.put_line('GenerationalLicense Records created : ' || i);
end;
