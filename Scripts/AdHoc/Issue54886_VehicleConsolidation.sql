-- Created on 4/23/2020 by ADMINISTRATOR 
declare 
  -- Local variables here
  t_Count                  integer := 0;
  t_PermitIds              api.Pkg_Definition.udt_IdList;
  t_ConsolidateToPermitId  number;
  t_PermitId               number;
  t_PermitToVehicleEP      number
      := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Vehicle');
  t_RelId                  number;
  t_VehicleCount           number := 0;
  t_NoteDefId              number;
  t_NoteId                 number;
begin
  select nd.NoteDefId
    into t_NoteDefId
    from api.notedefs nd
   where nd.Tag ='GEN';
  dbms_output.put_line('From Permit, To Permit, Vehicles Transferred');
  -- Loop through all permits valid for consolidation
  for i in (
    select x.*
      from (
      select p.LicenseNumber, p.LicenseObjectId,
             p.TAPPermitNumber, p.PermitObjectId_TAP,p.AssociatedPermitObjectId, p.AssociatedPermitNumber,
             pt.code PermitType,
             min(p.PermitNumber) over (partition by p.LicenseObjectId, p.PermitObjectId_TAP, p.AssociatedPermitObjectId, pt.code) ConsolidateToPermit,
             p.PermitNumber PermitNumber,
             count(1) over (partition by p.LicenseObjectId, p.PermitObjectId_TAP, p.AssociatedPermitObjectId, pt.code) count
        from api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'State', 'Active') ps
        join query.o_abc_permit p
            on p.objectid = ps.objectid
        join query.o_Abc_Permittype pt
            on pt.objectid = p.PermitTypeObjectId
       where pt.RequiresVehicles = 'Y'
         and (p.licensenumber is not null or p.TAPPermitNumber is not null or p.AssociatedPermitNumber is not null)
      ) x
    where x.count > 1
      and x.PermitNumber != x.ConsolidateToPermit
     order by 1, 3, 5
    ) loop
    begin
      t_VehicleCount :=0;
      api.pkg_logicaltransactionupdate.ResetTransaction;
      -- Get the Permit id and the permit id to consolidate to
      select t.objectid
        into t_PermitId
        from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', i.PermitNumber)) t
       where api.pkg_columnquery.value(t.objectid, 'State') = 'Active';
      select t.objectid
        into t_ConsolidateToPermitId
        from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', i.ConsolidateToPermit)) t
       where api.pkg_columnquery.value(t.objectid, 'State') = 'Active';
       
      -- Move all of the Vehicles to the lowest permit
      for v in (
        select v.RelationshipId, v.VehicleObjectId
          from query.r_abc_permitvehicle v
         where v.PermitObjectId = t_PermitId
        ) loop
        t_RelId := api.pkg_relationshipupdate.New(t_PermitToVehicleEP, t_ConsolidateToPermitId, v.vehicleobjectid);
        api.pkg_relationshipupdate.Remove(v.relationshipid);
        t_VehicleCount := t_VehicleCount+1;
      end loop;
      
      -- Cancel the permit
      t_NoteId := api.pkg_noteupdate.New(t_PermitId, t_NoteDefId, 'N', to_char(sysdate, 'Mon dd, yyyy HH:MI:SS PM') || '
Issue 54886 - COVID-19 Ruling
System Consolidated Vehicles to Permit# ' || i.ConsolidateToPermit);
      api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Cancelled');
      
      api.pkg_logicaltransactionupdate.EndTransaction;
      rollback;--commit;
    exception when others then
      --rollback;
      dbms_output.put_line(i.PermitNumber || ',' || i.ConsolidateToPermit || ', Failed: ' || sqlerrm);
      continue;
    end;
    dbms_output.put_line(i.permitnumber || ',' || i.consolidatetopermit || ',' || t_VehicleCount);
    
    t_Count := t_Count+1;
  end loop;

end;
