-- Created on 8/9/2017 by MICHEL.SCHEFFERS 
-- Issue 32713 - Police Portal - Data Conversion - Create Table
-- Run time < 10 seconds

drop   table possedba.issue32713_police;
create table possedba.issue32713_police
(
   RankTitle          varchar(50),
   FirstName          varchar(50),
   LastName           varchar(50),
   EmailAddress       varchar(4000),
   BusinessPhone      varchar(10),
   PhoneExtension     varchar(6),
   CellPhone          varchar(10),
   Municipality       varchar(4000),
   DateEntered        date,
   AuthenticationName varchar(40),
   UserId             integer,
   Error              varchar(4000)
);   

grant select on possedba.issue32713_police to possesys;
grant update on possedba.issue32713_police to possesys;
grant execute on extension.pkg_cxproceduralsearch to possesys;
