-- Created on 05/3/2017 by Dennis.OConnor 
-- Send Email notifications to Registrants with Preferred Contect Method of Email or 
-- to Registered users associated to the LE who have products with Expiration of  12/31/2015
-- and NonRenewalNoticeDate is NULL

-- Create table
--drop table possedba.LOG_Products_Update_NonRenewalNotice_Date;

declare
    FromAddress                       varchar2(100) := 'donotreply@lps.state.nj.us';
    Subject                           varchar2(100) := 'Action Required: Brand Registration for the 2016 Term';
    MessageBody                       varchar2(4000) := '
<font face="tahoma, arial, verdana, sans-serif" style="font-family: tahoma, arial, verdana, sans-serif; font-size: 12px;">
<span style="font-size: 12px;"><img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"></span></font><br><br><br>
<font face="tahoma" style="font-size: 12px;"><br><font>
<span style="font-size: 12px;">Dear Registrant,</span>
</font><br><br><font><span style="font-size: 12px;">
<br>Our records indicate that you did not renew products (brand registrations) for the <span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold;">2016 term (January 2016 - December 2016).</span>  Please access the ABC Online system<font><span style="font-size: 12px;">'||'&'||'nbsp;<a href="https://abc.lps.nj.gov/ABCPublic/Login.aspx">Here</a>'||'&'||'nbsp;to renew your license</span>
</font> and select Product Registration Renewal to see the list of products registered in 2015 and not renewed for 2016.
If you sold any of these products during the 2016 term, you <span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">MUST</span> renew those products for the 2016 term and pay the appropriate fees.  Your 2016 renewal application will be processed within 1-3 business days after you have submitted the renewal.  <span style="font-family: Helvetica,Arial,sans-serif; color: red;">Once this 2016 renewal is completed you will receive a notification to renew all products being sold for the 2017 term</span> (January 2017 - December 2017). <br><br>
If you do not renew these products and pay the appropriate fees prior to <span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold;">05/31/2017</span>, the products will be retired and you will no longer be registered to sell them.  Selling those products in 2017 will be in violation of NJAC 13:2-33.1.<br><br>
If you have any questions regarding any of the items to be renewed, please contact the ABC office at 609-984-2731.  Please check to make sure that you have not already initiated a NEW brand registration for any of the products listed.
<br><br><br><span style="font-family: Helvetica,Arial,sans-serif; color: red;">We apologize for any inconvenience.</span>
<br><br>Regards,<br><br>NJ Division of Alcoholic Beverage Control</span></font><br><br><br><br>';
    MimeType                          varchar2(100) := 'text/html';
    EmailResult                       number;  

begin

FOR i IN (SELECT 'dennis.oconnor@computronix.com' ContactEmailAddress FROM dual)
  /*(
  SELECT DISTINCT le.ContactEmailAddress
  FROM query.o_abc_product p 
  JOIN query.r_abc_productregistrant r ON r.ProductObjectId = p.ObjectId
  JOIN query.o_abc_legalentity le ON le.ObjectId = r.RegistrantObjectId
  WHERE p.ExpirationDate = to_date('12/31/2015', 'mm/dd/yyyy')
  AND p.State = 'Current'
  AND le.PreferredContactMethod = 'Email'
  UNION
  SELECT DISTINCT api.pkg_columnquery.Value(u.UserId, 'EmailAddress') ContactEmailAddress
  FROM query.o_abc_product p 
  JOIN query.r_abc_productregistrant r ON r.ProductObjectId = p.ObjectId
  JOIN query.o_abc_legalentity le ON le.ObjectId = r.RegistrantObjectId
  JOIN query.r_abc_userlegalentity ule ON ule.LegalEntityObjectId = le.ObjectId
  JOIN api.users u on u.UserId = ule.UserId
  WHERE p.ExpirationDate = to_date('12/31/2015', 'mm/dd/yyyy')
  AND p.State = 'Current'
  AND u.Active = 'Y'
  AND le.PreferredContactMethod = 'Mail'
  ORDER BY ContactEmailAddress)*/
  
  LOOP
    api.pkg_logicaltransactionupdate.ResetTransaction();
                                         
    EmailResult := extension.pkg_sendmail.send(FromAddress,
                                         i.ContactEmailAddress,
                                         Subject,
                                         MessageBody,
                                         MimeType,
                                         NULL,
                                         NULL,
                                         NULL,
                                         MessageBody,
                                         NULL);
                                    
  api.pkg_logicaltransactionupdate.EndTransaction();
                                          
  dbms_output.put_line ('Email: ' || i.ContactEmailAddress);
  END LOOP;
    
  extension.pkg_SendMail.FinalizeEmail();

end;