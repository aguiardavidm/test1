-- Created on 10/30/2017 by MICHEL.SCHEFFERS
-- Issue 33653 - New ABC_DoNotReply Mailbox
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  select s.ObjectId
  into   i
  from   query.o_systemsettings s;
  api.pkg_columnupdate.SetValue(i, 'FromEmailAddress', 'ABCDoNotReply@njoag.gov');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
  
end;
