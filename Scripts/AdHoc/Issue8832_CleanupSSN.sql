declare
  t_ColDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'SSNTINEncrypted');
begin
  
  for i in (select l.SSNTIN, abc.pkg_abc_encryption.decrypt(l.SSNTINEncrypted) ssndirty, 
                   abc.pkg_abc_encryption.encrypt(regexp_replace(abc.pkg_abc_encryption.decrypt(l.SSNTINEncrypted), '[^0-9a-zA-Z]*')) ssnclean,
                   objectid
              from query.o_abc_legalentity l
             where l.SSNTINEncrypted is not null) loop
    update possedata.objectcolumndata_T cd
       set cd.AttributeValue = i.ssnclean, cd.SearchValue = lower(substr(i.ssnclean, 1, 20))
     where cd.ObjectId = i.objectid
       and cd.ColumnDefId = t_ColDefId;
  end loop;
end;