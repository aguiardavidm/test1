-- Created on 5/26/2016 by MICHEL.SCHEFFERS 
-- Issue 8672 - Remove Outstanding Fee LegalEntity - User relationships
declare 
  -- Local variables here
  t_RelationshipDefId integer;
  t_Label             varchar2(4000) := 'Outstanding Fee Legal Entity:';
  i                   integer := 0;
begin
   
   begin
      select rd.RelationshipDefId
        into t_RelationshipDefId
        from api.relationshipdefs rd
       where rd.ToLabel = t_Label;
   exception
      when no_data_found then
         api.pkg_errors.RaiseError(-20000,'No RelationshipDef found for ' || t_Label);
   end;

   api.pkg_logicaltransactionupdate.ResetTransaction();
   for r in (select distinct(r.RelationshipId)
               from api.relationships r
              where r.RelationshipDefId = t_RelationshipDefId) loop
      dbms_output.put_line ('Found relationship for ' || r.relationshipid);
      i := i + 1;
      api.pkg_relationshipupdate.Remove(r.relationshipid);
   end loop;
   
   api.pkg_logicaltransactionupdate.EndTransaction();
   dbms_output.put_line ('Removed ' || i || ' relationships.');
   commit;
 
end;