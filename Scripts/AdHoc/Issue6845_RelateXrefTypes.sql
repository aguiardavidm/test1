--Est Runtime: 15 minutes
declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_ProductXref', 'ProductType');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_ProductXref', 'ProductType');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_ProductXrefProductType');
  t_RelId         number;
  t_CurrTransId   number;
  t_DupColDefId   number := api.pkg_configquery.ColumnDefIdForName('o_ABC_ProductXref', 'dup_NewType');
  
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_CurrTransId := api.pkg_logicaltransactionupdate.CurrentTransaction;
  
  for i in (select x.objectid, api.pkg_columnquery.value(x.ProductObjectId, 'TypeId') typeid
              from query.o_ABC_ProductXref x
             where x.objectdeftypeid = 1) loop
    if api.pkg_columnquery.value(i.objectid, 'NewTypeId') is null and i.typeid is not null then

      --Create the Product Type Rel
      select possedata.ObjectId_s.nextval
        into t_RelId 
       from dual;

      --Insert into ObjModelPhys.Objects
      insert into objmodelphys.Objects (
        LogicalTransactionId,
        CreatedLogicalTransactionId,
        ObjectId,
        ObjectDefId,
        ObjectDefTypeId,
        ClassId,
        InstanceId,
        EffectiveStartDate,
        EffectiveEndDate,
        ConfigReadSecurityClassId,
        ConfigReadSecurityInstanceId,
        ObjectReadSecurityClassId,
        ObjectReadSecurityInstanceId
      ) values (
        t_CurrTransId,
        t_CurrTransId,
        t_RelId,
        t_RelDef,
        4,
        4,
        t_RelDef,
        null,
        null,
        4,
        t_RelDef,
        null,
        null
      );

    --Insert Into Rel.StoredRelationships
      insert into rel.StoredRelationships (
        RelationshipId,
        EndPointId,
        FromObjectId,
        ToObjectId
      ) values (
        t_RelId,
        t_EndpointId,
        i.Objectid,
        i.Typeid
      );

      insert into rel.StoredRelationships (
        RelationshipId,
        EndPointId,
        FromObjectId,
        ToObjectId
      ) values (
        t_RelId,
        t_OthEndpointId,
        i.Typeid,
        i.Objectid
      );
      insert into possedata.objectcolumndata (
        logicaltransactionid,
        objectid,
        columndefid,
        attributevalue,
        searchvalue
      ) values (
        t_CurrTransId,
        i.objectid,
        t_DupColDefId,
        api.pkg_columnquery.value(i.TypeId, 'Type'),
        lower(api.pkg_columnquery.value(i.TypeId, 'Type'))
      );
    end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;