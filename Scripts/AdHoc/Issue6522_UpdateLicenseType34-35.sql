-- Created on 03/04/2016 by MICHEL SCHEFFERS 
-- Issue 6522: Seasonal License Type changes
-- Update License Types 34 and 35.
declare 
  -- Local variables here
  d                          number := 0;
   
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();

  for i in (select lt.ObjectId, lt.Code
              from query.o_abc_licensetype lt
             where lt.Code in ('34', '35')
           ) loop
     dbms_output.put_line('Updating License Type ' || i.code);
     api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationMethod', 'Seasonal');
     if i.Code = '34' then
        api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationStartDay', 01);
        api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationStartMonth', 'May');
        api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationEndDay', 14);
        api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationEndMonth', 'November');
        api.pkg_columnupdate.SetValue (i.objectid, 'CanRenewDaysPrior', to_Date(api.pkg_columnquery.Value(i.objectid, 'ExpirationEndMonth') || '/' || api.pkg_columnquery.Value(i.ObjectId, 'ExpirationEndDay'), 'MM/DD') - to_Date('04/01','MM/DD'));
        api.pkg_columnupdate.SetValue (i.objectid, 'ExpirationReminderDays', to_Date(api.pkg_columnquery.Value(i.objectid, 'ExpirationEndMonth') || '/' || api.pkg_columnquery.Value(i.ObjectId, 'ExpirationEndDay'), 'MM/DD') - to_Date('04/01','MM/DD'));
        api.pkg_columnupdate.SetValue (i.objectid, 'GracePeriodDays', (to_date('07/30','mm/dd') - to_date(api.pkg_columnquery.Value(i.objectid, 'ExpirationEndMonth') || '/' || api.pkg_columnquery.Value(i.ObjectId, 'ExpirationEndDay'), 'MM/DD') + 365));
     else
        api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationStartDay', 15);
        api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationStartMonth', 'November');
        api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationEndDay', 30);
        api.pkg_columnUpdate.SetValue (i.objectid, 'ExpirationEndMonth', 'April');
        api.pkg_columnupdate.SetValue (i.objectid, 'CanRenewDaysPrior', to_Date(api.pkg_columnquery.Value(i.objectid, 'ExpirationEndMonth') || '/' || api.pkg_columnquery.Value(i.ObjectId, 'ExpirationEndDay'), 'MM/DD') - to_Date('04/01','MM/DD'));
        api.pkg_columnupdate.SetValue (i.objectid, 'ExpirationReminderDays', to_Date(api.pkg_columnquery.Value(i.objectid, 'ExpirationEndMonth') || '/' || api.pkg_columnquery.Value(i.ObjectId, 'ExpirationEndDay'), 'MM/DD') - to_Date('04/01','MM/DD'));
        api.pkg_columnupdate.SetValue (i.objectid, 'GracePeriodDays', (to_date('07/30','mm/dd') - to_date(api.pkg_columnquery.Value(i.objectid, 'ExpirationEndMonth') || '/' || api.pkg_columnquery.Value(i.ObjectId, 'ExpirationEndDay'), 'MM/DD')));
     end if;
     d := d + 1;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

  dbms_output.put_line('License Types updated : ' || d);
end;
