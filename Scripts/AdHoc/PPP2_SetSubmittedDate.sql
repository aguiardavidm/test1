-- Created on 12/31/2018 by JAE.HOLDERBY
/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: This script retrievs the Created Date field from the
 * p_ABC_OnlinePermitSubmittal and p_ABC_EnterPermitApplication processess
 * and sets the Submitted Date property on j_ABC_PermitApplication and
 * j_ABC_PermitRenewal jobs.
 *---------------------------------------------------------------------------*/
declare
  -- Local variables here
  t_JobidList   api.Pkg_Definition.udt_IdList;
  t_DateList    api.pkg_definition.udt_DateList;
  t_Counter     integer := 0;
begin
  -- Test statements here

  api.pkg_logicaltransactionupdate.ResetTransaction();
  --Select the job Id and Date to be inserted into the SubmittedDate column, where application proccesses are complete.
  select j.jobid,
         p.DateCompleted
  bulk collect into
       t_JobidList,
       t_DateList
  from api.jobs j
  join api.jobtypes jt
    on jt.JobTypeId = j.JobTypeId
  join api.processes p
    on p.JobId = j.JobId
  join api.processtypes pt
    on pt.ProcessTypeId = p.ProcessTypeId
 where jt.Name in ('j_ABC_PermitApplication','j_ABC_PermitRenewal')
   and pt.Name in ('p_ABC_OnlinePermitSubmittal', 'p_ABC_EnterPermitApplication');

 --Cycle through Jobs and set the value of Submitteddate to the Completed Date from the processess.
  for i in 1.. t_JobidList.count loop
    api.pkg_columnupdate.SetValue(t_JobidList(i), 'SubmittedDate', t_DateList(i));

    t_Counter := t_Counter +1;

    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  dbms_output.put_line(t_Counter);
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
