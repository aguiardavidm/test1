-- Created on 2/22/2017 by MICHEL.SCHEFFERS
declare 
  -- Local variables here
  t_GLAccountId integer;
  t_GlAcctObjId integer;
  t_GlAcctDefId number := api.pkg_configquery.ObjectDefIdForName('o_GlAccount');  

begin
      -- Test statements here
    api.pkg_logicaltransactionupdate.ResetTransaction;
    
      select gl.GLAccountId 
        into t_GLAccountId 
        from api.GLAccounts gl
       where gl.GLAccount = 'FAF';
        
        -- Call the procedure
        api.pkg_objectupdate.RegisterExternalObject('o_GLAccount', t_GLAccountId);
          
      Select reo.ObjectId
        into t_GlAcctObjId
        from api.registeredexternalobjects reo 
       where reo.LinkValue = t_GlAccountId
         and reo.ObjectDefId = t_GlAcctDefId;
      
      api.pkg_columnupdate.SetValue(t_GlAcctObjId, 'Active', 'Y');

    api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
