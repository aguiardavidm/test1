/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: This script runs Apply instance security on Posse Report Letters
 *   that are either Permit IDs or Permit Certificates, to enable Municipal
 *   and Police Users to view the certificates on the MC/PC processes.
 *---------------------------------------------------------------------------*/

declare

t_CertificateIds        api.pkg_definition.udt_IdList;
t_Counter               integer := 0;
t_IDCardObjectIds       api.pkg_definition.udt_IdList;

begin

  api.pkg_logicaltransactionupdate.ResetTransaction();

  select pc.CertificateId
  bulk collect into t_CertificateIds
  from query.r_Permit_Certificate pc;

  select pid.IDCardObjectId
  bulk collect into t_IDCardObjectIds
  from query.r_Permit_IDCard pid;

  for i in 1..t_CertificateIds.count loop
    abc.pkg_instancesecurity.ApplyInstanceSecurity(t_CertificateIds(i), sysdate);
     t_Counter := t_Counter +1;
     if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
     commit;
    end if;
  end loop;

  for j in 1..t_IDCardObjectIds.count loop
    abc.pkg_instancesecurity.ApplyInstanceSecurity(t_IDCardObjectIds(j), sysdate);
     t_Counter := t_Counter +1;
     if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  dbms_output.put_line(t_Counter);
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
