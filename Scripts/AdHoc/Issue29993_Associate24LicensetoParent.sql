-- Created on 5/19/2017 by MICHEL.SCHEFFERS
-- Issue 29993 - Active 24-type License without a Parent License

declare 
  -- Local variables here
  t_EndpointId       integer := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'AddtlWarehouseLicense');
  t_ParentLicense    integer := 36478051; -- 3400-23-026-002
  t_WarehouseLicense integer := 36479023; -- 3404-24-272-001
  t_RelId            integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_relid := api.pkg_relationshipupdate.new(t_EndpointId, t_WarehouseLicense, t_ParentLicense);
  dbms_output.put_line ('Relationship ' || t_RelId);
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;