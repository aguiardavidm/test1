/* Write up a script with the following criteria:
�  Permit Type: TAP
�  Expiring Date between (including the day of): 03/30/2020 to 06/29/2020
�  Status: Active and Expired

This script will:
�  Set the expiration date: 06/30/2020
�  Set the State: Active
*/
declare
  t_PermitIds   api.pkg_Definition.udt_IdList;
  t_TAPTypeId   api.pkg_Definition.udt_Id
      := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PermitType', 'Code', 'TAP');
  t_Count       number := 0;
begin

  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20000, 'Script must be run as POSSEDBA');
  end if;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  select t.objectid
    bulk collect into t_PermitIds
    from api.pkg_SimpleSearch.CastableObjectsByIndexRange('o_ABC_Permit', 'ExpirationDate', '2020-03-30 00:00:00', '2020-06-29 23:59:59') t;

  for i in 1..t_PermitIds.count loop
    if api.pkg_ColumnQuery.value(t_PermitIds(i), 'PermitTypeObjectId') = t_TAPTypeId then
      if api.pkg_ColumnQuery.Value(t_PermitIds(i), 'State') in ('Active', 'Expired') then
        dbms_output.put_line('Permit #'||api.pkg_ColumnQuery.value(t_PermitIds(i), 'PermitNumber')|| 
            ': Expired ' || api.pkg_ColumnQuery.datevalue(t_PermitIds(i), 'ExpirationDate') || ' ('||api.pkg_ColumnQuery.value(t_PermitIds(i), 'State')||')');
        api.pkg_ColumnUpdate.SetValue(t_PermitIds(i), 'ExpirationDate', to_date('06/30/2020', 'mm/dd/yyyy'));
        api.pkg_ColumnUpdate.SetValue(t_PermitIds(i), 'State', 'Active');
        t_Count := t_count + 1;
      end if;
    end if;
  end loop;
  dbms_output.put_line('Updated permits: ' || t_Count);
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;