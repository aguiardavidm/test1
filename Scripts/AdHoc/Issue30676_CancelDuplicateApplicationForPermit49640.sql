-- Created on 06/26/2017 by MICHEL.SCHEFFERS 
-- Issue 30676 - OPS: Job 167307 - Permit 49640 - Permit exists on two applications
declare 
  -- Local variables here
  t_ProcessId      number;
  t_ProcessDefId   number := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus'); -- DefId for Process to create
  t_Outcome        varchar2(100):= 'Cancelled'; -- Outcome
  t_RelId          number;
  t_ApplicationJob number;
  t_PermitId       number := 42844753;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();

-- Remove relationship to Permit 49640
  dbms_output.put_line('Removing relationship from job 162788 to Permit 49640');
  select pap.RelationshipId, pap.PermitApplicationObjectId
  into   t_RelId, t_ApplicationJob
  from   query.r_abc_permitapppermit pap
  where  pap.PermitObjectId = t_PermitId
  and    api.pkg_columnquery.Value(pap.PermitApplicationObjectId,'StatusDescription') = 'New';
  api.pkg_relationshipupdate.Remove(t_RelId);

-- Cancel job 162788
  dbms_output.put_line('Cancelling job 162788 (' || t_ApplicationJob || ').');
  t_ProcessId := api.pkg_processupdate.New (t_ApplicationJob, t_ProcessDefId, 'Change Status',sysdate,sysdate,sysdate);
  api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange', 'Cancelled per Issue 30676.'); 
  api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);  

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
