declare
  t_NoteId    number;
  t_NoteDefId number;
begin
  if user != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20000, 'Must be run as POSSEDBA');
  end if;
  select nd.NoteDefId
    into t_NoteDefId
    from api.notedefs nd
   where nd.Tag = 'GEN';
     
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (
    select p.objectid, p.PermitNumber, pt.Code, p.ExpirationDate, p.PermitteeName
      from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'State', 'Active')) x
      join query.o_abc_permit p
          on p.ObjectId = x.objectid
      join query.r_abc_permitpermittype r
          on r.PermitId = x.objectid
      join query.o_abc_permittype pt
          on pt.ObjectId = r.PermitTypeId
     where pt.Code = 'TAP'
  ) loop
    dbms_output.put_line(i.objectid);
    api.pkg_columnupdate.SetValue(i.objectid, 'ExpirationDate', to_date('09/30/2020', 'mm/dd/yyyy'));
    t_NoteId := api.pkg_noteupdate.New(i.objectid, t_NoteDefId, 'N',
        to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) || 'System adjusted Expiration Date to September 30, 2020 for issue 55671');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
--  commit;
end;