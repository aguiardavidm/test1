/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 sec
 * Purpose: This script sets the subject/body for the Automatic Cancellation
 *  Email in the Online Emails tab of System Settings.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_Count                               pls_integer := 0;
  t_EmailText                           varchar2(32767);
  t_LogicalTransactionId                udt_Id;
  t_PermitTypeIds                       udt_IdList;
  t_SystemSettingsId                    udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

  procedure CreateUpdateEmailTemplate (
    a_ObjectId                          udt_Id,
    a_Tag                               varchar2,
    a_Text                              varchar2
  ) is
    t_NoteDefId                         udt_Id;
    t_NoteId                            udt_Id;
  begin

    select n.NoteId
    into t_NoteId
    from
      api.Notes n
      join api.NoteDefs nd
          on n.NoteDefId = nd.NoteDefId
    where n.ObjectId = a_ObjectId
      and nd.Tag = a_Tag;
    -- if there is an existing email body template in system settings, update it.
    api.pkg_NoteUpdate.Modify(t_NoteId, 'N', a_Text);

  exception
    -- Create a new email body template
    when no_data_found then
      select nd.NoteDefId
      into t_NoteDefId
      from api.NoteDefs nd
      where nd.Tag = a_Tag;
      t_NoteId := api.pkg_NoteUpdate.New(a_ObjectId, t_NoteDefId, 'N', a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  dbug(chr(13) || chr(10) || '---BEFORE---');
  select count(1)
  into t_Count
  from
    api.notes n
    join api.notedefs nd
        on n.NoteDefId = nd.NoteDefId
  where nd.Tag = 'ACEML';
  dbug('Existing Email Body Templates: ' || t_Count);
  dbug('Email Body Templates to be created: ' || (1 - t_Count));

  select s.ObjectId
  into t_SystemSettingsId
  from query.o_SystemSettings s
  where s.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings', 'SystemSettingsNumber',
      1);

  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'AutomaticCancellationEmailSubj',
      'Automatic Cancellation of Application');

  t_EmailText :=
      '<img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"><br>'
      || '<br><font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: 12px;">'
      || 'Dear {FirstName},<br>An application draft for {JobType}, {FileNumber} was created on'
      || ' {CreatedDate} and is set to expire on {AutoCancellationDate}. To complete or cancel'
      || ' your application please [[click here]]</font>.<br><br><br><br><br><br><font face='
      || '"Arial" size="1">Plain Text Link:<br>{Link}</font><br>';
  CreateUpdateEmailTemplate(t_SystemSettingsId, 'ACEML', t_EmailText);

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

  dbug(chr(13) || chr(10) || '---AFTER---');
  select count(1)
  into t_Count
  from
    api.LogicalTransactions l
    join api.Notes n
        on n.LogicalTransactionId = l.LogicalTransactionId
    join api.NoteDefs nd
        on n.NoteDefId = nd.NoteDefId
  where l.LogicalTransactionId = t_LogicalTransactionId
    and nd.Tag = 'ACEML';
  dbug('Email Body Templates Updated: ' || t_Count);

  commit;

end;
