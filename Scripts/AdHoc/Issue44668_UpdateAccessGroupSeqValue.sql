declare
  t_SequenceId                        number;
  t_NewSeqValue                       number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  --Get Sequence id to set
  select s.SequenceId
   into t_SequenceId
   from api.sequences s
   where s.Description = 'NextTokenAccessGroup';
   
  --Get the New Next Value
  SELECT to_number(ltrim(max(ag.Description), 'www')) + 1
  into t_NewSeqValue
  FROM api.accessgroups ag
       WHERE NOT EXISTS (
                         SELECT 1
                         FROM api.accessgroupusers agu
                              WHERE agu.accessgroupid=ag.accessgroupid)
     AND ag.description LIKE 'www0%'
     AND ag.Description > 'www000010000' -- to get a more accurate count
  ;
  
  --Update the Sequence  
  api.pkg_SequenceUpdate.SetValue(t_sequenceid, t_NewSeqValue);
  api.pkg_logicaltransactionupdate.EndTransaction();
  /* --To validate this worked run the following select statement to see the next pending value.
  select s.SequenceId, s.*
    from api.sequences s
    where s.Description = 'NextTokenAccessGroup';*/

  commit;
end;
