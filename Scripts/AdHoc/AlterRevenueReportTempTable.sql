alter table abc.RevenueReport_t rename column GLAccountDescriptions to GLAccountCategory;
alter table abc.RevenueReport_t rename column GLAccountId           to GLAccountCatObjId;
alter table abc.RevenueReport_t rename column LicenseTypeId         to GLAccountObjId;