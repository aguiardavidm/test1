/*-----------------------------------------------------------------------------
 * Author: Benjamin Albrecht
 * Expected run time: 10.3 seconds for 227 total in cxnjdev
 *  cxnjtest 8.200 seconds for 118
 *  njuat 84.656 seconds for 316 (175: 13.656 + 141: 71)
 * Purpose: For Issue 57376 - MC Unable to see the Certificate, this script 
 * will set the jobid and processid stored details where they were missed in 
 * the report bindings.
 *
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_EPtoGenerateLicense                 number(15);
  t_EPtoSendLicense                     number(15);
  t_IssueNumber                         varchar2(6) := '57376';
  t_LogicalTransactionId                pls_Integer;
  t_ObjectId                            pls_Integer;
  t_ObjectList                          api.udt_objectlist;
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_TimeEnd                             number(15);
  t_TimeStart                           number(15);
  t_UpdatedCount                        pls_Integer := 0;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  -- The 'BEFORE' section is useful if you want to print initial data or take an initial count.
  dbug(chr(13) || chr(10) || '---BEFORE---');

  select endpointid
  into t_EPtoGenerateLicense
  from api.endpoints ep
  join api.relationshipdefs rd on rd.ToEndPointId = ep.endpointid
  where ep.fromobjectdefid = api.pkg_configquery.ObjectDefIdForName('d_PosseReportLetter')
  and ep.ToObjectDefId = api.pkg_configquery.ObjectDefIdForName('p_ABC_GenerateLicense')
  and rd.Stored = 'Y'
  ;
  
  select endpointid
  into t_EPtoSendLicense
  from api.endpoints ep
  join api.relationshipdefs rd on rd.ToEndPointId = ep.endpointid
  where ep.fromobjectdefid = api.pkg_configquery.ObjectDefIdForName('d_PosseReportLetter')
  and ep.ToObjectDefId = api.pkg_configquery.ObjectDefIdForName('p_ABC_SendLicense')
  and rd.Stored = 'Y'
  ;

  -- 'DURING' is useful if you want to print data being changed.
  dbug(chr(13) || chr(10) || '---DURING---');
  t_TimeStart := dbms_utility.get_time();
  
  -- Loop through the License Certificates to update
  for i in (
    --Test one single set for p_ABC_GenerateLicense only for initial testing. 10 total
    select l.objectid, p.jobid, p.processid
    from query.d_possereportletter l
    join api.relationships r on r.FromObjectId = l.objectid
    join api.processes p on p.processid = r.toobjectid
    where (l.jobid is null or l.processid is null)
    --and l.objectid = 25206233 --Testing
    and l.Description = 'License Certificate'
    and r.endpointid in (1620113) --repot-To-p_ABC_GenerateLicense UAT:1620113
    union all
    --Test one single set for p_ABC_SendLicense only for initial testing. 217 total.
    select l.objectid, p.jobid, p.processid
    from query.d_possereportletter l
    join api.relationships r on r.FromObjectId = l.objectid
    join api.endpoints ep on ep.endpointid = r.endpointid
    join api.processes p on p.processid = r.toobjectid
    where (l.jobid is null or l.processid is null)
    --and l.objectid = 25213592 --Testing
    --and l.objectid = 9059443 --bad data
    and l.Description = 'License Certificate'
    and r.endpointid in (1405508) --repot-To-p_ABC_SendLicense

  ) loop
    -- Set the two details
    api.pkg_columnupdate.SetValue(i.objectid, 'JobId', i.jobid);
    api.pkg_columnupdate.SetValue(i.objectid, 'ProcessId', i.processid);
    
    -- Update Instance Security
    --   Instance Security auto updates itself on Post Verify

    --Debugging and logging of changed objects
    dbug('Updated: ' || i.objectid);
    t_UpdatedCount := t_UpdatedCount + 1;

  end loop;

  


  t_TimeEnd := dbms_utility.get_time();
  dbug('Time Elapsed: ' || (t_TimeEnd - t_TimeStart) / 100);

  api.pkg_logicaltransactionupdate.EndTransaction();

  dbug(chr(13) || chr(10) || '---AFTER---');
  dbug('Updated Object Count: ' || t_UpdatedCount);

  --commit;

end;
