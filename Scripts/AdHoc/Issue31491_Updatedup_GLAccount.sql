rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- Created on 11/27/2017 by MICHEL.SCHEFFERS 
-- Issue31491: Need to be able to reinstate licenses in states other than Revoked
-- Update all dup_GLAccount values for Miscellaneous Transaction jobs that are not equal to their counterpart.
declare 
  -- Local variables here
  c                          number := 0;
  t_Dup_GLDefId              number := api.pkg_configquery.ColumnDefIdForName('j_ABC_MiscellaneousRevenue', 'dup_GLAccount');
  t_CurrentTransaction       api.pkg_definition.Udt_Id;
      
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  t_CurrentTransaction := api.pkg_logicaltransactionupdate.CurrentTransaction;
  api.pkg_logicaltransactionupdate.EndTransaction();

  for mt in (select m.ObjectId, m.ExternalFileNum, m.GLAccount, m.dup_GLAccount
             from   query.j_abc_miscellaneousrevenue m
             where  m.GLAccount != m.dup_GLAccount
            ) loop
     dbms_output.put_line ('Updating job ' || mt.ExternalFileNum || ' from ' || mt.dup_GLAccount || ' to ' || mt.glaccount);
     update possedata.objectcolumndata cd
     set    cd.attributevalue = mt.GLAccount,
            cd.searchvalue = lower (rtrim(substr(mt.GLAccount, 1, 20)))
     where  cd.objectid = mt.ObjectId
     and    cd.columndefid = t_Dup_GLDefId;
     c := c + 1;
  end loop;

  commit;  

  dbms_output.put_line('GL Accounts updated : ' || c);
end;
/
