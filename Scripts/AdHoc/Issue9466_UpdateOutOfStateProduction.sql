-- Created on 9/22/2016 by MICHEL.SCHEFFERS 
-- Issue 9466 - Update Out of State Winery production
declare 
  -- Local variables here
  t_LicenseTypeId    number;
  i                  number := 0;
begin
  -- Test statements here
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  begin
     select lt.ObjectId
     into   t_LicenseTypeId
     from   query.o_abc_licensetype lt
     where  lt.Code = '41';
  exception
     when no_data_found then
        api.pkg_errors.RaiseError(-20000,'No License Type record for 41!');
  end;
  
  for oosw in (select l.ObjectId, l.OutOfStateWineryProduction
               from   query.o_abc_license l
               where  l.LicenseTypeObjectId = t_LicenseTypeId
              ) loop
     if oosw.outofstatewineryproduction in ('Less than 1,000 gallons', 'Between 1,000 and 2,500 gallons', 'Between 2,500 and 30,000 gallons', 'Between 30,000 and 50,000 gallons') then
        dbms_output.put_line('Updating Winery Production of ' ||  oosw.objectid || '(' || api.pkg_columnquery.Value(oosw.objectid,'LicenseNumber') || ') from ' || oosw.outofstatewineryproduction);
        i := i + 1;
        api.pkg_columnupdate.SetValue(oosw.objectid,'OutOfStateWineryProduction', 'Between 0 and 50,000 gallons');
     end if;
  end loop;
  
  dbms_output.put_line('Removing Salesroom Fee for ' || i || ' licenses.');  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
