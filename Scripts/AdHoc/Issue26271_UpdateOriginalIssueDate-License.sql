-- Created on 10/31/2017 by MICHEL.SCHEFFERS 
-- Issue 26271 - Set Original Issue Date on License
declare 
  -- Local variables here
  t_OriginalIssueDate    date;
  t_IdList               api.pkg_Definition.udt_IdList;
  t_LicenseColumnDefId   number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'OriginalIssueDate');
  t_PermitColumnDefId    number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'OriginalIssueDate');
  t_CurrentTransaction   api.pkg_definition.Udt_Id;
  t_FormattedDate        varchar2(400);
  c1                     integer := 0;
  c2                     integer := 0;
  c3                     integer := 0;
begin
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_CurrentTransaction := api.pkg_logicaltransactionupdate.CurrentTransaction;  
  -- Update Original Issue Date for Licenses
  dbms_output.put_line('Updating Original Issue Date for Licenses');
  for ml in (select l.ObjectId
             from   query.o_abc_masterlicense l
            ) loop
     c1 := c1 + 1;
     select min(api.pkg_columnquery.DateValue(mll.LicenseObjectId, 'IssueDate'))
     into   t_OriginalIssueDate
     from   query.r_abc_masterlicenselicense mll
     where  mll.MasterLicenseObjectId = ml.objectid;
     dbms_output.put_line('Original Issue Date for Master License ' || ml.objectid || ': ' || t_OriginalIssueDate);
     if t_OriginalIssueDate is not null then
        select r.LicenseObjectId
        bulk   collect into t_IdList
        from   query.r_ABC_MasterLicenseLicense r
        where  r.MasterLicenseObjectId = ml.objectid;
        dbms_output.put_line('   Records for License ' || ml.objectid || ': ' || t_IdList.count);
        t_FormattedDate := to_char(t_OriginalIssueDate,'yyyy-mm-dd') || ' ' || '00:00:00';

        for i in 1..t_IdList.count loop
           c2 := c2 + 1;
           if api.pkg_columnquery.Value(t_IdList(i), 'State') != 'Pending' and 
              api.pkg_columnquery.DateValue(t_IdList(i), 'OriginalIssueDate') is null then
              dbms_output.put_line('   Updating Original Issue Date for ' || t_IdList(i) || ' to ' || t_FormattedDate);
              c3 := c3 + 1;
              insert into possedata.objectcolumndata cd
                      (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue, cd.searchvalue)
               Values (t_CurrentTransaction, t_IdList(i), t_LicenseColumnDefId, t_FormattedDate, t_FormattedDate);
           end if;
        end loop;
     end if;
  end loop;
  dbms_output.put_line(c1 || ' Master Licenses found');
  dbms_output.put_line(c2 || ' Licenses Found');
  dbms_output.put_line(c3 || ' Licenses Updated');
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
