begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select a.ObjectId from query.o_abc_amendmenttype a where a.RequiresMunicipalityInvolvemen = 'Y') loop
    api.pkg_columnupdate.SetValue(i.objectid, 'RequiresResolution', 'Y');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;


