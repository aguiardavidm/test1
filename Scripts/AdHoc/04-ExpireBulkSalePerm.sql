declare
  t_AttributeId number;
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'State');
begin
  select dv.DomainValueId
    into t_AttributeId
    from api.domains d
    join api.domainlistofvalues dv on dv.domainid = d.domainid
   where d.name = 'ABC Permit State'
     and dv.AttributeValue = 'Expired';
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.objectid
              from dataconv.o_abc_permit p
              join dataconv.o_abc_permittype pt on pt.ObjectId = p.permittypelk
             where pt.code = 'BSP') loop
    update possedata.objectcolumndata o
       set o.attributevalueid = t_AttributeId
     where o.objectid = i.objectid
       and o.ColumnDefId = t_ColumnDefId;
    dbms_output.put_line(i.objectid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
/
