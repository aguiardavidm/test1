-- Created on 10/16/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;

  ----------------------------------------------------------------------------
  --  RemoveFees() -- PUBLIC
  --  Removes all the fees on a job - Called when the job moves into the
  --  cancelled status
  ----------------------------------------------------------------------------
  procedure RemoveFees (
    a_ObjectId                          number,
    a_AsOfDate                          date
  ) is
    t_JobId                             number;
    t_TransactionId                     number;
  begin
    t_JobId := a_ObjectId;

    -- Loop through all system generated fees on the job that have not been paid, adjusted, etc.
    -- and adjust them down to zero
    for c in (select a.FeeId,
                     a.Amount + a.AdjustedAmount NetAmount
                from api.Fees a
                where a.JobId = t_JobId
                  and a.SystemGenerated = 'Y'
                  and a.PostedDate is null
                  and (select count(*)
                        from api.FeeTransactions b
                       where b.FeeId = a.FeeId
                         and b.TransactionType = 'Pay') = 0) loop
	  if 	c.NetAmount != 0 then				 
      t_TransactionId := api.pkg_FeeUpdate.Adjust(c.FeeId, -c.NetAmount, 'Adjusted by system due to cancellation of job', sysdate);
	  end if;
   end loop;

  end RemoveFees;


begin
  -- Test statements here
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
    
   
    RemoveFees(31062380,sysdate);
    
    RemoveFees(31128482,sysdate);
    
    RemoveFees(31154694,sysdate);
    
    RemoveFees(31174507,sysdate);
    
    RemoveFees(31196850,sysdate);
    
    RemoveFees(31199832,sysdate);
    
    RemoveFees(31288627,sysdate);
    
    RemoveFees(31521418,sysdate);
    
    RemoveFees(31638474,sysdate);
    
  api.pkg_logicaltransactionupdate.EndTransaction();
  
end;
