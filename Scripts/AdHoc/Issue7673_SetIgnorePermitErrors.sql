/* table creation, TAKES A LONG TIME, RUN OVERNIGHT, can be run before change set / package code is installed
create table possedba.logpermiterrors_t
  (PermitObjectId number(9),
   PermintNumber varchar2(30),
   PermitType    varchar2(60),
   ErrorText     varchar2(4000));

-- Created on 2/26/2016 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
  t_value varchar2(4000);
begin
  -- Test statements here
  
  for c in (select p.objectid,
                   p.permitnumber,
                   p.permittype
                   
             from query.o_abc_permit p
             ) loop
  begin
    abc.pkg_abc_permit.ValidatePermitData(c.objectid,
                                          sysdate);  
  exception when others then
    t_value := SQLERRM;
  insert into possedba.logpermiterrors_t t 
    values(c.objectid,
           c.permitnumber,
           c.permittype,
           t_value);
  commit;         
  
  end;
  end loop;
  
end;
*/
declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'IgnoreErrors');
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *
              from possedba.logpermiterrors_t) loop
    insert into possedata.objectcolumndata
    (logicaltransactionid, objectid, columndefid, attributevalue, searchvalue)
    values
    (api.pkg_logicaltransactionupdate.CurrentTransaction(), i.permitobjectid, t_ColumnDefId, 'Y', 'y');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;
