declare
   p                     number := 0;
   l                     number := 0;
   t_PrevLicenseObject   number := 0;
   t_MasterLicenseObject number;
   t_NewObjectId         number;
   t_State               varchar2(80);
   t_RelId               number;
   t_PermitLicenseEP     number := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'License');
   t_LicensePermitEP     number := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'GenerationalLicensePermit');     

begin
   dbms_output.enable(null);
   api.pkg_logicaltransactionupdate.ResetTransaction;
   for lp in (select *
              from   possedba.issue6591 pi
              where  pi.newlicenseid is not null
             )
   loop
      -- Create Permit Generational License for old License
      dbms_output.put_line ('Creating Generational for License ' || lp.oldlicenseid || ' - ' || lp.oldlicensenumber ||'(' || api.pkg_columnquery.Value(lp.oldlicenseid,'State') || ')' 
                             || ' - Permit ' || lp.permitid || ' - ' || lp.permitnumber || ' (' || api.pkg_columnquery.Value(lp.permitid,'State') || ')');
      t_RelId := api.pkg_relationshipupdate.New(t_LicensePermitEP,lp.oldlicenseid, lp.permitid, sysdate);
      p := p + 1;
      begin
         select pl.RelationshipId
         into   t_RelId
         from   query.r_permitlicense pl
         where  pl.PermitObjectId = lp.permitid
         and    pl.LicenseObjectId = lp.oldlicenseid;
      exception
         when no_data_found then
            continue;
      end;
      -- Remove primary Permit License for old License
      dbms_output.put_line ('Removing Primary for License ' || lp.oldlicenseid || ' - ' || lp.oldlicensenumber ||'(' || api.pkg_columnquery.Value(lp.oldlicenseid,'State') || ')' 
                             || ' - Permit ' || lp.permitid || ' - ' || lp.permitnumber || ' (' || api.pkg_columnquery.Value(lp.permitid,'State') || ')');
      api.pkg_relationshipupdate.Remove(t_RelId);
      -- Create primary Permit License for new License
      dbms_output.put_line ('Creating Primary for License ' || lp.newlicenseid || ' - ' || lp.newlicensenumber ||'(' || api.pkg_columnquery.Value(lp.newlicenseid,'State') || ')' 
                             || ' - Permit ' || lp.permitid || ' - ' || lp.permitnumber || ' (' || api.pkg_columnquery.Value(lp.permitid,'State') || ')');
      l := l + 1;      
      t_RelId := api.pkg_relationshipupdate.New(t_PermitLicenseEP,lp.permitid, lp.newlicenseid, sysdate);
   end loop;
   
   dbms_output.put_line('Permit Generational License relationships for non-active Licenses: ' || p);
   dbms_output.put_line('Primary Permit License relationships for active Licenses         : ' || l);
   api.pkg_logicaltransactionupdate.EndTransaction;
   commit;

end;