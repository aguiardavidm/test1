begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.objectid, p.permitnumber, p.edit, api.pkg_configquery.ColumnDefIdForName('o_abc_Permit', 'Edit') ColDefId
              from query.o_abc_permit p
             where p.permitnumber in ('14009656', '14010009', '15007219', '15007504', '20580')) loop
    insert into possedata.objectcolumndata cd
      (
      logicaltransactionid ,
      objectid ,
      columndefid ,
      attributevalue ,
      searchvalue 
      )
      values
      (
      api.pkg_logicaltransactionupdate.CurrentTransaction,
      i.objectid,
      i.ColDefId,
      'Y',
      'y'
      );
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;