-- Created on 1/3/2018 by MICHEL.SCHEFFERS 
-- Apply Instance Security on Permit Application 45277582
-- Issue 35545
-- Run time 1 second
declare 
  -- Local variables here
  t_Jobs integer := 0;

begin 
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Check Permit Application job 200155
  abc.pkg_InstanceSecurity.ApplyInstanceSecurity(45277582, sysdate);
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;