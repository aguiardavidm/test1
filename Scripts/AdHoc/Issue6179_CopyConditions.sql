create table ISSUE_6179(SourceLicenseObjectId    number(9),
                        NewLicenseObjectId       number(9),
                        SourceConditionObjectId  number(9),
                        NewConditionObjectId     number(9));
/                                               
declare 
  -- Local variables here
  i                        integer;
  rel                      integer;
  t_conditionendpoint      integer := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Condition');
  t_conditiontypeendpoint  integer := api.pkg_configquery.EndPointIdForName('o_ABC_Condition', 'ConditionType');
  t_NewConditionObjectId   integer;
  t_Status                 varchar2(100);
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();

  dbms_output.put_line('Continue');
  for l in (select l1.ObjectId, l1.MasterLicenseObjectId
              from query.o_abc_license l1
             where l1.ObjectId in (29559242, 29560402, 29560467, 29561357, 29561152, 29559162, 29560500, 29560524, 29561346, 29561289, 29561290, 29561283, 29561415, 29561417,
                                   29561423, 29561419, 29561103, 29561032, 29561113, 29561041, 29561721, 29561671, 29561736, 29561673, 29561789, 29561744, 29561785, 29561780, 
                                   29561781, 29561784, 29561404, 29561801, 29561794, 29561796, 29561833))
  loop
     for ml in (select r.LicenseObjectId
                  from query.r_ABC_MasterLicenseLicense r
                 where r.MasterLicenseObjectId = l.masterlicenseobjectid
                   and r.LicenseObjectId != l.ObjectId)
     loop
        if api.pkg_columnquery.Value(ml.licenseobjectid,'State') = 'Active' then
           dbms_output.put_line(ml.licenseobjectid || ' Active License: ' || api.pkg_columnquery.Value(ml.licenseobjectid,'LicenseNumber'));
           select count (*)
             into i
             from query.r_abc_licensecondition lc
            where lc.LicenseObjectId = ml.licenseobjectid;
           if i = 0 then
              for c in (select lc.ConditionObjectId, cct.ConditionTypeObjectId
                          from query.r_abc_licensecondition lc
                          join query.r_abc_conditionconditiontype cct on cct.ConditionObjectId = lc.ConditionObjectId
                         where lc.LicenseObjectId = l.objectid)
              loop
                 extension.pkg_objectupdate.CopyObject(c.conditionobjectid, t_NewConditionObjectId);
                 dbms_output.put_line ('Creating condition rel from ' || ml.licenseobjectid || ' to ' || t_NewConditionObjectId);
                 rel := api.pkg_relationshipupdate.New(t_conditionendpoint, ml.licenseobjectid, t_NewConditionObjectId);
                 rel := api.pkg_relationshipupdate.New(t_conditiontypeendpoint, t_NewConditionObjectId,c.conditiontypeobjectid);
                 insert into possedba.issue_6179
                    values (l.objectid, ml.licenseobjectid, c.conditionobjectid, t_NewConditionObjectId);
              end loop;
           end if;
        end if;
        if api.pkg_columnquery.Value(ml.licenseobjectid,'State') = 'Pending' then
           dbms_output.put_line(ml.licenseobjectid || ' Pending License: ' || api.pkg_columnquery.Value(ml.licenseobjectid,'LicenseNumber'));
           select api.pkg_columnquery.Value(al.AmendAppJobId,'StatusDescription')
             into t_Status
             from query.r_ABC_AmendAppJobLicense al
            where al.LicenseObjectId = ml.licenseobjectid
           union
           select api.pkg_columnquery.Value(rl.RenewAppJobId,'StatusDescription')
             from query.r_ABC_RenewAppJobLicense rl
            where rl.LicenseObjectId = ml.licenseobjectid;
           dbms_output.put_line('Job Status ' || t_Status);
           if t_Status not in ('Cancelled', 'Rejected') then
              select count (*)
                into i
                from query.r_abc_licensecondition lc
               where lc.LicenseObjectId = ml.licenseobjectid;
              if i = 0 then
                 for c in (select lc.ConditionObjectId, cct.ConditionTypeObjectId
                             from query.r_abc_licensecondition lc
                             join query.r_abc_conditionconditiontype cct on cct.ConditionObjectId = lc.ConditionObjectId
                            where lc.LicenseObjectId = l.objectid)
                 loop
                    extension.pkg_objectupdate.CopyObject(c.conditionobjectid, t_NewConditionObjectId);
                    dbms_output.put_line ('Creating condition rel from ' || ml.licenseobjectid || ' to ' || t_NewConditionObjectId);
                    rel := api.pkg_relationshipupdate.New(t_conditionendpoint, ml.licenseobjectid, t_NewConditionObjectId);
                    rel := api.pkg_relationshipupdate.New(t_conditiontypeendpoint, t_NewConditionObjectId,c.conditiontypeobjectid);
                    insert into possedba.issue_6179
                       values (l.objectid, ml.licenseobjectid, c.conditionobjectid, t_NewConditionObjectId);
                 end loop;
              end if;
           end if;
        end if;
     end loop;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  
end;
/