-- Created on 10/31/2017 by MICHEL.SCHEFFERS 
-- Issue 26271 - Set Original Issue Date on Permit
declare 
  -- Local variables here
  t_OriginalIssueDate    date;
  t_IdList               api.pkg_Definition.udt_IdList;
  t_LicenseColumnDefId   number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'OriginalIssueDate');
  t_PermitColumnDefId    number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'OriginalIssueDate');
  t_CurrentTransaction   api.pkg_definition.Udt_Id;
  t_FormattedDate        varchar2(400);
  c1                     integer := 0;
  c2                     integer := 0;
  c3                     integer := 0;
begin
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_CurrentTransaction := api.pkg_logicaltransactionupdate.CurrentTransaction;  
  -- Update Original Issue Date for Permits
  dbms_output.put_line('Updating Original Issue Date for Permits');
  for p1 in (select p.PermitNumber, min(trunc(p.IssueDate)) IssueDate
             from   query.o_abc_permit p
             where  p.PermitNumber is not null
             and    p.IssueDate is not null
             group  by p.PermitNumber
            ) loop
     c1 := c1 + 1;
     dbms_output.put_line('Original Issue Date for Permit ' || p1.permitnumber || ': ' || p1.IssueDate);
     select objectid
     bulk   collect into t_IdList
     from   query.o_abc_permit p
     where  p.PermitNumber = p1.permitnumber;
     dbms_output.put_line('   Records for Permit ' || p1.permitnumber || ': ' || t_IdList.count);
     for i in 1..t_IdList.count loop
        c2 := c2 + 1;
        if api.pkg_columnquery.Value(t_IdList(i), 'State') != 'Pending' and 
           api.pkg_columnquery.DateValue(t_IdList(i), 'OriginalIssueDate') is null then
           t_FormattedDate := to_char(p1.issuedate, 'YYYY-MM-DD');
           c3 := c3 + 1;
           dbms_output.put_line('   Updating Original Issue Date for ' || t_IdList(i) || ' to ' || t_FormattedDate);
           insert into possedata.objectcolumndata cd
                   (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue, cd.searchvalue)
            Values (t_CurrentTransaction, t_IdList(i), t_PermitColumnDefId, t_FormattedDate, t_FormattedDate);
        end if;
     end loop;
  end loop;
  dbms_output.put_line(c1 || ' Master Permits found');
  dbms_output.put_line(c2 || ' PermitsFound');
  dbms_output.put_line(c3 || ' Permits Updated');
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
