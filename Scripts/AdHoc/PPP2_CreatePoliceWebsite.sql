-- Created on 1/21/2019 by JAE.HOLDERBY 
-- Creates a new Website Object named 'Police'
-- Executes < 1 sec 
declare
 
t_WebsiteDefId                          api.pkg_definition.udt_Id;
t_WebsiteObjectId                       api.pkg_definition.udt_Id;
begin

  select od.ObjectDefId
  into t_WebsiteDefId
  from api.objectdefs od
  where od.Name = 'o_ABC_WebSite';
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  t_WebsiteObjectId := api.pkg_objectupdate.New(t_WebsiteDefId);
  api.pkg_columnupdate.SetValue(t_WebsiteObjectId, 'SiteName', 'Police');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  
end;
