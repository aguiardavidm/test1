-- Created on 4/5/2016 by MICHEL.SCHEFFERS 
-- Register new process p_ABC_ConfirmRelief
-- Task 9323
declare

begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select pt.ProcessTypeId, pt.Name
              from api.processtypes pt 
             where pt.Name in (                         
                               'p_ABC_ConfirmRelief'
                              )
                and not exists (select 1
                                  from query.o_processtype q where q.ProcessTypeId = pt.ProcessTypeId)
           ) loop
     dbms_output.put_line('Registering ' || i.name);
     api.pkg_objectupdate.RegisterExternalObject('o_ProcessType', i.processtypeid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
