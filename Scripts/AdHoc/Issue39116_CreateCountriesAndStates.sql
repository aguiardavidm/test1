/*-----------------------------------------------------------------------------
 * Expected run time:
 * Purpose:To add all needed States and Countries to the state of registry
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_CountryId                           udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
  end;
  
  function CreateCountry(
    a_Name                              varchar2,
    a_Code                              varchar2
  ) return udt_id is
    t_CountryId                         udt_Id;
    t_CountryDefID                      udt_Id
      := api.pkg_configquery.ObjectDefIdForName('o_ABC_Country');
  begin
    t_CountryId := api.pkg_ObjectUpdate.New(t_CountryDefID);
    api.pkg_ColumnUpdate.SetValue(t_CountryId,'Name', a_Name);
    api.pkg_ColumnUpdate.SetValue(t_CountryId,'Code', a_Code);
    api.pkg_ColumnUpdate.SetValue(t_CountryId,'Active','Y');
    dbms_output.put_line(t_CountryId || ': ' || a_Code || ' - Country');
    return t_CountryId;
  end;
  
  procedure CreateState(
    a_Name                              varchar2,
    a_Code                              varchar2,
    a_Subdivision                       varchar2,
    a_CountryId                         udt_Id
  ) is
    t_CountryEndpointID                 udt_Id
      := api.pkg_Configquery.EndPointIdForName('o_ABC_Country','Country');
    t_RelId                             udt_Id;
    t_StateId                           udt_Id;
    t_StateDefID                        udt_Id
      := api.pkg_configquery.ObjectDefIdForName('o_ABC_State');
  begin
    t_StateId := api.pkg_ObjectUpdate.New(t_StateDefID);
    api.pkg_ColumnUpdate.SetValue(t_StateId,'Name', a_Name);
    api.pkg_ColumnUpdate.SetValue(t_StateId,'Code', a_Code);
    api.pkg_ColumnUpdate.SetValue(t_StateId,'Subdivision', a_Subdivision);
    api.pkg_ColumnUpdate.SetValue(t_StateId,'Active','Y');
    t_RelId := api.pkg_relationshipupdate.New(t_CountryEndpointId,a_CountryId,t_StateId);
    dbms_output.put_line(t_CountryId || ': ' || a_Code || ' - State');
  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  
  for i in (
    select objectid
    from query.o_ABC_State s) loop
    api.pkg_objectupdate.Remove(i.objectid, sysdate);
  end loop;
  
  for i in (
    select objectid
    from query.o_ABC_Country) loop
    api.pkg_objectupdate.Remove(i.objectid, sysdate);
  end loop;
  
  -- USA! USA! USA!
  t_CountryId := CreateCountry('United States of America','USA');
  CreateState('District of Columbia','DC','District',t_CountryId);
  CreateState('American Samoa','AS','Outlying Area',t_CountryId);
  CreateState('Guam','GU','Outlying Area',t_CountryId);
  CreateState('Northern Mariana Islands','MP','Outlying Area',t_CountryId);
  CreateState('Puerto Rico','PR','Outlying Area',t_CountryId);
  CreateState('United States Minor Outlying Islands','UM','Outlying Area',t_CountryId);
  CreateState('Virgin Islands, U.S.','VI','Outlying Area',t_CountryId);
  CreateState('Alabama','AL','State',t_CountryId);
  CreateState('Alaska','AK','State',t_CountryId);
  CreateState('Arizona','AZ','State',t_CountryId);
  CreateState('Arkansas','AR','State',t_CountryId);
  CreateState('California','CA','State',t_CountryId);
  CreateState('Colorado','CO','State',t_CountryId);
  CreateState('Connecticut','CT','State',t_CountryId);
  CreateState('Delaware','DE','State',t_CountryId);
  CreateState('Florida','FL','State',t_CountryId);
  CreateState('Georgia','GA','State',t_CountryId);
  CreateState('Hawaii','HI','State',t_CountryId);
  CreateState('Idaho','ID','State',t_CountryId);
  CreateState('Illinois','IL','State',t_CountryId);
  CreateState('Indiana','IN','State',t_CountryId);
  CreateState('Iowa','IA','State',t_CountryId);
  CreateState('Kansas','KS','State',t_CountryId);
  CreateState('Kentucky','KY','State',t_CountryId);
  CreateState('Louisiana','LA','State',t_CountryId);
  CreateState('Maine','ME','State',t_CountryId);
  CreateState('Maryland','MD','State',t_CountryId);
  CreateState('Massachusetts','MA','State',t_CountryId);
  CreateState('Michigan','MI','State',t_CountryId);
  CreateState('Minnesota','MN','State',t_CountryId);
  CreateState('Mississippi','MS','State',t_CountryId);
  CreateState('Missouri','MO','State',t_CountryId);
  CreateState('Montana','MT','State',t_CountryId);
  CreateState('Nebraska','NE','State',t_CountryId);
  CreateState('Nevada','NV','State',t_CountryId);
  CreateState('New Hampshire','NH','State',t_CountryId);
  CreateState('New Jersey','NJ','State',t_CountryId);
  CreateState('New Mexico','NM','State',t_CountryId);
  CreateState('New York','NY','State',t_CountryId);
  CreateState('North Carolina','NC','State',t_CountryId);
  CreateState('North Dakota','ND','State',t_CountryId);
  CreateState('Ohio','OH','State',t_CountryId);
  CreateState('Oklahoma','OK','State',t_CountryId);
  CreateState('Oregon','OR','State',t_CountryId);
  CreateState('Pennsylvania','PA','State',t_CountryId);
  CreateState('Rhode Island','RI','State',t_CountryId);
  CreateState('South Carolina','SC','State',t_CountryId);
  CreateState('South Dakota','SD','State',t_CountryId);
  CreateState('Tennessee','TN','State',t_CountryId);
  CreateState('Texas','TX','State',t_CountryId);
  CreateState('Utah','UT','State',t_CountryId);
  CreateState('Vermont','VT','State',t_CountryId);
  CreateState('Virginia','VA','State',t_CountryId);
  CreateState('Washington','WA','State',t_CountryId);
  CreateState('West Virginia','WV','State',t_CountryId);
  CreateState('Wisconsin','WI','State',t_CountryId);
  CreateState('Wyoming','WY','State',t_CountryId);

  -- OH CANADA!
  t_CountryId := CreateCountry('Canada','CAN');
  CreateState('Newfoundland and Labrador','NL','Province',t_CountryId);
  CreateState('Prince Edward Island','PE','Province',t_CountryId);
  CreateState('Nova Scotia','NS','Province',t_CountryId);
  CreateState('New Brunswick','NB','Province',t_CountryId);
  CreateState('Quebec','QC','Province',t_CountryId);
  CreateState('Ontario','ON','Province',t_CountryId);
  CreateState('Manitoba','MB','Province',t_CountryId);
  CreateState('Saskatchewan','SK','Province',t_CountryId);
  CreateState('Alberta','AB','Province',t_CountryId);
  CreateState('British Columbia','BC','Province',t_CountryId);
  CreateState('Yukon','YT','Territory',t_CountryId);
  CreateState('Northwest Territories','NT','Territory',t_CountryId);
  CreateState('Nunavut','NU','Territory',t_CountryId);
  
  --Other
  t_CountryId := CreateCountry('Other','O');
  api.pkg_ColumnUpdate.SetValue(t_CountryId,'Other','Y');
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
