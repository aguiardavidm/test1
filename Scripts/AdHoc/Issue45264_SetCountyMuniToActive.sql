-- Created on 02/26/2019 by JAE.HOLDERBY
/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: This script Sets the Active Detail on all Counties and
 * Municipalities in the system to 'Y'
 *---------------------------------------------------------------------------*/
declare
  -- Local variables here
  t_CountyList         api.Pkg_Definition.udt_IdList;
  t_MunicipalityList   api.Pkg_Definition.udt_IdList;
  t_Counter            integer := 0;
begin

  api.pkg_logicaltransactionupdate.ResetTransaction();
  --Collect the Inactive CountyId's and MunicipalityId's into a list to loop through
  select
    r.objectId
  bulk collect into
    t_CountyList
  from query.o_abc_region r
  where r.Active = 'N';

  select
    o.ObjectId
  bulk collect into
    t_MunicipalityList
  from query.o_abc_office o
  where o.Active = 'N';

 --Cycle through Counties and set them to Active
  for i in 1.. t_CountyList.count loop
    api.pkg_columnupdate.SetValue(t_CountyList(i), 'Active', 'Y');

    t_Counter := t_Counter +1;

    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  --Cycle through Municipalities and set them to Active
  for i in 1.. t_MunicipalityList.count loop
    api.pkg_columnupdate.SetValue(t_MunicipalityList(i), 'Active', 'Y');

    t_Counter := t_Counter +1;

    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  dbms_output.put_line(t_Counter);
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
