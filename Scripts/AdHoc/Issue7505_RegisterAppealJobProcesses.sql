declare

begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select pt.ProcessTypeId, pt.Name
              from api.processtypes pt 
             where pt.Name in (                         
                               -- Appeal Job
                               'p_ABC_EnterAppeal',
                               'p_ABC_InitialProcessing',
                               'p_ABC_ManageHearing',
                               'p_ABC_RecordOALDescision',
                               'p_ABC_DraftFinalOrder',
                               'p_ABC_SendOrderToLicensee',
                               'p_ABC_PaymentProcessing',
                               'p_ABC_FineAssessment'
                              )
                and not exists (select 1
                                  from query.o_processtype q where q.ProcessTypeId = pt.ProcessTypeId)
           ) loop
     dbms_output.put_line('Registering ' || i.name);
     api.pkg_objectupdate.RegisterExternalObject('o_ProcessType', i.processtypeid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
