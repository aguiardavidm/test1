/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 sec
 * Purpose: This script puts an Active License back into the Pending state and
 *  removes the license number. Then it changes the status of the related New
 *  Application job from "Approved" to "In Review" and adds back the
 *  Administrative Review Process.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_ChangeStatus                        pls_Integer;
  t_IssueNumber                         varchar2(6) := '55618';
  t_JobId                               pls_Integer;
  t_LicenseToJobEndpointId              pls_Integer;
  t_LicenseNumber                       varchar2(50) := '1106-44-014-001';
  t_LicenseObjectId                     pls_Integer;
  t_NoteText                            varchar2(4000);
  t_ProcessId                           pls_integer;
  t_ProcessTypesToAdd                   udt_IdList;
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

  procedure createCaseNote (
    a_ObjectId                          pls_integer,
    a_NoteText                          varchar2
  ) is
    t_NoteDefId                         pls_integer;
    t_NoteId                            pls_integer;
    t_NoteTag                           varchar2(6) := 'GEN';
  begin

  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;

  t_NoteId := api.pkg_NoteUpdate.New(a_ObjectId, t_NoteDefId, 'Y', a_NoteText);

  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  -- LICENSE
  t_LicenseToJobEndpointId := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_License',
      'NewApplication');

  select
    r.FromObjectId,
    r.ToObjectId
  into
    t_LicenseObjectId,
    t_JobId
  from api.relationships r
  where r.EndpointId = t_LicenseToJobEndpointId
    and r.FromObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_ABC_License', 'LicenseNumber',
        t_LicenseNumber);

  dbug('Updating License w/ ObjectId: ' || t_LicenseObjectId || chr(13) || chr(10) ||
      'Updating JobId: ' || t_JobId);

  -- Set License State back to "Pending" and remove License Number.
  api.pkg_ColumnUpdate.SetValue(t_LicenseObjectId, 'State', 'Pending');
  api.pkg_ColumnUpdate.RemoveValue(t_LicenseObjectId, 'LicenseNumber');

  -- JOB
  t_ChangeStatus := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');

  -- Get ProcessTypeId for Processes to Add
  select pt.ProcessTypeId
  bulk collect into t_ProcessTypesToAdd
  from api.processtypes pt
  where pt.Name in ('p_ABC_AdministrativeReview');

  -- Populate Case Note Text & create on Job
  t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
      'Job Completed before License was ready to issue. System corrected Job status from ' ||
      '"Approved" to "In Review." See Issue Tracker #' || t_IssueNumber || '.';
  createCaseNote(t_JobId, t_NoteText);

  -- Change Job Status
  t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ChangeStatus, null, null, null, null);
  api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ReasonForChange', t_NoteText);
  api.pkg_ProcessUpdate.Complete(t_ProcessId, 'In Review');

  -- Add new Processes
  for i in 1..t_ProcessTypesToAdd.count() loop
    t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ProcessTypesToAdd(i), null, null, null, null);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
