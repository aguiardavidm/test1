-- Created on 02/11/2016 by MICHEL.SCHEFFERS 
-- Issue7294: Distributor License not being updated for brands
-- Set boolean TransferDistributorLicense on Amendment Type to "Y" except for Person To Person (Retail & State) and Person to Person and Place to Place (Retail & State).
declare 
  -- Local variables here
  t_AmendmentObjectId  api.pkg_Definition.udt_idlist;
  t_Updated            number := 0;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  select a.ObjectId
    bulk collect into t_AmendmentObjectId
    from query.o_ABC_AmendmentType a
   where a.Name not in ('Person to Person Transfer - Retail Licenses', 'Person to Person Transfer - State Issued Licenses', 'Person to Person and Place to Place Transfer - Retail Licenses', 'Person to Person and Place to Place Transfer - State Issued Licenses');
  
  for x in 1..t_AmendmentObjectId.count loop
     dbms_output.put_line('Updating Amendment ' || api.Pkg_Columnquery.Value(t_AmendmentObjectId(x),'Name'));
     t_Updated := t_Updated + 1;
     api.Pkg_Columnupdate.SetValue(t_AmendmentObjectId(x), 'TransferDistributorLicense', 'Y');
  end loop;
     
  dbms_output.put_line ('Amendments updated: ' || t_Updated);  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
