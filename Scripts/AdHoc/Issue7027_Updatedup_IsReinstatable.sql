-- Created on 02/23/2016 by MICHEL.SCHEFFERS 
-- Issue7027: Need to be able to reinstate licenses in states other than Revoked
-- Update all dup_IsReinstatable for Licenses.
-- 03/04 Conv: 78,937 licenses, 24 seconds
declare 
  -- Local variables here
  c                          number := 0;
  d                          number := 0;
  l                          number := 0;
  t_LicenseIds               api.pkg_definition.udt_IdList;
  t_IsReinstatable           api.pkg_definition.udt_StringList;
  t_ColumnDefId              number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'dup_IsReinstatable');
  t_Count                    number;
  t_CurrentTransaction       api.pkg_definition.Udt_Id;
      
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  t_CurrentTransaction := api.pkg_logicaltransactionupdate.CurrentTransaction;
  api.pkg_logicaltransactionupdate.EndTransaction();

  select l.ObjectId, l.IsReinstatable
    bulk collect into t_LicenseIds, t_IsReinstatable
    from query.o_abc_License l;
  for i in 1 .. t_LicenseIds.count loop
     l := l + 1;
     select count (*)
       into t_Count
       from possedata.ObjectColumnData d
      where d.ObjectId = t_LicenseIds (i)
        and d.ColumnDefId = t_ColumnDefId;
             
     if t_Count = 0 and t_IsReinstatable (i) = 'Y' then
        insert into possedata.objectcolumndata cd
                   (cd.logicaltransactionid, cd.objectid, cd.columndefid, cd.attributevalue, cd.searchvalue)
            Values (t_CurrentTransaction, t_LicenseIds(i), t_ColumnDefId, 'Y', 'y');
        c := c + 1;
     end if;

     if t_Count > 0 and t_IsReinstatable (i) = 'Y' then
        update possedata.objectcolumndata cd
           set cd.attributevalue = 'Y',
               cd.searchvalue = 'y'
         where cd.objectid = t_LicenseIds(i)
           and cd.columndefid = t_ColumnDefId;
        c := c + 1;
     end if;

     if t_Count > 0 and t_IsReinstatable (i) = 'N' then
        delete from possedata.objectcolumndata cd
         where cd.objectid = t_LicenseIds(i)
           and cd.columndefid = t_ColumnDefId;
        d := d + 1;
     end if;
  end loop;

  commit;  

  dbms_output.put_line('License records found          : ' || l);
  dbms_output.put_line('IsReinstatable records created : ' || c);
  dbms_output.put_line('IsReinstatable records deleted : ' || d);
end;
