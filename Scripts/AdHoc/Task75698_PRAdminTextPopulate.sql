-- Script runs in around 0.6 sec
-- Sets informational text for brand registration
declare 
  t_RunAsUser   varchar2(30) := 'POSSEDBA';
  t_ObjectId Number;
begin
  if sys_context('userenv', 'current_schema') != t_RunAsUser then
    raise_application_error(-20006, 'Please run script as ' || t_RunAsUser || '.');
  end if;

  select objectid
  into t_ObjectId
  from query.o_systemsettings;  

  api.Pkg_Logicaltransactionupdate.ResetTransaction();
  
  -- General
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRJobLegalDisclaimer', '<font face="Calibri" size="2" color="red">By checking the box below,</font> you are certifying that the information that you are providing to the State of New Jersey is correct and accurate to the best of your knowledge. Upon submission of payment, you will receive a confirmation. However, if your payment is subsequently deemed unsuccessful by the bank, a penalty will be assessed by this Division.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InternalVintageRequestInstrTxt', 'These comments were made by the online applicant to request Vintages that were not available for selection via the public site and/or identify products with a deposit label or another state.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PublicVintageRequestInstrTxt', 'Use the field below to request a vintage that is not available for selection and/or to identify any product(s) with a deposit label of another state.');
  
  --Application
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRApplicationPreambleText', 'Note: All Registrants based in New Jersey must be associate with at least one Licensee in order to create a Product Registration Application');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRApplicationNewRegistrantText', 'Please list the person or entity applying to register products.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRApplicationQuestionsText', 'Please answer ALL of the following application questions. You may be required to upload any corresponding documents on the Documents page.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRApplicationDocumentsText', 'The following documents may be required to be submitted with your application. A mandatory upload will be denoted by the "yes" in the "Required" column. Use the "Upload File" button below to begin attaching the required documents. If you have any of the non mandatory application documents, you may upload them at this time as well. If a sample is provided, please follow the prescribed format.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'TTBCOLAInstructionalText', 'The TTB/COLA column in the table below is to indicate whether the product being registered has received TTB label approval.</br>If TTB label approval has not been received, a detailed explanation will need to be provided.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'TTBCOLAWarningText', 'By clicking "No", you are certifying that your product is exempt from TTB''s COLA requirement. The Division reserves its right to request additional information related to your representation.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRApplicationLegalDisclaimer', '<font face="Calibri" size="2" color="red">By checking the box below,</font> you are confirming that for malt alcoholic beverage products being registered (with the exception of malt coolers and cider products) where deposit information appears on a label sold in New Jersey that the total of all shipments from the brewery for the past full or part calendar year in New Jersey did not exceed 3,000 barrels of 31 fluid gallon capacity or the container equivalent. If the total of all shipments from the brewery of malt alcoholic beverages for the past full or part calendar year exceeds 3,000 barrels of 31 fluid gallons capacity or the container equivalent, you must apply to the division for a waiver of <u>N.J.A.C.</u> 13:2-27.2.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'TTBCOLALegalDisclaimer', '<font face="Calibri" size="2" color="red">By checking the box below,</font> you are certifying that your product(s) is exempt from TTB''s COLA requirement.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAppPaymentConfirmationTxt', 'Your payment has been received and your Product Registration Application has been successfully submitted for review.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAppNoPaymentConfirmTxt', 'Your Product Registration Application has been successfully submitted for review.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAppConfirmPostScript', 'If you have any questions, please contact us by using the information in the "Contact ABC" found in the above right hand corner of your screen. You may return to the Main Menu at any time to monitor the processing of your application.');
  
  -- Renewals
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRRenewalPreambleTxt', 'Select a Registrant with products that are eligible for renewal.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRRenewalQuestionsTxt', 'Before you can submit your renewal, please answer the following questions: ');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRRenewalDocumentsTxt', 'The following documents may be required to be submitted with your application. A mandatory upload will be denoted by the "yes" in the "Required" column. Use the "Upload File" button below to begin attaching the required documents. If you have any of the non mandatory application documents, you may upload them at this time as well. If a sample is provided, please follow the prescribed format.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRRenewalLegalDisclaimer', '<font face="Calibri" size="2" color="red">By checking the box below,</font> you are confirming that for malt alcoholic beverage products being registered (with the exception of malt coolers and cider products) where deposit information appears on a label sold in New Jersey that the total of all shipments from the brewery for the past full or part calendar year in New Jersey did not exceed 3,000 barrels of 31 fluid gallon capacity or the container equivalent. If the total of all shipments from the brewery of malt alcoholic beverages for the past full or part calendar year exceeds 3,000 barrels of 31 fluid gallons capacity or the container equivalent, you must apply to the division for a waiver of <u>N.J.A.C.</u> 13:2-27.2.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRRenewalPaymentConfirmTxt', 'Your payment has been received and your Product Registration Renewal has been successfully submitted for review.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRRenewalNoPaymentConfirmation', 'Your Product Registration Renewal has been successfully submitted for review.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRRenewalConfirmPostScript', 'The Division of Alcoholic Beverage Control is reviewing this application. You can log on and monitor the status of this or any application at any time. Thank You!');
  
  -- Amendments
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAmendmentPreambleText', 'If you manage a product that requires amending, select the Registrant to whom the product is registered.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAmendmentsQuestionsText', 'Before you can submit your amendment, please answer the following questions:');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAmendmentDocumentsText', 'The following documents may be required to be submitted with your application. A mandatory upload will be denoted by the "yes" in the "Required" column. Use the "Upload File" button below to begin attaching the required documents. If you have any of the non mandatory application documents, you may upload them at this time as well. If a sample is provided, please follow the prescribed format.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAmendmentLegalDisclaimer', '<font face="Calibri" size="2" color="red">By checking the box below,</font> you are confirming that for malt alcoholic beverage products being registered (with the exception of malt coolers and cider products) where deposit information appears on a label sold in New Jersey that the total of all shipments from the brewery for the past full or part calendar year in New Jersey did not exceed 3,000 barrels of 31 fluid gallon capacity or the container equivalent. If the total of all shipments from the brewery of malt alcoholic beverages for the past full or part calendar year exceeds 3,000 barrels of 31 fluid gallons capacity or the container equivalent, you must apply to the division for a waiver of <u>N.J.A.C.</u> 13:2-27.2.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAmendmentPaymentConfirmTxt', 'Your payment has been received and your Product Registration Amendment has been successfully submitted for review.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAmendNoPaymentConfirmTxt', 'Your Product Registration Amendment has been successfully submitted for review.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PRAmendConfirmPostScript', 'The Division of Alcoholic Beverage Control is reviewing this application. You can log on and monitor the status of this or any application at any time. Thank you!');
    -- PublicBrandDraftsInst
    -- PublicBrandAppsReviewInst
    -- PublicBrandDeniedInst
    -- ProductHighVolumeMessage
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
/
