begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.PermitNumber, p.State, p.CalculatedExpirationDate, p.ExpirationDate, p.IssueDate, p.ObjectId
              from possedba.permitdatefix_t f
              join query.o_abc_permit p on p.ObjectId = f.permitid
              where f.permittype like '%alesroo%'
              and p.PermitNumber not like '%-%'
              and p.IssueDate > to_date('6/29/2015', 'mm/dd/yyyy')
              order by p.IssueDate) loop
    api.pkg_columnupdate.SetValue(i.objectid, 'ExpirationDate', to_date('6/30/2016', 'mm/dd/yyyy'));
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;