-- Created on 9/07/2017 by Michel Scheffers
-- Issue 31795 12.18 Warning Text Data Fix
declare 
  -- Local variables here

begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;

  for l in (select w.objectid, r.LicenseId
            from   api.objects o
            join   query.o_abc_licensewarning w on w.LogicalTransactionId = o.LogicalTransactionId
            join   query.r_warninglicense r on r.LicenseWarningId = w.ObjectId
            where  o.ExternalFileNum = '177728'
           ) loop
     api.pkg_columnupdate.setvalue (l.objectid, 'Comments', '12.18 RULING REQUIRED FOR 2017-2018 ON AUG 02, 2017');
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;  
end;