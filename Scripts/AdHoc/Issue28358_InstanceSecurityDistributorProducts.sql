-- Created on 3/3/2017 by MICHEL.SCHEFFERS 
-- Issue 28358 - PROD PUBLIC site - Product search is not returning any results
declare 
  -- Local variables here
    t_LEObjectId          number;
    u                     integer := 0;
    p                     integer := 0;

  begin
    dbms_output.enable(null);
    api.pkg_logicaltransactionupdate.ResetTransaction;
    --find the Public User - Legal Entity relationships
    for le in (select ul.RelationshipId, ul.UserId, ul.LegalEntityObjectId
               from   query.r_ABC_UserLegalEntity ul
              ) loop
       t_LEObjectId := le.legalentityobjectid;
       u := u + 1;
       
       --Products
       for x in (select r.ProdictId ProductId
                 from   query.r_LegalEntityDist r
                 where  r.LegalEntityId = t_LEObjectId
                ) loop
          abc.pkg_instancesecurity.ApplyInstanceSecurity(x.ProductId, sysdate);
          p := p + 1;
       end loop;
    end loop;
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
    dbms_output.put_line (u || ' Users found, ' || p || ' Products updated');
end;
