-- Created on 4/11/2016 by MICHEL.SCHEFFERS 
-- Issue 9466 - Update Plenary Winery production
declare 
  -- Local variables here
  t_LicenseTypeId    number;
  i                  number := 0;
begin
  -- Test statements here
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  begin
     select lt.ObjectId
     into   t_LicenseTypeId
     from   query.o_abc_licensetype lt
     where  lt.Code = '21';
  exception
     when no_data_found then
        api.pkg_errors.RaiseError(-20000,'No License Type record for 21!');
  end;
  
  for pwp in (select l.ObjectId, l.PlenaryWineryProduction
               from   query.o_abc_license l
               where  l.LicenseTypeObjectId = t_LicenseTypeId
              ) loop
     if pwp.PlenaryWineryProduction = 'Less than 50,000 gallons' then
        dbms_output.put_line('Updating Winery Production of ' ||  pwp.objectid || '(' || api.pkg_columnquery.Value(pwp.objectid,'LicenseNumber') || ') from ' || pwp.PlenaryWineryProduction);
        i := i + 1;
        api.pkg_columnupdate.SetValue(pwp.objectid,'PlenaryWineryProduction', 'Between 0 and 50,000 gallons');
     end if;
  end loop;
  
  dbms_output.put_line('Updating Plenary Winery Production for ' || i || ' licenses.');  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
