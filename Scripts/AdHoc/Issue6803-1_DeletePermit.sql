declare
  t_PermitId number := api.pkg_simplesearch.ObjectByIndex('o_abc_permit', 'PermitNumber', '15010082');
begin
  api.pkg_logicaltransactionupdate.resetTransaction;
  api.pkg_objectupdate.Remove(t_PermitId, sysdate);
  api.pkg_logicaltransactionupdate.EndTransaction;
end;