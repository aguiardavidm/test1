/*-----------------------------------------------------------------------------
 * Modifying Author: Paul
 * Expected run time:
 * Purpose: To change the Job Status to In Review and add Administrative Review
 *  processes to Job in NJPROD.  No processes need to be removed.
 *
 *  For Issue (not created yet), when running script in NJPROD, set...
 *     t_JobNumber := '355841'
 *     t_ProcessTypeToRemove := ''
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_ChangeStatus                        number
      := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');
  t_JobId                               number;
  t_JobNumber                           varchar2(40) := '47127';
  t_ProcessId                           number;
  t_ProcessToRemoveId                   number;
  t_ProcessTypesToAdd                   udt_IdList;
  t_ProcessTypeToRemove                 varchar2(30) := '';
  t_ProcessTypeToRemoveId               number;
begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  -- Get ProcessTypeId for Processes to Add
  select pt.ProcessTypeId
  bulk collect into t_ProcessTypesToAdd
  from api.processtypes pt
  where pt.Name in ('p_ABC_AdministrativePermReview');

  -- Get JobId
  select
    x.ObjectId
  into
    t_JobId
  from
    table(api.pkg_SimpleSearch.CastableObjectsByIndex('j_ABC_PermitApplication',
        'ExternalFileNum', t_JobNumber)) x;

  -- Change Job Status to Review
  t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ChangeStatus, null, null, null, null);
  api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ReasonForChange',
      'Job Completed with wrong outcome. See Issue number');
  api.pkg_ProcessUpdate.Complete(t_ProcessId, 'In Review');

  -- Add Processes
  for i in 1..t_ProcessTypesToAdd.count() loop
    t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ProcessTypesToAdd(i), null, null, null, null);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();

  commit;

end;
