SELECT count(0), max(ag.Description)
  FROM api.accessgroups ag
       WHERE NOT EXISTS (
                         SELECT 1
                         FROM api.accessgroupusers agu
                              WHERE agu.accessgroupid=ag.accessgroupid)
     AND ag.description LIKE 'www0%'
     AND ag.Description > 'www000010000' -- to get a more accurate count