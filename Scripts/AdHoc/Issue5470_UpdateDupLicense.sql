declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Establishment', 'dup_LicenseNumbers');
begin
  for i in (select e.LicenseNumbers, e.ObjectId, e.dup_LicenseNumbers
              from query.o_abc_establishment e) loop
  if i.licensenumbers is not null and i.dup_LicenseNumbers is null then
    insert into possedata.objectcolumndata
        (
        logicaltransactionid,
        objectid,
        columndefid,
        attributevalue,
        searchvalue
        )
        values
        (
        api.pkg_logicaltransactionupdate.CurrentTransaction,
        i.ObjectId,
        t_ColumnDefId,
        i.licensenumbers,
        lower(substrb(i.licensenumbers, 1, 20))
        );
    end if;
  end loop;
end;
