--Mostly so we have the data easily accessible, helps performance too
create table dataconv.LegalEntityPhoneFix as
select l.ObjectId, l.DoingBusinessAs, l.ContactPhoneNumber, l.ContactAltPhoneNumber, l.ContactFaxNumber
  from query.o_abc_legalentity l
 where ((l.ContactPhoneNumber is not null and length(l.ContactPhoneNumber) != 10)
        or (l.ContactAltPhoneNumber is not null and length(l.ContactAltPhoneNumber) != 10)
        or (l.ContactFaxNumber is not null and length(l.ContactFaxNumber) != 10));
create table dataconv.EstablishmentPhoneFix as
select l.ObjectId, l.DoingBusinessAs, l.ContactPhoneNumber, l.ContactAltPhoneNumber, l.ContactFaxNumber
  from query.o_abc_establishment l
 where ((l.ContactPhoneNumber is not null and length(l.ContactPhoneNumber) != 10)
        or (l.ContactAltPhoneNumber is not null and length(l.ContactAltPhoneNumber) != 10)
        or (l.ContactFaxNumber is not null and length(l.ContactFaxNumber) != 10));
/
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *
               from dataconv.legalentityphonefix) loop
    if length(i.ContactPhoneNumber) != 10 then
      api.pkg_columnupdate.RemoveValue(i.objectid, 'ContactPhoneNumber');
    end if;
    if length(i.ContactAltPhoneNumber) != 10 then
      api.pkg_columnupdate.RemoveValue(i.objectid, 'ContactAltPhoneNumber');
    end if;
    if length(i.ContactFaxNumber) != 10 then
      api.pkg_columnupdate.RemoveValue(i.objectid, 'ContactFaxNumber');
    end if;
  end loop;
  for e in (select *
               from dataconv.establishmentphonefix) loop
    if length(e.ContactPhoneNumber) != 10 then
      api.pkg_columnupdate.RemoveValue(e.objectid, 'ContactPhoneNumber');
    end if;
    if length(e.ContactAltPhoneNumber) != 10 then
      api.pkg_columnupdate.RemoveValue(e.objectid, 'ContactAltPhoneNumber');
    end if;
    if length(e.ContactFaxNumber) != 10 then
      api.pkg_columnupdate.RemoveValue(e.objectid, 'ContactFaxNumber');
    end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;