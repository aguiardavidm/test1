drop   table possedba.issue29585InvalidEmails;
create table possedba.issue29585InvalidEmails
(
   ObjectId        number,
   Name            varchar2(1000),
   Email           varchar2(1000),
   Type            varchar2(40),
   Active          varchar2(01)
);
/
DECLARE
  I INTEGER;
  TYPE T_ARRAY_OF_VARCHAR IS TABLE OF VARCHAR2(2000) INDEX BY BINARY_INTEGER;
  MY_ARRAY T_ARRAY_OF_VARCHAR;
  MY_STRING VARCHAR2(4000);
BEGIN
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for le1 in (select le.ObjectId, le.FormattedName Name, le.ContactEmailAddress Email, 'Legal Entity' UserType, le.Active
              from   query.o_abc_legalentity le
              where  le.ContactEmailAddress is not null
              union
              select u.ObjectId, u.FormattedName Name, u.EmailAddress Email, u.UserType UserType, u.Active
              from   query.u_users u
              where  u.EmailAddress is not null
              union
              select e.ObjectId, e.DoingBusinessAs Name, e.ContactEmailAddress Email, 'Establishment' UserType, e.Active
              from   query.o_abc_establishment e
              where  e.ContactEmailAddress is not null
              order  by 2
             ) loop
     my_string := le1.email;
     FOR CURRENT_ROW IN (
     with test as    
        (select MY_STRING from dual)
         select regexp_substr(MY_STRING, '[^;]+', 1, rownum) SPLIT
         from   test
         connect by level <= length (regexp_replace(MY_STRING, '[^;]+'))  + 1)
     LOOP
       if not REGEXP_LIKE (ltrim(rtrim(CURRENT_ROW.SPLIT)),'^(\S+)\@(\S+)\.(\S+)$') then
          insert into possedba.issue29585InvalidEmails
                    (ObjectId, Name, Email, Type, Active)
                 values
                    (le1.objectid, le1.name, le1.email, le1.usertype, le1.active);
--          DBMS_OUTPUT.PUT_LINE(le1.name || ' (' || le1.usertype || '). Email: "' || CURRENT_ROW.SPLIT || '" is Invalid');
          MY_ARRAY(MY_ARRAY.COUNT) := CURRENT_ROW.SPLIT;
       end if;
     END LOOP;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
END;
