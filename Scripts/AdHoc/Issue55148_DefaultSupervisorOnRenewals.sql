declare
  t_TiaUserId       number := 22165691;
  t_DefaultSupEPId  number
      := api.pkg_configquery.EndPointIdForName('j_ABC_RenewalApplication', 'DefaultSupervisor');
  t_RelId           number;
begin
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20000, 'Must be run as POSSEDBA');
  end if;
  api.pkg_logicaltransactionupdate.ResetTransaction;
      dbms_output.put_line('Jobs Updated ');
  for i in (
    select r.ObjectId, r.DefaultSupervisor, r.DefaultSupervisorObjectId, ar.RelationshipId
      from query.j_abc_renewalapplication r
      join api.relationships ar
          on ar.FromObjectId = r.ObjectId
         and ar.EndPointId = t_DefaultSupEPId
         and ar.ToObjectId = 22165146
     where trunc(r.CreatedDate) = to_date('05/15/2020', 'mm/dd/yyyy')
    ) loop
    api.pkg_relationshipupdate.Remove(i.relationshipid);
    t_RelId := api.pkg_relationshipupdate.New(t_DefaultSupEPId, i.objectid, t_TiaUserId);
    dbms_output.put_line(i.objectid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;
/