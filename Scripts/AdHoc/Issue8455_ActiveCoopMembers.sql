declare
  t_EndPointId number := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'CoOpLicense');
  t_RelId      number;
  t_RelCount   number := 0;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.ObjectId permitobjectid, t.coopmemberobjectid, cl.RelationshipId, cl.CoOpMemberEffectiveDate
              from possedba.PermitActiveCoopMemberFix t
              join query.o_abc_permit p on p.PermitNumber = t.permitnumber
              join query.r_abc_permithistorcooplicenses cl on cl.PermitObjectId = p.ObjectId and cl.CoOpMemberObjectId = t.coopmemberobjectid
             where api.pkg_columnquery.value(cl.CoOpMemberObjectId, 'State') = 'Active') loop
    --If the current rel doesn't exist, create it
    select count(1)
      into t_RelCount
      from query.r_ABC_PermitCoOpLicenses r
     where r.PermitObjectId = i.permitobjectid
       and r.CoOpMemberObjectId = i.coopmemberobjectid;
    if t_RelCount = 0 then
      t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, i.permitobjectid, i.coopmemberobjectid);
      api.pkg_columnupdate.SetValue(t_RelId, 'CoOpMemberEffectiveDate', i.CoOpMemberEffectiveDate);
    end if;
    --Remove historic rel
    api.pkg_relationshipupdate.Remove(i.relationshipid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;