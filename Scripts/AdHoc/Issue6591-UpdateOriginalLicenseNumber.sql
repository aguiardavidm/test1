-- Created on 01/24/2017 by MICHEL.SCHEFFERS 
-- Issue 6591 - Permits not being transferred to the new version of a license
-- Set "Original License Number" on Permits from the License on the original Permit
-- Run Time: 4 minutes
-- *** NOTE: May need to add "Return null" in the beginning of abc.pkg_ABC_Permit.RaisePermitError to avoid errors. ***
declare 
  -- Local variables here
  c         integer := 0;
  f         integer := 0;
  t_Objects api.udt_ObjectList;
  t_JobList api.pkg_Definition.udt_IdList;

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(NULL);
  -- Find the Permit that has a License Number and an application job
  for p in (
            select p.objectid, p.PermitNumber, p.PermitTypeObjectId, p.LicenseNumber
            from   query.o_abc_permit p
            join   query.j_abc_permitapplication pa on pa.PermitObjectId = p.objectid
            where  p.ObjectDefTypeId = 1
            and    p.PermitNumber is not null
            and    pa.StatusName  in ('APP', 'NOTIF')     
           ) loop
     if p.licensenumber is not null then
        f := f + 1;
        t_Objects := api.udt_ObjectList();
        extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Permit');
        extension.pkg_cxproceduralsearch.SearchByIndex('PermitNumber',p.permitnumber,p.permitnumber,false);
        extension.pkg_cxproceduralsearch.PerformSearch(t_Objects, 'and');
        for l in (
                  Select *
                  from   table(cast(t_Objects as api.udt_objectlist))
                 ) loop
     -- Find all versions of the Permit and set "Original License Number"
           if api.pkg_columnquery.Value(l.objectid, 'PermitTypeObjectId') = p.PermitTypeObjectId and 
              api.pkg_columnquery.Value (l.objectid, 'OriginalLicenseNumber') is null            then
              api.pkg_columnupdate.SetValue (l.objectid, 'OriginalLicenseNumber', p.Licensenumber);
              dbms_output.put_line ('Application: Updating Permit ' || api.pkg_columnquery.Value(l.objectid, 'PermitNumber') || '(' || l.objectid || ') to License Number ' || p.licenseNumber);
              c := c + 1;
           end if;
        end loop;
     end if;
  end loop;

  dbms_output.put_line ('Application: Updating Permits found   ' || f);
  dbms_output.put_line ('Application: Updating Permits updated ' || c);
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

  -- Find the non-converted Permit that has a License Number but no application job
  c := 0;
  f := 0;
  for pr in (
             select p.ObjectId, p.PermitNumber, p.PermitTypeObjectId, p.LicenseNumber
             from   query.o_abc_permit p
             where  p.PermitNumber  is not null
             and    p.LicenseNumber is not null
             and    p.ObjectId in (
                                   select objectid
                                   from   query.o_abc_permit
                                   minus
                                   select objectid
                                   from   dataconv.o_abc_permit
                                  )
            ) loop
     select y.JobId
     bulk   collect into t_JobList
     from   (select r.PermitApplicationObjectId JobId, api.pkg_columnquery.NumericValue(x.ObjectId,'PermitTypeObjectId') PermitTypeObjectId
             from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', pr.Permitnumber)) x
             join query.r_ABC_PermitAppPermit r on r.PermitObjectId = x.objectid) y
     where  y.PermitTypeObjectId = pr.Permittypeobjectid;
   
     if t_JobList.count() = 0 then
        f := f + 1;
        t_Objects := api.udt_ObjectList();
        extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Permit');
        extension.pkg_cxproceduralsearch.SearchByIndex('PermitNumber',pr.permitnumber,pr.permitnumber,false);
        extension.pkg_cxproceduralsearch.PerformSearch(t_Objects, 'and');
        for l in (
                  Select *
                  from   table(cast(t_Objects as api.udt_objectlist))
                 ) loop
     -- Find all versions of the Permit and set "Original License Number"
           if api.pkg_columnquery.Value(l.objectid, 'PermitTypeObjectId') = pr.PermitTypeObjectId and 
              api.pkg_columnquery.Value (l.objectid, 'OriginalLicenseNumber') is null             then
              api.pkg_columnupdate.SetValue (l.objectid, 'OriginalLicenseNumber', pr.Licensenumber);
              dbms_output.put_line ('Non-Application: Updating Permit ' || api.pkg_columnquery.Value(l.objectid, 'PermitNumber') || '(' || l.objectid || ') to License Number ' || pr.licenseNumber);
              c := c + 1;
           end if;
        end loop;
     end if;
  end loop;

  dbms_output.put_line ('Non-Application: Updating Permits found   ' || f);
  dbms_output.put_line ('Non-Application: Updating Permits updated ' || c);
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;
