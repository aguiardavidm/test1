-- Created on 01/12/2016 by MICHEL.SCHEFFERS 
-- Create new relationship Permit - License for CoOp. A permit will be associated with all non-pending Licenses with the same License Number.
declare 
  -- Local variables here
  h                      integer := 0;
  d                      integer := 0;
  c                      integer := 0;
  r                      integer := 0;
  t_RelCount             number;
  t_RelId                number;
  t_PrevLicNum           varchar2(100) := ' ';
  t_LicenseObjectId      api.pkg_Definition.udt_idlist;
  t_PermitHistLicRelDef  number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitHistorCoOpLicenses');
  t_PermitHistLicenseEP  number := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'HistoricalCoOpLicense');
  t_HistLicensePermitEP  number := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'HistoricalCoOpLicense_Permit');  
  t_PermitCurrLicRelDef  number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitCoOpLicenses');
  t_PermitCurrLicenseEP  number := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'CoOpLicense');
  t_CurrLicensePermitEP  number := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'CoOpLicense_Permit');  
  
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  for p in (select pcl.RelationshipId, pcl.PermitObjectId, pcl.CoOpMemberObjectId, pcl.CoOpMemberEffectiveDate, api.pkg_columnquery.Value(pcl.CoOpMemberObjectId,'LicenseNumber') LicenseNumber
              from query.r_ABC_PermitCoOpLicenses pcl
             order by api.pkg_columnquery.Value(pcl.CoOpMemberObjectId,'LicenseNumber')
           ) loop
        r := r + 1;
--    Current License        
        if t_PrevLicNum != p.licensenumber then
           select lic.objectid
             bulk collect into t_LicenseObjectId
             from query.o_abc_license lic
            where lic.LicenseNumber = p.licensenumber
              and lic.State != 'Pending';
           t_PrevLicNum := p.licensenumber;
        end if;
        
        for x in 1..t_LicenseObjectId.count loop
--    Found License is Active, follow the next steps:
--      create a record in PermitCoOp relationship for the found License
--      create a record in HistoricalPermitCoOp relationship for the current license
--      remove the PermitCoOp relationship for the current license
          if api.pkg_columnquery.Value (t_LicenseObjectId (x),'State') = 'Active' then
             select count (*)
               into t_RelCount
               from query.r_ABC_PermitCoOpLicenses pgl
              where pgl.PermitObjectId  = p.permitobjectid
                and pgl.CoOpMemberObjectId = t_LicenseObjectId (x);
--    If the new Permit - License Relationship does not already exist, create it
             if t_RelCount = 0 then
                dbms_output.put_line ('Creating Current License (' || t_LicenseObjectId (x) || ') ' || api.pkg_columnquery.Value(t_LicenseObjectId (x),'LicenseNumber') ||'(' || api.pkg_columnquery.Value(t_LicenseObjectId (x),'State') || ')' || ' - Permit ' || api.pkg_columnquery.Value(p.permitobjectid,'PermitNumber') || ' (' || api.pkg_columnquery.Value(p.permitobjectid,'State') || ') - ' || p.coopmembereffectivedate);
--    Create the Permit - License Rel
--    Get the next sequence number
                select possedata.ObjectId_s.nextval
                  into t_RelId 
                  from dual;

--    Insert into ObjModelPhys.Objects
                insert into objmodelphys.Objects (
                                                 LogicalTransactionId,
                                                 CreatedLogicalTransactionId,
                                                 ObjectId,
                                                 ObjectDefId,
                                                 ObjectDefTypeId,
                                                 ClassId,
                                                 InstanceId,
                                                 EffectiveStartDate,
                                                 EffectiveEndDate,
                                                 ConfigReadSecurityClassId,
                                                 ConfigReadSecurityInstanceId,
                                                 ObjectReadSecurityClassId,
                                                 ObjectReadSecurityInstanceId
                                                 ) values 
                                                 (
                                                 1, -- Hard coded logical Transaction to 1
                                                 1, -- Hard coded logical Transaction to 1
                                                 t_RelId,
                                                 t_PermitCurrLicRelDef,
                                                 4,
                                                 4,
                                                 t_PermitCurrLicRelDef,
                                                 null,
                                                 null,
                                                 4,
                                                 t_PermitCurrLicRelDef,
                                                 null,
                                                 null
                                                 );
--     Insert Into Rel.StoredRelationships    
                insert into rel.StoredRelationships (
                                                    RelationshipId,
                                                    EndPointId,
                                                    FromObjectId,
                                                    ToObjectId
                                                    ) values 
                                                    (
                                                    t_RelId,
                                                    t_PermitCurrLicenseEP,
                                                    p.permitobjectid,
                                                    t_LicenseObjectId (x) 
                                                    );

                insert into rel.StoredRelationships (
                                                    RelationshipId,
                                                    EndPointId,
                                                    FromObjectId,
                                                    ToObjectId
                                                    ) values 
                                                    (
                                                    t_RelId,
                                                    t_CurrLicensePermitEP,
                                                    t_LicenseObjectId (x),
                                                    p.permitobjectid
                                                    );
                api.pkg_columnupdate.SetValue(t_RelId,'CoOpMemberEffectiveDate',p.coopmembereffectivedate); 
                c := c + 1;
             end if;            
             select count (*)
               into t_RelCount
               from query.r_ABC_PermitHistorCoOpLicenses pgl
              where pgl.PermitObjectId  = p.permitobjectid
                and pgl.CoOpMemberObjectId = p.CoOpMemberObjectId;
--    If the Historical Permit - License Relationship does not already exist, create it
             if t_RelCount = 0 then
                dbms_output.put_line ('Creating Historic License (' || p.CoOpMemberObjectId || ') ' || p.LicenseNumber ||'(' || api.pkg_columnquery.Value(p.coopmemberobjectid,'State') || ')' || ' - Permit ' || api.pkg_columnquery.Value(p.permitobjectid,'PermitNumber') || ' (' || api.pkg_columnquery.Value(p.permitobjectid,'State') || ') - ' || p.coopmembereffectivedate);
--    Create the Historical Permit - License Rel
--    Get the next sequence number
                select possedata.ObjectId_s.nextval
                  into t_RelId 
                  from dual;

--    Insert into ObjModelPhys.Objects
                insert into objmodelphys.Objects (
                                                 LogicalTransactionId,
                                                 CreatedLogicalTransactionId,
                                                 ObjectId,
                                                 ObjectDefId,
                                                 ObjectDefTypeId,
                                                 ClassId,
                                                 InstanceId,
                                                 EffectiveStartDate,
                                                 EffectiveEndDate,
                                                 ConfigReadSecurityClassId,
                                                 ConfigReadSecurityInstanceId,
                                                 ObjectReadSecurityClassId,
                                                 ObjectReadSecurityInstanceId
                                                 ) values 
                                                 (
                                                 1, -- Hard coded logical Transaction to 1
                                                 1, -- Hard coded logical Transaction to 1
                                                 t_RelId,
                                                 t_PermitHistLicRelDef,
                                                 4,
                                                 4,
                                                 t_PermitHistLicRelDef,
                                                 null,
                                                 null,
                                                 4,
                                                 t_PermitHistLicRelDef,
                                                 null,
                                                 null
                                                 );
--     Insert Into Rel.StoredRelationships    
                insert into rel.StoredRelationships (
                                                    RelationshipId,
                                                    EndPointId,
                                                    FromObjectId,
                                                    ToObjectId
                                                    ) values 
                                                    (
                                                    t_RelId,
                                                    t_PermitHistLicenseEP,
                                                    p.permitobjectid,
                                                    p.CoOpMemberObjectId 
                                                    );

                insert into rel.StoredRelationships (
                                                    RelationshipId,
                                                    EndPointId,
                                                    FromObjectId,
                                                    ToObjectId
                                                    ) values 
                                                    (
                                                    t_RelId,
                                                    t_HistLicensePermitEP,
                                                    p.CoOpMemberObjectId,
                                                    p.permitobjectid
                                                    );
                api.pkg_columnupdate.SetValue(t_RelId,'CoOpMemberEffectiveDate',p.coopmembereffectivedate); 
                h := h + 1;
             end if;
             dbms_output.put_line ('Deleting Current non-Active License (' || p.CoOpMemberObjectId || ') ' || api.pkg_columnquery.Value(p.CoOpMemberObjectId,'LicenseNumber') ||'(' || api.pkg_columnquery.Value(p.CoOpMemberObjectId,'State') || ')' || ' - Permit ' || api.pkg_columnquery.Value(p.permitobjectid,'PermitNumber') || ' (' || api.pkg_columnquery.Value(p.permitobjectid,'State') || ') - ' || p.coopmembereffectivedate);
             delete from rel.StoredRelationships
              where RelationshipId = p.relationshipid;
             delete from objmodelphys.Objects
              where ObjectId = p.relationshipid;
             d := d + 1;            
          else
--    Found License is not Active, follow the next steps:
--      create a record in HistoricalPermitCoOp relationship for the found license
             select count (*)
               into t_RelCount
               from query.r_ABC_PermitHistorCoOpLicenses pgl
              where pgl.PermitObjectId  = p.permitobjectid
                and pgl.CoOpMemberObjectId = t_LicenseObjectId (x);
--    If the Historical Permit - License Relationship does not already exist, create it
             if t_RelCount = 0 then
                dbms_output.put_line ('Creating Historic License (' || t_LicenseObjectId (x) || ') ' || p.LicenseNumber ||'(' || api.pkg_columnquery.Value(t_LicenseObjectId (x),'State') || ')' || ' - Permit ' || api.pkg_columnquery.Value(p.permitobjectid,'PermitNumber') || ' (' || api.pkg_columnquery.Value(p.permitobjectid,'State') || ') - ' || p.coopmembereffectivedate);
--    Create the Permit - License Rel
--    Get the next sequence number
                select possedata.ObjectId_s.nextval
                  into t_RelId 
                  from dual;

--    Insert into ObjModelPhys.Objects
                insert into objmodelphys.Objects (
                                                 LogicalTransactionId,
                                                 CreatedLogicalTransactionId,
                                                 ObjectId,
                                                 ObjectDefId,
                                                 ObjectDefTypeId,
                                                 ClassId,
                                                 InstanceId,
                                                 EffectiveStartDate,
                                                 EffectiveEndDate,
                                                 ConfigReadSecurityClassId,
                                                 ConfigReadSecurityInstanceId,
                                                 ObjectReadSecurityClassId,
                                                 ObjectReadSecurityInstanceId
                                                 ) values 
                                                 (
                                                 1, -- Hard coded logical Transaction to 1
                                                 1, -- Hard coded logical Transaction to 1
                                                 t_RelId,
                                                 t_PermitHistLicRelDef,
                                                 4,
                                                 4,
                                                 t_PermitHistLicRelDef,
                                                 null,
                                                 null,
                                                 4,
                                                 t_PermitHistLicRelDef,
                                                 null,
                                                 null
                                                 );
--     Insert Into Rel.StoredRelationships    
                insert into rel.StoredRelationships (
                                                    RelationshipId,
                                                    EndPointId,
                                                    FromObjectId,
                                                    ToObjectId
                                                    ) values 
                                                    (
                                                    t_RelId,
                                                    t_PermitHistLicenseEP,
                                                    p.permitobjectid,
                                                    t_LicenseObjectId (x)
                                                    );

                insert into rel.StoredRelationships (
                                                    RelationshipId,
                                                    EndPointId,
                                                    FromObjectId,
                                                    ToObjectId
                                                    ) values 
                                                    (
                                                    t_RelId,
                                                    t_HistLicensePermitEP,
                                                    t_LicenseObjectId (x),
                                                    p.permitobjectid
                                                    );
                api.pkg_columnupdate.SetValue(t_RelId,'CoOpMemberEffectiveDate',p.coopmembereffectivedate); 
                h := h + 1;
             end if;
          end if;
        end loop;
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
--  commit; 

  dbms_output.put_line('PermitLicense Records found     : ' || r);
  dbms_output.put_line('Historical CoOp Records created : ' || h);
  dbms_output.put_line('Current CoOp Records created    : ' || c);
  dbms_output.put_line('Non-Active CoOp Records deleted : ' || d);
end;
