-- Created on 07/21/2017 by Dennis.OConnor 
-- Modify products state for products that have a Exp in the future, but are in a Historical State

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(NULL);

  for p in (SELECT  p.ObjectId
			FROM query.o_abc_product p
			WHERE p.ExpirationDate > sysdate 
			AND p.IsProductActive = 'N')
        loop
      	api.pkg_columnupdate.SetValue (p.objectid, 'State', 'Current');
      	api.pkg_columnupdate.SetValue(p.objectid,'DirectExpire','N');
      	dbms_output.put_line ('Updating State to Active for ' || api.pkg_columnquery.Value (p.objectid,'RegistrationNumber') || '-' || api.pkg_columnquery.Value (p.objectid,'Name'));
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;