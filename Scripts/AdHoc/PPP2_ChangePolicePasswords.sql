declare
  t_UserIdList api.pkg_definition.udt_IdList;

begin
  select u.userid
    bulk collect
    into t_UserIdList
    from query.u_users u
   where u.ispoliceuser = 'Y'
     and u.Active = 'Y';
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  for i in 1 .. t_UserIdList.count loop
    abc.pkg_abc_users.ChangePassword(t_UserIdList(i), null, 'Dog$1234');
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

end;
