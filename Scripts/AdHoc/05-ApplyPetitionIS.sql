begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.objectid
              from query.j_abc_petition p) loop
    abc.pkg_instancesecurity.ApplyInstanceSecurity(i.objectid, sysdate);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;