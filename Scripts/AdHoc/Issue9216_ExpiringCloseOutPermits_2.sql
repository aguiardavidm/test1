-- Created on 8/3/2016 by MICHEL.SCHEFFERS 
-- Issue 9216 - Expiring Close Out permits with expiration date before today
-- Run the cursor first to ensure all permits are expired. If not, then run this script.

declare 
  -- Local variables here
  i            integer := 0;
  t_JobId      integer;
  t_RelId      integer;
  t_EndpointId integer := api.pkg_configquery.EndPointIdForName('j_ABC_Expiration', 'Permit');
  t_JobtypeId  integer := api.pkg_ConfigQuery.ObjectDefIdForName('j_ABC_Expiration');
  t_PermitType integer;
begin
  -- Test statements here
  dbms_output.enable(null);
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  select pt.ObjectId
  into   t_PermitType
  from   query.o_abc_permittype pt
  where  pt.Code = 'CO';
  
  for pe in (select p.ObjectId, p.ExpirationDate
             from   query.o_abc_permit p
             where  trunc(p.ExpirationDate) < trunc(sysdate)
             and    p.PermitTypeObjectId = t_PermitType
             and    p.state = 'Active'
             order  by p.ObjectId             
            ) loop
     dbms_output.put_line ('Expiring permit ' || api.pkg_columnquery.Value(pe.objectid,'PermitNumber') || '; expiration ' || pe.expirationdate);
     i := i + 1;
     api.pkg_ColumnUpdate.SetValue(pe.objectid, 'State', 'Expired');
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  dbms_output.put_line ('Expired ' || i || ' permits');
end;
