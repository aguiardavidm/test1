-- Created on 8/9/2017 by MICHEL.SCHEFFERS 
-- Issue 32713 - Police Portal - Permit Type - Set Notify Police
-- Run time < 1 minute
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
  end if;
  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);
  
  -- Loop through Permit Types where Notify Municipality is set
  for p in (select pt.ObjectId, pt.Code, pt.Name
            from   query.o_abc_permittype pt
            where  pt.MunicipalityRequired = 'Y'
           ) loop
     dbms_output.put_line ('Updating ' || p.Code || '-' || p.Name);
     api.pkg_columnupdate.SetValue (p.objectid, 'NotifyPolice', 'Y');
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;