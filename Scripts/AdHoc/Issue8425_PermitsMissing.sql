--dup_FormattedPermitteeObjectId is not set on a number of permits.  This script sets it to the correct value and syncs the corral table
--Est run time < 10m
declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'dup_FormattedPermitteeObjectId');
  t_Count number := 0;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.objectid, p.PermitNumber, p.FormattedPermitteeObjectId, p.dup_FormattedPermitteeObjectId
              from query.o_abc_permit p
             where p.objectdeftypeid = 1) loop
    --If the dup value is not set
    if (i.formattedpermitteeobjectid is not null and i.dup_formattedpermitteeobjectid is null) then
      insert into possedata.objectcolumndata
      (
      logicaltransactionid,
      objectid,
      columndefid,
      attributevalue,
      numericsearchvalue
      )
      values
      (
      api.pkg_logicaltransactionupdate.CurrentTransaction,
      i.ObjectId,
      t_ColumnDefId,
      i.formattedpermitteeobjectid,
      i.formattedpermitteeobjectid
      );
    --if the non dup value is empty and the dup has a value
    elsif (i.formattedpermitteeobjectid is null and i.dup_formattedpermitteeobjectid is not null) then
      delete from possedata.objectcolumndata cd
       where cd.ObjectId = i.objectid
         and cd.ColumnDefId = t_ColumnDefId;
    --if both fields are populated but inconsistent
    elsif ((i.formattedpermitteeobjectid is not null and i.dup_formattedpermitteeobjectid is not null) and (i.formattedpermitteeobjectid != i.dup_formattedpermitteeobjectid)) then
      update possedata.objectcolumndata cd
         set cd.LogicalTransactionId = api.pkg_logicaltransactionupdate.CurrentTransaction,
             cd.AttributeValue = i.FormattedPermitteeObjectId,
             cd.NumericSearchValue = i.FormattedPermitteeObjectId
       where cd.ObjectId = i.objectid
         and cd.ColumnDefId = t_ColumnDefId;
    end if;
    t_Count := t_Count+1;
  end loop;
  dbms_output.put_line(t_Count);
  dbms_output.put_line('Expecting around 220000');
  api.pkg_corral.SynchronizeTable('Reports', 'ABC_PERMIT', true);
  api.pkg_logicaltransactionupdate.EndTransaction;
end;