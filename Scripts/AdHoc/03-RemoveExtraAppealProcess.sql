declare
  t_Id number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select z.ProcessId, z.CreatedBy, z.Outcome
              from api.processes z 
              join api.processtypes x on x.ProcessTypeId = z.ProcessTypeId
             where x.Name = 'p_ABC_EnterAppeal'
               and z.JobId in 
              (select a.JobId
                from query.j_abc_appeal  a
                join api.processes p on p.JobId = a.objectid
                join api.processtypes pt on pt.ProcessTypeId = p.ProcessTypeId
               where pt.Name = 'p_ABC_EnterAppeal'
                 and z.Outcome is null
               group by a.JobId having count(*) > 1)
               order by 1,2,3) loop
    api.pkg_processupdate.Remove(i.processid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;