/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 1 second
 * Purpose: Register Finance Notes, Legal Notes, Licensing Notes and Permitting
 * Notes access groups as external objects.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_AccessGroupIds                      udt_IdList;
  t_Count                               pls_integer := 0;
  t_LogicalTransactionId                udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  select ag.AccessGroupId
  bulk collect into t_AccessGroupIds
  from api.AccessGroups ag
  where ag.Description = 'Finance Notes'
  union all
  select ag.AccessGroupId
  from api.AccessGroups ag
  where ag.Description = 'Legal Notes'
  union all
  select ag.AccessGroupId
  from api.AccessGroups ag
  where ag.Description = 'Licensing Notes'
  union all
  select ag.AccessGroupId
  from api.AccessGroups ag
  where ag.Description = 'Permitting Notes';

  for i in 1..t_AccessGroupIds.count() loop
    api.pkg_ObjectUpdate.RegisterExternalObject('o_AccessGroup', t_AccessGroupIds(i));
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;