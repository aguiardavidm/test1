/*-----------------------------------------------------------------------------
 * Author: Michel Scheffers
 * Expected run time: < 5 min
 * Purpose: Search for cancelled jobs that have open processes.
 * If assigned, unassign the process. Then remove the process.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_SearchResults is api.pkg_Definition.udt_SearchResults;
  -- Declare Local variables here
  t_RowsProcessed                       pls_integer := 0;
  t_ScriptDescription                   varchar2(4000) :=
      '36842 - Cancelled jobs on to do list';
  t_Text                                varchar2(4000);
  t_Assigned                            varchar2(4000);

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  dbug('Starting script ' || t_ScriptDescription);
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  rollback;

  dbms_output.enable(null);

  for pr in (select j.JobId, p.ProcessId
             from api.jobs j
             join api.processes p
               on p.JobId = j.JobId
             where api.pkg_columnquery.Value(j.JobId, 'StatusName') = 'CANCEL'
               and api.pkg_columnquery.Value(p.ProcessId, 'Outcome') is null
             ) loop
    T_Text := (api.pkg_columnquery.Value(pr.JobId, 'ExternalFileNum')
        || '-' || api.pkg_columnquery.Value(pr.JobId, 'ObjectDefDescription')
        || '"' || trunc (api.pkg_columnquery.DateValue(pr.JobId, 'CreatedDate'))
        || '"' || api.pkg_columnquery.Value(pr.ProcessId, 'ObjectDefDescription'));
    t_Assigned := '"';
    for pa in (select p.UserId
               from api.processassignments p
               where p.ProcessId = pr.processid
               ) loop
      t_Assigned := t_Assigned || api.pkg_columnquery.Value(pa.UserId, 'FormattedName') || '"';
      api.pkg_processupdate.Unassign(pr.processid, pa.UserId);
    end loop;
    dbug(T_Text || T_Assigned);
    api.pkg_processupdate.Remove(pr.ProcessId);
    t_RowsProcessed := t_RowsProcessed + 1;
  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;

  dbug('Jobs Processed: ' || t_RowsProcessed);
  dbug('Completed script ' || t_ScriptDescription);

end;