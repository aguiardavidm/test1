-- Created on 02/12/2016 by MICHEL SCHEFFERS 
-- Issue7294: Distributor License not being updated for brands.
-- Create new relationships Product - License. When a distributor license is renewed/amended, the link to product is severed.
-- Update the correct link between the new license and existing product, and remove the link between the old license and the product.
declare 
  -- Local variables here
  a                             integer := 0; -- Approved Amendment records found
  al                            integer := 0; -- Active Licenses
  d                             integer := 0; -- ProductDistributor records deleted
  h                             integer := 0; -- ProductHistoricDistributor records created
  i                             integer := 0; -- ProductDistributor records inserted
  n                             integer := 0; -- Non Person-to-Person Amendments
  p                             integer := 0; -- Person-to-Person Amendments
  r                             integer := 0; -- Approved Renewal records found
  t_RelCount                    integer := 0;
  t_OldLicense                  api.pkg_Definition.udt_id;
  t_NewLicense                  api.pkg_Definition.udt_id;
  t_NewLicenseAmended           api.pkg_Definition.udt_id;
  t_ProductDistRelDef           api.pkg_Definition.udt_id := api.pkg_configquery.ObjectDefIdForName('r_ABCProduct_Distributor');
  t_ProductHistDistRelDef       api.pkg_Definition.udt_id := api.pkg_configquery.ObjectDefIdForName('r_ABCProduct_Distributor');
  t_ProductDistributorEPId      api.pkg_Definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'DistributorLic');
  t_ProductHistDistributorEPId  api.pkg_Definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'HistoricProducts');
  t_RelId                       api.pkg_Definition.udt_Id;
  t_Licenses                    api.pkg_Definition.udt_IdList;
  t_LicenseNumber               varchar2(50);
  t_ObjectIds                   api.pkg_definition.udt_IdList;

begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  select pd.LicenseId
    bulk collect into t_Licenses
    from query.r_abcproduct_distributor pd
   group by pd.licenseid
   order by pd.LicenseId;

  for l in 1..t_Licenses.Count loop
     if api.pkg_columnquery.Value(t_Licenses(l), 'State') = 'Active' then
        al := al + 1;
--        dbms_output.put_line('License ' || api.pkg_columnquery.Value(t_Licenses(l), 'LicenseNumber') || ' is Active');
        continue;
     end if;
     t_OldLicense := t_Licenses(l);

-- Finding any Approved Renewal job(s) for the old license
     select count (*)
       into t_RelCount
       from query.j_abc_renewalapplication ra
      where ra.LicenseToRenewObjectId = t_OldLicense
        and ra.StatusName             in ('APP', 'DIST');
     
     if t_RelCount > 0 then
        r := r + 1;
        dbms_output.put_line('Approved Renewal for license ' || api.pkg_columnquery.Value(t_OldLicense, 'LicenseNumber'));
        t_LicenseNumber := substr(api.pkg_columnquery.Value(t_OldLicense,'LicenseNumber'), 1, 11);
        t_NewLicense := 0;
        begin
           select x.ObjectId
             bulk collect into t_ObjectIds
             from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', t_LicenseNumber) as api.udt_ObjectList)) x;
           for i1 in 1 .. t_ObjectIds.count loop
               if api.pkg_columnquery.Value(t_ObjectIds (i1), 'State') = 'Active' then
                  t_NewLicense := t_ObjectIds (i1);
                  exit;
               end if;
           end loop;
        end;
        if t_NewLicense > 0 then
--    Active License found. Now see if there are any amendments that made this license active   
           begin
              select aa.ObjectId
                into t_NewLicenseAmended
                from query.j_abc_amendmentapplication aa
               where aa.StatusName in ('APP', 'DIST')
                 and aa.LicenseObjectId = t_NewLicense;
           exception
             when no_data_found then
                t_NewLicenseAmended := 0;
           end;
           if t_NewLicenseAmended = 0 then              
              dbms_output.put_line('Finding products for ' || api.pkg_columnquery.Value(t_OldLicense,'LicenseNumber') ||
                                   '. Creating for License ' || api.pkg_columnquery.Value(t_NewLicense,'LicenseNumber'));
              for lp in (select pd.RelationshipId, pd.ProductId, pd.DistributionStatus
                           from query.r_abcproduct_distributor pd
                          where pd.LicenseId = t_OldLicense
                        ) loop
                 select count (*)
                   into t_RelCount
                   from query.r_Abcproduct_Distributor pd1
                  where pd1.LicenseId = t_NewLicense
                    and pd1.ProductId = lp.productid;
--    If the new Product - License Relationship does not already exist, create it
                 if t_RelCount = 0 then
--    Create the Product - License Rel
--    Get the next sequence number
                    select possedata.ObjectId_s.nextval
                      into t_RelId 
                      from dual;
--    Insert into ObjModelPhys.Objects
                    insert into objmodelphys.Objects (
                                                      LogicalTransactionId,
                                                      CreatedLogicalTransactionId,
                                                      ObjectId,
                                                      ObjectDefId,
                                                      ObjectDefTypeId,
                                                      ClassId,
                                                      InstanceId,
                                                      EffectiveStartDate,
                                                      EffectiveEndDate,
                                                      ConfigReadSecurityClassId,
                                                      ConfigReadSecurityInstanceId,
                                                      ObjectReadSecurityClassId,
                                                      ObjectReadSecurityInstanceId
                                                     ) values 
                                                     (
                                                      1, -- Hard coded logical Transaction to 1
                                                      1, -- Hard coded logical Transaction to 1
                                                      t_RelId,
                                                      t_ProductDistRelDef,
                                                      4,
                                                      4,
                                                      t_ProductDistRelDef,
                                                      null,
                                                      null,
                                                      4,
                                                      t_ProductDistRelDef,
                                                      null,
                                                      null
                                                     );
--    Insert Into Rel.StoredRelationships    
                    insert into rel.StoredRelationships (
                                                         RelationshipId,
                                                         EndPointId,
                                                         FromObjectId,
                                                         ToObjectId
                                                        ) values 
                                                        (
                                                         t_RelId,
                                                         t_ProductDistributorEPId,
                                                         lp.productid,
                                                         t_NewLicense
                                                        );
                    i := i + 1;
                 end if;
--    Remove relationship records for old license                   
                 delete from rel.StoredRelationships
                  where RelationshipId = lp.relationshipid;
                 delete from objmodelphys.Objects
                  where ObjectId = lp.relationshipid;
                 d := d + 1;            
              end loop;
           else
-- Found an Approved Amendment job for the new license
              a := a + 1;
              dbms_output.put_line('Approved Amendment after Renewal for license ' || api.pkg_columnquery.Value(t_NewLicense, 'LicenseNumber'));
              if api.pkg_columnquery.Value (t_NewLicenseAmended, 'AmendmentType') in ('Person to Person Transfer - Retail Licenses', 'Person to Person Transfer - State Issued Licenses', 'Person to Person and Place to Place Transfer - Retail Licenses', 'Person to Person and Place to Place Transfer - State Issued Licenses') then
                 p := p + 1;
                 dbms_output.put_line ('Amendment Type Person to Person: ' || api.pkg_columnquery.Value (t_NewLicenseAmended, 'AmendmentType'));
                 dbms_output.put_line('Finding products for ' || api.pkg_columnquery.Value(t_OldLicense,'LicenseNumber') ||
                                      '. Creating for License ' || api.pkg_columnquery.Value(t_NewLicense,'LicenseNumber'));
                 for lp in (select pd.RelationshipId, pd.ProductId, pd.DistributionStatus
                              from query.r_abcproduct_distributor pd
                             where pd.LicenseId = t_OldLicense
                           ) loop
                    select count (*)
                      into t_RelCount
                      from query.r_Abc_Product_Historic_Dist pd1
                     where pd1.LicenseId = t_OldLicense
                       and pd1.ProductId = lp.productid;
--    If the new Product - License Relationship does not already exist, create it
                    if t_RelCount = 0 then
--    Create the Product - License Rel
--    Get the next sequence number
                       select possedata.ObjectId_s.nextval
                         into t_RelId 
                         from dual;
--    Insert into ObjModelPhys.Objects
                       insert into objmodelphys.Objects (
                                                         LogicalTransactionId,
                                                         CreatedLogicalTransactionId,
                                                         ObjectId,
                                                         ObjectDefId,
                                                         ObjectDefTypeId,
                                                         ClassId,
                                                         InstanceId,
                                                         EffectiveStartDate,
                                                         EffectiveEndDate,
                                                         ConfigReadSecurityClassId,
                                                         ConfigReadSecurityInstanceId,
                                                         ObjectReadSecurityClassId,
                                                         ObjectReadSecurityInstanceId
                                                        ) values 
                                                        (
                                                         1, -- Hard coded logical Transaction to 1
                                                         1, -- Hard coded logical Transaction to 1
                                                         t_RelId,
                                                         t_ProductHistDistRelDef,
                                                         4,
                                                         4,
                                                         t_ProductHistDistRelDef,
                                                         null,
                                                         null,
                                                         4,
                                                         t_ProductHistDistRelDef,
                                                         null,
                                                         null
                                                        );
--    Insert Into Rel.StoredRelationships    
                       insert into rel.StoredRelationships (
                                                            RelationshipId,
                                                            EndPointId,
                                                            FromObjectId,
                                                            ToObjectId
                                                           ) values 
                                                           (
                                                            t_RelId,
                                                            t_ProductHistDistributorEPId,
                                                            lp.productid,
                                                            t_OldLicense
                                                           );
                       i := i + 1;
                    end if;
--    Remove relationship records for old license                   
                    delete from rel.StoredRelationships
                     where RelationshipId = lp.relationshipid;
                    delete from objmodelphys.Objects
                     where ObjectId = lp.relationshipid;     
                    d := d + 1;            
                 end loop;
              else          
                 n := n + 1;
                 dbms_output.put_line ('Amendment Type not Person to Person: ' || api.pkg_columnquery.Value (t_NewLicenseAmended, 'AmendmentType'));
                 dbms_output.put_line('Finding products for ' || api.pkg_columnquery.Value(t_OldLicense,'LicenseNumber') ||
                                      '. Creating for License ' || api.pkg_columnquery.Value(t_NewLicense,'LicenseNumber'));
                 for lp in (select pd.RelationshipId, pd.ProductId, pd.DistributionStatus
                              from query.r_abcproduct_distributor pd
                              where pd.LicenseId = t_OldLicense
                           ) loop
                    select count (*)
                      into t_RelCount
                      from query.r_Abcproduct_Distributor pd1
                     where pd1.LicenseId = t_NewLicense
                       and pd1.ProductId = lp.productid;
--    If the new Product - License Relationship does not already exist, create it
                    if t_RelCount = 0 then
--    Create the Product - License Rel
--    Get the next sequence number
                       select possedata.ObjectId_s.nextval
                         into t_RelId 
                         from dual;
--    Insert into ObjModelPhys.Objects
                       insert into objmodelphys.Objects (
                                                         LogicalTransactionId,
                                                         CreatedLogicalTransactionId,
                                                         ObjectId,
                                                         ObjectDefId,
                                                         ObjectDefTypeId,
                                                         ClassId,
                                                         InstanceId,
                                                         EffectiveStartDate,
                                                         EffectiveEndDate,
                                                         ConfigReadSecurityClassId,
                                                         ConfigReadSecurityInstanceId,
                                                         ObjectReadSecurityClassId,
                                                         ObjectReadSecurityInstanceId
                                                        ) values 
                                                        (
                                                         1, -- Hard coded logical Transaction to 1
                                                         1, -- Hard coded logical Transaction to 1
                                                         t_RelId,
                                                         t_ProductDistRelDef,
                                                         4,
                                                         4,
                                                         t_ProductDistRelDef,
                                                         null,
                                                         null,
                                                         4,
                                                         t_ProductDistRelDef,
                                                         null,
                                                         null
                                                        );
--    Insert Into Rel.StoredRelationships    
                       insert into rel.StoredRelationships (
                                                            RelationshipId,
                                                            EndPointId,
                                                            FromObjectId,
                                                            ToObjectId
                                                           ) values 
                                                           (
                                                            t_RelId,
                                                            t_ProductDistributorEPId,
                                                            lp.productid,
                                                            t_NewLicense
                                                           );
                       i := i + 1;
                    end if;
--    Remove relationship records for old license                   
                    delete from rel.StoredRelationships
                     where RelationshipId = lp.relationshipid;
                    delete from objmodelphys.Objects
                     where ObjectId = lp.relationshipid;
                    d := d + 1;            
                 end loop;
              end if;                      
           end if;
        end if;
     end if;
-- Finding any Approved Amendment job(s) for the old license
     begin
        for j in (select aa.ObjectId, aa.AmendmentType, aa.LicenseObjectId
                    from query.j_abc_amendmentapplication aa
                   where aa.StatusName in ('APP', 'DIST')
                     and aa.LicenseToAmendObjectId = t_OldLicense)
        loop           
           a := a + 1;
           dbms_output.put_line('Approved Amendment for license ' || api.pkg_columnquery.Value(t_OldLicense, 'LicenseNumber'));
           if j.AmendmentType in ('Person to Person Transfer - Retail Licenses', 'Person to Person Transfer - State Issued Licenses', 'Person to Person and Place to Place Transfer - Retail Licenses', 'Person to Person and Place to Place Transfer - State Issued Licenses') then
              p := p + 1;
              dbms_output.put_line ('Amendment Type Person to Person: ' || j.AmendmentType);
              dbms_output.put_line('Finding products for ' || api.pkg_columnquery.Value(t_OldLicense,'LicenseNumber'));
              for lp in (select pd.RelationshipId, pd.ProductId, pd.DistributionStatus
                           from query.r_abcproduct_distributor pd
                          where pd.LicenseId = t_OldLicense
                        ) loop
                 select count (*)
                   into t_RelCount
                   from query.r_Abc_Product_Historic_Dist pd1
                  where pd1.LicenseId = t_OldLicense
                    and pd1.ProductId = lp.productid;
--    If the new Product - License Relationship does not already exist, create it
                 if t_RelCount = 0 then
--    Create the Product - License Rel
--    Get the next sequence number
                    select possedata.ObjectId_s.nextval
                      into t_RelId 
                      from dual;
--    Insert into ObjModelPhys.Objects
                    insert into objmodelphys.Objects (
                                                      LogicalTransactionId,
                                                      CreatedLogicalTransactionId,
                                                      ObjectId,
                                                      ObjectDefId,
                                                      ObjectDefTypeId,
                                                      ClassId,
                                                      InstanceId,
                                                      EffectiveStartDate,
                                                      EffectiveEndDate,
                                                      ConfigReadSecurityClassId,
                                                      ConfigReadSecurityInstanceId,
                                                      ObjectReadSecurityClassId,
                                                      ObjectReadSecurityInstanceId
                                                     ) values 
                                                     (
                                                      1, -- Hard coded logical Transaction to 1
                                                      1, -- Hard coded logical Transaction to 1
                                                      t_RelId,
                                                      t_ProductHistDistRelDef,
                                                      4,
                                                      4,
                                                      t_ProductHistDistRelDef,
                                                      null,
                                                      null,
                                                      4,
                                                      t_ProductHistDistRelDef,
                                                      null,
                                                      null
                                                     );
--    Insert Into Rel.StoredRelationships    
                    insert into rel.StoredRelationships (
                                                         RelationshipId,
                                                         EndPointId,
                                                         FromObjectId,
                                                         ToObjectId
                                                        ) values 
                                                        (
                                                         t_RelId,
                                                         t_ProductHistDistributorEPId,
                                                         lp.productid,
                                                         t_OldLicense
                                                        );
                    i := i + 1;
                 end if;
--    Remove relationship records for old license                   
                 delete from rel.StoredRelationships
                  where RelationshipId = lp.relationshipid;
                 delete from objmodelphys.Objects
                  where ObjectId = lp.relationshipid;     
                 d := d + 1;            
              end loop;
           else          
              n := n + 1;
              dbms_output.put_line ('Amendment Type not Person to Person: ' || j.AmendmentType);
              dbms_output.put_line('Finding products for ' || api.pkg_columnquery.Value(t_OldLicense,'LicenseNumber') ||
                                   '. Creating for License ' || api.pkg_columnquery.Value(j.LicenseObjectId,'LicenseNumber'));
              for lp in (select pd.RelationshipId, pd.ProductId, pd.DistributionStatus
                           from query.r_abcproduct_distributor pd
                          where pd.LicenseId = t_OldLicense
                        ) loop
                 select count (*)
                   into t_RelCount
                      from query.r_Abcproduct_Distributor pd1
                     where pd1.LicenseId = j.LicenseObjectId
                       and pd1.ProductId = lp.productid;
--    If the new Product - License Relationship does not already exist, create it
                 if t_RelCount = 0 then
--    Create the Product - License Rel
--    Get the next sequence number
                    select possedata.ObjectId_s.nextval
                      into t_RelId 
                      from dual;
--    Insert into ObjModelPhys.Objects
                    insert into objmodelphys.Objects (
                                                      LogicalTransactionId,
                                                      CreatedLogicalTransactionId,
                                                      ObjectId,
                                                      ObjectDefId,
                                                      ObjectDefTypeId,
                                                      ClassId,
                                                      InstanceId,
                                                      EffectiveStartDate,
                                                      EffectiveEndDate,
                                                      ConfigReadSecurityClassId,
                                                      ConfigReadSecurityInstanceId,
                                                      ObjectReadSecurityClassId,
                                                      ObjectReadSecurityInstanceId
                                                     ) values 
                                                     (
                                                      1, -- Hard coded logical Transaction to 1
                                                      1, -- Hard coded logical Transaction to 1
                                                      t_RelId,
                                                      t_ProductDistRelDef,
                                                      4,
                                                      4,
                                                      t_ProductDistRelDef,
                                                      null,
                                                      null,
                                                      4,
                                                      t_ProductDistRelDef,
                                                      null,
                                                      null
                                                     );

--    Insert Into Rel.StoredRelationships    
                    insert into rel.StoredRelationships (
                                                         RelationshipId,
                                                         EndPointId,
                                                         FromObjectId,
                                                         ToObjectId
                                                        ) values 
                                                        (
                                                         t_RelId,
                                                         t_ProductDistributorEPId,
                                                         lp.productid,
                                                         j.LicenseObjectId
                                                        );
                    i := i + 1;
                 end if;
--    Remove relationship records for old license                   
                 delete from rel.StoredRelationships
                  where RelationshipId = lp.relationshipid;
                 delete from objmodelphys.Objects
                  where ObjectId = lp.relationshipid;
                 d := d + 1;            
              end loop;
           end if;   
           t_OldLicense := j.LicenseObjectId;     
        end loop;
     end;
  end loop;
         
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

  dbms_output.put_line('Number of licenses found                         : ' || t_Licenses.Count);
  dbms_output.put_line('Number of Active licenses found                  : ' || al);
  dbms_output.put_line('Approved Renewal jobs found                      : ' || r);
  dbms_output.put_line('Approved Amendment jobs found                    : ' || a);
  dbms_output.put_line('                                Person-to-Person : ' || p);
  dbms_output.put_line('                            Non Person-to-Person : ' || n);
  dbms_output.put_line('New ProductHistoricalDistributor Records created : ' || h);
  dbms_output.put_line('New ProductDistributor Records created           : ' || i);
  dbms_output.put_line('Old ProductDistributor Records deleted           : ' || d);
end;
