-- Created on 1/12/2016 by MICHEL.SCHEFFERS 
declare 
  -- Local variables here
  cert integer := 0;
begin
  -- Test statements here
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all License/Permit certificates
  for i in (select ObjectId
              from query.d_possereportletter
            ) loop
    -- Apply Instance Security for each certificate
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.ObjectId, sysdate);
    cert := cert + 1;
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbms_output.put_line('Instance Security updated for ' || cert || ' License/Permit certificates');

end;