PL/SQL Developer Test script 3.0
64
-- Created on 7/29/2016 by MICHEL.SCHEFFERS
-- Issue 9152 - 12.18 / 12.39 Warranty:  Review Online Renewal
declare 
  -- Local variables here
  i                     number := 0;
  t_IsStateIssued       varchar2(01);
  t_LicenseTypeObjectId api.pkg_Definition.udt_id;
  t_MunicipalityId      api.pkg_Definition.udt_id;
  t_DefaultClerk        api.pkg_Definition.udt_id;
  t_ObjectIdList        api.Pkg_Definition.udt_IdList;  

begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(null);
  for r in (select rr.ObjectId, rr.JobId, rr.AssignedStaff, rr.CreatedDate
            from   query.p_abc_reviewonlinerenewal rr
            where  rr.Outcome is null
            and    rr.CreatedDate > to_date ('07/25/2016', 'mm/dd/yyyy')
            order  by rr.ObjectId
           )loop
    t_IsStateIssued       := api.pkg_columnquery.Value(r.Jobid,'IsStateIssued');
    t_LicenseTypeObjectId := api.pkg_columnquery.NumericValue(r.Jobid,'LicenseTypeObjectId');
    t_MunicipalityId      := api.pkg_columnquery.NumericValue(r.Jobid,'OfficeObjectId');
    i                     := i + 1;

    dbms_output.put_line ('Task number ' || r.Objectid || ' created ' || r.createddate);
    begin
    if t_IsStateIssued = 'Y' then
       dbms_output.put_line ('     State issued. License Type ' || api.pkg_columnquery.Value(t_LicenseTypeObjectId,'Name'));
       select r.DefaultClerkId
       into   t_DefaultClerk
       from   query.r_ABC_LicenseTypeDefaultClerk r
       where  r.LicenseTypeId = t_LicenseTypeObjectId;
    else
       dbms_output.put_line ('     Non State issued. Municipality ' || api.pkg_columnquery.Value(t_MunicipalityId,'Name'));
       select r.ClerkObjectId
       into   t_DefaultClerk
       from   query.r_ClerkMunicipality r
       where  r.MunicipalityObjectId = t_MunicipalityId;
    end if;
    exception
       when no_data_found then
          t_DefaultClerk := 0;
    end;
    
    dbms_output.put_line('     Assigned to ' || r.AssignedStaff || ' and unassigning.');
    for p in (select pa.UserId
              from   api.processassignments pa
              where  pa.ProcessId = r.objectid
             )loop 
       api.pkg_processupdate.Unassign (r.objectid, p.userid);
    end loop;
    if t_DefaultClerk > 0 then
       dbms_output.put_line('     Re-assigning to ' || api.pkg_columnquery.Value(t_DefaultClerk, 'OracleLogonId') || '.');
       api.pkg_processupdate.Assign(r.Objectid, t_DefaultClerk);
    end if;

  end loop;
  
  dbms_output.put_line ('Number of ReviewOnlineRenewal processes re-assigned: ' || i);    
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
0
0
