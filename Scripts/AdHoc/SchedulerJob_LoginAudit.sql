begin
  sys.dbms_scheduler.create_job(job_name            => 'POSSESYS.AUDITLOCK',
                                job_type            => 'PLSQL_BLOCK',
                                job_action          => 'abc.pkg_abc_users.CreateLockAuditLogs;commit;',
                                start_date          => to_date('14-08-2015 00:00:00', 'dd-mm-yyyy hh24:mi:ss'),
                                repeat_interval     => 'Freq=Daily;ByHour=3;ByMinute=30;BySecond=0',
                                end_date            => to_date(null),
                                job_class           => 'DBMS_JOB$',
                                enabled             => true,
                                auto_drop           => false,
                                comments            => 'Daily Audit Of Locked Accounts');
end;
/