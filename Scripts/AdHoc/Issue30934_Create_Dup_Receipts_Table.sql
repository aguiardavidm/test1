create table possedba.remove_dup_receipts
(
   Payment_Id       integer,
   Receipt_Id       integer,
   Deleted_Date     date
);

grant insert on possedba.remove_dup_receipts to abc;
