-- Created on 11/15/2017 by MICHEL.SCHEFFERS
-- Issue 34261 - Remove Legal Entities for cancelled Product Batch Renewal Notification jobs.
declare 
  -- Local variables here
  i integer;
  l integer := 0;
begin
  -- Test statements here
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  select prc.ObjectId
  into   i
  from   query.o_ABC_ProductRenewalCycle prc  
  where  prc.RenewalCycle = 2017;

  for le in (select rt.*, api.pkg_columnquery.Value(rt.notificationjobid, 'StatusName')
             from abc.registrantnotifications_t rt
             where api.pkg_columnquery.Value(rt.notificationjobid, 'StatusName') = 'CANCEL'
             and   rt.renewalcycleobjectid = i
            ) loop
     dbms_output.put_line ('Removing Legal Entity ' || le.registrantobjectid);
     delete from abc.registrantnotifications_t r
     where  r.notificationjobid  = le.notificationjobid
     and    r.registrantobjectid = le.registrantobjectid;
     l := l + 1;
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
  dbms_output.put_line (l || ' Legal Entities removed');
end;