/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time:
 * Purpose: To remove the open Send Denial Notification process, change the
 *  Job Status to Municipal Review and add MC/PC Review processes to Job in
 *  NJPROD.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_ChangeStatus                        number
      := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');
  t_JobId                               number;
  t_JobNumber                           varchar2(40) := '322419';
  t_LogicalTransactionId                number;
  t_ProcessId                           number;
  t_ProcessToRemoveId                   number;
  t_ProcessTypesToAdd                   udt_IdList;
  t_SendDenial                          number
      := api.pkg_ConfigQuery.ObjectDefIdForName('p_ABC_SendPermitDenialNotif');

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  -- Get ProcessTypeId for Process(es) to Add
  select pt.ProcessTypeId
  bulk collect into t_ProcessTypesToAdd
  from api.processtypes pt
  where pt.Name in ('p_ABC_PoliceReview');

  -- Get JobId and ProcessId of the Send Denial Notification process
  select
    x.ObjectId
  into
    t_JobId
  from
    table(api.pkg_SimpleSearch.CastableObjectsByIndex('j_ABC_PermitApplication',
        'ExternalFileNum', t_JobNumber)) x;

  t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ChangeStatus, null, null, null, null);
  api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ReasonForChange',
      'Job Completed with wrong outcome.');
  api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Municipal Review');

  for i in 1..t_ProcessTypesToAdd.count() loop
    t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ProcessTypesToAdd(i), null, null, null, null);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();

  commit;

end;
