-- Created on 6/6/2017 by Michel Scheffers
-- Send Email notifications to Registrants with Preferred Contact Method of Email or 
-- to Registered users associated to the LE who have products with Expiration of  12/31/2016
-- and are present in non-renewal jobs created on 05/06/2017

declare 
  -- Local variables here
  t_Registrants integer := 0;
  t_Emails      integer := 0;
  FromAddress   varchar2(100) := 'donotreply@lps.state.nj.us';
  Subject       varchar2(100) := 'Action Required: Brand Registration for the 2017 Term';
  MessageBody   varchar2(4000) := '
<font face="tahoma, arial, verdana, sans-serif" style="font-family: tahoma, arial, verdana, sans-serif; font-size: 12px;">
<span style="font-size: 12px;"><img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"></span></font><br><br><br>
<font face="tahoma" style="font-size: 12px;"><br><font>
<span style="font-size: 12px;">Dear Registrant,</span>
</font><br><br><font><span style="font-size: 12px;">
<br>Thank you for completing the brand registration renewal for the 2016 term (January 2016 - December 2016).  You may now proceed with completing your brand renewal for the  <span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold;">2017 term (January 2017 - December 2017).</span>  Please access the ABC Online system<font><span style="font-size: 12px;">'||'&'||'nbsp;<a href="https://abc.lps.nj.gov/ABCPublic/Login.aspx">Here</a>'||'&'||'nbsp;to renew </span>
</font> and select Product Registration Renewal to see the list of products registered in 2016 and not renewed for 2017.
If you sold any of these products during the 2017 term, you <span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">MUST</span> renew those products for the 2017 term and pay the appropriate fees.  Your 2017 renewal application will be processed within 1-3 business days. <br><br>
If you do not submit your renewal for these products and pay the appropriate fees prior to <span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold;">June 23, 2017</span>, the products will be retired and you will no longer be registered to sell them.  Selling these products in 2017 will be in violation of <span style="text-decoration: underline;">N.J.A.C.</span> 13:2-33.1.<br><br>
If you have any questions regarding any of the items to be renewed, please contact the ABC office at 609-984-2731.  <br><br>PLEASE CHECK TO MAKE SURE THAT YOU HAVE NOT ALREADY INITIATED NEW BRAND REGISTRATIONS FOR ANY OF THE PRODUCTS LISTED ON THE RENEWAL.
<br><br><br><span style="font-family: Helvetica,Arial,sans-serif;">We apologize for any inconvenience.</span>
<br><br>Regards,<br><br>NJ Division of Alcoholic Beverage Control</span></font><br><br><br><br>';
  MimeType      varchar2(100) := 'text/html';
  EmailResult   number;  
  
begin
  -- Test statements here
  for r in (select distinct (pr.RegistrantObjectId), pr.Registrant
             from  query.j_abc_prnonrenewal j
             join  query.r_abc_ProductToNonRenewalJob rp on rp.NonRenewalJobID = j.JobId
             join  query.o_abc_product pr on pr.ObjectId = rp.ProductObjectId
             where trunc (j.CreatedDate) = to_date('05/06/2017','mm/dd/yyyy')
             and   trunc (pr.ExpirationDate) = to_date('12/31/2016','mm/dd/yyyy')
             and   pr.State = 'Current'
             order by pr.Registrant
            ) loop
     dbms_output.put_line ('Registrant ' || r.registrant);
     t_Registrants := t_Registrants + 1;
     for i in (SELECT DISTINCT le.ContactEmailAddress
               FROM   query.o_abc_legalentity le
               WHERE  le.ObjectId = r.registrantobjectid
               AND    le.Active = 'Y'   
               AND    le.PreferredContactMethod = 'Email'
               UNION
               SELECT DISTINCT api.pkg_columnquery.Value(u.UserId, 'EmailAddress') ContactEmailAddress
               FROM   query.o_abc_legalentity le
               JOIN   query.r_abc_userlegalentity ule ON ule.LegalEntityObjectId = le.ObjectId
               JOIN   api.users u on u.UserId = ule.UserId
               WHERE  le.ObjectId = r.registrantobjectid
               AND    u.Active = 'Y'
               AND    le.PreferredContactMethod = 'Mail'
              ) loop
        dbms_output.put_line ('           Email ' || i.contactemailaddress);
        api.pkg_logicaltransactionupdate.ResetTransaction;                                         
        EmailResult := extension.pkg_sendmail.send(FromAddress,
                                                   i.ContactEmailAddress,
                                                   Subject,
                                                   MessageBody,
                                                   MimeType,
                                                   NULL,
                                                   NULL,
                                                   NULL,
                                                   MessageBody,
                                                   NULL);                                  
        api.pkg_logicaltransactionupdate.EndTransaction;
        t_Emails := t_Emails + 1;
     end loop;
  end loop;
  extension.pkg_SendMail.FinalizeEmail;
    
  dbms_output.put_line(t_Registrants || ' Registrants.');
  dbms_output.put_line(t_Emails || ' emails sent.');

end;