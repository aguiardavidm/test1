-- Created on 9/24/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
begin
  
  DBMS_OUTPUT.ENABLE(100000000);
  api.pkg_logicaltransactionupdate.ResetTransaction();
-- Updating Description and Gallonage  
      for c in (select  case 
            when p2.quantity = 30 then 'Up to 1,000 Gallons'    
            when p2.quantity = 200 then 'Up to 1,000 Gallons'
            when p2.quantity = 400 then 'Up to 1,000 Gallons'
            when p2.quantity = 1000 then 'Up to 1,000 Gallons'
            when p2.quantity = 1001 then '1,001 to 2,500 Gallons'
            when p2.quantity = 2200 then '1,001 to 2,500 Gallons'  
            when p2.quantity = 2500 then '1,001 to 2,500 Gallons'
            when p2.quantity = 3000 then '2,501 to 5,000 Gallons'
            when p2.quantity = 5000 then '2,501 to 5,000 Gallons' 
            when p2.quantity = 10000 then '5,001 to 10,000 Gallons' 
            when p2.quantity > 10000 then 'More than 10,000 Gallons'
              else to_char(p2.quantity)
                  end quantity, p2.product, p.objectid    
            from dataconv.o_abc_permit p
            join abc11p.permit p2 on to_char(p2.perm_num) = p.legacykey
           where p2.perm_type_cd = 'FP'
            and p.objectid in (23842675, 23777822, 23743298, 23768379/*NJPROD Values*/,
                                  15889405, 15825917, 15783766, 15811623/*NJ UAT Values*/)) loop
       begin
        dbms_output.put_line('Updating Description & Gallonage for Permit: ' || c.objectid);
        api.pkg_columnupdate.SetValue(c.objectid, 'FPProductDescription', c.product);
        api.pkg_columnupdate.SetValue(c.objectid, 'FPNumberOfGallons', c.quantity);
       exception when others then 
         dbms_output.put_line('Permit Issue ' || c.objectid|| ' ' || c.quantity|| ' '|| c.quantity);
       end;  
      end loop; 

-- Updating Description and Gallonage  
      for c in (select p.objectid    
            from query.o_abc_permit p
           where p.FPNumberOfGallons = '10,001 to 20,000 Gallons') loop
       begin
        dbms_output.put_line('Updating Gallonage for Permit: ' || c.objectid);
        api.pkg_columnupdate.SetValue(c.objectid, 'FPNumberOfGallons', 'More than 10,000 Gallons');
       exception when others then 
         dbms_output.put_line('Permit Issue ' || c.objectid);
       end;  
      end loop; 
    
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
