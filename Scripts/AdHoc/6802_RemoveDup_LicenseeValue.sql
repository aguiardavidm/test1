rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
/* WRITTEN BY: JOSHUA LENON
   PURPOSE:    Delete values from the dup_Licensee column on the license object
               so that it may be deleted by config
   DATE:       12/11/2015
   RUNTIME:    2-2.5 Seconds */
   
declare 
  -- Local variables here
  t_ColumnDefId    number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
     
    select max(a.ColumnDefId)
      into t_ColumnDefId
      from query.o_Abc_License l
      join api.columndefs a
        on a.ObjectDefId = l.ObjectDefId
     where a.name = 'dup_Licensee';
                         
     delete from possedata.objectcolumndata_t cd
      where cd.columndefid = t_ColumnDefId;
    
  api.pkg_logicaltransactionupdate.EndTransaction();

end;
/
