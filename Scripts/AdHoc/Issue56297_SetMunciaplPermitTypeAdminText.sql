-- Set admin text for all permit type and license type admin texts.
-- Expected run time up to 11 sec
declare
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  t_IdList udt_IdList;
  t_AdminText varchar2(4000);
begin
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.Pkg_Logicaltransactionupdate.ResetTransaction();

  -- Permitting
  select pt.objectid
  bulk collect into t_IdList
  from query.o_abc_permittype pt;

  --(Municipal) Permit Review Instructional Text
  select '<p style="color:red; font-size:large;  font-weight:bold">Municipal Response</p><br />' || s.MuniPermitRevInstructionalText
  into t_AdminText
  from query.o_systemsettings s;

  for i in 1..t_IdList.count loop
    api.pkg_ColumnUpdate.SetValue(t_IdList(i), 'PermitReviewInstructionalTextM', t_AdminText);
  end loop;

  --(Police) Permit Review Instructional Text
  select '<p style="color:red; font-size:large;  font-weight:bold">Police Response</p><br />' || s.MuniPermitRevInstructionalText
  into t_AdminText
  from query.o_systemsettings s;
  
  for i in 1..t_IdList.count loop
    api.pkg_ColumnUpdate.SetValue(t_IdList(i), 'PermitReviewInstructionalTextP', t_AdminText);
  end loop;

  --(Municipal) Endorsement Acknowledgement
  select s.MuniEndorsementAcknowledgement
  into t_AdminText
  from query.o_systemsettings s;

  for i in 1..t_IdList.count loop
    api.pkg_ColumnUpdate.SetValue(t_IdList(i), 'EndorsementAcknowledgementMuni', t_AdminText);
  end loop;

  --(Police) Endorsement Acknowledgement
  for i in 1..t_IdList.count loop
    api.pkg_ColumnUpdate.SetValue(t_IdList(i), 'EndorsementAcknowledgementPoli', t_AdminText);
  end loop;

  -- License Type
  select lt.objectid
  bulk collect into t_IdList
  from query.o_abc_licensetype lt;

  --(Municipal) License Resolution Instructional Text
  t_AdminText :=
'<p style="color:red; font-size:large;  font-weight:bold">Municipal Response</p><br />
The below areas are for the municipality to respond to the license application. After you review the application please provide the following responses:

If you wish to put Special Conditions, respond "Yes" to the Special Conditions questions.

A Municipal Resolution document is required. The following file formats are acceptable: Word, Excel, PDF.';

  for i in 1..t_IdList.count loop
    api.pkg_ColumnUpdate.SetValue(t_IdList(i), 'LicenseResolutionInstructionM', t_AdminText);
  end loop;

  --(Municipal) Resolution Acknowledgement
  t_AdminText :=
'Read the below certification and follow the prompts to respond to this application.  Use the �Submit Resolution to NJ ABC� button to submit the application. Use the "Awaiting ABC Special Ruling" when the application is waiting relief or specific ruling. Once Ruling is received, use the "Ruling Confirmed" to proceed to provide the resolution response to this application. <br/><br/>


<font face="Calibri" size="2" color="red">By checking the box below, </font > I hereby certify that I:<br/>
<br/>
1.	Have the authority to act on behalf of the municipality in this matter;<br/>
2.	Have reviewed the application submitted;<br/>
3.	Have considered any objections made to this application. <br/>
<br/><br/>
I further certify that the statements provided herein are accurate.  If any of the foregoing statements are willfully false, I am subject to punishment.';

  for i in 1..t_IdList.count loop
    api.pkg_ColumnUpdate.SetValue(t_IdList(i), 'ResolutionAcknowledgementMuni', t_AdminText);
  end loop;

  api.Pkg_Logicaltransactionupdate.EndTransaction();
  commit;
end;



