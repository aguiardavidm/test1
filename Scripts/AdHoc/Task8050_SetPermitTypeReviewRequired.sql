rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
declare 

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();

--Set ReviewRequired for all PermitTypes

  for p in (select pt.ObjectId, pt.Name, pt.ReviewRequired
              from query.o_Abc_Permittype pt) loop
          if p.reviewrequired = 'N' then
             dbms_output.put_line('Setting Review Required for ' || p.name);
             api.pkg_columnupdate.SetValue(p.objectid,'ReviewRequired','Y');
          end if;
      end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
/
