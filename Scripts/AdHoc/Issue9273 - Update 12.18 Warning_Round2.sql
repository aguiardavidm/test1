drop   table possedba.delete1218warning_Round2;
create table possedba.delete1218warning_Round2
(
   licenseid     number,
   licensenumber varchar2(40),
   oldwarning    varchar2(80),
   newwarning    varchar2(80)
);
insert into possedba.delete1218warning_Round2
select l.ObjectId, l.LicenseNumber, null, null
from   query.o_abc_license l
where  l.LicenseNumber in
(
'0205-33-007-009',
'0231-33-005-008',
'0266-33-002-007',
'0306-36-001-008',
'0312-33-001-006',
'0318-33-002-006',
'0319-33-007-006',
'0408-31-232-001',
'0408-33-167-008',
'0427-33-001-006',
'0427-33-013-008',
'0434-44-018-001',
'0605-33-001-003',
'0701-33-043-009',
'0705-32-024-005',
'0711-33-007-006',
'0714-31-934-002',
'0714-32-698-006',
'0714-33-068-008',
'0714-33-280-002',
'0714-33-409-006',
'0714-33-471-008',
'0714-33-569-008',
'0714-44-721-007',
'0714-44-734-006',
'0901-33-054-004',
'0906-33-155-004',
'0906-33-259-005',
'0906-33-340-011',
'0906-33-358-004',
'0906-44-301-006',
'0906-44-357-011',
'0909-33-032-007',
'0910-33-007-011',
'0910-33-063-005',
'0910-33-093-005',
'0910-33-100-010',
'0910-33-148-007',
'0912-33-025-011',
'0912-33-084-007',
'1219-33-056-009',
'1317-33-030-008',
'1350-33-012-006',
'1412-33-006-004',
'1514-33-024-006',
'1514-33-049-003',
'1604-33-019-008',
'1608-44-072-007',
'1608-44-119-003',
'1811-33-022-002',
'2004-33-158-006',
'2012-33-026-004',
'2107-33-005-003'
);

drop   table possedba.update1218warning_Round2;
create table possedba.update1218warning_Round2
(
   licenseid     number,
   licensenumber varchar2(40),
   oldwarning    varchar2(80),
   newwarning    varchar2(80)
);
insert into possedba.update1218warning_Round2
select l.ObjectId, l.LicenseNumber, null, null
from   query.o_abc_license l
where  l.LicenseNumber in
(
'0320-33-012-003',
'0320-33-013-001',
'0711-44-018-006',
'0714-33-626-007',
'0717-33-071-006',
'0904-44-052-008',
'0906-33-493-003',
'1009-32-003-013',
'1111-33-070-004',
'1216-33-016-003',
'1220-33-028-008',
'1220-44-001-007',
'1318-33-005-010',
'1511-33-008-001',
'1608-33-139-005',
'2004-33-082-004'
);
/
declare
   t_LicWarningId        api.pkg_definition.udt_Id;
   t_RelId               api.pkg_definition.udt_Id;
   t_LicWarningType      api.pkg_definition.udt_Id := api.pkg_simplesearch.ObjectByIndex('o_ABC_LicenseWarningType', 'LicenseWarningType', '12.18 Restriction');
   t_LicWarningTypeEP    api.pkg_definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_LicenseWarning', 'LicenseWarning');
   t_LicLicWarningEP     api.pkg_definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseWarning');
   t_LicenseWarningDef   api.pkg_definition.udt_Id := api.pkg_configquery.ObjectDefIdForName('o_ABC_LicenseWarning');
   t_Comment             varchar2(100)             := '12.18 RULING REQUIRED FOR 2016-2017 ON AUG 02, 2016';
   t_WarningUpdated      varchar2(01)              := 'N';
   t_BatchDate           date                      := to_date ('2016/08/02', 'yyyy/mm/dd');   

begin
   dbms_output.enable(null);
   api.pkg_logicaltransactionupdate.ResetTransaction;
   for l in (select *
             from   possedba.delete1218warning_Round2
            )
   loop            
      for l2 in (select wl.RelationshipId, wl.LicenseWarningId, lw.Comments
                 from   query.r_warninglicense wl
                 join   query.o_abc_licensewarning lw on lw.ObjectId = wl.LicenseWarningId
                 where  wl.LicenseId = l.licenseid
                 and    lw.Active1218WarningType = 'Y'
                )
      loop
         dbms_output.put_line('Deleting warning for license ' || l.licensenumber || ' from ' || l2.Comments);
         -- Delete old Warning and remove relationship to license
         api.pkg_relationshipupdate.Remove(l2.relationshipid);
         api.pkg_objectupdate.Remove(l2.licensewarningid);
         -- Log it
         update possedba.delete1218warning_Round2 dw
         set    dw.oldwarning = l2.Comments
         where  dw.licenseid = l.licenseid;
      end loop;
   end loop;
   api.pkg_logicaltransactionupdate.EndTransaction;
   commit;

   api.pkg_logicaltransactionupdate.ResetTransaction;
   for l in (select *
             from   possedba.update1218warning_Round2
            )
   loop    
      t_WarningUpdated := 'N';        
      for l2 in (select wl.RelationshipId, wl.LicenseWarningId, lw.Comments
                 from   query.r_warninglicense wl
                 join   query.o_abc_licensewarning lw on lw.ObjectId = wl.LicenseWarningId
                 where  wl.LicenseId = l.licenseid
                 and    lw.Active1218WarningType = 'Y'
                )
      loop
         t_WarningUpdated := 'Y';
         dbms_output.put_line('Updating warning for license ' || l.licensenumber || ' from ' || l2.Comments || ' to ' || t_Comment);
         api.pkg_columnupdate.SetValue(l2.LicenseWarningId, 'StartDate', trunc(t_BatchDate));
         api.pkg_columnupdate.SetValue(l2.LicenseWarningId, 'Comments', t_Comment);
         -- Log it
         update possedba.update1218warning_Round2 dw
         set    dw.oldwarning = l2.Comments,
                dw.newwarning = t_Comment
         where  dw.licenseid = l.licenseid;
      end loop;
      if t_WarningUpdated = 'N' then
         dbms_output.put_line('Updating warning for license ' || l.licensenumber || ' from .. to ' || t_Comment);
         -- Create new Warning and relate to license
         t_LicWarningId := api.pkg_objectupdate.New(t_LicenseWarningDef);
         t_RelId := api.pkg_relationshipupdate.New(t_LicWarningTypeEP, t_LicWarningId, t_LicWarningType);
         api.pkg_columnupdate.SetValue(t_LicWarningId, 'StartDate', trunc(t_BatchDate));
         api.pkg_columnupdate.SetValue(t_LicWarningId, 'Comments', t_Comment);
         t_RelId := api.pkg_relationshipupdate.New(t_LicLicWarningEP, l.licenseid, t_LicWarningId);
         -- Log it
         update possedba.update1218warning_Round2 dw
         set    dw.oldwarning = null,
                dw.newwarning = t_Comment
         where  dw.licenseid = l.licenseid;
      end if;
   end loop;
   api.pkg_logicaltransactionupdate.EndTransaction;
   commit;
end;   
