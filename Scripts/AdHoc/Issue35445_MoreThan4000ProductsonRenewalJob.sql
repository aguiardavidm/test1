-- Created on 1/8/2018 by Michel Scheffers
-- Issue 35445 - More than 4000 Products on Renewal job
-- Run time < 1 minute
declare 
  -- Local variables here
  t_IdList            api.pkg_Definition.udt_IdList;
  p                   integer := 0; -- Total relationships deleted
  p1                  integer := 0; -- Relationships for job deleted
  t_XrefGroup1        integer := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'XrefGroup1');
  t_XrefGroup2        integer := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'XrefGroup2');
  t_XrefGroup3        integer := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'XrefGroup3');
  t_XrefGroup4        integer := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'XrefGroup4');
  t_XrefGroup5        integer := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'XrefGroup5');
  t_XrefGroup6        integer := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'XrefGroup6');
  t_XrefGroup7        integer := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'XrefGroup7');
  t_XrefGroup8        integer := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'XrefGroup8');
begin
  -- Test statements here
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;  
  dbms_output.enable(null);

  select r.ProductRenewalJobId
  bulk   collect into t_IdList
  from   query.r_ABC_ProductRenewalXref r
  group  by r.ProductRenewalJobId
  having count (*) > 3999;
  
  for i in 1..t_IdList.count loop
    if api.pkg_columnquery.Value (t_IdList(i), 'StatusName') = 'NEW' then
      dbms_output.put_line('Processing job ' || t_IdList(i));
      p1 := 0;
      for pr1 in (select r1.RelationshipId
                 from   query.r_ABC_ProductRenewalXref r1
                 where  r1.ProductRenewalJobId = t_IdList(i)
                 and    not exists (select 1
                                     from   api.relationships r
                                     where  r.EndPointId = t_XrefGroup1
                                     and    r.FromObjectId = r1.ProductRenewalJobId
                                     and    r.ToObjectId = r1.ProductXrefObjectId
                                     union
                                     select 1
                                     from   api.relationships r
                                     where  r.EndPointId = t_XrefGroup2
                                     and    r.FromObjectId = r1.ProductRenewalJobId
                                     and    r.ToObjectId = r1.ProductXrefObjectId
                                     union
                                     select 1
                                     from   api.relationships r
                                     where  r.EndPointId = t_XrefGroup3
                                     and    r.FromObjectId = r1.ProductRenewalJobId
                                     and    r.ToObjectId = r1.ProductXrefObjectId
                                     union
                                     select 1
                                     from   api.relationships r
                                     where  r.EndPointId = t_XrefGroup4
                                     and    r.FromObjectId = r1.ProductRenewalJobId
                                     and    r.ToObjectId = r1.ProductXrefObjectId
                                     union
                                     select 1
                                     from   api.relationships r
                                     where  r.EndPointId = t_XrefGroup5
                                     and    r.FromObjectId = r1.ProductRenewalJobId
                                     and    r.ToObjectId = r1.ProductXrefObjectId
                                     union
                                     select 1
                                     from   api.relationships r
                                     where  r.EndPointId = t_XrefGroup6
                                     and    r.FromObjectId = r1.ProductRenewalJobId
                                     and    r.ToObjectId = r1.ProductXrefObjectId
                                     union
                                     select 1
                                     from   api.relationships r
                                     where  r.EndPointId = t_XrefGroup7
                                     and    r.FromObjectId = r1.ProductRenewalJobId
                                     and    r.ToObjectId = r1.ProductXrefObjectId
                                     union
                                     select 1
                                     from   api.relationships r
                                     where  r.EndPointId = t_XrefGroup8
                                     and    r.FromObjectId = r1.ProductRenewalJobId
                                     and    r.ToObjectId = r1.ProductXrefObjectId
                      )
                ) loop
         p1 := p1 + 1;            
         api.pkg_relationshipupdate.Remove(pr1.relationshipid);
      end loop;      
      dbms_output.put_line(p1 || ' Relationships for ' || t_IdList(i) || ' deleted.');
      p := p + p1;
    end if;
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbms_output.put_line('Total of ' || p || ' relationships deleted.');

end;
