begin
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20000, 'Script must be run as POSSEDBA');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  for i in (
    select ocd.AttributeValue, ocd.ColumnDefId, ocd.ObjectId
      from possedata.objectcolumndata ocd
     where ocd.ColumnDefId = api.pkg_ConfigQuery.ColumnDefIdForName('u_Users', 'ToDoListSettings')
      ) loop
    api.pkg_ColumnUpdate.RemoveValue(i.objectid, 'ToDoListSettings');
    dbms_output.put_line('Removed to do settings for userid: ' || i.objectid);
    end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
--  commit;
end;
/
