/*--Check to see if something needs to be updated
select * from scheduler.sysschedule s
where s.description like 'Check Public Access Groups'
and status = 1;
*/
update scheduler.sysschedule s
set status = 2
where s.description like 'Check Public Access Groups'
and status = 1;
commit;
/*--Test update worked
select * from scheduler.sysschedule s
where s.description like 'Check Public Access Groups'
and status = 2;
*/
