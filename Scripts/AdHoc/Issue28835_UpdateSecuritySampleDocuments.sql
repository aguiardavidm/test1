-- Created on 3/23/2017 by MICHEL.SCHEFFERS 
-- Issue 28835 Update security for sample documents to Read for ABC Public Access Group.
declare 
  -- Local variables here
  i integer := 0;
  t integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);
  select a.AccessGroupId
  into   t
  from   api.accessgroups a
  where  a.Description = 'ABC Public';
  for d in (select distinct(r.ToObjectId)
            from   api.relationships r
            where  r.EndPointId = api.pkg_configquery.EndPointIdForName ('o_ABC_DocumentType', 'SampleForm')
            and    r.ToObjectId in
            (
             select ed.ObjectId
             from   query.r_OLLicenseTypeDocumentType r
             join   query.o_abc_documenttype dt on dt.ObjectId = r.DocumentTypeObjectId
             join   query.d_electronicdocument ed on ed.ObjectId = dt.SampleFormDocumentId
             union
             select ed.ObjectId
             from   query.r_ABC_PermitTypeDocumentType r
             join   query.o_abc_documenttype dt on dt.ObjectId = r.DocumentTypeObjectId
             join   query.d_electronicdocument ed on ed.ObjectId = dt.SampleFormDocumentId
            )
           order by r.ToObjectId
           ) loop
     api.pkg_objectupdate.grantprivilege(d.toobjectid, t, 'Read');
     dbms_output.put_line(d.toobjectid || '-' || api.pkg_columnquery.Value(d.toobjectid, 'sampleformdocumenttype') || ' for ' || api.pkg_columnquery.Value(d.toobjectid, 'description'));
     i := i + 1;     
  end loop;
  dbms_output.put_line(i || ' Documents updated');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;                                     
end;
