  -- Running Document fix for Permit Number 21268.
  -- 08/05/2015
declare 
  t_RelationshipId      number (9);
  t_PermitObjectId      number (9) := 31681760; -- ObjectId for Permit 21268
  t_PermitTypeObjectId  number (9);
  t_ResetTAPOverride    varchar2(1) := 'N';
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  -- Find LicenseRequired for Permit Type on Permit
  select pt.objectId
    into t_PermitTypeObjectId
    from query.o_abc_permittype pt
    join query.o_abc_permit p on p.PermitTypeObjectId = pt.objectid
   where p.objectid = t_PermitObjectId;
  
  if api.pkg_columnquery.Value (t_PermitTypeObjectId, 'LicenseRequired') = 'Required' then
    api.pkg_columnupdate.SetValue (t_PermitTypeObjectId, 'LicenseRequired', 'ResetBackToRequired');
  end if;
  if api.pkg_columnquery.Value (t_PermitTypeObjectId, 'AllowTAPOverride') = 'Y' then
     api.pkg_columnupdate.SetValue (t_PermitTypeObjectId, 'AllowTAPOverride', 'N');
     t_ResetTAPOverride := 'Y';
  end if;
  
  -- Find correct Relationship Id for the Permit - Certificate
  select min (r.RelationshipId)
  into t_RelationshipId
  from api.relationships r
  join api.objects o on o.ObjectId = r.ToObjectId
  join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
 where r.FromObjectId = t_PermitObjectId
   and od.Name = 'd_PosseReportLetter';

  -- Use the found RelationshipId as parameter:   
  api.pkg_relationshipupdate.Remove(t_RelationshipId);
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
  
  -- Reset other variables
  api.pkg_logicaltransactionupdate.ResetTransaction();
  if api.pkg_columnquery.Value (t_PermitTypeObjectId, 'LicenseRequired') = 'ResetBackToRequired' then
     api.pkg_columnupdate.SetValue (t_PermitTypeObjectId, 'LicenseRequired', 'Required');
  end if;
  if t_ResetTAPOverride = 'Y' then
     api.pkg_columnupdate.SetValue (t_PermitTypeObjectId, 'AllowTAPOverride', 'Y');
  end if;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;
