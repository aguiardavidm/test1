-- Created on 8/3/2016 by MICHEL.SCHEFFERS 
-- Issue 9216 - Expiring converted Close Out permits

declare 
  -- Local variables here
  i            integer := 0;
  t_JobId      integer;
  t_RelId      integer;
  t_EndpointId integer := api.pkg_configquery.EndPointIdForName('j_ABC_Expiration', 'Permit');
  t_JobtypeId  integer := api.pkg_ConfigQuery.ObjectDefIdForName('j_ABC_Expiration');
  t_PermitType integer;
begin
  -- Test statements here
  dbms_output.enable(null);
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  select pt.ObjectId
  into   t_PermitType
  from   query.o_abc_permittype pt
  where  pt.Code = 'CO'; -- Close Outs
  
  for pe in (select ppt.PermitId ObjectId, api.pkg_columnquery.DateValue(ppt.PermitId,'ExpirationDate') ExpirationDate
             from   query.r_abc_permitpermittype ppt
             join   dataconv.o_abc_permit dp on dp.objectid = ppt.PermitId
             where  ppt.PermitTypeId = t_PermitType
             and    api.pkg_columnquery.Value(ppt.PermitId,'State') = 'Active'
             order  by ppt.PermitId
            ) loop
     dbms_output.put_line ('Expiring permit ' || api.pkg_columnquery.Value(pe.objectid,'PermitNumber') || '; expiration ' || pe.expirationdate);
     i := i + 1;
     t_JobId := api.pkg_jobupdate.New(t_JobtypeId, 'Expiring Transit Insignia Permits', null);
     t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_JobId, pe.objectid, sysdate);
     -- Set expiration date to 07/15/2016
     api.pkg_columnupdate.SetValue(pe.objectid, 'ExpirationDate', to_date('07/15/2016','mm/dd/yyyy'), sysdate);
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  dbms_output.put_line ('Expired ' || i || ' permits');
end;
