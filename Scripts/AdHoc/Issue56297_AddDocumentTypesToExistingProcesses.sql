/*-----------------------------------------------------------------------------
 * Expected run time: < 1 min
 * Purpose: Update System setting Text for State of Registry
 * README: There are three queries at the bottom, after you run they should not return anything
 * README2: You must run Issue56297_SetSpecialConditions... before you run this.
 *---------------------------------------------------------------------------*/
declare
  -- Local variables here
  idlist api.pkg_definition.udt_IdList;
  t_ProcId                            api.pkg_definition.udt_Id;
  t_PermitTypeId                      number;
  t_LicenseTypeId                     number;
  t_JobType                           varchar2(4000);
  t_RelId                             number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  --Police Review 
  for i in 
      (select objectid 
      from query.p_Abc_Policereview
      where outcome is null
      ) loop
    t_PermitTypeId := api.pkg_columnquery.NumericValue(i.objectid, 'PermitTypeId');
    t_JobType := api.pkg_columnquery.Value(i.objectid, 'JobName');
    for d in (select
                r.DocumentTypeObjectId,
                (case 
                   when t_JobType = 'j_ABC_NewApplication' and r.PoliceAppMandatory = 'Y' then 'Y'
                   when t_JobType = 'j_ABC_RenewalApplication' and r.PoliceRenewMandatory = 'Y' then 'Y'
                   else 'N' 
                end) Mandatory,
                r.SpecialConditionsMandatory
              from query.r_ABC_PermitTypeDocumentType r
              where r.PermitTypeObjectId = t_PermitTypeId
                and (r.PoliceApp = 'Y'
                or r.PoliceRenew = 'Y')
                and r.DocumentTypeObjectId not in
                    (select r1.DocumentTypeId
                     from query.r_ABC_PoliceReviewRespDocType r1
                     where r1.PoliceReviewId = i.ObjectId)
             ) loop
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('p_ABC_PoliceReview',
          'ResponseDocumentType'), i.objectid, d.documenttypeobjectid);
      api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      api.pkg_columnupdate.SetValue(t_RelId, 'SpecialConditionsMandatory', d.SpecialConditionsMandatory);
    end loop;
  end loop;

  -- Submit Resolution
  for i in 
      (select objectid 
      from query.p_abc_submitresolution
      where outcome is null
      ) loop
    t_LicenseTypeId := api.pkg_columnquery.NumericValue(i.objectid, 'LicenseTypeId');
    t_JobType := api.pkg_columnquery.Value(i.objectid, 'JobName');
    for d in (select
                r.DocumentTypeObjectId,
                (case 
                   when t_JobType = 'j_ABC_NewApplication' and r.MunicipalAppMandatory = 'Y' then 'Y'
                   when t_JobType = 'j_ABC_AmendmentApplication' and r.MunicipalAmendmentMandatory = 'Y' then 'Y'
                   when t_JobType = 'j_ABC_RenewalApplication' and r.MunicipalRenewMandatory = 'Y' then 'Y'
                   else 'N' 
                end) Mandatory
              from query.r_OLLicenseTypeDocumentType r
              where r.LicenseTypeObjectId = t_LicenseTypeId
                and (r.MunicipalApp = 'Y'
                or r.MunicipalAmendment = 'Y'
                or r.MunicipalRenew = 'Y')
                and r.DocumentTypeObjectId not in
                    (select r1.DocumentTypeId
                     from query.r_ABC_SubmitResRespDocType r1
                     where r1.SubmitResolutionId = i.objectid)
             ) loop
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('p_ABC_SubmitResolution',
          'ResponseDocumentType'), i.objectid, d.documenttypeobjectid);
      api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
    end loop;
  end loop;

  -- Municipal Review
  for i in 
      (select objectid 
      from query.p_Abc_Municipalityreview
      where outcome is null
      ) loop
    t_PermitTypeId := api.pkg_columnquery.NumericValue(i.objectid, 'PermitTypeId');
    t_JobType := api.pkg_columnquery.Value(i.objectid, 'JobName');
    for d in (select
                r.DocumentTypeObjectId,
                (case 
                   when t_JobType = 'j_ABC_PermitApplication' and r.MunicipalAppMandatory = 'Y' then 'Y'
                   when t_JobType = 'j_ABC_PermitRenewal' and r.MunicipalRenewMandatory = 'Y' then 'Y'
                   else 'N'
                end) Mandatory,
                r.SpecialConditionsMandatory
              from query.r_ABC_PermitTypeDocumentType r
              where r.PermitTypeObjectId = t_PermitTypeId
                and (r.MunicipalApp = 'Y'
                or r.MunicipalRenew = 'Y')
                and r.DocumentTypeObjectId not in
                    (select r1.DocumentTypeId
                     from query.r_ABC_MuniReviewRespDocType r1
                     where r1.MunicipalReviewId = i.objectid)
             ) loop
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('p_ABC_MunicipalityReview',
          'ResponseDocumentType'), i.objectid, d.documenttypeobjectid);
      api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      api.pkg_columnupdate.SetValue(t_RelId, 'SpecialConditionsMandatory', d.SpecialConditionsMandatory);
    end loop;
  end loop;
  
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;

/*
select objectid, createddate, jobid, p.LicenseType, externalfilenum, p.JobExternalFileNum, p.outcome
from query.p_abc_submitresolution p
where not exists (select 1 from query.r_ABC_SubmitResRespDocType r where r.SubmitResolutionId = p.objectid)
  and p.outcome is null; 

select objectid, createddate, jobid, externalfilenum, p.JobExternalFileNum, p.outcome
from query.p_Abc_Policereview p
where not exists (select 1 from query.r_ABC_PoliceReviewRespDocType r where r.PoliceReviewId = p.objectid)  
and p.outcome is null; 

select objectid, createddate, jobid, externalfilenum, p.JobExternalFileNum, p.outcome
from query.p_Abc_Municipalityreview p
where not exists (select 1 from query.r_ABC_MuniReviewRespDocType r where r.MunicipalReviewId = p.objectid)  
and p.outcome is null; 
*/
