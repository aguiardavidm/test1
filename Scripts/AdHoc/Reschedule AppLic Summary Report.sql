-- Created on 4/23/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  t_count integer := 0;
begin
  -- Test statements here
  
  for c in (select s.schedulekey
  from scheduler.sysschedule s
  join scheduler.sysschedulelog ssl on ssl.schedulekey = s.schedulekey
 where s.servererrorstatus = 2
   and substr(s.description,0 , 33)  = 'Create process of type 1632889 on'
   and ssl.errortext is not null
   and ssl.schedulelogkey = ( select max(ssl2.schedulelogkey) 
                              from scheduler.sysschedulelog ssl2
                            where ssl2.schedulekey = ssl.schedulekey)) loop
   
   
   update  scheduler.sysschedule s2
     set s2.status = 1
     where s2.schedulekey = c.schedulekey;
   
   t_count := t_count+1;
   
   end loop;
   
   --Rows Updated
   dbms_output.put_line('Rows Updated: ' || t_count);
  
end;