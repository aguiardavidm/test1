rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- Created on 7/7/2015 by YARS.TSAYTAK 
declare 
  -- Local variables here
  j integer := 0;
begin
  -- Test statements here

  for i in (select * from api.processtypes where name = 'p_ABC_SendConfirmationLetter') loop  

    api.pkg_objectupdate.RegisterExternalObject('o_ProcessType', i.processtypeid);
    j := j + 1;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  dbms_output.put_line(j);
  commit;
end;
/
