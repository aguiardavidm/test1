-- Created on 04/11/2019 by JAE.HOLDERBY
/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: Initialize CPL Types Objects, and create relationships with licneses
 * where they are needed
 *---------------------------------------------------------------------------*/
declare
  -- Local variables here
  t_BeerLicenseIdList     api.Pkg_Definition.udt_IdList;
  t_WineSpiritsIdList     api.Pkg_Definition.udt_IdList;
  t_BothLicenseIdList     api.Pkg_Definition.udt_IdList;
  t_CPLObjectDefId        api.Pkg_Definition.udt_Id := api.pkg_objectdefquery.IdForName('o_ABC_CPLSubmissionType');
  t_CPLLicenseEndpointId  api.Pkg_Definition.udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_CPLSubmissionType', 'License');
  t_BeerCPLId             api.Pkg_Definition.udt_Id;
  t_WineSpiritsId         api.Pkg_Definition.udt_Id;
  t_RIPId                 api.Pkg_Definition.udt_Id;
  t_ComboId               api.Pkg_Definition.udt_Id;
  t_RelId                 api.Pkg_Definition.udt_Id;
  t_Counter               integer := 0;

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  --Check for existing CPL Types.
  select max(ct.ObjectId)
  into t_BeerCPLId
  from query.o_abc_cplsubmissiontype ct
  where ct.Name = 'Beer';

  select max(ct.ObjectId)
  into t_WineSpiritsId
  from query.o_abc_cplsubmissiontype ct
  where ct.Name = 'Wine / Spirits';

  select max(ct.ObjectId)
  into t_RIPId
  from query.o_abc_cplsubmissiontype ct
  where ct.Name = 'RIP';

  select max(ct.ObjectId)
  into t_ComboId
  from query.o_abc_cplsubmissiontype ct
  where ct.Name = 'Combo';

  --Create CPL Types if null
  --Beer
  if t_BeerCPLId is null then
    t_BeerCPLId := api.pkg_objectupdate.New(t_CPLObjectDefId);
    api.pkg_columnupdate.SetValue(t_BeerCPLId, 'Name', 'Beer');
  end if;
  --Wine/Spirits
  if t_WineSpiritsId is null then
    t_WineSpiritsId := api.pkg_objectupdate.New(t_CPLObjectDefId);
    api.pkg_columnupdate.SetValue(t_WineSpiritsId, 'Name', 'Wine/Spirits');
  end if;
  --RIP
  if t_RIPId is null then
    t_RIPId := api.pkg_objectupdate.New(t_CPLObjectDefId);
    api.pkg_columnupdate.SetValue(t_RIPId, 'Name', 'RIP');
  end if;
  --Combo
  if t_ComboId is null then
    t_ComboId := api.pkg_objectupdate.New(t_CPLObjectDefId);
    api.pkg_columnupdate.SetValue(t_ComboId, 'Name', 'Combo');
  end if;

  --Get license ids where CPLSubmission is Beer, Wine/Spirits, or Both.
  select l.ObjectId
  bulk collect into t_BeerLicenseIdList
  from query.o_abc_license l
  where l.CPLSubmission = 'Beer'
  minus
  select l.ObjectId
  from query.o_abc_license l
    join api.relationships r
      on r.ToObjectId = l.ObjectId
    join query.o_abc_cplsubmissiontype ct
      on r.FromObjectId = ct.ObjectId
  where ct.name in ('Beer','Wine / Spirits');

  select l.ObjectId
  bulk collect into t_WineSpiritsIdList
  from query.o_abc_license l
  where l.CPLSubmission = 'Wine/Spirits'
  minus
  select l.ObjectId
  from query.o_abc_license l
    join api.relationships r
      on r.ToObjectId = l.ObjectId
    join query.o_abc_cplsubmissiontype ct
      on r.FromObjectId = ct.ObjectId
  where ct.name in ('Beer','Wine / Spirits');

  select l.ObjectId
  bulk collect into t_BothLicenseIdList
  from query.o_abc_license l
  where l.CPLSubmission = 'Both'
  minus
  select l.ObjectId
  from query.o_abc_license l
    join api.relationships r
      on r.ToObjectId = l.ObjectId
    join query.o_abc_cplsubmissiontype ct
      on r.FromObjectId = ct.ObjectId
  where ct.name in ('Beer','Wine / Spirits');

  --Relate Licenses to Beer
  for i in 1.. t_BeerLicenseIdList.count loop
    t_RelId := api.pkg_relationshipupdate.New(t_CPLLicenseEndpointId, t_BeerCPLId, t_BeerLicenseIdList(i));
    t_Counter := t_Counter +1;
    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;
  --Relate Licenses to Wine/Spirits
  for i in 1.. t_WineSpiritsIdList.count loop
    t_RelId := api.pkg_relationshipupdate.New(t_CPLLicenseEndpointId, t_WineSpiritsId, t_WineSpiritsIdList(i));
    t_Counter := t_Counter +1;
    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;
  --Relate Licenses to Both
  for i in 1.. t_BothLicenseIdList.count loop
    t_RelId := api.pkg_relationshipupdate.New(t_CPLLicenseEndpointId, t_BeerCPLId, t_BothLicenseIdList(i));
    t_RelId := api.pkg_relationshipupdate.New(t_CPLLicenseEndpointId, t_WineSpiritsId, t_BothLicenseIdList(i));
    t_Counter := t_Counter +1;
    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  dbms_output.put_line(t_Counter);
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
