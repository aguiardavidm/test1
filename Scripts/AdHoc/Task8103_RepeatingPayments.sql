begin 
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select e.PaymentStatus, e.ObjectId ,e.PaymentProviderPaymentResponse
              from query.j_ecom e
             where e.objectid in (30971910,
                                  30971826,
                                  31033235,
                                  31168659,
                                  31437799,
                                  31437863,
                                  31438049,
                                  31439787,
                                  31439978,
                                  33030059)) loop
    api.pkg_columnupdate.SetValue(i.objectid, 'PaymentStatus','PaymentCancelled');
    api.pkg_columnupdate.SetValue(i.objectId, 
                                  'PaymentProviderPaymentResponse', 
                                  i.paymentproviderpaymentresponse || '### Payment Cancelled manually, see Task#8103');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;