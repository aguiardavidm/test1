/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 1 sec
 * Purpose: This script is meant to:
 *  (1) Preserve the prior function of the Notify Municipality/Police check
 *  boxes on the Permit Type, meaning that if either 'Notify' check box was
 *  checked, the corresponding Municipal/Police Review Required check boxes
 *  will be checked.
 *  (2) Set the 'Standard' Municipal/Police Notification email subject, header
 *  photo URL, body header and body footer to ' '.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_dbug                                udt_IdList;
  t_LogicalTransactionId                udt_Id;
  t_PermitTypeIds                       udt_IdList;
  t_PermitTypeObjectDefId               udt_Id
      := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_PermitType');
  t_SystemSettingsObjectId              udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  dbug(chr(13) || chr(10) || '---BEFORE---');

  -- Update Permit Types
  select
    pt.ObjectId
  bulk collect into
    t_PermitTypeIds
  from query.o_ABC_PermitType pt
  where pt.MunicipalityRequired = 'Y'
     or pt.NotifyPolice = 'Y';
  dbug('There are ' || t_PermitTypeIds.count() || ' permit types with either' ||
      ' "MunicipalityRequired" OR "NotifyPolice" checked.');

  for i in 1..t_PermitTypeIds.count() loop
    -- MunicipalityRequired previously corresponded to the 'Notify Municipality' check box
    if api.pkg_ColumnQuery.Value(t_PermitTypeIds(i), 'MunicipalityRequired') = 'Y' then
      api.pkg_ColumnUpdate.SetValue(t_PermitTypeIds(i), 'NotifyMunicipality', 'Y');
    end if;
    if api.pkg_ColumnQuery.Value(t_PermitTypeIds(i), 'NotifyPolice') = 'Y' then
      if api.pkg_ColumnQuery.Value(t_PermitTypeIds(i), 'ReviewRequired') = 'N' then
        api.pkg_ColumnUpdate.SetValue(t_PermitTypeIds(i), 'ReviewRequired', 'Y');
      end if;
      api.pkg_ColumnUpdate.SetValue(t_PermitTypeIds(i), 'PoliceRequired', 'Y');
    end if;
  end loop;

  -- Update System Settings
  select s.ObjectId
  into t_SystemSettingsObjectId
  from query.o_SystemSettings s
  where s.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
      'SystemSettingsNumber', 1);

  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsObjectId, 'MuniStdEmailSubject', ' ');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsObjectId, 'MuniStdEmailHeaderPhotoURL', ' ');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsObjectId, 'MuniStdEmailBodyHeader', ' ');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsObjectId, 'MuniStdEmailBodyFooter', ' ');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsObjectId, 'PoliceStdEmailSubject', ' ');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsObjectId, 'PoliceStdEmailHeaderPhotoURL', ' ');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsObjectId, 'PoliceStdEmailBodyHeader', ' ');
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsObjectId, 'PoliceStdEmailBodyFooter', ' ');

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

  dbug(chr(13) || chr(10) || '---AFTER---');

  select o.ObjectId
  bulk collect into t_dbug
  from
    api.Objects o
    join api.LogicalTransactions lt
        on o.LogicalTransactionId = lt.LogicalTransactionId
  where lt.LogicalTransactionId = t_LogicalTransactionId
    and o.ObjectDefId = t_PermitTypeObjectDefId;

  dbug(t_dbug.count() || ' Permit Types were updated:');
  for i in 1..t_dbug.count() loop
    dbug(api.pkg_ColumnQuery.Value(t_dbug(i), 'Name'));
  end loop;

  commit;

end;
