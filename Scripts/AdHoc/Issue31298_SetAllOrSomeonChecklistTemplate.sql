  /*---------------------------------------------------------------------------
   * Issue 31298 - Amendment Testing Municipal Clerk View
   *   Pre-populate AllOrSomeAmendmentTypes and AllOrSomeJobTypes on 
   *   o_ABC_ChecklistTemplate with 'All'
   *   Run time < 1 minute
   * ------------------------------------------------------------------------*/
declare
  -- Local variables here
  t_Count                               integer := 0;
begin

  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
  end if;

  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);

  for a in (
      select ObjectId
      from query.o_ABC_ChecklistTemplate
      ) loop
    t_Count := t_Count + 1;
    api.pkg_columnupdate.SetValue(a.objectid, 'AllOrSomeAmendmentTypes', 'All');
    api.pkg_columnupdate.SetValue(a.objectid, 'AllOrSomeJobTypes', 'All');
  end loop;

  dbms_output.put_line (t_Count || ' Checklist Template records updated.');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
