-- Issue 36764-36866
-- Set initial email for non-renewal notification
-- 08/16/2018
declare 
  t_SSObjectId                          number (9);
begin

  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
  end if;

  api.pkg_logicaltransactionupdate.ResetTransaction();
  begin
    select ss.ObjectId
      into t_SSObjectId
      from query.o_systemsettings ss;
  exception
      when no_data_found then
        api.pkg_errors.RaiseError(-20000,'No System Settings record found!!');
  end;
  api.pkg_columnupdate.SetValue(t_SSObjectId,'EmailNotificationAddress','Njabcbrands@njoag.gov');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;
