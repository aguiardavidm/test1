
declare
  -- Local variables here
  i                    number := 0;
  t_EndpointId         integer := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                                                                        'Vehicle');
  t_CancelEndpointId   integer := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                                                                        'PermitCancellation');
  t_ObjectDefId        integer := api.pkg_configquery.ObjectDefIdForName('o_ABC_PermitCancellation');
  t_RenewEndpointId    integer := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                                                                        'PermitRenewal');
  t_Reason             varchar2(400);
  t_ObjectId           integer := 0;
  t_RelId              integer;
  t_LicenseObjectId    integer;
  t_Vehicles           integer := 0;
  t_RenewalCount       integer := 0;
  t_SecondaryRenCounty integer := 0;
  t_ConExpirationDate  date;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);
  -- pt loop
  for pt in (select objectid, t.Name
               from query.o_abc_permittype t
              where t.RequiresVehicles = 'Y') loop
    dbms_output.put_line('Checking Licenses for ' || pt.name || '(' ||
                         pt.objectid || ')');
    delete from possedba.issue9442_permitlicense;
    --p loop   
    for p in (select LicenseObjectId, b.objectid
                from query.o_abc_permit b
               where b.PermitTypeObjectId = pt.objectid
                 and b.LicenseObjectId is not null
                 and b.ObjectDefTypeId = 1
                 and b.RequiresVehicle = 'Y'
                 and b.PermitTypeObjectId = pt.objectid
                 and b.IsActive = 'Y'
               order by b.licenseobjectid) loop
      t_LicenseObjectId := 0;
      --GL loop     
      for gl in (select pgl.LicenseObjectId
                   from query.r_abc_permitgenerationlicense pgl
                  where pgl.PermitObjectId = p.objectid) loop
        if api.pkg_columnquery.Value(gl.LicenseObjectId, 'State') =
           'Active' then
          t_LicenseObjectId := gl.licenseobjectid;
          exit;
        end if;
        -- GL loop   
      end loop;
      -- No "Latest and Greatest" version found, get the current License
      if t_LicenseObjectId = 0 then
        begin
          select pl.LicenseObjectId
            into t_LicenseObjectId
            from query.r_permitlicense pl
           where pl.PermitObjectId = p.objectid;
        exception
          when no_data_found then
            t_LicenseObjectId := 0;
        end;
      end if;
      insert into possedba.issue9442_permitlicense
        (PermitId, PermitNumber, LicenseId, LicenseNumber)
      values
        (p.objectid,
         api.pkg_columnquery.Value(p.objectid, 'PermitNumber'),
         t_LicenseObjectId,
         api.pkg_columnquery.Value(t_LicenseObjectId, 'LicenseNumber'));
      --p loop          
    end loop;
  
    --License Step
    --p2 loop
    for p2 in (select *
                 from possedba.issue9442_permitlicense
                order by LicenseId, PermitId desc) loop
      select count(*)
        into t_RenewalCount
        from api.relationships r
        join api.jobs j
          on j.JobId = r.ToObjectId
       where r.FromObjectId = p2.permitid
         and r.EndPointId = t_RenewEndpointId
         and j.JobStatus in ('NEW', 'REVIEW');
      if t_RenewalCount = 0 then
        t_LicenseObjectId   := p2.licenseid;
        t_ConExpirationDate := api.pkg_columnquery.DateValue(p2.permitid,
                                                             'ExpirationDate');
        if p2.licenseid != i then
          i := p2.licenseid;
          dbms_output.put_line('   License ' ||
                               api.pkg_columnquery.Value(t_LicenseObjectId,
                                                         'LicenseNumber') || '(' ||
                               api.pkg_columnquery.Value(t_LicenseObjectId,
                                                         'State') || ')');
          dbms_output.put_line('      Permit ' ||
                               api.pkg_columnquery.Value(p2.permitid,
                                                         'PermitNumber') ||
                               ' for ' ||
                               api.pkg_columnquery.Value(p2.permitid,
                                                         'PermitteeName'));
          --p3 Loop
          for p3 in (select r.PermitObjectId,
                            api.pkg_columnquery.Value(r.PermitObjectId,
                                                      'PermitNumber') PermitNumber,
                            api.pkg_columnquery.Value(r.PermitObjectId,
                                                      'PermitteeName') PermitteeName,
                            api.pkg_columnquery.Value(r.PermitObjectId,
                                                      'PermitType') PermitType
                       from query.r_permitlicense r
                      where r.LicenseObjectId = t_LicenseObjectId
                        and api.pkg_columnquery.Value(r.PermitObjectId,
                                                      'PermitTypeObjectId') =
                            pt.objectid
                        and api.pkg_columnquery.Value(r.permitobjectid,
                                                      'RequiresVehicle') = 'Y'
                        and api.pkg_columnquery.Value(r.permitobjectid,
                                                      'IsActive') = 'Y'
                        and r.PermitObjectId != p2.permitid
                        and (select count(*)
                               from api.relationships r
                               join api.jobs j
                                 on j.jobid = r.toobjectid
                              where r.fromobjectid = r.PermitObjectId
                                and r.endpointid = t_RenewEndpointId
                                and j.JobStatus in ('NEW', 'REVIEW')) = 0
                        and api.pkg_columnquery.DateValue(r.PermitObjectId,
                                                          'ExpirationDate') =
                            t_ConExpirationDate) loop
            t_Vehicles := 0;
            --V Loop
            for v in (select *
                        from query.r_abc_permitvehicle pv
                       where pv.PermitObjectId = p3.permitobjectid) loop
              -- Copy vehicle(s) from 'old' Permit to latest Permit
              dbms_output.put_line('         Copy Vehicle ' ||
                                   v.vehicleobjectid || ' from Permit ' ||
                                   p3.PermitNumber || ' (' ||
                                   p3.permitteename || ') to Permit ' ||
                                   api.pkg_columnquery.Value(p2.permitid,
                                                             'PermitNumber'));
              t_Vehicles := t_Vehicles + 1;
              t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ABC_Vehicle'));
              t_RelId    := api.pkg_relationshipupdate.New(t_EndpointId,
                                                           p2.permitid,
                                                           t_ObjectId,
                                                           sysdate);
              api.pkg_columnupdate.setvalue(t_ObjectId,
                                            'InsigniaNumber',
                                            api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                      'InsigniaNumber'));
              api.pkg_columnupdate.setvalue(t_ObjectId,
                                            'PreviousInsigniaNumber',
                                            api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                      'PreviousInsigniaNumber'));
              api.pkg_columnupdate.setvalue(t_ObjectId,
                                            'MakeModelYear',
                                            api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                      'makemodelyear'));
              api.pkg_columnupdate.setvalue(t_ObjectId,
                                            'OwnedOrLeasedLimousine',
                                            api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                      'ownedorleasedlimousine'));
              api.pkg_columnupdate.setvalue(t_ObjectId,
                                            'VIN',
                                            api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                      'vin'));
              api.pkg_columnupdate.setvalue(t_ObjectId,
                                            'Stateregistration',
                                            api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                      'Stateregistration'));
              api.pkg_columnupdate.setvalue(t_ObjectId,
                                            'Stateofregistration',
                                            api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                      'Stateofregistration'));
              insert into possedba.issue9442_consolidatepermits
                (OldPermitId,
                 OldPermitNumber,
                 NewPermitId,
                 NewPermitNumber,
                 OldVehicleId,
                 VehicleId)
              values
                (p3.Permitobjectid,
                 p3.permitnumber,
                 p2.permitid,
                 api.pkg_columnquery.Value(p2.permitid, 'PermitNumber'),
                 v.vehicleobjectid,
                 t_ObjectId);
              -- V Loop                             
            end loop;
            -- Cancel 'old' Permit
            t_Reason := '         Consolidated with Permit ' ||
                        api.pkg_columnquery.Value(p2.permitid,
                                                  'PermitNumber');
            dbms_output.put_line(t_Reason || '. ' || t_Vehicles ||
                                 ' Vehicles copied');
            t_ObjectId := api.pkg_objectupdate.New(t_ObjectDefId, sysdate);
            api.pkg_columnupdate.SetValue(p3.PermitObjectId,
                                          'State',
                                          'Cancelled');
            api.pkg_columnupdate.SetValue(p3.PermitObjectId, 'Edit', 'N');
            api.pkg_columnupdate.SetValue(p3.PermitObjectId,
                                          'SystemCancelled',
                                          'Y');
            api.pkg_columnupdate.SetValue(t_ObjectId,
                                          'CancellationType',
                                          'Cancellation',
                                          sysdate);
            api.pkg_columnupdate.SetValue(t_ObjectId,
                                          'Reason',
                                          t_Reason,
                                          sysdate);
            t_RelId := api.pkg_relationshipupdate.New(t_CancelEndpointId,
                                                      p3.PermitObjectId,
                                                      t_ObjectId,
                                                      sysdate);
            --p3 loop              
          end loop;
        end if;
      
      end if; -- (If t_RenewalCount = 0)
    --p2 loop 
    end loop;
  
    -- TAP Step
    i              := 0;
    t_RenewalCount := 0;
    dbms_output.put_line('Checking TAPs for ' || pt.name);
    --p loop
    for p in (select b.TAPPermitObjectId
                from query.o_abc_permit b
               where b.PermitTypeObjectId = pt.objectid
                 and b.TAPPermitObjectId is not null
                 and b.ObjectDefTypeId = 1
               order by b.TAPPermitObjectId) loop
      if p.TAPPermitObjectId != i then
        i := p.TAPPermitObjectId;
        dbms_output.put_line('   TAP ' ||
                             api.pkg_columnquery.Value(p.TAPPermitObjectId,
                                                       'PermitNumber') || '(' ||
                             api.pkg_columnquery.Value(p.tappermitobjectid,
                                                       'State') || ')');
        --p1 loop
        for p1 in (select r.PermitObjectId
                     from query.r_abc_tappermit r
                    where r.TAPPermitObjectId = p.tappermitobjectid
                      and api.pkg_columnquery.Value(r.permitobjectid,
                                                    'RequiresVehicle') = 'Y'
                      and api.pkg_columnquery.Value(r.permitobjectid,
                                                    'IsActive') = 'Y'
                   
                    order by api.pkg_columnquery.Value(r.permitobjectid,
                                                       'PermitNumber') desc) loop
          dbms_output.put_line('      Permit ' ||
                               api.pkg_columnquery.Value(p1.permitobjectid,
                                                         'PermitNumber') ||
                               ' for ' ||
                               api.pkg_columnquery.Value(p1.permitobjectid,
                                                         'PermitteeName'));
          select count(*)
            into t_RenewalCount
            from api.relationships r
            join api.jobs j
              on j.JobId = r.ToObjectId
           where r.FromObjectId = p1.PermitObjectId
             and r.EndPointId = t_RenewEndpointId
             and j.JobStatus in ('NEW', 'REVIEW');
          if t_RenewalCount = 0 then
            t_ConExpirationDate := api.pkg_columnquery.datevalue(p1.permitobjectid,
                                                                 'ExpirationDate');
          
            --p3 loop
            for p3 in (select r.PermitObjectId,
                              api.pkg_columnquery.Value(r.PermitObjectId,
                                                        'PermitNumber') PermitNumber,
                              api.pkg_columnquery.Value(r.PermitObjectId,
                                                        'PermitteeName') PermitteeName,
                              api.pkg_columnquery.Value(r.PermitObjectId,
                                                        'PermitType') PermitType
                         from query.r_abc_tappermit r
                        where r.TAPPermitObjectId = p.tappermitobjectid
                          and api.pkg_columnquery.Value(r.PermitObjectId,
                                                        'PermitTypeObjectId') =
                              pt.objectid
                          and api.pkg_columnquery.Value(r.permitobjectid,
                                                        'RequiresVehicle') = 'Y'
                          and api.pkg_columnquery.Value(r.permitobjectid,
                                                        'IsActive') = 'Y'
                          and api.pkg_columnquery.DateValue(r.PermitObjectId,
                                                            'ExpirationDate') =
                              t_ConExpirationDate
                          and (select count(*)
                                 from api.relationships r
                                 join api.jobs j
                                   on j.jobid = r.toobjectid
                                where r.fromobjectid = r.PermitObjectId
                                  and r.endpointid = t_RenewEndpointId
                                  and j.JobStatus in ('NEW', 'REVIEW')) = 0
                          and r.PermitObjectId != p1.permitobjectid) loop
              t_Vehicles := 0;
              --V loop
              for v in (select *
                          from query.r_abc_permitvehicle pv
                         where pv.PermitObjectId = p3.permitobjectid) loop
                -- Copy vehicle(s) from 'old' Permit to latest Permit
                dbms_output.put_line('         Copy Vehicle ' ||
                                     v.vehicleobjectid || ' from Permit ' ||
                                     p3.PermitNumber || ' (' ||
                                     p3.permitteename || ') to Permit ' ||
                                     api.pkg_columnquery.Value(p1.permitobjectid,
                                                               'PermitNumber'));
                t_Vehicles := t_Vehicles + 1;
                t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ABC_Vehicle'));
                t_RelId    := api.pkg_relationshipupdate.New(t_EndpointId,
                                                             p1.permitobjectid,
                                                             t_ObjectId,
                                                             sysdate);
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'InsigniaNumber',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'InsigniaNumber'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'PreviousInsigniaNumber',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'PreviousInsigniaNumber'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'MakeModelYear',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'makemodelyear'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'OwnedOrLeasedLimousine',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'ownedorleasedlimousine'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'VIN',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'vin'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'Stateregistration',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'Stateregistration'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'Stateofregistration',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'Stateofregistration'));
                insert into possedba.issue9442_consolidatepermits
                  (OldPermitId,
                   OldPermitNumber,
                   NewPermitId,
                   NewPermitNumber,
                   OldVehicleId,
                   VehicleId)
                values
                  (p3.Permitobjectid,
                   p3.permitnumber,
                   p1.permitobjectid,
                   api.pkg_columnquery.Value(p1.permitobjectid,
                                             'PermitNumber'),
                   v.vehicleobjectid,
                   t_ObjectId);
                --v loop
              end loop;
              -- Cancel 'old' Permit
              t_Reason := '         Consolidated with Permit ' ||
                          api.pkg_columnquery.Value(p1.permitobjectid,
                                                    'PermitNumber');
              dbms_output.put_line(t_Reason || '. ' || t_Vehicles ||
                                   ' Vehicles copied');
              t_ObjectId := api.pkg_objectupdate.New(t_ObjectDefId, sysdate);
              api.pkg_columnupdate.SetValue(p3.PermitObjectId,
                                            'State',
                                            'Cancelled');
              api.pkg_columnupdate.SetValue(p3.PermitObjectId, 'Edit', 'N');
              api.pkg_columnupdate.SetValue(p3.PermitObjectId,
                                            'SystemCancelled',
                                            'Y');
              api.pkg_columnupdate.SetValue(t_ObjectId,
                                            'CancellationType',
                                            'Cancellation',
                                            sysdate);
              api.pkg_columnupdate.SetValue(t_ObjectId,
                                            'Reason',
                                            t_Reason,
                                            sysdate);
              t_RelId := api.pkg_relationshipupdate.New(t_CancelEndpointId,
                                                        p3.Permitobjectid,
                                                        t_ObjectId,
                                                        sysdate);
              --p3 loop      
            end loop;
          end if;
          exit;
          --p1 loop
        end loop;
      end if;
      --p loop
    end loop;
  
    -- Associated Permit Step
    i              := 0;
    t_RenewalCount := 0;
    dbms_output.put_line('Checking Associated Permits for ' || pt.name);
    --p loop
    for p in (select b.AssociatedPermitObjectId
                from query.o_abc_permit b
               where b.PermitTypeObjectId = pt.objectid
                 and b.AssociatedPermitObjectId is not null
                 and b.ObjectDefTypeId = 1
               order by b.AssociatedPermitObjectId) loop
      if p.AssociatedPermitObjectId != i then
        i := p.AssociatedPermitObjectId;
        dbms_output.put_line('   Permit ' ||
                             api.pkg_columnquery.Value(p.AssociatedPermitObjectId,
                                                       'PermitNumber') || '(' ||
                             api.pkg_columnquery.Value(p.AssociatedPermitObjectId,
                                                       'State') || ')');
        --p1 Loop
        for p1 in (select r.PrimaryPermitObjectId
                     from query.r_abc_associatedpermit r
                    where r.AsscPermitObjectId = p.AssociatedPermitObjectId
                      and api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                    'RequiresVehicle') = 'Y'
                      and api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                    'IsActive') = 'Y'
                    order by api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                       'PermitNumber') desc) loop
          dbms_output.put_line('      Permit ' ||
                               api.pkg_columnquery.Value(p1.PrimaryPermitObjectId,
                                                         'PermitNumber') ||
                               ' for ' ||
                               api.pkg_columnquery.Value(p1.PrimaryPermitObjectId,
                                                         'PermitteeName'));
          select count(*)
            into t_RenewalCount
            from api.relationships r
            join api.jobs j
              on j.JobId = r.ToObjectId
           where r.FromObjectId = p1.PrimaryPermitObjectId
             and r.EndPointId = t_RenewEndpointId
             and j.JobStatus in ('NEW', 'REVIEW');
          if t_RenewalCount = 0 then
            t_ConExpirationDate := api.pkg_columnquery.DateValue(p1.PrimaryPermitObjectId,
                                                                 'ExpirationDate');
            --p3 Loop
            for p3 in (select r.PrimaryPermitObjectId,
                              api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                        'PermitNumber') PermitNumber,
                              api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                        'PermitteeName') PermitteeName,
                              api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                        'PermitType') PermitType
                         from query.r_abc_associatedpermit r
                        where r.AsscPermitObjectId =
                              p.AssociatedPermitObjectId
                          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                        'PermitTypeObjectId') =
                              pt.objectid
                          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                        'RequiresVehicle') = 'Y'
                          and api.pkg_columnquery.Value(r.PrimaryPermitObjectId,
                                                        'IsActive') = 'Y'
                          and api.pkg_columnquery.DateValue(r.PrimaryPermitObjectId,
                                                            'ExpirationDate') =
                              t_ConExpirationDate
                          and (select count(*)
                                 from api.relationships r
                                 join api.jobs j
                                   on j.jobid = r.toobjectid
                                where r.fromobjectid =
                                      r.PrimaryPermitObjectId
                                  and r.endpointid = t_RenewEndpointId
                                  and j.JobStatus in ('NEW', 'REVIEW')) = 0
                          and r.PrimaryPermitObjectId !=
                              p1.PrimaryPermitObjectId) loop
              t_Vehicles := 0;
              --V Loop
              for v in (select *
                          from query.r_abc_permitvehicle pv
                         where pv.PermitObjectId = p3.PrimaryPermitObjectId) loop
                -- Copy vehicle(s) from 'old' Permit to latest Permit
                dbms_output.put_line('         Copy Vehicle ' ||
                                     v.vehicleobjectid || ' from Permit ' ||
                                     p3.Permitnumber || ' (' ||
                                     p3.permitteename || ') to Permit ' ||
                                     api.pkg_columnquery.Value(p1.PrimaryPermitObjectId,
                                                               'PermitNumber'));
                t_Vehicles := t_Vehicles + 1;
                t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ABC_Vehicle'));
                t_RelId    := api.pkg_relationshipupdate.New(t_EndpointId,
                                                             p1.PrimaryPermitObjectId,
                                                             t_ObjectId,
                                                             sysdate);
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'InsigniaNumber',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'InsigniaNumber'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'PreviousInsigniaNumber',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'PreviousInsigniaNumber'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'MakeModelYear',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'makemodelyear'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'OwnedOrLeasedLimousine',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'ownedorleasedlimousine'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'VIN',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'vin'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'Stateregistration',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'Stateregistration'));
                api.pkg_columnupdate.setvalue(t_ObjectId,
                                              'Stateofregistration',
                                              api.pkg_columnquery.Value(v.vehicleobjectid,
                                                                        'Stateofregistration'));
                insert into possedba.issue9442_consolidatepermits
                  (OldPermitId,
                   OldPermitNumber,
                   NewPermitId,
                   NewPermitNumber,
                   OldVehicleId,
                   VehicleId)
                values
                  (p3.PrimaryPermitObjectId,
                   p3.Permitnumber,
                   p1.PrimaryPermitObjectId,
                   api.pkg_columnquery.Value(p1.PrimaryPermitObjectId,
                                             'PermitNumber'),
                   v.vehicleobjectid,
                   t_ObjectId);
                --V Loop
              end loop;
              -- Cancel 'old' Permit
              t_Reason := '         Consolidated with Permit ' ||
                          api.pkg_columnquery.Value(p1.PrimaryPermitObjectId,
                                                    'PermitNumber');
              dbms_output.put_line(t_Reason || '. ' || t_Vehicles ||
                                   ' Vehicles copied');
              t_ObjectId := api.pkg_objectupdate.New(t_ObjectDefId, sysdate);
              api.pkg_columnupdate.SetValue(p3.PrimaryPermitObjectId,
                                            'State',
                                            'Cancelled');
              api.pkg_columnupdate.SetValue(p3.PrimaryPermitObjectId,
                                            'Edit',
                                            'N');
              api.pkg_columnupdate.SetValue(p3.PrimaryPermitObjectId,
                                            'SystemCancelled',
                                            'Y');
              api.pkg_columnupdate.SetValue(t_ObjectId,
                                            'CancellationType',
                                            'Cancellation',
                                            sysdate);
              api.pkg_columnupdate.SetValue(t_ObjectId,
                                            'Reason',
                                            t_Reason,
                                            sysdate);
              t_RelId := api.pkg_relationshipupdate.New(t_CancelEndpointId,
                                                        p3.PrimaryPermitObjectId,
                                                        t_ObjectId,
                                                        sysdate);
              --p3 Loop      
            end loop;
          end if;
          exit;
          --p1 Loop      
        end loop;
      end if;
      --p loop   
    end loop;
  
  --pt loop;        
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;

end;
