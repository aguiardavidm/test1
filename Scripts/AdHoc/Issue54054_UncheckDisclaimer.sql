-- Created on 5/19/2020 by PAUL.ORSTAD
begin
  -- Test statements here
  if user != 'POSSEDBA' then
    api.pkg_Errors.RaiseError(-20000, 'Must be run as POSSEDBA');
  end if;
  dbms_output.put_line('Jobs updated:');
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  for i in (
      select j.JobId, j.ExternalFileNum, api.pkg_ColumnQuery.value(j.jobid, 'Acknowledgement') Acknowledgement
        from api.statuses s
        join api.jobstatuses js
            on js.StatusId = s.StatusId
        join api.jobtypes jt
            on jt.JobTypeId = js.JobTypeId
        join api.jobs j
            on j.StatusId = js.StatusId
           and j.JobTypeId = jt.JobTypeId
       where s.Tag = 'NEW'
         and jt.name in (
            'j_ABC_AmendmentApplication',
            'j_ABC_NewApplication',
            'j_ABC_RenewalApplication',
            'j_ABC_PRApplication',
            'j_ABC_PRRenewal',
            'j_ABC_PRAmendment',
            'j_ABC_PermitApplication',
            'j_ABC_PermitRenewal',
            'j_ABC_Petition')
         and api.pkg_ColumnQuery.value(j.jobid, 'EnteredOnline') = 'Y'
         and (select count(1)
                from api.fees f
               where f.jobid = j.jobid
                 and f.PaidAmount < 0) = 0
               ) loop
    if i.Acknowledgement = 'Y' then
      api.pkg_ColumnUpdate.setvalue(i.jobid, 'Acknowledgement', 'N');
      dbms_output.put_line(i.jobid);
    end if;
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
end;