-- Created on 03/24/2016 by MICHEL SCHEFFERS 
-- Issue 7774: "www" Access Groups should not be available for selection on Users and Roles
-- Update public AccessGroups to set IsAdministratable to N
declare 
  -- Local variables here
  d                          number := 0;
  t_AccessGroupIds           api.pkg_definition.udt_IdList;
      
begin
  dbms_output.enable(NULL);
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select ag.ObjectId
    bulk collect into t_AccessGroupIds
    from query.o_accessgroup ag
   where lower (ag.Description) like 'www%'
     and ag.IsAdministratable = 'Y';
  
  for i in 1 .. t_AccessGroupIds.count loop
     dbms_output.put_line('Updating Access Group ' || api.pkg_columnquery.Value (t_AccessGroupIds(i), 'Description'));
     api.pkg_columnupdate.SetValue(t_AccessGroupIds (i), 'IsAdministratable' ,'N');
     d := d + 1;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

  dbms_output.put_line('Access Groups updated : ' || d);
end;
