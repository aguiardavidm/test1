-- Adds special review document type to all permit types
--   and sets the special condtions mandatory checkbox.
-- Estimated Runtime < 20 seconds
declare
  idlist                            api.pkg_definition.udt_IdList;
  t_DocTypeId                       api.pkg_definition.udt_Id;
  t_RelId                           api.pkg_definition.udt_Id;
  t_EndpointId                      api.pkg_definition.udt_Id;    
begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  --Reset all special conditions checkboxes
  for i in 
    (select r.RelationshipId
    from query.r_ABC_PermitTypeDocumentType r
    where r.SpecialConditionsMandatory = 'Y') loop
    api.pkg_columnupdate.SetValue(i.relationshipid, 'SpecialConditionsMandatory', 'N');
  end loop;
  
  --Find or create Special Conditions Document
  begin
    select dt.ObjectId --,name
    into t_DocTypeId
    from query.o_abc_documenttype dt
    where dt.name like 'Special Conditions';
  exception
    when no_data_found then
      t_DocTypeId := api.pkg_objectupdate.New(api.Pkg_Configquery.ObjectDefIdForName('o_ABC_documentType'));
      api.pkg_ColumnUpdate.SetValue(t_DocTypeId,'Name','Special Conditions');
      api.pkg_ColumnUpdate.SetValue(t_DocTypeId,'Active','Y');
      api.pkg_ColumnUpdate.SetValue(t_DocTypeId,'OnlineDescription','Document that is required on permits when special conditions');
  end;
  
  --Permit Types
  select pt.ObjectId
  bulk collect into idlist
  from query.o_abc_permittype pt;
  
  t_EndpointId := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_PermitType', 'OnlineDocumentType');
  
  for i in 1..idlist.count loop
    -- Figure out if municipal review is a doctype on each permit type
    begin
      select r.RelationshipId
      into t_RelId
      from query.r_ABC_PermitTypeDocumentType r
      where r.PermitTypeObjectId = idlist(i)
        and r.DocumentTypeObjectId = t_DocTypeId;
    exception 
      when NO_DATA_FOUND then
        t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, idlist(i), t_DocTypeId);
    end; 
    -- Set special conditions checkbox on all municipal review document types
    api.pkg_columnupdate.SetValue(t_RelId, 'SpecialConditionsMandatory', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'MunicipalApp', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'MunicipalRenew', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'PoliceApp', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'PoliceRenew', 'Y');
  end loop;
  
  select dt.ObjectId --,name
  into t_DocTypeId
  from query.o_abc_documenttype dt
  where dt.name like 'Municipal Resolution';
  
  --License Types
  select pt.ObjectId
  bulk collect into idlist
  from query.o_abc_licensetype pt;
  
  t_EndpointId := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_LicenseType', 'OnlineDocumentType');
  
  for i in 1..idlist.count loop
    -- Figure out if a doctype exists on each 
    begin
      select r.RelationshipId
      into t_RelId
      from query.r_OLLicenseTypeDocumentType r
      where r.LicenseTypeObjectId = idlist(i)
        and r.DocumentTypeObjectId = t_DocTypeId;
    exception 
      when NO_DATA_FOUND then
        t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, idlist(i), t_DocTypeId);
    end; 
    -- Set availabe checkbox on document type
    api.pkg_columnupdate.SetValue(t_RelId, 'MunicipalAmendment', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'MunicipalApp', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'MunicipalRenew', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'PoliceAmendment', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'PoliceApp', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'PoliceRenew', 'Y');
    
    -- Set mandatory checkbox on document type
    api.pkg_columnupdate.SetValue(t_RelId, 'MunicipalAmendmentMandatory', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'MunicipalAppMandatory', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'MunicipalRenewMandatory', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'PoliceAmendmentMandatory', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'PoliceAppMandatory', 'Y');
    api.pkg_columnupdate.SetValue(t_RelId, 'PoliceRenewMandatory', 'Y');
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  
end;
