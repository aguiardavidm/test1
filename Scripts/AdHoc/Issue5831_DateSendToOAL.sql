-- Created on 12/14/2016 by M Scheffers
-- Issue 5831: Set Date Send to OAL
declare 
  -- Local variables here
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for j in (select p.ProcessId, api.pkg_columnquery.Value (p.processid, 'ObjectDefDescription') Process, api.pkg_columnquery.Value (p.JobId, 'ExternalFileNum') JobNumber
            from   api.processes p
            where  p.JobId in
            (
             select a.ObjectId
             from   query.j_abc_appeal a
             where  a.ExternalFileNum in ('7793', '7679', '7824')
            )
            and    p.Outcome = 'Send to OAL'
           ) loop
     if j.jobnumber = '7793' then
        dbms_output.put_line ('Updating job number ' || j.jobnumber);
        api.pkg_columnupdate.SetValue (j.processid, 'DateSentToOAL', to_date('10/22/2015', 'mm/dd/yyyy'));
     end if;
     if j.jobnumber = '7679' then
        dbms_output.put_line ('Updating job number ' || j.jobnumber);
        api.pkg_columnupdate.SetValue (j.processid, 'DateSentToOAL', to_date('03/06/2012', 'mm/dd/yyyy'));
     end if;
     if j.jobnumber = '7824' then
        dbms_output.put_line ('Updating job number ' || j.jobnumber);
        api.pkg_columnupdate.SetValue (j.processid, 'DateSentToOAL', to_date('08/01/2014', 'mm/dd/yyyy'));
     end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
