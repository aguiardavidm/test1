/* Author: Joshua Lenon
   Description: Create License Type 06 entry for ABC Fee Schedule
   Issue 
   Execution time: < 1 minute
*/
declare
  subtype udt_Id is api.pkg_definition.udt_Id;
-- Object IDs  
  t_FSObjId      udt_Id; -- FeeSchedule Object Id
  t_FDObjId      udt_Id; -- FeeDefinition Object Id
  t_FEObjId      udt_Id; -- FeeElement Object Id
  t_FLObjId          udt_Id; -- FieldElement Object Id
  t_GLObjId      udt_Id; -- GLAccount Object Id
  t_DSObjIdAppl  udt_id; -- Application Job Object Id
  t_DSObjIdRnw   udt_Id; -- Renewal Job Object Id
  t_DSObjIdAm    udt_Id; -- Amend Job Object Id
  t_ODObjIdAppl  udt_id; -- Application Job ObjectDef Object Id
  t_ODObjIdRnw   udt_Id; -- Renewal Job ObjectDef Object Id
  t_ODObjIdAm    udt_Id; -- Amend Job ObjectDef Object Id
-- Relationship/Endpoint IDs
  t_FSEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeSchedule', 'FeeDefinition');
  t_FDEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeSchedule', 'DataSource');
  t_DSEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeDefinition', 'DataSource');
  t_EDEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'DataSource');  
  t_EFEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'FeeSchedule');
  t_LSEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FieldElement', 'FeeSchedule');  
  t_LEEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FieldElement', 'FeeElement');    
  t_RelationshipId  udt_id;
  t_FeeElementId    udt_id;
  t_FieldElementId   udt_id;
  t_FeeScheduleId   udt_id;
  t_DataSourceId    udt_id;
  t_FeeDefinitionId udt_id;
-- Counters
  t_FDRecords number := 0;
  t_FERecords number := 0;
  t_FLRecords number := 0;
  
-- PROCEDURES --
procedure dbug (
    a_Text    varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
end;

procedure CreateFD(a_Amount                     varchar2,
                   a_Condition                  varchar2,
                   a_EffectiveDateColumnName    varchar2,
                   a_FeeDescription             varchar2,
                   a_Description                varchar2,
                   a_ODObjId                    udt_id,
                   a_DSObjId                    udt_id,
                   a_FSObjId                    udt_id,
                   a_GLObjId                    udt_id) is
  t_RelationshipId udt_Id; 
  
begin
  begin
    select fd.objectid
      into t_FDObjId
      from query.o_FeeDefinition fd
      join query.r_Feedef_Feeschedule fdfs on fdfs.FeeDefinitionObjectId = fd.objectid
     where fd.FeeDescription  = a_FeeDescription
       and fdfs.FeeScheduleId = a_FSObjId;
     exception
       when no_data_found then
         t_FDObjId := null;
  end;
  if t_FDObjId is null then
  --Create new object of FeeDefinition
     feescheduleplus.pkg_PosseExternalRegistration.RegisterFeeDefinition (t_FeeDefinitionId,
                                                                          a_Description,
                                                                          a_Condition,
                                                                          a_Amount,
                                                                          a_EffectiveDateColumnName,
                                                                          a_FeeDescription,
                                                                          sysdate,
                                                                          'POSSEDBA',
                                                                          sysdate,
                                                                          'POSSEDBA',
                                                                          api.pkg_columnquery.NumericValue(a_FSObjId,'FeeScheduleId'),
                                                                          api.pkg_columnquery.NumericValue(a_DSObjId,'DataSourceId'),
                                                                          null,
                                                                          '={GLAccountCode}',
                                                                          'Default',
                                                                          null
                                                                          );
     t_FDObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeDefinition',t_FeeDefinitionId);
     dbug ('Added FeeDefinition ' || t_FDObjId || ' - ' || a_FeeDescription);
     t_FDRecords := t_FDRecords + 1;
  --Create relationship from FeeSchedule to FeeDefinition
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FSEndpointId, a_FSObjId, t_FDObjId);
  --Create relationship from FeeDefinition to DataSource
     t_RelationshipId := api.pkg_relationshipupdate.New(t_DSEndpointId, t_FDObjId, a_DSObjId);
  end if;
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Amount', a_Amount);
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Condition', a_Condition);
  api.pkg_columnupdate.SetValue(t_FDObjId, 'EffectiveDateColumnName', a_EffectiveDateColumnName); 
  api.pkg_columnupdate.SetValue(t_FDObjId, 'FeeDescription', a_FeeDescription); 
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Description', a_Description); 

end;

procedure CreateFE(a_Datatype                varchar2,
                   a_Name                    varchar2,
                   a_Job                     varchar2,
                   a_DSObjId                 udt_id,
                   a_FSObjId                 udt_id) is
  t_RelationshipId udt_Id;
  
begin
  --Find FeeElement
  begin
    select fe.objectid
    into t_FEObjId
    from query.o_feeelement fe
    join query.r_DataSource_FeeElement dsfe on dsfe.FeeElementId = fe.ObjectId
    join query.o_datasource ds on ds.ObjectId = dsfe.DataSourceId
    join query.r_FeeSchedule_FeeElement fsfe on fsfe.FeeElementObjectId = fe.objectid
    join query.o_Feeschedule fs on fs.objectid = fsfe.FeeScheduleObjectId
   where fe.GlobalName    = a_Name
     and ds.PosseViewName = a_Job
     and fs.ObjectId      = a_FSObjId;
  exception
     when no_data_found then
       t_FEObjId := null;
  end;
  
  if t_FEObjId is null then
  --Create new object of FeeElement
     feescheduleplus.pkg_PosseExternalRegistration.RegisterFeeElement (
                                                                       t_FeeElementId,
                                                                       a_DataType,
                                                                       'Field',
                                                                       sysdate,
                                                                       'POSSEDBA',
                                                                       sysdate,
                                                                       'POSSEDBA',
                                                                       null,
                                                                       a_Name,
                                                                       api.pkg_columnquery.NumericValue(a_FSObjId,'FeeScheduleId'),
                                                                       api.pkg_columnquery.NumericValue(a_DSObjId,'DataSourceId')
                                                                       );
     t_FEObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeElement',t_FeeElementId);
     dbug ('Added FeeElement ' || t_FEObjId || ' - ' || a_Name || ' for ' || a_Job);
     t_FERecords := t_FERecords + 1;
  --Create relationship from FeeElement to FeeSchedule
     t_RelationshipId := api.pkg_relationshipupdate.New(t_EFEndpointId, t_FEObjId, a_FSObjId);
  --Create relationship from FeeElement to DataSource
     t_RelationshipId := api.pkg_relationshipupdate.New(t_EDEndpointId, t_FEObjId, a_DSObjId);
  end if;
  api.pkg_columnupdate.SetValue(t_FEObjId, 'ColumnName', a_Name); -- Posse Column Name

  --Find FieldElement
  begin
    select fe.objectid
    into t_FLObjId
    from query.o_fieldelement fe
    join query.r_FeeElement_FieldElement fefe on fefe.FieldElementId = fe.ObjectId
    join query.r_DataSource_FeeElement dsfe on dsfe.FeeElementId = fefe.FeeElementId
    join query.r_FeeSchedule_FieldElement fsfe on fsfe.FieldElementId = fe.ObjectId
    join query.o_datasource ds on ds.ObjectId = dsfe.DataSourceId
   where fe.ColumnName      = a_Name
     and ds.PosseViewName   = a_Job
     and fsfe.FeeScheduleId = a_FSObjId;
  exception
     when no_data_found then
       t_FLObjId := null;
  end;
  if t_FLObjId is null then
  --Create new object of FieldElement
     feescheduleplus.pkg_PosseExternalRegistration.RegisterFieldElement (t_FieldElementId, -- returned from call
                                                                         a_Name, -- name of Field Element
                                                                         api.pkg_columnquery.NumericValue(a_FSObjId,'FeeScheduleId'),-- related FeeSchedule Id
                                                                         api.pkg_columnquery.NumericValue(t_FEObjId,'FeeElementId'),-- related FeeElement Id
                                                                         sysdate, -- Created Date
                                                                         'POSSEDBA', -- Created By
                                                                         sysdate, -- Updated Date
                                                                         'POSSEDBA' -- Updated By
                                                                         );
     t_FLObjId := api.pkg_objectupdate.RegisterExternalObject('o_FieldElement',t_FieldElementId);
     t_FLRecords := t_FLRecords + 1;
  --Create relationship from FieldElement to FeeSchedule
     t_RelationshipId := api.pkg_relationshipupdate.New(t_LSEndpointId, t_FLObjId, a_FSObjId);
  --Create relationship from FieldElement to DataSource
     t_RelationshipId := api.pkg_relationshipupdate.New(t_LEEndpointId, t_FLObjId, t_FEObjId);
--     dbug (a_Name || ' for ' || a_Job);     
  end if;
  
end;

-- MAIN PROCESS --
begin
  dbug('Start Creating Fee script');
  api.pkg_logicaltransactionupdate.ResetTransaction();
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  select objectdefid 
  into t_ODObjIdAppl
  from api.objectdefs 
  where Name = 'j_ABC_NewApplication';

  select objectdefid 
  into t_ODObjIdRnw
  from api.objectdefs 
  where Name = 'j_ABC_RenewalApplication';

  select objectdefid 
  into t_ODObjIdAm
  from api.objectdefs 
  where Name = 'j_ABC_AmendmentApplication';

  t_GLObjId      := api.pkg_search.ObjectByColumnValue('o_glaccount', 'Code', '06');
  if t_GLObjId is null then
  --Error, GL Account should be present
      api.pkg_Errors.RaiseError(-20000, 'G/L Account - 06 should be present before you can continue');
  end if;

  t_FSObjId := api.pkg_search.ObjectByColumnValue('o_FeeSchedule', 'Description', 'ABC Fee Schedule');
  dbug(t_FSObjId);
  if t_FSObjId is null then
  --Create a new object of FeeSchedule
     feescheduleplus.pkg_posseexternalregistration.RegisterFeeSchedule(t_FeeScheduleId,
                                                                       'ABC Fee Schedule',
                                                                       to_date ('20150101','yyyymmdd'),
                                                                       to_date ('39991231','yyyymmdd'),
                                                                       sysdate,
                                                                       'POSSEDBA',
                                                                       sysdate,
                                                                       'POSSEDBA'
                                                                       );
     t_FSObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeSchedule',t_FeeScheduleId);
     dbug ('Added FeeSchedule ' || t_FSObjId || ' - ABC Fee Schedule');
  end if;

  -- Checking DataSource  
  --Application
  begin
     select ds.objectid 
       into t_DSObjIdAppl
       from query.r_DataSource_FeeSchedule dsfs 
       join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
      where dsfs.FeeScheduleId = t_FSObjId
        and ds.PosseViewName = 'j_ABC_NewApplication';
     exception
       when no_data_found then
         t_DSObjIdAppl := null;
  end;  
  if t_DSObjIdAppl is null then  
     feescheduleplus.pkg_posseexternalregistration.RegisterDataSource (
                                                                       t_DataSourceId,
                                                                       'New Application Job',
                                                                       'j_ABC_NewApplication',
                                                                       api.pkg_columnquery.NumericValue(t_FSObjId,'FeeScheduleId')
                                                                       );
     t_DSObjIdAppl := api.pkg_objectupdate.RegisterExternalObject('o_DataSource',t_DataSourceId);
     dbug ('Added DataSource ' || t_DSObjIdAppl || ' - Application');
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FDEndpointId, t_FSObjId, t_DSObjIdAppl);
  end if;
  
  --Renewal
  begin
     select ds.objectid 
       into t_DSObjIdRnw
       from query.r_DataSource_FeeSchedule dsfs 
       join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
      where dsfs.FeeScheduleId = t_FSObjId
        and ds.PosseViewName = 'j_ABC_RenewalApplication';
     exception
       when no_data_found then
         t_DSObjIdRnw := null;
  end;  
  if t_DSObjIdRnw is null then  
     feescheduleplus.pkg_posseexternalregistration.RegisterDataSource (
                                                                       t_DataSourceId,
                                                                       'Renewal Job',
                                                                       'j_ABC_RenewalApplication',
                                                                       api.pkg_columnquery.NumericValue(t_FSObjId,'FeeScheduleId')
                                                                       );
     t_DSObjIdRnw := api.pkg_objectupdate.RegisterExternalObject('o_DataSource',t_DataSourceId);
     dbug ('Added DataSource ' || t_DSObjIdRnw || ' - Renewal');
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FDEndpointId, t_FSObjId, t_DSObjIdRnw);
  end if;

  --Amendment
  begin
     select ds.objectid 
       into t_DSObjIdAm
       from query.r_DataSource_FeeSchedule dsfs 
       join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
      where dsfs.FeeScheduleId = t_FSObjId
        and ds.PosseViewName = 'j_ABC_AmendmentApplication';
     exception
       when no_data_found then
         t_DSObjIdAm := null;
  end;  
  if t_DSObjIdAm is null then  
     feescheduleplus.pkg_posseexternalregistration.RegisterDataSource (
                                                                       t_DataSourceId,
                                                                       'Amendment Job',
                                                                       'j_ABC_AmendmentApplication',
                                                                       api.pkg_columnquery.NumericValue(t_FSObjId,'FeeScheduleId')
                                                                       );
     t_DSObjIdAm := api.pkg_objectupdate.RegisterExternalObject('o_DataSource',t_DataSourceId);
     dbug ('Added DataSource ' || t_DSObjIdAm || ' - Amendment');
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FDEndpointId, t_FSObjId, t_DSObjIdAm);
  end if;
     
  CreateFE ('String',
            'SportingFacilityCapacityFee',
            'j_ABC_NewApplication',
            t_DSObjIdAppl,
            t_FSObjId);                                       
  CreateFE ('String',
            'SportingFacilityCapacityFee',
            'j_ABC_RenewalApplication',
            t_DSObjIdRnw,
            t_FSObjId);                                           
  CreateFE ('String',
            'SportingFacilityCapacityFee',
            'j_ABC_AmendmentApplication',
            t_DSObjIdAm,
            t_FSObjId);                                                                               

  -- Additional Warehouse
  CreateFD ('=case {SportingFacilityCapacityFee} 
              when  ''< 7,500 capacity'' then 2500
              when ''7,500 - 14,999 capacity'' then 5000
              when ''15,000 - 22,499 capacity'' then 7500
              when ''22,500 or more'' then 10000
              end case',
            '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''06'' and {SportingFacilityCapacityFee} is not null',
            'CreatedDate',
            'Sporting Event Facility Application Fee',
            'Sporting Event Facility Application Fee',
            t_ODObjIdAppl,
            t_DSObjIdAppl,
            t_FSObjId,
            t_GLObjId);
  CreateFD ('=case {SportingFacilityCapacityFee} 
              when  ''< 7,500 capacity'' then 2500
              when ''7,500 - 14,999 capacity'' then 5000
              when ''15,000 - 22,499 capacity'' then 7500
              when ''22,500 or more'' then 10000
              end case',
            '={LicenseTypeCode} = ''06'' and {SportingFacilityCapacityFee} is not null',
            'CreatedDate',
            'Sporting Event Facility Renewal Fee',
            'Sporting Event Facility Renewal Fee',
            t_ODObjIdRnw,
            t_DSObjIdRnw,
            t_FSObjId,
            t_GLObjId);
  CreateFD ('=case {AmendmentTypeCode}
              when ''PersonAndPlace'' then
                 (case {SportingFacilityCapacityFee} 
                  when  ''< 7,500 capacity'' then 2500
                  when ''7,500 - 14,999 capacity'' then 5000
                  when ''15,000 - 22,499 capacity'' then 7500
                  when ''22,500 or more'' then 10000
                  end
                 ) * 0.20
              else
                 (case {SportingFacilityCapacityFee} 
                  when  ''< 7,500 capacity'' then 2500
                  when ''7,500 - 14,999 capacity'' then 5000
                  when ''15,000 - 22,499 capacity'' then 7500
                  when ''22,500 or more'' then 10000
                  end
                 ) * 0.10
            end case',
            '={LicenseTypeCode} = ''06'' and {SportingFacilityCapacityFee} is not null',
            'CreatedDate',
            'Sporting Event Facility Amendment Fee',
            'Sporting Event Facility Amendment Fee',
            t_ODObjIdAm,
            t_DSObjIdAm,
            t_FSObjId,
            t_GLObjId);

  dbug ('FeeElement records added:    ' || t_FERecords);              
  dbug ('FeeDefinition records added: ' || t_FDRecords);              

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  dbug('End Creating Fee script');
end;
