-- Created on 3/27/2017 by MICHEL.SCHEFFERS 
-- Issue 8636 - Set Application Clemency Days on License Type.
declare 
  -- Local variables here
  i integer := 0;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);
  for d in (select lt.ObjectId
            from   query.o_abc_LicenseType lt
           )
  loop
     if api.pkg_columnquery.Value (d.objectid, 'ApplicationClemencyDays') is null then
        api.pkg_columnupdate.SetValue (d.objectid,'ApplicationClemencyDays', nvl(api.pkg_columnquery.Value (d.objectid, 'ExpirationReminderDays'),0));
        dbms_output.put_line('Setting Clemency Days for ' || api.pkg_columnquery.Value (d.objectid, 'Name') || ' to ' || nvl(api.pkg_columnquery.Value (d.objectid, 'ExpirationReminderDays'),0));
        i := i + 1;     
     end if;
  end loop;
  dbms_output.put_line(i || ' License Types updated');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;                                     
end;
