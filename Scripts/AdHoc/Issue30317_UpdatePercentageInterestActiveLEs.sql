-- Created on 6/14/2017 by Michel.Scheffers
-- Issue 30317 - Update relationship Legal Entity - Includes to modify the PercentageInterest of active LEs that have a percentage of more than 100
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for p in (
            select *
            from   query.r_abc_participatesinincludesle r
            where  r.ParticipatesInLEObjectId in (27771878, 27410544, 27417233, 27419139, 27772998)
           ) loop
     if p.PercentageInterest > 100 and
        api.pkg_columnquery.Value(p.Includesleobjectid,'Active') = 'Y' then
        dbms_output.put_line(api.pkg_columnquery.Value(p.includesleobjectid, 'FormattedName') || ' participates in ' || api.pkg_columnquery.Value(p.participatesinleobjectid, 'FormattedName')
                          || ' for ' || p.percentageinterest || '%');
        if p.ParticipatesInLEObjectId = 27771878 then
           dbms_output.put_line('    Updating ' || api.pkg_columnquery.Value(p.includesleobjectid, 'FormattedName') || ' to 16.19%');
           api.pkg_columnupdate.SetValue(p.relationshipid, 'PercentageInterest', 16.19);
        end if;
        if p.ParticipatesInLEObjectId = 27410544 then
           dbms_output.put_line('    Updating ' || api.pkg_columnquery.Value(p.includesleobjectid, 'FormattedName') || ' to 0.002%');
           api.pkg_columnupdate.SetValue(p.relationshipid, 'PercentageInterest', 0.002);
        end if;
        if p.ParticipatesInLEObjectId = 27417233 then
           dbms_output.put_line('    Updating ' || api.pkg_columnquery.Value(p.includesleobjectid, 'FormattedName') || ' to 69.33%');          
           api.pkg_columnupdate.SetValue(p.relationshipid, 'PercentageInterest', 69.33);
        end if;
        if p.ParticipatesInLEObjectId = 27419139 then
           dbms_output.put_line('    Updating ' || api.pkg_columnquery.Value(p.includesleobjectid, 'FormattedName') || ' to 22.5%');
           api.pkg_columnupdate.SetValue(p.relationshipid, 'PercentageInterest', 22.5);
        end if;
        if p.ParticipatesInLEObjectId = 27772998 then
           dbms_output.put_line('    Updating ' || api.pkg_columnquery.Value(p.includesleobjectid, 'FormattedName') || ' to 5.6%');
           api.pkg_columnupdate.SetValue(p.relationshipid, 'PercentageInterest', 5.6);
        end if;
     end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
       
end;