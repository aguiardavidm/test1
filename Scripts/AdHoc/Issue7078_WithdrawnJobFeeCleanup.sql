rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- Created on 12/21/2015 by JOSHUA.LENON 
-- Clean up the Fees tab of jobs that have already been withrawn before Issue 7078
-- Runtime: 1-6 seconds
declare 
  -- Local variables here
    type NumberList                     is table of number;
    t_JobId                             NumberList;
    t_TransactionId                     number;
  begin
    
    api.pkg_logicaltransactionupdate.ResetTransaction();
    
    -- Select the offending Object Ids from the Permit Application into t_JobId
    select ObjectId
      bulk collect into t_JobId 
      from query.j_Abc_PermitApplication p
     where api.pkg_ColumnQuery.Value(ObjectId, 'HasNonZeroBalances') = 'Y'
       and api.pkg_ColumnQuery.Value(ObjectId, 'StatusDescription') = 'Withdrawn'; 
    -- Loop through all system generated fees on the job that have not been paid, adjusted, etc.
    -- and adjust them down to zero
    for x in 1..t_JobId.count loop
      
      for c in (select a.FeeId,
                       a.Amount + a.AdjustedAmount NetAmount
                  from api.Fees a
                  where a.JobId = t_JobId(x)
                    and a.SystemGenerated = 'Y'
                    and a.PostedDate is null
                    and (select count(*)
                          from api.FeeTransactions b
                         where b.FeeId = a.FeeId
                           and b.TransactionType = 'Pay') = 0) loop
                           
        t_TransactionId := api.pkg_FeeUpdate.Adjust(c.FeeId, -c.NetAmount, 'Adjusted by system due to withdrawal of job', sysdate);
       end loop;
       
     end loop;
     
     
    -- Select offending Object Ids from the Permit Renewal Job into t_JobId
    select ObjectId
      bulk collect into t_JobId 
      from query.j_Abc_PermitRenewal p
     where api.pkg_ColumnQuery.Value(ObjectId, 'HasNonZeroBalances') = 'Y'
       and api.pkg_ColumnQuery.Value(ObjectId, 'StatusDescription') = 'Withdrawn'; 
    -- Loop through all system generated fees on the job that have not been paid, adjusted, etc.
    -- and adjust them down to zero
    for x in 1..t_JobId.count loop
      
      for c in (select a.FeeId,
                       a.Amount + a.AdjustedAmount NetAmount
                  from api.Fees a
                  where a.JobId = t_JobId(x)
                    and a.SystemGenerated = 'Y'
                    and a.PostedDate is null
                    and (select count(*)
                          from api.FeeTransactions b
                         where b.FeeId = a.FeeId
                           and b.TransactionType = 'Pay') = 0) loop
                           
        t_TransactionId := api.pkg_FeeUpdate.Adjust(c.FeeId, -c.NetAmount, 'Adjusted by system due to withdrawal of job', sysdate);
       end loop;
       
     end loop;
    api.pkg_logicaltransactionupdate.EndTransaction;     
    
  end;
/
