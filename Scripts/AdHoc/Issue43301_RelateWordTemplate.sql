/*-----------------------------------------------------------------------------
 * Author: Ash Schaefer
 * Expected run time: < 3 minutes
 * Purpose: Connect Word Interface Letters with deleted templates to a new version
 * of the template (hardcoded ObjectIds). Assumes only Investigation Reports are impacted.
 *---------------------------------------------------------------------------*/
declare

  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  -- Declare Local variables here
  t_RowsProcessed                       pls_integer := 0;
  t_ScriptDescription                   varchar2(4000) :=
      'Task 57000 : Issue 43014 (Reattach Template to Word Interface Letters)';
  t_NewWordTemplateId                   udt_Id := &templateid; -- newly uploaded template id
  t_RelId                               udt_Id;
  t_EndPointId                          udt_Id :=
      api.Pkg_Configquery.EndPointIdForName('o_ABC_Letter', 'WordInterfaceTemplate');
  t_WordInterfaceLetters                udt_IdList;
  t_LetterId               udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
  end;

begin
  dbug('Starting script ' || t_ScriptDescription);
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

 -- Get all deleted template ids
  select l.ObjectId
    bulk collect into t_WordInterfaceLetters
    from query.d_wordmergeletter l
    where not exists (select 1
                        from query.d_Wordinterfacetemplate t
                       where t.ObjectId = l.d_WordMergeTemplate_ObjectId);
   
  for w in 1..t_WordInterfaceLetters.count() loop
    t_LetterId := api.pkg_ColumnQuery.Value(t_WordInterfaceLetters(w),
        'LetterObjectId');
    api.pkg_ColumnUpdate.SetValue(t_WordInterfaceLetters(w),
        'd_WordMergeTemplate_ObjectId', t_NewWordTemplateId);
    t_RowsProcessed := t_RowsProcessed + 1;
      
    -- Update Template Relationship on Letter object
    select max(relationshipid)
    into t_RelId
    from api.relationships r
    where r.FromObjectId = t_LetterId
      and r.endpointid = t_EndPointId;
        
    if t_RelId is not null then
      api.pkg_RelationshipUpdate.Remove(t_RelId);
    end if;
      
    t_RelId := api.pkg_RelationshipUpdate.New(t_EndPointId,
        t_LetterId, t_NewWordTemplateId);
   
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
  dbug('Rows Processed: ' || t_RowsProcessed);
  dbug('Completed script ' || t_ScriptDescription);

end;
/
