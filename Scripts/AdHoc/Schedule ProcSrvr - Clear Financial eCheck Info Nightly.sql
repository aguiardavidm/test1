PL/SQL Developer Test script 3.0
37
-- RUN AS POSSESYS
-- Created by Andy Patterson (CXUSA)
-- Created on 7/29/2015
-- DESCRIPTION: This script inserts a nightly (9:30 PM) job onto the Process Server
--              to clear banking information from the eCheck interface

declare
  t_ScheduleId   number;
  t_Frequency    api.pkg_Definition.udt_Frequency;
begin
  t_Frequency := api.pkg_Definition.gc_Daily;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure('abc.pkg_Fees.ClearBankingInfo', --Name
                                                                 'Clear Banking Information (eCheck)', --Description
                                                                 t_Frequency, --Frequency
                                                                 null,        --IncrementBy
                                                                 null,        --Unit
                                                                 null,        --Arguments
                                                                 'N',        --SkipBacklog
                                                                 'Y',        --DisableOnError
                                                                 'Y',         --Sunday
                                                                 'Y',         --Monday
                                                                 'Y',         --Tuesday
                                                                 'Y',         --Wednesday
                                                                 'Y',         --Thursday
                                                                 'Y',         --Friday
                                                                 'Y',         --Saturday
                                                                 'N',        --MonthEnd
                                                                 --Run at 10 PM nightly
                                                                 (trunc(sysdate) + (21.5/24)), --StartDate
                                                                 null,        --EndDate
                                                                 null,        --Email
                                                                 null,        --ServerId
                                                                 api.pkg_Definition.gc_normal);       --Priority
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;
0
0
