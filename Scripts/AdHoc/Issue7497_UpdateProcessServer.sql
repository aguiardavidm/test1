rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- RUN AS POSSESYS
-- Created by Michel Scheffers
-- Created on 02/26/2016
-- DESCRIPTION: This script inserts an anual job (01:00am on January 1) onto the Process Server
--              to clear banking information from the eCheck interface

declare
  t_ScheduleId   number;
  t_Frequency    api.pkg_Definition.udt_Frequency;
begin
  t_Frequency := api.pkg_Definition.gc_Yearly;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure('abc.pkg_ABC_Common.dup_IsWithinThreshold', --Name
                                                                 'Update dup_IsWithinThreshold for Vintage', --Description
                                                                 t_Frequency, --Frequency
                                                                 null,        --IncrementBy
                                                                 null,        --Unit
                                                                 null,        --Arguments
                                                                 'N',        --SkipBacklog
                                                                 'Y',        --DisableOnError
                                                                 'N',         --Sunday
                                                                 'N',         --Monday
                                                                 'N',         --Tuesday
                                                                 'N',         --Wednesday
                                                                 'N',         --Thursday
                                                                 'N',         --Friday
                                                                 'N',         --Saturday
                                                                 'N',        --MonthEnd
                                                                 --Run at 01 AM anually
                                                                 (to_date('01/01/2017','MM/DD/YYYY')) + (01.00/24), --StartDate
                                                                 null,        --EndDate
                                                                 null,        --Email
                                                                 null,        --ServerId
                                                                 api.pkg_Definition.gc_normal);       --Priority
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;
/
