declare
  t_MuniObjId       api.pkg_definition.udt_idlist;
  t_PoliceRelIdLIst api.pkg_definition.udt_IdList;
begin
  -- Call the procedure
  select distinct objectid
    bulk collect
    into t_MuniObjId
    from query.o_ABC_Office;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();

  for i in 1 .. t_MuniObjId.count loop
  
    abc.pkg_abc_municipality.assignaccessgroup(t_muniObjId(i),
                                               trunc(sysdate));
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

  select distinct pm.RelationshipId
    bulk collect
    into t_PoliceRelIdLIst
    from query.r_PoliceUserMunicipality pm;

  for i in 1 .. t_PoliceRelIdLIst.count loop
    abc.pkg_abc_municipality.AddPoliceStaff(t_PoliceRelIdList(i),
                                            trunc(sysdate));
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

end;
