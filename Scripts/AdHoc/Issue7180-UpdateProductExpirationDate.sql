-- Created on 01/07/2016 by MICHEL.SCHEFFERS 
-- Set product expirationdate to 2016
declare 
  -- Local variables here

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(NULL);
  for p in (select ObjectId
              from query.o_abc_product
             where ExpirationDate = to_date('12/31/2017','MM/DD/YYYY')
			   and State = 'Current') loop
     api.pkg_columnupdate.SetValue (p.objectid, 'ExpirationDate', to_date('12/31/2016','mm/dd/yyyy'));
     api.pkg_columnupdate.SetValue (p.objectid, 'NonRenewalNoticeDate', to_date('03/01/2017','mm/dd/yyyy'));
     dbms_output.put_line ('Updating expiration date for ' || api.pkg_columnquery.Value (p.objectid,'RegistrationNumber') || '-' || api.pkg_columnquery.Value (p.objectid,'Name'));
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;
