-- Created on 6/21/2016 by MICHEL.SCHEFFERS 
-- Apply Instance Security on Amendment jobs
-- Issue 8518
declare 
  -- Local variables here
  t_Jobs integer := 0;

begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all Amendment Application jobs
  for i in (select aa.ObjectId
              from query.j_abc_amendmentapplication aa
            ) loop
    -- Apply Instance Security for each job
    t_Jobs := t_Jobs + 1;
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.ObjectId, sysdate);
  end loop;
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbms_output.put_line('Amendment Jobs updated: ' || t_Jobs);
end;
