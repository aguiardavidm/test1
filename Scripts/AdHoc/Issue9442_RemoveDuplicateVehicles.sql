-- Created on 7/19/2017 by MICHEL.SCHEFFERS 
-- Issue 9442 - Remove Permit-Vehicle relationship where duplicates exist

declare 
  -- Local variables here
  i                           number  := 0;
  t_ObjectId                  integer := 0;
  t_RelId                     integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);
  for pt in (select p.oldvehicleid
             from   possedba.issue9442_consolidatepermits p
             group  by p.oldvehicleid
             having count (*) > 1
            ) loop
     dbms_output.put_line('Checking Permits for Vehicle ' || pt.oldvehicleid || '-' || api.pkg_columnquery.Value(pt.oldvehicleid, 'MakeModelYear'));
     select max(p.vehicleid)
     into   t_ObjectId
     from   possedba.issue9442_consolidatepermits p
     where  p.oldvehicleid = pt.oldvehicleid;
     dbms_output.put_line('Vehicle to skip ' || t_ObjectId || '-' || api.pkg_columnquery.Value(t_ObjectId, 'MakeModelYear'));
     for v in (select *
               from   possedba.issue9442_consolidatepermits p1
               where  p1.oldvehicleid = pt.oldvehicleid
               and    p1.vehicleid   != t_ObjectId
              ) loop
        dbms_output.put_line('Permit ' || v.newpermitid || '-' || api.pkg_columnquery.Value(v.newpermitid, 'PermitNumber'));
        dbms_output.put_line('Vehicle to remove ' || v.vehicleid || '-' || api.pkg_columnquery.Value(v.vehicleid, 'MakeModelYear'));
        begin
           select r.RelationshipId
           into   t_RelId
           from   query.r_abc_permitvehicle r
           where  r.VehicleObjectId = v.vehicleid
           and    r.PermitObjectId = v.newpermitid;
           i := i + 1;          
        api.pkg_relationshipupdate.Remove(t_RelId);
--        api.pkg_objectupdate.Remove(v.vehicleid);
        exception
           when no_data_found then
              null;
        end;
     end loop;
  end loop;
             
  dbms_output.put_line(i || ' Vehicles removed');
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;

end;