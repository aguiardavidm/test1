declare
  t_SSNCount number := 0;
  t_DOBCount number := 0;
  t_LESSNDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'SSNTINEncrypted');
  t_LEHistSSNDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntityHistory', 'SSNTINEncrypted');
  t_LEDOBDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'BirthDateEncrypted');
  t_LEHistDOBDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntityHistory', 'BirthDateEncrypted');
begin
  --Find all encrypted SSNs
  for i in (select cd.ObjectId, cd.ColumnDefId, cd.AttributeValue, abc.pkg_abc_encryption.encrypt(abc.pkg_abc_encryption.decrypt(cd.AttributeValue)) reencrypt
              from possedata.objectcolumndata cd
             where cd.ColumnDefId in (t_LESSNDefId,
                                      t_LEHistSSNDefId)) loop
    --Update values
    update possedata.objectcolumndata_t t
       set t.attributevalue = i.reencrypt,
           t.searchvalue = lower(substr(i.reencrypt, 1, 20))
     where t.objectid = i.objectid
       and t.columndefid = i.columndefid;
    t_SSNCount := t_SSNCount + 1;
  end loop;
  
  --Find all encrypted birth dates
  for i in (select cd.ObjectId, cd.ColumnDefId, cd.AttributeValue, abc.pkg_abc_encryption.encrypt(abc.pkg_abc_encryption.decrypt(cd.AttributeValue)) reencrypt
              from possedata.objectcolumndata cd
             where cd.ColumnDefId in (t_LEDOBDefId,
                                      t_LEHistDOBDefId)) loop
    --Update values
    update possedata.objectcolumndata_t t
       set t.attributevalue = i.reencrypt,
           t.searchvalue = lower(substr(i.reencrypt, 1, 20))
     where t.objectid = i.objectid
       and t.columndefid = i.columndefid;
    t_DOBCount := t_DOBCount + 1;
  end loop;
  dbms_output.put_line('Number of SSNs Encrypted: ' || t_SSNCount);
  dbms_output.put_line('Number of DOBs Encrypted: ' || t_DOBCount);
  commit;
end;