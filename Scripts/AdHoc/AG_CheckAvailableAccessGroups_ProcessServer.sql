-- RUN AS POSSESYS
-- Created by Michel Scheffers
-- Created on 01/08/2018
-- DESCRIPTION: This script inserts a monthly job (1st of the month 01:00am) to check the available Public Access Groups

declare
  t_ScheduleId   number;
  t_Frequency    api.pkg_Definition.udt_Frequency;
begin
  if user != 'POSSESYS' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSESYS');
  end if;

  t_Frequency := api.pkg_Definition.gc_Monthly;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure('abc.pkg_ABC_Common.CheckPublicAccessGroups', --Name
                                                                 'Check Public Access Groups', --Description
                                                                 t_Frequency, --Frequency
                                                                 null,        --IncrementBy
                                                                 null,        --Unit
                                                                 null,        --Arguments
                                                                 'N',        --SkipBacklog
                                                                 'Y',        --DisableOnError
                                                                 'N',         --Sunday
                                                                 'N',         --Monday
                                                                 'N',         --Tuesday
                                                                 'N',         --Wednesday
                                                                 'N',         --Thursday
                                                                 'N',         --Friday
                                                                 'N',         --Saturday
                                                                 'N',        --MonthEnd
                                                                 --Run at 1:00 AM. Take first day of current month to trigger initial email
                                                                 to_date(to_char(sysdate,'mm') || '/01/' || to_char(sysdate,'yyyy'), 'mm/dd/yyyy') + 1/24,
                                                                 null,        --EndDate
                                                                 null,        --Email
                                                                 null,        --ServerId
                                                                 api.pkg_Definition.gc_normal);       --Priority
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;
