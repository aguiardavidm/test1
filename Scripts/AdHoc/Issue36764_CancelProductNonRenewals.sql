-- Issue 36764-36866
-- Cancel all Product Non-Renewal jobs that are in New or Review status
-- 08/16/2018
declare
  t_JobTypeId                           number (9);
  t_StatusId                            number (9);
  t_JobsCancelled                       number (9) := 0;
  t_ObjectList                          api.udt_ObjectList;
  t_ProcessId                           number;
  t_ProcessDefId                        number := 
      api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus');
  t_Outcome                             varchar2(100):= 'Cancelled';
begin

  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Execute as POSSEDBA');
  end if;

  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select jt.jobtypeid
  into t_JobTypeId
  from  api.jobtypes jt
  where jt.name = 'j_ABC_PRNonRenewal';
  select s.StatusId
  into t_StatusId
  from api.statuses s
  join api.jobstatuses js on js.StatusId=s.StatusId
  where s.description = 'New'
  and js.JobTypeId = t_JobTypeId;

  t_ObjectList := api.udt_ObjectList();
  extension.pkg_cxProceduralSearch.InitializeSearch('j_ABC_PRNonRenewal');
  extension.pkg_cxproceduralsearch.SearchBySystemColumn('JobTypeId', t_JobTypeId, t_JobTypeId, false);
  extension.pkg_cxProceduralSearch.PerformSearch(t_ObjectList, 'and');
  dbms_output.put_line('Jobs: ' || t_ObjectList.Count);

  for j in 1..t_ObjectList.Count loop
    if api.pkg_columnquery.Value(t_ObjectList(j).ObjectId, 'StatusName') in
        ('NEW', 'REVIEW') then
      t_JobsCancelled := t_JobsCancelled + 1;
      for pr in (select j.JobId, p.ProcessId, p.Outcome
                 from api.jobs j
                 join api.processes p
                   on p.JobId = j.JobId
                 where j.JobId = t_ObjectList(j).ObjectId
                ) loop
        if pr.processid is not null and
          pr.outcome is null then
          for pa in (select p.UserId
                     from api.processassignments p
                     where p.ProcessId = pr.processid
                    ) loop
            api.pkg_processupdate.Unassign(pr.processid, pa.userid);
          end loop;
          api.pkg_processupdate.Remove(pr.ProcessId);
        end if;
      end loop;
      t_ProcessId := api.pkg_processupdate.New (t_ObjectList(j).ObjectId, t_ProcessDefId, 'Change Status',sysdate,sysdate,sysdate);
      api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange', 'Cancelled per Issue 30674/36866.'); 
      api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);
    end if;
  end loop;
  
  dbms_output.put_line('Jobs Cancelled: ' || t_JobsCancelled);
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  

end;
