-- Created on 8/31/2015 by Keaton
-- Correct Inactive dates 
declare 
  t_UpdateLicenseId  number;
  t_InactiveDateCDId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'InactivityStartDate');
  t_InactiveDate     date;
begin
  
  --Transaction Handling
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  -- Find License Ids with license number 1216-33-016 – Inactive date should be 10/30/2012 not June 26, 2015.
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber like '%1216-33-016%'
    and l.InactivityStartDate is not null
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    t_InactiveDate := to_date('10/30/2012', 'MM/DD/YYYY HH:MI:SS');
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'InactivityStartDate', t_InactiveDate);
    
   -- Find License Ids with license number 1216-33-032 - Inactive date should be 10/01/2014 not June 19, 2015.
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber like '%1216-33-032%'
    and l.InactivityStartDate is not null
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    t_InactiveDate := to_date('10/01/2014', 'MM/DD/YYYY HH:MI:SS');
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'InactivityStartDate', t_InactiveDate);   
    
  -- Find License Ids with license number 1809-33-006 – Inactivity date should be 10/28/2012 not July 7, 2015
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber like '%1809-33-006%'
    and l.InactivityStartDate is not null
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    t_InactiveDate := to_date('10/28/2012', 'MM/DD/YYYY HH:MI:SS');
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'InactivityStartDate', t_InactiveDate);
    
    -- Find License Ids with license number 0428-33-004 – Inactive date should be 9/21/14
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber like '%0428-33-004%'
    and l.InactivityStartDate is not null
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    t_InactiveDate := to_date('9/21/2014', 'MM/DD/YYYY HH:MI:SS');
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'InactivityStartDate', t_InactiveDate);
   
    -- Find License Ids with license number 1809-33-011 - Inactive Date 5/20/2015
  
  select l.objectid
    into t_UpdateLicenseId
   from query.o_abc_license l
  where l.licensenumber like '%1809-33-011%'
    and l.InactivityStartDate is not null
    and l.State = 'Active';
  --  Set the Inactivity Start Date 
    t_InactiveDate := to_date('5/20/2015', 'MM/DD/YYYY HH:MI:SS');
    api.pkg_columnupdate.SetValue(t_UpdateLicenseId, 'InactivityStartDate', t_InactiveDate);
  
   --Transaction Handling
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;
