/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 seconds
 * Purpose: Grants select to ABCRW and ABCNJ on the Objects, ObjectColumnData,
 *   LogicalTransactions, DeletedObjects and StoredRelationship Tables in the
 *   PosseAudit Schema.
 * Notes: Run in a SQL window.
 *---------------------------------------------------------------------------*/
-- Grant Access To ABCRW on PosseAudit Schema tables
grant select
on PosseAudit.Objects
to ABCRW;

grant select
on PosseAudit.ObjectColumnData
to ABCRW;

grant select
on PosseAudit.LogicalTransactions
to ABCRW;

grant select
on PosseAudit.DeletedObjects
to ABCRW;

grant select
on PosseAudit.StoredRelationships
to ABCRW;

-- Grant Access To ABCNJ on PosseAudit Schema tables
grant select
on PosseAudit.Objects
to ABCNJ;

grant select
on PosseAudit.ObjectColumnData
to ABCNJ;

grant select
on PosseAudit.LogicalTransactions
to ABCNJ;

grant select
on PosseAudit.DeletedObjects
to ABCNJ;

grant select
on PosseAudit.StoredRelationships
to ABCNJ;