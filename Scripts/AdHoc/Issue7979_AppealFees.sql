begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select q.objectid, q.ExternalFileNum, q.StatusName
              from dataconv.j_abc_appeal j
              join query.j_abc_appeal q on q.objectid = j.objectid
             where q.StatusName != 'CLOSED') loop
    api.pkg_columnupdate.SetValue(i.objectid, 'DontChargeFees', 'Y');
    api.pkg_columnupdate.SetValue(i.objectid, 'GenerateFees', 'Y');
    dbms_output.put_line(i.externalfilenum);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;