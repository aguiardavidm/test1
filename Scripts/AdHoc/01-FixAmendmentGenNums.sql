--Add notes here
declare
  t_NewLicNum varchar2(40);
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *--NewL, OldL, StatusDescription, ExternalFileNum, CreatedDate
              from (select aa.LicenseObjectId,
                           aa.StatusDescription,
                           aa.ExternalFileNum,api.pkg_columnquery.Value(aa.LicenseObjectId, 'LicenseNumber') NewL,
                           api.pkg_columnquery.value(r.LicenseObjectId, 'LicenseNumber') OldLic,
                           aa.CreatedDate,
                           aa.AmendmentType
                     from query.j_abc_amendmentapplication aa
                     join query.r_ABC_AmendJobLicenseAmendType alt on alt.AmendJobLicenseId = aa.ObjectId
                     join query.o_abc_amendmenttype aat on aat.ObjectId = alt.AmendmentTypeId
                     join query.r_ABC_LicenseToAmendJobLicense r on r.AmendJobId = aa.objectid
                    where aat.IncrementLicenseNumber = 'Y')
            where Newl = OldLic) loop
    t_NewLicNum := substr(i.oldlic, 1, 12) || lpad(to_char(substr(i.oldlic, 13, 3)+1), 3, '000');
    api.pkg_columnupdate.SetValue(i.licenseobjectid, 'LicenseNumber', t_NewLicNum);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;