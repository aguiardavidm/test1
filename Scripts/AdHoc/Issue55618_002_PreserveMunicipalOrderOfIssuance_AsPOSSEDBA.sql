/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 5 sec
 * Purpose: This script ensures that once the job is approved, the
 *  license number will be the same as the original license. See issue 55618
 *  for more detail.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  t_MuniCode                            varchar2(4) := '1106';
  t_MuniSeqStart                        varchar2(3) := '14';
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  -- Preserve the Municipal Order of Issuance by resetting the start value of the sequence
  execute immediate 'drop sequence abc.muniorderissue' || t_MuniCode || '_seq';
  execute immediate 'create sequence abc.muniorderissue' || t_MuniCode || '_seq start with ' ||
      t_MuniSeqStart;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
