-- Created on 7/20/2018
-- Apply Instance Security on Amendment and Renewal jobs
-- Issue 31298
declare 
  -- Local variables here
  t_Jobs integer := 0;

begin 

  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all Amendment Application jobs
  t_Jobs := 0;
  for i in (select aa.ObjectId
              from query.j_abc_amendmentapplication aa
            ) loop
    -- Apply Instance Security for each job
    t_Jobs := t_Jobs + 1;
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.ObjectId, sysdate);
  end loop;
  dbms_output.put_line('Amendment Jobs updated: ' || t_Jobs);
  
  -- Loop through all Renewal Application jobs
  t_Jobs := 0;
  for i in (select ra.ObjectId
              from query.j_abc_RenewalApplication ra
            ) loop
    -- Apply Instance Security for each job
    t_Jobs := t_Jobs + 1;
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.ObjectId, sysdate);
  end loop;
  dbms_output.put_line('Renewal Jobs updated: ' || t_Jobs);
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;