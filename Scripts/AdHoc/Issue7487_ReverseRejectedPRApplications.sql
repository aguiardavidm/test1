-- Created on 02/17/2016 by MICHEL SCHEFFERS 
declare 
  -- Local variables here
  t_ProcessId integer;
  t_ProcessDefId number := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus'); -- DefId for Process to create
  t_ProcessDefId1 number := api.pkg_configquery.ObjectDefIdForName('p_ABC_PRAdministrativeReview'); -- DefId for Process to create
  t_Outcome varchar2(100):= 'In Review'; -- Outcome
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for j in (select pa.ObjectId
              from query.j_abc_prapplication pa
             where pa.ExternalFileNum in ('24374', '27294', '41128', '48639', '49436', '50066', '52352', '52501')
           ) loop
     t_ProcessId := api.pkg_processupdate.New (j.ObjectId, t_ProcessDefId, 'Change Status',sysdate,sysdate,sysdate);
     api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange', 'Reversal from Rejection');  
     api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);
     t_ProcessId := api.pkg_processupdate.New (j.ObjectId, t_ProcessDefId1, 'Administrative Review PR',sysdate,sysdate,sysdate);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;