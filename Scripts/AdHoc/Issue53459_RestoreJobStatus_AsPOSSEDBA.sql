/*-----------------------------------------------------------------------------
 * Creating Author: Maria Knigge
 * Expected run time:
 * Purpose: To remove the open Send Permit process, change the Job Status to
 *  Municipal Review and add MC/PC Review processes to Job in NJPROD.
 *
 *  For Issue 53459, when running script in NJPROD, set...
 *     t_JobNumber := '349755'
 *     t_ProcessTypeToRemove t:= 'p_ABC_SendPermit'
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_ChangeStatus                        number
      := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');
  t_JobId                               number;
  t_JobNumber                           varchar2(40) := '';
  t_ProcessId                           number;
  t_ProcessToRemoveId                   number;
  t_ProcessTypesToAdd                   udt_IdList;
  t_ProcessTypeToRemove                 varchar2(30) := '';
  t_ProcessTypeToRemoveId               number;
begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  -- Get ProcessTypeId for Processes to Add/Remove
  select pt.ProcessTypeId
  bulk collect into t_ProcessTypesToAdd
  from api.processtypes pt
  where pt.Name in ('p_ABC_MonitorReview', 'p_ABC_PoliceReview', 'p_ABC_MunicipalityReview');

  select pt.ProcessTypeId
  into t_ProcessTypeToRemoveId
  from api.processtypes pt
  where pt.Name = t_ProcessTypeToRemove;

  -- Get JobId and ProcessId of the Process to be removed
  select
    x.ObjectId,
    p.ProcessId
  into
    t_JobId,
    t_ProcessToRemoveId
  from
    table(api.pkg_SimpleSearch.CastableObjectsByIndex('j_ABC_PermitApplication',
        'ExternalFileNum', t_JobNumber)) x
    join api.processes p
        on x.ObjectId = p.JobId
  where p.ProcessTypeId = t_ProcessTypeToRemoveId;

  -- Remove Send Denial Notification Process, change status and add new MC/PC Review Processes
  api.pkg_ProcessUpdate.Remove(t_ProcessToRemoveId);

  -- Change Job Status to Municipal Review
  t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ChangeStatus, null, null, null, null);
  api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ReasonForChange',
      'Job Completed with wrong outcome.');
  api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Municipal Review');

  -- Add Processes
  for i in 1..t_ProcessTypesToAdd.count() loop
    t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ProcessTypesToAdd(i), null, null, null, null);
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction();

  commit;

end;
