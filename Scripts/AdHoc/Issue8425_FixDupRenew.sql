--dup_IsRenewable is not set on a number of permits.  This script sets it to the correct value
declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'dup_IsRenewable');
  t_Count number := 0;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.objectid, p.PermitNumber, p.IsRenewable, p.dup_IsRenewable
              from query.o_abc_permit p
             where p.objectdeftypeid = 1) loop
    --If the dup value is not set
    if (i.IsRenewable is not null and i.dup_IsRenewable is null) then
      insert into possedata.objectcolumndata
      (
      logicaltransactionid,
      objectid,
      columndefid,
      attributevalue,
      searchvalue
      )
      values
      (
      api.pkg_logicaltransactionupdate.CurrentTransaction,
      i.ObjectId,
      t_ColumnDefId,
      i.IsRenewable,
      lower(i.IsRenewable)
      );
    --if the non dup value is empty and the dup has a value
    elsif (i.IsRenewable is null and i.dup_IsRenewable is not null) then
      delete from possedata.objectcolumndata cd
       where cd.ObjectId = i.objectid
         and cd.ColumnDefId = t_ColumnDefId;
    --if both fields are populated but inconsistent
    elsif ((i.IsRenewable is not null and i.dup_IsRenewable is not null) and (i.IsRenewable != i.dup_IsRenewable)) then
      update possedata.objectcolumndata cd
         set cd.LogicalTransactionId = api.pkg_logicaltransactionupdate.CurrentTransaction,
             cd.AttributeValue = i.IsRenewable,
             cd.SearchValue = lower(i.IsRenewable)
       where cd.ObjectId = i.objectid
         and cd.ColumnDefId = t_ColumnDefId;
    end if;
    t_Count := t_Count+1;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
  dbms_output.put_line(t_Count);
  dbms_output.put_line('Expecting around 220000');
  commit;
end;