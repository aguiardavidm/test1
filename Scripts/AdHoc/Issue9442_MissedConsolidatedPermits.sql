-- Created on 9/29/2017 by MICHEL.SCHEFFERS/Paul Orstad
-- Issue 9442 - Consolidate Active permits of same type for same License/Permit.
-- Secondary script to account for permits missed becasue the Associated Permit had been renewed
create table possedba.issue9442_consolidatepermits2
(
   OldPermitId     number,
   OldPermitNumber varchar2(40),
   NewPermitId     number,
   NewPermitNumber varchar2(40),
   OldVehicleId    number,
   VehicleId       number
);

declare 
  -- Local variables here
  i                           number  := 0;
  t_EndpointId                integer := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Vehicle');
  t_CancelEndpointId          integer := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitCancellation');
  t_ObjectDefId               integer := api.pkg_configquery.ObjectDefIdForName('o_ABC_PermitCancellation');
  t_Reason                    varchar2(400);
  t_ObjectId                  integer := 0;
  t_RelId                     integer;
  t_LicenseObjectId           integer;
  t_Vehicles                  integer := 0;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  dbms_output.enable(null);
  for pt in (select objectid, t.Name
             from   query.o_abc_permittype t
             where  t.RequiresVehicles = 'Y'
            ) loop
     i := 0;
     dbms_output.put_line('Checking Associated Permits for ' || pt.name);
     for p in (select b.AssociatedPermitObjectId, b.AssociatedPermitNumber
               from   query.o_abc_permit b
               where  b.PermitTypeObjectId = pt.objectid
               and    b.AssociatedPermitObjectId is not null
               and    b.ObjectDefTypeId = 1
               order  by b.AssociatedPermitObjectId 
              ) loop
        if p.AssociatedPermitObjectId != i then
           i := p.AssociatedPermitObjectId;
           dbms_output.put_line('   Permit ' || api.pkg_columnquery.Value(p.AssociatedPermitObjectId, 'PermitNumber') || '(' || api.pkg_columnquery.Value(p.AssociatedPermitObjectId, 'State') || ')');
           for p1 in (select r.PrimaryPermitObjectId
                      from   query.r_abc_associatedpermit r
                      join table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', p.associatedpermitnumber)) ap on ap.objectid = r.AsscPermitObjectId
                      --where  r.AsscPermitObjectId = p.AssociatedPermitObjectId
                      where    api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'RequiresVehicle') = 'Y' 
                      and    api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'IsActive') = 'Y'
                      order  by api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitNumber') desc
                     ) loop
                 dbms_output.put_line('      Permit ' || api.pkg_columnquery.Value(p1.PrimaryPermitObjectId, 'PermitNumber') || ' for ' || api.pkg_columnquery.Value(p1.PrimaryPermitObjectId, 'PermitteeName'));
                 for p3 in (select r.PrimaryPermitObjectId, api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitNumber') PermitNumber, 
                                   api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitteeName') PermitteeName,
                                   api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitType') PermitType
                            from   query.r_abc_associatedpermit r
                            join table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', p.associatedpermitnumber)) ap on ap.objectid = r.AsscPermitObjectId
                            where  api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitTypeObjectId') = pt.objectid
                            and    api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'RequiresVehicle') = 'Y' 
                            and    api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'IsActive') = 'Y'
                            and    r.PrimaryPermitObjectId != p1.PrimaryPermitObjectId
                           ) loop
                       t_Vehicles := 0;
                       for v in (select *
                                 from   query.r_abc_permitvehicle pv
                                 where  pv.PermitObjectId = p3.PrimaryPermitObjectId
                                ) loop                
                          -- Copy vehicle(s) from 'old' Permit to latest Permit
                          dbms_output.put_line('         Copy Vehicle ' || v.vehicleobjectid || ' from Permit ' || p3.Permitnumber || ' (' || p3.permitteename || ') to Permit ' || api.pkg_columnquery.Value(p1.PrimaryPermitObjectId, 'PermitNumber'));
                          t_Vehicles := t_Vehicles + 1;
                          t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ABC_Vehicle'));
                          t_RelId := api.pkg_relationshipupdate.New (t_EndpointId, p1.PrimaryPermitObjectId, t_ObjectId, sysdate);
                          api.pkg_columnupdate.setvalue(t_ObjectId, 'InsigniaNumber', api.pkg_columnquery.Value(v.vehicleobjectid, 'InsigniaNumber'));
                          api.pkg_columnupdate.setvalue(t_ObjectId, 'MakeModelYear', api.pkg_columnquery.Value(v.vehicleobjectid, 'makemodelyear'));
                          api.pkg_columnupdate.setvalue(t_ObjectId, 'OwnedOrLeasedLimousine', api.pkg_columnquery.Value(v.vehicleobjectid, 'ownedorleasedlimousine'));
                          api.pkg_columnupdate.setvalue(t_ObjectId, 'VIN', api.pkg_columnquery.Value(v.vehicleobjectid, 'vin'));
                          api.pkg_columnupdate.setvalue(t_ObjectId, 'Stateregistration', api.pkg_columnquery.Value(v.vehicleobjectid, 'Stateregistration'));
                          api.pkg_columnupdate.setvalue(t_ObjectId, 'Stateofregistration', api.pkg_columnquery.Value(v.vehicleobjectid, 'Stateofregistration'));
                          insert into possedba.issue9442_consolidatepermits2
                                 (OldPermitId, OldPermitNumber, NewPermitId, NewPermitNumber, OldVehicleId, VehicleId)
                          values
                                 (p3.PrimaryPermitObjectId, p3.Permitnumber, p1.PrimaryPermitObjectId, api.pkg_columnquery.Value(p1.PrimaryPermitObjectId, 'PermitNumber'), v.vehicleobjectid, t_ObjectId);
                       end loop;
                       -- Cancel 'old' Permit
                       t_Reason := '         Consolidated with Permit ' ||  api.pkg_columnquery.Value(p1.PrimaryPermitObjectId, 'PermitNumber');
                       dbms_output.put_line(t_Reason || '. ' || t_Vehicles || ' Vehicles copied');
                       t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
                       api.pkg_columnupdate.SetValue(p3.PrimaryPermitObjectId, 'State', 'Cancelled');
                       api.pkg_columnupdate.SetValue(p3.PrimaryPermitObjectId, 'Edit', 'N');
                       api.pkg_columnupdate.SetValue(p3.PrimaryPermitObjectId, 'SystemCancelled', 'Y');
                       api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation' , sysdate);
                       api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
                       t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, p3.PrimaryPermitObjectId, t_ObjectId, sysdate);
                 end loop;
                 exit;
           end loop;
        end if;
     end loop;     

  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  
  commit;

end;
/
select distinct oldpermitnumber, newpermitnumber, 
       api.pkg_columnquery.Value(r.newpermitid, 'PermitteeName') PermitteeName,
       api.pkg_columnquery.Value(r.newpermitid, 'PermitType') PermitType,
       api.pkg_columnquery.Value(r.oldpermitid, 'PermitteeName') PermitteeName,
       api.pkg_columnquery.Value(r.oldpermitid, 'PermitType') PermitType
  from possedba.issue9442_consolidatepermits2 r
  order by 1,2;