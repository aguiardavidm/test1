/*******************************************************************************************
 * Description: Initialize the new instructional text fields for petitions
 * Author: Jada Quon
 * Last Modified: Jan 21, 2021
 * Run-time: < 1 sec
 * Task Number: 092855
 ********************************************************************************************/
declare
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_definition.udt_StringList;

  t_NameofScript                        varchar2(4000) := 'Task92855_02_InitializePetitionInstructionText.sql';
  t_Schema                              varchar2(4000) := 'POSSEDBA';
  t_DatabaseName                        varchar2(4000);
  t_PrintDebug                          boolean := True;
  t_RowsProcessed                       pls_integer := 0;
  t_TimeStart                           timestamp := systimestamp;
  t_TimeEnd                             timestamp;
  t_TimeDiff                            float;

  t_ObjectId                            udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    if t_PrintDebug then
      dbms_output.put_line(a_Text);
    end if;

    -- pipe debug
    pkg_debug.putline(a_Text);
  end;

begin
  dbug('Starting Script: ' || t_NameOfScript || ' at: ' || to_char(t_TimeStart));

  if user != t_Schema then
    raise_application_error(-20000, 'Script must be run as ' || t_Schema || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  -- Set Petition Type text
  t_ObjectId := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionType', 'Name', '12.18');
  if t_ObjectId is not null then
    dbug('Updating 12.18 text...');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'OnlineDescription',
        '<font color="red">An expired license requires a Special Ruling pursuant to N.J.S.A. 33:1-12.18.</font>');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrUseLicense',
        'Click the Use button below to select the License that you are applying for Relief.');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrEndorsementDisclaimerText',
        'Read the below certification and follow the prompts to submit this application.</br></br>'
        || 'By checking the box below, and entering my full name in the following format (First Name Last Name), '
        || 'I hereby certify that I:</br>1. Have the authority to act on behalf of the licensee in this matter;</br>'
        || '2. Have reviewed the application;</br>3. Acknowledge everything to be correct and true to the best of my knowledge.');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrPaymentConfirmation',
        'Your 12.18 petition application has now been submitted to the Division. This application could take 4-6 weeks to obtain the special ruling.');
    t_RowsProcessed := t_RowsProcessed + 1;
  end if;

  t_ObjectId := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionType', 'Name', '12.39');
  if t_ObjectId is not null then
    dbug('Updating 12.39 text...');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'OnlineDescription',
        '<font color="red">An expired license requires a Special Ruling pursuant to N.J.S.A. 33:1-12.39.</font>');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrUseLicense',
        'Click the Use button below to select the License that you are applying for Relief.');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrEndorsementDisclaimerText',
        'Read the below certification and follow the prompts to submit this application.</br></br>'
        || 'By checking the box below, and entering my full name in the following format (First Name Last Name), '
        || 'I hereby certify that I:</br>1. Have the authority to act on behalf of the licensee in this matter;</br>'
        || '2. Have reviewed the application;</br>3. Acknowledge everything to be correct and true to the best of my knowledge.');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrPaymentConfirmation',
        'Your 12.39 petition application has now been submitted to the Division. This application could take 4-6 weeks to obtain the special ruling.');
    t_RowsProcessed := t_RowsProcessed + 1;
  end if;

  -- Set petition amendment type text
  t_ObjectId := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionAmendmentType', 'Name', 'Term Correction');
  if t_ObjectId is not null then
    dbug('Updating Term Correction text...');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'OnlineDescription',
        'A Term Correction amendment will allow you to corrrect the relief terms for a license.');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrUseLicense',
        'Click the Use button below to select the License that you are amending relief for.');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrTermCorrection',
        'Please enter the requested Term Corrections in the grid below.  Any changes made here are Pending until approved by the Division.');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrEndorsementDisclaimerText',
        'Read the below certification and follow the prompts to submit this application.</br></br>'
        || 'By checking the box below, and entering my full name in the following format (First Name Last Name), '
        || 'I hereby certify that I:</br>1. Have the authority to act on behalf of the licensee in this matter;</br>'
        || '2. Have reviewed the application;</br>3. Acknowledge everything to be correct and true to the best of my knowledge.');
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InstrPaymentConfirmation',
        'Your Term Correction request has now been submitted to the Division. This application could take 4-6 weeks to process.');
    t_RowsProcessed := t_RowsProcessed + 1;
  end if;

  -- Set System Settings text
  t_ObjectId := api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings', 'SystemSettingsNumber', 1);
  dbug('Updating System Settings text...');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PublicPetitionDraftsInst',
      'Below are Petitions that have been started but have not been submitted to the division...');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PublicPetitionReviewInst', 
      'Below are Petitions that have been submitted and are currently being reviewed by the division...');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PublicPetitionApprovedInst',
      'Below are Petition Terms that have been approved by the division and relief has been granted...');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PublicPetitionDeniedInst',
      'Below are Petitions that have been denied by the division and relief has not been granted...');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PetitionCurrentReliefTermText',
      'Below are all the requested Relief Terms for this License. If a Term is in the "Pending" or "Draft" status, '
      || 'then Relief is not yet approved by the Division.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PetitionRequestReliefTermText',
      'To request additional Terms for this License, please click the "Add Term" button below until '
      || 'you have the number of Terms you wish to apply for.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PetitionEstimatedFeesText',
      'Below is the estimated fee total for the Requested Terms. This fee must be paid as part of your application.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PetitionDocumentsPreamble',
      'Please upload any documents to be attached to this Relief Petition.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PetitionSubmissionText',
      'Either press the Pay Now button below to pay your fees and submit your Petition application for processing or '
      || 'press the Pay Later / Add to Pay All Fees button to pay fees and submit using Pay All Fees from the main menu.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PetitionDisclaimerText',
      'By checking the box below, you are certifying that the information that you are providing to the State of New Jersey '
      || 'is correct and accurate to the best of your knowledge.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PetitionPaymentConfirmation',
      'Your payment has been received and your Application has been successfully submitted for processing.');
  api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PetitionNoPaymentConfirmation',
      'Your Application has been successfully submitted for processing.');
  t_RowsProcessed := t_RowsProcessed + 1;
  
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

  t_TimeEnd := systimestamp;
  t_TimeDiff := (EXTRACT (DAY FROM t_TimeEnd-t_TimeStart) * 24 * 60 * 60
                      + EXTRACT (HOUR FROM t_TimeEnd-t_TimeStart) * 60 * 60
                      + EXTRACT (MINUTE FROM t_TimeEnd-t_TimeStart) * 60
                      + EXTRACT (SECOND FROM t_TimeEnd-t_TimeStart));
  dbug('');
  dbug('Rows Processed:  ' || t_RowsProcessed);
  dbug('Ending Script:   ' || t_NameOfScript || ' at: ' || to_char(t_TimeEnd));
  dbug('Run Time:        ' || t_TimeDiff || 's');
end;
/
