begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select objectid from query.d_CPLSpreadsheet) loop
    api.pkg_objectupdate.Verify(i.objectid);
  end loop;
  for i in (select objectid from query.j_abc_cplsubmissions) loop
    begin
      api.pkg_objectupdate.Verify(i.objectid);
    exception when others then
      dbms_output.put_line(sqlerrm);
    end;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;