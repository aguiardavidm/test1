rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- Created on 4/2/2019 by PAUL.ORSTAD 
declare 
  -- Local variables here
  t_Paymentids api.pkg_definition.udt_IdList;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_PaymentIds := api.pkg_search.ObjectsByColumnValue('o_Payment', 'PaymentDate', trunc(sysdate-90), sysdate);
  dbms_output.put_line('Applying instance security for ' || t_Paymentids.count || ' payments.');
  -- Reapply instance security for all payments made in the last 91 days
  for i in 1 .. t_Paymentids.count loop
    abc.pkg_instancesecurity.ApplyInstanceSecurity(t_Paymentids(i), sysdate);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;
/
