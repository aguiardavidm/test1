-- Created on 02/28/2018 by JAE.HOLDERBY
/*-----------------------------------------------------------------------------
 * Author: Jae Holderby
 * Expected run time: < 1 minute
 * Purpose: This script sets IsPermitReview to 'Y' on Process Types
 * MunicipalityReview and PoliceReview
 *---------------------------------------------------------------------------*/
declare
  -- Local variables here
  t_ObjectList   api.Pkg_Definition.udt_IdList;
  t_Counter      integer := 0;
begin
  -- Test statements here

  api.pkg_logicaltransactionupdate.ResetTransaction();
  --Select the job Id and Date to be inserted into the SubmittedDate column, where application proccesses are complete.
 select pt.ObjectId
 bulk collect into t_ObjectList
 from query.o_processtype pt
 where pt.Name in ('p_ABC_MunicipalityReview', 'p_ABC_PoliceReview');

 --Cycle through Jobs and set the value of Submitteddate to the Completed Date from the processess.
  for i in 1.. t_ObjectList.count loop
    api.pkg_columnupdate.SetValue(t_ObjectList(i), 'IsPermitReview', 'Y');

    t_Counter := t_Counter +1;

    if mod(t_Counter, 2000) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  dbms_output.put_line(t_Counter);
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
