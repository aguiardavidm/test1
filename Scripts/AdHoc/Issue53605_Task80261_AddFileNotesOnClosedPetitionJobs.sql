/*-----------------------------------------------------------------------------
 * Author: David Aguiar
 * Expected run time: < 5 sec
 * Purpose: This script adds File Notes for Petition jobs specified by NJ ABC on issue
 *   53605.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  
  t_JobIds                              udt_IdList;
  t_NoteDefId                           udt_Id;
  t_NoteId                              udt_Id;
  t_Text                                varchar2(100) := 'System Cancelled by script for issue 53605.';
  t_Tag                                 varchar2(50) := 'GEN';
  
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  select j.JobId
  bulk collect into t_JobIds
  from
    api.jobs j
    join api.jobtypes jt
        on j.JobTypeId = jt.JobTypeId
  where jt.Name = 'j_ABC_Petition'
    and j.ExternalFileNum in (
        '418', '1783', '1133', '1641', '4347', '4399', '4476', '4518', '4866', '4924', '4718',
        '4743', '4752', '4763', '3910', '4050', '4223', '4242', '4272', '4275', '4315', '3456',
        '3720', '3736', '3284', '6678', '7222', '6326', '6329', '6339', '6342', '6361', '6408',
        '6412', '6439', '6448', '6505', '6507', '6509', '6532', '6551', '6552', '6572', '5998',
        '6050', '6091', '6174', '6189', '4987', '4994', '5078', '5338', '5518', '8880', '8893',
        '8895', '8933', '7733', '8971', '8977', '8984', '8987', '8988', '8997', '9017', '9020',
        '9021', '9026', '9030', '7809', '9032', '9033', '9034', '9035', '9036', '9037', '9038',
        '7894', '7912', '7959', '8029', '8030', '8143', '8201', '8280', '8406', '7302', '7394',
        '8665', '8666', '8702', '8703', '8704', '8744', '8789', '8820', '8853', '8854'
        );
  
  for i in 1..t_JobIds.count() loop
      select nd.NoteDefId
      into t_NoteDefId
      from api.NoteDefs nd
      where nd.Tag = t_Tag;

      t_NoteId := api.pkg_NoteUpdate.New(t_JobIds(i), t_NoteDefId, 'N', t_Text);
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;  
end;
