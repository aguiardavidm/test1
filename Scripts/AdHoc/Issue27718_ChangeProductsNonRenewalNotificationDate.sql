-- Created on 07/21/2017 by Dennis.OConnor 
-- Modify products NonRenewalNoticeDate to be standard date as set by NJ-ABC
declare
  t_SystemObjectId         pls_integer;

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  dbms_output.enable(NULL);

      SELECT  s.ObjectId
      into t_SystemObjectId
			FROM query.o_systemsettings s;

      api.pkg_columnupdate.SetValue(t_SystemObjectId, 'PRNonRenewalNoticeDay' , '1');
      api.pkg_columnupdate.SetValue(t_SystemObjectId, 'PRNonRenewalNoticeMonth' , 'February');

    for p in (SELECT  p.ObjectId
      FROM query.o_abc_product p
      WHERE p.IsProductActive = 'Y')
      loop
       api.pkg_columnupdate.SetValue(p.ObjectId, 'NonRenewalNoticeDate' , to_date('02/01/2018', 'MM/DD/YYYY'));
       dbms_output.put_line ('Updating NonRenewalNoticeDate for ' || api.pkg_columnquery.Value (p.objectid,'RegistrationNumber') || '-' || api.pkg_columnquery.Value (p.objectid,'Name'));
         end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;  
end;