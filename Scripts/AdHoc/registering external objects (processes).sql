/* This script registers processes that have been created.
All that needs to be done is to add the name of the new process
to the process list and run the script*/

declare

 i int;

begin

  api.pkg_LogicalTransactionUpdate.ResetTransaction();

  for i in (select pt.ProcessTypeId
              from api.processtypes pt
             where pt.Name in ('p_ABC_AdministrativeReview',
                               'p_ABC_PRAdministrativeReview',
                               'p_ABC_ApproveRejectApplication',
                               'p_ABC_InitiateInspection',
                               'p_ABC_CloseInspection',
                               'p_ABC_ConductInspection',
                               'p_ABC_DraftNonRenewalNotice',
                               'p_ABC_GenSubAppSummaryReport',
                               'p_ABC_PREnterApplication',
                               'p_ABC_InitiateAccusation',
                               'p_ABC_ManagePetition',
                               'p_ABC_MonitorHold',
                               'p_ABC_Notification',
                               'p_ABC_NotifyApplicantRejection',
                               'p_ABC_PRPaymentProcessing',
                               'p_ABC_PaymentProcessing',
                               'p_ABC_PenalizeLicense',
                               'p_ABC_ReceiveOnlineApplication',
                               'p_ABC_RecordPetition',
                               'p_ABC_RecordAndExecuteDecision',
                               'p_ABC_RegisterAccusationPkg',
                               'p_ABC_ReleasePrintBatch',
                               'p_ABC_Reminder',
                               'p_ABC_ReviewApplication',
                               'p_ABC_ReviewOnlineRenewal',
                               'p_ABC_ReviewPetition',
                               'p_ABC_ReviewStatusOfNonRenewal',
                               'p_ABC_RevokeLicense',
                               'p_ABC_SendLicense',
                               'p_ABC_SubmitApplication',
                               'p_ABC_SubmitComplaint',
                               'p_ABC_SubmitResolution',
                               'p_ABC_SuspendLicense',
                               'p_ABC_SystemProcess',
                               'p_ABC_TakeExpirationActions',
                               'p_ABC_TakeReactivationActions')
            minus
            select ProcessTypeId
              from api.ProcessTypes) loop


  api.pkg_Objectupdate.RegisterExternalObject('o_ProcessType', i.ProcessTypeId);

  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;