-- Created on 11/15/2017 by MICHEL.SCHEFFERS
-- Issue 34261 - Remove Legal Entity - Renewal Cycle relationship
declare 
  -- Local variables here
  i integer;
  l integer := 0;
  q integer;
begin
  -- Test statements here
  dbms_output.enable(null);
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  select prc.ObjectId
  into   i
  from   query.o_ABC_ProductRenewalCycle prc  
  where  prc.RenewalCycle = 2017;

  for r in (select *
            from   query.r_ABC_RegistrantProdRenewCycle r1
            where  r1.ProductRenewalCycleObjectId = i
           ) loop
     begin
        select a.notificationjobid
        into   q
        from   abc.registrantnotifications_t a
        where  a.registrantobjectid = r.registrantobjectid
        and    a.renewalcycleobjectid = i;
     exception
        when no_data_found then
           dbms_output.put_line ('Removing relationship for Legal Entity ' || r.registrantobjectid);
           l := l + 1;
           api.pkg_relationshipupdate.Remove(r.relationshipid);
     end;
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
  dbms_output.put_line (l || ' Legal Entities removed');
end;
