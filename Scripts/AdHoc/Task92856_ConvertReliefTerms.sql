/*******************************************************************************************
 * Description: Converts existing petition terms objects to fit the new model following
 *   the Petition enhancements
 * Author: Jada Quon
 * Last Modified: Jan 21, 2021
 * Run-time: 
 * Task Number: 092856
 ********************************************************************************************/
declare
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_definition.udt_StringList;

  t_NameofScript                        varchar2(4000) := 'Task92856_ConvertReliefTerms.sql';
  t_Schema                              varchar2(4000) := 'POSSEDBA';
  t_DatabaseName                        varchar2(4000);
  t_PrintDebug                          boolean := True;
  t_RowsProcessed                       pls_integer := 0;
  t_TimeStart                           timestamp := systimestamp;
  t_TimeEnd                             timestamp;
  t_TimeDiff                            float;

  type udt_TermRecord is record
  (
    ObjectId                            udt_Id,
    StartYear                           number(4),
    PetitionTypeId                      udt_Id,
    JobId                               udt_Id
  );

  type udt_Terms is table of udt_TermRecord
    index by PLS_INTEGER;

  t_1218PetitionTypeId                  udt_Id;
  t_1239PetitionTypeId                  udt_Id;
  t_Count                               number := 0;
  t_JobPetitionTypeEndPointId           udt_Id;
  t_JobPetitionTypeId                   udt_Id;
  t_RelationshipId                      udt_Id;
  t_Status                              varchar2(6);
  t_Terms                               udt_Terms;
  t_TermTypeEndPointId                  udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin
    if t_PrintDebug then
      dbms_output.put_line(a_Text);
    end if;

    -- pipe debug
    pkg_debug.putline(a_Text);
  end;

  procedure RelateLicense (
    a_PetitionTermId                    udt_Id,
    a_JobId                             udt_Id default null
  ) is
    t_MasterLicenseId                   udt_Id;
    t_LicenseId                         udt_Id;
    t_LicenseTermEndPoint               udt_Id;
  begin

    t_LicenseTermEndPoint := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'PetitionTerm');

    -- find the license based on the job
    if a_JobId is not null then
      begin
        select LicenseId
        into t_LicenseId
        from query.r_abc_LicensePetition
        where PetitionId = a_JobId;
      exception
        when no_data_found then
          dbug('No license found related to job. Could not relate a license to term ' || a_PetitionTermId);
      end;
    else
      dbug('No job found related to term ' || a_PetitionTermId);
    end if;

    if t_LicenseId is not null then
      if api.pkg_ColumnQuery.Value(t_LicenseId, 'IsLatestVersion') = 'N' then
        t_MasterLicenseId := api.pkg_ColumnQuery.NumericValue(t_LicenseId, 'MasterLicenseObjectId');
        begin
          select l.ObjectId
          into t_LicenseId
          from 
            query.r_ABC_MasterLicenseLicense r
            join query.o_ABC_License l
                on l.ObjectId = r.LicenseObjectId
          where r.MasterLicenseObjectId = t_MasterLicenseId
            and l.IsLatestVersion = 'Y'
            and l.State = 'Active';
        exception
          when no_data_found then
            dbug('Could not find latest version of license to relate term ' || a_PetitionTermId);
          when too_many_rows then
            dbug('Multiple licenses found marked as latest version. Could not relate term ' || a_PetitionTermId);
        end;
      end if;

      -- Relate to available license (even if not latest version)
      t_RelationshipId := api.pkg_RelationshipUpdate.New(
        t_LicenseTermEndPoint, t_LicenseId, a_PetitionTermId);
    end if;

  end RelateLicense;

begin
  dbug('Starting Script: ' || t_NameOfScript || ' at: ' || to_char(t_TimeStart));

  if user != t_Schema then
    raise_application_error(-20000, 'Script must be run as ' || t_Schema || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  t_1218PetitionTypeId := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionType', 'Name', '12.18');
  t_1239PetitionTypeId := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionType', 'Name', '12.39');

  if t_1218PetitionTypeId is null or t_1239PetitionTypeId is null then
    raise_application_error(-20000, 'Expected petition type meta data objects not found. '
        || 'Please ensure both 12.18 and 12.39 petition type objects are created before running this script.');
  end if;

  t_TermTypeEndPointId := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_PetitionTerm', 'PetitionType');

  select
    ObjectId,
    extract(year from StartDate),
    case zzzPetitionType
      when '12.18' then 
        t_1218PetitionTypeId
      when '12.39' then 
        t_1239PetitionTypeId 
    end,
    rp.PetitionObjectId
  bulk collect into t_Terms
  from query.o_ABC_PetitionTerm pt
  left outer join query.r_ABC_PetitionPetitionTerm rp
      on rp.PetitionTermObjectId = pt.ObjectId
  where zzzPetitionType is not null
    and StartDate is not null
    and PetitionTypeObjectId is null
    and pt.ObjectDefTypeId = 1;

  for i in 1..t_Terms.count loop
    t_RelationshipId := api.pkg_RelationshipUpdate.New(
        t_TermTypeEndPointId, t_Terms(i).ObjectId, t_Terms(i).PetitionTypeId);
    api.pkg_ColumnUpdate.SetValue(t_Terms(i).ObjectId, 'TermStartYear', t_Terms(i).StartYear);

    if t_Terms(i).JobId is not null then
      api.pkg_ColumnUpdate.SetValue(t_Terms(i).ObjectId, 'CreatedByJobId', t_Terms(i).JobId);
      t_Status := api.pkg_ColumnQuery.Value(t_Terms(i).JobId, 'StatusName');

      if t_Status in ('NEW', 'REVIEW') then
        api.pkg_ColumnUpdate.SetValue(t_Terms(i).Objectid, 'State', 'Pending');
        t_JobPetitionTypeEndPointId := api.pkg_ConfigQuery.EndPointIdForName('j_ABC_Petition', 'PetitionType');
        select count(RelationshipId)
        into t_Count
        from api.relationships
        where FromObjectId = t_Terms(i).JobId
          and EndPointId = t_JobPetitionTypeEndPointId;

        if t_Count = 0 then
          t_RelationshipId := api.pkg_RelationshipUpdate.New(
              t_JobPetitionTypeEndPointId, t_Terms(i).JobId, t_Terms(i).PetitionTypeId);
        else
          t_JobPetitionTypeId := api.pkg_ColumnQuery.NumericValue(t_Terms(i).JobId, 'PetitionTypeId');
          if t_JobPetitionTypeId != t_Terms(i).PetitionTypeId then
            dbug('Petition type on term ' || t_Terms(i).ObjectId || ' does not match petition type on Petition job ' 
                || api.pkg_ColumnQuery.Value(t_Terms(i).JobId, 'ExternalFileNum')
                || '. Please rectify/remove the term.');
          end if;
        end if;
      elsif t_Status in ('CANCEL', 'DENY') then
        api.pkg_ColumnUpdate.SetValue(t_Terms(i).Objectid, 'State', 'Cancelled');
      else
        api.pkg_ColumnUpdate.SetValue(t_Terms(i).Objectid, 'State', 'Granted');
        RelateLicense(t_Terms(i).ObjectId, t_Terms(i).JobId);
      end if;
    end if;

    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(i, 500) = 0 then
      api.pkg_LogicalTransactionUpdate.EndTransaction();
      commit;
    end if;
  end loop;

  -- Skip post-verify (API break)
  objmodelphys.pkg_EventQueue.EndSession('N');
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

  t_TimeEnd := systimestamp;
  t_TimeDiff := (EXTRACT (DAY FROM t_TimeEnd-t_TimeStart) * 24 * 60 * 60
              + EXTRACT (HOUR FROM t_TimeEnd-t_TimeStart) * 60 * 60
              + EXTRACT (MINUTE FROM t_TimeEnd-t_TimeStart) * 60
              + EXTRACT (SECOND FROM t_TimeEnd-t_TimeStart));
  dbug('');
  dbug('Rows Processed:  ' || t_RowsProcessed);
  dbug('Ending Script:   ' || t_NameOfScript || ' at: ' || to_char(t_TimeEnd));
  dbug('Run Time:        ' || t_TimeDiff || 's');
end;
/
