-- Created on 4/1/2016 by MICHEL.SCHEFFERS 
-- Apply Instance Security on Petition jobs
-- Task 9320
declare 
  -- Local variables here
  t_Jobs integer := 0;

begin 
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  -- Loop through all Permit Application jobs
  for i in (select ObjectId
              from query.j_ABC_Petition
            ) loop
    -- Apply Instance Security for each job
    t_Jobs := t_Jobs + 1;
    abc.pkg_InstanceSecurity.ApplyInstanceSecurity(i.ObjectId, sysdate);
  end loop;
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbms_output.put_line('Petition Jobs updated: ' || t_Jobs);
end;