/* Author: Michel Scheffers
   Description: Create/Update Fees for Insignia Permit Renewal job
   Issue 
   Execution time: < 5 minutes
*/
declare
  subtype            udt_Id is api.pkg_definition.udt_Id;
  a_FeeSchedule      varchar2 (100) := 'ABC Fee Schedule';
  a_ApplicationJob   varchar2 (100) := 'j_ABC_PermitApplication';
  a_ApplicationTitle varchar2 (100) := 'Permit Application Job';
  a_RenewalJob       varchar2 (100) := 'j_ABC_PermitRenewal';
  a_RenewalTitle     varchar2 (100) := 'Permit Renewal Job';
-- Object IDs  
  t_FSObjId          udt_Id; -- FeeSchedule Object Id
  t_FDObjId          udt_Id; -- FeeDefinition Object Id
  t_FEObjId          udt_Id; -- FeeElement Object Id
  t_FLObjId          udt_Id; -- FieldElement Object Id
  t_GLObjId          udt_Id; -- GLAccount Object Id
  t_DSObjIdAppl      udt_id; -- Application Job Object Id
  t_DSObjIdRnw       udt_id; -- Renewal Job Object Id
-- Relationship/Endpoint IDs
  t_FSEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FeeSchedule', 'FeeDefinition');
  t_FDEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FeeSchedule', 'DataSource');
  t_DSEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FeeDefinition', 'DataSource');
  t_EDEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'DataSource');  
  t_EFEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'FeeSchedule');  
  t_LSEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FieldElement', 'FeeSchedule');  
  t_LEEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_FieldElement', 'FeeElement');  
  t_RelationshipId   udt_id;
  t_FeeElementId     udt_id;
  t_FieldElementId   udt_id;
  t_FeeScheduleId    udt_id;
  t_DataSourceId     udt_id;
  t_FeeDefinitionId  udt_id;
-- Counters
  t_FDRecords        number := 0;
  t_FERecords        number := 0;
  t_FLRecords        number := 0;

-- PROCEDURES --
procedure dbug (
    a_Text    varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
end;

procedure CreateFD(a_Amount                     varchar2,
                   a_Condition                  varchar2,
                   a_EffectiveDateColumnName    varchar2,
                   a_FeeDescription             varchar2,
                   a_Description                varchar2,
				           a_GLAccount					        varchar2,
                   a_DSObjId                    udt_id,
                   a_FSObjId                    udt_id) is
  t_RelationshipId udt_Id; 
  
begin
  t_GLObjId      := api.pkg_search.ObjectByColumnValue('o_glaccount', 'Code', a_GLAccount);
  if t_GLObjId is null then
  --Error, GL Account should be present
      api.pkg_Errors.RaiseError(-20000, '!!!!**** G/L Account - ' || a_GLAccount || ' should be present before you can continue ****!!!!');
  end if;

  begin
    select fd.objectid
      into t_FDObjId
      from query.o_FeeDefinition fd
      join query.r_Feedef_Feeschedule fdfs on fdfs.FeeDefinitionObjectId = fd.objectid
     where fd.FeeDescription  = a_FeeDescription
       and fdfs.FeeScheduleId = a_FSObjId;
     exception
       when no_data_found then
         t_FDObjId := null;
  end;
  if t_FDObjId is null then
  --Create new object of FeeDefinition
     feescheduleplus.pkg_PosseExternalRegistration.RegisterFeeDefinition (t_FeeDefinitionId, -- returned from call
                                                                          a_Description, -- short description of Fee Definition
                                                                          a_Condition, -- condition for which the fee will be calculated
                                                                          a_Amount, -- Fee Amount
                                                                          a_EffectiveDateColumnName, -- Effective Date column name
                                                                          a_FeeDescription, -- description of Fee Definition
                                                                          sysdate, -- Created Date
                                                                          'POSSEDBA', -- Created By
                                                                          sysdate, -- Updated Date
                                                                          'POSSEDBA', -- Updated By
                                                                          api.pkg_columnquery.NumericValue(a_FSObjId,'FeeScheduleId'), -- related FeeSchedule Id
                                                                          api.pkg_columnquery.NumericValue(a_DSObjId,'DataSourceId'), -- related Data Source Id
                                                                          null, --related Fee Category Object Id (not used by NJ)
                                                                          a_GLAccount, -- GL Account
                                                                          'Default', -- Reponsible Party type
                                                                          null -- related Responsible Party Id
                                                                          );
     t_FDObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeDefinition',t_FeeDefinitionId);
     t_FDRecords := t_FDRecords + 1;
  --Create relationship from FeeSchedule to FeeDefinition
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FSEndpointId, a_FSObjId, t_FDObjId);
  --Create relationship from FeeDefinition to DataSource
     t_RelationshipId := api.pkg_relationshipupdate.New(t_DSEndpointId, t_FDObjId, a_DSObjId);
     dbug (a_FeeDescription);
  end if;
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Amount', a_Amount);
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Condition', a_Condition);
  api.pkg_columnupdate.SetValue(t_FDObjId, 'EffectiveDateColumnName', a_EffectiveDateColumnName); 
  api.pkg_columnupdate.SetValue(t_FDObjId, 'FeeDescription', a_FeeDescription); 
  api.pkg_columnupdate.SetValue(t_FDObjId, 'Description', a_Description); 

end;

procedure CreateFE(a_Datatype                varchar2,
                   a_Name                    varchar2,
                   a_Job                     varchar2,
                   a_DSObjId                 udt_id,
                   a_FSObjId                 udt_id) is
  t_RelationshipId udt_Id;
  
begin
  --Find FeeElement
  begin
    select fe.objectid
    into t_FEObjId
    from query.o_feeelement fe
    join query.r_DataSource_FeeElement dsfe on dsfe.FeeElementId = fe.ObjectId
    join query.o_datasource ds on ds.ObjectId = dsfe.DataSourceId
    join query.r_FeeSchedule_FeeElement fsfe on fsfe.FeeElementObjectId = fe.objectid
    join query.o_Feeschedule fs on fs.objectid = fsfe.FeeScheduleObjectId
   where fe.GlobalName    = a_Name
     and ds.PosseViewName = a_Job
     and fs.ObjectId      = a_FSObjId;
  exception
     when no_data_found then
       t_FEObjId := null;
  end;
  if t_FEObjId is null then
  --Create new object of FeeElement
     feescheduleplus.pkg_PosseExternalRegistration.RegisterFeeElement (t_FeeElementId, -- returned from call
                                                                       a_DataType, -- type of Data (String, Boolean etc)
                                                                       'Field', -- type of Element (Field or Expression)
                                                                       sysdate, -- Created Date
                                                                       'POSSEDBA', -- Created By
                                                                       sysdate, -- Updated Date
                                                                       'POSSEDBA', -- Updated By
                                                                       null, -- isGlobal (not used)
                                                                       a_Name, -- name of Fee Element
                                                                       api.pkg_columnquery.NumericValue(a_FSObjId,'FeeScheduleId'),-- related FeeSchedule Id
                                                                       api.pkg_columnquery.NumericValue(a_DSObjId,'DataSourceId')-- related Data Source Id
                                                                       );
     t_FEObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeElement',t_FeeElementId);
     t_FERecords := t_FERecords + 1;
  --Create relationship from FeeElement to FeeSchedule
     t_RelationshipId := api.pkg_relationshipupdate.New(t_EFEndpointId, t_FEObjId, a_FSObjId);
  --Create relationship from FeeElement to DataSource
     t_RelationshipId := api.pkg_relationshipupdate.New(t_EDEndpointId, t_FEObjId, a_DSObjId);
     dbug ('FeeElement ' || a_Name || ' for ' || a_Job);     
  end if;
  api.pkg_columnupdate.SetValue(t_FEObjId, 'ColumnName', a_Name); -- Posse Column Name

  --Find FieldElement
  begin
    select fe.objectid
    into t_FLObjId
    from query.o_fieldelement fe
    join query.r_FeeElement_FieldElement fefe on fefe.FieldElementId = fe.ObjectId
    join query.r_DataSource_FeeElement dsfe on dsfe.FeeElementId = fefe.FeeElementId
    join query.r_FeeSchedule_FieldElement fsfe on fsfe.FieldElementId = fe.ObjectId
    join query.o_datasource ds on ds.ObjectId = dsfe.DataSourceId
   where fe.ColumnName      = a_Name
     and ds.PosseViewName   = a_Job
     and fsfe.FeeScheduleId = a_FSObjId;
  exception
     when no_data_found then
       t_FLObjId := null;
  end;
  if t_FLObjId is null then
  --Create new object of FieldElement
     feescheduleplus.pkg_PosseExternalRegistration.RegisterFieldElement (t_FieldElementId, -- returned from call
                                                                         a_Name, -- name of Field Element
                                                                         api.pkg_columnquery.NumericValue(a_FSObjId,'FeeScheduleId'),-- related FeeSchedule Id
                                                                         api.pkg_columnquery.NumericValue(t_FEObjId,'FeeElementId'),-- related FeeElement Id
                                                                         sysdate, -- Created Date
                                                                         'POSSEDBA', -- Created By
                                                                         sysdate, -- Updated Date
                                                                         'POSSEDBA' -- Updated By
                                                                         );
     t_FLObjId := api.pkg_objectupdate.RegisterExternalObject('o_FieldElement',t_FieldElementId);
     t_FLRecords := t_FLRecords + 1;
  --Create relationship from FieldElement to FeeSchedule
     t_RelationshipId := api.pkg_relationshipupdate.New(t_LSEndpointId, t_FLObjId, a_FSObjId);
  --Create relationship from FieldElement to DataSource
     t_RelationshipId := api.pkg_relationshipupdate.New(t_LEEndpointId, t_FLObjId, t_FEObjId);
--     dbug ('FieldElement ' || a_Name || ' for ' || a_Job);     
  end if;

end;

-- MAIN PROCESS --
begin
  dbug('*** Start Fee script for ' || a_FeeSchedule || '. ***');
  api.pkg_logicaltransactionupdate.ResetTransaction();
  if user != 'POSSEDBA' then
    raise_application_error(-20000, '*** Script must be run as POSSEDBA. ***');
  end if;

  t_FSObjId := api.pkg_search.ObjectByColumnValue('o_FeeSchedule', 'Description', a_FeeSchedule);
  if t_FSObjId is null then
  --Create a new object of FeeSchedule
     feescheduleplus.pkg_posseexternalregistration.RegisterFeeSchedule(t_FeeScheduleId, -- returned from call
                                                                       a_FeeSchedule, -- Fee schedule description
                                                                       to_date ('20150101','yyyymmdd'), -- Effective Start Date
                                                                       to_date ('39991231','yyyymmdd'), -- Effective End Date
                                                                       sysdate, -- Created Date
                                                                       'POSSEDBA', -- Created By
                                                                       sysdate, -- Updated Date
                                                                       'POSSEDBA' -- Updated By
                                                                       );
     t_FSObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeSchedule',t_FeeScheduleId);
     dbug ('Added FeeSchedule ' || t_FSObjId || ' - ' || a_FeeSchedule);
  end if;

  -- Checking DataSource  
  --Application Job
  begin
     select ds.objectid 
       into t_DSObjIdAppl
       from query.r_DataSource_FeeSchedule dsfs 
       join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
      where dsfs.FeeScheduleId = t_FSObjId
        and ds.PosseViewName = a_ApplicationJob;
     exception
       when no_data_found then
         t_DSObjIdAppl := null;
  end;  
  if t_DSObjIdAppl is null then  
     feescheduleplus.pkg_posseexternalregistration.RegisterDataSource (t_DataSourceId, -- returned from call
                                                                       a_ApplicationTitle, -- Data Source description
                                                                       a_ApplicationJob, -- Posse Name
                                                                       api.pkg_columnquery.NumericValue(t_FSObjId,'FeeScheduleId') -- related Fee Schedule Id
                                                                       );
     t_DSObjIdAppl := api.pkg_objectupdate.RegisterExternalObject('o_DataSource',t_DataSourceId);
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FDEndpointId, t_FSObjId, t_DSObjIdAppl);
     dbug ('Added DataSource ' || t_DSObjIdAppl || ' - ' || a_ApplicationJob);
  end if;
  --Renewal Job
  begin
     select ds.objectid 
       into t_DSObjIdRnw
       from query.r_DataSource_FeeSchedule dsfs 
       join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
      where dsfs.FeeScheduleId = t_FSObjId
        and ds.PosseViewName = a_RenewalJob;
     exception
       when no_data_found then
         t_DSObjIdRnw := null;
  end;  
  if t_DSObjIdRnw is null then  
     feescheduleplus.pkg_posseexternalregistration.RegisterDataSource (t_DataSourceId, -- returned from call
                                                                       a_RenewalTitle, -- Data Source description
                                                                       a_RenewalJob, -- Posse Name
                                                                       api.pkg_columnquery.NumericValue(t_FSObjId,'FeeScheduleId') -- related Fee Schedule Id
                                                                       );
     t_DSObjIdRnw := api.pkg_objectupdate.RegisterExternalObject('o_DataSource',t_DataSourceId);
     t_RelationshipId := api.pkg_relationshipupdate.New(t_FDEndpointId, t_FSObjId, t_DSObjIdRnw);
     dbug ('Added DataSource ' || t_DSObjIdAppl || ' - ' || a_RenewalJob);
  end if;
  
  dbug ('*** Updating FeeDefinitions - Insignia Type permits.*** ');
  -- Limited Transportation Insignia
  CreateFD ('=75 * {TotalNumberofVehicles}',
            '={PermitTypeCode}=''LTC''',
            'CreatedDate',
            'Limited Transportation Insignia Renewal Fee',
            'Limited Transportation Insignia Renewal Fee',
--           1234567890123456789012345678901234567890123456789
			      'LTC',
            t_DSObjIdRnw,
            t_FSObjId);

  -- Transit Insignia
  CreateFD ('=50 * {TotalNumberofVehicles}',
            '={PermitTypeCode}=''TI''',
            'CreatedDate',
            'Transit Insignia Renewal Fee',
            'Transit Insignia Renewal Fee',
--           1234567890123456789012345678901234567890123456789
			      'TI',
            t_DSObjIdRnw,
            t_FSObjId);

  -- Transit License Insignia
  CreateFD ('=30 * {TotalNumberofVehicles}',
            '={PermitTypeCode}=''TLI''',
            'CreatedDate',
            'Transit License Insignia Renewal Fee',
            'Transit License Insignia Renewal Fee',
--           1234567890123456789012345678901234567890123456789
			      'TLI',
            t_DSObjIdRnw,
            t_FSObjId);
			
  dbug ('FeeElement records added:    ' || t_FERecords);              
  dbug ('FieldElement records added:  ' || t_FLRecords);              
  dbug ('FeeDefinition records added: ' || t_FDRecords);              

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  dbug('*** End Fee script for ' || a_FeeSchedule || '. ***');
end;
