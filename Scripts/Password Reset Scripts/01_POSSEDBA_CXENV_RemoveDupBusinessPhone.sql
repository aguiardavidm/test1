/*-----------------------------------------------------------------------------
 * Author: Chris Phillips (2019)
 * Expected run time: <30 sec
 * Purpose: Remove Duplicate BusinessPhone numbers from CXNJDEV and CXNJTEST
 *    by generating new phone numbers
 *---------------------------------------------------------------------------*/
declare

  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_SearchResults is api.pkg_Definition.udt_SearchResults;

  -- Declare Local variables here
  t_RowsProcessed                       pls_integer := 0;
  t_ScriptDescription                   varchar2(4000)
      := 'Task 073090 : Admin - Existing User presentation updates';
  t_LTId                                udt_Id;
  t_NotifyMunicipalityColDefId          udt_Id;
  t_Cnt                                 number;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

begin

  dbug('Starting script ' || t_ScriptDescription);
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  t_LTId := api.pkg_LogicalTransactionUpdate.CurrentTransaction;
  --rollback;

  for i in (
      select 
        ObjectId
      from query.u_Users
      where BusinessPhone is not null
    ) loop

      api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'BusinessPhone', '208416' || to_char(t_RowsProcessed, 'fm0000'));
      t_RowsProcessed := t_RowsProcessed + 1;

  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
  dbug('Rows Processed: ' || t_RowsProcessed);
  dbug('Completed script ' || t_ScriptDescription);

  --Confirmation Queries
  /*
  --Confirm count is 0
  select 
    count(*),
    BusinessPhone
  from query.u_Users
  where BusinessPhone is not null
  group by BusinessPhone
  having count(*) > 1;

  select 
    BusinessPhone
  from query.u_Users
  where BusinessPhone is not null;
  */
  
end;
/