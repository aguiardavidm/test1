--run as POSSEDBA
/* This script registers Emails that have not been registered yet */
declare
  t_RunAsUser   varchar2(30) := 'POSSEDBA';
begin
  if sys_context('userenv', 'current_schema') != t_RunAsUser then
    raise_application_error(-20006, 'Please run script as ' || t_RunAsUser || '.');
  end if;
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  for i in (select e.ObjectId
              from extension.emails e
            minus
            select q.ObjectId
              from query.o_email q) loop
    api.pkg_Objectupdate.RegisterExternalObject('o_Email', i.ObjectId);
  
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  end loop;
  commit;
end;