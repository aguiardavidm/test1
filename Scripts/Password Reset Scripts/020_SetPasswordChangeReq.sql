declare
  t_UserIds api.Pkg_Definition.udt_IdList;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  if user != 'POSSEDBA' then
    api.pkg_errors.raiseerror(-20000, 'Script must be run as POSSEDBA');
  end if;

  select objectid
    bulk collect into t_UserIds
    from query.u_users u
   where u.UserType in ('Public', 'Municipal', 'Police')
     and u.AuthenticationName != 'ABCWEBGUEST';
  
  for i in 1..t_UserIds.count loop
    api.pkg_columnupdate.SetValue(t_UserIds(i), 'PasswordChangeRequired', 'Y');
    api.pkg_columnupdate.SetValue(t_UserIds(i), 'PasswordChangeFirstTimeLogin', 'Y');
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
