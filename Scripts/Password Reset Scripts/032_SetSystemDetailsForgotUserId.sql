declare
  t_RunAsUser   varchar2(30) := 'POSSEDBA';
  t_noteid number;
  t_html   varchar2(4000);
begin
  if sys_context('userenv', 'current_schema') != t_RunAsUser then
    raise_application_error(-20006, 'Please run script as ' || t_RunAsUser || '.');
  end if;
  t_html := '<font face="tahoma, arial, verdana, sans-serif"><span style="font-size: 12px;"><img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"></span></font><br><br><font face="tahoma, arial, verdana, sans-serif"><span style="font-size: 12px;">A User ID recovery was triggered for your New Jersey Division of Alcoholic Beverage Control account. Please use this User Id to log into your account.</span></font><br><br>Your User ID is: {UserId}<br><br><span style="font-family: arial;">If you have any questions or concerns, please contact the Division at POSSEAdmin@lps.state.nj.us or by phone at 609-984-2830.</span>';
  select noteid into t_noteid 
    from api.notes n
  join api.notedefs d 
    on d.NoteDefId = n.notedefid
  where tag = 'FUIE';
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  for i in (select objectid from query.o_systemsettings where rownum = 1) loop
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'ForgotUserIdEmailSubject','Online ABC - Forgot User Id');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'EmailAddressRecoveryInstText','Please enter the cell phone number on your account.');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'EmailAddressRecoveryConfText','A text message with recovery details has been sent your cell phone.');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'UserIdRecoveryInstText','Please select the method you would like to use to recover your account.');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'UserIdRecoveryConfText','A message has been sent with recovery details.');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'CellPhoneDisclaimer','By entering a Cell Phone number you are agreeing to allow NJ ABC to send you text messages with information to recover your account. Messages / data rates may apply.');
    api.pkg_noteupdate.Modify(t_noteid, 'N', t_html);
	end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;
