declare
  t_RunAsUser   varchar2(30) := 'POSSEDBA';
  t_pubnoteid number;
  t_muninoteid number;
  t_polnoteid number;
  t_html   varchar2(4000);
begin
  if sys_context('userenv', 'current_schema') != t_RunAsUser then
    raise_application_error(-20006, 'Please run script as ' || t_RunAsUser || '.');
  end if;
  t_html := '<img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"><br><br><div style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><span style="font-size: 12px;"><font face="arial">The password for your NJ-ABC account has been changed.&nbsp;</font></span></div><font face="arial" style="font-size: medium;"><br></font><div style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><span style="font-size: 12px;"><font face="arial">If you have any questions or concerns, please contact the Division at POSSEAdmin@lps.state.nj.us or by phone at 609-984-2830.</font></span></div><br>';
  select noteid into t_pubnoteid 
    from api.notes n
  join api.notedefs d 
    on d.NoteDefId = n.notedefid
  where tag = 'PWREML';

  select noteid into t_muninoteid 
    from api.notes n
  join api.notedefs d 
    on d.NoteDefId = n.notedefid
  where tag = 'MPWREM';

  select noteid into t_polnoteid 
    from api.notes n
  join api.notedefs d 
    on d.NoteDefId = n.notedefid
  where tag = 'PPWREM';
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  for i in (select objectid from query.o_systemsettings where rownum = 1) loop
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'ResetEmailSubject','Online ABC - Password Reset');
    api.pkg_noteupdate.Modify(t_pubnoteid, 'N', t_html);

    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'MuniPwdResetEmailSubject','Online ABC - Password Reset');
    api.pkg_noteupdate.Modify(t_muninoteid, 'N', t_html);

    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'PolicePwdResetEmailSubject','Online ABC - Password Reset');
    api.pkg_noteupdate.Modify(t_polnoteid, 'N', t_html);
	end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;
