declare
  t_RunAsUser   varchar2(30) := 'POSSEDBA';
  t_noteid number;
  t_html   varchar2(4000);
begin
  if sys_context('userenv', 'current_schema') != t_RunAsUser then
    raise_application_error(-20006, 'Please run script as ' || t_RunAsUser || '.');
  end if;
  t_html := '<font face="tahoma, arial, verdana, sans-serif" style="color: rgb(0, 0, 0); font-family: tahoma, arial, verdana, sans-serif; font-size: 12px;"><img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"></font><br><br><font face="tahoma, arial, verdana, sans-serif">A password reset was triggered for your New Jersey Division of Alcoholic Beverage Control account. Please use the code below to verify ownership of your account.</font><br><br>Your security code is: {Code}<br>Note: This code will expire in 30 minutes.<br><br><span style="font-family: arial;">If you have any questions or concerns, please contact the Division at POSSEAdmin@lps.state.nj.us or by phone at 609-984-2830.</span><br><br>';
  select noteid into t_noteid 
    from api.notes n
  join api.notedefs d 
    on d.NoteDefId = n.notedefid
  where tag = 'PPRE';
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  for i in (select objectid from query.o_systemsettings where rownum = 1) loop
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'PublicPwdResetEmailSubject','Online ABC - Internal Password Reset Email');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'PasswordResetMethodInstText','Please select the method you would like to use to recover your account.');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'PasswordResetEmailInstText','For your security, we need to verify your identity. We have sent a code to the provided email address. Please enter it below.');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'PasswordResetSMSInstText','For your security, we need to verify your identity. We have sent a code to the provided cell phone number. Please enter it below.');
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'AccRecoveryAttemptsExceededTxt','You have reached the daily request limit for this account recovery method. Please contact NJ-ABC to recover your account or select a different recovery method.');
    api.pkg_noteupdate.Modify(t_noteid, 'N', t_html);
	end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;