declare
  t_RunAsUser   varchar2(30) := 'POSSEDBA';
  t_noteid number;
  t_html   varchar2(4000);
begin
  if sys_context('userenv', 'current_schema') != t_RunAsUser then
    raise_application_error(-20006, 'Please run script as ' || t_RunAsUser || '.');
  end if;
  t_html := '<img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg" style="font-family: tahoma, arial, verdana, sans-serif; font-size: 12px;"><br><br><font face="tahoma, arial, verdana, sans-serif"><span style="font-size: 12px;">A reset was triggered for your New Jersey Division of Alcoholic Beverage Control account. Please visit the link below and log in with your new temporary password. You will be prompted to change your password upon successfully logging in.</span></font><br><br>Your temporary password is: {Password}<br>Note: This temporary password will expire in 30 minutes.<br><br><span style="font-family: arial;">[[Sign In]]</span><br><br>';
  select noteid into t_noteid 
    from api.notes n
  join api.notedefs d 
    on d.NoteDefId = n.notedefid
  where tag = 'IPRETP';
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  for i in (select objectid from query.o_systemsettings where rownum = 1) loop
    api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'InternalPwdResetEmailSubject','Online ABC - Internal Password Reset Email');
    api.pkg_noteupdate.Modify(t_noteid, 'N', t_html);
	end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;
