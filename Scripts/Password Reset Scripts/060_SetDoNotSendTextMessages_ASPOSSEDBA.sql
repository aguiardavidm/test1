/*-----------------------------------------------------------------------------
 * Author: Maria Knigge
 * Expected run time: < 0.5 seconds
 * Purpose: Sets DoNotSendTextMessage to 'Y' for non-prod environments and 'N'
 *   for Prod.
 *---------------------------------------------------------------------------*/
declare
  t_DB                                  varchar2(10) := sys_context('USERENV', 'DB_NAME');
  t_SystemSettingsId                    number;
begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  select s.ObjectId
  into t_SystemSettingsId
  from query.o_SystemSettings s
  where s.ObjectId = api.pkg_SimpleSearch.ObjectByIndex(
      'o_SystemSettings', 'SystemSettingsNumber', 1);

  if t_DB = 'NJPD' then
     api.pkg_ColumnUpdate.RemoveValue(t_SystemSettingsId, 'DoNotSendTextMessages');
  else
    api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'DoNotSendTextMessages', 'Y');
  end if;

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;
