/*-----------------------------------------------------------------------------
 * Author: Chris Phillips
 * Expected run time: < 1 sec
 * Purpose: This script is used to set the email subject and body for Police
 *     and Municipal User Creation/Activation on the Account Notifications tab
 *     of System Settings.
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_Count                               pls_integer := 0;
  t_EmailText                           varchar2(32767);
  t_LogicalTransactionId                number;
  t_SystemSettingsId                    udt_Id;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

  procedure CreateUpdateEmailTemplate (
    a_ObjectId                          udt_Id,
    a_Tag                               varchar2,
    a_Text                              varchar2
  ) is
    t_NoteDefId                         udt_Id;
    t_NoteId                            udt_Id;
  begin

    select n.NoteId
    into t_NoteId
    from
      api.Notes n
      join api.NoteDefs nd
          on n.NoteDefId = nd.NoteDefId
    where n.ObjectId = a_ObjectId
      and nd.Tag = a_Tag;
    -- if there is an existing email body template in system settings, update it.
    api.pkg_NoteUpdate.Modify(t_NoteId, 'N', a_Text);

  exception
    -- Create a new email body template
    when no_data_found then
      select nd.NoteDefId
      into t_NoteDefId
      from api.NoteDefs nd
      where nd.Tag = a_Tag;
      t_NoteId := api.pkg_NoteUpdate.New(a_ObjectId, t_NoteDefId, 'N', a_Text);

  end;

begin

  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  select s.ObjectId
  into t_SystemSettingsId
  from query.o_SystemSettings s
  where s.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings', 'SystemSettingsNumber',
      1);

  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  dbug(chr(13) || chr(10) || '---BEFORE---');
  select count(1)
  into t_Count
  from
    api.notes n
    join api.notedefs nd
        on n.NoteDefId = nd.NoteDefId
  where nd.Tag in ('PUCRIE', 'MNPUE', 'PUCRAE', 'MUCRE');
  dbug('Existing Email Body Templates: ' || t_Count);
  dbug('Email Body Templates to be created: ' || (4 - t_Count)); -- total templates being updated

  -- Set Subjects and Notes
  -- Police User Creation (Inactive) Email
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliUserCrInactiveEmailSubject',
      'Online ABC - Police User Creation');

  t_EmailText := '
  <img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg">
  <br>
  <br>
  <font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: 16px;">
  Dear {RankTitle} {FirstName} {LastName},<br>
  <br>
  Your NJ-ABC law enforcement account has been created and is pending activation. You will be notified upon your account being activated.
  <br>
  </font>
  ';

  CreateUpdateEmailTemplate(t_SystemSettingsId, 'PUCRIE', t_EmailText);

  -- Municipal Notification for Police User Creation
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniNotiPoliceUserEmailSubject',
      'Online ABC - Municipal Notification for Police User Creation');

  t_EmailText := '
  <img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg">
  <br>
  <br>
  <font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: 16px;">
  A new Police user has been registered for your municipality. Please provide authorization to NJ-ABC to activate this user.<br>
  <br>
  <strong>Police User Information:</strong>
  <table border="1" cellspacing="2" cellpadding="2" style="font-family: tahoma, arial, verdana, sans-serif; width: 600px; color: rgb(0, 0, 0); font-size: 15px;">
    <tbody>
      <tr>
        <td>
          <font face="verdana"><font face="arial">First Name:<br></font></font>
        </td>
        <td>
          <font face="verdana"><strong><font color="#ff6600">{FirstName}</font></strong><br></font>
        </td>
      </tr>
      <tr>
        <td>
          <font face="verdana"><font face="arial">Last Name:<br></font></font>
        </td>
        <td>
          <font face="verdana"><strong><font color="#ff6600">{LastName}</font></strong><br></font>
        </td>
      </tr>
      <tr>
        <td>
          <font face="verdana"><font face="arial">Email Address:<br></font></font>
        </td>
        <td>
          <font face="verdana"><strong><font color="#ff6600">{EmailAddress}</font></strong><br></font>
        </td>
      </tr>
      <tr>
        <td>
          <font face="verdana"><font face="arial">Rank Title:<br></font></font>
        </td>
        <td>
          <font face="verdana"><strong><font color="#ff6600">{RankTitle}</font></strong><br></font>
        </td>
      </tr>
      <tr>
        <td>
          <font face="verdana"><font face="arial">Phone Number:<br></font></font>
        </td>
        <td>
          <font face="verdana"><font color="#ff6600"><strong>{FormattedPhoneNumber}</strong></font></font>
        </td>
      </tr>
      <tr>
        <td>
          <font face="verdana"><font face="arial">Cell Phone:<br></font></font>
        </td>
        <td>
          <font face="verdana"><font color="#ff6600"><strong>{FormattedCellPhone}</strong></font></font>
        </td>
      </tr>
    </tbody>
  </table><br>
  This email is being sent to:<br>{MunicipalRecipients}
  </font>
  ';

  CreateUpdateEmailTemplate(t_SystemSettingsId, 'MNPUE', t_EmailText);

  -- Police User Activiation Email
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'PoliUserCrActiveEmailSubject',
      'Online ABC - Police User Activation');

  t_EmailText := '
  <img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"><br>
  <br>
  <font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: 16px;">
  Dear {RankTitle} {FirstName} {LastName},<br>
  <br>
  Your NJ-ABC law enforcement account has been activated. Please visit the link below and log in with your new temporary password. You will be prompted to change your password upon successfully logging in.<br>
  <br>
  Email Address: {EmailAddress}<br>
  Your temporary password is: {Password}<br>
  <br>
  [[Sign In]]<br>
  <br>
  </font>
  <font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: 12px;">
  Plain Text Link:<br>
  {RegistrationLink}
  </font>
  ';

  CreateUpdateEmailTemplate(t_SystemSettingsId, 'PUCRAE', t_EmailText);


  -- Municipal User Creation Email
  api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'MuniUserCreationEmailSubject',
      'Online ABC - Municipal User Creation');

  t_EmailText := '
  <img alt="Email Header" src="https://abc.lps.nj.gov/ABCPublic/images/EmailHeader2.jpg"><br>
  <br>
  <font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: 16px;">
  Dear {FirstName},<br>
  <br>
  Your NJ-ABC municipal account has been created. Please visit the link below and log in with your new temporary password. You will be prompted to change your password upon successfully logging in.<br>
  <br>
  User Id: {UserId}<br>
  Your temporary password is: {Password}<br>
  <br>
  [[Sign In]]<br>
  <br>
  </font>
  <font face="calibri, tahoma, arial, verdana, sans-serif" style="font-size: 12px;">
  Plain Text Link:<br>
  {RegistrationLink}
  </font>
  ';

  CreateUpdateEmailTemplate(t_SystemSettingsId, 'MUCRE', t_EmailText);

  -- Done
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

  dbug(chr(13) || chr(10) || '---AFTER---');
  select count(1)
  into t_Count
  from
    api.LogicalTransactions l
    join api.Notes n
        on n.LogicalTransactionId = l.LogicalTransactionId
    join api.NoteDefs nd
        on n.NoteDefId = nd.NoteDefId
  where l.LogicalTransactionId = t_LogicalTransactionId
    and nd.Tag in ('PUCRIE', 'MNPUE', 'PUCRAE', 'MUCRE');
  dbug('Email Body Templates Updated: ' || t_Count);
  commit;

end;
