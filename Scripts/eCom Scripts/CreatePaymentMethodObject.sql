declare
  t_NewPaymentMethodId         number(9);
  t_CurrentECOMPaymentMethodId number(9);
  
begin
  select max(ObjectId)
    into t_CurrentECOMPaymentMethodId
    from query.o_PaymentMethod p
   where p.EComAndActive = 'Y';

  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  api.pkg_columnupdate.SetValue( t_CurrentECOMPaymentMethodId, 'ECOMCreditCardFlag', 'Y' );
  
  t_NewPaymentMethodId := api.pkg_objectupdate.New( api.pkg_configquery.ObjectDefIdForName( 'o_PaymentMethod' ) );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'Name', 'eCheck' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'EComFlag', 'Y' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'ReferenceRequiredFlag', 'N' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'ReferenceLabel', 'eCheck' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'ReceiptNumberLabel', 'Receipt Number' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'ActiveFlag', 'Y' );
  --api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'BCExpressPayCardTypeCode', '' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'UserSelectable', 'Y' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'RefundFlag', 'N' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'ECOMCreditCardFlag', 'N' );
  api.pkg_columnupdate.SetValue( t_NewPaymentMethodId, 'eCheckFlag', 'Y' );

  api.pkg_logicaltransactionupdate.EndTransaction;
  
  commit;
  
end;  
  
  
  
  
  
