import xml.sax as sax
sax.make_parser()


#Testing
import cx_Exceptions
import cx_Logging
import datetime
import os
import sys
sys.path.insert(0, [s for s in sys.path if s.find("CustomLib") > -1][0])
import time
from suds.client import Client

#import xml.sax as sax
#sax.make_parser()

#cx_Logging.getLogger('suds.client')

configCache = dataArea.configCache

sysdateRows = dataArea.ExecuteSql("DatabaseSysdate")
today, = sysdateRows.fetchone()
today = datetime.datetime(today.year, today.month, today.day,
        today.hour, today.minute, today.second)

cx_Logging.StartLogging('HarnessPaymentCleanup.log', 10, 10)
#cx_Logging.StartLogging('HarnessPaymentCleanup.log', 20, 10)
print(cx_Logging.GetLoggingFileName())


systemSettings = dataArea.ExecuteSql("SimpleSearch",
        objectDefName = "o_SystemSettings",
        columnName = "SystemSettingsNumber",
        value = 1)


systemSettingsRows = systemSettings.fetchall()

#get the wsdl and threshold from admin site
for objectId, in systemSettingsRows:
    dataArea.Clear()
    obj = dataArea.ObjectForId(objectId)
    newThresholdMinutes = obj.GetColumnValueByName("eComThresholdMinutes", True)
    nicusaWsdlUrl = obj.GetColumnValueByName("PaymentProviderWSDLURL", True)
    njAppName = obj.GetColumnValueByName("PaymentProviderClientAppName", True)
    eComJobId = obj.GetColumnValueByName("LimitToSpecificEComJobId", True)


#nicusaWsdlUrl = 'http://localhost:63161/TEST_NJ_NICUSA_WSService.asmx?wsdl'
nicusaWsdlUrl = 'http://localhost:51817/NJ_NICUSA_WSService.asmx?WSDL'


nicusa = Client(nicusaWsdlUrl)

print (nicusa)

#time.sleep(1)


nicusaInfoPing = nicusa.service.ping()

#cx_Logging.Info("  NICUSA Ping: %s", nicusaInfoPing)
print(nicusaInfoPing)


#test Get Payment
#paymentInfoRequest = nicusa.factory.create('ns0:njGetPaymentInfoRequest')
#paymentInfoResponse = nicusa.service.getPaymentInfo(paymentInfoRequest)

newThresholdDate = today - datetime.timedelta(minutes = newThresholdMinutes)

pendingPayments = dataArea.ExecuteSql("SimpleSearch",
        objectDefName = "j_eCom",
        columnName = "PaymentStatus",
        value = "PreparePayment")

pendingPaymentRows = pendingPayments.fetchall()

#run only the first row
pendingPaymentRows = [pendingPaymentRows[9]]


paymentInfoRequest = nicusa.factory.create('ns0:njGetPaymentInfoRequest')
paymentInfoRequest.njApplicationName = njAppName


for jobId, in pendingPaymentRows:
    dataArea.Clear()
    cx_Logging.Info("  Checking job %s", jobId)
    job = dataArea.ObjectForId(jobId)
    check = job.GetColumnValueByName("CreatedDate", True) < newThresholdDate and \
        job.GetColumnValueByName("StatusName", True) == "NEW" and \
        (eComJobId is None or eComJobId is not None and jobId == eComJobId)
    cx_Logging.Info("  Continue: %s", str(check))
    if job.GetColumnValueByName("CreatedDate", True) < newThresholdDate and \
        job.GetColumnValueByName("StatusName", True) == "NEW" and \
        (eComJobId is None or eComJobId is not None and jobId == eComJobId):
        paymentInfoRequest.token = job.GetColumnValueByName("PaymentProviderToken", True)
        #
        paymentInfoResponse = nicusa.service.getPaymentInfo(paymentInfoRequest)
        #
        doContinue = True
        if hasattr(paymentInfoResponse, 'FAILMESSAGE'):
            if '9000' in paymentInfoResponse.FAILMESSAGE or \
               '9010' in paymentInfoResponse.FAILMESSAGE:
                cx_Logging.Info("    skipping %s as it has a 9000 or 9010 error.", jobId)
                doContinue = False
        if doContinue:
            #this next section of code is intended to duplicate the logic in RecordPayment.aspx.cs
            if hasattr(paymentInfoResponse, 'AUTHCODE'):
                receiptNumber = paymentInfoResponse.AUTHCODE if paymentInfoResponse.AUTHCODE is not None else ""
            else:
                receiptNumber = ""
            #
            paymentSuccess = ((paymentInfoResponse.FAILCODE if paymentInfoResponse.FAILCODE is not None else "")=="N") and \
                            (not hasattr(paymentInfoResponse, 'FAILMESSAGE')) and \
                            (receiptNumber != "") and \
                            ((paymentInfoResponse.TOTALAMOUNT if paymentInfoResponse.TOTALAMOUNT is not None else "") != "")
            #
            print(paymentSuccess)
            #
            if paymentSuccess:
                #
                extendedValues = paymentInfoResponse.extendedValues
                #
                for x in extendedValues:
                    if x.NAME == "ORIG_COS_AMOUNT":
                        originalTotal = x.VALUE
                    if x.NAME == "ORIG_FEE_AMOUNT":
                        extraFees = x.VALUE
                    #if x.NAME == "ORIG_TRANS_TOTAL":
                    #    origTransTotal = x.VALUE
                #
                dtReceiptDate = datetime.datetime.strptime(paymentInfoResponse.RECEIPTDATE + " " + paymentInfoResponse.RECEIPTTIME[:-4], "%m/%d/%Y %I:%M:%S %p")
                sReceiptDate = datetime.datetime.strftime(dtReceiptDate, "%Y-%m-%d %H:%M:%S")
                #
                job.SetColumnValueByName("PaymentStatus", "PaymentSuccess")
                job.SetColumnValueByName("PaymentProviderPaymentResponse", str(paymentInfoResponse))
                job.SetColumnValueByName("PaymentProviderPayeeName", paymentInfoResponse.NAME)
                job.SetColumnValueByName("PaymentProviderOrderId", paymentInfoResponse.ORDERID)
                job.SetColumnValueByName("PaymentProviderReceiptDate", sReceiptDate)
                job.SetColumnValueByName("PaymentProviderTotalPaid", paymentInfoResponse.TOTALAMOUNT)
                job.SetColumnValueByName("PaymentProviderOriginalAmount", originalTotal)
                job.SetColumnValueByName("PaymentProviderExtraFees", extraFees)
                job.SetColumnValueByName("PaymentProviderPaymentType", paymentInfoResponse.PAYTYPE)
                job.SetColumnValueByName("PaymentProviderCCType", paymentInfoResponse.creditCardType)
                job.SetColumnValueByName("PaymentProviderCCNumber", paymentInfoResponse.LAST4NUMBER)         
                #
                proc = job.CreateProcess(configCache.ObjectDefForName("p_AcceptPayment"))
                proc.SetColumnValueByName("Outcome", "Accepted")
                #
                cx_Logging.Info("    Payment updated as successful %s", jobId)
            else:
                job.SetColumnValueByName("PaymentStatus", "PaymentFailed")
                job.SetColumnValueByName("PaymentProviderPaymentResponse", str(paymentInfoResponse))
				#
                proc = job.CreateProcess(configCache.ObjectDefForName("p_AbandonPayment"))
                proc.SetColumnValueByName("Outcome", "Denied")
                #
                cx_Logging.Info("    Payment updated as unsuccessful %s", jobId)
            	#
            try:
                dataArea.Update()
                cx_Logging.Info("    Checking Webservice Complete for jobId %s", jobId)
            except Exception, e:
                cx_Logging.Error("Error recording Payment Information %s", e)
                continue

cx_Logging.Info("CleanUpPaymentAttempts finished at %s", today.strftime("%Y-%m-%d %H:%M:%S"))
            #break

    # Get the job back again after the update
    #job = dataArea.ObjectForId(jobId)
print(cx_Logging.GetLoggingFileName())

del pendingPaymentRows
del paymentInfoRequest
