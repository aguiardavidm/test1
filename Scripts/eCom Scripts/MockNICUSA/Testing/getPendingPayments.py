def getPendingPayments():
  pendingPayments = dataArea.ExecuteSql("SimpleSearch",
	        objectDefName = "j_eCom",
	        columnName = "PaymentStatus",
	        value = "PreparePayment")
  #
  pendingPaymentRows = pendingPayments.fetchall()
  #
  return pendingPaymentRows

getPendingPayments()