﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace MockNICUSA
{
    /// <summary>
    /// Summary description for NJ_NICUSA_WSService
    /// </summary>
    [WebService(Namespace = "http://computronix.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class NJ_NICUSA_WSService : INJ_NICUSA_WSServiceSoapBinding
    {
        [return: XmlElement("return", Form = XmlSchemaForm.Unqualified)]
        public njGetPaymentInfoResponse getPaymentInfo(njGetPaymentInfoRequest njGetPaymentInfoRequest)
        {
            njNameValuePair[] inputExtendValues = new njNameValuePair[2];
            inputExtendValues[0] = 
                    new njNameValuePair()
                    {
                        NAME = "ORIG_COS_AMOUNT",
                        VALUE = "100"
                    };
            inputExtendValues[1] =
                    new njNameValuePair()
                    {
                        NAME = "ORIG_FEE_AMOUNT",
                        VALUE = "1"
                    };

            njGetPaymentInfoResponse testPaymentInfo = new njGetPaymentInfoResponse()
            {
                FAILCODE = "N",
                AUTHCODE = "1234",
                TOKEN = "1",
                TOTALAMOUNT = "100",
                extendedValues = inputExtendValues,
                NAME = "PayeeName",
                ORDERID = "321",
                RECEIPTDATE = "01/01/2019",
                RECEIPTTIME = "01:01:01 AMzzzz",
                PAYTYPE = "Credit Card",
                creditCardType = "VISA",
                LAST4NUMBER = "1111"
            };

            return testPaymentInfo;
        }
        
        [return: XmlElement("return", Form = XmlSchemaForm.Unqualified)]
        public string ping()
        {
            return "OK";
        }

        [return: XmlElement("return", Form = XmlSchemaForm.Unqualified)]
        public njPreparePaymentResult preparePayment(njPaymentInfo njPayInfo)
        {
            throw new NotImplementedException();
        }
    }
}
