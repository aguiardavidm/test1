"""
NJ eCom Cleanup

WSDL file locations:
Integration WSDL: https://www-cstg.state.nj.us//NJ_ENT_EPAY_WS_HOSTED_NICUSA/NJ_NICUSA_WSService?wsdl
Integration ws service endpoint: https://www-cstg.state.nj.us//NJ_ENT_EPAY_WS_HOSTED_NICUSA/NJ_NICUSA_WSService
System Test WSDL(may require a firewall rule):
https://pci-test.sa.state.nj.us/NJ_ENT_EPAY_WS_HOSTED_NICUSA/NJ_NICUSA_WSService?wsdl
System Test ws service endpoint:
https://pci-test.sa.state.nj.us/NJ_ENT_EPAY_WS_HOSTED_NICUSA/NJ_NICUSA_WSService
Production WSDL(may require a firewall rule): https://pci.sa.state.nj.us/NJ_ENT_EPAY_WS_HOSTED_NICUSA/NJ_NICUSA_WSService?wsdl
Production ws service endpoint: https://pci.sa.state.nj.us/NJ_ENT_EPAY_WS_HOSTED_NICUSA/NJ_NICUSA_WSService

Do not alter any records on your end that have a -9000 or -9010 (these error indicate failure to connect to NIC USA).
The error code and message will be present in the failmessage field for the "getPaymentInfo" method.
For the preparepayment method, the -9000, -9010 will be contained in the errorcode and errormesage fields.

"""
import cx_Exceptions
import cx_Logging
import datetime
import os
import sys
sys.path.insert(0, [s for s in sys.path if s.find("CustomLib") > -1][0])
import time
from suds.client import Client

configCache = dataArea.configCache

sysdateRows = dataArea.ExecuteSql("DatabaseSysdate")
today, = sysdateRows.fetchone()
today = datetime.datetime(today.year, today.month, today.day,
        today.hour, today.minute, today.second)

#print (today)

cx_Logging.Info("CleanUpPaymentAttempts started at %s",
        today.strftime("%Y-%m-%d %H:%M:%S"))
cx_Logging.Info("Logging output is here: %s",
        cx_Logging.GetLoggingFileName())

#print(cx_Logging.GetLoggingFileName())

systemSettings = dataArea.ExecuteSql("SimpleSearch",
        objectDefName = "o_SystemSettings",
        columnName = "SystemSettingsNumber",
        value = 1)

systemSettingsRows = systemSettings.fetchall()

'''
#hardcode the wsdl and threshold
nicusaWsdlUrl = "https://www-cstg.state.nj.us//NJ_ENT_EPAY_WS_HOSTED_NICUSA/NJ_NICUSA_WSService?wsdl"
newThresholdMinutes = 30

#get the njAppName from admin site...
njAppName = "LPS_POSSE"
'''

#get the wsdl and threshold from admin site
for objectId, in systemSettingsRows:
    dataArea.Clear()
    obj = dataArea.ObjectForId(objectId)
    newThresholdMinutes = obj.GetColumnValueByName("eComThresholdMinutes", True)
    nicusaWsdlUrl = obj.GetColumnValueByName("PaymentProviderWSDLURL", True)
    njAppName = obj.GetColumnValueByName("PaymentProviderClientAppName", True)
    eComJobId = obj.GetColumnValueByName("LimitToSpecificEComJobId", True)
del systemSettingsRows

if njAppName is None:
    cx_Logging.Error("The Payment Provider Client Application Name must be supplied in System Settings in the Administration website")
    sys.exit(0)

if nicusaWsdlUrl is None:
    cx_Logging.Error("The Payment Provider WSDL URL must be supplied in System Settings in the Administration website")
    sys.exit(0)

#OIT is recommending wait for 30 minutes
if newThresholdMinutes is None:
    newThresholdMinutes = 30
    
newThresholdDate = today - datetime.timedelta(minutes = newThresholdMinutes)

pendingPayments = dataArea.ExecuteSql("SimpleSearch",
        objectDefName = "j_eCom",
        columnName = "PaymentStatus",
        value = "PreparePayment")

pendingPaymentRows = pendingPayments.fetchall()


nicusa = Client(nicusaWsdlUrl)
nicusaInfoPing = nicusa.service.ping()
cx_Logging.Info("  NICUSA Ping: %s", nicusaInfoPing)
#print(nicusaInfoPing)

paymentInfoRequest = nicusa.factory.create('ns0:njGetPaymentInfoRequest')
paymentInfoRequest.njApplicationName = njAppName

cx_Logging.Info("Checking %s NEW rows for CreatedDate < %s",
        len(pendingPaymentRows), newThresholdDate.strftime("%Y-%m-%d %H:%M:%S"))

if eComJobId is None:
    cx_Logging.Info("  All jobs in the threshold will be processed")
else:
    cx_Logging.Info("  Settings indicated to limit processing to eCom Job %s", eComJobId)
    
for jobId, in pendingPaymentRows:
    dataArea.Clear()
    cx_Logging.Info("  Checking job %s", jobId)
    job = dataArea.ObjectForId(jobId)
    if job.GetColumnValueByName("CreatedDate", True) < newThresholdDate and \
       job.GetColumnValueByName("StatusName", True) == "NEW" and \
       (eComJobId is None or eComJobId is not None and jobId == eComJobId):
    #if job.GetColumnValueByName("CreatedDate", True) < newThresholdDate:
    #if jobId == 14026877: #Null Token
    #if jobId == 14028575: #successful payment
    #if jobId == 14033479: #successful payment
    #if jobId == 12632516:
    #if jobId == 14030264:
        cx_Logging.Info("    Checking Webservice for jobId %s", jobId)
        
        #print ("Status: " + job.GetColumnValueByName("StatusName", True))
        paymentInfoRequest.token = job.GetColumnValueByName("PaymentProviderToken", True)
        #print (paymentInfoRequest)
        cx_Logging.Info("    paymentInfoRequest %s", str(paymentInfoRequest))

        paymentInfoResponse = nicusa.service.getPaymentInfo(paymentInfoRequest)
        #print (paymentInfoResponse)
        cx_Logging.Info("    paymentInfoResponse %s", str(paymentInfoResponse))
        
        #check for the -9000 and -9010 error codes
        doContinue = True
        if hasattr(paymentInfoResponse, 'FAILMESSAGE'):
            if '9000' in paymentInfoResponse.FAILMESSAGE or \
               '9010' in paymentInfoResponse.FAILMESSAGE:
                cx_Logging.Info("    skipping %s as it has a 9000 or 9010 error.", jobId)
                doContinue = False
        if doContinue:
            #this next section of code is intended to duplicate the logic in RecordPayment.aspx.cs
            if hasattr(paymentInfoResponse, 'AUTHCODE'):
                receiptNumber = paymentInfoResponse.AUTHCODE if paymentInfoResponse.AUTHCODE is not None else ""
            else:
                receiptNumber = ""
            
            paymentSuccess = ((paymentInfoResponse.FAILCODE if paymentInfoResponse.FAILCODE is not None else "")=="N") and \
                            (not hasattr(paymentInfoResponse, 'FAILMESSAGE')) and \
                            (receiptNumber != "") and \
                            ((paymentInfoResponse.TOTALAMOUNT if paymentInfoResponse.TOTALAMOUNT is not None else "") != "")
            
            #print(paymentSuccess)
            
            if paymentSuccess:
                
                extendedValues = paymentInfoResponse.extendedValues
                
                for x in extendedValues:
                    if x.NAME == "ORIG_COS_AMOUNT":
                        originalTotal = x.VALUE
                    if x.NAME == "ORIG_FEE_AMOUNT":
                        extraFees = x.VALUE
                    #if x.NAME == "ORIG_TRANS_TOTAL":
                    #    origTransTotal = x.VALUE
                
                dtReceiptDate = datetime.datetime.strptime(paymentInfoResponse.RECEIPTDATE + " " + paymentInfoResponse.RECEIPTTIME[:-4], "%m/%d/%Y %I:%M:%S %p")
                sReceiptDate = datetime.datetime.strftime(dtReceiptDate, "%Y-%m-%d %H:%M:%S")
                
                job.SetColumnValueByName("PaymentStatus", "PaymentSuccess")
                job.SetColumnValueByName("PaymentProviderPaymentResponse", str(paymentInfoResponse))
                job.SetColumnValueByName("PaymentProviderPayeeName", paymentInfoResponse.NAME)
                job.SetColumnValueByName("PaymentProviderOrderId", paymentInfoResponse.ORDERID)
                job.SetColumnValueByName("PaymentProviderReceiptDate", sReceiptDate)
                job.SetColumnValueByName("PaymentProviderTotalPaid", paymentInfoResponse.TOTALAMOUNT)
                job.SetColumnValueByName("PaymentProviderOriginalAmount", originalTotal)
                job.SetColumnValueByName("PaymentProviderExtraFees", extraFees)
                job.SetColumnValueByName("PaymentProviderPaymentType", paymentInfoResponse.PAYTYPE)
                job.SetColumnValueByName("PaymentProviderCCType", paymentInfoResponse.creditCardType)
                job.SetColumnValueByName("PaymentProviderCCNumber", paymentInfoResponse.LAST4NUMBER)         
                
                proc = job.CreateProcess(configCache.ObjectDefForName("p_AcceptPayment"))
                proc.SetColumnValueByName("Outcome", "Accepted")
                
                cx_Logging.Info("    Payment updated as successful %s", jobId)
            else:
                job.SetColumnValueByName("PaymentStatus", "PaymentFailed")
                job.SetColumnValueByName("PaymentProviderPaymentResponse", str(paymentInfoResponse))

                proc = job.CreateProcess(configCache.ObjectDefForName("p_AbandonPayment"))
                proc.SetColumnValueByName("Outcome", "Denied")
                
                cx_Logging.Info("    Payment updated as unsuccessful %s", jobId)
            
            try:
                dataArea.Update()
                cx_Logging.Info("    Checking Webservice Complete for jobId %s", jobId)
            except Exception, e:
                cx_Logging.Error("Error recording Payment Information %s", e)
                continue
cx_Logging.Info("CleanUpPaymentAttempts finished at %s",
        today.strftime("%Y-%m-%d %H:%M:%S"))          
            #break

    # Get the job back again after the update
    #job = dataArea.ObjectForId(jobId)

del pendingPaymentRows
del paymentInfoRequest
