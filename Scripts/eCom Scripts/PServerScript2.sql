declare 
  x number;
begin
  x := api.pkg_processserver.SchedulePythonScript('eComCleanup', 'Clean up Payment Attempts', null, sysdate+1);
  commit;
end;
