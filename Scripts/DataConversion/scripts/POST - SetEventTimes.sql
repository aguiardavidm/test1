--Run time: < 3 minutes
declare
t_StartHoursCD    number :=api.pkg_configquery.ColumnDefIdForName('o_ABC_EventDate', 'StartTimeHours');
t_StartMinutesCD  number :=api.pkg_configquery.ColumnDefIdForName('o_ABC_EventDate', 'StartTimeMinutes');
t_StartAMPMCD     number :=api.pkg_configquery.ColumnDefIdForName('o_ABC_EventDate', 'StartTimeAMPM');
t_EndHoursCD      number :=api.pkg_configquery.ColumnDefIdForName('o_ABC_EventDate', 'EndTimeHours');
t_EndMinutesCD    number :=api.pkg_configquery.ColumnDefIdForName('o_ABC_EventDate', 'EndTimeMinutes');
t_EndAMPMCD       number :=api.pkg_configquery.ColumnDefIdForName('o_ABC_EventDate', 'EndTimeAMPM');


begin
  for i in (select (select objectid from dataconv.o_abc_eventdate e where e.legacykey = 'E' || pe.perm_num || pe.dt_event || pe.time_start || pe.time_end) objectid,
                   substr(pe.time_start, 1,2) StartHour,
                   substr(pe.time_start, 4,2) StartMinute,
                   substr(pe.time_end, 1,2) EndHour,
                   substr(pe.time_end, 4,2) EndMinute,
                   pe.start_am_pm,
                   pe.end_am_pm
              from abc11p.perm_event pe
             where 'E' || PERM_NUM
                   || DT_EVENT 
                   || TIME_START 
                   || TIME_END in (select x.legacykey
                                         from dataconv.o_abc_eventdate x)
            union
            select (select objectid from dataconv.o_abc_eventdate e where e.legacykey = 'R' || pe.perm_num || pe.dt_event || pe.time_start || pe.time_end) objectid,
                   substr(pe.time_start, 1,2) StartHour,
                   substr(pe.time_start, 4,2) StartMinute,
                   substr(pe.time_end, 1,2) EndHour,
                   substr(pe.time_end, 4,2) EndMinute,
                   pe.start_am_pm,
                   pe.end_am_pm
              from abc11p.perm_rain pe
             where 'R' || PERM_NUM
                   || DT_EVENT 
                   || TIME_START 
                   || TIME_END in (select x.legacykey
                                         from dataconv.o_abc_eventdate x)) loop
--Start Times
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
           i.ObjectId,
           t_StartHoursCD,
           null EffectiveStartDate,
           null EffectiveEndDate,
           i.starthour AttributeValue, 
           null AttributeValueId,
           null SearchValue,
           null NumericSearchValue
      from dual
     where not exists (select 1 
                         from possedata.ObjectColumnData
                        where ObjectId = i.ObjectId
                          and ColumnDefId = t_StartHoursCD);
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
           i.ObjectId,
           t_StartMinutesCD,
           null EffectiveStartDate,
           null EffectiveEndDate,
           i.startminute AttributeValue, 
           null AttributeValueId,
           null SearchValue,
           null NumericSearchValue
      from dual
     where not exists (select 1 
                         from possedata.ObjectColumnData
                        where ObjectId = i.ObjectId
                          and ColumnDefId = t_StartMinutesCD);
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
           i.ObjectId,
           t_StartAMPMCD,
           null EffectiveStartDate,
           null EffectiveEndDate,
           null AttributeValue, 
           (select dlv.DomainValueId
              from api.domains  d
              join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
             where d.Name = 'AM/PM'
               and dlv.AttributeValue = i.start_am_pm)AttributeValueId,
           null SearchValue,
           null NumericSearchValue
      from dual
     where not exists (select 1 
                         from possedata.ObjectColumnData
                        where ObjectId = i.ObjectId
                          and ColumnDefId = t_StartAMPMCD);
--End Times
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
           i.ObjectId,
           t_EndHoursCD,
           null EffectiveStartDate,
           null EffectiveEndDate,
           i.endhour AttributeValue, 
           null AttributeValueId,
           null SearchValue,
           null NumericSearchValue
      from dual
     where not exists (select 1 
                         from possedata.ObjectColumnData
                        where ObjectId = i.ObjectId
                          and ColumnDefId = t_EndHoursCD);
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
           i.ObjectId,
           t_EndMinutesCD,
           null EffectiveStartDate,
           null EffectiveEndDate,
           i.endminute AttributeValue, 
           null AttributeValueId,
           null SearchValue,
           null NumericSearchValue
      from dual
     where not exists (select 1 
                         from possedata.ObjectColumnData
                        where ObjectId = i.ObjectId
                          and ColumnDefId = t_EndMinutesCD);
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
           i.ObjectId,
           t_EndAMPMCD,
           null EffectiveStartDate,
           null EffectiveEndDate,
           null AttributeValue, 
           (select dlv.DomainValueId
              from api.domains  d
              join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
             where d.Name = 'AM/PM'
               and dlv.AttributeValue = i.end_am_pm)AttributeValueId,
           null SearchValue,
           null NumericSearchValue
      from dual
     where not exists (select 1 
                         from possedata.ObjectColumnData
                        where ObjectId = i.ObjectId
                          and ColumnDefId = t_EndAMPMCD);
  end loop;
  
end;