declare
  t_StateDefId     number := api.pkg_configquery.columndefidforname('o_ABC_LegalEntity', 'StateOfIncorporation');
  t_AuthDefId      number := api.pkg_configquery.columndefidforname('o_ABC_LegalEntity', 'AuthorizationToConductBusiness');
  t_CorpChartDefId number := api.pkg_configquery.columndefidforname('o_ABC_LegalEntity', 'CorporationCharterBeenRevoked');
  t_YesDomId       number;
  t_noDomId       number;
begin
  select dv.DomainValueId
    into t_YesDomId
    from api.domains d
    join api.domainlistofvalues dv on dv.DomainId = d.DomainId
   where d.name = 'Yes/No'
     and dv.AttributeValue = 'Yes';
  select dv.DomainValueId
    into t_NoDomId
    from api.domains d
    join api.domainlistofvalues dv on dv.DomainId = d.DomainId
   where d.name = 'Yes/No'
     and dv.AttributeValue = 'No';
  for i in (select l.objectid, n.CORP_CHART_ST, n.CORP_NJ_AUTH, n.CORP_NJ_REVOC_IND
              from dataconv.o_abc_LegalEntity l
              join abc11p.name_mstr n on to_char(n.name_mstr_id_num) = l.legacykey
             where n.CORP_CHART_ST is not null 
                or n.CORP_NJ_AUTH is not null
                or n.CORP_NJ_REVOC_IND is not null) loop
    --State
    if i.corp_chart_st is not null then
      delete from possedata.objectcolumndata cd
       where cd.ColumnDefId = t_StateDefId
         and cd.objectid = i.objectid;
      insert /*append*/ into possedata.objectcolumndata
        (
        LogicalTransactionId,
        ObjectId,
        ColumnDefId,
        AttributeValueId,
        SearchValue
        )
        values
        (
        1,
        i.ObjectId,
        t_StateDefId,
        (select dv.DomainValueId
           from api.domains d
           join api.domainlistofvalues dv on dv.DomainId = d.DomainId
          where d.name = 'State'
            and dv.AttributeValue = i.corp_chart_st),
        null
        );
    end if;
    --Auth
    if i.CORP_NJ_AUTH is not null then
      delete from possedata.objectcolumndata cd
       where cd.ColumnDefId = t_AuthDefId
         and cd.objectid = i.objectid;
      insert /*append*/ into possedata.objectcolumndata
        (
        LogicalTransactionId,
        ObjectId,
        ColumnDefId,
        AttributeValueId,
        SearchValue
        )
        values
        (
        1,
        i.ObjectId,
        t_AuthDefId,
        decode(i.CORP_NJ_AUTH, 'Y', t_YesDomId, 'N', t_NoDomId),
        null
        );
    end if;
    --Corp
    if i.CORP_NJ_REVOC_IND is not null then
      delete from possedata.objectcolumndata cd
       where cd.ColumnDefId = t_CorpChartDefId
         and cd.objectid = i.objectid;
      insert /*append*/ into possedata.objectcolumndata
        (
        LogicalTransactionId,
        ObjectId,
        ColumnDefId,
        AttributeValueId,
        SearchValue
        )
        values
        (
        1,
        i.ObjectId,
        t_CorpChartDefId,
        decode(i.CORP_NJ_REVOC_IND, 'Y', t_YesDomId, 'N', t_NoDomId),
        null
        );
    end if;
  end loop;
  commit;
end;
