--Run time: 1 minute
-- Created on 9/12/2014 by ADMINISTRATOR
declare
  -- Local variables here
  i integer;
  t_RowsProcessed number := 0;
begin
  if user != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20000, 'Script must be run as POSSEDBA');
  end if;

  for i in (select b.abc11puk BrnLK, l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num || l.gen_num LicLK, d.dist_stat
              from abc11p.brn_dist d
              join abc11p.brn_reg b on b.brn_reg_num = d.brn_reg_num
              join abc11p.lic_mstr l on l.cnty_muni_cd = d.cnty_muni_cd
                                    and l.lic_type_cd = d.lic_type_cd
                                    and l.muni_ser_num = d.muni_ser_num
                                    and l.gen_num = coalesce(d.gen_num, (select max(m.gen_num)
                                                                           from abc11p.lic_mstr m
                                                                          where m.cnty_muni_cd = d.cnty_muni_cd
                                                                            and m.lic_type_cd = d.lic_type_cd
                                                                            and m.muni_ser_num = d.muni_ser_num
                                                                            and m.lic_stat != 'X'))
             where d.dist_stat in ('C', 'H')
               and l.lic_stat in ('C', 'H')
               and not exists (select 1
                                 from dataconv.r_abcproduct_distributor ex
                                where l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num || l.gen_num = ex.o_abc_license
                                  and b.abc11puk = ex.o_abc_product)
               and not exists (select 1
                                 from dataconv.r_abc_product_historic_dist ex
                                where l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num || l.gen_num = ex.o_abc_license
                                  and b.abc11puk = ex.o_abc_product)
             group by b.abc11puk, l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num || l.gen_num, d.dist_stat) loop
    if i.dist_stat = 'C' then
      insert into dataconv.r_abcproduct_distributor pd
      (
      pd.o_abc_license,
      pd.o_abc_product
      )
      values
      (
      i.liclk,
      i.brnlk
      );
    elsif i.dist_stat = 'H' then
      insert into dataconv.r_abc_product_historic_dist ph 
      (
      ph.o_abc_license,
      ph.o_abc_product
      )
      values
      (
      i.liclk,
      i.brnlk
      );
    end if;  
  t_RowsProcessed := t_RowsProcessed+1;
  if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
    end if;
  end loop;
  dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
  commit;
end;