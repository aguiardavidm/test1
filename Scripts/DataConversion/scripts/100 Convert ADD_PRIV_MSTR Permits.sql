--Run time: 1 second
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_PermitType      varchar2(100);
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin  
    dbms_output.put_line('Error: '|| a_ErrorText || 'ErrorCode: ' ||a_ErrorCode);
  end;

begin
  
  select ObjectId 
    into t_PermitType 
   from dataconv.o_abc_permittype pt
  where pt.name = 'Winery Outlet / Salesroom';


  for c in (select t.rowid,
            CNTY_MUNI_CD ||'-'|| LIC_TYPE_CD ||'-'|| MUNI_SER_NUM ||'-'|| GEN_NUM ||'-'|| PRIV_SEQ_NUM LEGACYKEY,
            CNTY_MUNI_CD ||'-'|| LIC_TYPE_CD ||'-'|| MUNI_SER_NUM ||'-'|| GEN_NUM ||'-'|| PRIV_SEQ_NUM PERMITNUMBER,
            CNTY_MUNI_CD || LIC_TYPE_CD || MUNI_SER_NUM || GEN_NUM LICENSELK,
            t_PermitType PERMITTYPELK,
            case when PRIV_STAT = 'C' then 'Active' when PRIV_STAT = 'H' then 'Closed' end STATE,
            case when substr(PLACE_CNTY_MUNI_CD,1,2) = '34' then null else substr(PLACE_CNTY_MUNI_CD,1,2) end COUNTYLK,
            case when substr(PLACE_CNTY_MUNI_CD,1,2) = '34' then null else PLACE_CNTY_MUNI_CD end MUNICIPALITYLK,
            LOCATION AddressDescription,
            t.abc11puk || 'PREM' EVENTLOCATIONADDRESSLK, --STR_NUM ||' '|| STR_NAME ||' '|| MUNI_NAME ||' '|| ST_PROV_CD ||' '|| ZIP_1_5 ||'-'|| ZIP_6_9 EVENTLOCATIONADDRESSLK,
            ('||AREA_CD||') || PREFIX ||'-'|| SUFFIX AdditionalPermitInformation,
            CRTD_OPER_ID CREATEDBYUSERLEGACYKEY,
            DT_ROW_CRTD CONVCREATEDDATE,
            UPDT_OPER_ID LASTUPDATEDBYUSERLEGACYKEY,
            DT_ROW_UPDT LASTUPDATEDDATE,
            'Off Premise Consumption?: ' ||OFF_PREM_CONS_IND || chr(10)|| chr(13) || 'On Premise Consumption?: ' ||ON_PREM_CONS_IND || chr(10)|| chr(13) || ' Sampling?: '||SAMPLING_IND || chr(10)|| chr(13) || 'Building Total: ' ||  BLDG_TOT text
            from abc11p.ADD_PRIV_MSTR t) loop
    begin
    insert into dataconv.o_abc_permit a
    (   LEGACYKEY,
    PERMITNUMBER,
    PERMITTYPELK,
    STATE,
    COUNTYLK,
    MUNICIPALITYLK,
    EVENTLOCATIONADDRESSLK,
    AdditionalPermitInformation,
    CREATEDBYUSERLEGACYKEY,
    CONVCREATEDDATE,
    LASTUPDATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE
      )
    values (c.LEGACYKEY,
      c.PERMITNUMBER,
      c.PERMITTYPELK,
      c.STATE,
      c.COUNTYLK,
      c.MUNICIPALITYLK,
      c.EVENTLOCATIONADDRESSLK,
      c.AdditionalPermitInformation,
      c.CREATEDBYUSERLEGACYKEY,
      c.CONVCREATEDDATE,
      c.LASTUPDATEDBYUSERLEGACYKEY,
      c.LASTUPDATEDDATE      );   

  insert into dataconv.r_PermitLicense
        (o_abc_license, 
     o_abc_permit )
        values(c.LICENSELK,
         c.LEGACYKEY);    
         
    insert into dataconv.n_general
         (createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text)
     values (c.CREATEDBYUSERLEGACYKEY,
             c.CONVCREATEDDATE,
             c.LASTUPDATEDBYUSERLEGACYKEY,
             c.LASTUPDATEDDATE,
             'o_abc_permit',
             c.LEGACYKEY,
             'Y',
              c.text);         
      
    t_count := t_count +1; 
    
    for d in (select 
               'Building Number: ' || BLDG_NUM || chr(10) || chr(13) ||
               'Entire Building?: ' || ENTIRE_BLDG_IND || chr(10) || chr(13) ||
               'Adjacent Grounds?: ' || ADJCNT_GRND_IND || chr(10) || chr(13) ||
               'Unlicensed Area?: ' || UNLIC_AREA_IND || chr(10) || chr(13) ||
               'Applicant Owns Building?: ' || APPL_OWN_BLDG_IND || chr(10) || chr(13) ||
               'MTG_BLDG_IND: ' || MTG_BLDG_IND || chr(10) || chr(13) ||
               'Building Onwer Name: ' || (select nm.name from
                                            abc11p.name_mstr nm
                                         where nm.name_mstr_id_num = BLDG_NAME_MSTR_ID_NUM) || chr(10) || chr(13) ||
               'Applicant Leases Building?: ' || APPL_LEASE_BLDG_IND || chr(10) || chr(13) ||
               'Leasee Name: ' || (select nm.name from
                                     abc11p.name_mstr nm
                                    where nm.name_mstr_id_num = LEASE_NAME_MSTR_ID_NUM) text ,
                DT_ROW_UPDT CONVDATE,
                UPDT_OPER_ID CONVUSER,
                apd.CNTY_MUNI_CD ||'-'|| apd.LIC_TYPE_CD ||'-'|| apd.MUNI_SER_NUM ||'-'|| apd.GEN_NUM ||'-'|| apd.PRIV_SEQ_NUM || '-' || apd.bldg_num LegacyKey 
                from abc11p.ADD_PRIV_BLDG apd
               where apd.CNTY_MUNI_CD ||'-'|| apd.LIC_TYPE_CD ||'-'|| apd.MUNI_SER_NUM ||'-'|| apd.GEN_NUM ||'-'|| apd.PRIV_SEQ_NUM = c.legacykey) loop
      insert into dataconv.n_general
         (createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text)
     values (d.CONVUSER,
             d.CONVDATE,
             d.CONVUSER,
             d.CONVDATE,
             'o_abc_permit',
             c.LEGACYKEY,
             'Y',
              d.text); 
              
              for e in (select 
                'Floor Number: ' || apf.floor_num || chr(10) || chr(13) ||
                'All or Part: '|| apf.all_part_ind text,
                DT_ROW_UPDT CONVDATE,
                UPDT_OPER_ID CONVUSER
                from abc11p.add_priv_floor apf
               where  apf.CNTY_MUNI_CD ||'-'|| apf.LIC_TYPE_CD ||'-'|| apf.MUNI_SER_NUM ||'-'|| apf.GEN_NUM ||'-'|| apf.PRIV_SEQ_NUM || '-' || apf.bldg_num = d.LegacyKey
                ) loop
      insert into dataconv.n_general
         (createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text)
     values (e.CONVUSER,
             e.CONVDATE,
             e.CONVUSER,
             e.CONVDATE,
             'o_abc_permit',
             c.LEGACYKEY,
             'Y',
              e.text);             
    end loop; 
                          
    end loop;            
    
  /*  if mod(t_count,1000) = 0
      then
        commit;
    end if; */        
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
  commit;
  
end;   