--Run time: 2 Minutes
declare
  t_EncryptDOB      varchar2(4000);
  t_EncryptDOBDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'BirthDateEncrypted');
  t_DOBDefId        number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'BirthDate');
  t_LogTransId      number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_LogTransId := api.pkg_logicaltransactionupdate.CurrentTransaction();
  for i in (select l.objectid, to_char(nvl((select nm.dt_birth from abc11p.name_mstr nm where nm.name_stat != 'X' and nm.name = l.legalname and to_char(nm.name_mstr_id_num) = l.legacykey),
                               (select to_date(a.applicant_dob, 'MM-DD-YYYY HH24:MI:SS') from abc11p.ma_applicant_int_t a where a.applicant_id || 'MA' = l.legacykey)), 'YYYY-MM-DD HH24:MI:SS') BirthDate
              from dataconv.o_abc_legalentity l) loop
    
    if i.birthdate is not null then
      --Date of Birth
      t_EncryptDOB := abc.pkg_abc_encryption.encrypt(i.birthdate);
      --Set Encrypted Column
      update  possedata.objectcolumndata
      set AttributeValue = t_EncryptDOB,
          SearchValue = lower(substrb(t_EncryptDOB, 1, 20))
      where objectid = i.objectid
        and columndefid = t_EncryptDOBDefId;

    delete from possedata.objectcolumndata d
     where d.objectid = i.objectid
       and d.ColumnDefId = t_DOBDefId;
    end if;
  end loop;  
  api.pkg_logicaltransactionupdate.EndTransaction;
end;