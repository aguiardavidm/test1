delete 
 from possedata.objectcolumndata cd
 where cd.ObjectId in (select objectid from query.o_accessgroup where description like 'www%')
   and cd.ColumnDefId = api.pkg_configquery.ColumnDefIdForName('o_Accessgroup', 'IsAdministratable');
commit;