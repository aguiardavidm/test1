declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'Permittee_Permit');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'Permittee_Permit');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitPermittee');
  t_RelId         number;
begin
  for i in (select *
              from (select x.permitobjectid, a.perm_num, a.perm_name, (select max(objectid)
                                                                 from dataconv.o_abc_legalentity le
                                                                where le.legacykey = (select max(abc11puk) || 'PERM2'
                                                                                        from abc11p.permit sp
                                                                                       where sp.perm_name = a.perm_name
                                                                                         and sp.po_box || sp.zip_1_5 ||sp.zip_6_9 || sp.st_prov_cd || sp.str_name || sp.str_num || sp.city = 
                                                                                             a.po_box || a.zip_1_5 ||a.zip_6_9 || a.st_prov_cd || a.str_name || a.str_num || a.city
                                                                                       group by sp.perm_name, sp.po_box || sp.zip_1_5 ||sp.zip_6_9 || sp.st_prov_cd || sp.str_name || sp.str_num || sp.city)) leid
              from (select o.ObjectId PermitObjectId
                      from query.o_ABC_Permit o
                      join query.r_ABC_PermitPermitType r
                        on o.ObjectId = r.PermitId
                     where r.PermitTypeId in (select p.objectid
                      from query.o_abc_permittype p
                     where p.LicenseRequired in('Optional','N/A')
                        or (p.LicenseRequired = 'Required' and p.PermitteeIsLicensee = 'N')
                       and p.objectdeftypeid=1)

            minus

            select r2.PermitObjectId
              from query.r_ABC_PermitPermittee r2) x
              join dataconv.o_abc_permit d on d.objectid = x.permitobjectid
              join abc11p.permit a on a.perm_num = d.legacykey) z
            where z.leid is not null) loop
    --Create the Rel
    select possedata.ObjectId_s.nextval
      into t_RelId
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.leid,
      i.permitobjectid
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.permitobjectid,
      i.leid
    );
  end loop;
  commit;
end;
/

declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'Permittee_Permit');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'Permittee_Permit');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitPermittee');
  t_RelId         number;
begin
  for i in (select *
              from (select x.permitobjectid, a.perm_num, a.perm_name, (select max(objectid)
                                                                 from dataconv.o_abc_legalentity le
                                                                where le.legacykey = (select max(sp.perm_name) || 'PERM'
                                                                                        from abc11p.permit sp
                                                                                       where sp.perm_name = a.perm_name
                                                                                         and sp.po_box || sp.zip_1_5 ||sp.zip_6_9 || sp.st_prov_cd || sp.str_name || sp.str_num || sp.city = 
                                                                                             a.po_box || a.zip_1_5 ||a.zip_6_9 || a.st_prov_cd || a.str_name || a.str_num || a.city
                                                                                       group by sp.perm_name, sp.po_box || sp.zip_1_5 ||sp.zip_6_9 || sp.st_prov_cd || sp.str_name || sp.str_num || sp.city)) leid
              from (select o.ObjectId PermitObjectId
                      from query.o_ABC_Permit o
                      join query.r_ABC_PermitPermitType r
                        on o.ObjectId = r.PermitId
                     where r.PermitTypeId in (select p.objectid
                      from query.o_abc_permittype p
                     where p.LicenseRequired in('Optional','N/A')
                        or (p.LicenseRequired = 'Required' and p.PermitteeIsLicensee = 'N')
                       and p.objectdeftypeid=1)

            minus

            select r2.PermitObjectId
              from query.r_ABC_PermitPermittee r2) x
              join dataconv.o_abc_permit d on d.objectid = x.permitobjectid
              join abc11p.permit a on a.perm_num = d.legacykey) z
            where z.leid is not null) loop
    --Create the Rel
    select possedata.ObjectId_s.nextval
      into t_RelId
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.leid,
      i.permitobjectid
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.permitobjectid,
      i.leid
    );
  end loop;
  commit;
end;
/

declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'Permittee_Permit');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'Permittee_Permit');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitPermittee');
  t_RelId         number;
begin
  for i in (select *
              from (select y.PermitObjectId, (select listagg(le.objectid, ', ') within group (order by 1)
                                   from dataconv.o_abc_legalentity le
                                   join abc11p.permit p on p.ABC11PUK || 'PERM2' = LE.LEGACYKEY
                                  where le.legalname = y.perm_name
                                    and p.po_box || p.zip_1_5 ||p.zip_6_9 || p.st_prov_cd || p.str_name || p.str_num || p.city = y.address
                                  group by le.legalname) LEObjectId
                      from (select *
                              from (select x.permitobjectid, a.perm_num, a.perm_name, a.po_box || a.zip_1_5 ||a.zip_6_9 || a.st_prov_cd || a.str_name || a.str_num || a.city address, (select max(objectid)
                                                                                                                                                                                         from dataconv.o_abc_legalentity le
                                                                                                                                                                                        where le.legacykey = (select max(sp.perm_name) || 'PERM'
                                                                                                                                                                                                                from abc11p.permit sp
                                                                                                                                                                                                               where sp.perm_name = a.perm_name
                                                                                                                                                                                                                 and sp.po_box || sp.zip_1_5 ||sp.zip_6_9 || sp.st_prov_cd || sp.str_name || sp.str_num || sp.city = 
                                                                                                                                                                                                                     a.po_box || a.zip_1_5 ||a.zip_6_9 || a.st_prov_cd || a.str_name || a.str_num || a.city
                                                                                                                                                                                                               group by sp.perm_name, sp.po_box || sp.zip_1_5 ||sp.zip_6_9 || sp.st_prov_cd || sp.str_name || sp.str_num || sp.city)) leid
                                      from (select o.ObjectId PermitObjectId
                                              from query.o_ABC_Permit o
                                              join query.r_ABC_PermitPermitType r
                                                on o.ObjectId = r.PermitId
                                             where r.PermitTypeId in (select p.objectid
                                              from query.o_abc_permittype p
                                             where p.LicenseRequired in('Optional','N/A')
                                                or (p.LicenseRequired = 'Required' and p.PermitteeIsLicensee = 'N')
                                               and p.objectdeftypeid=1)

                                            minus

                                            select r2.PermitObjectId
                                              from query.r_ABC_PermitPermittee r2) x
                              join dataconv.o_abc_permit d on d.objectid = x.permitobjectid
                              join abc11p.permit a on a.perm_num = d.legacykey) z
                            where z.leid is null) y) w 
 where w.leobjectid is not null) loop
    --Create the Rel
    select possedata.ObjectId_s.nextval
      into t_RelId
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.leobjectid,
      i.permitobjectid
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.permitobjectid,
      i.leobjectid
    );
  end loop;
  commit;
end;
/

declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'Permittee_Permit');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'Permittee_Permit');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitPermittee');
  t_RelId         number;
begin
  for i in (select *
              from (select dp.objectid PermitObjectId, (select max(le.objectid)
                                            from dataconv.o_abc_legalentity le
                                           where le.legalname = ap.perm_name) LEObjectId
              from dataconv.o_abc_permit dp
              join abc11p.permit ap on ap.perm_num = dp.legacykey
             where dp.objectid in (select o.ObjectId PermitObjectId
                                  from query.o_ABC_Permit o
                                  join query.r_ABC_PermitPermitType r
                                    on o.ObjectId = r.PermitId
                                 where r.PermitTypeId in (select p.objectid
                                  from query.o_abc_permittype p
                                 where p.LicenseRequired in('Optional','N/A')
                                    or (p.LicenseRequired = 'Required' and p.PermitteeIsLicensee = 'N')
                                   and p.objectdeftypeid=1)

                        minus

                        select r2.PermitObjectId
                          from query.r_ABC_PermitPermittee r2)) 
             where LeObjectid is not null) loop
    --Create the Rel
    select possedata.ObjectId_s.nextval
      into t_RelId
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.leobjectid,
      i.permitobjectid
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.permitobjectid,
      i.leobjectid
    );
  end loop;
  commit;
end;