--Run time: 1 second
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_LegalEntityType varchar2(100);
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
  update abc11p.ma_applicant_int_t am
        set am.errortext = a_ErrorText,
            am.errorcode = a_ErrorCode
      where am.rowid = a_RowId;  
  
    commit;
  end;

begin

  select legacykey
    into t_ConvUserKey 
   from dataconv.u_users u
  where u.oraclelogonid = 'ABCCONV';
  
  select legacykey
    into t_LegalEntityType
   from dataconv.o_abc_legalentitytype l
  where l.name = 'An Individual';

  for c in (select rowid,
                   t.applicant_id || 'MA' LegacyKey,
                   t_ConvUserKey UserKey,
                   to_date(t.applicant_timestamp, 'MM/DD/YY HH24:MI:SS')  ConvDate,
                   t_LegalEntityType legalentitytypelk,
                   t.applicant_name FirstName,
                   to_date(t.applicant_dob, 'MM/DD/YY HH24:MI:SS') Birthdate,
                   t.applicant_name ContactName,
                   t.applicant_phone ContactPhone,
                   t.applicant_email ContactEmail,
                   'EMAIL' PreferredContactMethod,
                   case when t.applicant_retire_flag = 1 then 'Y' else 'N' end Active,
                   'Y' Edit  
             from abc11p.ma_applicant_int_t t
            where t.processed is null) loop
    begin
    insert into dataconv.o_abc_legalentity a
    (LEGACYKEY, 
     CREATEDBYUSERLEGACYKEY, 
     CONVCREATEDDATE, 
     LASTUPDATEDBYUSERLEGACYKEY,
     LASTUPDATEDDATE,
     legalentitytypelk,
     FIRSTNAME,
     BIRTHDATE,
     CONTACTNAME,
     CONTACTPHONENUMBER,
     CONTACTEMAILADDRESS,
     PREFERREDCONTACTMETHOD,
     ACTIVE,
     EDIT   )
    values ( c.LEGACYKEY, 
             c.UserKey, 
             c.ConvDate, 
             c.UserKey,
             c.ConvDate,
             c.legalentitytypelk,
             c.FIRSTNAME,
             c.BIRTHDATE,
             c.CONTACTNAME,
             c.ContactPhone,
             c.ContactEmail,
             c.PREFERREDCONTACTMETHOD,
             c.ACTIVE,
             c.EDIT   );    
      
    t_count := t_count +1; 
    
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;
