--Run time: 1 minute
update dataconv.o_abc_establishment e
set e.physicaladdresslk = null
where e.physicaladdresslk not in (select legacykey from dataconv.o_abc_address);
commit;

update dataconv.o_abc_establishment e
set e.mailingaddresslk = null
where e.mailingaddresslk not in (select legacykey from dataconv.o_abc_address);
commit;

update dataconv.o_abc_establishmenthistory e
set e.physicaladdresslk = null
where e.physicaladdresslk not in (select legacykey from dataconv.o_abc_address);
commit;

update dataconv.o_abc_establishmenthistory e
set e.mailingaddresslk = null
where e.mailingaddresslk not in (select legacykey from dataconv.o_abc_address);
commit;

update dataconv.o_abc_legalentity e
set e.physicaladdresslk = null
where e.physicaladdresslk not in (select legacykey from dataconv.o_abc_address);
commit;

update dataconv.o_abc_legalentity e
set e.mailingaddresslk = null
where e.mailingaddresslk not in (select legacykey from dataconv.o_abc_address);
commit;

update dataconv.o_abc_vehicle e
set e.addresslk = null
where e.addresslk not in (select legacykey from dataconv.o_abc_address);
commit;

update dataconv.o_abc_establishment e
set e.mailingaddresslk = e.physicaladdresslk
where e.mailingaddresslk is null;
commit;

update dataconv.o_abc_legalentity e
set e.mailingaddresslk = e.physicaladdresslk
where e.mailingaddresslk is null;
commit;