declare 
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'ContactName');
begin
  for i in (select le.objectid, le.legalname Name
              from dataconv.o_abc_legalentity le
             where api.pkg_columnquery.Value(le.objectid, 'ContactName') is null
--              join abc11p.permit p on p.perm_name || 'PERM' = le.legacykey
             --group by le.objectid
             ) loop
    if i.name is not null then
      delete from possedata.objectcolumndata cd 
       where cd.ObjectId = i.objectid 
         and cd.ColumnDefId = t_ColumnDefId;
         
      insert into possedata.objectcolumndata_t
      (
      LOGICALTRANSACTIONID,
      OBJECTID,
      COLUMNDEFID,
      ATTRIBUTEVALUE,
      SEARCHVALUE
      )
      values
      (
      1,
      i.objectid,
      t_ColumnDefId,
      i.name,
      lower(substrb(i.Name, 1, 20))
      );
    end if;
  end loop;
  commit;
end;