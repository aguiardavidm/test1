-- Created on 5/7/2014 by PAUL 
--Expected run time: 75 minutes
declare 
  -- Local variables here
  i                      integer;
  t_RowsProcessed        number := 0;
  t_CmtText              varchar2(4000);
  t_CurrentEstLK         varchar2(40);
  t_Abc_Num              varchar2(40);
  t_EstState             varchar2(1);
  t_DoingBusinessAs      varchar2(4000);
  t_DoingBusinessAsShort varchar2(400);
  t_DoingBusinessAsLong  varchar2(4000);
  t_EstablishmentType    varchar2(40);
  t_TypeCode             varchar2(2);
  t_OperatorLK           number;
begin
  for i in (select nm.*
              from abc11p.name_mstr nm
             where nm.name_type_cd = '2.1-LICENSEE'
               and nm.name_stat != 'X'
               and not exists (select 1
                                 from dataconv.o_abc_establishment e
                                where e.legacykey = to_char(nm.name_mstr_id_num))
               and not exists (select 1
                                 from dataconv.o_abc_establishmentHistory e
                                where e.legacykey = nm.name_mstr_id_num)
             order by substr(nm.abc_num, 6, 2), nm.name_mstr_id_num) loop
    --Get the Establishment Type once per type
    if t_TypeCode is null or t_TypeCode != substr(i.abc_num, 6, 2) then
      select max(LegacyKey), substr(i.abc_num, 6, 2)
        into t_EstablishmentType, t_TypeCode
        from dataconv.o_Abc_Establishmenttype et
       where upper(et.name) = (Select upper(lt.name)
                                 from query.o_Abc_Licensetype lt
                                where lt.ObjectDefTypeId = 1
                                  and lt.Code = substr(i.abc_num, 6, 2)
                                  and lt.Active = 'Y')
         and et.active = 'Y';
    end if;
    --Get the state of the Establishment
    select (case
              when substr(i.abc_num, 13, 3) = (select max(substr(nm2.abc_num, 13, 3))
                                                  from abc11p.name_mstr nm2
                                                 where substr(nm2.abc_num, 0, 11) = substr(i.abc_num, 0, 11)
                                                   and nm2.name_stat != 'X'
                                                   and nm2.name_type_cd = '2.1-LICENSEE')
                then 'C' --this is the most current record
              else 'H' --This is a historical record
            end) State
      into t_EstState
      from dual;
    --Get the DBA(s) for the Establishment
    select max(db.name)
      into t_DoingBusinessAsShort
      from abc11p.name_mstr db
     where db.name_mstr_id_num = (select min(db2.name_mstr_id_num)
                                    from abc11p.name_mstr db2
                                   where db2.name_type_cd in ('2.5-D/B/A', '2.6-D/B/A')
                                     and db2.abc_num = i.abc_num);

    select max(listagg(db.name, ', ') within group (order by db.abc_num))
      into t_DoingBusinessAsLong
      from abc11p.name_mstr db
     where db.name_type_cd in ('2.5-D/B/A', '2.6-D/B/A')
       and db.abc_num = i.abc_num
       and db.name_mstr_id_num != (select min(db2.name_mstr_id_num)
                                    from abc11p.name_mstr db2
                                   where db2.name_type_cd in ('2.5-D/B/A', '2.6-D/B/A')
                                     and db2.abc_num = i.abc_num)
      group by db.abc_num;

    --Not all records will have an email
    begin
      select cmt_text
        into t_CmtText
        from abc11p.lic_cmt
       where CNTY_MUNI_CD = i.CNTY_MUNI_CD
         and LIC_TYPE_CD = i.LIC_TYPE_CD
         and MUNI_SER_NUM = i.MUNI_SER_NUM
         and GEN_NUM = i.GEN_NUM
         and instr(cmt_text, '@') != 0;
    exception when no_data_found then --There is no email address, just pass
      null;
    end;
    --Get the Operator
    begin
    select max(n.name_mstr_id_num)
      into t_OperatorLK
      from abc11p.name_mstr n
      join abc11p.position p on p.name_mstr_id_num = n.name_mstr_id_num
     where n.abc_num = i.abc_num
       and p.position_cd = 'OWNER'
       and n.name_stat != 'X';
     exception when no_data_found then
       null;
     end;
BEGIN
    if t_EstState = 'C' then
      insert into dataconv.o_abc_establishment
        (
        MAILINGADDRESSLK,
        LEGACYKEY,
        CREATEDBYUSERLEGACYKEY,
        CONVCREATEDDATE,
        LASTUPDATEDBYUSERLEGACYKEY,
        LASTUPDATEDDATE,
        DOINGBUSINESSAS,
        ADDITIONALDBANAMES,
        CONTACTNAME,
        CONTACTPHONENUMBER,
        CONTACTALTPHONENUMBER,
        CONTACTFAXNUMBER,
        CONTACTEMAILADDRESS,
        PREFERREDCONTACTMETHOD,
        ACTIVE,
        OPERATORLK,
        PHYSICALADDRESSLK,
        ESTABLISHMENTTYPELK
        )
        values
        (
        i.abc11puk || 'MAIL',
        i.name_mstr_id_num,
        i.CRTD_OPER_ID,
        i.DT_ROW_CRTD,
        i.UPDT_OPER_ID,
        i.DT_ROW_UPDT,
        nvl(t_DoingBusinessAsShort,'Not Available'),
        t_DoingBusinessAsLong,
        i.Name,
        i.BUS_AREA_CD || i.BUS_PREFIX || i.BUS_SUFFIX,
        i.MAIL_AREA_CD || i.MAIL_PREFIX || i.MAIL_SUFFIX,
        i.HOME_AREA_CD || i.HOME_PREFIX || i.HOME_SUFFIX,
        t_CmtText,
        'Mail',
        decode(i.name_stat, 'C', 'Y', 'N'),
        t_OperatorLK,
        i.abc11puk || 'PREM',
        t_EstablishmentType
        );
      elsif t_EstState = 'H' then
        insert into dataconv.o_abc_establishmenthistory
        (
        MAILINGADDRESSLK,
        LEGACYKEY,
        CREATEDBYUSERLEGACYKEY,
        CONVCREATEDDATE,
        LASTUPDATEDBYUSERLEGACYKEY,
        LASTUPDATEDDATE,
        DOINGBUSINESSAS,
        ADDITIONALDBANAMES,
        CONTACTNAME,
        CONTACTPHONENUMBER,
        CONTACTALTPHONENUMBER,
        CONTACTFAXNUMBER,
        CONTACTEMAILADDRESS,
        PREFERREDCONTACTMETHOD,
        ACTIVE,
        OPERATORLK,
        PHYSICALADDRESSLK,
        ESTABLISHMENTTYPELK
        )
        values
        (
        i.abc11puk || 'MAIL',
        i.name_mstr_id_num,
        i.CRTD_OPER_ID,
        i.DT_ROW_CRTD,
        i.UPDT_OPER_ID,
        i.DT_ROW_UPDT,
        t_DoingBusinessAsShort,
        t_DoingBusinessAsLong,
        i.Name,
        i.BUS_AREA_CD || i.BUS_PREFIX || i.BUS_SUFFIX,
        i.MAIL_AREA_CD || i.MAIL_PREFIX || i.MAIL_SUFFIX,
        i.HOME_AREA_CD || i.HOME_PREFIX || i.HOME_SUFFIX,
        t_CmtText,
        'Mail',
        decode(i.name_stat, 'C', 'Y', 'N'),
        t_OperatorLK,
        i.abc11puk || 'PREM',
        t_EstablishmentType
        );
        --Create the Rel to the current establishment
        select max(nm.abc_num)
          into t_Abc_Num
          from abc11p.name_mstr nm
         where substr(nm.abc_num, 0, 11) = substr(i.abc_num, 0, 11)
           and nm.name_type_cd = '2.1-LICENSEE'
           and nm.name_stat != 'X';
        select max(nm2.name_mstr_id_num)
          into t_CurrentEstLK
          from abc11p.name_mstr nm2
         where nm2.abc_num = t_Abc_Num
           and nm2.name_type_cd = '2.1-LICENSEE'
           and nm2.name_stat != 'X';
        insert into dataconv.r_Abc_Establishmentesthistory
        (
        O_ABC_ESTABLISHMENT,
        O_ABC_ESTABLISHMENTHISTORY
        )
        values
        (
        t_CurrentEstLK,
        i.name_mstr_id_num
        );
      end if;
EXCEPTION WHEN OTHERS THEN
  dbms_output.put_line(sqlerrm || i.name);
end;
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
    end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  commit;
end;
