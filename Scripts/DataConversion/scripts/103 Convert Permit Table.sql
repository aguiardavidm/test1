--Run time: 1 minute
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_PermitType      varchar2(100);
  t_NoteText        long := '';
  t_LastSolNum      number(6);
  t_LEGALENTITYTYPELK varchar2(400);
  
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin  
    dbms_output.put_line('Error: '|| a_ErrorText || 'ErrorCode: ' ||a_ErrorCode);
  end;

begin
  select legacykey
    into t_LEGALENTITYTYPELK
    from dataconv.o_abc_legalentitytype lt
   where lt.name = 'Converted';
for i in (select p.perm_name,
                 Min(p.CRTD_OPER_ID) CREATEDBYUSERLEGACYKEY,
                 min(p.DT_ROW_CRTD) CONVCREATEDDATE,
                 max(p.UPDT_OPER_ID) LASTUPDATEDBYUSERLEGACYKEY,
                 max(p.DT_ROW_UPDT) LASTUPDATEDDATE
            from abc11p.Permit p
           where p.perm_stat in ('C', 'H')
             and p.cnty_muni_cd is null
           group by p.perm_name) loop
  insert into dataconv.o_abc_legalentity
  (
  LEGACYKEY,
  createdbyuserlegacykey,
  convcreateddate,
  lastupdatedbyuserlegacykey,
  lastupdateddate,
  LEGALNAME,
  LEGALENTITYTYPELK
  )
  values
  (
  i.perm_name || 'PERM',
  i.CREATEDBYUSERLEGACYKEY,
  i.CONVCREATEDDATE,
  i.LASTUPDATEDBYUSERLEGACYKEY,
  i.LASTUPDATEDDATE,
  i.perm_name,
  t_LEGALENTITYTYPELK
  );
end loop;


for c in (select p.rowid,
                p.PERM_NUM LEGACYKEY,
        p.PERM_NUM PERMITNUMBER,
        p.CRTD_OPER_ID CREATEDBYUSERLEGACYKEY,
        p.DT_ROW_CRTD CONVCREATEDDATE,
        p.UPDT_OPER_ID LASTUPDATEDBYUSERLEGACYKEY,
        p.DT_ROW_UPDT LASTUPDATEDDATE,
        CASE WHEN PERM_TYPE_CD = 'GEN' then 'MISC'
          WHEN  PERM_TYPE_CD = 'GEN2' then 'MISC'
          WHEN PERM_TYPE_CD = 'SEP' then 'EP'
          ELSE PERM_TYPE_CD
          END   PERMITTYPELK,
        (case p.PERM_STAT when 'C' then 'Active' when 'H' then 'Expired' end) STATE,
        p.CNTY_MUNI_CD || p.LIC_TYPE_CD || p.MUNI_SER_NUM || p.GEN_NUM LICENSELK,
        case
          when dt_effective is not null then dt_effective
          else 
          case when exists (select 1 from abc11p.perm_event e where e.perm_num = p.perm_num)
          then (select min(e.dt_event) from abc11p.perm_event e where e.perm_num = p.perm_num)
          else dt_issued
          end       
          end EFFECTIVEDATE,
         case
          when dt_expire is not null then dt_expire
          else 
          case when exists (select 1 from abc11p.perm_event e where e.perm_num = p.perm_num)
          then (select max(e.dt_event) from abc11p.perm_event e where e.perm_num = p.perm_num)
          else
            case perm_type_cd
            when 'SP' then add_months(dt_issued,12)-1 --1 year from issuance
        else
            case 
               when length(substr(p.lic_yr, instr(p.lic_yr,'-')+1)) =4 then
              case perm_type_cd
               when 'CTW' then to_date('06/30/'||substr(p.lic_yr, instr(p.lic_yr,'-')+1),'MM/DD/YYYY') --end of June
            when 'LTP' then to_date('09/30/'||substr(p.lic_yr, instr(p.lic_yr,'-')+1),'MM/DD/YYYY') --end of September
            when 'EMP' then to_date('03/31/'||substr(p.lic_yr, instr(p.lic_yr,'-')+1),'MM/DD/YYYY') --end of March
            when 'COOP' then to_date('07/31/'||substr(p.lic_yr, instr(p.lic_yr,'-')+1),'MM/DD/YYYY') --end of July
             end
             end
            end
          end       
          end EXPIRATIONDATE,
          DT_ISSUED ISSUEDATE,
          EVENT_TYPE || chr(13)||chr(10)|| EVENT_PLACE EventDetails,
          coalesce(EVENT_STR_NUM || EVENT_STR_NAME || EVENT_CITY || EVENT_ST_PROV_CD || EVENT_ZIP_1_5 || EVENT_ZIP_6_9,
                         STR_NUM || STR_NAME || PO_BOX || CITY || ST_PROV_CD || ZIP_1_5 ||ZIP_6_9 ||CNTRY_CD ||FOR_POST_CD ) EVENTLOCATIONADDRESSLK,
          OTH_CNTY_MUNI_CD || OTH_LIC_TYPE_CD || OTH_MUNI_SER_NUM || OTH_GEN_NUM SellersLicenseNumberLK,
          substr(EVENT_CNTY_MUNI_CD,1,2) COUNTYLK,
          EVENT_CNTY_MUNI_CD MUNICIPLALITYLK,
          (select count(*) 
           from abc11p.perm_solicitor ps
          where ps.PERM_NUM = p.PERM_NUM)NumberOfSolicitors,
          DT_EFFECTIVE PERMITTERMFROM,
          DT_EXPIRE PERMITTERMTO,
          ('Converted PERMIT information' || chr(10) ||
          'Brand Name: ' || BRN_NAME || chr(10) ||
          'Quantity: ' || QUANTITY || chr(10) ||
          'Price: ' || PRICE || chr(10) ||
          'Item Size: ' || ITEM_SIZE || chr(10) ||
          'Product: ' || PRODUCT || chr(10) ||
          'Age: ' || AGE || chr(10) ||
          'Hair Color: ' || HAIR_COLOR || chr(10) ||
          'Eye Color: ' || EYE_COLOR || chr(10) ||
          'Date of Birth: ' || DOB || chr(10) ||
          'Height Feet: ' || HT_FT || chr(10) ||
          'Height Inches: ' || HT_INCH || chr(10) ||
          'Weight: ' || WEIGHT || chr(10) ||
          'Sex: ' || SEX || chr(10) ||
          'Other State License Number: ' || OTH_ST_LIC_NUM || chr(10) ||
          'FED_PERM_NUM: ' || FED_PERM_NUM || chr(10) ||
          'FILE_NUM: ' || FILE_NUM || chr(10) ||
          'Vintage: ' || VINTAGE) text,
          p.perm_name,
          p.cnty_muni_cd
            from abc11p.Permit p
      where p.perm_stat in ('C', 'H')) loop
    begin
    insert into dataconv.o_abc_permit a
    (   LEGACYKEY,
    PERMITNUMBER,
    CREATEDBYUSERLEGACYKEY,
    CONVCREATEDDATE,
    LASTUPDATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE,
    PERMITTYPELK,
    STATE,
--    LICENSELK, moved to relationship view
    EFFECTIVEDATE,
    EXPIRATIONDATE,
    ISSUEDATE,
    EventDetails,
    EVENTLOCATIONADDRESSLK,
    SELLERSLICENSELK,
    COUNTYLK,
    MUNICIPALITYLK,
    NumberOfSolicitors,
    PERMITTERMFROM,
    PERMITTERMTO,
    PERMITTEELK
      )
    values (c.LEGACYKEY,
    c.PERMITNUMBER,
    c.CREATEDBYUSERLEGACYKEY,
    c.CONVCREATEDDATE,
    c.LASTUPDATEDBYUSERLEGACYKEY,
    c.LASTUPDATEDDATE,
    c.PERMITTYPELK,
    c.STATE,
--    c.LICENSELK, moved to relationship view
    c.EFFECTIVEDATE,
    c.EXPIRATIONDATE,
    c.ISSUEDATE,
    c.EventDetails,
    c.EVENTLOCATIONADDRESSLK,
    c.SellersLicenseNumberLK,
    c.COUNTYLK,
    c.MUNICIPLALITYLK,
    c.NumberOfSolicitors,
    c.PERMITTERMFROM,
    c.PERMITTERMTO,
    (case when c.cnty_muni_cd is null then c.perm_name || 'PERM' end));

  if c.licenselk is not null then
    insert into dataconv.r_PermitLicense
    (
    O_ABC_LICENSE,
    O_ABC_PERMIT
    )
    values
    (
    c.LICENSELK,
    c.LEGACYKEY
    );
  end if;
  insert into dataconv.n_general
         (createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text)
     values (c.CREATEDBYUSERLEGACYKEY,
             c.CONVCREATEDDATE,
             c.LASTUPDATEDBYUSERLEGACYKEY,
             c.LASTUPDATEDDATE,
             'o_abc_permit',
             c.LEGACYKEY,
             'Y',
              c.text);  
    t_count := t_count +1; 

/*  if mod(t_count,1000) = 0
      then
        commit;
    end if; */        
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;
