--Run time: 1 second
-- Created on 2/28/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
  t_RowsProcessed    number := 0;
  t_ConversionNote   long;

begin
  -- Test statements here
  for i in (select *
             from abc11p.rest_brew r
             join dataconv.o_abc_license l on l.licensenumber = r.cnty_muni_cd || '-' || r.lic_type_cd || '-' ||  r.muni_ser_num || '-' || r.gen_num
) loop
    update dataconv.o_abc_license
       set RestrictedBreweryProduction = i.max_barrels
     where licensenumber = i.cnty_muni_cd || '-' || i.lic_type_cd || '-' ||  i.muni_ser_num || '-' || i.gen_num;
    t_ConversionNote := 'Converted REST_BREW Information' || chr(10) ||
                        'Plenary Retail Consumption License: ' || i.PRCL_CNTY_MUNI_CD ||'-'|| i.PRCL_LIC_TYPE_CD ||'-'|| i.PRCL_MUNI_SER_NUM ||'-'|| i.PRCL_GEN_NUM|| chr(10) ||
                        '' || i.ON_PREM_CONS_IND|| chr(10) ||
                        '' || i.OFF_PREM_CONS_IND|| chr(10) ||
                        'Paper Name: ' || i.PAPER_NAME|| chr(10) ||
                        'Date of 1st Notice: ' || i.DT_1ST_NOTICE|| chr(10) ||
                        'Date of 2nd Notice: ' || i.DT_2ND_NOTICE;
    --Create the Conversion Note
    insert into dataconv.n_general
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    parenttablename,
    parentlegacykey,
    locked,
    text
    )
    values
    (
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    'O_ABC_LICENSE',
    i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
    'Y',
    t_ConversionNote
    );
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
    end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  commit;
end;