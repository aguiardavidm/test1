-- run as sysdba

-- written for a POSSE 7. database

-- script to truncate all runtime data from a POSSE database
-- BE VERY CAREFUL ON WHICH DATABASE THIS IS RUN!
-- Enter the SID and Host of database to be truncated at the top of the script

-- ALL objects are removed except for the PosseSys user.
-- To retain Metadata objects use the Metadata Transfer Tool to export them
-- BEFORE running this script.

-- Be aware that all external objects will have to be reregistered
--   or the external tables truncated manually.
-- See objmodelphys.ExternalObjectTypes for list of external object defs.

-- Note that doc.DocumentBlobs_t can take a long time to delete.
--   (10 minutes for 200k rows totaling around 15G in the tablespace)
--   If they are in a separate tablespace they could be done manually first.

-- Re-running this script:
--   Any existing backup tables will be left alone.  If the script fails
--   because one of the backup tables has no rows the table will have to be
--   dropped before it will be reloaded.

--   If the script is re-run after it has started to Restore saved rows from
--   backup then the successfully restored tables must be commented out in 
--   RestoreRowsFromBackup() because the backup tables will have been dropped
--   BE SURE TO UNCOMMENT THE LINES AFTER
--   FIX ME - enhance the script to handle this automatically

-- Start a debug session or comment out the pkg_debug.Enable call.

declare
  -- specify SID and Host of database to be truncated here
  gc_DbName                             constant varchar2(10) := 'KansasPR';
  gc_DBHost                             constant varchar2(20) := 'posseproddb';

  type udt_TableNames is table of varchar2(61);  
  g_RunTimeTables                      udt_TableNames;

  t_DbName                              varchar2(10);
  t_DbHost                              varchar2(200);

  -----------------------------------------------------------------------------
  -- RaiseError()
  -----------------------------------------------------------------------------
  procedure RaiseError (
    a_Message                           varchar2
  ) is
  begin
    pkg_debug.putline('ERROR: ' || a_Message);
    raise_application_error(-20000, a_Message); 
  end RaiseError;

  -----------------------------------------------------------------------------
  -- AlterConstraints()
  -----------------------------------------------------------------------------
  procedure AlterConstraints (
    a_Action                            varchar2
  ) is
    t_Sql                               varchar2(4000);

    cursor c_Constraints is
      select owner,table_name,constraint_name
        from metadata.schemas s,
             all_constraints ac
       where ac.owner = s.name
        and constraint_type = 'R'
        and ac.status = decode(a_Action, 'enable', 'DISABLED', 'ENABLED')
      order by owner,table_name,constraint_name;
  begin
    for c in c_Constraints loop
      pkg_debug.PutLine(a_Action || ' Constraint ' || c.constraint_name
          || ' on ' || c.owner || '.' || c.table_name);
      t_Sql := 'ALTER TABLE ' || c.owner || '.' || c.table_name || ' '
          || a_Action || ' CONSTRAINT ' || c.constraint_name;
      --pkg_Debug.PutLine(t_Sql);
      execute immediate t_Sql;
    end loop;
  end;

  -----------------------------------------------------------------------------
  -- CreateBackupTable()
  -----------------------------------------------------------------------------
  procedure CreateBackupTable (
    a_SourceOwner                       varchar2,
    a_SourceTable                       varchar2,
    a_WhereClause                       varchar2,
    a_RowsRequired                      boolean default true
  ) is
    t_Count                             number;
  begin
    pkg_Debug.putline('Backup ' || a_SourceTable);

    begin
      execute immediate
      'create table possedba.' || a_SourceTable ||
          'BAK tablespace largedata as ' ||
        'select * ' ||
        'from ' || a_SourceOwner || '.' || a_SourceTable || ' ' ||
        a_WhereClause;
    exception
    when others then
      -- ok if table already exists, will check row count later
      null;
    end;

    execute immediate
    'select count(*) ' ||
    'from possedba.' || a_SourceTable || 'BAK'
    into t_Count;

    if t_Count < 1 and a_RowsRequired then
      RaiseError('No ' || a_SourceTable || ' backup.');
    else
      pkg_Debug.putline('  ' || t_Count || ' rows saved');
    end if;
  end;

  -----------------------------------------------------------------------------
  -- InsertSavedRows()
  -----------------------------------------------------------------------------
  procedure InsertSavedRows (
    a_SourceOwner                       varchar2,
    a_SourceTable                       varchar2,
    a_RowsRequired                      boolean default true
  ) is
    t_Count                             number;
  begin

    execute immediate
    'insert into ' || a_SourceOwner || '.' || a_SourceTable || ' ' ||
    'select * ' ||
    '  from possedba.' || a_SourceTable || 'BAK';

    if sql%rowcount < 1 and a_RowsRequired then
      RaiseError('No ' || a_SourceTable ||
          ' rows restored.');
    else
      pkg_Debug.putline(t_Count || ' ' || a_SourceTable || ' rows restored');
    end if;

    commit;

    execute immediate 'drop table possedba.' || a_SourceTable || 'BAK purge';
  end InsertSavedRows;

  -----------------------------------------------------------------------------
  -- PopulateRunTimeTableArray()
  -----------------------------------------------------------------------------
  procedure PopulateRunTimeTableArray is
  begin
    g_RunTimeTables := udt_TableNames(
        'ADDRESSING.OBJECTADDRESSES_T',
        'ADDRESSING.OBJECTDERIVEDADDRESSES_T',
        'ADMIN.USERLOGS_T',
        'ADMIN.USERS_T',
        'DATAMART.OBJECTSTOSYNCHRONIZE_T',
        'DATAMART.QUEUEDOBJECTS_T',
        'DATAMART.TABLESTOSYNCHRONIZE_T',
        'DOC.DOCUMENTBLOBS_T',
        'DOC.DOCUMENTREVISIONS_T',
        'DOC.DOCUMENTS_T',
        'FINANCE.FEES_T',
        'FINANCE.OPENFEES_T',
        'FINANCE.PAYMENTS_T',
        'FINANCE.PAYMENTTRANSACTIONS_T',
        'FINANCE.TRANSACTIONS_T',
        'INSPECT.DEFICIENCIES_T',
        'OBJMAINT.BLOCKCODEVIOLATIONS_T',
        'OBJMAINT.BLOCKNOTES_T',
        'OBJMAINT.BLOCKOBJECTADDRESSES_T',
        'OBJMAINT.BLOCKOBJECTBLOBS_T',
        'OBJMAINT.BLOCKOBJECTDATA_T',
        'OBJMAINT.BLOCKOBJECTS_T',
        'OBJMAINT.BLOCKRELATIONSHIPOBJECTS_T',
        'OBJMAINT.BLOCKS_T',
        'OBJMODELPHYS.EXTERNALOBJECTS_T',
        'OBJMODELPHYS.NOTES_T',
        'POSSEDATA.COLUMNKEYWORDS_T',
        'POSSEDATA.MEMORIZEDOBJECTS_T',
        'POSSEDATA.NOTEKEYWORDS_T',
        'POSSEDATA.OBJECTCOLUMNDATA_T',
        'POSSEDATA.OBJECTS_T',
        'POSSEDATA.PENDINGDOCUMENTS_T',
        'POSSEDATA.PENDINGSESSIONS_T',
        'POSSEDATA.SESSIONS_T',
        'POSSEDATA.USERPROFILESETTINGS_T',
        'POSSEDATA.USERPROFILESTATE_T',
        'POSSEAUDIT.ACCESSGROUPUSERS_T',
        'POSSEAUDIT.CODEVIOLATIONS_T',
        'POSSEAUDIT.DELETEDOBJECTS_T',
        'POSSEAUDIT.DOCUMENTREVISIONS_T',
        'POSSEAUDIT.FEEADJUSTMENTS_T',
        'POSSEAUDIT.FEES_T',
        'POSSEAUDIT.INSTANCESECUREDOBJECTS_T',
        'POSSEAUDIT.LOGICALTRANSACTIONS_T',
        'POSSEAUDIT.NOTES_T',
        'POSSEAUDIT.OBJECTADDRESSES_T',
        'POSSEAUDIT.OBJECTCOLUMNDATAEFFDATED_T',
        'POSSEAUDIT.OBJECTCOLUMNDATA_T',
        'POSSEAUDIT.OBJECTEFFECTIVEDATES_T',
        'POSSEAUDIT.OBJECTINITIALIZATIONLOG_T',
        'POSSEAUDIT.OBJECTPURGELOG_T',
        'POSSEAUDIT.OBJECTSECURITY_T',
        'POSSEAUDIT.OBJECTSINSYNC_T',
        'POSSEAUDIT.OBJECTS_T',
        'POSSEAUDIT.PAYMENTDISTRIBUTIONS_T',
        'POSSEAUDIT.PAYMENTS_T',
        'POSSEAUDIT.PROCESSASSIGNMENTS_T',
        'POSSEAUDIT.STOREDRELATIONSHIPS_T',
        'POSSEAUDIT.VOIDDISTRIBUTIONS_T',
        'REL.STOREDRELATIONSHIPS_T',
        'REMOTE.ACTIVITYLOG_T',
        'REMOTE.DOWNLOADLOG_T',
        'REMOTE.INSTALLATIONSTATEDATA_T',
        'REMOTE.INSTALLATIONS_T',
        'REMOTE.OBJECTS_T',
        'REMOTE.QUEUEDOBJECTS_T',
        'REMOTE.UPLOADLOG_T',
        'REMOTE.USEROBJECTS_T',
        'SCHEDULER.SYSSCHEDULE',
        'SCHEDULER.SYSSCHEDULELOG',
        'SECURITY.ACCESSGROUPUSERS_T',
        'SECURITY.INSTANCES_T',
        'SECURITY.INSTANCESECURITY_T',
        'WORKFLOW.INCOMPLETEPROCESSASSIGNMENTS_T',
        'WORKFLOW.JOBS_T',
        'WORKFLOW.PROCESSES_T',
        'WORKFLOW.PROCESSASSIGNMENTS_T'
    );
  end PopulateRunTimeTableArray;

  -----------------------------------------------------------------------------
  -- RestoreRowsFromBackup()
  -----------------------------------------------------------------------------
  procedure RestoreRowsFromBackup is
  begin
    pkg_debug.PutLine('Restore saved rows from backup tables.');

    InsertSavedRows('posseaudit', 'LogicalTransactions_t');
    InsertSavedRows('admin', 'Users_t');
    InsertSavedRows('possedata', 'Objects_t');
    InsertSavedRows('possedata', 'ObjectColumnData_t');
    InsertSavedRows('possedata', 'ColumnKeywords_t', false);
    InsertSavedRows('security', 'AccessGroupUsers_t');
    InsertSavedRows('security', 'InstanceSecurity_t');
    InsertSavedRows('security', 'Instances_t');
  end RestoreRowsFromBackup;

  -----------------------------------------------------------------------------
  -- SaveRequiredRows()
  --   Note: If a backup table is created with no rows it will not get loaded
  -- unless the table is dropped before re-running this script.
  -----------------------------------------------------------------------------
  procedure SaveRequiredRows is
    t_Count                             pls_integer;
  begin
    pkg_Debug.putline('Save Required Rows (to be reinserted later).');

    CreateBackupTable('posseaudit', 'LogicalTransactions_t',
        'where LogicalTransactionId = 1');
    CreateBackupTable('admin', 'Users_t', 'where UserId = 1');
    CreateBackupTable('possedata', 'ColumnKeywords_t',
        'where ObjectId in (select UserId from possedba.Users_tBAK)', false);
    CreateBackupTable('possedata', 'Objects_t',
        'where ObjectId in (select UserId from possedba.Users_tBAK)');
    CreateBackupTable('possedata', 'ObjectColumnData_t',
        'where ObjectId in (select UserId from possedba.Users_tBAK)');
    CreateBackupTable('security', 'AccessGroupUsers_t',
        'where UserId in (select UserId from possedba.Users_tBAK)');

    -- Instances and InstanceSecurity contain both config and run time data
    select count(*)
      into t_Count
      from security.InstanceSecurity;
    pkg_Debug.putline('Total InstanceSecurity row count: ' || t_Count);

    -- backup config security but exclude run time security for 
    -- Objects - 11, Notes - 18, Documents - 23, Jobs - 69
    CreateBackupTable('security', 'InstanceSecurity_t',
        'where ClassId not in (11, 18, 23, 69)');

    CreateBackupTable('security', 'Instances_t',
        'where ClassId not in (11, 18, 23, 69)');

    -- include rows for users being backed up
    execute immediate
    'insert into possedba.InstanceSecurity_tBAK ' ||
    'select * from security.InstanceSecurity_t ' ||
    'where (ClassId, InstanceId) in ( ' ||
    '  select ObjectDefId, ObjectId ' ||
    '  from possedba.Objects_tBAK)';
    commit;
  end SaveRequiredRows;
 
  -----------------------------------------------------------------------------
  -- SetLogicalTransactionIds()
  -----------------------------------------------------------------------------
  procedure SetLogicalTransactionIds is
    cursor c_LogicalTransactionTables is
      select c.owner, c.table_name
        from
          metadata.schemas s
          join all_tables t
            on t.owner = s.name
          join all_tab_columns c
            on c.owner = t.owner
            and c.table_name = t.table_name
        where c.column_name = 'LOGICALTRANSACTIONID'
        order by owner,table_name;
  begin
    -- LogicalTransactionId is part of the PK on this table so there can't
    -- be duplicates.  Keep all rows for ObjectDefs that have the greatest
    -- TxnId setting AuditEnabled on.  The others will be deleted.
    update posseaudit.ObjectDefAuditChanges set
      LogicalTransactionId = 1
    where AuditEnabled = 'Y'
      and (ObjectDefId, LogicalTransactionId) in
          ( select ObjectDefId, max(LogicalTransactionId)
            from posseaudit.ObjectDefAuditChanges
            group by ObjectDefId
          );

    delete from posseaudit.ObjectDefAuditChanges
    where LogicalTransactionId <> 1;

    -- change all LogicalTransactionIds to 1
    for c in c_LogicalTransactionTables loop
      pkg_Debug.putline('Set LogicalTransactionIds on ' || c.owner || '.'
          || c.table_name);
      execute immediate 'update ' || c.owner || '.' || c.table_name || ' set '
          || 'LogicalTransactionId = 1';
      pkg_Debug.putline('  ' || sql%rowcount || ' rows updated');
      commit;
    end loop;

    update possedata.Objects set
      CreatedLogicalTransactionId = 1;
    commit;

    update admin.BackgroundProcesses set SubmittedBy = 1;

-- FIX ME - are these OK?  should be no rows if all changes are captured
    update posseconfigchanges.AuditLog_t set
      FirstLogicalTransactionId = 1,
      LastLogicalTransactionId = 1;
    commit;

    update posseconfigchanges.CurrentComments_t set
      UserId = 1,
      LastUsedLogicalTransactionId = 1;
    commit;
  
  end SetLogicalTransactionIds;

  -----------------------------------------------------------------------------
  -- TruncateRunTimeTables()
  -----------------------------------------------------------------------------
  procedure TruncateRunTimeTables is
  begin
    for i in 1..g_RunTimeTables.count loop
      pkg_Debug.putline('Truncate Table ' || g_RunTimeTables(i));
      execute immediate 'TRUNCATE TABLE ' || g_RunTimeTables(i);
    end loop;
  end TruncateRunTimeTables;

  -----------------------------------------------------------------------------
  -- ZeroSequenceValues()
  -----------------------------------------------------------------------------
  procedure ZeroSequenceValues is
  begin
    pkg_Debug.putline('Zero Sequence Values');
-- FIX ME - what about ResetAt column?
    update objmodelphys.SequenceValues set Value = 0;
    commit;    
  end ZeroSequenceValues;

-----------------------------------------------------------------------------
-- main body
-----------------------------------------------------------------------------
begin
  pkg_debug.Enable('prodconv');

  -- safety features
  select
    to_char(sys_context('USERENV', 'DB_NAME')) db_name, 
    to_char(sys_context ( 'USERENV', 'SERVER_HOST' )) db_host
  into
    t_DbName,
    t_DbHost
  from dual;

  pkg_debug.putline('Connected to ' || t_DbName || ' on ' || t_DbHost);

  if lower(t_DbName) <> lower(gc_DBName) then
    RaiseError('Wrong database.');
  end if;

  if lower(t_DbHost) <> lower(gc_DbHost) then
    RaiseError('Wrong host for database.');
  end if;

  -- primitive but it works in sqlplus and pl/sql developer
  if lower('&do_you_want_to_del_ALL_data') in ('yes', 'y')
      and lower('&do_you_have_a_current_backup') in ('yes','y')
      and lower('&are_you_really_really_sure') in ('yes','y') then
    null;
  else
    RaiseError(
       'Terminating script - the delete was not confirmed.');
  end if; 

  PopulateRunTimeTableArray;
  SaveRequiredRows;

  AlterConstraints('disable');

  TruncateRunTimeTables;
  RestoreRowsFromBackup;
  SetLogicalTransactionIds;
  ZeroSequenceValues;

  AlterConstraints('enable');

-- FIX ME - last run - SubmittedBy on background processes
-- FIX ME - warning to stop Corral processing before running this script
-- FIX ME - some method to clear corral as well.
-- FIX ME - truncate of DocumentBlobs_t took 1 hour (drop table instead)
-- FIX ME - should main object sequence be reset to max objectid + 10?

-- FIX ME - problem enabling these foreign keys
-- FIX ME - .instances_fk_2 - keep BackgroundProcess rows?
-- FIX ME - objmodelphys.allindexdefs_fk_1 - failed index build

-- FIX ME - if there are no uncaptured changes no rows should exist in the
--          following tables
-- FIX ME - posseconfigchanges.auditlog_fk_2 and 3 - changed LogTxn to 1
-- FIX ME - posseconfigchanges.currentcomments_fk_1 and 3 - changed to 1
  
  pkg_debug.putline('Done truncating run time data.');
end;
/
