--run as POSSEDBA after:
   -- the data purge has been completed

--there is only variable to set: the password of the guest user in the first script below.
declare
  t_Password                            varchar2(30) := 'kansasprod';
  
  t_AccessGroupId                       api.pkg_Definition.udt_Id;
  t_UserGuestCount                      number;
  t_UserId                              api.pkg_Definition.udt_Id;
  
begin
  if t_Password = 'XXX' then
    raise_application_error(-20000, 'Please set a password.');
  end if;
  
  select count(*)
    into t_UserGuestCount
    from api.users u
    where u.OracleLogonId = 'ABCWEBGUEST';
    
  if t_UserGuestCount = 0 then
    api.pkg_LogicalTransactionUpdate.ResetTransaction;
    rollback;
    
    select ag.AccessGroupId
      into t_AccessGroupId
      from api.AccessGroups ag
      where ag.Description = 'ABC Public';
      
    t_UserId := api.pkg_UserUpdate.New('Default', 'ABCWEBGUEST', 'ABC Web Guest', t_Password);
    api.pkg_ColumnUpdate.SetValue(t_UserId, 'FirstName', 'ABC');
    api.pkg_ColumnUpdate.SetValue(t_UserId, 'LastName', 'Web Guest');
    api.pkg_ColumnUpdate.SetValue(t_UserId, 'UserType', 'Guest');
    api.pkg_UserUpdate.AddToAccessGroup(t_UserId, t_AccessGroupId);
    api.pkg_LogicalTransactionUpdate.EndTransaction;
    commit;
  end if;
end;

--register Process Types
begin

  for i in (select processtypeid from api.processtypes) loop  

    api.pkg_objectupdate.RegisterExternalObject('o_ProcessType', i.processtypeid);

  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;


--register Job Types
begin

  for i in (select jobtypeid from api.jobtypes) loop  

    api.pkg_objectupdate.RegisterExternalObject('o_JobTypes', i.jobtypeid);

  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;


--register DashboardObjectTypes
begin

  for i in (select objectdefid from api.objectdefs) loop  

    api.pkg_objectupdate.RegisterExternalObject('o_DashboardObjectType', i.objectdefid);

  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;

--register Process Emails
begin

  for i in (select processtypeid from bcpdata.ProcessEmail_v) loop  

    api.pkg_objectupdate.RegisterExternalObject('o_ProcessEmail', i.processtypeid);

  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;

--register Process outcomes
begin

  for i in (select ProcessOutcomeId from api.ProcessOutcomes) loop  

    api.pkg_objectupdate.RegisterExternalObject('o_ProcessOutcome', i.ProcessOutcomeId);

  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;

--register Access Groups
begin

  for i in (select a.AccessGroupId from api.accessgroups a) loop  

    api.pkg_objectupdate.RegisterExternalObject('o_AccessGroup', i.accessgroupid);

  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;

/******************************************************************************
<Revision Info:  Do not edit>
$Revision: 1.1 $
$Author: bruce $
$Date: 2011/06/01 16:38:01 $
</Revision Info: Do not edit>
*
*  This script creates sets whether Access Group objects are administratable.
*
*  execution time:  about 4.5 seconds
*  changed rows:    about 41 rows
*  new rows:        0
*  deleted rows:    0
*
******************************************************************************/

declare
  cursor c_AccessGroups is
    select f.Description Folder, reo.ObjectId AccessGroupObjectId,
          api.pkg_ColumnQuery.Value(reo.ObjectId, 'Description') Description,
          api.pkg_ColumnQuery.Value(reo.ObjectId, 'IsAdministratable') IsAdministratable
      from stage.AccessGroups ag
        join api.RegisteredExternalObjects reo
          on ag.AccessGroupId = to_char(reo.LinkValue)
          join query.o_accessgroup oag on oag.objectid = reo.ObjectId
        join stage.Folders f
          on ag.FolderId = f.FolderId
      order by 1, 3;

begin
  if user() != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20001, 'Script must be run as POSSEDBA');
  end if;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  rollback;
  for o in c_AccessGroups loop
    if o.Folder in ('ABC') then
      api.pkg_ColumnUpdate.SetValue(o.AccessGroupObjectId, 'IsAdministratable', 'Y');
    else
      api.pkg_ColumnUpdate.RemoveValue(o.AccessGroupObjectId, 'IsAdministratable');
    end if;
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;

/******************************************************************************
<Revision Info:  Do not edit>
$Revision: 1.1 $
$Author: bruce $
$Date: 2011/06/01 16:38:01 $
</Revision Info: Do not edit>
*
*  This script adds the Super User Access Group object to those users which
*  already have the Super User Access Group.
*
*  execution time:  variable:  around
*  changed rows:    0
*  new rows:        variable
*  deleted rows:    0
*
******************************************************************************/

declare
  t_RelId                         number;
  t_SuperUserAccessGroupId        number;
  t_SuperUserAccessGroupObjectId  number;
  
  cursor c_SuperUsers is
    select agu.UserId
      from api.AccessGroupUsers agu
      where agu.AccessGroupId = t_SuperUserAccessGroupId
        and not exists (
          select 1
            from query.r_UserAccessGroup ruag
            where agu.UserId = ruag.UserId
              and ruag.AccessGroupObjectId = t_SuperUserAccessGroupObjectId);

begin
  if user() != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20001, 'Script must be run as POSSEDBA');
  end if;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  rollback;
  select ag.AccessGroupId, api.pkg_SimpleSearch.ObjectByIndex('o_AccessGroup', 'AccessGroupId', ag.AccessGroupId)
    into t_SuperUserAccessGroupId, t_SuperUserAccessGroupObjectId
    from api.AccessGroups ag
    where ag.Description = 'Super Users';
  
  for u in c_SuperUsers loop
    t_RelId := extension.pkg_RelationshipUpdate.New(u.UserId, t_SuperUserAccessGroupObjectId, 'AccessGroup');
  end loop;
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;


/******************************************************************************
<Revision Info:  Do not edit>
$Revision: 1.1 $
$Author: bruce $
$Date: 2011/06/01 16:38:01 $
</Revision Info: Do not edit>
*
*  This script sets up the roles.
*
*  execution time:  2 s
*  changed rows:    0
*  new rows:        12
*  deleted rows:    0
*
******************************************************************************/

declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  
  function CreateRole (
    a_RoleName                          varchar2,
    a_AccessGroupList                   varchar2 default null
  ) return udt_Id is
    t_AccessGroupObjectDefId            udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('o_AccessGroup');
    t_RoleObjectDefId                   udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('o_Role');
    t_RoleObjectId                      udt_Id;
    
    cursor c_AccessGroups is
      select reo.ObjectId AccessGroupObjectId
        from api.AccessGroups ag
          join api.RegisteredExternalObjects reo
            on to_char(ag.AccessGroupId) = reo.LinkValue
              and reo.ObjectDefId = t_AccessGroupObjectDefId
        where ag.Description in (
          select trim(regexp_substr(a_AccessGroupList, '[^,]+', 1, level)) str
            from dual
            connect by regexp_substr(a_AccessGroupList, '[^,]+', 1, level) is not null);
    
  begin
    t_RoleObjectId := api.pkg_ObjectUpdate.New(t_RoleObjectDefId);
    api.pkg_ColumnUpdate.SetValue(t_RoleObjectId, 'Name', a_RoleName);
    for a in c_AccessGroups loop
      extension.pkg_RelationshipUpdate.New(t_RoleObjectId, a.AccessGroupObjectId, 'AccessGroup');
    end loop;
    return t_RoleObjectId;
  end;
  
  
  procedure CreateRole(
    a_RoleName                          varchar2,
    a_AccessGroupList                   varchar2 default null
  ) is
    t_RoleObjectId                      udt_Id;
  begin
    t_RoleObjectId := CreateRole(a_RoleName, a_AccessGroupList);
  end;
  

begin
  if user() != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20001, 'Script must be run as POSSEDBA');
  end if;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  rollback;
  CreateRole('Licensing Clerk',        'Licensing Administration,Licensing Read Only,Establishment Maintenance,Legal Entity Maintenance');
  CreateRole('Licensing Supervisor',   'Licensing Review,Licensing Read Only,Establishment Maintenance,Legal Entity Maintenance');
  CreateRole('Licensing Manager',      'Licensing Approval,Licensing Read Only');
  CreateRole('System Administrator',   'System Administration');
  CreateRole('Enforcement Clerk',      'Enforcement Read Only,Enforcement Administration');
  CreateRole('Enforcement Supervisor', 'Enforcement Read Only,Accusation Registration');
  CreateRole('Legal',                  'Enforcement Read Only,Accusation Decision');
  CreateRole('Inspector',              'Enforcement Read Only,Licensing Read Only,Inspection Completion');
  CreateRole('Management Reporting',   'Management Reporting');
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;


--set process email permissions
declare
  cursor c_ProcessesToAdd is
    select o.ObjectId, o.Name, o.ShowOnUserPrefsAssignments
      from query.o_ProcessEmail o
      where o.Name like 'p_ABC_%'
        and o.ShowOnUserPrefsAssignments = 'N';
        
  cursor c_ProcessesToRemove is
    select o.ObjectId, o.Name, o.ShowOnUserPrefsAssignments
      from query.o_ProcessEmail o
      where o.Name not like 'p_ABC_%'
        and o.ShowOnUserPrefsAssignments = 'Y';
  
begin
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  commit;
  
  for p in c_ProcessesToAdd loop
    api.pkg_ColumnUpdate.SetValue(p.ObjectId, 'ShowOnUserPrefsAssignments', 'Y');
  end loop;
  
  for p in c_ProcessesToRemove loop
    api.pkg_ColumnUpdate.SetValue(p.ObjectId, 'ShowOnUserPrefsAssignments', 'N');
  end loop;
  
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;


--Posse Utilities object
declare 
  t_ObjectId number;
begin
  api.pkg_logicaltransactionupdate.StartTransaction();
  t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_PosseUtilities'));
  api.pkg_columnupdate.SetValue(t_ObjectId, 'SearchString', 'SearchValue');
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;


--System Settings object
declare
  t_ObjectId  number;
  t_EndPointId number;
  t_RelId number;
  t_NoteId number;
  t_NoteDefId number;
  t_EmailBody varchar2(32767);

begin
  
   rollback;
   api.pkg_logicaltransactionupdate.ResetTransaction();
  
  t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_SystemSettings'));
  api.pkg_columnupdate.SetValue(t_ObjectId, 'BatchNotificationDayOfMon', '1');
  
  select distinct e.EndPointId
  into t_EndPointId
  from api.endpoints e
  join api.relationshipdefs r on r.RelationshipDefId = e.RelationshipDefId
  join api.objectdefs od on od.ObjectDefId = e.ToObjectDefId
  where r.Name = 'r_SystemSettingUser'
  and od.Name = 'u_Users';
  
  t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_ObjectId, 1);
  
  select n.NoteDefId
    into t_NoteDefId
    from api.notedefs n
    where n.Tag = 'REGEML';

/*<<<<<<< .mine
  t_EmailBody := '<p><span style="font-size: 12px; background-color: rgb(255, 255, 255);"><img alt="Email Header" src="KansasTest\ws\int\abc\images\Logo.gif"><strong><br></strong></span><br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Dear {FirstName},<br>Thank you for registering. To activate your account and complete your registration,<br>please <a href="{RegistrationLink}"><font color="#ff6600">click here</font></a>.<br></font><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><strong>Your Registration Information:</strong></font></p><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">' ||
=======*/
  t_EmailBody := '<p><span style="font-size: 12px; background-color: rgb(255, 255, 255);"><img alt="Email Header" src="http://10.22.114.250/KansasTest/pub/abc/images/EmailHeader.jpg"><strong><br></strong></span><br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Dear {FirstName},<br>Thank you for registering. To activate your account and complete your registration,<br>please <a href="{RegistrationLink}"><font color="#ff6600">click here</font></a>.<br></font><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><strong>Your Registration Information:</strong></font></p><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">' ||
-->>>>>>> .r221
                  '<table style="width: 600px; text-align: left;" border="1" cellspacing="0" cellpadding="0">' ||
                  '<tbody>' ||
                  '<tr>' ||
                  '<td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">First Name:<br></font>' ||
                  '</font></td><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><strong><font color="#ff6600">{FirstName}</font></strong><br>' ||
                  '</font></td></tr><tr><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Last Name:<br></font>' ||
                  '</font></td><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><strong><font color="#ff6600">{LastName}</font></strong><br>' ||
                  '</font></td></tr><tr><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Email Address:<br></font>' ||
                  '</font></td><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><strong><font color="#ff6600">{EmailAddress}</font></strong><br>' ||
                  '</font></td></tr><tr><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Phone Number:<br></font>' ||
                  '</font></td><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><font color="#ff6600"><strong>{FormattedPhoneNumber}</strong><br></font>' ||
                  '</font></td></tr><tr><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Security Question:<br></font>' ||
                  '</font></td><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><font color="#ff6600"><strong>{SecurityQuestion}</strong><br></font>' ||
                  '</font></td></tr><tr><td><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Security Answer:<br></font>' ||
                  '</td><td><font color="#ff6600"><strong><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">{SecurityAnswer}</font></strong><br></font>' ||
                  '</td></tr></tbody></table>' ||
                  '<p><br><br><font style="font-size: 8px;" face="calibri, tahoma, arial, verdana, sans-serif">Plain Text Link:<br>{RegistrationLink}</font></p></font>';  
  t_NoteId := api.pkg_noteupdate.New(t_ObjectId, t_NoteDefId, 'N', t_EmailBody);
  
  select n.NoteDefId
    into t_NoteDefId
    from api.notedefs n
    where n.Tag = 'PWREML';
  
  
/*<<<<<<< .mine
  t_EmailBody := '<img alt="Email Header" src="KansasTest\ws\int\abc\images\Logo.gif"><br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Dear {FirstName},<br>You have reset your password - to complete the process, please <a href="{RegistrationLink}"><font color="#ff6600">click here</font></a>.</font><br><br><br><br><br><br><br><br><br><br><font size="1" face="Arial">Plain Text Link:<br>{RegistrationLink}</font><br>';
=======*/
  t_EmailBody := '<img alt="Email Header" src="http://10.22.114.250/KansasTest/pub/abc/images/EmailHeader.jpg"><br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Dear {FirstName},<br>You have reset your password - to complete the process, please <a href="{RegistrationLink}"><font color="#ff6600">click here</font></a>.</font><br><br><br><br><br><br><br><br><br><br><font size="1" face="Arial">Plain Text Link:<br>{RegistrationLink}</font><br>';
  t_NoteId := api.pkg_noteupdate.New(t_ObjectId, t_NoteDefId, 'N', t_EmailBody);

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  
end; 
 
/******************************************************************************
<Revision Info:  Do not edit>
$Revision: 1.1 $
$Author: bradv $
$Date: 2011/04/18 17:43:49 $
</Revision Info: Do not edit>
*
*  This script creates the street names
*
*  execution time:  about 1.2 second
*  changed rows:    0
*  new rows:        141 rows
*  deleted rows:    0
*
******************************************************************************/

declare
  subtype udt_id is api.pkg_Definition.udt_Id;

  procedure AddStreetType(
    a_StreetType                        varchar2,
    a_StreetTypeShort                   varchar2
  ) is
    t_ObjectDefId                       udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_StreetType');
    t_ObjectId                          udt_Id;
    
  begin
    t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'StreetType', a_StreetType);
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'StreetTypeShort', a_StreetTypeShort);
  end AddStreetType;
  
  
begin
  if user() != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20001, 'Script must be run as POSSEDBA');
  end if;
  
  api.pkg_LogicalTransactionUpdate.ResetTransaction;
  rollback;
  AddStreetType('Abbey', 'ABBEY');
  AddStreetType('Acres', 'ACRES');
  AddStreetType('All�e', 'ALL�E');
  AddStreetType('Alley', 'ALLEY');
  AddStreetType('Autoroute', 'AUT');
  AddStreetType('Avenue', 'AV');      -- ignoring French abbreviation for Avenue:  AV
  AddStreetType('Bay', 'BAY');
  AddStreetType('Beach', 'BEACH');
  AddStreetType('Bend', 'BEND');
  AddStreetType('Boulevard', 'BLVD'); -- ignoring French abbreviation for Boulevard:  BOUL
  AddStreetType('By-pass', 'BYPASS');
  AddStreetType('Byway', 'BYWAY');
  AddStreetType('Campus', 'CAMPUS');
  AddStreetType('Cape', 'CAPE');
  AddStreetType('Carr�', 'CAR');
  AddStreetType('Carrefour', 'CARREF');
  AddStreetType('Centre', 'CTR');
  AddStreetType('Cercle', 'CERCLE');
  AddStreetType('Chase', 'CHASE');
  AddStreetType('Chemin', 'CH');
  AddStreetType('Circle', 'CIR');
  AddStreetType('Circuit', 'CIRCT');
  AddStreetType('Close', 'CLOSE');
  AddStreetType('Common', 'COMMON');
  AddStreetType('Concession', 'CONC');
  AddStreetType('Corners', 'CRNRS');
  AddStreetType('C�te', 'C�TE');
  AddStreetType('Cour', 'COUR');
  AddStreetType('Cours', 'COURS');
  AddStreetType('Court', 'CRT');
  AddStreetType('Cove', 'COVE');
  AddStreetType('Crescent', 'CRES');
  AddStreetType('Croissant', 'CROIS');
  AddStreetType('Crossing', 'CROSS');
  AddStreetType('Cul-de-sac', 'CDS');
  AddStreetType('Dale', 'DALE');
  AddStreetType('Dell', 'DELL');
  AddStreetType('Diversion', 'DIVERS');
  AddStreetType('Downs', 'DOWNS');
  AddStreetType('Drive', 'DR');
  AddStreetType('�changeur', '�CH');
  AddStreetType('End', 'END');
  AddStreetType('Esplanade', 'ESPL');
  AddStreetType('Estates', 'ESTATE');
  AddStreetType('Expressway', 'EXPY');
  AddStreetType('Extension', 'EXTEN');
  AddStreetType('Farm', 'FARM');
  AddStreetType('Field', 'FIELD');
  AddStreetType('Forest', 'FOREST');
  AddStreetType('Freeway', 'FWY');
  AddStreetType('Front', 'FRONT');
  AddStreetType('Gardens', 'GDNS');
  AddStreetType('Gate', 'GATE');
  AddStreetType('Glade', 'GLADE');
  AddStreetType('Glen', 'GLEN');
  AddStreetType('Green', 'GREEN');
  AddStreetType('Grounds', 'GRNDS');
  AddStreetType('Grove', 'GROVE');
  AddStreetType('Harbour', 'HARBR');
  AddStreetType('Heath', 'HEATH');
  AddStreetType('Heights', 'HTS');
  AddStreetType('Highlands', 'HGHLDS');
  AddStreetType('Highway', 'HWY');
  AddStreetType('Hill', 'HILL');
  AddStreetType('Hollow', 'HOLLOW');
  AddStreetType('�le', '�LE');
  AddStreetType('Impasse', 'IMP');
  AddStreetType('Inlet', 'INLET');
  AddStreetType('Island', 'ISLAND');
  AddStreetType('Key', 'KEY');
  AddStreetType('Knoll', 'KNOLL');
  AddStreetType('Landing', 'LANDNG');
  AddStreetType('Lane', 'LANE');
  AddStreetType('Limits', 'LMTS');
  AddStreetType('Line', 'LINE');
  AddStreetType('Link', 'LINK');
  AddStreetType('Lookout', 'LKOUT');
  AddStreetType('Loop', 'LOOP');
  AddStreetType('Mall', 'MALL');
  AddStreetType('Manor', 'MANOR');
  AddStreetType('Maze', 'MAZE');
  AddStreetType('Meadow', 'MEADOW');
  AddStreetType('Mews', 'MEWS');
  AddStreetType('Mont�e', 'MONT�E');
  AddStreetType('Moor', 'MOOR');
  AddStreetType('Mount', 'MOUNT');
  AddStreetType('Mountain', 'MTN');
  AddStreetType('Orchard', 'ORCH');
  AddStreetType('Parade', 'PARADE');
  AddStreetType('Parc', 'PARC');
  AddStreetType('Park', 'PK');
  AddStreetType('Parkway', 'PKY');
  AddStreetType('Passage', 'PASS');
  AddStreetType('Path', 'PATH');
  AddStreetType('Pathway', 'PTWAY');
  AddStreetType('Pines', 'PINES');
  AddStreetType('Place', 'PL');       -- ignoring French abbreviation for Place:  PLACE
  AddStreetType('Plateau', 'PLAT');
  AddStreetType('Plaza', 'PLAZA');
  AddStreetType('Point', 'PT');
  AddStreetType('Pointe', 'POINTE');
  AddStreetType('Port', 'PORT');
  AddStreetType('Private', 'PVT');
  AddStreetType('Promenade', 'PROM');
  AddStreetType('Quai', 'QUAI');
  AddStreetType('Quay', 'QUAY');
  AddStreetType('Ramp', 'RAMP');
  AddStreetType('Rang', 'RANG');
  AddStreetType('Range', 'RG');
  AddStreetType('Ridge', 'RIDGE');
  AddStreetType('Rise', 'RISE');
  AddStreetType('Road', 'RD');
  AddStreetType('Rond-point', 'RDPT');
  AddStreetType('Route', 'RTE');
  AddStreetType('Row', 'ROW');
  AddStreetType('Rue', 'RUE');
  AddStreetType('Ruelle', 'RLE');
  AddStreetType('Run', 'RUN');
  AddStreetType('Sentier', 'SENT');
  AddStreetType('Square', 'SQ');
  AddStreetType('Street', 'ST');
  AddStreetType('Subdivision', 'SUBDIV');
  AddStreetType('Terrace', 'TERR');
  AddStreetType('Terrasse', 'TSSE');
  AddStreetType('Thicket', 'THICK');
  AddStreetType('Towers', 'TOWERS');
  AddStreetType('Townline', 'TLINE');
  AddStreetType('Trail', 'TRAIL');
  AddStreetType('Turnabout', 'TRNABT');
  AddStreetType('Vale', 'VALE');
  AddStreetType('Via', 'VIA');
  AddStreetType('View', 'VIEW');
  AddStreetType('Village', 'VILLGE');
  AddStreetType('Villas', 'VILLAS');
  AddStreetType('Vista', 'VISTA');
  AddStreetType('Voie', 'VOIE');
  AddStreetType('Walk', 'WALK');
  AddStreetType('Way', 'WAY');
  AddStreetType('Wharf', 'WHARF');
  AddStreetType('Wood', 'WOOD');
  AddStreetType('Wynd', 'WYND');
  api.pkg_LogicalTransactionUpdate.EndTransaction;
  commit;
end;

--set Sequences
update objmodelphys.sequencevalues sv
   set sv.value = 10000
   where sv.SequenceId = ( select s.SequenceId
                             from objmodelphys.sequences s
                             where s.Description = 'ABC Job Number');
                             
update objmodelphys.sequencevalues sv
   set sv.value = 3000
   where sv.SequenceId = ( select s.SequenceId
                             from objmodelphys.sequences s
                             where s.Description = 'ABC License Number');                             
                             
commit;

--Payment Methods
declare
  t_ObjectId  number;

begin
  
   rollback;
   api.pkg_logicaltransactionupdate.ResetTransaction();
  
  t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_PaymentMethod'));
  api.pkg_columnupdate.SetValue(t_ObjectId, 'Name', 'Credit Card');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'EComFlag', 'Y');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReferenceRequiredFlag', 'N');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReferenceLabel', 'Credit Card #');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReceiptNumberLabel', 'Receipt Number');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ActiveFlag', 'Y');

  t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_PaymentMethod'));
  api.pkg_columnupdate.SetValue(t_ObjectId, 'Name', 'Cash');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'EComFlag', 'N');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReferenceRequiredFlag', 'N');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReferenceLabel', 'Cash');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReceiptNumberLabel', 'Receipt Number');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ActiveFlag', 'Y');
  
  t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_PaymentMethod'));
  api.pkg_columnupdate.SetValue(t_ObjectId, 'Name', 'Check');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'EComFlag', 'N');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReferenceRequiredFlag', 'N');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReferenceLabel', 'Check');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReceiptNumberLabel', 'Receipt Number');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ActiveFlag', 'Y');  

  t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_PaymentMethod'));
  api.pkg_columnupdate.SetValue(t_ObjectId, 'Name', 'Other');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'EComFlag', 'N');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReferenceRequiredFlag', 'N');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReferenceLabel', 'Reference #');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ReceiptNumberLabel', 'Receipt Number');
  api.pkg_columnupdate.SetValue(t_ObjectId, 'ActiveFlag', 'Y');

  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  
end; 


--takes a custom value, creates a new note for those processemail objects that don't have one, and puts the custom value into the new note.
declare
t_NoteDefId api.pkg_definition.udt_Id;
t_EmailBody varchar2(32767);
t_Count number;
t_NoteId api.pkg_definition.udt_Id;
begin
  select nd.NoteDefId into t_NoteDefId from api.notedefs nd where nd.Tag = 'PROEML';
  api.pkg_logicaltransactionupdate.ResetTransaction;
/*<<<<<<< .mine
  t_EmailBody := '<img alt="Email Header" src="KansasTest\ws\int\abc\images\Logo.gif"><br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Hello {FirstName} {LastName},<br><br>You have been assigned to the following process: </font><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><strong>{LinkDescription}</strong></font><br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Please </font><a href="{Link}"><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">click here</font></a><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"> to go to this task.<br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Thanks,<br><br><strong>The POSSE ABC Team Computronix Canada</strong></font><br><br><br><br><br><br><br><br><font style="font-size: 8px;" face="calibri, tahoma, arial, verdana, sans-serif">Plain Text Link:<br>{Link}</font><br></font>';
=======*/
  t_EmailBody := '<img alt="Email Header" src="http://10.22.114.250/KansasTest/pub/abc/images/EmailHeader.jpg"><br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Hello {FirstName} {LastName},<br><br>You have been assigned to the following process: </font><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"><strong>{LinkDescription}</strong></font><br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Please </font><a href="{Link}"><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">click here</font></a><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif"> to go to this task.<br><br><font style="font-size: 12px;" face="calibri, tahoma, arial, verdana, sans-serif">Thank you and have a nice day!</font><br><br><br><br><br><br><br><br><font style="font-size: 8px;" face="calibri, tahoma, arial, verdana, sans-serif">Plain Text Link:<br>{Link}</font><br></font>';
-->>>>>>> .r221
  for i in (select objectid from query.o_processemail where Name Like 'p_ABC%')
  loop
    select count(*) into t_Count from api.notes n where n.ObjectId = i.objectid and n.NoteDefId = t_NoteDefId;
    if t_Count = 0 then
      t_NoteId := api.pkg_noteupdate.New(i.objectid, t_NoteDefId, 'N', t_EmailBody);
    end if;
  end loop;

  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
