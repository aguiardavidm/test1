
begin
  api.pkg_logicaltransactionupdate.resettransaction;
for x in (select objectid from query.o_processemail where name like '%ABC%') loop
  api.pkg_columnupdate.setvalue(x.objectid,'IsABC','Y');
  end loop;
    api.pkg_logicaltransactionupdate.endtransaction;
    commit;
  end;

declare
     t_EndPointId             pls_integer := api.pkg_configquery.endpointidforname('o_ProcessEmail', 'Users');
     t_NewRelId               pls_integer;
     t_ABCEndPointId          pls_integer := api.pkg_configquery.endpointidforname('o_ProcessEmail', 'ABCInternalUser');
     t_ABCNewRelId            pls_integer;
begin
  api.pkg_logicaltransactionupdate.resettransaction;
  for x in (select u.userid from api.users u ) loop
           for p in(select pr.objectid
                    from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ProcessEmail', 'IsABC', 'Y') as api.udt_ObjectList)) pr)loop
               t_ABCNewRelId := api.pkg_relationshipupdate.new(t_ABCEndPointId, p.ObjectId, x.userid);
           end loop; --p

    end loop;
    api.pkg_logicaltransactionupdate.endtransaction;
    commit;
  end;  
  
declare t_objectid pls_integer;  
begin
  select objectid
  into t_objectid from query.o_paymentmethod where name = 'Credit Card';
  api.pkg_columnupdate.removevalue(t_objectid,'ReferenceRequiredFlag');
    api.pkg_logicaltransactionupdate.endtransaction;
    commit;
end;
  
