--Run time: 10 seconds
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_PermitType      varchar2(100);
  t_NoteText        long := '';
  t_LastSolNum      number(6);
  
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin  
    dbms_output.put_line('Error: '|| a_ErrorText || 'ErrorCode: ' ||a_ErrorCode);
  end;

begin
  
  select ObjectId 
    into t_PermitType 
   from dataconv.o_abc_permittype pt
  where pt.CODE = 'SOL';


  for c in (select s.rowid,
            s.SOL_NUM || 'SOL' LEGACYKEY,
			s.SOL_NUM PERMITNUMBER,
			s.SOL_STAT STATE,
			(select nm2.NAME_MSTR_ID_NUM
               from abc11p.name_mstr nm2
              where nm2.ABC_NUM = to_char(s.SOL_NUM)) SOLICITORLK,
			t_PermitType  PERMITTYPELK,
			s.EMP_CNTY_MUNI_CD COUNTYLK,
			s.EMP_CNTY_MUNI_CD MUNICIPALITYLK,
			s.EMP_CNTY_MUNI_CD ||'-'||s.EMP_LIC_TYPE_CD ||'-'|| s.EMP_MUNI_SER_NUM LICENSELK,
			s.DT_ROW_CRTD CONVCREATEDDATE,
			s.CRTD_OPER_ID CREATEDBYUSERLEGACYKEY,
			s.DT_ROW_UPDT LASTUPDATEDDATE,
			s.UPDT_OPER_ID LASTUPDATEDBYUSERLEGACYKEY,
			min(st.DT_TERM_START) ISSUEDATE,
			max(st.DT_TERM_START) EFFECTIVEDATE,
			max(st.DT_TERM_END) EXPIRATIONDATE,
			'SPEC_COND_IND: ' ||SPEC_COND_IND ||
			'EFF_DT: ' || EFF_DT ||
			'SAL_IND: ' || SAL_IND ||
			'COMM_IND: ' || COMM_IND ||
			'BONUS_IND: ' || BONUS_IND ||
			'EXP_IND: ' || EXP_IND ||
			'PCT_IND: ' || PCT_IND ||
			'NONE_IND: ' || NONE_IND ||
			'OTHER_IND: ' || OTHER_IND ||
			'DT_EMP_START: ' || DT_EMP_START ||
			'DR_LIC_STATE: ' || DR_LIC_STATE ||
			'DR_LIC_NUM: ' || DR_LIC_NUM ||
			'PREV_EMP_IND: ' || PREV_EMP_IND ||
			'OTH_INT_IND: ' || OTH_INT_IND ||
			'MEM_OF_AUTH_IND: ' || MEM_OF_AUTH_IND ||
			'ENF_POS_IND: ' || ENF_POS_IND ||
			'DEN_PERM_IND: ' || DEN_PERM_IND ||
      'ABC_VIOL_IND: ' || ABC_VIOL_IND ||
      'CONV_OTH_IND: ' || CONV_OTH_IND ||
      'DISQ_REM_IND: ' || DISQ_REM_IND text
            from abc11p.SOLICITOR s
      join abc11p.SOL_TERM st on st.SOL_NUM = s.SOL_NUM
      group by
        s.rowid,
            s.SOL_NUM ,
      s.SOL_NUM ,
      s.SOL_STAT,
      t_PermitType  ,
      s.EMP_CNTY_MUNI_CD ,
      s.EMP_CNTY_MUNI_CD ,
      s.EMP_CNTY_MUNI_CD ||'-'||s.EMP_LIC_TYPE_CD ||'-'|| s.EMP_MUNI_SER_NUM ,
      s.DT_ROW_CRTD ,
      s.CRTD_OPER_ID, 
      s.DT_ROW_UPDT ,
      s.UPDT_OPER_ID,
      'SPEC_COND_IND: ' ||SPEC_COND_IND ||
			'EFF_DT: ' || EFF_DT ||
			'SAL_IND: ' || SAL_IND ||
			'COMM_IND: ' || COMM_IND ||
			'BONUS_IND: ' || BONUS_IND ||
			'EXP_IND: ' || EXP_IND ||
			'PCT_IND: ' || PCT_IND ||
			'NONE_IND: ' || NONE_IND ||
			'OTHER_IND: ' || OTHER_IND ||
			'DT_EMP_START: ' || DT_EMP_START ||
			'DR_LIC_STATE: ' || DR_LIC_STATE ||
			'DR_LIC_NUM: ' || DR_LIC_NUM ||
			'PREV_EMP_IND: ' || PREV_EMP_IND ||
			'OTH_INT_IND: ' || OTH_INT_IND ||
			'MEM_OF_AUTH_IND: ' || MEM_OF_AUTH_IND ||
			'ENF_POS_IND: ' || ENF_POS_IND ||
			'DEN_PERM_IND: ' || DEN_PERM_IND ||
      'ABC_VIOL_IND: ' || ABC_VIOL_IND ||
      'CONV_OTH_IND: ' || CONV_OTH_IND ||
      'DISQ_REM_IND: ' || DISQ_REM_IND  ) loop
    begin
    insert into dataconv.o_abc_permit a
    (   LEGACYKEY,
    PERMITNUMBER,
    STATE,
    SOLICITORLK,
    PERMITTYPELK,
    COUNTYLK,
    MUNICIPALITYLK,
    sellerslicenselk ,
    CONVCREATEDDATE,
    CREATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE,
    LASTUPDATEDBYUSERLEGACYKEY,
    ISSUEDATE,
    EFFECTIVEDATE,
    EXPIRATIONDATE
      )
    values (c.LEGACYKEY,
      c.PERMITNUMBER,
      c.STATE,
      c.SOLICITORLK,
      c.PERMITTYPELK,
      c.COUNTYLK,
      c.MUNICIPALITYLK,
      c.LICENSELK,
      c.CONVCREATEDDATE,
      c.CREATEDBYUSERLEGACYKEY,
      c.LASTUPDATEDDATE,
      c.LASTUPDATEDBYUSERLEGACYKEY,
      c.ISSUEDATE,
      c.EFFECTIVEDATE,
      c.EXPIRATIONDATE   );        
      
   
  insert into dataconv.n_general
         (createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text)
     values (c.CREATEDBYUSERLEGACYKEY,
             c.CONVCREATEDDATE,
             c.LASTUPDATEDBYUSERLEGACYKEY,
             c.LASTUPDATEDDATE,
             'o_abc_permit',
             c.LEGACYKEY,
             'Y',
              c.text);  
    t_count := t_count +1; 
    
    --SOL_CMT Note
      for d in (select 
                    sc.SOL_NUM,
                    sc.SEQ_NUM,
                    sc.CMT_TEXT,
                    (select ac.DT_ROW_UPDT
                       from abc11p.sol_cmt ac
                      where ac.sol_num = sc.sol_num
                        and ac.seq_num = ( select max(sc2.seq_num)
                                             from abc11p.sol_cmt sc2
                                           where sc2.sol_num = sc.sol_num ))DT_ROW_UPDT,
                    (select ac.UPDT_OPER_ID
                       from abc11p.sol_cmt ac
                      where ac.sol_num = sc.sol_num
                        and ac.seq_num = ( select max(sc2.seq_num)
                                             from abc11p.sol_cmt sc2
                                           where sc2.sol_num = sc.sol_num )) UPDT_OPER_ID
                  from abc11p.sol_cmt sc
                 where to_char(sc.sol_num) = c.legacykey
                 order by 1, 2
                  ) loop
         
         if t_LastSolNum = d.Sol_Num or t_LastSolNum is null then
         
         t_NoteText  := t_NoteText || chr(10) || chr(13) || d.Cmt_Text ;        
         
         elsif    t_LastSolNum != d.Sol_Num then    
                insert into dataconv.n_general
                 (createdbyuserlegacykey,
                  convcreateddate,
                  lastupdatedbyuserlegacykey,
                  lastupdateddate,
                  parenttablename,
                  parentlegacykey,
                  locked,
                  text)
             values (c.CREATEDBYUSERLEGACYKEY,
                     c.CONVCREATEDDATE,
                     c.LASTUPDATEDBYUSERLEGACYKEY,
                     c.LASTUPDATEDDATE,
                     'o_abc_permit',
                     c.LEGACYKEY,
                     'Y',
                      t_NoteText);
                        
             t_NoteText := '';         
           end if;           
           
           t_LastSolNum := d.Sol_Num;  
                
      end loop;            
    --SOL_CRIME Note
    t_NoteText := '';      
    t_LastSolNum := 0;  
    
    for e in (select 
                    sce.SOL_NUM,
                    sce.SEQ_NUM,
                    'Sequence Number: ' || sce.SEQ_NUM || chr(10) || chr(13) ||
                    'Nature of Offence: ' || sce.nature_of_off || chr(10) || chr(13) ||
                    'PENALTY_STAT: ' || sce.penalty_stat || chr(10) || chr(13) ||
                    'Date Convicted: ' || sce.dt_conv || chr(10) || chr(13) ||
                    'Jurisdiction: ' || sce.jurisdiction || chr(10) || chr(13) ||
                    'Identify Jurisdiction: ' || sce.identify_juris || chr(10) || chr(13) text,
                    (select ac.DT_ROW_UPDT
                       from abc11p.sol_cmt ac
                      where ac.sol_num = sce.sol_num
                        and ac.seq_num = ( select max(sce2.seq_num)
                                             from abc11p.sol_cmt sce2
                                           where sce2.sol_num = sce.sol_num ))DT_ROW_UPDT,
                    (select ac.UPDT_OPER_ID
                       from abc11p.sol_cmt ac
                      where ac.sol_num = sce.sol_num
                        and ac.seq_num = ( select max(sce2.seq_num)
                                             from abc11p.sol_cmt sce2
                                           where sce2.sol_num = sce.sol_num )) UPDT_OPER_ID
                  from abc11p.sol_crime sce
                 where to_char(sce.sol_num) = c.legacykey
                 order by 1, 2
                  ) loop
         
         if t_LastSolNum = e.Sol_Num or t_LastSolNum is null then
         
         t_NoteText  := t_NoteText || chr(10) || chr(13) || e.text ;        
         
         elsif    t_LastSolNum != e.Sol_Num then    
                insert into dataconv.n_general
                 (createdbyuserlegacykey,
                  convcreateddate,
                  lastupdatedbyuserlegacykey,
                  lastupdateddate,
                  parenttablename,
                  parentlegacykey,
                  locked,
                  text)
             values (c.CREATEDBYUSERLEGACYKEY,
                     c.CONVCREATEDDATE,
                     c.LASTUPDATEDBYUSERLEGACYKEY,
                     c.LASTUPDATEDDATE,
                     'o_abc_permit',
                     c.LEGACYKEY,
                     'Y',
                      t_NoteText);
                        
             t_NoteText := '';         
           end if;           
           
           t_LastSolNum := e.Sol_Num;  
                
      end loop;  
    --SOL_VIOLATION Note
      t_NoteText := '';      
      t_LastSolNum := 0;  
    
     for f in (select 
                    scf.SOL_NUM,
                    scf.SEQ_NUM,
                    'Sequence Number: ' || scf.SEQ_NUM || chr(10) || chr(13) ||
                    'Nature of Offence: ' || scf.nature_of_off || chr(10) || chr(13) ||
                    'PENALTY_STAT: ' || scf.penalty_stat || chr(10) || chr(13) ||
                    'Date Convicted: ' || scf.dt_conv || chr(10) || chr(13) ||
                    'Jurisdiction: ' || scf.jurisdiction || chr(10) || chr(13) ||
                    'Identify Jurisdiction: ' || scf.identify_juris || chr(10) || chr(13) text,
                    (select ac.DT_ROW_UPDT
                       from abc11p.sol_cmt ac
                      where ac.sol_num = scf.sol_num
                        and ac.seq_num = ( select max(scf2.seq_num)
                                             from abc11p.sol_cmt scf2
                                           where scf2.sol_num = scf.sol_num ))DT_ROW_UPDT,
                    (select ac.UPDT_OPER_ID
                       from abc11p.sol_cmt ac
                      where ac.sol_num = scf.sol_num
                        and ac.seq_num = ( select max(scf2.seq_num)
                                             from abc11p.sol_cmt scf2
                                           where scf2.sol_num = scf.sol_num )) UPDT_OPER_ID
                  from abc11p.sol_violation scf
                 where to_char(scf.sol_num) = c.legacykey
                 order by 1, 2
                  ) loop
         
         if t_LastSolNum = f.Sol_Num or t_LastSolNum is null then
         
         t_NoteText  := t_NoteText || chr(10) || chr(13) || f.text ;        
         
         elsif    t_LastSolNum != f.Sol_Num then    
                insert into dataconv.n_general
                 (createdbyuserlegacykey,
                  convcreateddate,
                  lastupdatedbyuserlegacykey,
                  lastupdateddate,
                  parenttablename,
                  parentlegacykey,
                  locked,
                  text)
             values (c.CREATEDBYUSERLEGACYKEY,
                     c.CONVCREATEDDATE,
                     c.LASTUPDATEDBYUSERLEGACYKEY,
                     c.LASTUPDATEDDATE,
                     'o_abc_permit',
                     c.LEGACYKEY,
                     'Y',
                      t_NoteText);
                        
             t_NoteText := '';         
           end if;           
           
           t_LastSolNum := f.Sol_Num;  
                
      end loop;        
    
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + 1;--t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;   
