-- Created on 5/4/2015 by KEATON 
declare 
  -- Local variables here
  t_RelationshipDef          number := api.pkg_configquery.ObjectDefIdForName('r_ABC_LELegalEntityType');
  t_RelationshipId           number;
  t_ConvertedTypeObjId       number;
  t_LETypeEndPointId         number := api.pkg_configquery.EndPointIdForName('o_ABC_LegalEntity','LegalEntityType');
  t_LegalEntityEndPointId    number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity','LegalEntityType');
  
  
begin

-- Select The  Correct Legal Entity Type

   select le.ObjectId
     into t_ConvertedTypeObjId 
     from query.o_abc_legalentitytype le
    where le.Name='Converted';

-- Loop through relationships and delete from the tables and create a new object.

for c in (select le.objectid fromobjectid
  from dataconv.o_abc_legalentity le
  join abc11p.permit p on p.abc11puk ||'PERM2' = le.legacykey) loop
   

select possedata.ObjectId_s.nextval
  into t_RelationshipId 
 from dual;

--Insert into ObjModelPhys.Objects
insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelationshipId,
      t_RelationshipDef,
      4,
      4,
      t_RelationshipDef,
      null,
      null,
      4,
      t_RelationshipDef,
      null,
      null
    );

 
--Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipId,
      t_LegalEntityEndPointId,
      t_ConvertedTypeObjId,
      c.Fromobjectid
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipId,
      t_LETypeEndPointId,
      c.fromobjectid,
      t_ConvertedTypeObjId
    ); 


end loop;
  delete
    from dataconv.posseconvmaintaindupsprogress p
  where p.tablename = 'O_ABC_LEGALENTITY'
    and p.columnname = 'DUP_FORMATTEDNAME';
end;
/