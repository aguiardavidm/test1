-- Created on 4/30/2015 by KEATON 
declare 
  -- Local variables here
  t_ColumnDef number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'Active');
  
 begin 

for c in ( select nm1.name, nm1.abc_num, nm1.name_type_cd, le1.objectid
			  from dataconv.o_abc_legalentity le1
			  join abc11p.name_mstr nm1 on to_char( nm1.name_mstr_id_num) = le1.legacykey
			  where  nm1.name||nm1.abc_num
			  in (select  nm.name||nm.abc_num
			  from dataconv.o_abc_legalentity le
			  join abc11p.name_mstr nm on to_char( nm.name_mstr_id_num) = le.legacykey
			 where nm.name_type_cd = '2.1-LICENSEE')
			   and nm1.name_type_cd in ('10.1-CORP', 
										  '10A-INTEREST', 
										  '6.1-CORP', 
										  '8.1-CORP',
										  '6A-INTEREST ',
										  '8A-INTEREST')--group by nm1.name, nm1.abc_num, nm1.name_type_cd
               order by 2,3) loop
   /* Delete the Active Boolean from objectcolumndata to set the value to N*/ 

   delete from possedata.Objectcolumndata ocd
     where ocd.Objectid = c.objectid
	   and ocd.columndefid = t_ColumnDef;


end loop;            
  
end;            
            