--Run time: 1 second
-- Created on 3/31/2015 by PAUL 
declare 
  -- Local variables here
  i integer;
  t_LETypeLK varchar2(40);
  t_count number := 0;
begin
  select legacykey
    into t_LETypeLK
    from dataconv.o_abc_legalentitytype let
   where let.name = 'Converted';
  -- Test statements here
  for i in (select l.legacykey
              from dataconv.o_abc_legalentity l
             where l.legalentitytypelk = 'CONVERTED') loop
    update dataconv.o_abc_legalentity le
       set le.legalentitytypelk = t_LETypeLK
     where le.legacykey = i.legacykey;
    t_count := t_count + 1;
  end loop;
  commit;
end;