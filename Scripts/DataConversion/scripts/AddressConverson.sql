--insert into dataconv.o_abc_address   --Premisis Address NAME_MSTR    
select null ,                            --  objectid  number(9)
       nm.po_box || nm.zip_1_5 ||nm.zip_6_9 || nm.st_prov_cd || nm.str_name || nm.str_num || nm.muni_name legacykey,   --  legacykey  varchar2(500)
       nm.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       nm.dt_row_crtd,                  --  convcreateddate  date
       nm.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       nm.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       nm.zip_1_5,                      --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       nm.str_name,                     --  street  varchar2(60)
       nm.str_num,                      --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       nm.muni_name,                    --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       nm.po_box,                       --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       nm.zip_6_9,                      --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.name_mstr nm      
  join abc11p.code_desc cd on cd.code = nm.st_prov_cd 
                           and cd.id = 'ST'      
 where nm.cntry_cd = 'US'                          

union
 --Mailing Address     NAME_MSTR
select null,                            --  objectid  number(9)
       nm.mail_po_box || nm.mail_zip_1_5 ||nm.mail_zip_6_9 || nm.mail_st_prov_cd || nm.mail_str_name || nm.mail_str_num || nm.mail_muni_name legacykey,   --  legacykey  varchar2(500)
       nm.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       nm.dt_row_crtd,                  --  convcreateddate  date
       nm.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       nm.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       nm.mail_zip_1_5,                 --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       nm.mail_str_name,                --  street  varchar2(60)
       nm.mail_str_num,                 --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       nm.mail_muni_name,               --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       nm.mail_po_box,                  --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       nm.mail_zip_6_9,                 --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.name_mstr nm      
  join abc11p.code_desc cd on cd.code = nm.st_prov_cd 
                           and cd.id = 'ST'      
 where nm.cntry_cd = 'US' 
 union
 --Warehouse Address  LIC_MSTR
select null,                            --  objectid  number(9)
       lm.war_zip_1_5 ||lm.war_zip_6_9 || lm.war_st || lm.war_str_name || lm.war_str_num || lm.war_muni legacykey,   --  legacykey  varchar2(500)
       lm.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       lm.dt_row_crtd,                  --  convcreateddate  date
       lm.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       lm.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       lm.war_zip_1_5,                  --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       lm.war_str_name,                 --  street  varchar2(60)
       lm.war_str_num,                  --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       lm.war_muni,                     --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       lm.war_zip_6_9,                  --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.lic_mstr lm      
  join abc11p.code_desc cd on cd.code = lm.war_st 
                           and cd.id = 'ST'      
-- where lm.war = 'US' 
  union
 --Vehicle Storage Address  LIC_VEH_VES
select null,                            --  objectid  number(9)
       lv.zip_1_5 ||lv.zip_6_9 || lv.st_prov_cd || lv.str_name || lv.str_num || lv.muni_name legacykey,   --  legacykey  varchar2(500)
       lv.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       lv.dt_row_crtd,                  --  convcreateddate  date
       lv.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       lv.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       lv.zip_1_5,                      --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       lv.str_name,                     --  street  varchar2(60)
       lv.str_num,                      --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       lv.muni_name,                    --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       lv.zip_6_9,                      --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.LIC_VEH_VES lv     
  join abc11p.code_desc cd on cd.code = lv.st_prov_cd
                           and cd.id = 'ST'      
  union
   --BRN_REG_NAME  Mailing Address
select null,                            --  objectid  number(9)
       bn.po_box || bn.zip_1_5 ||bn.zip_6_9 || bn.st_prov_cd || bn.str_name || bn.str_num || bn.muni_name legacykey,   --  legacykey  varchar2(500)
       bn.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       bn.dt_row_crtd,                  --  convcreateddate  date
       bn.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       bn.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       bn.zip_1_5,                      --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       bn.str_name,                     --  street  varchar2(60)
       bn.str_num,                      --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       bn.muni_name,                    --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       bn.po_box,                       --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       bn.zip_6_9,                      --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.BRN_REG_NAME bn      
  join abc11p.code_desc cd on cd.code = bn.st_prov_cd 
                           and cd.id = 'ST'      
 where bn.cntry_cd  = 'US'    
  
  union 
   --PERMIT  Permit Location Address
select null,                            --  objectid  number(9)
       p.po_box || p.zip_1_5 ||p.zip_6_9 || p.st_prov_cd || p.str_name || p.str_num || p.city legacykey,   --  legacykey  varchar2(500)
       p.crtd_oper_id,                  --  createdbyuserlegacykey  varchar2(500)
       p.dt_row_crtd,                   --  convcreateddate  date
       p.updt_oper_id,                  --  lastupdatedbyuserlegacykey  varchar2(500)
       p.dt_row_updt,                   --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       p.zip_1_5,                       --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       p.str_name,                      --  street  varchar2(60)
       p.str_num,                       --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       p.extra_addr_line,               --  line2  varchar2(60)
       p.city,                          --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       p.po_box,                        --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       p.zip_6_9,                       --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.PERMIT p      
  join abc11p.code_desc cd on cd.code = p.st_prov_cd 
                           and cd.id = 'ST'      
 where p.cntry_cd = 'US'  
 
 union
   --PERMIT  Permit Other Address
select null,                            --  objectid  number(9)
       p.oth_po_box || p.oth_zip_1_5 ||p.oth_zip_6_9 || p.oth_st_prov_cd || p.oth_str_name || p.oth_str_num || p.oth_city legacykey,   --  legacykey  varchar2(500)
       p.crtd_oper_id,                  --  createdbyuserlegacykey  varchar2(500)
       p.dt_row_crtd,                   --  convcreateddate  date
       p.updt_oper_id,                  --  lastupdatedbyuserlegacykey  varchar2(500)
       p.dt_row_updt,                   --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       p.oth_zip_1_5,                       --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       p.oth_str_name,                      --  street  varchar2(60)
       p.oth_str_num,                       --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       p.oth_city,                          --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       p.oth_po_box,                        --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       p.oth_zip_6_9,                       --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.PERMIT p      
  join abc11p.code_desc cd on cd.code = p.oth_st_prov_cd 
                           and cd.id = 'ST'      
 where p.oth_cntry_cd = 'US'   

  union 
 --PERMIT  Permit Event Address
select null,                            --  objectid  number(9)
      p.event_zip_1_5 ||p.event_zip_6_9 || p.event_st_prov_cd || p.event_str_name || p.event_str_num || p.event_city legacykey,   --  legacykey  varchar2(500)
       p.crtd_oper_id,                  --  createdbyuserlegacykey  varchar2(500)
       p.dt_row_crtd,                   --  convcreateddate  date
       p.updt_oper_id,                  --  lastupdatedbyuserlegacykey  varchar2(500)
       p.dt_row_updt,                   --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       p.event_zip_1_5,                 --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       p.event_str_name,                --  street  varchar2(60)
       p.event_str_num,                 --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       p.event_city,                          --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  othereraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       p.event_zip_6_9,                       --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.PERMIT p      
  join abc11p.code_desc cd on cd.code = p.event_st_prov_cd 
                           and cd.id = 'ST'      
  union  
  -- ADD_PRIV_MSTR Prem Address
select null,                            --  objectid  number(9)
       am.zip_1_5 ||am.zip_6_9 || am.st_prov_cd || am.str_name || am.str_num || am.muni_name legacykey,   --  legacykey  varchar2(500)
       am.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       am.dt_row_crtd,                  --  convcreateddate  date
       am.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       am.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       am.zip_1_5,                      --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       am.str_name,                     --  street  varchar2(60)
       am.str_num,                      --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       am.muni_name,                    --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       am.zip_6_9,                      --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N'                              --  streettype_rs  varchar2(1)
  from abc11p.Add_Priv_Mstr am      
  join abc11p.code_desc cd on cd.code = am.st_prov_cd 
                           and cd.id = 'ST'