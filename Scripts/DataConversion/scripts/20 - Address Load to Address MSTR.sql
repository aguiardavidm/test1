--Run time: 7 minutes
--drop table abc11p.address_mstr;
create table abc11p.ADDRESS_MSTR as select * from(

--insert into dataconv.o_abc_address   --Premisis Address NAME_MSTR    USA
select cast(null as number(9)) objectid,                            --  objectid  number(9)
       nm.po_box || nm.zip_1_5 ||nm.zip_6_9 || nm.st_prov_cd || nm.str_name || nm.str_num || nm.muni_name legacykey,   --  legacykey  varchar2(500)
       nm.crtd_oper_id createdbyuserlegacykey,                      --  createdbyuserlegacykey  varchar2(500)
       nm.dt_row_crtd convcreateddate,                              --  convcreateddate  date
       nm.updt_oper_id lastupdatedbyuserlegacykey,                  --  lastupdatedbyuserlegacykey  varchar2(500)
       nm.dt_row_updt lastupdateddate,                              --  lastupdateddate  date
       'N' rowstatus,                                               --  rowstatus  varchar2(1)
       'Civic' addresstype,                                         --  addresstype  varchar2(100)
       nm.zip_1_5 postalcode,                                       --  postalcode  varchar2(10)
       nm.cntry_cd country,                                               --  country  varchar2(100)
       cd.cd_desc State,                                            --  provincestate  varchar2(100)
       nm.str_name street,                                          --  street  varchar2(60)
       nm.str_num streetnumber,                                     --  streetnumber  varchar2(6)
       cast(null as varchar2(20)) streetnumbersuffix,               --  streetnumbersuffix  varchar2(20)
       cast(null as varchar2(20)) unitnumber,                       --  unitnumber  varchar2(20)
       cast(null as varchar2(60)) line2,                            --  line2  varchar2(60)
       nm.muni_name citytown,                                       --  citytown  varchar2(100)
       cast(null as varchar2(100))  streetdirection,                --  streetdirection  varchar2(100)
       cast(null as varchar2(400))otheraddress,                     --  otheraddress  varchar2(400)
       cast(null as varchar2(150))  po_additionaldeliveryinfo,      --  po_additionaldeliveryinfo  varchar2(150)
       nm.po_box po_postofficebox,                                  --  po_postofficebox  varchar2(20)
       cast(null as varchar2(40)) po_stationinformation,            --  po_stationinformation  varchar2(40)
       cast(null as varchar2(60))rr_additionaladdressinfo,          --  rr_additionaladdressinfo  varchar2(60)
       cast(null as varchar2(20)) rr_ruralrouteidentifier,          --  rr_ruralrouteidentifier  varchar2(20)
       cast(null as varchar2(40)) rr_stationinformation,            --  rr_stationinformation  varchar2(40)
       cast(null as varchar2(2))  gd_generaldeliveryindicator,      --  gd_generaldeliveryindicator  varchar2(2)
       cast(null as varchar2(40)) gd_stationinformation,            --  gd_stationinformation  varchar2(40)
       'Y' isconverted,                                             --  isconverted  char(1)
       'Y' convertedaddress,                                        --  convertedaddress  varchar2(4000)
       nm.zip_6_9 zipcodeextension,                                 --  zipcodeextension  number(4)
       cast(null as varchar2(500)) streettypelk,                    --  streettypelk  varchar2(500)
       cast(null as number(9)) streettyperid,                       --  streettyperid  number(9)
       'N' streettype_rs,                                           --  streettype_rs  varchar2(1)
       cast(nm.abc11puk || 'PREM' as varchar2(100)) abc11puk        -- abc11puk from source table
  from abc11p.name_mstr nm      
  left outer join abc11p.code_desc cd on cd.code = nm.st_prov_cd 
                           and cd.id = 'ST'                              

union all
 --Mailing Address     NAME_MSTR USA
select null,                            --  objectid  number(9)
       nm.mail_po_box || nm.mail_zip_1_5 ||nm.mail_zip_6_9 || nm.mail_st_prov_cd || nm.mail_str_name || nm.mail_str_num || nm.mail_muni_name legacykey,   --  legacykey  varchar2(500)
       nm.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       nm.dt_row_crtd,                  --  convcreateddate  date
       nm.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       nm.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       nm.mail_zip_1_5,                 --  postalcode  varchar2(10)
       nm.mail_cntry_cd,                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       nm.mail_str_name,                --  street  varchar2(60)
       nm.mail_str_num,                 --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       nm.mail_muni_name,               --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       nm.mail_po_box,                  --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       nm.mail_zip_6_9,                 --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       nm.abc11puk || 'MAIL'            -- abc11puk from source table
  from abc11p.name_mstr nm    
  left outer join abc11p.code_desc cd on cd.code = nm.mail_st_prov_cd 
                           and cd.id = 'ST'      
 union all
 --Warehouse Address  LIC_MSTR
select null,                            --  objectid  number(9)
       lm.war_zip_1_5 ||lm.war_zip_6_9 || lm.war_st || lm.war_str_name || lm.war_str_num || lm.war_muni legacykey,   --  legacykey  varchar2(500)
       lm.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       lm.dt_row_crtd,                  --  convcreateddate  date
       lm.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       lm.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       lm.war_zip_1_5,                  --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       lm.war_str_name,                 --  street  varchar2(60)
       lm.war_str_num,                  --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       lm.war_muni,                     --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       lm.war_zip_6_9,                  --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       lm.abc11puk  || 'WARH'            -- abc11puk from source table
  from abc11p.lic_mstr lm      
  left outer join abc11p.code_desc cd on cd.code = lm.war_st 
                           and cd.id = 'ST'      
  union all

 --Pending License Prem Address  LIC_PEND
select null,                            --  objectid  number(9)
       lm.bus_zip_1_5 ||lm.bus_zip_6_9 || lm.bus_st || lm.bus_str_name || lm.bus_str_num legacykey,   --  legacykey  varchar2(500)
       lm.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       lm.dt_row_crtd,                  --  convcreateddate  date
       lm.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       lm.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       lm.bus_zip_1_5,                  --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       lm.bus_str_name,                 --  street  varchar2(60)
       lm.bus_str_num,                  --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       null,--lm.bus_muni,                     --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       lm.bus_zip_6_9,                  --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       lm.abc11puk  || 'PREM'            -- abc11puk from source table
  from abc11p.lic_pend lm      
  left outer join abc11p.code_desc cd on cd.code = lm.bus_st
                           and cd.id = 'ST'
union all
 --Pending License Mail Address  LIC_PEND
select null,                            --  objectid  number(9)
       lm.mail_zip_1_5 ||lm.mail_zip_6_9 || lm.mail_st || lm.mail_str_name || lm.mail_str_num legacykey,   --  legacykey  varchar2(500)
       lm.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       lm.dt_row_crtd,                  --  convcreateddate  date
       lm.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       lm.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       lm.mail_zip_1_5,                  --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       lm.mail_str_name,                 --  street  varchar2(60)
       lm.mail_str_num,                  --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       null,--lm.mail_muni,                     --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       lm.mail_zip_6_9,                  --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       lm.abc11puk  || 'MAIL'            -- abc11puk from source table
  from abc11p.lic_pend lm      
  left outer join abc11p.code_desc cd on cd.code = lm.mail_st
                           and cd.id = 'ST'
 union all
 --Vehicle Storage Address  LIC_VEH_VES
select null,                            --  objectid  number(9)
       lv.zip_1_5 ||lv.zip_6_9 || lv.st_prov_cd || lv.str_name || lv.str_num || lv.muni_name legacykey,   --  legacykey  varchar2(500)
       lv.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       lv.dt_row_crtd,                  --  convcreateddate  date
       lv.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       lv.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       lv.zip_1_5,                      --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       lv.str_name,                     --  street  varchar2(60)
       lv.str_num,                      --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       lv.muni_name,                    --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       lv.zip_6_9,                      --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       lv.abc11puk || 'STRG'            -- abc11puk from source table
  from abc11p.LIC_VEH_VES lv     
  left outer join abc11p.code_desc cd on cd.code = lv.st_prov_cd
                           and cd.id = 'ST'      
  union all
   --BRN_REG_NAME  Mailing Address USA
select null,                            --  objectid  number(9)
       bn.po_box || bn.zip_1_5 ||bn.zip_6_9 || bn.st_prov_cd || bn.str_name || bn.str_num || bn.muni_name legacykey,   --  legacykey  varchar2(500)
       bn.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       bn.dt_row_crtd,                  --  convcreateddate  date
       bn.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       bn.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       bn.zip_1_5,                      --  postalcode  varchar2(10)
       bn.cntry_cd,                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       bn.str_name,                     --  street  varchar2(60)
       bn.str_num,                      --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       bn.muni_name,                    --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       bn.po_box,                       --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       bn.zip_6_9,                      --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       bn.abc11puk || 'MAIL'            -- abc11puk from source table
  from abc11p.BRN_REG_NAME bn      
  left outer join abc11p.code_desc cd on cd.code = bn.st_prov_cd 
                           and cd.id = 'ST'       
  
  union all 
   --PERMIT  Permit Location Address USA
select null,                            --  objectid  number(9)
       p.po_box || p.zip_1_5 ||p.zip_6_9 || p.st_prov_cd || p.str_name || p.str_num || p.city legacykey,   --  legacykey  varchar2(500)
       p.crtd_oper_id,                  --  createdbyuserlegacykey  varchar2(500)
       p.dt_row_crtd,                   --  convcreateddate  date
       p.updt_oper_id,                  --  lastupdatedbyuserlegacykey  varchar2(500)
       p.dt_row_updt,                   --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       p.zip_1_5,                       --  postalcode  varchar2(10)
        p.cntry_cd,                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       p.str_name,                      --  street  varchar2(60)
       p.str_num,                       --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       p.extra_addr_line,               --  line2  varchar2(60)
       p.city,                          --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       p.po_box,                        --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       p.zip_6_9,                       --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       p.abc11puk || 'PREM'             -- abc11puk from source table
  from abc11p.PERMIT p      
  left outer join abc11p.code_desc cd on cd.code = p.st_prov_cd 
                           and cd.id = 'ST'      
 
 union all
   --PERMIT  Permit Other Address USA
select null,                            --  objectid  number(9)
       p.oth_po_box || p.oth_zip_1_5 ||p.oth_zip_6_9 || p.oth_st_prov_cd || p.oth_str_name || p.oth_str_num || p.oth_city legacykey,   --  legacykey  varchar2(500)
       p.crtd_oper_id,                  --  createdbyuserlegacykey  varchar2(500)
       p.dt_row_crtd,                   --  convcreateddate  date
       p.updt_oper_id,                  --  lastupdatedbyuserlegacykey  varchar2(500)
       p.dt_row_updt,                   --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       p.oth_zip_1_5,                       --  postalcode  varchar2(10)
       oth_cntry_cd ,                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       p.oth_str_name,                      --  street  varchar2(60)
       p.oth_str_num,                       --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       p.oth_city,                          --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       p.oth_po_box,                        --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       p.oth_zip_6_9,                       --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       p.abc11puk || 'OTHR'             -- abc11puk from source table
  from abc11p.PERMIT p      
  left outer join abc11p.code_desc cd on cd.code = p.oth_st_prov_cd 
                           and cd.id = 'ST'

  union all 
 --PERMIT  Permit Event Address
select null,                            --  objectid  number(9)
      p.event_zip_1_5 ||p.event_zip_6_9 || p.event_st_prov_cd || p.event_str_name || p.event_str_num || p.event_city legacykey,   --  legacykey  varchar2(500)
       p.crtd_oper_id,                  --  createdbyuserlegacykey  varchar2(500)
       p.dt_row_crtd,                   --  convcreateddate  date
       p.updt_oper_id,                  --  lastupdatedbyuserlegacykey  varchar2(500)
       p.dt_row_updt,                   --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       p.event_zip_1_5,                 --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       p.event_str_name,                --  street  varchar2(60)
       p.event_str_num,                 --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       p.event_city,                          --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  othereraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       p.event_zip_6_9,                       --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       p.abc11puk || 'EVNT'             -- abc11puk from source table
  from abc11p.PERMIT p      
  left outer join abc11p.code_desc cd on cd.code = p.event_st_prov_cd 
                           and cd.id = 'ST'      
  union all  
  -- ADD_PRIV_MSTR Prem Address
select null,                            --  objectid  number(9)
       am.zip_1_5 ||am.zip_6_9 || am.st_prov_cd || am.str_name || am.str_num || am.muni_name legacykey,   --  legacykey  varchar2(500)
       am.crtd_oper_id,                 --  createdbyuserlegacykey  varchar2(500)
       am.dt_row_crtd,                  --  convcreateddate  date
       am.updt_oper_id,                 --  lastupdatedbyuserlegacykey  varchar2(500)
       am.dt_row_updt,                  --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       am.zip_1_5,                      --  postalcode  varchar2(10)
       'USA',                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       am.str_name,                     --  street  varchar2(60)
       am.str_num,                      --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       am.muni_name,                    --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       null,                            --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       am.zip_6_9,                      --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       am.abc11puk || 'PREM'            -- abc11puk from source table
  from abc11p.Add_Priv_Mstr am      
  left outer join abc11p.code_desc cd on cd.code = am.st_prov_cd 
                           and cd.id = 'ST')a;

  alter table abc11p.address_mstr
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);
  alter table ABC11P.ADDRESS_MSTR modify addresstype VARCHAR2(100);
    alter table ABC11P.ADDRESS_MSTR modify country VARCHAR2(100);   
grant all on abc11p.address_mstr to posseextensions;
  --Remove Erroneous Data that does not qualify as an address (Largest set is an address that only has a state populated)
               
  delete
    from abc11p.address_mstr am 
   where am.legacykey is null;  
  delete 
    from abc11p.address_mstr  am
  where length(am.legacykey) = 2;
  --Run Unique Key for ABC11P.ADDRESS_MSTR
  update abc11p.address_mstr am
    set am.legacykey = am.abc11puk;
  --Transform States from all Caps to Init Caps               
  update abc11p.address_mstr am
   set am.State = InitCap(am.state);               
  -- Correct District Of Columbia
  update abc11p.address_mstr am
    set am.State ='District of Columbia'
   where am.State = 'District Of Columbia';
  update abc11p.address_mstr am
    set am.COUNTRY = 'USA'
   where am.COUNTRY = 'US';
  update abc11p.address_mstr am
    set am.Addresstype = 'PO Box'  
   where am.PO_POSTOFFICEBOX is not null;
 update abc11p.address_mstr am
  set am.COUNTRY = (select cd.cd_desc
            from abc11p.code_desc cd
           where cd.id = 'CY'
             and cd.code != 'US'
             and cd.code = am.COUNTRY)
  where am.country != 'USA';         
  update abc11p.address_mstr am
    set am.Addresstype = 'Other / International'  
  where am.citytown is null or
    am.state is null or 
    am.postalcode is null or
    (am.po_postofficebox is null and (am.street is null or am.streetnumber is null))
  or am.COUNTRY != 'USA';
   update abc11p.address_mstr am
     set am.OTHERADDRESS = am.streetnumber || ' ' || am.street || ' ' || am.unitnumber || ' ' || am.line2 || ' 
   ' || am.CityTown || ' ' || am.Country || ' ' || am.PostalCode || ' ' || am.zipcodeextension
   ,am.State = null
   ,am.country = null
  where am.Addresstype = 'Other / International';
  
 commit; 

   -- Load Data into dataconv.o_abc_address
   
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
  update abc11p.address_mstr am
        set am.errortext = a_ErrorText,
            am.errorcode = a_ErrorCode
      where am.rowid = a_RowId;  
  
    commit;
  end;

begin
 
  for c in (select rowid,
                   objectid,
                   legacykey,
                   createdbyuserlegacykey,
                   convcreateddate ,
                   lastupdatedbyuserlegacykey,
                   lastupdateddate,   
                   rowstatus,
                   addresstype,
                   postalcode, 
                   country, 
                   state,
                   street, 
                   streetnumber, 
                   streetnumbersuffix, 
                   unitnumber, 
                   line2, 
                   citytown, 
                   streetdirection, 
                   otheraddress,
                   po_additionaldeliveryinfo,
                   po_postofficebox, 
                   po_stationinformation,
                   rr_additionaladdressinfo, 
                   rr_ruralrouteidentifier,
                   rr_stationinformation, 
                   gd_generaldeliveryindicator, 
                   gd_stationinformation, 
                   isconverted, 
                   convertedaddress, 
                   zipcodeextension,
                   streettypelk, 
                   streettyperid,
                   streettype_rs      
             from abc11p.address_mstr am
            where am.processed is null) loop
    begin
    insert into dataconv.o_abc_address a
    ( objectid                    ,
	  legacykey                   ,
	  createdbyuserlegacykey      ,
	  convcreateddate             ,
	  lastupdatedbyuserlegacykey  ,
	  lastupdateddate             ,
	  rowstatus                   ,
	  addresstype                 ,
	  postalcode                  ,
	  country                     ,
	  provincestate               ,
	  street                      ,
	  streetnumber                ,
	  streetnumbersuffix          ,
	  unitnumber                  ,
	  line2                       ,
	  citytown                    ,
	  streetdirection             ,
	  otheraddress                ,
	  po_additionaldeliveryinfo   ,
	  po_postofficebox            ,
	  po_stationinformation       ,
	  rr_additionaladdressinfo    ,
	  rr_ruralrouteidentifier     ,
	  rr_stationinformation       ,
	  gd_generaldeliveryindicator ,
	  gd_stationinformation       ,
	  isconverted                 ,
	  convertedaddress            ,
	  zipcodeextension             )
	
	values ( c.objectid,
             c.legacykey,
             c.createdbyuserlegacykey,
             c.convcreateddate ,
             c.lastupdatedbyuserlegacykey,
             c.lastupdateddate,   
             c.rowstatus,
             c.addresstype,
             c.postalcode, 
             c.country, 
             c.state,
             c.street, 
             c.streetnumber, 
             c.streetnumbersuffix, 
             c.unitnumber, 
             c.line2, 
             c.citytown, 
             c.streetdirection, 
             c.otheraddress,
             c.po_additionaldeliveryinfo,
             c.po_postofficebox, 
             c.po_stationinformation,
             c.rr_additionaladdressinfo, 
             c.rr_ruralrouteidentifier,
             c.rr_stationinformation, 
             c.gd_generaldeliveryindicator, 
             c.gd_stationinformation, 
             c.isconverted, 
             c.convertedaddress, 
             c.zipcodeextension);   
    
    update abc11p.address_mstr am
        set am.processed = 'Y',
            am.processeddate = sysdate
      where c.rowid = am.rowid;   
      
    t_count := t_count +1; 
    
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
  commit;
  
end;
/
create index abc11p.ADDRESS_MSTR_IXLK
on abc11p.address_mstr (LEGACYKEY);