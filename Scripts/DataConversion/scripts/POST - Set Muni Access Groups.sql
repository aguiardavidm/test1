-- Created on 4/28/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select objectid from query.o_abc_office) loop
    abc.pkg_ABC_Municipality.AssignAccessGroup(i.objectid, sysdate);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;