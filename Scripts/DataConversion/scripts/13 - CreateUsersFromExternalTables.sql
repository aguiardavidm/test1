--Run time: 13 seconds
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_PermitType      varchar2(100);
    -- Local variables here
  i integer;
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
 dbms_output.put_line(a_ErrorText);
  end;

-- Created on 8/25/2014 by Keaton 

begin
  -- Test statements here
  

  
  for c in(select   'OPS$DAG'||DAGCODE  LEGACYKEY,
        DATAACTIVE          ACTIVE,  
        DAGNAME          USERNAME,      
        'OPS$DAG'||DAGCODE   ORACLELOGONID,  
        'password'          PASSWORD,      
        DAGFONE             PHONENUMBER,
        substr(DAGNAME,0,(instr(DAGNAME,' ',1)))             FIRSTNAME,    
        substr(DAGNAME,(instr(DAGNAME,' ',1)))             LASTNAME
        from abc11p.P_tblDAGFile_int_t p
      where not exists (select *
                                from api.users u
                               where u.oraclelogonid =  'OPS$DAG'||p.DAGCODE)  
      union
      select   'OPS$DAG'||DAGCODE  LEGACYKEY,
         DATAACTIVE          ACTIVE,  
         DAGNAME          USERNAME,      
         'OPS$DAG'||DAGCODE   ORACLELOGONID,  
         'password'          PASSWORD,      
         DAGFONE             PHONENUMBER,
         substr(DAGNAME,0,(instr(DAGNAME,' ',1)))             FIRSTNAME,    
         substr(DAGNAME,(instr(DAGNAME,' ',1)))             LASTNAME
        from abc11p.A_tblDAGFile_int_t a             
            where not exists (select *
                                from api.users u
                               where u.Name =  a.DAGNAME)) loop
  begin
  
  -- 
  api.pkg_logicaltransactionupdate.ResetTransaction();
  i := api.pkg_userupdate.New('Default',
                          c.USERNAME,
                          upper(c.LEGACYKEY),
                          upper(c.LEGACYKEY),
                          'njdev');
  
  api.pkg_columnupdate.SetValue(i,'FirstName', c.Firstname);
  api.pkg_columnupdate.SetValue(i,'LastName', c.Lastname);
  api.pkg_columnupdate.SetValue(i,'UserType', 'Internal');
   api.pkg_logicaltransactionupdate.EndTransaction();
   commit;
  t_count := t_count +1;
  
  exception when others then
    LogError('UserName: '||c.username||' '||SQLERRM, SQLCODE);
    rollback;
  end;
  
  
  end loop;
  
    

  
  dbms_output.put_line(t_count);
  
end;