--Set Created by user on appeal jobs
update dataconv.j_Abc_Appeal a
     set CreatedByUserLegacyKey = 'ABCCONV';
commit;
--Fix solicitor countylk
update dataconv.O_ABC_PERMIT p
   set countylk = '34'
 where countylk like '34%';
 update dataconv.O_ABC_PERMIT p
   set countylk = '99'
 where countylk like '99%';
commit;
--Separate petition types into individual 12.18 and 12.39
declare 
  i integer;
begin
  for i in (select pt.legacykey,
                   pt.startdate,
                   pt.enddate,
                   (select r.j_abc_petition
                      from dataconv.r_abc_petitionpetitiontype r
                     where r.o_abc_petitiontype = pt.legacykey) j_abc_petition
              from dataconv.O_ABC_PETITIONTYPE pt
             where pt.petitiontype != '12.18'
               and pt.petitiontype != '12.39') loop
    insert into dataconv.o_abc_petitiontype t
    (
    t.legacykey,
    t.startdate,
    t.enddate,
    t.petitiontype
    )
    values
    (
    i.legacykey || '39',
    i.startdate,
    i.enddate,
    '12.39'
    );
    insert into dataconv.r_abc_petitionpetitiontype
    (
    j_abc_petition,
    o_abc_petitiontype 
    )
    values
    (
    i.j_abc_petition,
    i.legacykey || '39'
    );
  end loop
  commit;
end;
/
update dataconv.O_ABC_PETITIONTYPE pt
   set pt.petitiontype = '12.18'
 where pt.petitiontype != '12.18'
   and pt.petitiontype != '12.39';
commit;
--Update LK for user
update dataconv.j_abc_petition p
set p.attorneygenerallk =                     
(select u.legacykey
  from dataconv.u_users u
 where u.oraclelogonid in (select 'OPS$DAG'|| j.attorneygenerallk
                             from dataconv.j_abc_petition j
                            where j.legacykey = p.legacykey))
where exists (select u.legacykey
  from dataconv.u_users u
 where u.oraclelogonid in (select 'OPS$DAG'|| j.attorneygenerallk
                             from dataconv.j_abc_petition j
                            where j.legacykey = p.legacykey)) ;
--Remove invalid petitions and their related petition types
delete
  from dataconv.j_abc_petition p
  where not exists (select 1 from dataconv.o_abc_license l where l.legacykey = p.licenselk);
delete
  from dataconv.R_ABC_PETITIONPETITIONTYPE r
 where not exists (select 1 from dataconv.j_abc_petition p where p.legacykey = r.j_abc_petition);
--Clean LE Preferred contact method
delete
  from possedata.objectcolumndata cd
 where cd.ColumnDefId = api.pkg_configquery.ColumnDefIdForName('o_abc_LegalEntity', 'PreferredContactMethod')
   and cd.ObjectId in (select le.ObjectId
                         from dataconv.o_abc_legalentity le
                        where le.Mailingaddresslk is null and le.Physicaladdresslk is null);
commit;
-- Bad users on Petition Job set to ABCONV
-- Set Correct User
update dataconv.j_abc_petition p
set p.attorneygenerallk =                     
(select u.legacykey
  from dataconv.u_users u
 where u.oraclelogonid in (select 'OPS$DAG'|| j.attorneygenerallk
                             from dataconv.j_abc_petition j
                            where j.legacykey = p.legacykey))
where exists (select u.legacykey
  from dataconv.u_users u
 where u.oraclelogonid in (select 'OPS$DAG'|| j.attorneygenerallk
                             from dataconv.j_abc_petition j
                            where j.legacykey = p.legacykey)) ;
---- Set ABCCONV where user is not translateable
update dataconv.j_abc_petition p
set p.attorneygenerallk = 'ABCCONV'                    
where not exists (select u.legacykey
  from dataconv.u_users u
 where u.legacykey in (select j.attorneygenerallk
                             from dataconv.j_abc_petition j
                            where j.legacykey = p.legacykey)) ;
                            commit;

--Set manual dup column
insert into dataconv.posseconvmanualdupcolumns
(
TABLENAME,
COLUMNNAME
)
VALUES
(
'O_ABC_LICENSE',
'DUP_EXPDATEPLUSGRACEPERIOD'
);
COMMIT;