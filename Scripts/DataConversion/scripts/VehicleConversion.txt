-- Created on 2/23/2015 by PAUL 
declare 
  -- Local variables here
  i integer;
  t_RowsProcessed number := 0;

begin
  if user != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20000, 'Script must be run as POSSEDBA');
  end if;
  -- Test statements here
  for i in (select  abc11puk                        LEGACYKEY,
                    UPDT_OPER_ID                    CREATEDBYUSERLEGACYKEY,
                    DT_ROW_UPDT                     CONVCREATEDDATE,
                    UPDT_OPER_ID                    LASTUPDATEDBYUSERLEGACYKEY,
                    DT_ROW_UPDT                     LASTUPDATEDDATE,
                    (select l.abc11puk
                       from abc11p.lic_mstr l 
                      where l.CNTY_MUNI_CD || l.LIC_TYPE_CD || l.MUNI_SER_NUM || l.GEN_NUM = 
                            v.CNTY_MUNI_CD || v.LIC_TYPE_CD || v.MUNI_SER_NUM || v.GEN_NUM) LICENSELK,
                    (case 
                       when FNCD_NAME_MSTR_ID_NUM is not null 
                         then 'Financed'
                       when LSD_NAME_MSTR_ID_NUM is not null
                         then 'Leased'
                       when OWN_NAME_MSTR_ID_NUM is not null
                         then 'Owned'
                     end) OWNEDORLEASEDVESSEL,
                    INSIG_NUM                       INSIGNIANUMBER,
                    MAKE || MODEL ||MODEL_YEAR      MAKEMODELYEAR,
                    VIN                             VIN,
                    USCG_REG_NUM                    USCGREGISTRATIONNUMBER,
                    VESSEL_NAME                     VESSELNAME,
                    VESSEL_LENGTH                   VESSELLENGTH,
                    v.abc11puk || 'STRG'            INTERNALSTORAGELK,
                    PLATE_NUM                       PlateNum,
                    decode(VEH_VES_LIC_TYPE, 'LIMO', 'Limousine', 'BOAT', 'Boat', 'AIR', 'Plane') VEHICLETYPE
              from abc11p.lic_veh_ves v
             where v.abc11puk not in (select v2.legacykey
                                        from dataconv.o_abc_vehicle v2)) loop
    insert into dataconv.o_abc_vehicle
    (
    LEGACYKEY,
    CREATEDBYUSERLEGACYKEY,
    CONVCREATEDDATE,
    LASTUPDATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE,
    OWNEDORLEASEDVESSEL,
    OWNEDORLEASEDLIMOUSINE,
    INSIGNIANUMBER,
    MAKEMODELYEAR,
    VIN,
    USCGREGISTRATIONNUMBER,
    VESSELNAME,
    VESSELLENGTH,
    ADDRESSLK,
--    PlateNum,  Detail does not exist
    VEHICLETYPE
    )
    values 
    (
    i.LEGACYKEY,
    i.CREATEDBYUSERLEGACYKEY,
    i.CONVCREATEDDATE,
    i.LASTUPDATEDBYUSERLEGACYKEY,
    i.LASTUPDATEDDATE,
    i.OWNEDORLEASEDVESSEL,
    i.OWNEDORLEASEDVESSEL,
    i.INSIGNIANUMBER,
    i.MAKEMODELYEAR,
    i.VIN,
    i.USCGREGISTRATIONNUMBER,
    i.VESSELNAME,
    i.VESSELLENGTH,
    i.INTERNALSTORAGELK,
--    i.PlateNum,
    i.VEHICLETYPE
    );
    insert into dataconv.r_abc_licensevehicle
    (
    O_ABC_LICENSE,
    O_ABC_VEHICLE
    )
    values
    (
    i.legacykey,
    i.licenselk
    );
  t_RowsProcessed := t_RowsProcessed+1;
  if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
    end if;
  end loop;
  for i in (select * 
              from abc11p.perm_vehicle v
             where v.abc11puk not in (select legacykey from dataconv.o_abc_vehicle)) loop
    insert into dataconv.o_Abc_Vehicle
    (
    LEGACYKEY,
    LASTUPDATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE,
    INSIGNIANUMBER,
    MAKEMODELYEAR,
    VIN
    )
    values
    (
    i.abc11puk,
    i.updt_oper_id,
    i.dt_row_updt,
    i.decal_num,
    i.make || ', ' || i.model || ', ' || i.model_yr,
    i.vin
    );/* RELATIONSHIP DOES NOT EXIST
    insert into dataconv.r_ABC_PermitVehicle
    (
    O_ABC_PERMIT,
    O_ABC_VEHICLE
    )
    values
    (
    i.perm_num,
    i.abc11puk
    );*/
  t_RowsProcessed := t_RowsProcessed+1;
  if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
    end if;
  end loop;
  dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
  commit;
end;