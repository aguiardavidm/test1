declare
begin
  for i in (select cm.name_mstr_id_num OnlineAccessCode,
                   le.objectid,
                   api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'OnlineAccessCode') ColumnDefId
              from dataconv.o_abc_legalentity le
              join abc11p.name_mstr nm on le.legacykey = to_char(nm.name_mstr_id_num)
              join abc11p.lic_codemaster cm on substr(cm.abc_num, 1, 11) = substr(nm.abc_num, 1, 11)
             where nm.name_type_cd = '2.1-LICENSEE'
               and nm.srce_grp_cd != 'GENERAL'
               and nm.name_stat != 'X') loop
               
    update possedata.objectcolumndata_t t
       set attributevalue = i.onlineaccesscode,
           t.searchvalue = i.onlineaccesscode
     where t.objectid = i.objectid
       and t.columndefid = i.columndefid;
  end loop;
end;
/

declare
begin
  for i in (select nm.reg_name_mstr_id_num OnlineAccessCode,
                   le.objectid,
                   api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'OnlineAccessCode') ColumnDefId
              from dataconv.o_abc_legalentity le
              join abc11p.brn_reg_name nm on le.legacykey = to_char(nm.reg_name_mstr_id_num)) loop
               
    update possedata.objectcolumndata_t t
       set attributevalue = i.onlineaccesscode,
           t.searchvalue = i.onlineaccesscode
     where t.objectid = i.objectid
       and t.columndefid = i.columndefid;
  end loop;
end;
commit;