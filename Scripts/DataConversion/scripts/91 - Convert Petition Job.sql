--Run time: 4 minutes
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_LegalEntityType varchar2(100);
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
  update abc11p.P_tblABCAppeal_int_t am
        set am.errortext = a_ErrorText,
            am.errorcode = a_ErrorCode
      where am.rowid = a_RowId;  
  
    commit;
  end;

begin

  select legacykey
    into t_ConvUserKey 
   from dataconv.u_users u
  where u.oraclelogonid = 'ABCCONV';

  for c in (select t.rowid,
                   substr(t.LICENSE_NO,1,9) ||lpad((select max(substr(l.legacykey,11,3)) 
                                          from dataconv.o_abc_license l 
                                         where substr(l.legacykey,1,9)= 
                                           to_char(substr(LICENSE_NO,0,4) ||
                                                   substr(LICENSE_NO,5,2) ||
                                                   substr(LICENSE_NO,7,3))),3,'0') LICENSE_NO,
                   t.DOCKET_SEQ,
                   t.DOCKET_NUM,
                   to_date(t.DOC_DATE, 'MM/DD/YY HH24:MI:SS') DOC_DATE,
                   to_date(t.DATE_RECD, 'MM/DD/YY HH24:MI:SS') DATE_RECD,
                   t.ASSIGN_TO,
                   decode (t.STATUS, 
                           'DO', 'REVIEW',
                           'RC', 'COMP',
                           'OAL', 'REVIEW',
                           'NN', 'COMP',
                           'REVIEW') status,
                   to_date(t.CLOSE_DATE, 'MM/DD/YY HH24:MI:SS') CLOSE_DATE,
                   'Date to DAG: ' || t.DATE_TODAG || chr(10) ||
                   'Date to OAL: ' || t.DATE_TOOAL || chr(10) ||
                   'Docket SEQ Conv: ' || t.DOCKET_SEQ_CONVERT || chr(10) ||
                   'TRANS_CODE: ' || t.TRANS_CODE || chr(10) ||
                   'Special Condition: ' || t.SPEC_COND || chr(10) ||
                   'Action: ' || t.ACTION || chr(10) ||
                   'Comment: ' || t.comment_ac,
                   t.grant_yr1,  
                   t.grant_yr2,
                   t.grant_yr3,
                   t.grant_yr4,
                   t.grant_yr5,
                   t.grant_yr6,
                   t.grant_yr7,
                   t.Matter
             from abc11p.P_tblABCAppeal_int_t t
            where t.processed is null
              and t.docket_seq is not null) loop
    begin
    insert into dataconv.j_abc_petition j
    (LICENSELK,
     LEGACYKEY,
     EXTERNALFILENUM,
     AppealNumber,
     convcreateddate ,
     RECEIVEDDATE,
     attorneygenerallk ,
     statustag ,
     COMPLETEDDATE)
        values (c.LICENSE_NO,
                c.DOCKET_SEQ,
                c.DOCKET_SEQ,
                c.DOCKET_NUM,
                c.DOC_DATE,
                c.DATE_RECD,
                c.ASSIGN_TO,
                c.STATUS,
                c.CLOSE_DATE
                 );  

    if c.grant_yr1 is not null then
      insert into dataconv.o_abc_petitiontype pt
        (LEGACYKEY,
         PETITIONTYPE,
         STARTDATE,
         ENDDATE)
      values
        (c.docket_seq||c.grant_yr1,
         c.matter,
         to_date('7/1/'||substr(c.grant_yr1,0,2), 'MM/DD/RR'),
         to_date('6/30/'||substr(c.grant_yr1,3,2), 'MM/DD/RR'));  
       insert into dataconv.r_abc_petitionpetitiontype
        (j_abc_petition ,
         o_abc_petitiontype )
       values
        (c.docket_seq,
         c.docket_seq||c.grant_yr1);    
    end if;               
    
    if c.grant_yr2 is not null then
      insert into dataconv.o_abc_petitiontype pt
        (LEGACYKEY,
         PETITIONTYPE,
         STARTDATE,
         ENDDATE)
      values
        (c.docket_seq||c.grant_yr2,
         c.matter,
         to_date('7/1/'||substr(c.grant_yr2,0,2), 'MM/DD/RR'),
         to_date('6/30/'||substr(c.grant_yr2,3,2), 'MM/DD/RR'));
      insert into dataconv.r_abc_petitionpetitiontype
        (j_abc_petition ,
         o_abc_petitiontype )
       values
        (c.docket_seq,
         c.docket_seq||c.grant_yr2);    
    end if;
    
    if c.grant_yr3 is not null then
      insert into dataconv.o_abc_petitiontype pt
        (LEGACYKEY,
         PETITIONTYPE,
         STARTDATE,
         ENDDATE)
      values
        (c.docket_seq||c.grant_yr3,
         c.matter,
         to_date('7/1/'||substr(c.grant_yr3,0,2), 'MM/DD/RR'),
         to_date('6/30/'||substr(c.grant_yr3,3,2), 'MM/DD/RR'));  
       insert into dataconv.r_abc_petitionpetitiontype
        (j_abc_petition ,
        o_abc_petitiontype )
       values
        (c.docket_seq,
         c.docket_seq||c.grant_yr3);    
    end if;
    
    if c.grant_yr4 is not null then
      insert into dataconv.o_abc_petitiontype pt
        (LEGACYKEY,
         PETITIONTYPE,
         STARTDATE,
         ENDDATE)
      values
        (c.docket_seq||c.grant_yr4,
         c.matter,
         to_date('7/1/'||substr(c.grant_yr4,0,2), 'MM/DD/RR'),
         to_date('6/30/'||substr(c.grant_yr4,3,2), 'MM/DD/RR'));
       insert into dataconv.r_abc_petitionpetitiontype
        (j_abc_petition ,
        o_abc_petitiontype )
       values
        (c.docket_seq,
         c.docket_seq||c.grant_yr4);    
    end if;
    
    if c.grant_yr5 is not null then
      insert into dataconv.o_abc_petitiontype pt
        (LEGACYKEY,
         PETITIONTYPE,
         STARTDATE,
         ENDDATE)
      values
        (c.docket_seq||c.grant_yr5,
         c.matter,
         to_date('7/1/'||substr(c.grant_yr5,0,2), 'MM/DD/RR'),
         to_date('6/30/'||substr(c.grant_yr5,3,2), 'MM/DD/RR'));
       insert into dataconv.r_abc_petitionpetitiontype
        (j_abc_petition ,
        o_abc_petitiontype )
       values
        (c.docket_seq,
         c.docket_seq||c.grant_yr5);  
    end if;
    
    if c.grant_yr6 is not null then
      insert into dataconv.o_abc_petitiontype pt
        (LEGACYKEY,
         PETITIONTYPE,
         STARTDATE,
         ENDDATE)
      values
        (c.docket_seq||c.grant_yr6,
         c.matter,
         to_date('7/1/'||substr(c.grant_yr6,0,2), 'MM/DD/RR'),
         to_date('6/30/'||substr(c.grant_yr6,3,2), 'MM/DD/RR')); 
       insert into dataconv.r_abc_petitionpetitiontype
        (j_abc_petition ,
        o_abc_petitiontype )
       values
        (c.docket_seq,
         c.docket_seq||c.grant_yr6);     
    end if;
    
    if c.grant_yr7 is not null then
      insert into dataconv.o_abc_petitiontype pt
        (LEGACYKEY,
         PETITIONTYPE,
         STARTDATE,
         ENDDATE)
      values
        (c.docket_seq||c.grant_yr7,
         c.matter,
         to_date('7/1/'||substr(c.grant_yr7,0,2), 'MM/DD/RR'),
         to_date('6/30/'||substr(c.grant_yr7,3,2), 'MM/DD/RR'));
       insert into dataconv.r_abc_petitionpetitiontype
        (j_abc_petition ,
        o_abc_petitiontype )
       values
        (c.docket_seq,
         c.docket_seq||c.grant_yr7);    
    end if;
      
    t_count := t_count +1; 
    
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + 1;          
   
   end; 
   
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;
/

declare

t_count number := 0;

begin 
for c in (select count(*), pt.legacykey, max(pt.rowid) crowid
  from dataconv.o_abc_petitiontype pt  
 group by pt.legacykey 
 having count(*) > 1) loop

delete from
  dataconv.o_abc_petitiontype pt2
 where pt2.rowid = c.crowid;

t_count := t_count +1;
 
end loop; 
 commit;
 
 dbms_output.put_line('Rows Deleted: ' || t_count);
 
end;
/            