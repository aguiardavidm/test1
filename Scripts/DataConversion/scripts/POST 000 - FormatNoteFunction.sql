create or replace function abc11p.FormatConversionNote
  (
  a_BaseTableName   varchar2,
  a_RowId       varchar2,
  a_ColumnNameString varchar2,
  a_ColumnLabelString varchar2,
  a_NoteLabel         varchar2
  )
  return long
  is
--  t_ColumnNames dbms_sql.Varchar2_Table;
  t_NoteText    long;
  t_NoteLines   number       := 0;
  t_LineBreak   varchar2(40) := chr(10) || chr(13);
  t_Sql         varchar2(4000);
  t_TempLine    varchar2(4000);
begin
  --Testing a function to format the conversion note
  if REGEXP_COUNT(a_ColumnNameString, ',') != REGEXP_COUNT(a_ColumnLabelString, ',') then
    raise_application_error(-20001, 'Count of fields does not match count of labels. For ' || a_NoteLabel);
  end if;
  for i in (with temp as (select a_ColumnLabelString ColLabel, a_ColumnNameString ColName from dual)
            select
              trim(regexp_substr(t.ColName, '[^,]+', 1, levels.column_value)) as ColNames,
              trim(regexp_substr(t.ColLabel, '[^,]+', 1, levels.column_value)) as ColLabels
            from
              temp t,
              table(cast(multiset(select level from dual connect by  level <= length (regexp_replace(t.ColName, '[^,]+'))  + 1) as sys.OdciNumberList)) levels
            order by ColName) loop
    --TEST SQL, REMOVE
--    t_sql := 'select ' || i.colnames || ' from query.' || a_BaseTableName || ' where rowid = ''' || a_rowId || '''';
    --/TEST SQL
    t_sql := 'select ' || i.colnames || ' from abc11p.' || a_BaseTableName || ' where rowid = ''' || a_rowId || '''';
    begin
      execute immediate t_sql into t_TempLine;
      if trim(t_TempLine) is not null then
        t_Notetext := t_NoteText || i.collabels || ': ' || t_TempLine || t_LineBreak;
        t_NoteLines := t_NoteLines + 1;
      end if;
    exception when others then
      raise_application_error(-20000, sqlerrm || t_LineBreak || 'On table: ' || a_BaseTableName || t_LineBreak || 'RowId: ' || a_RowId || t_LineBreak || t_Sql);
    end;
  end loop;
  if t_NoteText is null then
    return null;
  else
    t_NoteText := 'Converted ' || upper(a_NoteLabel) || ' Information' || t_LineBreak || t_NoteText;
    t_NoteLines := t_NoteLines + 1;
  end if;
  return t_NoteText;
end;
