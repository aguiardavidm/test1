-- Created on 2/11/2015 by ADMINISTRATOR 
--Expected run time: 1s
declare 
  -- Local variables here
  i integer;
  t_RowsProcessed number := 0;
  t_RelRowsProcessed number := 0;
  t_LEType        number;
begin
  execute immediate 'alter table DATACONV.R_ABC_PARTICIPATESININCLUDESLE modify relationshiptype VARCHAR2(200)';
  if user != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20000, 'Script must be run as POSSEDBA');
  end if;
  select let.objectid
    into t_LEType
    from query.o_abc_legalentitytype let
   where let.Name = 'Organization / Company';
  -- Test statements here
  for i in (select le.abc11puk,
                   le.CRTD_OPER_ID,
                   le.DT_ROW_CRTD,
                   le.UPDT_OPER_ID,
                   le.DT_ROW_UPDT,
                   le.NAME LegalName,
                   le.TAX_ID_NO,
                   le.NAME DoingbusinessAs,
                   le.NAME ContactName,
                   le.AREA_CD || le.PREFIX || le.SUFFIX ContactPhone,
                   'Mail' PreferredContactMethod,
                   'Y' Active,
                   'N' Edit,
                   le.abc11puk || 'MAIL' MailingAddressLK,
                   le.reg_name_mstr_id_num,
                   le.REG_NAME_MSTR_ID_NUM OnlineAccessCode
              from abc11p.brn_reg_name le
             where le.reg_name_mstr_id_num not in (select x.legacykey from dataconv.o_Abc_Legalentity x)) loop
    insert into dataconv.o_Abc_Legalentity
      (
      LEGACYKEY,
      CREATEDBYUSERLEGACYKEY,
      CONVCREATEDDATE,
      LASTUPDATEDBYUSERLEGACYKEY,
      LASTUPDATEDDATE,
      LEGALNAME,
      CORPORATIONNUMBER,
      DOINGBUSINESSAS,
      CONTACTNAME,
      CONTACTPHONENUMBER,
      PREFERREDCONTACTMETHOD,
      ACTIVE,
      EDIT,
      LEGALENTITYTYPELK,
      MAILINGADDRESSLK,
      OnlineAccessCode
      )
      values
      (
      i.reg_name_mstr_id_num,
      i.CRTD_OPER_ID,
      i.DT_ROW_CRTD,
      i.UPDT_OPER_ID,
      i.DT_ROW_UPDT,
      i.Legalname,
      i.TAX_ID_NO,
      i.doingbusinessas,
      i.contactname,
      i.contactphone,
      i.PreferredContactMethod,
      i.Active,
      i.Edit,
      t_LEType,
      i.mailingaddresslk,
      i.OnlineAccessCode
      );
    --Create the relationship for the corporate structure
    for c in (select i.reg_name_mstr_id_num ParticipatesIn, pi.name_mstr_id_num Includes, pi.pct_bus_cntrl PercentageInterest, pi.num_shares_own CommonVotingShares, listagg(cd.cd_desc, '/') within group (order by pi.parent_id_number) RelationshipType
                from abc11p.name_mstr pi
                join abc11p.position p on p.name_mstr_id_num = pi.name_mstr_id_num
                join abc11p.code_desc cd on cd.code = p.position_cd
                 and cd.id = 'PS'
               where pi.name_type_cd not in ('2.5-D/B/A', '2.6-D/B/A')
                 and (pi.parent_id_number = i.reg_name_mstr_id_num)
                 and pi.srce_grp_cd != 'GENERAL'
                 and pi.name_stat != 'X'
               group by i.reg_name_mstr_id_num, pi.name_mstr_id_num, pi.pct_bus_cntrl, pi.num_shares_own) loop
      insert into dataconv.r_abc_participatesinincludesle
      (
      PARTICIPATESIN,
      INCLUDES,
      PERCENTAGEINTEREST,
      COMMONVOTINGSHARES,
      RELATIONSHIPTYPE
      )
      values
      (
      c.participatesin,
      c.includes,
      c.percentageinterest,
      c.commonvotingshares,
      c.RelationshipType
      );
      t_RelRowsProcessed := t_RelRowsProcessed +1;
    end loop;
  t_RowsProcessed := t_RowsProcessed+1;
  if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
      dbms_output.put_line('Processed ' || t_RelRowsProcessed || ' related LE rows.');
    end if;
  end loop;
  dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
  dbms_output.put_line('Processed ' || t_RelRowsProcessed || ' related LE rows.');
  commit;
end;