declare
  t_PaymentMethod pls_integer;

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  -- change cash to money order
  
  t_PaymentMethod := api.pkg_simplesearch.ObjectByIndex('o_PaymentMethod','Name','Cash');
  if t_PaymentMethod is not null then
    api.pkg_columnupdate.SetValue(t_PaymentMethod,'Name','Money Order');
    api.pkg_columnupdate.SetValue(t_PaymentMethod,'ReferenceLabel','Money Order number');
    api.pkg_columnupdate.SetValue(t_PaymentMethod,'ReferenceRequiredFlag','Y');
  end if;
  
  -- adjust check
  t_PaymentMethod := api.pkg_simplesearch.ObjectByIndex('o_PaymentMethod','Name','Check');
  api.pkg_columnupdate.SetValue(t_PaymentMethod,'ReferenceLabel','Check number');
  api.pkg_columnupdate.SetValue(t_PaymentMethod,'ReferenceRequiredFlag','Y');

-- adjust credit card
  t_PaymentMethod := api.pkg_simplesearch.ObjectByIndex('o_PaymentMethod','Name','Credit Card');
  api.pkg_columnupdate.RemoveValue(t_PaymentMethod,'ActiveFlag');  
  
-- adjust other
  t_PaymentMethod := api.pkg_simplesearch.ObjectByIndex('o_PaymentMethod','Name','Other');
  api.pkg_columnupdate.RemoveValue(t_PaymentMethod,'ActiveFlag');  
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  
end;