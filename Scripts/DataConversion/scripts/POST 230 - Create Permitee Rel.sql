declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'Permittee_Permit');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'Permittee_Permit');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitPermittee');
  t_RelId         number;
begin

for i in (select le.objectid LEId, dp.objectid PermitId
            from dataconv.o_abc_legalentity le
            join abc11p.permit p on p.abc11puk ||'PERM2' = le.legacykey
            join dataconv.o_abc_permit dp on dp.legacykey = to_char(p.perm_num)) loop

    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.LEId,
      i.PermitId
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.PermitId,
      i.LEId
    );
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;