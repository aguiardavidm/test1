--Run time: 1 second
declare
begin
  for i in (select distinct act_restr
            from abc11p.lic_mstr
           where act_restr is not null) loop
    insert into dataconv.o_abc_licensewarningtype
    (
    legacykey,
    active,
    LICENSEWARNINGTYPE
    )
    values
    (
    i.act_restr,
    'Y',
    i.act_restr
    );
  end loop;
  commit;
end;