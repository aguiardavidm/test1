--Run Time: 5 minutes
--Update LegacyKeys

update dataconv.u_users u
  set u.legacykey = nvl((select aum.username
                          from  abc11p.abc_user_names aum
                         where aum.username like 'OPS$%' and u.Oraclelogonid = upper(replace(substr(aum.fullname,instr(aum.fullname,',',1)+2,1) ||
                         rtrim(substr(aum.fullname,1,instr(aum.fullname,' ',1)-1),','),'-',''))),u.objectid);
                         
update dataconv.u_users u
  set u.legacykey = 'ABCCONV'
 where u.oraclelogonid = 'ABCCONV' ;

-- Created on 8/21/2014 by Keaton Run this to update all tables or a specific table by specifying t.table_name
declare 
  -- Local variables here
  t_DynamicUpdate varchar2(4000);
begin
  -- Test statements here
  
  for c in( 
      select 'update ' || dtc.owner || '.'||dtc.TABLE_NAME || ' gt ' 
       || ' set gt.' || dtc.COLUMN_NAME || ' = ''ABCCONV'''
       || ' where not exists(select un.username, un.fullname, un.emp_id
         from abc11p.abc_user_names un
            where gt.'|| dtc.COLUMN_NAME || ' = un.username
              and un.username like ''OPS$%'' )
              and gt.' || dtc.COLUMN_NAME || ' is not null' DynamicUpdate,
         dtc.TABLE_NAME,
         dtc.COLUMN_NAME           
    from dba_tab_cols dtc
    join dba_tables t on t.TABLE_NAME = dtc.TABLE_NAME
  where (dtc.COLUMN_NAME = 'CREATEDBYUSERLEGACYKEY'
     or dtc.COLUMN_NAME = 'LASTUPDATEDBYUSERLEGACYKEY')
  and t.TABLE_NAME not in (select pct.tablename
                             from dataconv.posseconvtables pct
                           where pct.fill = 'Y' )) loop
        begin
    
        EXECUTE IMMEDIATE c.Dynamicupdate;
        dbms_output.put_line('Table Name: ' || c.Table_Name || ' - ' || c.Column_Name || ' - ' || sql%rowcount);
        exception when others then
        dbms_output.put_line(c.Dynamicupdate);
        end;
    end loop;
    commit;
end;
