update dataconv.o_abc_permit
   set state = 'Active'
 where state = 'C';
commit;
update dataconv.o_abc_permit
   set state = 'Expired'
 where state = 'H';
commit;
delete from dataconv.R_PERMITLICENSE r
 where length(r.o_abc_license) = 2;
 update dataconv.o_abc_permit p
   set p.sellerslicenselk = replace(p.sellerslicenselk, '-', '') || (select max(substr(l.licensenumber, 13, 3))
                                                                       from dataconv.o_abc_license l
                                                                      where substr(l.licensenumber, 1, 11) = p.sellerslicenselk)
 where instr(p.sellerslicenselk, '-') > 0;
commit;
update dataconv.o_abc_permit 
   set sellerslicenselk = '' 
 where sellerslicenselk not in (select legacykey from dataconv.o_abc_license);
update dataconv.o_abc_permit
   set permittypelk = (select legacykey from dataconv.o_abc_permittype where code = 'MS')
 where permittypelk = 'MSO';
/
update dataconv.o_abc_permit p
   set p.permittypelk = (select legacykey
                           from dataconv.o_abc_permittype pt
                          where pt.code = p.permittypelk)
 where p.permittypelk not in (select legacykey from dataconv.o_abc_Permittype);
commit;
/