insert into abc11p.address_mstr
(select null,                            --  objectid  number(9)
       p.po_box || p.zip_1_5 ||p.zip_6_9 || p.st_prov_cd || p.str_name || p.str_num || p.city legacykey,   --  legacykey  varchar2(500)
       p.crtd_oper_id,                  --  createdbyuserlegacykey  varchar2(500)
       p.dt_row_crtd,                   --  convcreateddate  date
       p.updt_oper_id,                  --  lastupdatedbyuserlegacykey  varchar2(500)
       p.dt_row_updt,                   --  lastupdateddate  date
       'N',                             --  rowstatus  varchar2(1)
       'Civic',                         --  addresstype  varchar2(100)
       p.zip_1_5,                       --  postalcode  varchar2(10)
       null ,                           --  country  varchar2(100)
       cd.cd_desc State,                --  provincestate  varchar2(100)
       p.str_name,                      --  street  varchar2(60)
       p.str_num,                       --  streetnumber  varchar2(6)
       null,                            --  streetnumbersuffix  varchar2(20)
       null,                            --  unitnumber  varchar2(20)
       null,                            --  line2  varchar2(60)
       p.city,                          --  citytown  varchar2(100)
       null,                            --  streetdirection  varchar2(100)
       null,                            --  otheraddress  varchar2(400)
       null,                            --  po_additionaldeliveryinfo  varchar2(150)
       p.po_box,                        --  po_postofficebox  varchar2(20)
       null,                            --  po_stationinformation  varchar2(40)
       null,                            --  rr_additionaladdressinfo  varchar2(60)
       null,                            --  rr_ruralrouteidentifier  varchar2(20)
       null,                            --  rr_stationinformation  varchar2(40)
       null,                            --  gd_generaldeliveryindicator  varchar2(2)
       null,                            --  gd_stationinformation  varchar2(40)
       'Y',                             --  isconverted  char(1)
       'Y',                             --  convertedaddress  varchar2(4000)
       p.zip_6_9,                       --  zipcodeextension  number(4)
       null,                            --  streettypelk  varchar2(500)
       null,                            --  streettyperid  number(9)
       'N',                             --  streettype_rs  varchar2(1)
       p.abc11puk || 'PREM',             -- abc11puk from source table
       null,
       null,
       null,
       null
  from abc11p.coop p      
  left outer join abc11p.code_desc cd on cd.code = p.st_prov_cd 
                           and cd.id = 'ST');
commit;
               
  delete
    from abc11p.address_mstr am 
   where am.legacykey is null;  
  delete 
    from abc11p.address_mstr  am
  where length(am.legacykey) = 2;
  --Run Unique Key for ABC11P.ADDRESS_MSTR
  update abc11p.address_mstr am
    set am.legacykey = am.abc11puk;
  --Transform States from all Caps to Init Caps               
  update abc11p.address_mstr am
   set am.State = InitCap(am.state);               
  -- Correct District Of Columbia
  update abc11p.address_mstr am
    set am.State ='District of Columbia'
   where am.State = 'District Of Columbia';
  update abc11p.address_mstr am
    set am.COUNTRY = 'USA'
   where am.COUNTRY = 'US';
  update abc11p.address_mstr am
    set am.Addresstype = 'PO Box'  
   where am.PO_POSTOFFICEBOX is not null;
 update abc11p.address_mstr am
  set am.COUNTRY = (select cd.cd_desc
            from abc11p.code_desc cd
           where cd.id = 'CY'
             and cd.code != 'US'
             and cd.code = am.COUNTRY)
  where am.country != 'USA';         
  update abc11p.address_mstr am
    set am.Addresstype = 'Other / International'  
  where am.citytown is null or
    am.state is null or 
    am.postalcode is null or
    (am.po_postofficebox is null and (am.street is null or am.streetnumber is null))
  or am.COUNTRY != 'USA';
   update abc11p.address_mstr am
     set am.OTHERADDRESS = am.streetnumber || ' ' || am.street || ' ' || am.unitnumber || ' ' || am.line2 || ' 
   ' || am.CityTown || ' ' || am.Country || ' ' || am.PostalCode || ' ' || am.zipcodeextension
   ,am.State = null
   ,am.country = null
  where am.Addresstype = 'Other / International';
commit;
/


   -- Load Data into dataconv.o_abc_address
   
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
  update abc11p.address_mstr am
        set am.errortext = a_ErrorText,
            am.errorcode = a_ErrorCode
      where am.rowid = a_RowId;  
  
    commit;
  end;

begin
 
  for c in (select rowid,
                   objectid,
                   legacykey,
                   createdbyuserlegacykey,
                   convcreateddate ,
                   lastupdatedbyuserlegacykey,
                   lastupdateddate,   
                   rowstatus,
                   addresstype,
                   postalcode, 
                   country, 
                   state,
                   street, 
                   streetnumber, 
                   streetnumbersuffix, 
                   unitnumber, 
                   line2, 
                   citytown, 
                   streetdirection, 
                   otheraddress,
                   po_additionaldeliveryinfo,
                   po_postofficebox, 
                   po_stationinformation,
                   rr_additionaladdressinfo, 
                   rr_ruralrouteidentifier,
                   rr_stationinformation, 
                   gd_generaldeliveryindicator, 
                   gd_stationinformation, 
                   isconverted, 
                   convertedaddress, 
                   zipcodeextension,
                   streettypelk, 
                   streettyperid,
                   streettype_rs      
             from abc11p.address_mstr am
            where am.processed is null) loop
    begin
    insert into dataconv.o_abc_address a
    ( objectid                    ,
	  legacykey                   ,
	  createdbyuserlegacykey      ,
	  convcreateddate             ,
	  lastupdatedbyuserlegacykey  ,
	  lastupdateddate             ,
	  rowstatus                   ,
	  addresstype                 ,
	  postalcode                  ,
	  country                     ,
	  provincestate               ,
	  street                      ,
	  streetnumber                ,
	  streetnumbersuffix          ,
	  unitnumber                  ,
	  line2                       ,
	  citytown                    ,
	  streetdirection             ,
	  otheraddress                ,
	  po_additionaldeliveryinfo   ,
	  po_postofficebox            ,
	  po_stationinformation       ,
	  rr_additionaladdressinfo    ,
	  rr_ruralrouteidentifier     ,
	  rr_stationinformation       ,
	  gd_generaldeliveryindicator ,
	  gd_stationinformation       ,
	  isconverted                 ,
	  convertedaddress            ,
	  zipcodeextension             )
	
	values ( c.objectid,
             c.legacykey,
             c.createdbyuserlegacykey,
             c.convcreateddate ,
             c.lastupdatedbyuserlegacykey,
             c.lastupdateddate,   
             c.rowstatus,
             c.addresstype,
             c.postalcode, 
             c.country, 
             c.state,
             c.street, 
             c.streetnumber, 
             c.streetnumbersuffix, 
             c.unitnumber, 
             c.line2, 
             c.citytown, 
             c.streetdirection, 
             c.otheraddress,
             c.po_additionaldeliveryinfo,
             c.po_postofficebox, 
             c.po_stationinformation,
             c.rr_additionaladdressinfo, 
             c.rr_ruralrouteidentifier,
             c.rr_stationinformation, 
             c.gd_generaldeliveryindicator, 
             c.gd_stationinformation, 
             c.isconverted, 
             c.convertedaddress, 
             c.zipcodeextension);   
    
    update abc11p.address_mstr am
        set am.processed = 'Y',
            am.processeddate = sysdate
      where c.rowid = am.rowid;   
      
    t_count := t_count +1; 
    
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
  commit;
  
end;   
