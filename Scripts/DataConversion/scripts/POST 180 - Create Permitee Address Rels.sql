/* NO LONGER NEEDED
declare
  t_MailEndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'MailingAddress');
  t_MailOthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'MailingAddress');
  t_MailRelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_MailingLE');
  t_PhysEndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'PhysicalAddress');
  t_PhysOthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'PhysicalAddress');
  t_PhysRelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PhysicalLE');
  t_RelId         number;
begin

for i in () loop

    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.PermitId,
      i.AddressId
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.AddressId,
      i.PermitId
    );
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;*/