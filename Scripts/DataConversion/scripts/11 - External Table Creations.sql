--Run time: 5 seconds
begin
if user != 'SYSTEM' then
  raise_application_error(-20000, 'Must be run as SYSTEM');
end if;
end;
/

--Run statements as system:
create directory ext_marketing_agent as 'C:\Posse\AccessData\MarketingAgent';
create directory ext_petitions as 'C:\Posse\AccessData\Petitions';
create directory ext_appeals as 'C:\Posse\AccessData\Appeals';
grant read on directory ext_marketing_agent to possedba;
grant read on directory ext_marketing_agent to abc11p;
grant read on directory ext_petitions to possedba;
grant read on directory ext_petitions to abc11p;
grant read on directory ext_appeals to possedba;
grant read on directory ext_appeals to abc11p;

create table abc11p.MA_Applicant_ext_t
  (APPLICANT_ID number(9),
   APPLICANT_TIMESTAMP varchar2(500),
   APPLICANT_NAME varchar2(500),
   APPLICANT_ADDRESS varchar2(500),
   APPLICANT_CITY varchar2(500),
   APPLICANT_STATE varchar2(500),
   APPLICANT_ZIP number(10),
   APPLICANT_PHONE number(10),
   APPLICANT_EMAIL varchar2(500),
   APPLICANT_DOB varchar2(500),
   APPLICANT_DRIVER_LIC varchar2(500),
   APPLICANT_RETIRE_FLAG number(1))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY ext_marketing_agent
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('APPLICANT.CSV')
     ) reject limit unlimited;  
	 
create table abc11p.MA_Applicant_int_t as 
  select * from abc11p.MA_Applicant_ext_t;
	 
alter table abc11p.MA_Applicant_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);

create table abc11p.MA_Permit_ext_t
  (PERMIT_ID number(9),
   PERMIT_TIMESTAMP varchar2(500),
   LICENSE_ID number(9),
   PROMOTER_ID number(9),
   PERMIT_DATE_ISSUED varchar2(500),
   PERMIT_TERM_START_DATE varchar2(500),
   PERMIT_TERM_END_DATE   varchar2(500),
   PERMIT_FEE             varchar2(500),
   PERMIT_TRAN_NO         number(9))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY ext_marketing_agent
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('Permit.CSV')
     ) reject limit unlimited;  

create table abc11p.MA_Permit_int_t as 
  select * from abc11p.MA_Permit_ext_t;	 
  
alter table abc11p.MA_Permit_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);
  
  
create table abc11p.MA_LICENSE_ext_t
  (LICESNSE_ID number(9),
   LICENSE_TIMESTAMP varchar2(500),
   LICENSE_MUNI number(9),
   LICENSE_TYPE number(9),
   LICENSE_NO_IN_MUNI number(9),
   LICENSE_GEN number(9),
   LICENSEE_NAME  varchar2(500),
   LICENSEE_ADDRESS            varchar2(500),
   LICENSEE_STATE        varchar2(500),
   LICENSEE_ZIP          varchar2(10),
   LICENSEE_CONTACT_NAME   varchar2(500),
   LICENSEE_CONTACT_PHONE  varchar2(500),
   LICENSEE_CONTACT_EMAIL  varchar2(500),
   LICENSEE_AUTH_SIG_NAME  varchar2(500),
   LICENSEE_AUTH_SIG_TITLE varchar2(500),
   LICENSEE_AUTH_SIG_DATE  varchar2(500))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY ext_marketing_agent
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('License.CSV')
     ) reject limit unlimited;  	 

create table abc11p.MA_LICENSE_int_t as 
  select * from abc11p.MA_LICENSE_ext_t;

alter table abc11p.MA_LICENSE_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);  
  
create table abc11p.MA_PROMOTER_ext_t
  (PROMOTER_ID number(9),
   PROMOTER_TIMESTAMP varchar2(500),
   PROMOTER_NAME 	varchar2(500),
   PROMOTER_STREET varchar2(500),
   PROMOTER_CITY varchar2(500),
   PROMOTER_STATE varchar2(500),
   PROMOTER_ZIP          varchar2(10),
   PROMOTER_CONTACT   varchar2(500),
   PROMOTER_PHONE  varchar2(500),
   PROMOTER_EMAIL  varchar2(500))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY ext_marketing_agent
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('Promoter.CSV')
     ) reject limit unlimited; 	 
	 
create table abc11p.MA_PROMOTER_int_t as 
  select * from abc11p.MA_PROMOTER_ext_t;	

alter table abc11p.MA_PROMOTER_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);   
	   
create table abc11p.MA_APP_CARD_ext_t
  (APP_CARD_ID number(9),
   APP_CARD_TIMESTAMP varchar2(500),
   APPLICANT_ID number(9),
   PERMIT_ID varchar2(500),
   APP_CARD_TERM_START_DATE varchar2(500),
   APP_CARD_TERM_END_DATE   varchar2(500),
   APP_CARD_FEE   varchar2(500),
   APP_CARD_TRAN_NO  varchar2(500))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY ext_marketing_agent
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('APP_CARD.CSV')
     ) reject limit unlimited;    
   
create table abc11p.MA_APP_CARD_int_t as 
  select * from abc11p.MA_APP_CARD_ext_t;  

alter table abc11p.MA_APP_CARD_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);   	   

create table abc11p.P_tblDAGFile_ext_t
  (DAGCODE varchar2(500),
   DAGNAME varchar2(500),
   DAGFONE varchar2(500),
   DataActive varchar2(500))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY ext_petitions
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('tblDAGFile.CSV')
     ) reject limit unlimited;    
   
create table abc11p.P_tblDAGFile_int_t as 
  select * from abc11p.P_tblDAGFile_ext_t;  

alter table abc11p.P_tblDAGFile_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);     
	   
create table abc11p.P_tblABCAppeal_ext_t
  (DOCKET_SEQ number(9),
   LICENSE_NO varchar2(500),
   LICTERM varchar2(500),
   DOCKET_SEQ_CONVERT varchar2(500),
   DOCKET_NUM varchar2(500),
   DOC_DATE varchar2(500),
   MATTER varchar2(500),
   DATE_RECD varchar2(500),
   DATE_TODAG varchar2(500),
   ASSIGN_TO varchar2(500),
   DATE_TOOAL varchar2(500),
   DATE_FMOAL varchar2(500),
   SPEC_CODE varchar2(500), 
   SPEC_NOTES varchar2(500),
   PULL_DATE varchar2(500),
   PULL_COMM varchar2(500),
   STATUS varchar2(500),
   GRANT_YR1 varchar2(500),
   GRANT_YR2 varchar2(500),
   GRANT_YR3 varchar2(500),
   GRANT_YR4 varchar2(500),
   GRANT_YR5 varchar2(500),
   GRANT_YR6 varchar2(500),
   GRANT_YR7 varchar2(500),
   CLOSE_DATE varchar2(500),
   TRANS_CODE varchar2(500),
   SPEC_COND varchar2(500),
   ACTION varchar2(500),
   APPEAL_NO varchar2(500),
   MUN_REVNO varchar2(500),
   FILING_FEE varchar2(500),
   INITIAL_AC varchar2(500),
   OAL_CODE varchar2(500),
   CLOSED_BY varchar2(500),
   COMMENT_AC varchar2(500))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY EXT_PETITIONS
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('tblABCAppel.csv')
     ) reject limit unlimited;  
	 
create table abc11p.P_tblABCAppeal_int_t as 
  select * from abc11p.P_tblABCAppeal_ext_t;
	 
alter table abc11p.P_tblABCAppeal_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);	   

create table abc11p.A_tblDAGFile_ext_t
  (DAGCODE varchar2(500),
   DAGNAME varchar2(500),
   DAGFONE varchar2(500),
   DataActive varchar2(500))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY ext_appeals
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('tblDAGFile.CSV')
     ) reject limit unlimited;    
   
create table abc11p.A_tblDAGFile_int_t as 
  select * from abc11p.A_tblDAGFile_ext_t;  

alter table abc11p.A_tblDAGFile_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date); 
	   
create table abc11p.A_tblAppeal_ext_t
  (AppealNr number(9),
   LICENSE_NUM varchar2(500),
   LicenseName varchar2(500),
   Respondent varchar2(500),
   Class_a varchar2(500),
   DAGAssigned   varchar2(500),
   DateFiled   varchar2(500),
   DateClosed  varchar2(500),
   DateToOAL varchar2(500),
   DateReturnedToABC varchar2(500),
   Comment_a varchar2(500),
   MethodOfClosure varchar2(500))
 ORGANIZATION EXTERNAL
    (TYPE ORACLE_LOADER
     DEFAULT DIRECTORY ext_appeals
     ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
         FIELDS terminated BY '|'

            optionally enclosed BY '¦'

            lrtrim

            missing field VALUES are NULL
      )
   LOCATION ('tblAppeal.CSV')
     ) reject limit unlimited;    
   
create table abc11p.A_tblAppeal_int_t as 
  select * from abc11p.A_tblAppeal_ext_t;  

alter table abc11p.A_tblAppeal_int_t
  add (errortext varchar2(4000),
       errorcode varchar2(400),
       processed varchar2(1),
       processeddate date);  	   