--Run time: 10 seconds
-- Created on 5/7/2014 by PAUL
declare 
  -- Local variables here
  i          integer;
  t_EventRow abc11p.perm_event%rowtype;
  t_RainRow  abc11p.perm_rain%rowtype;
  t_RowsProcessed number := 0;
begin
  -- Test statements here
  
  for i in (select rowid, 'perm_event' Type
              from abc11p.perm_event pe
             where PERM_NUM
                   || DT_EVENT 
                   || TIME_START 
                   || TIME_END not in (select x.legacykey
                                         from dataconv.o_abc_eventdate x)
            union
            select rowid, 'perm_rain'
              from abc11p.perm_rain pe
             where PERM_NUM
                   || DT_EVENT 
                   || TIME_START 
                   || TIME_END not in (select x.legacykey
                                         from dataconv.o_abc_eventdate x)) loop
    --We're setting "type" in the select above and using it to determine the row type we need
    if i.type = 'perm_event' then
      select *
        into t_eventrow
        from abc11p.perm_event
       where rowid = i.rowid;
      insert into dataconv.o_abc_eventdate
        (
        LegacyKey,
        CreatedByUserLegacyKey,
        ConvCreatedDate,
        LastUpdatedByUserLegacyKey,
        LastUpdatedDate,
        StartDate,
        HoursOfOperation
        )
      values 
        (
        'E' || t_eventrow.perm_num || t_eventrow.dt_event || t_eventrow.time_start || t_eventrow.time_end,
        t_eventrow.updt_oper_id,
        t_eventrow.dt_row_updt,
        t_eventrow.updt_oper_id,
        t_eventrow.dt_row_updt,
        t_eventrow.dt_event,
        t_eventrow.time_start || t_eventrow.start_am_pm || ' - ' || t_eventrow.time_end || t_eventrow.end_am_pm
        );
    elsif i.type = 'perm_rain' then
      select *
        into t_rainrow
        from abc11p.perm_rain
       where rowid = i.rowid;
      insert into dataconv.o_abc_eventdate
        (
        LegacyKey,
        CreatedByUserLegacyKey,
        ConvCreatedDate,
        LastUpdatedByUserLegacyKey,
        LastUpdatedDate,
        StartDate,
        HoursOfOperation
        )
      values 
        (
        'R' || t_rainrow.perm_num || t_rainrow.dt_event || t_rainrow.time_start || t_rainrow.time_end,
        t_rainrow.updt_oper_id,
        t_rainrow.dt_row_updt,
        t_rainrow.updt_oper_id,
        t_rainrow.dt_row_updt,
        t_rainrow.dt_event,
        t_rainrow.time_start || t_rainrow.start_am_pm || ' - ' || t_rainrow.time_end || t_rainrow.end_am_pm
        );
    end if;
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
    end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  commit;
end;