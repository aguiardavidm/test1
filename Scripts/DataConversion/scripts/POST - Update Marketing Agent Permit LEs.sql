declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_Permit', 'License');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_Permit', 'License');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_PermitLicense');
  t_RelId         number;
  t_DelRelId      number;
begin

for i in (Select p.objectid PermitId, al.objectid LicenseId
			  from dataconv.o_abc_permit p
			  join dataconv.o_abc_permittype pt on pt.legacykey = p.permittypelk
			  join abc11p.ma_permit_int_t m on m.permit_id = rtrim(p.permitnumber,'MAPERMIT')
			  join abc11p.ma_license_int_t l on l.licesnse_id = m.license_id
			  join dataconv.o_abc_license al on al.legacykey = l.license_muni||l.license_type||lpad(l.license_no_in_muni,3,0)||lpad(l.license_gen,3,0)
			 where pt.code =  'MA') loop
    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;
	
    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.PermitId,
      i.LicenseId
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.LicenseId,
      i.PermitId
    );
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;