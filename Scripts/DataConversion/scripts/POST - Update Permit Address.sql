declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_Permit', 'EventLocationAddress');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_Permit', 'EventLocationAddress');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitEventAddress');
  t_RelId         number;
begin

for i in (select p.objectid PermitId, a.objectid AddressId
            from dataconv.o_abc_permit p
            join abc11p.permit ap on to_char(ap.perm_num)  = p.legacykey
            join abc11p.address_mstr am on to_char(ap.abc11puk) || 'EVNT' = am.legacykey
            join abc11p.address_xref3 xr3 on xr3.oldaddresslk = am.legacykey
            join dataconv.o_abc_address a on a.legacykey = xr3.newaddresslk
            group by p.objectid, a.objectid) loop

    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.PermitId,
      i.AddressId
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.AddressId,
      i.PermitId
    );
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;