--Run time: 1 second
-- Created on 3/20/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
  t_RowsProcessed number := 0;
begin
  -- Test statements here
  for i in (select *
              from abc11p.code_desc cd
             where cd.id = 'CN' 
--               and cd.code not in ('34', '99')
               and cd.code not in (select legacykey from dataconv.o_abc_region))loop
    insert into dataconv.o_abc_region
    (
    LEGACYKEY,
    NAME,
    COUNTYCODE,
    DEFAULTMANAGERLK
    )
    values
    (
    i.code,
    i.cd_desc,
    i.code,
    'OPS$LPBVALS'
    --(case when i.code in ('34', '99') then 'N' else 'Y' end) 
    );
    t_RowsProcessed := t_RowsProcessed + 1;
  end loop;
  for i in (select *
              from abc11p.code_desc cd
             where cd.id = 'CM'
--               and substr(cd.code, 1, 2) not in ('34', '99')
               and cd.code not in (select legacykey from dataconv.o_abc_office)) loop
    insert into dataconv.o_abc_office
    (
    LEGACYKEY,
    NAME,
    MUNICIPALITYCODE,
    REGIONLK
    )
    values
    (
    i.code,
    i.cd_desc,
    substr(i.code, 3, 4),
    substr(i.code, 1, 2)
    --(case when substr(i.code, 1, 2) in ('34', '99') then 'N' else 'Y' end)
    );
    t_RowsProcessed := t_RowsProcessed + 1;
  end loop;
  dbms_output.put_line(t_RowsProcessed || ' Rows processed');
  --Create the "State" County/Muni in case the above doesn't work.
  /*insert into dataconv.o_abc_region
    (
    LEGACYKEY,
    NAME,
    COUNTYCODE,
    DEFAULTMANAGERLK
    )
    values
    (
    '34',
    'State Issued',
    '34',
    'OPS$LPBVALS'    
    );
  insert into dataconv.o_abc_office
    (
    LEGACYKEY,
    NAME,
    MUNICIPALITYCODE,
    REGIONLK
    )
    values
    (
    '3400',
    'State Issued',
    '00',
    '34'
    );
  insert into dataconv.o_abc_region
    (
    LEGACYKEY,
    NAME,
    COUNTYCODE,
    DEFAULTMANAGERLK
    )
    values
    (
    '99',
    'State Issued',
    '99',
    'OPS$LPBVALS'    
    );
  insert into dataconv.o_abc_office
    (
    LEGACYKEY,
    NAME,
    MUNICIPALITYCODE,
    REGIONLK
    )
    values
    (
    '9900',
    'State Issued',
    '00',
    '99'
    );*/
  commit;
end;
