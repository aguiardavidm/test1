-- Create the user 
create user ABC11P identified by abc11p
  default tablespace CONV
  temporary tablespace TEMP
  profile DEFAULT
  quota unlimited on conv
  quota unlimited on data_large
  quota unlimited on data_medium
  quota unlimited on data_small
  quota unlimited on indexes_large
  quota unlimited on indexes_medium
  quota unlimited on indexes_small;
-- Grant/Revoke system privileges 
grant create sequence to ABC11P;
grant create session to ABC11P;
grant create table to ABC11P;
grant create view to ABC11P;

create table system.abc_user_names (
  username                        varchar2(30) not null,
  fullname                        varchar2(30) not null,
  emp_id                          varchar2(9) not null
);

grant all on system.abc_user_names to abc11p;