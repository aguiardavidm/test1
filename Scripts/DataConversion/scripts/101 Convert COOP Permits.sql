--Run Time: 15 seconds
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_PermitType      varchar2(100);
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin  
    dbms_output.put_line('Error: '|| a_ErrorText || 'ErrorCode: ' ||a_ErrorCode);
  end;

begin
  
  select ObjectId 
    into t_PermitType 
   from dataconv.o_abc_permittype pt
  where pt.code = 'COOP';


  for c in (select t.rowid,
            t.abc11puk,
            t.COOP_NUM || 'COOP'PERMITNUMBER,
			case when t.COOP_STAT = 'Y' then 'Active' else 'Closed' end STATE,
			t_PermitType PERMITTYPELK,
			t.COOP_NUM || 'COOP' COOPOFFICERLK,
			t.DT_EFFECTIVE ISSUEDATE,
			t.DT_ROW_CRTD  CONVCREATEDDATE,
			t.CRTD_OPER_ID CREATEDBYUSERLEGACYKEY,
			t.DT_ROW_UPDT  LASTUPDATEDDATE,
			t.UPDT_OPER_ID LASTUPDATEDBYUSERLEGACYKEY,
			min(ct.DT_TERM_START) EFFECTIVEDATE,
			max(ct.DT_TERM_END)  EXPIRATIONDATE,
			t.COOP_NAME LEGALNAME,
			t.CONTACT_NAME CONTACTNAME,
            t.PHONE_PREFIX || t.PHONE_SUFFIX CONTACTPHONE
            from abc11p.COOP t
			join abc11p.COOP_TERM ct on ct.COOP_NUM = t.COOP_NUM
		where not exists (select 1
							from dataconv.o_abc_permit p
						where p.legacykey = t.COOP_NUM || 'COOP')	
        group by
        t.rowid,
      t.COOP_NUM ,
      t.abc11puk,
			case when t.COOP_STAT = 'Y' then 'Active' else 'Closed' end ,
			t_PermitType ,
			t.COOP_NUM     ,
			t.DT_EFFECTIVE ,
			t.DT_ROW_CRTD  ,
			t.CRTD_OPER_ID ,
			t.DT_ROW_UPDT  ,
			t.UPDT_OPER_ID ,
			t.COOP_NAME ,
			t.CONTACT_NAME ,
            t.PHONE_PREFIX || t.PHONE_SUFFIX ) loop
    begin
    insert into dataconv.o_abc_permit a
    (   PERMITNUMBER,
		STATE,
		PERMITTYPELK,
		COOPOFFICERLK,
		ISSUEDATE,
		CONVCREATEDDATE,
		CREATEDBYUSERLEGACYKEY,
		LASTUPDATEDDATE,
		LASTUPDATEDBYUSERLEGACYKEY,
		EFFECTIVEDATE,
		EXPIRATIONDATE,
        eventlocationaddresslk,
        legacykey
      )
    values (c.PERMITNUMBER,
			c.STATE,
			c.PERMITTYPELK,
			c.COOPOFFICERLK,
			c.ISSUEDATE,
			c.CONVCREATEDDATE,
            c.CREATEDBYUSERLEGACYKEY,
            c.LASTUPDATEDDATE,
            c.LASTUPDATEDBYUSERLEGACYKEY,
            c.EFFECTIVEDATE,
            c.EXPIRATIONDATE,
            c.abc11puk,
            c.permitnumber);    
    
    insert into dataconv.o_ABC_LEGALENTITY
  (LEGACYKEY,
   ACTIVE,
   LEGALENTITYTYPELK,
   LEGALNAME,
   CONTACTNAME,
   CONTACTPHONENUMBER
  )
  values( c.PERMITNUMBER,
      case when c.STATE = 'Active' then 'Y' else 'N' end,
      'CONVERTED',
      c.LEGALNAME,
      c.CONTACTNAME,
      c.CONTACTPHONE
         );
  
  for d in (select COOP_NUM || 'COOP' PermitLK,
                   min(MEM_STAT) Active,
                   CNTY_MUNI_CD || LIC_TYPE_CD || MUNI_SER_NUM || GEN_NUM LicenseLK
              from abc11p.COOP_MEM cm
             where cm.COOP_NUM || 'COOP'  = c.PERMITNUMBER
             group by COOP_NUM || 'COOP',
             CNTY_MUNI_CD || LIC_TYPE_CD || MUNI_SER_NUM || GEN_NUM ) loop
    
    insert into dataconv.r_ABC_PermitCoOpLicenses
      ( o_abc_permit ,
        o_abc_license )
        values(d.PermitLK,
               d.LicenseLK);    
    
  end loop;      
      
    t_count := t_count +1; 
    
           
    
  /*  if mod(t_count,1000) = 0
      then
        commit;
    end if; */        
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;   
