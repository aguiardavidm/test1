-- Created on 5/4/2015 by KEATON 
declare 
  -- Local variables here
  t_RelationshipDef          number := api.pkg_configquery.ObjectDefIdForName('r_ABC_LELegalEntityType');
  t_RelationshipId           number;
  t_ConvertedTypeObjId       number;
  t_LETypeEndPointId         number := api.pkg_configquery.EndPointIdForName('o_ABC_LegalEntity','LegalEntityType');
  t_LegalEntityEndPointId    number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity','LegalEntityType');
  
  
begin

-- Select The  Correct Legal Entity Type

   select le.ObjectId
     into t_ConvertedTypeObjId 
     from query.o_abc_legalentitytype le
    where le.Name='Converted';

-- Loop through relationships and delete from the tables and create a new object.

for c in (select sr1.RelationshipId, 
           (select od.Name
              from api.objects o
              join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
             where o.ObjectId = sr1.FromObjectId )FromObjDefName,
             sr1.FromObjectId,
             (select od.Name
              from api.objects o
              join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
             where o.ObjectId = sr1.ToObjectId )ToObjDefName,
             sr1.ToObjectId
      from   rel.storedrelationships sr1
      where sr1.Relationshipid in (select sr.Relationshipid
      from rel.storedrelationships sr
       where sr.ToObjectId = (select le.objectid 
                                           from query.o_abc_legalentitytype le
                                          where le.Name='An Individual'))
            and (select od.Name
              from api.objects o
              join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
             where o.ObjectId = sr1.FromObjectId ) = 'o_ABC_LegalEntity'
             /*and sr1.fromobjectid = 16782479*/) loop


delete from rel.storedrelationships sr
  where sr.RelationshipId = c.relationshipid;
delete from objmodelphys.objects o 
  where o.ObjectId = c.relationshipid;  
   

select possedata.ObjectId_s.nextval
  into t_RelationshipId 
 from dual;

--Insert into ObjModelPhys.Objects
insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelationshipId,
      t_RelationshipDef,
      4,
      4,
      t_RelationshipDef,
      null,
      null,
      4,
      t_RelationshipDef,
      null,
      null
    );

 
--Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipId,
      t_LegalEntityEndPointId,
      t_ConvertedTypeObjId,
      c.Fromobjectid
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipId,
      t_LETypeEndPointId,
      c.fromobjectid,
      t_ConvertedTypeObjId
    ); 


end loop;
  
end;
