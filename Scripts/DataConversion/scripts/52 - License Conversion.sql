--Run Time: 4 minutes
-- Created on 2/27/2015 by ADMINISTRATOR
declare 
  -- Local variables here
  i integer;
  t_IssueDate      date;
  t_EffectiveDate  date;
  t_ExpirationDate date;
  t_LicenseTypeLK  varchar2(40);
  t_LTCode         varchar2(10);
  t_LicenseeLK     varchar2(40);
  t_EstablishmentLK varchar2(40);
  t_HistEstabLK    varchar2(40);
  t_CountyName     varchar2(40);
  t_ConversionNote long;
  t_RowsProcessed  number := 0;
begin
  -- Test statements here
  for i in (select l.*, (select legacykey
                           from dataconv.o_abc_masterlicense m
                          where substr(m.legacykey, 0, 9) = l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num) MasterLK
              from abc11p.lic_mstr l
             where l.lic_stat in ('C', 'H')
               and not exists (select 1
                                 from dataconv.o_abc_license m
                                where m.legacykey = l.CNTY_MUNI_CD || l.LIC_TYPE_CD || l.MUNI_SER_NUM || l.GEN_NUM)
            order by l.lic_type_cd) loop

    --Get the License Type and keep it without running the select again if it still matches
    if t_LTCode is null or t_LTCode != i.lic_type_cd then
      select max(objectid), max(lt.Code)
        into t_LicenseTypeLK, t_LTCode
        from query.o_Abc_Licensetype lt
       where lt.ObjectDefTypeId = 1
         and lt.Code = i.lic_type_cd
         and lt.Active = 'Y';
    end if;
    --Get the term dates, insert a conversion note if there are historical records
    select min(t.dt_term_start) IssueDate, max(t.dt_term_start) EffectiveDate, max(t.dt_term_end) ExpirationDate
      into t_IssueDate, t_EffectiveDate, t_ExpirationDate
      from abc11p.Lic_Term t
     where t.CNTY_MUNI_CD = i.CNTY_MUNI_CD
                 and t.LIC_TYPE_CD = i.LIC_TYPE_CD
                 and t.MUNI_SER_NUM = i.MUNI_SER_NUM
                 and t.GEN_NUM = i.GEN_NUM;
    t_ConversionNote := 'HISTORICAL TERMS FOR LICENSE NUMBER' || chr(10);
    for c in (select t.*
                from abc11p.Lic_Term t
               where t.CNTY_MUNI_CD = i.CNTY_MUNI_CD
                 and t.LIC_TYPE_CD = i.LIC_TYPE_CD
                 and t.MUNI_SER_NUM = i.MUNI_SER_NUM
                 and t.GEN_NUM = i.GEN_NUM) loop
      t_ConversionNote := t_ConversionNote || c.dt_term_start || ' to ' || c.dt_term_end || '. RESOLUTION_IND: ' || c.RESOLUTION_IND || chr(10);
    end loop;
    insert into dataconv.n_general
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    parenttablename,
    parentlegacykey,
    locked,
    text
    )
    values
    (
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    'O_ABC_LICENSE',
    i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
    'Y',
    t_ConversionNote
    );
    --Get the Licensee/Establishment LKs
    begin
    if i.lic_stat = 'C' then
      select nm.name_mstr_id_num, nm.name_mstr_id_num, nm.cnty_name
        into t_LicenseeLK, t_EstablishmentLK, t_CountyName
        from abc11p.name_mstr nm
       where nm.abc_num = i.CNTY_MUNI_CD || '-' || i.LIC_TYPE_CD || '-' || i.MUNI_SER_NUM || '-' || i.GEN_NUM
         and nm.name_type_cd = '2.1-LICENSEE'
         and nm.name_stat = 'C';
    elsif i.lic_stat = 'H' then
      select max(case
                   when exists(select 1 from dataconv.o_abc_establishment e where e.legacykey = to_char(nm.name_mstr_id_num)) then
                     nm.name_mstr_id_num
                 end),
             max(case 
                   when exists(select 1 from dataconv.o_abc_legalentity e where e.legacykey = to_char(nm.name_mstr_id_num)) then
                     nm.name_mstr_id_num
                 end), 
             max(nm.cnty_muni_cd),
             max(case
                when exists (select 1 from dataconv.o_abc_establishmenthistory e where e.legacykey = to_char(nm.name_mstr_id_num)) then
                  nm.name_mstr_id_num
                end)
        into t_EstablishmentLK, t_LicenseeLK, t_CountyName, t_HistEstabLK
        from abc11p.name_mstr nm
       where nm.abc_num = i.CNTY_MUNI_CD || '-' || i.LIC_TYPE_CD || '-' || i.MUNI_SER_NUM || '-' || i.GEN_NUM
         and nm.name_type_cd = '2.1-LICENSEE'
         and nm.name_stat in ('C', 'H');
    end if;
    exception when no_data_found then
      dbms_output.put_line(i.cnty_muni_cd || i.lic_type_cd || i.muni_ser_num || i.gen_num);
    end;

    --Format the Conversion Note
    t_ConversionNote := 'Converted LIC_MSTR Information' || chr(10) ||
                        'VEH_VES_LIC_TYPE: ' || i.VEH_VES_LIC_TYPE || chr(10) ||
                        'ACT_ID_CD: ' || i.ACT_ID_CD || chr(10) || 
                        'REASON_TRANS_CD: ' || i.REASON_TRANS_CD || chr(10) || 
                        'APPL_FILED_CD: ' || i.APPL_FILED_CD || chr(10) || 
                        'Special Conditions?: ' || i.SPEC_COND_IND || chr(10) || 
                        'License Active?: ' || i.LIC_ACTIVE_IND || chr(10) || 
                        'Entrance?: ' || i.ENTRANCE_IND || chr(10) || 
                        'OTH_BUS_PREM_IND: ' || i.OTH_BUS_PREM_IND || chr(10) || 
                        'Law Enforcement: ' || i.LAW_ENF_IND || chr(10) ||
                        'Other Retail Interest?: ' || i.OTH_RET_INT_IND || chr(10) ||
                        'Other Wholesale Interest?: ' || i.OTH_WHOL_INT_IND || chr(10) ||
                        'APPL_NJ_DEN_IND: ' || i.APPL_NJ_DEN_IND || chr(10) || 
                        'NON_APPL_NJ_DEN_IND: ' || i.NON_APPL_NJ_DEN_IND || chr(10) ||
                        'NEG_ACTION_IND: ' || i.NEG_ACTION_IND || chr(10) ||
                        'OTH_NJ_LIC_INT_IND: ' || i.OTH_NJ_LIC_INT_IND || chr(10) || 
                        'FAIL_QUAL_IND: ' || i.FAIL_QUAL_IND || chr(10) || 
                        'Fee Owed?: ' || i.FEE_OWED_IND || chr(10) ||
                        'Population Restricted?: ' || i.POP_RESTR_IND || chr(10) ||
                        'LIC_LMT_EXCPT_IND: ' || i.LIC_LMT_EXCPT_IND || chr(10) ||
                        'CLUB_NJ_ACTIVE_IND: ' || i.CLUB_NJ_ACTIVE_IND || chr(10) || 
                        'CLUB_CHART_IND: ' || i.CLUB_CHART_IND || chr(10) ||
                        'CLUB_QRTR_POS_IND: ' || i.CLUB_QRTR_POS_IND || chr(10) ||
                        'CLUB_VOTE_MEM_IND: ' || i.CLUB_VOTE_MEM_IND || chr(10) || 
                        'N_APP_INT_LIC_IND: ' || i.N_APP_INT_LIC_IND || chr(10) || 
                        'SEC_INT_IND: ' || i.SEC_INT_IND || chr(10) ||
                        'BENEF_INT_IND: ' || i.BENEF_INT_IND || chr(10) || 
                        'Vehicle/Vessel Leased?: ' || i.VEH_VES_LSD_IND || chr(10) || 
                        'Vehicle/Vessel Financed?: ' || i.VEH_VES_FNCD_IND || chr(10) ||
                        'DT_RECOM: ' || i.DT_RECOM|| chr(10) ||
                        'RECOM_INITS: ' || i.RECOM_INITS|| chr(10) ||
                        'DT_APPL_FILED: ' || i.DT_APPL_FILED|| chr(10) ||
                        'MUNI_FEE: ' || i.MUNI_FEE|| chr(10) ||
                        'SIGNED_WAIVER_IND: ' || i.SIGNED_WAIVER_IND|| chr(10) ||
                        'BATF_REF_IND: ' || i.BATF_REG_IND|| chr(10) ||
                        'DT_BATF_FILED: ' || i.DT_BATF_FILED|| chr(10) ||
                        'PREV_LIC_NUM_XFER: ' || i.PREV_LIC_NUM_XFER|| chr(10) ||
                        'FED_BAS_PERM_IND: ' || i.FED_BAS_PERM_IND|| chr(10) ||
                        'PLACE_CNTY_MUNI_CD: ' || i.PLACE_CNTY_MUNI_CD|| chr(10) ||
                        'Warehouse Phone: ' || i.WAR_AREA_CD || i.WAR_PREFIX ||i.WAR_SUFFIX|| chr(10) ||
                        'BLDG_TOT: ' || i.BLDG_TOT|| chr(10) ||
                        'EXCPT_RSN_CD: ' || i.EXCPT_RSN_CD|| chr(10) ||
                        'EXCPT_RSN_HTL: ' || i.EXCPT_RSN_HTL|| chr(10) ||
                        'EXCPT_RSN_RSTRNT: ' || i.EXCPT_RSN_RSTRNT|| chr(10) ||
                        'EXCPT_RSN_BWLNG: ' || i.EXCPT_RSN_BWLNG|| chr(10) ||
                        'EXCPT_RSN_AIRPRT: ' || i.EXCPT_RSN_AIRPRT|| chr(10) ||
                        'EXCPT_RSN_OTH: ' || i.EXCPT_RSN_OTH;

    insert into dataconv.o_abc_license
    (
    LEGACYKEY,
    CREATEDBYUSERLEGACYKEY,
    CONVCREATEDDATE,
    LASTUPDATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE,
    LICENSENUMBER,
    STATE,
    ISSUEDATE,
    EFFECTIVEDATE,
    EXPIRATIONDATE,
    MASTERLK,
    LICENSETYPELK,
    LICENSEELK,
    ESTABLISHMENTLK,
    ESTABLISHMENTHISTORYLK,
    REGIONLK,
    OFFICELK,
    EnablewarehousedataEntryStored,
    INACTIVITYSTARTDATE,
    CONFLICTOFINTEREST,
    CRIMINALCONVICTION,
    FEDERALTAXREGISTRATIONNUMBER
    )
    values
    (
    i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
    i.CRTD_OPER_ID,
    i.DT_ROW_CRTD,
    i.UPDT_OPER_ID,
    i.DT_ROW_UPDT,
    i.CNTY_MUNI_CD ||'-'|| i.LIC_TYPE_CD ||'-'|| i.MUNI_SER_NUM ||'-'|| i.GEN_NUM,
    decode(i.LIC_STAT, 'C', 'Active', 'Closed'),
    t_IssueDate, 
    t_EffectiveDate, 
    t_ExpirationDate,
    i.MasterLK,
    t_LicenseTypeLK,
    t_LicenseeLK,
    t_EstablishmentLK,
    t_HistEstabLK,
    substr(i.cnty_muni_cd, 1, 2),
    i.cnty_muni_cd,
    (case
      when i.war_str_name || i.war_str_num || i.war_po_box || i.war_muni || i.war_st ||
           i.war_zip_1_5 || i.war_zip_6_9 || i.war_area_cd || i.war_prefix || i.war_suffix is not null
     then 'Y'
       else 'N'
     end),
    i.BUS_INACT_DT,
    decode(i.ELEC_OFFICE_IND, 'Y', 'Yes', 'No'),
    decode(i.CRIME_CONV_IND, 'Y', 'Yes', 'No'),
    i.FED_TAX_REG_NUM
    );
    --Create the Conversion Note
    insert into dataconv.n_general
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    parenttablename,
    parentlegacykey,
    locked,
    text
    )
    values
    (
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    'O_ABC_LICENSE',
    i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
    'Y',
    t_ConversionNote
    );
    --Is this conversion note supposed to be on the License or the Legal Entity?
    for c in (select 'Mail'                   PREFERREDCONTACTMETHOD,
                     decode(nm.name_stat, 'C', 'Y', 'N') ACTIVE,
                     'N'                      EDIT,
                     nm.*
                from abc11p.name_mstr nm
               where /*nm.name_type_cd not in ('2.5-D/B/A', '2.6-D/B/A','10A-INTEREST', '2.1-LICENSEE', '10.1-CORP', '10.10-AGENT', '8A-INTEREST', '6A-INTEREST', '8.1-CORP', '8.10-AGENT', '6.1-CORP', '6.10-AGENT')
                 and*/ nm.srce_grp_cd != 'GENERAL'
                 and nm.name_stat != 'X'
                 and nm.abc_num = i.cnty_muni_cd || '-' || i.lic_type_cd || '-' || i.muni_ser_num || '-' || i.gen_num) loop
      --Format the conversion note
      if c.name_type_cd not in ('2.5-D/B/A', '2.6-D/B/A','10A-INTEREST', '2.1-LICENSEE', '10.1-CORP', '10.10-AGENT', '8A-INTEREST', '6A-INTEREST', '8.1-CORP', '8.10-AGENT', '6.1-CORP', '6.10-AGENT') then
      t_ConversionNote := 'Converted ' ||c.NAME_TYPE_CD || 'Information' || chr(10) ||
                          'Date of Birth: ' || c.DT_BIRTH || chr(10) ||
                          'SSN/TIN: ' || c.SSN_TIN || chr(10) ||
                          'New Jersey Tax Authorization Number: ' || c.NJ_TAX_AUTH_NUM|| chr(10) ||
                          'CERT_INC_NUM: ' || c.CERT_INC_NUM || chr(10) ||
                          'DT_CORP_CHART: ' || c.DT_CORP_CHART || chr(10) ||
                          'Name: ' || c.NAME|| chr(10) ||
                          'Business Phone: ' || c.BUS_AREA_CD || c.BUS_PREFIX || c.BUS_SUFFIX|| chr(10) ||
                          'Mailing Address Phone: ' || c.MAIL_AREA_CD || c.MAIL_PREFIX || c.MAIL_SUFFIX || chr(10) ||
                          'Home Phone: ' || c.HOME_AREA_CD || c.HOME_PREFIX || c.HOME_SUFFIX|| chr(10) ||
                          'Name Status: ' || c.NAME_STAT|| chr(10) ||
                          'Mailing Address: ' || c.MAIL_STR_NUM || c.MAIL_STR_NAME || c.MAIL_MUNI_NAME || c.ZIP_1_5|| chr(10) ||
                          'Address: ' || c.STR_NUM || c.STR_NAME || c.MUNI_NAME || c.ZIP_1_5|| chr(10) ||
                          'Position Title: ' || c.POS_TITLE || chr(10) ||
                          'VALID_CORP_IND: ' || c.VALID_CORP_IND || chr(10) ||
                          'CORP_CHART_ST: ' || c.CORP_CHART_ST || chr(10) ||
                          'CORP_NJ_AUTH: ' || c.CORP_NJ_AUTH || chr(10) ||
                          'CORP_NJ_REVOC_IND: ' || c.CORP_NJ_REVOC_IND;

      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      c.crtd_oper_id,
      c.dt_row_crtd,
      c.updt_oper_id,
      c.dt_row_updt,
      'O_ABC_LICENSE',
      i.cnty_muni_cd || '-' || i.lic_type_cd || '-' || i.muni_ser_num || '-' || i.gen_num,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
    end if;
    end loop;
    --Create a license warning if needed
    if i.act_restr is not null then
      insert into dataconv.o_abc_licensewarning
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      LegacyKey,
      Comments,
      StartDate
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
      i.act_restr,
      t_EffectiveDate
      );
      insert into dataconv.r_warninglicense
      (
      O_ABC_LICENSE,
      O_ABC_LICENSEWARNING
      )
      values
      (
      i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
      i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM
      );
      insert into dataconv.r_licensewarningtype
      (
      O_ABC_LICENSEWARNING,
      O_ABC_LICENSEWARNINGTYPE
      )
      values 
      (
      i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
      i.act_restr
      );
      t_RowsProcessed := t_RowsProcessed + 1;
    end if;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
    end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  commit;
end;
