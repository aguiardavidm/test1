-- Created on 3/12/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i                  integer;
  t_MuniObjectId     number;
  t_CountyObjectId   number;
  t_OrderofIssuance  number;
  t_StateCode        number;
  t_SystemSettingsId number;
  t_SeqName          varchar2(40);
  
begin
  -- Set the Municipalities' Order
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select substr(l.licensenumber, 0, 4) MuniCode, max(substr(l.licensenumber, 9, 3)) MaxLicNum
              from dataconv.o_abc_license l
             where substr(l.licensenumber, 0, 4) < 2200 --No municipality codes are higher than this. http://www.state.nj.us/treasury/taxation/pdf/lpt/cntycode.pdf
             group by substr(l.licensenumber, 0, 4)) loop
    select o.objectid
      into t_MuniObjectId
      from query.o_abc_office o
     where o.MunicipalityCountyCode = i.MuniCode;
     api.pkg_columnupdate.SetValue(t_MuniObjectId, 'OrderofIssuance', i.maxlicnum + 1);
      begin
        t_SeqName := 'abc.muniorderissue' || lpad(api.pkg_columnquery.value(t_MuniObjectId, 'MunicipalityCountyCode'), 4, 0) || '_seq';
        execute immediate 'drop sequence ' || t_SeqName;
      exception when others then
        dbms_output.put_line(t_SeqName || ' did not already exist, no need to delete');
      end;
     abc.pkg_abc_municipality.CreateOrderIssuanceSeq(t_MuniObjectId, sysdate, api.pkg_columnquery.Value(t_MuniObjectId, 'MunicipalityCountyCode'));
  end loop;
  --Set the State Issued Order
  select max(substr(l.licensenumber, 0, 4)) StateCode
    into t_StateCode
    from dataconv.o_abc_license l
   where substr(l.licensenumber, 0, 4) > 3000 --No municipality codes are higher than this. http://www.state.nj.us/treasury/taxation/pdf/lpt/cntycode.pdf
     and substr(l.licensenumber, 0, 2) != 99;
  select max(substr(l.licensenumber, 9, 3)) MaxLicNum
    into t_OrderofIssuance
    from dataconv.o_Abc_License l
   where substr(l.licensenumber, 0, 4) = t_StateCode;
  select objectid
    into t_SystemSettingsId
    from query.o_systemsettings
   where rownum < 2;
  api.pkg_columnupdate.SetValue(t_SystemSettingsId, 'StateCode', t_StateCode);
  api.pkg_columnupdate.SetValue(t_SystemSettingsId, 'OrderOfIssuance', t_OrderofIssuance + 1);
  api.pkg_logicaltransactionupdate.EndTransaction();
  begin
    execute immediate 'drop sequence abc.stateorderissue_seq';
  exception when others then
    dbms_output.put_line('abc.stateorderissue_seq did not already exist, no need to delete');
  end;
  execute immediate 'create sequence abc.stateorderissue_seq increment by 1 start with ' || to_char(t_OrderofIssuance + 1) || ' maxvalue 999 cycle nocache';
end;
/
commit;
/