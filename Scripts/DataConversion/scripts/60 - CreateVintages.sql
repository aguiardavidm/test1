--Run time: 2 seconds
-- Created on 9/11/2014 by ADMINISTRATOR
--run as POSSEDBA
declare 
  -- Local variables here
  i integer;
  t_VintageDefId number := api.pkg_configquery.ObjectDefIdForName('o_ABC_Vintage');
  t_NewVintage   number;
begin
  if user != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20000, 'Script must be run as POSSEDBA');
  end if;
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  for i in (select b.vintage
              from abc11p.brn_vintage b
             where not exists (select 1
                                 from query.o_abc_vintage v
                                where v.year = b.vintage)
             group by vintage
             order by 1) loop
             
    t_NewVintage := api.pkg_objectupdate.New(t_VintageDefId);
    api.pkg_columnupdate.SetValue(t_NewVintage, 'Year', i.vintage);
  
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;
/
update dataconv.posseconvtables ct
   set ct.Fill = 'Y'
 where ct.tablename = 'O_ABC_VINTAGE';
 commit;
 /