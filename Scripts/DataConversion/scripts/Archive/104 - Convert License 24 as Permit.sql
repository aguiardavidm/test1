--Run time: 1 minute
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_PermitType      varchar2(100);
  t_NoteText        long := '';
  t_LastSolNum      number(6);
  
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin  
    dbms_output.put_line('Error: '|| a_ErrorText || 'ErrorCode: ' ||a_ErrorCode);
  end;

begin
  
  select ObjectId 
    into t_PermitType 
   from dataconv.o_abc_permittype pt
  where pt.CODE = 'AW24';


for c in (select l.CNTY_MUNI_CD || l.LIC_TYPE_CD || l.MUNI_SER_NUM || l.GEN_NUM LEGACYKEY,
				 l.CRTD_OPER_ID CREATEDBYUSERLEGACYKEY,
				 l.DT_ROW_CRTD CONVCREATEDDATE,
				 l.DT_ROW_UPDT LASTUPDATEDDATE,
				 l.UPDT_OPER_ID LASTUPDATEDBYUSERLEGACYKEY,
				 l.CNTY_MUNI_CD || l.LIC_TYPE_CD || l.MUNI_SER_NUM || l.GEN_NUM PERMITNUMBER,
				 l.LIC_STAT STATE,
				 (select MIN(t.DT_TERM_START) 
            from abc11p.lic_term t 
           where t.cnty_muni_cd || t.lic_type_cd || t.muni_ser_num || t.gen_num = 
                 l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num || l.gen_num) ISSUEDATE,
				 (select MAX(t.DT_TERM_START) 
            from abc11p.lic_term t 
           where t.cnty_muni_cd || t.lic_type_cd || t.muni_ser_num || t.gen_num = 
                 l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num || l.gen_num) EFFECTIVEDATE,
				 (select MAX(t.dt_term_end)
            from abc11p.lic_term t 
           where t.cnty_muni_cd || t.lic_type_cd || t.muni_ser_num || t.gen_num = 
                 l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num || l.gen_num) EXPIRATIONDATE,
				 t_PermitType   PERMITTYPELK,
				 /*( select nm.NAME_MSTR_ID_NUM 
				     from name_mstr nm 
				    where nm.abc_num =  l.CNTY_MUNI_CD ||  '-' ||l.LIC_TYPE_CD || '-' || l.MUNI_SER_NUM ||'-' || l.GEN_NUM
					  and nm.NAME_TYPE_CD = '2.1.-LICENSEE') LICENSEELK,
				 ( select nm2.NAME_MSTR_ID_NUM 
				     from name_mstr nm2 
				    where nm2.abc_num =  l.CNTY_MUNI_CD ||  '-' ||l.LIC_TYPE_CD || '-' || l.MUNI_SER_NUM ||'-' || l.GEN_NUM
					  and nm2.NAME_TYPE_CD = '2.5-D/B/A') ESTABLISHMENTLK,*/
				 substr(l.CNTY_MUNI_CD,1,2) COUNTYLK, --(first two digits)
				 l.CNTY_MUNI_CD MUNICIPLALITYLK, -- (last two digits)
         ('Converted LIC_MSTR Information ' || chr(10) ||
         'Business Inactivity Date:  ' ||  BUS_INACT_DT || chr(10) ||
         'Elected Office?:  ' ||  ELEC_OFFICE_IND || chr(10) ||
         'Criminal Conviction?:  ' ||  CRIME_CONV_IND || chr(10) ||
         'Federal Tax Registration Number:  ' ||  FED_TAX_REG_NUM || chr(10) ||
         'VEH_VES_LIC_TYPE  ' ||  VEH_VES_LIC_TYPE || chr(10) ||
         'ACT_ID_CD  ' ||  ACT_ID_CD || chr(10) ||
         'REASON_TRANS_CD  ' ||  REASON_TRANS_CD || chr(10) ||
         'Applicant Filed Code:  ' ||  APPL_FILED_CD || chr(10) ||
         'Special Conditions?:  ' ||  SPEC_COND_IND || chr(10) ||
         'License Active?:  ' ||  LIC_ACTIVE_IND || chr(10) ||
         'Entrance?:  ' ||  ENTRANCE_IND || chr(10) ||
         'OTH_BUS_PREM_IND  ' ||  OTH_BUS_PREM_IND || chr(10) ||
         'Law Enforcement?:  ' ||  LAW_ENF_IND || chr(10) ||
         'Other Retail Interest?:  ' ||  OTH_RET_INT_IND || chr(10) ||
         'Other Wholesale Interst?:  ' ||  OTH_WHOL_INT_IND || chr(10) ||
         'APPL_NJ_DEN_IND  ' ||  APPL_NJ_DEN_IND || chr(10) ||
         'NON_APPL_NJ_DEN_IND  ' ||  NON_APPL_NJ_DEN_IND || chr(10) ||
         'NEG_ACTION_IND  ' ||  NEG_ACTION_IND || chr(10) ||
         'OTH_NJ_LIC_INT_IND  ' ||  OTH_NJ_LIC_INT_IND || chr(10) ||
         'FAIL_QUAL_IND  ' ||  FAIL_QUAL_IND || chr(10) ||
         'Fee Owed?:  ' ||  FEE_OWED_IND || chr(10) ||
         'Population Restricted?:  ' ||  POP_RESTR_IND || chr(10) ||
         'LIC_LMT_EXCPT_IND  ' ||  LIC_LMT_EXCPT_IND || chr(10) ||
         'CLUB_NJ_ACTIVE_IND  ' ||  CLUB_NJ_ACTIVE_IND || chr(10) ||
         'CLUB_CHART_IND  ' ||  CLUB_CHART_IND || chr(10) ||
         'CLUB_QRTR_POS_IND  ' ||  CLUB_QRTR_POS_IND || chr(10) ||
         'CLUB_VOTE_MEM_IND  ' ||  CLUB_VOTE_MEM_IND || chr(10) ||
         'N_APP_INT_LIC_IND  ' ||  N_APP_INT_LIC_IND || chr(10) ||
         'SEC_INT_IND  ' ||  SEC_INT_IND || chr(10) ||
         'BENEF_INT_IND  ' ||  BENEF_INT_IND || chr(10) ||
         'Vehicle/Vessel Leased?:  ' ||  VEH_VES_LSD_IND || chr(10) ||
         'Vehicle/Vessel Financed?:  ' ||  VEH_VES_FNCD_IND || chr(10) ||
         'ACT_RESTR  ' ||  ACT_RESTR) text,
         rowid
            from abc11p.Lic_Mstr l
           where l.lic_type_cd = '24'
             and not exists (select 1 
                               from dataconv.o_abc_permit 
                              where legacykey = l.CNTY_MUNI_CD || l.LIC_TYPE_CD || l.MUNI_SER_NUM || l.GEN_NUM)) loop
    begin
      insert into dataconv.o_abc_permit a
      (
      LEGACYKEY,
      CREATEDBYUSERLEGACYKEY,
      CONVCREATEDDATE,
      LASTUPDATEDBYUSERLEGACYKEY,
      LASTUPDATEDDATE,
      PERMITNUMBER,
      STATE,
      ISSUEDATE,
      EFFECTIVEDATE,
      EXPIRATIONDATE,
      PERMITTYPELK,
      COUNTYLK,
      MUNICIPALITYLK
      )
      values 
      (
      c.LEGACYKEY,
			c.CREATEDBYUSERLEGACYKEY,
			c.CONVCREATEDDATE,
			c.LASTUPDATEDBYUSERLEGACYKEY,
			c.LASTUPDATEDDATE,
			c.PERMITNUMBER,
			c.STATE,
			c.ISSUEDATE,
			c.EFFECTIVEDATE,
			c.EXPIRATIONDATE,
			c.PERMITTYPELK,
			c.COUNTYLK,
			c.MUNICIPLALITYLK
      );
      
   
  insert into dataconv.n_general
         (createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text)
     values (c.CREATEDBYUSERLEGACYKEY,
             c.CONVCREATEDDATE,
             c.LASTUPDATEDBYUSERLEGACYKEY,
             c.LASTUPDATEDDATE,
             'o_abc_permit',
             c.LEGACYKEY,
             'Y',
             c.text);  
    t_count := t_count +1; 

/*  if mod(t_count,1000) = 0
      then
        commit;
    end if; */        
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;