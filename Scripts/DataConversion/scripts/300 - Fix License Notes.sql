update dataconv.n_general n
   set n.parentlegacykey = replace(n.parentlegacykey, '-', '')
 where n.parenttablename = 'O_ABC_LICENSE'
   and instr(n.parentlegacykey, '-') > 0;
commit;

update dataconv.o_abc_license l
   set l.legacykey = replace(l.legacykey, '-', '')
 where instr(l.legacykey, '-') > 0;
commit;
 
update dataconv.j_abc_newapplication na
   set na.licenselk = replace(na.licenselk, '-', '')
 where instr(na.licenselk, '-') > 0;
commit;
 
delete
  from dataconv.n_general n
 where n.parenttablename = 'O_ABC_LICENSE'
   and not exists (select legacykey from dataconv.o_abc_license where legacykey = n.parentlegacykey);
commit;

update dataconv.n_general n
   set n.parenttablename = 'O_ABC_PERMIT'
 where n.parenttablename = 'o_abc_permit';