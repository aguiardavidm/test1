-- Created on 4/30/2015 by KEATON 
declare 
  -- Local variables here
  t_ColumnDef number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Vehicle', 'StateOfRegistration');
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.Resettransaction();
  
  for c in (select pv.state_reg, v.objectid
              from dataconv.o_abc_vehicle v
              join abc11p.lic_veh_ves pv on pv.abc11puk = v.legacykey
             where pv.state_reg is not null) loop
    
/*    select dlv.DomainValueId
      from api.domains  d
      join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
     where d.Name = 'State'
       and dlv.AttributeValue = c.state_reg;*/
  
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
       c.ObjectId,
       t_ColumnDef,
       null EffectiveStartDate,
       null EffectiveEndDate,
       null AttributeValue, 
       (select dlv.DomainValueId
        from api.domains  d
        join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
       where d.Name = 'State'
         and dlv.AttributeValue = c.state_reg)AttributeValueId,
       null SearchValue,
       null NumericSearchValue
  from query.o_ABC_Vehicle v  
 where v.objectid = c.objectid
 and not exists (select 1 
                     from possedata.ObjectColumnData
                    where ObjectId = c.ObjectId
                      and ColumnDefId = t_ColumnDef);
            
              
  end loop;            
  
  api.pkg_logicaltransactionupdate.Endtransaction();
end;