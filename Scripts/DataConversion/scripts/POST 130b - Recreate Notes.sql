--Pre-Conversion
declare
  t_NoteCount   varchar2(40);
  t_ConversionNote long;
  t_RowsProcessed  number := 0;
  t_TotalRows      number := 0;
  t_IssueDate      date;
  t_EffectiveDate  date;
  t_ExpirationDate date;
  t_ConvUserKey    varchar2(100);
begin
  raise_application_error(-20000, 'Are you sure you want to truncate and recreate notes?');
  truncate table dataconv.n_general;
  -- Created on 5/18/2015 by CXADMIN 

  --NAME_MSTR
  for i in (select nm.rowid,
                   nm.crtd_oper_id,
                   nm.dt_row_crtd,
                   nm.updt_oper_id,
                   nm.dt_row_updt,
                   nm.name_mstr_id_num,
                   nm.name_type_cd
              from abc11p.name_mstr nm
             where nm.name_type_cd in ('10A-INTEREST', '2.1-LICENSEE', '10.1-CORP', '10.10-AGENT', '8A-INTEREST', '6A-INTEREST', '8.1-CORP', '8.10-AGENT', '6.1-CORP', '6.10-AGENT', 'SOLICITOR')
               and nm.srce_grp_cd != 'GENERAL'
               and nm.name_stat != 'X') loop

  t_ConversionNote := abc11p.formatconversionnote('name_mstr', 
                                                  i.rowid, 
                                                  'VALID_CORP_IND, CORP_CHART_ST, CORP_NJ_AUTH, CORP_NJ_REVOC_IND, WT', 
                                                  '10.5 Is corp an existing valid corp?, 10.6  State of Incorporation, 10.7 Certificate of Incorporation Number, 10.9 Has Corp ever been revoked?, WT',
                                                  i.name_type_cd);
  if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LEGALENTITY',
      i.name_mstr_id_num,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' O_ABC_LEGALENTITY Notes.');
  commit;
  t_RowsProcessed := 0;
  --LIC_MSTR
  for i in (select l.rowid,
                   l.cnty_muni_cd,
                   l.lic_type_cd,
                   l.muni_ser_num,
                   l.gen_num,
                   l.crtd_oper_id,
                   l.dt_row_crtd,
                   l.updt_oper_id,
                   l.dt_row_updt
              from abc11p.lic_mstr l
             where l.lic_stat in ('C', 'H')) loop
    --Get the term dates, insert a conversion note if there are historical records
    select min(t.dt_term_start) IssueDate, max(t.dt_term_start) EffectiveDate, max(t.dt_term_end) ExpirationDate
      into t_IssueDate, t_EffectiveDate, t_ExpirationDate
      from abc11p.Lic_Term t
     where t.CNTY_MUNI_CD = i.CNTY_MUNI_CD
                 and t.LIC_TYPE_CD = i.LIC_TYPE_CD
                 and t.MUNI_SER_NUM = i.MUNI_SER_NUM
                 and t.GEN_NUM = i.GEN_NUM;
    t_ConversionNote := 'HISTORICAL TERMS FOR LICENSE NUMBER' || chr(10) || chr(13);
    for c in (select t.*
                from abc11p.Lic_Term t
               where t.CNTY_MUNI_CD = i.CNTY_MUNI_CD
                 and t.LIC_TYPE_CD = i.LIC_TYPE_CD
                 and t.MUNI_SER_NUM = i.MUNI_SER_NUM
                 and t.GEN_NUM = i.GEN_NUM) loop
      t_ConversionNote := t_ConversionNote || c.dt_term_start || ' to ' || c.dt_term_end || chr(10) || chr(13) ||
                          'Resolution Received: ' || c.RESOLUTION_IND || chr(10) || chr(13);
    end loop;
    insert into dataconv.n_general
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    parenttablename,
    parentlegacykey,
    locked,
    text
    )
    values
    (
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    'O_ABC_LICENSE',
    i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
    'Y',
    t_ConversionNote
    );
    t_ConversionNote := null;
    t_RowsProcessed := t_RowsProcessed + 1;
    t_TotalRows := t_totalRows + 1;
    --End TERM note

    --Format the License Note
    t_ConversionNote := abc11p.formatconversionnote('LIC_MSTR',
                                                    I.ROWID,
                                                    'VEH_VES_LIC_TYPE, ACT_ID_CD, REASON_TRANS_CD, SPEC_COND_IND, LIC_ACTIVE_IND, ENTRANCE_IND, OTH_BUS_PREM_IND, LAW_ENF_IND, OTH_RET_INT_IND, OTH_WHOL_INT_IND, APPL_NJ_DEN_IND, NON_APPL_NJ_DEN_IND, NEG_ACTION_IND, OTH_NJ_LIC_INT_IND, FAIL_QUAL_IND, FEE_OWED_IND, POP_RESTR_IND, LIC_LMT_EXCPT_IND, CLUB_NJ_ACTIVE_IND, CLUB_CHART_IND, CLUB_QRTR_POS_IND, CLUB_VOTE_MEM_IND, N_APP_INT_LIC_IND, SEC_INT_IND, BENEF_INT_IND, VEH_VES_LSD_IND, VEH_VES_FNCD_IND, DT_RECOM, RECOM_INITS, DT_APPL_FILED, MUNI_FEE, SIGNED_WAIVER_IND, BATF_REG_IND, DT_BATF_FILED, PREV_LIC_NUM_XFER, FED_BAS_PERM_IND, PLACE_CNTY_MUNI_CD, WAR_AREA_CD || WAR_PREFIX ||WAR_SUFFIX, BLDG_TOT, EXCPT_RSN_CD, EXCPT_RSN_HTL, EXCPT_RSN_RSTRNT, EXCPT_RSN_BWLNG, EXCPT_RSN_AIRPRT, EXCPT_RSN_OTH',
                                                    'Vehicle Type, ACT_ID_CD, Reason for Transfer, Special Conditions, License Active, ENTRANCE_IND, Other business conducted on Premise?, Is applicant a police officer?, Other Retail Interest?, Other Wholesale Interest?, 6.1 Has applicant ever been denied a NJ License?, 6.2 Has anyone other than the applicant been denied a NJ License or Permit?, 6.3 Has anyone mentioned ever been suspended or revoked?, 7.1 Any immediate family having interest in another NJ License., 7.2 Would any of the applicants fail to qualify for License?, 8.1 Does anyone of the applicant owe the State of NJ fees penalties or tax?, 8.2 Is this license being issed as a Hotel/Motel Exception to the Population?, 8.3 Has the license been issued as an exception to the two license limitation?, 8.10  Is Club Active for three years?, 8.11  Is Club Chartered?, 8.12  Is there exclusive use of qrtrs for three years?, 8.13  Are there 60 voting members, 9.1 Any indiv or corp other than appl have intrst in the license?, 9.2  Are they any chattel mort or sec intrst with lic applied for?, 9.3 Has appl agreed to share proceeds with anyone not mentioned in app?, Vehicle/Vessel Leased?, Vehicle/Vessel Financed?, Date of Recommendation, Initials of Issuer, Date of Application, Municipal Fee, 4.1 Is entrance near a church or school?, 4.2 Has applicant filed  w/TTB?, Date of TTB Filing, 1992 Previous license number transferred, Federal Basic Permit #, County/Municipal Code, Warehouse Phone, 3.1 Number of separate buildings to be included in license, 8.2 Exception Reason Code, 8.2 Is Exception a Hotel/Motel?, 8.2 Is Exception a Restaurant?, 8.2 Is Exception a Bowling Alley?, 8.2 Is Exception a Airport?, 8.2 Is Exception "Other"?',
                                                    'LIC_MSTR');
    --Create the Conversion Note
    if t_ConversionNote is not null then
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    --Extra related entities
    for c in (select nm.rowid,
                     nm.crtd_oper_id,
                     nm.dt_row_crtd,
                     nm.updt_oper_id,
                     nm.dt_row_updt,
                     nm.name_type_cd
                from abc11p.name_mstr nm
               where nm.srce_grp_cd != 'GENERAL'
                 and nm.name_stat != 'X'
                 and nm.abc_num = i.cnty_muni_cd || '-' || i.lic_type_cd || '-' || i.muni_ser_num || '-' || i.gen_num) loop
      --Format the conversion note
      if c.name_type_cd not in ('2.5-D/B/A', '2.6-D/B/A','10A-INTEREST', '2.1-LICENSEE', '10.1-CORP', '10.10-AGENT', '8A-INTEREST', '6A-INTEREST', '8.1-CORP', '8.10-AGENT', '6.1-CORP', '6.10-AGENT') then
        t_ConversionNote := abc11p.formatconversionnote('NAME_MSTR',
                                                        c.rowid,
                                                        'DT_BIRTH, SSN_TIN, NJ_TAX_AUTH_NUM, CERT_INC_NUM, DT_CORP_CHART, NAME, BUS_AREA_CD || BUS_PREFIX || BUS_SUFFIX, MAIL_AREA_CD || MAIL_PREFIX || MAIL_SUFFIX, HOME_AREA_CD || HOME_PREFIX || HOME_SUFFIX, NAME_STAT, MAIL_STR_NUM || chr(32) || MAIL_STR_NAME || chr(32) || MAIL_MUNI_NAME || chr(32) || MAIL_ZIP_1_5, STR_NUM || chr(32) || STR_NAME ||  chr(32) || MUNI_NAME || chr(32) || ZIP_1_5, POS_TITLE, VALID_CORP_IND, CORP_CHART_ST, CORP_NJ_AUTH, CORP_NJ_REVOC_IND',
                                                        'Date of Birth, SSN/TIN, New Jersey Tax Authorization Number, 10.3 NJ Sales Tax Authority Number, Date of Corp Charter, Name, Business Phone, Mailing Address Phone, Home Phone, Name Status, Mailing Address, Address, Position Title, 10.5 Is corp an existing valid corp?, 10.6  State of Incorporation, 10.7 Certificate of Incorporation Number, 10.9 Has Corp ever been revoked?',
                                                        c.name_type_cd);
        if t_ConversionNote is not null then
          --Create the Conversion Note
          insert into dataconv.n_general
          (
          createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text
          )
          values
          (
          c.crtd_oper_id,
          c.dt_row_crtd,
          c.updt_oper_id,
          c.dt_row_updt,
          'O_ABC_LICENSE',
          i.cnty_muni_cd || i.lic_type_cd || i.muni_ser_num || i.gen_num,
          'Y',
          t_ConversionNote
          );
          t_ConversionNote := null;
          t_RowsProcessed := t_RowsProcessed + 1;
          t_TotalRows := t_totalRows + 1;
        end if;
      end if;
      if mod(t_RowsProcessed, 10000) = 0 then
        commit;
        null;
      end if;
    end loop;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' O_ABC_LICENSE Notes.');
  commit;
  t_RowsProcessed := 0;

  -- Other License Notes
  for i in (select lc.rowid,
                   lc.cnty_muni_cd || lc.lic_type_cd || lc.muni_ser_num || lc.gen_num LegacyKey,
                   lc.updt_oper_id,
                   lc.dt_row_updt
                from abc11p.lic_cmt lc
               order by lc.seq_num) loop
      
    t_ConversionNote := abc11p.formatconversionnote('LIC_CMT', 
                                                    i.rowid, 
                                                    'FORM_ID, CMT_TEXT', 
                                                    'Form ID, Text',
                                                    'LIC_CMT');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' LIC_CMT Notes.');
  commit;
  t_RowsProcessed := 0;
  for i in (select n.name_mstr_id_num,
                   n.updt_oper_id,
                   n.dt_row_updt,
                   n.cnty_muni_cd || n.lic_type_cd || n.muni_ser_num || n.gen_num LegacyKey,
                   n.rowid
              from abc11p.negative_action n
             where exists (select 1 from dataconv.o_abc_license l where l.legacykey = n.cnty_muni_cd || n.lic_type_cd || n.muni_ser_num || n.gen_num)) loop
      
    t_ConversionNote := abc11p.formatconversionnote('NEGATIVE_ACTION', 
                                                    i.rowid, 
                                                    'DT_ROW_UPDT, DT_ACTION, DOCKET_NUM, PENALTY_IMPOSED_BY, FINED_IND, FINE_AMT, NOT_RENEWED_IND, SUSPENDED_IND, NUM_DAYS_SUSP, REVOKED_IND, CANCELLED_IND, OTHER_IND', 
                                                    'Last Updated, Date of Action, Docket Number, Penalty Imposed By, Fined?, Fine Amount, Not Renewed?, Suspended?, Number of Days Suspended, Revoked?, Cancelled?, Other?',
                                                    'NEGATIVE_ACTION');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' NEGATIVE_ACTION Notes.');
  commit;
  t_RowsProcessed := 0;
  for i in (select o.name_mstr_id_num,
                   o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num LegacyKey,
                   o.updt_oper_id,
                   o.dt_row_updt,
                   o.rowid,
                   o.nat_bus_cd
              from abc11p.oth_bus o
             where exists (select 1 from dataconv.o_abc_license l where l.legacykey = o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num)) loop      
    t_ConversionNote := abc11p.formatconversionnote('OTH_BUS', 
                                                    i.rowid, 
                                                    '(select d.cd_desc from  abc11p.code_desc d where d.code = ''' || Nvl(i.nat_bus_cd, 000) || ''' and id = ''NB''), OPER_BY_IND, BUS_TOBE_OPER, (select nm.name from abc11p.name_mstr nm where nm.name_mstr_id_num = ' || nvl(i.name_mstr_id_num, 000) || ')', 
                                                    'Nature of Business, Operated By, Business ToBe Operator, Name',
                                                    'OTH_BUS');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' OTH_BUS Notes.');
  commit;
  t_RowsProcessed := 0;
  for i in (select o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num LegacyKey,
                   o.RowId,
                   o.dt_row_updt,
                   o.updt_oper_id
              from abc11p.oth_lic_int o
             where exists (select 1 from dataconv.o_abc_license l where l.legacykey = o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num)) loop      
    t_ConversionNote := abc11p.formatconversionnote('OTH_LIC_INT', 
                                                    i.rowid, 
                                                    'OTH_CNTY_MUNI_CD || OTH_LIC_TYPE_CD || OTH_MUNI_SER_NUM || OTH_GEN_NUM', 
                                                    'Other License Interest',
                                                    'OTH_LIC_INT');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' OTH_LIC_INT Notes.');
  commit;
  t_RowsProcessed := 0;
  for i in (select o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num LegacyKey,
                   o.Rowid,
                   o.Updt_Oper_Id,
                   o.dt_row_updt
              from abc11p.lic_crime o
             where exists (select 1 from dataconv.o_abc_license l where l.legacykey = o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num)) loop
    t_ConversionNote := abc11p.formatconversionnote('LIC_CRIME', 
                                                    i.rowid, 
                                                    'DT_DISQ_REMOV, DISQ_REMOV_NUM', 
                                                    'Date of Disqual Removal, Disqual Removal Number',
                                                    'LIC_CRIME');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' LIC_CRIME Notes.');
  commit;
  t_RowsProcessed := 0;
  for i in (select r.rowid,
                   r.crtd_oper_id,
                   r.dt_row_crtd,
                   r.updt_oper_id,
                   r.dt_row_updt,
                   r.cnty_muni_cd || r.lic_type_cd || r.muni_ser_num || r.gen_num LegacyKey
             from abc11p.rest_brew r
             join dataconv.o_abc_license l on l.licensenumber = r.cnty_muni_cd || '-' || r.lic_type_cd || '-' ||  r.muni_ser_num || '-' || r.gen_num) loop
    t_ConversionNote := abc11p.formatconversionnote('REST_BREW', 
                                                    i.rowid, 
                                                    'PRCL_CNTY_MUNI_CD ||''-''|| PRCL_LIC_TYPE_CD ||''-''||PRCL_MUNI_SER_NUM ||''-''|| PRCL_GEN_NUM, ON_PREM_CONS_IND, OFF_PREM_CONS_IND, PAPER_NAME, DT_1ST_NOTICE, DT_2ND_NOTICE, DT_BATF_NOTICE', 
                                                    'Plenary Retail Consumption License, On Premise Consumption Indicator, Off Premise Consumption Indicator, Paper Name, Date of 1st Notice, Date of 2nd Notice, TTB Notice',
                                                    'REST_BREW');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' REST_BREW Notes.');
  commit;
  t_RowsProcessed := 0;
  --LIC_PEND
  for i in (select l.rowid,
                   l.crtd_oper_id,
                   l.dt_row_crtd,
                   l.updt_oper_id,
                   l.dt_row_updt,
                   l.CNTY_MUNI_CD || l.LIC_TYPE_CD || l.MUNI_SER_NUM || l.GEN_NUM LegacyKey
              from abc11p.lic_pend l) loop
    t_ConversionNote := abc11p.formatconversionnote('LIC_PEND', 
                                                    i.rowid, 
                                                    'TRANS_NUM, INIT_FEE, NJ_BOND_IND, BATF_PERM_IND, NOTICE_IND, SBI_FEE_IND, DT_INVST_REFRD, DT_INVST_BACK, INVEST_AGENCY, INVEST_NUM, INVEST_ASSN, AFF_OF_PUB, NOTICE_TO_MUNI, FULL_APP_IND', 
                                                    'Transaction Number, Initial Fee, NJ Bond?, TTB Permit, Notice?, SBI Code, Date Investigation Referred, Date Investigation Back, Investigation Agency, Investigation Number, Investigation Association, Affidavit of Publication, Notice to Municiaplity, Full Application?',
                                                    'LIC_PEND');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' LIC_PEND Notes.');
  commit;
  t_RowsProcessed := 0;
  --LIC_BLDG
  for i in (select b.rowid,
                   b.cnty_muni_cd || b.lic_type_cd || b.muni_ser_num || b.gen_num LegacyKey,
                   b.updt_oper_id,
                   b.dt_row_updt
              from abc11p.lic_bldg b) loop
    t_ConversionNote := abc11p.formatconversionnote('LIC_BLDG', 
                                                    i.rowid, 
                                                    'BLDG_NUM, ENTIRE_BLDG_IND, ADJCNT_GRND_IND, UNLIC_AREA_IND, APPL_OWN_BLDG_IND, MTG_BLDG_IND, APPL_LEASE_BLDG_IND', 
                                                    'Building Number, Is entire bldg licensed?, Are there adjacent grounds to be licensed?, Is there any Unlicensed Area bwtn ?, Does applicant own the building?, Is Bldg mortgaged?, Applicant Leases Building?',
                                                    'Building');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' LIC_BLDG Notes.');
  commit;
  t_RowsProcessed := 0;
  --LIC_FLOOR
  for i in (select b.rowid,
                   b.cnty_muni_cd || b.lic_type_cd || b.muni_ser_num || b.gen_num LegacyKey,
                   b.updt_oper_id,
                   b.dt_row_updt
              from abc11p.lic_floor b) loop
    t_ConversionNote := abc11p.formatconversionnote('LIC_FLOOR', 
                                                    i.rowid, 
                                                    'FLOOR_NUM, ALL_PART_IND', 
                                                    'Floor Number, All or Part',
                                                    'Building');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LICENSE',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' LIC_FLOOR Notes.');
  commit;
  t_RowsProcessed := 0;
  
  --Permits
  --Permit
  for i in (select p.rowid,
                   p.crtd_oper_id,
                   p.dt_row_crtd,
                   p.updt_oper_id,
                   p.dt_row_updt,
                   p.perm_num LegacyKey
              from abc11p.Permit p
             where p.perm_stat in ('C', 'H')) loop
    t_ConversionNote := abc11p.formatconversionnote('PERMIT', 
                                                    i.rowid, 
                                                    'LIC_YR, OTH_NAME, OTH_STR_NUM || chr(32) || OTH_STR_NAME || chr(32) || OTH_PO_BOX || chr(32) || OTH_CITY || chr(32) || OTH_ST_PROV_CD || chr(32) || OTH_ZIP_1_5 || OTH_ZIP_6_9 || chr(32) || OTH_CNTRY_CD || chr(32) || OTH_FOR_POST_CD, BRN_NAME, QUANTITY, PRICE, ITEM_SIZE, PRODUCT, AGE, HAIR_COLOR, EYE_COLOR, DOB, HT_FT, HT_INCH, WEIGHT, SEX, OTH_ST_LIC_NUM, FED_PERM_NUM, FILE_NUM, VINTAGE', 
                                                    'License Year, Other Name, Other Address, Brand Name, Quantity, Price, Item Size, Product, Age, Hair Color, Eye Color, Date of Birth, Height Feet, Height Inches, Weight, Sex, Other State License Number, Federal Permit #, File #, Vintage',
                                                    'Permit');
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' PERMIT Notes.');
  commit;
  t_RowsProcessed := 0;
  --PERM_CMT
  for i in (select min(p.updt_oper_id) crtd_oper_id,
                   min(p.dt_row_updt) dt_row_crtd, 
                   max(p.updt_oper_id) updt_oper_id,
                   max(p.dt_row_updt) dt_row_updt,
                   p.perm_num LegacyKey,
                   listagg(p.cmt_text, ' ') within group (order by p.seq_num) text
              from abc11p.perm_cmt p
             where exists (select 1 from dataconv.o_abc_permit dp where dp.legacykey = to_char(p.perm_num))
             group by p.perm_num) loop
    t_ConversionNote := 'Converted PERM_CMT Information' || chr(10) || chr(13) ||
                        i.text;

    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' PERM_CMT Notes.');
  commit;
  t_RowsProcessed := 0;
  --PERM_SOLICITOR
  for i in (select min(p.updt_oper_id) crtd_oper_id,
                   min(p.dt_row_updt) dt_row_crtd, 
                   max(p.updt_oper_id) updt_oper_id,
                   max(p.dt_row_updt) dt_row_updt,
                   p.perm_num LegacyKey,
                   listagg(p.sol_num, ', ') within group (order by p.sol_num) text
              from abc11p.Perm_Solicitor p
             where exists (select 1 from dataconv.o_abc_permit dp where dp.legacykey = to_char(p.perm_num))
             group by p.perm_num) loop
    t_ConversionNote := 'Converted PERM_SOLICITOR Information' || chr(10) || chr(13) ||
                        i.text;

    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' PERM_SOLICITOR Notes.');
  commit;
  t_RowsProcessed := 0;
  --SOLICITOR
  for i in (select s.rowid,
                   s.crtd_oper_id,
                   s.dt_row_crtd,
                   s.updt_oper_id,
                   s.dt_row_updt,
                   s.sol_num || 'SOL' LegacyKey
              from abc11p.solicitor s) loop
    t_ConversionNote := abc11p.formatconversionnote('SOLICITOR', 
                                                    i.rowid, 
                                                    'SPEC_COND_IND, EFF_DT, SAL_IND, COMM_IND, BONUS_IND, EXP_IND, PCT_IND, NONE_IND, OTHER_IND, DT_EMP_START, DR_LIC_STATE, DR_LIC_NUM, PREV_EMP_IND, OTH_INT_IND, MEM_OF_AUTH_IND, ENF_POS_IND, DEN_PERM_IND, ABC_VIOL_IND, CONV_OTH_IND, DISQ_REM_IND', 
                                                    'Special Conditions?, Effective Date, #7 Type of Compensation Salary?, #7 Type of Compensation Commision?, #7 Type of Compensation Bonus?, #7 Type of Compensation Expenses?, #7 Type of Compensation Percentage?, #7 Type of Compensation None?, OTHER_IND, Employee start date, Drivers License State, Drivers License Number, #22 Have you been previously employed as a Solicitor?, #23 Do you presently or ever held interest in NJ License?, MEM_OF_AUTH_IND, #25 Do you currently hold an offical law enforcement position?, #26 Have you ever been denied a NJ Solicitor''''s Permit?, #27 Are you being Investigated or have you ever been convicted of an ABCviolation?, #28 Are you being investigated or have you ever been convicted of any crime?, #29 Have you ever petitioned to ABC for a disqual of removal/elig?',
                                                    'Solicitor');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' SOLICITOR Notes.');
  commit;
  t_RowsProcessed := 0;
  --SOL_CMT
  for i in (select min(p.updt_oper_id) crtd_oper_id,
                   min(p.dt_row_updt) dt_row_crtd, 
                   max(p.updt_oper_id) updt_oper_id,
                   max(p.dt_row_updt) dt_row_updt,
                   p.sol_num || 'SOL' LegacyKey,
                   listagg(p.cmt_text, ', ') within group (order by p.seq_num) text
              from abc11p.sol_cmt p
             where exists (select 1 from dataconv.o_abc_permit dp where dp.legacykey = to_char(p.sol_num) || 'SOL')
             group by p.sol_num) loop
    t_ConversionNote := 'Converted SOL_CMT Information' || chr(10) || chr(13) ||
                        i.text;

    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' PERM_SOLICITOR Notes.');
  commit;
  t_RowsProcessed := 0;
  --SOL_CRIME
  for i in (select s.rowid,
                   s.updt_oper_id,
                   s.dt_row_updt,
                   s.sol_num || 'SOL' LegacyKey
              from abc11p.Sol_Crime s
              order by s.sol_num, s.seq_num) loop
    t_ConversionNote := abc11p.formatconversionnote('SOL_CRIME', 
                                                    i.rowid, 
                                                    'SEQ_NUM, NATURE_OF_OFF, PENALTY_STAT, DT_CONV, JURISDICTION, IDENTIFY_JURIS',
                                                    'Sequence Number, Nature of Offence, Penalty Status, Date Convicted, Jurisdiction, Identify Jurisdiction',
                                                    'SOL_CRIME');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' SOLICITOR Notes.');
  commit;
  t_RowsProcessed := 0;
  --SOL_CRIME
  for i in (select s.rowid,
                   s.updt_oper_id,
                   s.dt_row_updt,
                   s.sol_num || 'SOL' LegacyKey
              from abc11p.sol_violation s
              order by s.sol_num, s.seq_num) loop
    t_ConversionNote := abc11p.formatconversionnote('SOL_VIOLATION', 
                                                    i.rowid, 
                                                    'SEQ_NUM, NATURE_OF_OFF, PENALTY_STAT, DT_CONV, JURISDICTION, IDENTIFY_JURIS',
                                                    'Sequence Number, Nature of Offence, Penalty Status, Date Convicted, Jurisdiction, Identify Jurisdiction',
                                                    'SOL_VIOLATION');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' SOL_VIOLATION Notes.');
  commit;
  t_RowsProcessed := 0;
  --ADD_PRIV_MSTR
  for i in (select a.rowid,
                   a.crtd_oper_id,
                   a.dt_row_crtd,
                   a.updt_oper_id,
                   a.dt_row_updt,
                   a.CNTY_MUNI_CD || a.LIC_TYPE_CD || a.MUNI_SER_NUM || a.GEN_NUM || a.PRIV_SEQ_NUM LegacyKey
              from abc11p.add_priv_mstr a) loop
    t_ConversionNote := abc11p.formatconversionnote('ADD_PRIV_MSTR', 
                                                    i.rowid, 
                                                    'OFF_PREM_CONS_IND, ON_PREM_CONS_IND, SAMPLING_IND, BLDG_TOT',
                                                    'Off Premise Consumption?, On Premise Consumption?, Sampling?, Building Total',
                                                    'ADD_PRIV_MSTR');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' ADD_PRIV_MSTR Notes.');
  commit;
  t_RowsProcessed := 0;
  --ADD_PRIV_BLDG
  for i in (select a.rowid,
                   a.updt_oper_id,
                   a.dt_row_updt,
                   a.CNTY_MUNI_CD || a.LIC_TYPE_CD || a.MUNI_SER_NUM || a.GEN_NUM || a.PRIV_SEQ_NUM LegacyKey,
                   a.bldg_name_mstr_id_num,
                   a.lease_name_mstr_id_num
              from abc11p.ADD_PRIV_BLDG a) loop
    t_ConversionNote := abc11p.formatconversionnote('ADD_PRIV_BLDG', 
                                                    i.rowid, 
                                                    'BLDG_NUM, ENTIRE_BLDG_IND, ADJCNT_GRND_IND, UNLIC_AREA_IND, APPL_OWN_BLDG_IND, MTG_BLDG_IND, (select NM.name from  abc11p.NAME_MSTR NM where nm.name_mstr_id_num = ''' || nvl(i.BLDG_NAME_MSTR_ID_NUM, 0) || '''), APPL_LEASE_BLDG_IND, (select NM.name from  abc11p.NAME_MSTR NM where nm.name_mstr_id_num = ''' || nvl(i.LEASE_NAME_MSTR_ID_NUM, 0) || ''')',
                                                    'Building Number, Entire Building?, Adjacent Grounds?, Unlicensed Area?, Applicant Owns Building?, Does building have mortgage?, Building Owner Name, Applicant Leases Building?, Leasee Name',
                                                    'ADD_PRIV_BLDG');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' ADD_PRIV_BLDG Notes.');
  commit;
  t_RowsProcessed := 0;
  --ADD_PRIV_FLOOR
  for i in (select a.rowid,
                   a.updt_oper_id,
                   a.dt_row_updt,
                   a.CNTY_MUNI_CD || a.LIC_TYPE_CD || a.MUNI_SER_NUM || a.GEN_NUM || a.PRIV_SEQ_NUM LegacyKey
              from abc11p.ADD_PRIV_FLOOR a) loop
    t_ConversionNote := abc11p.formatconversionnote('ADD_PRIV_FLOOR', 
                                                    i.rowid, 
                                                    'FLOOR_NUM, ALL_PART_IND',
                                                    'Floor Number, All or Part',
                                                    'ADD_PRIV_FLOOR');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' ADD_PRIV_FLOOR Notes.');
  commit;
  t_RowsProcessed := 0;
  
  --Access Tables
  --MarketagentTables.mdb
  --Legal Entity
  select legacykey
    into t_ConvUserKey 
    from dataconv.u_users u
   where u.oraclelogonid = 'ABCCONV';
  for i in (select a.rowid,
                   t_ConvUserKey updt_oper_id,
                   to_date(a.applicant_timestamp, 'mm/dd/yyyy hh24:mi:ss') dt_row_updt,
                   a.applicant_id || 'MA' LegacyKey
              from abc11p.ma_applicant_int_t a) loop
    t_ConversionNote := abc11p.formatconversionnote('ma_applicant_int_t', 
                                                    i.rowid, 
                                                    'APPLICANT_DRIVER_LIC',
                                                    'Drivers License',
                                                    'Market Agent');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_LEGALENTITY',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' ma_applicant_int_t Notes.');
  commit;
  t_RowsProcessed := 0;
  --Permit Promoter
  for i in (select pi.rowid,
                   t_ConvUserKey updt_oper_id,
                   to_date(pi.promoter_timestamp, 'mm/dd/yyyy hh24:mi:ss') dt_row_updt,
                   t.permit_id || 'MA' LegacyKey
             from abc11p.ma_permit_int_t t
             join abc11p.ma_promoter_int_t pi on pi.promoter_id = t.promoter_id) loop
    t_ConversionNote := abc11p.formatconversionnote('ma_promoter_int_t',
                                                    i.rowid, 
                                                    'PROMOTER_TIMESTAMP, PROMOTER_NAME, PROMOTER_STREET, PROMOTER_CITY, PROMOTER_STATE, PROMOTER_ZIP, PROMOTER_CONTACT, PROMOTER_PHONE, PROMOTER_EMAIL',
                                                    'Promoter Timestamp, Promoter Name, Promoter Street, Promoter City, Promoter State, Promoter Zip, Promoter Contact, Promoter Phone, Promoter Email',
                                                    'Promoter');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' ma_promoter_int_t Notes.');
  commit;
  t_RowsProcessed := 0;
  --Permit Promoter
  for i in (select a.rowid,
                   t_ConvUserKey updt_oper_id,
                   to_date(a.datefiled, 'mm/dd/yyyy hh24:mi:ss') dt_row_updt,
                   a.License_Num LegacyKey
             from abc11p.A_tblAppeal_int_t a) loop
    t_ConversionNote := abc11p.formatconversionnote('A_tblAppeal_int_t',
                                                    i.rowid, 
                                                    'Respondent, DatetoOAL, datereturnedtoabc',
                                                    'Respondent, Date To OAL, Date Returned',
                                                    'Appeal');
    
    if t_ConversionNote is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      i.updt_oper_id,
      i.dt_row_updt,
      i.updt_oper_id,
      i.dt_row_updt,
      'O_ABC_PERMIT',
      i.legacykey,
      'Y',
      t_ConversionNote
      );
      t_ConversionNote := null;
      t_RowsProcessed := t_RowsProcessed + 1;
      t_TotalRows := t_totalRows + 1;
    end if;
    if mod(t_RowsProcessed, 10000) = 0 then
      commit;
      null;
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' A_tblAppeal_int_t Notes.');
  commit;
  t_RowsProcessed := 0;
  
  delete 
    from dataconv.posseconvprogress p
   where p.tablename = 'N_GENERAL';
  commit;
  delete 
    from dataconv.posseconvkeywordnoteprogress;
  commit;
  update dataconv.n_general n
   set n.lastupdatedbyuserlegacykey = 'ABCCONV'
  where not(LastUpdatedByUserLegacyKey is null and LastUpdatedDate is null or LastUpdatedByUserLegacyKey is not null and LastUpdatedDate is not null);
  
  update dataconv.n_general n
     set n.CreatedByUserLegacyKey = 'ABCCONV'
  where not(CreatedByUserLegacyKey is null and ConvCreatedDate is null or CreatedByUserLegacyKey is not null and ConvCreatedDate is not null);
  commit;
end;
