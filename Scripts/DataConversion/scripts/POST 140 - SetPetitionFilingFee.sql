declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('j_ABC_Petition', 'FilingFee');
begin
  for i in (select d.objectid, d.filingfee
              from dataconv.j_abc_petition d
             where api.pkg_columnquery.value(d.objectid, 'Filingfee') is null
               ) loop
    insert into possedata.objectcolumndata
    (
    LOGICALTRANSACTIONID,
    OBJECTID,
    COLUMNDEFID,
    ATTRIBUTEVALUE
    )
    values
    (
    1,
    i.objectid,
    t_ColumnDefId,
    0
    );
  end loop;
end;