declare 
  t_EndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_Permit', 'PermitType');
  t_OthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_Permit', 'PermitType');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitPermitType');
  t_RelId         number;
begin
  for i in (select p.objectid PermitId, pt.objectid TypeId
              from dataconv.o_abc_permit p
              join abc11p.add_priv_mstr a on a.CNTY_MUNI_CD ||'-'|| a.LIC_TYPE_CD ||'-'|| a.MUNI_SER_NUM ||'-'|| a.GEN_NUM ||'-'|| a.PRIV_SEQ_NUM = p.legacykey
              join dataconv.o_abc_Licensetype lt on lt.code = a.lic_type_cd
              join query.r_ABC_PermitTypeLicenseType r on r.LicenseTypeId = lt.objectid
              join dataconv.o_abc_permittype pt on pt.objectid = r.PermitTypeId
              where pt.code in ('APRVL', 'APROS','APRV')) loop
    for c in (select relationshipid
                from query.r_ABC_PermitPermitType
               where PermitId = i.PermitId) loop
      update rel.StoredRelationships r
         set ToObjectId = i.Typeid
       where r.RelationshipId = c.relationshipid
         and r.ENDPOINTID = t_EndpointId;
      update rel.StoredRelationships r
         set FromObjectId = i.Typeid
       where r.RelationshipId = c.relationshipid
         and r.ENDPOINTID = t_OthEndpointId;
    end loop;
  end loop;
end;
/

declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'AdditionalPermitInformation');
begin
  for i in (select CNTY_MUNI_CD ||'-'|| LIC_TYPE_CD ||'-'|| MUNI_SER_NUM ||'-'|| GEN_NUM ||'-'|| PRIV_SEQ_NUM PermNum,
                   '('||AREA_CD||')' || PREFIX ||'-'|| SUFFIX AdditionalPermitInformation,
                   p.objectid
              from abc11p.add_priv_mstr a
              join dataconv.o_abc_permit p on p.legacykey = CNTY_MUNI_CD ||'-'|| LIC_TYPE_CD ||'-'|| MUNI_SER_NUM ||'-'|| GEN_NUM ||'-'|| PRIV_SEQ_NUM
             where a.area_cd || a.prefix || a.suffix is not null) loop
    delete from possedata.objectcolumndata
     where objectid = i.objectid
       and columndefid = t_ColumnDefId;
    insert into possedata.objectcolumndata_t
    (
    LOGICALTRANSACTIONID,
    OBJECTID,
    COLUMNDEFID,
    ATTRIBUTEVALUE
    )
    values
    (
    1,
    i.objectid,
    t_ColumnDefId,
    i.additionalpermitinformation
    );
  end loop;
end;