--Run Time: 1 second
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_PermitType      varchar2(100);
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
  update abc11p.ma_app_card_int_t am
        set am.errortext = a_ErrorText,
            am.errorcode = a_ErrorCode
      where am.rowid = a_RowId;  
  
    commit;
  end;

begin

  select legacykey
    into t_ConvUserKey 
   from dataconv.u_users u
  where u.oraclelogonid = 'ABCCONV';
  
  select ObjectId 
    into t_PermitType 
   from dataconv.o_abc_permittype pt
  where pt.code = 'MA2';


  for c in (select t.rowid,
                   t.APP_CARD_ID || 'MAAPPCARD' LEGACYKEY,
                   t.PERMIT_ID || 'MAPERMIT'  PERMITLK,
                   t_PermitType PERMITTYPELK,
                   t.applicant_id || 'MA' PERMITEELK,
                   to_date(t.APP_CARD_TIMESTAMP, 'MM/DD/YY HH24:MI:SS') LASTUPDATEDDATE,
                   to_date(t.APP_CARD_TIMESTAMP, 'MM/DD/YY HH24:MI:SS') CONVCREATEDDATE,
                   t_ConvUserKey CREATEDBYUSERLEGACYKEY,
                   t_ConvUserKey LASTUPDATEDBYUSERLEGACYKEY,
                   to_date(t.APP_CARD_TERM_START_DATE, 'MM/DD/YY HH24:MI:SS') ISSUEDATE,
                   to_date(t.APP_CARD_TERM_START_DATE, 'MM/DD/YY HH24:MI:SS') EFFECTIVEDATE,
                   to_date(t.APP_CARD_TERM_END_DATE, 'MM/DD/YY HH24:MI:SS') EXPIRATIONDATE,
                   'APP Card Fee: ' || t.APP_CARD_FEE || ' APP Card Tran No:' ||t.APP_CARD_TRAN_NO AdditionalPermitInformation,
                   case when to_date(APP_CARD_TERM_END_DATE, 'MM/DD/YY HH24:MI:SS') > sysdate then 'Active' else 'Closed' end STATE 
             from abc11p.ma_app_card_int_t t/*
            where t.processed is null*/) loop
    begin
    insert into dataconv.o_abc_permit a
    ( LEGACYKEY,
	  PERMITNUMBER,
      PERMITTYPELK,
      LASTUPDATEDDATE,
      CONVCREATEDDATE,
      CREATEDBYUSERLEGACYKEY,
      LASTUPDATEDBYUSERLEGACYKEY,
      permitteelk ,
      associatedpermitlk ,
      ISSUEDATE,
      EFFECTIVEDATE,
      EXPIRATIONDATE,
      AdditionalPermitInformation,
      STATE
      )
    values ( c.LEGACYKEY,
	         c.LEGACYKEY,
             c.PERMITTYPELK,
             c.LASTUPDATEDDATE,
             c.CONVCREATEDDATE,
             c.CREATEDBYUSERLEGACYKEY,
             c.LASTUPDATEDBYUSERLEGACYKEY,
             c.PERMITEELK,
             c.PERMITLK,
             c.ISSUEDATE,
             c.EFFECTIVEDATE,
             c.EXPIRATIONDATE,
             c.AdditionalPermitInformation,
             c.state );    
      
    t_count := t_count +1; 
    
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;