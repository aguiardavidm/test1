-- Created on 3/1/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
  t_ConversionNote long;
  t_RowsProcessed  number := 0;
  t_EmailsSet number := 0;
begin
  -- Test statements here
  for i in (select *
              from abc11p.lic_cmt c
             order by c.seq_num) loop
    t_ConversionNote := 'Converted LIC_CMT Information' ||
                        'Form ID: ' || i.form_id || chr(10) ||
                        'Text: ' ||i.cmt_text;
    --Create the Conversion Note
    insert into dataconv.n_general
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    parenttablename,
    parentlegacykey,
    locked,
    text
    )
    values
    (
    i.updt_oper_id,
    i.dt_row_updt,
    i.updt_oper_id,
    i.dt_row_updt,
    'O_ABC_LICENSE',
    i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
    'Y',
    t_ConversionNote
    );
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
  end loop;
  -- Test statements here
  for i in (select 'Converted NEGATIVE_ACTION Information' || chr(10) ||
                   'Date of Action: ' || N.DT_ACTION || chr(10) ||
                   'Docket Number: ' || N.DOCKET_NUM || chr(10) ||
                   'Penalty Imposed By: ' || N.PENALTY_IMPOSED_BY || chr(10) ||
                   'Fined?: ' || N.FINED_IND || chr(10) ||
                   'Fine Amount: ' || N.FINE_AMT || chr(10) ||
                   'Not Renewed?: ' || N.NOT_RENEWED_IND || chr(10) ||
                   'Suspended?: ' || N.SUSPENDED_IND || chr(10) ||
                   'Number of Days Suspended: ' || N.NUM_DAYS_SUSP || chr(10) ||
                   'Revoked?: ' || N.REVOKED_IND || chr(10) ||
                   'Cancelled?: ' || N.CANCELLED_IND || chr(10) ||
                   'Other?: ' || N.OTHER_IND ConvNote,
                   n.name_mstr_id_num,
                   n.cnty_muni_cd,
                   n.lic_type_cd,
                   n.muni_ser_num,
                   n.gen_num
--              into t_NoteConvNote
              from abc11p.negative_action n
             where exists (select 1 from dataconv.o_abc_license l where l.legacykey = n.cnty_muni_cd || n.lic_type_cd || n.muni_ser_num || n.gen_num)) loop
    if i.cnty_muni_cd || i.lic_type_cd || i.muni_ser_num || i.gen_num is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      'O_ABC_LICENSE',
      i.cnty_muni_cd || i.lic_type_cd || i.muni_ser_num || i.gen_num,
      'Y',
      i.Convnote
      );
    end if;
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
  end loop;
        
  for i in (select 'Converted OTH_BUS Information' || chr(10) ||
                   'Nature of Business: ' || (select d.cd_desc from abc11p.code_desc d where d.code = o.nat_bus_cd and id = 'NB') || chr(10) ||
                   'Operated By: ' || o.OPER_BY_IND || chr(10) ||
                   'Business To Be Operator: ' || o.BUS_TOBE_OPER || chr(10) ||
                   'Name: ' || o.NAME_MSTR_ID_NUM ConvNote,
                   o.name_mstr_id_num,
                   o.cnty_muni_cd,
                   o.lic_type_cd,
                   o.muni_ser_num,
                   o.gen_num
              from abc11p.oth_bus o
             where exists (select 1 from dataconv.o_abc_license l where l.legacykey = o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num)) loop
    if i.cnty_muni_cd || i.lic_type_cd || i.muni_ser_num || i.gen_num is not null then
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      'O_ABC_LICENSE',
      i.cnty_muni_cd || i.lic_type_cd || i.muni_ser_num || i.gen_num,
      'Y',
      i.Convnote
      );
    end if;
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
  end loop;
  
  for i in (select 'Converted OTH_LIC_INT Information' || chr(10) ||
                   'Other License Interest: ' || o.OTH_CNTY_MUNI_CD || o.OTH_LIC_TYPE_CD || o.OTH_MUNI_SER_NUM || o.OTH_GEN_NUM ConvNote,
                   o.cnty_muni_cd,
                   o.lic_type_cd,
                   o.muni_ser_num,
                   o.gen_num
              from abc11p.oth_lic_int o
             where exists (select 1 from dataconv.o_abc_license l where l.legacykey = o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num)) loop
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      'O_ABC_LICENSE',
      i.cnty_muni_cd || i.lic_type_cd || i.muni_ser_num || i.gen_num,
      'Y',
      i.Convnote
      );
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
  end loop;

  for i in (select 'Converted LIC_CRIME Information' || chr(10) ||
                   'DT_DISQ_REMOV: ' || DT_DISQ_REMOV || chr(10) ||
                   'DISQ_REMOV_NUM: ' || DISQ_REMOV_NUM ConvNote,
                   o.cnty_muni_cd,
                   o.lic_type_cd,
                   o.muni_ser_num,
                   o.gen_num
              from abc11p.lic_crime o
             where exists (select 1 from dataconv.o_abc_license l where l.legacykey = o.cnty_muni_cd || o.lic_type_cd || o.muni_ser_num || o.gen_num)) loop
      --Create the Conversion Note
      insert into dataconv.n_general
      (
      parenttablename,
      parentlegacykey,
      locked,
      text
      )
      values
      (
      'O_ABC_LICENSE',
      i.cnty_muni_cd || i.lic_type_cd || i.muni_ser_num || i.gen_num,
      'Y',
      i.Convnote
      );
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
  end loop;
  dbms_output.put_line(t_RowsProcessed || ' Rows Processed');
  commit;
  for i in (select e.primary_email, l.licenseelk
              from abc11p.lic_email e
              join dataconv.o_abc_license l on l.licensenumber = e.license_number) loop
    update dataconv.o_abc_legalentity
       set Contactemailaddress = i.primary_email,
           PreferredContactMethod = 'Email'
     where legacykey = i.licenseelk;
    t_EmailsSet := t_EmailsSet + 1;
  end loop;
  dbms_output.put_line('Set ' || t_EmailsSet || ' Emails.');
  commit;
end;