-- Created on 2/24/2015 by ADMINISTRATOR 
--expected run time: 5m
declare 
  -- Local variables here
  i                  integer;
  t_LETypeLK         number;
  t_DoingBusinessAs  varchar2(100);
  t_RowsProcessed    number := 0;
  t_RelRowsProcessed number := 0;
  type IdArrayType is table of number(9);
  LETypes IdArrayType;
  t_ConversionNote   long;
  t_NoteAppend       long;
  t_NameIdNum        varchar2(40);
  t_EmailsSet        number := 0;
         
begin
  execute immediate 'alter table DATACONV.R_ABC_PARTICIPATESININCLUDESLE modify relationshiptype VARCHAR2(200)';
  select let.objectid
    bulk collect into LETypes
    from query.o_abc_legalentitytype let
   where let.Name in ('An Individual',
                     'Incorporated Club',
                     'A Partnership',
                     'Business Corporation',
                     'Limited Liability Corporation',
                     'Unincorporated Club',
                     'Limited Partnership',
                     'Converted')
   order by let.Name;
  for i in (select  decode(nm.name_stat, 'C', 'Y', 'N') ACTIVE,
                    'N'                      EDIT,
                    case 
                      when
                        nm.name_type_cd = '2.1-LICENSEE' then
                          decode(lm.appl_filed_cd, 'PART', '1',
                                                   'IND',  '2',
                                                   'CORP', '3',
                                                   'ICLB', '5',
                                                   'LPRT', '7',
                                                   'LLC', '6',
                                                   'UCLB', '8', '4')
                      else
                        '4'
                    END legalentitytype,
                    nm.BUS_AREA_CD || nm.BUS_PREFIX || nm.BUS_SUFFIX CONTACTPHONENUMBER,
                    nm.MAIL_AREA_CD || nm.MAIL_PREFIX || nm.MAIL_SUFFIX CONTACTALTPHONENUMBER,
                    nm.HOME_AREA_CD || nm.HOME_PREFIX || nm.HOME_SUFFIX CONTACTFAXNUMBER,
                    (case when nm.name_type_cd = '2.1-LICENSEE' then
                      (select max(cm.name_mstr_id_num)
                         from abc11p.lic_codemaster cm
                        where substr(cm.abc_num, 1, 11) = substr(nm.abc_num, 1, 11))
                    end) OnlineAccessCode,
                    nm.*
              from abc11p.name_mstr nm
              left join abc11p.lic_mstr lm on lm.cnty_muni_cd||'-'||lm.lic_type_cd||'-'||lm.muni_ser_num||'-'||lm.gen_num = nm.abc_num
             where nm.name_type_cd in ('10A-INTEREST', '2.1-LICENSEE', '10.1-CORP', '10.10-AGENT', '8A-INTEREST', '6A-INTEREST', '8.1-CORP', '8.10-AGENT', '6.1-CORP', '6.10-AGENT', 'SOLICITOR')
               and nm.srce_grp_cd != 'GENERAL'
               and nm.name_stat != 'X'
               and not exists (select le.legacykey
                                 from dataconv.o_abc_legalentity le
                                where le.legacykey = to_char(nm.name_mstr_id_num))) loop
    insert into dataconv.o_abc_legalentity 
    (
    LEGACYKEY,
    CREATEDBYUSERLEGACYKEY,
    CONVCREATEDDATE,
    LASTUPDATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE,
    BIRTHDATE,
    SSNTIN,
    NJTaxAuthNumber,
    DOINGBUSINESSAS,
    CORPORATIONNUMBER,
    INCORPORATIONDATE,
    CONTACTNAME,
    LEGALNAME,
    CONTACTPHONENUMBER,
    CONTACTALTPHONENUMBER,
    CONTACTFAXNUMBER,
    PREFERREDCONTACTMETHOD,
    ACTIVE,
    EDIT,
    LEGALENTITYTYPELK,
    MAILINGADDRESSLK,
    PHYSICALADDRESSLK,
    ONLINEACCESSCODE,
    STATEOFINCORPORATION
    )
    values
    (
    i.name_mstr_id_num,
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    i.dt_birth,
    i.SSN_TIN,
    i.NJ_TAX_AUTH_NUM,
    t_DOINGBUSINESSAS,
    i.cert_inc_num,
    i.Dt_Corp_Chart,
    i.NAME,
    i.NAME,
    i.CONTACTPHONENUMBER,
    i.CONTACTALTPHONENUMBER,
    i.CONTACTFAXNUMBER,
    'Mail',
    i.ACTIVE,
    i.EDIT,
    LETypes(i.legalentitytype),
    i.abc11puk || 'MAIL',
    i.abc11puk || 'PREM',
    i.name_mstr_id_num,
    i.corp_chart_st
    );
    --Add Conversion Notes
    t_ConversionNote := 'Converted ' ||i.NAME_TYPE_CD || 'Information' || chr(10) ||
                        'VALID_CORP_IND: ' || i.VALID_CORP_IND || chr(10) ||
                        'CORP_CHART_ST: ' || i.CORP_CHART_ST || chr(10) ||
                        'CORP_NJ_AUTH: ' || i.CORP_NJ_AUTH || chr(10) ||
                        'CORP_NJ_REVOC_IND: ' || i.CORP_NJ_REVOC_IND;
    --Create the Conversion Note
    insert into dataconv.n_general
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    parenttablename,
    parentlegacykey,
    locked,
    text
    )
    values
    (
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    'O_ABC_LEGALENTITY',
    i.name_mstr_id_num,
    'Y',
    t_ConversionNote
    );
    t_ConversionNote := null;
    --Create the relationship for the corporate structure
      for c in (select i.name_mstr_id_num ParticipatesIn, pi.name_mstr_id_num Includes, pi.pct_bus_cntrl PercentageInterest, pi.num_shares_own CommonVotingShares, listagg(cd.cd_desc, '/') within group (order by pi.parent_id_number) RelationshipType, pi.name_type_cd
                  from abc11p.name_mstr pi
                  join abc11p.position p on p.name_mstr_id_num = pi.name_mstr_id_num
                  join abc11p.code_desc cd on cd.code = p.position_cd
                   and cd.id = 'PS'
                 where /*pi.name_type_cd in ('10A-INTEREST', '2.1-LICENSEE', '10.1-CORP', '10.10-AGENT', '8A-INTEREST', '6A-INTEREST', '8.1-CORP', '8.10-AGENT', '6.1-CORP', '6.10-AGENT')
                   and */(pi.parent_id_number = i.name_mstr_id_num
                       or (pi.abc_num = i.abc_num and i.name_mstr_id_num = (select max(nm.name_mstr_id_num)
                                                                              from abc11p.name_mstr nm
                                                                             where nm.abc_num = i.abc_num
                                                                               and nm.srce_grp_cd != 'GENERAL'
                                                                               and nm.name_stat != 'X'
                                                                               and nm.name_type_cd = '2.1-LICENSEE')))
                   and pi.srce_grp_cd != 'GENERAL'
                   and pi.name_stat != 'X'
                   and i.name_type_cd = '2.1-LICENSEE'
                   
                 group by Pi.Parent_Id_Number, pi.name_mstr_id_num, pi.pct_bus_cntrl, pi.num_shares_own, pi.name_type_cd) loop
if c.name_type_cd in ('10A-INTEREST', '2.1-LICENSEE', '10.1-CORP', '10.10-AGENT', '8A-INTEREST', '6A-INTEREST', '8.1-CORP', '8.10-AGENT', '6.1-CORP', '6.10-AGENT', 'SOLICITOR') then
        insert into dataconv.r_abc_participatesinincludesle
        (
        PARTICIPATESIN,
        INCLUDES,
        PERCENTAGEINTEREST,
        COMMONVOTINGSHARES,
        RELATIONSHIPTYPE
        )
        values
        (
        c.participatesin,
        c.includes,
        c.percentageinterest,
        c.commonvotingshares,
        c.RelationshipType
        );
        t_RelRowsProcessed := t_RelRowsProcessed +1;
end if;
      end loop;
    --Create a warning if needed
    if i.pos_title is not null then
      insert into dataconv.o_warning
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      LegacyKey,
      LegalEntityLK,
      Description
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      i.name_mstr_id_num,
      i.name_mstr_id_num,
      i.pos_title
      );
    end if;
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 1000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
      dbms_output.put_line('Processed ' || t_RelRowsProcessed || ' related LE rows.');
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
  dbms_output.put_line('Processed ' || t_RelRowsProcessed || ' related LE rows.');
  commit;
end;