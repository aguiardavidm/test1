declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'RetailTransitType');
begin
  for i in (select l.objectid, 
                   l.LicenseNumber, 
                   nvl((select distinct v.vehicletype
                      from dataconv.r_abc_licensevehicle r
                      join dataconv.o_abc_vehicle v on v.legacykey = r.o_abc_vehicle
                     where r.o_abc_license = l.legacykey), 'Converted') Type
              from dataconv.o_abc_license l
              join dataconv.o_abc_licensetype lt on lt.legacykey = l.Licensetypelk
             where lt.name = 'Plenary Retail Transit License'
               and l.licensenumber not in ('3401-13-806-001',
                                           '3400-13-338-001',
                                           '3400-13-284-001',
                                           '3400-13-308-001',
                                           '3401-13-693-001',
                                           '3400-13-285-002',
                                           '3402-13-802-001',
                                           '3401-13-756-001',
                                           '3403-13-593-001',
                                           '9903-13-849-001',
                                           '3400-13-844-001',
                                           '9904-13-136-001')) loop
    delete from possedata.objectcolumndata cd
     where cd.ColumnDefId = t_ColumnDefId
       and cd.objectid = i.objectid;
    insert /*append*/ into possedata.objectcolumndata_t
      (
      LogicalTransactionId,
      ObjectId,
      ColumnDefId,
      AttributeValue,
      AttributeValueId,
      SearchValue
      )
      values
      (
      1,
      i.ObjectId,
      t_ColumnDefId,
      (case when i.type = 'Converted' then i.Type end),
      (case when i.type != 'Converted' then (select dv.DomainValueId
         from api.domains d
         join api.domainlistofvalues dv on dv.DomainId = d.DomainId
        where d.name = 'ABC Retail Transit Type'
          and dv.AttributeValue = i.type) end),
      null
      );
  end loop;
  --Set based on spreadsheet
  for i in (select l.objectid, 
                   l.LicenseNumber,                                            
                  decode(l.licensenumber, '3401-13-806-001', 'Plane',
                                          '3400-13-338-001', 'Rail',
                                          '3400-13-284-001', 'Plane',
                                          '3400-13-308-001', 'Plane',
                                          '3401-13-693-001', 'Plane',
                                          '3400-13-285-002', 'Plane',
                                          '3402-13-802-001', 'Plane',
                                          '3401-13-756-001', 'Plane',
                                          '3403-13-593-001', 'Plane',
                                          '9903-13-849-001', 'Plane',
                                          '3400-13-844-001', 'Plane',
                                          '9904-13-136-001', 'Plane', 'Converted') Type
              from dataconv.o_abc_license l
              join dataconv.o_abc_licensetype lt on lt.legacykey = l.Licensetypelk
             where lt.name = 'Plenary Retail Transit License'
               and l.licensenumber in ('3401-13-806-001',
                                           '3400-13-338-001',
                                           '3400-13-284-001',
                                           '3400-13-308-001',
                                           '3401-13-693-001',
                                           '3400-13-285-002',
                                           '3402-13-802-001',
                                           '3401-13-756-001',
                                           '3403-13-593-001',
                                           '9903-13-849-001',
                                           '3400-13-844-001',
                                           '9904-13-136-001')) loop
    delete from possedata.objectcolumndata cd
     where cd.ColumnDefId = t_ColumnDefId
       and cd.objectid = i.objectid;
    insert /*append*/ into possedata.objectcolumndata_t
      (
      LogicalTransactionId,
      ObjectId,
      ColumnDefId,
      AttributeValue,
      AttributeValueId,
      SearchValue
      )
      values
      (
      1,
      i.ObjectId,
      t_ColumnDefId,
      null,
      (select dv.DomainValueId
         from api.domains d
         join api.domainlistofvalues dv on dv.DomainId = d.DomainId
        where d.name = 'ABC Retail Transit Type'
          and dv.AttributeValue = i.type),
      null
      );
  end loop;
end;