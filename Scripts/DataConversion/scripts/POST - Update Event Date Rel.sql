-- Created on 5/4/2015 by KEATON 
declare 
  -- Event Date variables here
  t_EventRelDef              number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitEventDate');
  t_EventRelId               number;
  t_EventDateEndPointId      number := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','EventDate');
  t_PermitEventEndPointId    number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_Permit','EventDate');
  --Rain Date Variables here
  t_RainRelDef              number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitRainDate');
  t_RainRelId               number;
  t_RainDateEndPointId      number := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','RainDate');
  t_PermitRainEndPointId    number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_Permit','RainDate');
  
  
begin


-- Loop through the event dates and relate the correct permit as a rain date or event date relationship.

for c in (select ed.objectid EventDateId, 
			   (select p.objectid
				 from dataconv.o_abc_permit p
				where p.legacykey = TO_CHAR(pe.perm_num)) PermitId,
			   'Event' DateType
		  from dataconv.o_abc_eventdate ed
		  join abc11p.perm_event pe on ed.legacykey = 'E' || pe.PERM_NUM || pe.DT_EVENT || pe.TIME_START || pe.TIME_END
		union all  
		select ed.objectid , 
			   (select p.objectid
				 from dataconv.o_abc_permit p
				where p.legacykey = TO_CHAR(pR.perm_num)) PermitId, 
				'Rain'
		  from dataconv.o_abc_eventdate ed
		  join abc11p.perm_rain pr on ed.legacykey = 'R' || pr.PERM_NUM || pr.DT_EVENT || pr.TIME_START || pr.TIME_END  ) loop

if c.PermitId is not null then --Make sure Permit Exists
	if c.DateType = 'Event' then
	select possedata.ObjectId_s.nextval
	  into t_EventRelId 
	 from dual;

	--Insert into ObjModelPhys.Objects
	insert into objmodelphys.Objects (
		  LogicalTransactionId,
		  CreatedLogicalTransactionId,
		  ObjectId,
		  ObjectDefId,
		  ObjectDefTypeId,
		  ClassId,
		  InstanceId,
		  EffectiveStartDate,
		  EffectiveEndDate,
		  ConfigReadSecurityClassId,
		  ConfigReadSecurityInstanceId,
		  ObjectReadSecurityClassId,
		  ObjectReadSecurityInstanceId
		) values (
		  1, -- Hard coded logical Transaction to 1
		  1, -- Hard coded logical Transaction to 1
		  t_EventRelId,
		  t_EventRelDef,
		  4,
		  4,
		  t_EventRelDef,
		  null,
		  null,
		  4,
		  t_EventRelDef,
		  null,
		  null
		);

	 
	--Insert Into Rel.StoredRelationships    

		insert into rel.StoredRelationships (
		  RelationshipId,
		  EndPointId,
		  FromObjectId,
		  ToObjectId
		) values (
		  t_EventRelId,
		  t_EventDateEndPointId,
		  c.PermitId,
		  c.EventDateId  
		);

		insert into rel.StoredRelationships (
		  RelationshipId,
		  EndPointId,
		  FromObjectId,
		  ToObjectId
		) values (
		  t_EventRelId,
		  t_PermitEventEndPointId,
		  c.EventDateId,
		  c.PermitId
		); 
	end if;

	if c.DateType = 'Rain' then
	select possedata.ObjectId_s.nextval
	  into t_RainRelId 
	 from dual;

	--Insert into ObjModelPhys.Objects
	insert into objmodelphys.Objects (
		  LogicalTransactionId,
		  CreatedLogicalTransactionId,
		  ObjectId,
		  ObjectDefId,
		  ObjectDefTypeId,
		  ClassId,
		  InstanceId,
		  EffectiveStartDate,
		  EffectiveEndDate,
		  ConfigReadSecurityClassId,
		  ConfigReadSecurityInstanceId,
		  ObjectReadSecurityClassId,
		  ObjectReadSecurityInstanceId
		) values (
		  1, -- Hard coded logical Transaction to 1
		  1, -- Hard coded logical Transaction to 1
		  t_RainRelId,
		  t_RainRelDef,
		  4,
		  4,
		  t_RainRelDef,
		  null,
		  null,
		  4,
		  t_RainRelDef,
		  null,
		  null
		);

	 
	--Insert Into Rel.StoredRelationships    

		insert into rel.StoredRelationships (
		  RelationshipId,
		  EndPointId,
		  FromObjectId,
		  ToObjectId
		) values (
		  t_RainRelId,
		  t_RainDateEndPointId,
		  c.PermitId,
		  c.EventDateId  
		);

		insert into rel.StoredRelationships (
		  RelationshipId,
		  EndPointId,
		  FromObjectId,
		  ToObjectId
		) values (
		  t_RainRelId,
		  t_PermitRainEndPointId,
		  c.EventDateId,
		  c.PermitId
		); 
	end if;
end if; --End Make sure Permit Exists



end loop;
  
end;
