declare 
  -- Local variables here
  t_DynamicUpdate varchar2(4000);
begin
  -- Test statements here
  
  for c in( 
     select 'alter table abc11p.' || t.TABLE_NAME || ' add (ABC11PUK number) ' DynamicUpdate, t.Table_Name          
       from dba_tables t 
      where (t.owner = 'ABC11P'
        and t.tablename != 'ADDRESS_MSTR')) loop
        begin
    
        EXECUTE IMMEDIATE c.Dynamicupdate;
        dbms_output.put_line('Table Name: ' || c.Table_Name);
        exception when others then
        dbms_output.put_line(c.Dynamicupdate);
        end;
    end loop;
end;

create sequence abc11p.ABC11PUKS
  start with 1
  increment by 1
  cache 10000;

declare 
  -- Local variables here
  t_DynamicUpdate varchar2(4000);
begin
  -- Test statements here
  
  for c in( 
      select ('update ' || dtc.owner || '.'||dtc.TABLE_NAME || ' gt ' 
       || ' set gt.' || dtc.COLUMN_NAME ||'='|| 'abc11p.ABC11PUKS.NEXTVAL' )DynamicUpdate,
         dtc.TABLE_NAME,
         dtc.COLUMN_NAME           
    from dba_tab_cols dtc
    join dba_tables t on t.TABLE_NAME = dtc.TABLE_NAME
  where dtc.COLUMN_NAME = 'ABC11PUK') loop
        begin
    
        EXECUTE IMMEDIATE c.Dynamicupdate;
        dbms_output.put_line('Table Name: ' || c.Table_Name || ' - ' || c.Column_Name || ' - ' || sql%rowcount);
        exception when others then
        dbms_output.put_line(c.Dynamicupdate);
        end;
    end loop;
end;

--create some indexes to help performance
create index O_ABC_ESTABLISHMENT_IX08
on dataconv.o_Abc_Establishment (LEGACYKEY);
create index O_ABC_ESTABLISHMENTHISTOR_IX06
on dataconv.o_Abc_Establishmenthistory (LEGACYKEY);
create index I9_NAME_MSTR
on abc11p.name_mstr (ABC_NUM);