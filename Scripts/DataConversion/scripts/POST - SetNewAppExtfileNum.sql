declare
  t_SequenceId number;
  t_ExternalFileNum varchar2(40);
begin

  api.pkg_logicaltransactionupdate.ResetTransaction;

  select s.SequenceId
    into t_SequenceId
    from api.sequences s
   where s.Description = 'ABC Job Number';

  for i in (select j.jobid
              from query.j_abc_newapplication j
              where j.ExternalFileNum is null) loop
    t_ExternalFileNum := common.pkg_objectupdate.generateexternalfilenum('[9999999]', t_SequenceId);

    update objmodelphys.Objects set
      LogicalTransactionId = admin.pkg_Control.CurrentTransaction(),
      ExternalFileNum = t_ExternalFileNum
    where ObjectId = i.jobid;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;