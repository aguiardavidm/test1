begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select l.FormattedName, l.active, l.LegalEntityType, f.old_id oldobjid, f.new_id newobjid, f.relatetolic, f.licnum
              from dataconv.FixScenarioTwoReg f
              join query.o_abc_legalentity l on l.ObjectId = f.old_id
             where f.new_id is not null
               and f.licnum is null) loop
    api.pkg_columnupdate.SetValue(i.oldobjid, 'Active', 'N');
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;