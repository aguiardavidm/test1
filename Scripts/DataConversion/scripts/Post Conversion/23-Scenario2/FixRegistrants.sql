update dataconv.fixscenariotworeg f
   set RelateToLic = 'N'
 where (f.new_id in (27764535,27362007,27753168,27363512)
        or f.old_id in (27764535,27362007,27753168,27363512));
commit;
/
declare
  t_LicenseIds       api.pkg_definition.udt_IdList;
  t_LicenseId        number;
  t_ProductIds       api.pkg_definition.udt_IdList;
  t_EndpointId       number := api.pkg_configquery.endpointidforname('o_ABC_Product', 'RegistrantLic');
  t_OthEndpointId    number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_Product', 'RegistrantLic');
  t_RelDef           number := api.pkg_configquery.ObjectDefIdForName('r_ABCProduct_RegistrantLicense');
  t_RelId            number;
  t_LicenseRelExists number := 0;
  t_DupColumnDefId   number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Product', 'dup_RegistrantObjectId');
  t_LECount          number := 0;
  t_LicRelCount      number := 0;
  t_DupFixCount      number := 0;
  t_RegistrantId     number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select f.old_id oldobjid, f.new_id newobjid, f.relatetolic, (select max(t.licnum) from dataconv.fixscenariotworeg t where t.old_id = f.new_id) LicNum
              from dataconv.FixScenarioTwoReg f
             where f.processed = 'N') loop
    --Create rel to license
    if i.relatetolic = 'Y' then
      if i.licnum is not null and i.licnum != '9903-16-938-001' then
        t_LicenseIds := api.pkg_simplesearch.ObjectsByIndex('o_ABC_License', 'LicenseNumber',i.licnum);
        if t_LicenseIds.count > 1 then
          for n in 1 .. t_LicenseIds.count loop
            if api.pkg_columnquery.Value(t_LicenseIds(n), 'State') = 'Active' then
              t_LicenseId := t_LicenseIds(n);
            end if;
          end loop;
        end if;
        for c in (select r.RelationshipId, r.ProductObjectId
                    from query.r_ABC_ProductRegistrant r
                   where r.RegistrantObjectId = i.oldobjid) loop
          select count(1)
            into t_LicenseRelExists
            from query.r_ABCProduct_RegistrantLicense l
           where l.ProductId = c.productobjectid;
          if t_LicenseRelExists = 0 then
            --Create the Rel
            select possedata.ObjectId_s.nextval
              into t_RelId 
             from dual;

            insert into objmodelphys.Objects (
              LogicalTransactionId,
              CreatedLogicalTransactionId,
              ObjectId,
              ObjectDefId,
              ObjectDefTypeId,
              ClassId,
              InstanceId,
              EffectiveStartDate,
              EffectiveEndDate,
              ConfigReadSecurityClassId,
              ConfigReadSecurityInstanceId,
              ObjectReadSecurityClassId,
              ObjectReadSecurityInstanceId
            ) values (
              api.pkg_logicaltransactionupdate.CurrentTransaction(),
              api.pkg_logicaltransactionupdate.CurrentTransaction(),
              t_RelId,
              t_RelDef,
              4,
              4,
              t_RelDef,
              null,
              null,
              4,
              t_RelDef,
              null,
              null
            );

            insert into rel.StoredRelationships (
              RelationshipId,
              EndPointId,
              FromObjectId,
              ToObjectId
            ) values (
              t_RelId,
              t_EndpointId,
              c.productobjectid,
              t_LicenseId
            );
            insert into rel.StoredRelationships (
              RelationshipId,
              EndPointId,
              FromObjectId,
              ToObjectId
            ) values (
              t_RelId,
              t_OthEndpointId,
              t_LicenseId,
              c.productobjectid
            );
            t_LicRelCount := t_LicRelCount + 1;
          end if;
        end loop;
      end if;
    end if;
    --move product rels
    for c in (select r.RelationshipId
                from query.r_ABC_ProductRegistrant r
               where r.RegistrantObjectId = i.oldobjid) loop
      if i.newobjid is not null then
        --Swap relationship ids
        update rel.storedrelationships s
           set s.FromObjectId = i.newobjid
         where s.RelationshipId = c.relationshipid
           and s.FromObjectId = i.oldobjid;
        update rel.storedrelationships s
           set s.ToObjectId = i.newobjid
         where s.RelationshipId = c.relationshipid
           and s.ToObjectId = i.oldobjid;
        t_LECount := t_LEcount + 1;
      end if;
    end loop;
    if i.newobjid is not null then
      and i.licnum is null then
      --set the old LE to inactive
      api.pkg_columnupdate.SetValue(i.oldobjid, 'Active', 'N');
      update dataconv.fixscenariotworeg d
         set d.Processed = 'Y'
       where d.old_id = i.oldobjid;
    end if;
  end loop;
  dbms_output.put_line('Products moved to new LE: ' || t_LECount);
  dbms_output.put_line('Products related to License: ' || t_LicRelCount);
  
  --Set dup_RegistrantObjectId on all products (faster than filtering)
  select p.ObjectId
    bulk collect into t_ProductIds
    from query.o_abc_product p;
  for i in 1 .. t_ProductIds.count loop
    t_RegistrantId := api.pkg_columnquery.value(t_ProductIds(i), 'RegistrantObjectId');
    update possedata.objectcolumndata_t cd
       set cd.AttributeValue = t_RegistrantId,
           cd.numericsearchvalue = t_RegistrantId
     where cd.ObjectId = t_ProductIds(i)
       and cd.ColumnDefId = t_DupcolumnDefId;
    t_DupFixCount := t_DupFixCount + 1;
--    commit;
  end loop;
  dbms_output.put_line('Dups fixed: ' || t_DupFixcount);
  api.pkg_logicaltransactionupdate.EndTransaction;
--  commit;
end;
