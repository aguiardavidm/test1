--Create table for easy access to the data to be fixed
--drop table dataconv.FixEstabDBANames; --uncomment to run the full thing again
create table dataconv.FixEstabDBANames as (
select e.objectid, nm.abc_num,
       coalesce((select nm2.name 
          from abc11p.name_mstr nm2 
         where nm2.abc_num = nm.abc_num 
           and nm2.name_mstr_id_num = (select min(nm3.name_mstr_id_num) 
                                         from abc11p.name_mstr nm3 
                                        where nm3.abc_num = nm2.abc_num
                                          and nm3.name_type_cd IN ('2.5-D/B/A','2.6-D/B/A')
                                          and nm3.name_stat = 'C')
           and nm2.name_type_cd IN ('2.5-D/B/A','2.6-D/B/A')),
       (select nm2.name 
          from abc11p.name_mstr nm2 
         where nm2.abc_num = nm.abc_num 
           and nm2.name_mstr_id_num = (select max(nm3.name_mstr_id_num) 
                                         from abc11p.name_mstr nm3 
                                        where nm3.abc_num = nm2.abc_num
                                          and nm3.name_type_cd IN ('2.5-D/B/A','2.6-D/B/A'))
           and nm2.name_type_cd IN ('2.5-D/B/A','2.6-D/B/A')),
           'Not Available') DoingBusinessAs,
         cast('N' as varchar2(1)) Processed
  from dataconv.o_abc_establishment e
  join abc11p.name_mstr nm on to_char(nm.name_mstr_id_num) = e.legacykey
 where nm.name_type_cd = '2.1-LICENSEE');
alter table dataconv.FixEstabDBANames
add constraint FixEstabId primary key (ObjectId);
/

declare
  t_DBADefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Establishment', 'DoingBusinessAs');
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *
              from dataconv.FixEstabDBANames f
             where f.processed = 'N') loop
    update possedata.objectcolumndata_t cd
       set cd.AttributeValue = i.doingbusinessas,
           cd.SearchValue = lower(substrb(i.doingbusinessas, 1, 20))
     where cd.ObjectId = i.objectid
       and cd.ColumnDefId = t_DBADefId;
    update dataconv.FixEstabDBANames f
       set f.processed = 'Y'
     where f.objectid = i.objectid;
    dbms_output.put_line(i.objectid);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
  commit;
end;