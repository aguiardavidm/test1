declare
  t_Columndefid number := api.pkg_configquery.columndefidforname('o_ABC_Permit', 'State');
begin
  for i in (select d.objectid, d.permittypelk, d.expirationdate
              from dataconv.o_abc_permit d
            where d.permittypelk = (select objectid from query.o_abc_permittype where code = 'COOP')) loop
    if i.expirationdate > sysdate then
      update possedata.objectcolumndata cd
         set cd.AttributeValueId = (select dv.domainvalueid
                                      from api.domains do
                                      join api.domainlistofvalues dv on dv.DomainId = do.DomainId
                                     where do.Name = 'ABC Permit State'
                                       and dv.AttributeValue = 'Active')
       where cd.ColumnDefId = t_ColumndefId
         and cd.ObjectId = i.objectid;
    end if;
    if i.expirationdate < sysdate then
      update possedata.objectcolumndata cd
         set cd.AttributeValueId = (select dv.domainvalueid
                                      from api.domains do
                                      join api.domainlistofvalues dv on dv.DomainId = do.DomainId
                                     where do.Name = 'ABC Permit State'
                                       and dv.AttributeValue = 'Closed')
       where cd.ColumnDefId = t_ColumndefId
         and cd.ObjectId = i.objectid;
    end if;
  end loop;
  commit;
end;