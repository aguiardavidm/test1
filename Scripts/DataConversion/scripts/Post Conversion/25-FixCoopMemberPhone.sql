declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'ContactPhoneNumber');
begin
  for i in (select l.objectid, l.contactphonenumber, l.legacykey, c.phone_area_cd || c.phone_prefix || c.phone_suffix FullPhone
              from dataconv.o_Abc_Legalentity l
              join abc11p.coop c on c.coop_num || 'COOP' = l.legacykey) loop
    update possedata.objectcolumndata d
       set d.AttributeValue = i.fullphone,
           d.SearchValue = i.fullphone
     where d.ObjectId = i.objectid
       and d.ColumnDefId = t_ColumnDefId;
  end loop;
end;