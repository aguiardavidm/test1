declare
  t_EndpointId    number := api.pkg_configquery.endpointidforname('j_ABC_Appeal', 'AttorneyGeneral');
  t_OthEndpointId number;-- := api.pkg_configquery.OtherEndPointIdForName('', '');
  t_RelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_AppealDeputyAttorneyGen');
  t_RelId         number;
  t_DAGUserId     number;
begin

select rd.ToEndPointId
  into t_OthEndpointId
  from api.relationshipdefs rd
 where rd.FromEndPointId = api.pkg_configquery.endpointidforname('j_ABC_Appeal', 'AttorneyGeneral');
 
select u.ObjectId
  into t_DAGUserId
  from query.u_users u
 where u.AuthenticationName = 'OPS$DAGSBK';

for i in (select a.ObjectId AppealId
            from query.j_abc_appeal a
            join dataconv.j_abc_appeal d on d.objectid = a.ObjectId
           where not exists (select 1
                               from query.r_ABC_AppealDeputyAttorneyGen r
                              where r.AppealId = a.ObjectId)) loop

    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.appealid,
      t_DAGUserId
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      t_DAGUserId,
      i.appealid
    );
  end loop;
  commit;
end;