rem PL/SQL Developer Test Script

set feedback off
set autoprint off

rem Execute PL/SQL Block
-- Created on 6/2/2015 by JOSHUA.LENON 
declare 
  -- Local variables here
  t_MuniSiteId         number;
  t_PubSiteId          number;
  t_RelId              number;
  t_VideoId1           number;
  t_VideoId2           number;
  t_VideoId3           number;
  t_VideoId4           number;
  t_VideoId5           number;
  t_VideoId6           number;
  t_VideoId7           number;
  t_VideoId8           number;
  t_VideoId9           number;
  t_VideoId10          number;
  t_VideoId11          number;
  t_VideoId12          number;
  
begin
  -- Begin Transaction
  api.pkg_logicaltransactionupdate.ResetTransaction;
  
  -- Create Municipal Videos
    t_VideoId1 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId1, 'VideoTitle', 'Process a Retail License Renewal');
       api.pkg_columnupdate.setvalue(t_VideoId1, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/G5QTXDVkiD4" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId1, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId1, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId1, 'IsRegistrationVideo', 'N');      
       
    t_VideoId2 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId2, 'VideoTitle', 'Process a Retail License Amendment/Transfer');
       api.pkg_columnupdate.setvalue(t_VideoId2, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/iMibxqIkYaI" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId2, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId2, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId2, 'IsRegistrationVideo', 'N');  
       
    t_VideoId3 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId3, 'VideoTitle', 'Search for a specific application');
       api.pkg_columnupdate.setvalue(t_VideoId3, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/PToZvc8G_sA" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId3, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId3, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId3, 'IsRegistrationVideo', 'N');
       
    t_VideoId4 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId4, 'VideoTitle', 'Print an Application Data Summary report, including supporting documents');
       api.pkg_columnupdate.setvalue(t_VideoId4, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/bWkeA5STdtc" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId4, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId4, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId4, 'IsRegistrationVideo', 'N');
       
    t_VideoId5 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId5, 'VideoTitle', 'Print a Retail License certificate');
       api.pkg_columnupdate.setvalue(t_VideoId5, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/IE1kOvkgJMI" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId5, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId5, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId5, 'IsRegistrationVideo', 'N');              
       
  -- Create the Municipal Web Site
  
    t_MuniSiteId := api.pkg_objectupdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_WebSite'));
    
    -- populate web site name
      api.pkg_columnupdate.setvalue(t_MuniSiteId, 'SiteName', 'Municipal');
    
    -- Create rels between Municipal Site and Municipal Videos
    
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_MuniSiteId, t_VideoId1);
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_MuniSiteId, t_VideoId2);
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_MuniSiteId, t_VideoId3);
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_MuniSiteId, t_VideoId4);
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_MuniSiteId, t_VideoId5); 
 
    
  -- Create Public Videos      
    t_VideoId6 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId6, 'VideoTitle', 'Apply to renew my License');
       api.pkg_columnupdate.setvalue(t_VideoId6, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/hKJdPXkvzpM" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId6, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId6, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId6, 'IsRegistrationVideo', 'N');  
       
    t_VideoId7 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId7, 'VideoTitle', 'Pay for more than one renewal in a single payment');
       api.pkg_columnupdate.setvalue(t_VideoId7, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/jIu3SrxD2ak" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId7, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId7, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId7, 'IsRegistrationVideo', 'N');
       
    t_VideoId8 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId8, 'VideoTitle', 'Print an Application Data Summary report, including supporting documents');
       api.pkg_columnupdate.setvalue(t_VideoId8, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/ysymmu4sx_k" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId8, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId8, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId8, 'IsRegistrationVideo', 'N');
       
    t_VideoId9 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId9, 'VideoTitle', 'Print a Wholesale License certificate or Permit report');
       api.pkg_columnupdate.setvalue(t_VideoId9, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/C0GLYpi7W-I" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId9, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId9, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId9, 'IsRegistrationVideo', 'N');  
       
    t_VideoId10 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId10, 'VideoTitle', 'Print a Retail Permit report');
       api.pkg_columnupdate.setvalue(t_VideoId10, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/b9HJmjAeTVA" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId10, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId10, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId10, 'IsRegistrationVideo', 'N');       
       
  -- Create the registration video
    t_VideoId11 := api.pkg_ObjectUpdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_Video'));
    
       api.pkg_columnupdate.setvalue(t_VideoId11, 'VideoTitle', 'Register to use the system and associate my ID with an existing license');
       api.pkg_columnupdate.setvalue(t_VideoId11, 'VideoLink', '<iframe width="560" height="315" src="https://www.youtube.com/embed/3GILXubtEww" frameborder="0" allowfullscreen></iframe>');
       api.pkg_columnupdate.setvalue(t_VideoId11, 'VideoDate', sysdate);
       api.pkg_columnupdate.setvalue(t_VideoId11, 'VideoActive', 'Y');
       api.pkg_columnupdate.setvalue(t_VideoId11, 'IsRegistrationVideo', 'Y'); 
       
  -- Create the Public Web Site
  
    t_PubSiteId := api.pkg_objectupdate.new(api.pkg_configquery.ObjectDefIdForName('o_ABC_WebSite'));
    
    -- populate web site name
      api.pkg_columnupdate.setvalue(t_PubSiteId, 'SiteName', 'Public');
    
    -- Create rels between Municipal Site and Public Videos
    
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_PubSiteId, t_VideoId6);
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_PubSiteId, t_VideoId7);
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_PubSiteId, t_VideoId8);
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_PubSiteId, t_VideoId9);
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_PubSiteId, t_VideoId10); 
     t_RelId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('o_ABC_WebSite', 'Video'), t_PubSiteId, t_VideoId11);                                  
    
  api.pkg_logicaltransactionupdate.EndTransaction;
    
  commit;
  
end;
/
