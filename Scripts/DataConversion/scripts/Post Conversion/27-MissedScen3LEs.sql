--Oracle data if needed.  no need to run.
/*select le.legalname, nml.legalname, d.dist_stat,
       d.cnty_muni_cd || '-' || d.lic_type_cd || '-' || d.muni_ser_num || '-' || d.gen_num DistLicNum
      -- ,nm.abc_num LicNum
  from dataconv.o_abc_legalentity le
  join abc11p.brn_reg_name b on to_char(b.reg_name_mstr_id_num) = le.legacykey
  join abc11p.brn_reg p on p.reg_name_mstr_id_num = b.reg_name_mstr_id_num
  join abc11p.brn_dist d on d.brn_reg_num = p.brn_reg_num
  join abc11p.name_mstr nm on nm.abc_num like ('%' || d.cnty_muni_cd || '-' || d.lic_type_cd || '-' || d.muni_ser_num || '%')
    and nm.name_type_cd = '2.1-LICENSEE'
  join dataconv.o_abc_legalentity nml on nml.legacykey = to_char(nm.name_mstr_id_num)
 where d.gen_num is null
   and api.pkg_columnquery.Value(nml.objectid, 'Active') = 'Y'
   and d.dist_stat = 'C'
   and nml.legalname = le.legalname
  group by le.legalname, nml.legalname, d.dist_stat,
       d.cnty_muni_cd || '-' || d.lic_type_cd || '-' || d.muni_ser_num || '-' || d.gen_num
      -- ,nm.abc_num 
  order by 1;

*/--drop table dataconv.fixmissedscenthreereg;
--create table to pull data from (est 2 mins)
create table dataconv.fixmissedscenthreereg as (
select x.*, api.pkg_columnquery.value(l.objectid, 'State') as State,  'N' Processed, cast(null as date) ProcessedDate
  from (
select pr.RegistrantObjectId OldObjId, ll.LegalEntityObjectId NewObjId, --le.legacykey,
       api.pkg_columnquery.Value(pr.RegistrantObjectId, 'LegalEntityType') fromletype,
       api.pkg_columnquery.Value(ll.LegalEntityObjectId, 'LegalEntityType') toletype,
       api.pkg_columnquery.Value(pr.RegistrantObjectId, 'LegalName') lename,
       api.pkg_columnquery.Value(ll.LegalEntityObjectId, 'LegalName') NewLeName,
       api.pkg_columnquery.Value(ll.LicenseObjectId, 'LicenseNumber') LicNum,
       min(api.pkg_columnquery.Value(ll.LicenseObjectId, 'State')) MinState
  from query.r_ABC_ProductRegistrant pr
  join query.r_ABCProduct_Distributor pd on pd.ProductId = pr.ProductObjectId
  join query.r_ABC_LicenseLicenseeLE ll on ll.LicenseObjectId = pd.LicenseId
  join dataconv.o_abc_legalentity le on le.objectid = pr.RegistrantObjectId
  join abc11p.brn_reg_name b on to_char(b.reg_name_mstr_id_num) = le.legacykey
 where pr.RegistrantObjectId not in (select f.oldobjid from dataconv.fixregistrantrels f)
   and api.pkg_columnquery.Value(ll.LegalEntityObjectId, 'Active') = 'Y'
   and pr.RegistrantObjectId != ll.LegalEntityObjectId
   and api.pkg_columnquery.Value(ll.LegalEntityObjectId, 'LegalName') = api.pkg_columnquery.Value(pr.RegistrantObjectId, 'LegalName')
  group by api.pkg_columnquery.Value(pr.RegistrantObjectId, 'LegalName'),
       api.pkg_columnquery.Value(pr.RegistrantObjectId, 'LegalEntityType'),
       api.pkg_columnquery.Value(le.objectid, 'LegalEntityType'),
       api.pkg_columnquery.Value(ll.LegalEntityObjectId, 'LegalName'),
       pr.RegistrantObjectId, ll.LegalEntityObjectId, le.legacykey,
       api.pkg_columnquery.Value(ll.LicenseObjectId, 'LicenseNumber')
--       api.pkg_columnquery.Value(ll.LicenseObjectId, 'State')
  order by 3) x
  left join query.o_abc_license l on l.LicenseNumber = x.licnum
    and l.State = 'Active');
/
--copied from original fix script, jsut changed the names
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *
              from dataconv.fixmissedscenthreereg f
             where f.processed = 'N') loop
    --move all rels
    for c in (select r.RelationshipId
                from query.r_ABC_ProductRegistrant r
               where r.RegistrantObjectId = i.oldobjid) loop
      --Swap relationship ids
      update rel.storedrelationships s
         set s.FromObjectId = i.newobjid
       where s.RelationshipId = c.relationshipid
         and s.FromObjectId = i.oldobjid;
      update rel.storedrelationships s
         set s.ToObjectId = i.newobjid
       where s.RelationshipId = c.relationshipid
         and s.ToObjectId = i.oldobjid;
    end loop;
  --set the old LE to inactive
  api.pkg_columnupdate.SetValue(i.oldobjid, 'Active', 'N');
  update dataconv.fixmissedscenthreereg d
     set d.Processed = 'Y', d.Processeddate = sysdate
   where d.OldObjId = i.oldobjid;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;