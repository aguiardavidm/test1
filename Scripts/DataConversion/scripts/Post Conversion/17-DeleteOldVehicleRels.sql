create table possedba.old_vehicles
  as (select api.pkg_columnquery.Value(t.objectid,'LicenseNumber') LicenseNumber,
               r1.VehicleId,
               r1.RelationshipId
                from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ABC_License', 'State', 'Active')as api.udt_ObjectList)) t
                join query.r_ABC_LicenseVehicle r1
                  on r1.LicenseId = t.objectid
                join dataconv.o_abc_vehicle av
                  on av.objectid = r1.VehicleId
                join abc11p.lic_veh_ves v
                  on v.abc11puk = av.legacykey
               where (v.veh_stat = 'H'
                   or v.ves_stat= 'H'));
           
commit;           

/
begin
  -- Test statements here
  
  for c in (select ov.licensenumber,
                   ov.vehicleid,
                   ov.relationshipid
              from possedba.old_vehicles ov) loop
   
  for i in (select sr.RelationshipId
                from rel.storedrelationships sr
               where sr.FromObjectId = c.vehicleid) loop
  
  delete from rel.storedrelationships r
    where r.RelationshipId = i.relationshipid;   
  
  delete from possedata.objects o
    where o.ObjectId = i.relationshipid;
    
  end loop; 
     
  end loop;   
commit;     
end;
/
begin
  -- Test statements here
  
  for c in (select ov.licensenumber,
                   ov.vehicleid,
                   ov.relationshipid
              from possedba.old_vehicles ov) loop
   
    
  delete from possedata.objects o
    where o.ObjectId = c.vehicleid;  
 end loop;    
 commit;
end;
