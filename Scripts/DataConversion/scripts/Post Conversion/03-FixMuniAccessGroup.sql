begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select objectid
            from query.o_abc_office o
           where o.name != 'ZZZ') loop
    abc.pkg_ABC_Municipality.AssignAccessGroup(i.objectid, sysdate);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;