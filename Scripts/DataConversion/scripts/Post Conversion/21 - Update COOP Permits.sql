-- Created on 5/4/2015 by KEATON 
declare 
  -- Local variables here
  t_OldRelationshipDef          number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitCoOpOfficer');
  t_NewRelationshipDef          number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitPermittee');
  t_RelationshipId           number;
  t_ConvertedTypeObjId       number;
  t_PermiteeEndPoint         number := api.pkg_configquery.EndPointIdForName('o_abc_permit','Permittee');
  t_CoopEndPoint             number := api.pkg_configquery.OtherEndPointIdForName('o_abc_permit','Permittee');
  
  
begin



-- Loop through relationships and delete from the tables and create a new object.

for c in (select *
      from query.r_ABC_PermitCoOpOfficer)loop


delete from rel.storedrelationships sr
  where sr.RelationshipId = c.relationshipid;
delete from objmodelphys.objects o 
  where o.ObjectId = c.relationshipid;  
   

select possedata.ObjectId_s.nextval
  into t_RelationshipId 
 from dual;

--Insert into ObjModelPhys.Objects
insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelationshipId,
      t_NewRelationshipDef,
      4,
      4,
      t_NewRelationshipDef,
      null,
      null,
      4,
      t_NewRelationshipDef,
      null,
      null
    );

 
--Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipId,
      t_PermiteeEndPoint,
      c.CoOpOfficer_PermitId,
    c.CoOpOfficerID
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipId,
      t_CoopEndPoint,
      c.CoOpOfficerID,
    c.CoOpOfficer_PermitId
    ); 


end loop;
  
end;
