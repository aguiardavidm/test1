declare 
  t_ColumnDefId   number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Address', 'Country');
  t_DomainValueId number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  select dv.DomainValueId
    into t_DomainValueId 
    from api.domains d
    join api.domainlistofvalues dv on dv.DomainId = d.DomainId
   where d.Name = 'Country'
     and dv.AttributeValue = 'USA';
  for i in (select a.ObjectId
              from query.o_ABC_Address a
              join dataconv.o_abc_address d on d.objectid = a.ObjectId
             where a.AddressType = 'Civic'
               and a.Country is null) loop
    insert into possedata.objectcolumndata cd
       (logicaltransactionid, objectid, columndefid, attributevalueid)
       values
       (api.pkg_logicaltransactionupdate.CurrentTransaction(), i.objectid, t_ColumnDefId, t_DomainValueId);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;