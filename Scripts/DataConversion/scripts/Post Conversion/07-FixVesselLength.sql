declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Vehicle', 'VesselLength');
begin
  for i in (select v.ObjectId,
                   (select domainvalueid
                      from api.domains d
                      join api.domainlistofvalues dv on dv.domainid = d.domainid
                     where d.name = 'ABC Vessel Length'
                       and dv.AttributeValue = (case
                                                  when v.VesselLength < 66 
                                                    then 'UP TO 65 FEET'
                                                  when v.VesselLength >= 66 and v.VesselLength <= 110
                                                    then '66 TO 110 FEET'
                                                  when v.VesselLength > 110
                                                    then 'OVER 110 FEET'
                                                end)) AttId,
                   v.VesselLength
              from query.o_abc_vehicle v
             where v.VesselLength is not null
               and v.vessellength not in ('UP TO 65 FEET', '66 TO 110 FEET', 'OVER 110 FEET')) loop
    update possedata.objectcolumndata_t cd
       set cd.attributevalueid = i.attid,
           cd.attributevalue = null,
           cd.searchvalue = null
     where cd.objectid = i.objectid
       and cd.columndefid = t_ColumnDefId;
  end loop;
  commit;
end;