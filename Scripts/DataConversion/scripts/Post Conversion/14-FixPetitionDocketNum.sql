declare
  t_DocketNumCDef number := api.pkg_configquery.ColumnDefIdForName('j_ABC_Petition', 'ConstructorDocketNumber');
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select p.doc_date, p.docket_num, j.objectid, p.date_recd, to_date(p.docket_num || 01, 'MMRRDD') NewDate, p.docket_seq
              from abc11p.P_tblABCAppeal_int_t p
              join dataconv.j_abc_petition j on j.legacykey = p.docket_seq
             WHERE P.Docket_Num is not null
               and p.docket_num < 1300) loop
    update workflow.jobs w
       set w.CreatedDate = i.newdate
     where w.JobId = i.objectid;
    insert into possedata.objectcolumndata cd
    (
    logicaltransactionid,
    objectid,
    columndefid,
    attributevalue,
    searchvalue
    )
    values
    (
    api.pkg_logicaltransactionupdate.CurrentTransaction,
    i.objectid,
    t_DocketNumCDef,
    i.docket_seq,
    i.docket_seq
    );
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;

select docketnumber from query.j_abc_petition