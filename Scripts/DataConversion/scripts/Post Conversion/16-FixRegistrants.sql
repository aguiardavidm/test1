--Create a table with the LE's that are going to be changed
create table dataconv.FixRegistrantRels as (
--Connect Registrant LE to Licensee LE based on ABC_NUM
select le.objectid OldObjId, le2.objectid NewObjId, le.legalname OldLEName, le2.legalname NewLEName, nm.abc_num, 'N' Processed, cast(null as date) ProcessedDate
  from dataconv.o_abc_legalentity le
  join abc11p.brn_reg_name b on to_char(b.reg_name_mstr_id_num) = le.legacykey
  join abc11p.name_mstr nm on nm.abc_num = b.cnty_muni_cd || '-' || b.lic_type_cd || '-' || b.muni_ser_num || '-' || b.gen_num
    and nm.name_type_cd = '2.1-LICENSEE'
  join dataconv.o_abc_legalentity le2 on le2.legacykey = to_char(nm.name_mstr_id_num)
 where api.pkg_columnquery.Value(le2.objectid, 'Active') = 'Y'
           
union
           
 --Connect via distributor
select le.objectid OldObjId, nml.objectid NewObjId, le.legalname, nml.legalname, max(nm.abc_num), 'N' Processed, cast(null as date) ProcessedDate
  from dataconv.o_abc_legalentity le
  join abc11p.brn_reg_name b on to_char(b.reg_name_mstr_id_num) = le.legacykey
  join abc11p.brn_reg p on p.reg_name_mstr_id_num = b.reg_name_mstr_id_num
  join abc11p.brn_dist d on d.brn_reg_num = p.brn_reg_num
  join abc11p.name_mstr nm on nm.abc_num = d.cnty_muni_cd || '-' || d.lic_type_cd || '-' || d.muni_ser_num || '-' || d.gen_num
    and nm.name_type_cd = '2.1-LICENSEE'
  join dataconv.o_abc_legalentity nml on nml.legacykey = to_char(nm.name_mstr_id_num)
 where api.pkg_columnquery.Value(nml.objectid, 'Active') = 'Y'
   and nml.legalname = le.legalname
   and d.dist_stat = 'C'
  group by le.objectid, le.legalname, nml.objectid, nml.legalname
);
create index dataconv.FixRegistrantRels_IX01
on dataconv.FixRegistrantRels (OldObjId);
create index dataconv.FixRegistrantRels_IX02
on dataconv.FixRegistrantRels (NewObjId);
/

declare
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in (select *
              from dataconv.FixRegistrantRels f
             where f.processed = 'N') loop
    --move all rels
    for c in (select r.RelationshipId
                from query.r_ABC_ProductRegistrant r
               where r.RegistrantObjectId = i.oldobjid) loop
      --Swap relationship ids
      update rel.storedrelationships s
         set s.FromObjectId = i.newobjid
       where s.RelationshipId = c.relationshipid
         and s.FromObjectId = i.oldobjid;
      update rel.storedrelationships s
         set s.ToObjectId = i.newobjid
       where s.RelationshipId = c.relationshipid
         and s.ToObjectId = i.oldobjid;
    end loop;
  --set the old LE to inactive
  api.pkg_columnupdate.SetValue(i.oldobjid, 'Active', 'N');
  update dataconv.fixregistrantrels d
     set d.Processed = 'Y', d.Processeddate = sysdate
   where d.OldObjId = i.oldobjid;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;