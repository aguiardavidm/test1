alter table abc11p.lic_codemaster
  add char_mstr_id_num varchar2(10);
  
create index ABC11P.CODEMASTER_IX2
on abc11p.lic_codemaster (char_mstr_id_num);

  
update abc11p.lic_codemaster c
   set c.char_mstr_id_num = to_char(c.name_mstr_id_num);

create table abc.LegalEntityAccessCodes as (select le.objectid, 
                   (select lc.name_mstr_id_num
                      from abc11p.lic_codemaster lc
                     where lc.char_mstr_id_num = le.legacykey) Code, 'N' processed
          from dataconv.o_abc_legalentity le);
          
create unique index ABC.OBJECTID_PK on ABC.LEGALENTITYACCESSCODES (OBJECTID)
  tablespace LARGEDATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/
declare
  t_OnlineAccessCode   number(9);
  t_startingpoint      number(9);
  t_OACDefId           number := api.pkg_configquery.columndefidforname('o_ABC_LegalEntity', 'OnlineAccessCode');
  t_RowsProcessed      number := 0;
begin
  for i in (select objectid, code
              from abc.legalentityaccesscodes a
              where a.processed = 'N') loop
    if i.Code is null then
      delete from possedata.objectcolumndata
       where objectid = i.objectid
         and columndefid = T_OACDefId;
      
    end if;
    t_RowsProcessed := t_RowsProcessed + 1;
    update abc.legalentityaccesscodes
       set processed = 'Y'
     where objectid = i.objectid;
    if mod(t_RowsProcessed, 5000) = 0 then
      commit;
      dbms_output.put_line(t_RowsProcessed);
    end if;
  end loop;
end;
