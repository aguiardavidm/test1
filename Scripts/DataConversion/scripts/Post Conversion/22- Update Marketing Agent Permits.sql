-- Created on 10/16/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  t_MarketingAgentPermitType number := api.pkg_simplesearch.ObjectByIndex('o_ABC_PermitType', 'Code','MA');
  t_AddressObjectDefId       number := api.pkg_configquery.ObjectDefIdForName('o_ABC_Address');
  t_AddressObjectId          number;
  t_PermitEndPointId         number := api.pkg_configquery.EndPointIdForName('o_ABC_Address', 'Permit');
  t_AddressRelId             number;
  t_AddressRelCount          number := 0;
begin
  -- Test statements here
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for c in (select p.objectid,
                   p.permitnumber, 
                   t.permit_id, 
                   t.promoter_id,
                   ti.promoter_name, 
                   ti.promoter_street ||chr(13)||chr(10) || ti.promoter_city ||' ' ||ti.promoter_state || ' ' || ti.promoter_zip PromoAddress
              from dataconv.o_abc_permit p
              join abc11p.ma_permit_int_t t on t.permit_id = regexp_replace(p.legacykey, '[^[:digit:]]')--substr(p.legacykey,1,1)
              join abc11p.ma_promoter_int_t ti on ti.promoter_id = t.promoter_id
             where p.permittypelk = to_char(t_MarketingAgentPermitType)) loop
   begin
    api.pkg_columnupdate.SetValue(c.objectid,'PromoterCompanyName', c.promoter_name);
    --Create Address and Releate it to the Permit or update the current address
    begin 
    select max(r.FromObjectId), count(*) 
      into t_AddressObjectId, t_AddressRelCount
      from api.relationships r
     where r.ToObjectId = c.objectid
       and r.EndPointId = t_PermitEndPointId
       group by r.ToObjectId;
    exception when no_data_found then
      t_AddressRelCount :=0;
    end;     
       
    if t_AddressRelCount = 0 then   
    t_AddressObjectId := api.pkg_objectupdate.New(t_AddressObjectDefId);
    
    t_AddressRelId := api.pkg_relationshipupdate.New(t_PermitEndPointId,
                                                 t_AddressObjectId,
                                                 c.objectid);    
    end if;
    
    api.pkg_columnupdate.SetValue(t_AddressObjectId, 'OtherAddress', c.PromoAddress); 
    
    api.pkg_columnupdate.SetValue(t_AddressObjectId, 'dup_FormattedDisplay', c.PromoAddress); 
    
    api.pkg_columnupdate.SetValue(t_AddressObjectId,'AddressType','Other / International');   
    
  

    --End Transaction to Check for Errors
    api.pkg_logicaltransactionupdate.EndTransaction();    
    commit;
    dbms_output.put_line('Permit Updated: '|| c.permit_id ||' Permit Number: ' || c.permitnumber);    
    exception when others then
      dbms_output.put_line('Error: ' || SQLERRM);
      dbms_output.put_line('Errored Permit Update: '|| c.permit_id ||' Permit Number: ' || c.permitnumber); 
      rollback;
      api.pkg_logicaltransactionupdate.ResetTransaction();                                                 
   end;
       api.pkg_logicaltransactionupdate.ResetTransaction(); 
  end loop;           
  
    for c in(select a.objectid AddressObjectId
            from dataconv.o_abc_address a
           where a.addresstype = 'OTHER') loop
     api.pkg_columnupdate.SetValue(c.addressobjectid,'AddressType','Other / International');   
   end loop;    
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  
  
  commit;
end;
