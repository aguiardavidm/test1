
create table possedba.old_SpiritOfHudson
  as (select api.pkg_columnquery.Value(lv.LicenseId,'LicenseNumber') LicenseNumber,
             v.ObjectId vehicleid, 
             lv.relationshipId,
             'SPIRIT OF HUDSON' VesselName
              from query.r_abc_licensevehicle lv
              join query.o_abc_vehicle v on v.ObjectId = lv.VehicleId
             where lv.LicenseId in (29557934, 31676285)
               and v.VesselName = 'SPIRIT OF HUDSON');
           
commit;           

/

begin
  -- Test statements here
  
  for c in (select sh.vehicleid, sh.VesselName
              from possedba.old_SpiritOfHudson sh) loop
   
  for i in (select sr.RelationshipId
                from rel.storedrelationships sr
               where sr.FromObjectId = c.vehicleid) loop
  
  delete from rel.storedrelationships r
    where r.RelationshipId = i.relationshipid;   
  
  delete from possedata.objects o
    where o.ObjectId = i.relationshipid;
    
  end loop; 
     
  end loop;   
commit;     
end;
/
begin
  -- Test statements here
  
  for c in (select sh.vehicleid, sh.VesselName
              from possedba.old_SpiritOfHudson sh) loop
   
    
  delete from possedata.objects o
    where o.ObjectId = c.vehicleid;  
 end loop;    
 commit;
end;