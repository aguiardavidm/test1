-- Created on 5/4/2015 by KEATON 
declare 
  -- Local variables here
  t_RelationshipDef          number := api.pkg_configquery.ObjectDefIdForName('r_ABC_LELegalEntityType');
  t_RelationshipId           number;
  t_ConvertedTypeObjId       number;
  t_LETypeEndPointId         number := api.pkg_configquery.EndPointIdForName('o_ABC_LegalEntity','LegalEntityType');
  t_LegalEntityEndPointId    number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity','LegalEntityType');
  t_LegalEntityTypeObjDef    number := api.pkg_configquery.ObjectDefIdForName('o_ABC_LegalEntityType');
  t_NewLegalEntityType       number;
  
  
begin

-- Create a Converted Individual Legal Entity Type
api.pkg_logicaltransactionupdate.ResetTransaction();
t_NewLegalEntityType := api.pkg_objectupdate.New(t_LegalEntityTypeObjDef);

api.pkg_columnupdate.SetValue(t_NewLegalEntityType, 'Name', 'Converted Individual');
api.pkg_columnupdate.SetValue(t_NewLegalEntityType, 'OnlineDescription', 'Converted Individual');
api.pkg_logicaltransactionupdate.EndTransaction();
-- Select The  Correct Legal Entity Type

   select le.ObjectId
     into t_ConvertedTypeObjId 
     from query.o_abc_legalentitytype le
    where le.Name='Converted Individual';

-- Loop through relationships and delete from the tables and create a new object.

for c in (select sr1.RelationshipId, 
           (select od.Name
              from api.objects o
              join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
             where o.ObjectId = sr1.FromObjectId )FromObjDefName,
             sr1.FromObjectId,
             (select od.Name
              from api.objects o
              join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
             where o.ObjectId = sr1.ToObjectId )ToObjDefName,
             sr1.ToObjectId
      from rel.storedrelationships sr1
      join dataconv.o_abc_legalentity dle on dle.objectid = sr1.FromObjectId
      where sr1.Relationshipid in (select sr.Relationshipid
      from rel.storedrelationships sr
       where sr.ToObjectId = (select le.objectid 
                                           from query.o_abc_legalentitytype le
                                          where le.Name='Converted') 
                               and sr.fromobjectid != 27749924 /*Licensee for 9902-20-249-002 */     )
                
            and (select od.Name
              from api.objects o
              join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
             where o.ObjectId = sr1.FromObjectId ) = 'o_ABC_LegalEntity'
            and  sr1.EndPointId = api.pkg_configquery.EndPointIdForName('o_abc_legalEntity', 'LegalEntityType')
            and dle.legalentitytypelk = 22174200) loop


delete from rel.storedrelationships sr
  where sr.RelationshipId = c.relationshipid;
delete from objmodelphys.objects o 
  where o.ObjectId = c.relationshipid;  
   

select possedata.ObjectId_s.nextval
  into t_RelationshipId 
 from dual;

--Insert into ObjModelPhys.Objects
insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelationshipId,
      t_RelationshipDef,
      4, -- Hard Coded to ObjectDefType of 4 for relationship
      4, -- Hard Coded to ObjectDefType of 4 for relationship
      t_RelationshipDef,
      null,
      null,
      4, -- Hard Coded to ObjectDefType of 4 for relationship
      t_RelationshipDef,
      null,
      null
    );

 
--Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipId,
      t_LegalEntityEndPointId,
      t_ConvertedTypeObjId,
      c.Fromobjectid
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelationshipId,
      t_LETypeEndPointId,
      c.fromobjectid,
      t_ConvertedTypeObjId
    ); 


end loop;
  
end;
