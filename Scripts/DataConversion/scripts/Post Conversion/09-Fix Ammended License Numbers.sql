declare

    t_LicenseNumber               varchar2(20);
    t_GenerationNumber            number;
    t_count                       number :=0;
    t_LicenseNumColumnDefId       number := api.pkg_configquery.ColumnDefIdForName('o_abc_license', 'LcienseNumber');   
    
begin
api.pkg_logicaltransactionupdate.Resettransaction();


for c in (select LicenseObjectId from (select aa.LicenseObjectId, api.pkg_columnquery.Value(aa.LicenseObjectId, 'LicenseNumber') NewL, aa.LicenseNumber OldL
  from query.j_abc_amendmentapplication aa
  join query.r_ABC_AmendJobLicenseAmendType alt on alt.AmendJobLicenseId = aa.ObjectId
  join query.o_abc_amendmenttype aat on aat.ObjectId = alt.AmendmentTypeId
 where aat.IncrementLicenseNumber = 'Y')
where Newl = OldL

union

select LicenseObjectId from (select aa.LicenseObjectId, api.pkg_columnquery.Value(aa.LicenseObjectId, 'LicenseNumber') NewL, aa.LicenseNumber OldL
  from query.j_abc_amendmentapplication aa
  join query.r_ABC_AmendJobLicenseAmendType alt on alt.AmendJobLicenseId = aa.ObjectId
  join query.o_abc_amendmenttype aat on aat.ObjectId = alt.AmendmentTypeId
 where aat.IncrementLicenseNumber = 'Y')
where to_number(substr(Newl,13,3)) < to_number(substr(OldL,13,3))) loop

  t_LicenseNumber := api.pkg_columnquery.Value(c.LicenseObjectId, 'LicenseNumber');
  t_GenerationNumber := substr(t_LicenseNumber, 13,3);
  t_GenerationNumber := t_GenerationNumber + 1;
  t_LicenseNumber := substr(t_LicenseNumber, 1, 12) || lpad(to_char(t_GenerationNumber), 3, '0');
  t_count := t_count +1;
  
  --Go below APi to update License for incomplete amendments
  update possedata.objectcolumndata ocd
    set ocd.AttributeValue = t_LicenseNumber
   where ocd.ColumnDefId = t_LicenseNumColumnDefId
     and ocd.ObjectId = c.LicenseObjectId;  
  
  --api.pkg_ColumnUpdate.SetValue(c.LicenseObjectId, 'LicenseNumber', t_LicenseNumber);

end loop;

api.pkg_logicaltransactionupdate.Endtransaction();

dbms_output.put_line(t_count);

end;
