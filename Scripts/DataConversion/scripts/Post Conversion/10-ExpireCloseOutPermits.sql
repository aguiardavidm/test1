declare
  t_StateColumndefid number := api.pkg_configquery.columndefidforname('o_ABC_Permit', 'State');
  t_ExpColumnDefId   number := api.pkg_configquery.columndefidforname('o_ABC_Permit', 'ExpirationDate');
  t_CloseOutPTId     number;
  t_ExpDate          date;
  t_NewExpDate       varchar(100);
  t_Counter          number := 0;
begin

  Select pt.Objectid
    into t_CloseOutPTId
    from query.o_abc_permittype pt
   where pt.Code = 'CO'; --Closeout
  --Find all close out permits that have an issue date set.
  for i in (Select pt.permitid,
                   api.pkg_columnquery.DateValue(pt.permitid, 'ExpirationDate') ExpDate,
                   api.pkg_columnquery.DateValue(pt.permitid, 'IssueDate') IssueDate,
                   api.pkg_columnquery.Value(pt.permitid, 'PermitNumber') PermitNumber
              from query.r_ABC_PermitPermitType pt 
             where pt.PermitTypeId = t_CloseOutPTId
               and api.pkg_columnquery.Value(pt.permitid, 'IssueDate') is not null) loop
  --Set the Expiration date based off of the issue date, 3 months after issue date.
      t_NewExpDate := to_char(ADD_MONTHS(i.IssueDate, 3) , 'YYYY-MM-DD HH24:MI:SS');
      if i.expdate is null then  
        insert into possedata.objectcolumndata d
           (d.LogicalTransactionId, d.ObjectId, d.AttributeValue, d.columndefid)
        values
           (1,i.permitid, t_NewExpDate, t_ExpColumnDefId);
        dbms_output.put_line('Set Expiration date to ' || t_NewExpDate || ' on Permit ' || i.permitnumber);
      else
      update possedata.objectcolumndata d
         set d.AttributeValue = t_NewExpDate,
             d.searchvalue = lower(substrb(t_NewExpDate, 1, 20))
       where d.Columndefid = t_ExpColumnDefId
         and d.objectid = i.permitid;
      end if;
    t_ExpDate := ADD_MONTHS(i.IssueDate, 3);--Setting this variable so I can easily expire the permits after this section without running another loop
  --Set the permit state to Expired if the expiration date is in the past. 
    if t_ExpDate < sysdate then
      update possedata.objectcolumndata cd
         set cd.AttributeValueId = (select dv.domainvalueid
                                      from api.domains do
                                      join api.domainlistofvalues dv on dv.DomainId = do.DomainId
                                     where do.Name = 'ABC Permit State'
                                       and dv.AttributeValue = 'Expired')
       where cd.ColumnDefId = t_StateColumndefid
         and cd.ObjectId = i.permitid;
      dbms_output.put_line('Expired permit: ' || i.permitnumber);
      t_Counter := t_Counter + 1;
    end if;
  end loop;
  dbms_output.put_line('Total Count: ' || t_Counter);
  commit;
end;