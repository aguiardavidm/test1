-- Created on 4/26/2016 by Keaton Leikam 
declare 
  -- Local variables here
  t_PermitCount number:=0;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction();
  for c in (select    op.ObjectId,
          op.PermitTypeObjectId,
          op.LicenseObjectId,
          api.pkg_columnquery.Value(op.PermitTypeObjectId,'AutoExpire') AutoExpire,
          op.PermitNumber, 
          op.PermitType,
          op.PermitteeDisplay_SQL,
          op.State
          from query.o_abc_permit op
          where op.ObjectDefTypeId = 1
            and op.State in ('Active')
            and op.PermitTypeRenewable = 'Y'
            and op.ExpirationDate < sysdate
            order by 1 asc) loop
        if c.State = 'Active' then
            if c.AutoExpire = 'Y' then
              api.pkg_ColumnUpdate.SetValue(c.ObjectId, 'State', 'Expired');
              t_PermitCount := t_PermitCount+1;
            end if;
        end if;
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction();  
  commit;
end;