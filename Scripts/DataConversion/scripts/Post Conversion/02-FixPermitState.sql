create table abc.permitexpiration as (select p.objectid, p.legacykey, p.expirationdate, p.state, 'N' processed
              from dataconv.o_abc_permit p
             where p.state != 'Expired');
create unique index ABC.OBJECTID_PK2 on ABC.PERMITEXPIRATION (OBJECTID)
  tablespace LARGEDATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/
declare
  t_StateDefId   number := api.pkg_configquery.ColumnDefIdForName('o_ABC_Permit', 'State');
  t_EventEndDate date;
  t_RowsProcessed      number := 0;
begin
  for i in (select *
              from abc.permitexpiration e
             where e.processed = 'N') loop
    if i.expirationdate is not null
      and i.expirationdate < sysdate
      and i.state != 'Closed' then
      delete from possedata.objectcolumndata cd
       where cd.ObjectId = i.objectid
         and cd.ColumnDefId = t_StateDefId;
      insert into possedata.objectcolumndata cd
      (
      LOGICALTRANSACTIONID,
      OBJECTID,
      COLUMNDEFID,
      ATTRIBUTEVALUEID
      )
      values
      (
      1,
      i.objectid,
      t_StateDefId,
      (select dv.DomainValueId
         from api.domains d
         join api.domainlistofvalues dv on dv.DomainId = d.DomainId
        where d.Name = 'ABC Permit State'
          and dv.AttributeValue = 'Expired')
      );
    end if;
    if i.expirationdate is null then
      select max(EventDate)
        into t_EventEndDate
        from (
      select api.pkg_columnquery.value(ed.EventDateObjectId, 'EndDate') EventDate --End of day
        from query.r_ABC_PermitEventDate ed
       where ed.PermitObjectId = i.objectid
       union all
       select api.pkg_columnquery.value(ed.RainDateObjectId, 'EndDate') --End of day
        from query.r_ABC_PermitRainDate ed
       where ed.PermitObjectId = i.objectid);
      if t_EventEndDate is not null
         and t_EventEndDate < sysdate then
        
        delete from possedata.objectcolumndata cd
         where cd.ObjectId = i.objectid
           and cd.ColumnDefId = t_StateDefId;
        insert into possedata.objectcolumndata cd
        (
        LOGICALTRANSACTIONID,
        OBJECTID,
        COLUMNDEFID,
        ATTRIBUTEVALUEID
        )
        values
        (
        1,
        i.objectid,
        t_StateDefId,
        (select dv.DomainValueId
           from api.domains d
           join api.domainlistofvalues dv on dv.DomainId = d.DomainId
          where d.Name = 'ABC Permit State'
            and dv.AttributeValue = 'Expired')
        );
      end if;
    end if;
    t_RowsProcessed := t_RowsProcessed + 1;
    update abc.permitexpiration
       set processed = 'Y'
     where objectid = i.objectid;
    if mod(t_RowsProcessed, 5000) = 0 then
      commit;
      dbms_output.put_line(t_RowsProcessed);
    end if;
  end loop;
  commit;
end;