declare
  t_Count number := 0;
begin
  for i in (select r.objectid relationshipid, c.mem_stat
              from abc11p.coop_mem c
              join dataconv.r_ABC_PermitCoOpLicenses r on r.o_abc_license = CNTY_MUNI_CD || LIC_TYPE_CD || MUNI_SER_NUM || GEN_NUM
                and r.o_abc_permit = COOP_NUM || 'COOP'
             where c.mem_stat = 'H') loop

    delete from rel.storedrelationships r
      where r.RelationshipId = i.relationshipid;   
    
    delete from possedata.objects o
      where o.ObjectId = i.relationshipid;
      
    t_Count := t_Count + 1;
  end loop;
  dbms_output.put_line(t_Count || 'relationships deleted.');
  commit
end;
/
--See coops affected
select c.coop_num CoopNum, CNTY_MUNI_CD || '-' || LIC_TYPE_CD ||  '-' || MUNI_SER_NUM ||  '-' || GEN_NUM LicenseNum
              from abc11p.coop_mem c
              join dataconv.r_ABC_PermitCoOpLicenses r on r.o_abc_license = CNTY_MUNI_CD || LIC_TYPE_CD || MUNI_SER_NUM || GEN_NUM
                and r.o_abc_permit = COOP_NUM || 'COOP'
             where c.mem_stat = 'H'  