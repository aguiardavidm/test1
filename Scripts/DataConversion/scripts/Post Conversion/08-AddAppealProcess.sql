--Add the initial process to converted Appeal jobs in the NEW status
--run time < 2sec
create table dataconv.AddProcessOnAppealJobs as
(select d.objectid, d.statustag, 'N' Processed, cast('' as varchar2(4000)) ErrorMessage
   from dataconv.j_abc_appeal d
  where d.statustag = 'NEW');
commit;
/
declare
  t_ProcessDefId number := api.pkg_configquery.ObjectDefIdForName('p_ABC_EnterAppeal');
  t_ProcessId    number;
  t_UserId       number;
  t_Error        varchar2(4000);
begin
  select s.AppealProcessorId
    into t_UserId
    from query.r_ABC_SystemAppealProcessor s
   where rownum < 2;
  for i in (select *
              from dataconv.AddProcessOnAppealJobs a
             where a.processed = 'N') loop
    begin
      api.pkg_logicaltransactionupdate.ResetTransaction;
      t_ProcessId := api.pkg_processupdate.New(i.objectid, t_ProcessDefId, '', '', '', '');
      api.pkg_processupdate.Assign(t_ProcessId, t_UserId);
      update dataconv.AddProcessOnAppealJobs t
         set t.processed = 'Y'
       where t.objectid = i.objectid;
      api.pkg_logicaltransactionupdate.EndTransaction;
    exception when others then
      rollback;
      t_Error := sqlerrm;
      dbms_output.put_line(t_Error || 'For Id: ' || i.Objectid);
    end;
  end loop;
  commit;
end;