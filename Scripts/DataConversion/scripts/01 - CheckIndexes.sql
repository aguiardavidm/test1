--Run time: 1s
-- Created on 3/28/20count(1)5 by PAUL 
declare 
  -- Local variables here
  i integer := 0;
begin
  -- Test statements here
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'O_ABC_ESTABLISHMENT_IXLK';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index O_ABC_ESTABLISHMENT_IXLK');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'O_ABC_ESTABLISHMENTHISTOR_IXLK';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index O_ABC_ESTABLISHMENTHISTOR_IXLK');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'O_ABC_LEGALENTITY_IXLK';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index O_ABC_LEGALENTITY_IXLK');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'O_ABC_LICENSE_IXLK';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index O_ABC_LICENSE_IXLK');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'I9_NAME_MSTR';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index I9_NAME_MSTR');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'I5_LIC_MSTR';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index I5_LIC_MSTR');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'I5_BRN_REG';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index I5_BRN_REG');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'I6_BRN_REG';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index I6_BRN_REG');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'I3_BRN_DIST';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index I3_BRN_DIST');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'I2_BRN_TERM';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index I2_BRN_TERM');
   end if;
   i := 0;
  select count(1)
    into i
    from dba_indexes i
   where i.index_name = 'I10_NAME_MSTR';
   if i = 0 then
     raise_application_error(-20000, 'Missing Index I10_NAME_MSTR');
   end if;
  select count(1)
    into i 
    from abc11p.name_mstr 
   where abc11puk is null;
   if i != 0 then
     raise_application_error(-20000, 'abc11puk is null');
   end if;
end;