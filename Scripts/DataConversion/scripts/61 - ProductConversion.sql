-- Created on 8/12/2014 by ADMINISTRATOR
--Expected run time: <2 minutes
declare 
  -- Local variables here
  i integer;
  t_ProductRow   abc11p.brn_reg%rowtype;
  t_RegLicNum    varchar2(40);
  t_TermStart    varchar2(40);
  t_TermEnd      varchar2(40);
  t_LastUpdateBy varchar2(40);
  t_CreatedBy    varchar2(40);
  t_RowsProcessed number := 0;
  t_ProductTypeLK varchar2(40);
--  t_RegistrantLK varchar2(40);
  
begin
  select objectid
    into t_ProductTypeLK
    from query.o_abc_producttype p
   where p.Type = 'Conversion';
  -- Test statements here
  for i in (select *
              from abc11p.brn_reg b
             where b.abc11puk not in (select legacykey from dataconv.o_ABC_Product)) loop

    if i.reg_name_mstr_id_num is not null then
      with LicTemp as (select r.CNTY_MUNI_CD || '-' || r.LIC_TYPE_CD || '-' || r.MUNI_SER_NUM || '-' || max(r.GEN_NUM) LicNum
                        from abc11p.brn_reg_name r
                       where r.reg_name_mstr_id_num = i.reg_name_mstr_id_num
                       group by r.CNTY_MUNI_CD || '-' || r.LIC_TYPE_CD || '-' || r.MUNI_SER_NUM)
     select MAX(l.legacykey) --Can be null, avoid a no data found
       into t_RegLicnum
       from dataconv.o_abc_license l
       join LicTemp t on t.LicNum = l.licensenumber;

    else 
      t_RegLicNum := '';
    end if;
    
    select min(t.dt_term_start), max(t.dt_term_end)
      into t_TermStart, t_TermEnd
      from abc11p.brn_term t
     where t.brn_reg_num = i.brn_reg_num;
    
    insert into dataconv.o_Abc_Product
      (
      LEGACYKEY,
      REGISTRATIONNUMBER,
      REGISTRANTLICLK,
      LEREGISTRANTLK,
      NAME,
      REGISTRATIONDATE,
      EXPIRATIONDATE,
      STATE,
      CREATEDBYUSERLEGACYKEY,
      CONVCREATEDDATE,
      LASTUPDATEDBYUSERLEGACYKEY,
      LASTUPDATEDDATE,
      ProductTypeLK
      )
      values
      (
      i.Abc11puk,
      i.BRN_REG_NUM,
      t_RegLicNum,
      (case
         when exists (select 1 from dataconv.o_abc_legalentity l where l.legacykey = to_char(i.reg_name_mstr_id_num)) then
           i.reg_name_mstr_id_num
       end),
      i.BRN_NAME,
      i.DT_FILED,
      t_TermEnd,
      decode(i.BRN_STAT, 'C', 'Current', 'H', 'Historical', 'P', 'Pending'),
      i.Crtd_Oper_Id,
      i.DT_ROW_CRTD,
      i.Updt_Oper_Id,
      i.DT_ROW_UPDT,
      t_ProductTypeLK
      );
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  commit;
end;
