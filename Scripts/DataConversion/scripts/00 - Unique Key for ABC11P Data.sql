--Run time: 5m
declare 
  -- Local variables here
  t_DynamicUpdate varchar2(4000);
begin
  -- Test statements here
  
  for c in( 
     select 'alter table abc11p.' || t.TABLE_NAME || ' add (ABC11PUK number) ' DynamicUpdate, t.Table_Name          
       from dba_tables t 
      where (t.owner = 'ABC11P'
        and t.table_name not in ('ADDRESS_MSTR', 'ABCTA104_REPORT', 'ABCTA104_SUM', 'ABCTA104'))) loop
        begin
    
        EXECUTE IMMEDIATE c.Dynamicupdate;
        dbms_output.put_line('Table Name: ' || c.Table_Name);
        exception when others then
        dbms_output.put_line(c.Dynamicupdate);
        end;
    end loop;
end;
/
create sequence abc11p.ABC11PUKS
  start with 1
  increment by 1
  cache 10000;

declare 
  -- Local variables here
  t_DynamicUpdate varchar2(4000);
begin
  -- Test statements here
  
  for c in( 
      select ('update ' || dtc.owner || '.'||dtc.TABLE_NAME || ' gt ' 
       || ' set gt.' || dtc.COLUMN_NAME ||'='|| 'abc11p.ABC11PUKS.NEXTVAL' )DynamicUpdate,
         dtc.TABLE_NAME,
         dtc.COLUMN_NAME           
    from dba_tab_cols dtc
    join dba_tables t on t.TABLE_NAME = dtc.TABLE_NAME
  where dtc.COLUMN_NAME = 'ABC11PUK') loop
        begin
    
        EXECUTE IMMEDIATE c.Dynamicupdate;
        dbms_output.put_line('Table Name: ' || c.Table_Name || ' - ' || c.Column_Name || ' - ' || sql%rowcount);
        exception when others then
        dbms_output.put_line(c.Dynamicupdate);
        end;
    end loop;
end;
/
--create some indexes to help performance
create index dataconv.O_ABC_ADDRESS_IXLK
on dataconv.o_Abc_ADDRESS (LEGACYKEY);
create index dataconv.O_ABC_ESTABLISHMENT_IXLK
on dataconv.o_Abc_Establishment (LEGACYKEY);
create index dataconv.O_ABC_ESTABLISHMENTHISTOR_IXLK
on dataconv.o_Abc_Establishmenthistory (LEGACYKEY);
create index dataconv.O_ABC_LEGALENTITY_IXLK
on dataconv.O_ABC_LEGALENTITY (LEGACYKEY);
create index dataconv.o_Abc_License_IXLK
on dataconv.o_Abc_License (LEGACYKEY);
create index dataconv.o_Abc_Permit_IXLK
on dataconv.o_Abc_Permit (LEGACYKEY);
create index abc11p.I9_NAME_MSTR
on abc11p.name_mstr (ABC_NUM);
create index abc11p.I5_LIC_MSTR
on abc11p.LIC_MSTR (LIC_STAT);
create index abc11p.I5_BRN_REG
on abc11p.BRN_REG (ABC11PUK);
create index abc11p.I6_BRN_REG
on abc11p.BRN_REG (BRN_REG_NUM);
create index ABC11P.I3_BRN_DIST 
on ABC11P.BRN_DIST (CNTY_MUNI_CD, LIC_TYPE_CD, MUNI_SER_NUM, GEN_NUM);
create index abc11p.I2_BRN_TERM
on abc11p.BRN_TERM (BRN_REG_NUM);
create index abc11p.I10_NAME_MSTR
on abc11p.name_mstr (NAME_STAT);