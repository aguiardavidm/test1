create table dataconv.tempDupColumnData
  tablespace largedata  as
  select
    o.LogicalTransactionId,
    o.ObjectId,
    api.pkg_ColumnQuery.Value(o.ObjectId,
        'ExpDatePlusGracePeriod') ExpDatePlusGracePeriod
  from 
    dataconv.o_ABC_License x
    join possedata.Objects_t o
      on o.ObjectId = x.ObjectId;
   
insert /*+ append */ into
  possedata.ObjectColumnData_t  (
    LogicalTransactionId,
    ObjectId,
    ColumnDefId,
    AttributeValue,
    SearchValue)
  select
    LogicalTransactionId,
    ObjectId,
    api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'dup_ExpDatePlusGracePeriod'),
    ExpDatePlusGracePeriod,
    ExpDatePlusGracePeriod
  from dataconv.tempDupColumnData
  where ExpDatePlusGracePeriod is not null;
commit;