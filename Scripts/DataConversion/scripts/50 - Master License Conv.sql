--Run Time: 5 Minutes
-- Created on 2/27/2015 by ADMINISTRATOR
declare 
  -- Local variables here
  i integer;
  t_RowsProcessed  number := 0;
begin
  begin
    execute immediate 'drop index DATACONV.O_ABC_MASTERLICENSE_IXLK';
  exception when others then
    dbms_output.put_line(sqlerrm || '. Skipping Index drop');
  end;
  commit;
  -- Test statements here
  for i in (select lm.cnty_muni_cd || lm.lic_type_cd || lm.muni_ser_num || lm.gen_num LegacyKey, lm.crtd_oper_id, lm.dt_row_crtd, lm.updt_oper_id, lm.dt_row_updt
              from abc11p.lic_mstr lm
             where lm.cnty_muni_cd || lm.lic_type_cd || lm.muni_ser_num || lm.gen_num in (select l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num || min(l.gen_num)
                                                                                            from abc11p.lic_mstr l
                                                                                           where l.lic_stat in ('C', 'H')
                                                                                           group by l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num)
               and not exists (select 1
                                   from dataconv.o_Abc_Masterlicense m
                                  where m.legacykey = lm.cnty_muni_cd || lm.lic_type_cd || lm.muni_ser_num || lm.gen_num)) loop
    insert into dataconv.o_abc_masterlicense
    (
    LEGACYKEY,
    CREATEDBYUSERLEGACYKEY,
    CONVCREATEDDATE,
    LASTUPDATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE
    )
    values
    (
    i.LegacyKey,
    i.CRTD_OPER_ID,
    i.DT_ROW_CRTD,
    i.UPDT_OPER_ID,
    i.DT_ROW_UPDT
    );
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
    end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  commit;
  execute immediate 'create index dataconv.O_ABC_MASTERLICENSE_IXLK
                    on dataconv.o_abc_masterlicense (legacykey)';
  commit;
end;