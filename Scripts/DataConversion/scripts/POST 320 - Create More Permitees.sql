--Run Time: 15s
declare
  t_LEGALENTITYTYPELK varchar2(400);
  t_RowsProcessed number := 0 ;

begin
  select legacykey
    into t_LEGALENTITYTYPELK
    from dataconv.o_abc_legalentitytype lt
   where lt.name = 'Converted';
  for i in (select x.*
              from (select p.perm_name,
                               Min(p.CRTD_OPER_ID) CREATEDBYUSERLEGACYKEY,
                               min(p.DT_ROW_CRTD) CONVCREATEDDATE,
                               max(p.UPDT_OPER_ID) LASTUPDATEDBYUSERLEGACYKEY,
                               max(p.DT_ROW_UPDT) LASTUPDATEDDATE,
                               max(p.abc11puk) abc11puk
                          from abc11p.Permit p
                          join query.o_abc_permittype pt on pt.Code = decode(p.perm_type_cd, 'GEN', 'MISC', 'GEN2', 'MISC', p.perm_type_cd)
                         where pt.ObjectDefTypeId = 1
                           and p.perm_stat in ('C', 'H')
                           and p.cnty_muni_cd is not null
                           and pt.PermitteeIsLicensee = 'N'
                         group by p.perm_name, p.po_box || p.zip_1_5 ||p.zip_6_9 || p.st_prov_cd || p.str_name || p.str_num || p.city) x
             WHERE not exists (select 1 from dataconv.o_abc_legalentity le where le.legacykey = x.abc11puk || 'PERM2')) loop
    insert into dataconv.o_abc_legalentity
    (
    LEGACYKEY,
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    LEGALNAME,
    LEGALENTITYTYPELK
  --  MAILINGADDRESSLK,
--    PHYSICALADDRESSLK
    )
    values
    (
    i.abc11puk || 'PERM2',
    i.CREATEDBYUSERLEGACYKEY,
    i.CONVCREATEDDATE,
    i.LASTUPDATEDBYUSERLEGACYKEY,
    i.LASTUPDATEDDATE,
    i.perm_name,
    t_LEGALENTITYTYPELK
  --  i.addresslk,
--    i.addresslk
    );
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      dbms_output.put_line(t_RowsProcessed);
      commit;
    end if;
  end loop;
  dbms_output.put_line(t_RowsProcessed);
  delete
    from dataconv.posseconvprogress
   where tablename = 'O_ABC_LEGALENTITY';
  delete
    from dataconv.posseconvmaintaindupsprogress
   where tablename = 'O_ABC_LEGALENTITY';
   --Set the addresses
   t_RowsProcessed := 0;
  for i in (select a.legacykey malk, le.legacykey lelk
              from dataconv.o_abc_legalentity le 
              join abc11p.permit p on le.legacykey = p.abc11puk || 'PERM2'
              join abc11p.address_mstr am on am.legacykey = p.abc11puk || 'PREM'
              join abc11p.address_xref3 xr3 on xr3.oldaddresslk = am.legacykey
              join dataconv.o_abc_address a on a.legacykey = xr3.newaddresslk
             where p.perm_stat in ('C', 'H')
               and p.cnty_muni_cd is not null
               and le.rowstatus = 'N'
             group by a.legacykey , le.legacykey) loop
    update dataconv.o_abc_legalentity le
       set le.mailingaddresslk = i.malk, le.physicaladdresslk = i.malk
     where le.legacykey = i.lelk;
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      dbms_output.put_line(t_RowsProcessed);
      commit;
    end if;
  end loop;
  dbms_output.put_line(t_RowsProcessed);
  commit;
end;