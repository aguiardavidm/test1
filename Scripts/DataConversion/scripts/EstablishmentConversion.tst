-- Created on 5/7/2014 by PAUL 
declare 
  -- Local variables here
  i           integer;
  t_RowsProcessed number := 0;
  t_Row       abc11p.name_mstr%rowtype;
  t_CntactRow abc11p.name_mstr%rowtype;
  t_LicTypecd varchar2(10);
  t_CmtText   varchar2(4000);
  t_LicenseeLK varchar2(40);
 -- t_Licensee  varchar2(4000);
begin
  -- Test statements here
  
  for i in (select rowid
              from abc11p.name_mstr
             where name || name_type_cd not in (select x.legacykey
                                                  from dataconv.o_abc_establishment x)
               and name_stat = 'C'
               and name_type_cd in ('2.5-D/B/A', '2.6-D/B/A')) loop
    select *
      into t_Row
      from abc11p.name_mstr
     where rowid = i.rowid;
begin     
    select lic_type_cd
      into t_LicTypecd
      from abc11p.lic_mstr
     where CNTY_MUNI_CD || '-' || LIC_TYPE_CD || '-' || MUNI_SER_NUM || '-' || GEN_NUM = t_row.abc_num;

    select b.*
      into t_CntactRow
      from abc11p.name_mstr a
      join abc11p.name_mstr b on b.abc_num = a.abc_num
     where b.name_type_cd = '2.1-LICENSEE'
       and b.name_stat in ('C', 'H')
       and a.rowid = i.rowid;
exception when others then
  dbms_output.put_line('''' || i.rowid || ''',');
end;
    --Not all records will have an email
    begin  
      select cmt_text
        into t_CmtText
        from abc11p.lic_cmt
       where CNTY_MUNI_CD = t_Row.CNTY_MUNI_CD
         and LIC_TYPE_CD = t_Row.LIC_TYPE_CD
         and MUNI_SER_NUM = t_Row.MUNI_SER_NUM
         and GEN_NUM = t_Row.GEN_NUM
         and instr(cmt_text, '@') != 0;
    exception when no_data_found then
      null;
    end;
      
    insert into dataconv.o_abc_establishment
      (
      LICENSEELK,
      MAILINGADDRESSLK,
      LEGACYKEY,
      CREATEDBYUSERLEGACYKEY,
      CONVCREATEDDATE,
      LASTUPDATEDBYUSERLEGACYKEY,
      LASTUPDATEDDATE,
      DOINGBUSINESSAS,
      CONTACTNAME,
      CONTACTPHONENUMBER,
      CONTACTALTPHONENUMBER,
      CONTACTFAXNUMBER,
      CONTACTEMAILADDRESS,
      PREFERREDCONTACTMETHOD,
      ACTIVE,
      OPERATORLK,
      PHYSICALADDRESSLK,
      ESTABLISHMENTTYPELK
      )
      values
      (
      t_CntactRow.Abc11puk,
      t_CntactRow.abc11puk ||'NM', --To be changed when address conversion is changed, accounting for multiple matching addresses with one physical and one mailing
      t_Row.Abc11puk,
      t_Row.CRTD_OPER_ID,
      t_Row.DT_ROW_CRTD,
      t_Row.UPDT_OPER_ID,
      t_Row.DT_ROW_UPDT,
      t_Row.NAME,
      t_CntactRow.Name,
      t_CntactRow.BUS_AREA_CD || t_CntactRow.BUS_PREFIX || t_CntactRow.BUS_SUFFIX,
      t_CntactRow.MAIL_AREA_CD || t_CntactRow.MAIL_PREFIX || t_CntactRow.MAIL_SUFFIX,
      t_CntactRow.HOME_AREA_CD || t_CntactRow.HOME_PREFIX || t_CntactRow.HOME_SUFFIX,
      t_CmtText,
      'Mail',
      'Y',
      t_Row.NAME_MSTR_ID_NUM,
      t_CntactRow.Abc11puk,
      t_LicTypecd
      );
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
    end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  commit;
end;