--Run time: 1 second  
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_PermitType      varchar2(100);
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
  update abc11p.ma_permit_int_t am
        set am.errortext = a_ErrorText,
            am.errorcode = a_ErrorCode
      where am.rowid = a_RowId;  
  
    commit;
  end;

begin

  select legacykey
    into t_ConvUserKey 
   from dataconv.u_users u
  where u.oraclelogonid = 'ABCCONV';
  
  select ObjectId 
    into t_PermitType 
   from dataconv.o_abc_permittype pt
  where pt.code = 'MA';

  for c in (select t.rowid,
                   t.PERMIT_ID || 'MAPERMIT' LEGACYKEY,
                   t_PermitType PERMITTYPELK,
                   to_date(t.PERMIT_TIMESTAMP, 'MM/DD/YY HH24:MI:SS') LASTUPDATEDDATE,
                   to_date(t.PERMIT_TIMESTAMP, 'MM/DD/YY HH24:MI:SS') CONVCREATEDDATE,
                   t_ConvUserKey CREATEDBYUSERLEGACYKEY,
                   t_ConvUserKey LASTUPDATEDBYUSERLEGACYKEY,
                   to_date(t.PERMIT_DATE_ISSUED, 'MM/DD/YY HH24:MI:SS') ISSUEDATE,
                   to_date(t.PERMIT_TERM_START_DATE, 'MM/DD/YY HH24:MI:SS') EFFECTIVEDATE,
                   to_date(t.PERMIT_TERM_END_DATE, 'MM/DD/YY HH24:MI:SS') EXPIRATIONDATE,
                   'Permit Fee: ' || t.PERMIT_FEE || ' Permit Tran No:' ||t.PERMIT_TRAN_NO AdditionalPermitInformation,
                   'Promoter Timestamp: ' || pi.promoter_timestamp || chr(10)||
                   'Promoter Name: ' ||  pi.promoter_name   || chr(10)||
                   'Promoter Street: ' ||  pi.promoter_street || chr(10)||
                   'Promoter City: ' ||  pi.promoter_city  || chr(10)||
                   'Promoter State: ' ||  pi.promoter_state  || chr(10)||
                   'Promoter Zip: ' ||  pi.promoter_zip  || chr(10)||
                   'Promoter Contact: ' ||  pi.promoter_contact  || chr(10)||
                   'Promoter Phone: ' ||  pi.promoter_phone  || chr(10)||
                   'Promoter Email: ' ||  pi.promoter_email text,
                   case when to_date(t.PERMIT_TERM_END_DATE, 'MM/DD/YY HH24:MI:SS') > sysdate then 'Active' else 'Closed' end state
             from abc11p.ma_permit_int_t t
             join abc11p.ma_promoter_int_t pi on pi.promoter_id = t.promoter_id
            where t.processed is null) loop
    begin
    insert into dataconv.o_abc_permit a
    ( LEGACYKEY,
      PERMITNUMBER,
      PERMITTYPELK,
      LASTUPDATEDDATE,
      CONVCREATEDDATE,
      CREATEDBYUSERLEGACYKEY,
      LASTUPDATEDBYUSERLEGACYKEY,
      ISSUEDATE,
      EFFECTIVEDATE,
      EXPIRATIONDATE,
      AdditionalPermitInformation,
      STATE
      )
    values ( c.LEGACYKEY,
           c.LEGACYKEY,
             c.PERMITTYPELK,
             c.LASTUPDATEDDATE,
             c.CONVCREATEDDATE,
             c.CREATEDBYUSERLEGACYKEY,
             c.LASTUPDATEDBYUSERLEGACYKEY,
             c.ISSUEDATE,
             c.EFFECTIVEDATE,
             c.EXPIRATIONDATE,
             c.AdditionalPermitInformation,
             c.state       );    
    insert into dataconv.n_general
         (createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text)
     values (c.CREATEDBYUSERLEGACYKEY,
             c.CONVCREATEDDATE,
             c.LASTUPDATEDBYUSERLEGACYKEY,
             c.LASTUPDATEDDATE,
             'o_abc_permit',
             c.LEGACYKEY,
             'Y',
              c.text);         
      
    t_count := t_count +1; 
    
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;   
