--Run time: 2 Minutes
declare
  t_EncryptSSN      varchar2(4000);
  t_EncryptDOB      varchar2(4000);
  t_EncryptSSNDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'SSNTINEncrypted');
  t_SSNDefId        number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'SSNTIN');
  t_EncryptDOBDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'BirthDateEncrypted');
  t_DOBDefId        number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'BirthDate');
  t_LogTransId      number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_LogTransId := api.pkg_logicaltransactionupdate.CurrentTransaction();
  for i in (select le.objectid, le.ssntin, le.BirthDate
              from query.o_abc_legalentity le) loop
    if i.ssntin is not null then
      --SSN TIN
      t_EncryptSSN := abc.pkg_abc_encryption.encrypt(i.ssntin);
      --Set Encrypted Column
      insert into possedata.objectcolumndata d
      (
      d.AttributeValue,
      d.SearchValue,
      d.objectid,
      d.ColumnDefId,
      d.logicaltransactionid
      )
      values 
      (
      t_EncryptSSN,
      lower(substrb(t_EncryptSSN, 1, 20)),
      i.objectid,
      t_EncryptSSNDefId,
      t_LogTransId
      );
      --Update visible column
      update possedata.objectcolumndata d
         set d.AttributeValue = '*******' || substr(i.ssntin, -2, 2),
             d.SearchValue = lower(substrb('*******' || substr(i.ssntin, -2, 2), 1, 20))
       where d.objectid = i.objectid
         and d.ColumnDefId = t_SSNDefId;
    end if;
    
    if i.birthdate is not null then
      --Date of Birth
      t_EncryptDOB := abc.pkg_abc_encryption.encrypt(i.birthdate);
      --Set Encrypted Column
      insert into possedata.objectcolumndata d
      (
      d.AttributeValue,
      d.SearchValue,
      d.objectid,
      d.ColumnDefId,
      d.logicaltransactionid
      )
      values 
      (
      t_EncryptDOB,
      lower(substrb(t_EncryptDOB, 1, 20)),
      i.objectid,
      t_EncryptDOBDefId,
      t_LogTransId
      );
    --Update/remove visible column
    delete from possedata.objectcolumndata d
     where d.objectid = i.objectid
       and d.ColumnDefId = t_DOBDefId;
    end if;
  end loop;  
  api.pkg_logicaltransactionupdate.EndTransaction;
end;
/
commit;
/