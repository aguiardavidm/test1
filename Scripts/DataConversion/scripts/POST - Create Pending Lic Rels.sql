declare
  t_LicenseLEEP      number := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Licensee');
  t_LELicenseEP      number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_License', 'Licensee');
  t_LicEstabEP       number := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Establishment');
  t_EstabLicEP       number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_License', 'Establishment');
  t_LicenseLERelDef  number := api.pkg_configquery.ObjectDefIdForName('r_ABC_LicenseLicenseeLE');
  t_LicenseESTRelDef number := api.pkg_configquery.ObjectDefIdForName('r_ABC_LicenseEstablishment');
  t_RelId            number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for i in(select l.objectid LicenseId, le.objectid LegalEntityId
             from abc11p.lic_pend p 
             join dataconv.o_abc_license l on l.legacykey = p.cnty_muni_cd || p.lic_type_cd || p.muni_ser_num || p.gen_num
             join dataconv.o_abc_legalentity le on le.legacykey = substr(l.legacykey, 1,4) || '-' || 
                                                                  substr(l.legacykey, 5,2) || '-' || 
                                                                  substr(l.legacykey, 7,3) || '-' || 
                                                                  substr(l.legacykey, 10,3)) loop
             dbms_output.put_line('1');

    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_LicenseLERelDef,
      4,
      4,
      t_LicenseLERelDef,
      null,
      null,
      4,
      t_LicenseLERelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_LicenseLEEP,
      i.Licenseid,
      i.legalentityid  
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_LELicenseEP,
      i.legalentityid,
      i.Licenseid
    );
  end loop;
  for i in(select l.objectid LicenseId, e.objectid EstablishmentId
             from abc11p.lic_pend p 
             join dataconv.o_abc_license l on l.legacykey = p.cnty_muni_cd || p.lic_type_cd || p.muni_ser_num || p.gen_num
             join dataconv.o_Abc_Establishment e on e.legacykey = substr(l.legacykey, 1,4) || '-' || 
                                                                  substr(l.legacykey, 5,2) || '-' || 
                                                                  substr(l.legacykey, 7,3) || '-' || 
                                                                  substr(l.legacykey, 10,3)) loop

    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_LicenseESTRelDef,
      4,
      4,
      t_LicenseESTRelDef,
      null,
      null,
      4,
      t_LicenseESTRelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_LicEstabEP,
      i.LicenseId,
      i.establishmentid  
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EstabLicEP,
      i.establishmentid,
      i.LicenseId
    );
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;