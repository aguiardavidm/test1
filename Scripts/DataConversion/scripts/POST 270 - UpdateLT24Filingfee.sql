declare
  t_ColumnDefId number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'AdditionalWarehouseSalesroomFe');
begin
  for i in (select d.objectid, d.AdditionalWarehouseSalesroomFe
              from query.o_ABC_License d
             where d.AdditionalWarehouseSalesroomFe is null
               and d.LicenseTypeCode = '24') loop
    insert into possedata.objectcolumndata
    (
    LOGICALTRANSACTIONID,
    OBJECTID,
    COLUMNDEFID,
    ATTRIBUTEVALUE
    )
    values
    (
    1,
    i.objectid,
    t_ColumnDefId,
    0
    );
  end loop;
end;