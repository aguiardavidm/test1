-- Created on 4/30/2015 by KEATON 
declare 
  -- Local variables here
  t_ColumnDef08 number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'RestrictedBreweryProduction');
  t_ColumnDef11 number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'LimitedBreweryProduction');
  --t_ColumnDef13 number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'StateOfRegistration'); Transit Related Not known
  t_ColumnDef18 number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'SupplementaryLimitedDistillery');
  t_ColumnDef21 number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'PlenaryWineryProduction');
  t_ColumnDef22 number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'FarmWineryProduction');
  t_ColumnDef41 number := api.pkg_configquery.ColumnDefIdForName('o_ABC_License', 'OutOfStateWineryProduction');
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.Resettransaction();
  
  for c in (select l.objectid, lt.code
   from dataconv.o_abc_license l
   join dataconv.o_abc_licensetype lt on lt.legacykey = l.licensetypelk
  where l.licensetypelk in (select lt.objectid
                               from dataconv.o_abc_licensetype lt
                              where lt.code in ('08', '11', '13', '18', '21', '22', '41', 'OSWW')) ) loop
   /* Restricted Brewery*/ 
   if c.Code = '08' then
   delete
     from possedata.ObjectColumnData
    where ObjectId = c.ObjectId
      and ColumnDefId = t_ColumnDef08;
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
       c.ObjectId,
       t_ColumnDef08,
       null EffectiveStartDate,
       null EffectiveEndDate,
       null AttributeValue, 
       (select dlv.DomainValueId
        from api.domains  d
        join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
       where d.Name = 'ABC Restricted Brewery Production'
         and dlv.AttributeValue = 'UP TO 1,000 BBLS')AttributeValueId,
       null SearchValue,
       null NumericSearchValue
  from query.o_ABC_License l  
 where l.objectid = c.objectid
 and not exists (select 1 
                     from possedata.ObjectColumnData
                    where ObjectId = c.ObjectId
                      and ColumnDefId = t_ColumnDef08
                      and ObjectId = l.objectid);
  end if;          
    /* Limited Brewery*/ 
  if c.Code = '11' then
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
       c.ObjectId,
       t_ColumnDef11,
       null EffectiveStartDate,
       null EffectiveEndDate,
       null AttributeValue, 
       (select dlv.DomainValueId
        from api.domains  d
        join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
       where d.Name = 'ABC Limited Brewery Production'
         and dlv.AttributeValue = 'UP TO 50,000 BBLS')AttributeValueId,
       null SearchValue,
       null NumericSearchValue
  from query.o_ABC_License l  
 where l.objectid = c.objectid
 and not exists (select 1 
                     from possedata.ObjectColumnData ocd2
                    where ocd2.ObjectId = c.ObjectId
                      and ocd2.ColumnDefId = t_ColumnDef11
                      and ObjectId = l.objectid);
  end if;  
    /* Supplementary Limited Distillery*/ 
  if c.Code = '18' then
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
       c.ObjectId,
       t_ColumnDef18,
       null EffectiveStartDate,
       null EffectiveEndDate,
       null AttributeValue, 
       (select dlv.DomainValueId
        from api.domains  d
        join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
       where d.Name = 'ABC Supplementary Limited Distillery Production'
         and dlv.AttributeValue = 'UP TO 5,000 GALS')AttributeValueId,
       null SearchValue,
       null NumericSearchValue
  from query.o_ABC_License l  
 where l.objectid = c.objectid
 and not exists  (select 1 
                     from possedata.ObjectColumnData ocd3
                    where ocd3.ObjectId = c.ObjectId
                      and ocd3.ColumnDefId = t_ColumnDef18
                      and ObjectId = l.objectid);
  end if;
      
 /* Farm Winery*/ 
  if c.Code = '22' then
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
       c.ObjectId,
       t_ColumnDef22,
       null EffectiveStartDate,
       null EffectiveEndDate,
       null AttributeValue, 
       (select dlv.DomainValueId
        from api.domains  d
        join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
       where d.Name = 'ABC Farm Winery Production'
         and dlv.AttributeValue = 'UP TO 1,000 GALLONS')AttributeValueId,
       null SearchValue,
       null NumericSearchValue
  from query.o_ABC_License l  
 where l.objectid = c.objectid
 and not exists  (select 1 
                     from possedata.ObjectColumnData
                    where ObjectId = c.ObjectId
                      and ColumnDefId = t_ColumnDef22
                      and ObjectId = l.objectid);
  end if;

 /* Plenary Winery*/ 
  if c.Code = '21' then
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
       c.ObjectId,
       t_ColumnDef21,
       null EffectiveStartDate,
       null EffectiveEndDate,
       null AttributeValue, 
       (select dlv.DomainValueId
        from api.domains  d
        join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
       where d.Name = 'ABC Plenary Winery Production'
         and dlv.AttributeValue = 'Less than 50,000 gallons')AttributeValueId,
       null SearchValue,
       null NumericSearchValue
  from query.o_ABC_License l  
 where l.objectid = c.objectid
 and not exists  (select 1 
                     from possedata.ObjectColumnData
                    where ObjectId = c.ObjectId
                      and ColumnDefId = t_ColumnDef21
                      and ObjectId = l.objectid);
  end if;  

  /* Out Of State Winery*/ 
  if c.Code in ('OSWW', '41') then
    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
       c.ObjectId,
       t_ColumnDef41,
       null EffectiveStartDate,
       null EffectiveEndDate,
       null AttributeValue, 
       (select dlv.DomainValueId
        from api.domains  d
        join api.domainlistofvalues dlv on dlv.DomainId = d.DomainId
       where d.Name = 'ABC Out Of State Winery Production'
         and dlv.AttributeValue = 'Less than 1,000 gallons')AttributeValueId,
       null SearchValue,
       null NumericSearchValue
  from query.o_ABC_License l  
 where l.objectid = c.objectid
 and not exists  (select 1 
                     from possedata.ObjectColumnData
                    where ObjectId = c.ObjectId
                      and ColumnDefId = t_ColumnDef41
                      and ObjectId = l.objectid);
  end if;
  
    end loop;            
  
  api.pkg_logicaltransactionupdate.Endtransaction();
end;
