declare 

  t_DateColumnDefId number := api.pkg_configquery.ColumnDefIdForName('r_abc_permitcooplicenses','CoOpMemberEffectiveDate');
  t_CharDate        varchar2(100);
begin
  -- Test statements here
  for i in (Select m.dt_effective, r.objectid
              from dataconv.r_abc_permitcooplicenses r
              join abc11p.coop_mem m on to_char(m.coop_num || 'COOP') = r.o_abc_permit and m.Cnty_Muni_cd || m.lic_type_cd || m.muni_ser_num || m.gen_num = r.o_abc_license)
  loop
    if i.dt_effective is not null then
      
      t_chardate := to_char(i.dt_effective, 'YYYY-MM-DD HH24:MI:SS');
      
      delete from possedata.objectcolumndata d
      where d.objectid = i.objectid
         and d.ColumnDefId = t_DateColumnDefId;
    
      insert into possedata.objectcolumndata d
         (d.LogicalTransactionId, d.ObjectId, d.AttributeValue, d.columndefid)
      values
         (1,i.objectid, t_Chardate, t_DateColumnDefId);
         
    end if;
  end loop;
commit;
end;
/

