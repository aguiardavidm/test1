--run time: 20 minutes
create table abc11p.address_xref1 as
  ( select addresstype ||
       postalcode ||
       country ||
       provincestate ||
       street ||
       streetnumber ||
       streetnumbersuffix ||
       unitnumber ||
       line2 ||
       citytown ||
       streetdirection ||
       otheraddress ||
       po_additionaldeliveryinfo ||
       po_postofficebox ||
       po_stationinformation ||
       rr_additionaladdressinfo ||
       rr_ruralrouteidentifier ||
       rr_stationinformation ||
       gd_generaldeliveryindicator ||
       gd_stationinformation ||
       zipcodeextension ADDRESS,
       count(*) NUM_OF_DUP,
       max(am.legacykey) CorrectKey
    from dataconv.o_abc_address am
    group by addresstype ||
       postalcode ||
       country ||
       provincestate ||
       street ||
       streetnumber ||
       streetnumbersuffix ||
       unitnumber ||
       line2 ||
       citytown ||
       streetdirection ||
       otheraddress ||
       po_additionaldeliveryinfo ||
       po_postofficebox ||
       po_stationinformation ||
       rr_additionaladdressinfo ||
       rr_ruralrouteidentifier ||
       rr_stationinformation ||
       gd_generaldeliveryindicator ||
       gd_stationinformation ||
       zipcodeextension);
     
-- Create/Recreate indexes 
create index ABC11P.compositaddressXR1_ix1 on ABC11P.ADDRESS_XREF1 (address);
create index ABC11P.legacykeyXR1_ix2 on ABC11P.ADDRESS_XREF1 (correctkey);     
     
create table abc11p.address_xref2 as
  ( select addresstype ||
       postalcode ||
       country ||
       provincestate ||
       street ||
       streetnumber ||
       streetnumbersuffix ||
       unitnumber ||
       line2 ||
       citytown ||
       streetdirection ||
       otheraddress ||
       po_additionaldeliveryinfo ||
       po_postofficebox ||
       po_stationinformation ||
       rr_additionaladdressinfo ||
       rr_ruralrouteidentifier ||
       rr_stationinformation ||
       gd_generaldeliveryindicator ||
       gd_stationinformation ||
       zipcodeextension ADDRESS,
       am.legacykey OldKey
    from dataconv.o_abc_address am);

-- Create/Recreate indexes 
create index ABC11P.compositaddressXR2_ix1 on ABC11P.ADDRESS_XREF2 (address);
create index ABC11P.legacykeyXR2_ix2 on ABC11P.ADDRESS_XREF2 (OldKey);

create table abc11p.address_XRef3
       (NewAddressLK varchar2(100),
        OldAddressLK varchar2(100));
    
create index ABC11P.NewAddressLK_IX1 on ABC11P.address_XRef3 (NewAddressLK);
create index ABC11P.OldAddressLK_IX2 on ABC11P.address_XRef3 (OldAddressLK);    

declare 

t_count number := 0;

begin

  for c in (select Address,
             correctKey 
        from abc11p.address_xref1 xr1) loop
  
    for d in (select OldKey
          from abc11p.address_xref2 xr2
          where c.ADDRESS = xr2.Address) loop
      insert into abc11p.address_XRef3
       (NewAddressLK,
       OldAddressLK)
      values(
             c.correctKey,
       d.OldKey);
     t_count := t_count +1;
    end loop;        
  
  end loop;    
 dbms_output.put_line(t_count);
end;
/

alter table  abc11p.address_Xref3 add (processeddate date);

declare 

t_count number := 0;
t_sqlcur varchar2(1000);
t_sqlupt varchar2(1000);
type cur_type is ref cursor;
addcon cur_type;
type udt_legacykeys is table of varchar2(100) index by pls_integer;
t_legacykeys udt_legacykeys;
type udt_OLDLKs is table of varchar2(100)  index by pls_integer;
t_OLDLKs udt_OLDLKs;
type udt_NLKs is table of varchar2(100)  index by pls_integer;
t_NLKs udt_NLKs;

begin

  for c in (select d.owner,d.table_name,d.constraint_name, dc.column_name --, d.*
        from dba_constraints d
        join dba_cons_columns dc on dc.constraint_name = d.constraint_name
      where constraint_type = 'R'
         and r_constraint_name = 'O_ABC_ADDRESS_PK'
         and d.table_name not in ('R_WARNINGADR', 'R_ABC_ADDRESSVEHICLE', 'R_ABC_LICENSEWAREHOUSEADDRESS')
      ) loop -- loop 1
    
  t_sqlcur := 'select legacykey, ' || c.column_name 
              || ' OLDLK , (select NewAddressLK from abc11p.address_XRef3 xr3 where xr3.OldAddressLK = '
              || c.column_name ||' ) NLK from ' || c.owner || '.' || c.table_name || ' t' ||
              ' where ' || c.column_name || ' is not null';
  
    open addcon for t_sqlcur; 
     fetch addcon bulk collect  into t_legacykeys, t_OLDLKs, t_NLKs;
      if addcon%rowcount = 0 then
        continue;
      end if;
      for d in 1..t_legacykeys.Count loop -- loop 2
        begin
        t_sqlupt := 'update ' || c.owner 
                    || '.' || c.table_name || ' tab set ' 
                    || c.column_name || ' = '|| '''' 
                    || t_NLKs(d) || '''' 
                    || ' where tab.legacykey = '   
                    ||'''' || t_legacykeys(d) ||'''';
        execute immediate  t_sqlupt;
        t_sqlupt := 'update abc11p.address_Xref3 xr3 set processeddate = ' ||'''' || sysdate || ''''  || ' where xr3.oldaddresslk  = ' || '''' 
                    || replace(t_OLDLKs(d), '''', '''''') || '''' ;
        execute immediate  t_sqlupt;            
        t_count := t_count +1;
        
        exception when others then
          dbms_output.put_line(t_sqlupt || SQLERRM);
        end ; 
        if  mod(t_count, 5000) = 0 
          then commit;
        end if;  
          
      end loop; -- loop 2
  end loop; -- loop 1    
 dbms_output.put_line(t_count);
 commit;
end;
/

delete from 
  dataconv.o_abc_address a
 where a.legacykey not in (select xr3.newaddresslk
                            from abc11p.address_xref3 xr3);
commit;      
/
