declare
  t_EndpointId     number := api.pkg_configquery.endpointidforname('o_ABC_Permit', 'Product');
  t_OthEndpointId  number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_Permit', 'Product');
  t_RelDef         number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitProduct');
  t_RelId          number;
  t_NumberOfCaseCD number :=api.pkg_configquery.ColumnDefIdForName('r_ABC_PermitProduct', 'NumberOfCasesAffected');
  t_PricePerCaseCD number :=api.pkg_configquery.ColumnDefIdForName('r_ABC_PermitProduct', 'PricePerCase');
begin
  for i in (select dp.objectid PermitId, b.objectid BrandId, p.quantity, p.price
            from abc11p.Permit p
            join dataconv.o_abc_permit dp on dp.legacykey = to_char(p.perm_num)
            join dataconv.o_abc_product b on b.registrationnumber = p.brn_reg_num
            where p.perm_stat in ('C', 'H')
              and p.brn_reg_num is not null
               or p.brn_name is not null) loop
    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_RelDef,
      4,
      4,
      t_RelDef,
      null,
      null,
      4,
      t_RelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_EndpointId,
      i.PermitId,
      i.BrandId
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_OthEndpointId,
      i.BrandId,
      i.PermitId
    );
    if i.quantity is not null then
      insert /*+ append */ into possedata.Objectcolumndata_t nologging 
      select 1 LogicalTransactionId,
             t_RelId,
             t_NumberOfCaseCD,
             null EffectiveStartDate,
             null EffectiveEndDate,
             i.quantity AttributeValue, 
             null AttributeValueId,
             null SearchValue,
             null NumericSearchValue
        from dual
       where not exists (select 1 
                           from possedata.ObjectColumnData
                          where ObjectId = t_RelId
                            and ColumnDefId = t_NumberOfCaseCD);
    end if;
    if i.price is not null then
      insert /*+ append */ into possedata.Objectcolumndata nologging 
      select 1 LogicalTransactionId,
             t_RelId,
             t_PricePerCaseCD,
             null EffectiveStartDate,
             null EffectiveEndDate,
             i.price AttributeValue, 
             null AttributeValueId,
             null SearchValue,
             null NumericSearchValue
        from dual
       where not exists (select 1 
                           from possedata.ObjectColumnData
                          where ObjectId = t_RelId
                            and ColumnDefId = t_PricePerCaseCD);
    end if;
  end loop;
  commit;
end;