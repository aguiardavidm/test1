--Run time: 45 seconds
-- Created on 9/11/2014 by ADMINISTRATOR 
declare 
  -- Local variables here
  --i integer;
  t_RowsProcessed number := 0;
begin
  if user != 'POSSEDBA' then
    api.pkg_errors.RaiseError(-20000, 'Script must be run as POSSEDBA');
  end if;
  -- Test statements here
  for i in (select p.legacykey PRODUCTLEGACYKEY, v.legacykey VINTAGELEGACYKEY
              from abc11p.brn_vintage pv
              join dataconv.o_abc_vintage v on v.Year = pv.vintage
              join dataconv.o_abc_product p on p.registrationnumber = pv.brn_reg_num
             where not exists (select 1
                                 from dataconv.r_Abc_Producttovintage r
                                where p.legacykey = r.o_abc_product 
                                  and v.legacykey = r.o_abc_vintage)) loop
                                                          
    insert into dataconv.r_Abc_Producttovintage
      (
      o_abc_product,
      o_abc_vintage
      )
      values
      (
      I.Productlegacykey,
      I.Vintagelegacykey
      );
    t_RowsProcessed := t_RowsProcessed+1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
    end if;
  end loop;
  dbms_output.put_line(t_RowsProcessed || ' Rows processed.');
  commit;
end;