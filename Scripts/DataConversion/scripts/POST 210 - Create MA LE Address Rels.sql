declare
  t_MailEndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'MailingAddress');
  t_MailOthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'MailingAddress');
  t_MailRelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_MailingLE');
  t_PhysEndpointId    number := api.pkg_configquery.endpointidforname('o_ABC_LegalEntity', 'PhysicalAddress');
  t_PhysOthEndpointId number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'PhysicalAddress');
  t_PhysRelDef        number := api.pkg_configquery.ObjectDefIdForName('r_ABC_PhysicalLE');
  t_RelId             number;
begin
  for i in (select le.objectid Leid, a.ObjectId AddressId
              from dataconv.o_abc_legalentity le
              join abc11p.ma_applicant_int_t ma on ma.applicant_id || 'MA' = le.legacykey
              join dataconv.o_abc_address a on a.legacykey = ma.applicant_address || ma.applicant_city || ma.applicant_state || ma.applicant_zip) loop
    --Create the Mailing Address Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_MailRelDef,
      4,
      4,
      t_MailRelDef,
      null,
      null,
      4,
      t_MailRelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_MailEndpointId,
      i.Leid,
      i.AddressId
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_MailOthEndpointId,
      i.AddressId,
      i.Leid
    );
    --Create the Physical Address Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_PhysRelDef,
      4,
      4,
      t_PhysRelDef,
      null,
      null,
      4,
      t_PhysRelDef,
      null,
      null
    );

   
  --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_PhysEndpointId,
      i.Leid,
      i.AddressId
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_PhysOthEndpointId,
      i.AddressId,
      i.Leid
    );
  end loop;
  commit;
end;