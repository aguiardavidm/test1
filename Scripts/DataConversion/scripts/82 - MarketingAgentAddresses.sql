--Run time: 1 second
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
  update abc11p.ma_applicant_int_t am
        set am.errortext = a_ErrorText,
            am.errorcode = a_ErrorCode
      where am.rowid = a_RowId;  
  
    commit;
  end;

begin

  select legacykey
    into t_ConvUserKey 
   from dataconv.u_users u
  where u.oraclelogonid = 'ABCCONV';

  for c in (select t.applicant_address || applicant_city || applicant_state  ||applicant_zip LegacyKey,
                   t_ConvUserKey UserKey,
                   sysdate ConvDate,
                   t.applicant_address || applicant_city  || applicant_state || applicant_zip  OthAdd
             from abc11p.ma_applicant_int_t t
            where t.processed is null
            group by t.applicant_address || applicant_city || applicant_state   ||applicant_zip,
                   t_ConvUserKey,
                   sysdate,
                   t.applicant_address || applicant_city  || applicant_state || applicant_zip ) loop
    begin
    insert into dataconv.o_abc_address a
   (legacykey,
	createdbyuserlegacykey,
	convcreateddate,
	lastupdatedbyuserlegacykey,
    Lastupdateddate,
	rowstatus, 
	addresstype, 
	otheraddress, 
	isconverted )
    values ( c.legacykey, --
             c.UserKey, --
             c.ConvDate , --
             c.UserKey, --
             c.ConvDate,   --
             'N',
             'OTHER',
             c.OthAdd, --
             'Y' );    
      
    t_count := t_count +1; 
    
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode,null);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;
