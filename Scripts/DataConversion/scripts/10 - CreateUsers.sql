--get around someweirdness from the data export/import... Not needed any more?
--drop table abc11p.abc_user_names;
create table abc11p.abc_user_names as (select * from system.abc_user_names);
drop table system.abc_user_names;
/
-- Created on 8/25/2014 by Keaton 
--Run time < 1sec
declare 
  -- Local variables here
  i integer;
  t_count number := 0;
begin
  -- Test statements here
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for c in(select aum.username, 
                  aum.fullname,
                  substr(aum.fullname,instr(aum.fullname,',',1)+2) FirstName,
                  substr(aum.fullname,1,instr(aum.fullname,',',1)-1) LastName,
                  substr(aum.fullname,instr(aum.fullname,',',1)+2,1) ||
                  replace(rtrim(substr(aum.fullname,1,instr(aum.fullname,' ',1)-1),','),'-','') UName, 
                  aum.emp_id
            from abc11p.abc_user_names aum
            where aum.username like 'OPS$%'
              and not exists (select *
                                from api.users u
                               where u.shortname = upper(substr(aum.fullname,instr(aum.fullname,',',1)+2,1) ||
                  replace(rtrim(substr(aum.fullname,1,instr(aum.fullname,' ',1)-1),','),'-','')))) loop
  begin
  
  api.pkg_logicaltransactionupdate.ResetTransaction(); 
   
  i := api.pkg_userupdate.New('Default',
                          c.Fullname,
                          upper(c.UName),
                          upper(c.UName),
                          'njdev');
  
  api.pkg_columnupdate.SetValue(i,'FirstName', c.Firstname);
  api.pkg_columnupdate.SetValue(i,'LastName', c.Lastname);
  api.pkg_columnupdate.SetValue(i,'UserType', 'Internal');
  
  t_count := t_count +1;
  
  exception when others then
    dbms_output.put_line('UserName: '||c.username||' '||SQLERRM);
  end;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  
  end loop;
  
    
 api.pkg_logicaltransactionupdate.EndTransaction();
  
  dbms_output.put_line(t_count);
  
end;
/
-- Update user table to be a fill table
update dataconv.posseconvtables pct 
  set pct.fill = 'Y'
 where pct.tablename = 'U_USERS';
/
commit; 
/
--Run Filltables.bat