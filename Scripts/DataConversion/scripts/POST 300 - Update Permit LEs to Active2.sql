-- Created on 4/30/2015 by KEATON 
declare 
  -- Local variables here
  t_ColumnDef number := api.pkg_configquery.ColumnDefIdForName('o_ABC_LegalEntity', 'Active');
  
 begin 

for c in ( select le.objectid
        from dataconv.o_abc_legalentity le
       where le.legacykey in (select to_char(p.abc11puk) || 'PERM2'
                   from abc11p.permit p)) loop
   /* Make Legal Entites active that came from the Permitee name*/ 

    insert /*+ append */ into possedata.Objectcolumndata nologging 
    select 1 LogicalTransactionId,
       c.ObjectId,
       t_ColumnDef,
       null EffectiveStartDate,
       null EffectiveEndDate,
       'Y' AttributeValue, 
       null AttributeValueId,
       null SearchValue,
       null NumericSearchValue
  from query.o_ABC_LegalEntity le  
 where le.objectid = c.objectid
 and not exists (select 1 
                     from possedata.ObjectColumnData
                    where ObjectId = c.ObjectId
                      and ColumnDefId = t_ColumnDef
                      and ObjectId = le.objectid);

end loop;            
  
end;            
            