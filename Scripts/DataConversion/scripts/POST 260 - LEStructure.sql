--delete all corporate structure rels
--run time 20 min
begin
  for x in (select objectid from dataconv.r_abc_participatesinincludesle r) loop
    delete from rel.storedrelationships where relationshipid = x.objectid;
    delete from objmodelphys.objects where objectid = x.objectid;
    delete from dataconv.r_abc_participatesinincludesle where objectid = x.objectid;
  end loop;
end;

commit;

--get relationship rows into table
--run time: 2 min
create table abcconv.LEStructure_t tablespace conv
as
select le1.objectid participatesObjectId,
       le2.objectid includesobjectid,
       x.whatquery,
       x.participatesIn,
       x.includes,
       x.percentageinterest,
       x.commonvotingshares,
       x.Name_Type_Cd,
       x.RelationshipType,
       x.Name
from (
select 'A' whatquery,
       pi.parent_id_number ParticipatesIn,
       pi.name_mstr_id_num Includes,
       pi.pct_bus_cntrl PercentageInterest,
       pi.num_shares_own CommonVotingShares,
       pi.name_type_cd,
       nvl(listagg(cd.cd_desc, '/') within group (order by pi.parent_id_number), pi.relationship) RelationshipType,
       pi.name
from abc11p.name_mstr pi
left outer join abc11p.position p on p.name_mstr_id_num = pi.name_mstr_id_num
left outer join abc11p.code_desc cd on cd.code = p.position_cd and cd.id = 'PS'
where pi.parent_id_number is not null
and pi.parent_id_number <> 0
and pi.name_stat <> 'X'
--no need to exclude 2.5 and 2.6 as they are not found in this data set.
--no need to exclude srce_grp_cd of 'GENERAL' as they are not found in this data set.
group by pi.parent_id_number,
         pi.name_mstr_id_num,
         pi.pct_bus_cntrl,
         pi.num_shares_own,
         pi.name_type_cd,
         pi.relationship,
         pi.name
union all         
select 'B',i.name_mstr_id_num ParticipatesIn,
       pi.name_mstr_id_num Includes,
       pi.pct_bus_cntrl PercentageInterest,
       pi.num_shares_own CommonVotingShares,
       pi.name_type_cd,
       nvl(listagg(cd.cd_desc, '/') within group (order by pi.parent_id_number),pi.relationship) RelationshipType,
       pi.name
from abc11p.name_mstr i
join abc11p.name_mstr pi on pi.abc_num = i.abc_num
                         and pi.name_mstr_id_num <> i.name_mstr_id_num 
                         and pi.srce_grp_cd != 'GENERAL' 
                         and pi.name_stat != 'X' 
                         and pi.name_type_cd in ('10A-INTEREST','10.1-CORP','10.10-AGENT','SOLICITOR','8A-INTEREST','6A-INTEREST','8.1-CORP','8.10-AGENT','6.1-CORP' ,'6.10-AGENT')
left outer join abc11p.position p on p.name_mstr_id_num = pi.name_mstr_id_num
left outer join abc11p.code_desc cd on cd.code = p.position_cd and cd.id = 'PS'
where i.name_type_cd = '2.1-LICENSEE'
and i.srce_grp_cd != 'GENERAL'
and i.name_stat != 'X'
and pi.parent_id_number is null
group by i.name_mstr_id_num,
         pi.name_mstr_id_num,
         pi.pct_bus_cntrl,
         pi.num_shares_own,
         pi.name_type_cd,
         pi.relationship,
         pi.name
) x
join dataconv.o_abc_legalentity le1 on le1.legacykey = to_char(x.participatesin)
join dataconv.o_abc_legalentity le2 on le2.legacykey = to_char(x.includes)

alter table abcconv.lestructure_t add relationshipid number(11,0) null;


--recreate all Corporate Structure relationships.
--run time 1.5 hours
declare
  t_IncludesEP      number := api.pkg_configquery.EndPointIdForName('o_ABC_LegalEntity', 'Includes');
  t_ParticipatesEP  number := api.pkg_configquery.OtherEndPointIdForName('o_ABC_LegalEntity', 'Includes');
  t_ParticipatesIncludesRelDef number := api.pkg_configquery.ObjectDefIdForName('r_ABC_ParticipatesInIncludesLE');
  t_PercentageInterestDefId number := api.pkg_configquery.ColumnDefIdForName('r_ABC_ParticipatesInIncludesLE', 'PercentageInterest');
  t_CommonVotingSharesDefId number := api.pkg_configquery.ColumnDefIdForName('r_ABC_ParticipatesInIncludesLE', 'CommonVotingShares');
  t_RelationshipTypeDefId number := api.pkg_configquery.ColumnDefIdForName('r_ABC_ParticipatesInIncludesLE', 'RelationshipType');
  t_LogTransId number;
  t_RelId            number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  t_LogTransId := api.pkg_logicaltransactionupdate.CurrentTransaction();
  for i in (select l.participatesobjectid,
                   l.includesobjectid,
                   l.percentageinterest,
                   l.commonvotingshares,
                   l.relationshiptype
            from abcconv.lestructure_t l
            where l.relationshipid is null) loop

    --Create the Legal Entity Rel
    select possedata.ObjectId_s.nextval
      into t_RelId 
     from dual;

    --Insert into ObjModelPhys.Objects
    insert into objmodelphys.Objects (
      LogicalTransactionId,
      CreatedLogicalTransactionId,
      ObjectId,
      ObjectDefId,
      ObjectDefTypeId,
      ClassId,
      InstanceId,
      EffectiveStartDate,
      EffectiveEndDate,
      ConfigReadSecurityClassId,
      ConfigReadSecurityInstanceId,
      ObjectReadSecurityClassId,
      ObjectReadSecurityInstanceId
    ) values (
      1, -- Hard coded logical Transaction to 1
      1, -- Hard coded logical Transaction to 1
      t_RelId,
      t_ParticipatesIncludesRelDef,
      4,
      4,
      t_ParticipatesIncludesRelDef,
      null,
      null,
      4,
      t_ParticipatesIncludesRelDef,
      null,
      null
    );
   
   --Insert Into Rel.StoredRelationships    

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_IncludesEP,
      i.participatesobjectid,
      i.includesobjectid
    );

    insert into rel.StoredRelationships (
      RelationshipId,
      EndPointId,
      FromObjectId,
      ToObjectId
    ) values (
      t_RelId,
      t_ParticipatesEP,
      i.includesobjectid,
      i.participatesobjectid
    );
    
    if i.percentageinterest is not null then
      insert into possedata.objectcolumndata d
      (
      d.AttributeValue,
      d.SearchValue,
      d.objectid,
      d.ColumnDefId,
      d.logicaltransactionid
      )
      values 
      (
      i.percentageinterest,
      lower(i.percentageinterest),
      t_RelId,
      t_PercentageInterestDefId,
      t_LogTransId
      );
    end if;
    
    if i.commonvotingshares is not null then
      insert into possedata.objectcolumndata d
      (
      d.AttributeValue,
      d.SearchValue,
      d.objectid,
      d.ColumnDefId,
      d.logicaltransactionid
      )
      values 
      (
      i.commonvotingshares,
      lower(i.commonvotingshares),
      t_RelId,
      t_CommonVotingSharesDefId,
      t_LogTransId
      );
    end if;
    
    if i.relationshiptype is not null then
      insert into possedata.objectcolumndata d
      (
      d.AttributeValue,
      d.SearchValue,
      d.objectid,
      d.ColumnDefId,
      d.logicaltransactionid
      )
      values 
      (
      i.relationshiptype,
      lower(substr(i.relationshiptype,1,20)),
      t_RelId,
      t_RelationshipTypeDefId,
      t_LogTransId
      );
    end if;
    
    update abcconv.lestructure_t
    set relationshipid = t_RelId
    where participatesobjectid = i.participatesobjectid
    and includesobjectid = i.includesobjectid;
    
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;