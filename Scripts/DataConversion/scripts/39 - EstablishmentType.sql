--Run Time: 1 second
-- Created on 6/19/2014 by PAUL
declare 
  -- Local variables here
  i integer;
  t_RowsProcessed   number := 0;
  t_EstabTypeObjDef number := api.pkg_configquery.ObjectDefIdForName('o_ABC_EstablishmentType');
  t_NewEstabTypeId  number;
begin
  -- Test statements here
  api.pkg_logicaltransactionupdate.ResetTransaction;
  for c in (select l.code, l.name, l.CreatedLogicalTransactionId, l.LogicalTransactionId, l.Active
              from query.o_Abc_Licensetype l
             where l.active = 'Y'
               and not exists (select 1 
                                 from query.o_abc_establishmenttype et
                                where et.name = l.name)) loop

	t_NewEstabTypeId := api.pkg_objectupdate.New(t_EstabTypeObjDef);
    api.pkg_columnupdate.setvalue(t_NewEstabTypeId, 'Name', c.name);
    api.pkg_columnupdate.setvalue(t_NewEstabTypeId, 'Active', 'Y');
    t_RowsProcessed := t_RowsProcessed + 1; 
  end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  api.pkg_logicaltransactionupdate.endTransaction;
  
end;
/
-- Update user table to be a fill table
update dataconv.posseconvtables pct 
  set pct.fill = 'Y'
 where pct.tablename = 'O_ABC_ESTABLISHMENTTYPE';
--Run Filltables.bat
commit;