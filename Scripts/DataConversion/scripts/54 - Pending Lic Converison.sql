--Run Time: 15 seconds
-- Created on 2/28/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
  t_ConversionNote long;
  t_IssueDate date; 
  t_EffectiveDate date;
  t_ExpirationDate date;
  t_LicenseeEstLK  varchar2(40);
  t_LicenseTypeLK  varchar2(40);
  t_DefaultClerkLK varchar2(40);
  t_RowsProcessed  number := 0;
begin
  -- Test statements here
  for i in (select l.*, (select legacykey
                           from dataconv.o_abc_masterlicense m 
                          where substr(m.legacykey, 0, 9) = l.cnty_muni_cd || l.lic_type_cd || l.muni_ser_num) MasterLK
              from abc11p.lic_pend l
             where not exists(select 1
                                from dataconv.o_abc_license
                               where licensenumber = l.license_num)
               and l.lic_type_cd != '24') loop
    t_ConversionNote := 'Converted LIC_PEND Information' || chr(10) ||
                        ': ' || i.Trans_Num || chr(10) ||
                        'Initial Fee: ' || i.INIT_FEE || chr(10) ||
                        'NJ Bond?: ' || i.NJ_BOND_IND || chr(10) ||
                        '' || i.BATF_PERM_IND || chr(10) ||
                        'Notice?: ' || i.NOTICE_IND || chr(10) ||
                        '' || i.SBI_FEE_IND || chr(10) ||
                        'Date Investigation Referred: ' || i.DT_INVST_REFRD || chr(10) ||
                        'Date Investigation Back:' || i.DT_INVST_BACK || chr(10) ||
                        'Investigation Agency: ' || i.INVEST_AGENCY || chr(10) ||
                        'Investigation Number: ' || i.INVEST_NUM || chr(10) ||
                        'Investigation Association: ' || i.INVEST_ASSN || chr(10) ||
                        '' || i.AFF_OF_PUB || chr(10) ||
                        'Notice to Municipality: ' || i.NOTICE_TO_MUNI || chr(10) ||
                        'Full Application?: ' || i.FULL_APP_IND;

    select min(t.dt_term_start), max(t.dt_term_start), max(t.dt_term_end)
      into t_IssueDate, t_EffectiveDate, t_ExpirationDate
      from abc11p.Lic_Term t
     where t.CNTY_MUNI_CD || t.LIC_TYPE_CD || t.MUNI_SER_NUM || t.GEN_NUM = i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM;
     --Get the Licensee/Establishment LKs
      select max(nm.name_mstr_id_num) --some don't have a current licensee?
        into t_LicenseeEstLK
        from abc11p.name_mstr nm
       where nm.abc_num = i.CNTY_MUNI_CD || '-' || i.LIC_TYPE_CD || '-' || i.MUNI_SER_NUM || '-' || i.GEN_NUM
         and nm.name_type_cd = '2.1-LICENSEE'
         and nm.name_stat = 'C';

    --Get the License Type
    select max(objectid)
      into t_LicenseTypeLK
      from dataconv.o_Abc_Licensetype lt
     where lt.Code = i.lic_type_cd
       and lt.Active = 'Y';

    insert into dataconv.o_Abc_License
    (
    LEGACYKEY,
    CREATEDBYUSERLEGACYKEY,
    CONVCREATEDDATE,
    LASTUPDATEDBYUSERLEGACYKEY,
    LASTUPDATEDDATE,
    LICENSENUMBER,
    LICENSETYPELK,
    STATE,
    EFFECTIVEDATE,
    EXPIRATIONDATE,
    MASTERLK,
    LICENSEELK,
    ESTABLISHMENTLK,
    CONFLICTOFINTEREST,
    CRIMINALCONVICTION
    )
    values
    (
    i.LICENSE_NUM,
    i.CRTD_OPER_ID,
    i.DT_ROW_CRTD,
    i.UPDT_OPER_ID,
    i.DT_ROW_UPDT,
    i.LICENSE_NUM,
    t_LicenseTypeLK,
    'Pending',
    t_EffectiveDate,
    t_ExpirationDate,
    i.MasterLK,
    t_LicenseeEstLK,
    t_LicenseeEstLK,
    'No',
    'No'
    );
    --Create the Conversion Note
    insert into dataconv.n_general
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    parenttablename,
    parentlegacykey,
    locked,
    text
    )
    values
    (
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    'O_ABC_LICENSE',
    i.CNTY_MUNI_CD || i.LIC_TYPE_CD || i.MUNI_SER_NUM || i.GEN_NUM,
    'Y',
    t_ConversionNote
    );
    --Create new application job
    insert into dataconv.j_abc_newapplication
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    LEGACYKEY,
    APPLICATIONRECEIVEDDATE,
    STATUSTAG,
    LicenseLK
    )
    values
    (
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    i.license_num,
    i.dt_appl_filed,
    'NEW',
    i.license_num
    );
begin
    select lt.DefaultClerkId
      into t_DefaultClerkLK
      from query.r_ABC_LicenseTypeDefaultClerk lt
     where lt.LicenseTypeId = t_LicenseTypeLK;
exception when no_data_found then
  null;--what do
end;
    insert into dataconv.p_abc_submitapplication
    (
    createdbyuserlegacykey,
    convcreateddate,
    lastupdatedbyuserlegacykey,
    lastupdateddate,
    LEGACYKEY,
    JOBLEGACYKEY,
    JOBTABLENAME,
    ASSIGNTOUSERLEGACYKEY,
    SEQNUM
    )
    values
    (
    i.crtd_oper_id,
    i.dt_row_crtd,
    i.updt_oper_id,
    i.dt_row_updt,
    i.license_num,
    i.license_num,
    'J_ABC_NEWAPPLICATION',
    t_DefaultClerkLK,
    1
    );
    --Create and relate the LE and Establishment
    if i.appl_name is not null then
      insert into dataconv.o_abc_legalentity
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      LEGACYKEY,
      LEGALNAME,
      mailingaddresslk,
      physicaladdresslk,
      PREFERREDCONTACTMETHOD
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      i.license_num,
      i.appl_name,
      i.abc11puk || 'MAIL',
      i.abc11puk || 'PREM',
      'MAIL'
      );
    end if;
    if i.trade_name_1 || i.trade_name_2 is not null then
      insert into dataconv.o_abc_establishment
      (
      createdbyuserlegacykey,
      convcreateddate,
      lastupdatedbyuserlegacykey,
      lastupdateddate,
      LEGACYKEY,
      DOINGBUSINESSAS,
      mailingaddresslk,
      physicaladdresslk,
      ContactName,
      PreferredContactMethod
      )
      values
      (
      i.crtd_oper_id,
      i.dt_row_crtd,
      i.updt_oper_id,
      i.dt_row_updt,
      i.license_num,
      i.trade_name_1 || ', ' || i.trade_name_2,
      i.abc11puk || 'MAIL',
      i.abc11puk || 'PREM',
      i.appl_name,
      'MAIL'
      );
    end if;
    
    t_RowsProcessed := t_RowsProcessed + 1;
    if mod(t_RowsProcessed, 2000) = 0 then
      commit;
      dbms_output.put_line('Processed ' || t_RowsProcessed || ' rows.');
    end if;
    end loop;
  dbms_output.put_line('Processed ' || t_RowsProcessed || ' total rows.');
  commit;
end;