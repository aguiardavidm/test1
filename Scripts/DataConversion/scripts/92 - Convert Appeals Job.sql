--Run time: 10 seconds
declare 
  t_error     varchar2(4000);
  t_errorcode number;
  t_count     number :=0;
  t_errorCount       number := 0;
  t_ConvUserKey     varchar2(100);
  t_LegalEntityType varchar2(100);
  procedure LogError(a_ErrorText varchar2,
                     a_ErrorCode number,            
                     a_RowId     varchar2)
  is
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
  
  update abc11p.A_tblAppeal_int_t am
        set am.errortext = a_ErrorText,
            am.errorcode = a_ErrorCode
      where am.rowid = a_RowId;  
  
    commit;
  end;


  
begin

  for c in ( select rowid,
          AppealNr LEGACYKEY,
					license_num   LICENSELK,
					Class_A APPEALTYPE,
					DAGAssigned DEPUTYATTORNEYGENERALLK,
					to_date(DateFiled, 'MM/DD/YY HH24:MI:SS') CONVCREATEDDATE,
					to_date(DateClosed, 'MM/DD/YY HH24:MI:SS') COMPLETEDDATE,
					MethodOfClosure || Comment_A METHODOFCLOSURECOMMENTS,
					'Respondent: ' || t.Respondent||
					'Date To OAL: ' || t.DatetoOAL ||
					'Date Returned: ' || t.Datereturnedtoabc text
				from abc11p.A_tblAppeal_int_t	t
            where t.processed is null) loop
    begin
    insert into dataconv.j_ABC_Appeal a
    ( LEGACYKEY,
	  EXTERNALFILENUM,
	  appealnumber ,
	  LICENSELK,
	  APPEALTYPE,
	  attorneygenerallk ,
	  CONVCREATEDDATE,
	  RECEIVEDDATE,
	  COMPLETEDDATE,
	  METHODOFCLOSURECOMMENTS,
	  STATUSTAG
		 )
    values ( c.LEGACYKEY, 
             c.LEGACYKEY, 
             c.LEGACYKEY, 
             c.LICENSELK,
             c.APPEALTYPE,
             c.DEPUTYATTORNEYGENERALLK,
             c.CONVCREATEDDATE,
             c.CONVCREATEDDATE,
             c.COMPLETEDDATE,
             c.METHODOFCLOSURECOMMENTS,
             case when c.COMPLETEDDATE is null then 'NEW' else 'CLOSED' end );    
      
    
  insert into dataconv.n_general
         (createdbyuserlegacykey,
          convcreateddate,
          lastupdatedbyuserlegacykey,
          lastupdateddate,
          parenttablename,
          parentlegacykey,
          locked,
          text)
     values ('ABCCONV',
             c.CONVCREATEDDATE,
             null,--c.LASTUPDATEDBYUSERLEGACYKEY,
             null,--c.LASTUPDATEDDATE,
             'J_ABC_APPEAL',
             c.LEGACYKEY,
             'Y',
              c.text);
  
  t_count := t_count +1; 
    
  
  
    if mod(t_count,1000) = 0
      then
        commit;
    end if;         
    exception when others then
  --Log error with Pragma Autonomous Transaction allow oracle errors to pass through and continue to process data
      t_error := to_char(SQLERRM); 
      t_errorcode := to_char(SQLCODE);
      LogError(t_Error, t_errorcode ,c.rowid);  
      t_errorCount := t_errorCount + t_errorCount;          
   end; 
  end loop;            
  
  dbms_output.put_line('Count of processed Rows: ' || t_count || ' Error Count: ' || t_errorCount);
  
 commit;
  
end;
/

update dataconv.j_abc_appeal a
set a.attorneygenerallk =
(select u.legacykey
  from dataconv.u_users u
 where u.oraclelogonid in (select 'OPS$DAG'|| j.attorneygenerallk
                             from dataconv.j_abc_appeal j
                            where j.legacykey = a.legacykey));
/

commit;
