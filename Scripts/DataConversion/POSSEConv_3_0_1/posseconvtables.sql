insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('N_GENERAL', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('J_ABC_APPEAL', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('J_ABC_PETITION', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_VEHICLE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_PERMIT', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_PERMITTYPE', 'Y', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_LICENSEWARNING', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_LICENSEWARNINGTYPE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_VINTAGE', 'Y', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_PRODUCT', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_PRODUCTTYPE', 'Y', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_SUSPENSIONDATE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('J_ABC_EXPIRATION', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_ADDRESS', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_ESTABLISHMENT', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_ESTABLISHMENTHISTORY', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_ESTABLISHMENTTYPE', 'Y', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_EVENTDATE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_EVENTTYPE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_LEGALENTITY', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_LEGALENTITYHISTORY', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_LEGALENTITYTYPE', 'Y', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_LICENSE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_LICENSECANCELLATION', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_LICENSETYPE', 'Y', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_MASTERLICENSE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_PETITIONTYPE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('U_USERS', 'Y', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_REGION', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_OFFICE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_WARNING', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('P_ABC_SUBMITAPPLICATION', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('J_ABC_NEWAPPLICATION', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('J_ABC_RENEWALAPPLICATION', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_PROTESTANT', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_STREETTYPE', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_CONDITION', 'N', null);

insert into dataconv.posseconvtables (TABLENAME, FILL, GROUPID)
values ('O_ABC_CONDITIONTYPE', 'N', null);
