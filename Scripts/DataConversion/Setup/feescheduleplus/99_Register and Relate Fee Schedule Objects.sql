declare 
  -- Local variables here
  i integer;
  t_RelId number;
  t_EPId number;
  t_FeeScheduleId number;
  t_OtherObjectId number;
begin

  api.pkg_logicaltransactionupdate.resettransaction();
/*  --feeschedule
for c in (select d.feescheduleid
             from feescheduleplus.feeschedule d)loop
     api.pkg_objectupdate.RegisterExternalObject('o_FeeSchedule', c.feescheduleid);
  end loop; */
  
/*--datasource
  for c in (select d.datasourceid
             from feescheduleplus.DataSource d
             left outer join objmodelphys.externalobjects e on e.linkvalue = d.datasourceid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_DataSource')
             where e.LinkValue is null)loop
     api.pkg_objectupdate.RegisterExternalObject('o_DataSource', c.datasourceid);
  end loop;
--category
  
--feelement
  for c in (select d.feeelementid
             from feescheduleplus.feeelement d
             left outer join objmodelphys.externalobjects e on e.linkvalue = d.feeelementid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeElement')
             where e.LinkValue is null)loop
     api.pkg_objectupdate.RegisterExternalObject('o_FeeElement', c.feeelementid);
  end loop;
  
--feedefinition
  for c in (select d.feedefinitionid
             from feescheduleplus.feedefinition d
             left outer join objmodelphys.externalobjects e on e.linkvalue = d.feedefinitionid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeDefinition')
             where e.LinkValue is null)loop

     api.pkg_objectupdate.RegisterExternalObject('o_FeeDefinition', c.feedefinitionid);
  end loop;
  
  
  --fieldelement
  for c in (select d.fieldelementid
             from feescheduleplus.fieldelement d
             left outer join objmodelphys.externalobjects e on e.linkvalue = d.fieldelementid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FieldElement')
             where e.LinkValue is null)loop

     api.pkg_objectupdate.RegisterExternalObject('o_FieldElement', c.fieldelementid);
  end loop; */
  
    --feecategory
 /* for c in (select d.feecategoryid
             from feescheduleplus.feecategory d
             left outer join objmodelphys.externalobjects e on e.linkvalue = d.feecategoryid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeCategory')
             where e.LinkValue is null)loop

     api.pkg_objectupdate.RegisterExternalObject('o_FeeCategory', c.feecategoryid);
  end loop; */
  
  select objectid into t_FeeScheduleId
  from query.o_feeschedule;
  
/*-- Create relationships    
--datasource
  t_EpId := api.pkg_configquery.EndPointIdForName('o_DataSource', 'FeeSchedule');

  for c in (select e.objectid
             from feescheduleplus.DataSource d
             join objmodelphys.externalobjects e on e.linkvalue = d.datasourceid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_DataSource'))loop
     t_relid := api.pkg_relationshipupdate.new(t_epid, c.objectid, t_FeeScheduleId);
  end loop;
  
--feelement
  t_EpId := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'FeeSchedule');
  for c in (select e.objectid
             from feescheduleplus.feeelement d
             join objmodelphys.externalobjects e on e.linkvalue = d.feeelementid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeElement'))loop
     t_relid := api.pkg_relationshipupdate.new(t_epid, c.objectid, t_FeeScheduleId);
  end loop;
  
--feedefinition
  t_EpId := api.pkg_configquery.EndPointIdForName('o_FeeDefinition', 'FeeSchedule');
  for c in (select e.objectid
             from feescheduleplus.feedefinition d
             join objmodelphys.externalobjects e on e.linkvalue = d.feedefinitionid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeDefinition'))loop
     t_relid := api.pkg_relationshipupdate.new(t_epid, c.objectid, t_FeeScheduleId);
  end loop;
  t_EpId := api.pkg_configquery.EndPointIdForName('o_FeeDefinition', 'DataSource');
  for c in (select e.objectid, d.datasourceid
             from feescheduleplus.feedefinition d
             join objmodelphys.externalobjects e on e.linkvalue = d.feedefinitionid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeDefinition'))loop
     select d.objectid
       into t_OtherObjectId
       from query.o_datasource d
      where d.datasourceid = c.datasourceid;
     t_relid := api.pkg_relationshipupdate.new(t_epid, c.objectid, t_OtherObjectId);
  end loop;*/
  
/*--feecategories 
  t_EpId := api.pkg_configquery.EndPointIdForName('o_FeeCategory', 'FeeSchedule');
  for c in (select e.objectid
             from feescheduleplus.feecategory d
             join objmodelphys.externalobjects e on e.linkvalue = d.feecategoryid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeCategory'))loop
     t_relid := api.pkg_relationshipupdate.new(t_epid, c.objectid, t_FeeScheduleId);
  end loop; */

/*--feeelement
  t_EpId := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'FeeSchedule');
  for c in (select e.objectid
             from feescheduleplus.feeelement d
             join objmodelphys.externalobjects e on e.linkvalue = d.feeelementid
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeElement'))loop
     t_relid := api.pkg_relationshipupdate.new(t_epid, c.objectid, t_FeeScheduleId);
  end loop;
*/

  /*--field element to fee element
    t_EpId := api.pkg_configquery.EndPointIdForName('o_FieldElement', 'FeeElement');
    for c in (select e.objectid, d.feeelementid
             from feescheduleplus.fieldelement d
             join objmodelphys.externalobjects e on e.linkvalue = d.FieldElementId
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FieldElement'))loop
      -- find the objectid of the Fee Element
      select o.ObjectId
        into t_OtherObjectid
        from query.o_feeelement o
       where o.FeeElementId = c.feeelementid;
      -- create the rel       
      t_relid := api.pkg_relationshipupdate.new(t_epid, c.objectid, t_OtherObjectId);
    end loop; */
    
/*  -- fee element to data source (ignores bad data sources from cxnjdev)
    t_EpId := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'DataSource');
    for c in (select e.objectid, d.datasourceid
             from feescheduleplus.feeelement d
             join objmodelphys.externalobjects e on e.linkvalue = d.FeeElementId
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_FeeElement')
             where d.datasourceid is not null
               and d.datasourceid not in (778,818,802,858))loop
      -- find the objectid of the Fee Element
      select o.ObjectId
        into t_OtherObjectid
        from query.o_DataSource o
       where o.DataSourceId = c.datasourceid;
      -- create the rel       
      t_relid := api.pkg_relationshipupdate.new(t_epid, c.objectid, t_OtherObjectId);
    end loop; */
  
  
  api.pkg_logicaltransactionupdate.endtransaction();
  commit;
end;

