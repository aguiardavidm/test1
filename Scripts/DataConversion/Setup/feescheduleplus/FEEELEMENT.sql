prompt Importing table feescheduleplus.feeelement...
set feedback off
set define off
insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (698, 448, 449, 'String', 'Field', to_date('18-07-2012 13:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', to_date('18-07-2012 13:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', null, 'EnteredOnline');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (699, 448, 449, 'Number', 'Field', to_date('18-07-2012 13:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', to_date('18-07-2012 13:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', null, 'OnlineEVTNumOfDays');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (497, 448, 449, 'String', 'Field', to_date('29-04-2011 09:14:48', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA', to_date('25-10-2011 09:13:45', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', null, 'IsSpecialEventLicense');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (534, 448, 449, 'Number', 'Field', to_date('29-04-2011 12:42:49', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA', to_date('13-06-2011 14:44:43', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA', null, 'NumberOfEventDays');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (688, 448, 449, 'String', 'Field', to_date('16-09-2011 12:50:00', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', to_date('16-09-2011 12:50:00', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', null, 'OnlineEVTEventType');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (589, 448, 569, 'Number', 'Field', to_date('12-05-2011 11:50:31', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA', to_date('12-05-2011 11:50:31', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA', null, 'PenaltyAmount');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (668, 448, 449, 'String', 'Field', to_date('23-08-2011 15:51:36', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', to_date('23-08-2011 15:51:36', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', null, 'SpecialEventLicenseType');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (513, 448, null, 'Number', 'Expression', to_date('29-04-2011 10:27:30', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA', to_date('29-04-2011 10:27:30', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA', null, 'Violation Fee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (779, 448, 778, 'Number', 'Field', to_date('23-07-2014 14:41:57', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('25-07-2014 13:21:40', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'Product Xref Count');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (819, 448, 818, 'Number', 'Field', to_date('29-07-2014 10:44:28', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('29-07-2014 10:44:28', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'Product Xref Renewal Count');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2078, 448, 2058, 'String', 'Field', to_date('25-03-2015 10:53:30', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 10:53:30', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'AmendmentTypeCode');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2098, 448, 2058, 'String', 'Field', to_date('25-03-2015 10:55:05', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 10:55:05', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'IsStateIssued');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2138, 448, 2058, 'String', 'Field', to_date('25-03-2015 12:14:23', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 12:14:23', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'GLAccountCode');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1098, 448, 1018, 'String', 'Field', to_date('26-01-2015 14:11:32', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 14:11:32', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', null, 'GLAccountNumber');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1058, 448, 1018, 'String', 'Field', to_date('26-01-2015 11:08:29', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 11:08:29', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', null, 'LicenseGLAccount');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1060, 448, 1018, 'String', 'Field', to_date('26-01-2015 11:09:14', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 11:09:14', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', null, 'PermitGLAccount');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1678, 448, 449, 'String', 'Field', to_date('05-03-2015 13:50:07', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('06-03-2015 11:06:19', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'FarmWineryWholesalePrivForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1680, 448, 638, 'String', 'Field', to_date('05-03-2015 13:50:25', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('09-03-2015 09:55:12', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'FarmWineryWholesalePrivForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (919, 448, 918, 'Number', 'Field', to_date('22-09-2014 11:01:15', 'dd-mm-yyyy hh24:mi:ss'), 'ALEX', to_date('22-09-2014 12:58:04', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'FilingFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1658, 448, 1018, 'String', 'Field', to_date('05-03-2015 08:49:16', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('05-03-2015 08:49:57', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'FeeDescription');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (798, 448, 778, 'Number', 'Field', to_date('25-07-2014 13:16:51', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('04-08-2014 13:40:15', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, null);

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (800, 448, 758, 'Number', 'Field', to_date('25-07-2014 13:17:25', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('25-07-2014 13:17:25', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'Product Xref Count');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (803, 448, 802, 'Number', 'Field', to_date('25-07-2014 13:24:50', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('25-07-2014 13:49:01', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'Product Xref Count');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2598, 448, 2058, 'Number', 'Field', to_date('26-03-2015 10:42:27', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('26-03-2015 10:42:27', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'NumberOfVesselsLarge');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2600, 448, 2058, 'Number', 'Field', to_date('26-03-2015 10:42:40', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('26-03-2015 10:42:40', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'NumberOfVesselsMedium');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2602, 448, 2058, 'Number', 'Field', to_date('26-03-2015 10:42:50', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('26-03-2015 10:42:50', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'NumberOfVesselsSmall');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (898, 448, 858, 'Number', 'Field', to_date('02-09-2014 09:31:45', 'dd-mm-yyyy hh24:mi:ss'), 'CNELSON', to_date('02-09-2014 09:31:45', 'dd-mm-yyyy hh24:mi:ss'), 'CNELSON', null, 'Product Count For Fee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (901, 448, 900, 'Number', 'Field', to_date('02-09-2014 09:33:14', 'dd-mm-yyyy hh24:mi:ss'), 'CNELSON', to_date('02-09-2014 09:47:21', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'Product Count For Fee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2578, 448, 2058, 'String', 'Field', to_date('26-03-2015 10:24:12', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('26-03-2015 10:24:12', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'PlenaryWineryProduction');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2580, 448, 2058, 'String', 'Field', to_date('26-03-2015 10:24:29', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('26-03-2015 10:24:29', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'PlenaryWineryWholesalePrivileg');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1025, 448, 1018, 'String', 'Expression', to_date('23-01-2015 16:13:39', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 14:05:51', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'GLAccountMisc');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1038, 448, 1018, 'String', 'Field', to_date('26-01-2015 08:41:25', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 11:06:58', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'LicenseNumber');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1040, 448, 1018, 'String', 'Field', to_date('26-01-2015 08:42:07', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 11:08:57', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'PermitNumber');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1042, 448, 1018, 'String', 'Field', to_date('26-01-2015 08:42:51', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 09:40:54', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'LegalEntityFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1044, 448, 1018, 'String', 'Field', to_date('26-01-2015 08:43:38', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 09:41:06', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'TransactionNameFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1051, 448, 1018, 'Number', 'Field', to_date('26-01-2015 09:49:07', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('26-01-2015 09:49:07', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', null, 'FeeAmount');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1702, 448, 638, 'String', 'Field', to_date('06-03-2015 09:23:33', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('06-03-2015 11:31:49', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'PlenaryWholesalePrivilegeFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1704, 448, 638, 'String', 'Field', to_date('06-03-2015 09:23:44', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('06-03-2015 09:23:44', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'PlenaryWineryProductionForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1718, 448, 449, 'String', 'Field', to_date('06-03-2015 10:08:37', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('06-03-2015 11:01:20', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'OutOfStateWholesalePrivForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2438, 448, 2058, 'String', 'Field', to_date('25-03-2015 18:31:50', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 18:31:50', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'RestrictedBreweryProduction');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1698, 448, 449, 'String', 'Field', to_date('06-03-2015 09:22:45', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('06-03-2015 11:06:33', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'PlenaryWholesalePrivilegeFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1700, 448, 449, 'String', 'Field', to_date('06-03-2015 09:22:58', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('06-03-2015 09:22:58', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'PlenaryWineryProductionForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (759, 448, 758, 'Number', 'Field', to_date('21-05-2014 15:10:33', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('25-07-2014 13:20:30', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'Product Count');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1838, 448, 638, 'String', 'Field', to_date('09-03-2015 10:01:08', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('09-03-2015 10:01:08', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'OutOfStateWholesalePrivForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1582, 448, 449, 'String', 'Field', to_date('05-02-2015 11:30:50', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:30:50', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'SuppLimitedDistilleryForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1584, 448, 449, 'String', 'Field', to_date('05-02-2015 11:31:06', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:31:06', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'FarmWineryProductionForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1578, 448, 449, 'String', 'Field', to_date('05-02-2015 11:30:23', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:30:23', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'RstrctdBreweryProductionForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1580, 448, 449, 'String', 'Field', to_date('05-02-2015 11:30:37', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:30:37', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'LimitedBreweryProductionForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1586, 448, 449, 'String', 'Field', to_date('05-02-2015 11:31:17', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:31:17', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'OutOfStateWineryProductionFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1588, 448, 449, 'Boolean', 'Field', to_date('05-02-2015 11:31:33', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:31:33', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'FarmWinerySalesroomForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1590, 448, 449, 'String', 'Field', to_date('05-02-2015 11:31:57', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:31:57', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'RetailTransitTypeForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1592, 448, 449, 'Number', 'Field', to_date('05-02-2015 11:32:23', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:32:23', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'NumberOfVehicles');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1594, 448, 449, 'Number', 'Field', to_date('05-02-2015 11:32:36', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:32:36', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'NumberOfVesselsSmall');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1596, 448, 449, 'Number', 'Field', to_date('05-02-2015 11:32:50', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:32:50', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'NumberOfVesselsMedium');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1598, 448, 449, 'Number', 'Field', to_date('05-02-2015 11:33:05', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:33:05', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'NumberOfVesselsLarge');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1618, 448, 638, 'String', 'Field', to_date('05-02-2015 11:34:47', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:34:47', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'RstrctdBreweryProductionForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1620, 448, 638, 'String', 'Field', to_date('05-02-2015 11:35:02', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:35:02', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'LimitedBreweryProductionForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1622, 448, 638, 'String', 'Field', to_date('05-02-2015 11:35:13', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:35:13', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'SuppLimitedDistilleryForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1624, 448, 638, 'String', 'Field', to_date('05-02-2015 11:35:24', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:35:24', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'FarmWineryProductionForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1626, 448, 638, 'String', 'Field', to_date('05-02-2015 11:35:36', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:35:36', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'OutOfStateWineryProductionFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1628, 448, 638, 'Boolean', 'Field', to_date('05-02-2015 11:35:47', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:35:47', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'FarmWinerySalesroomForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1630, 448, 638, 'String', 'Field', to_date('05-02-2015 11:35:58', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:35:58', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'RetailTransitTypeForFee');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1638, 448, 638, 'Number', 'Field', to_date('05-02-2015 11:40:04', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:40:04', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'NumberOfVehicles');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1640, 448, 638, 'Number', 'Field', to_date('05-02-2015 11:40:17', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:40:17', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'NumberOfVesselsSmall');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1642, 448, 638, 'Number', 'Field', to_date('05-02-2015 11:40:29', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:40:29', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'NumberOfVesselsMedium');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1644, 448, 638, 'Number', 'Field', to_date('05-02-2015 11:40:44', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 11:40:44', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'NumberOfVesselsLarge');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (859, 448, 858, 'Number', 'Field', to_date('04-08-2014 12:37:00', 'dd-mm-yyyy hh24:mi:ss'), 'CNELSON', to_date('04-08-2014 12:37:00', 'dd-mm-yyyy hh24:mi:ss'), 'CNELSON', null, 'Product Count');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2538, 448, 2058, 'String', 'Field', to_date('26-03-2015 08:54:56', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('26-03-2015 08:54:56', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'OutOfStateWineryProduction');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2540, 448, 2058, 'String', 'Field', to_date('26-03-2015 08:56:00', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('26-03-2015 08:56:00', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'OutOfStateWineryWholesalePrivi');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (839, 448, 838, 'Number', 'Field', to_date('29-07-2014 11:40:25', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('29-07-2014 11:40:25', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'Product Xref Renew Count');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1118, 448, 1018, 'String', 'Field', to_date('27-01-2015 09:51:31', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('27-01-2015 09:51:31', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', null, 'GLAccountCode');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2758, 448, 2718, 'Number', 'Field', to_date('30-03-2015 11:16:51', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('30-03-2015 11:16:51', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'ObjectId');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2519, 448, 2058, 'Number', 'Field', to_date('25-03-2015 20:23:34', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('26-03-2015 10:42:01', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS', null, 'NumberOfVehicles');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1558, 448, 449, 'String', 'Field', to_date('05-02-2015 10:17:54', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 10:17:54', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'LicenseTypeCode');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1560, 448, 449, 'String', 'Field', to_date('05-02-2015 10:18:07', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 10:18:07', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'OnlineLicenseTypeCode');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1562, 448, 638, 'String', 'Field', to_date('05-02-2015 10:18:19', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('05-02-2015 10:18:19', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'LicenseTypeCode');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2478, 448, 2058, 'String', 'Field', to_date('25-03-2015 19:37:07', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 19:37:07', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'SupplementaryLimitedDistillery');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2198, 448, 2058, 'String', 'Field', to_date('25-03-2015 14:51:24', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 14:51:24', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'LicenseTypeCode');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2339, 448, 2058, 'String', 'Field', to_date('25-03-2015 16:18:50', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 16:18:50', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'RetailTransitType');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2358, 448, 2058, 'String', 'Field', to_date('25-03-2015 16:39:52', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 16:39:52', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'FarmWineryProduction');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2378, 448, 2058, 'String', 'Field', to_date('25-03-2015 16:51:07', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 16:51:07', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'FarmWineryWholesalePrivilege');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (2398, 448, 2058, 'String', 'Field', to_date('25-03-2015 17:39:35', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 17:39:35', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', null, 'LimitedBreweryProduction');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1699, 448, 638, 'String', 'Field', to_date('17-04-2015 18:56:44', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 18:56:44', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'GLAccountCode');

insert into feescheduleplus.feeelement (FEEELEMENTID, FEESCHEDULEID, DATASOURCEID, DATATYPE, FEEELEMENTTYPE, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY, ISGLOBAL, GLOBALNAME)
values (1701, 448, 449, 'String', 'Field', to_date('17-04-2015 19:02:51', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:02:51', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', null, 'GLAccountCode');

prompt Done.
