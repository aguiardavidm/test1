prompt Importing table feescheduleplus.datasource...
set feedback off
set define off
insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (449, 448, 'j_ABC_NewApplication', 'New Application Job', to_date('19-04-2011 10:53:40', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', to_date('19-04-2011 10:53:40', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (569, 448, 'j_ABC_Accusation', 'Accusation Job', to_date('11-05-2011 15:51:47', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA', to_date('11-05-2011 15:51:47', 'dd-mm-yyyy hh24:mi:ss'), 'POSSEDBA');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (638, 448, 'j_ABC_RenewalApplication', 'Renewal Job', to_date('26-06-2012 09:50:29', 'dd-mm-yyyy hh24:mi:ss'), 'FEESCHEDULEPLUS', to_date('03-02-2015 13:50:37', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (758, 448, 'j_ABC_PRApplication', 'Product Registration Application', to_date('21-05-2014 14:57:24', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('21-05-2014 14:57:24', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (838, 448, 'j_ABC_PRRenewal', 'Product Registration Renewal', to_date('29-07-2014 11:39:55', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('29-07-2014 11:39:55', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (900, 448, 'j_ABC_PRAmendment', 'Product Registration Amendment', to_date('02-09-2014 09:32:55', 'dd-mm-yyyy hh24:mi:ss'), 'CNELSON', to_date('02-09-2014 09:32:55', 'dd-mm-yyyy hh24:mi:ss'), 'CNELSON');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (918, 448, 'j_ABC_Petition', 'Petition Job', to_date('22-09-2014 10:58:43', 'dd-mm-yyyy hh24:mi:ss'), 'ALEX', to_date('22-09-2014 10:58:43', 'dd-mm-yyyy hh24:mi:ss'), 'ALEX');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1018, 448, 'j_ABC_MiscellaneousRevenue', 'Miscellaneous Revenue Transaction Job', to_date('23-01-2015 15:40:37', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('23-01-2015 15:40:37', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2058, 448, 'j_ABC_AmendmentApplication', 'Amendment Job', to_date('25-03-2015 10:52:59', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 10:52:59', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL');

insert into feescheduleplus.datasource (DATASOURCEID, FEESCHEDULEID, POSSEVIEWNAME, DESCRIPTION, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2718, 448, 'j_ABC_Appeal', 'Appeal Job', to_date('30-03-2015 08:34:35', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('30-03-2015 08:34:35', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS');

prompt Done.
