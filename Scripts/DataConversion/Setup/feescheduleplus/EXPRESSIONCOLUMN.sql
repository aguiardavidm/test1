prompt Importing table feescheduleplus.expressioncolumn...
set feedback off
set define off
insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (1, 'FEECATEGORY', 'CONDITION', 'Boolean', 'FEECATEGORYID');

insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (2, 'FEEDEFINITION', 'AMOUNT', 'Number', 'FEEDEFINITIONID');

insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (3, 'FEEDEFINITION', 'CONDITION', 'Boolean', 'FEEDEFINITIONID');

insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (4, 'FEEDEFINITION', 'GLACCOUNTNUMBER', 'String', 'FEEDEFINITIONID');

insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (5, 'FEEDEFINITION', 'FEEDESCRIPTION', 'String', 'FEEDEFINITIONID');

insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (6, 'EXPRESSIONELEMENT', 'SYNTAX', 'String', 'EXPRESSIONELEMENTID');

insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (7, 'EXPRESSIONELEMENT', 'SYNTAX', 'Number', 'EXPRESSIONELEMENTID');

insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (8, 'EXPRESSIONELEMENT', 'SYNTAX', 'Date', 'EXPRESSIONELEMENTID');

insert into feescheduleplus.expressioncolumn (EXPRESSIONCOLUMNID, TABLENAME, COLUMNNAME, DATATYPE, TABLEPRIMARYKEYNAME)
values (9, 'EXPRESSIONELEMENT', 'SYNTAX', 'Boolean', 'EXPRESSIONELEMENTID');

prompt Done.
