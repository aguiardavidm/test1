declare 
  -- Local variables here
  i integer;
  t_EPId number;
  t_OtherObjectId number;
begin

  api.pkg_logicaltransactionupdate.resettransaction();
  
 --GLAccount
  for c in (select d.GLAccountId
             from api.glaccounts d
             left outer join objmodelphys.externalobjects e on e.linkvalue = d.GLAccountId
             and e.ObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_GLAccount')
             where e.LinkValue is null)loop
     api.pkg_objectupdate.RegisterExternalObject('o_GLAccount', c.glaccountid);
     select objectid 
       into t_OtherObjectId 
       from query.o_glaccount g 
      where g.GLAccountId = c.glaccountid;
     api.pkg_columnupdate.SetValue(t_OtherObjectId,'Active','Y');
  end loop;
  
  
  api.pkg_logicaltransactionupdate.endtransaction();
  commit;
end;
