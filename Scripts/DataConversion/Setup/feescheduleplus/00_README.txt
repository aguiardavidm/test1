You may need to de-regester external POSSE objects as well, and this needs to be done prior to truncating the tables.
In NJDELIVERY, this was only necessary on the Fee Schedule objectdef, but you will want to confirm the other tables on a database by database basis.

The script used to do de-register o_FeeSchedule records in NJDELIVERY is below.

declare

begin
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for x in (select feescheduleid from feescheduleplus.feeschedule) loop

    api.pkg_objectupdate.DeregisterExternalObject('o_FeeSchedule',x.feescheduleid);

  end loop;

  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;

end;