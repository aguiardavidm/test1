prompt Importing table feescheduleplus.feedefinition...
set feedback off
set define off
insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2558, 448, 2058, 'Out Of State Winery Amendment Fee', '={LicenseTypeCode} = ''41'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '=case {AmendmentTypeCode}
           when ''PersonAndPlace'' then
(case {OutOfStateWineryProduction}
           when ''Less than 1,000 gallons''                          then 63
           when ''Between 1,000 and 2,500 gallons''        then 125
           when ''Between 2,500 and 30,000 gallons''      then 250
           when ''Between 30,000 and 50,000 gallons''    then 375
           when ''Between 50,000 and 100,000 gallons'' then 938
           when ''Between 100,000 and 150,000 gallons'' then 938
           when ''Between 150,000 and 250,000 gallons'' then 938
end +
case {OutOfStateWineryWholesalePrivi}
  when ''Y'' then
(case {OutOfStateWineryProduction}
           when ''Less than 1,000 gallons''                          then 100
           when ''Between 1,000 and 2,500 gallons''        then 100
           when ''Between 2,500 and 30,000 gallons''      then 100
           when ''Between 30,000 and 50,000 gallons''    then 100
           when ''Between 50,000 and 100,000 gallons'' then 250
           when ''Between 100,000 and 150,000 gallons'' then 500
           when ''Between 150,000 and 250,000 gallons'' then 1000
end)
else 0
  end 
  ) * 0.20
else    
(case {OutOfStateWineryProduction}
           when ''Less than 1,000 gallons''                          then 63
           when ''Between 1,000 and 2,500 gallons''        then 125
           when ''Between 2,500 and 30,000 gallons''      then 250
           when ''Between 30,000 and 50,000 gallons''    then 375
           when ''Between 50,000 and 100,000 gallons'' then 938
           when ''Between 100,000 and 150,000 gallons'' then 938
           when ''Between 150,000 and 250,000 gallons'' then 938
end + 
case {OutOfStateWineryWholesalePrivi}
  when ''Y'' then
(case {OutOfStateWineryProduction}
           when ''Less than 1,000 gallons''                          then 100
           when ''Between 1,000 and 2,500 gallons''        then 100
           when ''Between 2,500 and 30,000 gallons''      then 100
           when ''Between 30,000 and 50,000 gallons''    then 100
           when ''Between 50,000 and 100,000 gallons'' then 250
           when ''Between 100,000 and 150,000 gallons'' then 500
           when ''Between 150,000 and 250,000 gallons'' then 1000
end)
else 0
  end 
  ) * 0.10
end case', 'CreatedDate', null, '={GLAccountCode}', 'Out Of State Winery Amendment Fee', 'Default', null, to_date('26-03-2015 08:58:02', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1898, 448, 638, 'Plenary Winery Wholesale Privilege', '={LicenseTypeCode} = ''21'' and {PlenaryWholesalePrivilegeFee} = ''Y''', '=case {PlenaryWineryProductionForFee}
           when ''Less than 50,000 gallons''                          then 100
           when ''Between 50,000 and 100,000 gallons'' then 250
           when ''Between 100,000 and 150,000 gallons'' then 500
           when ''Between 150,000 and 250,000 gallons'' then 1000
           else                                                                                       0.01
end case', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Winery Wholesale Privilege Fee', 'Default', null, to_date('09-03-2015 13:29:34', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:42:08', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2498, 448, 2058, 'Supplementary Limited Distillery Amendment Fee', '={LicenseTypeCode} = ''18'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '=case {AmendmentTypeCode}
           when ''PersonAndPlace'' then
(case {SupplementaryLimitedDistillery}
           when ''UP TO 5,000 GALS''                        then 313
           when ''NOT MORE THAN 10,000 GALS'' then 625
           when ''UNLIMITED QUANTITY''                then 1250
end
) * 0.20
else
(case {SupplementaryLimitedDistillery}
           when ''UP TO 5,000 GALS''                        then 313
           when ''NOT MORE THAN 10,000 GALS'' then 625
           when ''UNLIMITED QUANTITY''                then 1250
end
) * 0.10
end case', 'CreatedDate', null, '={GLAccountCode}', 'Supplementary Limited Distillery Amendment Fee', 'Default', null, to_date('25-03-2015 19:40:08', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2178, 448, 2058, 'Annual State Permit Amendment Fee', '={LicenseTypeCode} = ''14'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 2000 * 0.10
             when ''PlaceToPlace''       then 2000 * 0.10
             when ''PersonAndPlace'' then 2000 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Annual State Permit Amendment Fee', 'Default', null, to_date('25-03-2015 14:44:25', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:03:24', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2218, 448, 2058, 'Bonded Warehouse Bottling Amendment Fee', '={LicenseTypeCode} = ''29'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 625 * 0.10
             when ''PlaceToPlace''       then 625 * 0.10
             when ''PersonAndPlace'' then 625 * 0.20
end case
', 'CreatedDate', null, '={GLAccountCode}', 'Bonded Warehouse Bottling Amendment Fee', 'Default', null, to_date('25-03-2015 15:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:03:59', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2219, 448, 2058, 'Broker Amendment Fee', '={LicenseTypeCode} = ''12'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 500 * 0.10
             when ''PlaceToPlace''       then 500 * 0.10
             when ''PersonAndPlace'' then 500 * 0.20
end case
', 'CreatedDate', null, '={GLAccountCode}', 'Broker Amendment Fee', 'Default', null, to_date('25-03-2015 15:14:09', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:04:31', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2220, 448, 2058, 'Craft Distillery Amendment Fee', '={LicenseTypeCode} = ''07'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 938 * 0.10
             when ''PlaceToPlace''       then 938 * 0.10
             when ''PersonAndPlace'' then 938 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Craft Distillery Amendment Fee', 'Default', null, to_date('25-03-2015 15:15:49', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:05:01', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2221, 448, 2058, 'Instructional Winemaking Facility Amendment Fee', '={LicenseTypeCode} = ''39'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 1000 * 0.10
             when ''PlaceToPlace''       then 1000 * 0.10
             when ''PersonAndPlace'' then 1000 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Instructional Winemaking Facility Amendment Fee', 'Default', null, to_date('25-03-2015 15:17:05', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:07:38', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2238, 448, 2058, 'Limited Distillery Amendment Fee', '={LicenseTypeCode} = ''17'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 3750 * 0.10
             when ''PlaceToPlace''       then 3750 * 0.10
             when ''PersonAndPlace''  then 3750 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Limited Distillery Amendment Fee', 'Default', null, to_date('25-03-2015 15:27:19', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('17-04-2015 19:21:08', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (805, 448, 838, 'Product Registration Renewal Fee', null, '={Product Xref Renew Count}*23', 'CREATEDDATE', null, 'BRD', 'Product Registration Renewal Fee', 'Default', null, to_date('25-07-2014 13:25:52', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('23-03-2015 13:01:46', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2618, 448, 2058, 'Retail Transit Boat Amendment Fee', '={LicenseTypeCode} = ''13'' and {RetailTransitType} = ''Boat'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '=case {AmendmentTypeCode}
           when ''PersonAndPlace'' then
         (({NumberOfVesselsSmall} * 63) + ({NumberOfVesselsMedium} * 125) + ({NumberOfVesselsLarge} * 375)) * 0.20
     else
         (({NumberOfVesselsSmall} * 63) + ({NumberOfVesselsMedium} * 125) + ({NumberOfVesselsLarge} * 375)) * 0.10
end case', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Boat Amendment Fee', 'Default', null, to_date('26-03-2015 10:45:38', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1323, 448, 638, 'Retail Transit Limousine Renewal Fee', '={LicenseTypeCode} = ''13'' and {RetailTransitTypeForFee} = ''Limousine'' and {NumberOfVehicles} > 0', '={NumberOfVehicles} * 31', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Limousine Renewal Fee', 'Default', null, to_date('04-02-2015 11:21:28', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:58:11', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1378, 448, 449, 'Restricted Brewery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''08'' and {RstrctdBreweryProductionForFee} is not null', '=case {RstrctdBreweryProductionForFee}
           when ''UP TO 1,000 BBLS''        then 1250
           when ''1,001 TO 2,000 BBLS''   then 1500
           when ''2,001 TO 3,000 BBLS''   then 1750
           when ''3,001 TO 4,000 BBLS''   then 2000
           when ''4,001 TO 5,000 BBLS''   then 2250
           when ''5,001 TO 6,000 BBLS''   then 2500
           when ''6,001 TO 7,000 BBLS''   then 2750
           when ''7,001 TO 8,000 BBLS''   then 3000
           when ''8,001 TO 9,000 BBLS''   then 3250
           when ''9,001 TO 10,000 BBLS'' then 3500
end case', 'CreatedDate', null, '={GLAccountCode}', 'Restricted Brewery Application Fee', 'Default', null, to_date('04-02-2015 15:30:13', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:53:53', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1379, 448, 638, 'Restricted Brewery Renewal Fee', '={LicenseTypeCode} = ''08'' and {RstrctdBreweryProductionForFee} is not null', '=case {RstrctdBreweryProductionForFee}
           when ''UP TO 1,000 BBLS''        then 1250
           when ''1,001 TO 2,000 BBLS''   then 1500
           when ''2,001 TO 3,000 BBLS''   then 1750
           when ''3,001 TO 4,000 BBLS''   then 2000
           when ''4,001 TO 5,000 BBLS''   then 2250
           when ''5,001 TO 6,000 BBLS''   then 2500
           when ''6,001 TO 7,000 BBLS''   then 2750
           when ''7,001 TO 8,000 BBLS''   then 3000
           when ''8,001 TO 9,000 BBLS''   then 3250
           when ''9,001 TO 10,000 BBLS'' then 3500
end case', 'CreatedDate', null, '={GLAccountCode}', 'Restricted Brewery Renewal Fee', 'Default', null, to_date('04-02-2015 15:31:27', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:54:24', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1398, 448, 449, 'Limited Brewery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''11'' and {LimitedBreweryProductionForFee} is not null', '=case {LimitedBreweryProductionForFee}
           when ''UP TO 50,000 BBLS''             then 1250
           when ''50,000 TO 100,000 BBLS''   then 2500
           when ''100,000 TO 200,000 BBLS'' then 5000
           when ''200,000 TO 300,000 BBLS'' then 7500
end case', 'CreatedDate', null, '={GLAccountCode}', 'Limited Brewery Application Fee', 'Default', null, to_date('04-02-2015 15:50:17', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:18:15', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1528, 448, 449, 'Farm Winery Wholesale Privilege', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''22'' and {FarmWineryWholesalePrivForFee} = ''Y''', '100', 'CreatedDate', null, '={GLAccountCode}', 'Farm Winery Wholesale Privilege Fee', 'Default', null, to_date('05-02-2015 09:48:10', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:15:25', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1199, 448, 638, 'Broker Renewal Fee', '={LicenseTypeCode} = ''12''', '500', 'CreatedDate', null, '={GLAccountCode}', 'Broker Renewal Fee', 'Default', null, to_date('04-02-2015 08:15:08', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:08:31', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1198, 448, 449, 'Broker Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''12''', '500', 'CreatedDate', null, '={GLAccountCode}', 'Broker Application Fee', 'Default', null, to_date('04-02-2015 08:13:34', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:07:51', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1200, 448, 449, 'Retail Transit Plane/Train Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''13'' and {RetailTransitTypeForFee} in (''Plane'', ''Rail'')', '375', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Plane/Train Application Fee', 'Default', null, to_date('04-02-2015 08:19:00', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:59:03', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1201, 448, 638, 'Retail Transit Plane/Train Renewal Fee', '={LicenseTypeCode} = ''13'' and {RetailTransitTypeForFee} in (''Plane'', ''Rail'')', '375', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Plane/Train Renewal Fee', 'Default', null, to_date('04-02-2015 08:21:24', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:59:27', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1202, 448, 449, 'Annual State Permit Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''14''', '2000', 'CreatedDate', null, '={GLAccountCode}', 'Annual State Permit Application Fee', 'Default', null, to_date('04-02-2015 08:22:46', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 18:59:32', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1203, 448, 638, 'Annual State Permit Renewal Fee', '={LicenseTypeCode} = ''14''', '2000', 'CreatedDate', null, '={GLAccountCode}', 'Annual State Permit Renewal Fee', 'Default', null, to_date('04-02-2015 08:28:55', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 18:59:48', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1243, 448, 638, 'Limited Distillery Renewal Fee', '={LicenseTypeCode} = ''17''', '3750', 'CreatedDate', null, '={GLAccountCode}', 'Limited Distillery Renewal Fee', 'Default', null, to_date('04-02-2015 09:52:14', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:21:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1258, 448, 449, 'State Beverage Distributor Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''19''', '1031', 'CreatedDate', null, '={GLAccountCode}', 'State Beverage Distributor Application Fee', 'Default', null, to_date('04-02-2015 10:14:50', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:01:37', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1259, 448, 638, 'State Beverage Distributor Renewal Fee', '={LicenseTypeCode} = ''19''', '1031', 'CreatedDate', null, '={GLAccountCode}', 'State Beverage Distributor Renewal Fee', 'Default', null, to_date('04-02-2015 10:15:48', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:02:02', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1278, 448, 449, 'Transportation Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''20''', '625', 'CreatedDate', null, '={GLAccountCode}', 'Transportation Application Fee', 'Default', null, to_date('04-02-2015 10:27:08', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:04:03', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1279, 448, 638, 'Transportation Renewal Fee', '={LicenseTypeCode} = ''20''', '625', 'CreatedDate', null, '={GLAccountCode}', 'Transportation Renewal Fee', 'Default', null, to_date('04-02-2015 10:29:06', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:04:28', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1280, 448, 449, 'Plenary Winery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''21''', '938', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Winery Application Fee', 'Default', null, to_date('04-02-2015 10:30:23', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:32:37', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1281, 448, 638, 'Plenary Winery Renewal Fee', '={LicenseTypeCode} = ''21''', '938', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Winery Renewal Fee', 'Default', null, to_date('04-02-2015 10:31:08', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:32:58', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1282, 448, 449, 'Plenary Wholesale Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = 23', '8750', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Wholesale Application Fee', 'Default', null, to_date('04-02-2015 10:31:59', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:31:14', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1283, 448, 638, 'Plenary Wholesale Renewal Fee', '={LicenseTypeCode} = ''23''', '8750', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Wholesale Renewal Fee', 'Default', null, to_date('04-02-2015 10:32:44', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:31:39', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1284, 448, 449, 'Limited Wholesale Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''25''', '1875', 'CreatedDate', null, '={GLAccountCode}', 'Limited Wholesale Application Fee', 'Default', null, to_date('04-02-2015 10:34:53', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:23:18', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1298, 448, 638, 'Limited Wholesale Renewal Fee', '={LicenseTypeCode} = ''25''', '1875', 'CreatedDate', null, '={GLAccountCode}', 'Limited Wholesale Renewal Fee', 'Default', null, to_date('04-02-2015 10:38:29', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:23:41', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1299, 448, 449, 'Wine Wholesale Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''26''', '3750', 'CreatedDate', null, '={GLAccountCode}', 'Wine Wholesale Application Fee', 'Default', null, to_date('04-02-2015 10:40:03', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:09:36', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1300, 448, 638, 'Wine Wholesale Renewal Fee', '={LicenseTypeCode} = ''26''', '3750', 'CreatedDate', null, '={GLAccountCode}', 'Wine Wholesale Renewal Fee', 'Default', null, to_date('04-02-2015 10:40:39', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:09:10', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1301, 448, 449, 'Warehouse Receipts Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''27''', '375', 'CreatedDate', null, '={GLAccountCode}', 'Warehouse Receipts Application Fee', 'Default', null, to_date('04-02-2015 10:47:29', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:05:15', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1302, 448, 638, 'Warehouse Receipts Renewal Fee', '={LicenseTypeCode} = ''27''', '375', 'CreatedDate', null, '={GLAccountCode}', 'Warehouse Receipts Renewal Fee', 'Default', null, to_date('04-02-2015 10:48:06', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:05:38', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1303, 448, 449, 'Public Warehouse Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''28''', '500', 'CreatedDate', null, '={GLAccountCode}', 'Public Warehouse Application Fee', 'Default', null, to_date('04-02-2015 10:48:42', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:46:07', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1304, 448, 638, 'Public Warehouse Renewal Fee', '={LicenseTypeCode} = ''28''', '500', 'CreatedDate', null, '={GLAccountCode}', 'Public Warehouse Renewal Fee', 'Default', null, to_date('04-02-2015 10:49:24', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:46:28', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1305, 448, 449, 'Bonded Warehouse Bottling Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''29''', '625', 'CreatedDate', null, '={GLAccountCode}', 'Bonded Warehouse Bottling Application Fee', 'Default', null, to_date('04-02-2015 10:50:34', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:02:09', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1306, 448, 638, 'Bonded Warehouse Bottling Renewal Fee', '={LicenseTypeCode} = ''29''', '625', 'CreatedDate', null, '={GLAccountCode}', 'Bonded Warehouse Bottling Renewal Fee', 'Default', null, to_date('04-02-2015 10:51:10', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:03:46', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1307, 448, 449, 'Instructional Winemaking Facility Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''39''', '1000', 'CreatedDate', null, '={GLAccountCode}', 'Instructional Winemaking Facility Application Fee', 'Default', null, to_date('04-02-2015 10:52:08', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:16:37', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1308, 448, 638, 'Instructional Winemaking Facility Renewal Fee', '={LicenseTypeCode} = ''39''', '1000', 'CreatedDate', null, '={GLAccountCode}', 'Instructional Winemaking Facility Renewal Fee', 'Default', null, to_date('04-02-2015 10:53:16', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:17:01', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1309, 448, 449, 'Special Permit Golf Facility Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''40''', '2000', 'CreatedDate', null, '={GLAccountCode}', 'Special Permit Golf Facility Application Fee', 'Default', null, to_date('04-02-2015 10:54:11', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:00:20', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1310, 448, 638, 'Special Permit Golf Facility Renewal Fee', '={LicenseTypeCode} = ''40''', '2000', 'CreatedDate', null, '={GLAccountCode}', 'Special Permit Golf Facility Renewal Fee', 'Default', null, to_date('04-02-2015 10:54:59', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:00:43', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2118, 448, 2058, 'Retail License Amendment Fee', '={AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'') and {IsStateIssued} = ''N''', '200', 'CreatedDate', null, '={GLAccountCode}', 'Retail License Amendment Fee', 'Default', null, to_date('25-03-2015 11:01:35', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('25-03-2015 16:22:05', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1142, 448, 449, 'Retail License Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) in (''31'',''32'',''33'',''34'',''35'',''36'',''37'',''43'',''44'')', '200', 'CreatedDate', null, '={GLAccountCode}', 'Retail License Application Fee', 'Default', null, to_date('03-02-2015 13:37:17', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:55:27', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1143, 448, 638, 'Retail License Renewal Fee', '={LicenseTypeCode} in (''31'',''32'',''33'',''34'',''35'',''36'',''37'',''43'',''44'')', '200', 'CreatedDate', null, '={GLAccountCode}', 'Retail License Renewal Fee', 'Default', null, to_date('03-02-2015 13:38:30', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 18:58:07', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1158, 448, 449, 'Wine Blending Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''09''', '625', 'CreatedDate', null, '={GLAccountCode}', 'Wine Blending Application Fee', 'Default', null, to_date('03-02-2015 14:05:24', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:06:27', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1159, 448, 638, 'Wine Blending Renewal Fee', '={LicenseTypeCode} = ''09''', '625', 'CreatedDate', null, '={GLAccountCode}', 'Wine Blending Renewal Fee', 'Default', null, to_date('03-02-2015 14:06:06', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:06:52', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1370, 448, 449, 'Retail Transit Boat Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''13'' and {RetailTransitTypeForFee} = ''Boat'' and ({NumberOfVesselsSmall} + {NumberOfVesselsMedium} + {NumberOfVesselsLarge}) > 0', '=({NumberOfVesselsSmall} * 63) + ({NumberOfVesselsMedium} * 125) + ({NumberOfVesselsLarge} * 375)', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Boat Application Fee', 'Default', null, to_date('04-02-2015 14:58:44', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:56:23', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1371, 448, 638, 'Retail Transit Boat Renewal Fee', '={LicenseTypeCode} = ''13'' and {RetailTransitTypeForFee} = ''Boat'' and ({NumberOfVesselsSmall} + {NumberOfVesselsMedium} + {NumberOfVesselsLarge}) > 0', '=({NumberOfVesselsSmall} * 63) + ({NumberOfVesselsMedium} * 125) + ({NumberOfVesselsLarge} * 375)', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Boat Renewal Fee', 'Default', null, to_date('04-02-2015 15:00:09', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:56:51', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1538, 448, 449, 'Out Of State Winery Application', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) in (''41'', ''OSWW'') and {OutOfStateWineryProductionFee} is not null', '=case {OutOfStateWineryProductionFee}
           when ''Less than 1,000 gallons''                          then 63
           when ''Between 1,000 and 2,500 gallons''        then 125
           when ''Between 2,500 and 30,000 gallons''      then 250
           when ''Between 30,000 and 50,000 gallons''    then 375
           when ''Between 50,000 and 100,000 gallons'' then 938
           when ''Between 100,000 and 150,000 gallons'' then 938
           when ''Between 150,000 and 250,000 gallons'' then 938
end case', 'CreatedDate', null, '={GLAccountCode}', 'Out Of State Winery Application Fee', 'Default', null, to_date('05-02-2015 10:00:54', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:26:32', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1539, 448, 638, 'Out Of State Winery Renewal Fee', '={LicenseTypeCode} in (''41'', ''OSWW'') and {OutOfStateWineryProductionFee} is not null', '=case {OutOfStateWineryProductionFee}
           when ''Less than 1,000 gallons''                          then 63
           when ''Between 1,000 and 2,500 gallons''        then 125
           when ''Between 2,500 and 30,000 gallons''      then 250
           when ''Between 30,000 and 50,000 gallons''    then 375
           when ''Between 50,000 and 100,000 gallons'' then 938
           when ''Between 100,000 and 150,000 gallons'' then 938
           when ''Between 150,000 and 250,000 gallons'' then 938
end case', 'CreatedDate', null, '={GLAccountCode}', 'Out of State Winery Renewal Fee', 'Default', null, to_date('05-02-2015 10:01:48', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:27:14', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (878, 448, 900, 'Product Registration Amendment Fee', null, '={Product Count For Fee}*10', 'CREATEDDATE', null, 'AMD', 'Product Registration Amendment Fee', 'Default', null, to_date('04-08-2014 13:26:50', 'dd-mm-yyyy hh24:mi:ss'), 'CNELSON', to_date('19-03-2015 09:30:04', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1078, 448, 1018, 'Miscellaneous Revenue Transaction Fee', '={FeeAmount}>0', '={FeeAmount}', 'CurrentDate', null, '={GLAccountCode}', '={FeeDescription}', 'Relationship', 'Legal Entity (PROC:)', to_date('26-01-2015 13:14:28', 'dd-mm-yyyy hh24:mi:ss'), 'JLENON', to_date('17-04-2015 18:39:57', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1218, 448, 449, 'Craft Distillery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''07''', '938', 'CreatedDate', null, '={GLAccountCode}', 'Craft Distillery Application Fee', 'Default', null, to_date('04-02-2015 09:17:17', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:10:20', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1219, 448, 638, 'Craft Distillery Renewal Fee', '={LicenseTypeCode} = ''07''', '938', 'CreatedDate', null, '={GLAccountCode}', 'Craft Distillery Renewal Fee', 'Default', null, to_date('04-02-2015 09:18:25', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:10:38', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1238, 448, 449, 'Rectifier and Blender Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''15''', '7500', 'CreatedDate', null, '={GLAccountCode}', 'Rectifier and Blender Application Fee', 'Default', null, to_date('04-02-2015 09:45:45', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:47:21', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1239, 448, 638, 'Rectifier and Blender Renewal Fee', '={LicenseTypeCode} = ''15''', '7500', 'CreatedDate', null, '={GLAccountCode}', 'Rectifier and Blender Renewal Fee', 'Default', null, to_date('04-02-2015 09:46:44', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:47:44', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1240, 448, 449, 'Plenary Distillery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''16''', '12500', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Distillery Application Fee', 'Default', null, to_date('04-02-2015 09:48:11', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:30:05', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1241, 448, 638, 'Plenary Distillery Renewal Fee', '={LicenseTypeCode} = ''16''', '12500', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Distillery Renewal Fee', 'Default', null, to_date('04-02-2015 09:49:08', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:30:25', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1242, 448, 449, 'Limited Distillery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''17''', '3750', 'CreatedDate', null, '={GLAccountCode}', 'Limited Distillery Application Fee', 'Default', null, to_date('04-02-2015 09:51:25', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:21:30', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2418, 448, 2058, 'Limited Brewery Amendment Fee', '={LicenseTypeCode} = ''11'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '=case {AmendmentTypeCode}
           when ''PersonAndPlace'' then
(case {LimitedBreweryProduction}
           when ''UP TO 50,000 BBLS''             then 1250
           when ''50,000 TO 100,000 BBLS''   then 2500
           when ''100,000 TO 200,000 BBLS'' then 5000
           when ''200,000 TO 300,000 BBLS'' then 7500
end
) * 0.20
else
(case {LimitedBreweryProduction}
           when ''UP TO 50,000 BBLS''             then 1250
           when ''50,000 TO 100,000 BBLS''   then 2500
           when ''100,000 TO 200,000 BBLS'' then 5000
           when ''200,000 TO 300,000 BBLS'' then 7500
end
) * 0.10
end case', 'CreatedDate', null, '={GLAccountCode}', 'Limited Brewery Amendment Fee', 'Default', null, to_date('25-03-2015 18:23:14', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:08:38', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1778, 448, 449, 'Plenary Winery Wholesale Privilege', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''21'' and {PlenaryWholesalePrivilegeFee} = ''Y''', '=case {PlenaryWineryProductionForFee}
           when ''Less than 50,000 gallons''                          then 100
           when ''Between 50,000 and 100,000 gallons'' then 250
           when ''Between 100,000 and 150,000 gallons'' then 500
           when ''Between 150,000 and 250,000 gallons'' then 1000
           else                                                                                       0.01
end case', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Winery Wholesale Privilege Fee', 'Default', null, to_date('06-03-2015 14:50:48', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('17-04-2015 19:42:37', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2458, 448, 2058, 'Restricted Brewery Amendment Fee', '={LicenseTypeCode} = ''08'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '=case {AmendmentTypeCode}
           when ''PersonAndPlace'' then
(case {RestrictedBreweryProduction}
           when ''UP TO 1,000 BBLS''        then 1250
           when ''1,001 TO 2,000 BBLS''   then 1500
           when ''2,001 TO 3,000 BBLS''   then 1750
           when ''3,001 TO 4,000 BBLS''   then 2000
           when ''4,001 TO 5,000 BBLS''   then 2250
           when ''5,001 TO 6,000 BBLS''   then 2500
           when ''6,001 TO 7,000 BBLS''   then 2750
           when ''7,001 TO 8,000 BBLS''   then 3000
           when ''8,001 TO 9,000 BBLS''   then 3250
           when ''9,001 TO 10,000 BBLS'' then 3500
end
) * 0.20
else
(case {RestrictedBreweryProduction}
           when ''UP TO 1,000 BBLS''        then 1250
           when ''1,001 TO 2,000 BBLS''   then 1500
           when ''2,001 TO 3,000 BBLS''   then 1750
           when ''3,001 TO 4,000 BBLS''   then 2000
           when ''4,001 TO 5,000 BBLS''   then 2250
           when ''5,001 TO 6,000 BBLS''   then 2500
           when ''6,001 TO 7,000 BBLS''   then 2750
           when ''7,001 TO 8,000 BBLS''   then 3000
           when ''8,001 TO 9,000 BBLS''   then 3250
           when ''9,001 TO 10,000 BBLS'' then 3500
end
) * 0.10
end case', 'CreatedDate', null, '={GLAccountCode}', 'Restricted Brewery Amendment Fee', 'Default', null, to_date('25-03-2015 18:37:37', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2319, 448, 2058, 'Special Permit Golf Facility Amendment Fee', '={LicenseTypeCode} = ''40'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 2000 * 0.10
             when ''PlaceToPlace''       then 2000 * 0.10
             when ''PersonAndPlace'' then 2000 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Special Permit Golf Facility Amendment Fee', 'Default', null, to_date('25-03-2015 15:48:14', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2320, 448, 2058, 'State Beverage Distributor Amendment Fee', '={LicenseTypeCode} = ''19'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 1031 * 0.10
             when ''PlaceToPlace''       then 1031 * 0.10
             when ''PersonAndPlace'' then 1031 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'State Beverage Distributor Amendment Fee', 'Default', null, to_date('25-03-2015 15:49:22', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2321, 448, 2058, 'Transportation Amendment Fee', '={LicenseTypeCode} = ''20'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 625 * 0.10
             when ''PlaceToPlace''       then 625 * 0.10
             when ''PersonAndPlace'' then 625 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Transportation Amendment Fee', 'Default', null, to_date('25-03-2015 15:50:20', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:11:08', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2322, 448, 2058, 'Warehouse Receipts Amendment Fee', '={LicenseTypeCode} = ''27'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 375 * 0.10
             when ''PlaceToPlace''       then 375 * 0.10
             when ''PersonAndPlace'' then 375 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Warehouse Receipts Amendment Fee', 'Default', null, to_date('25-03-2015 15:52:56', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:11:08', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2323, 448, 2058, 'Wine Blending Amendment Fee', '={LicenseTypeCode} = ''09'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 625 * 0.10
             when ''PlaceToPlace''       then 625 * 0.10
             when ''PersonAndPlace'' then 625 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Wine Blending Amendment Fee', 'Default', null, to_date('25-03-2015 15:53:54', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:11:08', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2324, 448, 2058, 'Wine Wholesale Amendment Fee', '={LicenseTypeCode} = ''26'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 3750 * 0.10
             when ''PlaceToPlace''       then 3750 * 0.10
             when ''PersonAndPlace'' then 3750 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Wine Wholesale Amendment Fee', 'Default', null, to_date('25-03-2015 15:54:50', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:11:08', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2338, 448, 2058, 'Retail Transit Plane/Train Amendment Fee', '={LicenseTypeCode} = ''13'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'') and {RetailTransitType} in (''Plane'', ''Rail'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 375 * 0.10
             when ''PlaceToPlace''       then 375 * 0.10
             when ''PersonAndPlace'' then 375 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Plane/Train Amendment Fee', 'Default', null, to_date('25-03-2015 16:18:35', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2360, 448, 2058, 'Farm Winery Amendment Fee', '={LicenseTypeCode} = ''22'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '=case {AmendmentTypeCode}
           when ''PersonAndPlace'' then
((case {FarmWineryProduction}
           when ''UP TO 1,000 GALLONS''          then 63
           when ''1,001 TO 2,500 GALLONS''     then 125
           when ''2,501 TO 30,000 GALLONS''   then 250
           when ''30,001 TO 50,000 GALLONS'' then 375
end) +
(case {FarmWineryWholesalePrivilege}
           when ''Y'' then 250
           else 0
end)) * 0.20
else
((case {FarmWineryProduction}
           when ''UP TO 1,000 GALLONS''          then 63
           when ''1,001 TO 2,500 GALLONS''     then 125
           when ''2,501 TO 30,000 GALLONS''   then 250
           when ''30,001 TO 50,000 GALLONS'' then 375
end) +
(case {FarmWineryWholesalePrivilege}
           when ''Y'' then 250
           else 0
end)) * 0.10
end case

', 'CreatedDate', null, '={GLAccountCode}', 'Farm Winery Amendment Fee', 'Default', null, to_date('25-03-2015 16:42:39', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:05:37', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (761, 448, 758, 'Product Registration Application Fee', null, '={Product Count}*23', 'CREATEDDATE', null, 'BRD', 'Product Registration Application Fee', 'Default', null, to_date('21-05-2014 15:39:42', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('25-03-2015 19:14:08', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (938, 448, 918, 'Petiton Filing Fee', null, '= {FilingFee}', 'CurrentDate', null, 'PETF', 'Petition Filing Fee', 'Default', null, to_date('22-09-2014 11:27:53', 'dd-mm-yyyy hh24:mi:ss'), 'ALEX', to_date('30-03-2015 11:16:10', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1818, 448, 638, 'Farm Winery Wholesale Privilege', '={LicenseTypeCode} = ''22'' and {FarmWineryWholesalePrivForFee} = ''Y''', '100', 'CreatedDate', null, '={GLAccountCode}', 'Farm Winery Wholesale Privilege Fee', 'Default', null, to_date('09-03-2015 09:47:45', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:15:52', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1858, 448, 638, 'Out Of State Wholesale Privilege', '={LicenseTypeCode} in (''41'', ''OSWW'') and {OutOfStateWholesalePrivForFee} = ''Y''', '=case {OutOfStateWineryProductionFee}
           when ''Less than 1,000 gallons''                          then 100
           when ''Between 1,000 and 2,500 gallons''        then 100
           when ''Between 2,500 and 30,000 gallons''      then 100
           when ''Between 30,000 and 50,000 gallons''    then 100
           when ''Between 50,000 and 100,000 gallons'' then 250
           when ''Between 100,000 and 150,000 gallons'' then 500
           when ''Between 150,000 and 250,000 gallons'' then 1000
  else 0.01
end case', 'CreatedDate', null, '={GLAccountCode}', 'Out Of State Wholesale Privilege Fee', 'Default', null, to_date('09-03-2015 10:17:31', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:24:44', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1878, 448, 449, 'Out Of State Wholesale Privilege', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) in (''41'', ''OSWW'') and {OutOfStateWholesalePrivForFee} = ''Y''', '=case {OutOfStateWineryProductionFee}
           when ''Less than 1,000 gallons''                          then 100
           when ''Between 1,000 and 2,500 gallons''        then 100
           when ''Between 2,500 and 30,000 gallons''      then 100
           when ''Between 30,000 and 50,000 gallons''    then 100
           when ''Between 50,000 and 100,000 gallons'' then 250
           when ''Between 100,000 and 150,000 gallons'' then 500
           when ''Between 150,000 and 250,000 gallons'' then 1000
  else 0.01
end case', 'CreatedDate', null, '={GLAccountCode}', 'Out Of State Wholesale Privilege Fee', 'Default', null, to_date('09-03-2015 10:28:06', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:25:16', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1498, 448, 638, 'Supplementary Limited Distillery Renewal Fee', '={LicenseTypeCode} = ''18'' and {SuppLimitedDistilleryForFee} is not null', '=case {SuppLimitedDistilleryForFee}
           when ''UP TO 5,000 GALS''                        then 313
           when ''NOT MORE THAN 10,000 GALS'' then 625
           when ''UNLIMITED QUANTITY''                then 1250
end case', 'CreatedDate', null, '={GLAccountCode}', 'Supplementary Limited Distillery Renewal Fee', 'Default', null, to_date('05-02-2015 09:00:02', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:03:01', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1439, 448, 638, 'Limited Brewery Renewal Fee', '={LicenseTypeCode} = ''11'' and {LimitedBreweryProductionForFee} is not null', '=case {LimitedBreweryProductionForFee}
           when ''UP TO 50,000 BBLS''             then 1250
           when ''50,000 TO 100,000 BBLS''   then 2500
           when ''100,000 TO 200,000 BBLS'' then 5000
           when ''200,000 TO 300,000 BBLS'' then 7500
end case', 'CreatedDate', null, '={GLAccountCode}', 'Limited Brewery Renewal Fee', 'Default', null, to_date('05-02-2015 08:42:39', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:20:27', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1478, 448, 449, 'Supplementary Limited Distillery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''18'' and {SuppLimitedDistilleryForFee} is not null', '=case {SuppLimitedDistilleryForFee}
           when ''UP TO 5,000 GALS''                        then 313
           when ''NOT MORE THAN 10,000 GALS'' then 625
           when ''UNLIMITED QUANTITY''                then 1250
end case', 'CreatedDate', null, '={GLAccountCode}', 'Supplementary Limited Distillery Application Fee', 'Default', null, to_date('05-02-2015 08:51:56', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 20:02:42', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1518, 448, 449, 'Farm Winery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''22'' and {FarmWineryProductionForFee} is not null', '=case {FarmWineryProductionForFee}
           when ''UP TO 1,000 GALLONS''          then 63
           when ''1,001 TO 2,500 GALLONS''     then 125
           when ''2,501 TO 30,000 GALLONS''   then 250
           when ''30,001 TO 50,000 GALLONS'' then 375
end case', 'CreatedDate', null, '={GLAccountCode}', 'Farm Winery Application Fee', 'Default', null, to_date('05-02-2015 09:42:37', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:13:36', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1519, 448, 638, 'Farm Winery Renewal Fee', '={LicenseTypeCode} = ''22'' and {FarmWineryProductionForFee} is not null', '=case {FarmWineryProductionForFee}
           when ''UP TO 1,000 GALLONS''          then 63
           when ''1,001 TO 2,500 GALLONS''     then 125
           when ''2,501 TO 30,000 GALLONS''   then 250
           when ''30,001 TO 50,000 GALLONS'' then 375
end case', 'CreatedDate', null, '={GLAccountCode}', 'Farm Winery Renewal Fee', 'Default', null, to_date('05-02-2015 09:44:57', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:14:05', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2738, 448, 2718, 'Appeal Filing Fee', null, '100', 'CreatedDate', null, 'APLF', 'Appeal Filing Fee', 'Default', null, to_date('30-03-2015 08:36:16', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('30-03-2015 11:17:46', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1178, 448, 449, 'Plenary Brewery Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''10''', '10625', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Brewery Application Fee', 'Default', null, to_date('03-02-2015 16:34:48', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:28:30', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1179, 448, 638, 'Plenary Brewery Renewal Fee', '={LicenseTypeCode} = ''10''', '10625', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Brewery Renewal Fee', 'Default', null, to_date('03-02-2015 16:36:03', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:28:49', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2518, 448, 2058, 'Retail Transit Limousine Amendment Fee', '={LicenseTypeCode} = ''13'' and {RetailTransitType} = ''Limousine'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '=case {AmendmentTypeCode}
             when ''PersonToPerson'' then ({NumberOfVehicles} * 31) * 0.10
             when ''PlaceToPlace''       then ({NumberOfVehicles} * 31) * 0.10
             when ''PersonAndPlace'' then ({NumberOfVehicles} * 31) * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Limousine Amendment Fee', 'Default', null, to_date('25-03-2015 20:23:21', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (1318, 448, 449, 'Retail Transit Limousine Application Fee', '=nvl({OnlineLicenseTypeCode},{LicenseTypeCode}) = ''13'' and {RetailTransitTypeForFee} = ''Limousine'' and {NumberOfVehicles} > 0', '={NumberOfVehicles} * 31', 'CreatedDate', null, '={GLAccountCode}', 'Retail Transit Limousine Application Fee', 'Default', null, to_date('04-02-2015 11:18:57', 'dd-mm-yyyy hh24:mi:ss'), 'POSSESYS', to_date('17-04-2015 19:57:47', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2239, 448, 2058, 'Limited Wholesale Amendment Fee', '={LicenseTypeCode} = ''25'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 1875 * 0.10
             when ''PlaceToPlace''       then 1875 * 0.10
             when ''PersonAndPlace'' then 1875 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Limited Wholesale Amendment Fee', 'Default', null, to_date('25-03-2015 15:28:54', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:08:38', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2240, 448, 2058, 'Plenary Brewery Amendment Fee', '={LicenseTypeCode} = ''10'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 10625 * 0.10
             when ''PlaceToPlace''       then 10625 * 0.10
             when ''PersonAndPlace'' then 10625 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Brewery Amendment Fee', 'Default', null, to_date('25-03-2015 15:30:11', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2241, 448, 2058, 'Plenary Distillery Amendment Fee', '={LicenseTypeCode} = ''16'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 12500 * 0.10
             when ''PlaceToPlace''       then 12500 * 0.10
             when ''PersonAndPlace'' then 12500 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Distillery Amendment Fee', 'Default', null, to_date('25-03-2015 15:31:24', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2258, 448, 2058, 'Plenary Wholesale Amendment Fee', '={LicenseTypeCode} = ''23'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 8750 * 0.10
             when ''PlaceToPlace''       then 8750 * 0.10
             when ''PersonAndPlace'' then 8750 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Wholesale Amendment Fee', 'Default', null, to_date('25-03-2015 15:38:48', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2278, 448, 2058, 'Plenary Winery Amendment Fee', '={LicenseTypeCode} = ''21'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '=case {AmendmentTypeCode}
           when ''PersonAndPlace'' then
(938 +
case {PlenaryWineryWholesalePrivileg}
  when ''Y'' then
(case {PlenaryWineryProduction}
           when ''Less than 50,000 gallons''                          then 100
           when ''Between 50,000 and 100,000 gallons'' then 250
           when ''Between 100,000 and 150,000 gallons'' then 500
           when ''Between 150,000 and 250,000 gallons'' then 1000
end)
else 0
  end 
  ) * 0.20
else    
(938 + 
case {PlenaryWineryWholesalePrivileg}
  when ''Y'' then
(case {PlenaryWineryProduction}
           when ''Less than 50,000 gallons''                          then 100
           when ''Between 50,000 and 100,000 gallons'' then 250
           when ''Between 100,000 and 150,000 gallons'' then 500
           when ''Between 150,000 and 250,000 gallons'' then 1000
end)
else 0
  end 
  ) * 0.10
end case', 'CreatedDate', null, '={GLAccountCode}', 'Plenary Winery Amendment Fee', 'Default', null, to_date('25-03-2015 15:40:06', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2298, 448, 2058, 'Public Warehouse Amendment Fee', '={LicenseTypeCode} = ''28'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 500 * 0.10
             when ''PlaceToPlace''       then 500 * 0.10
             when ''PersonAndPlace'' then 500 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Public Warehouse Amendment Fee', 'Default', null, to_date('25-03-2015 15:41:23', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:09:56', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

insert into feescheduleplus.feedefinition (FEEDEFINITIONID, FEESCHEDULEID, DATASOURCEID, DESCRIPTION, CONDITION, AMOUNT, EFFECTIVEDATECOLUMNNAME, FEECATEGORYID, GLACCOUNTNUMBER, FEEDESCRIPTION, RESPONSIBLEPARTYGENERATIONTYPE, RESPONSIBLEPARTYRELATIONSHIP, CREATEDDATE, CREATEDBY, UPDATEDDATE, UPDATEDBY)
values (2299, 448, 2058, 'Rectifier and Blender Amendment Fee', '={LicenseTypeCode} = ''15'' and {IsStateIssued} = ''Y'' and {AmendmentTypeCode} in (''PersonToPerson'', ''PlaceToPlace'', ''PersonAndPlace'')
', '= case {AmendmentTypeCode}
             when ''PersonToPerson'' then 7500 * 0.10
             when ''PlaceToPlace''       then 7500 * 0.10
             when ''PersonAndPlace'' then 7500 * 0.20
end case', 'CreatedDate', null, '={GLAccountCode}', 'Rectifier and Blender Amendment Fee', 'Default', null, to_date('25-03-2015 15:42:29', 'dd-mm-yyyy hh24:mi:ss'), 'MICHEL', to_date('31-03-2015 11:18:50', 'dd-mm-yyyy hh24:mi:ss'), 'OUTRIDERSYS');

prompt Done.
