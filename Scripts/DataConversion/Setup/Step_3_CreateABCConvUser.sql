/* This script creates the ABC Conversion User needed to run the POSSECONV tool in NLC */

declare
  t_UserId      pls_Integer;

begin 
  api.pkg_logicaltransactionupdate.ResetTransaction;
  -- Default is the 'Users with Oracle logons' scheme as determined from admin.usermanagementschemes
  t_UserId := api.pkg_UserUpdate.New('Default', 'ABC Conversion', 'ABCCONV' , 'ABCCONV', 'njconv');
  -- set mandatory columns on user 
  api.pkg_columnupdate.SetValue(t_UserId, 'FirstName', 'NJABC');
  api.pkg_columnupdate.SetValue(t_UserId, 'LastName', 'Conversion');
  api.pkg_columnupdate.SetValue(t_UserId, 'Active', 'Y'); 
  
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
  
end;  
