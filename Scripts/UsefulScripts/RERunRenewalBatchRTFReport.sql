/*
Step 1: Run this for your batch notification Job Id
Step 2: Find the Process Server RunRTFReport for this batch and reschedule.
*/

declare
  t_RelationshipToDelete number(9);
  t_BatchNotificationJobId number(9) := 60908995; --Batch Notification Job to delete current RTF Report.
begin
  select r.RelationshipId
  --, r.fromobjectid, r.toobjectid, ep.EndPointId, ep.name --Testing
    into t_RelationshipToDelete
  from api.relationships r
  join api.endpoints ep on ep.endpointid = r.endpointid
  where r.fromobjectid = t_BatchNotificationJobId
  and ep.name = 'BatchRenewalLetter'
  order by relationshipid;

  api.pkg_relationshipupdate.Remove(t_RelationshipToDelete);
  dbms_output.put_line('Relationship being removed: ' || t_RelationshipToDelete);
  
  api.pkg_logicaltransactionupdate.endTransaction();
end;