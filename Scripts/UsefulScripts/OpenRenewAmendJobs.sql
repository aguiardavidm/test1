/*
The warning that appears on the internal site for Product Renewal / Amendments indicating there are open jobs,
will cause them to NOT auto process and will require an Administrative reivew.
This warning uses a Global Expression to determine what warning should display. (RegistrantOpenPRJobWarning)
Which then uses a Global Expression to find what open jobs exist for Brand. (RegistrantOpenPRJobWarning)
This looks at two python details to find and comma seperate each open job OpenPRAmendments and OpenPRRenewals

The SQL below uses a Registrant (Legal Entity) object ID to search the same Endpoints for the statuses these 
expressions use to find the same Jobs but through SQL since, in order to perform the same query you would 
have to run all of these python statements in Harness.
*/
select r.toobjectid,
       api.pkg_columnquery.value(r.toobjectid, 'ObjecTDefDescription') JobName,
       api.pkg_columnquery.value(r.toobjectid, 'StatusName') JobStatus
from api.relationships r
join api.endpoints ep on ep.EndPointId = r.EndPointId
where ep.Name in ('ProductEndPoint', 'ProductRenewal')
and r.fromobjectid = 27763581
and api.pkg_columnquery.value(r.toobjectid, 'StatusName') in ('NEW','REVIEW', 'PENDRT')