declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  /*
   * KNINJA BREWING CO.: 24009991
   * MARIA KNIGGE: 21525155
   * MARIA E KNIGGE: 22812730
   */

  t_Counter                             number := 0;
  t_Products                            udt_IdList;
  t_LEId                                udt_Id := :a_LEId;
  t_EndpointId                          udt_Id;
  t_ExpirationDate                      date := trunc(sysdate) + :a_Days;
  t_NonRenewalNoticeDate                date;
begin

  api.pkg_LogicalTransactionUpdate.ResetTransaction();

  t_EndpointId := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_LegalEntity', 'Product');

  -- Get NonRenewalNoticeDate from system settings, or pick a non-renewal date using the code that
  -- is commented out
  select to_date(
      api.pkg_ColumnQuery.Value(o.ObjectId, 'PRNonRenewalNoticeDay') || '-'
      || substr(api.pkg_ColumnQuery.Value(o.ObjectId, 'PRNonRenewalNoticeMonth'), 0, 3) || '-'
      || (extract(year from sysdate) + 1))
  into t_NonRenewalNoticeDate
  from api.objects o
  where o.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
      'SystemSettingsNumber', 1);
  --t_NonRenewalNoticeDate := trunc(sysdate) + t_Days + 1;

  select r.ToObjectId
  bulk collect into t_Products
  from
    api.relationships r
    join query.o_ABC_Product p
        on r.ToObjectId = p.ObjectId
  where r.EndpointId = t_EndpointId
    and r.FromObjectId = t_LEId
    and EXTRACT(YEAR from p.ExpirationDate) != :a_Year;

  dbms_output.put_Line('Number of Products expiring outside of ' || :a_Year || ': ' ||
      t_Products.count());

  for i in 1..t_Products.count() loop
    api.pkg_ColumnUpdate.SetValue(t_Products(i), 'ExpirationDate', t_ExpirationDate);
    api.pkg_ColumnUpdate.SetValue(t_Products(i), 'State', 'Current');
    api.pkg_ColumnUpdate.SetValue(t_Products(i), 'NonRenewalNoticeDate', t_NonRenewalNoticeDate);
    t_Counter := t_Counter + 1;

    if mod(t_Counter, 500) = 0 then
      dbms_output.put_line(t_Counter);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  dbms_output.put_line('Number of Products updated: ' || t_Counter);
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

end;

