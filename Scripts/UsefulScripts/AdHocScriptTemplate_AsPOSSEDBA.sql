/*-----------------------------------------------------------------------------
 * Author:
 * Expected run time:
 * Purpose:
 *
 *---------------------------------------------------------------------------*/
declare
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  t_IssueNumber                         varchar2(6) := '######';
  t_LogicalTransactionId                pls_Integer;
  t_ObjectId                            pls_Integer;
  t_ObjectList                          udt_Id;
  t_NoteText                            varchar2(4000);
  t_RunAsUser                           varchar2(20) := 'POSSEDBA';
  t_TimeEnd                             number(15);
  t_TimeStart                           number(15);
  t_UpdatedCount                        pls_Integer := 0;

  procedure dbug (
    a_Text                              varchar2
  ) is
  begin

    dbms_output.put_line(a_Text);

  end;

  procedure createCaseNote (
    a_ObjectId                          pls_integer,
    a_NoteText                          varchar2
  ) is
    t_NoteDefId                         pls_integer;
    t_NoteId                            pls_integer;
    t_NoteTag                           varchar2(6) := 'GEN';
  begin

  select nd.NoteDefId
  into t_NoteDefId
  from api.NoteDefs nd
  where nd.Tag = t_NoteTag;

  t_NoteId := api.pkg_NoteUpdate.New(a_ObjectId, t_NoteDefId, 'Y', a_NoteText);

  end;

begin

  if user != t_RunAsUser then
    raise_application_error(-20000, 'Script must be run as ' || t_RunAsUser || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;
  t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction();
  dbug('Logical Transaction: ' || t_LogicalTransactionId);

  -- The 'BEFORE' section is useful if you want to print initial data or take an initial count.
  dbug(chr(13) || chr(10) || '---BEFORE---');

  -- 'DURING' is useful if you want to print data being changed.
  dbug(chr(13) || chr(10) || '---DURING---');
  t_TimeStart := dbms_utility.get_time();

  /*
  -- If you are looping through a sizable list, the following code is a good way to commit as you go
  for i in 1..t_ObjectList.count() loop
    -- modify t_ObjectList(i)

    t_UpdatedCount := t_UpdatedCount + 1;

    if mod(t_UpdatedCount, 500) = 0 then
      dbms_output.put_line(t_UpdatedCount);
      objmodelphys.pkg_eventqueue.endsession('N');
      api.pkg_logicaltransactionupdate.EndTransaction();
      commit;
    end if;
  end loop;

  objmodelphys.pkg_eventqueue.endsession('N');
  */

  -- Populate Case Note Text & create on object
  t_NoteText := to_char(sysdate, 'Mon dd, yyyy HH12:MI:SS PM') || chr(13) || chr(10) ||
      'Job Completed with wrong outcome. See Issue ' || t_IssueNumber || '.';
  createCaseNote(t_ObjectId, t_NoteText);

  t_TimeEnd := dbms_utility.get_time();
  dbug('Time Elapsed: ' || (t_TimeEnd - t_TimeStart) / 100);

  api.pkg_logicaltransactionupdate.EndTransaction();

  dbug(chr(13) || chr(10) || '---AFTER---');
  dbug('Updated Object Count: ' || t_UpdatedCount);

  --commit;

end;
