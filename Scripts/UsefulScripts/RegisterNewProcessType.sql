/*******************************************************************************************
 * Description: Registers a new ProcessType
 * Author: 
 * Last Modified: 
 * Run-time: < 1 sec
 * Task Number: 
 ********************************************************************************************/
declare
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_definition.udt_StringList;

  t_NameofScript                        varchar2(4000) := 'RegisterNewProcessType.sql';
  t_Schema                              varchar2(4000) := 'POSSEDBA';
  t_DatabaseName                        varchar2(4000);
  t_PrintDebug                          boolean := True;
  t_TimeStart                           timestamp := systimestamp;
  t_TimeEnd                             timestamp;
  t_TimeDiff                            float;

  t_Count                               number := 0;
  t_ProcessTypeName1                    varchar2(30) := 'p_PROCESSNAME1';
  t_ProcessTypeName2                    varchar2(30) := 'p_PROCESSNAME2';

  procedure dbug (
    a_Text                              varchar2
  ) is
    t_Status                            number;
  begin
    if t_PrintDebug then
      dbms_output.put_line(a_Text);
    end if;

    -- pipe debug
    pkg_debug.putline(a_Text);
  end;

begin
  dbug('Starting Script: ' || t_NameOfScript || ' at: ' || to_char(t_TimeStart));

  if user != t_Schema then
    raise_application_error(-20000, 'Script must be run as ' || t_Schema || '.');
  end if;

  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  rollback;

  for pt in (
      select ProcessTypeId, Name
      from api.ProcessTypes
      where Name in (t_ProcessTypeName1, t_ProcessTypeName2)
      ) loop
    select count(1)
    into t_Count
    from query.o_ProcessType
    where ProcessTypeId = pt.ProcessTypeId;

    if t_Count = 0 then
      api.pkg_ObjectUpdate.RegisterExternalObject('o_ProcessType', pt.ProcessTypeId);
      dbug(pt.Name || ' registered.');
    else
      dbug(pt.Name || ' already registered.');
    end if;
  end loop;

  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;

  t_TimeEnd := systimestamp;
  t_TimeDiff := (EXTRACT (DAY FROM t_TimeEnd-t_TimeStart) * 24 * 60 * 60
                      + EXTRACT (HOUR FROM t_TimeEnd-t_TimeStart) * 60 * 60
                      + EXTRACT (MINUTE FROM t_TimeEnd-t_TimeStart) * 60
                      + EXTRACT (SECOND FROM t_TimeEnd-t_TimeStart));
  dbug('');
  dbug('Ending Script:   ' || t_NameOfScript || ' at: ' || to_char(t_TimeEnd));
  dbug('Run Time:        ' || t_TimeDiff || 's');
end;
/
