/*---------------------------------------------------------------------------
 * Requirements: Must have a started j_ABC_PrApplication
 * Will create 100 products with a sequence appended to the name
 * Takes 20 Seconds(200) 12 min for (7000)
 *-------------------------------------------------------------------------*/
declare 
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  t_ProductId udt_ID;
  t_JobId udt_ID := 24350070; -- Put your job id here
  t_ProductDefId udt_ID 
    := api.pkg_configQuery.ObjectDefIdForName('o_ABC_Product');
  t_JobToProductRelID udt_ID
    := api.pkg_ConfigQuery.EndpointIdForName('j_ABC_PRApplication', 'Product');
  t_ProductToProductType udt_ID
    := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_Product', 'ProductType');
  t_ProductTypeID udt_ID;
  t_BaseName varchar2(4000) := 'Sake Sake'; -- Base Name of Products 
  t_CreatedRelID udt_ID;
  t_Counter number := 100;
begin
  api.pkg_LogicalTransactionUpdate.ResetTransaction();
  
  select objectid
  into t_ProductTypeId
  from query.o_Abc_Producttype p
  where lower(p.type) like ('%sake%');
  
  for a in 1..251 loop 
    -- Create Product 
    t_ProductId := api.pkg_ObjectUpdate.New(t_ProductDefID);
    
    -- Set Product Details
    api.pkg_ColumnUpdate.SetValue(t_ProductId, 'Name', t_BaseName || ' ' || to_Char(a));
    t_CreatedRelID := api.Pkg_Relationshipupdate.New(t_ProductToProductType, t_ProductID, t_ProductTypeId);
    api.pkg_ColumnUpdate.SetValue(t_ProductId, 'TTBCOLA', 'Yes');
    
    --Relate to application
    t_CreatedRelID := api.Pkg_Relationshipupdate.New(t_JobToProductRelID, t_JobId, t_ProductId);
  end loop;
  
  objmodelphys.pkg_eventqueue.endsession('N');
  api.pkg_LogicalTransactionUpdate.EndTransaction();
  commit;
end;


