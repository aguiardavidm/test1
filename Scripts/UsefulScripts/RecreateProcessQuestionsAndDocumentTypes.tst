PL/SQL Developer Test script 3.0
88
-- Created on 12/2/2020 by DAVID.AGUIAR
declare
  -- Local variables here
  id api.pkg_definition.udt_IdList;
  processid api.pkg_definition.udt_Id := :ProcId;
  ProcessName varchar2(4000);
begin
  api.pkg_logicaltransactionupdate.ResetTransaction();

  ProcessName := api.pkg_columnquery.value(processid, 'ObjectDefName');

  if ProcessName = 'p_ABC_SubmitResolution' then
    dbms_output.put_line('Submit Resolution');
    -- Questions
    select relationshipid
      bulk collect into id
      from query.r_Abc_Muniqaresponse
     where submitresolutionid = processid;
    for i in 1 .. id.count loop
      api.Pkg_Relationshipupdate.Remove(id(i));
    end loop;

    -- Document Types
    select relationshipid
      bulk collect into id
      from query.r_ABC_SubmitResRespDocType
     where submitresolutionid = processid;
    for i in 1 .. id.count loop
      api.Pkg_Relationshipupdate.Remove(id(i));
    end loop;

    abc.pkg_abc_workflow.CopySubmitResolutionQuestions(processid,sysdate);

  end if;

  if ProcessName = 'p_ABC_MunicipalityReview' then
    dbms_output.put_line('Muni Review');
    -- Questions
    select relationshipid
      bulk collect into id
      from query.r_ABC_MunicipalReviewQARespons r
     where r.MunicipalReviewId = processid;
    for i in 1 .. id.count loop
      api.Pkg_Relationshipupdate.Remove(id(i));
    end loop;

    -- Document Types
    select relationshipid
      bulk collect into id
      from query.r_ABC_MuniReviewRespDocType r
     where r.MunicipalReviewId = processid;
    for i in 1 .. id.count loop
      api.Pkg_Relationshipupdate.Remove(id(i));
    end loop;

    api.pkg_columnupdate.SetValue(processid,'IsEndorsementChecked','Y');
    abc.pkg_abc_workflow.CopyMunicipalReviewQuestions(processid,sysdate);

  end if;
  
  if ProcessName = 'p_ABC_PoliceReview' then
    dbms_output.put_line('Police Review');
    -- Questions
    select relationshipid
      bulk collect into id
      from query.r_ABC_PoliceReviewQAResponse r
     where r.PoliceReviewId = processid;
    for i in 1 .. id.count loop
      api.Pkg_Relationshipupdate.Remove(id(i));
    end loop;

    -- Document Types
    select relationshipid
      bulk collect into id
      from query.r_ABC_PoliceReviewRespDocType r
     where r.PoliceReviewId = processid;
    for i in 1 .. id.count loop
      api.Pkg_Relationshipupdate.Remove(id(i));
    end loop;

    api.pkg_columnupdate.setvalue(processid,'IsEndorsementChecked','Y');
    abc.pkg_abc_workflow.CopyPoliceReviewQuestions(processid,sysdate);

  end if;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;

