--Query to pull all email addresses of active retial licenses' licensees and their associated public users. 
--Could be sped up with api.pkg_simplesearch, returns in 40 sec so that fine
select listagg(name, ', ') within group(order by name) as name, email
  from 
 (select distinct le.FormattedName as name,
         trim(lower(le.ContactEmailAddress)) as email
  from query.o_Abc_License l
  join query.o_abc_licenseType lt
    on l.LicenseTypeObjectId = lt.ObjectId
  join query.r_ABC_LicenseLicenseeLE rle
    on l.objectid = rle.LicenseObjectId
  join query.o_abc_legalentity le
    on le.ObjectId = rle.LegalEntityObjectId
  where l.state = 'Active'
    and lt.IssuingAuthority = 'Municipality'
    and le.PreferredContactMethod = 'Email'
  union
  select distinct u.FormattedName as name,
         trim(lower(u.EmailAddress)) as email
  from query.o_Abc_License l
  join query.o_abc_licenseType lt
    on l.LicenseTypeObjectId = lt.ObjectId
  join query.r_ABC_LicenseLicenseeLE rle
    on l.objectid = rle.LicenseObjectId
  join query.o_abc_legalentity le
    on le.ObjectId = rle.LegalEntityObjectId
  join query.r_ABC_UserLegalEntity ule
    on ule.LegalEntityObjectId = le.ObjectId
  join query.u_users u
    on ule.UserId = u.ObjectId
  where l.state = 'Active'
    and lt.IssuingAuthority = 'Municipality'
    and le.PreferredContactMethod = 'Email')
Group by email
