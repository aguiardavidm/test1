-- Created on 7/1/2015 by KEATON 
declare 
  -- Local variables here
  t_LicenseDefId          number := api.pkg_configquery.ObjectDefIdForName('o_abc_license');
  t_LicenseObjectId       number;
  t_EstablishmentId       number := :EstId;
  t_EstablishmentEndPoint number := api.pkg_configquery.EndPointIdForName('o_abc_license','Establishment');
  t_EstablishmentRelId    number;
  t_LegalEntityId         number := :LEId;
  t_LegalEntityEndPoint   number := api.pkg_configquery.EndPointIdForName('o_abc_license','Licensee');
  t_LegalEntityRelId      number;
  t_LicenseTypeId         number;
  t_LicenseTypeEndPoint   number := api.pkg_configquery.EndPointIdForName('o_abc_license','LicenseType');
  t_LicenseTypeRelId      number;
  t_MuniciplalityId       number ;
  t_MuniciplalityEndPoint number := api.pkg_configquery.EndPointIdForName('o_abc_license','Office');
  t_MuniciplalityRelId    number;
  t_MunicipalityCode      varchar2(2) := '15';
  t_CountyId              number;
  t_CountyEndPoint        number := api.pkg_configquery.EndPointIdForName('o_abc_license','Region');
  t_CountyRelId           number;
  t_CountyCode            varchar2(2) := '99';
  t_CountyName            varchar2(100) := 'PENDING STATE LICENSE';
  t_LicenseTypeCode       number := :LicenseTypeCode;
  t_OrderOfIssuance       number := abc.muniorderissue9915_seq.nextval;
  t_LicenseNumber         varchar2(100);
  
begin
  -- Test statements here
  
  api.pkg_logicaltransactionupdate.Resettransaction();
  
  --Create a License and Set the License Number
  t_LicenseObjectId := api.pkg_objectupdate.New(t_LicenseDefId);
  
  select r.ObjectId
    into t_CountyId
    from query.o_abc_region r
   where r.CountyCode = t_CountyCode
     and r.Name = t_CountyName; 
  
   select o.ObjectId
    into t_MuniciplalityId
    from query.o_abc_office o
   where o.MunicipalityCode = t_MunicipalityCode
     and o.CountyCode = t_CountyCode;
   
   select lt.ObjectId
     into t_LicenseTypeId
     from query.o_abc_licensetype lt
    where lt.Code = t_LicenseTypeCode;
   
  
  t_LicenseNumber := lpad(t_CountyCode, 2, '00') 
                          || lpad(t_MunicipalityCode, 2, '00') 
                          || '-' || lpad(t_LicenseTypeCode, 2, '00') 
                          || '-' || lpad(t_OrderOfIssuance, 3, '000') 
                          || '-' || lpad(1, 3, '001');
  
  api.pkg_ColumnUpdate.SetValue(t_LicenseObjectId, 'LicenseNumber', t_LicenseNumber);

  api.pkg_columnupdate.SetValue(t_LicenseObjectId, 'ExpirationDate', sysdate+365);
  
  api.pkg_columnupdate.SetValue(t_LicenseObjectId, 'State', 'Active');
  
  api.pkg_columnupdate.SetValue(t_LicenseObjectId, 'IssueDate', sysdate);
  
  --Relate The Establishment
  t_EstablishmentRelId := api.pkg_relationshipupdate.New(t_EstablishmentEndPoint, t_LicenseObjectId, t_EstablishmentId);
  
  --Relate The Legal Entity
  t_LegalEntityRelId := api.pkg_relationshipupdate.New(t_LegalEntityEndPoint, t_LicenseObjectId, t_LegalEntityId);
  
  --Relate The License Type
  t_LicenseTypeRelId := api.pkg_relationshipupdate.New(t_LicenseTypeEndPoint, t_LicenseObjectId, t_LicenseTypeId);
  
  --Relate The County and Muni
  
  t_MuniciplalityRelId := api.pkg_relationshipupdate.New(t_MuniciplalityEndPoint, t_LicenseObjectId, t_MuniciplalityId);
  
  t_CountyRelId := api.pkg_relationshipupdate.New(t_CountyEndPoint, t_LicenseObjectId, t_CountyId);
  
  --License Type Specific Details Uncomment to Set Specifics
  
  --api.pkg_columnupdate.SetValue(t_LicenseObjectId, 'LimitedBreweryProduction', 'UP TO 50,000 BBLS');
  
  
  api.pkg_logicaltransactionupdate.Endtransaction();
  
  dbms_output.put_line('License Number: ' || t_LicenseNumber|| ' License ObjectId: ' || t_LicenseObjectId);
  
end;