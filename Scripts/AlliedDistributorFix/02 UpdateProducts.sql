PL/SQL Developer Test script 3.0
364
--Update Products that have 3402-23-272-% or 3402-23-159-% that need 3402-23-272-003 DISTRIBUTOR
-- 18,205 to be updated selected in 334.562 seconds (5.5 minutes)
-- unit testing 55 rows took 15.523 seconds
-- full run took 12 minutes in cxnjtest
-- ToBeMadeHistoricLicenseID should be made historic if they are not the -003 active one.
declare
--1 was run as first test
  t_runid                  number := 3;
  t_results                api.Udt_Objectlist;
  t_secondresults          api.Udt_Objectlist;
  
  t_License272             api.pkg_definition.udt_id;
  t_License159             api.pkg_definition.udt_id;
  t_rel272id               api.pkg_definition.udt_id;
  t_newrel272id            api.pkg_definition.udt_id;
  t_rel159id               api.pkg_definition.udt_id;
  t_newrel159id            api.pkg_definition.udt_id;
  t_NewHistRelId           api.pkg_definition.udt_id;
  t_NewLogicalTransId      api.pkg_definition.udt_id;
  
  t_DistRelDef             number := api.pkg_configquery.ObjectDefIdForName('r_ABCProduct_Distributor');
  t_EP_DistributorLic      api.pkg_definition.udt_id := api.pkg_configquery.endpointidforname('o_ABC_Product', 'DistributorLic');
  t_EP_DistributorProduct  api.pkg_definition.udt_id := api.pkg_configquery.endpointidforname('o_ABC_License', 'DistributorProduct');
  
  t_HistRelDef             number := api.pkg_configquery.ObjectDefIdForName('r_ABC_Product_Historic_Dist');
  t_EP_HistoricProducts    api.pkg_definition.udt_id := api.pkg_configquery.endpointidforname('o_ABC_Product', 'HistoricProducts');
  t_EP_HistoricDist        api.pkg_definition.udt_id := api.pkg_configquery.endpointidforname('o_ABC_License', 'HistoricDist');
  
  t_rowcount               number := 0;
  t_errorcount             number := 0;
  --LogRow
  procedure LogRow(a_productid number, 
                   a_originallicenseid number,
                   a_NewHistrelid number,
                   a_oldrel272id number,
                   a_newrel272id number,
                   a_oldrel159id number,
                   a_newrel159id number,
                   a_ErrorMessage varchar2) is
    pragma autonomous_transaction;
  begin

    insert into conversion.AlliedProductLog_t
    values
      (t_runid,
       a_productid,
       a_originallicenseid,
       a_NewHistrelid,
       a_oldrel272id,
       a_newrel272id,
       a_oldrel159id,
       a_newrel159id,
       sysdate,
       a_ErrorMessage,
       null,
       null);

    commit;
  end;
  
begin
  select objectid
  into t_License272
  from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', '3402-23-272-003'))
  where api.pkg_columnquery.value(objectid, 'State') = 'Active';
  
  select objectid
  into t_License159
  from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', '3402-23-159-003'))
  where api.pkg_columnquery.value(objectid, 'State') = 'Active';
  
  /*select posseaudit.Logicaltransactionid_s.nextval
    into t_NewLogicalTransId
    from dual;*/

  t_results := api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', '3402-23-272%');
  t_secondresults := api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', '3402-23-159%');
  
  extension.pkg_collectionutils.Append(t_results, t_secondresults);
  
  for c in (select p.objectid, 
                   --p.name, p.type, p.Registrant, p.expirationdate, p.state,  --Testing output
                   pdr.relationshipid, 
                   pdr.licenseid ToBeMadeHistoricLicenseId
    from query.r_ABCProduct_Distributor pdr
    join query.o_ABC_Product p on p.objectid = pdr.productid
   where pdr.licenseid in (select * from table(t_results))
  and p.state = 'Current'
  and (not exists (select 1 from query.r_ABCProduct_Distributor prl where prl.licenseid = t_License272 and prl.productid = p.objectid) --3402-23-272-003
  or not exists (select 1 from query.r_ABCProduct_Distributor prl where prl.licenseid = t_License159 and prl.productid = p.objectid) --3402-23-159-003
    )
    ) loop
    --
    begin
      --dbms_output.put_line('Update Products: ' || c.objectid || ' ToBeHistoric: ' || c.ToBeMadeHistoricLicenseId );
      --raise_application_error(-20101, 'Bens Test Error');
      --Delete the ToBeMadeHistoricLicenseId relationship
      t_NewHistRelId := null;
      if c.ToBeMadeHistoricLicenseId != t_License272
      and  c.ToBeMadeHistoricLicenseId != t_License159 then
        --rel to license endpoint
        Delete from rel.storedrelationships_t r 
         where r.ToObjectId = c.ToBeMadeHistoricLicenseId --License
           and r.fromobjectid = c.objectid --Product
           and r.endpointid = t_EP_DistributorLic;
        
        --rel to product endpoint
        Delete from rel.storedrelationships_t r 
         where r.ToObjectId =  c.objectid --Product
           and r.fromobjectid = c.ToBeMadeHistoricLicenseId --License
           and r.endpointid = t_EP_DistributorProduct;
        
        --Delete the object
        Delete from possedata.objects_t o 
        where o.objectid = c.relationshipid; --relationshipid
      
        
      --Create the ToBeMadeHistoricLicenseId historic relationship
        --GetNewRelId
        select possedata.ObjectId_s.nextval
        into t_NewHistRelId
        from dual;
        
        --Insert into ObjModelPhys.Objects
        insert into Possedata.Objects_t (
          LogicalTransactionId,
          CreatedLogicalTransactionId,
          ObjectId,
          ObjectDefId,
          ObjectDefTypeId,
          ClassId,
          InstanceId,
          EffectiveStartDate,
          EffectiveEndDate,
          ExternalFileNum,
          ConfigReadSecurityClassId,
          ConfigReadSecurityInstanceId,
          ObjectReadSecurityClassId,
          ObjectReadSecurityInstanceId
        ) values (
          1, -- Hard coded logical Transaction to 1
          1, -- Hard coded logical Transaction to 1
          t_NewHistRelId,
          t_HistRelDef,
          4,
          4,
          t_HistRelDef,
          null,
          null,
          null,
          4,
          t_HistRelDef,
          null,
          null
        );
        
        --to license ep 1
        insert into rel.StoredRelationships_t (
          RelationshipId,
          EndPointId,
          FromObjectId,
          ToObjectId
        ) values (
          t_NewHistRelId,
          t_EP_HistoricProducts,
          c.objectid, --Product
          c.ToBeMadeHistoricLicenseId --License
        );
        
        --to product ep 2
        insert into rel.StoredRelationships_t (
          RelationshipId,
          EndPointId,
          FromObjectId,
          ToObjectId
        ) values (
          t_NewHistRelId,
          t_EP_HistoricDist,
          c.ToBeMadeHistoricLicenseId, --License
          c.objectid --Product
        );
      end if;

      
      --Check to see if 272-003 is related, if not relate it.
      t_newrel272id := null;
      begin
        select relationshipid
        into t_rel272id
        from query.r_ABCProduct_Distributor r
        where r.ProductId = c.objectid --product
        and r.LicenseId = t_License272;
      exception when no_data_found then
        --Insert into Relationships
        --GetNewRelId
        select possedata.ObjectId_s.nextval
        into t_newrel272id
        from dual;
        
        --Insert into ObjModelPhys.Objects
        insert into Possedata.Objects_t (
          LogicalTransactionId,
          CreatedLogicalTransactionId,
          ObjectId,
          ObjectDefId,
          ObjectDefTypeId,
          ClassId,
          InstanceId,
          EffectiveStartDate,
          EffectiveEndDate,
          ExternalFileNum,
          ConfigReadSecurityClassId,
          ConfigReadSecurityInstanceId,
          ObjectReadSecurityClassId,
          ObjectReadSecurityInstanceId
        ) values (
          1, -- Hard coded logical Transaction to 1
          1, -- Hard coded logical Transaction to 1
          t_newrel272id,
          t_DistRelDef,
          4,
          4,
          t_DistRelDef,
          null,
          null,
          null,
          4,
          t_DistRelDef,
          null,
          null
        );
        
        --to license ep 1
        insert into rel.StoredRelationships_t (
          RelationshipId,
          EndPointId,
          FromObjectId,
          ToObjectId
        ) values (
          t_newrel272id,
          t_EP_DistributorLic,
          c.objectid, --Product
          t_License272 --License
        );
        
        --to product ep 2
        insert into rel.StoredRelationships_t (
          RelationshipId,
          EndPointId,
          FromObjectId,
          ToObjectId
        ) values (
          t_newrel272id,
          t_EP_DistributorProduct,
          t_License272, --License
          c.objectid --Product
        );
      end;
        
      --Check to see if 159-003 is related, if not relate it.
      t_newrel159id := null;
      begin
        select relationshipid
        into t_rel159id
        from query.r_ABCProduct_Distributor r
        where r.ProductId = c.objectid --product
        and r.LicenseId = t_License159;
      exception when no_data_found then
        --Insert into Relationships
        --GetNewRelId
        select possedata.ObjectId_s.nextval
        into t_newrel159id
        from dual;
        
        --Insert into ObjModelPhys.Objects
        insert into Possedata.Objects_t (
          LogicalTransactionId,
          CreatedLogicalTransactionId,
          ObjectId,
          ObjectDefId,
          ObjectDefTypeId,
          ClassId,
          InstanceId,
          EffectiveStartDate,
          EffectiveEndDate,
          ExternalFileNum,
          ConfigReadSecurityClassId,
          ConfigReadSecurityInstanceId,
          ObjectReadSecurityClassId,
          ObjectReadSecurityInstanceId
        ) values (
          1, -- Hard coded logical Transaction to 1
          1, -- Hard coded logical Transaction to 1
          t_newrel159id,
          t_DistRelDef,
          4,
          4,
          t_DistRelDef,
          null,
          null,
          null,
          4,
          t_DistRelDef,
          null,
          null
        );
        
        --to license ep 1
        insert into rel.StoredRelationships_t (
          RelationshipId,
          EndPointId,
          FromObjectId,
          ToObjectId
        ) values (
          t_newrel159id,
          t_EP_DistributorLic,
          c.objectid, --Product
          t_License159 --License
        );
        
        --to product ep 2
        insert into rel.StoredRelationships_t (
          RelationshipId,
          EndPointId,
          FromObjectId,
          ToObjectId
        ) values (
          t_newrel159id,
          t_EP_DistributorProduct,
          t_License159, --License
          c.objectid --Product
        );
      end;
      --Commit every row
      commit;
    
      t_rowcount := t_rowcount + 1;
      LogRow(
          c.objectid, 
          c.ToBeMadeHistoricLicenseId,
          t_NewHistRelId,
          t_rel272id,
          t_newrel272id,
          t_rel159id,
          t_newrel159id,
          Null);
  exception when others then
    --Log Error Message
    LogRow(
          c.objectid, 
          c.ToBeMadeHistoricLicenseId,
          t_NewHistRelId,
          t_rel272id,
          t_newrel272id,
          t_rel159id,
          t_newrel159id,
          SQLERRM);
    t_errorcount := t_errorcount + 1;
  end;
  end loop;
  
  dbms_output.put_line('Products Processed: '||t_rowcount || ' Error Rows: ' || t_errorcount);
end;

0
0
