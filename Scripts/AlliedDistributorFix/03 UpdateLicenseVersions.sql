PL/SQL Developer Test script 3.0
71
-- Update Registrant License Number to current License version
--Updates 239 Records in 75 seconds
declare
  t_NoteText      varchar2(4000) := 'License Number adjusted due to Allied Beverage special ruling.'; --TO BE UPDATED BEFORE PROD RUN
  t_results api.Udt_Objectlist;
  t_secondresults api.Udt_Objectlist;
  
  t_License272    api.pkg_definition.udt_id;
  t_License159    api.pkg_definition.udt_id; 
  t_NewRelid      api.pkg_definition.udt_id;
  t_NewNoteId     api.pkg_definition.udt_id;
  
  t_EP_Registrant api.pkg_definition.udt_id := api.pkg_configquery.endpointidforname('o_ABC_Product', 'RegistrantLic');
  t_NoteDefid     api.pkg_definition.udt_id;
  
  t_rowcount      number := 0;
begin
  select objectid
  into t_License272
  from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', '3402-23-272-003'))
  where api.pkg_columnquery.value(objectid, 'State') = 'Active';
  
  select objectid
  into t_License159
  from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', '3402-23-159-003'))
  where api.pkg_columnquery.value(objectid, 'State') = 'Active';
  
  /*select nd.NoteDefId
  into t_NoteDefId
  from api.notedefs nd
  where nd.Tag = 'GEN';*/

  t_results := api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', '3402-23-272%');
  t_secondresults := api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', '3402-23-159%');
  
  extension.pkg_collectionutils.Append(t_results, t_secondresults);
  
  dbms_output.put_line('License272: ' || t_License272);
  dbms_output.put_line('License159: ' || t_License159);
  
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  for c in (select p.objectid, 
                   --p.name, p.type, p.Registrant, p.expirationdate, p.state,  --Testing output
                   pdr.relationshipid, 
                   pdr.licenseid ToBeMadeHistoricLicenseId
    from query.r_ABCProduct_RegistrantLicense pdr
    join query.o_ABC_Product p on p.objectid = pdr.productid
   where pdr.licenseid in (select * from table(t_results))
     and pdr.licenseid != t_License272
     and pdr.licenseid != t_License159
     and p.state = 'Current'
  ) loop
    --Update the relationship
    api.pkg_relationshipupdate.Remove(c.relationshipid);
    
    t_NewRelid := api.pkg_relationshipupdate.new(t_EP_Registrant, c.objectid, t_License272);
    
    --t_NewNoteId := api.pkg_noteupdate.New(c.objectid, t_NoteDefid, 'Y', t_NoteText); --Only Do if ABC ASKS

    dbms_output.put_line('Product: '|| c.objectid || ' RemovedLicense: ' || c.ToBeMadeHistoricLicenseId);
    
    t_rowcount := t_rowcount + 1;
  end loop;
  
  api.pkg_logicaltransactionupdate.EndTransaction();
  
  dbms_output.put_line('Products Processed: '||t_rowcount );
  
  commit;
end; 
0
0
