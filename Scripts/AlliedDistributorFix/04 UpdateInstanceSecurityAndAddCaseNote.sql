PL/SQL Developer Test script 3.0
62
--UpdateInstanceSecurity
--13.167 seconds for 55 rows
--14659 rows in 1657.252 seconds (27.5 minutes) in cxnjtest
declare
  t_runid         number := 3; --if multiple test runs.

  t_NoteText      varchar2(4000) := 'Distributors adjusted due to Allied Beverage special ruling.'; --TO BE UPDATED BEFORE PROD RUN
  
  t_NewNoteId     api.pkg_definition.udt_id;
  t_NoteDefid     api.pkg_definition.udt_id;
  
  t_rowcount      number := 0;
  
  --LogRow
  procedure LogRow(a_productid number) is
    pragma autonomous_transaction;
  begin

    update conversion.AlliedProductLog_t t
    set 
      noteadded = 'Y',
      notedate = sysdate    
    where t.productid = a_productid;

    commit;
  end;
  
begin
  select nd.NoteDefId
  into t_NoteDefId
  from api.notedefs nd
  where nd.Tag = 'GEN';  

  api.pkg_logicaltransactionupdate.ResetTransaction();  
  
  for c in (
    select t.productid
    from Conversion.AlliedProductLog_t t
    where t.runid = t_runid
      and t.errormessage is null
    group by t.productid
)
  loop
    --Update Instance Security for each Modified product from UpdateProducts.sql
    abc.pkg_instancesecurity.ApplyInstanceSecurity(c.ProductId, sysdate);
  
    --Insert a Note Indicating it was updated.
    t_NewNoteId := api.pkg_noteupdate.New(c.productid, t_NoteDefid, 'Y', t_NoteText);
    
    --dbms_output.put_line('productid: ' || c.productid); --commented out for full run
    
    t_rowcount := t_rowcount + 1;
    
    LogRow(c.productid);
    
  end loop;
  
  api.pkg_logicaltransactionupdate.endtransaction();
  commit;
  
  dbms_output.put_line('Products Processed: '||t_rowcount );
end;
0
0
