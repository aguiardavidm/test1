--Logging Table for Allied Product Brand Registration Distributor Updates
--RUN AS CONVERSION
drop table Conversion.AlliedProductLog_t;
Create table Conversion.AlliedProductLog_t
(
       runid number,
       productid number,
       originallicenseid number,
       newhistrelid number,
       oldrel272id number,
       newrel272id number,
       oldrel159id number,
       newrel159id number,
       processeddate date,
       ErrorMessage varchar2(4000)
);

Alter table Conversion.AlliedProductLog_t add (NoteAdded varchar2(200), NoteDate date);
Grant all on conversion.AlliedProductLog_t to possedba;
