# -*- coding: utf-8 -*-
import re
import os

from os import listdir
from os.path import isfile, join, isdir

def replace_crlf(match):
  match = match.group()
  return match[:-1].replace('\n','\x1b')+match[-1:]

def fix_csv(f):
  csv_file=open(f,'r').read()
  print ("Fixing: " +  f)
  p = re.compile('¦((?:[^¦]|\n)+)¦[\n|]', re.MULTILINE)
  csv_file = re.sub(p,replace_crlf,csv_file)
  filename, fileextension = os.path.splitext(f)
  open(filename+'.csv','w').write(csv_file)

def sort_files(currentDir):
  os.chdir(currentDir)
  for f in listdir(currentDir):
    if isfile(f):
      filename, fileextension = os.path.splitext(f)
      if fileextension == '.csv':
        fix_csv(f)
    if isdir(f):
      sort_files(os.getcwd()+ '\\'+ f)
      os.chdir(startDir)

startDir = os.getcwd()    

sort_files(startDir)

print('Finished fixed')
