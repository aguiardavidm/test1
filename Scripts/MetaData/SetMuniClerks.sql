-- Created on 5/6/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
begin
    dbms_output.put_line('declare
t_ClerkEPID number := api.pkg_configquery.endpointidforname(''o_ABC_Office'', ''Clerk'');
t_ClerkId   number;
t_RelId     number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;');
  -- Test statements here
  for i in (select '''' || listagg(o.dup_MunicipalityCountyCode, ''', ''') within group (order by o.dup_MunicipalityCountyCode) || '''' Codes, u.AuthenticationName
              from query.o_abc_office o
              join query.u_users u on u.FormattedName = o.clerkname
               and u.UserType = 'Internal'
             group by u.AuthenticationName) loop
dbms_output.put_line('
  select u.objectid 
    into t_ClerkId
    from query.u_users u 
   where u.authenticationname = ''' || i.AuthenticationName || ''';
   
  for i in (select o.objectid from query.o_abc_Office o where o.dup_MunicipalityCountyCode in (' || i.codes || ')) loop
    for c in (select r.relationshipid
                from query.r_ClerkMunicipality r
               where r.MunicipalityObjectId = i.Objectid) loop
      api.pkg_relationshipupdate.remove(c.relationshipid);
    end loop;
    t_RelId := api.pkg_relationshipupdate.new(t_ClerkEPId, i.ObjectId, t_ClerkId);
  end loop;');
  end loop;
  dbms_output.put_line('
  api.pkg_logicaltransactionupdate.EndTransaction;
end;');
end;