-- Created on 5/6/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
begin
    dbms_output.put_line('declare
t_SuperEPID number := api.pkg_configquery.endpointidforname(''o_ABc_LicenseType'', ''DefaultLicensingSupervisor'');
t_SuperId   number;
t_RelId     number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;');
  -- Test statements here
  for i in (select '''' || listagg(o.Code, ''', ''') within group (order by o.code) || '''' Codes, u.AuthenticationName
              from query.o_ABC_LicenseType o
              join query.u_users u on u.FormattedName = o.DefaultLicensingSupervisor
               and u.UserType = 'Internal'
             group by u.AuthenticationName) loop
dbms_output.put_line('
  select u.objectid 
    into t_SuperId
    from query.u_users u 
   where u.authenticationname = ''' || i.AuthenticationName || ''';
   
  for i in (select o.objectid from query.o_abc_LicenseType o where o.code in (' || i.codes || ')) loop
    for c in (select r.relationshipid
                from query.r_SupervisorLicenseType r
               where r.LicenceTypeId = i.Objectid) loop
      api.pkg_relationshipupdate.remove(c.relationshipid);
    end loop;
    t_RelId := api.pkg_relationshipupdate.new(t_SuperEPId, i.ObjectId, t_SuperId);
  end loop;');
  end loop;
  dbms_output.put_line('
  api.pkg_logicaltransactionupdate.EndTransaction;
end;');
end;