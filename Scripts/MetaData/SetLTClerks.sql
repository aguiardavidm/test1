declare
  t_UserId number;
  t_EPID   number;
  t_RelId  number;
begin
  api.pkg_logicaltransactionupdate.ResetTransaction;
  select U.ObjectId
   into t_UserId
   from query.u_users u
  where u.AuthenticationName = 'RBONNEY';

  select e.EndPointId
    into t_EPID
    from api.endpoints e
   where e.FromObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_ABC_LicenseType')
     and e.toObjectDefId = api.pkg_configquery.ObjectDefIdForName('u_Users')
     and e.label = 'Default Clerk:';

  for c in (select relationshipid from query.r_ABC_LicenseTypeDefaultClerk) loop
    api.pkg_relationshipupdate.Remove(c.relationshipid);
  end loop;
  for i in (select objectid from query.o_abc_licensetype lt where lt.IssuingAuthority = 'State') loop
    t_RelId := api.pkg_relationshipupdate.New(t_EPID, i.objectid, t_UserId);
  end loop;
  api.pkg_logicaltransactionupdate.EndTransaction;
end;