declare
  t_SysId number;
begin
  select objectid
    into t_SysId
    from query.o_systemsettings
   where rownum < 2;
  api.pkg_logicaltransactionupdate.ResetTransaction;
  api.pkg_columnupdate.SetValue(t_SysId, 'WarningCreationDate1218', to_date('07/31/2016', 'mm/dd/yyyy'));
  api.pkg_columnupdate.SetValue(t_SysId, 'WarningCreationDate1239', to_date('07/1/2016', 'mm/dd/yyyy'));
  api.pkg_logicaltransactionupdate.EndTransaction;
end;