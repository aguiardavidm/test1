declare
 
  t_ObjectDefId  pls_integer; 
  t_ObjectId     pls_integer;

begin
  -- logical transaction start
  api.pkg_logicaltransactionupdate.ResetTransaction();

  select pm.objectid
      into t_ObjectId
      from query.o_paymentmethod pm
     where pm.name = 'Cheque'
  
  api.pkg_columnupdate.setvalue(t_objectId, 'Name', 'Check');
  
    select pm.objectid
      into t_ObjectId
      from query.o_paymentmethod pm
     where pm.name = 'Cash'
  
  api.pkg_columnupdate.SetValue(t_objectId, 'ActiveFlag', 'N');
 
  -- logical transaction end;
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
end;