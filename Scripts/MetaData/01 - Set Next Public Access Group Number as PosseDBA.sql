/*  Update System Settings*/
declare 
  t_systemObjectId                        number;
  procedure dbug (
    t_Text    varchar2
  ) is
  begin
    dbms_output.put_line(t_Text);
  end;
begin
  if user != 'POSSEDBA' then
    raise_application_error(-20000, 'Script must be run as POSSEDBA.');
  end if;

  rollback;
  api.pkg_logicaltransactionupdate.resettransaction();
  
  select max(objectId)
    into t_systemObjectId         
    from query.o_SystemSettings;
      
  api.pkg_ColumnUpdate.setValue(t_systemObjectId, 'NextPublicAccessGroupNumber', 1);
  
  api.pkg_logicaltransactionupdate.endtransaction;
  commit;
end;
/