--INSTRUCTIONS: INCREASE OUTPUT BUFFER SIZE.
    --Run this in NJDELIVERY, then copy the dbms_output, paste the output into a pl/sql developer window in the TARGET database, and run.  Check dbms_output for errors.
-- Created on 4/28/2015 by ADMINISTRATOR 
declare 
  -- Local variables here
  i integer;
  t_SqlText  long;
  t_MuniCode varchar2(10);
begin
  -- Test statements here
--  t_SQLText := 
dbms_output.put_line(
'declare 
  t_UserId number;
  t_MuniId number;
  t_EPId   number;
  t_Password varchar2(40) := '''';
  t_EncryptPW varchar2(400);
  t_RelId  number;
  t_AGId   number;
begin
  if t_Password is null then
    api.pkg_errors.raiseerror(-20000, ''Please enter a value for t_Password'');
  end if;
  t_EncryptPW := abc.pkg_abc_users.encrypt(t_Password);
  t_EPId := api.pkg_configquery.EndPointIdForName(''u_Users'', ''Municipality'');
  select ag.AccessGroupId
    into t_AGId
    from api.accessgroups ag
   where ag.Description = ''Municipal Users'';

  ');
  for i in (select u.Active, u.Name, u.ShortName, u.FirstName, u.LastName, u.MunicipalityName, u.PhoneNumber, u.EmailAddress, u.AuthenticationName, m.MunicipalityObjectId
              from query.u_users u
              join query.r_MunicipalUserMunicipality m on m.MunicipalUserObjectId = u.ObjectId
             where u.UserType = 'Municipal'
               and u.Name not in ('Cassandra Licata-Link (municipal user)',
                                  'Coleene Robinson (municipal user)',
                                  'Joann Frascella (municipal user)',
                                  'Kelly Troilo (municipal user)',
                                  'Lori Rosati (municipal user)',
                                  'Patti Bowen (municipal user)',
                                  'Patti Valsac (municipal user)',
                                  'Rosemary Bonney (municipal user)',
                                  'Sheila Inverso (municipal user)',
                                  'Tia johnson(muni)',
                                  'Test Municipal User 01',
                                  'Test Municipal User 02',
                                  'Jeremy Harder')) loop
  t_MuniCode := api.pkg_columnquery.Value(i.MunicipalityObjectId, 'dup_MunicipalityCountyCode'); 
--    t_SQLText := t_SQLText ||
dbms_output.put_line(
'
  begin
    api.pkg_logicaltransactionupdate.ResetTransaction;
    t_UserId := api.pkg_userupdate.New(''MunicipalityInternetUsers'', ''' || replace(i.Name, '''', '''''') || ''', ''' || i.ShortName || '1'', ''' || i.AuthenticationName || '1'', t_EncryptPW);
    api.pkg_columnupdate.SetValue(t_UserId, ''Active'', ''' || i.Active || ''');
    api.pkg_columnupdate.SetValue(t_UserId, ''FirstName'', ''' || replace(i.FirstName, '''', '''''') || '1'');
    api.pkg_columnupdate.SetValue(t_UserId, ''LastName'', ''' || replace(i.LastName, '''', '''''') || ''');
    api.pkg_columnupdate.SetValue(t_UserId, ''PhoneNumber'', ''' || i.PhoneNumber || ''');
    api.pkg_columnupdate.SetValue(t_UserId, ''EmailAddress'', ''' || i.EmailAddress || ''');
    api.pkg_columnupdate.SetValue(t_UserId, ''UserType'', ''Municipal'');
    api.pkg_ColumnUpdate.SetValue(t_UserId, ''PasswordWeb'', t_EncryptPW);
    api.pkg_columnUpdate.setValue(t_UserId, ''OSUserId'', ''JHARDERMUNI3'');

    t_MuniId := api.pkg_simplesearch.ObjectByIndex(''o_abc_Office'', ''dup_MunicipalityCountyCode'', ''' || t_MuniCode || ''');
    
    t_RelId := api.pkg_relationshipupdate.New(t_EPId, t_UserId, t_MuniId);
    api.pkg_userupdate.AddToAccessGroup(t_UserId, t_AGId);
    api.pkg_logicaltransactionupdate.EndTransaction;
    commit;
  exception when others then
    dbms_output.put_line(sqlerrm || '': For User: '' || ''' || i.authenticationname || ''');
    rollback;
  end;
    ');
  end loop;
--  t_SQLText := t_SQLText || 
  dbms_output.put_line(
  '
  api.pkg_logicaltransactionupdate.EndTransaction;
  end;');
  dbms_output.put_line(t_SqlText);
end;
