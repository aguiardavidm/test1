/* Author: Caleb Thiessen
   Description: Create new Fee Element Expression on the Data Source and change the Fee Description on the fee schedule to use the new expression.
   Execution time: ~3 seconds
*/
declare
  subtype udt_Id is api.pkg_definition.udt_Id;
-- IDs  
  t_DSObjId      udt_Id;
  t_FSObjId      udt_Id := api.pkg_search.ObjectByColumnValue('o_FeeSchedule', 'Description', 'ABC Fee Schedule');
  t_FEObjId      udt_Id;
  t_FeeElementId udt_id;
  t_EDEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'DataSource');  
  t_EFEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_FeeElement', 'FeeSchedule');
  t_FDObjId      udt_id;


-- PROCEDURES --
procedure dbug (
    a_Text    varchar2
  ) is
  begin
    dbms_output.put_line(a_Text);
end;

--Taken from other code, made a small modification to allow the creation of an expression fee element.
procedure CreateFE(a_Datatype                varchar2,
                   a_Name                    varchar2,
                   a_Syntax                  varchar2,
                   a_Job                     varchar2,
                   a_DSObjId                 udt_id,
                   a_FSObjId                 udt_id) is
  t_RelationshipId udt_Id;
  
begin
  --Find FeeElement
  begin
    select fe.objectid
    into t_FEObjId
    from query.o_feeelement fe
    join query.r_DataSource_FeeElement dsfe on dsfe.FeeElementId = fe.ObjectId
    join query.o_datasource ds on ds.ObjectId = dsfe.DataSourceId
    join query.r_FeeSchedule_FeeElement fsfe on fsfe.FeeElementObjectId = fe.objectid
    join query.o_Feeschedule fs on fs.objectid = fsfe.FeeScheduleObjectId
   where fe.GlobalName    = a_Name
     and ds.PosseViewName = a_Job
     and fs.ObjectId      = a_FSObjId;
  exception
     when no_data_found then
       t_FEObjId := null;
  end;
  
  if t_FEObjId is null then
  --Create new object of FeeElement
     feescheduleplus.pkg_PosseExternalRegistration.RegisterFeeElement (
                                                                       t_FeeElementId,
                                                                       a_DataType,
                                                                       'Expression',
                                                                       sysdate,
                                                                       'POSSEDBA',
                                                                       sysdate,
                                                                       'POSSEDBA',
                                                                       null,
                                                                       a_Name,
                                                                       api.pkg_columnquery.NumericValue(a_FSObjId,'FeeScheduleId'),
                                                                       api.pkg_columnquery.NumericValue(a_DSObjId,'DataSourceId')
                                                                       );
     t_FEObjId := api.pkg_objectupdate.RegisterExternalObject('o_FeeElement',t_FeeElementId);
     api.pkg_columnupdate.SetValue(t_feobjid, 'Syntax', a_Syntax);
     dbug ('Added FeeElement ' || t_FEObjId || ' - ' || a_Name || ' for ' || a_Job);
  --Create relationship from FeeElement to FeeSchedule
     t_RelationshipId := api.pkg_relationshipupdate.New(t_EFEndpointId, t_FEObjId, a_FSObjId);
  --Create relationship from FeeElement to DataSource
     t_RelationshipId := api.pkg_relationshipupdate.New(t_EDEndpointId, t_FEObjId, a_DSObjId);
  end if;
end;

begin
    --Get Data Source Objectid
    select ds.objectid 
     into t_DSObjId
     from query.r_DataSource_FeeSchedule dsfs 
     join query.o_datasource ds on ds.ObjectId = dsfs.DataSourceId
    where dsfs.FeeScheduleId = t_FSObjId
      and ds.PosseViewName = 'j_ABC_MiscellaneousRevenue';
    --Get Fee Definition Objectid
    select fd.objectid
      into t_FDObjId
      from query.o_FeeDefinition fd
      join query.r_Feedef_Feeschedule fdfs on fdfs.FeeDefinitionObjectId = fd.objectid
     where fd.Description  = 'Miscellaneous Revenue Transaction Fee'
       and fdfs.FeeScheduleId = t_FSObjId;
     
     api.pkg_logicaltransactionupdate.ResetTransaction;
     
     
     CreateFE ('String',
               'FeeDescriptionShort',
               '=substr({FeeDescription},1,60)',
               'j_ABC_MiscellaneousRevenue',
               t_DSObjId,
               t_FSObjId);
     --Update Fee Description to use new Fee Element
     api.pkg_columnupdate.SetValue(t_FDObjId,'FeeDescription','{FeeDescriptionShort}');

     api.pkg_logicaltransactionupdate.EndTransaction;

  commit;
  dbug('End Creating Fee script');
end;
/
