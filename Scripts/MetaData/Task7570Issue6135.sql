/* Author: Yars Tsaytak
   Description: Issue 6135 (Task 7570 to remove Data Cleaner access group)
*/
declare 
  t_AccessGroupObjectId      number; -- Object Id for 'Data Cleaner'

-- MAIN PROCESS --
begin
  dbms_output.put_line('start script to set Data Access access group IsAdministratable value to N');
  api.pkg_logicaltransactionupdate.ResetTransaction();
  
  --get the object id for the data cleaner access group
  select ag.ObjectId
         into t_AccessGroupObjectId
  from query.o_accessgroup ag 
  where ag.Description = 'Data Cleaner';

  --update the isAdministratable column
  api.pkg_columnupdate.SetValue(t_AccessGroupObjectId, 'ISADMINISTRATABLE', 'N');  
   
  api.pkg_logicaltransactionupdate.EndTransaction();
  commit;
  dbms_output.put_line('finished script to set Data Access access group IsAdministratable value to N');
  
end;
