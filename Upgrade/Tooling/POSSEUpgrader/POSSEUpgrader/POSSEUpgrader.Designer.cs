﻿namespace POSSEUpgrader
{
    partial class POSSEUpgrader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.UpgradeSteps = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttons_GroupBox = new System.Windows.Forms.GroupBox();
            this.run_button1 = new System.Windows.Forms.Button();
            this.previous_button1 = new System.Windows.Forms.Button();
            this.NextBtn = new System.Windows.Forms.Button();
            this.upgradeResultsBox = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.buttons_GroupBox.SuspendLayout();
            this.upgradeResultsBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.87097F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.12904F));
            this.tableLayoutPanel1.Controls.Add(this.UpgradeSteps, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 13);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(775, 425);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // UpgradeSteps
            // 
            this.UpgradeSteps.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UpgradeSteps.Enabled = false;
            this.UpgradeSteps.FormattingEnabled = true;
            this.UpgradeSteps.Location = new System.Drawing.Point(3, 3);
            this.UpgradeSteps.Name = "UpgradeSteps";
            this.UpgradeSteps.Size = new System.Drawing.Size(240, 407);
            this.UpgradeSteps.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.buttons_GroupBox, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.upgradeResultsBox, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(249, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.30549F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.69451F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(523, 419);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // buttons_GroupBox
            // 
            this.buttons_GroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttons_GroupBox.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttons_GroupBox.Controls.Add(this.run_button1);
            this.buttons_GroupBox.Controls.Add(this.previous_button1);
            this.buttons_GroupBox.Controls.Add(this.NextBtn);
            this.buttons_GroupBox.Location = new System.Drawing.Point(3, 372);
            this.buttons_GroupBox.Name = "buttons_GroupBox";
            this.buttons_GroupBox.Size = new System.Drawing.Size(517, 44);
            this.buttons_GroupBox.TabIndex = 0;
            this.buttons_GroupBox.TabStop = false;
            // 
            // run_button1
            // 
            this.run_button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.run_button1.Location = new System.Drawing.Point(355, 12);
            this.run_button1.Name = "run_button1";
            this.run_button1.Size = new System.Drawing.Size(75, 23);
            this.run_button1.TabIndex = 2;
            this.run_button1.Text = "Run";
            this.run_button1.UseVisualStyleBackColor = true;
            this.run_button1.Click += new System.EventHandler(this.run_button1_Click);
            // 
            // previous_button1
            // 
            this.previous_button1.Location = new System.Drawing.Point(6, 12);
            this.previous_button1.Name = "previous_button1";
            this.previous_button1.Size = new System.Drawing.Size(75, 23);
            this.previous_button1.TabIndex = 1;
            this.previous_button1.Text = "Back";
            this.previous_button1.UseVisualStyleBackColor = true;
            this.previous_button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NextBtn
            // 
            this.NextBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NextBtn.Location = new System.Drawing.Point(436, 12);
            this.NextBtn.Name = "NextBtn";
            this.NextBtn.Size = new System.Drawing.Size(75, 23);
            this.NextBtn.TabIndex = 0;
            this.NextBtn.Text = "Next";
            this.NextBtn.UseVisualStyleBackColor = true;
            this.NextBtn.Click += new System.EventHandler(this.NextBtn_Click);
            // 
            // upgradeResultsBox
            // 
            this.upgradeResultsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.upgradeResultsBox.Controls.Add(this.dataGridView1);
            this.upgradeResultsBox.Location = new System.Drawing.Point(3, 3);
            this.upgradeResultsBox.Name = "upgradeResultsBox";
            this.upgradeResultsBox.Size = new System.Drawing.Size(517, 363);
            this.upgradeResultsBox.TabIndex = 1;
            this.upgradeResultsBox.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 16);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(511, 344);
            this.dataGridView1.TabIndex = 0;
            // 
            // POSSEUpgrader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(800, 450);
            this.Name = "POSSEUpgrader";
            this.ShowIcon = false;
            this.Text = "POSSEUpgrader";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.buttons_GroupBox.ResumeLayout(false);
            this.upgradeResultsBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox UpgradeSteps;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox buttons_GroupBox;
        private System.Windows.Forms.Button NextBtn;
        private System.Windows.Forms.Button previous_button1;
        private System.Windows.Forms.Button run_button1;
        private System.Windows.Forms.GroupBox upgradeResultsBox;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

