﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace POSSEUpgrader
{
    public partial class POSSEUpgrader : Form
    {
        //AppData Config File for settings related to the specific running instance
        static string AppData = Application.LocalUserAppDataPath;
        static string AppDataFileName = "Config.dat";
        private string AppDataFile { get { return AppData + "\\" + AppDataFileName; } }

        //Config.json configuration for the actual upgrade
        static string ConfigJson = Application.StartupPath;
        static string ConfigJsonName = "config.json";
        private string ConfigJsonFile { get { return ConfigJson + "\\" + ConfigJsonName; } }

        public POSSEUpgrader()
        {
            //Event Handlers
            FormClosing += POSSEUpgrader_FormClosing;

            InitializeComponent();

            //Load Application Config
            loadConfig();

        }

        public void loadConfig()
        {
            //Load Json Config
            var stream = File.OpenText(ConfigJsonFile);
            string json = stream.ReadToEnd();
            var serializer = new JavaScriptSerializer();
            serializer.RegisterConverters(new[] { new DynamicJsonConverter() });
            dynamic configData = serializer.Deserialize(json, typeof(object));


            List<UpgradeListStep> UpgradeListSteps = new List<UpgradeListStep>();
            for (int i = 0; i < configData.UpgradeListSteps.Count; i++)
            {
                UpgradeListSteps.Add(new UpgradeListStep() {
                    Name = configData.UpgradeListSteps[i].Name,
                    Enable = Convert.ToBoolean(configData.UpgradeListSteps[i].Enable),
                    Order = (int)configData.UpgradeListSteps[i].Order,
                    Description = configData.UpgradeListSteps[i].Description,
                    Type = configData.UpgradeListSteps[i].Type,
                    ConsoleOutputFileName = configData.UpgradeListSteps[i].ConsoleOutputFileName,
                    InputResultsFileName = configData.UpgradeListSteps[i].InputResultsFileName,
                    ShowShell = Convert.ToBoolean(configData.UpgradeListSteps[i].ShowShell),
                    IsFirstLineHeader = Convert.ToBoolean(configData.UpgradeListSteps[i].IsFirstLineHeader),
                    Location = configData.UpgradeListSteps[i].Location
                });
            }

            //load
            foreach (UpgradeListStep step in UpgradeListSteps)
            {
                //UpgradeSteps.Items.Add(configData.UpgradeListSteps[i].Description);
                UpgradeSteps.Items.Add(step);
            }
            UpgradeSteps.BackColor = System.Drawing.SystemColors.Control;
            UpgradeSteps.ForeColor = System.Drawing.Color.White;

        }
        private void RunPython(UpgradeListStep step, string[] args)
        {
            //Setup File
            string pythonToRun = step.Location;
            //Setup Subprocess
            Process p = new Process();
            p.StartInfo.FileName = @"python.exe"; //this should be a setting to the generic python location. or see if we can import the python library and run it natively.
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            if (step.ShowShell != true)
                p.StartInfo.CreateNoWindow = true;

            //Setup Arguments
            string arguments = "";
            if (args != null)
            {
                arguments = String.Join(" ", args);
                pythonToRun = String.Concat(pythonToRun, " ", arguments);
            }

            //Run
            p.StartInfo.Arguments = pythonToRun;
            p.Start();

            StreamReader output = p.StandardOutput;
            string outputString = output.ReadToEnd();
            using (FileStream fStream = File.Create(ConfigJson + "\\" + step.ConsoleOutputFileName))
            {
                byte[] bytes = Encoding.ASCII.GetBytes(outputString);
                fStream.Write(bytes, 0, bytes.Length);
            }

            p.WaitForExit();
        }

        private void RunPython(UpgradeListStep step)
        {
            RunPython(step, null);
        }

        private void RunFile()
        {
            UpgradeListStep UpgradeStep = new UpgradeListStep();
            if (UpgradeSteps.SelectedItem != null)
            {
                int SelectedUpgradeStep = UpgradeSteps.SelectedIndex;
                UpgradeStep = (UpgradeListStep)UpgradeSteps.Items[SelectedUpgradeStep];
            }
            if (UpgradeStep.Type == "python")
            {
                RunPython(UpgradeStep);
                LoadOutput(UpgradeStep.InputResultsFileName, UpgradeStep.IsFirstLineHeader);
                //var msgResult = MessageBox.Show("The Type of 'python' is not implemented yet.", "Feature Not Implemented");
            }
            else if (UpgradeStep.Type == "batch")
            {
                var msgResult = MessageBox.Show("The Type of 'batch' is not implemented yet.", "Feature Not Implemented");
            }
            else
            {
                var msgResult = MessageBox.Show("Please check your config file and ensure a valid Type of python or batch is set.", "No Valid Run Type");
            }
        }


        private void NextStep()
        {
            object selected;
            if (UpgradeSteps.SelectedItem != null)
            {
                selected = UpgradeSteps.SelectedIndex;
                if ((int)selected != UpgradeSteps.Items.Count -1)
                {
                    UpgradeSteps.SelectedIndex = (int)selected + 1;
                }
                else
                {
                    UpgradeSteps.SelectedIndex = 0;
                }
                selected = UpgradeSteps.SelectedItem;
            }
            else
            {
                selected = UpgradeSteps.Items[0];
                UpgradeSteps.SelectedItem = selected;
            }
            //return selected;
        }

        private void NextBtn_Click(object sender, EventArgs e)
        {
            NextStep();
        }


        private void PreviousStep()
        {
            object selected;
            if (UpgradeSteps.SelectedItem != null)
            {
                selected = UpgradeSteps.SelectedIndex;
                if ((int)selected != 0)
                {
                    UpgradeSteps.SelectedIndex = (int)selected - 1;
                }
                else
                {
                    UpgradeSteps.SelectedIndex = UpgradeSteps.Items.Count - 1;
                }
                selected = UpgradeSteps.SelectedItem;
            }
            else
            {
                selected = UpgradeSteps.Items[UpgradeSteps.Items.Count - 1];
                UpgradeSteps.SelectedItem = selected;
            }
            //return selected;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PreviousStep();
        }

        /// <summary>
        /// Loads an output file that is formatted as a CVS.
        /// This must have a header or it will not load correctly.
        /// </summary>
        /// <param name="FileName">location in the current directory</param>
        private void LoadOutput(string FileName, bool isFirstLineHeader)
        {
            DataTable table = new DataTable();
            using (StreamReader sr = new StreamReader(FileName))
            {
                int lineNum = 0;
                string newline;
                while ((newline = sr.ReadLine()) != null)
                {
                    if (lineNum == 0)
                    {
                        //Add the header columns
                        string[] values = newline.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            table.Columns.Add(values[i]);
                        }
                    }
                    else
                    {
                        //populate data
                        DataRow row = table.NewRow();
                        string[] values = newline.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (values[i].ToLower().Contains("png"))
                            {
                                DataGridViewImageCell iconCell = new DataGridViewImageCell() { Value = Properties.Resources.Error };
                                row[i] = iconCell;
                            }
                            else
                            {
                                row[i] = values[i];
                            }
                        }
                        table.Rows.Add(row);
                    }
                    lineNum++;
                    
                    /*if (lineNum == 0)
                    {
                        //Add the header columns
                        string[] values = newline.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (values[i].ToLower().Contains("image"))
                            {
                                dataGridView1.Columns.Add(new DataGridViewImageColumn());
                            }
                            else
                            {
                                dataGridView1.Columns.Add(new DataGridViewColumn() { HeaderText = values[i] });
                            }
                            table.Columns.Add(values[i]);
                        }
                    }
                    else
                    {
                        //populate data
                        DataRow row = table.NewRow();
                        string[] values = newline.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (values[i].ToLower().Contains("error"))
                            {
                                DataGridViewImageCell iconCell = new DataGridViewImageCell() { Value = Properties.Resources.Error };
                                row[i] = iconCell;
                            }
                            else
                            {
                                row[i] = values[i];
                            }
                        }
                        table.Rows.Add(row);
                    }
                    lineNum++;
                    */
                }
            }
            dataGridView1.DataSource = table;
        }

        private void run_button1_Click(object sender, EventArgs e)
        {
            disableButtons();
            RunFile();
            enableButtons();
        }

        private void disableButtons()
        {
            previous_button1.Enabled = false;
            run_button1.Enabled = false;
            NextBtn.Enabled = false;
        }
        private void enableButtons()
        {
            previous_button1.Enabled = true;
            run_button1.Enabled = true;
            NextBtn.Enabled = true;
        }

        private void POSSEUpgrader_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (false) //debugging only
            {
                DialogResult results = MessageBox.Show("Are you sure you want to Exit?", "Exit Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (results == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
