﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSSEUpgrader
{
    class UpgradeListStep : Object
    {
        public string Name;
        public bool Enable;
        public int Order;
        public string Description;
        public string Type;
        public string ConsoleOutputFileName;
        public string InputResultsFileName;
        public string Location;
        public bool ShowShell;
        public bool IsFirstLineHeader;

        public override string ToString()
        {
            return Description;
        }
    }
}
