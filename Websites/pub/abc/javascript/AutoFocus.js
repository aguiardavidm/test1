function AppQuestionFocus() {
	// Applications with questions sometimes have radio buttons that toggle a textbox
	// when one of the radio buttons is toggled and a textbox becomes visible
	// the focus should automatically be set to the textbox.

	// To use this function, two css classes will need to be added to the Related Grid elements
	// 'af_QuestionAnswer' css class should be added to the radio button widget
	// 'af_QuestionResponse' css class should be added to the textarea widget
	var ActiveElement = PosseGetElementOptional(document.activeElement.id);
	if(typeof(ActiveElement) !== 'undefined' && ActiveElement !== null){
		var ActiveSpan = $("#"+ActiveElement.name+"_sp");

		// checks if the last active element has the required class
		if($(ActiveSpan).hasClass("af_QuestionAnswer")){
			var ActiveRow = $(ActiveSpan).closest("tbody");
			var FocusTarget = $(ActiveRow).find("textarea.af_QuestionResponse")
			
			if(typeof FocusTarget != "undefined" && FocusTarget != null && FocusTarget.length > 0){
				FocusTarget[0].focus();
			}		
		} // check for af_QuestionAnswer
	} // check for active element
} // End Function AppQuestionFocus()