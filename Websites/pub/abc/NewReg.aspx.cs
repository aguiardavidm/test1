//using ADODB;
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Computronix.POSSE.Outrider
{
    public partial class NewReg : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int objectId;
            string newObjectId;
            StringBuilder xml = new StringBuilder();
            List<OrderedDictionary> data;
            List<NewObject> objects;
            try
            {
				 
                xml.Append("<object id=\"NEW0\" objectdef=\"j_Registration\" action=\"Insert\">");
                xml.AppendFormat("<column name=\"SessionId\">{0}</column>", this.SessionId);
                xml.Append("</object>");

                this.ProcessXML(xml.ToString());

                objects = this.GetNewObjects();
                objectId = (int)objects[0].ObjectId;

                this.Response.Redirect(string.Format("{0}?PossePresentation=New&PosseObjectId={1}",
                    this.HomePageUrl, objectId));
            }
            catch (Exception exception)
            {
                this.ProcessError(exception);
                //this.RenderHelpBand(this.pnlHelpBand);
                this.RenderErrorMessage(this.pnlErrorBand);
                this.RenderTitle(this.pnlTitleBand, "New Registration");
                this.RenderFooterBand(this.pnlDebugLinkBand);
            }
        }
        #endregion
    }
}
