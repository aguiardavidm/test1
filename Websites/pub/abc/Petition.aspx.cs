﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Computronix.POSSE.Outrider
{
    public partial class Petition : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int objectId;
            string newObjectId, queryString, ObjectId;
            StringBuilder xml = new StringBuilder();
            StringBuilder xmlUpdate = new StringBuilder();
            List<OrderedDictionary> data;
            List<NewObject> objects;

            queryString = Request.Url.Query;
            ObjectId = Request.QueryString["LicenseObjectId"];

            try
            {
                xml.Append("<object id=\"NEW0\" objectdef=\"j_ABC_Petition\" action=\"Insert\">");
                xml.AppendFormat("<column name=\"LicenseObjectId\">{0}</column>", ObjectId);
                xml.Append("<column name=\"EnteredOnline\">Y</column>");
                xml.AppendFormat("<column name=\"LicenseObjectIdStored\">{0}</column>", ObjectId);
                xml.AppendFormat("<column name=\"OnlineUserObjectId\">{0}</column>", this.UserInformation[0]["objectid"].ToString());
                xml.Append("</object>");

                this.ProcessXML(xml.ToString());

                objects = this.GetNewObjects();
                objectId = (int)objects[0].ObjectId;

                this.Response.Redirect(string.Format("{0}{1}&PosseObjectId={2}",
                this.HomePageUrl, queryString, objectId));
            }
            catch (Exception exception)
            {
                this.ProcessError(exception);
                this.RenderUserInfo(this.pnlUserInfo);
                this.RenderMenuBand(this.pnlMenuBand);
                this.RenderErrorMessage(this.pnlErrorBand);
                this.RenderTitle(this.pnlTitleBand, "Error");
                this.RenderFooterBand(this.pnlDebugLinkBand);
            }
        }
        #endregion
    }
}