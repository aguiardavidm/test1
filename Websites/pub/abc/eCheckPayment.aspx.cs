using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Script.Serialization;
using System.Xml;
//using NJEPayWS;
using NJeCheckWS;
//using System.Reflection;

public partial class eCheckPayment : Computronix.POSSE.Outrider.PageBaseExt
{
    protected void Page_Load(object sender, EventArgs e)
    {
        const string USER_ERROR = "Fee Payment halted due to an unexpected system error.  The details have been logged.";

        string errorMessage = null;
        string xml;
        string eComTransactionId = null;

        this.RenderUserInfo(this.pnlUserInfo);
        this.RenderMenuBand(this.pnlMenuBand);
        try
        {
            //Get the database date
            DateTime dbDateTime = getDatabaseDate();

            string objectId = Request.QueryString["PosseObjectId"];
            string ePaymentMethodObjectId = Request.QueryString["ePaymentMethodObjectId"];
            string userObjectId = Request.QueryString["UserObjectId"];

            int[] staticIntArray = new int[1] { Convert.ToInt32(objectId) };
            string eComType = Request.QueryString["EComType"];
            eComTransactionId = CreateEComTransaction(eComType, staticIntArray);

            // Update ePaymentMethod Job to record the eComTransactionId
            xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                "<column name=\"eCOMJobId\"><![CDATA[{1}]]></column>" +
                                "</object>",
                                ePaymentMethodObjectId,
                                eComTransactionId);

            this.ProcessXML(xml.ToString());

            //DateTime today = DateTime.Today;
            DateTime endOfMonth = new DateTime(dbDateTime.Year,
                                               dbDateTime.Month,
                                               DateTime.DaysInMonth(dbDateTime.Year,
                                                                    dbDateTime.Month));
            int currentMonth = dbDateTime.Month;
            int currentYear = dbDateTime.Year;

            // Get the Tax Info
            var taxInfo = getTaxInfo(eComTransactionId, objectId);
            string legalName = getString(taxInfo["legalName"]);
            string taxNum = getString(taxInfo["taxNum"]);
            string taxCode = getString(taxInfo["taxCode"]);
            string odf = getString(taxInfo["odf"]);  // j_ABC_NewApplication-Y If the last Character is Y then this is for an individual
            //string payorType = (odf[odf.Length - 1] == 'Y') ? "I" : "B";

            if (legalName == null)
                throw new Exception("Missing Legal Information");

            // Get eCheckInfo
            var eCheckInfo = geteCheckInfo(eComTransactionId, ePaymentMethodObjectId, userObjectId);
            string BankRountingNumber = getString(eCheckInfo["bankRoutingNumber"]);
            string BankAccountNumber = getString(eCheckInfo["bankAccountNumber"]);
            string BankAccountTypeCode = getString(eCheckInfo["backAcctTypeCode"]);
            string DayTimePhone = getString(eCheckInfo["phoneNumber"]);
            string EmailAddress = getString(eCheckInfo["emailAddress"]);
            string ContactName = getString(eCheckInfo["contactName"]);

            // Get the list of fee objects.
            List<OrderedDictionary> fees = GetFees(eComTransactionId);

            decimal lineItemTotal = 0;
            int lineItemCount = 0;

            foreach (OrderedDictionary fee in fees)
            {
                // Ensure that the eCom job knows exactly which payments it is paying.
                xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                      "<relationship endpoint=\"FeeEndPoint\" toobjectid=\"{1}\" action=\"Insert\">" +
                                      "<column name=\"FeeAmountToPay\">{2}</column>" +
                                      "</relationship>" +
                                   "</object>",
                                   eComTransactionId, fee["ObjectId"].ToString(), Convert.ToSingle(fee["TotalOwing"]));
                lineItemTotal += Convert.ToDecimal(fee["TotalOwing"]);
                lineItemCount++;
                this.ProcessXML(xml.ToString());

            }

            decimal feeAmount = 0;
            try
            {
                feeAmount = GetEComTransactionFee(eComType, eComTransactionId);

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get fee for [{0}/{1}], ", eComType, eComTransactionId) + ex.Message);
            }
            if (feeAmount <= 0)
                throw new Exception(string.Format("Invalid attempt to pay a fee of {0}. Transaction:{1}", feeAmount, eComTransactionId));
            else if (feeAmount > lineItemTotal)
                throw new Exception(string.Format("Fee mismatch, the fee to be paid of {2:0.00}, exceeds the total of the {0} line item(s) of {1:0.00}. Transaction:{3}", lineItemCount, lineItemTotal, feeAmount, eComTransactionId));

            string confirmationobjectId = Request.QueryString["ConfirmationObjectId"];
            string toObjectId = objectId;
            if (!String.IsNullOrEmpty(confirmationobjectId))
                toObjectId = confirmationobjectId;

            //string recordPaymentBaseUrl = String.Format("{0}{1}", ConfigurationManager.AppSettings["RootUrl"], ConfigurationManager.AppSettings["RecordPaymentPageUrl"]);
            string recordPaymentBaseUrl = String.Format("{0}{1}", ConfigurationManager.AppSettings["RootUrl"], "Default.aspx");

            string successPaymentUrl = String.Format(
                "{0}?ReceiptForId={1}&PosseObjectId={2}&PossePresentation={3}&PossePane={4}&EComType={5}&SuccessFlag={6}",
                recordPaymentBaseUrl, eComTransactionId, toObjectId, Request.QueryString["SuccessPresentation"], Request.QueryString["SuccessPane"], eComType, Request.QueryString["SuccessFlag"]);
            string failurePaymentUrl = String.Format(
                "{0}?PosseObjectId={1}&PossePresentation={2}&PossePane={3}",
                recordPaymentBaseUrl, ePaymentMethodObjectId, Request.QueryString["FailurePresentation"], Request.QueryString["FailurePane"]);
            //string cancelPaymentUrl = failurePaymentUrl + "&Cancelled=true";

            //setup the call to the eCheck Service
            NJeCheckWS.NJENTeCHECKWSService eCheckService = new NJeCheckWS.NJENTeCHECKWSService();
            NJeCheckWS.eCheckInput eCheckInputRequest = new NJeCheckWS.eCheckInput();
            NJeCheckWS.eCheckOutput eCheckOutputResponseV = new NJeCheckWS.eCheckOutput();  //validated
            NJeCheckWS.eCheckOutput eCheckOutputResponseP = new NJeCheckWS.eCheckOutput();  //processed

            ////eCheckInputRequest.builtDirectory = "LPS_POSSE";
            ////eCheckInputRequest.applicationId = "ABC LICENSE";
            //eCheckInputRequest.paymentId = "I123456789012";
            //eCheckInputRequest.controlName = "CONT";
            //eCheckInputRequest.typeOfPay = "PMT";
            //eCheckInputRequest.amtOfPay = 110.00;
            //eCheckInputRequest.paymentYear = 2015;
            //eCheckInputRequest.paymentMonth = 7;
            //eCheckInputRequest.paymentType = "ECK";
            //eCheckInputRequest.paymentDueDate = "20150731";
            //eCheckInputRequest.settlementDate = "";
            //eCheckInputRequest.contactName = "Contact Name";
            //eCheckInputRequest.dayPhone = "(780) 222-2222";
            //eCheckInputRequest.dayPhoneExten = "";
            //eCheckInputRequest.contactEmail = "abc@computronix.com";
            //eCheckInputRequest.bankRoute = "031201360";
            //eCheckInputRequest.bankAcct = "12345";
            //eCheckInputRequest.bankAcctType = "C";
            //eCheckInputRequest.taxPayerName = "";
            //eCheckInputRequest.transSubCode = "";
            //eCheckInputRequest.taxCode = "06420"; //06420 for individuals, 06410 for businesses
            
            //eCheckInputRequest.paymentId = payorType + eComTransactionId.PadLeft(12,'0');
			eCheckInputRequest.paymentId = eComTransactionId.PadLeft(12,'0');
            eCheckInputRequest.controlName = getCustControl(ContactName);
            eCheckInputRequest.typeOfPay = "PMT";
            eCheckInputRequest.amtOfPay = decimal.ToDouble(feeAmount);
            eCheckInputRequest.paymentYear = currentYear;
            eCheckInputRequest.paymentMonth = currentMonth;
            eCheckInputRequest.paymentType = "ECK";
            eCheckInputRequest.paymentDueDate = endOfMonth.ToString("yyyyMMdd");
            eCheckInputRequest.settlementDate = "";
            eCheckInputRequest.contactName = ContactName;
            eCheckInputRequest.dayPhone = DayTimePhone;
            eCheckInputRequest.dayPhoneExten = "";
            eCheckInputRequest.contactEmail = EmailAddress;
            eCheckInputRequest.bankRoute = BankRountingNumber;
            eCheckInputRequest.bankAcct = BankAccountNumber;
            eCheckInputRequest.bankAcctType = BankAccountTypeCode;
            eCheckInputRequest.taxPayerName = legalName;
            eCheckInputRequest.transSubCode = "";
            eCheckInputRequest.taxCode = taxCode; //06420 for individuals, 06410 for businesses

            // Serialize this for logging purposes.
            var jsoneCheckInputRequestSerialized = new JavaScriptSerializer().Serialize(eCheckInputRequest);
            var jsoneCheckInputRequest = new System.Xml.Linq.XText(jsoneCheckInputRequestSerialized).ToString().Replace("\r", "").Replace("\n", "");

            // IMPORTANT: Now Add the private payment vendor information and SIN/EIN we don't want to log after we create the JSON.
            // I'm following the pattern set in Payment.aspx.cs
            addPrivatePaymentDetails(eCheckInputRequest);

            // Send the details to the payment provider for evaluation.
            LogAction(eComTransactionId, "Sending for Validation:" + jsoneCheckInputRequest);
            eCheckOutputResponseV = eCheckService.validateECHECK(eCheckInputRequest);

            var validateSuccess = ( (eCheckOutputResponseV.errCode == 0) && (eCheckOutputResponseV.beanErrCode == 0) );

            //rsh temp testing
            validateSuccess = true;


            // Save the Payment Provider response
            var jsoneCheckOutputResponseVSerialized = new JavaScriptSerializer().Serialize(eCheckOutputResponseV);
            var jsoneCheckOutputResponseV = new System.Xml.Linq.XText(jsoneCheckOutputResponseVSerialized).ToString();

            // Update some Ecom Job Details so we have history
            xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                "<column name=\"SourceObjectId\"><![CDATA[{1}]]></column>" +
                                "<column name=\"PaymentStatus\"><![CDATA[{2}]]></column>" +
                                "<column name=\"SentToPaymentProvider\"><![CDATA[{3}]]></column>" +
                                "<column name=\"PaymentProviderSetupResponse\"><![CDATA[{4}]]></column>" +
                                "<column name=\"PaymentMethod\"><![CDATA[{5}]]></column>" +
                                "</object>",
                                eComTransactionId,
                                objectId,
                                ((validateSuccess) ? "eCheckValidated" : "eCheckFailed"),
                                truncForLog(jsoneCheckInputRequest),
                                truncForLog(jsoneCheckOutputResponseV),
                                "eCheck");

            LogAction(eComTransactionId, "eCheckValidated:" + xml);

            this.ProcessXML(xml.ToString());


            if (validateSuccess)
            {
                // Send the details to the payment provider for processing.
                LogAction(eComTransactionId, "Sending for Processing:" + jsoneCheckInputRequest);
                eCheckOutputResponseP = eCheckService.processECHECK(eCheckInputRequest);

                var processSuccess = ((eCheckOutputResponseP.errCode == 0) && (eCheckOutputResponseP.beanErrCode == 0));

                //rsh temp testing
                processSuccess = true;


                string receiptNumber = eCheckOutputResponseP.confirmationNbr ?? "";

                // Save the Payment Provider response
                var jsoneCheckOutputResponsePSerialized = new JavaScriptSerializer().Serialize(eCheckOutputResponseP);
                var jsoneCheckOutputResponseP = new System.Xml.Linq.XText(jsoneCheckOutputResponsePSerialized).ToString();

                // Update some Ecom Job Details so we have history
                xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                    "<column name=\"SourceObjectId\"><![CDATA[{1}]]></column>" +
                                    "<column name=\"SuccessFlagColumnName\"><![CDATA[{2}]]></column>" +
                                    "<column name=\"PaymentStatus\"><![CDATA[{3}]]></column>" +
                                    "<column name=\"SentToPaymentProvider\"><![CDATA[{4}]]></column>" +
                                    "<column name=\"PaymentProviderPaymentResponse\"><![CDATA[{5}]]></column>" +
                                    "</object>",
                                    eComTransactionId,
                                    objectId,
                                    Request.QueryString["SuccessFlag"],
                                    ((processSuccess) ? "eCheckProcessed" : "eCheckFailed"),
                                    truncForLog(jsoneCheckInputRequest),
                                    truncForLog(jsoneCheckOutputResponseP));

                LogAction(eComTransactionId, "eCheckProcessed:" + xml);

                this.ProcessXML(xml.ToString());

                if (processSuccess)
                {
           
                    LogAction(eComTransactionId, "eCheck Processing Succeeded");

                    // Update ePayment with processing info and clear banking info (ePaymentMethodObjectId).
                    xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                        "<column name=\"confirmationNbr\"><![CDATA[{1}]]></column>" +
                                        "<column name=\"errCode\"><![CDATA[{2}]]></column>" +
                                        "<column name=\"errMsg1\"><![CDATA[{3}]]></column>" +
                                        "<column name=\"errMsg2\"><![CDATA[{4}]]></column>" +
                                        "<column name=\"beanErrCode\"><![CDATA[{5}]]></column>" +
                                        "<column name=\"beanErrMsg1\"><![CDATA[{6}]]></column>" +
                                        "<column name=\"beanErrMsg2\"><![CDATA[{7}]]></column>" +
                                        "<column name=\"eCOMJobId\"><![CDATA[{8}]]></column>" +
                                        "<column name=\"BankRoutingNumber\"><![CDATA[{9}]]></column>" +
                                        "<column name=\"BankAccountNumber\"><![CDATA[{9}]]></column>" +
                                        "</object>",
                                        ePaymentMethodObjectId,
                                        eCheckOutputResponseP.confirmationNbr,
                                        eCheckOutputResponseP.errCode,
                                        eCheckOutputResponseP.errMsg1,
                                        eCheckOutputResponseP.errMsg2,
                                        eCheckOutputResponseP.beanErrCode,
                                        eCheckOutputResponseP.beanErrMsg1,
                                        eCheckOutputResponseP.beanErrMsg2,
                                        eComTransactionId,
                                        "");

                    this.ProcessXML(xml.ToString());

                    // Successful Transaction...
                    RecordEComTransactionAcceptance(eComType, eComTransactionId.ToString(), receiptNumber);
                    Response.Redirect(successPaymentUrl);

                }
                else
                {

                    LogAction(eComTransactionId, "eCheck Processing Failed");
 
                    // Update ePayment with processing errors and don't clear banking info (ePaymentMethodObjectId).
                    xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                        "<column name=\"confirmationNbr\"><![CDATA[{1}]]></column>" +
                                        "<column name=\"errCode\"><![CDATA[{2}]]></column>" +
                                        "<column name=\"errMsg1\"><![CDATA[{3}]]></column>" +
                                        "<column name=\"errMsg2\"><![CDATA[{4}]]></column>" +
                                        "<column name=\"beanErrCode\"><![CDATA[{5}]]></column>" +
                                        "<column name=\"beanErrMsg1\"><![CDATA[{6}]]></column>" +
                                        "<column name=\"beanErrMsg2\"><![CDATA[{7}]]></column>" +
                                        "<column name=\"eCOMJobId\"><![CDATA[{8}]]></column>" +
                                        "<column name=\"PaymentStarted\"><![CDATA[{9}]]></column>" +
                                        "</object>",
                                        ePaymentMethodObjectId,
                                        eCheckOutputResponseP.confirmationNbr,
                                        eCheckOutputResponseP.errCode,
                                        eCheckOutputResponseP.errMsg1,
                                        eCheckOutputResponseP.errMsg2,
                                        eCheckOutputResponseP.beanErrCode,
                                        eCheckOutputResponseP.beanErrMsg1,
                                        eCheckOutputResponseP.beanErrMsg2,
                                        eComTransactionId,
                                        "N");

                    this.ProcessXML(xml.ToString());

                    RecordEComTransactionDenial(eComType, eComTransactionId);
                    Response.Redirect(failurePaymentUrl);
                }


            }
            else
            {
  
                LogAction(eComTransactionId, "eCheck Validation Failed");

                //need to put the error message somewhere so it can be diplayed to the user
                // Update ePayment with validate errors (ePaymentMethodObjectId).
                xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                    "<column name=\"confirmationNbr\"><![CDATA[{1}]]></column>" +
                                    "<column name=\"errCode\"><![CDATA[{2}]]></column>" +
                                    "<column name=\"errMsg1\"><![CDATA[{3}]]></column>" +
                                    "<column name=\"errMsg2\"><![CDATA[{4}]]></column>" +
                                    "<column name=\"beanErrCode\"><![CDATA[{5}]]></column>" +
                                    "<column name=\"beanErrMsg1\"><![CDATA[{6}]]></column>" +
                                    "<column name=\"beanErrMsg2\"><![CDATA[{7}]]></column>" +
                                    "<column name=\"eCOMJobId\"><![CDATA[{8}]]></column>" +
                                    "<column name=\"PaymentStarted\"><![CDATA[{9}]]></column>" +
                                    "</object>",
                                    ePaymentMethodObjectId,
                                    eCheckOutputResponseV.confirmationNbr,
                                    eCheckOutputResponseV.errCode,
                                    eCheckOutputResponseV.errMsg1,
                                    eCheckOutputResponseV.errMsg2,
                                    eCheckOutputResponseV.beanErrCode,
                                    eCheckOutputResponseV.beanErrMsg1,
                                    eCheckOutputResponseV.beanErrMsg2,
                                    eComTransactionId,
                                    "N");

                this.ProcessXML(xml.ToString());

                RecordEComTransactionDenial(eComType, eComTransactionId);
                Response.Redirect(failurePaymentUrl);

            }

        }
        catch (Exception ex)
        {
            LogAction(eComTransactionId, ex);
            // Note:  I purposely don't attempt to call RecordEComTransactionDenial() in this error handler...
            errorMessage = ex.Message;
        }

        if (errorMessage != null)
        {
            this.ErrorMessage = (USER_ERROR + "<br/>" + errorMessage).Replace("<br/>", "\r\n");
            this.RenderTitle(this.pnlTitleBand, "Error");
            this.RenderErrorMessage(this.pnlPaneBand);
        }
        if (this.ShowDebugSwitch)
        {
            this.RenderDebugLink(this.pnlDebugLinkBand);
        }
        this.RenderFooterBand(this.pnlFooterBand);

    }

    // Returns a Dynamic object / Generic Dictionary, containing the details
    // Access via result["taxNum"], result["legalName"]
    protected dynamic getTaxInfo(string eComId, string sourceObjectId)
    {
        string args;
        List<Dictionary<string, string>> results;

        args = String.Format("eComId={0}&sourceObjectId={1}", eComId, sourceObjectId);
        results = (List<Dictionary<string, string>>)this.ExecuteSql("GetReponsiblePartyInfo", args);

        var jss = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
        var info = getString(results[0]["RESULT"]);
        try
        {
            return jss.Deserialize<dynamic>(info);
        }
        catch (Exception ex)
        {
            throw new Exception("Can't deserialize tax info...]", ex);
        }
    }

    protected dynamic geteCheckInfo(string eComId, string ePaymentMethodObjectId, string userObjectId)
    {
        string args;
        List<Dictionary<string, string>> results;

        args = String.Format("eComId={0}&ePaymentMethodObjectId={1}&userObjectId={2}", eComId, ePaymentMethodObjectId, userObjectId);
        results = (List<Dictionary<string, string>>)this.ExecuteSql("GeteCheckInfo", args);

        var jss = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
        var info = getString(results[0]["RESULT"]);
        try
        {
            return jss.Deserialize<dynamic>(info);
        }
        catch (Exception ex)
        {
            throw new Exception("Can't deserialize ePayment Method info...]", ex);
        }
    }

    private string getCurrency(object input)
    {
        var strInput = getString(input);
        if (strInput == null)
            return null;
        else
        {
            try
            {
                return Convert.ToDecimal(input).ToString("#.00");
            }
            catch
            {
                throw new Exception("Can't format [" + strInput + "] to a decimal");
            }
        }
    }
    private string getString(object input)
    {
        if (input == null)
            return null;
        else
            return input.ToString();
    }

    private string truncForLog(string input, int max = 4000)
    {
        if (input == null)
            return null;
        else if (input.Length < max)
            return input;
        else if (max < 1)
            throw new Exception("truncForLog: Invalid Max");
        else if (max < 3)
            return input.Substring(0, max);
        else
            return input.Substring(0, max - 3) + "...";
    }

    private string getCustControl(string input)
    {
        if (string.IsNullOrEmpty(input))
            return "";
        // Remove all AlphaNumeric
        var newInput = "";
        foreach (char c in input)
        {
            if (Char.IsLetterOrDigit(c))
                newInput += c;
        }
        // Truncate to 4 and uppercase.
        if (newInput.Length < 4)
            return newInput.ToUpper();
        else
            return newInput.Substring(0, 4).ToUpper();
    }

    private List<OrderedDictionary> GetFees(string eComTransactionId)
    {
        this.StartDialog(String.Format("PosseObjectId={0}&PossePresentation=Default&PossePaneId=Fees", eComTransactionId));

        return this.GetPaneData();
    }

    protected void addPrivatePaymentDetails(NJeCheckWS.eCheckInput eCheckInputReq)
    {
        // Add the Private Details that should not be logged.
        //eCheckInputReq.builtDirectory = System.Configuration.ConfigurationManager.AppSettings["NJECheck.BuiltDirectory"]; //"LPS_POSSE";
        //eCheckInputReq.applicationId = System.Configuration.ConfigurationManager.AppSettings["NJECheck.ApplicationId"]; //"ABC LICENSE";
        eCheckInputReq.builtDirectory = "LPS_POSSE";
        eCheckInputReq.applicationId = "ABC LICENSE";
    }

    private void LogAction(string eComTransactionId, Exception error)
    {
        if (error is ThreadAbortException)
        {
            // http://stackoverflow.com/questions/2777105/why-response-redirect-causes-system-threading-threadabortexception 
            return;
        }
        LogAction(eComTransactionId, error.ToString());
    }
    private void LogAction(string eComTransactionId, string message, int retry = 0)
    {
        try
        {
            var dateTime = DateTime.Now;
            var logPath = ConfigurationManager.AppSettings["PaymentLoggingFileDir"];

            if (String.IsNullOrEmpty(logPath))
                throw new Exception("Payment logging is not configured.");

            var filePath = Path.Combine(logPath, String.Format("{0}_{1}_eCom#{2}_{3}{4}.{5}",
                "Payment",
                DateTime.Now.ToString("yyyy-MM-dd"),
                ((String.IsNullOrEmpty(eComTransactionId)) ? "ERR" : eComTransactionId),
                DateTime.Now.ToString("HH-mm-ss.fff"),
                ((retry == 0) ? "" : "_" + retry),
                "log"));

            if (retry == 0)
                message = "URL: " + Request.Url.ToString() + "\n" + message + "\n";

            // This opens, writes and closes the file.  This ensures minimal locking and that all data is flushed.
            File.AppendAllText(filePath, message);
        }
        catch (IOException ex)
        {
            if (retry < 3)
            {
                Thread.Sleep(200);
                LogAction(eComTransactionId, message, retry + 1);
            }
            else
            {
                // Ensure we don't display too much.
                throw new Exception("Can't log details");
            }
        }
    }

    private DateTime getDatabaseDate()
    {
        //note that executsql needs both input and output variables
        //the registered statment needs to reflect this in its bindings
        //only strings can be retured

        string args;
        args = String.Format("DATETIMEASSTRING={0}", "");
        List<Dictionary<string, string>> results;
        results = (List<Dictionary<string, string>>)this.ExecuteSql("DatabaseSysdateOutputString", args);

        string sDateTime = getString(results[0]["DATETIMEASSTRING"]);
        DateTime dDateTime = DateTime.ParseExact(sDateTime, "yyyy-MM-dd HH:mm:ss", null);

        return dDateTime;

    }
    //private void writeeCheckInput(NJeCheckWS.eCheckInput eCheckInputReq)
    //{
    //    Response.Write("<br/>");
    //    Response.Write("builtDirectory: " + eCheckInputReq.builtDirectory);
    //    Response.Write("<br/>");
    //    Response.Write("applicationId: " + eCheckInputReq.applicationId);
    //    Response.Write("<br/>");
    //    Response.Write("paymentId: " + eCheckInputReq.paymentId);
    //    Response.Write("<br/>");
    //    Response.Write("controlName: " + eCheckInputReq.controlName);
    //    Response.Write("<br/>");
    //    Response.Write("typeOfPay: " + eCheckInputReq.typeOfPay);
    //    Response.Write("<br/>");
    //    Response.Write("amtOfPay: " + eCheckInputReq.amtOfPay);
    //    Response.Write("<br/>");
    //    Response.Write("paymentYear: " + eCheckInputReq.paymentYear);
    //    Response.Write("<br/>");
    //    Response.Write("paymentMonth: " + eCheckInputReq.paymentMonth);
    //    Response.Write("<br/>");
    //    Response.Write("paymentType: " + eCheckInputReq.paymentType);
    //    Response.Write("<br/>");
    //    Response.Write("paymentDueDate: " + eCheckInputReq.paymentDueDate);
    //    Response.Write("<br/>");
    //    Response.Write("settlementDate: " + eCheckInputReq.settlementDate);
    //    Response.Write("<br/>");
    //    Response.Write("contactName: " + eCheckInputReq.contactName);
    //    Response.Write("<br/>");
    //    Response.Write("dayPhone: " + eCheckInputReq.dayPhone);
    //    Response.Write("<br/>");
    //    Response.Write("dayPhoneExten: " + eCheckInputReq.dayPhoneExten);
    //    Response.Write("<br/>");
    //    Response.Write("contactEmail: " + eCheckInputReq.contactEmail);
    //    Response.Write("<br/>");
    //    Response.Write("bankRoute: " + eCheckInputReq.bankRoute);
    //    Response.Write("<br/>");
    //    Response.Write("bankAcct: " + eCheckInputReq.bankAcct);
    //    Response.Write("<br/>");
    //    Response.Write("bankAcctType: " + eCheckInputReq.bankAcctType);
    //    Response.Write("<br/>");
    //    Response.Write("taxPayerName: " + eCheckInputReq.taxPayerName);
    //    Response.Write("<br/>");
    //    Response.Write("transSubCode: " + eCheckInputReq.transSubCode);
    //    Response.Write("<br/>");
    //    Response.Write("taxCode: " + eCheckInputReq.taxCode);
    //    Response.Write("<br/><br/>");
    //}

    //private void writeeCheckOutput(NJeCheckWS.eCheckOutput eCheckOutputResp)
    //{

    //    Response.Write("<br/>");
    //    Response.Write("confirmationNbr: " + eCheckOutputResp.confirmationNbr);
    //    Response.Write("<br/>");
    //    Response.Write("errCode: " + eCheckOutputResp.errCode.ToString());
    //    Response.Write("<br/>");
    //    Response.Write("errMsg1: " + eCheckOutputResp.errMsg1);
    //    Response.Write("<br/>");
    //    Response.Write("errMsg2: " + eCheckOutputResp.errMsg2);
    //    Response.Write("<br/>");
    //    Response.Write("beanErrCode: " + eCheckOutputResp.beanErrCode.ToString());
    //    Response.Write("<br/>");
    //    Response.Write("beanErrMsg1: " + eCheckOutputResp.beanErrMsg1);
    //    Response.Write("<br/>");
    //    Response.Write("beanErrMsg2: " + eCheckOutputResp.beanErrMsg2);
    //    Response.Write("<br/><br/>");

    //}


}