using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{

    public partial class Login : Computronix.POSSE.Outrider.PageBaseExt
    {
        private bool KeyLog = false;
        //<summary>
        //Provides access to the Log key. Used to prevent double audit trail of logins
        //</summary>
        protected virtual bool KeyLogAudit
        {
            get
            {
                return KeyLog;
            }
        }

        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetValidationMessage(this.GetCookie("Message"));
            this.SetCookie("Message", null);
            this.SetInitialFocus(this.txtEmail);

            string authenticationName, args, tempPassArgs, updateProfileArgs, locked, passExpired, updateProfile, profilePresName, errorMessage;
            List<OrderedDictionary> data;
            string userObjectId = null;

            this.LoadPresentation();

            if (this.ComesFrom == "possesignin" && string.IsNullOrEmpty(this.ErrorMessage))
            {
                locked = "N";
                passExpired = "N";
                updateProfile = "N";
                profilePresName = "Default";
                try
                {
                    args = String.Format("EmailAddress={0}", this.txtEmail.Text);

                    List<Dictionary<string, string>> results = (List<Dictionary<string, string>>)ExecuteSql("ABCOnlineEmailLogin", args);

                    authenticationName = results[0]["AUTHENTICATIONNAME"];
                    userObjectId = results[0]["OBJECTID"];

                    // Checks if the user is locked out
                    List<Dictionary<string, string>> LockoutCheck = (List<Dictionary<string, string>>)ExecuteSql("ABCOnlineEmailLockoutCheck", args);

                    locked = LockoutCheck[0]["ISLOCKEDOUT"];

                    // Checks if the user's temp password has expired
                    tempPassArgs = String.Format("AuthenticationName={0}", authenticationName);
                    List<Dictionary<string, string>> PassExpiredCheck = (List<Dictionary<string, string>>)ExecuteSql("ABCOnlineTempPassExpired", tempPassArgs);

                    passExpired = PassExpiredCheck[0]["TEMPPASSWORDISEXPIRED"];

                    if (passExpired == "Y")
                    {
                      // Throw an exception and show the generic login failed error
                      throw new Exception();
                    }

                    //set key to A
                    this.KeyLog = true;

                    this.SessionId = this.OutriderNet.NewSession(authenticationName, this.txtPassword.Text,
                          this.UserHostAddress, this.DebugKey);

                    userObjectId = this.UserInformation[0]["objectid"].ToString();

                    string redirectUrl;
                    redirectUrl = this.RedirectUrl;

                    // Checks if the User needs to update their profile information
                    updateProfileArgs = String.Format("AuthenticationName={0}", authenticationName);
                    List<Dictionary<string, string>> ProfileInfoCheck = (List<Dictionary<string, string>>)ExecuteSql("ABCOnlineMyProfileInfoRequired", updateProfileArgs);
                    updateProfile = ProfileInfoCheck[0]["CHECKMYPROFILEINFORMATION"];
                    profilePresName = ProfileInfoCheck[0]["MYPROFILEPRESENTATION"];

                    if (updateProfile == "Y")
                    {
                      redirectUrl = string.Format("{0}?PossePresentation={1}&PosseObjectId={2}",
                          this.HomePageUrl, profilePresName, userObjectId);
                    }

                    //Audit Login as Successful.
                    this.TrackLoginSuccess(this.txtEmail.Text, "Login", "Login Attempt Successful", userObjectId);

                    if (redirectUrl != null)
                    {
                      this.RedirectUrl = null;
                      this.ReleaseOutriderNet();
                      this.Response.Redirect(redirectUrl, false);
                    } else
                    {
                      this.ReleaseOutriderNet();
                      this.Response.Redirect(string.Format("{0}?PossePresentation=Default&PosseObjectId={1}",
                          this.HomePageUrl, userObjectId), false);
                    }
                    //reset key
                    this.KeyLog = false;
                }
                catch (Exception ex)
                {
                    // checks if there is an issue with loggin in (ie either the account is locked or the username/password are not correct)
                    if (passExpired == "Y")
                    {
                        this.TrackLogin(this.txtEmail.Text, "Login", "Login Failed Password Expired", userObjectId);
                        errorMessage = "<span style='text-decoration: underline'>Login Failed:</span><span style='font-weight: normal'> Your password has expired. Please reset your password.</span>";
                    }
                    else if (locked == "Y")
                    {
                        this.TrackLogin(this.txtEmail.Text, "Login", "Login Failed Account Locked", userObjectId);
                        errorMessage = "<span style='text-decoration: underline'>Account Locked:</span><span style='font-weight: normal'> Consecutive failed login attempts have caused your account to be locked. Click on \"Forgot password?\" to change your password and unlock the account.</span>";
                    }
                    else
                    {
                        this.TrackLogin(this.txtEmail.Text, "Login", "Login Failed", userObjectId);
                        errorMessage = "<span style='text-decoration: underline'>Login Failed:</span><span style='font-weight: normal'> Please check your Email / User Name and Password and try again. Consecutive failed login attempts will lock your account.</span>";
                    }

                    // if the error message is an issue with the login, use "ShowLogin" do display error, otherwise use RenderErrorMessage
                    if (string.IsNullOrEmpty(this.ErrorMessage))
                    {
                        this.ShowLogin(errorMessage);
                    }
                    else
                    {
                        this.RenderErrorMessage(this.pnlErrorBand);
                    }
                }
            }
            else
            {
                this.CheckClient();
            }

            if (this.Request.QueryString["ShowPWSuccess"] ==  "Y")
            {
                ShowNotice("Password reset successfully.");
            }
            
            this.RenderFooterBand(this.pnlDebugLinkBand);
        }

        protected void ShowNotice(string notice)
        {
            string script = @"document.getElementById('noticePanelText').innerText = '{0}';
                              document.getElementById('noticePanel').style.display = 'table'";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowNotice", String.Format(script, notice), true);
        }
        #endregion
    }
}
