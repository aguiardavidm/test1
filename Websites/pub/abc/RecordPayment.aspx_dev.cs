using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Script.Serialization;
using System.Xml;
using NJEPayWS;

public partial class Payment : Computronix.POSSE.Outrider.PageBaseExt
{
    protected void Page_Load(object sender, EventArgs e)
    {
		const string PAYMENT_PARAMETER = "ePaymentId";
		const string CANCELLED_PARAMETER = "Cancelled";

		const string RECEIPT_PARAMETER = "ReceiptForId";
		
		// Could not process payment result.  Payment likely not successful but unknown.
		const string SYSTEM_ERROR_PAYMENT_UNKNOWN = 
			"The payment result could not be processed due to an unexpected system error.  Do NOT attempt to pay again or you may be billed twice.\n\n" +
			"The error details have been logged.";
		// Payment successful but posse could not process.
		const string SYSTEM_ERROR_PAYMENT_SUCCESS = 
			"Your payment was successful but we could not process the results.  Do NOT attempt to pay again or you may be billed twice.\n\n" +
			"The error details have been logged.";
		
		string eComTransactionId = Request.QueryString[PAYMENT_PARAMETER];
		
		// Need to know so that I can warn the user NOT to attempt to pay again if it was successful.
		bool paymentSuccess = false;
		string errorMessage  = null;

		try
		{
			this.RenderUserInfo(this.pnlUserInfo);
			this.RenderMenuBand(this.pnlMenuBand);
			
			string eComType = Request.QueryString["EComType"];
			bool cancelled = Convert.ToBoolean(Request.QueryString[CANCELLED_PARAMETER]);

			if (String.IsNullOrEmpty(eComTransactionId)) {
				throw new Exception("Missing Parameters");
			}
			string xml;
			// Do this primarily to validate the eCom ObjectId.
			eComTransactionId = GetEComTransactionObjectId(eComType, eComTransactionId).ToString();
			
			// Remove the extra parameters to prevent a posse postback error.
			var queryString = HttpUtility.ParseQueryString(Request.QueryString.ToString());
			queryString.Remove(PAYMENT_PARAMETER);
			queryString.Remove(CANCELLED_PARAMETER);
			
			queryString.Add(RECEIPT_PARAMETER,eComTransactionId);

			var redirectURL = String.Format("Default.aspx?{0}", queryString.ToString());

			
			if (cancelled) {
				xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
										"<column name=\"PaymentStatus\"><![CDATA[{1}]]></column>" +
										"</object>",
										eComTransactionId,
										"PaymentCancelled" );
				LogAction(eComTransactionId,
					"Cancelled:" + xml + "\n" +
					"Redirect to:" + redirectURL);
				this.ProcessXML(xml.ToString());

				RecordEComTransactionDenial(eComType, eComTransactionId);
				this.ErrorMessage = "Payment Cancelled";

				Response.Redirect(redirectURL);            
			}
			else
			{
				// Record the payment result
				// NJ_NICUSA_WSClient wsClient = new NJ_NICUSA_WSClient();

				// njGetPaymentInfoRequest infoRequest = new njGetPaymentInfoRequest();
				// infoRequest.token = Request.QueryString["token"];
				// infoRequest.njApplicationName = "LPS_POSSE";
				// njGetPaymentInfoResponse infoResponse = wsClient.getPaymentInfo(infoRequest);

				// Need to check all to ensure we have a real response and real success.
				string receiptNumber = "1234"; // infoResponse.AUTHCODE??"";
				paymentSuccess = true;
					// ((infoResponse.FAILCODE??"") == "N") && 
					// String.IsNullOrEmpty(infoResponse.FAILMESSAGE) && 
					// !String.IsNullOrEmpty(receiptNumber) &&
					// !String.IsNullOrEmpty(infoResponse.TOTALAMOUNT);
			
				// Save the response regardless of what it was.
				// var jsonInfoResponse = new JavaScriptSerializer().Serialize(infoResponse);
				// jsonInfoResponse = new System.Xml.Linq.XText(jsonInfoResponse).ToString();
								
				if (paymentSuccess)
				{
					// var extendedValues = infoResponse.extendedValues;
										
					var extraFees = "";
					var originalTotal = "";
					// foreach(var item in extendedValues) {
						// switch(item.NAME)
						// {
							// case "ORIG_COS_AMOUNT":
								// originalTotal = item.VALUE;
								// break;
							// case "ORIG_FEE_AMOUNT":
								// extraFees = item.VALUE;
								// break;		
						// }
					// }
					DateTime receiptDate = DateTime.Now; // getDate(infoResponse.RECEIPTDATE + " " + infoResponse.RECEIPTTIME);
									
					xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
										"<column name=\"PaymentStatus\"><![CDATA[{1}]]></column>" +
										"<column name=\"PaymentProviderPaymentResponse\"><![CDATA[{2}]]></column>" +
										"<column name=\"PaymentProviderPayeeName\"><![CDATA[{3}]]></column>" +
										"<column name=\"PaymentProviderOrderId\"><![CDATA[{4}]]></column>" +
										"<column name=\"PaymentProviderReceiptDate\">{5}</column>" +
										"<column name=\"PaymentProviderTotalPaid\"><![CDATA[{6}]]></column>" +
										"<column name=\"PaymentProviderOriginalAmount\"><![CDATA[{7}]]></column>" +
										"<column name=\"PaymentProviderExtraFees\"><![CDATA[{8}]]></column>" +
										"<column name=\"PaymentProviderPaymentType\"><![CDATA[{9}]]></column>" +
										"<column name=\"PaymentProviderCCType\"><![CDATA[{10}]]></column>" +
										"<column name=\"PaymentProviderCCNumber\"><![CDATA[{11}]]></column>" +
										"</object>",
										eComTransactionId,
										((paymentSuccess) ? "PaymentSuccess": "PaymentFailed"), 
										"Dev - jsonInfoResponse",// jsonInfoResponse,
										"Development User", // infoResponse.NAME,
										9999,// infoResponse.ORDERID,
										receiptDate.ToString("yyyy-MM-dd HH:mm:ss"),
										Request.QueryString["TotalAmount"],//infoResponse.TOTALAMOUNT,
										Request.QueryString["TotalAmount"],//originalTotal,
										extraFees,
										Request.QueryString["PayType"],//infoResponse.PAYTYPE,
										"VISA",// infoResponse.creditCardType,
										6789// infoResponse.LAST4NUMBER 
                                        );

					LogAction(eComTransactionId, "PaymentSuccess: " + xml + "\n" + "Redirect to:" + redirectURL);
					this.ProcessXML(xml.ToString());
					
					// Successful Transaction...
					RecordEComTransactionAcceptance(eComType, eComTransactionId.ToString(), receiptNumber);
					Response.Redirect(redirectURL);
				}
				else
				{
					// Failed Payment
					xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
										"<column name=\"PaymentStatus\"><![CDATA[{1}]]></column>" +
										"<column name=\"PaymentProviderPaymentResponse\"><![CDATA[{2}]]></column>" +
										"</object>",
										eComTransactionId,
										"PaymentFailed", 
										"jsonInfoResponse" // jsonInfoResponse
                                        );

					LogAction(eComTransactionId, "PaymentFailed: " + xml + "\n" + "Redirect to:" + redirectURL);
					this.ProcessXML(xml.ToString());
					
					RecordEComTransactionDenial(eComType, eComTransactionId);
					this.ErrorMessage = SYSTEM_ERROR_PAYMENT_UNKNOWN;//+ "<br/><br/>" + infoResponse.FAILMESSAGE;   // <<== This seems to get lost.
					Response.Redirect(redirectURL);            
				}
			}
		}
		catch(Exception ex)
		{
			var messageId = " (Id:"+eComTransactionId+")";
			LogAction(eComTransactionId,ex);
			if (paymentSuccess) {
				// The payment was known to be successful but it could not be processed.
				// Need to warn the client not to attempt payment again or they will be double charged.
				errorMessage = SYSTEM_ERROR_PAYMENT_SUCCESS + messageId;
			}
			else
				errorMessage = SYSTEM_ERROR_PAYMENT_UNKNOWN + messageId;
		}

		if (errorMessage != null)
		{
            this.ErrorMessage = (errorMessage).Replace("<br/>","\r\n");
            this.RenderTitle(this.pnlTitleBand, "Error");
            this.RenderErrorMessage(this.pnlPaneBand);
		}
        if (this.ShowDebugSwitch)
        {
            this.RenderDebugLink(this.pnlDebugLinkBand);
        }
        this.RenderFooterBand(this.pnlFooterBand);
    }
    private void LogAction (string eComTransactionId, Exception error)
    {
		if (error is ThreadAbortException) {
			// http://stackoverflow.com/questions/2777105/why-response-redirect-causes-system-threading-threadabortexception 
			return;
		}	
		LogAction(eComTransactionId, error.ToString());
	}
    private void LogAction (string eComTransactionId, string message, int retry = 0)
    {
		try
		{
			var dateTime = DateTime.Now;
			var logPath = ConfigurationManager.AppSettings["PaymentLoggingFileDir"];
			
			if (String.IsNullOrEmpty(logPath))
				throw new Exception("Payment logging is not configured.");
				
			var filePath = Path.Combine(logPath, String.Format("{0}_{1}_eCom#{2}_{3}{4}.{5}", 
				"Payment", 
				DateTime.Now.ToString("yyyy-MM-dd"),
				((String.IsNullOrEmpty(eComTransactionId)) ? "ERR":eComTransactionId),
				DateTime.Now.ToString("HH-mm-ss.fff"),
				((retry==0) ? "":"_"+retry),				
				"log"));

			if (retry == 0)
				message = "URL: " + Request.Url.ToString() + "\n" + message + "\n";
				
			// This opens, writes and closes the file.  This ensures minimal locking and that all data is flushed.
			File.AppendAllText(filePath, message);
		}
		catch(IOException ex)
		{
			if (retry < 3) {
				Thread.Sleep(200);
				LogAction(eComTransactionId, message, retry + 1);
			}
			else {
				// Ensure we don't display too much.
				throw new Exception("Can't log details");
			}
		}
    }
	
    private string stripEndsWith(string input, string needle)
    {
        if (input.EndsWith(needle, StringComparison.InvariantCultureIgnoreCase))
            return input.Substring(0, input.Length - needle.Length).Trim();
        return input;
    }
    private DateTime getDate(string input)
    {
        // I am expecting....  "3/4/2015 02:41:56 PM EST" or "... EDT"
        foreach(string tz in new string[] {"EST","EDT","MST"}) {
            input = stripEndsWith(input, tz);
        }
        try
        {
            // Base time..   
            var dateFormatString = "M/d/yyyy hh:mm:ss tt";

            return DateTime.ParseExact(
                input,
                dateFormatString,
                CultureInfo.InvariantCulture);
        }
        catch (Exception ex)
        {
            LogAction(null, ex);
            return DateTime.Now;
        }
    }
}
