using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Script.Serialization;
using System.Xml;
using NJEPayWS;


public partial class Payment : Computronix.POSSE.Outrider.PageBaseExt
{
    private njPreparePaymentResult preparePayResult;
    private string jsonPayInfo;
    private string jsonPayInfoString;
    private string objectId;
    private string eComType;
    private bool success;
    string jsonPreparePayResultString;

    protected void Page_Load(object sender, EventArgs e)
    {
		const string USER_ERROR = "Fee Payment halted due to an unexpected system error.  The details have been logged.";
		
		string errorMessage = null;
		string xml;
		string eComTransactionId=null;
        string successPaymentUrl=null;
		
        this.RenderUserInfo(this.pnlUserInfo);
        this.RenderMenuBand(this.pnlMenuBand);
        try
        {
            objectId = Request.QueryString["PosseObjectId"];
            string ePaymentMethodObjectId = Request.QueryString["ePaymentMethodObjectId"];

            int[] staticIntArray = new int[1] { Convert.ToInt32(objectId) };
            eComType = Request.QueryString["EComType"];
            eComTransactionId = CreateEComTransaction(eComType, staticIntArray);


            // Update ePaymentMethod Job to record the eComTransactionId
            xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                "<column name=\"eCOMJobId\"><![CDATA[{1}]]></column>" +
                                "</object>",
                                ePaymentMethodObjectId,
                                eComTransactionId);

            this.ProcessXML(xml.ToString());
            // Get the Tax Info
            var taxInfo = getTaxInfo(eComTransactionId, objectId);
            var legalName = getString(taxInfo["legalName"]);
            var taxNum = getString(taxInfo["taxNum"]);
            var taxCode = getString(taxInfo["taxCode"]);

            if (legalName == null)
                throw new Exception("Missing Legal Information");

            // Get the list of fee objects.
            List<OrderedDictionary> fees = GetFees(eComTransactionId);

            decimal lineItemTotal = 0;
            int lineItemCount = 0;
            var lineItemList = new List<NJEPayWS.njLineItem>();
            foreach (OrderedDictionary fee in fees)
            {
                // Ensure that the eCom job knows exactly which payments it is paying.
                xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                      "<relationship endpoint=\"FeeEndPoint\" toobjectid=\"{1}\" action=\"Insert\">" +
                                      "<column name=\"FeeAmountToPay\">{2}</column>" +
                                      "</relationship>" +
                                   "</object>",
                                   eComTransactionId, fee["ObjectId"].ToString(), Convert.ToSingle(fee["TotalOwing"]));
                lineItemTotal += Convert.ToDecimal(fee["TotalOwing"]);
                lineItemCount++;

                // Build up a list of line items	
                lineItemList.Add(new NJEPayWS.njLineItem
                {
                    ITEM_ID = lineItemCount,
                    DESCRIPTION = getString(fee["Description"]),
                    QUANTITY = "1",
                    SKU = taxCode,
                    UNIT_PRICE = getCurrency(fee["TotalOwing"])
                });

                this.ProcessXML(xml.ToString());
            }

            decimal feeAmount = 0;
            try
            {
                feeAmount = GetEComTransactionFee(eComType, eComTransactionId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't get fee for [{0}/{1}], ", eComType, eComTransactionId) + ex.Message);
            }
            if (feeAmount <= 0)
                throw new Exception(string.Format("Invalid attempt to pay a fee of {0}. Transaction:{1}", feeAmount, eComTransactionId));
            else if (feeAmount > lineItemTotal)
                throw new Exception(string.Format("Fee mismatch, the fee to be paid of {2:0.00}, exceeds the total of the {0} line item(s) of {1:0.00}. Transaction:{3}", lineItemCount, lineItemTotal, feeAmount, eComTransactionId));

            string confirmationobjectId = Request.QueryString["ConfirmationObjectId"];
            string toObjectId = objectId;
            if (!String.IsNullOrEmpty(confirmationobjectId))
                toObjectId = confirmationobjectId;

            string recordPaymentBaseUrl = String.Format("{0}{1}", ConfigurationManager.AppSettings["RootUrl"], ConfigurationManager.AppSettings["RecordPaymentPageUrl"]);
            successPaymentUrl = String.Format(
                "{0}?ePaymentId={1}&PosseObjectId={2}&PossePresentation={3}&PossePane={4}&EComType={5}&SuccessFlag={6}&TotalAmount={7}&PayType={8}",
                recordPaymentBaseUrl, eComTransactionId, toObjectId, Request.QueryString["SuccessPresentation"], Request.QueryString["SuccessPane"], eComType, Request.QueryString["SuccessFlag"], getCurrency(feeAmount), "CC");
            string failurePaymentUrl = String.Format(
                "{0}?ePaymentId={1}&PosseObjectId={2}&PossePresentation={3}&PossePane={4}&EComType={5}",
                recordPaymentBaseUrl, eComTransactionId, toObjectId, Request.QueryString["FailurePresentation"], Request.QueryString["FailurePane"], eComType);
            string cancelPaymentUrl = failurePaymentUrl + "&Cancelled=true";
            // Setup the payment and redirect to payment provider
            NJ_NICUSA_WSClient wsClient = new NJ_NICUSA_WSClient();

            // Create a list of keyvalue pairs.
            var fieldList = new List<NJEPayWS.njField>();
            var now = DateTime.Now;

            fieldList.Add(new NJEPayWS.njField { FIELDNAME = "Customer_Identification", FIELDVALUE = taxNum });
            fieldList.Add(new NJEPayWS.njField { FIELDNAME = "Customer_Name_Control", FIELDVALUE = getCustControl(legalName) });
            fieldList.Add(new NJEPayWS.njField { FIELDNAME = "Return_Year", FIELDVALUE = now.Year.ToString() });
            fieldList.Add(new NJEPayWS.njField { FIELDNAME = "Return_Month", FIELDVALUE = now.Month.ToString() });
            fieldList.Add(new NJEPayWS.njField { FIELDNAME = "Return_Day", FIELDVALUE = now.Day.ToString() });

            // Create the PaymentInfo object.
            var payInfo = new njPaymentInfo();

            payInfo.AMOUNT = getCurrency(feeAmount);
            payInfo.LOCALREFID = eComTransactionId; // Client wan't a more meaningful value.
            payInfo.UNIQUETRANSID = eComTransactionId;  // If not unique then it will result in a duplicate payment error
            payInfo.PAYTYPE = "CC";

            payInfo.HREFSUCCESS = successPaymentUrl;
            payInfo.HREFFAILURE = failurePaymentUrl;
            payInfo.HREFCANCEL = cancelPaymentUrl;
            payInfo.ORDERATTRIBUTES = fieldList.ToArray();
            payInfo.LINEITEMS = lineItemList.ToArray();

            // Serialize this for logging purposes.
            jsonPayInfo = new JavaScriptSerializer().Serialize(payInfo);
            jsonPayInfoString = new System.Xml.Linq.XText(jsonPayInfo).ToString();
            if (!String.IsNullOrEmpty(taxNum))
                jsonPayInfoString = jsonPayInfoString.Replace(taxNum, "*******");  // Redact the TaxNum so it won't be visible in the log
            LogAction(eComTransactionId, "Sending:" + jsonPayInfoString);

            // IMPORTANT: Now Add the private payment vendor information and SIN/EIN we don't want to log after we create the JSON.
            addPrivatePaymentDetails(payInfo);
            payInfo.ORDERATTRIBUTES = fieldList.ToArray();

            // Send the details to the vendor for evaluation.
            // preparePayResult = wsClient.preparePayment(payInfo);

            // var jsonPreparePayResult = new JavaScriptSerializer().Serialize(preparePayResult);
            // jsonPreparePayResultString = new System.Xml.Linq.XText(jsonPreparePayResult).ToString();
 
            success = true; //preparePayResult.APPLICATION_REDIRECT_WITH_TOKEN.Length > 0;
        }
        catch (Exception ex)
        {
            LogAction(eComTransactionId, ex);
            // Note:  I purposely don't attempt to call RecordEComTransactionDenial() in this error handler...
            errorMessage = ex.Message;
        }
        finally
        {
            try
            {
                // Update some Ecom Job Details so we have history
                xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                    "<column name=\"SourceObjectId\"><![CDATA[{1}]]></column>" +
                                    "<column name=\"SuccessFlagColumnName\"><![CDATA[{2}]]></column>" +
                                    "<column name=\"PaymentStatus\"><![CDATA[{3}]]></column>" +
                                    "<column name=\"SentToPaymentProvider\"><![CDATA[{4}]]></column>" +
                                    "<column name=\"PaymentProviderSetupResponse\"><![CDATA[{5}]]></column>" +
                                    "<column name=\"PaymentProviderToken\"><![CDATA[{6}]]></column>" +
                                    "<column name=\"PaymentMethod\"><![CDATA[{7}]]></column>" +
                                    "</object>",
                                    eComTransactionId,
                                    objectId,
                                    Request.QueryString["SuccessFlag"],
                                    ((success) ? "PreparePayment" : "PrepareRejected"),
                                    truncForLog(jsonPayInfoString),
                                    jsonPreparePayResultString,
                                    1,
                                     "Credit Card");

                this.ProcessXML(xml.ToString());
                LogAction(eComTransactionId, "PreparedReceived:" + xml);
                
                if (success)
                {
                    Response.Redirect(successPaymentUrl, false);
                }
                else
                {
                    // DEV NOTE:  All of the Debug information is stored on the eCom Job.
                    errorMessage = preparePayResult.ERRORMESSAGE;
                    RecordEComTransactionDenial(eComType, eComTransactionId);
                }
            }
            catch (Exception ex)
            {
                LogAction(eComTransactionId, ex);
                // Note:  I purposely don't attempt to call RecordEComTransactionDenial() in this error handler...
                errorMessage = ex.Message;
            }
        }

		if (errorMessage != null)
		{
            this.ErrorMessage = (USER_ERROR + "<br/>" + errorMessage).Replace("<br/>","\r\n");
            this.RenderTitle(this.pnlTitleBand, "Error");
            this.RenderErrorMessage(this.pnlPaneBand);
		}
        if (this.ShowDebugSwitch)
        {
            this.RenderDebugLink(this.pnlDebugLinkBand);
        }
        this.RenderFooterBand(this.pnlFooterBand);
    }

	// Returns a Dynamic object / Generic Dictionary, containing the details
	// Access via result["taxNum"], result["legalName"]
	protected dynamic getTaxInfo(string eComId, string sourceObjectId)
	{
		string args;
		List<Dictionary<string, string>> results;

		args = String.Format("eComId={0}&sourceObjectId={1}", eComId, sourceObjectId);
		results = (List<Dictionary<string, string>>)this.ExecuteSql("GetReponsiblePartyInfo", args);

		var jss = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
		var info = getString(results[0]["RESULT"]);
		try
		{
			return jss.Deserialize<dynamic>(info);
		}
		catch (Exception ex)
		{
			throw new Exception("Can't deserialize tax info...]", ex);
		}
	}

	
	private string getCurrency(object input) {
		var strInput = getString(input);
		if (strInput == null)
			return null;
		else {
			try
			{
				return Convert.ToDecimal(input).ToString("#.00");
			}
			catch
			{
				throw new Exception("Can't format [" + strInput + "] to a decimal");
			}
		}
	}
	private string getString(object input) {
		if (input == null)
			return null;
		else
			return input.ToString();
	}

	private string truncForLog(string input, int max = 4000) {
		if (input == null)
			return null;
		else if (input.Length < max)
			return input;
		else if (max < 1)
			throw new Exception("truncForLog: Invalid Max");
		else if (max < 3)
			return input.Substring(0, max);
		else
			return input.Substring(0, max - 3) + "...";
	}
	
	private string getCustControl(string input) {
		if (string.IsNullOrEmpty(input))
			return "";
		// Remove all AlphaNumeric
		var newInput = "";
		foreach(char c in input){
			if (Char.IsLetterOrDigit(c))
				newInput += c;
		} 
		// Truncate to 4 and uppercase.
		if (newInput.Length < 4)
			return newInput.ToUpper();
		else
			return newInput.Substring(0,4).ToUpper();
	}
	
    private List<OrderedDictionary> GetFees(string eComTransactionId)
    {
        this.StartDialog(String.Format("PosseObjectId={0}&PossePresentation=Default&PossePaneId=Fees", eComTransactionId));

        return this.GetPaneData();
    }
	
    protected void addPrivatePaymentDetails(njPaymentInfo payInfo)
    {
		// Add the Private Details that should not be logged.
        payInfo.STATECD = System.Configuration.ConfigurationManager.AppSettings["NJEPay.StateCD"]; //"NJ"
        payInfo.MERCHANTID = System.Configuration.ConfigurationManager.AppSettings["NJEPay.MerchantId"]; //"njlpsabc";
        payInfo.MERCHANTKEY = System.Configuration.ConfigurationManager.AppSettings["NJEPay.MerchantKey"]; //"bRawus3u";
        payInfo.SERVICECODE = System.Configuration.ConfigurationManager.AppSettings["NJEPay.ServiceCode"]; //"lpsAbcLicense"; // For License Fees...Use lpsABCFines for Fines, if applicable.
        payInfo.njApplicationName = System.Configuration.ConfigurationManager.AppSettings["NJEPay.ApplicationName"]; //"LPS_POSSE";
    }

    private void LogAction (string eComTransactionId, Exception error)
    {
		if (error is ThreadAbortException) {
			// http://stackoverflow.com/questions/2777105/why-response-redirect-causes-system-threading-threadabortexception 
			return;
		}	
		LogAction(eComTransactionId, error.ToString());
	}
    private void LogAction (string eComTransactionId, string message, int retry = 0)
    {
		try
		{
			var dateTime = DateTime.Now;
			var logPath = ConfigurationManager.AppSettings["PaymentLoggingFileDir"];
			
			if (String.IsNullOrEmpty(logPath))
				throw new Exception("Payment logging is not configured.");
				
			var filePath = Path.Combine(logPath, String.Format("{0}_{1}_eCom#{2}_{3}{4}.{5}", 
				"Payment", 
				DateTime.Now.ToString("yyyy-MM-dd"),
				((String.IsNullOrEmpty(eComTransactionId)) ? "ERR":eComTransactionId),
				DateTime.Now.ToString("HH-mm-ss.fff"),
				((retry==0) ? "":"_"+retry),				
				"log"));

			if (retry == 0)
				message = "URL: " + Request.Url.ToString() + "\n" + message + "\n";
				
			// This opens, writes and closes the file.  This ensures minimal locking and that all data is flushed.
			File.AppendAllText(filePath, message);
		}
		catch(IOException ex)
		{
			if (retry < 3) {
				Thread.Sleep(200);
				LogAction(eComTransactionId, message, retry + 1);
			}
			else {
				// Ensure we don't display too much.
				throw new Exception("Can't log details");
			}
		}
    }
}