<%@ Page Language="C#" AutoEventWireup="true"
  CodeFile="UploadNew.aspx.cs" Inherits="Computronix.POSSE.Outrider.UploadNew" Title="Upload" %>

<html>
<head id="Head1" runat="server">
    <title>Outrider Popup Master</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="Common.css" rel="stylesheet" type="text/css" />
    <script src="posseglobal<%=ConfigurationManager.AppSettings["PosseGlobalVersion"]%>.js" language="javascript" type="text/javascript"></script>
    <script src="posseextension<%=ConfigurationManager.AppSettings["PosseExtensionVersion"]%>.js" language="javascript" type="text/javascript"></script>
    <script src="jquery-1.5.1.min.js" language="javascript" type="text/javascript"></script>

    <style type="text/css">@import url(plupload/examples/css/plupload.queue.css);</style>

    <!-- Thirdparty intialization scripts, needed for the Google Gears and BrowserPlus runtimes -->
    <script type="text/javascript" src="plupload/js/gears_init.js"></script>

    <!-- Load plupload and all it's runtimes and finally the jQuery queue widget -->
    <script type="text/javascript" src="plupload/js/plupload.full.min.js"></script>
    <!--<script type="text/javascript" src="plupload/js/jquery.plupload.queue.min.js"></script>-->
    <script type="text/javascript" src="plupload/src/javascript/jquery.plupload.queue.js"></script>

    <script type="text/javascript" src="PossePLUpload.js"></script>

</head>
<body>
    <form id="Form1" runat="server"></form>
    <asp:PlaceHolder runat="server" ID="scriptPlaceholder" />
    <form id="uploadform" onsubmit="return false">
        <div id="uploader">
            <p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
        </div>
    </form>
</body>
</html>