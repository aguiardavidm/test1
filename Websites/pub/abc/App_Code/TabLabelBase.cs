using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
	/// <summary>
	/// Base class for all tab labels.
	/// </summary>
	public partial class TabLabelBase :
		System.Web.UI.UserControl
	{
		#region Data Members
		private string text = null;
		/// <summary>
		/// Provides access to the text for the tab label.
		/// </summary>
		public virtual string Text
		{
			get
			{
				return this.text;
			}
			set
			{
				this.text = value;
			}
		}

		private string navigationUrl = null;
		/// <summary>
		/// Provides access to the navigation url for the tab label.
		/// </summary>
		public virtual string NavigationUrl
		{
			get
			{
				return this.navigationUrl;
			}
			set
			{
				this.navigationUrl = value;
			}
		}

		private bool selected = false;
		/// <summary>
		/// Indicates if this is the currently selected tab label.
		/// </summary>
		public virtual bool Selected
		{
			get
			{
				return this.selected;
			}
			set
			{
				this.selected = value;
			}
		}

		private bool enabled = false;
		/// <summary>
		/// Indicates if the tab label is enabled or disabled.
		/// </summary>
		public virtual bool Enabled
		{
			get
			{
				return this.enabled;
			}
			set
			{
				this.enabled = value;
			}
		}

		private string condition = null;
		/// <summary>
		/// Provides access to the condition for the tab label.
		/// </summary>
		public virtual string Condition
		{
			get
			{
				return this.condition;
			}
			set
			{
				this.condition = value;
			}
		}

		private int count = 0;
		/// <summary>
		/// Indicates the number of tab labels to be rendered.
		/// </summary>
		public virtual int Count
		{
			get
			{
				return this.count;
			}
			set
			{
				this.count = value;
			}
		}

		private int index = 0;
		/// <summary>
		/// Indicates the index of this tab label.
		/// </summary>
		public virtual int Index
		{
			get
			{
				return this.index;
			}
			set
			{
				this.index = value;
			}
		}
		#endregion
	}
}