using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Computronix.Ext
{
    /// <summary>
    /// Exception thrown when a member does not exist on a class.
    /// </summary>
    [Serializable()]
    public class InvokeMemberException : System.ApplicationException, ISerializable
    {
        private object target;
        /// <summary>
        /// Target on which the member was invoked.
        /// </summary>
        public object Target
        {
            get
            {
                return this.target;
            }
        }

        private string member;
        /// <summary>
        /// Name of the member that caused the exception.
        /// </summary>
        public string Member
        {
            get
            {
                return this.member;
            }
        }

        private object[] args;
        /// <summary>
        /// Arguments to the member.
        /// </summary>
        public object[] Args
        {
            get
            {
                return this.args;
            }
        }

        /// <summary>
        /// Returns the types of the arguments as a comma delimited list.
        /// </summary>
        /// <param name="args">Array arguments.</param>
        /// <returns>Comma delimited list of argument types.</returns>
        protected static string GetArgTypes(object[] args)
        {
            StringBuilder result = new StringBuilder();

            foreach (object arg in args)
            {
                if (arg == null)
                {
                    result.Append("null,");
                }
                else
                {
                    result.Append(arg.GetType().Name + ",");
                }
            }

            if (args.Length > 0)
            {
                result.Remove(result.Length - 1, 1);
            }

            return result.ToString();
        }

        /// <summary>
        /// Constructor with a default message and no inner exception.
        /// </summary>
        /// <param name="target">Target on which the member was invoked.</param>
        /// <param name="member">Name of the member that caused the exception.</param>
        /// <param name="args">Arguments to the member.</param>
        public InvokeMemberException(object target, string member, object[] args) :
            base("Member was not found on target.\r\n\r\nTarget=" + target.GetType().FullName + "\r\nMember=" + member + "\r\nArgs=" + InvokeMemberException.GetArgTypes(args))
        {
            this.target = target;
            this.member = member;
            this.args = args;
        }

        /// <summary>
        /// Constructor with a given message and no inner exception.
        /// </summary>
        /// <param name="target">Target on which the member was invoked.</param>
        /// <param name="member">Name of the member that caused the exception.</param>
        /// <param name="args">Arguments to the member.</param>
        /// <param name="message">The message to display to the user.</param>
        public InvokeMemberException(object target, string member, object[] args, string message) :
            base(message)
        {
            this.target = target;
            this.member = member;
            this.args = args;
        }

        /// <summary>
        /// Constructor with a default message and the inner exception.
        /// </summary>
        /// <param name="target">Target on which the member was invoked.</param>
        /// <param name="member">Name of the member that caused the exception.</param>
        /// <param name="args">Arguments to the member.</param>
        /// <param name="exception">A reference to the inner exception.</param>
        public InvokeMemberException(object target, string member, object[] args, Exception exception) :
            base("Member was not found on target.\r\n\r\nTarget=" + target.GetType().FullName + "\r\nMember=" + member + "\r\nArgs=" + InvokeMemberException.GetArgTypes(args), exception)
        {
            this.target = target;
            this.member = member;
            this.args = args;
        }

        /// <summary>
        /// Constructor with a given message and the inner exception.
        /// </summary>
        /// <param name="target">Target on which the member was invoked.</param>
        /// <param name="member">Name of the member that caused the exception.</param>
        /// <param name="args">Arguments to the member.</param>
        /// <param name="message">The message to display to the user.</param>
        /// <param name="exception">A reference to the inner exception.</param>
        public InvokeMemberException(object target, string member, object[] args, string message, Exception exception) :
            base(message, exception)
        {
            this.target = target;
            this.member = member;
            this.args = args;
        }

        /// <summary>
        /// Creates a new instance of this class from the specified serialization information.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Context information.</param>
        protected InvokeMemberException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            else
            {
                this.target = info.GetValue("Target", typeof(object));
                this.member = info.GetString("Member");
                this.args = (object[])info.GetValue("Args", typeof(object[]));
            }
        }

        /// <summary>
        /// Populates the serialization information with current property values.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Context information.</param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            else
            {
                info.AddValue("Target", this.Target, typeof(object));
                info.AddValue("Member", this.Member);
                info.AddValue("Args", this.Args, typeof(object[]));
            }
        }
    }
}
