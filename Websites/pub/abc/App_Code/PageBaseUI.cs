using System;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Computronix.POSSE.Outrider
{
    /// <summary>
    /// Base class for all outrider pages.
    /// </summary>
    public partial class PageBase :
        System.Web.UI.Page
    {
        #region Data Members
        /// <summary>
        /// Provides access to the navigation xml.
        /// </summary>
        protected virtual XmlDocument NavigationDOM
        {
            get
            {
                XmlDocument result = (XmlDocument)this.Cache["NavigationDOM"];

                if (result == null)
                {
                    result = new XmlDocument();
                    this.Cache["NavigationDOM"] = result;
                    result.Load(Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath,
                        ConfigurationManager.AppSettings["SiteNavigationFileName"]));
                }

                return result;
            }
        }

        private string mainMenuName;
        /// <summary>
        /// Provides access to the name of the main menu.  (AppSettings["MainMenuName"])
        /// </summary>
        protected virtual string MainMenuName
        {
            get
            {
                if (string.IsNullOrEmpty(this.mainMenuName))
                {
                    this.mainMenuName = ConfigurationManager.AppSettings["MainMenuName"];
                }

                return this.mainMenuName;
            }
        }

        /// <summary>
        /// Provides access the name of the menu to render.  (Request.QueryString["PosseMenuName"])
        /// </summary>
        protected virtual string MenuName
        {
            get
            {
                return this.Request.QueryString["PosseMenuName"];
            }
        }

        /// <summary>
        ///
        /// </summary>
        protected class MenuEntryPoint
        {
            public string Label;
            public string Url;
            public string Help;
            public MenuInfo SubMenu;
        }

        /// <summary>
        ///
        /// </summary>
        protected class MenuEntryPointGroup
        {
            public string Name;
            public string Label;
            public int ColSpan;
            public bool MultiColumn;
            public bool SameRow;
            public List<MenuEntryPoint> EntryPoints;

            /// <summary>
            /// Displays a count of entrypoints on this entrypoint group and all sub-menus beneath.
            /// </summary>
            /// <returns></returns>
            public int EntryPointCount
            {
                get
                {
                    int count = this.EntryPoints.Count;
                    foreach (MenuEntryPoint item in this.EntryPoints)
                    {
                        if (item.SubMenu != null)
                        {
                            count += item.SubMenu.EntryPointCount;
                        }
                    }
                    return count;
                }
            }

            /// <summary>
            ///
            /// </summary>
            public MenuEntryPointGroup()
            {
                this.EntryPoints = new List<MenuEntryPoint>();
            }
        }

        /// <summary>
        ///
        /// </summary>
        protected class MenuInfo
        {
            public string Name;
            public string Label;
            public string ImageUrl;
            public MenuInfo ParentMenu;
            public string ParentEntryPointGroup;
            public string ParentEntryPoint;
            public List<MenuEntryPointGroup> EntryPointGroups;

            /// <summary>
            /// Displays a count of entrypoints on this menu and all sub-menus beneath this menu.
            /// </summary>
            /// <returns></returns>
            public int EntryPointCount
            {
                get
                {
                    int count = 0;
                    foreach (MenuEntryPointGroup group in this.EntryPointGroups)
                    {
                        count += group.EntryPointCount;
                    }
                    return count;
                }
            }

            /// <summary>
            /// Constructor
            /// </summary>
            public MenuInfo()
                    {
                this.EntryPointGroups = new List<MenuEntryPointGroup>();
                    }
                }

        /// <summary>
        /// Data structure containing the menu information for the current session.
        /// </summary>
        protected Dictionary<string, MenuInfo> Menus
        {
            get
            {
                Dictionary<string, MenuInfo> result = (Dictionary<string, MenuInfo>)this.Cache[this.SessionId + ":Menus"];
                if (result == null)
                {
                    result = new Dictionary<string, MenuInfo>();
                    this.Cache.Add(this.SessionId + ":Menus", result, null, Cache.NoAbsoluteExpiration,
                        new TimeSpan(0, this.CacheExpiration, 0), CacheItemPriority.Default, null);
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access the version header.  (AppSettings["VersionHeader"])
        /// </summary>
        protected virtual string VersionHeader
        {
            get
            {
                return ConfigurationManager.AppSettings["VersionHeader"];
            }
        }

        /// <summary>
        /// Provides access to the presentation name for system information.
        /// (AppSettings["SystemInfoPresentation"])
        /// </summary>
        protected virtual String SystemInfoPresentation
        {
            get
            {
                String systemInfoPresentation;
                try
                {
                    systemInfoPresentation = ConfigurationManager.AppSettings["SystemInfoPresentation"];
                }
                catch (System.Configuration.ConfigurationErrorsException)
                {
                    systemInfoPresentation = "";
                }
                return systemInfoPresentation;
            }
        }

        /// <summary>
        /// Provides access to the query string paramaters used for the system information presentation.
        /// (AppSettings["SystemInfoParameters"])
        /// </summary>
        protected virtual String SystemInfoParameters
        {
            get
            {
                String systemInfoParameters;
                try
                {
                    systemInfoParameters = ConfigurationManager.AppSettings["SystemInfoParameters"];
                }
                catch (System.Configuration.ConfigurationErrorsException)
                {
                    systemInfoParameters = "";
                }
                return systemInfoParameters;
            }
        }

        /// <summary>
        /// Provides access to the cached system info.
        /// </summary>
        protected virtual Dictionary<string, OrderedDictionary> SystemInfo
        {
            get
            {
                Dictionary<string, OrderedDictionary> result = (Dictionary<string, OrderedDictionary>)this.Cache["SystemInfo"];

                if (result == null)
                {
                    result = new Dictionary<string, OrderedDictionary>();
                    this.Cache["SystemInfo"] = result;
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access to the cached ExecuteSql object information
        /// </summary>
        protected virtual Dictionary<string, string> PosseUtilitiesInfo
        {
            get
            {
                Dictionary<string, string> result = (Dictionary<string, string>)this.Cache["PosseUtilitiesInfo"];

                if (result == null)
                {
                    this.StartDialog("PossePresentation=GetPosseUtilitiesObjectId");
                    OrderedDictionary data = this.GetPaneData()[0];

                    result = new Dictionary<string, string>();
                    result["ObjectId"] = data["ObjectId"].ToString();
                    this.StartDialog(String.Format("PossePresentation=GetPaneId&PosseObjectId={0}",
                        result["ObjectId"]));
                    result["PaneId"] = this.PaneId.ToString();

                    this.Cache.Add("PosseUtilitiesInfo", result, null, Cache.NoAbsoluteExpiration,
                        new TimeSpan(0, this.CacheExpiration, 0), CacheItemPriority.Default, null);
                }

                return result;
            }
        }

        /// <summary>
        /// Execute a registered statment (no arguments)
        /// </summary>
        /// <param name="statementName"></param>
        /// <returns></returns>
        protected object ExecuteSql(String statementName)
        {
            return this.ExecuteSql(statementName, null);
        }

        /// <summary>
        /// Execute a registered statment (with arguments)
        /// </summary>
        protected object ExecuteSql(String statementName, String args)
        {
            int paneId;
            List<OrderedDictionary> data;
            string objectId, returnValue;
            StringBuilder xml;
            object result;

            objectId = this.PosseUtilitiesInfo["ObjectId"];
            paneId = Int32.Parse(this.PosseUtilitiesInfo["PaneId"]);

            xml = new StringBuilder();
            xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", objectId);
            xml.AppendFormat("<column name=\"StatementName\">{0}</column>", statementName);
            if (!String.IsNullOrEmpty(args))
            {
                xml.AppendFormat("<column name=\"Arguments\">{0}</column>",
                    HttpUtility.HtmlEncode(args));
            }
            xml.Append("</object>");

            //TO DO: this should do a RoundTripFunction.Refresh, and then retrieve from a record set (instead of a submit and then StartDialog...)
            this.ProcessFunction(objectId, paneId, paneId, RoundTripFunction.Submit,
                "", "", xml.ToString());

            this.StartDialog(string.Format("PosseObjectId={0}&PossePresentation=Results", objectId));
            data = this.GetPaneData(true);

            if (data.Count > 0)
            {
                returnValue = (string)data[0]["Results"];
            }
            else
            {
                returnValue = null;
            }

            //BV: I commented this out as it seems the SignIn page does not have access to RecordSets... (I added the code above instead)
            //data = this.GetData("Results", Int32.Parse(objectId));
            //returnValue = data[0][0].ToString();

            result = cxJSON.JSON.Instance.Parse(returnValue);

            return result;
        }

        /// <summary>
        /// Provides access to the cached band menus.
        /// </summary>
        protected virtual Dictionary<string, Control> BandMenus
        {
            get
            {
                Dictionary<string, Control> result = (Dictionary<string, Control>)this.Cache[this.SessionId + ":BandMenus"];

                if (result == null)
                {
                    result = new Dictionary<string, Control>();
                    this.Cache.Add(this.SessionId + ":BandMenus", result, null, Cache.NoAbsoluteExpiration,
                        new TimeSpan(0, this.CacheExpiration, 0), CacheItemPriority.Default, null);
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access to the cached band resources.
        /// </summary>
        protected virtual Dictionary<string, Control> BandResources
        {
            get
            {
                Dictionary<string, Control> result = (Dictionary<string, Control>)this.Cache[this.SessionId + ":BandResources"];

                if (result == null)
                {
                    result = new Dictionary<string, Control>();
                    this.Cache.Add(this.SessionId + ":BandResources", result, null, Cache.NoAbsoluteExpiration,
                        new TimeSpan(0, this.CacheExpiration, 0), CacheItemPriority.Default, null);
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access to the cached pane menus.
        /// </summary>
        protected virtual Dictionary<string, Control> PaneMenus
        {
            get
            {
                Dictionary<string, Control> result = (Dictionary<string, Control>)this.Cache[this.SessionId + ":PaneMenus"];

                if (result == null)
                {
                    result = new Dictionary<string, Control>();
                    this.Cache.Add(this.SessionId + ":PaneMenus", result, null, Cache.NoAbsoluteExpiration,
                        new TimeSpan(0, this.CacheExpiration, 0), CacheItemPriority.Default, null);
                }

                return result;
            }
        }


        /// <summary>
        /// The number of minutes the menu cache should expire in.
        /// </summary>
        protected virtual int CacheExpiration
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["CacheExpiration"]);
            }
        }


        private bool showAsterisk = false;
        /// <summary>
        /// Provides access to the ShowAsterisk condition.
        /// </summary>
        protected virtual bool ShowAsterisk
        {
            get
            {
                return this.showAsterisk;
            }
            set
            {
                this.showAsterisk = value;
            }
        }

        private bool disableCancel = false;
        /// <summary>
        /// Provides access to the DisableCancel condition.
        /// </summary>
        protected virtual bool DisableCancel
        {
            get
            {
                return this.disableCancel;
            }
            set
            {
                this.disableCancel = value;
            }
        }

        private bool filteredList = false;
        /// <summary>
        /// Provides access to the FilteredList condition.
        /// </summary>
        protected virtual bool FilteredList
        {
            get
            {
                return this.filteredList;
            }
            set
            {
                this.filteredList = value;
            }
        }

        private bool saveAsExcel = false;
        /// <summary>
        /// Provides access to the SaveAsExcel condition.
        /// </summary>
        protected virtual bool SaveAsExcel
        {
            get
            {
                return this.saveAsExcel;
            }
            set
            {
                this.saveAsExcel = value;
            }
        }

        private bool saveAsExcelReport = false;
        /// <summary>
        /// Provides access to the SaveAsExcelReport condition.
        /// </summary>
        protected virtual bool SaveAsExcelReport
        {
            get
            {
                return this.saveAsExcelReport;
            }
            set
            {
                this.saveAsExcelReport = value;
            }
        }

        private bool saveAsCSVReport = false;
        /// <summary>
        /// Provides access to the saveAsCSVReport condition.
        /// </summary>
        protected virtual bool SaveAsCSVReport
        {
            get
            {
                return this.saveAsCSVReport;
            }
            set
            {
                this.saveAsCSVReport = value;
            }
        }

        private bool saveIsNext = false;
        /// <summary>
        /// Provides access to the SaveIsNext condition.
        /// </summary>
        protected virtual bool SaveIsNext
        {
            get
            {
                return this.saveIsNext;
            }
            set
            {
                this.saveIsNext = value;
            }
        }

        private bool submitOnClose = false;
        /// <summary>
        /// Provides access to the SubmitOnClose condition.
        /// </summary>
        protected virtual bool SubmitOnClose
        {
            get
            {
                return this.submitOnClose;
            }
            set
            {
                this.submitOnClose = value;
            }
        }

        private bool validateGuestSession = false;
        /// <summary>
        /// Provides access to the ValidateGuestSession condition.
        /// </summary>
        protected virtual bool ValidateGuest
        {
            get
            {
                return this.validateGuestSession;
            }
            set
            {
                this.validateGuestSession = value;
            }
        }

        /// <summary>
        /// Provides access to the directory where the SiteBusy log files should be created.  (AppSettings["SiteBusyLogDirectory"])
        /// </summary>
        protected virtual string SiteBusyLogDirectory
        {
            get
            {
                return ConfigurationManager.AppSettings["SiteBusyLogDirectory"];
            }
        }

        /// <summary>
        /// Provides access the version header.  (AppSettings["VersionHeader"])
        /// </summary>
        protected virtual string IconPath
        {
            get
            {
                return ConfigurationManager.AppSettings["IconPath"];
            }
        }

        /// <summary>
        /// Provides access to the cached icon images.
        /// </summary>
        protected virtual Dictionary<string, string> IconImages
        {
            get
            {
                Dictionary<string, string> result = (Dictionary<string, string>)this.Cache["IconImages"];

                if (result == null)
                {
                    result = new Dictionary<string, string>();
                    this.Cache["IconImages"] = result;
                }

                return result;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Renders the tab labels and adds them to the specified container.
        /// </summary>
        /// <param name="paneId">A reference to the pane Id.</param>
        /// <param name="paneName">A reference to the pane Name.</param>
        /// <param name="label">A reference to the tab label.</param>
        /// <param name="enabled">A reference to the enabled flag for the tab.</param>
        /// <param name="condition">A reference to the conditon for the tab.</param>
        /// <param name="isJobPane">Indicates if this is a pane from a job being displayed on a process.</param>
        protected virtual TabLabelBase RenderTab(int paneId, string paneName, string label,
            bool enabled, string condition, bool isJobPane)
        {
            TabLabelBase tabLabel;

            if (paneId == this.CurrentPaneId)
            {
                // Render the selected tab.
                tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.SelectedTabLabelUrl);
                tabLabel.Selected = true;
                tabLabel.Enabled = true;
            }
            else
            {
                // Render non-selected tabs.
                if (enabled)
                {
                    tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.TabLabelUrl);

                    if (isJobPane)
                    {
                        tabLabel.NavigationUrl = this.GetJobTabAction(paneName);
                    }
                    else
                    {
                        tabLabel.NavigationUrl = this.GetTabAction(paneId.ToString());
                    }
                    tabLabel.Enabled = true;
                }
                else
                {
                    tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.DisabledTabLabelUrl);
                    tabLabel.Enabled = false;
                }
                tabLabel.Selected = false;
            }

            tabLabel.Text = label;
            tabLabel.Condition = condition;

            return tabLabel;
        }

        /// <summary>
        /// Renders the tab labels and adds them to the specified container.
        /// </summary>
        /// <param name="paneId">A reference to the pane Id.</param>
        /// <param name="paneName">A reference to the pane Name.</param>
        /// <param name="label">A reference to the tab label.</param>
        /// <param name="enabled">A reference to the enabled flag for the tab.</param>
        /// <param name="isJobPane">Indicates if this is a pane from a job being displayed on a process.</param>
        protected virtual TabLabelBase RenderWarningTab(int paneId, string paneName, string label,
            bool enabled, string condition, bool isJobPane)
        {
            TabLabelBase tabLabel;

            if (paneId == this.CurrentPaneId)
            {
                // Render the selected tab.
                tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.SelectedWarningTabLabelUrl);
                tabLabel.Selected = true;
                tabLabel.Enabled = true;
            }
            else
            {
                // Render non-selected tabs.
                if (enabled)
                {
                    tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.WarningTabLabelUrl);

                    if (isJobPane)
                    {
                        tabLabel.NavigationUrl = this.GetJobTabAction(paneName);
                    }
                    else
                    {
                        tabLabel.NavigationUrl = this.GetTabAction(paneId.ToString());
                    }
                    tabLabel.Enabled = true;
                }
                else
                {
                    tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.DisabledTabLabelUrl);
                    tabLabel.Enabled = false;
                }
                tabLabel.Selected = false;
            }

            tabLabel.Text = label;
            tabLabel.Condition = condition;

            return tabLabel;
        }

        /// <summary>
        /// Renders the tab labels and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderTabLabelBand(WebControl container)
        {
            Panel panelContainer;
            TabLabelBase tabLabel;
            int index, tabCount;

            if (container != null)
            {
                switch (this.PresentationType)
                {
                    case PresType.Tabs:
                        tabCount = this.Panes.Count;
                        if (this.JobPanes != null)
                            tabCount += this.JobPanes.Count;

                        if (tabCount > 0)
                        {
                            panelContainer = new Panel();
                            panelContainer.CssClass = "tablabelband";

                            index = 0;

                            if (tabCount == 1 && string.IsNullOrEmpty(this.headerPaneHtml))
                            {
                                //remove the line below the tabs, as there will be no tabs in this case
                                panelContainer.Style.Add("border-bottom", "none");
                            }
                            else
                            {
                                foreach (VisiblePaneInfo pane in this.Panes)
                                {
                                    if (this.GetCondition(pane.PaneId, "Warning"))
                                    {
                                        tabLabel = this.RenderWarningTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                    }
                                    else
                                    {
                                    tabLabel = this.RenderTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                    }
                                    tabLabel.Index = index;
                                    tabLabel.Count = tabCount;

                                    panelContainer.Controls.Add(tabLabel);

                                    index = index + 1;
                                }

                                // Load the job presentation tabs if present
                                if (this.JobPanes != null)
                                {
                                    foreach (VisiblePaneInfo pane in this.JobPanes)
                                    {
                                        if (this.GetCondition(pane.PaneId, "Warning"))
                                        {
                                            tabLabel = this.RenderWarningTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                        }
                                        else
                                        {
                                            tabLabel = this.RenderTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                        }
                                        tabLabel.Index = index;
                                        tabLabel.Count = tabCount;

                                        panelContainer.Controls.Add(tabLabel);

                                        index = index + 1;
                                    }
                                }
                            }
                            container.Controls.Add(panelContainer);
                        }
                        break;
                    case PresType.Wizard:

                        panelContainer = new Panel();
                        panelContainer.CssClass = "tablabelband";

                        tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.SelectedTabLabelUrl);
                        tabLabel.Selected = true;
                        tabLabel.Enabled = true;
                        tabLabel.Text = this.PaneLabel;
                        tabLabel.Index = 0;
                        tabLabel.Count = 1;

                        panelContainer.Controls.Add(tabLabel);
                        container.Controls.Add(panelContainer);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Renders the functions for the top of the page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderTopFunctionBand(WebControl container)
        {
            HtmlTable table;
            HtmlTableRow row;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();

                if (this.PresentationType == PresType.Wizard)
                {
                    this.RenderFunctionLink(row, "Back", RoundTripFunction.Previous);
                    this.RenderFunctionLink(row, "Next", RoundTripFunction.Next);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Submit]))
                {
                    this.RenderFunctionLink(row, "Save", RoundTripFunction.Submit);
                }

                if (this.SaveAsExcel)
                {
                    string excelUrl = string.Format("{0}?{1}", this.ExcelDownloadUrl, this.Request.QueryString);
                    string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                        excelUrl, (int)RoundTripFunction.Refresh, this.PaneId);
                    this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                }

                if (this.SaveAsExcelReport)
                {
                    int excelPaneId = this.GetPaneIdFromName("Excel", this.PaneId);
                    string excelUrl = string.Format("{0}?{1}&SaveAsExcelReport=Y", this.ExcelDownloadUrl, this.Request.QueryString);
                    string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                        excelUrl, (int)RoundTripFunction.Refresh, excelPaneId);
                    this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                }

                if (this.SaveAsCSVReport)
                {
                    int excelPaneId = this.GetPaneIdFromName("Excel", this.PaneId);
                    string excelUrl = string.Format("{0}?{1}&SaveAsCSVReport=Y", this.ExcelDownloadUrl, this.Request.QueryString);
                    string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                        excelUrl, (int)RoundTripFunction.Refresh, excelPaneId);
                    this.RenderFunctionLink(row, "Save as CSV", navigationUrl);
                }

                if (this.SaveAsExcelOnSearches)
                {
                    string excelUrl = string.Format("{0}?{1}", this.ExcelDownloadUrl, this.Request.QueryString);
                    string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                        excelUrl, (int)RoundTripFunction.PerformSearch, this.PaneId);
                    this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]))
                {
                    defaultFunction = this.RenderFunctionLink(row, "Search", RoundTripFunction.PerformSearch);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Search]))
                {
                    this.RenderFunctionLink(row, "Search Again", RoundTripFunction.Search);
                }

                if (!this.DisableCancel)
                {
                    this.RenderFunctionLink(row, "Cancel", this.HomePageUrl);
                }

                table = new HtmlTable();
                table.Controls.Add(row);
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 0;

                container.Controls.Add(table);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" +
                        defaultFunction.ClientID + "')";
                }
            }
        }

        /// <summary>
        /// Renders the functions for the bottom of the page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderBottomFunctionBand(WebControl container)
        {
            // If we have a tab presentation style but no actual tabs to display,
            // then don't show the bottom function band
            if (this.PresentationType == PresType.Tabs)
            {
                int tabCount = this.Panes.Count;
                if (this.JobPanes != null)
                    tabCount += this.JobPanes.Count;
                if (tabCount == 0)
                    return;
            }

                this.RenderTopFunctionBand(container);
            }

        /// <summary>
        /// Renders the functions for the lookup popup page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderLookupFunctionBand(WebControl container)
        {
            HtmlTable table;
            HtmlTableRow row;
            HtmlTableCell cell;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();
                cell = new HtmlTableCell();
                row.Cells.Add(cell);

                defaultFunction = this.RenderFunctionLink(row, "Search", RoundTripFunction.PerformSearch);
                this.RenderFunctionLink(row, "Cancel", "javascript:window.close();");
                this.RenderFunctionLink(row, "Cancel and Clear", "javascript:PosseLookupDelete(); close();");

                table = new HtmlTable();
                table.Controls.Add(row);
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 0;
                container.Controls.Add(table);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" +
                        defaultFunction.ClientID + "')";
                }
            }
        }

        /// <summary>
        /// Renders the functions for a popup page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderPopupFunctionBand(WebControl container)
        {
            this.RenderPopupFunctionBand(container, "Cancel");
        }

        /// <summary>
        /// Renders the functions for a popup page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="label">Label for the close button.</param>
        protected virtual void RenderPopupFunctionBand(WebControl container, string label)
        {
            HtmlTable table;
            HtmlTableRow row;
            HtmlTableCell cell;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();
                cell = new HtmlTableCell();
                row.Cells.Add(cell);

                if (label != "Close")
                {
                    defaultFunction = this.RenderFunctionLink(row, "Save", RoundTripFunction.Submit);
                }

                this.RenderFunctionLink(row, label, "javascript:window.close();");

                table = new HtmlTable();
                table.Controls.Add(row);
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 0;
                container.Controls.Add(table);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" +
                        defaultFunction.ClientID + "')";
                }
            }
        }

        /// <summary>
        /// Renders the functions for the Save and Return page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="url">The url to navigate to after submitting.</param>
        protected virtual void RenderSaveAndReturnFunctionBand(WebControl container, string cancelUrl)
        {
            HtmlTableRow row;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();

                defaultFunction = this.RenderFunctionLink(row, "Save", RoundTripFunction.Submit);

                string saveReturnUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}?SaveReturnButton=Y', {1}, {2});", this.HomePageUrl, 2, this.paneId);
                this.RenderFunctionLink(row, "Save & Return", saveReturnUrl);
                this.RenderFunctionLink(row, "Cancel", cancelUrl);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" +
                      defaultFunction.ClientID + "')";
                }
            }
        }

        /// <summary>
        /// Renders the functions for the select objects popup page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderSelectObjectsFunctionBand(WebControl container)
        {
            HtmlTableRow row;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();

                if (this.HasFunction(RoundTripFunction.Search))
                {
                    this.RenderFunctionLink(row, "Select", string.Format(
                        "javascript:PosseNavigate('{0}&Action=Submit');", this.HomePageUrl));
                    this.RenderFunctionLink(row, "Clear All", string.Format(
                      "javascript:setAllCheckboxes('N');", this.HomePageUrl));
                    this.RenderFunctionLink(row, "Check All", string.Format(
                      "javascript:setAllCheckboxes('Y');", this.HomePageUrl));
                }
                defaultFunction = this.RenderFunctionLink(row, "Search", RoundTripFunction.PerformSearch);
                this.RenderFunctionLink(row, "Refine Search", RoundTripFunction.Search);

                HtmlTable table = new HtmlTable();
                table.Controls.Add(row);
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 0;

                container.Controls.Add(table);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" + defaultFunction.ClientID + "')";
                }
            }
        }

        /// <summary>
        /// Renders a function and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="text">The label to render for the function</param>
        /// <param name="functionType">The function type for the function</param>
        /// <remarks></remarks>
        protected virtual FunctionLinkBase RenderFunctionLink(Control container, string text,
            RoundTripFunction functionType)
        {
            HtmlTableCell cell;
            FunctionLinkBase functionLink = null;
            string navigationUrl = this.GetFunctionHref(functionType);

            if (!string.IsNullOrEmpty(navigationUrl))
            {
                functionLink = (FunctionLinkBase)TemplateControl.LoadControl(this.FunctionLinkUrl);
                functionLink.NavigationUrl = navigationUrl;
                functionLink.Enabled = true;

                functionLink.Text = text;
                functionLink.FunctionType = functionType;

                cell = new HtmlTableCell();
                cell.Controls.Add(functionLink);
                container.Controls.Add(cell);
            }
            else
            {
                functionLink = (FunctionLinkBase)TemplateControl.LoadControl(this.DisabledFunctionLinkUrl);
                functionLink.NavigationUrl = "";
                functionLink.Enabled = false;

                functionLink.Text = text;
                functionLink.FunctionType = functionType;

                cell = new HtmlTableCell();
                cell.Controls.Add(functionLink);
                container.Controls.Add(cell);
            }

            return functionLink;
        }

        /// <summary>
        /// Renders a function and adds it to the specified container. This version renders a specific
        /// url rather than one of the function types
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="text">Th elabel to render for the function</param>
        /// <param name="navigation">The url for the function</param>
        /// <remarks>Use this version to render a function link that is not one of the base
        /// function types.</remarks>
        protected virtual void RenderFunctionLink(Control container, string text, string navigationUrl)
        {
            HtmlTableCell cell;
            FunctionLinkBase functionLink;

            if (!string.IsNullOrEmpty(navigationUrl))
            {
                functionLink = (FunctionLinkBase)TemplateControl.LoadControl(this.FunctionLinkUrl);
                functionLink.NavigationUrl = navigationUrl;
                functionLink.Enabled = true;
                functionLink.Text = text;

                cell = new HtmlTableCell();
                cell.Controls.Add(functionLink);

                container.Controls.Add(cell);
            }
        }

        /// <summary>
        /// Returns a javascript href for the specified function type.
        /// </summary>
        /// <param name="functionType">The function type to construct the href for.</param>
        /// <returns>A javascript href for the function type.</returns>
        protected virtual string GetFunctionHref(RoundTripFunction functionType)
        {
            string result = null, paneId, function, script = null, objectHandle, iconImageName = null;

            if ((int)functionType <= this.PaneFunctions.Count - 1)
            {
                paneId = this.PaneFunctions[(int)functionType];

                if (!string.IsNullOrEmpty(paneId))
                {
                    if (this.SaveIsNext && functionType == RoundTripFunction.Next)
                    {
                        function = ((int)RoundTripFunction.Submit).ToString();
                        objectHandle = this.ObjectId;

                        if (string.IsNullOrEmpty(objectHandle))
                        {
                            objectHandle = "N0";
                        }

                        script = string.Format("PosseChangeColumn('{0}', 'NavigateToSpecificWizardPane', 'Y');", objectHandle);
                    }
                    else
                    {
                        function = ((int)functionType).ToString();
                    }

                    if (functionType == RoundTripFunction.PerformSearch || functionType == RoundTripFunction.Search)
                    {
                        if (Request.QueryString["IconName"] != null)
                            iconImageName = string.Format("&IconName={0}", Request.QueryString["IconName"]);
                    }

                    result = string.Format("javascript:{0}PosseSubmitLinkReturn('{1}{2}', {3}, {4})",
                        script, this.CurrentUrl, iconImageName, function, paneId);
                }
            }

            return result;
        }

        /// <summary>
        /// Adds the rendered header pane html to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderHeaderPane(WebControl container, WebControl topContainer)
        {
            Literal html;

            if (container != null)
            {
                if (!string.IsNullOrEmpty(this.HeaderPaneHtml))
                {
                    html = new Literal();
                    html.Text = this.HeaderPaneHtml;
                    container.Controls.Add(html);
                }
                else
                {
                    // so that the border isn't visible
                    container.Visible = false;
                }
            }
        }

        /// <summary>
        /// Adds the rendered presentation html to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderPane(WebControl container, WebControl messageContainer)
        {
            Literal html;
            Label label;

            if (container != null)
            {
                if (!string.IsNullOrEmpty(this.PaneHtml))
                {
                    html = new Literal();
                    html.Text = this.PaneHtml;

                    container.Controls.Add(html);

                    if (this.PaneName == "JobSummary" || this.PaneName == "Processes")
                    {
                        this.RenderPaneInsertChoices(container, "Processes", "InsertProcess");
                    }
                }

                if (messageContainer != null)
                {
                    if (this.ShowAsterisk)
                    {
                        label = new Label();
                        label.Text = "* An asterisk indicates a required field.";
                        label.CssClass = "posserequired";
                        messageContainer.Controls.Add(label);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the rendered insert choices to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="paneName"></param>
        /// <param name="menuName"></param>
        protected virtual void RenderPaneInsertChoices(WebControl container,
            string paneName, string menuName)
        {
            List<InsertChoice> insertChoices;
            string objectDefName, objectDefDescription, endPointName,
                presentationName, relationshipLabel, description, html = null, url = null, xml;
            bool canCreateNewObject;
            Literal htmlLiteral;

            insertChoices = this.InsertChoices[paneName];

            if (insertChoices.Count > 0)
            {
                html = string.Format("<div onMouseOver=\"stopTimer(timerID);\" onMouseOut=\"timerID=startTimer();\" id=\"{0}\" style=\"z-index: 100; visibility: hidden; position: absolute; top: -999px; left: -999px;\">", menuName);
                html += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";

                foreach (InsertChoice choice in insertChoices)
                {
                    objectDefName = choice.ObjectDefName;
                    objectDefDescription = choice.ObjectDefDescription;
                    presentationName = choice.PresentationName;
                    canCreateNewObject = choice.CanCreateNewObject;
                    endPointName = choice.EndPointName;
                    relationshipLabel = choice.RelationshipLabel;

                    if (canCreateNewObject)
                    {
                        url = null;

                        if (string.IsNullOrEmpty(presentationName))
                        {
                            if (paneName == "Processes")
                            {
                                xml = string.Format(
                                    "<object id=\"NEW0\" action=\"Insert\" objectdef=\"{0}\" jobid=\"{1}\" />",
                                    objectDefName, this.ObjectId);
                            }
                            else if (string.IsNullOrEmpty(endPointName))
                            {
                                xml = string.Format(
                                    "<object id=\"NEW0\" action=\"Insert\" objectdef=\"{0}\" />", objectDefName);
                            }
                            else
                            {
                                xml = string.Format(
                                    "<object id=\"NEW0\" action=\"Insert\" objectdef=\"{0}\" />", objectDefName);
                                xml += string.Format("<object id=\"{0}\" action=\"Update\">", this.ObjectId);
                                xml += string.Format("<relationship id=\"NEW1\" action=\"Insert\" " +
                                    "endpoint=\"{0}\" toobjectid=\"NEW0\"/>", endPointName);
                                xml += "</object>";
                            }

                            url = string.Format(
                                "javascript:PosseAppendChangesXML(unescape('{0}')); PosseNavigate(null);",
                                this.Server.HtmlEncode(xml));
                        }
                        else if (presentationName == "Default")
                        {
                            url = string.Format("{0}?PossePresentation={1}&PosseObjectDef={2}",
                                this.HomePageUrl, presentationName, objectDefName);

                            if (paneName == "Processes")
                            {
                                url += string.Format("&JobId={0}", this.ObjectId);
                            }
                            else if (!string.IsNullOrEmpty(endPointName))
                            {
                                url += string.Format("&PosseFromObjectId={0}&PosseEndPoint={1}",
                                    this.ObjectId, endPointName);
                            }
                        }

                        if (!string.IsNullOrEmpty(url))
                        {
                            description = relationshipLabel;

                            if (string.IsNullOrEmpty(description))
                            {
                                description = objectDefDescription;
                            }

                            html += string.Format("<tr><td class=\"InsertProcesses\"><a class=\"possegrid\" href=\"{0}\">{1}</a></td></tr>",
                                url, description);
                        }
                    }
                }

                html += "</table></div>";
            }

            if (container != null)
            {
                htmlLiteral = new Literal();
                htmlLiteral.Text = html;

                container.Controls.Add(htmlLiteral);
            }
        }

        /// <summary>
        /// Returns null for DBNull values.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <returns>Null if value is DBNull otherwise value.</returns>
        protected virtual object CheckDbNull(object value)
        {
            object result = value;

            if (value == DBNull.Value)
            {
                result = null;
            }

            return result;
        }

        /// <summary>
        /// Adds the rendered search presentation html to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderSearch(WebControl container)
        {
            Literal html;

            if (container != null)
            {
                html = new Literal();
                html.Text = this.SearchHtml;

                container.Controls.Add(html);
            }
        }

        /// <summary>
        /// Adds the rendered results presentation html to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderResults(WebControl container)
        {
            Literal html;

            if (container != null)
            {
                html = new Literal();
                html.Text = this.PaneHtml;

                container.Controls.Add(html);
            }
        }

        /// <summary>
        /// Renders the greeting presentation and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderGreeting(WebControl container)
        {
            Literal html;

            if (container != null)
            {
                html = new Literal();

                if (!this.IsGuest)
                {
                    html.Text = string.Format("<h3>Welcome, {0}</h3>", this.UserName);
                }
                else
                {
                    html.Text = "<h3>Welcome</h3>";
                }

                container.Controls.Add(html);
            }
        }

        /// <summary>
        /// Renders the user name and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderUserName(WebControl container, HyperLink logoutLink)
        {
            Literal html;
            string organizationName;

            if (container != null)
            {
                html = new Literal();

                if (!this.IsGuest)
                {
                    html.Text = "<div class=\"noprint\">";
                    html.Text += this.UserName;

                    if ((string)this.UserInformation[0]["organizationname"] != null)
                    {
                        organizationName = (string)this.UserInformation[0]["organizationname"];

                        if (!string.IsNullOrEmpty(organizationName))
                        {
                            html.Text += string.Format("<br>{0}", this.Server.HtmlEncode(organizationName));
                        }
                    }

                    html.Text += "</div>";
                }

                container.Controls.Add(html);

                logoutLink.Text = "Sign Out";
                logoutLink.NavigateUrl = this.LogoutPageUrl;
            }
        }

        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="text">Title text to be displayed.</param>
        protected virtual void RenderUserInfo(WebControl container)
        {
            Label label;
            Panel panel = new Panel();

            if (container != null)
            {
            if (this.SessionRequired && this.SessionId != this.NoSession && !this.IsGuest)
            {

                label = new Label();
                label.CssClass = "welcomemessage"; //change to username and make it something good
			    label.Text = string.Format("Welcome {0}", Server.HtmlEncode((string)this.UserInformation[0]["dup_FormattedName2"].ToString()));
                panel.Controls.Add(label);
                if (!string.IsNullOrEmpty(this.VersionHeader))
                {

                    label = new Label();
                    label.CssClass = "versionheader";
                    label.Text = String.Format("{0}", this.VersionHeader);
                    panel.Controls.Add(label);
                }
            }

            container.Controls.Add(panel);
            }
        }

        /// <summary>
        /// Renders the debug link presentation and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderDebugLink(WebControl container)
        {

            string screenId = "0";
            Label label;
            HtmlTable table = new HtmlTable();
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell cell;

            if (container != null)
            {
                cell = new HtmlTableCell();
                cell.Width = "100%";
                cell.Attributes.Add("align", "right");

                if (this.HasPresentation)
                {
                    screenId = this.PaneId.ToString();
                }
                else
                {
                    screenId = "1000000";
                }

                label = new Label();
                label.Text = string.Format("Screen ID: {0}", screenId);
                label.CssClass = "screenid";
                label.Attributes.Add("onclick", "PosseToggleDebugKey()");
                cell.Controls.Add(label);

                row.Cells.Add(cell);
                table.Rows.Add(row);
                table.CellSpacing = 0;
                table.Attributes.Add("width", "100%");

                container.Controls.Add(table);
            }
        }

        /// <summary>
        /// Retrieves the current note and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderNote(WebControl container)
        {
            Literal html;

            if (container != null)
            {
                this.CheckClient();
                html = new Literal();
                html.Text = this.Note;

                container.Controls.Add(html);
            }
        }

        /// <summary>
        /// Load the menu data structures for the current user session.
        /// </summary>
        /// <param name="menuName">The name of the menu to load.</param>
        protected virtual void LoadMenu(string menuName)
        {
            XmlAttribute attribute;
            XmlNode menuNode, parentNode;
            MenuInfo menu;
            List<EntryPoint> entryPoints = null;

            if (!this.Menus.ContainsKey(menuName))
            {
                menuNode = this.NavigationDOM.DocumentElement.SelectSingleNode(
                    string.Format("//Menu[@name='{0}']", menuName));
                menu = new MenuInfo();
                menu.Name = menuName;
                attribute = menuNode.Attributes["label"];
                if (attribute != null)
                    menu.Label = menuNode.Attributes["label"].Value;
                attribute = menuNode.Attributes["imageurl"];
                if (attribute != null)
                    menu.ImageUrl = menuNode.Attributes["imageurl"].Value;
                this.Menus[menuName] = menu;

                // Populate the entry point groups
                foreach (XmlNode node in menuNode.ChildNodes)
                {
                    if (node.Name.ToLower() == "entrypointgroup")
                    {
                        MenuEntryPointGroup group = new MenuEntryPointGroup();
                        group.Name = node.Attributes["name"].Value;
                        attribute = node.Attributes["label"];
                        if (attribute != null)
                            group.Label = node.Attributes["label"].Value;
                        attribute = node.Attributes["colspan"];
                        if (attribute != null)
                            group.ColSpan = int.Parse(node.Attributes["colspan"].Value);
                        attribute = node.Attributes["multicolumn"];
                        if (attribute != null)
                            group.MultiColumn = bool.Parse(node.Attributes["multicolumn"].Value);
                        attribute = node.Attributes["samerow"];
                        if (attribute != null)
                            group.SameRow = bool.Parse(node.Attributes["samerow"].Value);
                        menu.EntryPointGroups.Add(group);

                        // Determine if there are sub-menus linked to entry points
                        Dictionary<string, string> subMenus = new Dictionary<string,string>();
                        foreach (XmlNode childNode in node.ChildNodes)
                        {
                            if (childNode.Name.ToLower() == "menu")
                            {
                                subMenus[childNode.Attributes["parententrypoint"].Value] = childNode.Attributes["name"].Value;
                            }
                        }

                        // Populate the entry points
                        entryPoints = this.GetSiteNavigation(group.Name, null);
                        if (entryPoints != null)
                        {
                            foreach(EntryPoint ep in entryPoints)
                            {
                                MenuEntryPoint entrypoint = new MenuEntryPoint();
                                entrypoint.Label = ep.Label;
                                entrypoint.Url = ep.Url;
                                entrypoint.Help = ep.Help;

                                // Process a linked sub menu if any
                                if (subMenus.ContainsKey(entrypoint.Label))
                                {
                                    LoadMenu(subMenus[entrypoint.Label]);
                                    entrypoint.SubMenu = this.Menus[subMenus[entrypoint.Label]];
                                }

                                group.EntryPoints.Add(entrypoint);
                            }
                        }

                    }
                }

                // Add a link to the parent menu as well
                parentNode = menuNode.ParentNode;
                if (parentNode.Name.ToLower() == "entrypointgroup")
                {
                    menu.ParentEntryPoint = menuNode.Attributes["parententrypoint"].Value;
                    menu.ParentEntryPointGroup = parentNode.Attributes["name"].Value;
                    if (parentNode.ParentNode.Name.ToLower() == "menu")
                    {
                        LoadMenu(parentNode.ParentNode.Attributes["name"].Value);
                        menu.ParentMenu = this.Menus[parentNode.ParentNode.Attributes["name"].Value];
                    }
                    else
                    {
                        throw new Exception(String.Format("EntryPointGroup {0} does not have a parent Menu tag.", menu.ParentEntryPointGroup));
                    }
                }
            }
        }

        /// <summary>
        /// Renders the current menu and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderMenuBand(WebControl container)
        {
            string url;
            XmlNode menu;
            string menuName = this.MainMenuName;
            int count = 0;

            if (container != null)
            {
                Literal litMenu = new Literal();
                litMenu.Text = "<div class='nav'>";
                container.Controls.Add(litMenu);

                menu = this.NavigationDOM.DocumentElement.SelectSingleNode(
                    string.Format("//Menu[@name='{0}']", menuName));

                if (menu != null)
                {
                    LoadMenu(menuName);

                    //add the cached menu to the container
                    if (this.BandMenus.ContainsKey(menuName))
                    {
                        container.Controls.Add(this.BandMenus[menuName]);
						this.OutriderNet.LogMessage(LogLevel.Error, string.Format("Using saved menu structure. Name={0}",menuName));
                        litMenu = new Literal();
                        litMenu.Text = "</div>";
                        container.Controls.Add(litMenu);
                    }
                    //discover the current menu
                    else
                    {
                        litMenu = new Literal();
						this.OutriderNet.LogMessage(LogLevel.Error, string.Format("Creating menu structure. Name={0}",menuName));
                        litMenu.Text = "<ul id='navigation'>";
                        container.Controls.Add(litMenu);

                        foreach (MenuEntryPointGroup group in this.Menus[menuName].EntryPointGroups)
                        {
                            foreach (MenuEntryPoint item in group.EntryPoints)
                            {
                                if (count > 0)
                                {
                                    litMenu = new Literal();
                                    litMenu.Text = "<span class=\"nav-spacer\">|</span>";
                                    container.Controls.Add(litMenu);

                                }
                                count = count + 1;
                                if (item.SubMenu != null)
                                    url = String.Format("{0}?PosseMenuName={1}", this.homePageUrl, item.SubMenu.Name);
                                else
                                    url = item.Url;
                                this.RenderMenuItem(container, url, item.Help, item.Label);

                            }

                        }
                        litMenu = new Literal();
                        litMenu.Text = "</ul></div>";
                        container.Controls.Add(litMenu);
						this.OutriderNet.LogMessage(LogLevel.Error, string.Format("Saving menu structure. Name={0}",menuName));
                        this.BandMenus[menuName] = container;
                    }//end menu discovery
                }
            }
        }

        /// <summary>
        /// Renders the resources and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderResourceBand(WebControl container)
        {
            Label label;
            Panel panel;

            if (container != null)
            {
                if (!string.IsNullOrEmpty(this.MenuName))
                {
                    if (this.BandResources.ContainsKey(this.MenuName))
                    {
                        container.Controls.Add(this.BandResources[this.MenuName]);
                    }
                    else
                    {
                        panel = new Panel();
                        panel.Style.Add("background-color", "#CCCCCC");
                        panel.Style.Add("border-left", "1px solid #999999");
                        panel.Style.Add("border-right", "1px solid #999999");
                        panel.Style.Add("border-top", "1px solid #999999");
                        panel.Style.Add("padding-top", "10px");
                        panel.Style.Add("padding-left", "15px");
                        panel.Style.Add("padding-bottom", "15px");

                        label = new Label();
                        label.CssClass = "menubarheader";
                        label.Text = "Resources";

                        panel.Controls.Add(label);

                        label = new Label();
                        label.CssClass = "username";
                        label.Text = string.Format("({0})", this.UserName);

                        panel.Controls.Add(label);

                        this.BandResources[this.MenuName] = panel;
                        container.Controls.Add(panel);
                    }
                }
            }
        }

        /// <summary>
        /// Renders the error message and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected virtual void RenderErrorMessage(WebControl container)
        {
            Literal literal;
            string errorMessage;

            if (container != null && !string.IsNullOrEmpty(this.ErrorMessage))
            {
                // ErrorMessage has a settr which encodes, so we can't set
                // it again here otherwise it would be encoded twice.  So
                // we use a local.
                errorMessage = this.ErrorMessage;
                if (errorMessage.StartsWith("Button Click failed. "))
                {
                    errorMessage = errorMessage.Substring(21);
                }

                if (errorMessage.StartsWith("Submit failed. "))
                {
                    errorMessage = errorMessage.Substring(15);
                }

                literal = new Literal();
                literal.Text = string.Format("<table style=\"border: 3px solid red; padding: 5px; margin-bottom: 5px; vertical-align: top; width: 94%\"><tr><td valign=\"top\"><img id=\"errorImage\" src=\"images/icon_error.gif\"/></td><td class=\"posseerror\" width=\"100%\">{0}</td></tr></table>", errorMessage);

                container.Controls.Add(literal);
                this.ClientScript.RegisterStartupScript(this.GetType(), "SetErrorFocus", "errorFocus = true;", true);

            }
        }

        /// <summary>
        /// Retrieves the current report and writes it to the output stream.
        /// </summary>
        protected virtual void RenderReport()
        {
            if (this.Report != null)
            {
                this.CheckClient();
                this.Response.Clear();

                this.Response.AddHeader("Content-Disposition", "filename=report.pdf");
                this.Response.AddHeader("Content-Length", this.Report.Length.ToString());
                this.Response.ContentType = "application/pdf";

                if (this.Report.Length > 0)
                {
                    this.Response.BinaryWrite(this.Report);
                }

                this.Response.End();
            }
        }

        /// <summary>
        /// Retrieves the current document and writes it to the output stream.
        /// </summary>
        protected virtual void RenderDocument()
        {
            string contentType;

            if (this.Document != null)
            {
                this.CheckClient();
                this.Response.Clear();

                switch (this.DocumentExtension.ToLower())
                {
                    case "asf":
                        contentType = "video/x-ms-asf";
                        break;
                    case "avi":
                        contentType = "video/avi";
                        break;
                    case "doc":
                        contentType = "application/msword";
                        break;
                    case "gif":
                        contentType = "image/gif";
                        break;
                    case "jpg":
                    case "jpeg":
                        contentType = "image/jpeg";
                        break;
                    case "mp3":
                        contentType = "audio/mpeg3";
                        break;
                    case "mpg":
                    case "mpeg":
                        contentType = "video/mpeg";
                        break;
                    case "pdf":
                        contentType = "application/pdf";
                        break;
                    case "rtf":
                        contentType = "application/rtf";
                        break;
                    case "txt":
                        contentType = "text/plain";
                        break;
                    case "wav":
                        contentType = "audio/wav";
                        break;
                    case "xls":
                        contentType = "application/vnd.ms-excel";
                        break;
                    case "zip":
                        contentType = "application/zip";
                        break;
                    default:
                        contentType = "application/octet-stream";
                        break;
                }

                if (contentType == "application/octet-stream")
                {
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + this.DocumentName + "\"");
                }
                else
                {
                    this.Response.AddHeader("Content-Disposition", "filename=\"" + this.DocumentName + "\"");
                }

                this.Response.AddHeader("Content-Length", this.Document.Length.ToString());
                this.Response.ContentType = contentType;

                if (this.Document.Length > 0)
                {
                    this.Response.BinaryWrite(this.Document);
                }

                this.Response.End();
            }
        }

        /// <summary>
        /// Returns the JavaScript to complete the upload process.
        /// </summary>
        /// <returns>The JavaScript.</returns>
        protected virtual string GetUploadCompleteScript()
        {
            return this.GetUploadCompleteScript(false);
        }

        /// <summary>
        /// Returns the JavaScript to complete the upload process.
        /// </summary>
        /// <param name="autoSubmit">If true then make a call to PosseSubmit() when updating this document.</param>
        /// <returns>The JavaScript.</returns>
        protected virtual string GetUploadCompleteScript(bool autoSubmit)
        {
            string result = null, xml = null;
            string endPoint = this.Request.QueryString["UploadEndPoint"];
            string DocumentId = this.Request.QueryString["DocumentId"];
            string newObjectId = "NEW" + DateTime.Now.Millisecond.ToString();

            if (string.IsNullOrEmpty(endPoint))
            {
                result = string.Format(
                    "opener.Posse{0}Uploaded({1}, \"{2}\", \"{3}\", \"{4}\", \"{5}\"); window.close();",
                    this.UniqueId, this.LastUploadDocumentId, this.LastUploadFileName,
                    this.LastUploadExtension, this.LastUploadSize, this.LastUploadContentType);
            }
            else
            {
                if (string.IsNullOrEmpty(DocumentId))
                {
                    xml = string.Format("<object id=\"{0}\" objectdef=\"{1}\" action=\"Insert\">" +
                        "<pendingdocument id=\"{2}\"/></object>" +
                        "<object id=\"{3}\"><relationship endpoint=\"{4}\" toobjectid=\"{0}\" action=\"Insert\">" +
                        "</relationship></object>",
                        newObjectId, this.Request.QueryString["UploadObjectDef"],
                        this.LastUploadDocumentId, this.ObjectId, endPoint);
                }
                else
                {
                    xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                        "<pendingdocument id=\"{1}\"/></object>",
                        DocumentId, this.LastUploadDocumentId);
                }

                // First encode the xml as a javascript string literal to ensure that special characters such as double
                // quotes do not break the script.
                if (autoSubmit)
                {
                    result = string.Format("opener.PosseAppendChangesXML({0});opener.PosseSubmit(null);window.close();",
                        this.EncodeJSString(xml));
                }
                else
                {
                    result = string.Format("opener.PosseAppendChangesXML({0});opener.PosseNavigate(null);window.close();",
                        this.EncodeJSString(xml));
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the lookup script.
        /// </summary>
        /// <returns>Lookup script.</returns>
        protected virtual string GetLookupScript()
        {
            return this.OutriderNet.ProcessLookup(this.FromObjectHandle, this.FromRelationshipHandle,
                this.EndPointId, this.ToObjectId, int.Parse(this.Request.QueryString["PossePaneId"]),
                this.MappedItemInstanceId, this.DataChanges, this.RoundTripOnChange);
        }

        /// <summary>
        /// Renders the help link and adds it to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="text">Title text to be displayed.</param>
        protected virtual void RenderHelpBand(WebControl container)
        {
            HyperLink link;
            Panel panel;
            OrderedDictionary paneData;

            if (container != null)
            {
                panel = new Panel();

                if (this.hasPresentation)
                {
                    try
                {
                        paneData = this.GetPaneData(true)[0];

                        if (paneData.Contains("HelpTopicId"))
                        {
                            link = new HyperLink();
                            link.NavigateUrl = this.RootUrl + ConfigurationManager.AppSettings["HelpBaseUrl"] + paneData["HelpTopicId"].ToString() + ".htm";
                            link.ImageUrl = IconPath + "help.png";
                            link.ToolTip = "View Help for this area of the system";
                            link.Target = "_blank";
                            panel.Controls.Add(link);
                            container.Controls.Add(panel);
                }
                }
                    catch (Exception exception)
                {
                }
                }
            }
        }

        /// <summary>
        /// Renders the footer controls into the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="text">Title text to be displayed.</param>
        protected virtual void RenderFooterBand(WebControl container)
        {
            Panel footer;

            if (container != null)
            {
                footer = new Panel(); // add your footer contents to this panel
                container.Controls.Add(footer);
            }
        }

        /// <summary>
        /// Displays the search criteria pane in the container.
        /// </summary>
        protected virtual void RenderSearchCriteriaPane(WebControl container)
        {
            Literal html;
            if (container != null && !string.IsNullOrEmpty(this.SearchHtml))
            {
                html = new Literal();
                html.Text = this.SearchHtml;
                container.Controls.Add(html);
            }
        }

        /// <summary>
        /// Displays the given text in the title band.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="text">Title text to be displayed.</param>
        protected virtual void RenderTitle(WebControl container, string text)
        {
            Label label;
            Image img;
            Panel panel;
            string iconImageName = null, textNoIcon = null;
            int posStart, posEnd;
            OrderedDictionary paneData;

            if (container != null)
            {
                panel = new Panel();


                try
                {

                    if (this.hasPresentation)
                    {
                        //if there is <titleicon> tag in the Title property of the presentation, show the icon in the title
                        posStart = text.IndexOf("<titleicon>");
                        posEnd = text.IndexOf("</titleicon>");
                        if (posStart != -1 && posEnd != -1)
                        {
                            iconImageName = text.Substring(posStart + 11, (posEnd - (posStart + 11)));
                            textNoIcon = text.Substring(0, posStart);
                        }

                        if (iconImageName != null)
                        {
                            img = new Image();
                            img.ImageUrl = IconPath + iconImageName;
                            panel.Controls.Add(img);
                        }
                    }
                }
                catch (Exception exception)
                {
                }

                label = new Label();
                if (textNoIcon != null)
                {
                    label.Text = textNoIcon;
                }
                else
                {
                label.Text = text;
                }
                panel.Controls.Add(label);

                container.Controls.Add(panel);
            }
        }

        /// <summary>
        /// Renders the menu pane.
        /// </summary>
        /// <param name="titleContainer">Container to render the menu title into.</param>
        /// <param name="container">Container to render the menu into.</param>
        /// <param name="menuName">The name of the menu to render.</param>
        protected virtual void RenderMenuPane(WebControl container, WebControl titleContainer, WebControl imageContainer)
        {
            int index = 0, colSpan = 0, groupIndex;
            string link, url;
            Label label;
            Literal literal;
            Panel panel;
            MenuInfo menu, currentMenu;
            HtmlTable table = new HtmlTable();
            HtmlTableRow row = null, groupRow = null;
            HtmlTableCell newCell;
            List<HtmlTableCell> cells = new List<HtmlTableCell>();

            if (container != null)
            {
                LoadMenu(this.MenuName);
                menu = this.Menus[this.MenuName];

                // Build the breadcrumb
                literal = new Literal();
                literal.Text = menu.Label;
                if (menu.ParentMenu != null)
                {
                    currentMenu = menu;
                    while (currentMenu.ParentMenu != null && currentMenu.ParentMenu.Name != this.mainMenuName)
                    {
                        link = String.Format("<a class=\"breadcrumb\" href=\"{0}?PosseMenuName={1}\">{2}</a>",
                            this.homePageUrl, currentMenu.ParentMenu.Name, currentMenu.ParentMenu.Label);
                        literal.Text = String.Format("{0}&nbsp;&gt;&nbsp;{1}", link, literal.Text);
                        currentMenu = currentMenu.ParentMenu;
                    }
                }
                link = String.Format("<a class=\"breadcrumb\" href=\"{0}\">Home</a>", this.homePageUrl);
                literal.Text = String.Format("<div class=\"breadcrumb\">{0}&nbsp;&gt;&nbsp;{1}</div>", link, literal.Text);
                titleContainer.Controls.Add(literal);

                // Add the title and image if necessary
                if (menu.Label != null)
                {
                    this.RenderTitle(titleContainer, menu.Label);
                }
                if (menu.ImageUrl != null)
                {
                    literal = new Literal();
                    literal.Text = string.Format("<div><img src=\"{0}\"></div>", menu.ImageUrl);
                        imageContainer.Controls.Add(literal);
                }

                if (this.PaneMenus.ContainsKey(this.MenuName))
                {
                    container.Controls.Add(this.PaneMenus[this.MenuName]);
                }
                else
                {
                    foreach (MenuEntryPointGroup group in menu.EntryPointGroups)
                    {
                        if (row != null)
                        {
                            foreach (HtmlTableCell cell in cells)
                            {
                                cell.VAlign = "Top";
                                literal = new Literal();
                                literal.Text = "</ul>";
                                cell.Controls.Add(literal);

                                row.Cells.Add(cell);
                            }
                        }

                        // Render the group if there are entrypoints visible
                        if (group.EntryPointCount > 0)
                        {
                            if (group.SameRow && cells.Count > 0)
                            {
                                cells.Add(new HtmlTableCell());
                                index = cells.Count - 1;
                            }
                            else
                            {
                                cells.Clear();
                                cells.Add(new HtmlTableCell());
                                index = 0;
                                groupRow = new HtmlTableRow();
                                table.Rows.Add(groupRow);
                                row = new HtmlTableRow();
                                table.Rows.Add(row);
                            }

                            if (group.ColSpan != null)
                                colSpan = group.ColSpan;
                            else
                                colSpan = 0;
                            if (group.MultiColumn)
                            {
                                for (int colIndex = 1; colIndex < colSpan; colIndex++)
                                {
                                    newCell = new HtmlTableCell();

                                    literal = new Literal();
                                    literal.Text = "<ul class=\"menupanelist\">";
                                    newCell.Controls.Add(literal);

                                    cells.Add(newCell);
                                }
                            }
                            else
                            {
                                cells[index].ColSpan = colSpan;
                            }

                            groupIndex = index;
                            this.RenderMenuPaneHeader(group, table, groupRow);

                            literal = new Literal();
                            literal.Text = "<ul class=\"menupanelist\">";
                            cells[index].Controls.Add(literal);

                            // Generate entry point links
                            foreach (MenuEntryPoint item in group.EntryPoints)
                            {
                                if (item.SubMenu != null && item.SubMenu.EntryPointCount > 0)
                                    url = String.Format("{0}?PosseMenuName={1}", this.homePageUrl, item.SubMenu.Name);
                                        else
                                    url = item.Url;
                                this.RenderMenuPaneItem(cells[index], url, item.Help, item.Label, (item.SubMenu != null));
                                if (group.MultiColumn)
                                {
                                    if (index < groupIndex + colSpan - 1)
                                        index++;
                                    else
                                        index = groupIndex;
                                }
                            }
                        }

                        foreach (HtmlTableCell cell in cells)
                        {
                            cell.VAlign = "Top";
                            literal = new Literal();
                            literal.Text = "</ul>";
                            cell.Controls.Add(literal);

                            row.Cells.Add(cell);
                        }

                        if (row == null)
                        {
                            label = new Label();
                            label.Text = "No options available";
                            panel = new Panel();
                            panel.Controls.Add(label);

                            this.PaneMenus[this.MenuName] = (Control)panel;
                            container.Controls.Add(panel);
                        }
                        else
                        {
                            table.Rows.Add(row);

                            this.PaneMenus[this.MenuName] = (Control)table;
                            container.Controls.Add(table);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Renders the header for a menu group on a menu pane.
        /// </summary>
        /// <param name="node">Node representing the group.</param>
        /// <param name="table">The table to add the header to.</param>
        protected virtual void RenderMenuPaneHeader(MenuEntryPointGroup group, HtmlTable table, HtmlTableRow groupRow)
        {
            Label label;
            Literal literal;
            HtmlTableCell groupCell;

            groupCell = new HtmlTableCell();
            if (group.ColSpan != null)
            {
                groupCell.ColSpan = group.ColSpan;
            }

            label = new Label();
            label.CssClass = "menupaneheader";
            label.Text = group.Label;
            groupCell.Controls.Add(label);

            literal = new Literal();
            literal.Text = "<hr class=\"menupaneline\">";

            groupCell.Controls.Add(literal);
            groupRow.Cells.Add(groupCell);
        }

        /// <summary>
        /// Renders the header for a menu group on the menu band.
        /// </summary>
        /// <param name="node">Node representing the group.</param>
        /// <param name="table">The table to add the header to.</param>
        protected virtual void RenderMenuHeader(XmlNode node, HtmlTable table, HtmlTableRow groupRow)
        {
            Label label;
            HtmlTableCell cell;

            cell = new HtmlTableCell();
            cell.Attributes.Add("class", "lefthandmenu");

            label = new Label();
            label.CssClass = "menubarheader";
            label.Text = node.Attributes["label"].Value;
            cell.Controls.Add(label);

            groupRow.Cells.Add(cell);
        }


        /// <summary>
        /// Renders a menu item on a menu pane.
        /// </summary>
        /// <param name="cell">A reference to the table cell to add the menu item to.</param>
        /// <param name="url">The url for the menu item.</param>
        /// <param name="title">The title for the menu item.</param>
        /// <param name="label">The label for the menu item.</param>
        /// <param name="subMenuExists">If a sub-menu is linked to this item, pass true to display an indicator next to the link.</param>
        protected virtual void RenderMenuPaneItem(HtmlTableCell cell, string url, string title, string label, bool subMenuExists)
        {
            Literal literal = new Literal();
            string linkImage = "";
            string iconName = "";
            string iconImage = "";

            if (HttpUtility.ParseQueryString(url)["IconName"] != null)
            {
                iconName = HttpUtility.ParseQueryString(url)["IconName"];
                iconImage = string.Format("<img src=\"images/icons/{0}\"/>", iconName);
            }

            if (subMenuExists)
                linkImage = "<img src=\"images/menuarrow.gif\" style=\"border-style: none; vertical-align: bottom;\"/>";

            //literal.Text = string.Format("<li><div class=\"menupaneitem\"><a href=\"{0}\" title=\"{1}\">", url, title);
            //literal.Text += string.Format("<span>{0}</span></a></li>", label);
            literal.Text = string.Format("<li><div class=\"menupaneitem\"><a class=\"menupaneitem\" href=\"{0}\" title=\"{1}\">{2}{3}</a>{4}</div></li>",
                url, title, iconImage, label, linkImage);

            cell.Controls.Add(literal);
        }

        /// <summary>
        /// Renders a menu item on the menu band.
        /// </summary>
        /// <param name="row">A reference to the table row to add the menu item to.</param>
        /// <param name="url">The url for the menu item.</param>
        /// <param name="title">The title for the menu item.</param>
        /// <param name="label">The label for the menu item.</param>
        protected virtual void RenderMenuItem(WebControl div, string url, string title, string label)
        {
            Literal literal = new Literal();

            //literal.Text = string.Format("<li class=\"menubaritem\"><a href=\"{0}\" title=\"{1}\" class=\"menubaritem\">{2}</a></li>",
                //url, title, label);
            literal.Text = string.Format("<li><a href=\"{0}\" title=\"{1}\">{2}</a></li>",
                url, title, label);

            div.Controls.Add(literal);
        }

        /// <summary>
        /// For the current presentation, load the header pane if available.
        /// </summary>
        protected virtual void LoadHeaderPane()
        {
            int headerPaneId = 0, jobId = 0, jobHeaderPaneId = 0;
            string paneHtml, headerPaneHtml = "", jobHeaderHtml = "", includeJobPres = null, displayJobPane = null;
            OrderedDictionary paneData;

            if (this.HasPresentation)
            {
                // Determine header pane id.
                headerPaneId = this.GetHeaderPaneId();

                if (headerPaneId > 0)
                {
                    displayJobPane = Request.QueryString["DisplayJobPossePane"];
                    // We must fetch the pane to display first in order to ensure that the tab order
                    // starts with that pane.
                    if (this.CurrentPaneId == headerPaneId)
                    {
                        if (this.Panes.Count >= 2)
                        {
                            this.FetchPane(this.ObjectId, this.Panes[1].PaneId);
                            paneHtml = this.GetPaneHTML();
                            this.GetPaneChangesHTML(); // prime the property with the current Changes HTML
                            this.startTabIndex += 10000;
                            this.LoadPane();
                        }
                        else
                        {
                            paneHtml = null;
                            this.CurrentPaneId = 0;
                        }
                    }
                    else
                    {
                        paneHtml = this.GetPaneHTML();
                        this.StartTabIndex += 10000;
                        this.GetPaneChangesHTML(); // prime the property with the current Changes HTML
                    }

                    // Remove the header tab from the pane list.
                    this.RemovePane(this.Panes, headerPaneId);

                    // Now fetch the header pane.
                    this.FetchPane(this.ObjectId, headerPaneId);

                    // We need to do this check here in case the header pane has conditional display
                    // logic.  In those cases Outrider may not actually return the header pane
                    // (Not sure if this is a bug).
                    if (this.PaneId == headerPaneId)
                    {
                        //this.HeaderPaneHtml = this.GetPaneHTML();
                        headerPaneHtml = this.GetPaneHTML();
                        this.StartTabIndex += 10000;

                        // If a job pane is being displayed, then take the pane changes from the
                        // header so that focus is set to a field on the header.
                        if (!string.IsNullOrEmpty(displayJobPane))
                        {
                            this.GetPaneChangesHTML();
                        }

                        // Determine if we need to include a job presentation with the current presentation.
                        paneData = this.GetPaneData(true)[0];
                        if (paneData.Contains("includejobpresentation") && paneData["includejobpresentation"] != null)
                        {
                            includeJobPres = paneData["includejobpresentation"].ToString();
                        }
                        if (paneData.Contains("jobid") && paneData["jobid"] != null)
                        {
                            jobId = (int) paneData["jobid"];
                        }
                    }

                    // Determine if there is an attached job presentation to display as well
                    if (!string.IsNullOrEmpty(includeJobPres) && jobId > 0)
                    {
                        string queryString = string.Format("PossePresentation={0}&PosseObjectId={1}", includeJobPres, jobId);
                        if (!string.IsNullOrEmpty(displayJobPane))
                            queryString += "&PossePane=" + displayJobPane;
                        this.OutriderNet.StartDialog(queryString);
                        this.JobPanes = this.OutriderNet.GetVisiblePaneInfo();
                        if (!string.IsNullOrEmpty(displayJobPane))
                        {
                            paneHtml = this.GetPaneHTML(this.StartTabIndex);
                            this.StartTabIndex += 10000;
                            this.CurrentPaneId = this.OutriderNet.GetPaneId();
                        }

                        // Retrieve the job header pane if present
                        jobHeaderPaneId = this.GetPaneId("JobHeader");
                        if (jobHeaderPaneId > 0)
                        {
                            this.RemovePane(this.JobPanes, jobHeaderPaneId);
                            if (this.OutriderNet.GetPaneId() != jobHeaderPaneId)
                                this.FetchPane(jobId.ToString(), jobHeaderPaneId);
                            // We need to do this check here in case the header pane has conditional display
                            // logic.  In those cases Outrider may not actually return the header pane
                            // (Not sure if this is a bug).
                            if (this.OutriderNet.GetPaneId() == jobHeaderPaneId)
                            {
                                jobHeaderHtml = this.OutriderNet.GetPaneHTML(this.StartTabIndex);
                                this.StartTabIndex += 10000;
                            }
                        }

                        // Retrieve the first job pane if we still don't have a current pane
                        if (this.CurrentPaneId == null && this.JobPanes.Count > 0)
                        {
                            this.FetchPane(jobId.ToString(), JobPanes[0].PaneId);
                            paneHtml = this.OutriderNet.GetPaneHTML(this.startTabIndex);
                            this.startTabIndex += 10000;
                            this.CurrentPaneId = this.OutriderNet.GetPaneId();
                        }

                    }

                    this.PaneHtml = paneHtml;
                    if (!string.IsNullOrEmpty(jobHeaderHtml))
                        this.HeaderPaneHtml = jobHeaderHtml;
                    if (!string.IsNullOrEmpty(headerPaneHtml))
                        this.HeaderPaneHtml += headerPaneHtml;
                }
            }
        }

            /// <summary>
        /// Returns the pane id for the tombstone pane.
        /// </summary>
        /// <returns>The pane id of the tombstone pane.</returns>
        protected virtual int GetHeaderPaneId()
        {
            int result = 0;

            if (this.Panes != null && this.Panes.Count > 0)
            {
                foreach (VisiblePaneInfo pane in this.Panes)
                {
                    if (pane.Name == "Tombstone")
                    {
                        result = pane.PaneId;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Remove the given pane from the list of tabs
        /// </summary>
        /// <param name="paneList">Reference to a set of panes.</param>
        /// <param name="paneId">Id of the pane to remove.
        /// </param>
        protected virtual void RemovePane(List<VisiblePaneInfo> paneList, int paneId)
        {
            if (paneList != null)
            {
                foreach (VisiblePaneInfo pane in paneList)
                {
                    if (pane.PaneId == paneId)
                    {
                        paneList.Remove(pane);
                        break;
                    }

                }
            }
        }


        /// <summary>
        /// Use XSLT to transform outrider GetPaneXML format to Microsoft Excel XML.
        /// </summary>
        /// <param name="inputXML">XmlTextReader to read source XML in GetPaneXML format from.</param>
        /// <param name="outputXML">Stream to write XML in Excel format to.</param>
        /// <param name="xsltName">String filename of XSLT file.</param>
        protected void TransformToExcelXML(XmlTextReader inputXML, System.IO.Stream outputXML, string xsltName)
        {
            // Create a new XslTransform object.
            XslCompiledTransform xslt = new XslCompiledTransform();

            // Load the stylesheet.
            xslt.Load(this.Request.MapPath(xsltName));

            // Setup the XML reader and use it to load an XPathDocument
            XPathDocument mydata = new XPathDocument(inputXML);

            // Setup the XML writer for output
            XmlWriter writer = new XmlTextWriter(outputXML, null);
            writer.WriteStartDocument(true);

            // Transform the data and write it out to the outputXML stream
            xslt.Transform(mydata, writer);
            writer.Flush();
        }

        /// <summary>
        /// Use XSLT to transform outrider GetPaneXML format to Microsoft Excel XML.
        /// </summary>
        /// <param name="inputXML">Stream to read source XML in GetPaneXML format from.</param>
        /// <param name="outputXML">Stream to write XML in Excel format to.</param>
        /// <param name="xsltName">String filename of XSLT file.</param>
        protected void TransformToExcelXML(System.IO.Stream inputXML, System.IO.Stream outputXML, string xsltName)
        {
            // Create a new XslTransform object.
            XslCompiledTransform xslt = new XslCompiledTransform();

            // Load the stylesheet.
            xslt.Load(this.Request.MapPath(xsltName));

            // Setup the XML reader and use it to load an XPathDocument
            XmlTextReader xr = new XmlTextReader(inputXML);
            XPathDocument mydata = new XPathDocument(xr);

            // Setup the XML writer for output
            XmlWriter writer = new XmlTextWriter(outputXML, null);
            writer.WriteStartDocument(true);

            // Transform the data and write it out to the outputXML stream
            xslt.Transform(mydata, writer);
            writer.Flush();
        }

        /// <summary>
        /// Use XSLT to transform outrider GetPaneXML format to Microsoft Excel XML.
        /// </summary>
        /// <param name="inputXML">String containing source XML in GetPaneXML format.</param>
        /// <param name="outputXML">Stream to write XML in Excel format to.</param>
        /// <param name="xsltName">String filename of XSLT file.</param>
        protected void TransformToExcelXML(String inputXML, System.IO.Stream outputXML, string xsltName)
        {
            // Setup a memory stream for the input XML
            using (MemoryStream inputStream = new MemoryStream(inputXML.Length + 100))
            {
                using (StreamWriter sw = new StreamWriter(inputStream))
                {
                    // Add an xml header if we're missing on
                    if (!inputXML.StartsWith("<?xml"))
                    {
                        sw.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                    }
                    sw.Write(inputXML);
                    sw.Flush();
                    inputStream.Position = 0;
                    this.TransformToExcelXML(inputStream, outputXML, xsltName);
                }
            }
        }

        /// <summary>
        /// Load and cache the system information from POSSE.
        /// The cache is a Dictionary<string, RecordSet>
        /// </summary>
        protected virtual void LoadSystemInformation()
        {
            if (!string.IsNullOrEmpty(this.SystemInfoPresentation))
            {
                this.StartDialog(string.Format("PossePresentation={0}&PosseShowCriteriaPane=No&{1}",
                    this.SystemInfoPresentation, this.SystemInfoParameters));

                this.SystemInfo[this.MenuName] = this.GetPaneData()[0];
                if ((bool)this.SystemInfo[this.MenuName]["isunavailable"])
                {
                    this.Response.Redirect(this.SystemInfo[this.MenuName]["isunavailable"].ToString());
                }
            }
        }

        /// <summary>
        /// Load the conditions.
        /// </summary>
        protected virtual void LoadConditions()
        {
            this.DisableCancel = this.GetCondition(this.CurrentPaneId, "DisableCancel");
            this.FilteredList = this.GetCondition(this.CurrentPaneId, "FilteredList");
            this.SaveAsExcel = this.GetCondition(this.CurrentPaneId, "SaveAsExcel");
            this.SaveAsExcelReport = this.GetCondition(this.CurrentPaneId, "SaveAsExcelReport");
            this.SaveAsCSVReport = this.GetCondition(this.CurrentPaneId, "SaveAsCSVReport");
            this.SaveIsNext = this.GetCondition(this.CurrentPaneId, "SaveIsNext");
            this.ShowAsterisk = this.GetCondition(this.CurrentPaneId, "ShowAsterisk");
            this.SubmitOnClose = this.GetCondition(this.CurrentPaneId, "SubmitOnClose");
        }
        #endregion
    }
}
