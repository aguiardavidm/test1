<%@ Page Language="C#" MasterPageFile="~/OutriderPopup.master" AutoEventWireup="true" CodeFile="DefaultLite.aspx.cs" Inherits="Computronix.POSSE.Outrider.DefaultLite" Title="Default Lite" %>

<asp:Content ID="cntTitleBand" runat="server" ContentPlaceHolderID="cphTitleBand">
	<asp:Panel ID="pnlTitleBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntTopBand" runat="server" ContentPlaceHolderID="cphTopBand">
	<asp:Panel ID="pnlTopBand" runat="server" Style="margin-left: 10px; margin-top: 10px">
		<asp:Panel ID="pnlHeaderBand" runat="server" CssClass="headerband">
		</asp:Panel>
		<asp:Panel ID="pnlSearchCriteriaBand" runat="server" CssClass="embeddedCriteriaBand">
		</asp:Panel>
	    <asp:Panel ID="pnlTopFunctionBand" runat="server">
	    </asp:Panel>
		<asp:Panel ID="pnlTabLabelBand" runat="server" CssClass="tablabelband">
		</asp:Panel>
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntPaneBand" ContentPlaceHolderID="cphPaneBand" runat="Server">
	<asp:Panel ID="pnlPaneBand" runat="server" CssClass="datazone">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntBottomFunctionBand" runat="server" ContentPlaceHolderID="cphBottomFunctionBand">
	<asp:Panel ID="pnlBottomFunctionBand" runat="server">
	</asp:Panel>
</asp:Content>

