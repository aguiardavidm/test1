function LBD_LoadSound(soundPlaceholderId, soundLink)
{
    if(document.getElementById)
    {
        var placeholder = document.getElementById(soundPlaceholderId);
        var objectSrc = "<object id='captchaSound' classid='clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95' height='0' width='0' style='width:0; height:0;'><param name='AutoStart' value='1' /><param name='Volume' value='0' /><param name='PlayCount' value='1' /><param name='FileName' value='" + soundLink + "' /><embed id='captchaSoundEmbed' src='"+ soundLink + "' autoplay='true' hidden='true' volume='100' type='"+ LBD_GetMimeType() +"' style='display:inline;' /></object>";

        placeholder.innerHTML = "";
        placeholder.innerHTML = objectSrc;
    }
}

function LBD_GetMimeType()
{
    var mimeType = "audio/x-wav"; //default
    return mimeType;
}
