using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Computronix.POSSE.Outrider
{
    public partial class Download : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			
		    // First, check if this is a guest and if they have access to the document they are looking at (based on SessionId)
		    // For non-guests, this is all handled by instance security
		    if (this.IsGuest)
		    {
			OrderedDictionary RecordData;
			RecordData = this.GetData("ObjectInformation", int.Parse(this.ObjectId))[0];
			String PresentSessionId = this.SessionId.ToString();
			
			String ApplicationSessionId = RecordData["SessionId"].ToString();
			
			//if this document is not attached to any session, just render it
			if (ApplicationSessionId == "no session id")
			{
				this.RenderDocument();
			}
			//if this document is attached to a session, and it is this session, render it 
			else if (PresentSessionId == ApplicationSessionId)
			{
				this.RenderDocument();
			}
			//else, no access
			else
			{
				this.ReleaseOutriderNet();
				this.Response.Redirect("NoAccess.aspx");
			}
		    }
		    //if not a guest, just render it and let instance security take over
		    else 
		    {
		    	this.RenderDocument();
		    }
		}
		#endregion
	}
}