"""Contains validation methods for the LMS Product.

This module is meant to contain validation utilties and object
validation routine that would typically run in On-Row Change Scripts and
Pre-Save scripts. Action Buttons (and anything else) could also make use
of these routines.

"""
import cx_Logging
import re

#-----------------------------------------------------------------------
# Validation Utilities
#-----------------------------------------------------------------------
regexEmailAddress = re.compile(r'^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$', re.I)
regexZipPostalCode = re.compile(r'^(\d{5}(-\d{4})?|[A-Z]\d[A-Z][ -]?\d[A-Z]\d)$', re.I)

def IsEmailValid(emailAddress):
    """Validates whether an email address is in a correct format."""
    return bool(regexEmailAddress.match(emailAddress))


def IsZipPostalCodeValid(zipPostalCode):
    """Validates whether a Zip or Postal Code is in a correct format."""
    return bool(regexZipPostalCode.match(zipPostalCode))


#-----------------------------------------------------------------------
# Object Validation Methods
#-----------------------------------------------------------------------
def ValidateApplicationDocument(dataArea, doc):
    """Validates mandatory details on an Application Document popup

    Checks that an Attachment Type has been specified. If the Document
    has been marked as Not Accepted, verifies that a Not Accepted Reason
    has been provided.

    """
    # Validate Attachment Type
    docTypeEndPt = dataArea.configCache.EndPointForName(
            doc.objectDef, "DocumentType")
    if len(doc.GetRelatedObjects([docTypeEndPt])) == 0:
        dataArea.QueueBusinessErrorForEndPoint(
                "An Attachment type must be selected.", doc, docTypeEndPt)

    # Validate Not Accepted Reason
    if (doc.GetColumnValueByName("AcceptanceStatus") == "Not Accepted" and
            not doc.GetColumnValueByName("NotAcceptedReason")):
        dataArea.QueueBusinessErrorForColumn(
                "Please enter a Not Accepted Reason.", doc, "NotAcceptedReason")


def ValidateContactInfo(dataArea, contact):
    """Validates mandatory details on the o_CR_Contact ObjectDef"""
    ContactVal = contact.GetColumnValueByName
    emailAddress = ContactVal('EmailAddress')

    if not ContactVal('PhoneNumber') and not emailAddress:
        dataArea.QueueBusinessErrorForColumn("One of either Phone Number " \
                "or Email Address must be entered.", contact, 'PhoneNumber')
        dataArea.QueueBusinessErrorForColumn("", contact, 'EmailAddress')
    elif emailAddress and not IsEmailValid(emailAddress):
        dataArea.QueueBusinessErrorForColumn("Email Address is invalid. " \
                "Please provide a valid Email Address.", contact, 'EmailAddress')


def ValidateCustomerInfo(dataArea, cust, checkTempAddress=False):
    """Validates that mandatory data on the Customer has been filled in.

    Specifically, it validates the customer name, notification method,
    details need for the notification method, and related phone numbers
    and mailing addresses where appropriate.

    If the "checkTempAddress" argument is passed as true, the primary
    address check will include the "TempMailingAddress" relationship.

    """
    configCache = dataArea.configCache
    CustVal = cust.GetColumnValueByName

    # Validate Company/Individual name
    cx_Logging.Debug('*** isIndividual: %r' % CustVal('isIndividual', True))
    if CustVal('isIndividual', True):
        if not CustVal('FirstName'):
            dataArea.QueueBusinessErrorForColumn(
                    "Please provide a value for First Name.", cust, 'FirstName')
        if not CustVal('LastName'):
            dataArea.QueueBusinessErrorForColumn(
                    "Please provide a value for Last Name.", cust, 'LastName')
    else:
        cx_Logging.Debug('*** Company Name: %r' % CustVal('CompanyName'))
        if not CustVal('CompanyName'):
            dataArea.QueueBusinessErrorForColumn("Please provide a " \
                    "value for Company Name", cust, 'CompanyName')

    # Validate email address format
    emailAddress = CustVal('EmailAddress')
    if emailAddress and not IsEmailValid(emailAddress):
        dataArea.QueueBusinessErrorForColumn("Email Address is invalid. " \
                "Please provide a valid Email Address.", cust, 'EmailAddress')

    # Validate Notification Method
    notificationMethod = CustVal('NotificationMethod')
    if not notificationMethod:
        dataArea.QueueBusinessErrorForColumn("Please provide a value for " \
                "Notification Method.", cust, 'NotificationMethod')
    elif notificationMethod == 'Email' and not emailAddress:
        dataArea.QueueBusinessErrorForColumn(
                "Please provide a value for Email Address.", cust, 'emailAddress')

    # Validate Mailing Address
    addressEndPtNames = ['MailingAddress']
    if checkTempAddress:
        addressEndPtNames.append('TempMailingAddress') # Check Temp Address rel
    addressEndPtList = [configCache.EndPointForName(cust.objectDef, endPtName) \
            for endPtName in addressEndPtNames]
    primaryAddresses = [address for address in cust.GetRelatedObjects(addressEndPtList)
            if address.GetColumnValueByName('IsPrimary', True)]
    if not primaryAddresses:
        dataArea.QueueBusinessErrorForEndPoint("Please provide at least one " \
                "Primary Mailing Address.", cust, addressEndPtList[0])
    elif len(primaryAddresses) > 1:
        dataArea.QueueBusinessErrorForEndPoint("Please provide only one " \
                "Primary Mailing Address.", cust, addressEndPtList[0])

    if notificationMethod in ('Phone', 'Fax'):
        # If Notification Method is phone or fax, ensure that a related phone
        # or fax number exists
        phoneNumEndPt = configCache.EndPointForName(cust.objectDef, 'PhoneNumber')
        if notificationMethod == 'Fax':
            phoneNumbers = [phoneNum for phoneNum in \
                    cust.GetRelatedObjects([phoneNumEndPt])
                    if phoneNum.GetColumnValueByName('Type').endswith('Fax')]
        else:
            phoneNumbers = [phoneNum for phoneNum in \
                    cust.GetRelatedObjects([phoneNumEndPt])
                    if not phoneNum.GetColumnValueByName('Type').endswith('Fax')]

        if not phoneNumbers:
            dataArea.QueueBusinessErrorForEndPoint("Please provide at least " \
                    "one {0} Number.".format(notificationMethod), cust, 'PhoneNumber')


def ValidateMailingAddress(dataArea, addressObj):
    """Validates mandatory details on the o_CP_MailingAddress Object."""
    AddressVal = addressObj.GetColumnValueByName

    addressType = AddressVal('AddressType')

    if not addressType and not AddressVal('IsConverted', True):
        # If not a converted address, ensure the Address Type has been entered
        dataArea.QueueBusinessErrorForColumn("Please provide a value " \
                "for Address Type.", addressObj, 'AddressType')
    elif addressType == 'Other / International':
        # Validate "Other / International" Address
        if not AddressVal('OtherAddress'):
            dataArea.QueueBusinessErrorForColumn("Please enter all " \
                    "information for Other Address.", addressObj, 'OtherAddress')
    elif addressType is not None: # Validate all other address types
        # Validate type-specific details
        if addressType == 'Civic':
            if not AddressVal('StreetAddress'):
                dataArea.QueueBusinessErrorForColumn("Please provide a value" \
                        " for Street Address.", addressObj, 'StreetAddress')
        if addressType == 'PO Box':
            if not AddressVal('PO_PostOfficeBox'):
                dataArea.QueueBusinessErrorForColumn("Please provide a value" \
                        " for PO Box Number", addressObj, 'PO_PostOfficeBox')
        if addressType == 'Rural Route':
            if not AddressVal('RR_RuralRouteIdentifier'):
                dataArea.QueueBusinessErrorForColumn("Please provide a value" \
                        " for Rural Route Identifier", addressObj,
                        'RR_RuralRouteIdentifier')
        if addressType == 'General Delivery':
            if not AddressVal('GD_GeneralDeliveryIndicator'):
                dataArea.QueueBusinessErrorForColumn("Please provide a value" \
                        " for GD Indicator", addressObj,
                        'GD_GeneralDeliveryIndicator')

        # Validate general details
        if not AddressVal('CityTown'):
            dataArea.QueueBusinessErrorForColumn("Please provide a value for" \
                    " City/Town", addressObj, 'CityTown')
        if not AddressVal('ProvinceState'):
            dataArea.QueueBusinessErrorForColumn("Please provide a value for" \
                    " State/Province", addressObj, 'ProvinceState')
        if not AddressVal('Country'):
            dataArea.QueueBusinessErrorForColumn("Please provide a value for" \
                    " Country", addressObj, 'Country')

        # Validate Zip/Postal code existence and format
        zipPostalCode = AddressVal('PostalCode')
        if not zipPostalCode:
            dataArea.QueueBusinessErrorForColumn("Please provide a value for" \
                    " Zip/Postal Code", addressObj, 'PostalCode')
        elif not IsZipPostalCodeValid(zipPostalCode):
            dataArea.QueueBusinessErrorForColumn("Zip/Postal Code " \
                    "is invalid. Please enter a valid Zip/Postal Code.",
                    addressObj, 'PostalCode')


def ValidatePhoneNumber(dataArea, phoneNumberObj):
    """Validates mandatory details on the o_CR_PhoneNumber Object."""
    PhoneNumberVal = phoneNumberObj.GetColumnValueByName

    phoneNumberType = PhoneNumberVal('Type')

    if phoneNumberType == 'International':
        internationalNumber = PhoneNumberVal('InternationalPhoneNumber')

        if not internationalNumber:
            dataArea.QueueBusinessErrorForColumn("Please provide a value for" \
                    " International Phone Number.", phoneNumberObj,
                    'InternationalPhoneNumber')
        elif not re.match('^[-0-9 ()]{6,26}$', internationalNumber):
            dataArea.QueueBusinessErrorForColumn(
                    "Please provide a value for International Phone Number " \
                    "that is between 6 and 26 numeric characters.",
                    phoneNumberObj, 'InternationalPhoneNumber')
    else:
        if not PhoneNumberVal('PhoneNumber'):
            dataArea.QueueBusinessErrorForColumn("Please provide a value for" \
                    " Phone Number.", phoneNumberObj, 'PhoneNumber')
        else:
            phoneNumberExt = PhoneNumberVal('PhoneNumberExtension')

            if phoneNumberExt is not None and not re.match('^[0-9]+$', phoneNumberExt):
                dataArea.QueueBusinessErrorForColumn("Please provide a value" \
                        " for Extension that only contains numbers.",
                        phoneNumberObj, 'PhoneNumberExtension');
