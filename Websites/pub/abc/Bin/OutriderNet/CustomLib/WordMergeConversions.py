from openpack.basepack import Relationship
import StringIO
from lxml import etree
from openpack.zippack import ZipPackage


def _AddRelationship(relationshipType, relFrom, relTo):
    relFrom.relationships.add(Relationship(relFrom, relTo, relationshipType))


def _ChangeContentType(package, partName, newContentType):
    part = package[partName]
    part.content_type = newContentType
    contentType = package.content_types.find_for(partName)
    contentType.name = newContentType


def _ChangeCustomProperty(xml, propertyName, newValue):
    properties = etree.fromstring(xml)
    for property in properties:
        if property.attrib["name"].lower() == propertyName.lower():
            value = list(property)[0]
            if isinstance(newValue, bool):
                adjustedValue = ("%s" % newValue).lower()
            else:
                adjustedValue = newValue
            value.text = adjustedValue
            break
    return "%s\n%s" % (xml.split("\n")[0], etree.tostring(properties))


def _CopyPart(sourcePackage, targetPackage, partName, contentType, override=True):
    part = sourcePackage[partName]
    part.package = targetPackage
    part.content_type = contentType
    targetPackage.add(part, override=override)


def _GetCustomPropertyNames(xml):
    properties = etree.XML(xml)
    result = {}
    for property in properties:
        result[property.attrib["name"]] = None
    return result


def _RemoveRelationship(relationshipType, relFrom, relTo):
    new = set([])
    for x in relFrom.relationships:
        if (x.target != relTo and "/%s"%x.target != relTo) or x.type != relationshipType:
            new.add(x)
    relFrom.relationships.children.intersection_update(new)


def _StripPart(package, partName, contentType=None):
    if partName in package:
        del package[partName]
        if contentType:
            new = set([])
            for ct in package.content_types:
                if ct.name != contentType:
                    new.add(ct)
            package.content_types.intersection_update(new)


def AddMacros(_sourceDoc, _sourceMacros):
    f = StringIO.StringIO(_sourceDoc)
    _source = ZipPackage.from_stream(f)
    g = StringIO.StringIO(_sourceMacros)
    _macro = ZipPackage.from_stream(g)

    _CopyPart(_macro, _source, "/docProps/custom.xml",
        "application/vnd.openxmlformats-officedocument.custom-properties+xml")
    _AddRelationship("http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties",
            _source, "/docProps/custom.xml")

    if "/customUI/customUI14.xml" in _macro:
        _CopyPart(_macro, _source, "/customUI/customUI14.xml",
                "application/xml", override=False)
        _AddRelationship("http://schemas.microsoft.com/office/2007/relationships/ui/extensibility",
            _source, "/customUI/customUI14.xml")

    if "/word/customizations.xml" in _macro:
        _CopyPart(_macro, _source, "/word/customizations.xml",
                "application/vnd.ms-word.keyMapCustomizations+xml")
        _AddRelationship("http://schemas.microsoft.com/office/2006/relationships/keyMapCustomizations",
            _source["/word/document.xml"], "customizations.xml")
        if "/word/attachedToolbars.bin" in _macro:
            _CopyPart(_macro, _source, "/word/attachedToolbars.bin",
                "application/vnd.ms-word.attachedToolbars")
#    _AddRelationship("http://schemas.microsoft.com/office/2006/relationships/attachedToolbars.bin",
#            _source["/word/customizations.xml"], "attachedToolbars.bin")

    _CopyPart(_macro, _source, "/word/vbaProject.bin",
        "application/vnd.ms-office.vbaProject")
    _AddRelationship("http://schemas.microsoft.com/office/2006/relationships/vbaProject",
            _source["/word/document.xml"], "vbaProject.bin")

    _CopyPart(_macro, _source, "/word/vbaData.xml",
        "application/vnd.ms-word.vbaData+xml")
#    _AddRelationship("http://schemas.microsoft.com/office/2006/relationships/wordVbaData",
#            _source["/word/vbaProject.bin"], "vbaData.xml")

    if "/word/vbaProjectSignature.bin" in _macro:
        _CopyPart(_macro, _source, "/word/vbaProjectSignature.bin",
            "application/vnd.ms-office.vbaProjectSignature")
#        _AddRelationship("http://schemas.microsoft.com/office/2006/relationships/vbaProjectSignature",
#                _source["/word/vbaProject.bin"], "vbaProjectSignature.bin")

    _ChangeContentType(_source, "/word/document.xml",
            "application/vnd.ms-word.document.macroEnabled.main+xml")
    propertyNames = _GetCustomPropertyNames(_source["/docProps/custom.xml"].dump())
    f = _source.as_stream()
    value = f.getvalue()
    return (value, propertyNames)


def SetProperties(_sourceDoc, properties):
    f = StringIO.StringIO(_sourceDoc)
    _source = ZipPackage.from_stream(f)
    customProperties = _source["/docProps/custom.xml"].dump()
    for propertyName in properties:
        customProperties = _ChangeCustomProperty(customProperties, propertyName, properties[propertyName])
    _source["/docProps/custom.xml"].load(customProperties)
    f = _source.as_stream()
    return f.getvalue()


def StripMacros(_sourceDoc):
    f = StringIO.StringIO(_sourceDoc)
    _source = ZipPackage.from_stream(f)

    _RemoveRelationship(
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties",
            _source,
            "/docProps/custom.xml")

    _RemoveRelationship("http://schemas.microsoft.com/office/2007/relationships/ui/extensibility",
            _source,
            "/customUI/customUI14.xml")

    if "/word/document.xml" in _source:
        _RemoveRelationship("http://schemas.microsoft.com/office/2006/relationships/keyMapCustomizations",
                _source["/word/document.xml"],
                "customizations.xml")
        _RemoveRelationship("http://schemas.microsoft.com/office/2006/relationships/vbaProject",
                _source["/word/document.xml"],
                "vbaProject.bin")
    if "/word/customizations.xml" in _source:
        _RemoveRelationship("http://schemas.microsoft.com/office/2006/relationships/attachedToolbars",
                _source["/word/customizations.xml"],
                "attachedToolbars.bin")
    if "/word/vbaProject.bin" in _source:
        _RemoveRelationship("http://schemas.microsoft.com/office/2006/relationships/wordVbaData",
                _source["/word/vbaProject.bin"],
                "vbaData.xml")
    if "/word/vbaProjectSignature.bin" in _source:
        _RemoveRelationship("http://schemas.microsoft.com/office/2006/relationships/vbaProjectSignature",
                _source["/word/vbaProject.bin"],
                "vbaProjectSignature.bin")

    _StripPart(_source, "/docProps/custom.xml",
            "application/vnd.openxmlformats-officedocument.custom-properties+xml")
    _StripPart(_source, "/word/customizations.xml",
            "application/vnd.ms-word.keyMapCustomizations+xml")
    _StripPart(_source, "/word/attachedToolbars.bin",
            "application/vnd.ms-word.attachedToolbars")
    _StripPart(_source, "/word/vbaProject.bin",
            "application/vnd.ms-office.vbaProject")
    _StripPart(_source, "/word/vbaData.xml",
            "application/vnd.ms-word.vbaData+xml")
    _StripPart(_source, "/word/vbaProjectSignature.bin",
            "application/vnd.ms-office.vbaProjectSignature")
    _StripPart(_source, "/customUI/customUI14.xml")

    _ChangeContentType(_source, "/word/document.xml",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml")

    f = _source.as_stream()
    return f.getvalue()

if __name__ == '__main__':
    TESTDOC    = r"c:\dev\WordMerge\input.docx"
    TESTSTRIP  = r"c:\dev\WordMerge\strip.docx"
    TESTMACRO  = r"c:\dev\WordMerge\macros.docm"
    TESTRESULT = r"c:\dev\WordMerge\Result2.docm"
    TESTCLEAN  = r"c:\dev\WordMerge\Clean2.docx"
    TESTPROPERTIES = {
            "PossePresentation":      "newWordInterface",
            "PosseWebServiceWsdlUrl": "newhttps://demo.computronix.com/USLMSDemo/ws/lms/int/wordinterface.asmx?wsdl",
            "PosseRecipientPaneName": "newRecipientAddressesWI",
            "PosseSendCredentials":   True,
            "PosseAutoGetData":       True
            }

    sourceDoc = file(TESTDOC, "rb").read()
    sourceDoc = StripMacros(sourceDoc)
    file(TESTSTRIP,"wb").write(sourceDoc)

    macroDoc = file(TESTMACRO, "rb").read()

    (new, propertyNames) = AddMacros(sourceDoc, macroDoc)
    new = SetProperties(new, TESTPROPERTIES)
    file(TESTRESULT, "wb").write(new)

    clean = StripMacros(new)
    file(TESTCLEAN, "wb").write(clean)
