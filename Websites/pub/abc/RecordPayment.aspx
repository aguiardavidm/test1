<%@ Page Language="C#" MasterPageFile="~/Outrider.master" AutoEventWireup="true" CodeFile="RecordPayment.aspx.cs" Inherits="Payment" Title="Home" %>
<%@ Import Namespace="Computronix.POSSE.Outrider" %>

<asp:Content ID="cntUserInfo" runat="server" ContentPlaceHolderID="cphUserInfo">
	<asp:Panel ID="pnlUserInfo" runat="server" CssClass="userInfoPanel">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntMenuBand" runat="server" ContentPlaceHolderID="cphMenuBand">
	<asp:Panel ID="pnlMenuBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntTitleBand" runat="server" ContentPlaceHolderID="cphTitleBand">
	<asp:Panel ID="pnlTitleBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntHelpBand" runat="server" ContentPlaceHolderID="cphHelpBand">
	<asp:Panel ID="pnlHelpBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntTopBand" runat="server" ContentPlaceHolderID="cphTopBand">
	<asp:Panel ID="pnlTopBand" runat="server">
		<asp:Panel ID="pnlSearchCriteriaBand" runat="server" CssClass="embeddedCriteriaBand">
		</asp:Panel>
		<asp:Panel ID="pnlHeaderBand" runat="server" CssClass="headerband">
		</asp:Panel>
		<div class="rightButtons">
            <asp:Panel ID="pnlTopFunctionBand" runat="server">
            </asp:Panel>
        </div>
		<asp:Panel ID="pnlTabLabelBand" runat="server">
	    </asp:Panel>
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntPaneBand" ContentPlaceHolderID="cphPaneBand" runat="Server">
	<asp:Panel ID="pnlPaneBand" runat="server" CssClass="datazone">
	    <div id="todolist"></div>
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntBottomFunctionBand" runat="server" ContentPlaceHolderID="cphBottomFunctionBand">
	<asp:Panel ID="pnlBottomFunctionBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntDebugLinkBand" runat="server" ContentPlaceHolderID="cphDebugLinkBand">
	<asp:Panel ID="pnlDebugLinkBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntFooterBand" runat="server" ContentPlaceHolderID="cphFooterBand">
	<asp:Panel ID="pnlFooterBand" runat="server">
	</asp:Panel>
</asp:Content>