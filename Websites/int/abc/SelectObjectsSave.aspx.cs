using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class SelectObjectsSave : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Literal html;
            string currentObjectIds = this.Request.QueryString["CurrentObjectIds"];
            int objCount = 0;
            List<OrderedDictionary> paneData;
            Dictionary<string, bool> currentObjects = new Dictionary<string, bool>();
            List<string> selectedObjects = new List<string>();
            List<string> unSelectedObjects = new List<string>();
            StringBuilder changeCheckboxXML = new StringBuilder();

            if (currentObjectIds != null)
            {
                foreach (string objectId in currentObjectIds.Split(new char[] { ',' }))
                {
					string ObjectId = objectId.Trim();
					this.ValidateUrlParameter("CurrentObjectIds", ObjectId);
					currentObjects[ObjectId] = true;
                }
            }

            this.ValidateUrlParameter("FromObjectId", this.FromObjectId);
            this.ValidateUrlParameter("EndPoint", this.EndPoint);

            this.HomePageUrl = this.RootUrl + string.Format("selectobjectssave.aspx?FromObjectId={0}&EndPoint={1}&CurrentObjectIds={2}",
            this.FromObjectId, this.EndPoint, currentObjectIds);
            this.LoadPresentationCore(false);
            if (this.HasPresentation)
            {

                if (currentObjects.Count > 0 && this.Request.QueryString["Action"] != "Submit") // pre-select those that are already selected
                {
                    paneData = this.GetPaneData(true);
                    foreach (OrderedDictionary row in paneData)
                    {
                        if (currentObjects.ContainsKey(row["objecthandle"].ToString()))
                        {
                            // set the check box
                            changeCheckboxXML.AppendFormat("<object id=\"{0}\" action=\"Update\"><column name=\"WebSearchSelected\">Y</column></object>",
                                row["objecthandle"].ToString());
                        }
                    }
                    if (changeCheckboxXML.Length > 0)
                    {
                        this.ProcessFunction(null, this.PaneId, this.PaneId, RoundTripFunction.Refresh,
                            this.GetCacheState(), this.SortColumns, changeCheckboxXML.ToString());
                    }
                }

                this.PaneHtml = this.GetPaneHTML();
                this.GetPaneChangesHTML();  // prime the property with the current Changes HTML.
                this.StartTabIndex += 10000;
                this.LoadHeaderPane();
            }
            this.RenderErrorMessage(this.pnlPaneBand);

            // If the user pressed "Submit" then check for selected objects and close the popup.
            if (selectedObjects.Count == 0)
            {
                if (this.ComesFrom == "posse" && this.Request.QueryString["Action"] == "Submit" &&
                  string.IsNullOrEmpty(this.ErrorMessage))
                {
                    // Determine which objects were selected.  Remove Rels for objects already related but no longer selected.
                    paneData = this.GetPaneData(true);

                    foreach (OrderedDictionary row in paneData)
                    {
                        if ((bool)row["websearchselected"])
                        {
                            objCount += 1;
                            if (!currentObjects.ContainsKey(row["objecthandle"].ToString()))
                            {
                                selectedObjects.Add(row["objecthandle"].ToString());
                            }
                        }
                        else
                        {
                            if (currentObjects.ContainsKey(row["objecthandle"].ToString()))
                            {
                                unSelectedObjects.Add(row["objecthandle"].ToString());
                            }
                        }
                    }

                    // Build the javascript to return to the browser. Create the xml to relate any objects
                    // that were selected.
                    html = new Literal();

                    if (selectedObjects.Count > 0 || unSelectedObjects.Count > 0)
                    {
                        html.Text = this.SelectedObjectsReturnScript(selectedObjects, unSelectedObjects, this.FromObjectId, this.EndPoint);
                    }
                    else if (objCount == 0)
                    {
                        html.Text += "<script>alert('You did not select any items.'); window.close();</script>";
                    }
                    else
                    {
                        html.Text += "<script>window.close();</script>";
                    }

                    this.pnlPaneBand.Controls.Add(html);
                    this.HasPresentation = false;
                }
                else if (this.HasPresentation)
                {
                    this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
                    this.CheckClient();
                    //this.RenderSelectObjectsFunctionBand(this.pnlTopFunctionBand);
                    this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
                    this.RenderSelectObjectsFunctionBand(this.pnlBottomFunctionBand);
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Build javascript to relate the selected objects to the source object.
        /// </summary>
        /// <param name="selectedObjects">An array of object id's that were selected.</param>
        /// <param name="fromObjectId">The source object to relate the selected objects to</param>
        /// <param name="endPoint">End Point name of the relationship to relate the selected objects to.</param>
        protected virtual string SelectedObjectsReturnScript(List<string> selectedObjects, List<string> unSelectedObjects,
          string fromObjectId, string endPoint)
        {
            int index;
            StringBuilder xml = new StringBuilder();

            xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", fromObjectId);
            index = 1;

            foreach (string objectId in selectedObjects)
            {
                xml.AppendFormat("<relationship action=\"Insert\" endpoint=\"{1}\" toobjectid=\"{2}\"/>", index, endPoint, objectId);
                index += 1;
            }

            foreach (string objectId in unSelectedObjects)
            {
                xml.AppendFormat("<relationship action=\"Delete\" endpoint=\"{1}\" toobjectid=\"{2}\"/>", index, endPoint, objectId);
            }

            xml.Append("</object>");


            return string.Format("<script>opener.PosseAppendChangesXML(unescape('{0}')); opener.PosseSubmit(null); window.close();</script>",
              xml);
        }
        #endregion
    }
}