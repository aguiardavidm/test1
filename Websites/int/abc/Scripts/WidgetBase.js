/// External javascript references required for Intellisense. -------------------------------------
/// <reference path='../extjs/ext-all-debug.js' />

Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*','Ext.data.*']);

Ext.namespace('Computronix.POSSE.Dashboard');
Ext.namespace('Ext.ux');

// Fit to Parent plugin
Ext.define('Ext.ux.FitToParent', {
    alias: 'plugin.fittoparent',
    extend: 'Ext.AbstractPlugin',
    /**
    * @cfg {HTMLElement/Ext.Element/String} parent The element to fit the component size to (defaults to the element the component is rendered to).
    */
    /**
    * @cfg {Boolean} fitWidth If the plugin should fit the width of the component to the parent element (default <tt>true</tt>).
    */
    fitWidth: true,
    /**
    * @cfg {Boolean} fitHeight If the plugin should fit the height of the component to the parent element (default <tt>true</tt>).
    */
    fitHeight: true,
    /**
    * @cfg {Boolean} offsets Decreases the final size with [width, height] (default <tt>[0, 0]</tt>).
    */
    offsets: [0, 0],

    /**
    * @constructor
    * @param {HTMLElement/Ext.Element/String/Object} config The parent element or configuration options.
    * @ptype fittoparent
    */
    constructor: function (config) {
        config = config || {};
        if (config.tagName || config.dom || Ext.isString(config)) {
            config = { parent: config };
        }
        Ext.apply(this, config);
    },

    init: function (grid) {
        grid.on('render', this.onRender, this, { single: true });
    },

    onRender: function (grid) {
        var me = this;
        me.component = grid;
        me.parent = Ext.get(me.parent || grid.getPositionEl().dom.parentNode);
        if (grid.doLayout) {
            grid.monitorResize = true;
            grid.doLayout = Ext.Function.createInterceptor(grid.doLayout, me.fitSize, me);
        } /*else {
           me.fitSize();
           Ext.EventManager.onWindowResize(me.fitSize, me);
        }*/
        Ext.EventManager.onWindowResize(me.fitSize, me);
    },

    fitSize: function () {
        var me = this;
        var pos = me.component.getPosition(true),
            size = me.parent.getViewSize();
        me.component.setSize(
            me.fitWidth ? size.width - pos[0] - me.offsets[0] : undefined,
            me.fitHeight ? size.height - pos[1] - me.offsets[1] : undefined);
        return true;
    }
});

// Defining a model for a User
Ext.define('User', {
    extend: 'Ext.data.Model',
    fields: [
            { name: 'UserId', type: 'int' },
            { name: 'name', type: 'string' }
        ]
});

// Defining a model for a Time Period
Ext.define('TimePeriod', {
    extend: 'Ext.data.Model',
    fields: [
            { name: 'TimePeriodId', type: 'int' },
            { name: 'name', type: 'string' }
        ]
});

// Defining a model for Custom Criteria
Ext.define('CustomCriteria', {
    extend: 'Ext.data.Model',
    fields: [
            { name: 'CriteriaId', type: 'int' },
            { name: 'name', type: 'string' }
        ]
});

// Define the theme to use
Ext.chart.theme.White = Ext.extend(Ext.chart.theme.Base, {
    constructor: function () {
        Ext.chart.theme.White.superclass.constructor.call(this, {
            axis: {
                stroke: 'rgb(8,69,148)',
                'stroke-width': 1
            },
            axisLabel: {
                fill: 'rgb(8,69,148)',
                font: '12px Arial',
                'font-family': '"Arial',
                spacing: 2,
                padding: 5,
                renderer: function (v) { return v; }
            },
            axisTitle: {
                font: 'bold 18px Arial'
            }
        });
    }
});

// Define the default background
Computronix.POSSE.Dashboard.defaultBackground = {
    gradient: {
        id: 'backgroundGradient',
        angle: 45,
        stops: {
            0: {
                color: '#ffffff'
            },
            100: {
                color: '#eaf1f8'
            }
        }
    }
}

// TODO: remove this global when all references are updated
var defaultBackground = Computronix.POSSE.Dashboard.defaultBackground;

// I don't know of other shapes that work.
// 'square' displays in the legend, but displays as a circle on the line
Computronix.POSSE.Dashboard.shapes = ['circle', 'cross', 'plus', 'diamond', 'triangle'];
Computronix.POSSE.Dashboard.colours = ['#00ffff', // aqua
           '#000000', // black
           '#0000ff', // blue
           '#ff00ff', // fuchsia
           '#808080', // gray
           '#008000', // green
           '#00ff00', // lime
           '#800000', // maroon
           '#000080', // navy
           '#808000', // olive
           '#800080', // purple
           '#ff0000', // red
           '#c0c0c0', // silver
           '#008080', // teal
           '#ffff00']; // yellow

// Define a sequence to use for models - necessary to prevent name collisions for multiple widgets
Computronix.POSSE.Dashboard.modelSeq = 0;
Computronix.POSSE.Dashboard.defineModel = function (name, data) {
    Computronix.POSSE.Dashboard.modelSeq++;
    return Ext.define(name + Computronix.POSSE.Dashboard.modelSeq, data);
};

/*---------------------------------------------------------------------------*\
- function createPeriodCriteria
-  Create the criteria widgets for a Period criteria
\*---------------------------------------------------------------------------*/
Computronix.POSSE.Dashboard.createPeriodCriteria = function (widget) {
    var timeperiodDropDownListStore, timeperiods, criteriaPeriod;

    /*************************************************************************\
    * Create the time period widgets
    \*************************************************************************/

    // Define Time Periods
    if (widget.widgetInfo.FiscalYearEndDate == ''){
    timeperiods = {
         "data": [{ "TimePeriodId": 1, "name": "Current week" },
             { "TimePeriodId": 2, "name": "Current month" },
             { "TimePeriodId": 3, "name": "Current quarter" },
             { "TimePeriodId": 4, "name": "Current year" },
             { "TimePeriodId": 5, "name": "Current year to date" },
             { "TimePeriodId": 6, "name": "Previous week" },
             { "TimePeriodId": 7, "name": "Previous month" },
             { "TimePeriodId": 8, "name": "Previous quarter" },
             { "TimePeriodId": 9, "name": "Previous year" },
             { "TimePeriodId": 10, "name": "Last 3 months" },
             { "TimePeriodId": 11, "name": "Last 6 months" },
             { "TimePeriodId": 12, "name": "Last 12 months" },
             { "TimePeriodId": 14, "name": "Custom" }
            ]
    };
    } else {
        timeperiods = {
            "data": [{ "TimePeriodId": 1, "name": "Current week" },
             { "TimePeriodId": 2, "name": "Current month" },
             { "TimePeriodId": 3, "name": "Current quarter" },
             { "TimePeriodId": 4, "name": "Current year" },
             { "TimePeriodId": 5, "name": "Current year to date" },
             { "TimePeriodId": 6, "name": "Previous week" },
             { "TimePeriodId": 7, "name": "Previous month" },
             { "TimePeriodId": 8, "name": "Previous quarter" },
             { "TimePeriodId": 9, "name": "Previous year" },
             { "TimePeriodId": 9, "name": "Previous fiscal year" },
             { "TimePeriodId": 9, "name": "Next fiscal year" },
             { "TimePeriodId": 10, "name": "Last 3 months" },
             { "TimePeriodId": 11, "name": "Last 6 months" },
             { "TimePeriodId": 12, "name": "Last 12 months" },
             { "TimePeriodId": 14, "name": "Custom" }
            ]
    };
    }

    // The data store holding the time periods
    timeperiodDropDownListStore = Ext.create('Ext.data.Store', {
        autoLoad: true,
        model: 'TimePeriod',
        data: timeperiods,
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                root: 'data'
            }
        }
    });

    // Create the period drop and and dates
    criteriaPeriod = Ext.create('Ext.panel.Panel', {
        border: 0,
        height: 35,
        padding: '5 5 5 5',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'fieldcontainer',
            itemId: 'criteriaPeriod',
            width: 100,
            height: 25,
            layout: 'hbox',
            items: [{
                xtype: 'combo',
                itemId: 'period',
                fieldLabel: 'Period',
                displayField: 'name',
                width: 50,
                maxWidth: 200,
                labelWidth: 50,
                store: timeperiodDropDownListStore,
                queryMode: 'local',
                editable: false,
                value: widget.widgetInfo.period == '' ? widget.widgetInfo.defaultperiod : widget.widgetInfo.period,
                flex: 1.5,
                listeners: {
                    'select': function (item, value, options) {
                        if (value[0].data.name == 'Custom') {
                            this.ownerCt.getComponent('fromDate').enable();
                            this.ownerCt.getComponent('toDate').enable();
                        } else {
                            this.ownerCt.getComponent('fromDate').disable();
                            this.ownerCt.getComponent('toDate').disable();
                        }
                    }
                }
            }, {
                xtype: 'splitter',
                maxWidth: 40
            }, {
                xtype: 'datefield',
                fieldLabel: 'From',
                itemId: 'fromDate',
                value: widget.widgetInfo.fromdate,
                disabled: true,
                labelWidth: 30,
                maxWidth: 180,
                flex: 1,
                listeners: {
                    'change': function (item, value, options) {
                        enableCompareRadioButton();
                    }
                }
            }, {
                xtype: 'splitter',
                maxWidth: 40
            }, {
                xtype: 'datefield',
                fieldLabel: 'To',
                itemId: 'toDate',
                value: widget.widgetInfo.todate,
                disabled: true,
                labelWidth: 30,
                maxWidth: 180,
                flex: 1,
                listeners: {
                    'change': function (item, value, options) {
                        enableCompareRadioButton();
                    }
                }
            }, {
                xtype: 'splitter',
                maxWidth: 40
            }, {
                xtype: 'button',
                text: 'Go',
                itemId: 'goButton',
                handler: function (item, value, options) {
                    widget.reload();
                }
            }]
        }]
    });

    //Check for initial Custom value
    if (criteriaPeriod.getComponent('criteriaPeriod').getComponent('period').getValue() === 'Custom') {
        criteriaPeriod.getComponent('criteriaPeriod').getComponent('fromDate').enable();
        criteriaPeriod.getComponent('criteriaPeriod').getComponent('toDate').enable();
    }

    return criteriaPeriod;
}

/*---------------------------------------------------------------------------*\
- function createPeriodViewCriteria
-  Create the criteria widgets for a Period + View criteria
\*---------------------------------------------------------------------------*/
Computronix.POSSE.Dashboard.createPeriodViewCriteria = function (widget) {
    var timeperiodDropDownListStore, timeperiods, criteriaPeriod, criteriaView;

    /*-----------------------------------------------------------------------*\
    - enableCompareRadioButton
    -  Enable or disable the Compare radio button.
    \*-----------------------------------------------------------------------*/
    function enableCompareRadioButton() {
        var period = criteriaPeriod.getComponent('period').getValue();
        var tempDate = criteriaPeriod.getComponent('fromDate').getValue();
        var toDate = criteriaPeriod.getComponent('toDate').getValue();

        if (typeof (tempDate.getFullYear) != 'undefined') {
            tempDate.setFullYear(tempDate.getFullYear() + 1);
        }

        if (period == 'All' || (period == 'Custom' && toDate != null && tempDate != null && !(toDate - tempDate <= 0))) {
            criteriaView.getComponent('radioCompare').disable();
            if (criteriaView.getComponent('radioCompare').getValue()) {
                criteriaView.getComponent('radioSummary').setValue(true);
            }
        } else {
            criteriaView.getComponent('radioCompare').enable();
        }
    }

    /*************************************************************************\
    * Create the time period widgets
    \*************************************************************************/

    // Define Time Periods
    if (widget.widgetInfo.FiscalYearEndDate == '') {
        timeperiods = {
            "data": [{ "TimePeriodId": 1, "name": "Current week" },
             { "TimePeriodId": 2, "name": "Current month" },
             { "TimePeriodId": 3, "name": "Current quarter" },
             { "TimePeriodId": 4, "name": "Current year" },
             { "TimePeriodId": 5, "name": "Current year to date" },
             { "TimePeriodId": 6, "name": "Previous week" },
             { "TimePeriodId": 7, "name": "Previous month" },
             { "TimePeriodId": 8, "name": "Previous quarter" },
             { "TimePeriodId": 9, "name": "Previous year" },
             { "TimePeriodId": 10, "name": "Last 3 months" },
             { "TimePeriodId": 11, "name": "Last 6 months" },
             { "TimePeriodId": 12, "name": "Last 12 months" },
             { "TimePeriodId": 14, "name": "Custom" }
            ]
        };
    } else {
        timeperiods = {
            "data": [{ "TimePeriodId": 1, "name": "Current week" },
             { "TimePeriodId": 2, "name": "Current month" },
             { "TimePeriodId": 3, "name": "Current quarter" },
             { "TimePeriodId": 4, "name": "Current year" },
             { "TimePeriodId": 5, "name": "Current year to date" },
             { "TimePeriodId": 6, "name": "Previous week" },
             { "TimePeriodId": 7, "name": "Previous month" },
             { "TimePeriodId": 8, "name": "Previous quarter" },
             { "TimePeriodId": 9, "name": "Previous year" },
             { "TimePeriodId": 9, "name": "Previous fiscal year" },
             { "TimePeriodId": 9, "name": "Next fiscal year" },
             { "TimePeriodId": 10, "name": "Last 3 months" },
             { "TimePeriodId": 11, "name": "Last 6 months" },
             { "TimePeriodId": 12, "name": "Last 12 months" },
             { "TimePeriodId": 14, "name": "Custom" }
            ]
        };
    }

    // The data store holding the time periods
    timeperiodDropDownListStore = Ext.create('Ext.data.Store', {
        autoLoad: true,
        model: 'TimePeriod',
        data: timeperiods,
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                root: 'data'
            }
        }
    });

    // Create the period drop and and dates
    criteriaPeriod = Ext.create('Ext.form.FieldContainer', {
        itemId: 'criteriaPeriod',
        width: 100,
        height: 25,
        layout: 'hbox',
        items: [{
            xtype: 'combo',
            itemId: 'period',
            fieldLabel: 'Period',
            displayField: 'name',
            width: 50,
            maxWidth: 200,
            labelWidth: 50,
            store: timeperiodDropDownListStore,
            queryMode: 'local',
            editable: false,
            value: widget.widgetInfo.period == '' ? widget.widgetInfo.defaultperiod : widget.widgetInfo.period,
            flex: 1.5,
            listeners: {
                'select': function (item, value, options) {
                    enableCompareRadioButton();

                    if (value[0].data.name == 'Custom') {
                        this.ownerCt.getComponent('fromDate').enable();
                        this.ownerCt.getComponent('toDate').enable();
                    } else {
                        this.ownerCt.getComponent('fromDate').disable();
                        this.ownerCt.getComponent('toDate').disable();
                    }
                }
            }
        }, {
            xtype: 'splitter',
            maxWidth: 40
        }, {
            xtype: 'datefield',
            fieldLabel: 'From',
            itemId: 'fromDate',
            value: widget.widgetInfo.fromdate,
            disabled: true,
            labelWidth: 30,
            maxWidth: 180,
            flex: 1,
            listeners: {
                'change': function (item, value, options) {
                    enableCompareRadioButton();
                }
            }
        }, {
            xtype: 'splitter',
            maxWidth: 40
        }, {
            xtype: 'datefield',
            fieldLabel: 'To',
            itemId: 'toDate',
            value: widget.widgetInfo.todate,
            disabled: true,
            labelWidth: 30,
            maxWidth: 180,
            flex: 1,
            listeners: {
                'change': function (item, value, options) {
                    enableCompareRadioButton();
                }
            }
        }, {
            xtype: 'splitter',
            maxWidth: 40
        }, {
            xtype: 'button',
            text: 'Go',
            itemId: 'goButton',
            handler: function (item, value, options) {
                widget.reload();
            }
        }]
    });

    // Create the view radio buttons
    criteriaView = Ext.create('Ext.form.FieldContainer', {
        itemId: 'criteriaView',
        height: 25,
        fieldLabel: 'View',
        labelWidth: 50,
        layout: 'hbox',
        defaults: {
            layout: '100%',
            margin: '0 15 0 0'
        },
        items: [{
            xtype: 'radio',
            itemId: 'radioDetailed',
            boxLabel: 'Detailed',
            name: 'graphview' + criteriaPeriod.getId(),
            value: 'detailed',
            checked: (widget.widgetInfo.view == 'Detailed'),
            disabled: widget.widgetInfo.subgroup == '' && widget.widgetInfo.widgettype != 'DaysToCompleteByStaff' && widget.widgetInfo.group1label == ''
        }, {
            xtype: 'radio',
            itemId: 'radioSummary',
            boxLabel: 'Summary',
            name: 'graphview' + criteriaPeriod.getId(),
            value: 'summary',
            checked: (widget.widgetInfo.view == 'Summary')
        }, {
            xtype: 'radio',
            itemId: 'radioCompare',
            boxLabel: 'Compare Previous Years',
            name: 'graphview' + criteriaPeriod.getId(),
            value: 'compare',
            checked: (widget.widgetInfo.view == 'Compare')
        }]
    });

    return Ext.create('Ext.panel.Panel', {
        border: 0,
        height: 60,
        padding: '5 5 5 5',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [criteriaPeriod, criteriaView]
    });
}

// Computronix.POSSE.Dashboard.Ajax
//=============================================================================
// An Ajax queuing mechanism.  This is to work around an Oracle session pooling bug which Oracle patch 11.2.0.1.11 fixes
// If the patch has been applied on this machine then set enableAjaxQueue to false
Computronix.POSSE.Dashboard.enableAjaxQueue = true;
Computronix.POSSE.Dashboard.Ajax = {
    _queue: [],
    _processing: false,
    request: function (config) {
        if (!Computronix.POSSE.Dashboard.enableAjaxQueue) {
            Ext.Ajax.request(config);
        } else {
            var that = this;
            var processNextRequest = function () {
                if (that._queue.length > 0 && !that._processing) {
                    that._processing = true;
                    var obj = that._queue.splice(0, 1)[0];

                    Ext.Ajax.request({
                        url: obj.url,
                        method: obj.method,
                        scope: obj.scope,
                        params: obj.params,
                        success: function (response, opts) {
                            that._processing = false;
                            processNextRequest();
                            if (obj.success)
                                obj.success.call(this, response, opts);
                        },
                        failure: function (response, opts) {
                            that._processing = false;
                            processNextRequest();
                            if (obj.failure)
                                obj.failure.call(this, response, opts);
                        }
                    });
                }
            }

            this._queue.push(config);
            if (!this._processing) {
                processNextRequest();
            }
        }
    }
}


Ext.define('Computronix.POSSE.Dashboard.Widget', {
    extend: 'Ext.panel.Panel',
    alias: 'dashboardwidget',
    // Data Members
    //=============================================================================
    //    
    widgetid: null,
    widgetInfo: null,
    data: [],
    chartMask: null,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    // Methods
    //=============================================================================
    //    
    initComponent: function () {
        var widget = this;

        this.callParent();
        this.setTitle(this.widgetInfo.title);

        this.widgetid = this.widgetInfo.widgetid;
        this.data = this.widgetInfo.data;

        var disabled = false;
        if (this.widgetInfo.allowexport == true) {
            disabled = false;
        }
        else {
            disabled = true;
        }

        // Set up the drill down toolbar if applicable
        if (this.drillDownSupported) {
            this.addDocked({
                xtype: 'toolbar',
                dock: 'top',
                itemId: 'topToolbar',
                items: [{
                    xtype: 'button',
                    itemId: 'backButton',
                    text: 'Back',
                    cls: 'x-btn-text-icon',
                    icon: 'extjs/resources/themes/images/default/grid/dd-insert-arrow-left.png',
                    disabled: true,
                    handler: function () {
                        widget.drillBack();
                    }
                }, {
                    xtype: 'tbseparator'
                }, {
                    xtype: 'tbtext',
                    itemId: 'groupBreadcrumb',
                    text: 'Top'
                }, {
                    xtype: 'tbfill'
                }, {
                    xtype: 'button',
                    itemId: 'exportButton',
                    text: 'Export',
                    icon: 'images/icons/Export.png',
                    disabled: disabled,
                    handler: function () {
                        widget.Export();
                    }
                }
                ]
            });

            this.backButton = this.getComponent('topToolbar').getComponent('backButton');
            this.groupBreadcrumb = this.getComponent('topToolbar').getComponent('groupBreadcrumb');
        }
        else {
            var disabled = false;
            if (this.widgetInfo.allowexport == true) {
                disabled = false;
            }
            else {
                disabled = true;
            }
            this.addDocked({
                xtype: 'toolbar',
                dock: 'top',
                itemId: 'topToolbar',
                items: [{
                    xtype: 'tbfill'
                }, {
                    xtype: 'button',
                    itemId: 'exportButton',
                    text: 'Export',
                    icon: 'images/icons/Export.png',
                    disabled: disabled,
                    handler: function () {
                        widget.Export();
                    }
                }
                    ]
            });
        }

        // Set current grouping level
        this.currentGroupLevel = 1;

        //Check for widget info parameters and set group level
        if (this.widgetInfo.group1value !== null && typeof(this.widgetInfo.group1value) !== 'undefined') {
            this.currentGroupLevel = 2; }
        if (this.widgetInfo.group2value !== null && typeof(this.widgetInfo.group2value) !== 'undefined') {
            this.currentGroupLevel = 3; }
        if (this.widgetInfo.group3value !== null && typeof(this.widgetInfo.group3value) !== 'undefined') {
            this.currentGroupLevel = 4; }
        if (this.widgetInfo.group4value !== null && typeof(this.widgetInfo.group4value) !== 'undefined') {
            this.currentGroupLevel = 5; }


        //if (this.widgetInfo.displaytwogroups == 'Y') {
        //    this.currentGroupLevel = 2;
        //}

        // Create the panel for the graph
        this.chartPanel = Ext.create('Ext.panel.Panel', {
            flex: 1,
            layout: 'fit',
            border: 0
        });
        this.chartMask = new Ext.LoadMask(this.chartPanel, { msg: "Loading..." });
        this.add(this.chartPanel);

        // Create the criteria
        this.add(this.createCriteria());

        // Load the chart
        this.renderTo.add(this);
        this.doLayout();
        this.reload();

        //Don't know if this is the right place for this, but need to refresh breadcrumbs...
        if (widget.currentGroupLevel > 1) {
            widget.refreshGroupBreadcrumb();
            widget.backButton.enable();
        }
    },

    // Each widget supplies createCriteria, createChart, and getCriteriaValues
    createCriteria: function () { return []; },

    getCriteriaValues: function () { return {}; },

    createChart: function () { },

    showMask: function () { this.chartMask.show(); },
    hideMask: function () { this.chartMask.hide(); },

    Export: function () {
        // Default settings
        var config = {};

        config.arguments = this.getCriteriaValues();

        config.success = config.success || function () { };
        config.failure = config.failure || function () { };

        var i, j;
        for (i = 1; i <= 4; i++) {
            if (this.widgetInfo['group' + i + 'value']) {
                config.arguments['group' + i + 'value'] = this.widgetInfo['group' + i + 'value'];
            }
        }

        for (j = 1; j <= 4; j++) {
            if (this.widgetInfo['group' + j + 'nottopnvalues']) {
                config.arguments['group' + j + 'nottopnvalues'] = this.widgetInfo['group' + j + 'nottopnvalues'];
            }
        }

        var name;
        var widget = this;
        var url = 'WidgetHandler.ashx?action=ExportObjects&widgetid=' + this.widgetid;
        for (name in config.arguments) {
            if (config.arguments[name]) {
                if (config.arguments[name].constructor == Date) {
                    url += '&' + name + '=' + Ext.util.Format.date(config.arguments[name]);
                } else {
                    url += '&' + name + '=' + encodeURIComponent(config.arguments[name]);
                }
            }
        }

        //Use a hidden from to submit the request to WidgetHandler for Export.  THis will prompt a download.
        var hiddenForm = Ext.create('Ext.form.Panel', {
            renderTo: Ext.getBody(),
            title: 'hiddenForm',
            standardSubmit: true,
            url: url,
            height: 0,
            width: 0,
            hidden: true
        });

        hiddenForm.getForm().submit();
    },

    drillDown: function (item, subGroupIndex) {
        var i, value, subGroupValue, drillDownUrl, nextLevel;
        subGroupValue = null;
        nextLevel = this.currentGroupLevel + 1;

        // If this is a timeline chart, then the second level group is the actual group value
        if ((this.widgetInfo.widgettype == "ObjectSummary") && (this.widgetInfo.displaytype == "Timeline")) {
            value = this.widgetInfo.serieslabels[Ext.Array.indexOf(this.seriesNames, item.series.yField)];
        } else {
            value = item.storeItem.data.x;
            if (this.widgetInfo.displaytwogroups && nextLevel == 2) {
                subGroupValue = this.widgetInfo.serieslabels[subGroupIndex];
                nextLevel += 1;
            }
        }

        if (this.widgetInfo['group' + (nextLevel) + 'label']) {
            this.widgetInfo['group' + (this.currentGroupLevel) + 'value'] = value;
            if (this.widgetInfo.nottopnvalues) {
                this.widgetInfo['group' + (this.currentGroupLevel) + 'nottopnvalues'] = this.widgetInfo.nottopnvalues;
            }
            if (subGroupValue) {
                this.widgetInfo['group' + (this.currentGroupLevel + 1) + 'value'] = subGroupValue;
            }
            this.currentGroupLevel = nextLevel;
            this.backButton.setDisabled(false);
            this.refreshGroupBreadcrumb();
            this.reload();
        } else if (this.widgetInfo.drilldownhandling == "Navigate to List Presentation") {
            var href = this.widgetInfo.drilldownurl;

            // Replace group values if we've drilled down
            for (i = 1; i < this.currentGroupLevel; i++) {
                href = href.replace('{' + (i - 1) + '}', this.widgetInfo['group' + i + 'value'])
            }

            // Replace values selected from the chart
            href = href.replace('{' + (this.currentGroupLevel - 1) + '}', value);
            if (subGroupValue) {
                href = href.replace('{' + (this.currentGroupLevel) + '}', subGroupValue);
            }
            href = href.replace('{fromdate}', Ext.util.Format.date(this.widgetInfo.fromdate, 'Y-m-d'));
            href = href.replace('{todate}', Ext.util.Format.date(this.widgetInfo.todate, 'Y-m-d'));
            window.open(href, "_blank");
        } else if (this.widgetInfo.drilldownhandling == "Display Objects") {
            //Make an AJAX request to GetDrillDownObjects and display a grid of the objects represented on the current chart level.
            var config = { };
            config.arguments = this.getCriteriaValues();
            config.success = config.success || function () { };
            config.failure = config.failure || function () { };

            //Include the last clicked on value so it is incorporated into the select statement for the grid popup
            this.widgetInfo['group' + (this.currentGroupLevel) + 'value'] = value;
            if (this.widgetInfo.nottopnvalues) {
                this.widgetInfo['group' + (this.currentGroupLevel) + 'nottopnvalues'] = this.widgetInfo.nottopnvalues;
            }
            if (subGroupValue) {
                this.widgetInfo['group' + (this.currentGroupLevel + 1) + 'value'] = subGroupValue;
            }

            //Refresh the breadcrumb so you can click another value after closing the grid.
            if (this.currentGroupLevel < 5) {
                this.currentGroupLevel = nextLevel;
                this.refreshGroupBreadcrumb();
            }

            //Add clicked on values into the arguments.
            var i, j;
            for (i = 1; i <= 4; i++) {
                if (this.widgetInfo['group' + i + 'value']) {
                    config.arguments['group' + i + 'value'] = this.widgetInfo['group' + i + 'value'];
                }
            }

            for (j = 1; j <= 4; j++) {
                if (this.widgetInfo['group' + j + 'nottopnvalues']) {
                    config.arguments['group' + j + 'nottopnvalues'] = this.widgetInfo['group' + j + 'nottopnvalues'];
                }
            }

            //build url from the arguments
            var name;
            var widget = this;
            var url = 'WidgetHandler.ashx?action=GetDrillDownObjects&widgetid=' + this.widgetid;
            for (name in config.arguments) {
                if (config.arguments[name]) {
                    if (config.arguments[name].constructor == Date) {
                        url += '&' + name + '=' + Ext.util.Format.date(config.arguments[name]);
                    } else {
                        url += '&' + name + '=' + encodeURIComponent(config.arguments[name]);
                    }
                }
            }
            //make AJAX request for drill-down objects
            this.showMask();
            Computronix.POSSE.Dashboard.Ajax.request({
                url: url,
                method: 'GET',
                scope: widget,
                success: function (response, opts) {
                    if (!widget.rendered)
                        return;
                    this.hideMask();
                    var results = Ext.JSON.decode(response.responseText);
                    if (results) {
                        if (results.error) {
                            alert("Error: " + results.error.message);
                        }
                        else {
                            //define the objects from the JSON and create the columns for the grid.
                            var object = results[0];

                            var columndefs = '[';
                            for (var i = 0; i < object.fieldnames.length; i++) {

                                columndefs = columndefs + '{text: ' + '"' + object.fieldnames[i] + '"' + ', width: ' + (object.fieldnames[i].length * 9);

                                if (i == (object.fieldnames.length - 1)) {
                                    columndefs = columndefs + ', flex: 1, dataIndex: ' + '"' + object.fieldnames[i] + '"'; //Make the last column stretch to the width of the grid.
                                }
                                else {
                                    columndefs = columndefs + ', dataIndex: ' + '"' + object.fieldnames[i] + '"';
                                }

                                if (i == (object.fieldnames.length - 1)) {
                                    columndefs = columndefs + '}';
                                }
                                else {
                                    columndefs = columndefs + '}, ';
                                }
                            }

                            columndefs = columndefs + ']';

                            //define the data structure
                            Ext.define('GridObject', {
                                extend: 'Ext.data.Model',
                                fields: object.fieldnames
                            });

                            //create a store of "GridObject" and fill it with data from the results of the request
                            var GridStore = Ext.create('Ext.data.Store', {
                                model: 'GridObject',
                                data: object.data
                            });

                            //display a grid with the new data
                            var grid = Ext.create('Ext.grid.Panel', {
                                renderTo: Ext.getBody(),
                                store: GridStore,
                                width: 750,
                                height: 450,
                                columns: Ext.decode(columndefs)

                            });

                            var myWin = Ext.create("Ext.Window", {
                                layout: 'fit',
                                title: 'Drill-Down Objects',
                                width: 800,
                                height: 500,
                                closable: true,
                                buttonAlign: 'center',
                                items: [grid],
                                modal: true
                            });
                            myWin.show();

                            config.success();
                            this.drillBack(); //This allows you to pick another value after closing the popup.
                        }
                    } else {
                        //I put the error message into an alert so the chart will not continue to look like it its loading, and then it drills back to reset
                        alert("GetDrillDownObjects failed (no results) for widgetid " + widget.widgetid);
                        config.failure();
                        widget.drillBack();
                    }
                },
                failure: function (response, opts) {
                    if (!widget.rendered)
                        return;
                    this.hideMask();

                    //I put the error message into an alert so the chart will not continue to look like it is loading, and then it drills back to reset
                    var results = Ext.JSON.decode(response.responseText);
                    alert("GetDrillDownObjects failed for widgetid " + widget.widgetid + ': ' + results);
                    config.failure();
                    widget.drillBack();
                }
            });
        }

    },

    drillBack: function () {
        var currentLevel = this.currentGroupLevel;
        var nextLevel = (currentLevel - 1);
        this.widgetInfo['group' + (nextLevel) + 'value'] = null;
        this.widgetInfo['group' + (nextLevel) + 'nottopnvalues'] = null;

        if (nextLevel == 2 && this.widgetInfo.displaytwogroups) {
            nextLevel = 1;
            this.widgetInfo['group' + (nextLevel) + 'value'] = null;
            this.widgetInfo['group' + (nextLevel) + 'nottopnvalues'] = null;
        }

        this.widgetInfo.nottopnvalues = this.widgetInfo['group' + (nextLevel) + 'nottopnvalues'];
        this.backButton.setDisabled((nextLevel == 1));
        this.currentGroupLevel = nextLevel;
        this.refreshGroupBreadcrumb();
        this.reload();
    },

    refreshGroupBreadcrumb: function () {
        var text = 'Top';
        for (i = 1; i <= 4; i++) {
            if (this.widgetInfo['group' + i + 'value']) {
                if (text) {
                    text += ' > ';
                }
                text += this.widgetInfo['group' + i + 'value'];
            }
        }
        this.groupBreadcrumb.setText(text);
    },

    getXAxisLabel: function () {
        var labelLevel = this.currentGroupLevel;
        if (labelLevel == 1) {
            return this.widgetInfo.group1label || this.widgetInfo.xaxislabel;
        }
        else {
            return this.widgetInfo['group' + labelLevel + 'label'];
        }
    },

    loadData: function (config) {
        // Default settings
        config.arguments = config.arguments || {};
        config.success = config.success || function () { };
        config.failure = config.failure || function () { };

        var name;
        var widget = this;
        var url = 'WidgetHandler.ashx?action=GetWidgetData&widgetid=' + this.widgetid;
        for (name in config.arguments) {
            if (config.arguments[name]) {
                if (config.arguments[name].constructor == Date) {
                    url += '&' + name + '=' + Ext.util.Format.date(config.arguments[name]);
                } else {
                    url += '&' + name + '=' + encodeURIComponent(config.arguments[name]);
                }
            }
        }
        Computronix.POSSE.Dashboard.Ajax.request({
            url: url,
            method: 'GET',
            scope: widget,
            success: function (response, opts) {
                if (!widget.rendered)
                    return;
                var results = Ext.JSON.decode(response.responseText);
                if (results) {
                    if (results.error) {
                        alert("Error: " + results.error.message);
                    }
                    else {
                        this.widgetInfo = results[0];
                        this.data = results[0].data;
                        this.seriesNames = this.widgetInfo.fieldnames.split(',').slice(1);
                        config.success();
                    }
                } else {
                    alert("GetWidgetData failed (no results) for widgetid " + widget.widgetid);
                    config.failure();
                }
            },
            failure: function (response, opts) {
                if (!widget.rendered)
                    return;
                var results = Ext.JSON.decode(response.responseText);
                var message = "";
                if (results)
                    message = results.Message;
                alert("GetWidgetData failed for widgetid " + widget.widgetid + ': ' + message);
                config.failure();
            }
        });
    },

    refresh: function () {
        this.chartPanel.removeAll();
        this.hideMask();

        if (this.refreshCriteria) {
            this.refreshCriteria();
        }

        var chart = this.createChart();
        this.chartPanel.add(chart);
        chart.hide();
        chart.show();
    },

    reload: function () {
        var args = this.getCriteriaValues();
        var i, j;
        // Add drill down group values to the arguments
        for (i = 1; i <= 4; i++) {
            if (this.widgetInfo['group' + i + 'value']) {
                args['group' + i + 'value'] = this.widgetInfo['group' + i + 'value'];
            }
        }

        for (j = 1; j <= 4; j++) {
            if (this.widgetInfo['group' + j + 'nottopnvalues']) {
                args['group' + j + 'nottopnvalues'] = this.widgetInfo['group' + j + 'nottopnvalues'];
            }
        }

        var widget = this;
        this.showMask();
        this.loadData({
            arguments: args,
            success: function () {
                widget.refresh();
            },
            failure: function () {
                widget.refresh();
            }
        });
    },

    // Events
    //=============================================================================
    //    
    listeners: {
        'removed': function (widget, ownerCt, eOpts) {
            widget.hideMask();
        }
    }

});

Computronix.POSSE.Dashboard.widgetClasses = {
    'ObjectIntakeRate': 'Computronix.POSSE.Dashboard.ObjectIntakeRateWidget',
    'CustomCode': 'Computronix.POSSE.Dashboard.CustomCode',
    'ToDoListSummary': 'Computronix.POSSE.Dashboard.ToDoList',
    'ObjectSummary': 'Computronix.POSSE.Dashboard.ObjectSummaryWidget',
    'JobInStatus': 'Computronix.POSSE.Dashboard.JobInStatus',
    'WarningGaugeCustom': 'Computronix.POSSE.Dashboard.WarningGaugeWidget',
    'WarningGauge': 'Computronix.POSSE.Dashboard.WarningGaugeWidget',
    'ProcessCompletionProgressGauge': 'Computronix.POSSE.Dashboard.WarningGaugeWidget',
    'DaysToCompleteByStaff': 'Computronix.POSSE.Dashboard.DaysToCompleteByStaff',
    'DaysToCompleteSummary': 'Computronix.POSSE.Dashboard.DaysToCompleteSummary'
};

Computronix.POSSE.Dashboard.CreateWidget = function (widgetId, renderTo, width, height) {
    function loadWidget(widgetInfo) {
        var panel = Ext.create("Ext.panel.Panel", {
            layout: 'fit',
            border: 0,
            renderTo: renderTo,
            width: width,
            height: height
        });

        var widget = Ext.create(Computronix.POSSE.Dashboard.widgetClasses[widgetInfo.widgettype], {
            widgetInfo: widgetInfo,
            renderTo: panel
        });
    }

    Computronix.POSSE.Dashboard.Ajax.request({
        url: 'WidgetHandler.ashx?action=GetWidgetInfo&WidgetId=' + widgetId,
        method: 'GET',
        success: function (response, opts) {
            try {
                var results = Ext.JSON.decode(response.responseText);
            } catch (err) {
                alert(err);
            }
            if (results.error) {
                alert("Error: " + results.error.message);
            }
            else {
                loadWidget(results, renderTo, 480);
            }
        },
        failure: function (response, opts) {
            try {
                var results = Ext.JSON.decode(response.responseText);
                if (results)
                    alert(results.Message);
            } catch (err) {
                alert(err);
            }
        }
    });

}