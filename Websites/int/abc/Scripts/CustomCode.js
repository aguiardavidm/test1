/// External javascript references required for Intellisense. -------------------------------------
/// <reference path='../extjs/ext-all-debug.js' />
/// <reference path='../Scripts/WidgetBase.js' />

Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*','Ext.data.*']);

/*---------------------------------------------------------------------------*\
- function createObjectIntakeChart
-  Create the chart
\*---------------------------------------------------------------------------*/
function customCodeChart(widget) {
    var graphSeries, modelFields, seriesLabelArray, seriesNameArray;
    var markers = [], markerLength = 0;
    var customCodeStore;
    var i;

    /*---------------------------------------------------------------------------*\
    - function getNextMarker
    -   Randomly return the next marker, but make sure you go through all the
    - possible markers before you start recycling markers.
    \*---------------------------------------------------------------------------*/
    function getNextMarker() {
      if (markerLength == 0) {
        markerLength = markers.length;
        return markers[0];
      } else {
        var m = Math.floor(Math.random() * markerLength);
        var chosen = markers[m];
        markers[m] = markers[--markerLength];
        markers[markerLength] = chosen;
        return chosen;
      }
    }


    // Set up the markers
    for (s in Computronix.POSSE.Dashboard.shapes) {
      for (c in Computronix.POSSE.Dashboard.colours) {
        markers[markerLength++] = { type: Computronix.POSSE.Dashboard.shapes[s], size: 4, radius: 4, 'fill' : Computronix.POSSE.Dashboard.colours[c] }
      }
    }

    // Create the graph Model
    graphSeries = new Array;
    modelFields = new Array;
    modelFields[0] = { name: 'x', type: 'string' };
    seriesLabelArray = widget.widgetInfo.serieslabels;
    seriesNameArray = widget.widgetInfo.fieldnames.split(',').slice(1);

    if (widget.widgetInfo.displaytype = 'Line Graph') {
        widget.widgetInfo.graphtype = 'line';
    } else if (widget.widgetInfo.displaytype = 'Bar Graph') {
        widget.widgetInfo.graphtype = 'column';
    }

    // create the series to display and specify the model;
    for (i = 0; i < seriesNameArray.length; i++) {
        modelFields[i + 1] = { name: seriesNameArray[i], type: 'int' };
        // for line graphs, we need one series per line
        if (widget.widgetInfo.graphtype == 'line') {
            graphSeries[i] = {
                type: widget.widgetInfo.graphtype,
                axis: 'left',
                highlight: { size: 1 },
                itemId: seriesNameArray[i],
                markerConfig: getNextMarker(),
                selectionTolerance: 5,
                title: seriesLabelArray[i],
                xField: 'x',
                yField: seriesNameArray[i],
                showMarkers: widget.widgetInfo.showmarkers,
                smooth: widget.widgetInfo.smooth,
                tips: {
                    trackMouse: true,
                    minWidth: 200,
                    minHeight: 26,
                    width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
                    height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
                    renderer: function (storeItem, item) {
                        if (widget.widgetInfo.xaxisisnumeric) {
                            this.setTitle(widget.widgetInfo.xaxislabel + ': ' + storeItem.get('x') + ', '
                                + widget.widgetInfo.yaxislabel + ': ' + item.value[1]);
                        } else {
                            this.setTitle(storeItem.get('x') + ': ' + Ext.util.Format.plural(item.value[1], 'object'));
                        }
                    }
                }
            };
        }
    }

    // for column graphs, we need only one series
    if (widget.widgetInfo.graphtype == 'column') {
        graphSeries[0] = {
            type: widget.widgetInfo.graphtype,
            axis: 'left',
            highlight: { size: 1 },
            itemId: 'customCodeGraphSeries',
            markerConfig: getNextMarker(),
            selectionTolerance: 5,
            title: seriesLabelArray,
            yField: seriesNameArray,
            label: {
                display: seriesNameArray.length * widget.data.length <= 15 ? 'insideEnd' : 'none',
                'text-anchor': 'middle',
                orientation: 'horizontal',
                color: '#333',
                field: seriesNameArray,
                renderer: Ext.util.Format.numberRenderer('0,0')
            },
            tips: {
                trackMouse: true,
                minWidth: 200,
                minHeight: 26,
                width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
                height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
                renderer: function (storeItem, item) {
                    this.setTitle(storeItem.get('x') + ': ' + Ext.util.Format.plural(item.value[1], 'object'));
                }
            }
        };
    }

    theModel = Computronix.POSSE.Dashboard.defineModel('CustomCode', {
        extend: 'Ext.data.Model',
        fields: modelFields
    });


    customCodeStore = Ext.create('Ext.data.Store', {
        autoLoad: true,
        model: theModel,
        data: widget,
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                root: 'data'
            }
        }
    });


    /*************************************************************************\
    * Create the graph
    *  Default the graph to have at least height of 5 on the y axis.
    \*************************************************************************/
    var graphAxes = [{
        type: 'Numeric',
        position: 'left',
        fields: seriesNameArray,
        grid: true,
        title: widget.widgetInfo.yaxislabel,
        minimum: 0,
        maximum: function () {
            var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
            if (maxVal == 0) {
                return 10;
            } else if (maxVal <= 10) {
                return maxVal;
            }

            return undefined;
        } (),
        majorTickSteps: function () {
            var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
            if (maxVal % 2 == 1)
                maxVal -= 1;

            if (maxVal == 2) {
                return 1.4;
            } else if (maxVal < 10) {
                return maxVal - 0.5;
            } else {
                return 10;
            }
        } (),
        decimals: 0,
        grid: {
            odd: {
                opacity: 1,
                fill: '#ddd',
                stroke: '#bbb',
                'stroke-width': 0.5
            }
        }
    }];
    if (widget.widgetInfo.xaxisisnumeric) {
        graphAxes.push({
            type: 'Numeric',
            position: 'bottom',
            fields: ['x'],
            title: widget.widgetInfo.xaxislabel,
            majorTickSteps: 10,
            grid: true,
            label: {
                rotate: {
                    degrees: 300
                }
            }
        });
    } else {
        graphAxes.push({
            type: 'Category',
            position: 'bottom',
            fields: ['x'],
            title: widget.widgetInfo.xaxislabel,
            label: {
                rotate: {
                    degrees: function () {
                        if (widget.data.length > 12)
                            return 270;
                        else if (widget.data.length > 6)
                            return 300;
                        return 0;
                    } ()
                }
            }
        });
    }
        
    var graph = Ext.create('Ext.chart.Chart', {
        itemId: 'customChart',
        animate: true,
        shadow: true,
        theme: 'Base:gradients',
        background: Computronix.POSSE.Dashboard.defaultBackground,
        store: customCodeStore,
        xField: 'x',
        legend: seriesNameArray.length == 1 ? false : { position: 'right' },
        axes: graphAxes,
        series: graphSeries
    });

    return graph;
}

/*---------------------------------------------------------------------------*\
- function createCustomCriteria
-  Create the criteria widgets for Custom criteria
\*---------------------------------------------------------------------------*/
function createCustomCriteria(widget) {
    var dropdownListStore = [], customCriteria = [], i, j;
	var criteriaValues = widget.widgetInfo.chartCriteria;

	for (j = 0; j < criteriaValues.length; j++) {

		// Create the Items Array for the container below
		var itemsArray = [];
		if (criteriaValues[j].type == 'String Value') {
			itemsArray[0] = {
				xtype: 'textfield',
				itemId: criteriaValues[j].name + '_Criteria',
				fieldLabel: criteriaValues[j].label,
				selectOnFocus: true,
				width: 450,
				maxWidth: 450,
				labelWidth: 100,
				flex: 1.5
				};			
		} else if (criteriaValues[j].type == 'Select List') {
			var listValues = widget.widgetInfo.chartCriteria[j].listValues;
			var listValueArray = [];
			for (i = 0; i < listValues.length; i++) {
			  var obj = {"name": listValues[i]};
			  listValueArray[listValueArray.length] = obj;
			}
			var listValueData = {
					"data": listValueArray
			};

			// The data store holding the criteria
			dropdownListStore[j] = Ext.create('Ext.data.Store', {
				autoLoad: true,
				model: 'CustomCriteria',
				data: listValueData,
				proxy: {
					type: 'memory',
					reader: {
						type: 'json',
						root: 'data'
					}
				}
			});

			itemsArray[0] = {
				xtype: 'combo',
				itemId: criteriaValues[j].name + '_Criteria',
				fieldLabel: criteriaValues[j].label,
				displayField: 'name',
				width: 450,
				maxWidth: 450,
				labelWidth: 100,
				store: dropdownListStore[j],
				queryMode: 'local',
				typeAhead: true,
				selectOnFocus: true,
				flex: 1.5,
				listeners: {
					'select': function (item, value, options) {
					}
				}};		
		}
		// Go button only on first row
		if (j == 0) {
			itemsArray[1] = {
				xtype: 'splitter',
				maxWidth: 40
			};
			itemsArray[2] = {
				xtype: 'button',
				text: 'Go',
				itemId: 'goButton',
				handler: function (item, value, options) {
					widget.reload();
				}
			};
		} else {
			itemsArray[1] = {};
			itemsArray[2] = {};		
		}

		// Create the container for each criteria
		customCriteria[j] = Ext.create('Ext.form.FieldContainer', {
				itemId: criteriaValues[j].name+'_CriteriaFieldContainer',
				height: 25,
				layout: 'hbox',
				items: itemsArray
		});
	};

    return customCriteria;
}

Ext.define('Computronix.POSSE.Dashboard.CustomCode', {
    extend: 'Computronix.POSSE.Dashboard.Widget',

    createCriteria: function () {
        if (this.widgetInfo.chartCriteria) {
            var customCriteria = createCustomCriteria(this);
            var panelHeight = customCriteria.length * 30 + 5;

            this.criteriaPanel = Ext.create('Ext.panel.Panel', {
                border: 0,
                height: panelHeight,
                padding: '5 5 5 5',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: customCriteria
            });
			return this.criteriaPanel;
        } else {
            return [];
        }
    },
    getCriteriaValues: function () {
        var args = [], j;
        var criteriaValues = this.widgetInfo.chartCriteria;

		if (this.widgetInfo.chartCriteria) {
			for (j = 0; j < criteriaValues.length; j++) {
				var fieldContainer = this.criteriaPanel.getComponent(criteriaValues[j].name + "_CriteriaFieldContainer");
				var field = fieldContainer.getComponent(criteriaValues[j].name + "_Criteria");
				args[criteriaValues[j].name] = field.getValue();
			}
		}
		return args;
    },
    createChart: function () {
        var chart = customCodeChart(this);
        return chart;
    }
});