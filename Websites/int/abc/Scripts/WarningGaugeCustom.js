Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*','Ext.data.*']);

// Defining a model for Warning Gauge Widget Type
Ext.define('WarningGauge', {
    extend: 'Ext.data.Model',
    fields: [
            { name: 'widgettype', type: 'string' },
            { name: 'title', type: 'string' },
            { name: 'displaytype', type: 'string' },
            { name: 'x', type: 'string' },
            { name: 'y1', type: 'int' },
            { name: 'min', type: 'int' },
            { name: 'max', type: 'int' },
            { name: 'alert', type: 'int' },
            { name: 'pct', type: 'boolean' }
        ]
});


function createWarningGauge(config) {
    var color;

        if (config.gauge.alert == 0) {
            color = '#99CC00';
        }
        else if (config.gauge.alert == 1) {
            color = '#FFFF00';
        }
        else {
            color = '#FF0000';
        }
        
        var total = 0;
        config.store.each(function (rec) {
            total += rec.data.y1;
        });

        var percentage;
        if (config.gauge.pct) {
            percentage = total + '%';
        }

        return Ext.create('widget.panel', {
        title: config.title,
        layout: 'fit',
        width: 100,
        height: 250,
        items: {
            xtype: 'chart',
            animate: true,
            padding: '10 10 0 0',
            insetPadding: 20,
            theme: 'Base:gradients',
            background: defaultBackground,
            store: config.store,
            axes: [{
                type: 'gauge',
                position: 'gauge',
                title: percentage,
                minimum: config.gauge.min,
                maximum: config.gauge.max,
                steps: 10,
                margin: 7
            }],
            series: [{
                type: 'gauge',
                field: 'y1',
                colorSet: [color, '#ddd'],
                highlight: true
            }]
        }
    });
}


function createWarningChart(config) {
    // Create the data store
    var warningGaugeStore = Ext.create('Ext.data.Store', {
        autoLoad: true,
        model: 'WarningGauge',
        data: config,
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                root: 'data'
            }
        }
    });

    config.store = warningGaugeStore;

    if (config.displaytype == 'Gauge') {
        config.renderTo.add(createWarningGauge(config));
    }
}
