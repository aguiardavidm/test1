/// External javascript references required for Intellisense. -------------------------------------
/// <reference path='../extjs/ext-all-debug.js' />
/// <reference path="WidgetBase.js" />

Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.*','Ext.data.*']);

/*---------------------------------------------------------------------------*\
- function createObjectSummaryChart
-  Create the chart
\*---------------------------------------------------------------------------*/
function createObjectSummaryChart(widget) {
   var graphSeries, objectSummaryStore, modelFields, seriesLabelArray, seriesNameArray;
   var theGraph, timeperiodDropDownListStore, timeperiods;
   var totalDataCount = 0;
   var markers = [], markerLength = 0;
   var i;

   /*---------------------------------------------------------------------------*\
   - function getNextMarker
   -   Randomly return the next marker, but make sure you go through all the
   - possible markers before you start recycling markers.
   \*---------------------------------------------------------------------------*/
   function getNextMarker() {
       if (markerLength == 0) {
           markerLength = markers.length;
           return markers[0];
       } else {
           var m = Math.floor(Math.random() * markerLength);
           var chosen = markers[m];
           markers[m] = markers[--markerLength];
           markers[markerLength] = chosen;
           return chosen;
       }
   }

    /*---------------------------------------------------------------------------*\
    - function formatItem
    -   Format an item value for a label or tooltip
    \*---------------------------------------------------------------------------*/
   function formatItem(item, percentage) {
       if (item !== undefined && item > 0) {
           if (widget.widgetInfo.valueFormat == "Currency") {
               return Ext.util.Format.currency(item, '$', widget.widgetInfo.decimalplaces, false);
           } else if (percentage) { //TODO: Figure out how best to determine if percentage calc is being used
               return Ext.String.format('{0} %', parseFloat(item).toFixed(widget.widgetInfo.decimalplaces));
           } else {
               //return item.toFixed(widget.widgetInfo.decimalplaces);
               return Ext.util.Format.number(item,'0,000,000,000'); //No decimal places
           }
       }

       return "";
   }

    /*---------------------------------------------------------------------------*\
    - function trimLabel
    -   Trim a label if it exceeds the length and add "..." to the end
    \*---------------------------------------------------------------------------*/
    function trimLabel(label, length) {
        if (label && label.length > length)
            return label.slice(0, length) + '...';
        return label;
    }

   //Set up percentages if necessary
   if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
       if (widget.widgetInfo.data) {
           //Calculate the total amount for all data
           Ext.each(widget.widgetInfo.data, function(item) {
               if (item) {
                   totalDataCount = totalDataCount + item.y1;
               }
           }, this);

           // Calculate percentages
           Ext.each(widget.widgetInfo.data, function(item) {
               if (item) {
                   item.y1r = item.y1;
                   item.y1 = (item.y1r / totalDataCount) * 100;
               }
           }, this);
       }
   }

   // Set up the markers
   for (s in Computronix.POSSE.Dashboard.shapes) {
       for (c in Computronix.POSSE.Dashboard.colours) {
           markers[markerLength++] = { type: Computronix.POSSE.Dashboard.shapes[s], size: 4, radius: 4, 'fill': Computronix.POSSE.Dashboard.colours[c] }
       }
   }

   graphSeries = new Array;
   modelFields = new Array;
   modelFields[0] = { name: 'x', type: 'string' };

   seriesLabelArray = widget.widgetInfo.serieslabels;
   seriesNameArray = widget.widgetInfo.fieldnames.split(',').slice(1);

   if (widget.widgetInfo.displaytype == 'Timeline' && widget.widgetInfo.view != 'Compare') {
       widget.widgetInfo.graphtype = 'line';
   } else {
       widget.widgetInfo.graphtype = 'column';
   }

    // create the series to display and specify the model;
    for (i = 0; i < seriesNameArray.length; i++) {
        modelFields[i + 1] = { name: seriesNameArray[i], type: 'float' };

    modelFields[modelFields.length] = { name: 'y1r', type: 'float' };

    // for line graphs, we need one series per line
    if (widget.widgetInfo.graphtype == 'line') {
        graphSeries[i] = {
            type: widget.widgetInfo.graphtype,
            axis: 'left',
            highlight: { size: 1 },
            itemId: seriesNameArray[i],
            markerConfig: getNextMarker(),
            selectionTolerance: 5,
            title: function () {
                if (widget.widgetInfo.trimlabels) {
                    if (widget.widgetInfo.trimlabellength) {
                        return trimLabel(seriesLabelArray[i], widget.widgetInfo.trimlabellength);
                    } else if (seriesLabelArray[i].length > 12) {
                        return trimLabel(seriesLabelArray[i], 9);
                    }
                }
                return seriesLabelArray[i];
            } (),
            xField: 'x',
            yField: seriesNameArray[i],
            label: {
                display: widget.seriesNames.length * widget.data.length <= 15 ? 'insideEnd' : 'none',
                'text-anchor': 'middle',
                orientation: 'horizontal',
                color: '#333',
                field: seriesNameArray[i],
                renderer: function (item) {
                    if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                        return formatItem(item, true);
                    }
                    else {
                        return formatItem(item);
                    }
                }
            },
            tips: {
                trackMouse: true,
                minWidth: 200,
                minHeight: 26,
                width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
                height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
                renderer: function (storeItem, item) {
                    var name = widget.widgetInfo.serieslabels[Ext.Array.indexOf(widget.seriesNames, item.series.itemId)];
                    var timePeriod = storeItem.get('x');
                    var value = item.value[1];
                    if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                        var rawValue = item.value[2];
                        this.setTitle(name + ' (' + timePeriod + '): ' + formatItem(value, true) + ' (' + formatItem(rawValue) + ')'); //TODO: Maybe check on currency format here.
                    }
                    else {
                        this.setTitle(name + ' (' + timePeriod + '): ' + formatItem(value));
                    }
                }
            },
            listeners: {
                'itemmouseup': function (item) {
                    widget.drillDown(item);
                }
            }
        };
    }
}

// for column graphs, we need only one series
if (widget.widgetInfo.graphtype == 'column') {
    graphSeries[0] = {
        type: widget.widgetInfo.graphtype,
        axis: 'left',
        highlight: { size: 1 },
        itemId: 'objectSummaryGraphSeries',
        markerConfig: getNextMarker(),
        selectionTolerance: 5,
        title: seriesLabelArray,
        yField: seriesNameArray,
        label: {
            display: seriesNameArray.length * widget.data.length <= 15 ? 'insideEnd' : 'none',
            'text-anchor': 'middle',
            orientation: 'horizontal',
            color: '#333',
            field: seriesNameArray,
            renderer: function (item) {
                if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                    return formatItem(item, true);
                }
                else {
                    return formatItem(item);
                }
            }
        },
        tips: {
            trackMouse: true,
            minWidth: 200,
            minHeight: 26,
            width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
            height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
            renderer: function (storeItem, item) {
                if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                    this.setTitle(storeItem.get('x') + ': ' + formatItem(item.value[1], true) + ' (' + formatItem(item.value[2]) + ')'); //TODO: check currency format here
                }
                else {
                    this.setTitle(storeItem.get('x') + ': ' + formatItem(item.value[1]));
                }
            }
        },
        listeners: {
            'itemmouseup': function (item) {
                widget.drillDown(item);
            }
        }
    };
    }


    var theModel = Computronix.POSSE.Dashboard.defineModel('ObjectsInWarningState', {
        extend: 'Ext.data.Model',
        fields: modelFields
    });

    objectSummaryStore = Ext.create('Ext.data.Store', {
        autoLoad: true,
        model: theModel,
        data: widget.widgetInfo,
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                root: 'data'
            }
        }
    });

    /*************************************************************************\
    * Create the graph
    *  Default the graph to have at least height of 5 on the y axis.
    \*************************************************************************/
    if (widget.widgetInfo.displaytype == 'Pie Chart') {
        theGraph = Ext.create('Ext.chart.Chart', {
            itemId: 'pieChart',
            animate: true,
            shadow: true,
            legend: {
                position: "right",
                labelFont: "10px Arial"
            },
            theme: 'Base:gradients',
            background: Computronix.POSSE.Dashboard.defaultBackground,
            store: objectSummaryStore,
            series: [{
                type: 'pie',
                field: 'y1',
                showInLegend: true,
                title: function () {
                    // Trim the labels for the legend to 25 characters
                    var labels = [];
                    for (var i = 0; i < widget.data.length; i++) {
                        if (widget.widgetInfo.trimlabels) {
                            labels.push(trimLabel(widget.data[i].x, widget.widgetInfo.trimlabellength || 25));
                        } else {
                            labels.push(widget.data[i].x);
                        }
                    }

                    return labels;
                } (),
                tips: {
                    trackMouse: true,
                    minWidth: 200,
                    minHeight: 26,
                    width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
                    height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
                    renderer: function (storeItem, item) {
                        //var fieldName = "y1";
                        //this.setTitle(storeItem.get('x') + ': ' + formatItem(storeItem.get(fieldName)));
                        var name = storeItem.get('x');
                        var value = storeItem.get('y1');

                        if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                            this.setTitle(name + ': ' + formatItem(value, true) + ' (' + formatItem(storeItem.get('y1r')) + ')');
                        }
                        else {
                            this.setTitle(name + ': ' + formatItem(value));
                        }
                    }
                },
                listeners: {
                    'itemmouseup': function (item) {
                        widget.drillDown(item);
                    }
                },
                highlight: {
                    segment: {
                        margin: 20
                    }
                },
                label: {
                    field: 'x',
                    display: function () {
                        //"rotate", "middle", "insideStart", "insideEnd", "outside", "over", "under"
                        if (widget.data.length > 5) {
                            return 'rotate';
                        } else {
                            return 'middle';
                        }
                    } (),
                    contrast: true,
                    font: '14px Arial',
                    renderer: function (item) {
                        var value, total = 0;
                        for (var i = 0; i < widget.data.length; i++) {
                            total += widget.data[i].y1;
                            if (widget.data[i].x == item)
                                value = widget.data[i].y1;
                        }
                        var percentage = total > 0 ? (value / total) : 0;

                        // If this item makes up less than 1.5% of the total and there are more than 5 slices, don't show a label
                        // If there is one item, trim the label to 50 characters
                        // If there are 2+ items, trim the label to 25 characters.
                        if (percentage < 0.015 && widget.data.length > 5) {
                            return "";
                        } else if (item && widget.widgetInfo.trimlabels) {
                            return trimLabel(item, widget.widgetInfo.trimlabellength || 25);
                        }
                        return item;
                    }
                }
            }]
        });

    } else if (widget.widgetInfo.displaytype == 'Bar Graph') {
        var seriesColors = new Array(seriesNameArray.length);
        function getFieldIndexByColor(color) {
            for (var i=0; i < seriesColors.length; i++) {
                if (seriesColors[i] == color)
                    return i;
            }
            return -1;
        }

        theGraph = Ext.create('Ext.chart.Chart', {
            itemId: 'objectSummaryChart',
            animate: true,
            shadow: true,
            theme: 'Base:gradients',
            background: Computronix.POSSE.Dashboard.defaultBackground,
            store: objectSummaryStore,
            xField: 'x',
            legend: widget.widgetInfo.displaytwogroups && widget.currentGroupLevel == 1 ? {
                position: "right",
                labelFont: "10px Arial"
            } : false,
            axes: [{
                type: 'Numeric',
                position: 'left',
                fields: seriesNameArray,
                grid: true,
                title: widget.widgetInfo.yaxislabel,
                minimum: 0,
                maximum: function () {
                    if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                        return 100;
                    }
                    else {
                        var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
                        if (widget.widgetInfo.displaytwogroups && widget.currentGroupLevel == 1) {
                            for (var i = 0; i < widget.data.length; i++) {
                                var val = 0;
                                for (var j = 0; j < widget.seriesNames.length; j++) {
                                    val += widget.data[i][widget.seriesNames[j]];
                                }
                                if (val > maxVal)
                                    maxVal = val;
                            }
                        }

                        if (maxVal == 0) {
                            return 10;
                        } else if (maxVal <= 10) {
                            return maxVal;
                        }
                    }

                    return undefined;
                } (),
                majorTickSteps: function () {
                    var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
                    if (maxVal % 2 == 1)
                        maxVal -= 1;

                    if (maxVal == 2) {
                        return 1.4;
                    } else if (maxVal < 10) {
                        return maxVal - 0.5;
                    } else {
                        return 10;
                    }
                } (),
                grid: {
                    odd: {
                        opacity: 1,
                        fill: '#ddd',
                        stroke: '#bbb',
                        'stroke-width': 0.5
                    }
                },
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0')
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['x'],
                title: widget.getXAxisLabel(),
                label: {
                    font: "11px Arial",
                    rotate: {
                        degrees: function () {
                            if (widget.data.length > 10)
                                return 270;
                            else if (widget.data.length > 6)
                                return 300;
                            return 0;
                        } ()
                    },
                    renderer: function (item) {
                        if (item && widget.widgetInfo.trimlabels) {
                            if (widget.widgetInfo.trimlabellength) {
                                return trimLabel(item, widget.widgetInfo.trimlabellength);
                            } else {
                                if (widget.data.length > 5) {
                                    return trimLabel(item, 7);
                                } else if (widget.data.length > 1) {
                                    return trimLabel(item, 10);
                                } else {
                                    return trimLabel(item, 50);
                                }
                            }
                        }
                        return item;
                    }
                }
            }],
            series: [{
                type: 'column',
                axis: 'left',
                highlight: { size: 1 },
                id: 'warningy',
                selectionTolerance: 5,
                title: function () {
                    // Trim the sub-group labels if this is a stacked chart
                    var labels = widget.widgetInfo.serieslabels.slice();
                    if (widget.seriesNames.length > 1) {
                        for (var i = 0; i < labels.length; i++) {
                            if (widget.widgetInfo.trimlabels) {
                                labels[i] = trimLabel(labels[i], widget.widgetInfo.trimlabellength || 15);
                            }
                        }
                    }

                    return labels;
                } (),
                yField: widget.seriesNames,
                stacked: true,
                label: {
                    display: widget.seriesNames.length * widget.data.length <= 15 ? 'insideEnd' : 'none',
                    'text-anchor': 'middle',
                    orientation: 'horizontal',
                    color: '#333',
                    field: seriesNameArray,
                    renderer: function (item) {
                        if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                            return formatItem(item, true);
                        }
                        else {
                            return formatItem(item);
                        }
                    }
                },
                tips: {
                    trackMouse: true,
                    minWidth: 200,
                    minHeight: 26,
                    width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
                    height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
                    renderer: function (storeItem, item) {
                        var fieldName = "y1";
                        var label = storeItem.get('x');

                        // If this is stacked chart, show the second groups label in the tooltip
                        if (widget.widgetInfo.displaytwogroups && widget.currentGroupLevel == 1) {
                            var group2Index = getFieldIndexByColor(item.attr.fill);
                            fieldName = seriesNameArray[group2Index];
                            label += " --> " + widget.widgetInfo.serieslabels[group2Index];
                        }

                        if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                            this.setTitle(label + ': ' + formatItem(storeItem.get(fieldName), true) + ' (' + formatItem(storeItem.get('y1r')) + ')'); //TODO: Check currency here
                        }
                        else{
                            this.setTitle(label + ': ' + formatItem(storeItem.get(fieldName)));
                        }
                    }
                },
                listeners: {
                    'itemmouseup': function (item) {
                        if (widget.widgetInfo.displaytwogroups && widget.currentGroupLevel == 1) {
                            widget.drillDown(item, getFieldIndexByColor(item.attr.fill));
                        } else {
                            widget.drillDown(item);
                        }
                    },
                    // Hack to determine which field each stacked segment corresponds to.  Match the color to the array value
                    'afterrender': function () {
                        for (var i = 0; i < seriesNameArray.length; i++) {
                            seriesColors[i] = this.getLegendColor(i);
                        }
                    }
                }
            }]
        });

    } else {
        theGraph = Ext.create('Ext.chart.Chart', {
            itemId: 'TimelineChart',
            animate: true,
            shadow: true,
            theme: 'Base:gradients',
            background: Computronix.POSSE.Dashboard.defaultBackground,
            store: objectSummaryStore,
            xField: 'x',
            legend: {
                position: "right",
                labelFont: "9px Arial",
                visible: widget.seriesNames.length > 1,
                itemSpacing: 5
            },
            axes: [{
                type: 'Numeric',
                position: 'left',
                fields: seriesNameArray,
                grid: true,
                title: widget.widgetInfo.yaxislabel,
                minimum: 0,
                maximum: function () {
                    if (widget.widgetInfo.calculation === 'PercentOfSum' || widget.widgetInfo.calculation === 'PercentOfCount') {
                        return 100;
                    }
                    else {
                        var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
                        if (maxVal == 0) {
                            return 10;
                        } else if (maxVal <= 10) {
                            return maxVal;
                        }
                    }

                    return undefined;
                } (),
                majorTickSteps: function () {
                    var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
                    if (maxVal % 2 == 1)
                        maxVal -= 1;

                    if (maxVal == 2) {
                        return 1.4;
                    } else if (maxVal < 10) {
                        return maxVal - 0.5;
                    } else {
                        return 10;
                    }
                } (),
                decimals: 0,
                grid: {
                    odd: {
                        opacity: 1,
                        fill: '#ddd',
                        stroke: '#bbb',
                        'stroke-width': 0.5
                    }
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['x'],
                title: widget.widgetInfo.xaxislabel,
                label: {
                    rotate: {
                        degrees: function () {
                            if (widget.data.length > 12)
                                return 270;
                            else if (widget.data.length > 6)
                                return 300;
                            return 0;
                        } ()
                    },
                    font: "11px Arial"
                }
            }],
            series: graphSeries
        });

    }
    return theGraph;
}

function loadGroupValueData(widget) {
    var i;
    var selectedValues = {};
    
    // Build the distinct group value list from the displayed values, excluding the last "Other" item
    widget.groupValueList = [];
    if (widget.widgetInfo.displaytype == "Timeline") {
        var valueList = widget.widgetInfo.serieslabels;
        for (i=0; i < valueList.length; i++) {
            if (i < valueList.length-1 || valueList[i] != "Other") {
                widget.groupValueList[i] = {
                    id: i,
                    name: valueList[i],
                    selected: true
                };
                selectedValues[valueList[i]] = null;
            }
        }
    } else {
        for (i=0; i < widget.widgetInfo.data.length; i++) {
            if (i < widget.widgetInfo.data.count-1 || widget.widgetInfo.data[i].x != "Other") {
                widget.groupValueList[i] = {
                    id: i,
                    name: widget.widgetInfo.data[i].x,
                    selected: true
                };
                selectedValues[widget.widgetInfo.data[i].x] = null;
            }
        }
    }
    
    // Add the Not Top-N values - Fix later Not Top-N values aren't properly encoded
    if (widget.widgetInfo.nottopnvalues) {
        var valueList = widget.widgetInfo.nottopnvalues
        var len = widget.groupValueList.length;
        for (i = 0; i < valueList.length; i++) {
            if (!(valueList[i] in selectedValues)) {
                widget.groupValueList[widget.groupValueList.length] = {
                    id: len+i,
                    name: valueList[i],
                    selected: false
                };
            }
        }
    }
    
    widget.groupValueListStore.load();
}

function createGroupValueCriteria(widget) {
    var i;

    widget.groupValueList = [];
    var groupValueModel = Computronix.POSSE.Dashboard.defineModel('GroupValueModel', {
        extend: 'Ext.data.Model',
        fields: [
                { name: 'id', type: 'int' },
                { name: 'name', type: 'string' },
                { name: 'selected', type: 'bool' }
            ]
    });

    widget.groupValueListStore = Ext.create('Ext.data.Store', {
        autoLoad: false,
        autoDestroy: true,
        model: groupValueModel,
        data: widget,
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                root: 'groupValueList'
            }
        },
        sorters: [{
            property: 'selected',
            direction: 'DESC'
        },{
            property: 'name',
            direction: 'ASC'
        }]
    });

    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 0
    });

    var groupValueEdit = Ext.create('Ext.Window', {
        height: 500,
        width: 400,
        minHeight: 500,
        minWidth: 400,
        hidden: true,
        maximizable: false,
        closeAction: 'hide',
        modal: true,
        title: 'Select',
        renderTo: Ext.getBody(),
        layout: 'fit',
        items: [{
            xtype: 'grid',
            border: false,
            store: widget.groupValueListStore,
            columns: [{
                header: 'Name',
                dataIndex: 'name',
                flex: 1
            }, {
                xtype: 'checkcolumn',
                header: 'Selected',
                dataIndex: 'selected',
                width: 55
            }],
            frame: true,
            bbar: [{
                xtype: 'button',
                text: 'OK',
                cls: 'x-btn-text-icon',
                icon: 'extjs/resources/themes/images/default/dd/drop-yes.gif',
                handler: function (item, value, options) {
                    widget.updateGroupValueList();
                    widget['valuesSelected'] = true;
                    widget.reload();
                    widget.groupValuePopup.hide();
                }
            }, {
                xtype: 'button',
                text: 'Cancel',
                cls: 'x-btn-text-icon',
                icon: 'extjs/resources/themes/images/default/dd/drop-no.gif',
                handler: function (item, value, options) {
                    widget.groupValuePopup.hide();
                }
            }]
        }]
    });

    widget.groupValuePopup = groupValueEdit;

    var criteria = Ext.create('Ext.panel.Panel', {
        border: 0,
        height: 35,
        padding: '5 5 5 5',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'fieldcontainer',
            itemId: 'groupValueCriteria',
            width: 100,
            height: 25,
            layout: 'hbox',
            items: [{
                xtype: 'textfield',
                itemId: 'groupValueList',
                fieldLabel: widget.widgetInfo['group' + widget.currentGroupLevel + 'label'],
                width: 140,
                labelWidth: 45,
                readOnly: true,
                flex: 1
            }, {
                xtype: 'button',
                text: 'Select...',
                itemId: 'goButton',
                handler: function (item, value, options) {
                    widget.groupValuePopup.alignTo(this);
                    widget.groupValuePopup.show(this);
					widget.groupValuePopup.doLayout();
                }
            }]
        }]
    });

    return criteria;
}

Ext.define('Computronix.POSSE.Dashboard.ObjectSummaryWidget', {
    extend: 'Computronix.POSSE.Dashboard.Widget',
    // Data Members
    //=============================================================================
    //    
    drillDownSupported: true,


    // Methods
    //=============================================================================
    //  

    createCriteria: function () {
        if (this.widgetInfo.timeperioddetail) {
            var criteriaPeriod = Computronix.POSSE.Dashboard.createPeriodCriteria(this);
            this.criteriaPeriod = criteriaPeriod.getComponent('criteriaPeriod');

            if (this.widgetInfo.defaulttopnvalues && this.widgetInfo.allowgroupvalueselection) {
                var criteriaGroupValue = createGroupValueCriteria(this);
                this.groupValueCriteria = criteriaGroupValue.getComponent('groupValueCriteria');

                return Ext.create('Ext.panel.Panel', {
                    border: 0,
                    height: 75,
                    padding: '5 5 5 5',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [criteriaPeriod, criteriaGroupValue]
                });
            } else {
                return criteriaPeriod;
            }
        } else {
            return [];
        }
    },
    refreshCriteria: function () {
        if (this.groupValueCriteria) {
            loadGroupValueData(this);
            this.updateGroupValueList();
        }
    },
    updateGroupValueList: function () {
        var groupValues;
        var allValuesSelected = true;
        for (i = 0; i < this.groupValueListStore.data.items.length; i++) {
            if (this.groupValueListStore.data.items[i].data.selected) {
                if (groupValues) {
                    groupValues += ',' + this.groupValueListStore.data.items[i].data.name.toString();
                } else {
                    groupValues = this.groupValueListStore.data.items[i].data.name.toString();
                }
            } else {
                allValuesSelected = false;
            }
        }

        if (groupValues) {
            this.groupValueCriteria.getComponent('groupValueList').setValue(groupValues);
        }
        //if (allValuesSelected) {
        //    this.groupValueCriteria.getComponent('groupValueList').setValue('(All)');
        //} else {
        //    this.groupValueCriteria.getComponent('groupValueList').setValue(groupValues);
        //}
    },
    getGroupValues: function () {
        var groupValues;
        for (i = 0; i < this.groupValueListStore.data.items.length; i++) {
            if (this.groupValueListStore.data.items[i].data.selected) {
                if (groupValues) {
                    groupValues += ',' + this.groupValueListStore.data.items[i].data.name;
                } else {
                    groupValues = this.groupValueListStore.data.items[i].data.name;
                }
            }
        }
        return groupValues;
    },
    getCriteriaValues: function () {
        var args = {}, i;
        if (this.widgetInfo.timeperioddetail) {
            args.period = this.criteriaPeriod.getComponent('period').getValue();
            args.fromDate = this.criteriaPeriod.getComponent('fromDate').getValue();
            args.toDate = this.criteriaPeriod.getComponent('toDate').getValue();
            
            // Add selected group values (if any)
            if (this.groupValueListStore && this.valuesSelected) {
                args.groupSelectedValues = this.getGroupValues();
            }

            // Add all the non-Top N values if there is another group level to drill down to
            if (this.widgetInfo['group' + (this.currentGroupLevel) + 'label']) {
                if (this.widgetInfo['group' + (this.currentGroupLevel) + 'value'] == 'Other' && this.widgetInfo.nottopnvalues && this.widgetInfo.nottopnvalues.length > 0) {
                    args.notTopNValues = this.widgetInfo.nottopnvalues;
                }
            }
        }
        return args;
    },
    createChart: function () {
        var chart = createObjectSummaryChart(this);
        return chart;
    }
});