/// External javascript references required for Intellisense. -------------------------------------
/// <reference path='../extjs/ext-all-debug.js' />
/// <reference path="WidgetBase.js" />

Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.*','Ext.data.*']);

Ext.define('Computronix.POSSE.Dashboard.JobSummaryWidget', {
    extend: 'Computronix.POSSE.Dashboard.Widget',
    // Data Members
    //=============================================================================
    //    
    drillDownSupported: true,


    // Methods
    //=============================================================================
    //    
    createCriteria: function () {
        var criteria = Computronix.POSSE.Dashboard.createPeriodCriteria(this);
        this.criteriaPeriod = criteria.getComponent('criteriaPeriod');
        return criteria;
    },
    getCriteriaValues: function () {
        var args = {}, i;

        args.period = this.criteriaPeriod.getComponent('period').getValue();
        args.fromDate = this.criteriaPeriod.getComponent('fromDate').getValue();
        args.toDate = this.criteriaPeriod.getComponent('toDate').getValue();

        return args;
    },
    createChart: function () {
        var jobSummaryStore, modelFields;
        var theGraph, timeperiodDropDownListStore, timeperiods;
        var i;
        var widget = this;

        modelFields = new Array;
        modelFields[0] = { name: 'x', type: 'string' };

        this.seriesLabels = widget.widgetInfo.serieslabels.split(',');
        this.seriesNames = widget.widgetInfo.fieldnames.split(',').slice(1);

        // create the series to display and specify the model;
        for (i = 0; i < this.seriesNames.length; i++) {
            modelFields[i + 1] = { name: this.seriesNames[i], type: 'int' };
        }

        var theModel = Computronix.POSSE.Dashboard.defineModel('JobsInWarningState', {
            extend: 'Ext.data.Model',
            fields: modelFields
        });

        jobSummaryStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            model: theModel,
            data: widget.widgetInfo,
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            }
        });

        /*************************************************************************\
        * Create the graph
        *  Default the graph to have at least height of 5 on the y axis.
        \*************************************************************************/
        if (widget.widgetInfo.displaytype == 'Pie Chart') {
            theGraph = Ext.create('Ext.chart.Chart', {
                itemId: 'pieChart',
                animate: true,
                shadow: true,
                theme: 'Base:gradients',
                background: Computronix.POSSE.Dashboard.defaultBackground,
                store: jobSummaryStore,
                series: [{
                    type: 'pie',
                    field: 'y1',
                    showInLegend: true,
                    tips: {
                        trackMouse: true,
                        width: 200,
                        height: 40,
                        renderer: function (storeItem, item) {
                            var fieldName = "y1";
                            if (widget.seriesLabels.length > 0 && widget.seriesLabels[0])
                                fieldName = 'y' + ((item.series.items.indexOf(item) % widget.data.length) + 1);
                            this.setTitle(storeItem.get('x') + ': ' + Ext.util.Format.number(storeItem.get(fieldName), '0,0'));
                        }
                    },
                    listeners: {
                        'itemmouseup': function (item) {
                            widget.drillDown(item);
                        }
                    },
                    highlight: {
                        segment: {
                            margin: 20
                        }
                    },
                    label: {
                        field: 'x',
                        display: 'rotate',
                        contrast: true,
                        font: '18px Arial'
                    }
                }]
            });

        } else {
            theGraph = Ext.create('Ext.chart.Chart', {
                itemId: 'jobSummaryChart',
                animate: true,
                shadow: true,
                theme: 'Base:gradients',
                background: Computronix.POSSE.Dashboard.defaultBackground,
                store: jobSummaryStore,
                xField: 'x',
                legend: this.seriesNames.length == 1 ? false : { position: 'right' },
                axes: [{
                    type: 'Numeric',
                    position: 'left',
                    fields: this.seriesNames,
                    grid: true,
                    title: widget.widgetInfo.yaxislabel,
                    grid: {
                        odd: {
                            opacity: 1,
                            fill: '#ddd',
                            stroke: '#bbb',
                            'stroke-width': 0.5
                        }
                    },
                    label: {
                        renderer: Ext.util.Format.numberRenderer('0,0')
                    }
                }, {
                    type: 'Category',
                    position: 'bottom',
                    fields: ['x'],
                    title: widget.getXAxisLabel(),
                    label: {
                        rotate: {
                            degrees: function () {
                                if (widget.data.length > 10)
                                    return 270;
                                else if (widget.data.length > 6)
                                    return 300;
                                return 0;
                            } ()
                        }
                    }
                }],
                series: [{
                    type: 'column',
                    axis: 'left',
                    highlight: { size: 1 },
                    id: 'warningy',
                    selectionTolerance: 5,
                    title: this.seriesLabels,
                    yField: this.seriesNames,
                    label: {
                        display: this.seriesNames.length * widget.data.length <= 15 ? 'insideEnd' : 'none',
                        'text-anchor': 'middle',
                        orientation: 'horizontal',
                        color: '#333',
                        field: this.seriesNames,
                        renderer: Ext.util.Format.numberRenderer('0,0')
                    },
                    tips: {
                        trackMouse: true,
                        width: 200,
                        height: 40,
                        renderer: function (storeItem, item) {
                            var fieldName = "y1";
                            if (widget.seriesLabels.length > 0 && widget.seriesLabels[0])
                                fieldName = 'y' + ((item.series.items.indexOf(item) % widget.data.length) + 1);
                            this.setTitle(storeItem.get('x') + ': ' + Ext.util.Format.number(storeItem.get(fieldName), '0,0'));
                        }
                    },
                    listeners: {
                        'itemmouseup': function (item) {
                            widget.drillDown(item);
                        }
                    }
                }]
            });
        }
        return theGraph;
    }
});