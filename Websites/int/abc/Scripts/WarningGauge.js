Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*','Ext.data.*']);

// Defining a model for Warning Gauge Widget Type
Ext.define('WarningGauge', {
    extend: 'Ext.data.Model',
    fields: [
            { name: 'widgettype', type: 'string' },
            { name: 'title', type: 'string' },
            { name: 'displaytype', type: 'string' },
            { name: 'x', type: 'string' },
            { name: 'y1', type: 'float' },
            { name: 'min', type: 'float' },
            { name: 'max', type: 'float' },
            { name: 'alert', type: 'int' },
            { name: 'pct', type: 'boolean' }
        ]
});

Ext.define('Computronix.POSSE.Dashboard.WarningGaugeWidget', {
    extend: 'Computronix.POSSE.Dashboard.Widget',

    // Data Members
    //=============================================================================
    //    
    layout: 'fit',

    // Methods
    //=============================================================================
    //    
    createChart: function () {
        // Create the data store
        var warningGaugeStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            model: 'WarningGauge',
            data: this,
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            }
        });

        var color;

        if (this.widgetInfo.gauge.alert == 0) {
            color = '#99CC00';
        }
        else if (this.widgetInfo.gauge.alert == 1) {
            color = '#FFFF00';
        }
        else {
            color = '#FF0000';
        }

        var total = 0;
        warningGaugeStore.each(function (rec) {
            total += rec.data.y1;
        });

        var label = Ext.util.Format.number(total, '0,0');
        if (this.widgetInfo.gauge.pct) {
            label += '%';
        }

        var steps = (this.widgetInfo.gauge.max - this.widgetInfo.gauge.min);
        if (steps > 10) {
            steps = 10;
        }

        return Ext.create('widget.panel', {
            layout: 'fit',
            border: 0,
            items: {
                xtype: 'chart',
                animate: true,
                insetPadding: 30,
                theme: 'Base:gradients',
                background: Computronix.POSSE.Dashboard.defaultBackground,
                store: warningGaugeStore,
                axes: [{
                    type: 'gauge',
                    position: 'gauge',
                    title: label,
                    minimum: this.widgetInfo.gauge.min,
                    maximum: this.widgetInfo.gauge.max,
                    steps: steps,
                    margin: 7
                }],
                series: [{
                    type: 'gauge',
                    field: 'y1',
                    colorSet: [color, '#ddd'],
                    highlight: true
                }]
            }
        });
    }
});