Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
//Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*','Ext.data.*']);


/*---------------------------------------------------------------------------*\
- function createJobsInWarningStatus
-  Displays a barchart a la function name
\*---------------------------------------------------------------------------*/
function createJobsInWarningStatus(config) {
    var graphSeries, jobsInWarningStateStore, modelFields
    var seriesLabelArray, seriesNameArray, theGraph, timeperiodDropDownListStore, timeperiods;
    var i;

	graphSeries = new Array;
	modelFields = new Array;
	modelFields[0] = { name: 'x', type: 'string' };	

	seriesLabelArray = config.serieslabels.split(',');
	seriesNameArray = config.fieldnames.split(',').slice(1);

	// create the series to display and specify the model;
	for (i = 0; i < seriesNameArray.length; i++) {
		modelFields[i + 1] = { name: seriesNameArray[i], type: 'int' };
	
	}

	graphSeries[0] = {
		type: 'column',
		axis: 'left',
		highlight: { size: 1 },
		id: 'warningy',
		selectionTolerance: 5,
		title: seriesLabelArray,
		yField: seriesNameArray,
		label: {
			display: seriesNameArray.length * config.data.length <= 15 ? 'insideEnd' : 'none',
			'text-anchor': 'middle',
			orientation: 'horizontal',
			color: '#333',
			field: seriesNameArray,
			renderer: Ext.util.Format.numberRenderer('0,0')
		},
		tips: {
			trackMouse: true,
			width: 85,
			height: 26,
			renderer: function (storeItem, item) {
				this.setTitle(Ext.util.Format.plural(item.value[1], 'job'));
			}
		}
	};

	var theModel = Computronix.POSSE.Dashboard.defineModel('JobsInWarningState', {
		extend: 'Ext.data.Model',
		fields: modelFields
	});
	
	jobsInWarningStateStore = Ext.create('Ext.data.Store', {
		autoLoad: true,
		model: theModel,
		data: config,
		proxy: {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'data'
			}
		}
	});

	/*************************************************************************\
	* Create the graph
	*  Default the graph to have at least height of 5 on the y axis.
	\*************************************************************************/

	theGraph = Ext.create('widget.panel', {
		//title: config.title,
		height: 300,
		layout: 'fit',
		items: {
			xtype: 'chart',
			itemId: 'warningChart',
			animate: true,
			shadow: true,
			theme: 'Base:gradients',
			background: defaultBackground,
			store: jobsInWarningStateStore,
			xField: 'x',
			legend: false,
			axes: [{
				type: 'Numeric',
				position: 'left',
				fields: seriesNameArray,
				grid: true,
				title: config.yaxislabel,
				grid: {
					odd: {
						opacity: 1,
						fill: '#ddd',
						stroke: '#bbb',
						'stroke-width': 0.5
					}
				},
				label: {
					renderer: Ext.util.Format.numberRenderer('0,0')
				}
			}, {
				type: 'Category',
				position: 'bottom',
				fields: ['x'],
				title: config.xaxislabel
			}],
			series: graphSeries
		}
	});

	// it's easier to destroy a legend when you don't need it than to create it when you do
	if (seriesNameArray.length == 1) {
		theGraph.items.items[0].legend = false;
	}	
    /*************************************************************************\
    * Render the graph
    \*************************************************************************/
    config.renderTo.add(theGraph);
}
