/// External javascript references required for Intellisense. -------------------------------------
/// <reference path='../extjs/ext-all-debug.js' />
/// <reference path='../Scripts/WidgetBase.js' />

Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*','Ext.data.*']);

/*---------------------------------------------------------------------------*\
- function createObjectIntakeChart
-  Create the chart
\*---------------------------------------------------------------------------*/
function createObjectIntakeChart(widget) {
    var graphSeries, modelFields, seriesLabelArray, seriesNameArray;
    var markers = [], markerLength = 0;
    var objectIntakeRateStore;
    var i;

    /*---------------------------------------------------------------------------*\
    - function getNextMarker
    -   Randomly return the next marker, but make sure you go through all the
    - possible markers before you start recycling markers.
    \*---------------------------------------------------------------------------*/
    function getNextMarker() {
      if (markerLength == 0) {
        markerLength = markers.length;
        return markers[0];
      } else {
        var m = Math.floor(Math.random() * markerLength);
        var chosen = markers[m];
        markers[m] = markers[--markerLength];
        markers[markerLength] = chosen;
        return chosen;
      }
    }


    // Set up the markers
    for (s in Computronix.POSSE.Dashboard.shapes) {
      for (c in Computronix.POSSE.Dashboard.colours) {
        markers[markerLength++] = { type: Computronix.POSSE.Dashboard.shapes[s], size: 4, radius: 4, 'fill' : Computronix.POSSE.Dashboard.colours[c] }
      }
    }

    // Create the graph Model
    graphSeries = new Array;
    modelFields = new Array;
    modelFields[0] = { name: 'x', type: 'string' };
    seriesLabelArray = widget.widgetInfo.serieslabels;
    seriesNameArray = widget.widgetInfo.fieldnames.split(',').slice(1);

    if (widget.widgetInfo.displaytype == 'Timeline' && widget.widgetInfo.view != 'Compare') {
        widget.widgetInfo.graphtype = 'line';
    } else {
        widget.widgetInfo.graphtype = 'column';
    }

    // create the series to display and specify the model;
    for (i = 0; i < seriesNameArray.length; i++) {
        modelFields[i + 1] = { name: seriesNameArray[i], type: 'int' };

        // for line graphs, we need one series per line
        if (widget.widgetInfo.graphtype == 'line') {
            graphSeries[i] = {
                type: widget.widgetInfo.graphtype,
                axis: 'left',
                highlight: { size: 1 },
                itemId: seriesNameArray[i],
                markerConfig: getNextMarker(),
                selectionTolerance: 5,
                title: seriesLabelArray[i],
                xField: 'x',
                yField: seriesNameArray[i],
                tips: {
                    trackMouse: true,
                    minWidth: 200,
                    minHeight: 26,
                    width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
                    height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
                    renderer: function (storeItem, item) {
                        this.setTitle(storeItem.get('x') + ': ' + Ext.util.Format.plural(item.value[1], 'object'));
                    }
                }
            };
        }
    }

    // for column graphs, we need only one series
    if (widget.widgetInfo.graphtype == 'column') {
        graphSeries[0] = {
            type: widget.widgetInfo.graphtype,
            axis: 'left',
            highlight: { size: 1 },
            itemId: 'objectIntakeGraphSeries',
            markerConfig: getNextMarker(),
            selectionTolerance: 5,
            title: seriesLabelArray,
            yField: seriesNameArray,
            label: {
                display: seriesNameArray.length * widget.data.length <= 15 ? 'insideEnd' : 'none',
                'text-anchor': 'middle',
                orientation: 'horizontal',
                color: '#333',
                field: seriesNameArray,
                renderer: Ext.util.Format.numberRenderer('0,0')
            },
            tips: {
                trackMouse: true,
                minWidth: 200,
                minHeight: 26,
                width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
                height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
                renderer: function (storeItem, item) {
                    this.setTitle(storeItem.get('x') + ': ' + Ext.util.Format.plural(item.value[1], 'object'));
                }
            }
        };
    }

    theModel = Computronix.POSSE.Dashboard.defineModel('ObjectIntakeRate', {
        extend: 'Ext.data.Model',
        fields: modelFields
    });


    objectIntakeRateStore = Ext.create('Ext.data.Store', {
        autoLoad: true,
        model: theModel,
        data: widget,
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                root: 'data'
            }
        }
    });


    /*************************************************************************\
    * Create the graph
    *  Default the graph to have at least height of 5 on the y axis.
    \*************************************************************************/
    var graph = Ext.create('Ext.chart.Chart', {
        itemId: 'intakeChart',
        animate: true,
        shadow: true,
        theme: 'Base:gradients',
        background: Computronix.POSSE.Dashboard.defaultBackground,
        store: objectIntakeRateStore,
        xField: 'x',
        legend: seriesNameArray.length == 1 ? false : {
            position: 'right',
            labelFont: '10px Arial'
        },
        axes: [{
            type: 'Numeric',
            position: 'left',
            fields: seriesNameArray,
            grid: true,
            title: widget.widgetInfo.yaxislabel,
            minimum: 0,
            maximum: function () {
                var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
                if (maxVal == 0) {
                    return 10;
                } else if (maxVal <= 10) {
                    return maxVal;
                }

                return undefined;
            } (),
            majorTickSteps: function () {
                var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
                if (maxVal % 2 == 1)
                    maxVal -= 1;

                if (maxVal == 2) {
                    return 1.4;
                } else if (maxVal < 10) {
                    return maxVal - 0.5;
                } else {
                    return 10;
                }
            } (),
            decimals: 0,
            grid: {
                odd: {
                    opacity: 1,
                    fill: '#ddd',
                    stroke: '#bbb',
                    'stroke-width': 0.5
                }
            }
        }, {
            type: 'Category',
            position: 'bottom',
            fields: ['x'],
            title: widget.widgetInfo.xaxislabel,
            label: {
                font: '11px Arial',
                rotate: {
                    degrees: function () {
                        if (widget.data.length > 12)
                            return 270;
                        else if (widget.data.length > 6)
                            return 300;
                        return 0;
                    }()
                }
            }
        }],
        series: graphSeries
    });

    return graph;
}

Ext.define('Computronix.POSSE.Dashboard.ObjectIntakeRateWidget', {
    extend: 'Computronix.POSSE.Dashboard.Widget',
    createCriteria: function () {
        this.widgetInfo.view = this.widgetInfo.view || "Summary";
        var criteria = Computronix.POSSE.Dashboard.createPeriodViewCriteria(this);
        this.criteriaPeriod = criteria.getComponent('criteriaPeriod');
        this.criteriaView = criteria.getComponent('criteriaView');

        return criteria;
    },
    getCriteriaValues: function () {
        var args = {};

        if (this.criteriaView.getComponent('radioDetailed').getValue()) {
            args.view = 'Detailed';
        } else if (this.criteriaView.getComponent('radioSummary').getValue()) {
            args.view = 'Summary';
        } else {
            args.view = 'Compare';
        }
        args.period = this.criteriaPeriod.getComponent('period').getValue();
        args.fromDate = this.criteriaPeriod.getComponent('fromDate').getValue();
        args.toDate = this.criteriaPeriod.getComponent('toDate').getValue();

        return args;
    },
    createChart: function () {
        var chart = createObjectIntakeChart(this);
        return chart;
    }
});