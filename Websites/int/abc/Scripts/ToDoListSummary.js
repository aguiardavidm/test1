Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', 'extjs/ux');

Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*', 'Ext.data.*']);
Ext.require(['Ext.panel.Panel', 'Ext.Action', 'Ext.button.Button', 'Ext.window.MessageBox']);
Ext.require(['Ext.selection.CellModel','Ext.ux.CheckColumn']);

// Defining a model for To Do List Widget Type
var toDoListModel = Computronix.POSSE.Dashboard.defineModel('ToDoListSummary', {
    extend: 'Ext.data.Model',
    fields: [
            { name: 'widgetid', type: 'int' },
            { name: 'widgettype', type: 'string' },
            { name: 'title', type: 'string' },
            { name: 'displaytype', type: 'string' },
            { name: 'accessgroup', type: 'string' },
            { name: 'xaxislabel', type: 'string' },
            { name: 'yaxislabel', type: 'string' },
            { name: 'user', type: 'int' },
            { name: 'drilldownurl', type: 'string' },
            { name: 'x', type: 'string' },
            { name: 'y1', type: 'int' }
        ]
});

Ext.define('Computronix.POSSE.Dashboard.ToDoList', {
    extend: 'Computronix.POSSE.Dashboard.Widget',
    createCriteria: function () {
        var i;
        var widget = this;

        this.userList = [];
        for (i = 0; i < this.widgetInfo.users.length; i++) {
            this.userList[i] = {
                id: this.widgetInfo.users[i].id,
                name: this.widgetInfo.users[i].name,
                selected: true
            };
        }

        var userModel = Computronix.POSSE.Dashboard.defineModel('ToDoListSummaryUserModel', {
            extend: 'Ext.data.Model',
            fields: [
                { name: 'id', type: 'int' },
                { name: 'name', type: 'string' },
                { name: 'selected', type: 'bool' }
            ]
        });

        this.userDropDownListStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            autoDestroy: true,
            model: userModel,
            data: this,
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'userList'
                }
            },
            sorters: [{
                property: 'name',
                direction: 'ASC'
            }]
        });

        if (widget.widgetInfo.accessgroup) {
            var multiselect = true;
            var user = 'Select user(s)';
            var fieldlabel = 'Select user(s)';
        } else {
            var multiselect = false;
            var user = widget.widgetInfo.user;
            var fieldlabel = 'Select a single user';
        }

        var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 0
        });

        var userEdit = Ext.create('Ext.Window', {
            height: 400,
            width: 300,
            minHeight: 400,
            minWidth: 300,
            hidden: true,
            maximizable: false,
            closeAction: 'hide',
            modal: true,
            title: 'Select Users',
            renderTo: Ext.getBody(),
            layout: 'fit',
            items: [{
                xtype: 'grid',
                border: false,
                store: this.userDropDownListStore,
                columns: [{
                    header: 'Name',
                    dataIndex: 'name',
                    flex: 1
                }, {
                    xtype: 'checkcolumn',
                    header: 'Selected',
                    dataIndex: 'selected',
                    width: 55
                }],
                frame: true,
                bbar: [{
                    xtype: 'button',
                    text: 'OK',
                    cls: 'x-btn-text-icon',
                    icon: 'extjs/resources/themes/images/default/dd/drop-yes.gif',
                    handler: function (item, value, options) {
                        //widget.userDropDownListStore.proxy.update({ action: 'update' });
                        widget.updateUserList();
                        widget.reload();
                        widget.userEditPopup.hide();
                    }
                }, {
                    xtype: 'button',
                    text: 'Cancel',
                    cls: 'x-btn-text-icon',
                    icon: 'extjs/resources/themes/images/default/dd/drop-no.gif',
                    handler: function (item, value, options) {
                        widget.userEditPopup.hide();
                    }
                }]
            }]
        });

        this.userEditPopup = userEdit;

        var criteria = Ext.create('Ext.panel.Panel', {
            border: 0,
            height: 35,
            padding: '5 5 5 5',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'fieldcontainer',
                itemId: 'userCriteria',
                width: 100,
                height: 25,
                layout: 'hbox',
                //fieldLabel: 'Access Group / Users:',
                items: [{
                    xtype: 'textfield',
                    itemId: 'userList',
                    fieldLabel: 'Users',
                    width: 140,
                    labelWidth: 45,
                    readOnly: true,
                    flex: 1
                }, {
                    xtype: 'button',
                    text: 'Select...',
                    itemId: 'goButton',
                    handler: function (item, value, options) {
                        widget.userEditPopup.alignTo(this);
                        widget.userEditPopup.show(this);
                    }
                }]
            }]
        });
        this.userCriteria = criteria.getComponent('userCriteria');
        this.userCriteria.getComponent('userList').setValue(widget.widgetInfo.accessgroup);
        return criteria;
    },

    getUsers: function () {
        var users;
        for (i = 0; i < this.userDropDownListStore.data.items.length; i++) {
            if (this.userDropDownListStore.data.items[i].data.selected) {
                if (users) {
                    users += ',' + this.userDropDownListStore.data.items[i].data.id.toString();
                } else {
                    users = this.userDropDownListStore.data.items[i].data.id.toString();
                }
            }
        }
        return users;
    },
    updateUserList: function () {
        var users;
        var allUsersSelected = true;
        for (i = 0; i < this.userDropDownListStore.data.items.length; i++) {
            if (this.userDropDownListStore.data.items[i].data.selected) {
                if (users) {
                    users += ',' + this.userDropDownListStore.data.items[i].data.name.toString();
                } else {
                    users = this.userDropDownListStore.data.items[i].data.name.toString();
                }
            } else {
                allUsersSelected = false;
            }
        }

        if (allUsersSelected) {
            this.userCriteria.getComponent('userList').setValue(this.widgetInfo.accessgroup);
        } else {
            this.userCriteria.getComponent('userList').setValue(users);
        }
    },
    getCriteriaValues: function () {
        var args = {};
        args.users = this.getUsers();
        return args;
    },
    createChart: function () {
        // Create the data store
        var widget = this;
        var toDoListStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            model: toDoListModel,
            data: widget.widgetInfo,
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            }
        });

        var chart = Ext.create('Ext.chart.Chart', {
            itemId: 'toDoListChart',
            animate: true,
            shadow: true,
            theme: 'Base:gradients',
            background: Computronix.POSSE.Dashboard.defaultBackground,
            store: toDoListStore,
            axes: [{
                type: 'Numeric',
                position: 'left',
                fields: ['y1'],
                label: {
                    renderer: Ext.util.Format.numberRenderer('0')
                },
                title: widget.widgetInfo.yaxislabel,
                grid: true,
                minimum: 0,
                maximum: function () {
                    var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
                    if (maxVal == 0) {
                        return 10;
                    } else if (maxVal <= 10) {
                        return maxVal;
                    }

                    return undefined;
                } (),
                majorTickSteps: function () {
                    var maxVal = Math.ceil(parseFloat(widget.widgetInfo.maxyvalue));
                    if (maxVal % 2 == 1)
                        maxVal -= 1;

                    if (maxVal == 2) {
                        return 1.4;
                    } else if (maxVal < 10) {
                        return maxVal - 0.5;
                    } else {
                        return 10;
                    }
                } (),
                decimals: 0
                }, {
                type: 'Category',
                position: 'bottom',
                fields: ['x'],
                title: widget.widgetInfo.xaxislabel,
                label: {
                    rotate: {
                        degrees: function () {
                            if (widget.data.length > 10)
                                return 270;
                            else if (widget.data.length > 6)
                                return 300;
                            return 0;
                        } ()
                    },
                    renderer: function (item) {
                        if (item && item.length > 10 && widget.data.length > 4) {
                            return item.slice(0, 7) + "...";
                        }
                        return item;
                    }
                }
            }],
            series: [{
                type: 'column',
                axis: 'left',
                highlight: true,
                tips: {
                    trackMouse: true,
                    minWidth: 200,
                    minHeight: 26,
                    width: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 200 } (),
                    height: function () { if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) return 52 } (),
                    renderer: function (storeItem, item) {
                        this.setTitle(storeItem.get('x') + ': ' + Ext.util.Format.plural(storeItem.get('y1'), 'task'));
                    }
                },
                listeners: {
                    'itemmouseup': function (item) {
                        var drillDownUrl = widget.widgetInfo.drilldownurl;
                        var accessgroup = widget.widgetInfo.accessgroup;
                        if ((drillDownUrl) && (accessgroup)) {
                            var processtype = item.storeItem.data.x;
                            var href = widget.widgetInfo.drilldownurl;
                            href = href.replace('{Users}', widget.getUsers());
                            href = href.replace('{ProcessType}', processtype);
                            window.open(href, "_blank");
                        }
                    }
                },
                label: {
                    display: 'insideEnd',
                    field: 'y1',
                    renderer: Ext.util.Format.numberRenderer('0'),
                    color: '#333',
                    'text-anchor': 'middle'
                },
                xField: 'x',
                yField: 'y1'
            }]
        });
        return chart;
    }
});
