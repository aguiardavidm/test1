Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*','Ext.data.*']);

// Defining a model for Job In Status Widget Type
var jobInStatusModel = Computronix.POSSE.Dashboard.defineModel('JobInStatus', {
    extend: 'Ext.data.Model',
    fields: [
            { name: 'widgetid', type: 'int' }, 
            { name: 'widgettype', type: 'string' },
            { name: 'title', type: 'string' },
            { name: 'displaytype', type: 'string' },
            { name: 'xaxislabel', type: 'string' },
            { name: 'yaxislabel', type: 'string' },
            { name: 'drilldownurl', type: 'string' },
            { name: 'x', type: 'string' },
            { name: 'y1', type: 'int' }
        ]
});

Ext.define('Computronix.POSSE.Dashboard.JobInStatus', {
    extend: 'Computronix.POSSE.Dashboard.Widget',
    createChart: function () {
        //Declare widget
        var widget = this;
        // Create the data store
        var jobInStatusStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            model: jobInStatusModel,
            data: widget.widgetInfo,
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'data'
                }
            }
        });
        var chart;

        
        if (widget.widgetInfo.displaytype == 'Pie Chart') {
            chart = Ext.create('Ext.chart.Chart', {
                itemId: 'pieChart',
                animate: true,
                shadow: true,
                theme: 'Base:gradients',
                background: Computronix.POSSE.Dashboard.defaultBackground,
                store: jobInStatusStore,
                series: [{
                    type: 'pie',
                    field: 'y1',
                    showInLegend: true,
                    tips: {
                        trackMouse: true,
                        width: 200,
                        height: 40,
                        renderer: function (storeItem, item) {
                            //calculate and display percentage on hover
                            var total = 0;
                            jobInStatusStore.each(function (rec) {
                                total += rec.data.y1;
                            });
                            this.setTitle(storeItem.get('x') + ': ' + Math.round(storeItem.get('y1') / total * 100) + '%');
                        }
                    },
                    listeners: {
                        'itemmouseup': function (item) {
                            var drillDownUrl = widget.widgetInfo.drilldownurl;
                            if (drillDownUrl) {
                                var val = item.storeItem.data.x;
                                var href = widget.widgetInfo.drilldownurl + val;
                                href = href.replace('{fromdate}', Ext.util.Format.date(widget.widgetInfo.fromdate, 'Y-m-d'));
                                href = href.replace('{todate}', Ext.util.Format.date(widget.widgetInfo.todate, 'Y-m-d'));
                                window.open(href, "_blank");
                            }
                        }
                    },
                    highlight: {
                        segment: {
                            margin: 20
                        }
                    },
                    label: {
                        field: 'x',
                        display: 'rotate',
                        contrast: true,
                        font: '18px Arial'
                    }
                }]
            });

        } else {

            chart = Ext.create('Ext.chart.Chart', {
                itemId: 'barGraph',
                animate: true,
                shadow: true,
                theme: 'Base:gradients',
                background: Computronix.POSSE.Dashboard.defaultBackground,
                store: jobInStatusStore,
                axes: [{
                    type: 'Numeric',
                    position: 'left',
                    fields: ['y1'],
                    label: {
                        renderer: Ext.util.Format.numberRenderer('0')
                    },
                    title: widget.widgetInfo.yaxislabel,
                    grid: true,
                    minimum: 0
                }, {
                    type: 'Category',
                    position: 'bottom',
                    fields: ['x'],
                    title: widget.widgetInfo.xaxislabel
                }],
                series: [{
                    type: 'column',
                    axis: 'left',
                    highlight: true,
                    tips: {
                        trackMouse: true,
                        width: 200,
                        height: 40,
                        renderer: function (storeItem, item) {
                            this.setTitle(storeItem.get('x') + ': ' + Ext.util.Format.plural(storeItem.get('y1'), 'task'));
                        }
                    },
                    listeners: {
                        'itemmouseup': function (item) {
                            var drillDownUrl = widget.widgetInfo.drilldownurl;
                            if (drillDownUrl) {
                                var val = item.storeItem.data.x;
                                var href = widget.widgetInfo.drilldownurl + val;
                                href = href.replace('{fromdate}', Ext.util.Format.date(widget.widgetInfo.fromdate, 'Y-m-d'));
                                href = href.replace('{todate}', Ext.util.Format.date(widget.widgetInfo.todate, 'Y-m-d'));
                                window.open(href, "_blank");
                            }
                        },
                        label: {
                            display: 'insideEnd',
                            field: 'y1',
                            renderer: Ext.util.Format.numberRenderer('0'),
                            color: '#333',
                            'text-anchor': 'middle'
                        },
                        xField: 'x',
                        yField: 'y1'
                    }
                }]
            });
        }
        return chart;
    }
});