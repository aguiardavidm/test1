Ext.require('Ext.chart.*');
Ext.require(['Ext.data.*', 'Ext.grid.*']);
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);
Ext.require(['Ext.form.field.ComboBox', 'Ext.form.FieldSet', 'Ext.tip.QuickTipManager', 'Ext.data.*']);
Ext.require(['Ext.form.*', 'Ext.data.*']);
Ext.require(['Ext.panel.Panel', 'Ext.Action', 'Ext.button.Button', 'Ext.window.MessageBox']);
Ext.require(['Ext.selection.CellModel', 'Ext.ux.CheckColumn']);

var args = Ext.Object.fromQueryString(document.location.search);

//Change all property names in args to lowercase
for (var argName in args) {
    if (argName !== argName.toLowerCase()) {
        args[argName.toLowerCase()] = args[argName];
        delete args[argName];
    }
}

Computronix.POSSE.Dashboard.WIDGET_HEIGHT = 450;
Computronix.POSSE.Dashboard.allWidgets = [];
Computronix.POSSE.Dashboard.rootPanel = null;
Computronix.POSSE.Dashboard.categories = [];
Computronix.POSSE.Dashboard.selectedCategory = args.category || null;

Computronix.POSSE.Dashboard.settingWidgetModel = Computronix.POSSE.Dashboard.defineModel('SettingWidgetModel', {
    extend: 'Ext.data.Model',
    fields: [
                { name: 'id', type: 'int' },
                { name: 'category', type: 'string' },
                { name: 'title', type: 'string' },
                { name: 'selected', type: 'bool' }
            ]
});

Computronix.POSSE.Dashboard.loadWidget = function (widgetInfo, panel, height) {
    var widget = Ext.create(Computronix.POSSE.Dashboard.widgetClasses[widgetInfo.widgettype], {
        widgetInfo: widgetInfo,
        height: height,
        renderTo: panel
    });
}

Computronix.POSSE.Dashboard.drawCategory = function (categoryTitle, mainWidgets, smallWidgets) {
    var nextPosition = 0, i;
    var mainPanel = Computronix.POSSE.Dashboard.rootPanel.getComponent('mainPanel');
    Computronix.POSSE.Dashboard.rootPanel.getComponent('mainPanel').removeAll();
    //Computronix.POSSE.Dashboard.drawSettingButton();

    if (categoryTitle == null) {
        categoryTitle = "Alerts";  // Interesting Top Panel + Some widgets don't return a category.
    }

    // Create the left and right panels
    var leftPanel = new Ext.panel.Panel({
        columnWidth: 0.496,
        border: 0,
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            margin: '5 5 5 5'
        }
    });
    var rightPanel = new Ext.panel.Panel({
        columnWidth: 0.496,
        border: 0,
        layout: 'anchor',
        defaults: {
            anchor: '100%',
            margin: '5 5 5 5'
        }
    });

    var categoryPanel = Ext.create('Ext.panel.Panel', {
        title: ''/*categoryTitle*/,
        layout: 'column',
        collapsible: false,
        border: 0,
        items: [
            leftPanel,
            rightPanel
        ]
    });

   mainPanel.add(categoryPanel);

    // Now display all gauges at 1/4 size filling in the remaining area.
    //if (nextPosition > 0) {
    //    nextPosition = -4;
    //}
    for (i = 0; i < smallWidgets.length; i++) {
        var destPanel = null;
        if (nextPosition < 0) {
            destPanel = rightPanel;
        }
        else if ((i % 4) < 2) {
            destPanel = leftPanel;
        }
        else {
            destPanel = rightPanel;
        }

        nextPosition++;
        if (i % 2 == 0) {
            leftGauge = new Ext.panel.Panel(
                {
                    columnWidth: 0.5,
                    border: 0,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100% 100%',
                        margin: '0 5 0 0',
                        height: Computronix.POSSE.Dashboard.WIDGET_HEIGHT / 2
                    }
                });
            rightGauge = new Ext.panel.Panel(
                {
                    columnWidth: 0.5,
                    border: 0,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100% 100%',
                        margin: '0 0 0 5',
                        height: Computronix.POSSE.Dashboard.WIDGET_HEIGHT / 2
                    }
                });
            var gaugePanel = Ext.create('Ext.panel.Panel', {
                //plugins: [new Ext.ux.FitToParent("destPanel")],
                border: 0,
                margins: '0 0 0 0',
                layout: 'column',
                items: [
                        leftGauge,
                        rightGauge
                    ]
            });
            destPanel.add(gaugePanel);
        }


        if (i % 2 == 0) {
            Computronix.POSSE.Dashboard.loadWidget(smallWidgets[i], leftGauge, (Computronix.POSSE.Dashboard.WIDGET_HEIGHT / 2) - 5);
        } else {
            Computronix.POSSE.Dashboard.loadWidget(smallWidgets[i], rightGauge, (Computronix.POSSE.Dashboard.WIDGET_HEIGHT / 2) - 5);
        }
    }

    // First display all normal widgets..
    for (i = 0; i < mainWidgets.length; i++) {
        if (i % 2 == 0) {
            Computronix.POSSE.Dashboard.loadWidget(mainWidgets[i], leftPanel, Computronix.POSSE.Dashboard.WIDGET_HEIGHT);
        } else {
            Computronix.POSSE.Dashboard.loadWidget(mainWidgets[i], rightPanel, Computronix.POSSE.Dashboard.WIDGET_HEIGHT);
        }
        nextPosition = ((i + 1) % 2);
    }

}

Computronix.POSSE.Dashboard.drawSidebarCategory = function (pos) {
    var category = Computronix.POSSE.Dashboard.categories[pos];
    Computronix.POSSE.Dashboard.selectedCategory = category.categoryName;
    Computronix.POSSE.Dashboard.drawCategory(category.categoryName, category.mWidgets, category.sWidgets);
}


Computronix.POSSE.Dashboard.updateWidgetList = function (settingListStore) {
    var i, j;
    var widgetList = [], visibleList = [];
    var allWidgets = Computronix.POSSE.Dashboard.allWidgets;
    for (i = 0; i < allWidgets.length; i++) {
        var widget = allWidgets[i];
        for (j = 0; j < settingListStore.data.items.length; j++) {
            if (widget.widgetid == settingListStore.data.items[j].data.id) {
                widget.visible = settingListStore.data.items[j].data.selected;
            }
        }
        widgetList.push(widget.widgetid);
        visibleList.push(widget.visible ? 'Y' : 'N');
    }

    // Update the settings for the current user
    Ext.Ajax.request({
        url: 'WidgetHandler.ashx?action=SetDashboardInfo',
        method: 'POST',
        params: {
            widgetlist: widgetList.join(','),
            visiblelist: visibleList.join(',')
        },
        success: function (response, opts) {
            Computronix.POSSE.Dashboard.drawDashboard();
        },
        failure: function (response, opts) {
            try {
                result = Ext.JSON.decode(response.responseText);
                alert(result.Message);
            } catch (err) {
                alert(err);
            }
            //alert("Update failed");
            Computronix.POSSE.Dashboard.drawDashboard();
        }
    });
}

Computronix.POSSE.Dashboard.displaySettingsWindow = function () {

    var allWidgets = Computronix.POSSE.Dashboard.allWidgets;
    var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
        groupHeaderTpl: 'Category: {name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
    });

    var settingListStore = null;
    if (!settingListStore) {
        var i;
        var widgetList = [];
        // Scan the list separating out the widgets by widget Title:
        for (i = 0; i < allWidgets.length; i++) {
            var widget = allWidgets[i];

            widgetList[i] = {
                id: widget.widgetid,
                category: widget.category,
                title: widget.title,
                selected: widget.visible
            };
        }


        var settingListStore = Ext.create('Ext.data.Store', {
            autoLoad: true,
            autoDestroy: true,
            model: Computronix.POSSE.Dashboard.settingWidgetModel,
            data: widgetList,
            sorters: ['category', 'title'],
            groupField: 'category'
        });
    }

    var setWidgets = Ext.create('Ext.Window', {
        height: 600,
        width: 400,
        minHeight: 400,
        minWidth: 300,
        hidden: true,
        maximizable: false,
        closeAction: 'hide',
        modal: true,
        title: 'Select Charts',
        renderTo: Ext.getBody(),
        layout: 'fit',
        items: [{
            xtype: 'grid',
            border: false,
            store: settingListStore,
            features: [groupingFeature],
            columns: [{
                header: 'Title',
                flex: 1,
                dataIndex: 'title'
            }, {
                xtype: 'checkcolumn',
                header: 'Selected',
                dataIndex: 'selected',
                width: 55
            }],
            frame: true,
            bbar: [{
                xtype: 'button',
                text: 'OK',
                cls: 'x-btn-text-icon',
                icon: 'extjs/resources/themes/images/default/dd/drop-yes.gif',
                handler: function (item, value, options) {
                    Computronix.POSSE.Dashboard.rootPanel.getComponent('mainPanel').removeAll();
                    Computronix.POSSE.Dashboard.drawSettingButton(allWidgets, settingListStore);
                    Computronix.POSSE.Dashboard.sidebarPanel.getComponent('sidebarPane').removeAll();
                    Computronix.POSSE.Dashboard.updateWidgetList(settingListStore, allWidgets);
                    setWidgets.hide();
                }
            }, {
                xtype: 'button',
                text: 'Cancel',
                cls: 'x-btn-text-icon',
                icon: 'extjs/resources/themes/images/default/dd/drop-no.gif',
                handler: function (item, value, options) {
                    setWidgets.hide();
                }
            }]
        }]
    });

    setWidgets.show();
}

Computronix.POSSE.Dashboard.drawSettingButton = function() {
    var settingButton = Ext.create('Ext.panel.Panel', {
        border: 0,
        height: 35,
        width: 200,
        padding: '5 5 5 5',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'fieldcontainer',
            itemId: 'WidgetSettings',
            height: 25,
            layout: 'hbox',
            items: [{
                xtype: 'button',
                style: 'margin-left: 125px;',
                text: 'Settings...',
                itemId: 'goButton',
                handler: function (item, value, options) {
                    Computronix.POSSE.Dashboard.displaySettingsWindow();
                }
            }]
        }]
    });
    Computronix.POSSE.Dashboard.sidebarPanel.getComponent('sidebarPane').add(settingButton);
}

Computronix.POSSE.Dashboard.drawDashboard = function () {
    var i;
    //debugger;
    var mainWidgets = new Array();
    var smallWidgets = new Array();
    var currentCategory = null;
    var sideCategories = new Array();
    var allWidgets = Computronix.POSSE.Dashboard.allWidgets;


    // Scan the list separating out the widgets by:
    // 1) Category,
    // 2) the widgets that need to appear at quarter size..
    // Draw the category as soon as I bump into the next.
    for (i = 0; i < allWidgets.length; i++) {
        var widget = allWidgets[i];

        if (!widget.visible) {
            continue;
        }

        if (currentCategory == null && widget.category != "") {
            currentCategory = widget.category;
        }
        else if (currentCategory != widget.category && widget.category != "") {
            // Currently a number of widgets return no category.
            sideCategories.push({
                categoryName: currentCategory,
                mWidgets: mainWidgets,
                sWidgets: smallWidgets
            });
            //drawCategory(currentCategory, mainWidgets, smallWidgets);
            currentCategory = widget.category;
            mainWidgets = new Array();
            smallWidgets = new Array();
        }

        if (widget.widgettype.indexOf("WarningGauge") != -1) {
            smallWidgets.push(widget);
        }
        else {
            mainWidgets.push(widget);
        }
    }
    sideCategories.push({
        categoryName: currentCategory,
        mWidgets: mainWidgets,
        sWidgets: smallWidgets
    });
    Computronix.POSSE.Dashboard.categories = sideCategories;
    Computronix.POSSE.Dashboard.drawSidebar();
    //drawCategory(currentCategory, mainWidgets, smallWidgets);
}


Computronix.POSSE.Dashboard.drawSidebar = function () {
    var i;
    //set global Side Category
    var sideCategories = Computronix.POSSE.Dashboard.categories;
    var l = sideCategories.length;
    var sclass;

    if (l > 0) {
        /*var mw = new Array();
        var sw = new Array();
        mw = mainWidgets[sideCategories[1]];
        sw = smallWidgets[sideCategories[1]];*/
        //Computronix.POSSE.Dashboard.drawCategory(sideCategories[0].categoryName, sideCategories[0].mWidgets, sideCategories[0].sWidgets);
        var sidebarText = '<ul>';
        for (i = 0; i < l; i++) {
            //mw = sideCategories[i].mWidgets;
            //sw = sideCategories[i].sWidgets;

            if (Computronix.POSSE.Dashboard.selectedCategory != null) {

                if (sideCategories[i].categoryName == Computronix.POSSE.Dashboard.selectedCategory) {
                    sclass = 'class="active"';
                    Computronix.POSSE.Dashboard.drawCategory(sideCategories[i].categoryName, sideCategories[i].mWidgets, sideCategories[i].sWidgets);
                }
                else {
                    sclass = "";
                } 
            }

            else if (i == 0 && (Computronix.POSSE.Dashboard.selectedCategory == null)) {
                sclass = 'class="active"';
                Computronix.POSSE.Dashboard.drawCategory(sideCategories[i].categoryName, sideCategories[i].mWidgets, sideCategories[i].sWidgets);
            }
            else {
                sclass = "";
            }

            sidebarText = sidebarText + '<li ' + sclass + ' onclick="Computronix.POSSE.Dashboard.drawSidebarCategory(' + i + ');"> ' + sideCategories[i].categoryName + '</li>';
        }
        sidebarText = sidebarText + '</ul>';
    }
    else { sidebarText = '<h2>No Categories Found</h2>'; }
    document.getElementById('categorysidebar').innerHTML = sidebarText;
}


Ext.onReady(function () {
    Computronix.POSSE.Dashboard.rootPanel = Ext.create('Ext.panel.Panel', {
        renderTo: 'dashboard',
        plugins: function () {
            if (Ext.isIE && !Ext.isIE9)
                return null;
            return [new Ext.ux.FitToParent("dashboard")];
        } (),
        margins: '0 0 0 0',
        layout: 'column',
        border: 0,
        items: [
        {
            itemId: "mainPanel",
            columnWidth: .985,
            border: 0,
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                margin: '0 0 0 3'
            }
        }],
        autoScroll: true
    });
    var mainPanel = Computronix.POSSE.Dashboard.rootPanel.getComponent('mainPanel');

    Computronix.POSSE.Dashboard.sidebarPanel = Ext.create('Ext.panel.Panel', {
        renderTo: 'settingsButtonHidden',
        plugins: function () {
            if (Ext.isIE && !Ext.isIE9)
                return null;
            return [new Ext.ux.FitToParent("settingsButtonHidden")];
        } (),
        margins: '0 0 0 0',
        layout: 'column',
        border: 0,
        items: [
        {
            itemId: "sidebarPane",
            columnWidth: .95,
            border: 0,
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                margin: '20 0 0 3'
            }
        }],
        autoScroll: true
    });


    // Fetch the widgets for the dashboard
    var results;

    if (args.widgetid || (args.dashboardname && args.chartname)) {

        Computronix.POSSE.Dashboard.Ajax.request({
            url: 'WidgetHandler.ashx',
            params: function () {
                var paramsObj = {
                    action: 'GetWidgetInfo'
                }

                if (args.widgetid) {
                    paramsObj.WidgetId = args.widgetid; }
                else if (args.dashboardname && args.chartname) {
                    paramsObj.action = 'GetWidgetInfoForName';
                    paramsObj.dashboardName = args.dashboardname;
                    paramsObj.chartName = args.chartname; }

                if (args.period) {
                    paramsObj.period = args.period; }
                else if (args.fromdate || args.todate) {
                    paramsObj.period = 'Custom'; }

                if (args.fromdate) {
                    paramsObj.fromDate = args.fromdate; }
                if (args.todate) {
                    paramsObj.toDate = args.todate; }

                if (args.group1value) {
                    paramsObj.group1value = args.group1value; }
                if (args.group2value) {
                    paramsObj.group2value = args.group2value; }
                if (args.group3value) {
                    paramsObj.group3value = args.group3value; }
                if (args.group4value) {
                    paramsObj.group4Value = args.group4value; }

                if (args.view) {
                    paramsObj.view = args.view; }

                return paramsObj;
            },
            method: 'GET',
            success: function (response, opts) {
                try {
                    results = Ext.JSON.decode(response.responseText);
                } catch (err) {
                    alert(err);
                }

                if (results.error) {
                    alert("Error: " + results.error.message);
                }
                else {
                    Computronix.POSSE.Dashboard.allWidgets.push(results);
                    Computronix.POSSE.Dashboard.drawDashboard(Computronix.POSSE.Dashboard.allWidgets);
                }
            },
            failure: function (response, opts) {
                try {
                    results = Ext.JSON.decode(response.responseText);
                    alert(results.Message);
                } catch (err) {
                    alert(err);
                }
            }
        });
    }
    else {
        args.dashboardname = args.dashboardname || "Main";
        Computronix.POSSE.Dashboard.Ajax.request({
            url: 'WidgetHandler.ashx?action=GetDashboardInfo&DashboardName=' + args.dashboardname,
            method: 'GET',
            success: function (response, opts) {
                try {
                    results = Ext.JSON.decode(response.responseText);
                    Computronix.POSSE.Dashboard.allWidgets = results;
                } catch (err) {
                    alert(err);
                }

                if (results.error) {
                    alert("Error: " + results.error.message);
                }
                else {                   
                    //Computronix.POSSE.Dashboard.drawSettingButton();
                    Computronix.POSSE.Dashboard.drawDashboard();
                }
            },
            failure: function (response, opts) {
                try {
                    results = Ext.JSON.decode(response.responseText);
                    alert(results.Message);
                } catch (err) {
                    alert(err);
                }
            }
        });
    }
});
