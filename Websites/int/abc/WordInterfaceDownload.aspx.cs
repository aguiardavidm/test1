using System.Text;
using System;
using System.Collections.Generic;

namespace Computronix.POSSE.Outrider
{
	partial class WordInterfaceDownload : Computronix.POSSE.Outrider.PageBase
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, System.EventArgs e)
		{
			this.RenderWordInterface();
		}
		#endregion

		#region PageBaseUI
        /// <summary>
        /// Upload a new document to Outrider then copy it and download it to the stream
        /// </summary>
        protected virtual void RenderWordInterface()
        {

			string fileName, newFileName, action = this.Request.QueryString["Action"], documentId = "0";

			if (action == "New")
			{
				if (this.Document != null)
				{
					if (this.DocumentExtension.ToLower() == "docx")
					{

						//Get values from query string (from WordInterfaceDownloadCreateDoc global expression)
						string wordInterfaceDocumentType = this.Request.QueryString["DocumentType"],
							wordInterfaceDocumentTypeId = this.Request.QueryString["DocumentTypeId"],
							wordInterfaceClassification = this.Request.QueryString["Classification"],
							wordInterfaceLetterId = this.Request.QueryString["LetterObjectId"],
							wordInterfaceTemplateId = this.Request.QueryString["PosseObjectId"],
							wordInterfaceMacroId = this.Request.QueryString["MacroObjectId"],
							wordInterfaceEndpoint = this.Request.QueryString["EndPointName"];
                            

						// Create a new document copied from the template
						newFileName = wordInterfaceTemplateId + documentId;
	                    newFileName += this.GetCheckDigit(newFileName) + wordInterfaceTemplateId.Length.ToString() + ".docm";

						StringBuilder processXML;
						processXML = new StringBuilder();
						int newDocumentId = this.OutriderNet.SetDocument(Convert.ToInt32(wordInterfaceDocumentTypeId), "WordInterface", "docm", this.Document);
						string newObjectId = "NEW" + DateTime.Now.Millisecond.ToString();
						
						//Sets all details and relates to the Macro and "shadow document" Letter object
						processXML.AppendFormat(String.Concat(
						    "<object id=\"{8}\" action=\"Insert\" objectdef=\"{0}\">",
						      "<pendingdocument id=\"{1}\" />",
						      "<column name=\"FileName\">{2}</column>",
						      "<column name=\"Description\">{3}</column>",
						      "<column name=\"Classification\">{4}</column>",
						      "<column name=\"FileExtension\">docm</column>",
						      "<column name=\"FileSize\">{5}</column>",
						      "<column name=\"CreatedByWordInterface\">Y</column>",
						      "<column name=\"d_WordMergeTemplate_ObjectId\">{6}</column>",
						      "<column name=\"d_WordMergeMacro_ObjectId\">{7}</column>",
						      "<column name=\"PosseSessionToken\">{11}</column>",
						    "</object>",
						    "<object id=\"{8}\">",
						      "<relationship endpoint=\"{12}\" toobjectid=\"{9}\" action=\"Insert\"></relationship>",
						    "</object>",
						    "<object id=\"{8}\">",
						      "<relationship endpoint=\"Macro\" toobjectid=\"{10}\" action=\"Insert\"></relationship>",
						    "</object>"),
						    wordInterfaceDocumentType, newDocumentId,
						    newFileName, newFileName, wordInterfaceClassification, this.Document.Length,
						    wordInterfaceTemplateId, wordInterfaceMacroId, newObjectId,
						    wordInterfaceLetterId, wordInterfaceMacroId, this.SessionId, wordInterfaceEndpoint);
		
						// Now call ProcessXML to make the document real
						this.OutriderNet.ProcessXML(processXML.ToString());
						
						List<object> results;
						results = (List<object>)ExecuteSql("GetWordMergeLetterId", String.Format("T_LETTERID={0}", wordInterfaceLetterId));
		                var docFromDatabaseId = (string)((Dictionary<string, object>)(results[0]))["T_WORDLETTERID"];

						//Download the newly created document as a stream to the browser
						byte[] newDocument = this.GetDocument(docFromDatabaseId);
						fileName = wordInterfaceLetterId + documentId;
	                    fileName += this.GetCheckDigit(fileName) + wordInterfaceLetterId.Length.ToString() + ".docm";

	                    this.CheckClient();
						this.Response.Clear();
						this.Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName);
						this.Response.AddHeader("Content-Length", newDocument.Length.ToString());
						this.Response.ContentType = "application/msword";

						this.Response.OutputStream.Write(newDocument, 0, newDocument.Length);
						this.Response.End();
					}
				}
			}
			if (action == "Download")
			{
				//Get values from query string
				string wordInterfaceLetterId = this.Request.QueryString["LetterObjectId"],
					   wordInterfaceDocId = this.Request.QueryString["PosseObjectId"];

				//Download the document to the browser
				this.CheckClient();
				this.Response.Clear();

				//Resets the session id
				StringBuilder processXML;
				processXML = new StringBuilder();
				processXML.AppendFormat(String.Concat(
					"<object id=\"{0}\" action=\"Update\">",
					"<column name=\"PosseSessionToken\">{1}</column>",
					"</object>"),
					wordInterfaceDocId, this.SessionId);
				this.OutriderNet.ProcessXML(processXML.ToString());
				
				fileName = wordInterfaceLetterId + documentId;
                fileName += this.GetCheckDigit(fileName) + wordInterfaceLetterId.Length.ToString();
                var docByte = this.OutriderNet.GetDocument(wordInterfaceDocId);
				string docExt = this.OutriderNet.GetDocumentExtension(wordInterfaceDocId);

				//this.Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName);
				//this.Response.AddHeader("Content-Length", docByte.Length.ToString());
				//this.Response.ContentType = "application/msword";
				this.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.{1}", fileName, docExt));
                this.Response.ContentType = "application/octet-stream";

                this.Response.OutputStream.Write(docByte, 0, docByte.Length);
				this.Response.End();
			}

		}

        // Return the check digit for the given token.
		public string GetCheckDigit(string token)
		{
			string result = "-1";
			int digit, total = 0;

			for (int index = token.Length - 1; index >= 0; index--)
			{
				digit = int.Parse(token.Substring(index, 1));

				if (index % 2 == 1)
				{
					digit *= 2;

					if (digit > 9)
					{
						digit -= 9;
					}
				}

				total += digit;
			}

			result = (total % 10).ToString();

			return result;
		}
		#endregion
	}
}