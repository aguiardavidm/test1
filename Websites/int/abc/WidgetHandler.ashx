﻿<%@ WebHandler Language="C#" Class="Computronix.POSSE.Outrider.WidgetHandler" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Globalization;
using ClosedXML.Excel;
using System.Diagnostics;
using cxJSON;

namespace Computronix.POSSE.Outrider
{
    public class WidgetHandler : HandlerBase
    {

        protected string GetWidgetJson(int widgetId, string widgetArgs)
        {
            // Execute the GetWidgetJSON registered statement
            string args = String.Format("WidgetId={0}&WidgetArgs={1}", widgetId, HttpUtility.UrlEncode(widgetArgs));
            List<object> results = (List<object>)ExecuteSql("GetWidgetJSON", args);
            return (string)((Dictionary<string, object>)(results[0]))["WIDGETJSON"];
        }

        protected MemoryStream ExportObjects(int widgetId, string widgetArgs)
        {
            MemoryStream outputXLSX = new MemoryStream();
//            Stopwatch stopwatch = new Stopwatch();
//            stopwatch.Start();
            
            XLWorkbook workBook = new XLWorkbook();
            IXLWorksheet dataSheet = workBook.Worksheets.Add("Data");
            IXLWorksheet sumSheet = workBook.Worksheets.Add("Summary");
            
            //Execute the ExportDashboardObjects and GetChartJSON registered statement
            string args = String.Format("WidgetId={0}&WidgetArgs={1}", widgetId, HttpUtility.UrlEncode(widgetArgs));
//            dataSheet.Cell(1, 26).Value = stopwatch.ElapsedMilliseconds.ToString();
            List<object> ExportResults = (List<object>)ExecuteSql("ExportDashboardObjects", args);
//            dataSheet.Cell(1, 27).Value = stopwatch.ElapsedMilliseconds.ToString();
            List<object> ChartResults = (List<object>)ExecuteSql("GetWidgetJSON", args);
//            dataSheet.Cell(1, 28).Value = stopwatch.ElapsedMilliseconds.ToString();
         
            int col_num = 1;
            int row_num = 1;
            int col_count = 0;

            //Parse the JSON results
//            dataSheet.Cell(1, 29).Value = stopwatch.ElapsedMilliseconds.ToString();
            List<object> rows = (List<object>)JSON.Instance.Parse((string)((Dictionary<string, object>)(ExportResults[0]))["WIDGETJSON"]);
//            dataSheet.Cell(1, 30).Value = stopwatch.ElapsedMilliseconds.ToString();
            
            List<string> keys = new List<string>(((Dictionary<string, object>)rows[0]).Keys);

            //Add the headers for Data sheet
            foreach (string key in keys)
            {
                dataSheet.Cell(row_num, col_num).Value = key.ToString();
                col_num++;
            }

            row_num++;

            //Add the data for Data Sheet 
            foreach (Dictionary<string, object> row in rows)
            {
                col_num = 1;
                col_count = row.Values.Count;
                foreach (object item in row.Values)
                {
                    try
                    {
                        dataSheet.Cell(row_num, col_num).SetValue<string>(item.ToString());
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(item.ToString());
                    }
                    //dataSheet.Cell(row_num, 26).Value = stopwatch.ElapsedMilliseconds.ToString();
                    col_num++;
                }

                row_num++;
            }

            //dataSheet.Cell(row_num, col_num+1).Value = stopwatch.ElapsedMilliseconds.ToString();
            
            //Style and Formatting
            dataSheet.Rows(1, 1).Style.Font.Bold = true;
            dataSheet.Columns(1, col_count).AdjustToContents();

            //////////////////////////////////////////////////////////////////////////
            /// Second Sheet                                                       ///
            /// //////////////////////////////////////////////////////////////////////
            col_num = 2;
            row_num = 1;
            col_count = 1;

            Dictionary<string, object> items = (Dictionary<string, object>)JSON.Instance.Parse((string)((Dictionary<string, object>)(ChartResults[0]))["WIDGETJSON"]);

            //If there is only a single series being displayed (i.e. serieslabels only has y1 in it), then display chart.yaxislabel as the second column
            List<object> serieslabels = (List<object>)items["serieslabels"];
            if (serieslabels.Count == 1)
            {
                sumSheet.Cell(row_num, col_num).Value = items["yaxislabel"].ToString();
            }
            else if (serieslabels.Count > 1)
            {
                foreach (var name in serieslabels)
                {
                    sumSheet.Cell(row_num, col_num).Value = name.ToString();
                    col_num++;
                }
            }
            else
            {
                throw new Exception("Error in JSON decode. 'Serieslabels' is invalid.");
            }

            //Populate the rows for each label along the bottom of the chart.
            row_num = 2;
            List<object> data = (List<object>)items["data"];
            foreach (Dictionary<string, object> item in data)
            {
                col_num = 1;
                sumSheet.Cell(row_num, col_num).Value = item["x"].ToString();
                col_num++;
                string current_y = "y1";
                do
                {
                    sumSheet.Cell(row_num, col_num).Value = item[current_y].ToString();
                    current_y = "y" + col_num;
                    col_num++;
                } while (item.ContainsKey(current_y));
                col_count = col_num;
                row_num++;
            }

            //Display and formatting
            sumSheet.Rows(1, 1).Style.Font.Bold = true;
            sumSheet.Columns(1, 1).Style.Font.Bold = true;
            sumSheet.Columns(1, col_count).AdjustToContents();           
            workBook.SaveAs(outputXLSX);
            return outputXLSX;
        }
        
        protected string GetDrillDownObjects(int widgetId, string widgetArgs)
        {
            //Execute the GetDrillDownObjects registered statement
            string args = String.Format("WidgetId={0}&WidgetArgs={1}", widgetId, HttpUtility.UrlEncode(widgetArgs));
            List<object> results = (List<object>)ExecuteSql("GetDrillDownObjects", args);

            return (string)((Dictionary<string, object>)(results[0]))["WIDGETJSON"];
            //string results = (string)ExecuteSqlTest("TestString");
            //return results;
        }

        protected string GetWidgetInfo(int widgetId, string widgetArgs)
        {
            // Execute the GetWidgetInfo registered statement
            string args = String.Format("WidgetId={0}&WidgetArgs={1}", widgetId, HttpUtility.UrlEncode(widgetArgs));
            List<object> results = (List<object>)ExecuteSql("GetWidgetInfo", args);
            return (string)((Dictionary<string, object>)(results[0]))["WIDGETJSON"];
        }

        protected string GetWidgetInfoForName(string dashboardName, string chartName, string widgetArgs)
        {
            // Execute the GetWidgetInfo registered statement
            string args = String.Format("DashboardName={0}&ChartName={1}&ChartArgs={2}", dashboardName, chartName, HttpUtility.UrlEncode(widgetArgs));
            List<object> results = (List<object>)ExecuteSql("GetWidgetInfoForName", args);
            return (string)((Dictionary<string, object>)(results[0]))["WIDGETJSON"];
        }

        protected string GetDashboardInfo(string dashboardName)
        {
            // Execute the GetDashboardInfo registered statement
            string args = String.Format("DashboardName={0}", dashboardName);
            List<object> results = (List<object>)ExecuteSql("GetDashboardInfo", args);
            return (string)((Dictionary<string, object>)(results[0]))["DASHBOARDJSON"];
        }

        protected string GetAccessGroupUsers(string accessGroup)
        {
            // Execute the GetAccessGroupUsersJSON registered statement
            string args = String.Format("AccessGroup={0}", accessGroup);
            List<object> results = (List<object>)ExecuteSql("GetAccessGroupUsersJSON", args);
            return (string)((Dictionary<string, object>)(results[0]))["USERSJSON"];
        }

        protected void SetDashboardInfo(string widgetList, string visibleList)
        {
            // Execute the SetDashboardInfo registered statement
            string args = String.Format("WidgetList={0}&VisibleList={1}", widgetList, visibleList);
            object results = (List<object>)ExecuteSql("SetDashboardInfo", args);
            return;
        }

        public override void ProcessRequest()
        {
            string jSON;
            
            switch (this["action"])
            {
                case "GetWidgetData":
                    // Build the widget args from the URL query string
                    StringBuilder widgetArgs = new StringBuilder();
                    foreach (string key in this.Request.QueryString)
                    {
                        if (key.ToUpper() != "WIDGETID" && key.ToUpper() != "ACTION")
                        {
                            if (widgetArgs.Length > 0)
                                widgetArgs.Append("&");
                            widgetArgs.AppendFormat("{0}={1}", key, HttpUtility.UrlEncode(this.Request.QueryString[key]));
                        }
                    }
                    jSON = GetWidgetJson(int.Parse(this.Request.QueryString["WidgetId"]), widgetArgs.ToString());
                    this.Response.Write("[" + jSON + "]");
                    break;
                    
                case "GetDrillDownObjects":
                    // Build the widget args from the URL query string
                    StringBuilder widgetArgs2 = new StringBuilder();
                    foreach (string key in this.Request.QueryString)
                    {
                        if (key.ToUpper() != "WIDGETID" && key.ToUpper() != "ACTION")
                        {
                            if (widgetArgs2.Length > 0)
                                widgetArgs2.Append("&");
                            widgetArgs2.AppendFormat("{0}={1}", key, HttpUtility.UrlEncode(this.Request.QueryString[key]));
                        }
                    }
                    jSON = GetDrillDownObjects(int.Parse(this.Request.QueryString["WidgetId"]), widgetArgs2.ToString());
                    
                    this.Response.Write("[" + jSON + "]");
                    
                    break;
                case "ExportObjects":
                        MemoryStream output;
                        // Build the widget args from the URL query string
                        widgetArgs2 = new StringBuilder();
                        foreach (string key in this.Request.QueryString)
                        {
                            if (key.ToUpper() != "WIDGETID" && key.ToUpper() != "ACTION")
                            {
                                if (widgetArgs2.Length > 0)
                                    widgetArgs2.Append("&");
                                widgetArgs2.AppendFormat("{0}={1}", key, HttpUtility.UrlEncode(this.Request.QueryString[key]));
                            }
                        }
                        output = ExportObjects(int.Parse(this.Request.QueryString["WidgetId"]), widgetArgs2.ToString());

                        Response.ClearHeaders();
                        Response.ClearContent();
                        Response.Clear();
                        Response.Buffer = true;

                        Response.AddHeader("Content-Disposition", "Attachment; filename=" + "Result.xlsx");
                        Response.AddHeader("Content-Length", output.Length.ToString());
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                        output.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    
                    break;

                case "GetWidgetInfo":
                    // Build the widget args from the URL query string
                    StringBuilder widgetInfoArgs = new StringBuilder();
                    foreach (string key in this.Request.QueryString)
                    {
                        if (key.ToUpper() != "WIDGETID" && key.ToUpper() != "ACTION")
                        {
                            if (widgetInfoArgs.Length > 0)
                                widgetInfoArgs.Append("&");
                            widgetInfoArgs.AppendFormat("{0}={1}", key, HttpUtility.UrlEncode(this.Request.QueryString[key]));
                        }
                    }
                    this.Response.Write(GetWidgetInfo(int.Parse(this.Request.QueryString["WidgetId"]), widgetInfoArgs.ToString()));
                    break;
                case "GetWidgetInfoForName":
                    // Build the widget args from the URL query string
                    StringBuilder widget3Args = new StringBuilder();
                    foreach (string key in this.Request.QueryString)
                    {
                        if (key.ToUpper() != "DASHBOARDNAME" &&key.ToUpper() != "CHARTNAME" && key.ToUpper() != "ACTION")
                        {
                            if (widget3Args.Length > 0)
                                widget3Args.Append("&");
                            widget3Args.AppendFormat("{0}={1}", key, HttpUtility.UrlEncode(this.Request.QueryString[key]));
                        }
                    }
                    this.Response.Write(GetWidgetInfoForName(this.Request.QueryString["DashboardName"], this.Request.QueryString["ChartName"], widget3Args.ToString()));
                    break;
                case "GetDashboardInfo":
                    this.Response.Write(GetDashboardInfo(this["dashboardname"].ToString()));
                    break;
                case "GetAccessGroupUsers":
                    this.Response.Write(GetAccessGroupUsers(this["accessgroup"].ToString()));
                    break;
                case "SetDashboardInfo":
                    this.SetDashboardInfo(this.Request.Form["widgetlist"], this.Request.Form["visiblelist"]);
                    break;
            }
        }
    }
}