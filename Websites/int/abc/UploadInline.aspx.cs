using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
	public partial class Upload : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            this.RenderTitle(this.pnlTitleBand, "Upload Document");
			this.RenderErrorMessage(this.pnlPaneBand);
			this.SetInitialFocus(this.fullFileName);

			// If an end point is not provided then hide any additional data columns.
			if (string.IsNullOrEmpty(this.Request.QueryString["UploadEndPoint"]) || string.IsNullOrEmpty(this.Request.QueryString["ShowDescription"]))
			{
				this.pnlData.Visible = false;
			}
		}

		/// <summary>
		/// Click event handler for the upload button.
		/// </summary>
		/// <param name="sender">A reference to the upload button.</param>
		/// <param name="e">Event arguments.</param>
		protected void btnUpload_Click(object sender, EventArgs e)
		{
			string extension;
			bool success = false;

			if (this.fullFileName.HasFile)
			{
				if (this.fullFileName.FileBytes.Length > this.MaxFileSize)
				{
					this.ErrorMessage = "Upload Error: Files greater than " + this.MaxFileSize.ToString() + " bytes are not allowed.";
					this.RenderErrorMessage(this.pnlPaneBand);
				}
				else
				{
					extension = this.fullFileName.FileName.Substring(this.fullFileName.FileName.LastIndexOf(".") + 1);

					try
					{
						success = UploadDocument(this.fullFileName.FileBytes, this.fullFileName.FileName, extension);
					}
					catch (Exception exception)
					{
						this.ProcessError(exception);
						this.RenderErrorMessage(this.pnlPaneBand);
					}

					if (success)
					{
						this.LastUploadFileName = this.fullFileName.FileName;
						this.LastUploadExtension = extension;
						this.LastUploadSize = this.fullFileName.PostedFile.ContentLength;
						this.LastUploadContentType = this.fullFileName.PostedFile.ContentType;

						this.ClientScript.RegisterStartupScript(this.GetType(), "UploadCompleteScript",
							this.GetUploadInlineCompleteScript(this.txtDescription.Text, this.AutoSubmit), true);
					}
				}
			}
		}
		#endregion
	}
}
