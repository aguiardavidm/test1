//// *********************************** ////
//// State of California master template ////
////             Version 1.20            ////
////       Last Updated 03/23/2007       ////
//// *********************************** ////


initNavigation = function() {    
	if (document.getElementById) { // Does the browser support the getElementById method?
	    navigationBand = document.getElementById("navigation");
		navRoot = document.getElementById("nav_list"); // Get main list ul
		lineHeight = 25;

        if (navRoot == null) // If there is no tabs on this page, exit this function
            return;
            
		for (i=0; i<navRoot.childNodes.length; i++) { // Loop over main list items
			node = navRoot.childNodes[i];
				 		   
			if (node.nodeName == "LI") {
    		    if (node.getElementsByTagName("ul")[0]) 
	 		        setWidth(node.getElementsByTagName("ul")[0], Math.max(document.body.clientWidth, 600));
	 		        setWidth(navigationBand, Math.max(document.body.clientWidth, 600));
				if (node.className == "nav_default_on") { // Found default main nav item
					defaultMainListIndex = i;
					
					if (node.getElementsByTagName("ul").length > 0) { // Check if there are any entrypoints for this group
                        if (Math.floor(getHeight(node.getElementsByTagName("ul")[0])/lineHeight) > 1 ) // Set class for navigation band on default item
		    			   defaultMainListClassName = Math.floor(getHeight(node.getElementsByTagName("ul")[0])/lineHeight) + "_lines";					   
		    			else
		    			   defaultMainListClassName = "";
					}
					else
					    defaultMainListClassName = "";

					navigationBand.className = defaultMainListClassName;
				} else {

					////// Apply onmouseover and onmouseout event handlers to each main list item //////
					node.onmouseover = function() {
						if (defaultMainListIndex != -1) { // Is there a default main list item?
							navRoot.childNodes[defaultMainListIndex].className = "nav_default_off"; // De-activate it														
						}
						this.className = "mouse_over"; // Activate the hovered item
						
						if (this.getElementsByTagName("ul").length > 0) { // Check if there are any entrypoints for this group
						   if (Math.floor(getHeight(this.getElementsByTagName("ul")[0])/lineHeight) > 1 ) // Change class for navigation band
					          navigationBand.className = Math.floor(getHeight(this.getElementsByTagName("ul")[0])/lineHeight) + "_lines";
					       else
					          navigationBand.className = "";
					    }
					    else
					       navigationBand.className = "";
						
						
					}
					node.onmouseout = function() {
					    navigationBand.className = defaultMainListClassName;
						this.className = ""; // De-activate the hovered item
						if (defaultMainListIndex != -1) { // Is there a default main list item?
							navRoot.childNodes[defaultMainListIndex].className = "nav_default_on"; // Activate it
						}
						
				    }
				}
			}
		}
	}
}

// addLoadEvent by Simon Willison
// Adds a handler to an event without over-riding other handlers

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			if (oldonload) {
				oldonload();
			}
			func();
		}
	}
}

var defaultMainListIndex = -1; // Initialize the index of the default main list item
var defaultMainListClassName = "";
addLoadEvent(initNavigation); // Add initNavigation to the page onload event handler
