/*
Scripting API for interacting with POSSE.

The API has been implemented as methods on a literal object named
PosseScriptingApi, which simulates a namespace by that name.

If you wish to use any of these API functions in an HTML web page you should
put the following line at the top of your code:

<script type="text/javascript" language="JavaScript" src="PosseScriptingApi.js"></script>

Then in your code you can call the various methods, like this:

  var dbName;
  dbName = PosseScriptingApi.GetProfile("DatabaseName", "");
  if (dbName == "") {
    dbName = AskUserForDatabase();
    PosseScriptingApi.SetProfile("DatabaseName", dbName);
    etc.
  }

The API functions appear in two sections, the Public and the Private section.
You should only call functions in the Public section.  Private functions may
change without notice.  To help you keep in mind which are which the Private
functions all begin with an underscore.
*/


var PosseScriptingApi = {

    //*** Start of Public section ********************************************

    /*------------------------------------------------------------------------
    Close the POSSE object window, if it is open.  If the object window has
    outstanding changes, the user will be prompted to save, and can cancel.
    Return true if the object window is no longer open (or was never open);
    false if the user cancelled the close or an error occurred.
    ------------------------------------------------------------------------*/
    CloseObjectWindow: function(objectId)
    {
        this._PerformAction("CloseObjectWindow", objectId);
        if (this._ReturnValue == null) {
            return false;
        } else {
            return this._ReturnValue;
        }
    },

    /*------------------------------------------------------------------------
    Close the current POSSE window (the window containing the browser control).
    ------------------------------------------------------------------------*/
    CloseWindow: function()
    {
        this._PerformAction("CloseWindow");
    },

    /*------------------------------------------------------------------------
    Return the value of the given key from the local profile.  If the key is
    not in the local profile return the default value instead.
    Note that the key is given a prefix to guarantee no name collisions.
    ------------------------------------------------------------------------*/
    GetProfile: function(key, defaultValue)
    {
        var args = "api_" + key;
        if (defaultValue != null) {
            args += "\t" + defaultValue;
        }
        this._PerformAction("GetProfile", args);
        return this._ReturnValue;
    },

    /*------------------------------------------------------------------------
    Return the title of the POSSE window that is hosting the browser control.
    ------------------------------------------------------------------------*/
    GetTitle: function()
    {
        this._PerformAction("GetTitle");
        return this._ReturnValue;
    },

    /*------------------------------------------------------------------------
    Log a message at Critical level.
    ------------------------------------------------------------------------*/
    LogCritical: function(message)
    {
        this._PerformAction("LogCritical", message);
    },

    /*------------------------------------------------------------------------
    Log a message at Debug level.
    ------------------------------------------------------------------------*/
    LogDebug: function(message)
    {
        this._PerformAction("LogDebug", message);
    },

    /*------------------------------------------------------------------------
    Log a message at Error level.
    ------------------------------------------------------------------------*/
    LogError: function(message)
    {
        this._PerformAction("LogError", message);
    },

    /*------------------------------------------------------------------------
    Log a message at Info level.
    ------------------------------------------------------------------------*/
    LogInfo: function(message)
    {
        this._PerformAction("LogInfo", message);
    },

    /*------------------------------------------------------------------------
    Always log the message, regardless of logging level.
    ------------------------------------------------------------------------*/
    LogTrace: function(message)
    {
        this._PerformAction("LogTrace", message);
    },

    /*------------------------------------------------------------------------
    Log a message at Warning level.
    ------------------------------------------------------------------------*/
    LogWarning: function(message)
    {
        this._PerformAction("LogWarning", message);
    },

    /*------------------------------------------------------------------------
    Open the POSSE object window, if a presentation exists.  If the window is
    already open, bring it to the front and give it focus.  Return true if the
    window was opened; false if no presentation exists or you do not have read
    access to the object or an error occurred.
    ------------------------------------------------------------------------*/
    OpenObjectWindow: function(objectId)
    {
        this._PerformAction("OpenObjectWindow", objectId);
        if (this._ReturnValue == null) {
            return false;
        } else {
            return this._ReturnValue;
        }
    },

    /*------------------------------------------------------------------------
    Execute a block of XML.  It must be formatted the same as in Outrider.
    Return an array containing all the new objects created, or an empty array
    if no new objects were created by the XML.  Return null if an error
    occurred.
    ------------------------------------------------------------------------*/
    ProcessXML: function(xml)
    {
        this._PerformAction("ProcessXML", xml);
        if (this._ReturnValue != null) {
            var objects = [];
            objects = eval(this._ReturnValue);
            return objects;
        }
        return null;
    },

    /*------------------------------------------------------------------------
    Refresh the POSSE object window, if it is open.  If the object window has
    outstanding changes, the user will be prompted to save, and can cancel.
    Return true if the object window was retrieved; false if it was not open
    or the user cancelled the retrieve or an error occurred.
    ------------------------------------------------------------------------*/
    RefreshObjectWindow: function(objectId)
    {
        this._PerformAction("RefreshObjectWindow", objectId);
        if (this._ReturnValue == null) {
            return false;
        } else {
            return this._ReturnValue;
        }
    },

    /*------------------------------------------------------------------------
    Set the MDI Frame�s microhelp to the given value.
    ------------------------------------------------------------------------*/
    SetMicrohelp: function(message)
    {
        window.status = message;
    },

    /*------------------------------------------------------------------------
    Set the pointer to an arrow for those parts of the POSSE window that
    aren�t the browser control.
    ------------------------------------------------------------------------*/
    SetPointerArrow: function()
    {
        this._PerformAction("SetPointerArrow");
    },

    /*------------------------------------------------------------------------
    Set the pointer to an arrow for those parts of the POSSE window that
    aren�t the browser control.
    ------------------------------------------------------------------------*/
    SetPointerHourglass: function()
    {
        this._PerformAction("SetPointerHourglass");
    },

    /*------------------------------------------------------------------------
    Set the value of the given key in the local profile.  This is used to
    store user preferences or settings for retrieval by GetProfile().
    Note that the key is given a prefix to guarantee no name collisions.
    ------------------------------------------------------------------------*/
    SetProfile: function(key, value)
    {
        this._PerformAction("SetProfile", ["api_" + key, value].join("\t"));
    },

    /*------------------------------------------------------------------------
    Set the title of the POSSE window that hosts the browser control.
    ------------------------------------------------------------------------*/
    SetTitle: function(title)
    {
        this._PerformAction("SetTitle", title);
    },

    //*** End of Public section **********************************************


    //*** Start of Private section *******************************************
    //* The following functions are all Private.  Do not call them directly
    //* since they might change at any time without notice.


    // Global variables
    _ErrorMessage: null,
    _ReturnValue: null,


    // Make a call to Powerbuilder.  This is done by serializing the desired
    // action and an optional parameter and briefly setting the status of the
    // window to that string.  This causes the string to be passed into PB,
    // where it is parsed and acted upon.
    _PerformAction: function(action, parm)
    {
        var oldText = window.status;
        var message = "scriptingapi\t" + action;

        this._SetErrorMessage(null);
        this._SetReturnValue(null);

        if (parm != undefined) {
            message += "\t" + parm;
        }
        window.status = message;
        window.status = oldText;
    },

    // Set the error message received from the front end.
    _SetErrorMessage: function(value)
    {
        this._ErrorMessage = value;
    },
    // Lowercase overload for calls from Powerbuilder.
    _seterrormessage: function(value)
    {
      this._SetErrorMessage(value);
    },

    // Set the return value of a call to the front end.
    _SetReturnValue: function(value)
    {
        this._ReturnValue = value;
    },
    // Lowercase overload for calls from Powerbuilder.
    _setreturnvalue: function(value)
    {
      this._SetReturnValue(value);
    }

    //*** End of Private section *********************************************
};

// Lowercase overload for calls from Powerbuilder.
var possescriptingapi = PosseScriptingApi;
