// Some utils
String.prototype.format = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

// Keep track of pending documents
var pendingDocs = {};

// Convert divs to queue widgets when the DOM is ready
//function PossePLUpload(id, opener, objectHandle, endPointName, uploadObjectDef, uploadObjectDefId) {
function PossePLUpload(settings) {
    settings.max_file_size = settings.max_file_size || "101mb";
    settings.runtimes = settings.runtimes || "gears,flash,silverlight,html5,html4";
    settings.filters = settings.filters || [
            { title: "All files", extensions: "*" },
            { title: "Document files", extensions: "doc,docx,log,msg,odt,pages,pdf,rtf,tex,txt,wpd,wps,xlr,xls,xlsx" },
            { title: "Image files", extensions: "bmp,dds,dng,gif,jpg,jpeg,png,psd,pspimage,tga,thm,tif,yuv" },
            { title: "Zip files", extensions: "7z,deb,gz,pkg,rar,rpm,sit,sitx,tar.gz,zip,zipx" },
            { title: "Audio files", extensions: "aif,iff,m3u,m4a,mid,mp3,mpa,ra,wav,wma" },
            { title: "Video files", extensions: "3g2,3gp,asf,asx,avi,flv,mov,mp4,mpg,rm,srt,swf,vob,wmv" }
        ];
    if (!settings.posse_custom_details)
        settings.posse_custom_details = {}

    $(settings.id).pluploadQueue({
        // General settings
        runtimes: settings.runtimes,
        url: 'PossePLUpload.aspx?PosseObjectDefId=' + settings.uploadObjectDefId.toString(),
        max_file_size: settings.max_file_size,
        //chunk_size : '1mb',
        //unique_names : true,

        // Resize images on clientside if we can
        // resize: { width: 320, height: 240, quality: 90 },

        // Specify what files to browse for
        filters: settings.filters,

        // Flash settings
        flash_swf_url: 'plupload/js/plupload.flash.swf',
        urlstream_upload: true,

        // Silverlight settings
        silverlight_xap_url: 'plupload/js/plupload.silverlight.xap',

        // Posse specific extensions
        posse_include_description: settings.posse_include_description,
        posse_include_classification: settings.posse_include_classification
    });

    var uploader = $(settings.id).pluploadQueue();

    // Get the response and add the pending doc id to a list of doc id's
    uploader.bind('FileUploaded', function (up, file, response) {
        if (!response.status || response.status == 200) {
            if (response.response) {
                pendingDoc = $.parseJSON(response.response);
                pendingDoc.id = file.id;
                if (settings.posse_include_description) {
                    pendingDoc.description = uploader.getPosseDescription(file.id);
                }
                else
                    pendingDoc.description = "";
                if (settings.posse_include_classification) {
                    pendingDoc.classification = uploader.getPosseClassification(file.id);
                }
//                else 
//                { pendingDoc.classification = settings.endPointName; }
                pendingDocs[file.id] = pendingDoc;
            }
        }
    });

    uploader.bind('UploadComplete', function (up, files) {
        // Set up custom document columns
        customDetailXml = "";
        for (key in settings.posse_custom_details) {
            customDetailXml += '<column name="{0}"><![CDATA[{1}]]></column>'.format(key, settings.posse_custom_details[key]);
        }

        // Build XML for XML Changes
        var xml = "";
        for (key in pendingDocs) {
            xml = '<object id="{0}" objectdef="{1}" action="Insert">' +
                '<column name="Description"><![CDATA[{2}]]></column>' +
                '<column name="FileName"><![CDATA[{3}]]></column>' +
                '<column name="FileSize">{4}</column>' +
                '<column name="FileExtension"><![CDATA[{5}]]></column>' +
                customDetailXml +
                '<pendingdocument id="{6}"/>' +
                '</object>' +
                '<object id="{7}"><relationship endpoint="{9}" toobjectid="{0}" action="Insert"></relationship></object>' +
                '<object id="{0}"><relationship endpoint="DocumentType" toobjectid="{8}" action="Insert"></relationship></object>';
                 xml = xml.format(pendingDocs[key].newobjectid, settings.uploadObjectDef, pendingDocs[key].description,
                pendingDocs[key].filename, pendingDocs[key].filesize, pendingDocs[key].extension,
                pendingDocs[key].pendingdocid, settings.objectHandle, pendingDocs[key].classification, settings.endPointName);
            if (settings.opener) {
                settings.opener.PosseAppendChangesXML(xml);
            } else {
                PosseAppendChangesXML(xml);
            }
        }

        // Clear process outcome if necessary
        if (settings.posse_clear_outcome) {
            if (settings.opener)
                settings.opener.PosseChangeColumn(settings.objectHandle, "Outcome", "");
            else
                PosseChangeColumn(settings.objectHandle, "Outcome", "");
        }

        // Navigate / Submit on the parent
        if (settings.opener) {
            if (settings.posse_auto_save)
                settings.opener.PosseSubmit();
            else
                settings.opener.PosseNavigate();
            window.close();
        } else {
            if (settings.posse_auto_save)
                PosseSubmit();
            else
                PosseNavigate();
        }
    });
}
