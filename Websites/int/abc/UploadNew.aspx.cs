using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class UploadNew : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string posseAutoSave = "false";
            string clearOutcome = "false";
            if ((this.Request.QueryString["PosseAutoSave"] ?? "").ToLower() == "y")
            {
                posseAutoSave = "true";
                if ((this.Request.QueryString["PosseIsProcessType"] ?? "").ToLower() == "y")
                    clearOutcome = "true";
            }

            string args = String.Format("Argument={0}", "0");
            List<object> results = (List<object>)ExecuteSql("GetDocumentTypeJSON", args);
            string DocumentTypes = (string)((Dictionary<string, object>)(results[0]))["DOCTYPEJSON"];
			
//            List<Dictionary<string, string>> results = (List<Dictionary<string, string>>)ExecuteSql("GetDocumentTypeJSON", args);
//            string DocumentTypes = results[0]["DOCTYPEJSON"];
            string posseIncludeDocTypes = "";
            if (!String.IsNullOrEmpty(DocumentTypes))
            {
                posseIncludeDocTypes = String.Format("posse_include_classification: {0}", DocumentTypes);
            }
            Literal script = new Literal();
            string val = @"<script>
                $(document).ready(function () {{
                    PossePLUpload({{
                        id: ""#uploader"",
                        opener: opener,
                        objectHandle: ""{0}"",
                        endPointName: ""{1}"",
                        uploadObjectDef: ""{2}"",
                        uploadObjectDefId: ""{3}"",
                        filters: [
                            {{ title: ""All files"", extensions: ""*"" }},
                            {{ title: ""Document files"", extensions: ""doc,docx,log,msg,odt,pages,pdf,rtf,tex,txt,wpd,wps,xlr,xls,xlsx"" }},
                            {{ title: ""Image files"", extensions: ""bmp,dds,dng,gif,jpg,jpeg,png,psd,pspimage,tga,thm,tif,yuv"" }},
                            {{ title: ""Zip files"", extensions: ""7z,deb,gz,pkg,rar,rpm,sit,sitx,tar.gz,zip,zipx"" }},
                            {{ title: ""Audio files"", extensions: ""aif,iff,m3u,m4a,mid,mp3,mpa,ra,wav,wma"" }},
                            {{ title: ""Video files"", extensions: ""3g2,3gp,asf,asx,avi,flv,mov,mp4,mpg,rm,srt,swf,vob,wmv"" }}
                        ],
                        max_file_size: ""101mb"",
                        posse_include_description: true,
                        //posse_custom_details: {{DocDetailName: ""Value"", DocDetailName2: ""Value""}},
                        {6},
                        posse_auto_save: {4},
                        posse_clear_outcome: {5}
                    }})
                }});
                </script>";
            script.Text = string.Format(val,
                this.Request.QueryString["PosseObjectId"].ToString(), this.Request.QueryString["UploadEndPoint"].ToString(),
                this.Request.QueryString["UploadObjectDef"].ToString(), this.Request.QueryString["PosseObjectDefId"].ToString(),
                posseAutoSave, clearOutcome, posseIncludeDocTypes);
				
            this.scriptPlaceholder.Controls.Add(script);
        }

        #endregion
    }
}
