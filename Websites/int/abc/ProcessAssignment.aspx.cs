using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class ProcessAssignment : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			string title, saveButton, check, disabled, selectAllButton, clearAllButton;
			int count, index;
			StringBuilder html = new StringBuilder();
			Literal literal = new Literal();
            List<PossibleAssignment> candidates;

			// Get the assignment candidates.
			candidates = this.GetPossibleAssignments(this.ObjectId);
			count = candidates.Count;

			// Format the output
            title = "<div class=\"title\">Re-Assign</div><br>";
            saveButton = "<div class=\"button\" title=\"Save\"><a onclick=\"AssignAndClose()\"><span>Save and Close</span></a></div>";
            selectAllButton = "<div class=\"button\" title=\"Select All\"><a onclick=\"SetCheckboxes(true)\"><span>Select All</span></a></div>";
            clearAllButton = "<div class=\"button\" title=\"Clear All\"><a onclick=\"SetCheckboxes(false)\"><span>Clear All</span></a></div>";
			html.Append("<script language=\"JavaScript\">\r\nfunction AssignAndClose() {");
			html.AppendFormat("  xml = '<object id=\"{0}\"><assignments>';", this.ObjectId);
			html.AppendFormat("  for(i=1; i <= {0}; i++) {{", count);
            html.Append("    e = document.getElementById(\"assignments_\" + i);");
			html.Append("    if (e.checked) {");
			html.Append("      xml += '<user id=\"' + e.value + '\"/>';");
			html.Append("    }");
			html.Append("  }");
			html.Append("  xml += '</assignments></object>';");
			html.Append("  opener.PosseAppendChangesXML(xml);");
			html.Append("  opener.PosseSubmit();");
			html.Append("  window.close();");
			html.Append("}");
            html.Append("function SetCheckboxes(checked) {");
            html.Append(String.Format("  for(i=1; i <= {0}; i++) {{", count));
            html.Append("    e = document.getElementById(\"assignments_\" + i);");
            html.Append("    e.checked = checked;");
            html.Append("  }");
            html.Append("}");
            html.Append("</script>");

            html.Append(title);
            html.Append(saveButton);
            html.Append("<br>");
            html.Append("<table border=0 cellpadding=0 cellspacing=0><tr><td>");
            html.Append(selectAllButton);
            html.Append("</td><td>");
            html.Append(clearAllButton);
            html.Append("</td</tr></table>");
			html.Append("<table border=0>" +
				"<tr class=\"possegrid\">" +
				"<th class=\"possegrid\">Assigned</th>" +
				"<th class=\"possegrid\">Name</th>" +
				"</tr>");

			index = 1;
            foreach (PossibleAssignment candidate in candidates)
            {
				html.AppendFormat("<tbody class=\"posseband_{0}\"><tr class=\"possegrid\"><td class=\"possegrid\">", (index % 2) + 1);
				check = "";
				disabled = "";

                if (candidate.CurrentlyAssigned)
                {
					check = "checked";
				}

                if (!candidate.CanUnassign)
                {
					disabled = "disabled";
				}

				html.AppendFormat("<input type=\"checkbox\" id=\"assignments_{0}\" value=\"{1}\" {2} {3}></td>", index, candidate.ObjectId, disabled, check);
				html.AppendFormat("<td class=\"possegrid\">{0}</td></tr></tbody>", candidate.Name);

				index += 1;
			}

			html.Append("</table>" + saveButton);
			literal.Text = html.ToString();
			this.pnlPaneBand.Controls.Add(literal);
		}
		#endregion
	}
}