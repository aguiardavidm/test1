﻿<!DOCTYPE html>
<%@ Page Language="C#" AutoEventWireup="true"
	CodeFile="Dashboard.aspx.cs" Inherits="Computronix.POSSE.Outrider.Dashboard" Title="POSSE Dashboard" %>
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>POSSE Dashboard</title>
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />-->
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
     
    <link href="css/global.css" rel="stylesheet" type="text/css" />
    <link href="css/layout.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/buttons.css" rel="stylesheet" type="text/css" />
	<link href="css/tabs.css" rel="stylesheet" type="text/css" />
	<link href="css/dashboard.css" rel="stylesheet" type="text/css" />
	<!--[if IE 7]>
	      <link href="css/ie7.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="extjs/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css" href="extjs/ux/css/CheckHeader.css" />
    <script type="text/javascript" src="extjs/ext-all.js">
	<link href="css/insertmenu.css" rel="stylesheet" type="text/css" />    
    <link rel="shortcut icon" href="<%=ResolveUrl("~/favicon.ico")%>" type="image/x-icon" />
	<script src="javascript/posseglobal<%=ConfigurationManager.AppSettings["PosseGlobalVersion"]%>.js"
		language="javascript" type="text/javascript"></script>

	<script src="javascript/posseextension<%=ConfigurationManager.AppSettings["PosseExtensionVersion"]%>.js"
		language="javascript" type="text/javascript"></script>

	<script src="javascript/jquery-ui-1.8.5.custom\js\jquery-1.4.2.min.js" language="javascript" type="text/javascript"></script>
   
    <script type="text/javascript" src="Scripts/WidgetBase.js"></script>
    <script type="text/javascript" src="Scripts/CustomCode.js"></script>
    <script type="text/javascript" src="Scripts/ObjectIntakeRate.js"></script>
    <script type="text/javascript" src="Scripts/ToDoListSummary.js"></script>
	<script type="text/javascript" src="Scripts/WarningGauge.js"></script>
	<script type="text/javascript" src="Scripts/ObjectSummary.js"></script>
    <script type="text/javascript" src="Scripts/Dashboard.js"></script>
	<script type="text/javascript" src="Scripts/DaysToCompleteByStaff.js"></script>
    <script type="text/javascript" src="Scripts/DaysToCompleteSummary.js"></script>
	<script type="text/javascript" language="JavaScript" src="javascript/PosseScriptingApi.js"></script>

</head>
<body>
 <div class="header" style="text-align: center;">
        <div class="centerpanel">
            <div class="sitelogo">
                <a href="Default.aspx" title="">
                    <img src="images/headerlogo.png"/>
                </a>
                </div>
		    <div class="welcomemessage">
	            <asp:Panel ID="pnlUserInfo" runat="server" CssClass="userInfoPanel">
	            </asp:Panel>
            </div>
            <div class="metanav">
                <a href="">Using this Site</a><span>|</span><a href="">Contact Us</a>
            </div>
	        <div class="menuband">
	            <asp:Panel ID="pnlMenuBand" runat="server">
	            </asp:Panel>
	        </div>
        </div>
    </div>
    <div class="contentbody">
        <div style="overflow: hidden; position:relative; width:160px; float:left;">
            <div id="categorysidebar" class="categorysidebar">
                   <script>
                       $(function () {
                           $('#categorysidebar ul li').live('click', function (e) {
                               e.preventDefault();
                               var $this = $(this);
                               $this.closest('ul').children('li').removeClass('active');
                               $this.addClass('active');
                           });
                       });             
                     </script>     
            </div>
        
            <div id="settingsButton" class="dashboardbutton" style="clear:both;" onclick="javascript: Computronix.POSSE.Dashboard.displaySettingsWindow();">Settings</div>
            <div id="settingsButtonHidden"></div>
        </div>     
    <div id="dashboard" style="overflow: hidden; position:relative; float: none;"></div>
    </div>
 </form>
</body>
</html>