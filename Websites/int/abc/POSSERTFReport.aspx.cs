using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Computronix.CXTools.SuperClass;
using Computronix.RTFReporter;

namespace Computronix.POSSE.Outrider
{
    public partial class POSSERTFReport : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            byte[] pdf;
            String reportName;
            int objectId, docDefId;

            // Create context manager and push parms etc. into it
            ContextManager contextManager = new ContextManager();
            Sys.ParseContext(this.Session, this.Request, contextManager);

            // Read parms from request
            reportName = Request.Params.Get("ReportName");
            if (Sys.IsBlank(reportName))
            {
                throw new Exception("ReportName parameter missing");
            }
            string tempDocDefId = Request.Params.Get("DocumentDefId");
            if (!int.TryParse(tempDocDefId, out docDefId))
            {
                docDefId = -1;
            }

            String status = "FAILED";
            try
            {
                Sys.TraceManager.WriteLine(3, "POSSERTFReport", String.Format("POSSERTFReport - Generating report for {0}...", reportName));
                Sys.TraceManager.Indent();

                // Build the report
                using (RTFReporter.ReportBuilder rtfReporter = new RTFReporter.ReportBuilder(reportName, contextManager, Sys.InvalidTokenHandling))
                {
                    rtfReporter.LoadSQL();
                    rtfReporter.GenerateRTF();

                    // Turn the RTF document into a PDF
                    Sys.TraceManager.WriteLine(4, "POSSERTFReport", "Converting to PDF...");
                    rtfReporter.ConvertRTFToPDF();

                    // Check if we're supposed to save the document in POSSE
                    if (docDefId > -1)
                    {
                        if (rtfReporter.DocBindings.HasDocumentInfo)
                        {
                            // Save the document using outrider
                            Sys.TraceManager.WriteLine(4, "POSSERTFReport", "Saving into POSSE document...");
                            string newObjectId = "NEW" + DateTime.Now.Millisecond.ToString();
                            int pendingDoc = SetDocument(docDefId, Path.GetFileNameWithoutExtension(this.LastUploadFileName), "pdf", rtfReporter.GetPDFAsBytes());

                            // Build the xml for the bindings
                            StringBuilder sourceSQL = new StringBuilder();
                            DataManager dmBinding = new DataManager();
                            StringBuilder bindings = new StringBuilder();

                            // Check if there are any bindings defined
                            if (rtfReporter.DocBindings.Length > 0)
                            {
                                if (!rtfReporter.DocBindings.ValuesResolved)
                                {
                                    rtfReporter.DocBindings.ResolveValues(rtfReporter.MainObjectId, dmBinding);
                                }

                                foreach (DocBinding binding in rtfReporter.DocBindings)
                                {
                                    bindings.AppendFormat("<column name=\"{0}\"><![CDATA[{1}]]></column>", binding.Target, binding.Value);
                                }
                            }

                            if (rtfReporter.DocBindings.DocumentTypeName == "")
                            {
                                throw new Exception("[Document Info] section must contain:\n  DocumentTypeName=DocumentTypeName");
                            }

                            // Now build the XML
                            String xml = string.Format("<object id=\"{0}\" objectdef=\"{1}\" action=\"Insert\">" +
                                          "{2}" +
                                          "<pendingdocument id=\"{3}\"/></object>" +
                                          "<object id=\"{4}\"><relationship endpoint=\"{5}\" toobjectid=\"{0}\" action=\"Insert\">" +
                                          "</relationship></object>",
                                          newObjectId,  rtfReporter.DocBindings.DocumentTypeName,
                                          bindings,
                                          pendingDoc,
                                          rtfReporter.MainObjectId, rtfReporter.DocBindings.DocEndPointName);

                            // Finalize the document
                            ProcessXML(xml);
                            dmBinding.Dispose();

                        }
                        else
                        {
                            throw new Exception("SaveReport was specified but no [Document Info] section was provided.");
                        }
                    }

                    // Return the generated PDF
                    Sys.TraceManager.WriteLine(4, "POSSERTFReport", "Streaming Data...");
                    long length = rtfReporter.PDFFile.Length;
                    if (length > 0)
                    {
                        //this.CheckClient();
                        this.Response.Clear();
                        this.Response.BufferOutput = false;

                        rtfReporter.PDFFile.Position = 0;
                        using (BinaryReader reader = new BinaryReader(rtfReporter.PDFFile))
                        {
                            this.Response.AddHeader("Content-Disposition", String.Concat("Attachment; filename=", reportName, ".pdf"));
                            this.Response.AddHeader("Content-Length", length.ToString());
							//Commented out, was causing "Server cannot set content type after HTTP headers have been sent." error
							//Issue 5758 - based on Issue 15189 for NLC
                            
                            //this.Response.ContentType = "application/pdf";

                            //Write the output in 4k chunks
                            const int CHUNKSIZE = 4096;
                            int chunk = CHUNKSIZE;
                            byte[] temp = new byte[CHUNKSIZE];
                            int pos = 0;
                            while (chunk > 0)
                            {
                                int bytesRead = reader.Read(temp, 0, chunk);
                                if (bytesRead == 0)
                                {
                                    throw new Exception(String.Format("Expected to read {0:n} bytes from PDF file, but read 0.", chunk));
                                }
                                this.Response.OutputStream.Write(temp, 0, bytesRead);
                                this.Response.Flush();
                                pos += bytesRead;
                                chunk = pos + CHUNKSIZE < length ? CHUNKSIZE : (int)length - pos;
                            }
                            status = "SUCCESS";
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                // Just ignore this.  It's a result of the Response.End()
                status = "SUCCESS";
            }
            finally
            {
                Sys.TraceManager.Unindent();
                Sys.TraceManager.WriteLine(3, "POSSERTFReport", String.Format("POSSERTFReport - Generating report for {0}... {1}", reportName, status));
            }

        }
        #endregion
    }
}
