<%@ Page Language="C#" MasterPageFile="~/OutriderPopup.master" AutoEventWireup="true"
	CodeFile="Upload.aspx.cs" Inherits="Computronix.POSSE.Outrider.Upload" Title="Upload" %>
<asp:Content ID="cntTitleBand" runat="server" ContentPlaceHolderID="cphTitleBand">
	<asp:Panel ID="pnlTitleBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntPaneBand" ContentPlaceHolderID="cphPaneBand" runat="Server">
	<table>
		<tr>
			<td colspan="3">
				<asp:Panel ID="pnlPaneBand" runat="server">
				</asp:Panel>
			</td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="lblFileName" runat="server" CssClass="posselabel possedetail" Text="File Name:"
					Width="70px"></asp:Label></td>
			<td>
				<asp:FileUpload ID="fullFileName" runat="server" CssClass="uploadfield" Width="406px" /></td>
			<td>
				<asp:RequiredFieldValidator ID="rfvFileName" runat="server" ControlToValidate="fullFileName"
					ErrorMessage="File Name is required." CssClass="posseerror">*</asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td colspan="3" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
				padding-top: 0px;">
				<asp:Panel ID="pnlData" runat="server">
					<table>
						<tr>
							<td valign="top">
								<asp:Label ID="lblDescription" runat="server" CssClass="uploadlabel" Text="Description:"
									Width="70px"></asp:Label></td>
							<td>
								<asp:TextBox ID="txtDescription" runat="server" CssClass="uploadfield" MaxLength="4000"
									TextMode="MultiLine" Rows="4" Width="400px"></asp:TextBox></td>
							<td></td>
						</tr>
					</table>
				</asp:Panel>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="left">
	                <div class="button">
                        <asp:LinkButton ID="Button1" runat="server" OnClick="btnUpload_Click">
                            <span id="uploadbtnlbl">Upload</span>
                        </asp:LinkButton>
                        </a>
                    </div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<asp:ValidationSummary ID="vsmMain" runat="server" CssClass="posseerror" DisplayMode="List"
					ForeColor="" />
			</td>
		</tr>
	</table>
</asp:Content>
