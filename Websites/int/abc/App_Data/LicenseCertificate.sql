[Settings]
ApplyWatermark=N
mainobjectid={Main:ProcessId}
ApplyImageWatermark=Y
WatermarkImage=watermark.gif
WatermarkImageTransparency=80
watermarkImageHeight=580
watermarkImageWidth=580
WatermarkEscapement=0
PDFA=N

[Main]
select /*+ optimizer_features_enable('11.2.0.4') */ :PosseObjectId ProcessId,
       l.Objectid Licid,
       UPPER(l.LicenseType) Type,
       (case when l.Establishment is not null then l.Establishment else l.EventLocationAddress end) Establishment,
       UPPER(l.Licensee) Licensee,
       l.LicenseNumber LicenseNumber,
       to_char(l.ExpirationDate, 'FXMM/DD/YYYY') ExpirationDate,
       to_char(l.EffectiveDate, 'FXMM/DD/YYYY') EffectiveDate,
       UPPER(l.CertificateAddressDisplay) PhysicalAddress,
       l.Operator Operator,
	   api.pkg_columnquery.Value(:PosseObjectId, 'DraftWatermark') Draft,
       (case when l.Establishment is not null then 'Establishment:' else 'Event Location:' end) EstablishmentLabel,
       (case when l.Operator is not null then 'Operator:' else null end) OperatorLabel,
       (case when l.CertificateAddressDisplay is not null then 'Physical Address:' else null end) PhysicalAddressLabel,
       l.EventType,
       (case when l.EventType is not null then 'Event Type:' else null end) EventTypeLabel,
     (case l.IssuingAuthority when 'State' then 'State Issued'
                when 'Municipality' then UPPER(l.Office) end) IssuingAuthority,
     (case l.IssuingAuthority when 'State' then 'Attest: ' || (select api.pkg_columnquery.value(s.objectid,'DirectorTitle') as DirectorTitle
                                                               from   api.objects s
                                                               where  s.objectdefid = api.pkg_configquery.objectdefidforname('o_SystemSettings')
                                                               and rownum < 2) || ' of ABC'
                when 'Municipality' then 'Attest: ' || UPPER(l.Office) end) Attest,           
     (case l.IssuingAuthority when 'Municipality' then UPPER(l.Region) else null end) Region,
     l.LicenseYear,
     nvl(l.AssociatedApplicationFees, '0.00') AssociatedApplicationFees,
     (case when instr(l.AssociatedApplicationFees, '.') = 0 then '.00' else null end) Cents,
     l.LicenseCertificateText,
     l.ConditionsExistOnLicense,
   (case when l.FarmWineryWholesalePrivilege = 'Y' or l.OutOfStateWineryWholesalePrivi = 'Y' or l.PlenaryWineryWholesalePrivileg = 'Y' then 'WHOLESALE PRIVILEGE APPROVED' else null end) WholesalePrivilege,
   (case l.IssuingAuthority when 'State' then 'Y' when 'Municipality' then 'N' end) AS StateVisible,
   (case when l.IssuingAuthority = 'Municipality' then '___________________________________' end) SignatureLine,
   (case when l.LicenseTypeCode = '13' and l.RetailTransitType = 'Boat' then 'Y' else 'N' end) ShowVessels,
   (case when (l.ConditionsExistOnLicense = 'Y') or (l.LicenseTypeCode = '13' and l.RetailTransitType = 'Boat') then 'Y' else 'N' end) ShowHeader
  from query.o_ABC_License l
 where l.objectdeftypeid = 1
   and l.ObjectId = api.pkg_columnquery.NumericValue(:PosseObjectId, 'LicenseObjectId')

[Conditions]
select /*+ optimizer_features_enable('11.2.0.4') */ c.ObjectId, c.Code Code, 
       c.ConditionText || chr(13) || chr(10) ConditionText
  from query.o_abc_condition c
  join api.relationships r
    on c.Objectid = r.ToObjectId
   where r.FromObjectId = :[Main]LicID
   order by c.SortOrder

[SecondaryLic]
select /*+ optimizer_features_enable('11.2.0.4') */ l.ObjectId,
    l.LicenseType SecondaryType,
    l.LicenseNumber SecondaryNumber,
    to_char(l.ExpirationDate, 'FMMonth DD, YYYY') ExpirationDate,
    to_char(l.EffectiveDate, 'FMMonth DD, YYYY') EffectiveDate
  from query.o_abc_license l
  join query.r_ABC_PrimarySecondaryLicense r
    on r.SecondaryLicenseObjectId = l.ObjectId
 where r.PrimaryLicenseObjectId = :[Main]LicID

[Signature]
select /*+ optimizer_features_enable('11.2.0.4') */ dr.Value,
       dr.Extension
from api.documents d
join api.documentrevisions dr
     on dr.DocumentId = d.DocumentId
     and dr.RevisionNum = d.TailRevisionNum
where  d.DocumentId = (select max(s.DirectorSignatureObjectId)
from query.o_systemsettings s)
and rownum < 2

[Vessels]
select /*+ optimizer_features_enable('11.2.0.4') */ api.pkg_columnquery.Value(lv.VehicleId,'VesselName') VesselName
 from query.r_abc_licensevehicle lv
where lv.LicenseId = :[Main]LicID
order by api.pkg_columnquery.Value(lv.VehicleId,'VesselName')

[Document Info]
EndpointName=LicCertificate
DocumentTypeName=d_PosseReportLetter

[Document Bindings]
Description=License Certificate
JobId=<JobId>
ProcessId=<ObjectId>