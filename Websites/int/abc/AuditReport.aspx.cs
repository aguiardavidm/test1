using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class AuditReport : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CheckClient();

            string reportType = this.Request.QueryString["PosseReportType"];
            switch (reportType.ToLower())
            {
                case "event":
                    EventAuditReport();
                    break;
                case "forensic":
                    ForensicAuditReport();
                    break;
                default:
                    throw new Exception(string.Format("Unknown PosseReportType " +
                            "in QueryString: {0}", reportType));
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Produce an Event audit report using the given arguments.
        /// </summary>
        protected void EventAuditReport()
        {
            string arg;
            int objectId;
            int? reportLevels = null;
            string reportSubtitle;
            string startDateTime;
            string endDateTime;
            byte[] report = null;

            objectId = int.Parse(this.Request.QueryString["PosseObjectId"]);
            arg = this.Request.QueryString["PosseReportLevels"];
            if (arg != null)
            {
                reportLevels = int.Parse(arg);
            }
            reportSubtitle = this.Request.QueryString["PosseReportSubtitle"];
            startDateTime = this.Request.QueryString["PosseStartDateTime"];
            if (startDateTime == null || startDateTime == "")
            {
                throw new Exception("Empty PosseStartDateTime in QueryString");
            }
            endDateTime = this.Request.QueryString["PosseEndDateTime"];
            if (endDateTime == null || endDateTime == "")
            {
                throw new Exception("Empty PosseEndDateTime in QueryString");
            }

            report = this.OutriderNet.CreateEventAuditReport(objectId, reportLevels,
                    reportSubtitle, startDateTime, endDateTime);

            this.Response.Clear();
            this.Response.AddHeader("Content-Disposition", "filename=report.pdf");
            this.Response.AddHeader("Content-Length", report.Length.ToString());
            this.Response.ContentType = "application/pdf";
            if (report.Length > 0)
            {
                this.Response.BinaryWrite(report);
            }
            this.Response.End();
        }

        /// <summary>
        /// Produce a Forensic audit report using the given arguments.
        /// </summary>
        protected void ForensicAuditReport()
        {
            string arg;
            int objectId;
            int? reportLevels = null;
            byte[] report = null;

            objectId = int.Parse(this.Request.QueryString["PosseObjectId"]);
            arg = this.Request.QueryString["PosseReportLevels"];
            if (arg != null)
            {
                reportLevels = int.Parse(arg);
            }
            report = this.OutriderNet.CreateForensicAuditReport(objectId, reportLevels);

            this.Response.Clear();
            this.Response.AddHeader("Content-Disposition", "filename=report.pdf");
            this.Response.AddHeader("Content-Length", report.Length.ToString());
            this.Response.ContentType = "application/pdf";
            if (report.Length > 0)
            {
                this.Response.BinaryWrite(report);
            }
            this.Response.End();
        }
        #endregion
    }
}
