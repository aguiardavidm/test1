using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Xml;
using System.Net;
using Newtonsoft.Json;


namespace Computronix.POSSE.Outrider
{
    public partial class _Default : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Ensure the outer ASPX form is never submitted.  Works around browser behaviour that submits single field forms when [Enter] is pressed.
            this.Form.Attributes["onsubmit"] = "return false";

            this.LoadPresentation();
            this.RenderUserInfo(this.pnlUserInfo);
            this.RenderMenuBand(this.pnlMenuBand);
            this.RenderErrorMessage(this.pnlPaneBand);

            //Start logging to investigate the Menu band randomly disappering.
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            using (HtmlTextWriter hw = new HtmlTextWriter(sw))
            {
                this.pnlMenuBand.RenderControl(hw);

                // Count the number of "li" elements in the MenuBand to check if we're missing some
                int lastIndex = 0;
                int liCount = 0;
                string findStr = "<li>";

                while(lastIndex != -1){

                    lastIndex = sb.ToString().IndexOf(findStr, lastIndex);

                    if(lastIndex != -1){
                        liCount++;
                        lastIndex += findStr.Length;
                    }
                }

                if (liCount < 2)
                {
                    this.OutriderNet.LogMessage(LogLevel.Error, "****************************************************************");
                    this.OutriderNet.LogMessage(LogLevel.Error, "Recording Information for Error in Menu Band");
                    this.OutriderNet.LogMessage(LogLevel.Error, "URL: " + HttpContext.Current.Request.Url.ToString());
                    this.OutriderNet.LogMessage(LogLevel.Error, "Navigation Menu Html Rendering:");
                    this.OutriderNet.LogMessage(LogLevel.Error, sb.ToString() + "MenuHtml");
                    this.pnlUserInfo.RenderControl(hw);
                    this.OutriderNet.LogMessage(LogLevel.Error, "User Information Html Rendering:");
                    this.OutriderNet.LogMessage(LogLevel.Error, sb.ToString());
                    this.OutriderNet.LogMessage(LogLevel.Error, "Session Id: " + this.SessionId);
                    HttpBrowserCapabilities bc = Request.Browser;
                    this.OutriderNet.LogMessage(LogLevel.Error, "Browser Info: " + bc.Type + ", version: " + bc.Version);
                }
            }
            //End Logging for the menu disappering.


            if (this.HasPresentation)
            {
                if (this.IsGuest && !this.GetCondition(this.CurrentPaneId, "ShowGuest"))
                {
                    //redirect to login
                    this.ShowLogin("");
                }

                this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
                this.RenderHelpBand(this.pnlHelpBand);
                this.RenderHeaderPane(this.pnlHeaderBand, this.pnlTopBand);
                if (this.EmbedCriteriaOnSearches)
                {
                    this.RenderSearchCriteriaPane(this.pnlSearchCriteriaBand);
                }
                else
                {
                    this.pnlSearchCriteriaBand.Visible = false;
                    this.RenderTabLabelBand(this.pnlTabLabelBand);
                }

                if (this.PresentationName != "ToDoList") //Don't render the pane if this is a TO DO List
                {
                    this.RenderTopFunctionBand(this.pnlTopFunctionBand);
                    this.CheckClient();
                    this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
                }
                this.RenderBottomFunctionBand(this.pnlBottomFunctionBand);

                // For reports we need to fix the height of the pane so the embedded PDF
                // fills the page.  Add bottom padding for Firefox.  This assumes that a report
                // pane is named "ReportPane...".
                if (this.PaneName.StartsWith("ReportPane"))
                {
                    this.pnlPaneBand.Style["height"] = "600px";
                    this.pnlPaneBand.Style["padding-bottom"] = "5px";
                }
            }
            else
            {
                // Redirect Guest to login
                if (this.IsGuest)
                {
                    //redirect to login
                    this.ShowLogin("");
                }

                if (!string.IsNullOrEmpty(this.MenuName))
                {
                    this.RenderMenuPane(this.pnlPaneBand, this.pnlTitleBand, this.pnlTopBand);
                }
                else
                {
                    string startMenuName = ConfigurationManager.AppSettings["StartMenuName"];
                    if (startMenuName != null)
                    {
                        Response.Redirect(String.Format("Default.aspx?PosseMenuName={0}", startMenuName));
                    }
                    else
                    {
                        this.RenderGreeting(this.pnlPaneBand);
                    }
                }
            }

            if (this.ShowDebugSwitch)
            {
                this.RenderDebugLink(this.pnlDebugLinkBand);
            }
            this.RenderFooterBand(this.pnlFooterBand);
        }
        #endregion

        // ProcessExtJsToDoListData
        private List<string> ProcessExtJsToDoListData()
        {
            List<OrderedDictionary> data = GetExtJsToDoListData();
            List<OrderedDictionary> noteData = this.ToDoNoteInformation;

            // Get the processes for our user that should show the note history button
            string args = String.Format("{{\"userid\":{0}}}", this.UserInformation[0]["ObjectId"]);
            String showHistoryJson = this.ServiceApi.ExecuteStatement(this.SessionId, "ABCGetHistoryVisibleProcesses", args);
            var showHistoryData = JsonConvert.DeserializeObject<List<OrderedDictionary>>(showHistoryJson);

            List<string> dataRows = new List<string>();
            string objectHandle = string.Empty;
            foreach (OrderedDictionary dic in data)
            {
                string dataRow = "[";
                foreach (DictionaryEntry entry in dic)
                {
                    bool isNotGapRow = false;
                    int proccesId = 1;
                    if (entry.Value != null)
                    {
                        dataRow = String.Format("{0}'{1}',", dataRow, entry.Value.ToString().Replace("\\", " ").Replace("'", @"\'").Replace("\r", " ").Replace("\n", " "));
                    }
                    else
                        dataRow = String.Format("{0}'{1}',", dataRow, entry.Value);
                    if (entry.Key.ToString().ToLower() == "objectid")
                    {

                        foreach (OrderedDictionary dic2 in noteData)
                        {
                          if ((int)dic2["ProcessId"] == (int)entry.Value && proccesId != (int)entry.Value)
                          {
                             isNotGapRow = true;
                             proccesId = (int)dic2["ProcessId"];
                             //Doing try/catch because outrider record set calls seem to die no matter what you do if the detail is null
                             try
                            {
                             dataRow = String.Format("{0}'{1}',", dataRow, dic2["NoteText"].ToString().Replace("\\", " ").Replace("'", @"\'").Replace("\r", " ").Replace("\n", " "));
                            }
                            catch
                            {
                                dataRow = String.Format("{0}'{1}',", dataRow, "");
                            }
                             dataRow = String.Format("{0}'{1}',", dataRow, dic2["ObjectId"].ToString().Replace("\\", " ").Replace("'", @"\'").Replace("\r", " ").Replace("\n", " "));
                          }
                        }
                      if (!isNotGapRow && proccesId != (int)entry.Value)
                       {
                         dataRow = String.Format("{0}'{1}',", dataRow, null);
                         dataRow = String.Format("{0}'{1}',", dataRow, null);
                       }

                    }

                }

                // Update the datarows with a flag indicating if the history button should appear
                bool isNotGapRow2 = false;
                int processId = 0;
                foreach (OrderedDictionary dic3 in showHistoryData)
                    {
                    foreach (DictionaryEntry processEntry in dic3)
                        {
                            if (Convert.ToInt32(processEntry.Value) == Convert.ToInt32(dic["ObjectId"]))
                                {
                                    isNotGapRow2 = true;
                                    processId = Convert.ToInt32(processEntry.Value);
                                    dataRow = String.Format("{0}'{1}',", dataRow, "Y".ToString().Replace("\\", " ").Replace("'", @"\'").Replace("\r", " ").Replace("\n", " "));
                                    break;
                                }
                            
                        }
                    }
                if (!isNotGapRow2 && processId != (int)dic["ObjectId"])
                   {
                        dataRow = String.Format("{0}'{1}',", dataRow, null);
                }

                //Get additional data from a recordset
                if (!String.IsNullOrEmpty(objectHandle))
                {
                    List<OrderedDictionary> processData = GetExtJsToDoListItemData(Convert.ToInt32(objectHandle));
                    foreach (OrderedDictionary dataItem in processData)
                    {
                        foreach (DictionaryEntry entry in dataItem)
                        {
                            if (entry.Value != null)
                            {
                                dataRow = String.Format("{0}'{1}',", dataRow, entry.Value.ToString().Replace("\\", " ").Replace("'", @"\'").Replace("\r", " ").Replace("\n", " "));
                            }
                            else
                                dataRow = String.Format("{0}'{1}',", dataRow, entry.Value);
                        }
                    }
                }
                //Check last item in row before capping off with a square bracket.
                if (dataRow.Length > 1)
                    dataRow = dataRow.Substring(0, dataRow.Length - 1) + "]";
                dataRows.Add(dataRow);
            }
            return dataRows;
        }

        protected void RenderToDoListJavascript()
        {

            if (this.PresentationName == "ToDoList")
            {
                //Write top half :)
                Response.Write(@"Ext.onReady(function(){
                                    Ext.QuickTips.init();

                                    function formatDate(value){
                                            return value ? value.dateFormat('M d, Y') : '';
                                        }
                                    ");
                Response.Write(@"var fm = Ext.form;

                                    // sample static data for the store
                                    var toDoData = [
                                        ");
                List<string> listItems = ProcessExtJsToDoListData();

                //Merge the noteItems and listItems

                char[] cc = {'[', ']'};
                for (int i = 0; i < listItems.Count; i++)
                {
                    if (i < (listItems.Count - 1))
                    {
                        Response.Write(listItems[i] + @",
                                                       ");
                    }
                    else
                    {
                        Response.Write(listItems[i]  + @"
                                                       ");
                    }
                }

                //Render the bottom half :)
                Response.Write(@"];
                                // create the data store
                                var reader = new Ext.data.ArrayReader({}, [
                                       {name: 'processtype', mapping:0},
                                       {name: 'jobnumber', mapping:1},
                                       {name: 'jobid', mapping:2},
                                       {name: 'startdate', type: 'date', dateFormat: 'n/j/Y h:i:s A', mapping:3},
                                       {name: 'duedate', type: 'date', dateFormat: 'n/j/Y h:i:s A', mapping:4},
                                       {name: 'assignments', mapping:5},
                                       //Begin columns not shown in grid
                                       {name: 'objectid', mapping:6},
                                       {name: 'userid', mapping:9},
                                       {name: 'gotoobjectid', mapping:10},
                                       {name: 'gotopresentationname', mapping:11},
                                       {name: 'processdescription', mapping:12},
                                       {name: 'jobaddress', mapping:13},
                                       {name: 'jobtype', mapping:14},
                                       {name: 'jobstatus', mapping:15},
                                       {name: 'licenseestablishment', mapping:16},
                                       {name: 'routeorder', mapping:17},
                                       // Newly added columns for Issue 5437
                                       {name: 'ToDoListLicenseePermittee', mapping:18},
                                       {name: 'ToDoListLicNumPermNum', mapping:19},
                                       {name: 'ToDoListLtPt', mapping:20},
                                       // Newly added columns for Issue 40781
                                       {name: 'notetext', mapping:7},
                                       {name: 'notetextid', mapping:8},
                                       // New column for Issue 48304
                                       {name: 'ShowHistory', mapping:22}
                                    ]);
                                
                                userGrouping = "+ this.UserInformation[0]["ToDoListGroup"] +@"
                                if (userGrouping !== undefined) {
                                    groupBy = userGrouping
                                    if (groupBy == 'false') {
                                        groupBy = false
                                    }
                                    } else {
                                    groupBy = 'startdate'
                                        };
                                userSortField = "+ this.UserInformation[0]["ToDoListSortField"] +@"
                                if (userSortField !== undefined) {
                                    sortField = userSortField
                                    } else {
                                    sortField = 'startdate'
                                        };
                                userSortDir = "+ this.UserInformation[0]["ToDoListSortDirection"] +@"
                                if (userSortDir !== undefined) {
                                    sortDir = userSortDir
                                    } else {
                                    sortDir = 'ASC'
                                        };
                                var store = new Ext.data.GroupingStore({
                                                reader: reader,
                                                data: toDoData,
                                                groupField: groupBy,
                                                sortInfo: {field: sortField,
                                                           direction: sortDir}
                                            });

                                string = " + this.UserInformation[0]["ToDoListSettings"] +@"

                                var userColumnModel = new Ext.grid.ColumnModel({
                                    columns: string
                                    });

                            // Note history windows and functions (Issue 48304)
                                var noteHistoryReader = new Ext.data.JsonReader({root: 'rows',
								    fields: [
                                       {name: 'jobid', mapping:'jobid'},
                                       {name: 'name', mapping:'name'},
                                       {name: 'notetext', mapping:'notetext'},
                                       {name: 'notedate', mapping:'notedate'}
                                    ]
								});
                                var noteHistoryStore = new Ext.data.Store({
                                        id: 'noteHistoryStore',
                                        autoDestroy: true,
                                        reader: noteHistoryReader
                                });

                            function getNoteHistory (jobId) {
                                var userId = " + this.UserInformation[0]["ObjectId"] +@";
                                // The store will contain all of the note history for every process where 
                                //  you've hovered over the icon. Filter the store so it only shows the 
                                //  history for the process/job you last hovered over.
                                //  Note the filter still applies even though we may add to the store after filtering.
                                noteHistoryStore.filter([
                                    {
                                        property: 'jobid',
                                        value: jobId
                                    }]);
                                // if the store has not been loaded for this job yet, then load it
                                if (noteHistoryStore.getCount() == 0) {
                                    Ext.Ajax.request({
                                        disableCaching: true,
                                        url: 'ToDoHandler.ashx?action=getJobNoteHistory',
                                        method: 'GET',
                                        params: { jobId: jobId, userId: userId},
                                        success: function (response, opts) {
                                            responseJson = Ext.decode('{rows:' + response.responseText + '}');
                                            console.log(responseJson);
                                            // Load the data into the store
                                            // Check for values again just in case multiple calls get
                                            //  made at the same time, so data doesn't get duplicated
                                            if (noteHistoryStore.getCount() == 0) {
                                                noteHistoryStore.loadData(responseJson, true);
                                            }
                                        },
                                        failure: function (response, opts) {
                                            // Not ideal but show an alert on failure.
                                            // Could be part of a loading mask instead if we upgrade from 3.4
                                            alert('Failed to Load Note History. Please try again.');
                                        }
                                    })
                                        }
                            };
                            function createNoteTextPopup(isHistory, index) {
                                // Creates the actual popup window showing the large text area
                                //  uses either the note history store of the to do list store based on where we create the window from
                                var noteTextPopup = new Ext.Window ({
                                    id: 'noteTextPopup',
                                    width: 400,
                                    height: 500,
                                    closable: true,
                                    title: 'Note Text',
                                    layout: 'fit',
                                    items: [
                                        {
                                            id: 'notetext',
                                            fieldLabel: 'Text',
                                            xtype: 'textarea',
                                            anchor: '100%',
                                            enableKeyEvents: true,
                                            readOnly: isHistory,
                                            value: ((isHistory) ? noteHistoryStore : store).getAt(index).get('notetext')
                                        }],
                                    listeners: {
                                        show: function(window){
                                            // Doesn't actually work, something else seems to be stealing focus.  Not worth the time to fix.
                                            window.findById('notetext').focus();
                                        },
                                        beforeclose: function(window){
                                            // If the field is editable then update the store and add to processxml
                                            if (!isHistory && Ext.getCmp('notetext').isDirty())
                                                {
                                                var record = store.getAt(index);
                                                var noteId = record.get('notetextid');
                                                // if a note object doesn't exist already, create one
                                                if (noteId == '') {
                                                    var noteObject = [];
                                                    var objId = record.get('objectid');
                                                    var jobId = record.get('jobid');
                                                    var userId = " + this.UserInformation[0]["ObjectId"] +@";
                                                    var addNewNote = true;
                                                    for(i = 0; i < noteObject.length; i++) {
                                                        if (ProcessId == noteObject[i]['processId']) {
                                                            noteObject[i]['noteText'] = Ext.getCmp('notetext').getValue();
                                                            addNewNote = false;
                                                            break;
                                                        };
                                                    };
                                                    if (addNewNote) {
                                                        noteObject.push({
                                                            noteText : Ext.getCmp('notetext').getValue(),
                                                            processId : objId,
                                                            userId : userId,
                                                            jobId : jobId
                                                        });
                                                    };
                                                  var xml = buildXML(noteObject, []);
                                                  PosseAppendChangesXML(xml);
                                                  // Set the id to the temp id in the XML so we don't create multiple objects on save
                                                  record.set('notetextid', 'NEW' + objId);
                                                }
                                                // Otherwise update the existing note
                                                else
                                                {
                                                    PosseChangeColumn(noteId, 'NoteText', Ext.getCmp('notetext').getValue());   
                                                }
                                                // Update the text in the store so it shows in the grid
                                                record.set('notetext', Ext.getCmp('notetext').getValue());
                                            }
                                        }
                                    }
                                })
                                return noteTextPopup;
                            };
                            function historyItems(jobId) {
                                // The actual grid of note history rows
                                // Since the actual get history is an async ajax call, we start it as
                                //  soon as possible and it will update the store when it's done.
                                getNoteHistory(jobId);
                            var noteHistoryGrid = new Ext.grid.GridPanel ({
                                    store: noteHistoryStore,
                                    width: 600,
                                height: 180,
                                columns: [{
                                    header: 'Note Date',
                                    width: 120, //flex:1
                                    dataIndex: 'notedate'
                                },{
                                    header: 'Process Type',
                                    width: 200,
                                    sortable: false,
                                    hideable: false,
                                    dataIndex: 'name'
                                }, {
                                    header: 'Note',
                                    width: 255,
                                        dataIndex: 'notetext',
                                        listeners: {
                                            dblclick: function(column, grid, index, e) {
                                                // Close the existing popup
                                                popup = Ext.getCmp('noteTextPopup');
                                                if (typeof popup !== 'undefined') popup.close();

                                                // Create the popup with the history text
                                                var rowData = noteHistoryStore.getAt(index);
                                                var noteTextPopup = createNoteTextPopup(true, index);
                                                var noteText = rowData.get('notetext');
                                                noteTextPopup.show();
                                                return;
                                            }
                                        }
                                }]
                                })
                                return noteHistoryGrid;
                            };
                            var noteHistoryPopup = new Ext.Window({
                                 id:'noteHistoryPopup'
                                ,width:620
                                ,height:200
                                ,stateful :false
                                ,layout:'fit'
                                ,closable:true
                                ,closeAction : 'hide'
                                ,title:'Note History (Double Click Note text to expand note)'
								,hideIf: function () {
                                    var win = Ext.getCmp('noteHistoryPopup');
									if (!win.initialConfig.clickShow) win.hide();
                                }
                                ,showAt: function (el, isClick, jobId) {
                                    var win = Ext.getCmp('noteHistoryPopup');
                                    if (isClick) {
                                        win.initialConfig.clickShow = true;
                                        if (win.initialConfig.jobId != jobId) {
                                            win.items.add(historyItems(jobId));
                                            win.show();
                                            var pos = el.getBoundingClientRect();
                                            win.setPagePosition(pos.x + 20, pos.y);
                                            return;
                                        }
                                    }
									if (!win.isVisible()) {
                                        win.items.add(historyItems(jobId));
									win.show();
                                    var pos = el.getBoundingClientRect();
                                    win.setPagePosition(pos.x + 20, pos.y);
                                        return;
                                    }
                                },
                                listeners: {
                                    beforehide: function(window) {
                                        window.initialConfig.clickShow = false;
                                }
                                },
                                items: []
                            });
                                if (userColumnModel !== undefined) {
                                    columnModel = userColumnModel;
                                    } else {
                                        // Due to the column on the user being an expression with override,
                                        //  we should never actually get into this else statement.
                                    var columnModel = new Ext.grid.ColumnModel({
                                        id: 'toDoGridModel',
                                        columns: [
                                            {
                                                id       :'processtype',
                                                header   : 'Process Type',
                                                width    : 155,
                                                dataIndex: 'processtype'
                                            },
                                            {
                                                id       :'jobnumber',
                                                header   : 'Job No.',
                                                width    : 55,
                                                dataIndex: 'jobnumber'
                                            },
                                            {
                                                id       :'ToDoListLicNumPermNum',
                                                header   : 'License/Permit',
                                                width    : 100,
                                                dataIndex: 'ToDoListLicNumPermNum'
                                            },
                                            {
                                                id       :'ToDoListLtPt',
                                                header   : 'License/Permit Type',
                                                width    : 115,
                                                dataIndex: 'ToDoListLtPt'
                                            },
                                            {
                                                id       :'notetext',
                                                header   : 'Note',
                                                width    : 100,
                                                dataIndex: 'notetext',
                                                readOnly: false,
                                                editor: null,
                                                listeners: {
                                                    click: function(column, grid, index, e) {
                                                        // If the popup is already open, close it
                                                        popup = Ext.getCmp('noteTextPopup');
                                                        if (typeof popup !== 'undefined') popup.close();
                                                        // Create the window and populate the data but don't show it
                                                        var rowData = store.getAt(index);
                                                        var noteTextPopup = createNoteTextPopup(false, index);
                                                        var noteText = rowData.get('notetext');
                                                        return;
                                                    }
                                                }
                                            },
                                            {
                                                id       :'notehistory',
                                                width    : 24,
                                                dataIndex: 'ShowHistory',
                                                fixed: true,
												editable: false,
                                                sortable: false,
                                                groupable: false,/*
                                                renderer: function(val, meta, record, rowIndex){
                                                    // Only show the button if the flag to show it is set
                                                    if (val != 'Y') return '';
                                                    var jobId = record.get('jobid');
                                                    return '<img'
																+ ' onmouseout=""Ext.getCmp(\'noteHistoryPopup\').initialConfig.hideIf()""'
																+ ' onmouseover=""Ext.getCmp(\'noteHistoryPopup\').initialConfig.showAt(this, false, '+jobId+')""'
																+ ' onclick=""Ext.getCmp(\'noteHistoryPopup\').initialConfig.showAt(this, true, '+jobId+')""'
																+ ' src=""images/icons/note-history-icon.png"" width=""16"" height=""16"">';
                                                },*/
                                            },
                                            {
                                                id       : 'startdate',
                                                header   : 'Scheduled',
                                                width    : 85,
                                                dataIndex: 'startdate',
                                                editor : null
                                            },
                                            {
                                                id       : 'duedate',
                                                header   : 'Due Date',
                                                width    : 65,
                                                dataIndex: 'duedate',
                                                editor: null
                                            }
                                        ],
                                        defaults: {
                                            sortable: true
                                        }
                                    });
                                    };
                                    columnModel.getColumnById('notetext').on('click', function(column, grid, index, e) {
                                                        // If the popup is already open, close it
                                                        popup = Ext.getCmp('noteTextPopup');
                                                        if (typeof popup !== 'undefined') popup.close();
                                                        // Create the window and populate the data but don't show it
                                                        var rowData = store.getAt(index);
                                                        var noteTextPopup = createNoteTextPopup(false, index);
                                                        var noteText = rowData.get('notetext');
                                                        return;
                                                    });
                                    columnModel.setRenderer(columnModel.getIndexById('notehistory'), function(val, meta, record, rowIndex, colIndex, store)
                                        {
                                            // Only show the button if the flag to show it is set
                                            //if (record.get('ShowHistory') != 'Y') return '';
                                            if (val != 'Y') return '';
                                            var jobId = record.get('jobid');
                                            return '<img'
                                                        + ' onmouseout=""Ext.getCmp(\'noteHistoryPopup\').initialConfig.hideIf()""'
                                                        + ' onmouseover=""Ext.getCmp(\'noteHistoryPopup\').initialConfig.showAt(this, false, '+jobId+')""'
                                                        + ' onclick=""Ext.getCmp(\'noteHistoryPopup\').initialConfig.showAt(this, true, '+jobId+')""'
                                                        + ' src=""images/icons/note-history-icon.png"" width=""16"" height=""16"">';
                                        });

                                for (var i = 0, len = columnModel.config.length; i < len; i++){
                                    if(columnModel.config[i].id.indexOf('date') !== -1){
                                        columnModel.config[i].renderer= Ext.util.Format.dateRenderer('m/d/Y');
                                            columnModel.config[i].editor= new fm.DateField({
                                                        format: 'm/d/y',
                                                        updateEl: true,
                                                        cancelOnEsc: true,
                                                        updateOnEnter: true,
                                                        ignoreNoChange: true,
                                                        listeners: {
                                                            focus: function(editor) {
                                                                editor.selectText();
                                                            }
                                                        }
                                                    })
                                    } else if(columnModel.config[i].id.indexOf('note') !== -1){
                                        columnModel.config[i].editor = new fm.TextField({
                                                        updateEl: true,
                                                        cancelOnEsc: true,
                                                        updateOnEnter: true,
                                                        ignoreNoChange: true,
                                                        listeners: {
                                                            render: function(cmp) {
                                                                cmp.getEl().on('dblclick', function(e){
                                                                    // The popup window is created by the click action on the column
                                                                    //  so all we do here is show it
                                                                    // The window appears in the center of the browser,
                                                                    //  not worth the time to figure out positioning
                                                                    Ext.getCmp('noteTextPopup').show();
                                                                    return;
                                                                })
                                                            },
                                                            focus: function(editor) {
                                                                editor.selectText();
                                                            }
                                                        }
                                                    })
                                    }
                                }
                                // manually load local data
                                store.loadData(toDoData);

                                var dateRouting = new Ext.form.DateField({
                                        id: 'dateRouting',
                                        value: new Date()
                                    });

                                var routingStore = new Ext.data.GroupingStore({
                                                        reader: reader,
                                                        data: toDoData,
                                                        sortInfo: {
                                                            field: 'routeorder',
                                                            direction: 'ASC'
                                                        }
                                                    });

                                 var routeButtonHandler = function(button,event) {
                                                var dateValue = dateRouting.getValue();
                                                var addresses = [];
                                                var waypts = [];
                                                var counter = 1;
                                                var start;
                                                var end;
                                                routingStore.each(function(e) {
                                                 if (e.data['startdate'] != null) {

                                                   if (dateValue.format('Y-m-d').toString() == e.data['startdate'].format('Y-m-d').toString()) {
                                                       if (counter == 1) {
                                                          start = e.data['jobaddress'];
                                                       }
                                                       if (counter > 1) {
                                                            waypts.push({
                                                                location:e.data['jobaddress'],
                                                                stopover:true
                                                           });
                                                           end = e.data['jobaddress'];
                                                       }
                                                       counter = counter + 1;
                                                   }
                                                 }
                                                });
                                            };

                                excelId = "+ this.GetPaneIdFromName("Excel", this.PaneId) +@"
                                // create the Grid
                                var grid = new Ext.grid.EditorGridPanel({
                                    id: 'toDoGridPanel',
                                    store: store,
                                    colModel: columnModel,
                                    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
                                    view: new Ext.grid.GroupingView({
                                                forceFit:true,
                                                groupTextTpl: '{text}'
                                            }),
                                    viewConfig: {
                                        forceFit: true
                                    },
                                    tbar:[
                                        {text: 'Save',
                                        handler: function(){
                                          // Close any opened popups
                                          popup = Ext.getCmp('noteTextPopup');
                                          if (typeof popup !== 'undefined') popup.close();
                                          historyPopup = Ext.getCmp('noteHistoryPopup');
                                          if (typeof historyPopup !== 'undefined') historyPopup.close();
                                          var xml = buildXML(noteObject, deleteNoteObject);
                                          PosseAppendChangesXML(xml);
                                          PosseSubmit();
                                            }},
                                        {xtype: 'tbseparator' },
                                        {text: 'Save as Excel',
                                        handler: function(){
                                            PosseSubmitLinkReturn('ExcelDownload.aspx?PossePresentation=ToDoList&SaveAsExcelReport=Y', 3, excelId);
                                            }}
                                        ],
                                    stripeRows: true,
                                    width: 660,
                                    split: true,
                                    region: 'center',
                                    title: 'To Do List',
                                    autoExpandColumn: 'process',
                                    clicksToEdit: 1

                                });
                    ");
                //this is for specialized string handling for markup
                Response.Write("var processTplMarkup = [\n" +
                               "    '<span class=\"toDoHeader\">{processtype}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Job Number:</span> <span class=\"toDoField\">{jobnumber}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Job Type:</span> <span class=\"toDoField\">{jobtype}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Job Status:</span> <span class=\"toDoField\">{jobstatus}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Assignments:</span> <span class=\"toDoField\">{assignments}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Establishment:</span> <span class=\"toDoField\">{licenseestablishment}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Address:</span> <span class=\"toDoField\">{jobaddress}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">License Number / Permit Number:</span> <span class=\"toDoField\">{ToDoListLicNumPermNum}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Licensee / Permittee / Registrant:</span> <span class=\"toDoField\">{ToDoListLicenseePermittee}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">License Type / Permit Type:</span> <span class=\"toDoField\">{ToDoListLtPt}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Note:</span> <span class=\"toDoField\">{notetext}</span><br/>',\n" +
                               "    '<div class=\"toDoButtonContainer\">',\n" +
                               "    '   <div class=\"button\"><a href=\"Default.aspx?PossePresentation=Default&PosseObjectId={jobid}\"><span>Go To Job</span></a></div>',\n" +
                               "    '   <div class=\"button\"><a href=\"Default.aspx?PossePresentation={gotopresentationname}&PosseObjectId={gotoobjectid}\"><span>Go To Process</span></a></div>',\n" +
                               "    '   <div class=\"button\"><a href=\"javascript:PossePopup(\\'Assignments_{objectid}\\', \\'ProcessAssignment.aspx?PosseObjectId={objectid}\\', 250, 725, \\'Assignments_{objectid}\\')\"><span>Re-Assign</span></a></div>',\n" +
                               "    '</div>'\n" +
                               "];\n");
                Response.Write(@"var processTpl = new Ext.Template(processTplMarkup);
                                var userId = " + this.UserInformation[0]["ObjectId"]+@"
                                var ct = new Ext.Panel({
                                    renderTo: 'todolist',
                                    frame: true,
                                    width: 960,
                                    height: 700,
                                    layout: 'border',
                                    items: [
                                        {   id: 'centerPanel',
                                            region: 'center',
                                    layout: 'border',
                                    items: [
                                                grid
                                            ]
                                        },
                                        {
                                            id: 'detailPanel',
                                            region: 'east',
                                            width: " + this.UserInformation[0]["ToDoPaneLastSize"] +@",
                                            collapsible: true,
                                            split: true,
                                            autoScroll: true,
                                            title: 'Details',
                                            collapsed: " + this.UserInformation[0]["ToDoPaneCollapsed"] +@",
                                            bodyStyle: {
                                                background: '#F7F7F7',
                                                padding: '7px'
                                            },
                                            html: 'Please select a process to see additional details.'
                                        }
                                    ]
                                });
                                grid.getSelectionModel().on('rowselect', function(sm, rowIdx, r) {
                                    var detailPanel = Ext.getCmp('detailPanel');
                                    processTpl.overwrite(detailPanel.body, r.data);
                                });

                                var noteObject = [];
                                var deleteNoteObject = [];
                                function newNote(t, p, u, jobId) {
                                  if (t.length < 1){
                                    return ''
                                  }
                                  var record = store.getAt(store.findExact('objectid', p)); 
                                  // Only return the xml for a new object if one hasn't been created for this process already
                                  if (record.get('notetextid') == '')
                                  {
                                    return '<object id=""NEW'+p+'"" objectdef=""o_ABC_ToDoNote"" action=""Insert""> <column name=""NoteText"">' + t + '</column> <column name=""ProcessId"">' + p + '</column> <column name=""UserId"">'+ u +'</column> <column name=""JobId"">'+ jobId +'</column> </object> '
                                  }
                                  else
                                  {
                                    // Technically this does nothing but I couldn't find a better way to do it.
                                    return '<object id=""NEW'+p+'""></object>';
                                  }
                                };
                                function deleteNote (noteId, noteText) {
                                  if (noteText.length < 1) {
                                    // we don't actually delete the note object any more because we need it for history (Issue 48304)
                                    PosseChangeColumn(noteId, 'NoteText', '');
                                    return '';
                                  } else {
                                    PosseChangeColumn(noteId, 'NoteText', noteText);
                                    return '';
                                  }
                                }
                                function buildXML(noteObject, deleteNoteObject){
                                  var xmlString = ''
                                  for (j = 0; j < noteObject.length; j++) {
                                    xmlString += newNote(noteObject[j]['noteText'], noteObject[j]['processId'], noteObject[j]['userId'], noteObject[j]['jobId']);
                                  }
                                  for (k = 0; k < deleteNoteObject.length; k++) {
                                    xmlString += deleteNote(deleteNoteObject[k]['noteId'], deleteNoteObject[k]['noteText']);
                                  }
                                  return xmlString
                                };
                                grid.on('afteredit', function(e) {
                                  var noteId = e.record.get('notetextid');
                                  var objId = e.record.get('objectid');
                                  var jobId = e.record.get('jobid');
                                  var userId = " + this.UserInformation[0]["ObjectId"] +@";
                                  function buildNoteObject(noteObject, ProcessId) {
                                    var addNewNote = true;
                                    for(i = 0; i < noteObject.length; i++) {
                                      if (ProcessId == noteObject[i]['processId']) {
                                        noteObject[i]['noteText'] = e.value.toString();
                                        addNewNote = false;
                                        break;
                                      };
                                    };
                                    if (addNewNote) {
                                      noteObject.push({
                                        noteText : e.value.toString(),
                                        processId : objId,
                                        userId : userId,
                                        jobId : jobId
                                        });
                                    };
                                  };
                                  function buildDeleteNoteObject(deleteNoteObject, ProcessId) {
                                    var addDeleteObject = true;
                                    for (i = 0; i < deleteNoteObject.length; i++) {
                                      if (ProcessId == deleteNoteObject[i]['processId']) {
                                        deleteNoteObject[i]['noteText'] = e.value.toString();
                                        addDeleteObject = false;
                                        break;
                                      };
                                    };
                                    if (addDeleteObject) {
                                      deleteNoteObject.push({
                                        noteText : e.value.toString(),
                                        noteId : noteId,
                                        processId : objId
                                        });
                                    };
                                  };
                                  if (e.field == 'notetext') {
                                    if (noteId) {
                                      buildDeleteNoteObject(deleteNoteObject, objId);
                                    } else {
                                      buildNoteObject(noteObject, objId);
                                    }
                                  };
                                  if (e.field == 'startdate') {
                                     PosseChangeColumn(objId, 'ScheduledStartDate', e.value.format('Y-m-d').toString());
                                  }
                                  if (e.field == 'duedate')
                                     PosseChangeColumn(objId, 'ScheduledCompleteDate', e.value.format('Y-m-d').toString());
                                  if (e.field == 'RouteOrder')
                                      PosseChangeColumn(objId, 'RouteOrder', e.value.toString());
                                });
                                grid.on('rowclick', function(t, r, e) {
                                    var store = t.getStore();
                                    var record = store.getAt(r);
                                    var address = record.get('jobaddress');
                                    var title = record.get('licenseestablishment');
                                });

                                // All the events to register when to save grid changes
                                grid.on('columnresize',function(t) {
                                    var myJSON = JSON.stringify(grid.colModel.config,function(key,value){
                                        if(key =='scope'||key=='events'||key=='editor'){return null;}
                                        else {return value;}
                                        })
                                    PosseChangeColumn(userId, 'ToDoListSettings', myJSON.toString())
                                });
                                grid.colModel.on('columnmoved',function(t) {
                                    var myJSON = JSON.stringify(grid.colModel.config,function(key,value){
                                        if(key =='scope'||key=='events'||key=='editor'){return null;}
                                        else {return value;}
                                        })
                                    PosseChangeColumn(userId, 'ToDoListSettings', myJSON.toString())
                                });
                                grid.on('sortchange',function(t) {
                                    var sortField = grid.store.sortInfo.field
                                    var sortDirection = grid.store.sortInfo.direction
                                    PosseChangeColumn(userId, 'ToDoListSortField', ""'"" + sortField +""'"")
                                    PosseChangeColumn(userId, 'ToDoListSortDirection', ""'"" + sortDirection +""'"")
                                });
                                grid.on('groupchange',function(t) {
                                    var groupChange = grid.store.groupField;
                                    PosseChangeColumn(userId, 'ToDoListGroup', ""'"" + groupChange.toString() +""'"")
                                });
                                grid.on('groupclick',function(t) {
                                    var groupChange = grid.store.groupField;
                                    PosseChangeColumn(userId, 'ToDoListGroup', ""'"" + groupChange.toString() +""'"")
                                });
                                grid.on('resize',function(t) {
                                    var myJSON = JSON.stringify(grid.colModel.config,function(key,value){
                                        if(key =='scope'||key=='events'||key=='editor'){return null;}
                                        else {return value;}
                                        })
                                    PosseChangeColumn(userId, 'ToDoListSettings', myJSON.toString())
                                });
                                grid.colModel.on('hiddenchange',function(t, columnIndex, hidden) {
                                    var myJSON = JSON.stringify(grid.colModel.config,function(key,value){
                                        if(key =='scope'||key=='events'||key=='editor'){return null;}
                                        else {return value;}
                                        })
                                    PosseChangeColumn(userId, 'ToDoListSettings', myJSON.toString())
                                });
                                ct.items.items[1].on('resize',function(t){
                                    PosseChangeColumn(userId, 'ToDoPaneLastSize', ct.items.items[1].lastSize.width)
                                });
                                ct.items.items[1].on('collapse',function(t){
                                    PosseChangeColumn(userId, 'ToDoPaneCollapsed', 'true')
                                });
                                ct.items.items[1].on('expand',function(t){
                                    PosseChangeColumn(userId, 'ToDoPaneCollapsed', 'false')
                                });
                                grid.GridPanel
                            });");

                Response.Write(@"");
            }
        }
    }
}
