using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
	public partial class AssignProcesses : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			int firstProcessId = 0, band = 0;
			bool hasList = false;
			Literal literal = new Literal();
			StringBuilder html = new StringBuilder(), script = new StringBuilder();
            List<OrderedDictionary> processes;
            List<PossibleAssignment> candidates;

			this.LoadPresentation();

			processes = this.GetPaneData(true);

			script.Append("function AssignProcesses(userId) {");

            foreach (OrderedDictionary process in processes)
            {
                //if ((bool)processes.Fields["WebSelected"].Value
                if (true)
                {
                    if (firstProcessId == 0)
                    {
                        firstProcessId = (int)process["processid"];
                    }

                    script.AppendFormat("opener.PosseAppendChangesXML('<object id=\"{0}\"><assignments><user id=\"' + userId + '\"/></assignments></object>');",
                        process["processid"]);
                }
			}

			script.Append("opener.PosseSubmit();window.close();}");
			this.ClientScript.RegisterClientScriptBlock(this.GetType(), "AssignAndClose", script.ToString(), true);

			if (firstProcessId == 0)
			{
				this.ErrorMessage = "You did not select any items to reassign.  Please close this window and try again.";
			}
			else
			{
				html.Append("<table class=\"possegrid\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">");
				html.Append("<thead class=\"possegrid\">");
				html.Append("<tr class=\"possegrid\">");
				html.Append("<th class=\"possegrid\" valign=\"bottom\" style=\"text-align:left\">User Name</th>");
				html.Append("<th class=\"possegrid\" valign=\"bottom\" style=\"text-align:left\">Logon Id</th>");
				html.Append("</tr></thead>");

				candidates = this.GetPossibleAssignments(firstProcessId.ToString());
                candidates.Sort();


                foreach (PossibleAssignment candidate in candidates)
                {
                    if (!candidate.CurrentlyAssigned && candidate.CanUnassign)
                    {
                        band = (band + 1) % 2;
                        hasList = true;

                        html.AppendFormat("<tbody class=\"posseband_{0}\"><tr class=\"possegrid\" style=\"cursor:pointer\" title=\"Click to assign\" onclick=\"AssignProcesses({1})\">", band, candidate.ObjectId.ToString());
                        html.AppendFormat("<td class=\"possegrid\" valign=\"middle\" style=\"text-align: left\" nowrap>{0}</td>", candidate.Name);
                        html.AppendFormat("<td class=\"possegrid\" valign=\"middle\" style=\"text-align: left\" nowrap>{0}</td>", candidate.ShortName);
                        html.Append("</tr></tbody>");
                    }
                }
                
				html.Append("</table>");


				if (hasList)
				{
					literal.Text = html.ToString();

					this.pnlPaneBand.Controls.Add(literal);
				}
				else
				{
					this.ErrorMessage = "You may not reassign these items.";
				}
			}

			this.RenderErrorMessage(this.pnlPaneBand);
			this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
			this.HasPresentation = false;
		}
		#endregion
	}
}