<%@ Page Language="C#" MasterPageFile="~/Outrider.master" AutoEventWireup="true"
	CodeFile="Payment.aspx.cs" Inherits="Computronix.POSSE.Outrider.Payment" Title="Payment" %>

<asp:Content ID="cntPaneBand" ContentPlaceHolderID="cphPaneBand" runat="Server">
        <table ID="Table1" Width="100%">
          <tr>
            <td class="titleheader">Payment Error</td>
          </tr>
          <tr>
            <td ID="TableCell1" class="possedetail">
              <br />
              An Unexpected error occurred in attempting to start a payment.<br />
              <br />
              The error reported was:
            </td>
          </tr>
          <tr>
            <td class="possedetail">
              <asp:Label ID="ErrorSummary" runat="server" CssClass="possedetail" Width="570px" Text="Summary"></asp:Label>
              <br />
              <br />
            </td>
          </tr>
          <tr>
            <td class="titleheader">Details</td>
          </tr>
          <tr>
            <td class="possedetail">
              <asp:TextBox ID="ErrorDetail" runat="server" CssClass="possedetail" Width="570px" Text="Detail" Rows=10 TextMode=MultiLine></asp:TextBox>
            </td>
          </tr>
          <tr>
            <td class="possedetail">
              <asp:HyperLink ID="Link1" runat="server" NavigateUrl="~/Default.aspx">Click here to return</asp:HyperLink>
            </td>
          </tr>
        </table>
</asp:Content>
