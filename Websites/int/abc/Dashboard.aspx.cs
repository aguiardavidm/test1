using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class Dashboard : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            if (this.IsGuest)
            {
                SetCookie("ToDashboard", "1");
                Response.Redirect(this.LoginPageUrl);
            }
            SetCookie("ToDashboard", "0");
            // Ensure the outer ASPX form is never submitted.  Works around browser behaviour that submits single field forms when [Enter] is pressed.
            //this.Form.Attributes["onsubmit"] = "return false";
            this.LoadPresentation();
            this.RenderUserInfo(this.pnlUserInfo);
            this.RenderMenuBand(this.pnlMenuBand);
            //this.RenderFooterBand(this.pnlFooterBand);
        }
		#endregion
	}
}
