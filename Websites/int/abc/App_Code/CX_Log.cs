using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Computronix.Logging
{
    /// <summary>
    /// Summary description for Logging
    /// </summary>
    public class Log
    {
        private static List<Log> logItems = new List<Log>();
        private static string currentFileName;
        private static StreamWriter streamWriter;

        public string Message { get; set; }
        public DateTime Time { get; set; }
        public LogLevel Level { get; set; }
        public object Identifier { get; set; }

        private string formattedMessage
        {
            get
            {
                if (Message.Contains("{0}"))
                {
                    return String.Format(Message, Identifier);
                }
                else
                {
                    return Message;
                }
            }
        }

        public string FormattedLogMessage
        {
            get
            {
                return String.Format("{0:yyyy_MM_dd-hh:mm:ss:ff} {1}", Time, Message);
            }
        }

        public static void ClearLog()
        {
            logItems.Clear();
        }

        public static void Write(string message, DateTime time, LogLevel level, object identifier)
        {
            var log = new Log()
            {
                Message = message,
                Time = time,
                Level = level,
                Identifier = identifier
            };
            logItems.Add(log);
            writeToFile(log);
        }

        public static void Write(string message)
        {
            Write(message, DateTime.Now, LogLevel.Normal, null);
        }

        public static void Write(string message, object identifer)
        {
            Write(message, DateTime.Now, LogLevel.Normal, identifer);
        }

        public static void Write(string message, LogLevel level)
        {
            Write(message, DateTime.Now, level, null);
        }

        public static void Write(string message, LogLevel level, object identifier)
        {
            Write(message, DateTime.Now, level, identifier);
        }

        private static void writeToFile(Log log)
        {
            var logLevel = getLogLevel();

            if (logLevel == log.Level || (logLevel == LogLevel.Debug) || (logLevel == LogLevel.Normal && log.Level != LogLevel.Debug))
            {
                var fileName = getLogFileName();
                if (!String.IsNullOrEmpty(fileName))
                {
                    if (streamWriter == null || String.IsNullOrEmpty(currentFileName) || currentFileName.ToLower() != fileName.ToLower())
                    {
                        currentFileName = fileName;
                        streamWriter = new StreamWriter(fileName, true);
                    }

                    if (!streamWriter.AutoFlush)
                    {
                        streamWriter.AutoFlush = true;
                    }

                    streamWriter.WriteLine(log.FormattedLogMessage);
                }
                else
                {
                    throw new Exception("Log file name not found!");
                }
            }
        }

        private static string getLogFileName()
        {
            var logFileName = System.Configuration.ConfigurationManager.AppSettings["CX_Log.FileName"];

            if (!String.IsNullOrEmpty(logFileName) && Directory.Exists(Path.GetDirectoryName(logFileName)))
            {
                return Path.Combine(Path.GetDirectoryName(logFileName), String.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(logFileName), DateTime.Now.ToString("yyyyMM"), Path.GetExtension(logFileName)));
            }
            return string.Empty;
        }

        private static LogLevel getLogLevel()
        {
            var logLevel = System.Configuration.ConfigurationManager.AppSettings["CX_Log.LogLevel"];
            if (!String.IsNullOrEmpty(logLevel))
            {
                switch (logLevel.ToLower())
                {
                    case "debug":
                        return LogLevel.Debug;
                    case "error":
                        return LogLevel.Error;
                    case "normal":
                    default:
                        return LogLevel.Normal;
                }
            }
            else
            {
                return LogLevel.Normal;
            }
        }
    }

    public enum LogLevel
	{
        Debug, Normal, Error
	}
}