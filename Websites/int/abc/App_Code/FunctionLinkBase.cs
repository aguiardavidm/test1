using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
	/// <summary>
	/// Base class for all function links.
	/// </summary>
	public partial class FunctionLinkBase :
		System.Web.UI.UserControl
	{
		#region Data Members
		private string text = null;
		/// <summary>
		/// Provides access to the text for the function link.
		/// </summary>
		public virtual string Text
		{
			get
			{
				return this.text;
			}
			set
			{
				this.text = value;
			}
		}

		private string navigationUrl = null;
		/// <summary>
		/// Provides access to the navigation url for the function link.
		/// </summary>
		public virtual string NavigationUrl
		{
			get
			{
				return this.navigationUrl;
			}
			set
			{
				this.navigationUrl = value;
			}
		}

		private bool enabled = false;
		/// <summary>
		/// Indicates if the function link is enabled or disabled.
		/// </summary>
		public virtual bool Enabled
		{
			get
			{
				return this.enabled;
			}
			set
			{
				this.enabled = value;
			}
		}

        private RoundTripFunction functionType = RoundTripFunction.Submit;
		/// <summary>
		/// Indicates the function type the function link performs.
		/// </summary>
        public virtual RoundTripFunction FunctionType
		{
			get
			{
				return this.functionType;
			}
			set
			{
				this.functionType = value;
			}
		}
		#endregion
	}
}