using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Computronix.CXTools.SuperClass;

namespace Computronix
{
    /// <summary>
    /// Switches the level of the SuperClass trace manager to higher = more detail.
    /// </summary>
    public class cxTraceManger : TraceManager
    {
        public override void Write(int level, string component, string value)
        {
            if (level <= this.Level)
                base.Write(1, component, value);
        }

        public override void Write(int level, string component, string value, string category)
        {
            if (level <= this.Level)
                base.Write(1, component, value, category);
        }

        public override void WriteLine(int level, string component, string value)
        {
            if (level <= this.Level)
                base.WriteLine(1, component, value);
        }

        public override void WriteLine(int level, string component, string value, int indentLevel)
        {
            if (level <= this.Level)
                base.WriteLine(1, component, value, indentLevel);
        }

        public override void WriteLine(int level, string component, string value, string category)
        {
            if (level <= this.Level)
                base.WriteLine(1, component, value, category);
        }

        public override void WriteLine(int level, string component, string value, string category, int indentLevel)
        {
            if (level <= this.Level)
                base.WriteLine(1, component, value, category, indentLevel);
        }

        public override void WriteLineIn(int level, string component, string value)
        {
            if (level <= this.Level)
                base.WriteLineIn(1, component, value);
        }

        public override void WriteLineIn(int level, string component, string value, string category)
        {
            if (level <= this.Level)
                base.WriteLineIn(1, component, value, category);
        }

        public override void WriteLineOut(int level, string component, string value)
        {
            if (level <= this.Level)
                base.WriteLineOut(1, component, value);
        }

        public override void WriteLineOut(int level, string component, string value, string category)
        {
            if (level <= this.Level)
                base.WriteLineOut(1, component, value, category);
        }

    }
}
