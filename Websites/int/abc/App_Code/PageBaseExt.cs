using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Computronix.POSSE.Winchester.Api;

namespace Computronix.POSSE.Outrider
{
    /// <summary>
    /// Extension class for PageBase.  Place any customizations in here.
    /// </summary>
    public class PageBaseExt : PageBase
    {
	    #region PageBase

        private string ipAddress = null;
        //<summary>
        //Provides access to the IP Address. We first try yo geth the IP Address the proxy recorded, if present
        //</summary>
        protected virtual string IPAddress
        {
            get
            {
                if (ipAddress == null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }
                if (ipAddress == null)
                {
                    ipAddress = Request.ServerVariables["Remote_ADDR"];
                }
                if (ipAddress == null)
                {
                    ipAddress = Request.UserHostAddress;
                }
                return ipAddress;
            }

        }


        private string hostName = null;
        //<summary>
        //Provides access to hostName
        //</summary>
        protected virtual string HostName
        {
            get
            {
                if (hostName == null)
                {
                    hostName = Request.UserHostName;
                }
                return hostName;
            }

        }

        //<summary>
        //Provides access to the Login Audit User
        //</summary>
        protected virtual string LoginAuditUser
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginAuditUser"];
            }
        }

        //<summary>
        //Provides access to the Login Audit Password
        //</summary>
        protected virtual string LoginAuditPass
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginAuditPass"];
            }
        }

	    /// <summary>
        /// Whether to enable Save As Excel on the current search presentation - this requires an Excel pane built on the search with a Python rendering that returns XML
        /// </summary>
        protected virtual bool SaveAsExcelExtractOnSearches
        {
            get
            {
                string saveAsExcel = ConfigurationManager.AppSettings["SaveAsExcelExtractOnSearches"];
                string excludePresNames = ConfigurationManager.AppSettings["SaveAsExcelExtractExcludeSearchPresentations"];

                return this.PresentationType == PresType.List &&
                    string.Compare(saveAsExcel, "true", StringComparison.InvariantCultureIgnoreCase) == 0
                    && !excludePresNames.Contains(this.Presentation);
            }
        }

        private string changesOnObject = "";
        /// <summary>
        /// Provides access to the ChangesOnObject from the form data.  (Request.Form["ChangesOnObject"])
        /// </summary>
        protected virtual string ChangesOnObject
        {
            get
            {
            changesOnObject = this.Request.Form["ChangesOnObject"];
            return changesOnObject;
            }
            set
            {
            changesOnObject = value;
            }
        }

        private string changesPending = "";
        /// <summary>
        /// Provides access to the ChangesPending from the form data.  (Request.Form["ChangesPending"])
        /// </summary>
        protected virtual string ChangesPending
        {
            get
            {
            if (this.ChangesOnObject == this.ObjectId)
            {
                if (String.IsNullOrEmpty(changesPending))
                {
                changesPending = this.Request.Form["ChangesPending"];
                }
            }
            else
            {
                changesPending = "";
            }
            return changesPending;
            }
            set
            {
            changesPending = value;
            }
        }

        /// <summary>
        /// Provides access to the creator of the last document to be uploaded.  (Request.QueryString["PosseCreatedBy"])
        /// </summary>
        protected virtual string LastUploadCreatedBy
        {
            get
            {
                return this.Request.QueryString["PosseCreatedBy"];
            }
        }

        private Dictionary <int, Boolean> outstandingFeesCondition = null;
        /// <summary>
        /// Provides access to the cached value of OutstandingFees condition, which
        /// is only used when we're doing a filteredSearch.
        /// </summary>
        protected virtual Dictionary <int, Boolean> OutstandingFeesCondition
        {
            get
            {
                return outstandingFeesCondition;
            }
            set
            {
                outstandingFeesCondition = value;
            }
        }

        /// <summary>
        /// Returns the HTML that renders the current pane.
        /// </summary>
        /// <param name="startTabIndex">The value to use for the first tab index.</param>
        /// <returns>The HTML necessary to render the current pane.</returns>
        protected override string GetPaneHTML(int startTabIndex)
        {
            //return StripFormTags(this.OutriderNet.GetPaneHTML(startTabIndex));
            return this.OutriderNet.GetPaneHTML(startTabIndex);
        }

        /// <summary>
        /// Returns the HTML that creates the hidden "changes" form.
        /// </summary>
        /// <returns>The HTML for the data changes cache.</returns>
        protected override void GetPaneChangesHTML()
        {
            string changesObjectId;
            if (!String.IsNullOrEmpty(this.ObjectId))
            {
            changesObjectId = this.ObjectId;
            }
            else
            {
            changesObjectId = "";
            }
            String changesPendingInput = String.Format("<input name=\"changespending\" id=\"changespending\" value=\"{0}\"/>", this.ChangesPending);
            String changesObjectInput = String.Format("<input name=\"changesonobject\" id=\"changesonobject\" value=\"{0}\"/>", changesObjectId);
            this.PaneChangesHtml = this.OutriderNet.GetPaneChangesHTML().Replace("</form>", String.Format("{0}{1}</form>", changesPendingInput, changesObjectInput));

            //we need to make the forms fields non-hidden to get around a Chrome bug
            this.PaneChangesHtml = this.PaneChangesHtml.Replace("type=hidden", "");
            return;
        }

        /// <summary>
        /// Generates the requested presentation and loads the pane html.
        /// </summary>
        protected override void LoadPresentation(bool returnAfterSubmit, bool startDialogWithChanges)
        {
            bool searchPressed = false, sortCriteriaExists = false, clickSortPressed = false;
            int criteriaCount = 0;
            string objectId = null;
            StringBuilder search = new StringBuilder(), xml = new StringBuilder();
            OrderedDictionary data = null;

            // hook to load system information however the site may define and manage it.
            LoadSystemInformation();

            this.LoadPresentationCore(returnAfterSubmit, startDialogWithChanges);

            // Initialize condition properties.
            if (this.HasPresentation)
            {
                LoadConditions();
            }

            if (returnAfterSubmit && this.RoundTripFunction == RoundTripFunction.Submit &&
                string.IsNullOrEmpty(this.ErrorMessage))
            {
                return;
            }

            if (this.HasPresentation)
            {
                // The filtered list functionality is toggled by the "FilteredList"
                // condition on the pane and is implemented in two parts.
                // First, before we load the pane for the user we populate
                // search fields from cookies so the user's previous search is remembered.
                // We then refresh the pane so the fields contain the new values.
                // The first part is now finished.
                // We then let the normal pane loading happen at this point.
                // When the presentation is loaded the second part happens.
                // Based on what search criteria has been entered, and some hidden
                // control fields, we do the search and retrieve the results grid.
                // The results grid HTML is appended to the HTML loaded during
                // the normal pane loading.  Also, the values used for the search
                // are stored in the cookies so they are ready for the next page load.
                if ((this.PresentationType == PresType.Tabs
                    || this.PresentationType == PresType.Wizard)
                    && this.FilteredList)
                {

                    // Cache the value of the "OutstandingFees"
                    OutstandingFeesCondition = new Dictionary <int, Boolean>();
                    foreach (VisiblePaneInfo pane in this.Panes)
                    {
                        OutstandingFeesCondition.Add(pane.PaneId, this.GetCondition(pane.PaneId, "OutstandingFees"));
                    }

                    data = this.GetPaneData(true)[0];
                    objectId = data["objecthandle"].ToString();

                    xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", objectId);

                    if (this.GetCookie("SearchObjectID") != this.Request.QueryString["PosseObjectId"] && 
                            !this.Request.QueryString["PossePresentation"].ToString().StartsWith("FIL_"))
                    {
                        foreach (DictionaryEntry column in data)
                        {
                            string[] allCookies = this.Request.Cookies.AllKeys;
                            foreach(string cookie in allCookies)
                            {
                                if (cookie.Contains("Search:"))
                                {
                                    this.RemoveCookie(cookie);
                                }
                            }
                        }
                        this.SetCookie("SearchObjectId", this.Request.QueryString["PosseObjectId"]);
                    }

                    if (this.GetCookie("Search:PaneId") != this.PaneId.ToString())
                    {
                        clickSortPressed = false;
                        this.SetCookie("Search:PaneId", this.PaneId.ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(this.SortColumns))
                        {
                            clickSortPressed = (this.SortColumns.Length > 2);
                        }
                    }

                    if ((bool)data["ignore_searchbuttonpressed"] || clickSortPressed)
                    {
                        searchPressed = true;
                        xml.Append("<column name=\"Ignore_SearchButtonPressed\">false</column>");
                    }
                    else
                    {
                        foreach (DictionaryEntry column in data)
                        {
                            if (!column.Key.ToString().StartsWith("Ignore_") && column.Key.ToString() != "objecthandle")
                            {
                                if (column.Value == null &&
                                  !string.IsNullOrEmpty(this.GetCookie("Search:" + column.Key.ToString())))
                                {
                                    criteriaCount++;
                                    xml.AppendFormat("<column name=\"{0}\">{1}</column>", column.Key.ToString(),
                                      this.GetCookie("Search:" + column.Key.ToString()));
                                }
                                // Set a default value for a field if the cookie is empty
                                //  Requires having a "DEF_" detail of the same name painted on the presentation, 
                                //  containing the value the FIL_ field should be defaulted to
                                if (column.Key.ToString().StartsWith("DEF_") && string.IsNullOrEmpty(this.GetCookie("Search:USE_" + column.Key.ToString())) &&
                                  string.IsNullOrEmpty(this.GetCookie("Search:" + column.Key.ToString().Replace("DEF_", "FIL_"))))
                                {
                                    criteriaCount++;
                                    xml.AppendFormat("<column name=\"{0}\">{1}</column>", column.Key.ToString().Replace("DEF_", "FIL_"),
                                      column.Value);
                                    this.SetCookie("Search:USE_"+ column.Key.ToString(), "N");
                                }
                            }
                        }
                    }

                    xml.Append("</object>");

                    if (criteriaCount > 0 || searchPressed)
                    {
                        this.ProcessFunction(objectId, this.PaneId, this.PaneId, RoundTripFunction.Refresh,
                            (this.DataChanges == null ? this.GetCacheState(): this.DataChanges), this.SortColumns, xml.ToString());

                        data = this.GetPaneData()[0];
                    }
                }

                this.PaneHtml = this.GetPaneHTML();
                this.GetPaneChangesHTML();  // prime the property with the current Changes HTML.
                this.StartTabIndex += 10000;
                this.LoadHeaderPane();

                if ((this.PresentationType == PresType.Tabs
                    || this.PresentationType == PresType.Wizard)
                    && this.FilteredList)
                {
                    criteriaCount = 0;

                    search.AppendFormat("PossePresentation={0}", data["Ignore_SearchPresentationname"]);

                    foreach (DictionaryEntry column in data)
                    //for (int index = 0; index < data.Fields.Count; index++)
                    {
                        if (!column.Key.ToString().StartsWith("Ignore_") && column.Key.ToString() != "objecthandle")
                        {
                            if (column.Value != null && column.Value is DateTime)
                            {
                                search.AppendFormat("&{0}={1}", column.Key.ToString(),
                                  ((DateTime)column.Value).ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                search.AppendFormat("&{0}={1}", column.Key, column.Value);
                            }

                            if (column.Value != null)
                            {
                                criteriaCount++;

                                if (column.Value is DateTime)
                                {
                                    this.SetCookie("Search:" + column.Key.ToString(), ((DateTime)column.Value).ToString("yyyy-MM-dd"));
                                }
                                else
                                {
                                    this.SetCookie("Search:" + column.Key.ToString(), column.Value.ToString());
                                }
                            }
                            else
                            {
                                this.RemoveCookie("Search:" + column.Key.ToString());
                            }
                        }
                    }

                    search.Append("&PosseShowCriteriaPane=No");

                    if (criteriaCount > 1 || searchPressed)
                    {
                        try
                        {
                            this.StartDialog(search.ToString());

                            if (!string.IsNullOrEmpty(this.SortColumns))
                            {
                                sortCriteriaExists = (this.SortColumns.Length > 2);
                            }

                            if (sortCriteriaExists)
                            {
                                this.ProcessFunction(objectId, this.PaneId, this.PaneId, RoundTripFunction.Refresh,
                                  this.GetCacheState(), this.SortColumns);
                            }

                            this.PaneHtml += this.GetPaneHTML();
                        }
                        catch (Exception exception)
                        {
                            this.ProcessError(exception);
                        }
                    }
                }
                // Note: the embedded criteria functionality was ported from the VB.NET templates and
                // they embedded the criteria before calling LoadHeaderPane.  We moved embedded criteria
                // here since we also have to deal with filtered lists and made the assumption that header
                // panes and list presentations are mutually exclusive.
                else if (this.EmbedCriteriaOnSearches)
                {
                    // If we are embedding the search criteria pane, and we are viewing a results pane
                    // (the "Search Again" function type is active), then retrieve the search criteria pane
                    // and adjust the available functions. Otherwise if this is just the initial search
                    // criteria pane, then move the PaneHtml to SearchHtml.
                    if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Refresh]))
                    {
                        this.ProcessFunction(this.ObjectId, this.CurrentPaneId,
                            Int32.Parse(this.PaneFunctions[(int)RoundTripFunction.Search]),
                            RoundTripFunction.Search, this.GetCacheState(), "");
                        this.SearchHtml = this.GetPaneHTML(this.StartTabIndex);

                        // Adjust the pane functions.  Remove the function to refine
                        // the search and add the function to perform the search.
                        this.PaneFunctions[(int)RoundTripFunction.Search] = "";
                        List<string> paneFunctions = new List<string>();
                        paneFunctions.AddRange(
                            this.OutriderNet.GetPaneFunctions().Split(new char[] { ',' }));
                        if (!string.IsNullOrEmpty(paneFunctions[(int)RoundTripFunction.PerformSearch]))
                        {
                            this.PaneFunctions[(int)RoundTripFunction.PerformSearch] = paneFunctions[(int)RoundTripFunction.PerformSearch];
                        }
                    }
                    else
                    {
                        this.SearchHtml = this.PaneHtml;
                        this.PaneHtml = "";
                    }
                }
            }
        }

        protected override void LoadPresentationCore(bool returnAfterSubmit, bool startDialogWithChanges)
        {
            int paneId, currentPaneId;
            bool autoSave;

            this.SetSiteProperty(SiteProperty.BaseUrl, this.HomePageUrl);
            this.SetSiteProperty(SiteProperty.LookupUrl, this.LookupPageUrl);
            this.SetSiteProperty(SiteProperty.LookupReturnUrl, this.LookupReturnUrl);
            this.SetSiteProperty(SiteProperty.NoteUrl, this.NotePageUrl);
            this.SetSiteProperty(SiteProperty.ReportUrl, this.ReportPageUrl);
            this.SetSiteProperty(SiteProperty.UploadUrl, this.UploadPageUrl);

            if (startDialogWithChanges)
            {
                if (!string.IsNullOrEmpty(this.PresentationId) ||
                  !string.IsNullOrEmpty(this.Presentation))
                {
                    try
                    {
                        // Eventually when the StartDialog function allows data changes to be passed
                        // through, we will update it here.
                        this.StartDialog(this.Request.QueryString.ToString());
                        this.ReleaseOutriderNet();

                        this.StartSession();

                        this.ProcessFunction(this.ObjectId, this.PaneId, this.PaneId, this.RoundTripFunction,
                            this.DataChanges, this.SortColumns, this.ChangesXml);
                        // if we're submitting to the database, clear the ChangesPending flag.
                                    if (this.RoundTripFunction == RoundTripFunction.Submit)
                                    {
                                            this.ChangesPending = "F";
                                    }
                    }
                    catch (Exception exception)
                    {
                        this.ProcessError(exception);
                    }

                    if (returnAfterSubmit)
                    {
                        this.HasPresentation = true;
                    }
                    else
                    {
                        this.CheckRedirect();
                    }
                }
            }
            else if (this.ComesFrom == "posse")
            {
                currentPaneId = int.Parse(this.Request.Form["CurrentPaneId"]);

                if (string.IsNullOrEmpty(this.Pane))
                {
                    paneId = int.Parse(this.Request.Form["PaneId"]);
                }
                else
                {
                    // TODO: Gord says its a bug that needs to be fixed in the COM.
                    this.StartDialog(this.Request.QueryString.ToString());

                    paneId = this.GetPaneIdFromName(this.Pane, currentPaneId);
                }

                this.ChangesXml = this.Request.Form["ChangesXml"] + this.ChangesXml;

                try
                {
                    this.ProcessFunction(this.ObjectId, currentPaneId, paneId,
                      this.RoundTripFunction, this.DataChanges, this.SortColumns, this.ChangesXml);

                                // if we're submitting to the database, clear the ChangesPending flag.
                                if (this.RoundTripFunction == RoundTripFunction.Submit)
                                {
                                    this.ChangesPending = "F";
                                }

                    // Check to see if the auto-save condition is set for this pane.  If so then
                    // perform a submit.
                    try
                    {
                        autoSave = this.GetCondition(this.PaneId, "AutoSave");
                    }
                    catch
                    {
                        autoSave = false;
                    }

                    if (autoSave)
                    {
                        this.ProcessFunction(null, this.PaneId, this.PaneId, RoundTripFunction.Submit,
                          this.GetCacheState(), null);
                        this.ChangesPending = "F";
                    }
                }
                catch (Exception exception)
                {
                    this.ProcessError(exception);
                }

                if (returnAfterSubmit && (this.RoundTripFunction == RoundTripFunction.Submit ||
                  this.RoundTripFunction == RoundTripFunction.Refresh || this.GetNewObjects().Count > 0))
                {
                    this.HasPresentation = true;
                }
                else
                {
                    this.CheckRedirect();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(this.PresentationId) ||
                  !string.IsNullOrEmpty(this.Presentation))
                {
                    try
                    {
                        this.StartDialog(this.Request.QueryString.ToString());
                        string xmlChangesColumn = this.Request.QueryString["PosseAppendChangesXMLColumn"];

                        if (this.AutoSubmit)
                        {
                            this.ProcessFunction(null, this.PaneId, this.PaneId,
                              RoundTripFunction.Submit, this.GetCacheState(), null);
                                            this.ChangesPending = "F";
                        }
                        else if (!string.IsNullOrEmpty(xmlChangesColumn))
                        {
                            OrderedDictionary data = this.GetPaneData(true)[0];
                            string xmlChanges = data[xmlChangesColumn].ToString();
                            if (xmlChanges.Substring(0, 7) != "<object") {
                                xmlChanges = String.Format("<object id=\"{0}\" action=\"Update\">{1}</object>",
                                                              data["objecthandle"].ToString(),
                                                              data[xmlChangesColumn].ToString());
                            }
                            this.OutriderNet.ProcessFunction("", this.OutriderNet.GetPaneId(), this.OutriderNet.GetPaneId(),
                                RoundTripFunction.Refresh, this.OutriderNet.GetCacheState(), "", xmlChanges);
                            this.ChangesPending = "T";
                        }
                    }
                    catch (Exception exception)
                    {
                        this.ProcessError(exception);
                    }

                    if (returnAfterSubmit)
                    {
                        this.HasPresentation = true;
                    }
                    else
                    {
                        this.CheckRedirect();
                    }
                }
            }

            this.LoadPane();
        }

        /// <summary>
        /// Ends the current session and redirects the user to the home page.
        /// </summary>
        protected override void Logout()
        {
            this.Logout(this.LoginPageUrl);
        }

        /// <summary>
        /// Used to create a session to login as the audit user. Will end the session once information about login attempt
        /// has been logged.
        /// </summary>
        protected virtual void TrackLogin(string authenticationName)
        {
            this.SessionId = this.OutriderNet.NewSession(this.LoginAuditUser, this.LoginAuditPass,
                          this.UserHostAddress, this.DebugKey);
            LoginAudit("Login", "Login Attempt", authenticationName, "Internal");
            this.OutriderNet.ExpireSession(this.SessionId, this.DebugKey);
            this.SessionId = this.NoSession;
        }

        /// <summary>
        /// Used to track all login attempts (valid and invalid). Information tracked include account attempting to access
        /// IP address and hostname of user attempting to access site and site user is attempting to access.
        /// Information is stored in external posse table.
        /// </summary>
        protected virtual void LoginAudit(string Event, string EventStatus, string account, string site)
        {
            string xml = "<object id=\"NEW0\" objectdef=\"o_LoginAudit\" action=\"Insert\">";
            xml += string.Format("<column name=\"Event\"><![CDATA[{0}]]></column>", Event);
            xml += string.Format("<column name=\"EventStatus\"><![CDATA[{0}]]></column>", EventStatus);
            xml += string.Format("<column name=\"IPAddress\"><![CDATA[{0}]]></column>", this.IPAddress);
            xml += string.Format("<column name=\"HostName\"><![CDATA[{0}]]></column>", this.HostName);
            xml += string.Format("<column name=\"Account\"><![CDATA[{0}]]></column>", account);
            xml += string.Format("<column name=\"Site\"><![CDATA[{0}]]></column>", site);
            xml += string.Format("</object>");
            this.ProcessXML(xml);
        }


        /// <summary>
        /// Stores the current UserId attempt
        /// </summary>
        protected string CurrentUserId = "";
        protected virtual void SetCurrentUserId(string UserId)
        {
            this.CurrentUserId = UserId;
        }

        /// <summary>
        /// Evaluates the specified exception and provides an error message or redirects the user to the login page.
        /// </summary>
        /// <param name="exception">A reference to the exception to be processed.</param>
        protected override void ProcessError(Exception exception)
        {
            int errorCode = this.GetErrorCode(exception);

            List<OrderedDictionary> data;
            string args, errorMessage;

            if (!(exception is ThreadAbortException))
            {
                if (errorCode > 0)
                {
                    switch (errorCode)
                    {
                        case 1006:  //Search error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1029:  //EditMask error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1035:  //Upload error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1045:  //Pane mandatory error.
                        case 1046:  //Pane mandatory custom error.
                        case 1066:  //Detail mandatory error.
                        case 1067:  //Job detail mandatory error. (Due to workflow)
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1052:  //Update error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1074:  //Unique index error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1075:  //Insufficient privileges error.
                        case 1277:  //AuthenticationRequired.
                            this.ShowLogin("The page which you are trying to access requires you to Sign in.");
                            break;
                        case 1278: //InvalidUserOrPassword
                            // Logs in as Audit to be able to run ExecuteSql
                            this.SessionId = this.OutriderNet.NewSession(this.LoginAuditUser, this.LoginAuditPass,
                                          this.UserHostAddress, this.DebugKey);
                            args = String.Format("ShortName={0}", this.CurrentUserId);
                            List<object> LockoutCheck = (List<object>)ExecuteSql("ABCInternalLockoutCheck", args);
                            this.OutriderNet.ExpireSession(this.SessionId, this.DebugKey);
                            this.SessionId = this.NoSession;

                            errorMessage = "<span style='text-decoration: underline'>Login Failed:</span><span style='font-weight: normal'> Please check your Email / User Name and Password and try again.<br><br>Consecutive failed login attempts will lock your account.</span>";
                            if (LockoutCheck.Count > 0)
                            {
                                //throw new Exception("Here");
                                if (((Dictionary<string, object>)LockoutCheck[0])["ISLOCKEDOUT"].ToString() == "Y")
                                {
                                    errorMessage = "<span style='text-decoration: underline'>Account Locked:</span><span style='font-weight: normal'> Consecutive failed login attempts have caused your account to be locked.</span>";
                                }
                            }
                            this.ShowLogin(errorMessage);
                            break;
                        case 1315:  //Site busy
                            this.ReleaseOutriderNet();
                            this.WriteToSiteBusyLog();
                            this.Server.Transfer("SiteBusy.html");
                            break;
                        case 1316:  //Site reloading
                            this.ReleaseOutriderNet();
                            this.Server.Transfer("SiteLoading.html");
                            break;
                        case 1436:  // BusinessError.
                            this.ErrorMessage = exception.Message;
                            break;
                        default:
                            this.ReleaseOutriderNet();
                            // This is an awkward approach. In order to pass exception information
                            // to the error page we have to ensure that the exception is trapped by the
                            // page level error handler (Page_Error) so that Server.GetLastError()
                            // will be available to the error page.  Rethrowing the same exception
                            // ensures that the exception is caught by Page_Error and then sent
                            // back to this method.
                            //
                            // Originally cookies were used to stored the error info and
                            // Response.Redirect() was called to redirect to the error page.
                            // However the error messages that Outrider produces may contain HTML
                            // and could be quite large.  Cookies will trucate anything after a
                            // semicolon, cutting off the error message.
                            //
                            // Other options include using ASP.NET session variables or putting the
                            // error messages in a hash table (indexed by the Outrider sessionid)
                            // on the Outrider COM .Net component itself.

                            if (this.Server.GetLastError() == null)
                            {
                                throw exception;
                            }

                            this.Server.Transfer(ConfigurationManager.AppSettings["ErrorPageUrl"]);
                            break;
                    }
                }
                else
                {
                    this.ReleaseOutriderNet();

                    if (this.Server.GetLastError() == null)
                    {
                        throw exception;
                    }

                    this.Server.Transfer(ConfigurationManager.AppSettings["ErrorPageUrl"]);
                }
            }

            this.Server.ClearError();
        }

        #endregion


        #region PageBaseUI

        /// <summary>
        /// Renders the tab labels and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected override void RenderTabLabelBand(WebControl container)
        {
            Panel panelContainer;
            TabLabelBase tabLabel;
            int index, tabCount;

            if (container != null)
            {
                switch (this.PresentationType)
                {
                    case PresType.Tabs:
                        tabCount = this.Panes.Count;
                        if (tabCount > 0)
                        {
                            panelContainer = new Panel();
                            panelContainer.CssClass = "tablabelband";
                            index = 0;

                            if (tabCount == 1 && string.IsNullOrEmpty(this.HeaderPaneHtml))
                            {
                                //panelContainer.Style.Add("border-bottom", "solid 1px #CCCCCC");
                            }
                            else
                            {

                                foreach (VisiblePaneInfo pane in this.Panes)
                                {
                                    Boolean outstandingFees;

                                    if(OutstandingFeesCondition != null)
                                    {
                                        // We need to read this out of the cache as we must be doing a filteredSearch
                                        outstandingFees = OutstandingFeesCondition[pane.PaneId];
                                    }
                                    else
                                    {
                                        outstandingFees = this.GetCondition(pane.PaneId, "OutstandingFees");
                                    }

                                    if(outstandingFees)
                                    {
                                        tabLabel = this.RenderWarningTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                    }
                                    else
                                    {
                                        tabLabel = this.RenderTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                    }

                                    tabLabel.Index = index;
                                    tabLabel.Count = tabCount;

                                    panelContainer.Controls.Add(tabLabel);

                                    index = index + 1;
                                }
                                if (this.JobPanes != null)
                                {
                                    foreach (VisiblePaneInfo pane in this.JobPanes)
                                    {
                                        if (this.GetCondition(pane.PaneId, "OutstandingFees"))
                                        {
                                            tabLabel = this.RenderWarningTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                        }
                                        else
                                        {
                                        tabLabel = this.RenderTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                        }
                                        tabLabel.Index = index;
                                        tabLabel.Count = tabCount;

                                        panelContainer.Controls.Add(tabLabel);

                                        index = index + 1;
                                    }
                                }
                            container.Controls.Add(panelContainer);
                            }
                        }

                        break;
                    case PresType.Wizard:

                        panelContainer = new Panel();
                        panelContainer.CssClass = "tablabelband";

                        tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.SelectedTabLabelUrl);
                        tabLabel.Selected = true;
                        tabLabel.Enabled = true;
                        tabLabel.Text = this.PaneLabel;
                        tabLabel.Index = 0;
                        tabLabel.Count = 1;

                        panelContainer.Controls.Add(tabLabel);
                        container.Controls.Add(panelContainer);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Renders the functions for the top of the page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected override void RenderTopFunctionBand(WebControl container)
        {
            HtmlTable table;
            HtmlTableRow row;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();

                if (this.PresentationType == PresType.Wizard)
                {
                    this.RenderFunctionLink(row, "Back", RoundTripFunction.Previous);
                    this.RenderFunctionLink(row, "Next", RoundTripFunction.Next);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Submit]))
                {
                    this.RenderFunctionLink(row, "Save", RoundTripFunction.Submit);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]) && this.PresentationName != "LegacyFinancialSearch")
                {
                    defaultFunction = this.RenderFunctionLink(row, "Search", RoundTripFunction.PerformSearch);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Search]))
                {
                    this.RenderFunctionLink(row, "Search Again", RoundTripFunction.Search);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]))
                {
                    if (this.SaveAsExcel)
                    {
                        string excelUrl = string.Format("{0}?{1}", this.ExcelDownloadUrl, this.Request.QueryString);
                        string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.Refresh, this.PaneId);
                        this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                    }

                    if (this.SaveAsExcelReport)
                    {
                        int excelPaneId = this.GetPaneIdFromName("Excel", this.PaneId);
                        string excelUrl = string.Format("{0}?{1}&SaveAsExcelReport=Y", this.ExcelDownloadUrl, this.Request.QueryString);
                        string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.Refresh, excelPaneId);
                        this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                    }

                    if (this.SaveAsCSVReport)
                    {
                        int excelPaneId = this.GetPaneIdFromName("Excel", this.PaneId);
                        string excelUrl = string.Format("{0}?{1}&SaveAsCSVReport=Y", this.ExcelDownloadUrl, this.Request.QueryString);
                        string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.Refresh, excelPaneId);
                        this.RenderFunctionLink(row, "Save as CSV", navigationUrl);
                    }

                    if (this.SaveAsExcelOnSearches)
                    {
                        string excelUrl = string.Format("{0}?{1}", this.ExcelDownloadUrl, this.Request.QueryString);
                    string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.PerformSearch, this.PaneId);
                        this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                    }

                    if (this.SaveAsExcelExtractOnSearches)
                    {
                        int excelPaneId = this.GetPaneIdFromName("Excel", this.PaneId);
                        string excelUrl = string.Format("{0}?{1}&SaveAsExcelReport=Y", this.ExcelDownloadUrl, this.Request.QueryString);
                        string navigationUrl = string.Format("javascript:PosseSubmitLink('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.Refresh, excelPaneId);
                        this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                    }
                }

                //if (!this.DisableCancel)
                //{
                    //this.RenderFunctionLink(row, "Cancel", this.HomePageUrl);
                //}

                table = new HtmlTable();
                table.Controls.Add(row);
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 2;

                container.Controls.Add(table);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" +
                        defaultFunction.ClientID + "')";
                }
            }
        }

        /// <summary>
        /// Renders the functions for the select objects popup page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected override void RenderSelectObjectsFunctionBand(WebControl container)
        {
            HtmlTableRow row;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();

                if (this.HasFunction(RoundTripFunction.Submit))
                {
                    this.RenderFunctionLink(row, "Select", string.Format(
                        "javascript:PosseNavigate('{0}&Action=Submit');", this.HomePageUrl));
                    this.RenderFunctionLink(row, "Clear All", string.Format(
                      "javascript:setAllCheckboxes('N');", this.HomePageUrl));
                    this.RenderFunctionLink(row, "Check All", string.Format(
                      "javascript:setAllCheckboxes('Y');", this.HomePageUrl));
                }
                defaultFunction = this.RenderFunctionLink(row, "Search", RoundTripFunction.PerformSearch);
                this.RenderFunctionLink(row, "Refine Search", RoundTripFunction.Search);

                HtmlTable table = new HtmlTable();
                table.Controls.Add(row);
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 0;

                container.Controls.Add(table);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" + defaultFunction.ClientID + "')";
                }
            }
        }

        protected virtual List<OrderedDictionary> GetExtJsToDoListData()
        {
            List<OrderedDictionary> data = this.GetPaneData(true);
            return data;
        }

        protected virtual List<OrderedDictionary> GetExtJsToDoListItemData(int objectId)
        {
            List<OrderedDictionary> data = GetData("ToDoListInfo", objectId);
            return data;
        }

        /// <summary>
        /// Returns the javascript to complete the upload process.
        /// </summary>
        /// <param name="description">The description data to be added.</param>
        /// <returns>The javascript.</returns>
        protected virtual string GetUploadCompleteScript(string description)
        {
            return this.GetUploadCompleteScript(description, false);
        }

        /// <summary>
        /// Returns the javascript to complete the upload process.
        /// </summary>
        /// <param name="description">The description data to be added.</param>
        /// <param name="autoSubmit">If true then make a call to PosseSubmit() when updating this document.</param>
        /// <returns>The javascript.</returns>
        protected virtual string GetUploadCompleteScript(string description, bool autoSubmit)
        {
            string result = null, xml = null;
            string endPoint = this.Request.QueryString["UploadEndPoint"];
            string DocumentId = this.Request.QueryString["DocumentId"];
            string newObjectId = "NEW" + DateTime.Now.Millisecond.ToString();

            if (string.IsNullOrEmpty(endPoint))
            {
                result = string.Format(
                    "opener.Posse{0}Uploaded({1}, \"{2}\", \"{3}\", \"{4}\", \"{5}\" \"{6}\");window.close();",
                    this.UniqueId, this.LastUploadDocumentId, this.LastUploadFileName,
                    this.LastUploadSize, this.LastUploadExtension, this.LastUploadContentType);
            }
            else
            {
                if (string.IsNullOrEmpty(DocumentId))
                {
                    xml = string.Format("<object id=\"{0}\" objectdef=\"{1}\" action=\"Insert\">" +
                        "<column name=\"Description\"><![CDATA[{2}]]></column>" +
                        "<column name=\"FileName\"><![CDATA[{3}]]></column>" +
                        "<column name=\"FileSize\">{4}</column>" +
                        "<column name=\"CreatedBy\"><![CDATA[{5}]]></column>" +
                        "<pendingdocument id=\"{6}\"/></object>" +
                        "<object id=\"{7}\"><relationship endpoint=\"{8}\" toobjectid=\"{0}\" action=\"Insert\">" +
                        "</relationship></object>",
                        newObjectId, this.Request.QueryString["UploadObjectDef"], description,
                        this.LastUploadFileName, this.LastUploadSize, this.LastUploadCreatedBy,
                        this.LastUploadDocumentId, this.ObjectId, endPoint);
                }
                else
                {
                    xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                        "<column name=\"Description\"><![CDATA[{1}]]></column>" +
                        "<column name=\"FileName\"><![CDATA[{2}]]></column>" +
                        "<column name=\"FileSize\">{4}</column>" +
                        "<column name=\"CreatedBy\"><![CDATA[{3}]]></column>" +
                        "<pendingdocument id=\"{5}\"/></object>",
                        DocumentId, description,
                        this.LastUploadFileName, this.LastUploadSize, this.LastUploadCreatedBy,
                        this.LastUploadDocumentId);
                }

                // First encode the xml as a javascript string literal to ensure that special characters such as double
                // quotes do not break the script.
                if (autoSubmit)
                {
                    result = string.Format("opener.PosseAppendChangesXML({0});opener.PosseSubmit(null);window.close();",
                        this.EncodeJSString(xml));
                }
                else
                {
                    result = string.Format("opener.PosseAppendChangesXML({0});opener.PosseNavigate(null);window.close();",
                        this.EncodeJSString(xml));
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the javascript to complete the upload process - NOT in a popup, but inline
        /// </summary>
        /// <param name="description">The description data to be added.</param>
        /// <returns>The javascript.</returns>
        protected virtual string GetUploadInlineCompleteScript(string description)
        {
            return this.GetUploadCompleteScript(description, false);
        }

        /// <summary>
        /// Returns the javascript to complete the upload process - NOT in a popup, but inline
        /// </summary>
        /// <param name="description">The description data to be added.</param>
        /// <param name="autoSubmit">If true then make a call to PosseSubmit() when updating this document.</param>
        /// <returns>The javascript.</returns>
        protected virtual string GetUploadInlineCompleteScript(string description, bool autoSubmit)
        {
            string result = null, xml = null;
            string endPoint = this.Request.QueryString["UploadEndPoint"];
            string DocumentId = this.Request.QueryString["DocumentId"];
            string newObjectId = "NEW" + DateTime.Now.Millisecond.ToString();
            string fromObjectId = this.Request.QueryString["PosseObjectId"];
            string fromPresName = this.Request.QueryString["FromPresName"];
            string fromPaneName = this.Request.QueryString["FromPaneName"];

            if (string.IsNullOrEmpty(endPoint))
            {
                result = string.Format(
                    "opener.Posse{0}Uploaded({1}, \"{2}\", \"{3}\", \"{4}\", \"{5}\" \"{6}\");window.close();",
                    this.UniqueId, this.LastUploadDocumentId, this.LastUploadFileName,
                    this.LastUploadSize, this.LastUploadExtension, this.LastUploadContentType);
            }
            else
            {
                if (string.IsNullOrEmpty(DocumentId))
                {
                    xml = string.Format("<object id=\"{0}\" objectdef=\"{1}\" action=\"Insert\">" +
                        "<column name=\"Description\"><![CDATA[{2}]]></column>" +
                        "<column name=\"FileName\"><![CDATA[{3}]]></column>" +
                        "<column name=\"FileSize\">{4}</column>" +
                        "<column name=\"CreatedBy\"><![CDATA[{5}]]></column>" +
                        "<pendingdocument id=\"{6}\"/></object>" +
                        "<object id=\"{7}\"><relationship endpoint=\"{8}\" toobjectid=\"{0}\" action=\"Insert\">" +
                        "</relationship></object>",
                        newObjectId, this.Request.QueryString["UploadObjectDef"], description,
                        this.LastUploadFileName, this.LastUploadSize, this.LastUploadCreatedBy,
                        this.LastUploadDocumentId, this.ObjectId, endPoint);
                }
                else
                {
                    xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                        "<column name=\"Description\"><![CDATA[{1}]]></column>" +
                        "<column name=\"FileName\"><![CDATA[{2}]]></column>" +
                        "<column name=\"FileSize\">{4}</column>" +
                        "<column name=\"CreatedBy\"><![CDATA[{3}]]></column>" +
                        "<pendingdocument id=\"{5}\"/></object>",
                        DocumentId, description,
                        this.LastUploadFileName, this.LastUploadSize, this.LastUploadCreatedBy,
                        this.LastUploadDocumentId);
                }

                this.ProcessXML(xml.ToString());
                this.Response.Redirect(string.Format("Default.aspx?PosseObjectId={0}&PossePresentation={1}&PossePane={2}", fromObjectId, fromPresName, fromPaneName));
            }
            return result;
        }

        /// <summary>
        /// Displays the given text in the title band.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="text">Title text to be displayed.</param>
        protected override void RenderTitle(WebControl container, string text)
        {
            Label label;
            Image img;
            Panel panel;
            string iconImageName = null;
            OrderedDictionary paneData;


            if (container != null)
            {
                panel = new Panel();

                try
                {

                    if (this.HasPresentation)
                    {
                        string PresentationKey = Convert.ToString(this.PaneId);

                        if (!this.IconImages.ContainsKey(PresentationKey)) // Find the Icon Name if it is not already stored in memory
                        {
                            paneData = this.GetPaneData(true)[0];

                            if (Request.QueryString["IconName"] != null)
                            {
                                iconImageName = Request.QueryString["IconName"];
                                this.IconImages[PresentationKey] = iconImageName;
                            }
                            else if (paneData.Contains("ImageName"))
                            {

                                string imageName = paneData["ImageName"].ToString();
                                if (imageName != null)
                                    iconImageName = imageName;
                            }
                        }


                        if (PresentationKey != null && (iconImageName != null || this.IconImages.ContainsKey(PresentationKey)))
                        {
                            img = new Image();
                            if (this.IconImages.ContainsKey(PresentationKey))
                            {
			                    img.ImageUrl = IconPath + this.IconImages[PresentationKey];
                            }
                            else
                            {
			                    img.ImageUrl = IconPath + iconImageName;
                            }
                            panel.Controls.Add(img);
                        }
                    }
                }
                catch (Exception exception)
                {
			     }



                label = new Label();
                label.Text = text;
                panel.Controls.Add(label);

                container.Controls.Add(panel);
            }
        }

        /// <summary>
        /// Renders the menu pane.
        /// </summary>
        /// <param name="titleContainer">Container to render the menu title into.</param>
        /// <param name="container">Container to render the menu into.</param>
        /// <param name="menuName">The name of the menu to render.</param>
        protected override void RenderMenuPane(WebControl container, WebControl titleContainer, WebControl imageContainer)
        {
            int index = 0, colSpan = 0, groupIndex;
            string link, url, singleEntrypointURL = "";
            Label label;
            Literal literal;
            Panel panel;
            MenuInfo menu, currentMenu;
            HtmlTable table = new HtmlTable();
            HtmlTableRow row = null, groupRow = null;
            HtmlTableCell newCell;
            List<HtmlTableCell> cells = new List<HtmlTableCell>();

            if (container != null)
            {
                LoadMenu(this.MenuName);
                menu = this.Menus[this.MenuName];

                // Build the breadcrumb
                literal = new Literal();
                literal.Text = menu.Label;
                if (menu.ParentMenu != null)
                {
                    currentMenu = menu;
                    while (currentMenu.ParentMenu != null && currentMenu.ParentMenu.Name != this.MainMenuName)
                    {
                        link = String.Format("<a class=\"breadcrumb\" href=\"{0}?PosseMenuName={1}\">{2}</a>",
                            this.HomePageUrl, currentMenu.ParentMenu.Name, currentMenu.ParentMenu.Label);
                        literal.Text = String.Format("{0}&nbsp;&gt;&nbsp;{1}", link, literal.Text);
                        currentMenu = currentMenu.ParentMenu;
                    }
                }
                link = String.Format("<a class=\"breadcrumb\" href=\"{0}\">Home</a>", this.HomePageUrl);
                literal.Text = String.Format("<div class=\"breadcrumb\">{0}&nbsp;&gt;&nbsp;{1}</div>", link, literal.Text);
                titleContainer.Controls.Add(literal);

                // Add the title and image if necessary
                if (menu.Label != null)
                {
                    this.RenderTitle(titleContainer, menu.Label);
                }

                if (this.PaneMenus.ContainsKey(this.MenuName))
                {
                    container.Controls.Add(this.PaneMenus[this.MenuName]);
                }
                else
                {
                    foreach (MenuEntryPointGroup group in menu.EntryPointGroups)
                    {
                        if (row != null)
                        {
                            foreach (HtmlTableCell cell in cells)
                            {
                                cell.VAlign = "Top";
                                literal = new Literal();
                                literal.Text = "</ul>";
                                cell.Controls.Add(literal);

                                row.Cells.Add(cell);
                            }
                        }

                        // Render the group if there are entrypoints visible
                        if (group.EntryPointCount > 0)
                        {
                            if (group.SameRow && cells.Count > 0)
                            {
                                cells.Add(new HtmlTableCell());
                                index = cells.Count - 1;
                            }
                            else
                            {
                                cells.Clear();
                                cells.Add(new HtmlTableCell());
                                index = 0;
                                groupRow = new HtmlTableRow();
                                table.Rows.Add(groupRow);
                                row = new HtmlTableRow();
                                table.Rows.Add(row);
                            }

                            if (group.ColSpan != null)
                                colSpan = group.ColSpan;
                            else
                                colSpan = 0;
                            if (group.MultiColumn)
                            {
                                for (int colIndex = 1; colIndex < colSpan; colIndex++)
                                {
                                    newCell = new HtmlTableCell();

                                    literal = new Literal();
                                    literal.Text = "<ul class=\"menupanelist\">";
                                    newCell.Controls.Add(literal);

                                    cells.Add(newCell);
                                }
                            }
                            else
                            {
                                cells[index].ColSpan = colSpan;
                            }

                            groupIndex = index;
                            this.RenderMenuPaneHeader(group, table, groupRow);

                            literal = new Literal();
                            literal.Text = "<ul class=\"menupanelist\">";
                            cells[index].Controls.Add(literal);

                            // Generate entry point links
                            foreach (MenuEntryPoint item in group.EntryPoints)
                            {
                                if (item.SubMenu != null && item.SubMenu.EntryPointCount > 0)
                                    url = String.Format("{0}?PosseMenuName={1}", this.HomePageUrl, item.SubMenu.Name);
                                else
                                    url = item.Url;

                                //if we only have one Entry point and it's the To-Do-List, we automatically navigate to it
                                if (group.EntryPointCount == 1)
                                {
                                    if (group.Name == "ToDoList")
                                    {
                                        singleEntrypointURL = url;
                                        Response.Redirect(singleEntrypointURL);
                                    }
                                }

                                this.RenderMenuPaneItem(cells[index], url, item.Help, item.Label, (item.SubMenu != null));
                                if (group.MultiColumn)
                                {
                                    if (index < groupIndex + colSpan - 1)
                                        index++;
                                    else
                                        index = groupIndex;
                                }
                            }
                        }

                        foreach (HtmlTableCell cell in cells)
                        {
                            cell.VAlign = "Top";
                            literal = new Literal();
                            literal.Text = "</ul>";
                            cell.Controls.Add(literal);

                            row.Cells.Add(cell);
                        }

                        if (row == null)
                        {
                            label = new Label();
                            label.Text = "No options available";
                            panel = new Panel();
                            panel.Controls.Add(label);

                            this.PaneMenus[this.MenuName] = (Control)panel;
                            container.Controls.Add(panel);
                        }
                        else
                        {
                            table.Rows.Add(row);

                            this.PaneMenus[this.MenuName] = (Control)table;
                            container.Controls.Add(table);
                        }
                    }
                }
            }
        }

	    #endregion



        #region Data Members
        private IServiceApi serviceApi = null;
        /// <summary>
        /// Provides a reference to the ServiceApi interface.
        /// </summary>
        protected virtual IServiceApi ServiceApi
        {
            get
            {
                if (this.serviceApi == null)
                {
                    //Check for web config values
                    var serverName = System.Configuration.ConfigurationManager.AppSettings["ServiceApiServerName"];
                    var serverPort = System.Configuration.ConfigurationManager.AppSettings["ServiceApiServerPort"];
                    var serverTimeout = System.Configuration.ConfigurationManager.AppSettings["ServiceApiTimeout"];

                    if (String.IsNullOrEmpty(serverName))
                        throw new Exception("Cannot load parameter named 'ServiceApiServerName' from web.config!");
                    if (String.IsNullOrEmpty(serverPort))
                        throw new Exception("Cannot load parameter named 'ServiceApiServerPort' from web.config!");
                    if (String.IsNullOrEmpty(serverTimeout))
                        throw new Exception("Cannot load parameter named 'ServiceApiTimeout' from web.config!");

                    if (!String.IsNullOrEmpty(serverName) && !String.IsNullOrEmpty(serverPort) && !String.IsNullOrEmpty(serverTimeout))
                    {
                        //var client = new Computronix.POSSE.AppServerClient.AppServerClient(serverName, serverPort, Convert.ToInt32(serverTimeout));
                        //this.serviceApi = (IServiceApi)client;
                        this.serviceApi = ServiceApiFactory.GetServiceApi(serverName, Convert.ToInt32(serverPort), Convert.ToInt32(serverTimeout));
                    }
                }
                return this.serviceApi;
            }
            set
            {
                this.serviceApi = value;
            }
        }

        #endregion

    }
}
