﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Serialization;

namespace Computronix.POSSE.Outrider
{
	/// <summary>
	/// Base class for all service handlers.
	/// </summary>
	public abstract class HandlerBase : IHttpHandler
	{
		#region Data Members
        /// <summary>
        /// Value used to represent the absence of a session in this.SessionId.
        /// </summary>
        protected readonly string NoSession = "-1";

        private IOutriderNet2 outriderNet = null;
        /// <summary>
        /// Provides a reference to the OutriderNet interface.
        /// </summary>
        protected virtual IOutriderNet2 OutriderNet
        {
            get
            {
                if (this.outriderNet == null)
                {
                    this.outriderNet = new OutriderNet();
                }

                return this.outriderNet;
            }
            set
            {
                this.outriderNet = value;
            }
        }

        protected string this[string key]
		{
			get
			{
				return this.Request[key];
			}
		}

		protected HttpContext Context
		{
			get;
			private set;
		}

		protected HttpRequest Request
		{
			get;
			private set;
		}

		protected HttpResponse Response
		{
			get;
			private set;
		}

		private JavaScriptSerializer javaScriptSerializer = null;
		protected virtual JavaScriptSerializer JavaScriptSerializer
		{
			get
			{
				if (this.javaScriptSerializer == null)
				{
					this.javaScriptSerializer = new JavaScriptSerializer();
				}

				return this.javaScriptSerializer;
			}
		}

        private string _debugkey = null;
        /// <summary>
        /// Provides access to the current debug key.
        /// </summary>
        protected virtual string DebugKey
        {
            get
            {
                this._debugkey = this.GetCookie("PosseDebugKey");

                return this._debugkey;
            }
            set
            {
                this._debugkey = value;
                this.SetCookie("PosseDebugKey", value);
            }
        }

        /// <summary>
        /// Provides access to the virtual path for all cookies.  (AppSettings["CookiePath"])
        /// </summary>
        protected virtual string CookiePath
        {
			get
			{
				return ConfigurationManager.AppSettings["CookiePath"];
			}
        }

        /// <summary>
        /// The number of minutes the menu cache should expire in.
        /// </summary>
        protected virtual int CacheExpiration
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["CacheExpiration"]);
            }
        }

        /// <summary>
        /// Provides access to the IP address of the calling browser
        /// </summary>
        protected virtual string UserHostAddress
        {
            get
            {
                string result = this.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(result))
                {
                    result = this.Request.UserHostAddress.Substring(0, Math.Min(this.Request.UserHostAddress.Length, 15));
                }

                return result;
            }
        }
        #endregion

		#region Constructors
		public HandlerBase()
		{
		}
		#endregion

		#region Methods
        /// <summary>
        /// Sets the specified cookie value.
        /// </summary>
        /// <param name="name">The name of the value.</param>
        /// <param name="value">The value being set.</param>
        protected virtual void SetCookie(string name, string value)
        {
            HttpCookie cookie = this.Response.Cookies[name];

            cookie.Value = value;
            cookie.Path = this.CookiePath;
        }


        /// <summary>
        /// Returns the specified cookie value.
        /// </summary>
        /// <param name="name">The name of the value to retrieve.</param>
        /// <returns>The specified value otherwise null.</returns>
        protected virtual string GetCookie(string name)
        {
            string result = null;
            HttpCookie cookie;

            if (((IList)this.Response.Cookies.AllKeys).Contains(name))
            {
                cookie = this.Response.Cookies[name];
                result = cookie.Value;
            }
            else
            {
                cookie = this.Request.Cookies[name];

                if (cookie != null)
                {
                    result = cookie.Value;
                }
            }

            return result;
        }


        /// <summary>
        /// Removes the specified cookie value.
        /// </summary>
        /// <param name="name">The name of the value to remove.</param>
        protected virtual void RemoveCookie(string name)
        {
            if (this.Request.Cookies[name] != null)
            {
                this.Request.Cookies.Remove(name);
                this.Response.Cookies[name].Expires = DateTime.MinValue;
                this.Response.Cookies[name].Path = this.CookiePath;
            }
        }

        private bool sessionRequired = true;
        /// <summary>
        /// Indicates if the current page requires an active session.
        /// </summary>
        protected virtual bool SessionRequired
        {
            get
            {
                return this.sessionRequired;
            }
            set
            {
                this.sessionRequired = value;
            }
        }

        /// <summary>
        /// Provides access to the name of the session id cookie value.  (AppSettings["SessionIdName"])
        /// </summary>
        protected virtual string SessionIdName
        {
            get
            {
                return ConfigurationManager.AppSettings["SessionIdName"];
            }
        }

                    
        private string _sessionId;

        /// <summary>
        /// Provides access to the id for the current session.
        /// </summary>
        protected virtual string SessionId
        {
            get
            {
                if (this._sessionId == this.NoSession)
                {
                    // See if there is a session cookie out there
                    string sessionCookie = this.GetCookie(this.SessionIdName);
                    if (!string.IsNullOrEmpty(sessionCookie))
                    {
                        this._sessionId = sessionCookie;
                    }
                }
                return this._sessionId;
            }
            set
            {
                this._sessionId = value;

                this.SetCookie(this.SessionIdName, this._sessionId.ToString());
            }
        }

        /// <summary>
        /// Establishes a session.
        /// </summary>
        protected virtual void StartSession()
        {
            //this.isGuestNull = true;
            //this.userInformation = null;
            //this.siteNavigation = null;

            if (this.SessionId == this.NoSession)
            {
                this.SessionId = this.OutriderNet.NewSession(
                    this.UserHostAddress, this.DebugKey);
            }
            else
            {
                if (!this.AttachSession())
                {
                    this.SessionId = this.NoSession;
                    throw new Exception("Session expired.");
                }
            }
        }

        /// <summary>
        /// Attaches to an existing session.
        /// </summary>
        /// <returns>True, if the session is valid and has not timed out; False otherwise.</returns>
        protected virtual bool AttachSession()
        {
            //this.isGuestNull = true;
            //this.userInformation = null;
            //this.siteNavigation = null;

            return this.OutriderNet.AttachSession(this.SessionId, this.DebugKey);
        }

        /// <summary>
        /// Releases the reference to the outrider net inteface.
        /// </summary>
        protected virtual void ReleaseOutriderNet()
        {
            this.OutriderNet.Dispose();
            this.outriderNet = null;
        }

        /// <summary>
        /// Provides access to the cached ExecuteSql object information
        /// </summary>
        protected virtual Dictionary<string, string> PosseUtilitiesInfo
        {
            get
            {
                Dictionary<string, string> result = (Dictionary<string, string>)this.Context.Cache["PosseUtilitiesInfo"];

                if (result == null)
                {
                    this.OutriderNet.StartDialog("PossePresentation=GetPosseUtilitiesObjectId");
                    OrderedDictionary data = this.OutriderNet.GetPaneRecordSet()[0];

                    result = new Dictionary<string, string>();
                    result["ObjectId"] = data["ObjectId"].ToString();
                    this.OutriderNet.StartDialog(String.Format("PossePresentation=GetPaneId&PosseObjectId={0}",
                        result["ObjectId"]));
                    result["PaneId"] = this.OutriderNet.GetPaneId().ToString();

                    this.Context.Cache.Add("PosseUtilitiesInfo", result, null, Cache.NoAbsoluteExpiration,
                        new TimeSpan(0, this.CacheExpiration, 0), CacheItemPriority.Default, null);
                }

                return result;
            }
        }

        /// <summary>
        /// Execute a registered statment (no arguments)
        /// </summary>
        /// <param name="statementName"></param>
        /// <returns></returns>
        protected object ExecuteSql(String statementName)
        {
            return this.ExecuteSql(statementName, null);
        }

        /// <summary>
        /// Execute a registered statment (with arguments)
        /// </summary>
        protected object ExecuteSql(String statementName, String args)
        {
            int paneId;
            List<OrderedDictionary> data;
            string objectId, returnValue;
            StringBuilder xml;
            object result;

            objectId = this.PosseUtilitiesInfo["ObjectId"];
            paneId = Int32.Parse(this.PosseUtilitiesInfo["PaneId"]);

            xml = new StringBuilder();
            xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", objectId);
            xml.AppendFormat("<column name=\"StatementName\">{0}</column>", statementName);
            if (!String.IsNullOrEmpty(args))
            {
                xml.AppendFormat("<column name=\"Arguments\">{0}</column>",
                    HttpUtility.HtmlEncode(args));
            }
            xml.Append("</object>");
            
            this.OutriderNet.ProcessFunction(objectId, paneId, paneId, RoundTripFunction.Refresh,
                "", "", xml.ToString());

            //this.OutriderNet.LogMessage(LogLevel.Debug, "About to call GetRecordSet");
            data = this.OutriderNet.GetRecordSet("Results", Int32.Parse(objectId));
            //this.OutriderNet.LogMessage(LogLevel.Debug, "Done call to GetRecordSet");

            returnValue = data[0][0].ToString();

            //Below is a temp workaround to a .NET 4.0 bug in GetRecordSet().  
            //This reverses the python replacement that happens in "Results" on the PosseUtilties object.
            //returnValue = returnValue.Replace("@@", "\\\"").Replace("@@@", "\\"); 
            
            //throw new Exception(returnValue);
            result = cxJSON.JSON.Instance.Parse(returnValue);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
		{
			string responseText = null;

			try
			{
				this.Context = context;
				this.Request = context.Request;
				this.Response = context.Response;


                // Start the Outrider Session
                this._sessionId = this.NoSession;
                if (this.SessionRequired)
                {
                    this.StartSession();
                }

				this.Response.Clear();
				this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
				this.Response.Cache.SetExpires(DateTime.Today.AddDays(-1));
				this.Response.ContentType = "application/json";

				this.ProcessRequest();
                this.ReleaseOutriderNet();
			}
			// If the error was a web request then use the response from the web exception.
			catch (WebException exception)
			{
                this.ReleaseOutriderNet();
				this.Response.Clear();
				this.Response.StatusCode = 500;
                this.Response.TrySkipIisCustomErrors = true;

				if (exception.Response == null)
				{
					this.Log("{0}\r\n{1}", exception.Message, HandlerBase.Write(exception));
					this.Response.Write(string.Format("{{StatusCode: {0}, Message: '{1}'}}", this.Response.StatusCode,
						HandlerBase.EscapeJson(exception.Message)));
				}
				else
				{
					responseText = HandlerBase.GetResponseText(exception.Response);

					this.Log("{0}\r\n{1}\r\n{2}", exception.Message, responseText,
						HandlerBase.Write(exception));
					this.Response.Write(responseText);
				}

				this.Response.End();
			}
			catch (Exception exception)
			{
                this.ReleaseOutriderNet();
				this.Log("{0}\r\n{1}", exception.Message, HandlerBase.Write(exception));
				this.Response.Clear();
				this.Response.StatusCode = 500;
                this.Response.TrySkipIisCustomErrors = true;
                this.Response.Write(string.Format("{{StatusCode: {0}, Message: '{1}'}}", this.Response.StatusCode,
					HandlerBase.EscapeJson(exception.Message)));
				this.Response.End();
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

		public abstract void ProcessRequest();
	
		/// <summary>
		/// Logs a message to the debug output window.
		/// </summary>
		/// <param name="message">Message text or message format.</param>
		/// <param name="args">Argument values for message format.</param>
		public virtual void Log(object message, params object[] args)
		{
			if (args.Length > 0)
			{
				System.Diagnostics.Debug.WriteLine(string.Format(message.ToString(), args));
			}
			else
			{
				System.Diagnostics.Debug.WriteLine(message);
			}
		}

		/// <summary>
		/// Returns the response text as a string from the specified response.
		/// </summary>
		/// <param name="response">Reference to a web response.</param>
		/// <returns>The response text as a string.</returns>
		public static string GetResponseText(WebResponse response)
		{
			Stream responseStream = response.GetResponseStream();
			string result = null;

			if (responseStream.CanRead)
			{
				result = new StreamReader(responseStream).ReadToEnd();
			}

			responseStream.Close();

			return result;
		}

		/// <summary>
		/// Executes the specified uri and returns the response text as a string.
		/// </summary>
		/// <param name="uri">The uri to execute.</param>
		/// <returns>The response text as a string.</returns>
		public static string GetResponseText(string uri)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
			return HandlerBase.GetResponseText(request.GetResponse());
		}

		/// <summary>
		/// Executes the specified uri and returns the response as a text file.
		/// </summary>
		/// <param name="uri">The uri to execute.</param>
		/// <returns>The response as a text file.</returns>
		public virtual string GetResponseTextFile(string uri)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
			WebResponse response = request.GetResponse();

			this.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.txt", this["fileName"]));
			this.Response.AddHeader("Content-Length", response.ContentLength.ToString());
			this.Response.ContentType = response.ContentType;

			return HandlerBase.GetResponseText(response);

		}

		/// <summary>
		/// Writes the specified exception as a string.
		/// </summary>
		/// <param name="exception">A reference to the exception being written.</param>
		/// <returns>The exception as a string.</returns>
		public static string Write(Exception exception)
		{
			string label = "Exception", format = "{0}={1}\r\n";
			StringBuilder result = new StringBuilder();
			ArrayList properties = new ArrayList();
			object value = null;

			while (exception != null)
			{
				properties.Clear();
				properties.AddRange(exception.GetType().GetProperties(
					BindingFlags.Public |
					BindingFlags.Instance |
					BindingFlags.FlattenHierarchy));

				result.AppendFormat(format, label, exception.GetType().FullName);

				if (exception.Data != null)
				{
					foreach (object key in exception.Data.Keys)
					{
						result.AppendFormat(format, "Data[" + key.ToString() + "]", exception.Data[key]);
					}
				}

				foreach (PropertyInfo property in properties)
				{
					if (property.Name != "InnerException" && property.Name != "Data")
					{
						value = HandlerBase.GetPropertyValue(exception, property.Name);

						if (value != null)
						{
							result.AppendFormat(format, property.Name, value);
						}
					}
				}

				exception = exception.InnerException;
				label = "Inner Exception";
			}

			return result.ToString();
		}

		/// <summary>
		/// Returns the current value of the specified property of an object.
		/// </summary>
		/// <param name="target">The object from which to get the property value.</param>
		/// <param name="name">The name of the property value to return.</param>
		/// <returns>The current value of the property.</returns>
		protected static object GetPropertyValue(object target, string name)
		{
			return HandlerBase.InvokeMember(target, name, BindingFlags.GetProperty, new object[] { });
		}

		/// <summary>
		/// Invokes the given member of an object, passing arguments.
		/// </summary>
		/// <param name="target">The object on which to invoke the member.</param>
		/// <param name="name">The name of the member to invoke.</param>
		/// <param name="binding">Indicates the type of binding to perform.</param>
		/// <param name="args">Arguments to be passed to the member.</param>
		/// <returns>The return value of the invoked member.</returns>
		protected static object InvokeMember(object target, string name, BindingFlags binding, object[] args)
		{
			object result;

			try
			{
				if (target is Type)
				{
					result = ((Type)target).InvokeMember(name,
						binding | BindingFlags.FlattenHierarchy | BindingFlags.Static | BindingFlags.Public,
						null, null, args);
				}
				else
				{
					result = target.GetType().InvokeMember(name, binding, null, target, args);
				}
			}
			catch (MissingMemberException exception)
			{
                //throw new InvokeMemberException(target, name, args, exception);
                throw exception;
			}

			return result;
		}

		/// <summary>
		/// Determines whether or not the given string can be considered true.
		/// </summary>
		/// <param name="value">The value that is to be interpreted as either true or false.</param>
		/// <returns>True if the lower case of <paramref name="value" /> is considered "true", otherwise false.</returns>
		public static bool IsTrue(string value)
		{
			bool result = false;
			double numericValue;

			if (!string.IsNullOrEmpty(value))
			{
				value = value.ToLower();
				result = ((value == "yes") || (value == "y") || (value == "true") ||
					(value == "t") || (value == "on") || (value == "ok") || (value == "1"));

				if (!result)
				{
					if (double.TryParse(value, out numericValue))
					{
						result = (numericValue > 0);
					}
				}
			}

			return result;
		}

		/// <summary>
		/// Escapes the specified json string.
		/// </summary>
		/// <param name="json">The json string to escape.</param>
		/// <returns>The escaped json string.</returns>
		public static string EscapeJson(string json)
		{
			return json.Replace("'", "\\'");
		}
		#endregion
	}
}