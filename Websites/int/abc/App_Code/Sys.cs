using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Computronix.RTFReporter;

/// <summary>
/// Summary description for Sys
/// </summary>
public class Sys : Computronix.CXTools.SuperClass.SysBase
{
    private static InvalidTokenHandlingType invalidTokenHandling;
    private static bool saveCopyOfRTF;
    private static bool saveCopyOfRTFLoaded;
    private static string documentRelName;
    private static bool documentRelNameLoaded;

    /// <summary>
    /// Define how invalid tokens (ones which appear in RTF but not in SQL) are handled:
    ///   Null:   Replace token with empty string
    ///   Ignore: Leave token as is
    ///   Flag:   Replace token with {<Token>_INVALID}
    ///   Error:  Throw an error
    /// </summary>
    public static InvalidTokenHandlingType InvalidTokenHandling
    {
        get
        {
            if (invalidTokenHandling == InvalidTokenHandlingType.Undefined)
            {
                switch (Sys.ConfigManager["InvalidTokenHandling"].ToString().ToLower())
                {
                    case "null":
                        invalidTokenHandling = InvalidTokenHandlingType.Null;
                        break;

                    case "ignore":
                        invalidTokenHandling = InvalidTokenHandlingType.Ignore;
                        break;

                    case "flag":
                        invalidTokenHandling = InvalidTokenHandlingType.Flag;
                        break;

                    default:
                        invalidTokenHandling = InvalidTokenHandlingType.Error;
                        break;
                }
            }
            return invalidTokenHandling;
        }
        set
        {
            invalidTokenHandling = value;
        }
    }

    /// <summary>
    /// If true, save a copy of the RTF just before streaming it to the browser
    /// </summary>
    public static bool SaveCopyOfRTF
    {
        get
        {
            if (saveCopyOfRTFLoaded)
            {
                return saveCopyOfRTF;
            }
            else
            {
                try
                {
                    if (Sys.IsTrue(Sys.ConfigManager["SaveCopyOfRTF"].ToString()))
                        saveCopyOfRTF = true;
                    else
                        saveCopyOfRTF = false;
                }
                catch (Exception)
                {
                    saveCopyOfRTF = false;
                }
                saveCopyOfRTFLoaded = true;
            }
            return saveCopyOfRTF;
        }
    }

    /// <summary>
    /// Relationship Name of document relationship used to store report.
    /// If null, document is not stored in POSSE.
    /// </summary>
    public static string DocumentRelName
    {
        get
        {
            if (documentRelNameLoaded)
            {
                return documentRelName;
            }
            else
            {
                try
                {
                    documentRelName = Sys.ConfigManager["DocumentRelName"].ToString();
                }
                catch (Exception)
                {
                    documentRelName = "";
                }
                documentRelNameLoaded = true;
            }
            return documentRelName; 
        }
    }


    private Sys()
    {
    }

    static Sys()
    {
        saveCopyOfRTFLoaded = false;
        documentRelNameLoaded = false;
        Sys.TraceManagerClassName = "Computronix.cxTraceManager";
    }

}
