import simplejson as json
import base64
import binascii
import urllib2
import urllib
import zlib


def Get(sessionId, documentKey, webServiceAddress):

    try:
        #Get the document
        arguments = urllib.urlencode({'action':'getBinary', 'sessionId':sessionId, 'documentKey': documentKey})
        request = urllib2.Request(url=webServiceAddress, data=arguments)
        response = urllib2.urlopen(request)

        x = response.read()
        
        try:
            file = zlib.decompress(x)
        except:
            file = x

        return file

    except urllib2.HTTPError, error:
        raise error


def Set(sessionId, documentKey, document, webServiceAddress):
    try:
        #Set the document
        if documentKey is not None:
            arguments = str('action=setBinary&sessionId=%s&document=%s&documentKey=%s'%(sessionId, document, documentKey))
        else:
            arguments = str('action=setBinary&sessionId=%s&document=%s'%(sessionId, document))
        response = urllib2.urlopen(url=webServiceAddress, data=arguments)

        jsonObject = json.loads(response.read())
        documentKey = jsonObject['documentKey']
        return documentKey
    except urllib2.HTTPError, error:
        raise error


def Remove(sessionId, documentKey, webServiceAddress):

    try:
        #Get the document
        arguments = urllib.urlencode({'action':'delete', 'sessionId':sessionId, 'documentKey': documentKey})
        request = urllib2.Request(url=webServiceAddress, data=arguments)
        response = urllib2.urlopen(request)

    except urllib2.HTTPError, error:
        raise error
