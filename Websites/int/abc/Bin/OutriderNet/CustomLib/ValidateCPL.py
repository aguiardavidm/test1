""" 
Client: New Jersey
Purpose: XLSX data-type validation and processing for CPL Sheets 
Developer: Michael F

High-level description:
  Given a model of acceptable row datatypes, test each row in the CPL sheet of the XLSX file handed to the script
  A limited number of errors will be tolerated before processing will cease so as not to overwhelm the display

Columns expected
SKU,CustomField2,Wholesaler,Submission Date,From Date,To Date,Brand Reg#,DESCRIPTION,PROOF,Unit Type,Unit Quantity,Unit Volume Type,Unit Volume Amt,Pack,Sleeve,Copack Y/N,List Price Case,List Price Bottle,Best Case $,Best Bottle $,Split Chg,Level 1 Qty,Level 1 Disc,Level 2 Qty,Level 2 Disc,Level 3 Qty,Level 3 Disc,Level 4 Qty,Level 4 Disc,Level 5 Qty,Level 5 Disc,Level 6 Qty,Level 6 Disc

TODO:  Add RIP following ComboPack

This mechanism takes a number of seconds to perform the validation.  If this becomes unbearable at some point, an AJAX call in JS to populate a field in the CPL doc grid might be the right thing to do.

"""

import xlrd
import cx_Logging
from datetime import datetime

#Constants
ROW_ERROR_THRESHOLD = 10 # limits number of rows in error before processing stops and a report is generated
NUM_COLS = 33  # expected number of columns

#DateTypes to English
type_text = {
	xlrd.XL_CELL_EMPTY  : 'empty',
	xlrd.XL_CELL_TEXT   : 'text',
	xlrd.XL_CELL_NUMBER : 'number',
	xlrd.XL_CELL_DATE   : 'date',
	xlrd.XL_CELL_BOOLEAN: 'bool',
	xlrd.XL_CELL_ERROR  : 'error',
	xlrd.XL_CELL_BLANK  : 'blank'
	}
	
#Map columns to specific types
#0,1 could be text or number... we'll not test this in this manner

#Define Dict in reverse to make it more intuitive, then invert it
ws_type_dict = {  xlrd.XL_CELL_DATE:   [3, 4, 5],
				  xlrd.XL_CELL_NUMBER: [6, 8, 10, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32],
				  xlrd.XL_CELL_TEXT:   [2, 7, 9, 11, 15]
			   }
#now invert dictionary
ws_type_dict = {value:key for key in ws_type_dict for value in ws_type_dict[key]}

def AddError(errorList, txt, tag="li"):
	if tag:
		errorList.append("<%s>%s</%s>" % (tag, txt, tag) )

def ValidateWorkbookWineSpirits(eL,wb):
	#test for CPL sheet
	cpl=None
	try:
		cpl = wb.sheet_by_name('CPL')
		cx_Logging.Debug('CPL worksheet found')
	except xlrd.XLRDError as e:
		cx_Logging.LogException(e.message)
		AddError(eL, "Could not find worksheet named 'CPL'")
		return
	if cpl:
		ValidateSheet_WineSpirits(eL, cpl)
	else:
		#not sure this could happen that we'd not have cpl sheet here
		pass
		
def ValidateSheet_WineSpirits(eL, ws):
	sheetError = False
	rowErrorCount = 0
	if ws.ragged_rows:
		AddError(eL,"Worksheet has inconsistent row size")
		sheetError=True
	if ws.ncols != NUM_COLS:
		AddError(eL,"Worksheet has %d columns but %d were expected "% (ws.ncols,NUM_COLS) )
		sheetError=True
	if sheetError:
		return
	else:
		#Validate Rows
		cx_Logging.Debug('Validate row data types')
		errorRows = {}
		for i in range(1,ws.nrows):
			if [c.ctype for c in ws.row_slice(i,2,ws.ncols)] != ws_type_dict.values() \
				or type_text[ws.cell_type(i,0)] not in ['number', 'text'] \
				or type_text[ws.cell_type(i,1)] not in ['number', 'text']:
				cx_Logging.Debug('Row %d has errors' % (i+1))
				#increment error counter
				rowErrorCount = rowErrorCount+1
				errorRows[i] = []
				if type_text[ws.cell_type(i,0)] not in ['number', 'text']:
					AddError(eL,"Cell %s has data type '%s' but 'number' or 'text' was expected" % ( xlrd.cellname(i,0), type_text[ws.cell_type(i,0)] ) )
				if type_text[ws.cell_type(i,1)] not in ['number', 'text']:
					AddError(eL,"Cell %s has data type '%s' but 'number' or 'text' was expected" % ( xlrd.cellname(i,2), type_text[ws.cell_type(i,2)] ) )
				for j in range(2,ws.ncols):
					if ws.cell_type(i,j) != ws_type_dict[j]:
						AddError(eL,"Cell %s has data type '%s' but '%s' was expected" % ( xlrd.cellname(i,j), type_text[ws.cell_type(i,j)], type_text[ws_type_dict[j]] ) )
			if rowErrorCount >= ROW_ERROR_THRESHOLD:
				AddError(eL,"Error threshold reached.  Procesing terminated", "b")
				return # abort processing as we've had more than enough rows-in-error

def ValidateWineSpirits(wb, fileName):
	errorList=[]
	valid=False
	htmlPart=None
	try:
		ValidateWorkbookWineSpirits(errorList,wb)
	except Exception as e: 
		cx_Logging.LogException()
		AddError(errorList,"Exception %s" % e.message)
	finally:
		del wb
		if len(errorList) > 0:
			htmlPart = "<p>The Wine / Spirits CPL file '%s' has validation errors</p>" % fileName + \
				   ''.join(errorList) 
		else:
			valid = True
			htmlPart = "<p>The Wine / Spirits CPL file '%s' appears to be valid</p>" % fileName
		return valid,htmlPart
	
def CreateRecords(doc,wb):
	ws = wb.sheet_by_name('CPL')
	cx_Logging.Info("Processing %d CPL rows", ws.nrows-1)
	for i in range(1,ws.nrows):
	    # fix this when RIP is added to spreadsheet
		try:
			dataArea.ExecuteSql("ABCCPLInsertActiveRowData", \
				cplsubmissionid   =doc.objectId, \
				sku               =ws.cell_value(i,0), \
				customfield2      =ws.cell_value(i,1), \
				wholesaler        =ws.cell_value(i,2), \
				submissiondate    =datetime(*xlrd.xldate_as_tuple(ws.cell_value(i,3),wb.datemode)), \
				fromdate          =datetime(*xlrd.xldate_as_tuple(ws.cell_value(i,4),wb.datemode)), \
				todate            =datetime(*xlrd.xldate_as_tuple(ws.cell_value(i,5),wb.datemode)), \
				brandregistration =ws.cell_value(i,6), \
				description       =ws.cell_value(i,7), \
				proof             =ws.cell_value(i,8), \
				unittype          =ws.cell_value(i,9), \
				unitquantity      =ws.cell_value(i,10), \
				unitvolumetype    =ws.cell_value(i,11), \
				pack              =ws.cell_value(i,12), \
				sleeve            =ws.cell_value(i,13), \
				unitvolumeamount  =ws.cell_value(i,14), \
				combopack         =ws.cell_value(i,15), \
				rip               =ws.cell_value(i,15), \
				listpricecase     =ws.cell_value(i,16), \
				listpricebottle   =ws.cell_value(i,17), \
				bestcase          =ws.cell_value(i,18), \
				bestbottle        =ws.cell_value(i,19), \
				splitchg          =ws.cell_value(i,20), \
				level1qty         =ws.cell_value(i,21), \
				level1disc        =ws.cell_value(i,22), \
				level2qty         =ws.cell_value(i,23), \
				level2disc        =ws.cell_value(i,24), \
				level3qty         =ws.cell_value(i,25), \
				level3disc        =ws.cell_value(i,26), \
				level4qty         =ws.cell_value(i,27), \
				level4disc        =ws.cell_value(i,28), \
				level5qty         =ws.cell_value(i,29), \
				level5disc        =ws.cell_value(i,30), \
				level6qty         =ws.cell_value(i,31), \
				level6disc        =ws.cell_value(i,32))
		except Exception as e:
			cx_Logging.LogException()
			cx_Logging.Error("Row causing exception")
			cx_Logging.Error(','.join([str(i) for i in cpl.row_values(0)]))
			cx_Logging.Error(','.join([str(i) for i in cpl.row_values(i)]))
			raise Exception('Row %s produced an error: %s' % (i+1, e.message))
	
def Main(job, processDocument=False):
	rels = job.relationships["CPLDocuments"]
	docsWineSpirits = [i.toObject for i in rels if i.toObject["CPLType"]=='Wine / Spirits']

	# currently set up to process a single CPL doc
	if len(docsWineSpirits)==0:
		return False,"<p>No Wine / Spirits CPL documents located</p>"
	elif len(docsWineSpirits) > 1:
		return False,"<p>Multiple Wine / Spirits CPL documents located, one expected</p>"
	else:
		# retrieve document revision
		doc = docsWineSpirits[0]
		if doc['Processed'] and processDocument:
				raise Exception("%s already processed" % doc["FileName"])
		rev = doc.GetRevision()
		#load data as XLSX
		wb = xlrd.open_workbook(file_contents=rev.GetData())
		result = ValidateWineSpirits(wb, doc["FileName"])
		if processDocument:
			doc['IsValid'] = ('N','Y')[result[0]] # shortcut to get a Y or N from a boolean
			doc['ValidationMessage'] = result[1]
			doc['Processed']=True
			if result[0]:
				#record rows
				cx_Logging.Info("CPL Processing Rows")
				CreateRecords(doc,wb)
		return result
