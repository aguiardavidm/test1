"""Contains utilities used in grids in Winchester LMS.

Ideally, this should contain methods that can be used generically on
grids on multiple ObjectDefs. Functionality highly specific to a certain
ObjectDef should generally be put in a more appropriate location.
Nevertheless, this is left up to the developer's discretion.

"""

def UncheckPrimaryFlagOnOtherRows(dataArea, rowObject, mainObject, endPtNameList):
    """Ensure that the current row is the only Primary row.

    If the 'IsPrimary' boolean detail is checked on the current row,
    make sure all other rows related to the main object have their
    primary flag unchecked. The other rows will be found based on a list
    of EndPointNames from the main object to the row objects.

    Example function call:
    UncheckPrimaryFlagOnOtherRows(context.dataArea, context.rowObject,
            context.layoutObject, ['MailingAddress', 'TempMailingAddress'])

    """
    if rowObject.GetColumnValueByName('IsPrimary', True):
        configCache = dataArea.configCache
        objectDef = mainObject.objectDef
        endPtList = [configCache.EndPointForName(objectDef, endPtName)
                for endPtName in endPtNameList]
        for otherRowObject in mainObject.GetRelatedObjects(endPtList):
            if otherRowObject != rowObject and \
                    otherRowObject.GetColumnValueByName('IsPrimary', True):
                otherRowObject.SetColumnValueByName('IsPrimary', False)