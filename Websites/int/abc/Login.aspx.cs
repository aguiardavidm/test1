using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class Login : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Constructors
        /// <summary>
        /// Creates an instance of this class.
        /// </summary>
        public Login()
        {
            this.SessionRequired = false;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((HttpContext.Current.Request.Url.AbsolutePath).Contains("login.aspx"))
            {
                this.Response.Redirect(this.LoginPageUrl);
            }
            this.SetValidationMessage(this.GetCookie("Message"));
            this.SetCookie("Message", null);
            this.SetInitialFocus(this.txtUserId);

        }

        /// <summary>
        /// Click event handler for the login button.
        /// </summary>
        /// <param name="sender">A reference to the login button.</param>
        /// <param name="e">Event arguments.</param>
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string redirectUrl;
            // sets a variable to check if the user is locked out
            this.SetCurrentUserId(this.txtUserId.Text);

            if (this.IsValid)
            {
                try
                {
                    //Audit Login
                    this.TrackLogin(this.txtUserId.Text);
                }
                catch (Exception ex)
                {
                    this.ErrorMessage = ex.Message;
                    this.OutriderNet.ExpireSession(this.SessionId, this.DebugKey);
                    this.SessionId = this.NoSession;

                }

                this.Login(this.txtUserId.Text, this.txtPassword.Text);

                redirectUrl = this.RedirectUrl;
                this.RedirectUrl = null;
                this.ReleaseOutriderNet();
                this.Response.Redirect(redirectUrl);
            }
        }
        #endregion
    }
}
