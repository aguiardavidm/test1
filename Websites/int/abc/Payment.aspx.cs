using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Computronix.POSSE.Outrider
{
	public partial class Payment : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			string eComTransactionId = null, payType, fromObject, fileNum, returnUrl, successBaseUrl, successPresentation, 
				successPane, failureBaseUrl, failurePresentation, failurePane, fromContractor, contractorName;
			StringBuilder xml = new StringBuilder();
			string baseUrl = this.HomePageUrl, xmlColumnFormat = "<column name=\"{0}\">{1}</column>",
				xmlCDataColumnFormat = "<column name=\"{0}\"><![CDATA[{1}]]></column>";
            List<NewObject> newObjects;

			payType = this.Request.QueryString["PayType"];
			fromObject = this.Request.QueryString ["FromObject"];
			fileNum = this.Request.QueryString ["FileNum"];
			

			if (!string.IsNullOrEmpty(this.Request.QueryString["ReturnUrl"]))
			{
				returnUrl = this.Request.QueryString["ReturnUrl"];
			}
			else
			{
				returnUrl = string.Format("{0}?PosseObjectId={1}&PossePresentation=Default&PossePane=FeesAndPayments", baseUrl, this.ObjectId);
			}

			if (!string.IsNullOrEmpty(this.Request.QueryString["SuccessBaseUrl"]))
			{
				successBaseUrl = this.Request.QueryString["SuccessBaseUrl"];
			}
			else
			{
				successBaseUrl = baseUrl;
			}

			if (!string.IsNullOrEmpty(this.Request.QueryString["SuccessPresentation"]))
			{
				successPresentation = this.Request.QueryString["SuccessPresentation"];
			}
			else
			{
				successPresentation = "Success";
			}

			if (!string.IsNullOrEmpty(this.Request.QueryString["SuccessPane"]))
			{
				successPane = this.Request.QueryString["SuccessPane"];
			}
			else
			{
				successPane = null;
			}

			if (!string.IsNullOrEmpty(this.Request.QueryString["FailureBaseUrl"]))
			{
				failureBaseUrl = this.Request.QueryString["FailureBaseUrl"];
			}
			else
			{
				failureBaseUrl = string.Format("{0}?PresentationName=StartServiceFailure", baseUrl);
			}

			if (!string.IsNullOrEmpty(this.Request.QueryString["FailurePresentation"]))
			{
				failurePresentation = this.Request.QueryString["FailurePresentation"];
			}
			else
			{
				failurePresentation = "Success";
			}

			if (!string.IsNullOrEmpty(this.Request.QueryString["FailurePane"]))
			{
				failurePane = this.Request.QueryString["FailurePane"];
			}
			else
			{
				failurePane = null;
			}
			
			if (!string.IsNullOrEmpty(this.Request.QueryString["FromContractor"]))
			{
				fromContractor = this.Request.QueryString["FromContractor"];
			}
			else
			{
				fromContractor = null;
			}
			
			if (!string.IsNullOrEmpty(this.Request.QueryString["ContractorName"]))
			{
				contractorName = this.Request.QueryString["ContractorName"];
			}
			else
			{
				contractorName = null;
			}
            
			// set up an outer block so that any unanticipated errors result in at least SOMETHING
			// coming back to the browser.
			
			
			try
			{
				// Create new proposed payment.
		        xml.Append("<object id=\"NEW1\" objectdef=\"j_ProposedPayment\" action=\"Insert\">");
		        xml.AppendFormat(xmlColumnFormat, "FromObjectId", this.ObjectId);
				xml.AppendFormat(xmlCDataColumnFormat, "FromObject", fromObject);
				xml.AppendFormat(xmlCDataColumnFormat, "FileNum", fileNum);
		        xml.AppendFormat(xmlCDataColumnFormat, "PayType", payType);
		        xml.AppendFormat(xmlCDataColumnFormat, "SuccessBaseUrl", successBaseUrl);
		        xml.AppendFormat(xmlCDataColumnFormat, "SuccessPresentation", successPresentation);
		        xml.AppendFormat(xmlCDataColumnFormat, "SuccessPane", successPane);
		        xml.AppendFormat(xmlCDataColumnFormat, "FailureBaseUrl", failureBaseUrl);
		        xml.AppendFormat(xmlCDataColumnFormat, "FailurePresentation", failurePresentation);
		        xml.AppendFormat(xmlCDataColumnFormat, "FailurePane", failurePane);
		        xml.AppendFormat(xmlCDataColumnFormat, "ReturnUrl", returnUrl);
				xml.AppendFormat(xmlCDataColumnFormat, "FromContractor", fromContractor);
		        xml.AppendFormat(xmlCDataColumnFormat, "ContractorName", contractorName);
				xml.Append("</object>");

				// Create the new proposed payment.
				this.ProcessXML(xml.ToString());

				// Get objectId;
				newObjects = this.GetNewObjects();
				

				// Check if this user is allowed to do manual payments.
				if ((bool)this.UserInformation[0]["CanDoManualPayment"])
				{
				

					if (payType == "Refund")
					{
						// Redirect to proposed refund window to allow manual refund.
						this.Response.Redirect(string.Format("{0}?PosseObjectId={1}&PossePresentation=NewPendingRefund",
							baseUrl, newObjects[0].ObjectId));
					}
					else
					{
						// Redirect to proposed payment window to allow manual payments.
                        if (fromContractor == "Y")
                        {
                            this.Response.Redirect(string.Format("{0}?PosseObjectId={1}&PossePresentation=NewPendingPaymentContractor",
                                baseUrl, newObjects[0].ObjectId));
                        }
                        else
                        { 						
                            this.Response.Redirect(string.Format("{0}?PosseObjectId={1}&PossePresentation=NewPendingPayment",
                                baseUrl, newObjects[0].ObjectId));
                        }
					}
				}
				else
				{
				throw new ArgumentException("A start-up parameter is required.");
                eComTransactionId = newObjects[0].ObjectId.ToString();
				}
			}
			catch (Exception exception)
			{
				this.ErrorSummary.Text = exception.Message;
				this.ErrorDetail.Text = exception.StackTrace;
				this.Link1.NavigateUrl = string.Format("{0}?PosseObjectId={1}&PossePresentation=StartServiceFailure", 
					baseUrl, eComTransactionId);
			}
		}
		#endregion
	}
}