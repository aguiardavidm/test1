using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
	public partial class PaymentAdjustment : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				StringBuilder xml = new StringBuilder();
                List<NewObject> objects;

				// Create Payment Adjustment job using XML.

				xml.Append("<object id=\"NEW1\" objectdef=\"j_ABC_PaymentAdjustment\" action=\"Insert\">");
				xml.AppendFormat("<column name=\"PaymentObjectId\">{0}</column>", this.ObjectId);
				xml.Append("</object>");

				this.ProcessXML(xml.ToString());

                objects = this.GetNewObjects();
				
				this.ReleaseOutriderNet();

				this.Response.Redirect(string.Format("default.aspx?PosseObjectId={0}&PossePresentation=Default",
                    objects[0].ObjectId));
			}
			catch (Exception exception)
			{
				this.ProcessError(exception);
				this.RenderErrorMessage(this.pnlPaneBand);
			}
		
		}
		#endregion
	}
}
