<%@ WebHandler Language="C#" Class="Computronix.POSSE.Outrider.ToDoHandler" %>
/*** Author: Louis E. ***/
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;
using System.Xml;
using Computronix.CXTools.SuperClass;
using Computronix.POSSE.Winchester.Api;


namespace Computronix.POSSE.Outrider
{

    public class ToDoHandler : Computronix.POSSE.Outrider.PageBaseExt, IHttpHandler
    {    

        #region Data Members
        private IServiceApi serviceApi = null;
        /// <summary>
        /// Provides a reference to the ServiceApi interface.
        /// </summary>
        protected virtual IServiceApi ServiceApi
        {
            get
            {
                if (this.serviceApi == null)
                {
                    //Check for web config values
                    var serverName = System.Configuration.ConfigurationManager.AppSettings["ServiceApiServerName"];
                    var serverPort = System.Configuration.ConfigurationManager.AppSettings["ServiceApiServerPort"];
                    var serverTimeout = System.Configuration.ConfigurationManager.AppSettings["ServiceApiTimeout"];

                    if (String.IsNullOrEmpty(serverName))
                        throw new Exception("Cannot load parameter named 'ServiceApiServerName' from web.config!");
                    if (String.IsNullOrEmpty(serverPort))
                        throw new Exception("Cannot load parameter named 'ServiceApiServerPort' from web.config!");
                    if (String.IsNullOrEmpty(serverTimeout))
                        throw new Exception("Cannot load parameter named 'ServiceApiTimeout' from web.config!");

                    if (!String.IsNullOrEmpty(serverName) && !String.IsNullOrEmpty(serverPort) && !String.IsNullOrEmpty(serverTimeout))
                    {
                        //var client = new Computronix.POSSE.AppServerClient.AppServerClient(serverName, serverPort, Convert.ToInt32(serverTimeout));
                        //this.serviceApi = (IServiceApi)client;
                        this.serviceApi = ServiceApiFactory.GetServiceApi(serverName, Convert.ToInt32(serverPort), Convert.ToInt32(serverTimeout));
                    }
                }
                return this.serviceApi;
            }
            set
            {
                this.serviceApi = value;
            }
        }

        #endregion
        private IOutriderNet2 OutriderNet;

        private HttpResponse Response;

        private HttpRequest Request;
        
        private string SessionToken;
        
        private string Action;
        
        private string JobId;

        private string UserId;

        public void ProcessRequest(HttpContext context)
        {

            // Create context manager and push parms etc. into it
            //ContextManager contextManager = new ContextManager();
            //Sys.ParseContext(this.Session, this.Request, contextManager);

            int retryCount = 0;
            this.Response = context.Response;
            this.Request = context.Request;
            this.Response.ContentType = "application/json";
            this.Action = this.Request.QueryString["action"];
            using (OutriderNet = new OutriderNet()) {
                Authenticate();              

                this.OutriderNet.LogMessage(LogLevel.Info, "Begin action: " + this.Action);                 
                Execute();
                this.OutriderNet.LogMessage(LogLevel.Info, "End: Action" + this.Action);
            }
        }

        private void Authenticate() {
            this.OutriderNet.LogMessage(LogLevel.Info,"Query String: " + this.Request.Url.Query);
            bool hasToken = this.Request.Url.Query.Contains("TokenId=");
            this.OutriderNet.LogMessage(LogLevel.Info,"Has token: " + hasToken.ToString());
            if (!hasToken){
                this.SessionToken = GetCookie(ConfigurationManager.AppSettings["SessionIdName"]);
                if (string.IsNullOrEmpty(this.SessionToken) || !OutriderNet.AttachSession(this.SessionToken)) {
                    ShowLogin("This page requires you to be logged in.");
                    return;
                } 
            } else if (!OutriderNet.AttachSession(this.Request.QueryString["TokenId"])) {
                this.Response.Write("Invalid Token");
                this.Response.End();
            }
        }

        private void Execute() {            
            switch (this.Action)
            {
                case "getJobNoteHistory":
                    this.JobId = this.Request.QueryString["jobId"];
                    this.UserId = this.Request.QueryString["userId"];
                    getJobNoteHistory(this.JobId, this.UserId);
                    break;
                default:
                    throw new Exception("Invalid request action received.");
                    break;
            }
        }

        /// <summary>
        /// Get to do note history for a given job and user
        /// </summary>
        private void getJobNoteHistory(string jobId, string userId)
        {
            //string jobNoteHistoryJson = "[{\"name\": \"POasd - Administrative Review PR\",\"note\": \"Updated notes for admin review.\",\"notedate\": \"04/20/2018 8:12 AM\"},{\"name\": \"PO - Administrative Review PR\",\"note\": \"Notes for this admin review.\",\"notedate\": \"04/20/2018 8:12 AM\"}]";
            //List<Dictionary<string, string>> jobNoteHistoryJson = (List<Dictionary<string, string>>)ExecuteSql("ABC_ActiveLegalEntityType");

            //Winchester Service API
            string args = String.Format("{{\"jobid\":{0}, \"userid\":{1}}}", this.JobId, this.UserId);
            String jobNoteHistory = this.ServiceApi.ExecuteStatement(this.SessionToken.ToString(), "ABCGetToDoNoteHistory", args);

            this.Response.Write(jobNoteHistory);
        }        

        /// <summary>
        /// Returns the specified cookie value.
        /// </summary>
        /// <param name="name">The name of the value to retrieve.</param>
        /// <returns>The specified value otherwise null.</returns>
        protected virtual string GetCookie(string name)
        {
            string result = null;
            HttpCookie cookie;

            if (((IList)this.Response.Cookies.AllKeys).Contains(name))
            {
                cookie = this.Response.Cookies[name];
                result = cookie.Value;
            }
            else
            {
                cookie = this.Request.Cookies[name];

                if (cookie != null)
                {
                    result = cookie.Value;
                }
            }

            return result;
        }

        /// <summary>
        /// Sets the specified cookie value.
        /// </summary>
        /// <param name="name">The name of the value.</param>
        /// <param name="value">The value being set.</param>
        protected virtual void SetCookie(string name, string value)
        {
            HttpCookie cookie = this.Response.Cookies[name];

            cookie.Value = value;
            cookie.Path = ConfigurationManager.AppSettings["CookiePath"];
        }
        
        /// <summary>
        /// Presents the login page with the specified message.
        /// </summary>
        /// <param name="message">The message to display.</param>
        protected virtual void ShowLogin(string message)
        {
            this.OutriderNet.Dispose();
            SetCookie("Message", message);
            this.Response.Redirect(ConfigurationManager.AppSettings["LoginPageUrl"]);
        }
 
        public bool IsReusable {
            get {
                return false;
            }
        }

    }
}