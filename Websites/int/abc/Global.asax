﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application start up
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Application_BeginRequest(object sender, EventArgs e)
    {
        // If the path the user is coming in through does not match the case of the application path,
        // redirect to the application patch.  This is to ensure that cookies (which have case-sensitive paths)
        // always get sent down properly.
        string url = HttpContext.Current.Request.Url.PathAndQuery;
        string application = HttpContext.Current.Request.ApplicationPath;

        if (!url.StartsWith(application))
        {
            HttpContext.Current.Response.Redirect(application + url.Substring(application.Length));
        }
    }
           
</script>
