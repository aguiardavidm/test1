<%@ Page Language="C#" MasterPageFile="~/OutriderLogin.master" AutoEventWireup="true"
	CodeFile="Login.aspx.cs" Inherits="Computronix.POSSE.Outrider.Login" Title="Login" %>

<asp:Content ID="cntPaneBand" ContentPlaceHolderID="cphPaneBand" runat="Server">
	<table style="width: 300px; margin-bottom: 50px;">
		<tr>
			<td>
				<asp:Label ID="lblUserId" runat="server" Text="User Id:" CssClass="possedetail"></asp:Label></td>
			<td align="right">
				<asp:TextBox ID="txtUserId" width="230px" runat="server" CssClass="fieldtext;" autofocus></asp:TextBox>
			</td>
			<td>
				<asp:RequiredFieldValidator ID="rfvUserId" runat="server" ErrorMessage="User Id is required."
					ControlToValidate="txtUserId">*</asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="lblPassword" runat="server" Text="Password:" CssClass="possedetail"></asp:Label></td>
			<td align="right">
				<asp:TextBox ID="txtPassword" width="230px" runat="server" CssClass="fieldtext" TextMode="Password"></asp:TextBox>
			</td>
			<td>
				<asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Password is required."
					ControlToValidate="txtPassword">*</asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td align="right" colspan="2">
				<asp:Button ID="btnLogin" runat="server" CssClass="loginButton" OnClick="btnLogin_Click" /></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3">
				<asp:ValidationSummary ID="vsmMain" runat="server" CssClass="posseerror" DisplayMode="List"
					ForeColor="" />
			</td>
		</tr>
	</table>
</asp:Content>
