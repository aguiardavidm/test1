using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace Computronix.POSSE.Outrider
{
    public partial class FunctionLink : Computronix.POSSE.Outrider.FunctionLinkBase
    {
        #region Data Members

        public string topImageUrl, backImageUrl, bottomImageUrl, leftImageUrl, rightImageUrl;
        public override RoundTripFunction FunctionType
        {
            set
            {
                base.FunctionType = value;
                this.hlkBtnLink.ID = value.ToString();
            }
        }
        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            bool disableButtons = true;
            bool checkDisable = true;
            string identifier = this.hlkBtnLink.ClientID;
            StringBuilder onclick = new StringBuilder();
            this.lblBtnLabel.Text = this.Text;
            
            if (this.Text == "Save as Excel")
                this.pnlButton.CssClass = "buttonExcel";
            else if (this.Text == "Search")
                this.pnlButton.CssClass = "buttonlookup";
            else
                this.pnlButton.CssClass = "button";

            switch (this.Text)
            {
                case "Cancel":
                case "Close":
                case "Back":
                case "Cancel and Clear":
                    checkDisable = false;
                    disableButtons = false;
                    break;
            }

            onclick.AppendFormat("function {0}_fn(){{", identifier);

            if (checkDisable)
            {
                onclick.Append("if (vProcessFunctionLinks) {");
            }

            if (disableButtons)
            {
                onclick.Append("vProcessFunctionLinks=false; ");
            }

            // if the href was already javascript, strip the js prefix, otherwise, do a navigate.
            if (this.NavigationUrl.Substring(0, 10).ToLower() == "javascript")
                onclick.AppendFormat("var validEntry = {0};", this.NavigationUrl.Substring(11));
            else
                onclick.AppendFormat("var validEntry = true; window.location='{0}';", this.NavigationUrl);

            if (disableButtons)
            {
                if (this.Text == "Save as Excel")
                {
                    onclick.Append(" window.setTimeout(waitASec, 2000);");
                }
                else
                {
                    onclick.Append(" if (!validEntry) {vProcessFunctionLinks=true;}");
                }
            }

            if (checkDisable)
            {
                onclick.Append("}");
            }

            onclick.Append(" return false;}\r\n");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), identifier, onclick.ToString(), true);
            this.hlkBtnLink.Attributes.Add("onclick", string.Format("return {0}_fn();", identifier));
        }
        #endregion

        #region Methods
        public override string ClientID
        {
            get
            {
                return this.hlkBtnLink.ClientID;
            }
        }
        #endregion
    }
}