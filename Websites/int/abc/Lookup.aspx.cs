using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class Lookup : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.LoadLookup())
            {
                this.RenderErrorMessage(this.pnlPaneBand);

                if (this.HasPresentation)
                {
                    this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
                    this.CheckClient();
                    this.RenderSearch(this.pnlHeaderBand);
                    this.RenderResults(this.pnlPaneBand);
                    this.RenderLookupFunctionBand(this.pnlBottomFunctionBand);
                }
            }
            else
            {
                this.Response.Clear();

                if(this.UsingAJAX)
                {
                    this.Response.Write(this.GetLookupResultsJavaScript(this.UsingAJAX, this.DataChanges));
                }
                else
                {
                    this.Response.Write(this.GetLookupResultsJavaScript(this.UsingAJAX, ""));
                }
                this.Response.End();
            }
        }
        #endregion
    }
}