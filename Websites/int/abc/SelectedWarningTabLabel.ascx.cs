﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Computronix.POSSE.Outrider
{

    public partial class SelectedWarningTabLabel : Computronix.POSSE.Outrider.TabLabelBase
    {

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblTxtLabel.Text = this.Text;
            this.hlkTabLink.NavigateUrl = this.NavigationUrl;

            if (this.Condition == "WarningsExist")
            {
                this.hlkTabLink.ForeColor = System.Drawing.Color.Red;
            }
            else if (this.Condition == "Highlight")
            {
                this.hlkTabLink.ForeColor = System.Drawing.Color.Yellow;
                this.hlkTabLink.Font.Bold = true;
            }
        }
        #endregion
    }
}

