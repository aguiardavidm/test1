using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class RetryProcess : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			this.OutriderNet.LogMessage(LogLevel.Error, "****************************************************************");
			this.OutriderNet.LogMessage(LogLevel.Error, "Attempting Retry of System Processing.");
			string paneType, pane;
			int objectId;
			string currentItem, newItem, description, output, temp;
			int index, paneId, numRows;

			objectId = int.Parse(Request.QueryString["PosseObjectId"]);
			this.OutriderNet.LogMessage(LogLevel.Error, String.Format("PosseObjectId: {0}", objectId));
			this.OutriderNet.RetrySystemProcessing(objectId);
			Response.Redirect("Default.aspx?PossePresentation=" +
				Request.QueryString["PossePresentation"] +
				"&PosseObjectId=" + Request.QueryString["PosseObjectId"]);
			this.ReleaseOutriderNet();
			this.OutriderNet.LogMessage(LogLevel.Error, "****************************************************************");
		}
		#endregion
	}
}