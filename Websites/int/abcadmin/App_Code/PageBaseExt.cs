﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Computronix.POSSE.Outrider
{
    /// <summary>
    /// Extension class for PageBase.  Place any customizations in here.
    /// </summary>
    public class PageBaseExt : PageBase
    {
        #region PageBase
		private string ipAddress = null;
        //<summary>
        //Provides access to the IP Address. We first try yo geth the IP Address the proxy recorded, if present
        //</summary>
        protected virtual string IPAddress
        {
            get
            {
                if (ipAddress == null)
                {
                    ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }
                if (ipAddress == null)
                {
                    ipAddress = Request.ServerVariables["Remote_ADDR"];
                }
                if (ipAddress == null)
                {
                    ipAddress = Request.UserHostAddress;
                }
                return ipAddress;
            }

        }


        private string hostName = null;
        //<summary>
        //Provides access to hostName
        //</summary>
        protected virtual string HostName
        {
            get
            {
                if (hostName == null)
                {
                    hostName = Request.UserHostName;
                }
                return hostName;
            }

        }

        //<summary>
        //Provides access to the Login Audit User
        //</summary>
        protected virtual string LoginAuditUser
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginAuditUser"];
            }
        }

        //<summary>
        //Provides access to the Login Audit Password
        //</summary>
        protected virtual string LoginAuditPass
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginAuditPass"];
            }
        }
		
		/// <summary>
        /// Whether to enable Save As Excel on the current search presentation - this requires an Excel pane built on the search with a Python rendering that returns XML
        /// </summary>
        protected virtual bool SaveAsExcelExtractOnSearches
        {
            get
            {
                string saveAsExcel = ConfigurationManager.AppSettings["SaveAsExcelExtractOnSearches"];

                return this.PresentationType == PresType.List &&
                    string.Compare(saveAsExcel, "true", StringComparison.InvariantCultureIgnoreCase) == 0;
            }
        }

        private string changesOnObject = "";
        /// <summary>
        /// Provides access to the ChangesOnObject from the form data.  (Request.Form["ChangesOnObject"])
        /// </summary>
        protected virtual string ChangesOnObject
        {
            get
            {
            changesOnObject = this.Request.Form["ChangesOnObject"];
            return changesOnObject;
            }
            set
            {
            changesOnObject = value;
            }
        }

        private string changesPending = "";
        /// <summary>
        /// Provides access to the ChangesPending from the form data.  (Request.Form["ChangesPending"])
        /// </summary>
        protected virtual string ChangesPending
        {
            get
            {
            if (this.ChangesOnObject == this.ObjectId)
            {
                if (String.IsNullOrEmpty(changesPending))
                {
                changesPending = this.Request.Form["ChangesPending"];
                }
            }
            else
            {
                changesPending = "";
            }
            return changesPending;
            }
            set
            {
            changesPending = value;
            }
        }

        /// <summary>
        /// Provides access to the creator of the last document to be uploaded.  (Request.QueryString["PosseCreatedBy"])
        /// </summary>
        protected virtual string LastUploadCreatedBy
        {
            get
            {
                return this.Request.QueryString["PosseCreatedBy"];
            }
        }

        /// <summary>
        /// Returns the HTML that creates the hidden "changes" form.
        /// </summary>
        /// <returns>The HTML for the data changes cache.</returns>
        protected override void GetPaneChangesHTML()
        {
            string changesObjectId;
            if (!String.IsNullOrEmpty(this.ObjectId))
            {
            changesObjectId = this.ObjectId;
            }
            else
            {
            changesObjectId = "";
            }
            String changesPendingInput = String.Format("<input name=\"changespending\" id=\"changespending\" value=\"{0}\"/>", this.ChangesPending);
            String changesObjectInput = String.Format("<input name=\"changesonobject\" id=\"changesonobject\" value=\"{0}\"/>", changesObjectId);
            this.PaneChangesHtml = this.OutriderNet.GetPaneChangesHTML().Replace("</form>", String.Format("{0}{1}</form>", changesPendingInput, changesObjectInput));

            //we need to make the forms fields non-hidden to get around a Chrome bug
            this.PaneChangesHtml = this.PaneChangesHtml.Replace("type=hidden", "");
            return;
        }

        /// <summary>
        /// Processes the current request.
        /// </summary>
        protected override void LoadPresentationCore(bool returnAfterSubmit, bool startDialogWithChanges)
        {
            int paneId, currentPaneId;
            bool autoSave;

            this.SetSiteProperty(SiteProperty.BaseUrl, this.HomePageUrl);
            this.SetSiteProperty(SiteProperty.LookupUrl, this.LookupPageUrl);
            this.SetSiteProperty(SiteProperty.LookupReturnUrl, this.LookupReturnUrl);
            this.SetSiteProperty(SiteProperty.NoteUrl, this.NotePageUrl);
            this.SetSiteProperty(SiteProperty.ReportUrl, this.ReportPageUrl);
            this.SetSiteProperty(SiteProperty.UploadUrl, this.UploadPageUrl);

            if (startDialogWithChanges)
            {
                if (!string.IsNullOrEmpty(this.PresentationId) ||
                  !string.IsNullOrEmpty(this.Presentation))
                {
                    try
                    {
                        // Eventually when the StartDialog function allows data changes to be passed
                        // through, we will update it here.
                        this.StartDialog(this.Request.QueryString.ToString());
                        this.ReleaseOutriderNet();

                        this.StartSession();

                        this.ProcessFunction(this.ObjectId, this.PaneId, this.PaneId, this.RoundTripFunction,
                            this.DataChanges, this.SortColumns, this.ChangesXml);
                        // if we're submitting to the database, clear the ChangesPending flag.
                                    if (this.RoundTripFunction == RoundTripFunction.Submit)
                                    {
                                            this.ChangesPending = "F";
                                    }
                    }
                    catch (Exception exception)
                    {
                        this.ProcessError(exception);
                    }

                    if (returnAfterSubmit)
                    {
                        this.HasPresentation = true;
                    }
                    else
                    {
                        this.CheckRedirect();
                    }
                }
            }
            else if (this.ComesFrom == "posse")
            {
                currentPaneId = int.Parse(this.Request.Form["CurrentPaneId"]);

                if (string.IsNullOrEmpty(this.Pane))
                {
                    paneId = int.Parse(this.Request.Form["PaneId"]);
                }
                else
                {
                    // TODO: Gord says its a bug that needs to be fixed in the COM.
                    this.StartDialog(this.Request.QueryString.ToString());

                    paneId = this.GetPaneIdFromName(this.Pane, currentPaneId);
                }

                this.ChangesXml = this.Request.Form["ChangesXml"] + this.ChangesXml;

                try
                {
                    this.ProcessFunction(this.ObjectId, currentPaneId, paneId,
                      this.RoundTripFunction, this.DataChanges, this.SortColumns, this.ChangesXml);

                                // if we're submitting to the database, clear the ChangesPending flag.
                                if (this.RoundTripFunction == RoundTripFunction.Submit)
                                {
                                    this.ChangesPending = "F";
                                }

                    // Check to see if the auto-save condition is set for this pane.  If so then
                    // perform a submit.
                    try
                    {
                        autoSave = this.GetCondition(this.PaneId, "AutoSave");
                    }
                    catch
                    {
                        autoSave = false;
                    }

                    if (autoSave)
                    {
                        this.ProcessFunction(null, this.PaneId, this.PaneId, RoundTripFunction.Submit,
                          this.GetCacheState(), null);
                        this.ChangesPending = "F";
                    }
                }
                catch (Exception exception)
                {
                    this.ProcessError(exception);
                }

                if (returnAfterSubmit && (this.RoundTripFunction == RoundTripFunction.Submit ||
                  this.RoundTripFunction == RoundTripFunction.Refresh || this.GetNewObjects().Count > 0))
                {
                    this.HasPresentation = true;
                }
                else
                {
                    this.CheckRedirect();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(this.PresentationId) ||
                  !string.IsNullOrEmpty(this.Presentation))
                {
                    try
                    {
                        this.StartDialog(this.Request.QueryString.ToString());
                        string xmlChangesColumn = this.Request.QueryString["PosseAppendChangesXMLColumn"];

                        if (this.AutoSubmit)
                        {
                            this.ProcessFunction(null, this.PaneId, this.PaneId,
                              RoundTripFunction.Submit, this.GetCacheState(), null);
                                            this.ChangesPending = "F";
                        }
                        else if (!string.IsNullOrEmpty(xmlChangesColumn))
                        {
                            OrderedDictionary data = this.GetPaneData(true)[0];
                            string xmlChanges = String.Format("<object id=\"{0}\" action=\"Update\">{1}</object>",
                                                              data["objecthandle"].ToString(),
                                                              data[xmlChangesColumn].ToString());
                            this.OutriderNet.ProcessFunction("", this.OutriderNet.GetPaneId(), this.OutriderNet.GetPaneId(),
                                RoundTripFunction.Refresh, this.OutriderNet.GetCacheState(), "", xmlChanges);
                            this.ChangesPending = "T";
                        }
                    }
                    catch (Exception exception)
                    {
                        this.ProcessError(exception);
                    }

                    if (returnAfterSubmit)
                    {
                        this.HasPresentation = true;
                    }
                    else
                    {
                        this.CheckRedirect();
                    }
                }
            }

            this.LoadPane();
        }
		
		/// <summary>
        /// Starts a new session for the specified user.
        /// </summary>
        /// <param name="userId">The userid of the user logging in.</param>
        /// <param name="password">The password of the user logging in.</param>
        protected override void Login(string userId, string password)
        {
            this.SessionId = this.OutriderNet.NewSession(userId, password,
                this.UserHostAddress, this.DebugKey);
			if (!(bool)this.UserInformation[0]["IsSystemAdministration"])
			{
				string errorMessage;
				
				errorMessage = "<span style='text-decoration: underline'>Login Failed:</span>";
				errorMessage += "<span style='font-weight: normal'> Please check your Email / User Name and Password and try again.</span>";
				
                this.SetCookie("Message", errorMessage);
				this.Logout();
	}
			this.ReleaseOutriderNet();
        }

        /// <summary>
        /// Ends the current session and redirects the user to the home page.
        /// </summary>
        protected override void Logout()
        {
            this.Logout(this.LoginPageUrl);
        }
		
        /// <summary>
        /// Used to create a session to login as the audit user. Will end the session once information about login attempt
        /// has been logged.
        /// </summary>
		protected virtual void TrackLogin(string authenticationName)
        {
            this.SessionId = this.OutriderNet.NewSession(this.LoginAuditUser, this.LoginAuditPass,
                          this.UserHostAddress, this.DebugKey);
            LoginAudit("Login", "Login Attempt", authenticationName, "Admin");
			this.OutriderNet.ExpireSession(this.SessionId, this.DebugKey);
			this.SessionId = this.NoSession;
        }

        /// <summary>
        /// Used to track all login attempts (valid and invalid). Information tracked include account attempting to access
        /// IP address and hostname of user attempting to access site and site user is attempting to access.
        /// Information is stored in external posse table.
        /// </summary>
        protected virtual void LoginAudit(string Event, string EventStatus, string account, string site)
        {
            string xml = "<object id=\"NEW0\" objectdef=\"o_LoginAudit\" action=\"Insert\">";
            xml += string.Format("<column name=\"Event\"><![CDATA[{0}]]></column>", Event);
            xml += string.Format("<column name=\"EventStatus\"><![CDATA[{0}]]></column>", EventStatus);
            xml += string.Format("<column name=\"IPAddress\"><![CDATA[{0}]]></column>", this.IPAddress);
            xml += string.Format("<column name=\"HostName\"><![CDATA[{0}]]></column>", this.HostName);
            xml += string.Format("<column name=\"Account\"><![CDATA[{0}]]></column>", account);
            xml += string.Format("<column name=\"Site\"><![CDATA[{0}]]></column>", site);
            xml += string.Format("</object>");
            this.ProcessXML(xml);
        }

        /// <summary>
        /// Stores the current UserId attempt
        /// </summary>
        protected string CurrentUserId = "";
        protected virtual void SetCurrentUserId(string UserId)
        {
            this.CurrentUserId = UserId;
        }

        /// <summary>
        /// Evaluates the specified exception and provides an error message or redirects the user to the login page.
        /// </summary>
        /// <param name="exception">A reference to the exception to be processed.</param>
        protected override void ProcessError(Exception exception)
        {
            int errorCode = this.GetErrorCode(exception);

            List<OrderedDictionary> data;
            string args, errorMessage;

            if (!(exception is ThreadAbortException))
            {
                if (errorCode > 0)
                {
                    switch (errorCode)
                    {
                        case 1006:  //Search error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1029:  //EditMask error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1035:  //Upload error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1045:  //Pane mandatory error.
                        case 1046:  //Pane mandatory custom error.
                        case 1066:  //Detail mandatory error.
                        case 1067:  //Job detail mandatory error. (Due to workflow)
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1052:  //Update error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1074:  //Unique index error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1075:  //Insufficient privileges error.
                        case 1277:  //AuthenticationRequired.
                            this.ShowLogin("The page which you are trying to access requires you to Sign in.");
                            break;
                        case 1278: //InvalidUserOrPassword
                            // Logs in as Audit to be able to run ExecuteSql
                            this.SessionId = this.OutriderNet.NewSession(this.LoginAuditUser, this.LoginAuditPass,
                                          this.UserHostAddress, this.DebugKey);

                            //throw new Exception (this.GetCookie(this.SessionIdName));

                            args = String.Format("ShortName={0}", this.CurrentUserId);
                            List<Dictionary<string, string>> LockoutCheck = (List<Dictionary<string, string>>)ExecuteSql("ABCInternalLockoutCheck", args);
                            this.OutriderNet.ExpireSession(this.SessionId, this.DebugKey);
                            this.SessionId = this.NoSession;

                            errorMessage = "<span style='text-decoration: underline'>Login Failed:</span><span style='font-weight: normal'> Please check your Email / User Name and Password and try again.<br><br>Consecutive failed login attempts will lock your account.</span>";
                            if (LockoutCheck.Count > 0)
                            {
                                if (LockoutCheck[0]["ISLOCKEDOUT"].ToString() == "Y")
                                {
                                    errorMessage = "<span style='text-decoration: underline'>Account Locked:</span><span style='font-weight: normal'> Consecutive failed login attempts have caused your account to be locked.</span>";
                                }
                            }
                            this.ShowLogin(errorMessage);
                            break;
                        case 1315:  //Site busy
                            this.ReleaseOutriderNet();
                            this.WriteToSiteBusyLog();
                            this.Server.Transfer("SiteBusy.html");
                            break;
                        case 1316:  //Site reloading
                            this.ReleaseOutriderNet();
                            this.Server.Transfer("SiteLoading.html");
                            break;
                        case 1436:  // BusinessError.
                            this.ErrorMessage = exception.Message;
                            break;
                        default:
                            this.ReleaseOutriderNet();
                            // This is an awkward approach. In order to pass exception information
                            // to the error page we have to ensure that the exception is trapped by the
                            // page level error handler (Page_Error) so that Server.GetLastError()
                            // will be available to the error page.  Rethrowing the same exception
                            // ensures that the exception is caught by Page_Error and then sent
                            // back to this method.
                            //
                            // Originally cookies were used to stored the error info and
                            // Response.Redirect() was called to redirect to the error page.
                            // However the error messages that Outrider produces may contain HTML
                            // and could be quite large.  Cookies will trucate anything after a
                            // semicolon, cutting off the error message.
                            //
                            // Other options include using ASP.NET session variables or putting the
                            // error messages in a hash table (indexed by the Outrider sessionid)
                            // on the Outrider COM .Net component itself.

                            if (this.Server.GetLastError() == null)
                            {
                                throw exception;
                            }
                            this.Server.Transfer(ConfigurationManager.AppSettings["ErrorPageUrl"]);
                            break;
                    }
                }
                else
                {
                    this.ReleaseOutriderNet();

                    if (this.Server.GetLastError() == null)
                    {
                        throw exception;
                    }

                    this.Server.Transfer(ConfigurationManager.AppSettings["ErrorPageUrl"]);
                }
            }

            this.Server.ClearError();
        }

        #endregion

        #region PageBaseUI

        /// <summary>
        /// Execute a registered statment (no arguments)
        /// </summary>
        /// <param name="statementName"></param>
        /// <returns></returns>
        protected object ExecuteSql(String statementName)
        {
            return this.ExecuteSql(statementName, null);
        }

        /// <summary>
        /// Execute a registered statment (with arguments)
        /// </summary>
        protected object ExecuteSql(String statementName, String args)
        {
            int paneId;
            List<OrderedDictionary> data;
            string objectId, returnValue;
            StringBuilder xml;
            object result;

            objectId = this.PosseUtilitiesInfo["ObjectId"];
            paneId = Int32.Parse(this.PosseUtilitiesInfo["PaneId"]);

            xml = new StringBuilder();
            xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", objectId);
            xml.AppendFormat("<column name=\"StatementName\">{0}</column>", statementName);
            if (!String.IsNullOrEmpty(args))
            {
                xml.AppendFormat("<column name=\"Arguments\">{0}</column>",
                    HttpUtility.HtmlEncode(args));
            }
            xml.Append("</object>");

            this.ProcessFunction(objectId, paneId, paneId, RoundTripFunction.Refresh,
                "", null, xml.ToString());

            data = this.GetData("Results", Int32.Parse(objectId));
            returnValue = data[0][0].ToString();

            result = cxJSON.JSON.Instance.Parse(returnValue);

            return result;
        }

        /// <summary>
        /// Renders the tab labels and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected override void RenderTabLabelBand(WebControl container)
        {
            Panel panelContainer;
            TabLabelBase tabLabel;
            int index, tabCount;

            if (container != null)
            {
                switch (this.PresentationType)
                {
                    case PresType.Tabs:
                        tabCount = this.Panes.Count;

                        if (tabCount > 0)
                        {
                            panelContainer = new Panel();
                            panelContainer.CssClass = "tablabelband";

                            index = 0;

                            if (tabCount == 1 && string.IsNullOrEmpty(this.HeaderPaneHtml))
                            {
                                //remove the line below the tabs, as there will be no tabs in this case
                                panelContainer.Style.Add("border-bottom", "none");
                            }
                            else
                            {
                                foreach (VisiblePaneInfo pane in this.Panes)
                                {
                                    if (this.GetCondition(pane.PaneId, "OutstandingFees"))
                                    {
                                        tabLabel = this.RenderWarningTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                    }
                                    else
                                    {
                                    tabLabel = this.RenderTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                                    }
                                    tabLabel.Index = index;
                                    tabLabel.Count = tabCount;

                                    panelContainer.Controls.Add(tabLabel);

                                    index = index + 1;
                                }

                                // Load the job presentation tabs if present
                                if (this.JobPanes != null)
                                {
                                    foreach (VisiblePaneInfo pane in this.JobPanes)
                                    {
                        if (this.GetCondition(pane.PaneId, "OutstandingFees"))
                        {
                        tabLabel = this.RenderWarningTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                        }
                        else
                        {
                                        tabLabel = this.RenderTab(pane.PaneId, pane.Name, pane.Label, pane.Enabled, pane.Condition, false);
                        }
                                        tabLabel.Index = index;
                                        tabLabel.Count = tabCount;

                                        panelContainer.Controls.Add(tabLabel);

                                        index = index + 1;
                                    }
                                }
                            }
                            container.Controls.Add(panelContainer);
                        }
                        break;
                    case PresType.Wizard:

                        panelContainer = new Panel();
                        panelContainer.CssClass = "tablabelband";

                        tabLabel = (TabLabelBase)TemplateControl.LoadControl(this.SelectedTabLabelUrl);
                        tabLabel.Selected = true;
                        tabLabel.Enabled = true;
                        tabLabel.Text = this.PaneLabel;
                        tabLabel.Index = 0;
                        tabLabel.Count = 1;

                        panelContainer.Controls.Add(tabLabel);
                        container.Controls.Add(panelContainer);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Renders the functions for the top of the page and adds them to the specified container.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        protected override void RenderTopFunctionBand(WebControl container)
        {
            HtmlTable table;
            HtmlTableRow row;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();

                if (this.PresentationType == PresType.Wizard)
                {
                    this.RenderFunctionLink(row, "Back", RoundTripFunction.Previous);
                    this.RenderFunctionLink(row, "Next", RoundTripFunction.Next);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Submit]))
                {
                    this.RenderFunctionLink(row, "Save", RoundTripFunction.Submit);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]))
                {
                    defaultFunction = this.RenderFunctionLink(row, "Search", RoundTripFunction.PerformSearch);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Search]))
                {
                    this.RenderFunctionLink(row, "Search Again", RoundTripFunction.Search);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]))
                {
                    if (this.SaveAsExcel)
                    {
                        string excelUrl = string.Format("{0}?{1}", this.ExcelDownloadUrl, this.Request.QueryString);
                        string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.Refresh, this.PaneId);
                        this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                    }

                    if (this.SaveAsExcelReport)
                    {
                        int excelPaneId = this.GetPaneIdFromName("Excel", this.PaneId);
                        string excelUrl = string.Format("{0}?{1}&SaveAsExcelReport=Y", this.ExcelDownloadUrl, this.Request.QueryString);
                        string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.Refresh, excelPaneId);
                        this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                    }

                    if (this.SaveAsCSVReport)
                    {
                        int excelPaneId = this.GetPaneIdFromName("Excel", this.PaneId);
                        string excelUrl = string.Format("{0}?{1}&SaveAsCSVReport=Y", this.ExcelDownloadUrl, this.Request.QueryString);
                        string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.Refresh, excelPaneId);
                        this.RenderFunctionLink(row, "Save as CSV", navigationUrl);
                    }

                    if (this.SaveAsExcelOnSearches)
                    {
                        string excelUrl = string.Format("{0}?{1}", this.ExcelDownloadUrl, this.Request.QueryString);
                        string navigationUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}', {1}, {2});",
                            excelUrl, (int)RoundTripFunction.PerformSearch, this.PaneId);
                        this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
                    }
					
					if (this.SaveAsExcelExtractOnSearches)
                    {
						int excelPaneId = 0;
                        try 
						{
							excelPaneId = this.GetPaneIdFromName("Excel", this.PaneId);
						} catch {}
						
						if (excelPaneId != 0)
						{
							string excelUrl = string.Format("{0}?{1}&SaveAsExcelReport=Y", this.ExcelDownloadUrl, this.Request.QueryString);
							string navigationUrl = string.Format("javascript:PosseSubmitLink('{0}', {1}, {2});",
								excelUrl, (int)RoundTripFunction.Refresh, excelPaneId);
							this.RenderFunctionLink(row, "Save as Excel", navigationUrl);
						}
                    }
                }

                //if (!this.DisableCancel)
                //{
                    //this.RenderFunctionLink(row, "Cancel", this.HomePageUrl);
                //}

                table = new HtmlTable();
                table.Controls.Add(row);
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 2;

                container.Controls.Add(table);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" +
                        defaultFunction.ClientID + "')";
                }
            }
        }

        protected virtual List<OrderedDictionary> GetExtJsToDoListData()
        {
            List<OrderedDictionary> data = this.GetPaneData(true);
            return data;
        }

        protected virtual List<OrderedDictionary> GetExtJsToDoListItemData(int objectId)
        {
            List<OrderedDictionary> data = GetData("ToDoListInfo", objectId);
            return data;
        }

        /// <summary>
        /// Returns the JavaScript to complete the upload process.
        /// </summary>
        /// <param name="description">The description data to be added.</param>
        /// <returns>The java script.</returns>
        protected string GetUploadCompleteScript(string description)
        {
            return this.GetUploadCompleteScript(description, false);
        }

        /// <summary>
        /// Returns the java script to complete the upload process.
        /// </summary>
        /// <param name="description">The description data to be added.</param>
        /// <param name="autoSubmit">If true then make a call to PosseSubmit() when updating this document.</param>
        /// <returns>The java script.</returns>
        protected virtual string GetUploadCompleteScript(string description, bool autoSubmit)
        {
            string result = null, xml = null;
            string endPoint = this.Request.QueryString["UploadEndPoint"];
            string DocumentId = this.Request.QueryString["DocumentId"];
            string newObjectId = "NEW" + DateTime.Now.Millisecond.ToString();

            if (string.IsNullOrEmpty(endPoint))
            {
                result = string.Format(
                    "opener.Posse{0}Uploaded({1}, \"{2}\", \"{3}\", \"{4}\", \"{5}\");window.close();",
                    this.UniqueId, this.LastUploadDocumentId, this.LastUploadFileName,
                    this.LastUploadSize, this.LastUploadExtension, this.LastUploadContentType);
            }
            else
            {
                if (string.IsNullOrEmpty(DocumentId))
                {
                    xml = string.Format("<object id=\"{0}\" objectdef=\"{1}\" action=\"Insert\">" +
                        "<column name=\"Description\"><![CDATA[{2}]]></column>" +
                        "<column name=\"FileName\"><![CDATA[{3}]]></column>" +
                        "<column name=\"FileSize\">{4}</column>" +
                        "<column name=\"CreatedBy\"><![CDATA[{5}]]></column>" +
                        "<pendingdocument id=\"{6}\"/></object>" +
                        "<object id=\"{7}\"><relationship endpoint=\"{8}\" toobjectid=\"{0}\" action=\"Insert\">" +
                        "</relationship></object>",
                        newObjectId, this.Request.QueryString["UploadObjectDef"], description,
                        this.LastUploadFileName, this.LastUploadSize, this.LastUploadCreatedBy,
                        this.LastUploadDocumentId, this.ObjectId, endPoint);
                }
                else
                {
                    xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                        "<column name=\"Description\"><![CDATA[{1}]]></column>" +
                        "<column name=\"FileName\"><![CDATA[{2}]]></column>" +
                        "<column name=\"FileSize\">{4}</column>" +
                        "<column name=\"CreatedBy\"><![CDATA[{3}]]></column>" +
                        "<pendingdocument id=\"{5}\"/></object>",
                        DocumentId, description,
                        this.LastUploadFileName, this.LastUploadSize, this.LastUploadCreatedBy,
                        this.LastUploadDocumentId);
                }

                // First encode the xml as a javascript string literal to ensure that special characters such as double
                // quotes do not break the script.
                if (autoSubmit)
                {
                    result = string.Format("opener.PosseAppendChangesXML({0});opener.PosseSubmit(null);window.close();",
                        this.EncodeJSString(xml));
                }
                else
                {
                    result = string.Format("opener.PosseAppendChangesXML({0});opener.PosseNavigate(null);window.close();",
                        this.EncodeJSString(xml));
                }
            }

            return result;
        }

        /// <summary>
        /// Displays the given text in the title band.
        /// </summary>
        /// <param name="container">A reference to the target container.</param>
        /// <param name="text">Title text to be displayed.</param>
        protected override void RenderTitle(WebControl container, string text)
        {
            Label label;
            Image img;
            Panel panel;
            string iconImageName = null;
            OrderedDictionary paneData;

            if (container != null)
            {
                panel = new Panel();


                try
                {

                    if (this.HasPresentation)
                    {
                        string PresentationKey = Convert.ToString(this.PaneId);
                        if (!this.IconImages.ContainsKey(PresentationKey)) // Find the Icon Name if it is not already stored in memory
                        {
                            paneData = this.GetPaneData(true)[0];

                            if (Request.QueryString["IconName"] != null)
                            {
                                iconImageName = Request.QueryString["IconName"];
                                this.IconImages[PresentationKey] = iconImageName;
                            }
                            else if (paneData.Contains("ImageName"))
                            {

                                string imageName = paneData["ImageName"].ToString();
                                if (imageName != null)
                                    iconImageName = imageName;
                            }
                        }

                        if (PresentationKey != null && (iconImageName != null || this.IconImages.ContainsKey(PresentationKey)))
                        {
                            img = new Image();
                            if (this.IconImages.ContainsKey(PresentationKey))
                            {
                                img.ImageUrl = IconPath + this.IconImages[PresentationKey];
                            }
                            else
                            {
                                img.ImageUrl = IconPath + iconImageName;
                            }
                            panel.Controls.Add(img);
                        }
                    }
                }
                catch (Exception exception)
                {
                }

                label = new Label();
                label.Text = text;
                panel.Controls.Add(label);

                container.Controls.Add(panel);
            }
        }

        /// <summary>
        /// Renders the menu pane.
        /// </summary>
        /// <param name="titleContainer">Container to render the menu title into.</param>
        /// <param name="container">Container to render the menu into.</param>
        /// <param name="menuName">The name of the menu to render.</param>
        protected override void RenderMenuPane(WebControl container, WebControl titleContainer, WebControl imageContainer)
        {
            int index = 0, colSpan = 0, groupIndex;
            string link, url, singleEntrypointURL = "";
            Label label;
            Literal literal;
            Panel panel;
            MenuInfo menu, currentMenu;
            HtmlTable table = new HtmlTable();
            HtmlTableRow row = null, groupRow = null;
            HtmlTableCell newCell;
            List<HtmlTableCell> cells = new List<HtmlTableCell>();

            if (container != null)
            {
                LoadMenu(this.MenuName);
                menu = this.Menus[this.MenuName];

                // Build the breadcrumb
                literal = new Literal();
                literal.Text = menu.Label;
                if (menu.ParentMenu != null)
                {
                    currentMenu = menu;
                    while (currentMenu.ParentMenu != null && currentMenu.ParentMenu.Name != this.MainMenuName)
                    {
                        link = String.Format("<a class=\"breadcrumb\" href=\"{0}?PosseMenuName={1}\">{2}</a>",
                            this.HomePageUrl, currentMenu.ParentMenu.Name, currentMenu.ParentMenu.Label);
                        literal.Text = String.Format("{0}&nbsp;&gt;&nbsp;{1}", link, literal.Text);
                        currentMenu = currentMenu.ParentMenu;
                    }
                }
                link = String.Format("<a class=\"breadcrumb\" href=\"{0}\">Home</a>", this.HomePageUrl);
                literal.Text = String.Format("<div class=\"breadcrumb\">{0}&nbsp;&gt;&nbsp;{1}</div>", link, literal.Text);
                titleContainer.Controls.Add(literal);

                // Add the title and image if necessary
                if (menu.Label != null)
                {
                    this.RenderTitle(titleContainer, menu.Label);
                }

                if (this.PaneMenus.ContainsKey(this.MenuName))
                {
                    container.Controls.Add(this.PaneMenus[this.MenuName]);
                }
                else
                {
                    foreach (MenuEntryPointGroup group in menu.EntryPointGroups)
                    {
                        if (row != null)
                        {
                            foreach (HtmlTableCell cell in cells)
                            {
                                cell.VAlign = "Top";
                                literal = new Literal();
                                literal.Text = "</ul>";
                                cell.Controls.Add(literal);

                                row.Cells.Add(cell);
                            }
                        }

                        // Render the group if there are entrypoints visible
                        if (group.EntryPointCount > 0)
                        {
                            if (group.SameRow && cells.Count > 0)
                            {
                                cells.Add(new HtmlTableCell());
                                index = cells.Count - 1;
                            }
                            else
                            {
                                cells.Clear();
                                cells.Add(new HtmlTableCell());
                                index = 0;
                                groupRow = new HtmlTableRow();
                                table.Rows.Add(groupRow);
                                row = new HtmlTableRow();
                                table.Rows.Add(row);
                            }

                            if (group.ColSpan != null)
                                colSpan = group.ColSpan;
                            else
                                colSpan = 0;
                            if (group.MultiColumn)
                            {
                                for (int colIndex = 1; colIndex < colSpan; colIndex++)
                                {
                                    newCell = new HtmlTableCell();

                                    literal = new Literal();
                                    literal.Text = "<ul class=\"menupanelist\">";
                                    newCell.Controls.Add(literal);

                                    cells.Add(newCell);
                                }
                            }
                            else
                            {
                                cells[index].ColSpan = colSpan;
                            }

                            groupIndex = index;
                            this.RenderMenuPaneHeader(group, table, groupRow);

                            literal = new Literal();
                            literal.Text = "<ul class=\"menupanelist\">";
                            cells[index].Controls.Add(literal);

                            // Generate entry point links
                            foreach (MenuEntryPoint item in group.EntryPoints)
                            {
                                if (item.SubMenu != null && item.SubMenu.EntryPointCount > 0)
                                    url = String.Format("{0}?PosseMenuName={1}", this.HomePageUrl, item.SubMenu.Name);
                                else
                                    url = item.Url;

                                //if we only have one Entry point, we automatically navigate to it
                                if (group.EntryPointCount == 1)
                                {
                                    singleEntrypointURL = url;
                                    Response.Redirect(singleEntrypointURL);
                                }

                                this.RenderMenuPaneItem(cells[index], url, item.Help, item.Label, (item.SubMenu != null));
                                if (group.MultiColumn)
                                {
                                    if (index < groupIndex + colSpan - 1)
                                        index++;
                                    else
                                        index = groupIndex;
                                }
                            }
                        }

                        foreach (HtmlTableCell cell in cells)
                        {
                            cell.VAlign = "Top";
                            literal = new Literal();
                            literal.Text = "</ul>";
                            cell.Controls.Add(literal);

                            row.Cells.Add(cell);
                        }

                        if (row == null)
                        {
                            label = new Label();
                            label.Text = "No options available";
                            panel = new Panel();
                            panel.Controls.Add(label);

                            this.PaneMenus[this.MenuName] = (Control)panel;
                            container.Controls.Add(panel);
                        }
                        else
                        {
                            table.Rows.Add(row);

                            this.PaneMenus[this.MenuName] = (Control)table;
                            container.Controls.Add(table);
                        }
                    }
                }
            }
        }

        #endregion
    }
}