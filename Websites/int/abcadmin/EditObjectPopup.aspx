<%@ Page Language="C#" MasterPageFile="~/OutriderPopup.master" AutoEventWireup="true" CodeFile="EditObjectPopup.aspx.cs" Inherits="Computronix.POSSE.Outrider.EditObjectPopup" Title="EditObjectPopup" %>
<asp:Content ID="cntTitleBand" runat="server" ContentPlaceHolderID="cphTitleBand">
	<asp:Panel ID="pnlTitleBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntHeaderBand" runat="server" ContentPlaceHolderID="cphHeaderBand">
    <asp:Panel ID="pnlHeaderBand" runat="server">
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntHelpBand" runat="server" ContentPlaceHolderID="cphHelpBand">
	<asp:Panel ID="pnlHelpBand" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="cntTabLabelBand" runat="server" ContentPlaceHolderID="cphTabLabelBand">
    <asp:Panel ID="pnlTabLabelBand" runat="server">
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntTopFunctionBand" runat="server" ContentPlaceHolderID="cphTopFunctionBand">
    <asp:Panel ID="pnlTopFunctionBand" runat="server">
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntPaneBand" ContentPlaceHolderID="cphPaneBand" Runat="Server">
    <asp:Panel ID="pnlPaneBand" runat="server">
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntBottomFunctionBand" ContentPlaceHolderID="cphBottomFunctionBand" Runat="Server">
    <asp:Panel ID="pnlBottomFunctionBand" runat="server">
    </asp:Panel>
</asp:Content>
