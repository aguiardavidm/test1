using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class UploadNew : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string posseAutoSave = "false";
            string clearOutcome = "false";
            if ((this.Request.QueryString["PosseAutoSave"] ?? "").ToLower() == "y")
            {
                posseAutoSave = "true";
                if ((this.Request.QueryString["PosseIsProcessType"] ?? "").ToLower() == "y")
                    clearOutcome = "true";
            }

            Literal script = new Literal();
            string val = @"<script>
                $(document).ready(function () {{
                    PossePLUpload({{
                        id: ""#uploader"",
                        opener: opener,
                        objectHandle: ""{0}"",
                        endPointName: ""{1}"",
                        uploadObjectDef: ""{2}"",
                        uploadObjectDefId: ""{3}"",
                        filters: [
                            {{ title: ""Document files"", extensions: ""doc,docx,xls,xlsx,pdf,txt"" }},
                            {{ title: ""Image files"", extensions: ""jpg,jpeg,gif,png"" }},
                            {{ title: ""Zip files"", extensions: ""zip"" }}
                        ],
                        max_file_size: ""20mb"",
                        posse_include_description: true,
                        //posse_custom_details: {{DocDetailName: ""Value"", DocDetailName2: ""Value""}},
                        posse_auto_save: {4},
                        posse_clear_outcome: {5}
                    }})
                }});
                </script>";
            script.Text = string.Format(val,
                this.Request.QueryString["PosseObjectId"].ToString(), this.Request.QueryString["UploadEndPoint"].ToString(),
                this.Request.QueryString["UploadObjectDef"].ToString(), this.Request.QueryString["PosseObjectDefId"].ToString(),
                posseAutoSave, clearOutcome);
            this.scriptPlaceholder.Controls.Add(script);
        }

        #endregion
    }
}
