using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;


namespace Computronix.POSSE.Outrider
{
    public partial class _Default : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Ensure the outer ASPX form is never submitted.  Works around browser behaviour that submits single field forms when [Enter] is pressed.
            this.Form.Attributes["onsubmit"] = "return false";

            this.LoadPresentation();
            this.RenderUserInfo(this.pnlUserInfo);
            this.RenderMenuBand(this.pnlMenuBand);
            this.RenderErrorMessage(this.pnlPaneBand);

            
            //Start logging to investigate the Menu band randomly disappering.
            StringBuilder sb = new StringBuilder(); 
            StringWriter sw = new StringWriter(sb); 
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            this.pnlMenuBand.RenderControl(hw); 

            if (sb.ToString().Length < 200)
            {
                this.OutriderNet.LogMessage(LogLevel.Error, "****************************************************************");
                this.OutriderNet.LogMessage(LogLevel.Error, "Recording Information for Error in Menu Band");
                this.OutriderNet.LogMessage(LogLevel.Error, "URL: " + HttpContext.Current.Request.Url.ToString());
                this.OutriderNet.LogMessage(LogLevel.Error, "Navigation Menu Html Rendering:");
                this.OutriderNet.LogMessage(LogLevel.Error, sb.ToString() + "MenuHtml");
                this.pnlUserInfo.RenderControl(hw);
                this.OutriderNet.LogMessage(LogLevel.Error, "User Information Html Rendering:");
                this.OutriderNet.LogMessage(LogLevel.Error, sb.ToString());
                this.OutriderNet.LogMessage(LogLevel.Error, "Session Id: " + this.SessionId);
                HttpBrowserCapabilities bc = Request.Browser;
                this.OutriderNet.LogMessage(LogLevel.Error, "Browser Info: " + bc.Type + " " + bc.Version);
            }

            //End Logging for the menu disappering.

            if (this.HasPresentation)
            {
                if (this.IsGuest && !this.GetCondition(this.CurrentPaneId, "ShowGuest"))
                {
                    //redirect to login
                    this.ShowLogin("");
                }

                this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
                this.RenderHelpBand(this.pnlHelpBand);
                this.RenderHeaderPane(this.pnlHeaderBand, this.pnlTopBand);
                if (this.EmbedCriteriaOnSearches)
                {
                    this.RenderSearchCriteriaPane(this.pnlSearchCriteriaBand);
                }
                else
                {
                    this.pnlSearchCriteriaBand.Visible = false;
                    this.RenderTabLabelBand(this.pnlTabLabelBand);
                }
                
                if (this.PresentationName != "ToDoList") //Don't render the pane if this is a TO DO List
                {
                    this.RenderTopFunctionBand(this.pnlTopFunctionBand);
                    this.CheckClient();
                    this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
                }
                this.RenderBottomFunctionBand(this.pnlBottomFunctionBand);

                // For reports we need to fix the height of the pane so the embedded PDF 
                // fills the page.  Add bottom padding for Firefox.  This assumes that a report
                // pane is named "ReportPane...".
                if (this.PaneName.StartsWith("ReportPane"))
                {
                    this.pnlPaneBand.Style["height"] = "600px";
                    this.pnlPaneBand.Style["padding-bottom"] = "5px";
                }
            }
            else
            {
                // Redirect Guest to login 
                if (this.IsGuest)
                {
                    //redirect to login
                    this.ShowLogin("");
                }

                if (!string.IsNullOrEmpty(this.MenuName))
                {
                    this.RenderMenuPane(this.pnlPaneBand, this.pnlTitleBand, this.pnlTopBand);
                }
                else
                {
                    string startMenuName = ConfigurationManager.AppSettings["StartMenuName"];
                    if (startMenuName != null)
                    {
                        Response.Redirect(String.Format("Default.aspx?PosseMenuName={0}", startMenuName));
                    }
                    else
                    {
                        this.RenderGreeting(this.pnlPaneBand);
                    }
                }
            }

            if (this.ShowDebugSwitch)
            {
                this.RenderDebugLink(this.pnlDebugLinkBand);
            }
            this.RenderFooterBand(this.pnlFooterBand);
        }
        #endregion

        // ProcessExtJsToDoListData
        private List<string> ProcessExtJsToDoListData()
        {
            List<OrderedDictionary> data = GetExtJsToDoListData();
            List<string> dataRows = new List<string>();
            string objectHandle = string.Empty;
            foreach (OrderedDictionary dic in data)
            {
                string dataRow = "[";
                foreach (DictionaryEntry entry in dic)
                {
                    if (entry.Value != null)
                    {
                        dataRow = String.Format("{0}'{1}',", dataRow, entry.Value.ToString().Replace("'", "\\'"));
                    }
                    else
                        dataRow = String.Format("{0}'{1}',", dataRow, entry.Value);
                    //if (entry.Key.ToString().ToLower() == "objectid")
                    //    objectHandle = entry.Value.ToString();
                }
                //Get additional data from a recordset
                if (!String.IsNullOrEmpty(objectHandle))
                {
                    List<OrderedDictionary> processData = GetExtJsToDoListItemData(Convert.ToInt32(objectHandle));
                    foreach (OrderedDictionary dataItem in processData)
                    {
                        foreach (DictionaryEntry entry in dataItem)
                        {
                            if (entry.Value != null)
                            {
                                dataRow = String.Format("{0}'{1}',", dataRow, entry.Value.ToString().Replace("'", "\\'"));
                            }
                            else
                                dataRow = String.Format("{0}'{1}',", dataRow, entry.Value);
                        }
                    }
                }
                //Check last item in row before capping off with a square bracket.
                if (dataRow.Length > 1)
                    dataRow = dataRow.Substring(0, dataRow.Length - 1) + "]";
                dataRows.Add(dataRow);
            }
            return dataRows;
        }

        protected void RenderToDoListJavascript()
        {

            if (this.PresentationName == "ToDoList")
            {
                //Write top half :)
                Response.Write(@"/*!
                                 * Ext JS Library 3.3.1
                                 * Copyright(c) 2006-2010 Sencha Inc.
                                 * licensing@sencha.com
                                 * http://www.sencha.com/license
                                 */
                                Ext.onReady(function(){
                                    Ext.QuickTips.init();
                                
                                    function formatDate(value){
                                            return value ? value.dateFormat('M d, Y') : '';
                                        }
                                    ");
                Response.Write("var mapHtml = [" +
                               "    '<div id=\"map_canvas\">'," +
                               "    '</div>'" +
                               " ];" +
                               "   ");

                Response.Write(@"var fm = Ext.form;
                                
                                    // sample static data for the store
                                    var toDoData = [
                                        ");
                List<string> listItems = ProcessExtJsToDoListData();
                for (int i = 0; i < listItems.Count; i++)
                {
                    if (i < (listItems.Count - 1))
                    {
                        Response.Write(listItems[i] + @",
                                                       ");
                    }
                    else
                    {
                        Response.Write(listItems[i] + @"
                                                       ");
                    }
                }

                //Render the bottom half :)
                Response.Write(@"];

                                // create the data store
                                var reader = new Ext.data.ArrayReader({}, [
                                       {name: 'processtype'},
                                       {name: 'jobnumber'},
                                       {name: 'jobid'},
                                       {name: 'startdate', type: 'date', dateFormat: 'n/j/Y h:i:s A'},
                                       {name: 'duedate', type: 'date', dateFormat: 'n/j/Y h:i:s A'},
                                       {name: 'assignments'},
                                       //Begin columns not shown in grid
                                       {name: 'objectid'},
                                       {name: 'userid'},
                                       {name: 'gotoobjectid'},
                                       {name: 'gotopresentationname'},
                                       {name: 'processdescription'},
                                       {name: 'jobaddress'},
                                       {name: 'jobtype'},
                                       {name: 'jobstatus'},
                                       {name: 'licenseestablishment'},
                                       {name: 'routeorder'}
                                    ]);
                            
                                var store = new Ext.data.GroupingStore({
                                                reader: reader,
                                                data: toDoData,
                                                groupField:'startdate',
                                                sortInfo: {
                                                    field: 'startdate',
                                                    direction: 'ASC'
                                                }
                                            });
                            
                                var columnModel = new Ext.grid.ColumnModel({
                                    columns: [
                                        {
                                            id       : 'routeorder',
                                            header   : 'Route',
                                            width    : 40,
                                            dataIndex: 'routeorder',
                                            tooltip  : 'The estimated order of completion for inspections.',
                                            editor: new fm.TextField({
                                                        updateEl: true,
                                                        cancelOnEsc: true,
                                                        updateOnEnter: true,
                                                        listeners: {
                                                            focus: function(editor) {
                                                                editor.selectText();
                                                            }
                                                        }
                                                    })
                                        },
                                        {
                                            id       :'processtype',
                                            header   : 'Process Type', 
                                            width    : 160, 
                                            dataIndex: 'processtype'
                                        },
                                        {
                                            id       :'jobnumber',
                                            header   : 'Job Number', 
                                            width    : 85, 
                                            dataIndex: 'jobnumber'
                                        },                                      
                                        {
                                            id       : 'startdate',
                                            header   : 'Scheduled Start', 
                                            width    : 85, 
                                            renderer : Ext.util.Format.dateRenderer('m/d/Y'), 
                                            dataIndex: 'startdate',
                                            editor: new fm.DateField({
                                                        format: 'm/d/y',
                                                        updateEl: true,
                                                        cancelOnEsc: true,
                                                        updateOnEnter: true,
                                                        ignoreNoChange: true,
                                                        listeners: {
                                                            focus: function(editor) {
                                                                editor.selectText();

                                                            }
                                                        }
                                                    })
                                        },
                                        {
                                            id       :'licenseestablishment',
                                            header   : 'Establishment', 
                                            width    : 160, 
                                            dataIndex: 'licenseestablishment'
                                        }
                                    ],
                                    defaults: {
                                        sortable: true
                                    }
                                });

                                // manually load local data
                                store.loadData(toDoData);

                                var dateRouting = new Ext.form.DateField({
                                        id: 'dateRouting',
                                        value: new Date()
                                    });
                            
                                var routingStore = new Ext.data.GroupingStore({
                                                        reader: reader,
                                                        data: toDoData,
                                                        sortInfo: {
                                                            field: 'routeorder',
                                                            direction: 'ASC'
                                                        }
                                                    });

                                 var routeButtonHandler = function(button,event) {
                                                var dateValue = dateRouting.getValue();
                                                var addresses = [];
                                                var waypts = [];
                                                var counter = 1;
                                                var start;
                                                var end;
                                                routingStore.each(function(e) {
                                                 if (e.data['startdate'] != null) {  
                                                   
                                                   if (dateValue.format('Y-m-d').toString() == e.data['startdate'].format('Y-m-d').toString()) {
                                                       if (counter == 1) {
                                                          start = e.data['jobaddress'];
                                                       }
                                                       if (counter > 1) {      
                                                            waypts.push({
                                                                location:e.data['jobaddress'],
                                                                stopover:true
                                                           });
                                                           end = e.data['jobaddress']; 
                                                       }
                                                       counter = counter + 1;
                                                   }
                                                 }
                                                });                                          
                                     
                                     	        //remove the last record as we have it as the destination
                                     	        waypts.pop();
                                          
                                                  var request = {
                                                      origin: start,
                                                      destination: end,
                                                      waypoints: waypts,
                                                      optimizeWaypoints: true,
                                                      travelMode: google.maps.TravelMode.DRIVING
                                                  };

                                                  directionsService.route(request, function(response, status) {
                                                    if (status == google.maps.DirectionsStatus.OK) {
                                                      directionsDisplay.setDirections(response);
                                                      var route = response.routes[0];
                                                  
                                                    }
                                                  });

                                            };
                            
                                // create the Grid
                                var grid = new Ext.grid.EditorGridPanel({
                                    store: store,
                                    colModel: columnModel,
                                    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
                                    view: new Ext.grid.GroupingView({
                                                forceFit:true,
                                                groupTextTpl: '{text}'
                                            }),
		                            viewConfig: {
			                            forceFit: true
		                            },
                                    stripeRows: true,
                                    width: 660,
                                    split: true,
		                            region: 'center',
                                    title: 'To Do List',
                                    autoExpandColumn: 'process',
                                    clicksToEdit: 1
                                });

                    ");
                //this is for specialized string handling for markup
                Response.Write("var processTplMarkup = [\n" +
                               "    '<span class=\"toDoHeader\">{processtype}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Job Number:</span> <span class=\"toDoField\">{jobnumber}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Job Type:</span> <span class=\"toDoField\">{jobtype}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Job Status:</span> <span class=\"toDoField\">{jobstatus}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Assignments:</span> <span class=\"toDoField\">{assignments}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Establishment:</span> <span class=\"toDoField\">{licenseestablishment}</span><br/>',\n" +
                               "    '<span class=\"toDoLabel\">Address:</span> <span class=\"toDoField\">{jobaddress}</span><br/>',\n" +
                               "    '<div class=\"toDoButtonContainer\">',\n" +
                               "    '   <div class=\"button\"><a href=\"Default.aspx?PossePresentation=Default&PosseObjectId={jobid}\"><span>Go To Job</span></a></div>',\n" +
                               "    '   <div class=\"button\"><a href=\"Default.aspx?PossePresentation={gotopresentationname}&PosseObjectId={gotoobjectid}\"><span>Go To Process</span></a></div>',\n" +
                               "    '   <div class=\"button\"><a href=\"javascript:PossePopup(\\'Assignments_{objectid}\\', \\'ProcessAssignment.aspx?PosseObjectId={objectid}\\', 250, 725, \\'Assignments_{objectid}\\')\"><span>Re-Assign</span></a></div>',\n" +
                               "    '</div>'\n" +
                               "];\n");
                Response.Write(@"var processTpl = new Ext.Template(processTplMarkup);
                            
                                var ct = new Ext.Panel({
	                                renderTo: 'todolist',
	                                frame: true,
	                                width: 960,
                                    height: 700,
	                                layout: 'border',
	                                items: [
                                        {   id: 'centerPanel',
                                            region: 'center',
	                                layout: 'border',
	                                items: [
                                                {
                                                    id: 'mapPanel',
                                                    region: 'south',
                                                    height: 450, 
                                                    split: true,
                                                    title: 'Map',
                                                    bodyStyle: {
                                                        background: '#F7F7F7',
                                                        padding: '7px'
                                                    },
                                                    html: mapHtml.join(''),
                                                    bbar: new Ext.Toolbar({
                                                            items: ['Route by date (choose a date and click \'Route\')','->',
                                                                dateRouting,
                                                            {
                                                                text: 'Route',
                                                                handler:routeButtonHandler
                                                            }]
                                                        })
                                                },
                                                grid
                                            ]
                                        },                                        
		                                {
			                                id: 'detailPanel',
			                                region: 'east',
                                            width: 300,
                                            collapsible: true, 
                                            split: true,
                                            title: 'Details',
			                                bodyStyle: {
				                                background: '#F7F7F7',
				                                padding: '7px'
			                                },
			                                html: 'Please select a process to see additional details.'
		                                }
                                    
	                                ]
                                });

                                grid.getSelectionModel().on('rowselect', function(sm, rowIdx, r) {
                                    var detailPanel = Ext.getCmp('detailPanel');
	                                processTpl.overwrite(detailPanel.body, r.data);
                                });
                                grid.on('afteredit', function(e) {
                                    var objId = e.record.get('objectid');
                                    if (e.column === 3)
                                        PosseChangeColumn(objId, 'ScheduledStartDate', e.value.format('Y-m-d').toString());
                                    if (e.column === 0)
                                        PosseChangeColumn(objId, 'RouteOrder', e.value.toString());
                                });
                                grid.on('rowclick', function(t, r, e) {
                                    var store = t.getStore();
                                    var record = store.getAt(r);
                                    var address = record.get('jobaddress');
                                    var title = record.get('licenseestablishment');
                                    codeGoogleMapAddress(address, title);
                                });
                                initializeGoogleMap(); 
                                store.load();
                            });");

                Response.Write(@"");

            }
        }
    }
}
