using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class ConfirmObjectPopup : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder script = new StringBuilder();

            string pageUrl = "ConfirmObjectPopup.aspx";
            bool isNew = false;

            if (!string.IsNullOrEmpty(this.FromObjectId) && !string.IsNullOrEmpty(this.EndPoint))
            {
                pageUrl += "?FromObjectId=" + this.FromObjectId + "&EndPoint=" + this.EndPoint + "&RelatedInfoGrid=" + Request.QueryString["RelatedInfoGrid"];
                isNew = true;
            }

            string savePressed = Request.QueryString["SavePressed"];
            string relatedInfoGrid = Request.QueryString["RelatedInfoGrid"];

            this.HomePageUrl = this.RootUrl + pageUrl;
            this.LoadPresentation(true);

            if (this.RoundTripFunction == RoundTripFunction.Submit && !(savePressed == "Y"))
            {
                this.CheckRedirect();
            }
            this.RenderErrorMessage(this.pnlPaneBand);

            if (this.HasPresentation && this.RoundTripFunction == RoundTripFunction.Submit &&
              string.IsNullOrEmpty(this.ErrorMessage) && savePressed == "Y")
            {
                if (isNew && string.IsNullOrEmpty(relatedInfoGrid))
                {
                    script.AppendFormat("opener.PosseAppendChangesXML(unescape('<object id=\"{0}\" action=\"Update\"><relationship id=\"NEW1\" action=\"Insert\" endpoint=\"{1}\" toobjectid=\"{2}\"/></object>'));",
                      this.FromObjectId, this.EndPoint, this.GetPaneData(true)[0]["objecthandle"]);
                }
                script.Append("opener.PosseSubmit(null); window.close();");
                this.ClientScript.RegisterStartupScript(this.GetType(), "Submit", script.ToString(), true);

            }

            if (this.HasPresentation)
            {
                this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
                this.RenderHelpBand(this.pnlHelpBand);
                this.RenderHeaderPane(this.pnlHeaderBand, this.pnlTopFunctionBand);
                this.RenderTabLabelBand(this.pnlTabLabelBand);
                this.CheckClient();
                this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
                this.RenderWizardPopFunctionBand(this.pnlBottomFunctionBand);
            }

        }
        #endregion

        protected void RenderWizardPopFunctionBand(WebControl container)
        {
            HtmlTable table;
            HtmlTableRow row;
            HtmlTableCell cell;
            FunctionLinkBase defaultFunction = null;

            if (container != null)
            {
                row = new HtmlTableRow();

                if (!this.DisableCancel)
                {
                    this.RenderFunctionLink(row, "Cancel", "javascript:window.close();");
                }

                if (this.PresentationType == PresType.Wizard)
                {
                    this.RenderFunctionLink(row, "Back", RoundTripFunction.Previous);
                    this.RenderFunctionLink(row, "Next", RoundTripFunction.Next);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Submit]))
                {
                    //this.RenderFunctionLink(row, "Save", RoundTripFunction.Submit);
                    string delimiter = "?";
                    if (this.HomePageUrl.IndexOf('?') >= 0)
                    {
                        delimiter = "&";
                    }
                    string saveUrl = string.Format("javascript:PosseSubmitLink('{0}{1}SavePressed=Y', {2}, {3});", this.HomePageUrl, delimiter, 2, this.PaneId);
                    this.RenderFunctionLink(row, "Confirm & Close", saveUrl);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Search]))
                {
                    this.RenderFunctionLink(row, "Search Again", RoundTripFunction.Search);
                }

                if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]))
                {
                    defaultFunction = this.RenderFunctionLink(row, "Search", RoundTripFunction.PerformSearch);
                }

                cell = new HtmlTableCell();
                cell.Width = "100%";
                row.Cells.Add(cell);

                table = new HtmlTable();
                table.Controls.Add(row);
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 0;
                table.Width = "100%";

                container.Controls.Add(table);

                if (defaultFunction != null)
                {
                    this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" +
                      defaultFunction.ClientID + "')";
                }
            }
        }
    }
}