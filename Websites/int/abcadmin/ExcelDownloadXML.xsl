<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:user="urn:my-scripts"
  xmlns:o="urn:schemas-microsoft-com:office:office"
  xmlns:x="urn:schemas-microsoft-com:office:excel"
  xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >

  <xsl:output method="xml" omit-xml-declaration="no" indent="yes" encoding="utf-8"/>

  <xsl:template match="/">
    <xsl:processing-instruction name="mso-application">progid="Excel.Sheet"</xsl:processing-instruction>
    <Workbook
      xmlns="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:html="http://www.w3.org/TR/REC-html40">

      <Styles>
        <Style ss:ID="Default" ss:Name="Normal">
          <Alignment ss:Vertical="Bottom"/>
          <Borders/>
          <Font/>
          <Interior/>
          <NumberFormat/>
          <Protection/>
        </Style>
        <Style ss:ID="Header">
          <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"    ss:Color="#C0C0C0"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"    ss:Color="#C0C0C0"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#C0C0C0"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#C0C0C0"/>
          </Borders>
          <Font ss:Size="9" ss:Color="#000000" ss:Bold="1"/>
          <Interior ss:Color="#969696" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="Item">
          <Alignment ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
          </Borders>
          <Font ss:Size="9" ss:Color="#000000"/>
          <Interior/>
        </Style>
        <Style ss:ID="ItemBold">
          <Alignment ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
          </Borders>
          <Font ss:Size="9" ss:Color="#000000" ss:Bold="1"/>
          <Interior/>
        </Style>
        <Style ss:ID="ItemDate">
          <Alignment ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
          </Borders>
          <Font ss:Size="9" ss:Color="#000000"/>
          <NumberFormat ss:Format="Mmm d, yyyy;@"/>
          <Interior/>
        </Style>
        <Style ss:ID="AltItem">
          <Alignment ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#C0C0C0"/>
          </Borders>
          <Font ss:Size="9" ss:Color="#000000"/>
          <!--<Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>-->
        </Style>
        <Style ss:ID="AltItemDate">
          <Alignment ss:Vertical="Top" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#C0C0C0"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"  ss:Color="#C0C0C0"/>
          </Borders>
          <Font ss:Size="9" ss:Color="#000000"/>
          <NumberFormat ss:Format="Mmm d, yyyy;@"/>
          <!--<Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>-->
        </Style>
      </Styles>

      <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
        <Author>Exported from e-Licensing</Author>
      </DocumentProperties>

      <Worksheet ss:Name="Data">
        <Table>
          <xsl:for-each select="(Report/Table/Data)[1]">
            <xsl:for-each select="*">
              <Column ss:Width="82.5"/>
            </xsl:for-each>
          </xsl:for-each>

          <xsl:for-each select="(Report/Table/Data)[1]">
            <Row ss:Height="13.5">
              <xsl:for-each select="*">
                <Cell ss:StyleID="Header">
                  <Data ss:Type="String">
                    <xsl:value-of select="name(current())"/>
                  </Data>
                </Cell>
              </xsl:for-each>
            </Row>
          </xsl:for-each>

          <xsl:for-each select="Report/Table">
            <xsl:apply-templates />
          </xsl:for-each>
        </Table>
      </Worksheet>

      <xsl:for-each select="Report/Criteria">
        <Worksheet ss:Name="Criteria">
          <Table>
            <Column ss:Width="150"/>
            <Column ss:Width="150"/>

            <Row ss:Height="13.5">
              <Cell ss:StyleID="Header">
                <Data ss:Type="String">Criteria</Data>
              </Cell>
              <Cell ss:StyleID="Header">
                <Data ss:Type="String">Value</Data>
              </Cell>
            </Row>

            <xsl:for-each select="Parameter">
              <Row ss:Height="13.5">
                <Cell ss:StyleID="ItemBold">
                  <Data ss:Type="String">
                    <xsl:value-of select="@name"/>
                  </Data>
                </Cell>
                <xsl:choose>
                  <xsl:when test="(string-length(text())=19 or string-length(text())=10) and substring(text(),5,1)='-' and substring(text(),8,1)='-'">
                    <xsl:choose>
                      <xsl:when test="substring(text(),1,4)>='1900'">
                        <Cell ss:StyleID="ItemDate">
                          <Data ss:Type="DateTime">
                            <xsl:value-of select="concat(concat(concat(substring(text(),1,10),'T'),substring(text(),12)),'.000')"/>
                          </Data>
                        </Cell>
                      </xsl:when>
                      <xsl:otherwise>
                        <Cell ss:StyleID="Item">
                          <Data ss:Type="String">
                            <xsl:value-of select="substring(text(),1,10)"/>
                          </Data>
                        </Cell>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:when test="string(number(text()))='NaN' or substring(text(),string-length(text()),1)=' '">
                    <Cell ss:StyleID="Item">
                      <Data ss:Type="String">
                        <xsl:value-of select="text()"/>
                      </Data>
                    </Cell>
                  </xsl:when>
                  <xsl:when test="string(number(text()))!='NaN'">
                    <Cell ss:StyleID="Item">
                      <Data ss:Type="Number">
                        <xsl:value-of select="text()"/>
                      </Data>
                    </Cell>
                  </xsl:when>
                </xsl:choose>
              </Row>
            </xsl:for-each>
          </Table>
        </Worksheet>
      </xsl:for-each>
    </Workbook>
  </xsl:template>

  <xsl:template match="Data">
    <Row>
      <xsl:choose>
        <xsl:when test="position() mod 2 = 0">
          <xsl:for-each select="*">
            <xsl:choose>
              <xsl:when test="(string-length(text())=19 or string-length(text())=10) and substring(text(),5,1)='-' and substring(text(),8,1)='-'">
                <xsl:choose>
                  <xsl:when test="substring(text(),1,4)>='1900'">
                    <Cell ss:StyleID="AltItemDate">
                      <Data ss:Type="DateTime">
                        <xsl:value-of select="concat(concat(concat(substring(text(),1,10),'T'),substring(text(),12)),'.000')"/>
                      </Data>
                    </Cell>
                  </xsl:when>
                  <xsl:otherwise>
                    <Cell ss:StyleID="AltItem">
                      <Data ss:Type="String">
                        <xsl:value-of select="substring(text(),1,10)"/>
                      </Data>
                    </Cell>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:when test="string(number(text()))='NaN' or substring(text(),string-length(text()),1)=' '">
                <Cell ss:StyleID="AltItem">
                  <Data ss:Type="String">
                    <xsl:value-of select="text()"/>
                  </Data>
                </Cell>
              </xsl:when>
              <xsl:when test="string(number(text()))!='NaN'">
                <Cell ss:StyleID="AltItem">
                  <Data ss:Type="Number">
                    <xsl:value-of select="text()"/>
                  </Data>
                </Cell>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="*">
            <xsl:choose>
              <xsl:when test="(string-length(text())=19 or string-length(text())=10) and substring(text(),5,1)='-' and substring(text(),8,1)='-'">
                <xsl:choose>
                  <xsl:when test="substring(text(),1,4)>='1900'">
                    <Cell ss:StyleID="ItemDate">
                      <Data ss:Type="DateTime">
                        <xsl:value-of select="concat(concat(concat(substring(text(),1,10),'T'),substring(text(),12)),'.000')"/>
                      </Data>
                    </Cell>
                  </xsl:when>
                  <xsl:otherwise>
                    <Cell ss:StyleID="Item">
                      <Data ss:Type="String">
                        <xsl:value-of select="substring(text(),1,10)"/>
                      </Data>
                    </Cell>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:when test="string(number(text()))='NaN' or substring(text(),string-length(text()),1)=' '">
                <Cell ss:StyleID="Item">
                  <Data ss:Type="String">
                    <xsl:value-of select="text()"/>
                  </Data>
                </Cell>
              </xsl:when>
              <xsl:when test="string(number(text()))!='NaN'">
                <Cell ss:StyleID="Item">
                  <Data ss:Type="Number">
                    <xsl:value-of select="text()"/>
                  </Data>
                </Cell>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>
    </Row>
  </xsl:template>
</xsl:stylesheet>