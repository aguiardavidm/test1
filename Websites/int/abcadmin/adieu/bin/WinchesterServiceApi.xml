<?xml version="1.0"?>
<doc>
    <assembly>
        <name>WinchesterServiceApi</name>
    </assembly>
    <members>
        <member name="T:Computronix.POSSE.Winchester.Api.AppServerException">
            <summary>
            Root exception class for exceptions thrown by the application server.
            </summary>
        </member>
        <member name="T:Computronix.POSSE.Winchester.Api.AppServerTimeoutException">
            <summary>
            Exception thrown when a request to the application server times out.
            </summary>
        </member>
        <member name="T:Computronix.POSSE.Winchester.Api.InvalidTraceKeyException">
            <summary>
            Exception raised when a trace key is provided to an application server method call
            and the trace key is not present in the database.
            </summary>
        </member>
        <member name="T:Computronix.POSSE.Winchester.Api.IPosseAuthenticationProvider">
            <summary>
            Interface specification for a PosseInternal Presentation Server Authentication Provider.
            </summary>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IPosseAuthenticationProvider.Authenticate">
            <summary>
            Hook for custom authentication processing on the presentation server.
            </summary>
            <remarks>
            Implementations might read data from a request header, query for the
            current WindowsIdentity, or perform other processing related to
            authentication.
            If an exception is raised, the authentication attempt will be cancelled.
            </remarks>
        </member>
        <member name="P:Computronix.POSSE.Winchester.Api.IPosseAuthenticationProvider.AuthenticationName">
            <summary>
            The AuthenticationName as determined by the Authenticate method.
            </summary>
        </member>
        <member name="P:Computronix.POSSE.Winchester.Api.IPosseAuthenticationProvider.Evidence">
            <summary>
            Secondary data provided by the Authenticate method for use by the
            Application Server script.
            </summary>
        </member>
        <member name="T:Computronix.POSSE.Winchester.Api.IServiceApi">
            <summary>
            Interface describing all of the methods provided by the Application Server API.
            </summary>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.ConvertDateToString(System.Nullable{System.DateTime})">
            <summary>
            Converts a .Net date into a string using the POSSE format.
            </summary>
            <param name="date">.Net date to convert</param>
            <returns>String formatting the date in the POSSE format.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.ConvertStringToDate(System.String)">
            <summary>
            Converts a POSSE date-formatted string to a .Net DateTime object
            </summary>
            <param name="posseDateString">String formatted according to the POSSE format</param>
            <returns>Converted date.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.CreateAndSaveOperationalReport(System.String,System.Int32,System.String,System.String)">
            <summary>
            Execute a report and save as a document, returning the document id.
            </summary>
            <param name="sessionToken">Session Token as acquired from CreateSession identifying the session.</param>
            <param name="objectId">The object id of the object on which to run and save the report. This object id will be passed into the report as its sole parameter.</param>
            <param name="objectDefReportName">The Name as configured in Stage for this report on the object def for the object id provided.</param>
            <param name="traceKey">Trace key to log server activity to.</param>
            <returns>Document Object Id of document that report PDF was saved to.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.CreateReport(System.String,System.String,System.String,System.String)">
            <summary>
            Run a report for a report def name and return the PDF.
            </summary>
            <param name="sessionToken">
            The session token returned from NewSession().
            </param>
            <param name="reportDefName">
            Name of the report to run.
            </param>
            <param name="argumentsJson">
            All the argument values needed to run the report as a JSON array.
            </param>
            <param name="traceKey">
            When not empty the call will be executed using the debugging options set for
            this key.  Note that this key needs to be a valid trace key. (Default: null,
            indicating no debugging is to be done)
            </param>
            <returns></returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.CreateSession(System.String,System.String,System.Int32,System.String,System.String)">
            <summary>
            Authenticate to the Posse application server and obtain a session
            token to be used for subsequent calls.
            </summary>
            <param name="authenticationName">User id to authenticate as.</param>
            <param name="password">Password to authenticate with.</param>
            <param name="idleSessionTimeoutMinutes">Session will automatically expire if no actions are performed with this session token in the number of minutes specified.</param>
            <param name="authenticationHandshakeKey">Pre-shared AES key used to validate the Service API with the application server.</param>
            <param name="traceKey">Trace key to log server activity to.</param>
            <returns>Session Token if successful, null if unsuccessful.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.ExecuteStatement(System.String,System.String,System.String,System.String)">
            <summary>
            Execute a registered statement.
            </summary>
            <param name="sessionToken">Session Token as acquired for CreateSession identifying the session</param>
            <param name="registeredStatementName">Name of the registered statement as registered in Stage (case doesn't matter)</param>
            <param name="arguments">JSON-formatted list of arguments, in the style:
            {
                "bindVariableName": value,
                ...
            }
            </param>
            <param name="traceKey">Trace key to log server activity to.</param>
            <returns>A JSON representation of the data.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.ExpireSession(System.String,System.String)">
            <summary>
            Close off the session so that no further hits can use it.
            </summary>
            <param name="sessionToken">Session Token as acquired from CreateSession identifying the session.</param>
            <param name="traceKey">Trace key to log server activity to.</param>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.GetDisplayFormat(System.String,System.Int32,System.String)">
            <summary>
            Gets the display format of an object.
            </summary>
            <param name="sessionToken">Session Token as acquired from CreateSession identifying the session.</param>
            <param name="objectId">The object id of the object from which to get the display format.</param>
            <param name="traceKey">Trace key to log server activity to.</param>
            <returns>The display format of the object.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.GetDocument(System.String,System.Int32,System.String)">
            <summary>
            Get a document blob for a given document id.
            </summary>
            <param name="sessionToken">Session Token as acquired from CreateSession identifying the session.</param>
            <param name="documentId">The object id of a document object.</param>
            <param name="traceKey">Trace key to log server activity to.</param>
            <returns>Byte array with the bits of the document blob.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.GetTitle(System.String,System.Int32,System.String)">
            <summary>
            Gets the title of an object.
            </summary>
            <param name="sessionToken">Session Token as acquired from CreateSession identifying the session.</param>
            <param name="objectId">The object if of the object from which to get the title.</param>
            <param name="traceKey">Trace key to log server activity to.</param>
            <returns>The title of the object.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.GetRecordSet(System.String,System.Int32,System.String,System.String)">
             <summary>
             Execute a record set and return the results.
             </summary>
             <param name="sessionToken">Session Token as acquired from CreateSession identifying the session.</param>
             <param name="objectId">The object from which to get the record set data.</param>
             <param name="recordSetName">The name of the record set as configured in Stage.</param>
             <param name="traceKey">Trace key to log server activity to.</param>
             <returns>If the record set is a "View of Object", the result will be a JSON object like this:
             {
                 "columnName": value,
                 ...
             }
            
             For "Related Objects" record sets, the result with be a JSON list like this:
             [
                 {
                     "columnName": value,
                     ...
                 }
             ]
             </returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.HasRecordSet(System.String,System.Int32,System.String,System.String)">
            <summary>
            Determines whether a record set exists based on the object type of a supplied object id.
            </summary>
            <param name="sessionToken">Session Token as acquired from CreateSession identifying the session.</param>
            <param name="objectId">The object from which to get the record set data.</param>
            <param name="recordSetName">The name of the record set as configured in Stage.</param>
            <param name="traceKey">Trace key to log server activity to.</param>
            <returns>true if the record set exists, else false.</returns>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.Ping(System.String,System.String)">
            <summary>
            Ping the application server.
            </summary>
            <param name="sessionToken">
            The session to attach to.
            </param>
            <param name="traceKey">
            When not empty the call will be executed using the debugging options set for
            this key.  Note that this key needs to be a valid trace key. (Default: null,
            indicating no debugging is to be done)
            </param>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.IServiceApi.ProcessXml(System.String,System.String,System.String)">
            <summary>
            Insert, update, or delete POSSE data using XML which identifies the changes to be made.
            </summary>
            <param name="sessionToken">
            The session to attach to.
            </param>
            <param name="changesXml">A valid snippet of changes XML.</param>
            <param name="traceKey">Trace key to log server activity to.</param>
            <returns></returns>
        </member>
        <member name="T:Computronix.POSSE.Winchester.Api.MergeChangesFailedException">
            <summary>
            Exception thrown when a request is made to the application server for
            an object that does not exist.
            </summary>
        </member>
        <member name="T:Computronix.POSSE.Winchester.Api.ObjectDoesNotExistException">
            <summary>
            Exception thrown when a request is made to the application server for
            an object that does not exist.
            </summary>
        </member>
        <member name="T:Computronix.POSSE.Winchester.Api.ServiceApiFactory">
            <summary>
            Factory class for obtaining an instance of the Application Server API. Note that
            the implementation of the API will be in a separate assembly from the PosseAppServerApi
            assembly. Because of this, reflection is used to find the assembly containing
            the application server implementation and create an instance of it.
            </summary>
        </member>
        <member name="M:Computronix.POSSE.Winchester.Api.ServiceApiFactory.GetServiceApi(System.String,System.Int32,System.Int32)">
            <summary>
            Create an instance of the Application Server API connected to
            a given host and port.
            </summary>
            <param name="hostName">Host Name of the server running the application server.</param>
            <param name="portNumber">Port Number that the application server is listening on.</param>
            <param name="timeoutSeconds">How many seconds to wait after a call to the application server before timing out.</param>
            <returns></returns>
        </member>
        <member name="T:Computronix.POSSE.Winchester.Api.SessionExpiredException">
            <summary>
            Exception thrown when a request is made to the application server with
            a session token that has expired.
            </summary>
        </member>
    </members>
</doc>
