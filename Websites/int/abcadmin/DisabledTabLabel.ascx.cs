using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
  public partial class DisabledTabLabel : Computronix.POSSE.Outrider.TabLabelBase
  {
    #region Event Handlers
    protected void Page_Load(object sender, EventArgs e)
    {
      this.lblTabLabel.Text = this.Text;

      if (this.Condition == "Warning")
      {
        this.lblTabLabel.ForeColor = System.Drawing.Color.Red;
      }
      else if (this.Condition == "Highlight")
      {
		  this.lblTabLabel.ForeColor = System.Drawing.Color.Yellow;
          this.lblTabLabel.Font.Bold = true;
      }
    }
    #endregion
  }
}