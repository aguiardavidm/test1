// POSSE Extension Functions

var vProcessFunctionLinks = true;
var vSpellCheckFieldList = "";

function waitASec() {
  vProcessFunctionLinks = true;
}

var debug = "";
function showDebug() {
  var begin = 0;

  while (begin < debug.length) {
    for (var i = debug.indexOf('\n', begin), count = 1;
         i >= 0 && i < debug.length && count < 30;
         i = debug.indexOf('\n', i + 1), count++) {
    }
    if (i < 0 || i >= debug.length) {
      debug += "----END----";
    }
    alert(debug.substring(begin, i < 0 ? debug.length : i));
    begin = i < 0 ? debug.length : i + 1;
  }
  debug = "";
}

function disablelistprocessor() {
  var dl = document.getElementById("elementstodisablelist");
  if (dl)
  {
    var el, elList, pos
    elList = dl.value;
    pos = elList.indexOf(";")
    while (pos > -1)
    {
      el = elList.substr(0, pos);
      elList = elList.substr(pos + 1);
      var dElem = document.getElementById(el);
      if (dElem)
      {
        dElem.disabled="disabled";
      }
      pos = elList.indexOf(";")
    }
  }
}

var errorFocus = false;

//Set up the container for validating whether the PosseDataChanges is in
//synch with the currently displayed values in fields.  Checkboxes are
//a special case, as you can't safely fire their click event because it may
//actually change the value.  We need a container to hold the code that
//needs to be fired if we manually change the value in grid clicking.
var validatedValues = new Object;
var initialValues = new Object;
var initialTextareaValues = new Object;
var validatedValuesClickHandlers = new Object;

function initializeValidatedValues()
{
  for (var i=0; i < document.forms.length; i++)
  {
    var theForm = document.forms[i];
    if (theForm.id == 'aspnetForm')
    {
      for (var n=0; n < theForm.elements.length; n++)
      {
        var theElement = theForm.elements[n];
          if (theElement.type != "hidden") 
		  {
	          switch (theElement.nodeName)
	          {
	            case 'INPUT' :
	              if (theElement.type != "button")
	                addSetValidatedValueToChange(theElement);
	              break;
	            case 'TEXTAREA' :
	            case 'SELECT' :
	              addSetValidatedValueToBlur(theElement);
	              break;
	          }
          }
      }
    }
  }
    //make a copy of the validated values into an inital values object
    for (v in validatedValues) {
        initialValues[v] = validatedValues[v];
    }
    //  convert text areas 
    if (vConvertTextareasToEdits) {
        setUpTextEditors();
    }
  if (errorFocus)
  {
    if (document.body)
    {
      if (document.body.scrollLeft) document.body.scrollLeft = 0;
      if (document.body.scrollTop) document.body.scrollTop = 0;
    }
  }
}

function addSetValidatedValueToChange(obj) {
  // Check to ensure that setValidatedValue is not already being called.
  // This might have been done to ensure that setValidatedValue gets called prior
  //  to an explicit PosseNavigate in the onchange event handler.
  // Checkboxes use a click event, and an 'out of stream' handler, not a change event.
  if (obj.nodeName == "INPUT" && obj.type == "checkbox")
  {
    if (!obj.onclick || obj.onclick.toString().indexOf('setValidatedValue',0) == -1)
    {
      if (obj.addEventListener)
      {
        obj.addEventListener('click', function(){setValidatedValue(obj);}, false);
      }
      else if (obj.attachEvent)
      {
        if (obj.onclick)
        {
            obj.attachEvent('onclick', function(){setValidatedValue(obj);});
        } else {
            obj.onclick = function(){setValidatedValue(obj);}
        }
      }

      if (obj.onclick)
      {
        var fn = obj.onclick.toString();
        var start = fn.indexOf("{",0);

        var fn1 = fn.substr(start + 1, fn.length - (start + 2)) ;
        var fn2 = "{var obj = document.getElementById('" + obj.id + "');"  + fn1.replace(/this/g, "obj") + ";setValidatedValue(obj);}";
        validatedValuesClickHandlers[obj.id] = new Function(fn2);
        setValidatedValue(obj);
      }
    }
  }
  else
  {
    if (!obj.onchange || obj.onchange.toString().indexOf('setValidatedValue',0) == -1)
    {
      if (obj.addEventListener)
      {
        obj.addEventListener('change', function(){setValidatedValue(obj);}, false);
      }
      else if (obj.attachEvent)
      {
        obj.attachEvent('onchange', function(){setValidatedValue(obj);});
      }
      setValidatedValue(obj);
    }
  }
}

function addSetValidatedValueToBlur(obj) {
  // Check to ensure that setValidatedValue is not already being called.
  // This might have been done to ensure that setValidatedValue gets called prior
  //  to an explicit PosseNavigate in the onchange event handler.
  if (!obj.onblur || obj.onblur.toString().indexOf('setValidatedValue',0) == -1)
  {
    if (obj.addEventListener)
    {
      obj.addEventListener('blur', function(){setValidatedValue(obj);}, false);
    }
    else if (obj.attachEvent)
    {
      obj.attachEvent('onblur', function(){setValidatedValue(obj);});
    }
    setValidatedValue(obj);
  }
}

function setValidatedValue(obj) {
  if (posseValidationFailed) return;
  if (typeof(obj) == "string")
  {
    obj = document.getElementById(obj);
  }
  switch (obj.nodeName)
  {
    case 'INPUT' :
    case 'TEXTAREA' :
      if (obj.type == 'checkbox')
      {
        validatedValues[obj.id] = obj.checked;
      }
      else
      {
        validatedValues[obj.id] = obj.value;
      }
      break;
    case 'SELECT' :
      validatedValues[obj.id] = obj.selectedIndex;
      break;
  }
}

function PosseNotifyCoordinatedLookupChanged(elements) {
   for (var i=0; i<elements.length; i++) {
     setValidatedValue(elements[i]);
   }
}

var comparingValidatedValues = false;
function compareValidatedValues() {
  if (comparingValidatedValues) return false;
  comparingValidatedValues = true;
    checkForChanges();
  for (v in validatedValues)
  {
    var obj = document.getElementById(v);
    if (obj && (obj.onchange || (obj.type == 'checkbox' && obj.onclick)))
    {
      switch (obj.nodeName)
      {
        case 'INPUT' :
        case 'TEXTAREA' :
          if (obj.type == 'checkbox' && obj.checked != validatedValues[v])
          {
            // fire the out-of-stream handler rather than triggering the click so a to
            // avoid actually changing the value
            validatedValuesClickHandlers[obj.id]();
          }
          else if (obj.type != 'checkbox' && obj.value != validatedValues[v])
          {
            if (!fireChangeEvent(obj) || posseValidationFailed)
            {
              comparingValidatedValues = false;
              return false;
            }
          }
          break;
        case 'SELECT' :
          if (obj.selectedIndex != validatedValues[v])
          {
            if (!fireChangeEvent(obj) || !fireBlurEvent(obj))
            {
              comparingValidatedValues = false;
              return false;
            }
          }
          break;
      }
    }
  }
  comparingValidatedValues = false;
  return true;
}

function PosseOnSubmitCustomValidate(){
  return compareValidatedValues();
}

function selectOnFocus(id) {
  // Ensure that the text in an element is selected at focus.
  var obj = PosseGetElement(id);
  if (obj.addEventListener)
  {
    obj.addEventListener('focus', function(){obj.select();}, false);
  }
  else if (obj.attachEvent)
  {
    obj.attachEvent('onfocus', function(){obj.select();});
  }
}

function setDefaultButton(id) {
  // Ensure that the text in an element is selected at focus.
  var theForm = PosseGetElement("aspnetForm");

  fire = function(evt) {FireDefaultButton(evt, id);}

  if (theForm.addEventListener)
  {
    theForm.addEventListener('keypress', fire, false);
  }
  else if (theForm.attachEvent)
  {
    theForm.attachEvent('onkeypress', fire);
  }
}

// Fires either the Change or Click event, based on whether
// the obj is a checkbox.
function fireChangeEvent(obj) {
  var eventName;
  if (obj.nodeName == "INPUT" && obj.type == "checkbox") {
    eventName = "click"
  } else {
    eventName = "change"
  }
  if( document.createEvent ) {
    var evObj = document.createEvent('HTMLEvents');
    evObj.initEvent(eventName, true, true );
    return obj.dispatchEvent(evObj);
  } else if( document.createEventObject ) {
    return obj.fireEvent('on' + eventName);
  }
}


function fireBlurEvent(obj) {
  if( document.createEvent )
  {
    var evObj = document.createEvent('HTMLEvents');
    evObj.initEvent( 'blur', true, true );
    return obj.dispatchEvent(evObj);
  }
  else if( document.createEventObject )
  {
    return obj.fireEvent('onblur');
  }
}

function addFireBlurOnChangeHandlers() {
  if (vFireBlurOnChangeList) {
    for (f in vFireBlurOnChangeList) {
      var elem = PosseGetElementOptional(f);
      if (elem) {
        if (!elem.fireBlurOnChangeAttached) {
          elem["fireBlurOnChangeAttached"] = "Y";
          if (elem.addEventListener) {
            elem.addEventListener('change', function(){fireBlurEvent(elem);}, false);
          }
          else if (elem.attachEvent) {
            elem.attachEvent('onchange', function(){fireBlurEvent(elem);});
          }
        }
      }
    }
  }
}

function addtodisablelist(disableElement) {
  var dl = document.getElementById("elementstodisablelist");
  if (dl)
  {
    dl.value += disableElement.id + ";";
    disableElement.disabled="disabled";
  }
}


function msg(type, label) {
  debug += "  >>> " + type + ": " + label + "\n";
}


function memberOf(lst, elem) {
  /* Test to see if an element exists in a list. */
  for (var i = 0, membership = false; i < lst.length && !membership; i++) {
    membership = elem == lst[i];
  }
  return membership;
}


function removeFromList(lst, elem) {
  /* Remove an element from a list. */
  var newList = [];

  for (var i = 0; i < lst.length; i++) {
    if (elem != lst[i]) {
      newList.push(lst[i]);
    }
  }
  return newList;
}


function pushElem(lst, elem) {
  /* Push an element onto the end of an ordered set, if it doesn't already exist. */
  for (var i = 0; i < lst.length; i++) {
    if (lst[i] == elem) {
      break;
    }
  }
  if (i == lst.length) {
    lst.push(elem)
  }
}



/* Create a new XMLHttpRequest object to talk to the Web server
   there are two ActiveX's in IE, Mozilla uses XMLHttpRequest */
var xmlHttp = false;


function CreateXmlObject(){
    /*@cc_on @*/
    /*@if (@_jscript_version >= 5)
    try {
       xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
    try {
      xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e2) {
      xmlHttp = false;
    }
  }
  @end @*/

  if (!xmlHttp && typeof XMLHttpRequest != 'undefined') {
    xmlHttp = new XMLHttpRequest();
  }
  if (!xmlHttp) {
    alert("Error initializing XMLHttpRequest!")
  }
  return true;
}


/* On Return from the request do this */
function OnReady(doAlert){
  /* gets called for part of the processing on the server, we only
     want this to execute when the server is done */

  if (xmlHttp.readyState == 4 ) {
    /* status = 200 means no error, so if there was an error
       display it to the user */
    if (xmlHttp.status != 200){
      document.write(xmlHttp.responseText);
      document.close();
    } else {
      var response = xmlHttp.responseText;
      if (doAlert)
      {
        alert("Report Saved");
      }
    }
  }
  return true;
}



/* Submit the form synchronously */

function SaveReport(JobId,reportName) {
  var vSync = false;
  CreateXmlObject();
  xmlHttp.open("GET", "savereport.aspx?PosseObjectId="+JobId+"&PosseReport="+reportName, vSync);

  if (vSync){
      xmlHttp.onreadystatechange = OnReady(true);
  }
  xmlHttp.send(null);

  /* A synchronous call does not trigger the onreadystatechange handler in firefox so do it manually */
  if (!vSync){
     var dummy= OnReady(true);
  }
}


function AutoSaveReport(JobId,reportName) {
  var vSync = false;
  CreateXmlObject();
  xmlHttp.open("GET", "savereport.aspx?PosseObjectId="+JobId+"&PosseReport="+reportName, vSync);

  if (vSync) {
      xmlHttp.onreadystatechange = OnReady(false);
  }
  xmlHttp.send(null);

  /* A synchronous call does not trigger the onreadystatechange handler in firefox so do it manually */
  if (!vSync) {
     var dummy= OnReady(false);
  }
}


// cross-browser function to get an object's style object given its id
function getStyleObject(objectId) {
  if(document.getElementById && document.getElementById(objectId)) {
    // W3C DOM
    return document.getElementById(objectId).style;
  } else if (document.all && document.all(objectId)) {
    // MSIE 4 DOM
    return document.all(objectId).style;
  } else if (document.layers && document.layers[objectId]) {
    // NN 4 DOM.. note: this won't find nested layers
    return document.layers[objectId];
  } else {
    return false;
  }
}


// get a reference to the cross-browser style object and make sure the object exists
function changeObjectVisibility(objectId, newVisibility) {
  var styleObject = getStyleObject(objectId);
  if(styleObject) {
    styleObject.visibility = newVisibility;
    return true;
  } else {
    // we couldn't find the object, so we can't change its visibility
    return false;
  }
}


// get a reference to the cross-browser style object and make sure the object exists
function moveObject(objectId, newXCoordinate, newYCoordinate) {
  var styleObject = getStyleObject(objectId);
  if(styleObject) {
    styleObject.left = newXCoordinate + "px";
    styleObject.top = newYCoordinate + "px";
    return true;
  } else {
    // we couldn't find the object, so we can't very well move it
    return false;
  }
}


function getPosX(obj) {
  var curleft = 0;
  if (obj.offsetParent) {
    while (obj.offsetParent) {
      curleft += obj.offsetLeft
      obj = obj.offsetParent;
    }
  }
  else if (obj.x) {
    curleft += obj.x;
  }
  return curleft;
}


function getPosY(obj) {
  var curtop = 0;
  if (obj.offsetParent) {
    while (obj.offsetParent) {
      curtop += obj.offsetTop
      obj = obj.offsetParent;
    }
  }
  else if (obj.y) {
    curtop += obj.y;
  }
  return curtop;
}


function getWidth(obj) {
  return obj.offsetWidth;
}


function getHeight(obj) {
  return obj.offsetHeight;
}


function setWidth(obj, width) {
  obj.offsetWidth = width;
}


function setHeight(obj, height) {
  obj.offsetHeight = height;
}


function getSelectedFees(aJobId) {
  var fees = document.getElementsByName("Fee")
  var idstring = ""
  var xml = ""
  var amount = 0
  var temp = ""
  var x = 0

  for (var i = 0; i < fees.length; i++) {
    if (fees[i].checked == true) {
      temp = fees[i].id
      x = temp.indexOf("_")
      amount = amount + Number(temp.substr(x + 1))
      idstring = idstring + temp.substr(0, x) + ", "
    }
  }
  amount = (Math.round(amount * 100)) / 100
  idstring = idstring.substr(0, idstring.length - 2)
  xml = xml + '<object id="' + aJobId + '"><column name="PayObjectList"><![CDATA[' + idstring + ']]></column><column name="SelectedFeesTotal"><![CDATA[' + amount + ']]></column></object>'
  PosseAppendChangesXML(unescape(xml));
}


function selectFeesFromInvoice(id) {
  var feeElemId
  var invoiceCheckValue = document.getElementById(id).checked

  for (var i=0; i < parentElems.length; i++) {
    if (parentElems[i] == id) {
      document.getElementById(childElems[i]).checked = invoiceCheckValue
    }
  }
}


function selectInvoicesFromAccount(id) {
  var invElemId
  var accountCheckValue = document.getElementById(id).checked

  for (var i=0; i < parentElems.length; i++) {
    if (parentElems[i] == id) {
      document.getElementById(childElems[i]).checked = accountCheckValue
      selectFeesFromInvoice(childElems[i])
    }
  }
}


function addFeeElements(parent, child) {
  parentElems.push(parent);
  childElems.push(child);
}


function addClientAccount(clientId) {
  var dropdownId = document.getElementById("AccountInsertDropdown")
  var accountType = dropdownId.options[dropdownId.selectedIndex].value
  var xml = ""

  if (accountType) {
    xml = '<object id="NEWacct" objectdef="' + accountType + '" action="Insert"><relationship id="NEW1" action="Insert" endpoint="Client" toobjectid="' + clientId + '" /></object>'
    PosseAppendChangesXML(unescape(xml));
    PosseNavigate("", "")
  }
}


function setAccountId(jobid, element) {
  var xml = ""
  var accountid = element.options[element.selectedIndex].value

  xml = xml + '<object id="' + jobid + '"><column name="ClientAccountObjectId"><![CDATA[' + accountid + ']]></column></object>'
  PosseAppendChangesXML(unescape(xml));
  PosseSubmit("", "")
}

var __nonMSDOMBrowser = (window.navigator.appName.toLowerCase().indexOf('explorer') == -1);

function FireDefaultButton(event, target) {
    if (event.keyCode == 13 && !(event.srcElement && (event.srcElement.tagName.toLowerCase() == "textarea"))) {
        var defaultButton;
        if (__nonMSDOMBrowser) {
            defaultButton = document.getElementById(target);
        }
        else {
            defaultButton = document.all[target];
        }
        if (defaultButton && ((typeof(defaultButton.onclick) != "undefined") || (typeof(defaultButton.href) != "undefined"))) {
            if (event.target && event.target.tagName.toLowerCase() == "input" && event.target.onchange) {
                if (!event.target.onchange()) {
                    // Return and cancel additional key processing
                    return false;
                }
            }
            // In IE, we are passed a srcElement instead of a target
            else if (event.srcElement && event.srcElement.tagName.toLowerCase() == "input" && event.srcElement.onchange) {
                if (!event.srcElement.onchange())
                {
                    // Return and cancel additional key processing
                    return false;
                }
            }
            if (typeof(defaultButton.onclick) != "undefined") {
                defaultButton.onclick();
            }
            else if (typeof(defaultButton.href) != "undefined") {
                location.replace(defaultButton.href);
            }

            event.cancelBubble = true;
            if (event.stopPropagation) event.stopPropagation();
            return false;
        }
    }
    return true;
}


var vPhoneNumberFields = new Array();

function SetUpPhoneNumberValidation(aArea, aFirst, aLast) {
  var vFields = new Array(PosseGetElement(aArea),PosseGetElement(aFirst),PosseGetElement(aLast));
  vPhoneNumberFields[vPhoneNumberFields.length] = vFields;
}

function ValidatePhoneNumbers() {
  for (i=0; i<vPhoneNumberFields.length; i++) {
    var vArea = vPhoneNumberFields[i][0];
    var vFirst = vPhoneNumberFields[i][1];
    var vLast = vPhoneNumberFields[i][2];
    if (!(ValidatePhoneNumberArea(vArea) &&
        ValidatePhoneNumberFirst(vFirst) &&
        ValidatePhoneNumberLast(vLast)))
      return false;
  }
  return true;
}

function ValidatePhoneNumberArea(aValue) {
  var len = 0;
  var str;
  str = aValue.value;

  if (str == "") return true;

  len = str.length;

  if ((len != 3) || isNaN(str)){
    alert("Please enter a 3-digit area code.");
    return false;
  }

  return true;
}

function ValidatePhoneNumberFirst(aValue) {
  var len = 0;
  var str;
  str = aValue.value;

  if (str == "") return true;

  len = str.length;

  if ((len != 3) || isNaN(str)) {
    alert("Invalid phone number.  Please enter using format of (123)456-7890.");
    return false;
  }

  return true;
}

function ValidatePhoneNumberLast(aValue) {
  var len = 0;
  var str;
  str = aValue.value;

  if (str == "") return true;

  len = str.length;

  if ((len != 4) || isNaN(str)){
    alert("Invalid phone number.  Please enter using format of (123)456-7890.");
    return false;
  }

  return true;
}

function PosseSubmitCustomValidate(){
    return ValidatePhoneNumbers();
}

//autotab functionality copied from autotab.js
  var secondTry = false;

  function autoTab(textElem, trigEvent, prevField, nextField) {
    var t = textElem.value, cursorPos = getSelectionStart(textElem), maxLength = textElem.getAttribute('maxLength');
    var textLength = t.length;
    if (nextField && textLength == maxLength && cursorPos == maxLength && (IsNumber(trigEvent) || IsRight(trigEvent)))
      if (IsRight(trigEvent))
        if (secondTry) {
          secondTry = false;
          nextField.focus();
        } else secondTry = true;
      else {
        secondTry = false;
        nextField.focus();
        nextField.select();
      }
    else
      if (prevField && cursorPos == 0 && IsLeft(trigEvent)) {
        if (secondTry) {
          secondTry = false;
          prevField.focus();
          setCursorPos(prevField, prevField.getAttribute('maxLength'));
        } else secondTry = true;
      } else secondTry = false;
    return true;
  }

  function getSelectionStart(o) {
    if (o.createTextRange) {
      var r = document.selection.createRange().duplicate();
      r.moveEnd('character', o.value.length);
      if (r.text == '') return o.value.length;
      return o.value.lastIndexOf(r.text);
    } else return o.selectionStart;
  }

  function getSelectionEnd(o) {
    if (o.createTextRange) {
      var r = document.selection.createRange().duplicate();
      r.moveStart('character', -o.value.length);
      return r.text.length;
    } else return o.selectionEnd
  }

  function IsNumber(e) {
    e = (e) ? e : event;
    var charCode = (e.charCode) ? e.charCode : ((e.keyCode) ? e.keyCode : ((e.which) ? e.which : 0));
    return ((charCode > 47 && charCode < 58) || (charCode > 95 && charCode < 106))
  }

  function IsRight(e) {
    e = (e) ? e : event;
    var charCode = (e.charCode) ? e.charCode : ((e.keyCode) ? e.keyCode : ((e.which) ? e.which : 0));
    return (charCode == 39)
  }

  function IsLeft(e) {
    e = (e) ? e : event;
    var charCode = (e.charCode) ? e.charCode : ((e.keyCode) ? e.keyCode : ((e.which) ? e.which : 0));
    return (charCode == 8 || charCode == 37)
  }

  function setCursorPos(textElem, pos) {
    if(textElem.createTextRange) {
        /* Create a TextRange, set the internal pointer to
           a specified position and show the cursor at this
           position
        */
        var range = textElem.createTextRange();
        range.move("character", pos);
        range.select();
    } else if(textElem.selectionStart) {
        /* Gecko is a little bit shorter on that. Simply
           focus the element and set the selection to a
           specified position
        */
        textElem.focus();
        textElem.setSelectionRange(pos, pos);
    }
  }

// The "Insert Processes" functionality
// This section handles the timeout
var timerID;
function startTimer() {
    timerID = window.setTimeout("hideAll()",500);
    return timerID;
}

function stopTimer(timerID) {
    window.clearTimeout(timerID);
}

// This section handles the dynamic display of rollover menu items.
function showMenu(objectId, srcObject) {
  var top, left, clientHeight, menuHeight, menu, orig_top, scrollTop;
  var listItems, listItemCount, listItemsCol1
  stopTimer(timerID);
  menu = document.getElementById(objectId);
  menuList = menu.getElementsByTagName("ul")[0];
  listItems = menuList.getElementsByTagName("li");
  listItemCount = listItems.length;
  listItemsCol1 = Math.round(listItemCount/2) - 1;

  // This is where the menu starts in pixels
  left = getPosX(srcObject) - 10; // + getWidth(srcObject);
  top = getPosY(srcObject) + getHeight(srcObject);

  // Reset the position in case the user has clicked on the button more than once
  moveObject(objectId, left, top);
  for (i=0; i < listItemCount; i++)
  {
    listItems[i].style.marginLeft = 0;
  }
  //listItems[listItemsCol1].style.marginTop = null;

  // Set the variables we need for later in the package
  clientHeight = document.body.clientHeight;
  menuHeight = menu.offsetHeight;
  scrollPos = document.body.scrollTop;
  menuBottom = menuHeight + getPosY(menu);
  menuTop = getPosY(menu);

  // Go to 2 columns if the bottom of the menu is off the bottom of the screen
  /*if (menuBottom > clientHeight + scrollPos) {
    // Instead of setting width of the list, set margins on the items so that it
    // goes down one column then down the second column
    for (i=0; i < listItemCount; i++)
    {
      if (i < listItemsCol1)
      {
        //Column 1
        listItems[i].style.marginLeft = 0;
      }
      else
      {
        //Column 2
        listItems[i].style.marginLeft = getWidth(menuList.firstChild);
      }
    }
    listItems[listItemsCol1].style.marginTop = -(getHeight(menuList.firstChild) * listItemsCol1);

    // The menu height and bottom position changed, so get our variables again
    menuHeight = menu.offsetHeight;
    menuBottom = menuHeight + getPosY(menu);
  }*/

  // Move the menu up if the bottom of the menu is off the bottom of the screen
//  if (menuBottom > clientHeight + scrollPos) {
//    top = top - (menuBottom - (clientHeight + scrollPos));
//    moveObject(objectId, left, top - 5);
//
//    // The menu position changed, so get our top and bottom variables again
//    menuTop = getPosY(menu);
//    menuBottom = menuHeight + getPosY(menu);
//  }

  changeObjectVisibility(objectId, "visible");
}

function hideMenu(objectId) {
    try {
      changeObjectVisibility(objectId, "hidden");
    } catch (err) {
      // ignore possible error when page isn't completely rendered yet
    }
}

function hideAll() {
    hideMenu("InsertMenu");
    hideMenu("M_Permit");
    hideMenu("M_Planning");
}

function showHide(object, visibility){
      try {
        object.style.display=(visibility=="show")?"inline":"none"
      } catch (err) {
        // ignore possible error when page isn't completely rendered yet
      }
}

// So that we don't call the event millions of times, we need to set these variables to turn off
// the function being called once we do the action
var vNavigate = true;
var vToggle = true;

// Make all grids navigate to first anchor tag in grid if clicked,
// except if the clicked item is an input field
function makeGridsClickable() {
  // Find all table rows on the page
  var rows = document.getElementsByTagName("tr");

    for (var i = 0; i < rows.length; i++) {
    var row = rows[i];

    // http://www.w3schools.com/DOM/prop_node_firstchild.asp
    // Note: Firefox, and most other browsers, will treat empty white-spaces or new lines as
    // text nodes, Internet Explorer will not. So, in the example below, we have a function
    // that checks the node type of the first child node.
    var firstRowChild = row.firstChild;
        while (firstRowChild && firstRowChild.nodeType != 1) {
      firstRowChild=firstRowChild.nextSibling;
    }

        if (row.className == "possegrid") {
            if (row.getElementsByTagName("a").length > 0) {
        // Get first anchor tag in the row and add click events if it is not a delete widget
        // or does not contain simply a '#' symbol
        var anchor = row.getElementsByTagName("a")[0];
        if (anchor &&
            anchor.href.indexOf("PosseDelete") == -1 &&
            (anchor.name.indexOf("NoNavInGrid") == -1) &&
            String(anchor.onclick).indexOf("PosseDelete") == -1 &&
                    anchor.href.indexOf("#") + 1 != anchor.href.length) {

                    // set the row to navigate to the href of the first anchor tag.
          row.onclick = function() {navigateToAnchor(this.getElementsByTagName("a")[0]);};

                    // replace the anchor's own onclick to do the navigate to anchor call in order to get the
                    // preserve changes logic to run.
                    anchor.onclick = function () { return navigateToAnchor(this); };

                    // set up the rest of the row's visual attributes
          row.style.cursor = "pointer";
          row.onmouseover = function () {this.setAttribute('bgColor', '#cfcfd0');};
          row.onmouseout = function() {this.removeAttribute('bgColor');};

                    // loop through the cells of the row, determining whether to wrap an anchor around the 
                    // content so that right-clicking will work (open in new tab, etc).
                    for (var cell = 0, td; td = row.cells[cell]; cell++) {
                        var span = td.childNodes[0];
                        if (span && span.tagName == "SPAN") {
                            var containsNoNonNavigableElements = true;
                            for (var child = 0, node; node = span.childNodes[child]; child++ ) {
                                if (node.tagName == "A" || node.tagName == "INPUT" ||
                    node.tagName == "SELECT" || node.tagName == "TEXTAREA" ||
                    (node.name && node.name.indexOf("NoNavInGrid") > -1) ||
                    (node.href && node.href.indexOf("PosseDelete") > -1) ||
                    (node.onclick && String(node.onclick).indexOf("PosseDelete") > -1)) {
                                    containsNoNonNavigableElements = false;
                                    // inform the ROW click handler not to do anything, as the node's click handler will
                                    // take care of the navigation.
                  if (node.attachEvent) { //IE
                                        node.attachEvent('onclick', function () { vNavigate = false; });
                  } else if (node.addEventListener) { // all other browsers
                                        node.addEventListener('click', function () { vNavigate = false; }, false);
            }
            }
          }
                            if (containsNoNonNavigableElements) {
                var aName = span.id + "_A";
                                span.innerHTML = '<a id="' + aName + '" name="' + aName + '" class="possegrid gridlink" href="' + anchor.href + '" onclick="navigateToAnchor(this); vNavigate=false; return false;" >' + span.innerHTML + '</a>'; 
            }
            }
          }
        }
      } else if (firstRowChild && firstRowChild.getElementsByTagName("input").length > 0) {
        row.onclick = function() {toggleCheckbox(this.getElementsByTagName("input")[0]);};
        row.style.cursor = "pointer";
        row.onmouseover = function () {this.setAttribute('bgColor', '#cfcfd0');};
        row.onmouseout = function() {this.removeAttribute('bgColor');};

        var checkbox = firstRowChild.getElementsByTagName("input")[0];

        if (checkbox.addEventListener) { // all other browsers
          checkbox.addEventListener('click', function() {vToggle = false;}, false);
        } else if (checkbox.attachEvent) { //IE
          checkbox.attachEvent('onclick', function() {vToggle = false;});
        }
      }
    }
  }
}

function navigateToAnchor(anchor) {
  if (vNavigate)
  {
        if (preserveChanges()) {
            return false;
        }
    window.open(anchor.href, anchor.target=="_blank"? anchor.target:"_self");
  }
  else
    vNavigate = true;

  return false;
}

// Fires either the Change event (this used to fire click if the item was a checkbox, but in some
// browsers, that's destructive (it changes the value))
function fireChangeEvent(obj) {
  if( document.createEvent )
  {
    var evObj = document.createEvent('HTMLEvents');
    evObj.initEvent('change', true, true);
    return obj.dispatchEvent(evObj);
  }
  else if( document.createEventObject )
  {
    return obj.fireEvent('onchange');
  }
}

function setAllCheckboxes(state) {
  var theForm = document.getElementById("aspnetForm");

  if (theForm)
  {
    for (var i = 0; i < theForm.elements.length; i++)
    {
      var theElement = theForm.elements[i];
      if (theElement.nodeName == 'INPUT' && theElement.type == 'checkbox')
      {
        if (theElement.checked != (state == 'Y'))
        {
          theElement.checked = (state == 'Y');
          // don't fire the change event.  The validatedValue processing will pick up the change
          //  fireChangeEvent(theElement);
        }
      }
    }
  }
}

function toggleCheckbox(checkbox) {

  if (vToggle && checkbox.nodeName == 'INPUT' && checkbox.type == 'checkbox')
  {
    checkbox.checked = !checkbox.checked;
    // don't fire the change event.  The validatedValue processing will pick up the change.
  }
  else
    vToggle = true;
}

function resizeTextArea(id, delta)
{
    el = window.document.getElementById(id);
    if (delta == 0)
    {
        el.rows = 2;
    }
    else
    {
        el.rows += delta;
    }
}

var directionDisplay;
//var directionsService = new google.maps.DirectionsService();
var geocoder;
var map;

function initializeGoogleMap() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(54, -97);
    var myOptions = {
      zoom: 3,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    directionsDisplay.setMap(map);
}

function codeGoogleMapAddress(address, title) {
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var contentString = '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td valign="top"><img src="images/icons/establishment.png"></td><td valign="top"><b>' + title + '</b><br>' + address + '</td></tr></table>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        map.setCenter(results[0].geometry.location);
        map.setZoom(15);
        var marker = new google.maps.Marker({
            map: map,
            title: title,
            position: results[0].geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map,marker);
        });

        //KML can be added here - the KML file must be on a publicly accessible site in order for the Google Maps API to access it
        //var ctaLayer = new google.maps.KmlLayer('http://www.greenfoxweb.com/New.kml', {preserveViewport:true});
        //ctaLayer.setMap(map);

      }
    });
}

function getHtmlFromEditor(editorIndex) {
    var html = HTMLEditors[editorIndex].getValue();
    if (html.charCodeAt(0) == 8203) {
        if (html.length > 1) {
            return html.substr(1);
        }
        else {
            return "";
        }
    }
    return html;
}

function checkForChanges() {
    if (document.possedocumentchangeform) {
        // copy the values from HTML Editors back to their corresponding TEXTAREAs
        // so that comparison with initial values will pick up pending changes.
        for (var editors = 0; editors < textAreasToConvert.length; editors++) {
            textareas[editors].value = getHtmlFromEditor(editors);
        }
        if (document.possedocumentchangeform.changesonobject > "") {
            var outstandingChanges = document.possedocumentchangeform.changespending.value == "T";

            if (!outstandingChanges) {
                for (v in initialValues) {
                    var obj = document.getElementById(v);
                    switch (obj.nodeName) {
                        case 'INPUT':
                        case 'TEXTAREA':
                            if (obj.type == 'checkbox' && obj.checked != initialValues[v]) {
                                outstandingChanges = true;
                            }
                            else if (obj.type != 'checkbox' && obj.value != initialValues[v]) {
                                outstandingChanges = true;
                            }
                            break;
                        case 'SELECT':
                            if (obj.selectedIndex != initialValues[v]) {
                                outstandingChanges = true;
                            }
                            break;
                    }
                    if (outstandingChanges) {
                        break;
                    }
                }
            }
        }

        if (outstandingChanges) {
            document.possedocumentchangeform.changespending.value = "T";
        }
        return outstandingChanges;
    }
    else {
        return false;
    }
}

function preserveChanges() {
    var abandonChanges = true;
    if (checkForChanges()) {
        abandonChanges = confirm("You have outstanding changes - press OK to abandon them.");
    }

    return !abandonChanges;
}

var HTMLEditors = new Array();
var textareas = new Array();
var textEditors = 0;
var textAreasToConvert = new Array();
var vConvertTextareasToEdits = false;

function setTextAreaToConvert(areaId) {
    vConvertTextareasToEdits = true;
    textAreasToConvert.push(areaId);
}

function setUpTextEditors() {
    var spans = new Array();

    // set up to use Ext for HTML Editors
    Ext.require("Ext.tip");
    Ext.require("Ext.panel");
    Ext.tip.QuickTipManager.init();

    for (var i = 0; i < textAreasToConvert.length; i++) {
        textareas[i] = document.getElementById(textAreasToConvert[i]);
        var taHeight = textareas[i].offsetHeight;
        var taWidth = textareas[i].offsetWidth;

        spans[i] = textAreasToConvert[i] + "_sp";

        textareas[i].style.display = "none";
        var HTMLEditor = new Ext.panel.Panel({
            renderTo: Ext.get(spans[i]),
            width: taWidth,
            height: taHeight,
            frame: true,
            layout: 'fit',
            items: {
                xtype: 'htmleditor',
                value: textareas[i].value,
                id: textAreasToConvert[i] + "_he"
            }
        });
        HTMLEditors[i] = HTMLEditor.child("#" + textAreasToConvert[i] + "_he");
        initialTextareaValues[i] = textareas[i].value;
    }
};