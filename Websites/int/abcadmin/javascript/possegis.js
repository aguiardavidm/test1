var mapPage;
var mapState = new Object();
var keys = [];
var pageKeys = [];
var showAllContainer;
var useGisSelectionLabel;
var useGisFlagContainer;

function GISPageLocate()
{
   openMap(this.GISPageLocateCore);
}

function GISPageLocateCore()
{
    mapPage.clearGraphics();
    mapPage.executeSqlQuery(pageKeys, 3, true);
}

function GISSearch() 
{
    openMapSearch(this.GISSearchCore);
}

function GISSearchCore() {
    mapPage.clearGraphics();
    clearSelectedObjects();
}

function GISLocate()
{
   openMap(this.GISLocateCore);
   return false;}

function GISLocateCore()
{
    mapPage.clearGraphics();
    mapPage.executeSqlQuery(keys, 3, true);
}

function setSelectedObject(objectId, linkColumn, linkValue, queryIndex) {
    //alert("ControlPage.aspx:setSelectedObject()\n" + objectId);
    //alert(linkColumn);
    //alert(linkValue);
    //alert(queryIndex);
    mapState.selectedObjects.push(objectId.toString());
    mapState.selectedLinkColumns.push(linkColumn.toString());
    mapState.selectedLinkValues.push(linkValue.toString());
    mapState.selectedQueryIndexes.push(queryIndex.valueOf());
    setGisDisplay();
}
function clearSelectedObjects() {
//    alert("ControlPage.aspx:clearSelectedObject()");
    mapState.selectedObjects = [];
    mapState.selectedLinkColumns = [];
    mapState.selectedLinkValues = [];
    mapState.selectedQueryIndexes = [];
    setGisDisplay();
}
function setGisDisplay() {
    var doGisKeys = document.getElementById("usemapselectioncheckbox");
    if (mapState && mapState.selectedObjects) {
        if (mapState.selectedObjects.length == 0) {
            if (useGisSelectionLabel) {
                useGisSelectionLabel.innerHTML = "Please select items on map:";
                doGisKeys.checked = false;
            }        }
        else if (mapState.selectedObjects.length == 1) {
            if (useGisSelectionLabel) {
                useGisSelectionLabel.innerHTML = "Use item selected on map:";
                doGisKeys.checked = true;
            }
        }
        else {
            if (useGisSelectionLabel) {
                useGisSelectionLabel.innerHTML = "Use " + mapState.selectedObjects.length + " map items selected:";
                doGisKeys.checked = true;
            }
        }
    }
}
function openMap(callBack) {
    isOpen = false;
    try {
	isOpen = mapPage.mapState;
	if (callBack)
	    callBack();
    } catch (ex) {
       mapPage = window.open('GISInterface/PosseArcGISMapEdit.htm', 'mapPage', 'height=570, width=930, resizable=1');
       if (callBack)
	   var t=setTimeout(callBack,2000);
    }
}
function openMapSearch(callBack) {
    isOpen = false;
    try {
        isOpen = mapPage.mapState;
        if (callBack)
            callBack();
    } catch (ex) {
        mapPage = window.open('GISInterface/PosseArcGISMapSearch.htm', 'mapPage', 'height=570, width=930, resizable=1');
        if (callBack)
            var t = setTimeout(callBack, 2000);
    }
}
function closeMap() {
    var undefined;
    try {
    	mapPage.window.close();
    	mapPage = undefined;
    } catch (ex) {
        mapPage = undefined;
    }
}
function appendGISKeys() {
    var values = "";
    var notFirst = false;
    var doGisKeys = document.getElementById("usemapselectioncheckbox");
    if (doGisKeys) {
    	if (doGisKeys.checked) {
        for (var i = 0; i < mapState.selectedObjects.length; i++) {
            if (mapState.selectedQueryIndexes[i] == 0) {
                if (notFirst) {
                    values = values + "," + mapState.selectedLinkValues[i];
                }
                else {
                    notFirst = true;
                    values = mapState.selectedLinkValues[i];
                }
            }
        }
    }
    PosseChangeColumn("S0", "GISKeys", values);
    }
}
// Get cookie routine by Shelley Powers
function getCookie(Name) {
    var search = Name + "="
    var returnvalue = "";
    var offset;
    if (document.cookie.length > 0) {
        offset = document.cookie.indexOf(search)
        // if cookie exists
        if (offset != -1) {
            offset += search.length
            // set index of beginning of value
            end = document.cookie.indexOf(";", offset);
            // set index of end of cookie value
            if (end == -1) end = document.cookie.length;
            returnvalue=unescape(document.cookie.substring(offset, end))
        }
    }
    return returnvalue;
}

// Save current menu state in a cookie
function saveMapState() {
    if (mapState) {
        mapState.featureSet = "";
        var a = JSON.stringify(mapState);
        document.cookie=window.location.pathname+".mapstate=" + a;
    }
}

// Load current menu state from a cookie (if it exists)
function loadMapState() {
    try {
        var cookieValue = getCookie(window.location.pathname+".mapstate")
        if (cookieValue != '') {
            var temp = JSON.parse(cookieValue);
            mapState = temp;
        }
    } catch(ex) {    
        clearSelectedObjects();
        mapState.extentList = "";
    }
    if (pageKeys && pageKeys.length < 1 && showAllContainer) {
          showAllContainer.style.display = "none";
    }
    setGisDisplay();
}
if (window.addEventListener) {
    window.addEventListener('unload', closeMap, false);
    window.addEventListener('unload', saveMapState, false);
    window.addEventListener('load', loadMapState, false);
}    
if (window.attachEvent) {
    window.attachEvent('onunload',closeMap);
    window.attachEvent('onunload',saveMapState);
    window.attachEvent('onload',loadMapState);
}    
