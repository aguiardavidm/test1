using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class Error : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Constructors
		///<summary>
		///Creates an instance of this class.
		///</summary>
		public Error()
		{
			this.SessionRequired = false;
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				Exception lastException = this.Server.GetLastError();

				this.Response.Clear();

				if (lastException != null)
				{
					if (this.ShowDetailedErrors)
					{
						this.lblError.Text = HttpUtility.HtmlEncode("\r\n" + this.GetErrorCode(lastException) + ": ") +
							lastException.Message + "\r\n\r\n" +HttpUtility.HtmlEncode(lastException.StackTrace) + "\r\n\r\n";
					}
					else
					{
						this.lblError.Text = lastException.Message + "\r\n\r\n";
					}

					if (lastException.InnerException != null)
					{
						if (this.ShowDetailedErrors)
						{
							this.lblError.Text += 
								HttpUtility.HtmlEncode(HttpUtility.UrlDecode("\r\n" + this.GetErrorCode(lastException.InnerException).ToString() + 
								": " + lastException.InnerException.Message + "\r\n\r\n" + lastException.InnerException.StackTrace));
						}
						else
						{
							this.lblError.Text += 
								HttpUtility.HtmlEncode(HttpUtility.UrlDecode("\r\n" + lastException.InnerException.Message + "\r\n\r\n"));
						}
					}
				}
				else
				{
					this.lblError.Text = HttpUtility.HtmlEncode("\r\n" + "An error occured." + "\r\n\r\n");
				}
			}
			catch (Exception exception)
			{
				this.lblError.Text = HttpUtility.HtmlEncode("\r\n" + exception.Message + "\r\n\r\n" +
					exception.StackTrace);
			}
		}
		#endregion
	}
}
