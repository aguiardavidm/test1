using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
	public partial class ChangePassword : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		/// <remarks></remarks>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (this.pnlChangePassword.Visible == true)
            {
                this.SetInitialFocus(this.txtPassword);
            }
		}

		/// <summary>
		/// Click event handler for the change password button.
		/// </summary>
		/// <param name="sender">A reference to the change password button.</param>
		/// <param name="e">Event arguments.</param>
		protected void btnChangePassword_Click(object sender, EventArgs e)
		{
			if (this.ChangePassword(this.txtPassword.Text, this.txtNewPassword.Text))
			{
				this.pnlChangePassword.Visible = false;
				this.pnlContinue.Visible = true;
			}
			else
			{
				this.SetValidationMessage("Change password failed.");
			}
		}

		/// <summary>
		/// Click event handler for the continue button.
		/// </summary>
		/// <param name="sender">A reference to the continue button.</param>
		/// <param name="e">Event arguments.</param>
		/// <remarks></remarks>
		protected void btnContinue_Click(object sender, EventArgs e)
		{
			this.Response.Redirect(this.HomePageUrl);
		}
		#endregion
}
}