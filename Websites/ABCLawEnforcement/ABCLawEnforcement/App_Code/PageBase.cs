using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    /// <summary>
    /// Base class for all outrider pages.
    /// </summary>
    public partial class PageBase :
      System.Web.UI.Page
    {
        #region Data Members
        /// <summary>
        /// Value used to represent the absence of a session in this.SessionId.
        /// </summary>
        protected readonly string NoSession = "-1";

        /// <summary>
        /// Value used to translate COM error codes.
        /// </summary>
        protected readonly int ObjectError = -2147221504;

        private IOutriderNet2 outriderNet = null;
        /// <summary>
        /// Provides a reference to the OutriderNet interface.
        /// </summary>
        protected virtual IOutriderNet2 OutriderNet
        {
            get
            {
                if (this.outriderNet == null)
                {
                    this.outriderNet = new OutriderNet();
                }

                return this.outriderNet;
            }
            set
            {
                this.outriderNet = value;
            }
        }

        /// <summary>
        /// Provides access the system key to be loaded.  (AppSettings["Outrider.SystemName"])
        /// </summary>
        protected virtual string SystemName
        {
            get
            {
                return ConfigurationManager.AppSettings["Outrider.SystemName"];
            }
        }

        /// <summary>
        /// Provides access to the virtual path for all cookies.  (AppSettings["CookiePath"])
        /// </summary>
        protected virtual string CookiePath
        {
            get
            {
                return ConfigurationManager.AppSettings["CookiePath"];
            }
        }

        /// <summary>
        /// Provides access to the name of the session id cookie value.  (AppSettings["SessionIdName"])
        /// </summary>
        protected virtual string SessionIdName
        {
            get
            {
                return ConfigurationManager.AppSettings["SessionIdName"];
            }
        }

        /// <summary>
        /// Provides access to the maximum upload file size.  (AppSettings["MaxFileSize"])
        /// </summary>
        protected virtual int MaxFileSize
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["MaxFileSize"]);
            }
        }

        private string sessionId;

        /// <summary>
        /// Provides access to the id for the current session.
        /// </summary>
        protected virtual string SessionId
        {
            get
            {
                if (this.sessionId == this.NoSession)
                {
                    // See if there is a session cookie out there
                    string sessionCookie = this.GetCookie(this.SessionIdName);
                    if (!string.IsNullOrEmpty(sessionCookie))
                    {
                        this.sessionId = sessionCookie;
                    }
                }
                return this.sessionId;
            }
            set
            {
                this.sessionId = value;
                this.SetCookie(this.SessionIdName, value);
            }
        }

        /// <summary>
        /// Whether to show the search criteria and results on the same pane on the current list presentation.
        /// </summary>
        protected virtual bool EmbedCriteriaOnSearches
        {
            get
            {
                string embedInSearches = this.Request.QueryString["PosseEmbedCriteriaPane"];

                return this.PresentationType == PresType.List &&
                    (string.Compare(embedInSearches, "y", StringComparison.InvariantCultureIgnoreCase) == 0 ||
                    string.Compare(embedInSearches, "yes", StringComparison.InvariantCultureIgnoreCase) == 0);
            }
        }

        /// <summary>
        /// Whether to enable Save As Excel on the current search presentation.
        /// </summary>
        protected virtual bool SaveAsExcelOnSearches
        {
            get
            {
                string saveAsExcel = ConfigurationManager.AppSettings["SaveAsExcelOnSearches"];
                string excludePresNames = ConfigurationManager.AppSettings["SaveAsExcelExcludeSearchPresentations"];

                return this.PresentationType == PresType.List &&
                    string.Compare(saveAsExcel, "true", StringComparison.InvariantCultureIgnoreCase) == 0
                    && !excludePresNames.Contains(this.Presentation);
            }
        }

        private string debugkey = null;
        /// <summary>
        /// Provides access to the current debug key.
        /// </summary>
        protected virtual string DebugKey
        {
            get
            {
                this.debugkey = this.GetCookie("PosseDebugKey");

                return this.debugkey;
            }
            set
            {
                this.debugkey = value;
                this.SetCookie("PosseDebugKey", value);
            }
        }

        /// <summary>
        /// Whether to show the debug switch.  (AppSettings["ShowDebugSwitch"])
        /// </summary>
        protected virtual bool ShowDebugSwitch
        {
            get
            {
                return (string.Compare(ConfigurationManager.AppSettings["ShowDebugSwitch"], "true",
                    StringComparison.InvariantCultureIgnoreCase) == 0);
            }
        }

        /// <summary>
        /// Indicates if a secure session should be used for the login and change password pages.  (AppSettings["IsSecure"])
        /// </summary>
        protected virtual bool IsSecure
        {
            get
            {
                return (string.Compare(ConfigurationManager.AppSettings["IsSecure"], "true",
                  StringComparison.InvariantCultureIgnoreCase) == 0);
            }
        }

        /// <summary>
        /// Provides access to the root url for the web site.  (AppSettings["RootUrl"])
        /// </summary>
        protected virtual string RootUrl
        {
            get
            {
                string result = ConfigurationManager.AppSettings["RootUrl"];

                // Provide access through sslexplorer.
                if (!string.IsNullOrEmpty(this.AlternateRootUrl) &&
                  this.Request.ServerVariables["HTTP_X_FORWARDED_HOST"] == this.AlternateHost)
                {
                    result = this.AlternateRootUrl;
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access to the alternate root url for the web site.  (AppSettings["AlternateRootUrl"])
        /// </summary>
        protected virtual string AlternateRootUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["AlternateRootUrl"];
            }
        }

        /// <summary>
        /// Provides access to the host for the alternate root url for the web site.  (AppSettings["AlternateHost"])
        /// </summary>
        protected virtual string AlternateHost
        {
            get
            {
                return ConfigurationManager.AppSettings["AlternateHost"];
            }
        }

        private string homePageUrl = null;
        /// <summary>
        /// Provides access to the url for the home page.  (RootUrl + AppSettings["HomePageUrl"])
        /// </summary>
        protected virtual string HomePageUrl
        {
            get
            {
                if (string.IsNullOrEmpty(this.homePageUrl))
                {
                    this.homePageUrl = this.RootUrl + ConfigurationManager.AppSettings["HomePageUrl"];
                }

                return this.homePageUrl;
            }
            set
            {
                this.homePageUrl = value;
            }
        }

        /// <summary>
        /// Provides access to the url for the login page.  (AppSettings["LoginPageUrl"])
        /// </summary>
        protected virtual string LoginPageUrl
        {
            get
            {
                string result = this.RootUrl + ConfigurationManager.AppSettings["LoginPageUrl"];

                if (this.IsSecure && string.Compare(result.Substring(0, 5), "http:",
                  StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    result = "https:" + result.Substring(5);
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access to the url for the logout page.  (AppSettings["LogoutPageUrl"])
        /// </summary>
        protected virtual string LogoutPageUrl
        {
            get
            {
                string result = this.RootUrl + ConfigurationManager.AppSettings["LogoutPageUrl"];

                if (this.IsSecure && string.Compare(result.Substring(0, 5), "http:",
                  StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    result = "https:" + result.Substring(5);
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access to the url for the change password page.  (RootUrl + AppSettings["ChangePasswordPageUrl"])
        /// </summary>
        protected virtual string ChangePasswordPageUrl
        {
            get
            {
                string result = this.RootUrl + ConfigurationManager.AppSettings["ChangePasswordPageUrl"];

                if (this.IsSecure && string.Compare(result.Substring(0, 5), "http:",
                  StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    result = "https:" + result.Substring(5);
                }

                return result;
            }
        }

        /// <summary>
        ///   Provides access to the url for the Microsoft Excel download page.  (AppSettings["ExcelDownloadUrl"])
        /// </summary>
        protected virtual string ExcelDownloadUrl
        {
            get
            {
                return this.RootUrl + ConfigurationManager.AppSettings["ExcelDownloadUrl"];
            }
        }

        /// <summary>
        ///   Provides access to the url for the error page.  (AppSettings["ErrorPageUrl"])
        /// </summary>
        protected virtual string ErrorPageUrl
        {
            get
            {
                return this.RootUrl + ConfigurationManager.AppSettings["ErrorPageUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the lookup page.  (AppSettings["LookupPageUrl"])
        /// </summary>
        protected virtual string LookupPageUrl
        {
            get
            {
                return this.RootUrl + ConfigurationManager.AppSettings["LookupPageUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the lookup return page.  (AppSettings["LookupReturnUrl"])
        /// </summary>
        protected virtual string LookupReturnUrl
        {
            get
            {
                return this.RootUrl + ConfigurationManager.AppSettings["LookupReturnUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the notes page.  (AppSettings["NotePageUrl"])
        /// </summary>
        protected virtual string NotePageUrl
        {
            get
            {
                return this.RootUrl + ConfigurationManager.AppSettings["NotePageUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the report page.  (AppSettings["ReportPageUrl"])
        /// </summary>
        protected virtual string ReportPageUrl
        {
            get
            {
                return this.RootUrl + ConfigurationManager.AppSettings["ReportPageUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the upload page.  (AppSettings["UploadPageUrl"])
        /// </summary>
        protected virtual string UploadPageUrl
        {
            get
            {
                return this.RootUrl + ConfigurationManager.AppSettings["UploadPageUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the tab label user control.  (AppSettings["TabLabelUrl"])
        /// </summary>
        protected virtual string TabLabelUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["TabLabelUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the selected tab label user control.  (AppSettings["SelectedTabLabelUrl"])
        /// </summary>
        protected virtual string SelectedTabLabelUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SelectedTabLabelUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the disabled tab label user control.  (AppSettings["DisabledTabLabelUrl"])
        /// </summary>
        protected virtual string DisabledTabLabelUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["DisabledTabLabelUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the fee tab label user control.  (AppSettings["WarningTabLabelUrl"])
        /// </summary>
        protected virtual string WarningTabLabelUrl
        {
            get
            {
            return ConfigurationManager.AppSettings["WarningTabLabelUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the selected fee tab label user control.  (AppSettings["SelectedWarningTabLabelUrl"])
        /// </summary>
        protected virtual string SelectedWarningTabLabelUrl
        {
            get
            {
            return ConfigurationManager.AppSettings["SelectedWarningTabLabelUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the function link user control.  (AppSettings["FunctionLinkUrl"])
        /// </summary>
        protected virtual string FunctionLinkUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["FunctionLinkUrl"];
            }
        }

        /// <summary>
        /// Provides access to the url for the disabled function link user control.  (AppSettings["DisabledFunctionLinkUrl"])
        /// </summary>
        protected virtual string DisabledFunctionLinkUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["DisabledFunctionLinkUrl"];
            }
        }

        /// <summary>
        /// Provides access to the show detailed errors flag.  (AppSettings["ShowDetailedErrors"])
        /// </summary>
        protected virtual bool ShowDetailedErrors
        {
            get
            {
                return bool.Parse(ConfigurationManager.AppSettings["ShowDetailedErrors"]);
            }
        }

        private bool sessionRequired = true;

        /// <summary>
        /// Indicates if the current page requires an active session.
        /// </summary>
        protected virtual bool SessionRequired
        {
            get
            {
                return this.sessionRequired;
            }
            set
            {
                this.sessionRequired = value;
            }
        }

        /// <summary>
        /// Provides access to the object id for the current request.  (Request.QueryString["PosseObjectId"])
        /// </summary>
        protected virtual string ObjectId
        {
            get
            {
                return this.Request.QueryString["PosseObjectId"];
            }
        }

        /// <summary>
        /// Provides access to the from object id for the current request.  (Request.QueryString["FromObjectId"]).
        /// </summary>
        protected virtual string FromObjectId
        {
            get
            {
                return this.Request.QueryString["FromObjectId"];
            }
        }

        /// <summary>
        /// Provides access to the end point for the current request.  (Request.QueryString["EndPoint"]).
        /// </summary>
        protected virtual string EndPoint
        {
            get
            {
                return this.Request.QueryString["EndPoint"];
            }
        }

        /// <summary>
        /// Provides access to the can do ajax flag.  (Request.QueryString["PosseCanDoLookupAJAX"])
        /// </summary>
        protected virtual bool CanDoLookupAJAX
        {
            get
            {
                return bool.Parse(this.Request.QueryString["PosseCanDoLookupAJAX"]);
            }
        }

        /// <summary>
        /// Provides access to the using ajax flag.  (this.Request.QueryString["PosseUsingAJAX"])
        /// </summary>
        protected virtual bool UsingAJAX
        {
            get
            {
                return (this.Request.QueryString["PosseUsingAJAX"] == "1");
            }
        }

        /// <summary>
        /// Provides access to the comes from lookup popup flag from the form data.  (Request.Form["ComesFromLookupPopup"])
        /// </summary>
        protected virtual bool ComesFromLookupPopup
        {
            get
            {
                return (this.Request.Form["ComesFromLookupPopup"] == "1");
            }
        }

        /// <summary>
        /// Provides access to the PreventAutoReturn1Row flag.  (this.Request.QueryString["PreventAutoReturn1Row"] == "1")
        /// This prevents the lookup window from automatically closing and returning a single-row result
        /// </summary>
        protected virtual bool PreventAutoReturn1Row
        {
            get
            {
                return (this.Request.QueryString["PreventAutoReturn1Row"] == "1");
            }
        }

        /// <summary>
        /// Provides access  to the object def id for the current request.  (Request.QueryString["PosseObjectDefId"])
        /// </summary>
        protected virtual string ObjectDefId
        {
            get
            {
                return this.Request.QueryString["PosseObjectDefId"];
            }
        }

        /// <summary>
        /// Provides access to the presentation id for the current request.  (Request.QueryString["PossePresentationId"])
        /// </summary>
        protected virtual string PresentationId
        {
            get
            {
                return this.Request.QueryString["PossePresentationId"];
            }
        }

        /// <summary>
        /// Provides access to the presentation from the current request.  (Request.QueryString["PossePresentation"])
        /// </summary>
        protected virtual string Presentation
        {
            get
            {
                return this.Request.QueryString["PossePresentation"];
            }
        }

        /// <summary>
        /// Provides access to the pane from the current request.  (Request.QueryString["PossePane"])
        /// </summary>
        protected virtual string Pane
        {
            get
            {
                return this.Request.QueryString["PossePane"];
            }
        }

        /// <summary>
        /// Provides access to the auto submit flag from the current request.  (Request.QueryString["PosseAutoSubmit"])
        /// </summary>
        protected virtual bool AutoSubmit
        {
            get
            {
                string autoSubmit = this.Request.QueryString["PosseAutoSubmit"];

                return string.Compare(autoSubmit, "y", StringComparison.InvariantCultureIgnoreCase) == 0 ||
                    string.Compare(autoSubmit, "yes", StringComparison.InvariantCultureIgnoreCase) == 0;
            }
        }

        /// <summary>
        /// Provides access tothe show title flag fro mthe current request.  (Request.QueryString["PosseShowTitle"])
        /// </summary>
        protected virtual bool ShowTitle
        {
            get
            {
                string showTitle = this.Request.QueryString["PosseShowTitle"];

                return string.Compare(showTitle, "y", StringComparison.InvariantCultureIgnoreCase) == 0 ||
                    string.Compare(showTitle, "yes", StringComparison.InvariantCultureIgnoreCase) == 0;
            }
        }

        /// <summary>
        /// Provides access to the arguments from the current request.  (Request.QueryString["PosseArguments"])
        /// </summary>
        protected virtual string Arguments
        {
            get
            {
                return this.Request.QueryString["PosseArguments"];
            }
        }

        /// <summary>
        /// Provides access to the unique id from the current request.  (Request.QueryString["PosseUniqueId"])
        /// </summary>
        protected virtual string UniqueId
        {
            get
            {
                return this.Request.QueryString["PosseUniqueId"];
            }
        }

        /// <summary>
        /// Provides access to the from object handle from the current request.  (Request.QueryString["PosseFromObjectHandle"])
        /// </summary>
        protected virtual string FromObjectHandle
        {
            get
            {
                return this.Request.QueryString["PosseFromObjectHandle"];
            }
        }

        /// <summary>
        /// Provides access to the from relationship handle from the current request.  (Request.QueryString["PosseFromRelationshipHandle"])
        /// </summary>
        protected virtual string FromRelationshipHandle
        {
            get
            {
                return this.Request.QueryString["PosseFromRelationshipHandle"];
            }
        }

        /// <summary>
        /// Provides access to the end point id from the current request.  (Request.QueryString["PosseEndPointId"])
        /// </summary>
        protected virtual int EndPointId
        {
            get
            {
                int result;

                int.TryParse(this.Request.QueryString["PosseEndPointId"], out result);

                return result;
            }
        }

        /// <summary>
        /// Provides access to the to object id from the current request.  (Request.QueryString["PosseToObjectId"])
        /// </summary>
        protected virtual string ToObjectId
        {
            get
            {
                return this.Request.QueryString["PosseToObjectId"];
            }
        }

        /// <summary>
        /// Provides access to the mapped item instance id from the current request.  (Request.QueryString["PosseMappedItemInstanceId"])
        /// </summary>
        protected virtual int MappedItemInstanceId
        {
            get
            {
                int result;

                int.TryParse(this.Request.QueryString["PosseMappedItemInstanceId"], out result);

                return result;
            }
        }

        /// <summary>
        /// Provides access to the round trip on change flag from the current request.  (Request.QueryString["PosseRoundTripOnChange"])
        /// </summary>
        protected virtual bool RoundTripOnChange
        {
            get
            {
                bool result;

                bool.TryParse(this.Request.QueryString["PosseRoundTripOnChange"], out result);

                return result;
            }
        }

        private List<OrderedDictionary> userInformation = null;
        /// <summary>
        /// Provides access to the user information data.
        /// </summary>
        protected virtual List<OrderedDictionary> UserInformation
        {
            get
            {
                if (this.userInformation == null)
                {
                    this.userInformation = this.GetData("UserInformation");
                }

                return this.userInformation;
            }
        }

        /// <summary>
        /// Provides access to the current user id.
        /// </summary>
        protected virtual string UserId
        {
            get
            {
                string result = null;

                if (this.UserInformation.Count > 0)
                {
                    //result = (string)this.UserInformation.Fields["Name"].Value;
                    result = (string)this.UserInformation[0]["name"];
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access to the current user name.
        /// </summary>
        protected virtual string UserName
        {
            get
            {
                string result = null;

                if (this.UserInformation.Count > 0)
                {
                    result = this.Server.HtmlEncode((string)this.UserInformation[0]["name"]);
                }

                return result;
            }
        }

        /// <summary>
        /// Provides access to the site navigation group name.  (AppSettings["SiteNavigationGroupName"])
        /// </summary>
        protected virtual string SiteNavigationGroupName
        {
            get
            {
                return ConfigurationManager.AppSettings["SiteNavigationGroupName"];
            }
        }

        private List<EntryPoint> siteNavigation = null;
        /// <summary>
        /// Provides access to the site navigation data.
        /// </summary>
        protected virtual List<EntryPoint> SiteNavigation
        {
            get
            {
                if (this.siteNavigation == null)
                {
                    this.siteNavigation = this.GetSiteNavigation(this.SiteNavigationGroupName, this.HomePageUrl);
                }

                return this.siteNavigation;
            }
        }

        /// <summary>
        /// Provides access to the name of the application.  (AppSettings["ApplicationName"])
        /// </summary>
        protected virtual string ApplicationName
        {
            get
            {
                return ConfigurationManager.AppSettings["ApplicationName"];
            }
        }

        private string presentationTitle = null;
        /// <summary>
        /// Provides access to the title of the presentation.
        /// </summary>
        protected virtual string PresentationTitle
        {
            get
            {
                return this.presentationTitle;
            }
            set
            {
                this.presentationTitle = value;
            }
        }

        private bool hasPresentation = false;
        /// <summary>
        /// Indicates if presentation has been rendered.
        /// </summary>
        public virtual bool HasPresentation
        {
            get
            {
                return this.hasPresentation;
            }
            set
            {
                this.hasPresentation = value;
            }
        }

        private int presentationDefId = 0;
        /// <summary>
        /// Provides access to the current presentation def id.
        /// </summary>
        protected virtual int PresentationDefId
        {
            get
            {
                return this.presentationDefId;
            }
            set
            {
                this.presentationDefId = value;
            }
        }

        /// <summary>
        /// Indicates the preentation style specified by the current presentation def id.
        /// </summary>
        private PresType presType;
        protected virtual PresType PresentationType
        {
            get
            {
                return presType;
            }
            set
            {
                this.presType = value;
            }
        }

        private string paneLabel = null;
        /// <summary>
        /// Provides access to the current pane label.
        /// </summary>
        protected virtual string PaneLabel
        {
            get
            {
                return this.paneLabel;
            }
            set
            {
                this.paneLabel = value;
            }
        }

        private List<VisiblePaneInfo> panes = null;
        /// <summary>
        /// Provides access to the pane data for a tab style presentation.
        /// </summary>
        protected virtual List<VisiblePaneInfo> Panes
        {
            get
            {
                return this.panes;
            }
            set
            {
                this.panes = value;
            }
        }

        private List<VisiblePaneInfo> jobPanes = null;
        protected virtual List<VisiblePaneInfo> JobPanes
        {
            get
            {
                return this.jobPanes;
            }
            set
            {
                this.jobPanes = value;
            }
        }

        private int currentPaneID = 0;
        /// <summary>
        /// Provides access to the current pane id.
        /// </summary>
        protected virtual int CurrentPaneId
        {
            get
            {
                return this.currentPaneID;
            }
            set
            {
                this.currentPaneID = value;
            }
        }

        private string currentUrl = null;
        /// <summary>
        /// Provides access to the current url.
        /// </summary>
        protected virtual string CurrentUrl
        {
            get
            {
                return this.currentUrl;
            }
            set
            {
                this.currentUrl = value;
            }
        }

        private List<string> paneFunctions = null;
        /// <summary>
        /// Provides access to an array of the current pane functions.
        /// </summary>
        protected virtual List<string> PaneFunctions
        {
            get
            {
                return this.paneFunctions;
            }
            set
            {
                this.paneFunctions = value;
            }
        }

        private Dictionary<string, List<InsertChoice>> insertChoices = new Dictionary<string, List<InsertChoice>>();
        /// <summary>
        /// Provides access to the pane insert choices, indexed by pane name.
        /// </summary>
        protected virtual Dictionary<string, List<InsertChoice>> InsertChoices
        {
            get
            {
                return this.insertChoices;
            }
        }

        private string paneHtml = null;
        /// <summary>
        /// Provides access to the pane html.
        /// </summary>
        protected virtual string PaneHtml
        {
            get
            {
                return this.paneHtml;
            }
            set
            {
                this.paneHtml = value;
            }
        }

        private string paneChangesHtml = null;
        /// <summary>
        /// Provides access to the pane Changes html.
        /// </summary>
        public virtual string PaneChangesHtml
        {
            get
            {
                return this.paneChangesHtml;
            }
            set
            {
                this.paneChangesHtml = value;
            }
        }

        private int startTabIndex = 1;
        /// <summary>
        /// Provides access to the starting tab index for the pane.
        /// </summary>
        protected virtual int StartTabIndex
        {
            get
            {
                return this.startTabIndex;
            }
            set
            {
                this.startTabIndex = value;
            }
        }

        private string headerPaneHtml = null;
        /// <summary>
        /// Provides access to the html for the header pane for the current presentation.
        /// </summary>
        protected virtual string HeaderPaneHtml
        {
            get
            {
                return this.headerPaneHtml;
            }
            set
            {
                this.headerPaneHtml = value;
            }
        }

        private string changesXml = null;
        /// <summary>
        /// Provides access to the changes xml for the current pane.
        /// </summary>
        protected virtual string ChangesXml
        {
            get
            {
                return this.changesXml;
            }
            set
            {
                this.changesXml = value;
            }
        }

        private string presentationName = null;
        /// <summary>
        /// Provides access to the current presentation name.
        /// </summary>
        protected virtual string PresentationName
        {
            get
            {
                return this.presentationName;
            }
            set
            {
                this.presentationName = value;
            }
        }

        private string objectDefName = null;
        /// <summary>
        /// Provides access to the current object def name.
        /// </summary>
        protected virtual string ObjectDefName
        {
            get
            {
                return this.objectDefName;
            }
            set
            {
                this.objectDefName = value;
            }
        }

        private string paneName;
        /// <summary>
        /// Provides access to the name of the current pane.
        /// </summary>
        protected virtual string PaneName
        {
            get
            {
                return this.paneName;
            }
            set
            {
                this.paneName = value;
            }
        }

        private string searchHtml = null;
        /// <summary>
        /// Provides access to the html for the current search pane.
        /// </summary>
        protected virtual string SearchHtml
        {
            get
            {
                return this.searchHtml;
            }
            set
            {
                this.searchHtml = value;
            }
        }

        private bool isGuestNull = true;
        private bool isGuest = false;
        /// <summary>
        /// Indicates if the current user is using the guest account.
        /// </summary>
        protected virtual bool IsGuest
        {
            get
            {
                if (this.isGuestNull)
                {
                    this.isGuestNull = false;
                    this.isGuest = this.OutriderNet.IsGuest();
                }

                return this.isGuest;
            }
        }

        private string errorMessage = null;
        /// <summary>
        /// Provides access to the text of the error message to be displayed to the user.
        /// </summary>
        protected virtual string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = System.Web.HttpUtility.HtmlEncode(value);
            }
        }

        /// <summary>
        /// Provides access to the url the user should be redirected to.
        /// </summary>
        protected virtual string RedirectUrl
        {
            get
            {
                string result = this.GetCookie("RedirectUrl");

                if (string.IsNullOrEmpty(result))
                {
                    result = this.HomePageUrl;
                }

                return result;
            }
            set
            {
                this.SetCookie("RedirectUrl", value);
            }
        }

        private string note = null;
        /// <summary>
        /// Provides access to the note data identified by for the current object id.
        /// </summary>
        protected virtual string Note
        {
            get
            {
                if (string.IsNullOrEmpty(this.note))
                {
                    this.note = this.GetNote(this.ObjectId);
                }

                return this.note;
            }
        }

        private byte[] report = null;
        /// <summary>
        /// Provides access to the current report data.
        /// </summary>
        protected virtual byte[] Report
        {
            get
            {
                string arguments;

                if (this.report == null)
                {
                    arguments = this.Request.QueryString["PosseArguments"];

                    if (string.IsNullOrEmpty(arguments))
                    {
                        this.report = this.GetReport(this.Request.QueryString["PosseReport"], this.ObjectId);
                    }
                    else
                    {
                        this.report = this.GetReport(arguments);
                    }
                }

                return this.report;
            }
        }

        private byte[] document = null;
        /// <summary>
        /// Provides access to the document data identified by the current object id.
        /// </summary>
        protected virtual byte[] Document
        {
            get
            {
                if (this.document == null)
                {
                    this.document = this.GetDocument(this.ObjectId);
                }

                return this.document;
            }
        }

        private string documentExtension = null;
        /// <summary>
        /// Provides access to the file name extension for the document identified by the current object id.
        /// </summary>
        protected virtual string DocumentExtension
        {
            get
            {
                if (string.IsNullOrEmpty(this.documentExtension))
                {
                    this.documentExtension = this.GetDocumentExtension(this.ObjectId);
                }

                return this.documentExtension;
            }
            set
            {
                this.documentExtension = value;
            }
        }

        private string documentName = null;
        /// <summary>
        /// Provides access the name of the document identified by the current object id.
        /// </summary>
        protected virtual string DocumentName
        {
            get
            {
                if (string.IsNullOrEmpty(this.documentName))
                {
                    this.documentName = this.GetDocumentName(this.ObjectId);
                }

                return this.documentName;
            }
            set
            {
                this.documentName = value;
            }
        }

        private string lastUploadFileName = null;
        /// <summary>
        /// Provides access to the file name of the last document to be uploaded.
        /// </summary>
        protected virtual string LastUploadFileName
        {
            get
            {
                return this.lastUploadFileName;
            }
            set
            {
                this.lastUploadFileName = value;
            }
        }

        private string lastUploadExtension = null;
        /// <summary>
        /// Provides access to the file name extension of the last document to be uploaded.
        /// </summary>
        protected virtual string LastUploadExtension
        {
            get
            {
                return this.lastUploadExtension;
            }
            set
            {
                this.lastUploadExtension = value;
            }
        }

        private int lastUploadSize = 0;
        /// <summary>
        /// Provides access to the size of the last document to be uploaded.
        /// </summary>
        protected virtual int LastUploadSize
        {
            get
            {
                return this.lastUploadSize;
            }
            set
            {
                this.lastUploadSize = value;
            }
        }

        private string lastUploadContentType = null;
        /// <summary>
        /// Provides access to the content type of the last document to be uploaded.
        /// </summary>
        protected virtual string LastUploadContentType
        {
            get
            {
                return this.lastUploadContentType;
            }
            set
            {
                this.lastUploadContentType = value;
            }
        }

        private int lastUploadDocumentId = 0;
        /// <summary>
        /// Provides access to the id of the last document to be uploaded.
        /// </summary>
        protected virtual int LastUploadDocumentId
        {
            get
            {
                return this.lastUploadDocumentId;
            }
            set
            {
                this.lastUploadDocumentId = value;
            }
        }

        private int paneId = 0;
        /// <summary>
        /// Provides access to the curent pane id.
        /// </summary>
        protected virtual int PaneId
        {
            get
            {
                if (this.paneId == 0)
                {
                    this.paneId = this.OutriderNet.GetPaneId();
                }

                return this.paneId;
            }
        }

        /// <summary>
        /// Provides access to the round trip function from the form data.
        /// </summary>
        protected virtual RoundTripFunction RoundTripFunction
        {
            get
            {
                int result;

                int.TryParse(this.Request.Form["FunctionDef"], out result);

                switch (result)
                {
                    case 0:
                        return RoundTripFunction.Previous;
                    case 1:
                        return RoundTripFunction.Next;
                    case 2:
                        return RoundTripFunction.Submit;
                    case 3:
                        return RoundTripFunction.Refresh;
                    case 4:
                        return RoundTripFunction.Search;
                    case 5:
                        return RoundTripFunction.PerformSearch;
                    case 6:
                        return RoundTripFunction.ButtonClicked;
                }

                return RoundTripFunction.Refresh;
            }
        }

        /// <summary>
        /// Provides access to the data changes from the form data.  (Request.Form["DataChanges"])
        /// </summary>
        protected virtual string DataChanges
        {
            get
            {
                return this.Request.Form["DataChanges"];
            }
        }

        /// <summary>
        /// Provides access to the sort columns from the form data.  (Request.Form["SortColumns"])
        /// </summary>
        protected virtual string SortColumns
        {
            get
            {
                return this.Request.Form["SortColumns"];
            }
        }

        /// <summary>
        /// Provides access to the comes from value from the form data.  (Request.Form["comesfrom"])
        /// </summary>
        protected virtual string ComesFrom
        {
            get
            {
                return this.Request.Form["comesfrom"];
            }
        }

        /// <summary>
        /// Provides access to the IP address of the calling browser
        /// </summary>
        protected virtual string UserHostAddress
        {
            get
            {
                string result = this.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(result))
                {
                    result = this.Request.UserHostAddress.Substring(0, Math.Min(this.Request.UserHostAddress.Length, 15));
                }

                return result;
            }
        }

        private string version = null;
        /// <summary>
        /// Provides access to the current version of outrider.
        /// </summary>
        protected virtual string Version
        {
            get
            {
                if (string.IsNullOrEmpty(this.version))
                {
                    this.version = this.OutriderNet.GetVersion();
                }

                return this.version;
            }
        }

        #endregion


        /// <summary>
        /// Default constructor.
        /// </summary>
        public PageBase()
        {
            this.Load += new EventHandler(this.PageBase_Load);
            this.Unload += new EventHandler(this.PageBase_Unload);
            this.PreRender += new EventHandler(this.PageBase_PreRender);
            this.Error += new EventHandler(this.PageBase_Error);
            this.sessionId = this.NoSession;
        }

        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected virtual void PageBase_Load(object sender, EventArgs e)
        {
            if (this.SessionRequired)
            {
                    this.StartSession();
            }

            this.CheckClient();
        }

        /// <summary>
        /// Page prerender event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected virtual void PageBase_PreRender(object sender, EventArgs e)
        {
            this.SetTitle();
        }

        /// <summary>
        /// Page unload event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected virtual void PageBase_Unload(object sender, EventArgs e)
        {
            this.ReleaseOutriderNet();
        }

        /// <summary>
        /// Page error event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected virtual void PageBase_Error(object sender, EventArgs e)
        {
            this.ProcessError(this.Server.GetLastError());
        }
        #endregion

        #region Methods

        /// <summary>
        /// Sets the specified cookie value.
        /// </summary>
        /// <param name="name">The name of the value.</param>
        /// <param name="value">The value being set.</param>
        protected virtual void SetCookie(string name, string value)
        {
            HttpCookie cookie = this.Response.Cookies[name];

            cookie.Value = value;
            cookie.Path = this.CookiePath;
        }

        /// <summary>
        /// Returns the specified cookie value.
        /// </summary>
        /// <param name="name">The name of the value to retrieve.</param>
        /// <returns>The specified value otherwise null.</returns>
        protected virtual string GetCookie(string name)
        {
            string result = null;
            HttpCookie cookie;

            if (((IList)this.Response.Cookies.AllKeys).Contains(name))
            {
                cookie = this.Response.Cookies[name];
                result = cookie.Value;
            }
            else
            {
                cookie = this.Request.Cookies[name];

                if (cookie != null)
                {
                    result = cookie.Value;
                }
            }

            return result;
        }

        /// <summary>
        /// Removes the specified cookie value.
        /// </summary>
        /// <param name="name">The name of the value to remove.</param>
        protected virtual void RemoveCookie(string name)
        {
            if (this.Request.Cookies[name] != null)
            {
                this.Request.Cookies.Remove(name);
                this.Response.Cookies[name].Expires = DateTime.MinValue;
                this.Response.Cookies[name].Path = this.CookiePath;
            }
        }

        /// <summary>
        /// Returns the specified data for the current user.
        /// </summary>
        /// <param name="recordsetName">The name of the dataset to return.</param>
        /// <returns>The specified data as a record set.</returns>
        protected virtual List<OrderedDictionary> GetData(string recordsetName)
        {
            return this.OutriderNet.GetRecordSet(recordsetName);
        }

        /// <summary>
        /// Returns the specified data.
        /// </summary>
        /// <param name="recordsetName">The name of the dataset to return.</param>
        /// <returns>The specified data as a record set.</returns>
        protected virtual List<OrderedDictionary> GetData(string recordsetName, int objectId)
        {
            return this.OutriderNet.GetRecordSet(recordsetName, objectId);
        }

        /// <summary>
        /// Returns the encoded cache state string.
        /// </summary>
        /// <returns>The specified data as a string.</returns>
        protected virtual string GetCacheState()
        {
            return this.OutriderNet.GetCacheState();
        }

        /// <summary>
        /// Returns the HTML that renders the current pane.
        /// </summary>
        /// <returns>The HTML necessary to render the current pane.</returns>
        protected virtual string GetPaneHTML()
        {
            return this.GetPaneHTML(this.StartTabIndex);
        }

        /// <summary>
        /// Returns the HTML that renders the current pane.
        /// </summary>
        /// <param name="startTabIndex">The value to use for the first tab index.</param>
        /// <returns>The HTML necessary to render the current pane.</returns>
        protected virtual string GetPaneHTML(int startTabIndex)
        {
            return StripFormTags(this.OutriderNet.GetPaneHTML(startTabIndex));
        }

        /// <summary>
        /// Returns the HTML that creates the hidden "changes" form.
        /// </summary>
        /// <returns>The HTML for the data changes cache.</returns>
        protected virtual void GetPaneChangesHTML()
        {
            this.PaneChangesHtml = this.OutriderNet.GetPaneChangesHTML();
            return;
        }

        /// <summary>
        /// Returns the specified navigation data.
        /// </summary>
        /// <param name="groupName">The name of the navigation data ro return.</param>
        /// <param name="baseUrl">The base url to use.</param>
        /// <returns>The specified navigation data as a record set.</returns>
        protected virtual List<EntryPoint> GetSiteNavigation(string groupName, string baseUrl)
        {
            return this.OutriderNet.GetEntryPoints(groupName);
        }

        /// <summary>
        /// Returns the url for the specified entry point group item.
        /// </summary>
        /// <param name="groupName">The name of the navigation data ro return.</param>
        /// <param name="label">The label of the item to locate.</param>
        /// <returns>The url of the specified entry point group item, otherwise null.</returns>
        protected virtual string GetEntryPointUrl(string groupName, string label)
        {
            string result = null;
            List<EntryPoint> entryPoints = null;

            entryPoints = this.GetSiteNavigation(groupName, null);

            if (entryPoints != null && entryPoints.Count > 0)
            {
                foreach (EntryPoint ep in entryPoints)
                {
                    this.Response.Write(ep.Label + "<br>");
                    if (ep.Label != null && ep.Label == label)
                    {
                        if (ep.Url != null)
                        {
                            result = ep.Url;
                        }
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the note identified by the specified object id.
        /// </summary>
        /// <param name="objectId">The object id of the note.</param>
        /// <returns>The text of the note.</returns>
        protected virtual string GetNote(string objectId)
        {
            return this.OutriderNet.GetNote(objectId);
        }

        /// <summary>
        /// Returns the report data identified by the specified report name and object id.
        /// </summary>
        /// <param name="reportName">The name of the report.</param>
        /// <param name="objectId">The object id of the report.</param>
        /// <returns>The report data.</returns>
        protected virtual byte[] GetReport(string reportName, string objectId)
        {
            return this.OutriderNet.CreateObjectReport(reportName, int.Parse(objectId));
        }

        /// <summary>
        /// Returns the report data identified by the specified arguments.
        /// </summary>
        /// <param name="arguments">Encrypted arguments supplied by Outrider as a URL parameter.</param>
        /// <returns>The report data.</returns>
        protected virtual byte[] GetReport(string arguments)
        {
            return this.OutriderNet.CreateReport(arguments);
        }

        /// <summary>
        /// Returns the document data identified by the specified object id.
        /// </summary>
        /// <param name="objectId">The object id of the document.</param>
        /// <returns>The document data.</returns>
        protected virtual byte[] GetDocument(string objectId)
        {
            return this.OutriderNet.GetDocument(objectId);
        }

        /// <summary>
        /// Returns the unique id of the new document.
        /// </summary>
        /// <param name="objectDefId">The id of the document type.</param>
        /// <param name="baseName">The file name (without extension) of the document.</param>
        /// <param name="extension">The file name extension of the document.</param>
        /// <param name="blob">The document data.</param>
        /// <returns>The unique id of the new document.</returns>
        protected virtual int SetDocument(int objectDefId, string baseName, string extension, byte[] blob)
        {
            return this.OutriderNet.SetDocument(objectDefId, baseName, extension, blob);
        }

        /// <summary>
        /// Returns the file name extension of the document identified by the specified object id.
        /// </summary>
        /// <param name="objectId">The id of the document.</param>
        /// <returns>The file name extension of the document.</returns>
        protected virtual string GetDocumentExtension(string objectId)
        {
            return this.OutriderNet.GetDocumentExtension(objectId);
        }

        /// <summary>
        /// Returns the name of the document identified by the specified object id.
        /// </summary>
        /// <param name="objectId">The id of the document.</param>
        /// <returns>The name of the document.</returns>
        protected virtual string GetDocumentName(string objectId)
        {
            return this.OutriderNet.GetDocumentName(objectId);
        }

        /// <summary>
        /// Establishes a session.
        /// </summary>
        protected virtual void StartSession()
        {
            this.isGuestNull = true;
            this.userInformation = null;
            this.siteNavigation = null;

            if (this.SessionId == this.NoSession)
            {
                this.SessionId = this.OutriderNet.NewSession(
                    this.UserHostAddress, this.DebugKey);
            }
            else
            {
                if (!this.AttachSession())
                {
                    this.SessionId = this.NoSession;
                    this.ShowLogin("Your session has timed out, please log in to continue.");
                }
            }
        }

        /// <summary>
        /// Attaches to an existing session.
        /// </summary>
        /// <returns>True, if the session is valid and has not timed out; False otherwise.</returns>
        protected virtual bool AttachSession()
        {
            this.isGuestNull = true;
            this.userInformation = null;
            this.siteNavigation = null;

            return this.OutriderNet.AttachSession(this.SessionId,
                    this.DebugKey);
        }

        /// <summary>
        /// Presents the login page with the specified message.
        /// </summary>
        /// <param name="message">The message to display.</param>
        protected virtual void ShowLogin(string message)
        {
            if (!this.Request.RawUrl.Contains(ConfigurationManager.AppSettings["LoginPageUrl"]))
            {
                this.RedirectUrl = this.Request.RawUrl;
            }
            this.ReleaseOutriderNet();
            this.SetCookie("Message", message);
            this.Response.Redirect(this.LoginPageUrl);
        }

        /// <summary>
        /// Starts a new session for the specified user.
        /// </summary>
        /// <param name="userId">The userid of the user logging in.</param>
        /// <param name="password">The password of the user logging in.</param>
        protected virtual void Login(string userId, string password)
        {
            this.SessionId = this.OutriderNet.NewSession(userId, password,
                this.UserHostAddress, this.DebugKey);
            this.ReleaseOutriderNet();
        }

        /// <summary>
        /// Releases the reference to the outrider net inteface.
        /// </summary>
        protected virtual void ReleaseOutriderNet()
        {
            this.OutriderNet.Dispose();
        }

        /// <summary>
        /// Checks if the client is still connected.
        /// </summary>
        protected virtual void CheckClient()
        {
            if (!this.Response.IsClientConnected)
            {
                this.ReleaseOutriderNet();
                this.Response.End();
            }
        }

        /// <summary>
        /// Sets the page title.
        /// </summary>
        protected virtual void SetTitle()
        {
            string title = this.Request.QueryString["Title"];

            if (string.IsNullOrEmpty(this.PresentationTitle))
            {
                if (string.IsNullOrEmpty(title))
                {
                    title = this.ApplicationName + " - " + this.Title;
                }
            }
            else
            {
                //if the <titleicon> tag is in the title, we need to remove it
                int pos = this.PresentationTitle.IndexOf("<titleicon>");
                if (pos != -1)
                {
                    title = this.ApplicationName + " - " + this.PresentationTitle.Substring(0, pos);
                }
                else
                {
                title = this.ApplicationName + " - " + this.PresentationTitle;
            }
            }

            this.Title = title;
        }

        /// <summary>
        /// Evaluates the specified exception and provides an error message or redirects the user to the login page.
        /// </summary>
        /// <param name="exception">A reference to the exception to be processed.</param>
        protected virtual void ProcessError(Exception exception)
        {
            int errorCode = this.GetErrorCode(exception);

            if (!(exception is ThreadAbortException))
            {
                if (errorCode > 0)
                {
                    switch (errorCode)
                    {
                        case 1006:  //Search error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1029:  //EditMask error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1035:  //Upload error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1045:  //Pane mandatory error.
                        case 1046:  //Pane mandatory custom error.
                        case 1066:  //Detail mandatory error.
                        case 1067:  //Job detail mandatory error. (Due to workflow)
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1052:  //Update error.
                            this.ErrorMessage = exception.Message;
                            break;
                        case 1075:  //Insufficient privileges error.
                        case 1277:  //AuthenticationRequired.
                            this.ShowLogin("The page which you are trying to access requires you to Sign in.");
                            break;
                        case 1278: //InvalidUserOrPassword
                            this.ShowLogin("Invalid username or password.");
                            break;
                        case 1315:  //Site busy
                          this.ReleaseOutriderNet();
                          this.WriteToSiteBusyLog();
                          this.Server.Transfer("SiteBusy.html");
                          break;
                        case 1316:  //Site reloading
                          this.ReleaseOutriderNet();
                          this.Server.Transfer("SiteLoading.html");
                          break;
                        default:
                            this.ReleaseOutriderNet();
                            // This is an awkward approach. In order to pass exception information
                            // to the error page we have to ensure that the exception is trapped by the
                            // page level error handler (Page_Error) so that Server.GetLastError()
                            // will be available to the error page.  Rethrowing the same exception
                            // ensures that the exception is caught by Page_Error and then sent
                            // back to this method.
                            //
                            // Originally cookies were used to stored the error info and
                            // Response.Redirect() was called to redirect to the error page.
                            // However the error messages that Outrider produces may contain HTML
                            // and could be quite large.  Cookies will trucate anything after a
                            // semicolon, cutting off the error message.
                            //
                            // Other options include using ASP.NET session variables or putting the
                            // error messages in a hash table (indexed by the Outrider sessionid)
                            // on the Outrider COM .Net component itself.

                            if (this.Server.GetLastError() == null)
                            {
                                throw exception;
                            }

                            this.Server.Transfer(ConfigurationManager.AppSettings["ErrorPageUrl"]);
                            break;
                    }
                }
                else
                {
                    this.ReleaseOutriderNet();

                    if (this.Server.GetLastError() == null)
                    {
                        throw exception;
                    }

                    this.Server.Transfer(ConfigurationManager.AppSettings["ErrorPageUrl"]);
                }
            }

            this.Server.ClearError();
        }

        /// <summary>
        /// Returns the error code from a COM exception.
        /// </summary>
        /// <param name="exception">A reference to the exception to evaluate.</param>
        /// <returns>The error code if the exception is a com exception otherwise 0.</returns>
        protected virtual int GetErrorCode(Exception exception)
        {
            int errorCode = 0;

            if (exception is OutriderException)
            {
                errorCode = (int)exception.Data["templateId"];
            }

            return errorCode;
        }

        /// <summary>
        /// Sets the specified property to the given value.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="value">The value to be set.</param>
        protected virtual void SetSiteProperty(SiteProperty property, string value)
        {
            this.OutriderNet.SetSiteProperty(property, value);
        }

        /// <summary>
        /// Processes changed data and prepares for the new rendering.
        /// </summary>
        /// <param name="objectId">The ObjectId of the primary object being worked on.</param>
        /// <param name="currentPaneId">The unique internal identifier of the current page.</param>
        /// <param name="NewPaneId">The unique internal identifier of the next page to display, under normal circumstances.</param>
        /// <param name="functionDefId">The function the user just selected.</param>
        /// <param name="dataChanges">The coded data changes for all outstanding changes for this presentation.</param>
        /// <param name="sortColumns">Coded data indicating how each multirow form is to be sorted.</param>
        protected virtual void ProcessFunction(string objectId, int currentPaneId, int newPaneId,
          RoundTripFunction function, string dataChanges, string sortColumns)
        {
            this.paneId = 0;
            this.outriderNet.ProcessFunction(objectId, currentPaneId, newPaneId, function, dataChanges, sortColumns);
        }

        /// <summary>
        /// Processes changed data and prepares for the new rendering.
        /// </summary>
        /// <param name="objectId">The ObjectId of the primary object being worked on.</param>
        /// <param name="currentPaneId">The unique internal identifier of the current page.</param>
        /// <param name="NewPaneId">The unique internal identifier of the next page to display, under normal circumstances.</param>
        /// <param name="functionDefId">The function the user just selected.</param>
        /// <param name="dataChanges">The coded data changes for all outstanding changes for this presentation.</param>
        /// <param name="sortColumns">Coded data indicating how each multirow form is to be sorted.</param>
        /// <param name="xml">Addition data changes in an XML format.</param>
        protected virtual void ProcessFunction(string objectId, int currentPaneId, int newPaneId,
          RoundTripFunction function, string dataChanges, string sortColumns, string xml)
        {
            this.paneId = 0;
            this.outriderNet.ProcessFunction(objectId, currentPaneId, newPaneId, function, dataChanges, sortColumns, xml);
        }

        /// <summary>
        /// Retrieve the pane id for the specified pane name.
        /// </summary>
        /// <param name="paneName">The name of the pane.</param>
        /// <returns>The id of the specified pane otherwise 0.</returns>
        protected virtual int GetPaneId(string paneName)
        {
            int result = 0;

            try
            {
                result = this.OutriderNet.GetPaneId(paneName);
            }
            catch
            {
            }

            return result;
        }

        /// <summary>
        /// Generates the requested presentation and loads the pane html.
        /// </summary>
        protected virtual void LoadPresentation()
        {
            this.LoadPresentation(false, false);
        }

        /// <summary>
        /// Generates the requested presentation and loads the pane html.
        /// </summary>
        protected virtual void LoadPresentation(bool returnAfterSubmit)
        {
            this.LoadPresentation(returnAfterSubmit, false);
        }

        /// <summary>
        /// Generates the requested presentation and loads the pane html.
        /// </summary>
        protected virtual void LoadPresentation(bool returnAfterSubmit, bool startDialogWithChanges)
        {
            bool searchPressed = false, sortCriteriaExists = false, clickSortPressed = false;
            int criteriaCount = 0;
            string objectId = null;
            StringBuilder search = new StringBuilder(), xml = new StringBuilder();
            OrderedDictionary data = null;

            // hook to load system information however the site may define and manage it.
            LoadSystemInformation();

            this.LoadPresentationCore(returnAfterSubmit, startDialogWithChanges);

            // Initialize condition properties.
            if (this.HasPresentation)
            {
                LoadConditions();
            }

            if (returnAfterSubmit && this.RoundTripFunction == RoundTripFunction.Submit &&
                string.IsNullOrEmpty(this.ErrorMessage))
            {
                return;
            }

            if (this.HasPresentation)
            {
                // The filtered list functionality is toggled by the "FilteredList"
                // condition on the pane and is implemented in two parts.
                // First, before we load the pane for the user we populate
                // search fields from cookies so the user's previous search is remembered.
                // We then refresh the pane so the fields contain the new values.
                // The first part is now finished.
                // We then let the normal pane loading happen at this point.
                // When the presentation is loaded the second part happens.
                // Based on what search criteria has been entered, and some hidden
                // control fields, we do the search and retrieve the results grid.
                // The results grid HTML is appended to the HTML loaded during
                // the normal pane loading.  Also, the values used for the search
                // are stored in the cookies so they are ready for the next page load.
                if ((this.PresentationType == PresType.Tabs
                    || this.PresentationType == PresType.Wizard)
                    && this.FilteredList)
                {
                    data = this.GetPaneData(true)[0];
                    objectId = data["objecthandle"].ToString();

                    xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", objectId);

                    if (this.GetCookie("Search:PaneId") != this.PaneId.ToString())
                    {
                        clickSortPressed = false;
                        this.SetCookie("Search:PaneId", this.PaneId.ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(this.SortColumns))
                        {
                            clickSortPressed = (this.SortColumns.Length > 2);
                        }
                    }

                    if ((bool)data["ignore_searchbuttonpressed"] || clickSortPressed)
                    {
                        searchPressed = true;
                        xml.Append("<column name=\"Ignore_SearchButtonPressed\">false</column>");
                    }
                    else
                    {
                        foreach (DictionaryEntry column in data)
                        {
                            //if (!data.Fields[index].Name.StartsWith("Ignore_") && data.Fields[index].Name != "ObjectHandle")
                            if (!column.Key.ToString().StartsWith("ignore_") && column.Key.ToString() != "objecthandle")
                            {
                                if (column.Value == null &&
                                  !string.IsNullOrEmpty(this.GetCookie("Search:" + column.Key.ToString())))
                                {

                                    criteriaCount++;
                                    xml.AppendFormat("<column name=\"{0}\">{1}</column>", column.Key.ToString(),
                                      this.GetCookie("Search:" + column.Key.ToString()));
                                }
                            }
                        }
                    }

                    xml.Append("</object>");

                    if (criteriaCount > 0 || searchPressed)
                    {
                        this.ProcessFunction(objectId, this.PaneId, this.PaneId, RoundTripFunction.Refresh,
                            this.DataChanges, this.SortColumns, xml.ToString());

                        data = this.GetPaneData()[0];
                    }
                }

                this.PaneHtml = this.GetPaneHTML();
                this.GetPaneChangesHTML();  // prime the property with the current Changes HTML.
                this.StartTabIndex += 10000;
                this.LoadHeaderPane();

                if ((this.PresentationType == PresType.Tabs
                    || this.PresentationType == PresType.Wizard)
                    && this.FilteredList)
                {
                    criteriaCount = 0;

                    search.AppendFormat("PossePresentation={0}", data["Ignore_SearchPresentationname"]);

                    foreach (DictionaryEntry column in data)
                    //for (int index = 0; index < data.Fields.Count; index++)
                    {
                        if (!column.Key.ToString().StartsWith("ignore_") && column.Key.ToString() != "objecthandle")
                        {
                            if (column.Value != null && column.Value is DateTime)
                            {
                                search.AppendFormat("&{0}={1}", column.Key.ToString(),
                                  ((DateTime)column.Value).ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                search.AppendFormat("&{0}={1}", column.Key, column.Value);
                            }

                            if (column.Value != null)
                            {
                                criteriaCount++;

                                if (column.Value is DateTime)
                                {
                                    this.SetCookie("Search:" + column.Key.ToString(), ((DateTime)column.Value).ToString("yyyy-MM-dd"));
                                }
                                else
                                {
                                    this.SetCookie("Search:" + column.Key.ToString(), column.Value.ToString());
                                }
                            }
                            else
                            {
                                this.RemoveCookie("Search:" + column.Key.ToString());
                            }
                        }
                    }

                    search.Append("&PosseShowCriteriaPane=No");

                    if (criteriaCount > 1 || searchPressed)
                    {
                        try
                        {
                            this.StartDialog(search.ToString());

                            if (!string.IsNullOrEmpty(this.SortColumns))
                            {
                                sortCriteriaExists = (this.SortColumns.Length > 2);
                            }

                            if (sortCriteriaExists)
                            {
                                this.ProcessFunction(objectId, this.PaneId, this.PaneId, RoundTripFunction.Refresh,
                                  this.GetCacheState(), this.SortColumns);
                            }

                            this.PaneHtml += this.GetPaneHTML();
                        }
                        catch (Exception exception)
                        {
                            this.ProcessError(exception);
                        }
                    }
                }
                // Note: the embedded criteria functionality was ported from the VB.NET templates and
                // they embedded the criteria before calling LoadHeaderPane.  We moved embedded criteria
                // here since we also have to deal with filtered lists and made the assumption that header
                // panes and list presentations are mutually exclusive.
                else if (this.EmbedCriteriaOnSearches)
                {
                    // If we are embedding the search criteria pane, and we are viewing a results pane
                    // (the "Search Again" function type is active), then retrieve the search criteria pane
                    // and adjust the available functions. Otherwise if this is just the initial search
                    // criteria pane, then move the PaneHtml to SearchHtml.
                    if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Refresh]))
                    {
                        this.ProcessFunction(this.ObjectId, this.CurrentPaneId,
                            Int32.Parse(this.PaneFunctions[(int)RoundTripFunction.Search]),
                            RoundTripFunction.Search, this.GetCacheState(), "");
                        this.SearchHtml = this.GetPaneHTML(this.StartTabIndex);

                        // Adjust the pane functions.  Remove the function to refine
                        // the search and add the function to perform the search.
                        this.PaneFunctions[(int)RoundTripFunction.Search] = "";
                        List<string> paneFunctions = new List<string>();
                        paneFunctions.AddRange(
                            this.OutriderNet.GetPaneFunctions().Split(new char[] { ',' }));
                        if (!string.IsNullOrEmpty(paneFunctions[(int)RoundTripFunction.PerformSearch]))
                        {
                            this.PaneFunctions[(int)RoundTripFunction.PerformSearch] = paneFunctions[(int)RoundTripFunction.PerformSearch];
                        }
                    }
                    else
                    {
                        this.SearchHtml = this.PaneHtml;
                        this.PaneHtml = "";
                    }
                }
            }
        }

        /// <summary>
        /// Generates the requested search presentation and loads the search and pane html.
        /// </summary>
        /// <returns>True if the search popup should be display, otherwise false.</returns>
        protected virtual bool LoadLookup()
        {
            bool result = true;

            this.LoadPresentationCore(false);

            if (this.HasPresentation)
            {
                if (!this.ComesFromLookupPopup && !this.PreventAutoReturn1Row)
                {
                    result = !this.GetLookupResults1Row();
                }

                if (result)
                {
                    this.SearchHtml = StripFormTags(this.OutriderNet.GetLookupSearchHTML());
                    this.PaneHtml = StripFormTags(this.OutriderNet.GetLookupResultsHTML()).Replace(@"tabindex=""1""", @"tabindex=""10000""");
                    this.GetPaneChangesHTML(); // prime the property with the current Changes HTML
                }
            }

            return result;
        }

        /// <summary>
        /// Processes the current request.
        /// </summary>
        protected virtual void LoadPresentationCore(bool returnAfterSubmit)
        {
            this.LoadPresentationCore(returnAfterSubmit, false);
        }

        /// <summary>
        /// Processes the current request.
        /// </summary>
        protected virtual void LoadPresentationCore(bool returnAfterSubmit,
          bool startDialogWithChanges)
        {
            int paneId, currentPaneId;
            bool autoSave;

            this.SetSiteProperty(SiteProperty.BaseUrl, this.HomePageUrl);
            this.SetSiteProperty(SiteProperty.LookupUrl, this.LookupPageUrl);
            this.SetSiteProperty(SiteProperty.LookupReturnUrl, this.LookupReturnUrl);
            this.SetSiteProperty(SiteProperty.NoteUrl, this.NotePageUrl);
            this.SetSiteProperty(SiteProperty.ReportUrl, this.ReportPageUrl);
            this.SetSiteProperty(SiteProperty.UploadUrl, this.UploadPageUrl);

            if (startDialogWithChanges)
            {
                if (!string.IsNullOrEmpty(this.PresentationId) ||
                  !string.IsNullOrEmpty(this.Presentation))
                {
                    try
                    {
                        // Eventually when the StartDialog function allows data changes to be passed
                        // through, we will update it here.
                        this.StartDialog(this.Request.QueryString.ToString());
                        this.ReleaseOutriderNet();

                        this.StartSession();

                        this.ProcessFunction(this.ObjectId, this.PaneId, this.PaneId, this.RoundTripFunction,
                            this.DataChanges, this.SortColumns, this.ChangesXml);
                    }
                    catch (Exception exception)
                    {
                        this.ProcessError(exception);
                    }

                    if (returnAfterSubmit)
                    {
                        this.HasPresentation = true;
                    }
                    else
                    {
                        this.CheckRedirect();
                    }
                }
            }
            else if (this.ComesFrom == "posse")
            {
                currentPaneId = int.Parse(this.Request.Form["CurrentPaneId"]);

                if (string.IsNullOrEmpty(this.Pane))
                {
                    paneId = int.Parse(this.Request.Form["PaneId"]);
                }
                else
                {
                    // TODO: Gord says its a bug that needs to be fixed in the COM.
                    this.StartDialog(this.Request.QueryString.ToString());

                    paneId = this.GetPaneIdFromName(this.Pane, currentPaneId);
                }

                this.ChangesXml = this.Request.Form["ChangesXml"] + this.ChangesXml;

                try
                {
                    this.ProcessFunction(this.ObjectId, currentPaneId, paneId,
                      this.RoundTripFunction, this.DataChanges, this.SortColumns, this.ChangesXml);

                    // Check to see if the auto-save condition is set for this pane.  If so then
                    // perform a submit.
                    try
                    {
                        autoSave = this.GetCondition(this.PaneId, "AutoSave");
                    }
                    catch
                    {
                        autoSave = false;
                    }

                    if (autoSave)
                    {
                        this.ProcessFunction(null, this.PaneId, this.PaneId, RoundTripFunction.Submit,
                          this.GetCacheState(), null);
                    }
                }
                catch (Exception exception)
                {
                    this.ProcessError(exception);
                }

                if (returnAfterSubmit && (this.RoundTripFunction == RoundTripFunction.Submit ||
                  this.RoundTripFunction == RoundTripFunction.Refresh || this.GetNewObjects().Count > 0))
                {
                    this.HasPresentation = true;
                }
                else
                {
                    this.CheckRedirect();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(this.PresentationId) ||
                  !string.IsNullOrEmpty(this.Presentation))
                {
                    try
                    {
                        this.StartDialog(this.Request.QueryString.ToString());
                        string xmlChangesColumn = this.Request.QueryString["PosseAppendChangesXMLColumn"];

                        if (this.AutoSubmit)
                        {
                            this.ProcessFunction(null, this.PaneId, this.PaneId,
                              RoundTripFunction.Submit, this.GetCacheState(), null);
                        }
                        else if (!string.IsNullOrEmpty(xmlChangesColumn))
                        {
                            OrderedDictionary data = this.GetPaneData(true)[0];
                            string xmlChanges = String.Format("<object id=\"{0}\" action=\"Update\">{1}</object>",
                                                              data["objecthandle"].ToString(),
                                                              data[xmlChangesColumn].ToString());
                            this.OutriderNet.ProcessFunction("", this.OutriderNet.GetPaneId(), this.OutriderNet.GetPaneId(),
                                RoundTripFunction.Refresh, this.OutriderNet.GetCacheState(), "", xmlChanges);
                        }
                    }
                    catch (Exception exception)
                    {
                        this.ProcessError(exception);
                    }

                    if (returnAfterSubmit)
                    {
                        this.HasPresentation = true;
                    }
                    else
                    {
                        this.CheckRedirect();
                    }
                }
            }

            this.LoadPane();
        }

        /// <summary>
        /// Populates the function list, tab list and other properties based on the current pane.
        /// </summary>
        protected virtual void LoadPane()
        {
            PaneInfo pane;

            if (this.HasPresentation)
            {
                this.CurrentPaneId = this.PaneId;
                pane = this.OutriderNet.GetPaneInfo();

                this.PresentationName = pane.PresentationName;
                this.ObjectDefName = pane.ObjectDefName;
                this.PaneName = pane.PaneName;
                this.PresentationType = pane.PresentationType;

                if (this.PresentationType == PresType.Tabs ||
                    this.PresentationType == PresType.Wizard)
                {
                    this.Panes = this.OutriderNet.GetVisiblePaneInfo();
                }

                this.PaneFunctions = new List<string>();
                this.PaneFunctions.AddRange(
                    this.OutriderNet.GetPaneFunctions().Split(new char[] { ',' }));
                this.CurrentUrl = this.OutriderNet.GetURL();
                this.PaneLabel = this.OutriderNet.GetPaneLabel();
                this.PresentationTitle = this.OutriderNet.GetPresentationTitle();

                // For now we are hard coding this shortcut for processes tabs.
                if (this.PaneName == "JobSummary" || this.PaneName == "Processes")
                {
                    this.InsertChoices["Processes"] =
                      this.GetInsertChoices(this.GetPaneId("Processes"));
                }
            }
        }

        /// <summary>
        /// Load the specified pane.  This assumes that StartDialog has been called
        /// and a presentation is already in context.
        /// </summary>
        /// <param name="objectId">The object id of the pane to retrieve.</param>
        /// <param name="paneId">The pane id of the pane to retrieve.</param>
        /// <param name="sortColumns">The sortColumns form value from Outrider to use when fetching the pane. Only specify if this is the main pane (not a header pane).</param>
        protected virtual void FetchPane(string objectId, int paneId, string sortColumns = null)
        {
            try
            {
                this.ProcessFunction(objectId, paneId, paneId,
                    RoundTripFunction.Refresh, this.GetCacheState(), sortColumns);
            }
            catch (Exception exception)
            {
                this.ProcessError(exception);
            }
        }

        /// <summary>
        /// Redirects the user to another url if specified, otherwise enables the current presentation.
        /// </summary>
        protected virtual void CheckRedirect()
        {
            string url = this.OutriderNet.GetRedirect();

            if (string.IsNullOrEmpty(url))
            {
                if (string.IsNullOrEmpty(this.Response.RedirectLocation))
                {
                    this.HasPresentation = true;
                }
            }
            else
            {
                this.ReleaseOutriderNet();
                this.Response.Redirect(url);
            }
        }

        /// <summary>
        /// Returns the id of the specified pane.
        /// </summary>
        /// <param name="paneName">The name of the requested pane.</param>
        /// <param name="id">The id of the current pane.</param>
        /// <returns>The pane id for the specified pane.</returns>
        protected virtual int GetPaneIdFromName(string paneName, int id)
        {
            PaneInfo pane = this.OutriderNet.GetPaneInfo(id);

            return this.OutriderNet.GetPaneId(paneName, pane.PresentationType, pane.PresentationName, pane.ObjectDefName);
        }

        /// <summary>
        /// Returns the javascript action for the specified pane.
        /// </summary>
        /// <param name="paneId">The pane id to construct the javascript for.</param>
        /// <returns>The javascript for the pane.</returns>
        protected virtual string GetTabAction(string paneId)
        {
            return string.Format("javascript:PosseSubmitLink('{0}', {1}, {2})",
              this.CurrentUrl, (int)RoundTripFunction.Refresh, paneId);
        }

        /// <summary>
        /// Returns the javascript action for the specified job pane that is being displayed on a process.
        /// </summary>
        /// <param name="paneName">The pane name to construct the javascript for.</param>
        /// <returns>The javascript for the pane.</returns>
        protected virtual string GetJobTabAction(string paneName)
        {
            return string.Format("javascript:PosseNavigate('{0}&DisplayJobPossePane={1}')",
              this.CurrentUrl, paneName);
        }

        /// <summary>
        /// Indicates if the specified function is supported by the current pane.
        /// </summary>
        /// <param name="functionType">The function type to chekc.</param>
        /// <returns>True if the function exists.  False otherwise.</returns>
        protected virtual bool HasFunction(RoundTripFunction functionType)
        {
            bool result = false;

            if ((int)functionType <= this.PaneFunctions.Count - 1)
            {
                result = !string.IsNullOrEmpty(this.PaneFunctions[(int)functionType]);
            }

            return result;
        }

        /// <summary>
        /// Ends the current session and redirects the specified page.
        /// </summary>
        /// <param name="url">The url to redirect to.</param>
        protected virtual void Logout(string url)
        {
            this.OutriderNet.ExpireSession(this.SessionId, this.DebugKey);
            this.SessionId = this.NoSession;
            this.ReleaseOutriderNet();
            this.Response.Redirect(url);
        }

        /// <summary>
        /// Ends the current session and redirects the user to the home page.
        /// </summary>
        protected virtual void Logout()
        {
            this.Logout(this.HomePageUrl);
        }

        /// <summary>
        /// Adds a message to be displayed to the user in the validation summary.
        /// </summary>
        /// <param name="message">The message to be displayed.</param>
        protected virtual void SetValidationMessage(string message)
        {
            CustomValidator validator = new CustomValidator();

            validator.ErrorMessage = message;
            validator.IsValid = false;

            this.Validators.Add(validator);
        }

        /// <summary>
        /// Changes the password for the current user.
        /// </summary>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns>True if the change was successful otherwise false.</returns>
        protected virtual bool ChangePassword(string oldPassword, string newPassword)
        {
            bool result;

            result = this.OutriderNet.ChangePassword(oldPassword, newPassword);
            this.ReleaseOutriderNet();

            return result;
        }

        /// <summary>
        /// Returns the status of the given condition for the specified pane.
        /// </summary>
        /// <param name="paneId">The pane id to evaluate the condition for.</param>
        /// <param name="condition">The condition to be returned.</param>
        /// <returns>True if the condition is true otherwise false.</returns>
        protected virtual bool GetCondition(int paneId, string condition)
        {
            return this.OutriderNet.GetCondition(paneId, condition);
        }

        /// <summary>
        /// Returns the status of the highest priority condition for the current pane.
        /// </summary>
        /// <returns>True if the condition is true otherwise false.</returns>
        protected virtual string GetFirstCondition()
        {
            return this.OutriderNet.GetFirstCondition();
        }

        /// <summary>
        /// Returns the status of the highest priority condition for the specified pane.
        /// </summary>
        /// <param name="paneId">The pane id to evaluate the condition for.</param>
        /// <returns>True if the condition is true otherwise false.</returns>
        protected virtual string GetFirstCondition(int paneId)
        {
            return this.OutriderNet.GetFirstCondition(paneId);
        }

        /// <summary>
        /// Generates a java script block that sets focus to the specified control when the page is
        /// presented to the user.
        /// </summary>
        /// <param name="control">A reference to the control.</param>
        protected virtual void SetInitialFocus(WebControl control)
        {
            if (control != null)
            {
                this.ClientScript.RegisterStartupScript(this.GetType(), "SetInitialFocus",
                  string.Format("PosseGetElement('{0}').focus();", control.ClientID), true);
            }
        }

        /// <summary>
        /// Saves the specified document with the given file name extension.
        /// </summary>
        /// <param name="data">The new document data.</param>
        /// <param name="fileName">The file name of the new document.</param>
        /// <param name="extension">The file name extension of the new document.</param>
        /// <returns>True if a new document was created otherwise false.</returns>
        protected virtual bool UploadDocument(byte[] data, string fileName, string extension)
        {
            this.LastUploadDocumentId = this.SetDocument(int.Parse(this.ObjectDefId),
                Path.GetFileNameWithoutExtension(fileName), extension, data);

            return (this.LastUploadDocumentId > 0);
        }

        /// <summary>
        /// Returns the data associated with the current pane.
        /// </summary>
        /// <returns>Record set of pane data.</returns>
        protected virtual List<OrderedDictionary> GetPaneData()
        {
            return this.OutriderNet.GetPaneRecordSet();
        }

        /// <summary>
        /// Returns the data associated with the current pane.
        /// </summary>
        /// <param name="includeObjectHandle">Indicates if the object hanlde should be included.</param>
        /// <returns>Record set of pane data.</returns>
        protected virtual List<OrderedDictionary> GetPaneData(bool includeObjectHandle)
        {
            return this.OutriderNet.GetPaneRecordSet(includeObjectHandle);
        }

        /// <summary>
        /// Returns a list containing the users that can be assigned to a
        /// process, with an indication of whether they are currently assigned.
        /// </summary>
        /// <param name="objectId">The unique identifier of the process.</param>
        /// <returns>The specified user data as a record set.</returns>
        protected virtual List<PossibleAssignment> GetPossibleAssignments(string objectId)
        {
            return this.OutriderNet.GetPossibleAssignments(objectId);
        }

        /// <summary>
        /// Returns a list of newly created objects.
        /// </summary>
        /// <returns>The list of new objects as a record set.</returns>
        protected virtual List<NewObject> GetNewObjects()
        {
            return this.OutriderNet.GetNewObjectXRef();
        }

        /// <summary>
        /// Returns a list of the items which can be inserted on the
        /// given pane.
        /// </summary>
        /// <returns>The list of insertable items for the specified pane as a record set.</returns>
        protected virtual List<InsertChoice> GetInsertChoices(int paneId)
        {
            return this.OutriderNet.GetInsertChoices(paneId);
        }

        /// <summary>
        /// Returns a list of the deficiencies that can be created for
        /// the checklist item.
        /// </summary>
        /// <returns>The list of possible deficiencies as a record set.</returns>
        protected virtual List<PossibleDeficiency> GetPossibleDeficiencies(string processId, string columnDefName)
        {
            return this.OutriderNet.GetPossibleDeficienciesForColumn(processId, columnDefName);
        }

        /// <summary>
        /// Encodes a string to be represented as a javascript string literal.
        /// The string returned includes outer quotes.
        /// </summary>
        /// <remarks>
        /// This function will encode the input such that the returned string can
        /// be treated as a javascript string literal and included in a generated script.
        /// When generating a script, any arbitrary text content that is going to be assigned to
        /// a variable in the script or otherwise included as a string literal,
        /// should be passed through this function first.
        /// </remarks>
        /// <param name="value">The string to encode.</param>
        /// <returns>The encoded string.</returns>
        protected virtual string EncodeJSString(string value)
        {
            StringBuilder result = new StringBuilder("\"");
            int ascii;

            foreach (char chr in value)
            {
                switch (chr)
                {
                    case '"':
                        result.Append("\\\"");
                        break;
                    case '\\':
                        result.Append("\\");
                        break;
                    case '\b':
                        result.Append("\\b");
                        break;
                    case '\f':
                        result.Append("\\f");
                        break;
                    case '\n':
                        result.Append("\\n");
                        break;
                    case '\r':
                        result.Append("\\r");
                        break;
                    case '\t':
                        result.Append("\\t");
                        break;
                    case '\v':
                        result.Append("\\v");
                        break;
                    default:
                        ascii = (int)chr;

                        if (ascii < 32 || ascii > 127)
                        {
                            result.AppendFormat("\\u{0:X04}", ascii);
                        }
                        else
                        {
                            result.Append(chr);
                        }
                        break;
                }
            }

            result.Append("\"");

            return result.ToString();
        }

        /// <summary>
        /// Returns a boolean indicating whether the lookup found exactly 1 row.
        /// </summary>
        /// <returns>Whether the lookup found exactly one row.</returns>
        protected virtual bool GetLookupResults1Row()
        {
            return this.OutriderNet.GetLookupResults1Row();
        }

        /// <summary>
        /// Returns the JavaScript that sets page fields for the 1 row found.
        /// </summary>
        /// <param name="usingAjax">Does this browser support AJAX?</param>
        /// <param name="dataChanges">The coded data changes for all outstanding changes for this presentation.</param>
        /// <returns>Java script.</returns>
        protected virtual string GetLookupResultsJavaScript(bool usingAjax, string dataChanges)
        {
            return this.OutriderNet.GetLookupResultsJavaScript(usingAjax, dataChanges);
        }

        /// <summary>
        /// Updates the changes indicated in the XML to the database.
        /// </summary>
        /// <param name="xml">Data changes in an XML format.</param>
        protected virtual void ProcessXML(string xml)
        {
            this.OutriderNet.ProcessXML(xml);
        }

        /// <summary>
        /// Starts a dialog from an entrypoint URL.
        /// </summary>
        /// <param name="queryString">The parameters on the current page's URL.</param>
        protected virtual void StartDialog(string queryString)
        {
            this.paneId = 0;
            this.OutriderNet.StartDialog(queryString);
        }

        /// <summary>
        /// Returns the HTML minus the outer form tags.
        /// </summary>
        /// <param name="inString">The HTML wrapped in a form tag</param>
        /// <returns>The HTML minus the outer form tags.</returns>
        protected virtual string StripFormTags(string inString)
        {
            int pos = inString.IndexOf('>') + 2;
            return inString.Substring(pos, inString.Length - (8 + pos));
        }

        /// <summary>
        /// Creates an e-com transaction returning the transaction identifier.
        /// </summary>
        /// <remarks>
        /// Create and return the string identifier of an EComTransaction job or object according
        /// to the configuration specified for this Outrider system. Attach the list of objects
        /// or the shopping cart object to the transaction.  If a preparation process was
        /// configured for the transaction type, create and complete it with the configured
        /// outcome (to trigger any needed workflow).
        /// </remarks>
        /// <param name="name">
        /// The name of the transaction type to use.
        /// </param>
        /// <param name="objectList">
        /// An array of ObjectIds of the objects to be paid for using the transaction.
        /// </param>
        /// <returns>
        /// The identifier of the EcomTransaction "object".
        /// </returns>
        protected virtual string CreateEComTransaction(string name, int[] objectList)
        {
            return this.OutriderNet.CreateEComTransaction(name, objectList);
        }

        /// <summary>
        /// Creates an e-com transaction returning the transaction identifier.
        /// </summary>
        /// <remarks>
        /// Create and return the string identifier of an EComTransaction job or object according
        /// to the configuration specified for this Outrider system. Attach the list of objects
        /// or the shopping cart object to the transaction.  If a preparation process was
        /// configured for the transaction type, create and complete it with the configured
        /// outcome (to trigger any needed workflow).
        /// </remarks>
        /// <param name="name">
        /// The name of the transaction type to use.
        /// </param>
        /// <param name="shoppingCartObjectId">
        /// The objectId of an explicit shopping cart that already contains links to the
        /// items being purchased.
        /// </param>
        /// <returns>
        /// The identifier of the EcomTransaction "object".
        /// </returns>
        protected virtual string CreateEComTransaction(string name, int shoppingCartObjectId)
        {
            return this.OutriderNet.CreateEComTransaction(name, shoppingCartObjectId);
        }

        /// <summary>
        /// Records the successful payment of the e-com transaction.
        /// </summary>
        /// <param name="name">The name of the transaction type to use.</param>
        /// <param name="transactionId">The unique identifier of the transaction.</param>
        /// <param name="receipt">The confirmation number from the ecommerce provider as a reference to the transaction.</param>
        protected virtual void RecordEComTransactionAcceptance(string name, string transactionId, string receipt)
        {
            this.OutriderNet.RecordEComTransactionAcceptance(name, transactionId, receipt);
        }

        /// <summary>
        /// Records the successful payment of the e-com transaction.
        /// </summary>
        /// <param name="name">The name of the transaction type to use.</param>
        /// <param name="transactionId">The unique identifier of the transaction.</param>
        /// <param name="receipt">The confirmation number from the ecommerce provider as a reference to the transaction.</param>
        /// <param name="payment">The amount of the payment.  If provided, this will be compared to the payment amount determined when the transaction was created, and an error will be raised if the amounts differ.</param>
        protected virtual void RecordEComTransactionAcceptance(string name, string transactionId, string receipt, decimal payment)
        {
            this.OutriderNet.RecordEComTransactionAcceptance(name, transactionId, receipt, payment);
        }

        /// <summary>
        /// Records the denial of the e-com transaction.
        /// </summary>
        /// <param name="name">The name of the transaction type to use.</param>
        /// <param name="transactionId">The unique identifier of the transaction.</param>
        protected virtual void RecordEComTransactionDenial(string name, string transactionId)
        {
            this.OutriderNet.RecordEComTransactionDenial(name, transactionId);
        }

        /// <summary>
        /// Returns the total amount to be paid by the e-com transaction.
        /// </summary>
        /// <param name="name">The name of the transaction type to use.</param>
        /// <param name="transactionId">The unique identifier of the transaction.</param>
        protected virtual decimal GetEComTransactionFee(string name, string transactionId)
        {
            return this.OutriderNet.GetEComTransactionFee(name, transactionId);
        }

        /// <summary>
        /// Verify that the value passed in is alpha-numeric with underscores.
        /// </summary>
        /// <param name="name">The name of the URL parameter.</param>
        /// <param name="value">The value of the URL parameter.</param>
        protected virtual void ValidateUrlParameter(string name, string value)
        {
            if(value != null && !value.All(c => char.IsLetterOrDigit(c) || c == '_'))
            {
                OutriderNet.LogMessage(LogLevel.Warning,
                        String.Format("Error on page {0}\n  Invalid query string {1} parameter value \"{2}\"",
                        this.Request.Url.ToString(), name, value));
                throw new HttpException(400,
                        String.Format("Invalid query string parameter value for {0}.",
                        name));
            }
        }

        /// <summary>
        /// Returns the internal ObjectId of the e-com transaction.
        /// </summary>
        /// <param name="name">The name of the transaction type to use.</param>
        /// <param name="transactionId">The unique identifier of the transaction.</param>
        protected virtual int GetEComTransactionObjectId(string name, string transactionId)
        {
            return this.OutriderNet.GetEComTransactionObjectId(name, transactionId);
        }
    /// <summary>
    /// Logs the time that a SiteBusy error occured.
    /// </summary>
    public virtual void WriteToSiteBusyLog()
    {
        try
        {
            // Generate the logfile name
            string fileName = String.Format("{0:s}SiteBusy-{1:s}-{2:s}.log", this.SiteBusyLogDirectory,
                    ConfigurationManager.AppSettings["HostName"], this.SystemName);
            if (!File.Exists(fileName))
            {
                File.Create(fileName).Close();
            }
            using (StreamWriter w = File.AppendText(fileName))
            {
                w.WriteLine("{0} {1} - User redirected to SiteBusy.html",
                        DateTime.Today.ToString("yyyy.MM.dd"), DateTime.Now.ToString("HH:mm:ss.fffffff"));
                w.Flush();
                w.Close();
            }
        }
        catch (Exception)
        {
            // If it doesn't work, we'll just skip it as we don't want the user to see the message.
        }
    }
    #endregion
  }
}
