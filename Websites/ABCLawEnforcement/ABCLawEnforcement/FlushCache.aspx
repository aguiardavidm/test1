<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FlushCache.aspx.cs" Inherits="Computronix.POSSE.Outrider.FlushCache" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
    <title>Flush Cache</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<asp:Button ID="btnFlush" runat="server" OnClick="btnFlush_Click" Text="Flush Cache" /></div>
    </form>
</body>
</html>
