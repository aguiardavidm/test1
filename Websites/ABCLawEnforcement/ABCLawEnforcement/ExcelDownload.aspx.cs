using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;

using ClosedXML.Excel;

namespace Computronix.POSSE.Outrider
{
    public partial class ExcelDownload : Computronix.POSSE.Outrider.PageBaseExt
        {
                #region Event Handlers
                /// <summary>
                /// Page load event handler.
                /// </summary>
                /// <param name="sender">A reference to the page.</param>
                /// <param name="e">Event arguments.</param>
                protected void Page_Load(object sender, EventArgs e)
                {
                        this.RenderExcel();
                }

        /// <summary>
        /// Return a stream from a string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        protected Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Transform the input XML from an Outrider Excel Report tab into CSV
        /// </summary>
        /// <param name="inputXML">XML from an Outrider tab configured with the Excel Report pattern</param>
        /// <param name="outputCSV">Stream for the output CSV to be written to.</param>
        protected void TransformToCSV(string inputXML, Stream outputCSV)
        {
            // Parse the XML and generate the CSV
            StringBuilder csv = new StringBuilder();
            using (Stream s = GenerateStreamFromString(inputXML))
            {
                XmlTextReader reader = new XmlTextReader(s);
                StringBuilder row = new StringBuilder();
                StringBuilder headerRow = new StringBuilder();
                bool inColumn = false, isDate = false;
                int rowNum = 0, colNum = 0;
                while (reader.Read())
                {
                    if (reader.Name.ToLower() == "data" && reader.NodeType == XmlNodeType.EndElement)
                    {
                        // Write the row to the CSV
                        if (row.Length > 0)
                        {
                            if (rowNum == 0)
                                csv.AppendLine(headerRow.ToString());
                            csv.AppendLine(row.ToString());
                            row.Remove(0, row.Length);
                        }
                        colNum = 0;
                        rowNum++;
                    }
                    else if (reader.Name.ToLower().StartsWith("col") && reader.NodeType == XmlNodeType.Element)
                    {
                        inColumn = true;

                        // Append a comma
                        if (colNum > 0)
                            row.Append(",");

                        // Parse the attributes
                        while (reader.MoveToNextAttribute())
                        {
                            switch (reader.Name.ToLower())
                            {
                                case "label":
                                    // Write the column name to the header if this is the first row
                                    if (rowNum == 0)
                                    {
                                        if (colNum > 0)
                                            headerRow.Append(",");
                                        headerRow.Append(reader.Value);
                                    }
                                    break;
                                case "datatype":
                                    isDate = (reader.Value.ToLower() == "date");
                                    break;
                            }
                        }
                    }
                    else if (reader.Name.ToLower().StartsWith("col") && reader.NodeType == XmlNodeType.EndElement)
                    {
                        inColumn = false;
                        colNum++;
                    }
                    else if (reader.NodeType == XmlNodeType.Text && inColumn)
                    {
                        // Append the value
                        string value = reader.Value;
                        if (isDate && value.EndsWith(" 00:00:00"))
                            value = value.Replace(" 00:00:00", "");  // Remove time portion if none is specified
                        if (value.Contains("\n") || value.Contains("\r") || value.Contains(",") || value.Contains("\""))
                            row.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                        else
                            row.Append(value);
                    }
                }
            }

            // Write the output CSV
            StreamWriter writer = new StreamWriter(outputCSV);
            writer.Write(csv.ToString());
            writer.Flush();
        }

        /// <summary>
        /// Transform the input XML from an Outrider Excel Report tab into XLSX (OpenDocument)
        /// </summary>
        /// <param name="inputXML">XML from an Outrider tab configured with the Excel Report pattern</param>
        /// <param name="outputCSV">Stream for the output CSV to be written to.</param>
        protected void TransformToXLSX(string inputXML, Stream outputXLSX)
        {
            // Parse the XML and generate the XLSX
            XLWorkbook workBook = new XLWorkbook();
            IXLWorksheet dataSheet = workBook.Worksheets.Add("Data");
            IXLWorksheet criteriaSheet = null;

            using (Stream s = GenerateStreamFromString(inputXML))
            {
                XmlTextReader reader = new XmlTextReader(s);
                try
                {
                    while (reader.Read())
                    {
                        if (reader.Name.ToLower() == "table" && reader.NodeType == XmlNodeType.Element)
                        {
                            // We are now in the data block.
                            ReadTableFromXML(dataSheet, reader);
                        }
                        else if (reader.Name.ToLower() == "criteria" && reader.NodeType == XmlNodeType.Element)
                        {
                            // We are now in the criteria block.
                            if (criteriaSheet == null)
                                criteriaSheet = workBook.Worksheets.Add("Criteria");
                            ReadCriteriaFromXML(criteriaSheet, reader);
                        }
                    }
                }
                catch (XmlException ex)
                {
                    string problemLine = GetXMLAtError(inputXML, reader.LineNumber, reader.LinePosition, 132);
                    if (problemLine != null)
                    {
                        throw new XmlException(String.Format("Can't parse [{0}]",HttpUtility.HtmlEncode(problemLine)),ex);
                    }
                    throw;
                }
            }
            workBook.SaveAs(outputXLSX);
        }

        private static void ReadTableFromXML(IXLWorksheet dataSheet, XmlTextReader reader)
        {
            int rowNum = 2;  // The data starts on the second row. (Headings on row 1)
            int colMax = 1;
            int colCur = 1;  // The data starts on the second column.

            bool inData = false;
            bool inColumn = false;
            string dataType = null;

            while (reader.Read())
            {
                if (reader.Name.ToLower() == "table" && reader.NodeType == XmlNodeType.EndElement)
                {
                    // Do some final formatting to make it pretty and a bit more functional... (if data exists)
                    colMax--;
                    rowNum--;

                    if (colMax > 0 && rowNum > 1)
                    {
                        BeautifyTable(dataSheet, rowNum, colMax);
                    }
                    return;
                }
                else if (reader.Name.ToLower() == "data" && reader.NodeType == XmlNodeType.Element)
                {
                    inData = true;
                }
                else if (reader.Name.ToLower() == "data" && reader.NodeType == XmlNodeType.EndElement)
                {
                    inData = false;
                    colMax = colCur;
                    colCur = 1;
                    rowNum++;
                }
                else if (inData && reader.NodeType == XmlNodeType.Element)
                {
                    string columnName = reader.Name; // reader.Name.ToLower().StartsWith("col")
                    inColumn = true;
                    // Parse the attributes
                    while (reader.MoveToNextAttribute())
                    {
                        switch (reader.Name.ToLower())
                        {
                            case "label":
                                // Write the column name to the header if this is the first row
                                columnName = reader.Value;
                                break;
                            case "datatype":
                                dataType = reader.Value.ToLower();
                                break;
                        }
                    }

                    if (rowNum == 2)
                    {
                        dataSheet.Cell(1, colCur).Value = columnName;
                    }
                }
                else if (inData && reader.NodeType == XmlNodeType.EndElement)
                {
                    inColumn = false;
                    colCur++;
                }
                else if (reader.NodeType == XmlNodeType.Text && inColumn)
                {
                    // Append the value
                    SetColumnValue(dataSheet, rowNum, colCur, reader, dataType);
                }
            }
        }

        private static void ReadCriteriaFromXML(IXLWorksheet criteriaSheet, XmlTextReader reader)
        {
            const int colMax = 2;
            int rowNum = 1;

            bool inParameter = false;

            criteriaSheet.Cell(rowNum, 1).Value = "Parameter Name";
            criteriaSheet.Cell(rowNum, 2).Value = "Parameter Value";

            while (reader.Read())
            {
                if (reader.Name.ToLower() == "criteria" && reader.NodeType == XmlNodeType.EndElement)
                {
                    // Do some final formatting to make it pretty and a bit more functional... (if data exists)
                    if (rowNum > 1)
                    {
                        BeautifyTable(criteriaSheet, rowNum, colMax);
                    }
                    return;
                }
                else if (reader.Name.ToLower() == "parameter" && reader.NodeType == XmlNodeType.Element)
                {
                    rowNum++;
                    inParameter = true;
                    // Parse the attributes
                    while (reader.MoveToNextAttribute())
                    {
                        switch (reader.Name.ToLower())
                        {
                            case "name":
                                // Write the column name to the header if this is the first row
                                SetColumnValue(criteriaSheet, rowNum, 1, reader, "string");
                                break;
                        }
                    }
                }
                else if (reader.Name.ToLower() == "parameter" && reader.NodeType == XmlNodeType.EndElement)
                {
                    inParameter = false;
                }
                else if (reader.NodeType == XmlNodeType.Text && inParameter)
                {
                    // Append the value
                    SetColumnValue(criteriaSheet, rowNum, 2, reader, null);
                }
            }
        }

        private static void BeautifyTable(IXLWorksheet criteriaSheet, int rows, int cols)
        {
            // Turn the data into a table with filters.
            var rngData = criteriaSheet.Range(1, 1, rows, cols);
            rngData.CreateTable();

            // Autoformat the column widths.
            criteriaSheet.Columns(1, cols).AdjustToContents();

            // Freeze the top row.
            criteriaSheet.SheetView.FreezeRows(1);
        }

        private static void SetColumnValue(IXLWorksheet workSheet, int rowNum, int colCur, XmlTextReader reader, string dataType)
        {
            string value = reader.Value;

            if (dataType == null)
            {
                // If no type is specified then we do some investigative work.
                DateTime dateValue;
                Double doubleValue;
                Boolean boolValue;
                if (DateTime.TryParseExact(value, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
                    dataType = "date";
                else if (Double.TryParse(value, out doubleValue))
                    dataType = "number";
                else if (Boolean.TryParse(value, out boolValue))
                    dataType = "boolean";
                else
                    dataType = "string";
            }

            switch (dataType.ToLower())
            {
                case "date":
                    if (value.EndsWith(" 00:00:00"))
                    {
                        value = value.Replace(" 00:00:00", ""); // Remove time portion if none is specified
                    }
                    workSheet.Cell(rowNum, colCur).SetDataType(XLCellValues.DateTime);
                    workSheet.Cell(rowNum, colCur).SetValue<DateTime>(Convert.ToDateTime(reader.Value));
                    break;
                case "number":
                    workSheet.Cell(rowNum, colCur).SetDataType(XLCellValues.Number);
                    workSheet.Cell(rowNum, colCur).SetValue<Decimal>(Convert.ToDecimal(reader.Value));
                    break;
                case "boolean": // Future use???
                    workSheet.Cell(rowNum, colCur).SetDataType(XLCellValues.Boolean);
                    workSheet.Cell(rowNum, colCur).SetValue<Boolean>(Convert.ToBoolean(reader.Value));
                    break;
                case "string":
                    workSheet.Cell(rowNum, colCur).SetDataType(XLCellValues.Text);
                    workSheet.Cell(rowNum, colCur).SetValue<String>(reader.Value.ToString());
                    break;
                default: // Deals with anything else.....
                    workSheet.Cell(rowNum, colCur).SetDataType(XLCellValues.Text);
                    workSheet.Cell(rowNum, colCur).SetValue<String>(reader.Value.ToString());
                    break;
            }
        }

        private string GetXMLAtError(string input, int lineNumber, int linePosition, int maxLength)
        {
            var lines = input.Split('\n');
            if (lines.Length >= lineNumber)
            {
                string line = lines[lineNumber - 1];

                if (line.Length > maxLength)
                {
                    int start = linePosition - (maxLength / 2);
                    if (start <= 0)
                        return line.Substring(0, maxLength) + "...";
                    else if ((start + maxLength) > line.Length)
                        return "..." + line.Substring(line.Length - maxLength, maxLength);
                    else
                        return "..." + line.Substring(start, maxLength) + "...";
                }
                else
                    return line;
            }

            return null;
        }

        /// <summary>
        /// Format the criteria XML in preparation for transformation.
        /// </summary>
        /// <param name="data">Criteria data retrieved from GetPaneRecordset()</param>
        /// <returns>Formatted criteria XML</returns>
        protected string FormatCriteriaXML(OrderedDictionary data)
        {
            StringBuilder criteria = new StringBuilder();

            criteria.Append("<Criteria>");
            foreach (string key in data.Keys)
            {
                if (data[key] is DateTime)
                    criteria.AppendFormat("<Parameter name=\"{0}\">{1:yyyy}-{1:MM}-{1:dd} {1:HH}:{1:mm}:{1:ss}</Parameter>", key, (DateTime)data[key]);
                else
                    criteria.AppendFormat("<Parameter name=\"{0}\">{1}</Parameter>", key, FormatAsXMLValue(data[key]));
            }

            // Add the current date
            criteria.AppendFormat("<Parameter name=\"Report Run On\">{0:yyyy}-{0:MM}-{0:dd} {0:HH}:{0:mm}:{0:ss}</Parameter>", DateTime.Now);
            criteria.Append("</Criteria>");
            return criteria.ToString();
        }

        protected string FormatAsXMLValue(object input)
        {
            if (input == null)
                return null;
            else
                return System.Security.SecurityElement.Escape(input.ToString());
        }

		/// <summary>
		/// Renders search results as XML and streams it out to the browser
		/// </summary>
		protected void RenderExcel()
		{
            string fileName, contentType, inputXML = "", criteriaXML = "", criteriaRecordSet = this.Request.QueryString["CriteriaRecord"];
            int startPos, endPos, objectId;

            bool excelReport = (this.Request.QueryString["SaveAsExcelReport"] == "Y");
            bool csvReport = (this.Request.QueryString["SaveAsCSVReport"] == "Y");
            int.TryParse(this.Request.QueryString["PosseObjectId"], out objectId);

            // If this is an Excel Report, first get the criteria
            criteriaXML = "";
            if (excelReport)
            {
                if (!string.IsNullOrEmpty(this.Request.Form["CurrentPaneId"]))
                {
                int paneId = int.Parse(this.Request.Form["CurrentPaneId"]);
                this.ProcessFunction(this.ObjectId, paneId, paneId, RoundTripFunction.Refresh,
                    Request.Form["DataChanges"], Request.Form["SortColumns"], Request.Form["ChangesXml"]);
                criteriaXML = FormatCriteriaXML(this.OutriderNet.GetPaneRecordSet()[0]);
            }
            }

            // Load the presentation
            this.LoadPresentation();
            this.CheckClient();

            // Get the input XML for Excel/CSV Reports
            if (excelReport || csvReport)
            {
                inputXML = this.PaneHtml;
                startPos = inputXML.IndexOf("~~~**~~**~~~");
                endPos = startPos > -1 ? inputXML.IndexOf("~~~**~~**~~~", startPos + 12) : -1;
                if (startPos > -1 && endPos > -1)
                    inputXML = inputXML.Substring(startPos + 12, endPos - startPos - 12);
                else
                    inputXML = "<Table></Table>";
            }
            // Get the criteria and results for searches
            else
            {
                if (this.EmbedCriteriaOnSearches)
                {
                    // If this is an embedded criteria pane, the first pane will be the criteria.
                    // Then, perform the search to retrieve the data
                    criteriaXML = FormatCriteriaXML(this.OutriderNet.GetPaneRecordSet()[0]);
                    if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]))
                    {
                        this.ProcessFunction(this.ObjectId, this.CurrentPaneId,
                            Int32.Parse(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]),
                            RoundTripFunction.PerformSearch, this.GetCacheState(), "");
                        inputXML = this.OutriderNet.GetPaneXML("Table", "Data");
                    }
                }
                else if (!string.IsNullOrEmpty(criteriaRecordSet) && objectId != null)
                {
                    criteriaXML = FormatCriteriaXML(this.OutriderNet.GetRecordSet(criteriaRecordSet, objectId)[0]);
                    inputXML = this.OutriderNet.GetPaneXML("Table", "Data");
                }
                else
            {
                    // Retrieve the data first and then navigate back to the criteria pane to retrieve the criteria
                    inputXML = this.OutriderNet.GetPaneXML("Table", "Data");
                    if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Search]))
                    {
                        this.ProcessFunction(this.ObjectId, this.CurrentPaneId,
                            Int32.Parse(this.PaneFunctions[(int)RoundTripFunction.Search]),
                            RoundTripFunction.Search, this.GetCacheState(), "");
                        criteriaXML = FormatCriteriaXML(this.OutriderNet.GetPaneRecordSet()[0]);
                    }
                }
            }

            // Transform and stream the output
            using (MemoryStream output = new MemoryStream())
            {
                if (csvReport)
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["ExportToExcelFileName"]))
                        fileName = this.Request.QueryString["ExportToExcelFileName"] + ".csv";
                    else
                        fileName = this.PaneLabel + ".csv";
                    contentType = "text/csv";
                    TransformToCSV(inputXML, output);
                }
                else if (excelReport)
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["ExportToExcelFileName"]))
                        fileName = this.Request.QueryString["ExportToExcelFileName"] + ".xlsx";
                    else
                        fileName = this.PaneLabel + ".xlsx";
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    string xml = String.Format("<Report>{0}{1}</Report>", inputXML, criteriaXML);
                    TransformToXLSX(xml, output);
                }
                else
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["ExportToExcelFileName"]))
                        fileName = this.Request.QueryString["ExportToExcelFileName"] + ".xlsx";
                    else
                        fileName = this.Request.QueryString["PossePresentation"] + ".xlsx";
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    string xml = String.Format("<Report>{0}{1}</Report>", inputXML, criteriaXML);
                    TransformToXLSX(xml, output);
                }

                // Write output to Response stream
                this.CheckClient();

                // This version appears to be simpler and more reliable...
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Clear();
                Response.Buffer = true;

                Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName);
                Response.AddHeader("Content-Length", output.Length.ToString());
                Response.ContentType = contentType;

                output.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
        #endregion
    }
}