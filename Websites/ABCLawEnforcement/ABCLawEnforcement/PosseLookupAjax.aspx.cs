using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class PosseLookupAjax : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetSiteProperty(SiteProperty.BaseUrl, this.HomePageUrl);
            this.SetSiteProperty(SiteProperty.LookupReturnUrl, 
                this.LookupReturnUrl);

            if (this.CanDoLookupAJAX)
            {
                this.Response.Write(this.GetLookupScript());
            }
            else
            {
                this.Response.Write(
                    "<script language=javascript>\r\n" + 
                    "var vPosseWindow = parent;\r\n" +
                    "var vPosseScriptSnippet = function () {\r\n" +
                    this.GetLookupScript() + "\r\n"+
                    "}\r\n" +
                    "vPosseWindow.PosseLookupExecuteScript(" +
                    "vPosseScriptSnippet);\r\n" +
                    "</script>");
            }
        }
        #endregion
    }
}
