using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class SelectObjectsExt : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Literal html;
            string currentObjectIds = this.Request.QueryString["CurrentObjectIds"], columnName = this.Request.QueryString["ColumnName"];
            int objCount = 0;
            Dictionary<string, bool> currentObjects = new Dictionary<string, bool>();
            List<string> selectedObjects = new List<string>();

            foreach (string objectId in currentObjectIds.Split(new char[] { ',' }))
            {
                string trimmedObjectId = objectId.Trim();
                this.ValidateUrlParameter("CurrentObjectIds", trimmedObjectId);
                currentObjects[trimmedObjectId] = true;
            }

            this.ValidateUrlParameter("FromObjectId", this.FromObjectId);
            this.ValidateUrlParameter("ColumnName", columnName);

            this.HomePageUrl = this.RootUrl + string.Format("selectobjectsext.aspx?FromObjectId={0}&ColumnName={1}&CurrentObjectIds={2}",
            this.FromObjectId, columnName, currentObjectIds);
            this.LoadPresentation();
            this.RenderErrorMessage(this.pnlPaneBand);

          /*
          // If only a single row was returned by the search then automatically select this object.
          if (this.HasFunction(FunctionType.Search) && string.IsNullOrEmpty(this.ErrorMessage))
          {
            paneData = this.GetPaneData(true);

            if (paneData.RecordCount == 1)
            {
              if (!currentObjects.ContainsKey((string)paneData.Fields["ObjectHandle"].Value))
              {
                objCount += 1;
                selectedObjects.Add((string)paneData.Fields["ObjectHandle"].Value);
                html = new Literal();
                html.Text = this.SelectedObjectsReturnScript(selectedObjects, this.FromObjectId, columnName);
                this.pnlPaneBand.Controls.Add(html);
                this.HasPresentation = false;
              }
            }
          }
          */

            // If the user pressed "Submit" then check for selected objects and close the popup.
            if (selectedObjects.Count == 0)
            {
                if (this.ComesFrom == "posse" && this.Request.QueryString["Action"] == "Submit" &&
                    string.IsNullOrEmpty(this.ErrorMessage))
                {
                    // Determine which objects were selected.  Ignore objects already related.
                    foreach (OrderedDictionary row in this.GetPaneData(true))
                    {
                        if ((bool)row["websearchselected"])
                        {
                            objCount += 1;
                            if (!currentObjects.ContainsKey(row["objecthandle"].ToString()))
                                selectedObjects.Add(row["code"].ToString());
                        }
                    }

                    // Build the javascript to return to the browser. Create the xml to relate any objects
                    // that were selected.
                    html = new Literal();

                    if (selectedObjects.Count > 0)
                    {
                        html.Text = this.SelectedObjectsReturnScript(selectedObjects, this.FromObjectId, columnName);
                    }
                    else if (objCount == 0)
                    {
                        html.Text += "<script>alert('You did not select any items.'); window.close();</script>";
                    }
                    else
                    {
                        html.Text += "<script>window.close();</script>";
                    }

                    this.pnlPaneBand.Controls.Add(html);
                    this.HasPresentation = false;
                }
                else if (this.HasPresentation)
                {
                    this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
                    this.CheckClient();
                    this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
                    this.RenderSelectObjectsFunctionBand(this.pnlBottomFunctionBand);
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Build javascript to relate the selected objects to the source object.
        /// </summary>
        /// <param name="selectedObjects">An array of object id's that were selected.</param>
        /// <param name="fromObjectId">The source object to relate the selected objects to</param>
        /// <param name="columnName">The name of the column to set.</param>
        protected virtual string SelectedObjectsReturnScript(List<string> selectedObjects,
            string fromObjectId, string columnName)
        {
            StringBuilder xml = new StringBuilder();

            if (selectedObjects.Count > 0)
            {
                xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", fromObjectId);

                foreach (string objectId in selectedObjects)
                {
                    xml.AppendFormat("<column name=\"{0}\">{1}</column>", columnName, objectId);
                }

                xml.Append("</object>");
            }

            return string.Format("<script>opener.PosseAppendChangesXML(unescape('{0}'));opener.PosseNavigate(null); window.close();</script>",
                xml);
        }
        #endregion
    }
}
