using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class PossePLUpload : Computronix.POSSE.Outrider.PageBaseExt
  {

        public PossePLUpload()
        {
            this.PreRender -= new EventHandler(this.PageBase_PreRender);
        }


    #region Event Handlers
    /// <summary>
    /// Page load event handler.
    /// </summary>
    /// <param name="sender">A reference to the page.</param>
    /// <param name="e">Event arguments.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

        // Check to see whether there are uploaded files to process them
        if (Request.Files.Count > 0)
        {
            int pendingDocId = 0;
            int chunk = Request["chunk"] != null ? int.Parse(Request["chunk"]) : 0;
            string fileName = Request["name"] != null ? Request["name"] : string.Empty;
            string extension = fileName.Substring(fileName.LastIndexOf(".") + 1);

            HttpPostedFile fileUpload = Request.Files[0];

            //string uploadPath = Server.MapPath("~/UploadedFiles");
            //using (FileStream fs = new FileStream(Path.Combine(uploadPath, fileName), chunk == 0 ? FileMode.Create : FileMode.Append))
            //{
            //    byte[] buffer = new byte[fileUpload.InputStream.Length];
            //    fileUpload.InputStream.Read(buffer, 0, buffer.Length);

            //    fs.Write(buffer, 0, buffer.Length);
            //}
            byte[] data = new byte[fileUpload.InputStream.Length];
            fileUpload.InputStream.Read(data, 0, data.Length);
            pendingDocId = this.SetDocument(int.Parse(Request.QueryString["PosseObjectDefId"]), extension, data);
            if (pendingDocId > 0)
            {
                //Random random = System.Random();

                string newObjectId = "NEW" + DateTime.Now.Millisecond.ToString();// +random.Next().ToString();
                string json = "{" + String.Format("\"pendingdocid\" : {0}, \"filename\" : \"{1}\", \"extension\" : \"{2}\", \"newobjectid\": \"{3}\", \"filesize\": \"{4}\"",
                    pendingDocId.ToString(), fileName, extension, newObjectId, data.Length.ToString()) + "}";
                Response.Write(json);
                Response.End();
            }
        }
        
    }

    #endregion
  }
}
