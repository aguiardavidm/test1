using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Computronix.POSSE.Outrider
{
    public partial class _Default : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Ensure the outer ASPX form is never submitted.  Works around browser behaviour that submits single field forms when [Enter] is pressed.
            this.Form.Attributes["onsubmit"] = "return false";

            this.LoadPresentation();
            this.RenderUserInfo(this.pnlUserInfo);
            this.RenderMenuBand(this.pnlMenuBand);
            this.RenderErrorMessage(this.pnlPaneBand);
            this.RenderTitle(this.pnlTitleBand, "Access Denied");

            if (this.ShowDebugSwitch)
            {
                this.RenderDebugLink(this.pnlDebugLinkBand);
            }
            this.RenderFooterBand(this.pnlFooterBand);
        }
        #endregion

    }
}
