using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class EditRelatedObject : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string pageUrl = "EditRelatedObject.aspx";
            bool isNew = false;
            StringBuilder script = new StringBuilder();

            this.ValidateUrlParameter("FromObjectId", this.FromObjectId);
            this.ValidateUrlParameter("EndPoint", this.EndPoint);

            if (!string.IsNullOrEmpty(this.FromObjectId) && !string.IsNullOrEmpty(this.EndPoint))
            {
                pageUrl += "?FromObjectId=" + this.FromObjectId + "&EndPoint=" + this.EndPoint;
                isNew = true;
            }

            this.HomePageUrl = this.RootUrl + pageUrl;
            this.LoadPresentation(true);
            this.RenderErrorMessage(this.pnlPaneBand);

            if (this.ComesFrom == "posse" && this.RoundTripFunction == RoundTripFunction.Submit &&
                string.IsNullOrEmpty(this.ErrorMessage))
            {
                this.HasPresentation = false;

                if (isNew)
                {
                    script.AppendFormat("opener.PosseAppendChangesXML(unescape('<object id=\"{0}\" action=\"Update\"><relationship id=\"NEW1\" action=\"Insert\" endpoint=\"{1}\" toobjectid=\"{2}\"/></object>'));",
                        this.FromObjectId, this.EndPoint, this.GetPaneData(true)[0]["objecthandle"]);
                }

                if (this.SubmitOnClose)
                {
                    script.Append("opener.PosseSubmit(null); window.close();");
                }
                else
                {
                    script.Append("opener.PosseNavigate(null); window.close();");
                }
                script.Append("vProcessFunctionLinks = false;");
                this.ClientScript.RegisterStartupScript(this.GetType(), "Submit", script.ToString(), true);
            }
            else if (this.HasPresentation)
            {
                this.RenderHeaderPane(this.pnlHeaderBand, this.pnlTopBand);
                this.RenderTabLabelBand(this.pnlTabLabelBand);
                this.RenderTopFunctionBand(this.pnlTopFunctionBand);
                this.CheckClient();
                this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
                this.RenderPopupFunctionBand(this.pnlBottomFunctionBand);
            }
        }
        #endregion
    }
}
