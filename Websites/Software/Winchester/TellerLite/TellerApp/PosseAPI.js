Function.prototype.$extIsFunction = true;

// Posse API.
PosseApi =
{
    _addOn: null,
    _addOnHost: false,
    _addOnName: null,
    _configurationName: null,
    _loaded: false,
    _configured: false,
    _busy: false,
    _showBusy: true,
    _window: null
};

// Methods.

// Call this method when the  page has finished loading.
PosseApi.addOnLoaded = function (addOn)
{
    this._loaded = true;
    this._addOn = addOn || PosseAddOn;
    this._window = window;

    if (this._getAddOnHost())
    {
        this._getAddOnName();
        this._getAddOnHost().addOnLoaded(this);
    }
    else
    {
        this._getPosseInternalApi().addOnLoaded(this._getAddOnName(), this);
    }

    if (this._getAddOn().resize)
    {
        if (this._getAddOn().resizeFrequencyPeriod)
        {
            this._getAddOn().resize = this._getPosseInternal().Ext.Function.createThrottled(this._getAddOn().resize, this._getAddOn().resizeFrequencyPeriod);
        }

        this._getPosseInternal().Ext.fly(this._window).addListener('resize', this._addOnResize, this);
        this._addOnResize();
    }
}

PosseApi._addOnResize = function ()
{
    if (this._getAddOn())
    {
        this._getAddOn().resize(this._window.innerWidth, this._window.innerHeight);
    }
}

// Call this method when the  add on configuration has changed.
PosseApi.configurationChanged = function (name)
{
    this._configurationName = name;

    if (this._getAddOnHost())
    {
        this._getAddOnHost().configurationChanged(name);
    }
    else
    {
        this._getPosseInternalApi().configurationChanged(this._getAddOnName(), name);
    }
}

// Call this method when the  add on configuration is loaded.
PosseApi.configurationLoaded = function ()
{
    this._configured = true;

    if (this._getAddOnHost())
    {
        this._getAddOnHost().configurationLoaded();
    }
    else
    {
        this._getPosseInternalApi().configurationLoaded(this._getAddOnName());
    }

    this.setBusy(false);
}

// Creates a new object of the type specified in Posse.
PosseApi.createObject = function (componentTypeName, queryString)
{
    this._getPosseInternalApi().createObject(componentTypeName, queryString);
}

PosseApi._defer = function (method, arguments)
{
    this._getPosseInternal().Ext.Function.defer(method,
    this._getPosseInternalApi().callDeferPeriod, this, arguments);
}

// Config example.
// url: 'SysHandler.ashx',
// params:
// {
//    action: 'ping'
// },
// success: function() { alert('Done'); }
PosseApi.executeAjax = function (config)
{
    this._getPosseInternalApi().executeAjax(config);
}

// Returns a reference to the add on.
PosseApi._getAddOn = function ()
{
    var result = null;

    if (this._addOn)
    {
        result = this._addOn;
    }
    else
    {
        this._showError(new Error('PosseAddOn does not exist and the AddOnLoaded method was not passed an object reference'));
    }

    return result;
}

// Returns a reference to the add on host widget in Posse Internal.
PosseApi._getAddOnHost = function ()
{
    var args  = null;

    if (this._addOnHost == false)
    {
        args  = this._getQueryArgs();
        this._addOnHost =
            this._getPosseInternal().Ext.getCmp(args.PosseAddOnHostId);
    }

    return this._addOnHost;
}

PosseApi._getAddOnName = function ()
{
    var args  = null;

    if (!this._addOnName)
    {
        args  = this._getQueryArgs();
        this._addOnName = args.PosseAddOnName
    }

    return this._addOnName;
}

PosseApi._getConfigurationName = function ()
{
    return this._configurationName;
}

PosseApi.getObjectBuffer = function ()
{
    var result = null;

    if (this._getAddOnHost())
    {
        result = this._getAddOnHost().getObjectBuffer();
    }
    else
    {
        result = this._getPosseInternalApi().getObjectBuffer(this._getAddOnName());
    }

    return result;
}

PosseApi._getPosseInternal = function (noError)
{
    var result = null;

    // If the host window was closed, refreshed or a different page was loaded then throw an exception.
    try
    {
        if ((window.opener || window.parent).Computronix &&
            (this._addOnHost || !this._addOnName ||
            window.opener.Computronix.POSSE.PosseInternal.Api.hasAddOn(this._getAddOnName())))
        {
            result =  (window.opener || window.parent);
        }
        else
        {
            throw new Error('Add on is not hosted by Posse');
        }
    }
    catch (exception)
    {
        if (noError != true)
        {
            this._showError(new Error('Add on is not hosted by Posse'));
        }
    }

    return result;
}

PosseApi._getPosseInternalApi = function ()
{
    return this._getPosseInternal().Computronix.POSSE.PosseInternal.Api;
}

PosseApi._getQueryArgs = function ()
{
    return this._getPosseInternal().Ext.Object.fromQueryString(window.location.search);
}

PosseApi.getSessionToken = function ()
{
    return this._getPosseInternal().Computronix.Ext.getCookie('sessionId');
}

PosseApi.getTraceKey = function ()
{
    return this._getPosseInternalApi().getTraceKey();
}

PosseApi._isBusy = function ()
{
    return this._busy;
}

PosseApi._isConfigured = function ()
{
    return this._configured;
}

PosseApi._isLoaded = function ()
{
    return this._loaded;
}

PosseApi._isReady = function ()
{
    return this._isLoaded() && this._isConfigured() && !this._isBusy();
}

// Adds the specified message to the console log.
PosseApi._log =  function (message)
{
    try
    {
        this._getPosseInternal(true).log.apply(this._getPosseInternal(), arguments);
    }
    catch (exception)
    {
        // If the log attempt fails, ignore it.
    }
}

// Loads the specified object in Posse.
PosseApi.openObject = function (objectId, refreshIfOpen)
{
    this._getPosseInternalApi().openObject(objectId, refreshIfOpen);
}

// Loads the specified search in Posse.
PosseApi.openSearch = function (searchName, value, text, queryString)
{
    this._getPosseInternalApi().openSearch(this._getAddOnName(), searchName, value, text, queryString);
}

PosseApi._refresh = function ()
{
    if (this._isReady())
    {
        this._getAddOn().refresh();
    }
    else
    {
        this._defer(this._refresh, arguments);
    }
}

// Refreshes the specified object in Posse.
PosseApi.refreshObjects = function (objectIds)
{
    this._getPosseInternalApi().refreshObjects(objectIds);
}

PosseApi.setBusy = function (busy, silent)
{
    this._busy = busy;

    if (silent != true && this._showBusy)
    {
        this._getPosseInternalApi().setBusy(busy);
    }
}

PosseApi._setConfiguration = function (name)
{
    if (PosseApi._getConfigurationName() != name)
    {
        if (this._isLoaded() && !this._isBusy())
        {
            PosseApi.setBusy(true);
            this._getAddOn().setConfiguration(name);
            this._configurationName = name;
        }
        else
        {
            this._defer(this._setConfiguration, arguments);
        }
    }
}

PosseApi.setDataBuffer = function (data)
{
    if (this._getAddOnHost())
    {
        this._getAddOnHost().setDataBuffer(data);
    }
    else
    {
        this._getPosseInternalApi().setDataBuffer(this._getAddOnName(), data);
    }
}

PosseApi._setReadOnly = function (readOnly)
{
    if (this._isReady())
    {
        this._getAddOn().setReadOnly(readOnly);
    }
    else
    {
        this._defer(this._setReadOnly, arguments);
    }
}

// Loads the specified search in Posse.
PosseApi.setSearchCriteria = function (value, text)
{
    this._getPosseInternalApi().setSearchCriteria(this._getAddOnName(), value, text);
}

PosseApi._showCriteria = function (value)
{
    if (this._isReady())
    {
        this._getAddOn().showCriteria(value);
    }
    else
    {
        this._defer(this._showCriteria, arguments);
    }
}

PosseApi._showData = function ()
{
    if (this._isReady())
    {
        this._getAddOn().showData();
    }
    else
    {
        this._defer(this._showData, arguments);
    }
}

// Show error message.
PosseApi._showError = function (exception, message)
{
    alert(message || exception.message);
    this._log(exception ? exception.message : message);

    if (exception)
    {
        throw exception;
    }
}
