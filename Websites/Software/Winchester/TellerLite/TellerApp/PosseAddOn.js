/* POSSE Addon object that implements the required methods. */
PosseAddOn =
{
    refresh: function () { },
    setConfiguration: function (name) { },
    setReadOnly: function (readOnly) { },
    showCriteria: function (value) { },
    firstShowData: true,
    showData: function () {
        // This will be called when the object on the Winchester side is ready for Teller Lite to use.
        // It will also be called if the user invokes Teller Lite while it is already open.

        if (!PosseAddOn.firstShowData) {
            var objectBuffer = PosseApi.getObjectBuffer();
            if (objectBuffer && objectBuffer.length > 0) {
                objectId = objectBuffer[0].objectHandle;
                Teller.app.setObjectId(objectId);
            }
        }

        PosseAddOn.firstShowData = false;
    }
};
