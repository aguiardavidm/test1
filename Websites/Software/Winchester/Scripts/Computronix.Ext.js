// Global variable declarations.
//=============================================================================
//
// Reference to the ext application object.
Computronix.Ext.Application = null;

// Additional application configuration properties.
Computronix.Ext.ApplicationConfig = {};

// Reference to the center panel of the view port.
Computronix.Ext.centerPanel = null;

// The virtual path for all cookies.
Computronix.Ext.cookiePath = '/';

// Default date display format for fields and grid columns.
Computronix.Ext.dateFormat = 'M j, Y';

// Url for the data handler service.
Computronix.Ext.dataHandlerUrl = 'DataHandler.ashx';

// Error page url.
Computronix.Ext.errorUrl = null;

// Reference to the footer panel of the view port.
Computronix.Ext.footerPanel = null;

// Indicates that the next hash to be processed on a history change event
// should be ignored if it is the stored value.
Computronix.Ext.hashToIgnore = null;

// Reference to the header panel of the view port.
Computronix.Ext.headerPanel = null;

// Home page url.
Computronix.Ext.homeUrl = 'Default.aspx';

// Indicates if outstanding changes should be ignored.
Computronix.Ext.ignoreChanges = false;

// Keep alive timer interval in seconds.
Computronix.Ext.keepAliveInterval = 0;

// Keep alive task.
Computronix.Ext.keepAliveTask = null;

// Indicates the current state of control keys.
Computronix.Ext.keys = { ctrl: false, alt: false, shift: false };

// Time period in milliseconds to defer a script or presentaion load if the loader is currently busy.
Computronix.Ext.loadDeferPeriod = 50;

// Time period in milliseconds to defer firing an event if the target has already been loaded.
Computronix.Ext.loadEventDeferPeriod = 15;

// Reference to the list of current console messages.
Computronix.Ext.logMessages = new Ext.util.HashMap();

// Reference to the logon dialog.
Computronix.Ext.logonDialog = null;

// Url for the presentation loader service.
Computronix.Ext.presentationLoaderUrl = 'PresentationHandler.ashx';

// Indicates if the presentation has been rendered.
Computronix.Ext.presentationRendered = false;

// The key combination required to clear the request cache.
Computronix.Ext.requestCacheHotKey = { key: Ext.event.Event.R, ctrl: true, alt: true, shift: false };

// Reference to the list of current javascript errors.
Computronix.Ext.scriptErrors = new Ext.util.HashMap();

// Indicates if the client scripts have finished executing.
Computronix.Ext.scriptsComplete = false;

// Url for the script loader service.
Computronix.Ext.scriptLoaderUrl = 'ScriptHandler.ashx';

// Indicates if the logoff button should be displayed in the UI.
Computronix.Ext.showLogoffButton = false;

// Indicates if the pane id should be shown when the user hovers over a panel.
Computronix.Ext.showPaneId = false;

// Indicates if the system monitor should be shown when the app loads.
Computronix.Ext.showSystemMonitor = false;

// Url for the presentation loader service.
Computronix.Ext.sysHandlerUrl = 'SysHandler.ashx';

// System configuration settings.
Computronix.Ext.systemConfiguration = null;

// The key combination required to open the system monitor.
Computronix.Ext.systemMonitorHotKey = { key: Ext.event.Event.M, ctrl: true, alt: true, shift: false };

// Url for the system monitor.
Computronix.Ext.systemMonitorUrl = 'scripts/SystemMonitor.js';

// Reference to the tool panel of the view port.
Computronix.Ext.toolPanel = null;

// Url for the file upload service.
Computronix.Ext.uploadUrl = 'UploadHandler.ashx';

// Version of Computronix Ext library.
Computronix.Ext.version = '3.0.0';

// Reference to the view port.
Computronix.Ext.viewport = null;

// Traps javascript errors.
window.onerror = function (message, source, lineNumber, column, error)
{
    var errorObj;

    if (Computronix.Ext.Ajax.enableRequestCache &&
        Computronix.Ext.Ajax.clearRequestCacheOnError)
    {
        Computronix.Ext.Ajax.clearRequestCache();
    }

    errorObj =
    {
        exceptionType: 'javascript',
        message: message,
        callStack: null
    };

    if (error && error.stack)
    {
        errorObj.callStack = error.stack;
    }
    else
    {
        errorObj.callStack = Ext.String.format('{0} at line {1}', source, lineNumber);
    }

    if (Computronix.Ext.errorUrl)
    {
        Computronix.Ext.showErrorPage(errorObj);
    }
    else
    {
        Computronix.Ext.scriptErrors.add(Computronix.Ext.scriptErrors.getCount(), errorObj);
    }
}

// Default stub functions.
//=============================================================================
//
// Executes after client scripts.
// Implemented in each page.
Computronix.Ext.afterClientScripts = function ()
{
}

// Executes after client presentation is rendered.
// Implemented in each page.
Computronix.Ext.afterRenderScripts = function ()
{
}

// Executes before client scripts.
// Implemented in each page.
Computronix.Ext.beforeClientScripts = function ()
{
}

// Executes before presentation is rendered.
// Implemented in each page.
Computronix.Ext.beforeRenderScripts = function ()
{
}

// Renders the presentation for the application.
// Generated by PageBase.
Computronix.Ext.renderPresentation = function ()
{
}

// Renders the view port for the application.
// Implemented in Presentations.js
Computronix.Ext.renderViewport = function ()
{
}

// Event handlers.
//=============================================================================
//
// About request event handler.
Computronix.Ext.onAboutButtonClick = function (button, event)
{
    Computronix.Ext.AboutDialog.showAboutDialog();
}

// Before an ajax request begins.
Computronix.Ext.onBeforeRequest = function (connection, options)
{
    if (Computronix.Ext.Ajax.interceptRequest(connection, options))
    {
        Computronix.Ext.busy.show();
    }
    else
    {
        return false;
    }
}

// Before unload event handler for the body.
Computronix.Ext.onBeforeUnload = function (event)
{
    if (!Computronix.Ext.ignoreChanges && Computronix.Ext.hasChanges())
    {
        // Can not use Ext.MessageBox because it is asyncronous.
        event.returnValue = 'There are outstanding changes.';
    }
}

// Help request event handler.
Computronix.Ext.onHelpButtonClick = function (button, event)
{
    alert('Show Help');
}

Computronix.Ext.onHistoryChange = function (token)
{
    var hash = Computronix.Ext.getHash(false);
    var hashToIgnore = Computronix.Ext.hashToIgnore;
    Computronix.Ext.hashToIgnore = null;

    if (!hash)
    {
        if (hashToIgnore != "")
        {
            Computronix.Ext.parseUrl(null);
        }
    }
    else if (hash != hashToIgnore)
    {
        Computronix.Ext.parseUrl(Ext.Object.fromQueryString(hash));
    }
}

Computronix.Ext.onLogoff = function (response, options)
{
    Computronix.Ext.goHome();
}

Computronix.Ext.onLogon = function (response, options)
{
    Computronix.Ext.LogonDialog.instance.close();

    if (Computronix.Ext.scriptsComplete)
    {
        Computronix.Ext.start();
    }
    else
    {
        Computronix.Ext.processClientScripts();
    }
}

// Callback when an ajax request completes.
Computronix.Ext.onRequestComplete = function (connection, response, options)
{
    var warningMessages = /"?warnings"?:/g;

    // To support IE 9 and older browsers, file uploads must be done using a form submit.
    // If a form submit that uploads a file does not return an ajax object then it failed.
    if (options.isUpload)
    {
        try
        {
            Ext.decode(response.responseText);
        }
        catch (exception)
        {
            Computronix.Ext.showError(null, 'File upload failed.');

            log(response.responseText);

            response.responseText = '{ success: false }';
        }
    }

    // If warning messages were returned, add them to the message summary.
    if (response.responseText && warningMessages.test(response.responseText))
    {
        try
        {
            Computronix.Ext.setWarnings(Ext.decode(response.responseText).warnings);
        }
        catch (exception)
        {
        }
    }

    if (Computronix.Ext.Ajax.registerRequest(connection, response, options))
    {
        Computronix.Ext.busy.hide();
    }
}

// Callback when an ajax request exception occurs.
Computronix.Ext.onRequestException = function (connection, response, options)
{
    var exception;

    Computronix.Ext.Ajax.removeRequest(options, false);

    if (Computronix.Ext.Ajax.enableRequestCache &&
        Computronix.Ext.Ajax.clearRequestCacheOnError &&
        !response.aborted && !response.timedout)
    {
        Computronix.Ext.Ajax.clearRequestCache();
    }

    if (response.aborted)
    {
        // What if anything are we going to do for aborted requests?
    }
    else if (response.timedout)
    {
        if (!options.suppressError)
        {
            Computronix.Ext.showErrorPage(null, Ext.String.format('{0}<br/>Request timed out.', response.statusText));
        }
    }
    else if (response.status < 1 && !response.responseText)
    {
        Ext.Ajax.abortAll();

        if (Computronix.Ext.keepAliveTask)
        {
            Ext.TaskManager.stop(Computronix.Ext.keepAliveTask);
        }
    }
    else
    {
        try
        {
            exception = Ext.decode(response.responseText);

            if (exception.exceptionType == 'SessionExpiredException')
            {
                Ext.Ajax.abortAll();

                if (Computronix.Ext.keepAliveTask)
                {
                    Ext.TaskManager.stop(Computronix.Ext.keepAliveTask);
                }

                Computronix.Ext.showError(exception, null,
                    Computronix.Ext.LogonDialog.instance ? function () { Computronix.Ext.setHash(null, false, false) } : Computronix.Ext.goHome);
            }
            else
            {
                if (!options.suppressError)
                {
                    Computronix.Ext.showErrorPage(exception);
                }
            }
        }
        catch (exception)
        {
            if (response.responseText)
            {
                Ext.getBody().update(exception.message + '<br/>' + response.responseText);
            }
            else
            {
                if (!options.suppressError)
                {
                    Computronix.Ext.showErrorPage(null, 'Unknown error');
                }
            }
        }
    }

    Computronix.Ext.busy.hide();
}

// Callback when data has been saved to the server.
Computronix.Ext.onSave = function (response, options)
{
    Computronix.Ext.refresh();
}

// Global methods.
//=============================================================================
//
// Runs after all client scripts have executed.
Computronix.Ext.clientScriptsComplete = function ()
{
    Computronix.Ext.scriptsComplete = true;

    if (Computronix.Ext.Ajax.batches.getCount() == 0)
    {
        Computronix.Ext.renderPresentation();
        Computronix.Ext.busy.hide();
    }
}

// Runs before any client scripts have executed.
Computronix.Ext.clientScriptsStart = function ()
{
}

// Completes any pending edit actions and updates the associated data records.
Computronix.Ext.completeEdit = function ()
{
    var result = true;

    Computronix.Ext.messageTool.clear();

    Ext.ComponentManager.each(function (id, component)
    {
        if (component.isXType('computronixextform') || component.isXType('computronixextgrid'))
        {
            if (!component.ignoreError)
            {
                result = component.completeEdit();
                return result;
            }
        }
    });

    if (!Computronix.Ext.messageTool.update())
    {
        result = false;
    }

    return result;
}

// Prompts the user if outstanding changes exist.
Computronix.Ext.confirmContinue = function ()
{
    var result = true;

    // Can not use Ext.MessageBox because it is asyncronous.
    if (Computronix.Ext.hasChanges() &&
        !confirm('There are outstanding changes.\n\nDo you want to discard the changes?'))
    {
        result = false;
    }
    else
    {
        Computronix.Ext.messageTool.clear();
    }

    return result;
}

Computronix.Ext.getMatcher = function (value, anyMatch, caseSensitive, exactMatch)
{
    var result = false;
    var regex;

    value = Ext.String.escapeRegex(String(value));

    if (anyMatch === false)
    {
        value = '^' + value;

        if (exactMatch)
        {
            value += '$';
        }
    }

    result = new RegExp(value, caseSensitive ? '' : 'i');

    return result;
}


// Get outstanding changes.
Computronix.Ext.getChanges = function (completeEdit, changes)
{
    var result = changes || [];
    var _changes;
    var ok = true;

    if (completeEdit)
    {
        ok = Computronix.Ext.completeEdit();
    }

    if (ok)
    {
        Ext.data.StoreManager.each(function (store)
        {
            if (store.isComputronixExtStore)
            {
                if (!store.ignoreChanges)
                {
                    _changes = store.getChanges();

                    if (_changes)
                    {
                        result.push(_changes);
                    }
                }
            }
        });
    }

    return result;
}

// Gets the value of the specified cookie.
Computronix.Ext.getCookie = function (name)
{
    return Ext.util.Cookies.get(name);
}

// Returns the lookup display value for the specified key value(s).
Computronix.Ext.getDisplayValue = function (store, value, displayField)
{
    var result = null;
    var model;

    if (Ext.isArray(value))
    {
        if (value.length > 0)
        {
            result = '';

            Ext.each(value, function (_value)
            {
                model = store.getById(_value);

                if (model)
                {
                    result += Ext.String.format('{0}, ', model.get(displayField));
                }
            }, this);

            result = result.substr(0, result.length - 2);
        }
    }
    else
    {
        model = store.getById(value);

        if (model)
        {
            result = model.get(displayField);
        }
    }
    return result;
}

// Returns all error messages from forms and grids.
Computronix.Ext.getErrors = function ()
{
    var result = Computronix.Ext.errors;
    var errors;

    Ext.ComponentManager.each(function (id, component)
    {
        if (component.isXType('computronixextform') || component.isXType('computronixextgrid')
            || component.isXType('computronixposseinternalhybridgridpanel'))
        {
            if (!component.ignoreError)
            {
                errors = component.getErrors();

                if (errors.length > 0)
                {
                    result = Ext.Array.merge(result, errors);
                }
            }
        }
    });

    return result;
}

// Returns the form field with the specified id or name.
Computronix.Ext.getField = function (id)
{
    var result = null;

    result = Ext.getCmp(id);

    if (!result || !result.isFormField)
    {
        result = null;

        Ext.ComponentManager.each(function (key, component)
        {
            if (component.isFormField && component.name == id)
            {
                result = component;

                return false;
            }
        });
    }

    return result;
}

// Returns the first parent container that is a form.
Computronix.Ext.getForm = function (component)
{
    var result = null;
    var parent = component;

    while (parent)
    {
        if (parent.isXType('form'))
        {
            result = parent;
            parent = null;
        }
        else
        {
            parent = parent.ownerCt;
        }
    }

    return result;
}

// Gets the current hash value.
Computronix.Ext.getHash = function (asObject)
{
    var result = null;
    var hash;

    // Firefox has a bug that decodes the values in the hash when accessed using the hash property.
    if (Ext.isGecko)
    {
        hash = window.location.href.substr(window.location.href.lastIndexOf('#'));
    }
    else
    {
        hash = window.location.hash;
    }

    if (hash.length > 1)
    {
        result = hash.substr(1, hash.length - 1);

        if (asObject != false)
        {
            result = Ext.Object.fromQueryString(result);
        }
    }

    return result;
}

// Returns all warning messages from forms and grids.
Computronix.Ext.getWarnings = function ()
{
    var result = Computronix.Ext.warnings;
    var messages;

    Ext.ComponentManager.each(function (id, component)
    {
        if (component.isXType('computronixextform') || component.isXType('computronixextgrid')
            || component.isXType('computronixposseinternalhybridgridpanel'))
        {
            if (!component.ignoreError)
            {
                messages = component.getWarnings();

                if (messages.length > 0)
                {
                    result = Ext.Array.merge(result, messages);
                }
            }
        }
    });

    return result;
}

// Reloads the bowser with the home page.
Computronix.Ext.goHome = function ()
{
    Computronix.Ext.ignoreChanges = true;
    window.location.href = Computronix.Ext.homeUrl;
}

// Indicates if the page has outstanding changes.
Computronix.Ext.hasChanges = function ()
{
    var result = false;

    if (Computronix.Ext.completeEdit())
    {
        Ext.data.StoreManager.each(function (store)
        {
            if (store.isComputronixExtStore)
            {
                if (store.hasChanges())
                {
                    result = true;
                    return false;
                }
            }
        });
    }
    else
    {
        // It the validation failed then there must be changes or bad source data.
        result = true;
    }

    return result;
}

// Hide button text.
Computronix.Ext.hideButtonText = function ()
{
    Ext.ComponentManager.each(function (id, component)
    {
        if (component.isXType('computronixextbutton') || component.isXType('computronixextsplitbutton'))
        {
            if (component.enableHiddenText)
            {
                component.hideText();
            }
        }
    });
}

// Returns true if the values can be considered equal.
Computronix.Ext.isEqual = function (value1, value2)
{
    var result = (value1 == value2 || Ext.isEmpty(value1) && Ext.isEmpty(value2));

    if (!result && value1 instanceof Date && value2 instanceof Date)
    {
        return value1.getTime() == value2.getTime();
    }

    return result;
}

// Returns true if the current session is valid.
Computronix.Ext.isSessionValid = function ()
{
    return !Ext.isEmpty(Computronix.Ext.getCookie('sessionId'));
}

// Dynamically loads the system monitor if required and then shows it.
Computronix.Ext.loadSystemMonitor = function (minimize)
{
    if (Computronix.Ext.SystemMonitor)
    {
        Computronix.Ext.SystemMonitor.showSystemMonitor(minimize);
    }
    else
    {
        Computronix.Ext.ScriptLoader.loadScriptFile(Computronix.Ext.systemMonitorUrl,
            function () { Computronix.Ext.SystemMonitor.showSystemMonitor(minimize) });
    }
}

// Logs the specified errors.
Computronix.Ext.logErrors = function (errors)
{
    showMembers(errors);
}

Computronix.Ext.logoff = function ()
{
    var button;

    if (Computronix.Ext.confirmContinue())
    {
        if (Computronix.Ext.Ajax.enableRequestCache &&
            Computronix.Ext.Ajax.clearRequestCacheOnLogon &&
            Computronix.Ext.Ajax.requestCacheStorage == 'LocalStorage')
        {
            Computronix.Ext.Ajax.clearRequestCache();
        }

        Ext.Ajax.request(
        {
            url: Computronix.Ext.sysHandlerUrl,
            params:
            {
                action: 'logoff',
                ajaxBatchId: 'logoff',
                ajaxBatchComplete: true
            },
            success: Computronix.Ext.onLogoff
        });
    }
    else
    {
        button = Ext.getCmp('cxLogoffButton');

        if (button)
        {
            button.hideAjaxBatchMask();
        }
    }
}

Computronix.Ext.logon = function (userId, password)
{
    if (Computronix.Ext.Ajax.enableRequestCache &&
        Computronix.Ext.Ajax.clearRequestCacheOnLogon &&
        Computronix.Ext.Ajax.requestCacheStorage == 'LocalStorage')
    {
        Computronix.Ext.Ajax.clearRequestCache();
    }

    Ext.Ajax.request(
    {
        url: Computronix.Ext.sysHandlerUrl,
        params:
        {
            action: 'logon',
            userId: userId,
            _password: password,
            ajaxBatchId: 'logon',
            ajaxBatchComplete: true
        },
        success: Computronix.Ext.onLogon
    });
}

Computronix.Ext.parseUrl = function (hash)
{
    if (hash)
    {
        // Restore previous presentation.
    }
    else
    {
        Computronix.Ext.reload();
    }
}

// Refresh data.
Computronix.Ext.refresh = function ()
{
    Computronix.Ext.messageTool.clear();

    Ext.data.StoreManager.each(function (store)
    {
        if (store.isComputronixExtStore)
        {
            if (!store.constant)
            {
                store.reload();
            }
        }
    });
}

Computronix.Ext.reload = function ()
{
    window.location.reload();
}

// Removes the specified cookie.
Computronix.Ext.removeCookie = function (name)
{
    Ext.util.Cookies.clear(name, Computronix.Ext.cookiePath);
}

// Removes the specified hash value.
Computronix.Ext.removeHash = function (name, historyChange)
{
    var hash = Computronix.Ext.getHash();
    var _hash = {};

    for (var member in hash)
    {
        if (member != name)
        {
            _hash[member] = hash[member];
        }
    }

    Computronix.Ext.setHash(_hash, false, historyChange);
}

// Runs after all render scripts have executed.
Computronix.Ext.renderScriptsComplete = function ()
{
    if (Computronix.Ext.showSystemMonitor)
    {
        Computronix.Ext.loadSystemMonitor(true);
    }

    if (Computronix.Ext.isSessionValid())
    {
        Computronix.Ext.start();
    }
}

// Runs before any render scripts have executed.
Computronix.Ext.renderScriptsStart = function ()
{
    Computronix.Ext.presentationRendered = true;
}

// Replaces the current hash value for bookmarking with out creating a browser
// history point. Any change made here will likely need to be made in
// UserProfilePanel.js onUserProfileTabPanelTabChange as well.
Computronix.Ext.replaceHash = function (value, merge)
{
    var hash;
    var newHashStr;

    if (typeof value == 'string')
    {
        newHashStr = value;
    }
    else if (merge == false)
    {
        newHashStr = Ext.Object.toQueryString(value);
    }
    else
    {
        hash = Computronix.Ext.getHash() || {};
        newHashStr = Ext.Object.toQueryString(Ext.apply(hash, value));
    }

    Computronix.Ext.hashToIgnore = newHashStr;

    var host = window.location.href.split('#')[0];
    var newUrl = Ext.String.format('{0}#{1}', host, newHashStr);
    window.location.replace(newUrl);
}

// Saves outstanding changes.
Computronix.Ext.save = function ()
{
    var result = true;
    var changes = [];

    if (Computronix.Ext.completeEdit())
    {
        changes = Computronix.Ext.getChanges(false);

        if (changes.length > 0)
        {
            Ext.Ajax.request(
            {
                url: Computronix.Ext.dataHandlerUrl,
                params:
                {
                    action: 'save',
                    ajaxBatchId: 'save',
                    ajaxBatchComplete: true,
                    changes: Ext.encode(changes)
                },
                success: Computronix.Ext.onSave
            });
        }
        else
        {
            Computronix.Ext.messageTool.update();
        }
    }
    else
    {
        result = false;
    }

    return result;
}

// Sets the value of the specified cookie.
Computronix.Ext.setCookie = function (name, value)
{
    Ext.util.Cookies.set(name, value, null, Computronix.Ext.cookiePath);
}

// Sets the global error messages.
Computronix.Ext.setErrors = function (messages, merge, update)
{
    if (merge == false)
    {
        if (messages)
        {
            if (Ext.isArray(messages))
            {
                Computronix.Ext.errors = messages;
            }
            else
            {
                Computronix.Ext.errors.push(messages);
            }
        }
        else
        {
            Computronix.Ext.errors = [];
        }
    }
    else
    {
        if (messages)
        {
            if (Ext.isArray(messages))
            {
                if (messages.length > 0)
                {
                    Computronix.Ext.errors = Computronix.Ext.errors.concat(messages);
                }
            }
            else
            {
                Computronix.Ext.errors.push(messages);
            }
        }
    }

    if (update != false)
    {
        Computronix.Ext.messageTool.update();
    }
}

// Sets the current hash value for bookmarking.
Computronix.Ext.setHash = function (value, merge, historyChange)
{
    var hash = Computronix.Ext.getHash();
    var newHashStr;

    if (hash)
    {
        if (typeof value == 'string')
        {
            newHashStr = value;
        }
        else if (merge == false)
        {
            newHashStr = Ext.Object.toQueryString(value);
        }
        else
        {
            newHashStr = Ext.Object.toQueryString(value);
        }

        if (historyChange === false)
        {
            Computronix.Ext.hashToIgnore = newHashStr;
        }

        window.location.hash = newHashStr;
    }
    else
    {
        Computronix.Ext.replaceHash(value, merge);
    }
}

Computronix.Ext.setKeys = function (event)
{
    Computronix.Ext.keys.ctrl = event.ctrlKey;
    Computronix.Ext.keys.alt = event.altKey;
    Computronix.Ext.keys.shift = event.shiftKey;
}

Computronix.Ext.setTimeout = function (timeoutMilliseconds)
{
    /*
     * VERSION_WARNING 6.0.2
     *
     * In order to prevent AJAX timeouts in ExtJS 6.0.2, it is necessary to set
     * the Ext.Ajax.timeout using an undocumented setTimeout function. It
     * is unknown if it will continue to exist or work in future versions.
     * Similarly, Ext.data.proxy.Ajax must be overridden, but it is
     * insufficient to set simply using Ext.override or the static
     * Ext.data.proxy.Ajax.override. Instead, we need both. On upgrade, this
     * behaviour may change. Both of these will need testing.
     *
     */
    Ext.Ajax.setTimeout(timeoutMilliseconds);
    Ext.override(Ext.data.proxy.Ajax, { timeout: Ext.Ajax.timeout });
    Ext.data.proxy.Ajax.override({ timeout: Ext.Ajax.timeout });
}

// Sets the global warning messages.
Computronix.Ext.setWarnings = function (messages, merge, update)
{
    if (merge == false)
    {
        if (messages)
        {
            if (Ext.isArray(messages))
            {
                Computronix.Ext.warnings = messages;
            }
            else
            {
                Computronix.Ext.warnings.push(messages);
            }
        }
        else
        {
            Computronix.Ext.warnings = [];
        }
    }
    else
    {
        if (messages)
        {
            if (Ext.isArray(messages))
            {
                if (messages.length > 0)
                {
                    Computronix.Ext.warnings = Computronix.Ext.warnings.concat(messages);
                }
            }
            else
            {
                Computronix.Ext.warnings.push(messages);
            }
        }
    }

    if (update != false)
    {
        Computronix.Ext.messageTool.update();
    }
}

// Show button text.
Computronix.Ext.showButtonText = function ()
{
    Ext.ComponentManager.each(function (id, component)
    {
        if (component.isXType('computronixextbutton') || component.isXType('computronixextsplitbutton'))
        {
            if (component.enableHiddenText)
            {
                component.showText();
            }
        }
    });
}

// Show error message.
Computronix.Ext.showError = function (exception, message, action, scope)
{
    if (!exception)
    {
        exception = {};
    }

    Computronix.Ext.messageBox.show(
    {
        autoScroll: true,
        buttons: Ext.MessageBox.OK,
        fn: action,
        icon: Ext.MessageBox.ERROR,
        maxWidth: 1000,
        msg: message || Ext.htmlEncode(exception.message),
        scope: scope,
        title: 'Error',
    });
}

// Show error page.
Computronix.Ext.showErrorPage = function (exception, message)
{
    if (Computronix.Ext.errorUrl)
    {
        if (!exception)
        {
            exception = {};
        }

        switch (exception.exceptionType)
        {
            default:
                Computronix.Ext.ignoreChanges = true;

                Computronix.Ext.Ajax.submitForm(
                {
                    url: Computronix.Ext.errorUrl,
                    params:
                    {
                        exceptionType: exception.exceptionType,
                        message: message || exception.message,
                        callStack: exception.callStack
                    }
                });
                break;
        }
    }
    else
    {
        Computronix.Ext.showError(exception, message);
    }
}

Computronix.Ext.start = function ()
{
    if (Computronix.Ext.keepAliveInterval > 0)
    {
        // start the keep alive timer.
        Computronix.Ext.keepAliveTask = Ext.TaskManager.start(
        {
            run: function ()
            {
                Ext.Ajax.request(
                {
                    url: Computronix.Ext.sysHandlerUrl,
                    params:
                    {
                        action: 'ping'
                    }
                });
            },
            interval: Computronix.Ext.keepAliveInterval
        });
    }

    Computronix.Ext.parseUrl();
}

// Initializes page processing.
Computronix.Ext.initialize = function ()
{
    var body = Ext.getBody();

    Ext.ariaWarn = Ext.emptyFn;
    Ext.QuickTips.init();
    Ext.Ajax.addListener('beforerequest', Computronix.Ext.onBeforeRequest);
    Ext.Ajax.addListener('requestcomplete', Computronix.Ext.onRequestComplete);
    Ext.Ajax.addListener('requestexception', Computronix.Ext.onRequestException);

    Ext.util.History.init();
    Ext.util.History.addListener('change', Computronix.Ext.onHistoryChange);

    body.insertFirst(
    {
        id: 'ComputronixExtScriptHost',
        tag: 'div',
    });

    body.addListener('keydown', Computronix.Ext.setKeys);
    body.addListener('keyup', Computronix.Ext.setKeys);

    // Default global key handlers.
    if (Computronix.Ext.systemMonitorHotKey)
    {
        new Ext.util.KeyMap(body,
            Ext.apply(Computronix.Ext.systemMonitorHotKey,
            {
                fn: function ()
                {
                    Computronix.Ext.loadSystemMonitor();
                }
            })
        );
    }

    if (Computronix.Ext.requestCacheHotKey)
    {
        new Ext.util.KeyMap(body,
            Ext.apply(Computronix.Ext.requestCacheHotKey,
            {
                fn: function ()
                {
                    Computronix.Ext.Ajax.clearRequestCache();
                    Computronix.Ext.reload();
                }
            })
        );
    }
}

// Mixin objects.
//=============================================================================
//
// Ajax batch mask mixin.
// ############################################################################################
//
Ext.define('Computronix.Ext.AjaxBatchMask',
{
    // Data Members
    //=============================================================================
    //
    ajaxBatchId: null,
    disableMode: 'disable',
    showMask: true,
    _mask: null,
    maskComponent: null,
    maskMessageFormat: '{0} processing...',
    splitButton: null,
    shortCutScope: null,
    shortCutKey: null,
    restoreFocus: true,
    hadFocus: false,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function (config)
    {
        this.callParent(arguments);

        // If the json was encoded such that the handler is a string then enable it.
        if (typeof this.handler == 'string')
        {
            eval(Ext.String.format('this.handler = {0}', this.handler));
        }

        this.addListener('click', this.onAjaxBatchMaskClick, this);
        this.addListener('afterrender', this.onAjaxBatchMaskAfterRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onAjaxBatchMaskAfterRender: function (component)
    {
        Ext.Function.defer(function ()
        {
            if (this.shortCutKey)
            {
                // If the shortCutScope was set during initialization then we will have a reference to the component not a
                // reference to it's element so get it now that the component has been rendered.
                if (this.shortCutScope && !(this.shortCutScope instanceof Ext.dom.Element) && this.shortCutScope.getEl())
                {
                    this.shortCutScope = this.shortCutScope.getEl();
                }

                if (this.shortCutScope && this.shortCutScope instanceof Ext.dom.Element)
                {
                    new Ext.util.KeyMap(this.shortCutScope,
                    Ext.apply(
                    {
                        fn: function (keyCode, event)
                        {
                            if (!Computronix.Ext.busy.isBusy())
                            {
                                this.doAction(event);
                            }
                        }, scope: this
                    },
                    this.shortCutKey));
                }
            }
        }, Computronix.Ext.loadDeferPeriod, this);
    },

    onAjaxBatchMaskBatchComplete: function (ajax, ajaxBatchId, success)
    {
        if (this.ajaxBatchId == ajaxBatchId)
        {
            Computronix.Ext.Ajax.removeListener('batchcomplete', this.onAjaxBatchMaskBatchComplete, this);

            this.hideAjaxBatchMask(true);
        }
    },

    onAjaxBatchMaskClick: function ()
    {
        if (this.showMask && this.ajaxBatchId)
        {
            // Only register with the batch manager when instigating the batch so that if multiple action items are
            // configured to handle the same batch only this one will process the complete event handler.
            Computronix.Ext.Ajax.addListener('batchcomplete', this.onAjaxBatchMaskBatchComplete, this);

            this.showAjaxBatchMask();
        }

        this.showMask = true;
    },

    // Methods
    //=============================================================================
    //
    doAction: function (event)
    {
        if (!this.disabled)
        {
            this.fireEvent('click', this, event);

            if (this.handler)
            {
                this.handler.call(this.scope || this, this, event);
            }
        }
    },

    hideAjaxBatchMask: function (showMask)
    {
        var focusCls;
        var focusEl;

        switch (this.disableMode)
        {
            case 'disable':
                if (!this.isDestroyed)
                {
                    this.enable();

                    if (this.hadFocus)
                    {
                        this.hadFocus = false;

                        if (this.restoreFocus)
                        {
                            if (Computronix.Ext.messageBox.isVisible())
                            {
                                // This is required in Chrome because before the messagebox is shown the current element has
                                // already been set to the body tag.
                                Computronix.Ext.messageBox.lastFocusedElement = this.getFocusEl();
                            }
                            else
                            {
                                this.focus();

                                if (this.hasFocus)
                                {
                                    focusCls = this.focusCls;
                                    focusEl = this.getFocusEl();

                                    if (focusCls && focusEl)
                                    {
                                        focusEl.addCls(this.addClsWithUI(focusCls, true));
                                    }
                                }
                            }
                        }
                    }

                    // if the item is a menu item in a splitbutton then enable the split button as well.
                    if (this.splitButton)
                    {
                        this.splitButton.enable();
                    }
                }
                break;
            case 'mask':
                if (this._mask)
                {
                    this._mask.hide();
                }

                // if the item is a menu item in a splitbutton then hide the mask for the split button as well.
                if (this.splitButton)
                {
                    if (this.splitButton._mask)
                    {
                        this.splitButton._mask.hide();
                    }
                }

                break;
        }

        this.showMask = (showMask == true);
    },

    showAjaxBatchMask: function ()
    {
        var maskText;

        switch (this.disableMode)
        {
            case 'disable':
                this.hadFocus = this.hasFocus;
                this.disable();
                break;
            case 'mask':
                if (!this._mask)
                {
                    if (!this.maskComponent)
                    {
                        this.maskComponent = Computronix.Ext.viewport;
                    }

                    maskText = Ext.String.format(this.maskMessageFormat,
                            (this.initialText || this.text));

                    this._mask = new Ext.LoadMask(
                    {
                        msg: maskText,
                        target: this.maskComponent,
                    });
                }

                this._mask.show();
                break;
        }
    }
});

// Mandatory indicator mixin.
// ############################################################################################
//
Ext.define('Computronix.Ext.MandatoryIndicator',
{
    // Data Members
    //=============================================================================
    //
    enableMandatoryIndicator: true,
    enforceMandatory: true,
    showMandatory: true,
    mandatory: false,
    mandatoryIndicatorEl: null,
    mandatoryText: 'This field is required',
    mandatoryToolTip: null,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function (config)
    {
        this.callParent(arguments);

        if (this.enableMandatoryIndicator)
        {
            if (this.mandatoryText == 'This field is required' && this.blankText)
            {
                this.mandatoryText = this.blankText;
            }

            this.addListener('afterrender', this.onMandatoryIndicatorAfterRender, this);
        }
    },

    // Event Handlers
    //=============================================================================
    //
    onMandatoryIndicatorAfterRender: function ()
    {
        this.createMandatoryIndicatorEl();
    },

    // Methods
    //=============================================================================
    //
    createMandatoryIndicatorEl: function ()
    {
        // Insert a container for the mandatory indicator. This will reserve 6px
        // for the indicator whether it is enabled or not. Note that some
        // widgets do not include this mixin, but must still align with widgets
        // that do.
        var width;

        if (this.labelEl)
        {
            width = this.labelEl.getWidth();

            if (width < 6)
            {
                this.labelEl.setStyle({ 'min-width': '6px' });
            }

            this.mandatoryIndicatorEl = Ext.DomHelper.insertFirst(this.labelEl, { tag: 'div', cls: 'cxmandatory', html: '&nbsp;' }, true);
        }

        this.setMandatoryIndicator(this.mandatory);
    },

    setMandatoryIndicator: function (show)
    {
        this.mandatory = show;

        if (show)
        {
            if (this.mandatoryIndicatorEl && this.showMandatory)
            {
                this.mandatoryIndicatorEl.update('*');

                if (!this.mandatoryToolTip)
                {
                    this.mandatoryToolTip = new Ext.tip.ToolTip({ target: this.mandatoryIndicatorEl, html: this.mandatoryText });
                }
            }

            this.allowBlank = !this.enforceMandatory;
        }
        else
        {
            if (this.mandatoryIndicatorEl)
            {
                this.mandatoryIndicatorEl.update('&nbsp;');

                if (this.mandatoryToolTip)
                {
                    this.mandatoryToolTip.destroy();
                    this.mandatoryToolTip = null;
                }
            }

            this.allowBlank = true;
        }
    }
});

// Message mixin.
// ############################################################################################
//
Ext.define('Computronix.Ext.Message',
{
    statics:
    {
        tip: null,

        initTip: function ()
        {
            var copy;

            if (this.tip)
            {
                return;
            }

            this.tip = Ext.create('Ext.tip.QuickTip',
            {
                id: 'computronix-ext-message-warning-tip',
                ui: 'form-invalid'
            });

            /*
             * VERSION_WARNING 6.0.2
             *
             * Override the tagConfig of Ext.tip.QuickTip in order to specify a
             * custom attribute that is associated with display of this quick-
             * tip. This is to ensure that the use of other quick-tips does not
             * cause this warning tip to appear when it should not.
             *
             * tagConfig is currently explicitly documented as a private
             * attribute and the setTagConfig method is not part of the
             * documented API. Therefore there is a risk that this will no
             * longer work as expected when we upgrade ExtJS.
             */
            copy = Ext.apply({}, this.tip.tagConfig);
            copy.attribute = 'warningqtip';
            this.tip.setTagConfig(copy);
        }
    },

    // Data Members
    //=============================================================================
    //
    errorMessages: null,
    onMessagesChangedCallback: null,
    warningMessages: null,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function (config)
    {
        this.callParent(arguments);

        Computronix.Ext.Message.initTip();
    },

    // Methods
    //=============================================================================
    //
    addWarningMessages: function (messages)
    {
        var result = messages;
        var warningMessages = '';

        if (!this.readOnly && this.hasWarningMessages())
        {
            Ext.each(this.getWarningMessages(), function (message)
            {
                warningMessages += Ext.String.format('<li class="cxwarningtooltip">{0}</li>', message);
            }, this);

            if (messages.length > 0)
            {
                result = Ext.String.format('{0}{1}', messages.substr(0, messages.length - 5),
                    Ext.String.format('{0}</ul>', warningMessages));
            }
            else
            {
                result = Ext.String.format('<ul class="x-list-plain">{0}</ul>', warningMessages);
            }
        }

        return result;
    },

    clearMessages: function ()
    {
        this.errorMessages = null;
        this.warningMessages = null;

        if (this.onMessagesChangedCallback)
        {
            this.onMessagesChangedCallback();
        }
    },

    getErrorMessages: function ()
    {
        return Ext.Array.from(this.errorMessages);
    },

    getWarningMessages: function ()
    {
        return Ext.Array.from(this.warningMessages);
    },

    hasErrorMessages: function ()
    {
        return !Ext.isEmpty(this.errorMessages);
    },

    hasWarningMessages: function ()
    {
        return !Ext.isEmpty(this.warningMessages);
    },

    hasWarningOnly: function ()
    {
        return this.hasActiveError()
                && this.getErrors(this.getValue()).length == 0;
    },

    setErrorMessages: function (messages)
    {
        this.errorMessages = messages;

        if (this.onMessagesChangedCallback)
        {
            this.onMessagesChangedCallback();
        }
    },

    setWarningMessages: function (message)
    {
        this.warningMessages = message;

        if (this.onMessagesChangedCallback)
        {
            this.onMessagesChangedCallback();
        }
    },
});

// Microhelp mixin.
// ############################################################################################
//
Ext.define('Computronix.Ext.Microhelp',
{
    // Data Members
    //=============================================================================
    //
    helpText: null,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function (config)
    {
        this.callParent(arguments);

        if (Computronix.Ext.microhelpTool)
        {
            this.addListener('focus', this.onMicrohelpFocus, this);
            this.addListener('blur', this.onMicrohelpBlur, this);
        }
    },

    // Event Handlers
    //=============================================================================
    //
    onMicrohelpFocus: function (field)
    {
        if (this.helpText)
        {
            Computronix.Ext.microhelpTool.updateText(this.helpText);
        }
        else
        {
            Computronix.Ext.microhelpTool.clear();
        }
    },

    onMicrohelpBlur: function (field)
    {
        if (this.helpText && (Computronix.Ext.microhelpTool.getText() == this.helpText))
        {
            Computronix.Ext.microhelpTool.clear();
        }
    },

    // Methods
    //=============================================================================
    //
    setMicroHelp: function (microhelp)
    {
        this.helpText = microhelp;
    }
});

// Presentation loader mixin.
// ############################################################################################
//
Ext.define('Computronix.Ext.PresentationLoader',
{
    // Data Members
    //=============================================================================
    //
    isBindable: true,
    isUpdatable: true,
    busy: false,
    loaderUrl: null,
    loadPresentationOnRender: true,
    currentPaneId: null,
    paneId: null,
    presentationAjaxBatchId: null,
    presentationAjaxBatchComplete: false,
    scriptId: null,
    scriptAjaxBatchId: null,
    scriptAjaxBatchComplete: false,
    storesAjaxBatchId: null,
    lookupStoresAjaxBatchId: null,
    stores: null,
    lookupStores: null,
    storeLoadParams: null,
    isRefresh: false,
    loadMask: false,
    _mask: null,
    maskComponent: null,
    maskMessage: 'Loading...',
    cacheRequest: true,
    abortPendingRequests: true,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function (config)
    {
        var _stores;
        var _lookupStores;

        this.loaderUrl = Computronix.Ext.presentationLoaderUrl;

        Ext.applyIf(this, { loader: {} });
        Ext.applyIf(this.loader, { url: this.loaderUrl, renderer: 'component' });

        if (this.stores)
        {
            _stores = this.stores;
            this.stores = [];

            Ext.each(_stores, function (store)
            {

                this.stores.push(Ext.getStore(store));
            }, this);
        }
        else
        {
            this.stores = [];
        }

        if (this.lookupStores)
        {
            _lookupStores = this.lookupStores;
            this.lookupStores = [];

            Ext.each(_lookupStores, function (store)
            {
                this.lookupStores.push(Ext.getStore(store));
            }, this);
        }
        else
        {
            this.lookupStores = [];
        }

        this.callParent(arguments);

        // In Ext JS 4.2.3, if we are using ajax caching the method needs to handle the case
        // where active no longer exists.
        this.getLoader().setOptions =
            function (active, options)
            {
                if (active)
                {
                    active.removeAll = Ext.isDefined(options.removeAll) ? options.removeAll : this.removeAll;
                    active.rendererScope = options.rendererScope || this.rendererScope || this.target;
                }
            },

        this.addListener('afterrender', this.onPresentationLoaderAfterRender, this);
        this.addManagedListener(Ext.Ajax, 'requestcomplete', this.onPresentationLoaderRequestComplete, this);
        this.addManagedListener(Ext.Ajax, 'requestexception', this.onPresentationLoaderRequestException, this);
        this.addManagedListener(Computronix.Ext.Ajax, 'batchcomplete', this.onPresentationLoaderBatchComplete, this);
        this.addManagedListener(this.getLoader(), 'load', this.onPresentationLoaderLoad, this);
        this.addManagedListener(Computronix.Ext.ScriptLoader, 'scriptload', this.onPresentationLoaderScriptLoaderScriptLoad, this);
        this.addListener('beforedestroy', this.onPresentationLoaderBeforeDestroy, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onPresentationLoaderAfterRender: function (container)
    {
        if (this.loadPresentationOnRender && this.paneId)
        {
            this.loadPresentation();
        }
    },

    onPresentationLoaderBatchComplete: function (ajax, ajaxBatchId, success)
    {
        // Load the main stores after the lookup stores have been loaded.
        if (this.lookupStoresAjaxBatchId == ajaxBatchId && success)
        {
            if (this.storeLoadParams)
            {
                if (!this.storesAjaxBatchId)
                {
                    this.storesAjaxBatchId = Ext.String.format('stores{0}', this.id);
                }

                // Load all associated data stores.
                Ext.each(this.stores, function (store)
                {
                    store.load(Ext.apply(Ext.clone(this.storeLoadParams),
                    {
                        ajaxBatchId: this.storesAjaxBatchId
                    }));
                }, this);

                this.storeLoadParams = null;
                this.isRefresh = false;

                Computronix.Ext.Ajax.setBatchComplete(this.storesAjaxBatchId, true);
            }
            else if (this.isRefresh)
            {
                // Reload all associated data stores.
                Ext.each(this.stores, function (store)
                {
                    if (store.isComputronixExtStore)
                    {
                        if (!store.constant)
                        {
                            store.reload();
                        }
                    }
                }, this);

                this.isRefresh = false;

                Computronix.Ext.Ajax.setBatchComplete(this.storesAjaxBatchId, true);
            }

            // Hide the mask after the lookup stores have been loaded if there are no stores.
            if (Ext.isEmpty(this.stores))
            {
                this.hideMask();
            }
        }

        // Hide the mask after the stores have been loaded.
        if (this.storesAjaxBatchId == ajaxBatchId && success)
        {
            this.hideMask();
        }

        if (this.scriptAjaxBatchId == ajaxBatchId && success)
        {
            if (this.paneId)
            {
                this.loadPane();
            }
        }
    },

    onPresentationLoaderBeforeDestroy: function (container)
    {
        if (this.abortPendingRequests)
        {
            this.abortRequests();
        }
    },

    onPresentationLoaderLoad: function (loader, response, options)
    {
        this.currentPaneId = this.paneId;
        this.storesAjaxBatchId = Ext.String.format('stores{0}', this.currentPaneId);
        this.lookupStoresAjaxBatchId = Ext.String.format('lookupStores{0}', this.currentPaneId);

        this.fireEvent('presentationload', this, loader, response, options);
        Computronix.Ext.Ajax.removeRequest(options, true);
        this.busy = false;

        if (Computronix.Ext.showPaneId && Computronix.Ext.SystemMonitor && Computronix.Ext.SystemMonitor.instance)
        {
            new Ext.tip.ToolTip(
            {
                target: this.el,
                html: Ext.String.format('Pane Id: [{0}]',this.paneId),
                listeners:
                {
                    beforeshow:
                    {
                        fn: function (tip) { return Computronix.Ext.keys.ctrl; }
                    }
                }
            });
        }
    },

    onPresentationLoaderRequestComplete: function (connection, response, options)
    {
        if (!options.scripts && options.params && options.params.paneId && options.params.paneId == this.paneId)
        {
            this.fireEvent('presentationrequestcomplete', this, connection, response, Ext.clone(options));
        }
    },

    onPresentationLoaderRequestException: function (connection, response, options)
    {
        if (!options.scripts && options.params && options.params.paneId && options.params.paneId == this.paneId)
        {
            this.busy = false;
        }
    },

    onPresentationLoaderScriptLoaderScriptLoad: function (scriptLoader, loader, response, options)
    {
        var stores;
        var lookupStores;

        if (options.params.scriptId == this.scriptId)
        {
            stores = eval(Ext.String.format('Computronix.Ext._{0}$Stores', this.paneId));
            lookupStores = eval(Ext.String.format('Computronix.Ext._{0}$LookupStores', this.paneId));

            if (stores)
            {
                this.stores = [];

                Ext.each(stores, function (store)
                {
                    this.stores.push(Ext.getStore(store));
                }, this);
            }

            if (lookupStores)
            {
                this.lookupStores = [];

                Ext.each(lookupStores, function (store)
                {
                    this.lookupStores.push(Ext.getStore(store));
                }, this);
            }

            if (!this.scriptAjaxBatchId)
            {
                if (this.paneId)
                {
                    this.loadPane(Ext.apply(Ext.clone(options.params),
                    {
                        paneId: this.paneId,
                        ajaxBatchId: this.presentationAjaxBatchId,
                        ajaxBatchComplete: this.presentationAjaxBatchComplete
                    }));
                }
            }
        }
    },

    // Methods
    //=============================================================================
    //
    abortRequests: function ()
    {
        Computronix.Ext.Ajax.abortRequests(
        {
            paneId: this.currentPaneId,
        });

        Computronix.Ext.Ajax.abortRequests(
        {
            ajaxBatchId: this.lookupStoresAjaxBatchId,
        });

        Computronix.Ext.Ajax.abortRequests(
        {
            ajaxBatchId: this.storesAjaxBatchId,
        });
    },

    clearLookupStores: function ()
    {
        // Clear all associated lookup data stores.
        Ext.each(this.lookupStores, function (store)
        {
            if (store.isComputronixExtStore)
            {
                store.clear();
            }
        }, this);
    },

    clearPane: function ()
    {
        if (this.abortPendingRequests)
        {
            this.abortRequests();
        }

        this.removeAll();
        this.currentPaneId = null;
    },

    clearStores: function ()
    {
        this.storeLoadParams = null;
        this.isRefresh = false;

        // Clear all associated data stores.
        Ext.each(this.stores, function (store)
        {
            if (store.isComputronixExtStore)
            {
                store.clear();
            }
        }, this);

        this.clearLookupStores();

        this.items.each(function (item)
        {
            if (item.isBindable || item.isUpdatable)
            {
                item.clearStores();
            }
        }, this);
    },

    getChanges: function (completeEdit, changes, hasChanges)
    {
        var result = changes || [];
        var _changes;
        var ok = true;

        if (completeEdit)
        {
            ok = Computronix.Ext.completeEdit();
        }

        if (ok)
        {
            Ext.each(this.stores, function (store)
            {
                if (store.isComputronixExtStore)
                {
                    if (!store.ignoreChanges)
                    {
                        _changes = store.getChanges();

                        if (_changes)
                        {
                            result.push(_changes)
                        }
                    }
                }
            }, this);

            this.items.each(function (item)
            {
                if (item.isUpdatable)
                {
                    _changes = item.getChanges(false, null, hasChanges || result.length > 0);

                    if (_changes.length > 0)
                    {
                        result = result.concat(_changes);
                    }
                }
            }, this);
        }

        return result;
    },

    // Indicates if the presentation has outstanding changes.
    hasChanges: function (completeEdit)
    {
        var result = false;
        var ok = true;

        if (completeEdit != false)
        {
            ok = Computronix.Ext.completeEdit();
        }

        if (ok)
        {
            Ext.each(this.stores, function (store)
            {
                if (store.isComputronixExtStore)
                {
                    if (store.hasChanges())
                    {
                        result = true;
                        return false;
                    }
                }
            });

            if (!result)
            {
                this.items.each(function (item)
                {
                    if (item.isUpdatable)
                    {
                        if (item.hasChanges(false))
                        {
                            result = true;
                            return false;
                        }
                    }
                }, this);
            }
        }
        else
        {
            // It the validation failed then there must be changes or bad source data.
            result = true;
        }

        return result;
    },

    hasPresentation: function ()
    {
        return (this.currentPaneId != null);
    },

    hideMask: function ()
    {
        if (this._mask)
        {
            this._mask.hide();
        }
    },

    loadLookupStores: function (params)
    {
        if (!Ext.isEmpty(this.lookupStores))
        {
            if (!this.lookupStoresAjaxBatchId)
            {
                this.lookupStoresAjaxBatchId = Ext.String.format('lookupStores{0}', this.id);
            }

            if (this.loadMask)
            {
                this.showMask();
            }

            params = params || {};

            // Load all associated lookup data stores.
            Ext.each(this.lookupStores, function (store)
            {
                store.load(Ext.apply(Ext.clone(params),
                {
                    ajaxBatchId: this.lookupStoresAjaxBatchId
                }));
            }, this);

            Computronix.Ext.Ajax.setBatchComplete(this.lookupStoresAjaxBatchId, true);
        }
    },

    loadPane: function (params)
    {
        if (this.destroyed)
        {
            return;
        }

        if (this.busy)
        {
            Ext.Function.defer(this.loadPane, Computronix.Ext.loadDeferPeriod, this, arguments);
        }
        else
        {
            params = params || {};

            if (typeof params == 'number' || typeof params == 'string')
            {
                params = { paneId: params };
            }

            if (typeof params.paneId != 'undefined')
            {
                this.paneId = params.paneId;
            }

            if (this.paneId != this.currentPaneId)
            {
                if (typeof params.ajaxBatchId != 'undefined')
                {
                    this.presentationAjaxBatchId = params.ajaxBatchId;
                }

                if (typeof params.ajaxBatchComplete != 'undefined')
                {
                    this.presentationAjaxBatchComplete = params.ajaxBatchComplete;
                }

                Ext.applyIf(params,
                {
                    cacheRequest: this.cacheRequest
                });

                this.removeAll();
                this.busy = true;

                this.getLoader().load(
                {
                    params: Ext.apply(params,
                    {
                        paneId: this.paneId,
                        ajaxBatchId: this.presentationAjaxBatchId,
                        ajaxBatchComplete: this.presentationAjaxBatchComplete
                    })
                });
            }
        }
    },

    loadPresentation: function (params)
    {
        params = params || {};

        if (typeof params == 'number' || typeof params == 'string')
        {
            params = { paneId: params };
        }

        if (typeof params.paneId != 'undefined')
        {
            this.paneId = params.paneId;
        }

        if (typeof params.presentationAjaxBatchId != 'undefined')
        {
            this.presentationAjaxBatchId = params.presentationAjaxBatchId;
        }

        if (typeof params.presentationAjaxBatchComplete != 'undefined')
        {
            this.presentationAjaxBatchComplete = params.presentationAjaxBatchComplete;
        }

        if (typeof params.scriptId != 'undefined')
        {
            this.scriptId = params.scriptId;
        }

        if (this.scriptId)
        {
            Ext.apply(params, { scriptId: this.scriptId });

            if (typeof params.scriptAjaxBatchId != 'undefined')
            {
                this.scriptAjaxBatchId = params.scriptAjaxBatchId;
            }

            if (typeof params.scriptAjaxBatchComplete != 'undefined')
            {
                this.scriptAjaxBatchComplete = params.scriptAjaxBatchComplete;
            }

            Computronix.Ext.ScriptLoader.loadScript(Ext.apply(params,
            {
                scriptId: this.scriptId,
                ajaxBatchId: this.scriptAjaxBatchId,
                ajaxBatchComplete: this.scriptAjaxBatchComplete
            }));
        }
        else
        {
            if (this.paneId)
            {
                this.loadPane(Ext.apply(params,
                {
                    paneId: this.paneId,
                    ajaxBatchId: this.presentationAjaxBatchId,
                    ajaxBatchComplete: this.presentationAjaxBatchComplete
                }));
            }
        }
    },

    loadStores: function (params)
    {
        params = params || {};

        if (Ext.isEmpty(this.lookupStores))
        {
            if (!Ext.isEmpty(this.stores))
            {
                if (!this.storesAjaxBatchId)
                {
                    this.storesAjaxBatchId = Ext.String.format('stores{0}', this.id);
                }

                if (this.loadMask)
                {
                    this.showMask();
                }

                // Load all associated data stores.
                Ext.each(this.stores, function (store)
                {
                    store.load(Ext.apply(Ext.clone(params),
                    {
                        ajaxBatchId: this.storesAjaxBatchId
                    }));
                }, this);

                Computronix.Ext.Ajax.setBatchComplete(this.storesAjaxBatchId, true);
            }
        }
        else
        {
            this.storeLoadParams = Ext.clone(params);
            this.isRefresh = false;
            this.loadLookupStores(params);
        }

        this.items.each(function (item)
        {
            if (item.isBindable)
            {
                item.loadStores(params);
            }
        }, this);
    },

    refreshLookupStores: function ()
    {
        if (!Ext.isEmpty(this.lookupStores))
        {
            if (this.loadMask)
            {
                this.showMask();
            }

            // Reload all associated lookup data stores.
            Ext.each(this.lookupStores, function (store)
            {
                if (store.isComputronixExtStore)
                {
                    if (!store.constant)
                    {
                        store.reload();
                    }
                }
            }, this);

            Computronix.Ext.Ajax.setBatchComplete(this.lookupStoresAjaxBatchId, true);
        }
    },

    refreshStores: function ()
    {
        this.storeLoadParams = null;
        this.isRefresh = true;

        if (Ext.isEmpty(this.lookupStores))
        {
            if (!Ext.isEmpty(this.stores))
            {
                if (this.loadMask)
                {
                    this.showMask();
                }

                // Reload all associated data stores.
                Ext.each(this.stores, function (store)
                {
                    if (store.isComputronixExtStore)
                    {
                        if (!store.constant)
                        {
                            store.reload();
                        }
                    }
                }, this);

                Computronix.Ext.Ajax.setBatchComplete(this.storesAjaxBatchId, true);
            }
        }
        else
        {
            this.refreshLookupStores();
        }

        this.items.each(function (item)
        {
            if (item.isBindable)
            {
                item.refreshStores();
            }
        }, this);
    },

    setLookupStoreParams: function (params)
    {
        if (!Ext.isEmpty(this.lookupStores))
        {
            params = params || {};

            // Load all associated lookup data stores.
            Ext.each(this.lookupStores, function (store)
            {
                store.setParams(Ext.apply(Ext.clone(params),
                {
                    ajaxBatchId: this.lookupStoresAjaxBatchId
                }));
            }, this);
        }
    },

    setStoreParams: function (params)
    {
        params = params || {};

        if (!Ext.isEmpty(this.stores))
        {
            // Load all associated data stores.
            Ext.each(this.stores, function (store)
            {
                store.setParams(Ext.apply(Ext.clone(params),
                {
                    ajaxBatchId: this.storesAjaxBatchId
                }));
            }, this);
        }

        this.items.each(function (item)
        {
            if (item.isBindable)
            {
                item.setStoreParams(params);
            }
        }, this);

        this.setLookupStoreParams(params);
    },

    showMask: function ()
    {
        if (!this._mask)
        {
            if (!this.maskComponent)
            {
                this.maskComponent = this;
            }

            this._mask = new Ext.LoadMask(
            {
                msg: this.maskMessage,
                target: this.maskComponent,
            });
        }

        this._mask.show();
    }
});

// Manager objects.
//=============================================================================
//
// Manages ajax batches.
// ############################################################################################
//
Ext.define('Computronix.Ext.Ajax',
{
    extend: 'Ext.data.Connection',
    singleton: true,
    autoAbort: false,

    // Data Members
    //=============================================================================
    //
    batches: new Ext.util.HashMap(),
    successHandlers: new Ext.util.HashMap(),
    errorHandlers: new Ext.util.HashMap(),
    completeBatches: new Ext.util.HashMap(),
    requestCacheId: 'requestCache',
    requestCache: null,
    requestCacheStorage: 'LocalStorage',
    clearRequestCacheOnLogon: true,
    clearRequestCacheOnError: true,
    enableRequestCache: true,
    minRequestCacheSize: 50,
    showConcurrentWarning: true,
    showOrphanWarning: false,
    uploadProgressHandler: null,
    uploadWidget: null,
    uploadFile: null,
    form: null,
    abortPendingRequests: true,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function ()
    {
        this.callParent(arguments);

        if (this.enableRequestCache)
        {
            if (this.requestCacheStorage == 'LocalStorage' && !Ext.util.LocalStorage.supported)
            {
                this.requestCacheStorage = 'InProcess';
            }
        }
    },

    // Methods
    //=============================================================================
    //
    abortAll: function ()
    {
        Ext.Ajax.abortAll();
    },

    abortRequests: function (params)
    {
        var request;
        var abort;

        if (params)
        {
            for (var requestId in Ext.Ajax.requests)
            {
                request = Ext.Ajax.requests[requestId];

                if (request.options.params)
                {
                    abort = false;

                    Ext.Object.each(params, function (param, value)
                    {
                        if (Ext.isEmpty(value))
                        {
                            abort = false;
                            return false;
                        }
                        else
                        {
                            if (request.options.params[param] == value)
                            {
                                abort = true;
                                // Make sure to hide the busy indicator when
                                // aborting a request so that it doesn't get
                                // get orphaned
                                Computronix.Ext.busy.hide();
                            }
                            else
                            {
                                abort = false;
                                return false;
                            }
                        }
                    })

                    if (abort)
                    {
                        Ext.Ajax.abort(request);
                    }
                }
            }
        }
    },

    // Adds the request to the specified batch.
    addRequest: function (options)
    {
        var params = options.params;
        var requestKey;

        if (params && params.ajaxBatchId)
        {
            if (this.completeBatches.containsKey(params.ajaxBatchId))
            {
                this.orphanRequest(params.ajaxBatchId);
            }

            if (this.abortPendingRequests
                && this.completeBatches.containsKey(params.ajaxBatchId))
            {
                this.abortRequests(
                {
                    ajaxBatchId: params.ajaxBatchId,
                });

                // If we're making a request on a completed batch and
                // we've deliberately aborted all requests, let's reset
                // the batch entirely.
                this.completeBatches.removeByKey(params.ajaxBatchId);
                this.batches.removeByKey(params.ajaxBatchId);
            }

            requestKey = this.generateRequestKey(options);

            if (!this.batches.containsKey(params.ajaxBatchId))
            {
                this.batches.add(params.ajaxBatchId,
                    new Ext.util.HashMap());
            }

            if (this.batches.get(params.ajaxBatchId).containsKey(requestKey))
            {
                this.concurrentRequest(params.ajaxBatchId);
            }
            else
            {
                this.batches.get(params.ajaxBatchId).add(requestKey, 'requested');

                if (params.ajaxBatchComplete)
                {
                    this.setBatchComplete(params.ajaxBatchId);
                }

                // If the request has a success handler append a call to
                // removeRequest() to the end of it.
                if (options.success)
                {
                    options.success = Ext.Function.createSequence(
                        options.success,
                        function (response, options)
                        {
                            Computronix.Ext.Ajax.removeRequest(options, true);
                        });
                }
                // Do not remove the request if it is a store load,
                // presentation load, script load.
                // The load will remove the request.
                else if (options.params && !options.params.storeId
                    && !options.params.paneId && !options.params.scriptId)
                {
                    options.success = function (response, options)
                    {
                        Computronix.Ext.Ajax.removeRequest(options, true);
                    }
                }
            }
        }
    },

    addRequestCacheItem: function (requestCacheKey, response)
    {
        var item =
        {
            response:
            {
                responseText: response.responseText,
                cached: true
            },
            success: true
        }

        switch (this.requestCacheStorage)
        {
            case 'InProcess':
                this.getRequestCache().add(requestCacheKey, item);
                break;
            case 'LocalStorage':
                try
                {
                    this.getRequestCache().setItem(requestCacheKey, Ext.encode(item));
                }
                catch (exception)
                {
                    if (exception.name == 'QuotaExceededError')
                    {
                        // Need to reparse keys because items may have been added by other page instances.
                        this.getRequestCache()._keys = null;
                        if (this.getRequestCache().getKeys().length < this.minRequestCacheSize)
                        {
                            window.localStorage.clear();
                            log('Request cache quota exceeded, local storage cleared.');

                            try
                            {
                                this.getRequestCache().setItem(requestCacheKey, Ext.encode(item));
                            }
                            catch (exception)
                            {
                                if (exception.name == 'QuotaExceededError')
                                {
                                    this.requestCacheStorage = 'InProcess';
                                    this.requestCache = new Ext.util.HashMap();
                                    this.addRequestCacheItem(requestCacheKey, response);
                                }
                                else
                                {
                                    throw exception;
                                }
                            }
                        }
                        else
                        {
                            this.clearRequestCache();
                            log('Request cache quota exceeded, cache cleared.');
                            this.addRequestCacheItem(requestCacheKey, response);
                        }
                    }
                    else
                    {
                        throw exception;
                    }
                }
                break;
        }
    },

    clearRequestCache: function ()
    {
        this.getRequestCache().clear();
    },

    completeBatch: function (ajaxBatchId)
    {
        if (this.batches.containsKey(ajaxBatchId))
        {
            if (!this.batches.get(ajaxBatchId).contains('requested'))
            {
                if (this.batches.get(ajaxBatchId).contains('error'))
                {
                    if (this.errorHandlers.containsKey(ajaxBatchId))
                    {
                        this.errorHandlers.get(ajaxBatchId).call(this, ajaxBatchId);
                    }

                    this.fireEvent('batchcomplete', this, ajaxBatchId, false);
                }
                else
                {
                    if (this.successHandlers.containsKey(ajaxBatchId))
                    {
                        this.successHandlers.get(ajaxBatchId).call(this, ajaxBatchId);
                    }
                    else
                    {
                        if (!Computronix.Ext.presentationRendered)
                        {
                            Computronix.Ext.renderPresentation();
                        }
                    }

                    this.fireEvent('batchcomplete', this, ajaxBatchId, true);
                }

                this.batches.removeAtKey(ajaxBatchId);
                this.completeBatches.removeAtKey(ajaxBatchId);
            }
        }
        else
        {
            this.completeBatches.removeAtKey(ajaxBatchId);

            Computronix.Ext.messageBox.show(
            {
                title: 'Ajax Batch Error',
                msg: Ext.String.format('Ajax batch ({0}) contains no requests, complete failed.', ajaxBatchId),
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    },

    concurrentRequest: function (ajaxBatchId)
    {
        if (this.showConcurrentWarning)
        {
            Computronix.Ext.messageBox.show(
            {
                title: 'Ajax Batch Warning',
                msg: Ext.String.format('Attempting to add a duplicate request to ajax batch ({0}).', ajaxBatchId),
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
        }
    },

    generateRequestKey: function (options)
    {
        return Ext.encode({ url: options.url, params: options.params });
    },

    getRequestCache: function ()
    {
        if (!this.requestCache)
        {
            switch (this.requestCacheStorage)
            {
                case 'InProcess':
                    this.requestCache = new Ext.util.HashMap();
                    break;
                case 'LocalStorage':
                    this.requestCache = new Ext.util.LocalStorage(
                    {
                        id: Ext.String.format('{0}{1}',
                            this.requestCacheId, Computronix.Ext.cookiePath)
                    });
                    break;
            }
        }

        return this.requestCache;
    },

    getRequestCacheItem: function (requestCacheKey)
    {
        var result = null;

        switch (this.requestCacheStorage)
        {
            case 'InProcess':
                result = this.getRequestCache().get(requestCacheKey);
                break;
            case 'LocalStorage':
                // Need to reparse keys because items may have been added by other page instances.
                this.getRequestCache()._keys = null;
                result = Ext.decode(this.getRequestCache().getItem(requestCacheKey));
                break;
        }

        return result;
    },

    getRequestCacheKey: function (options)
    {
        var params = Ext.clone(options.params);

        delete params.cacheRequest;
        delete params.ajaxBatchId;
        delete params.ajaxBatchComplete;
        delete params.presentationAjaxBatchId;
        delete params.presentationAjaxBatchComplete;
        delete params.scriptAjaxBatchId;
        delete params.scriptAjaxBatchComplete;
        delete params.requestTime;

        return Ext.encode({ url: options.url, params: params });
    },

    hasRequestCacheItem: function (requestCacheKey)
    {
        var result = false;

        switch (this.requestCacheStorage)
        {
            case 'InProcess':
                result = this.getRequestCache().containsKey(requestCacheKey);
                break;
            case 'LocalStorage':
                // Need to reparse keys because items may have been added by other page instances.
                this.getRequestCache()._keys = null;
                Ext.each(this.getRequestCache().getKeys(), function (key)
                {
                    if (key == requestCacheKey)
                    {
                        result = true;
                        return false;
                    }
                }, this);
                break;
        }

        return result;
    },

    // Intercepts cached requests.
    interceptRequest: function (connection, options)
    {
        var result = true;
        var params = options.params;
        var requestCacheKey;
        var request;

        if (Computronix.Ext.SystemMonitor && Computronix.Ext.SystemMonitor.instance)
        {
            Computronix.Ext.SystemMonitor.instance.onSystemMonitorBeforeRequest(connection, options);
        }

        if (params)
        {
            // Do not add the request if it is a store load.  The load will add the request.
            if (!options.params.storeId)
            {
                Computronix.Ext.Ajax.addRequest(options);
            }

            if (this.enableRequestCache && params.cacheRequest)
            {
                requestCacheKey = this.getRequestCacheKey(options);

                if (this.hasRequestCacheItem(requestCacheKey))
                {
                    request = this.getRequestCacheItem(requestCacheKey);

                    connection.fireEvent('requestcomplete', connection, request.response, options);
                    Ext.callback(options.success, options.scope, [request.response, options]);
                    Ext.callback(options.callback, options.scope, [options, request.success, request.response]);

                    result = false;
                }
            }
        }

        return result;
    },

    orphanRequest: function (ajaxBatchId)
    {
        if (this.showOrphanWarning)
        {
            Computronix.Ext.messageBox.show(
            {
                title: 'Ajax Batch Warning',
                msg: Ext.String.format('Attempting to add a request to a completed ajax batch ({0}).', ajaxBatchId),
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING
            });
        }
    },

    // Registers a request in the cache.
    registerRequest: function (connection, response, options)
    {
        var result = true;
        var params = options.params;
        var requestCacheKey;

        if (this.enableRequestCache && params && params.cacheRequest)
        {
            requestCacheKey = this.getRequestCacheKey(options);

            if (this.hasRequestCacheItem(requestCacheKey))
            {
                result = false;
            }
            else
            {
                this.addRequestCacheItem(requestCacheKey, response);
            }
        }

        return result;
    },

    // Removes the request from the specified batch and fires the appropriate handler if all requests are complete.
    removeRequest: function (options, success)
    {
        var params = options.params;
        var requestKey;

        if (params && params.ajaxBatchId)
        {
            requestKey = this.generateRequestKey(options);

            if (this.batches.containsKey(params.ajaxBatchId) &&
                this.batches.get(params.ajaxBatchId).containsKey(requestKey))
            {
                if (success || params.ajaxIgnoreError)
                {
                    this.batches.get(params.ajaxBatchId).replace(requestKey, 'success');
                }
                else
                {
                    this.batches.get(params.ajaxBatchId).replace(requestKey, 'error');
                }

                if (this.completeBatches.containsKey(params.ajaxBatchId))
                {
                    this.completeBatch(params.ajaxBatchId);
                }
            }
        }
    },

    request: function (options)
    {
        return Ext.Ajax.request(options);
    },

    setBatchComplete: function (ajaxBatchId, silent)
    {
        if (ajaxBatchId)
        {
            // This option does not need to be supported as a param argument because if the ajax requests are
            // optional then we can not depend on params to complete the batch.
            if (silent && !this.batches.containsKey(ajaxBatchId))
            {
                if (this.successHandlers.containsKey(ajaxBatchId))
                {
                    this.successHandlers.get(ajaxBatchId).call(this, ajaxBatchId);
                }

                this.fireEvent('batchcomplete', this, ajaxBatchId, true);
            }
            else
            {
                if (!this.completeBatches.containsKey(ajaxBatchId))
                {
                    this.completeBatches.add(ajaxBatchId, null);
                    this.completeBatch(ajaxBatchId);
                }
            }
        }
    },

    setErrorHandler: function (ajaxBatchId, handler)
    {
        if (this.errorHandlers.containsKey(ajaxBatchId))
        {
            this.errorHandlers.replace(ajaxBatchId, handler);
        }
        else
        {
            this.errorHandlers.add(ajaxBatchId, handler);
        }
    },

    setSuccessHandler: function (ajaxBatchId, handler)
    {
        if (this.successHandlers.containsKey(ajaxBatchId))
        {
            this.successHandlers.replace(ajaxBatchId, handler);
        }
        else
        {
            this.successHandlers.add(ajaxBatchId, handler);
        }
    },

    setupHeaders: function (xhr, options, data, params)
    {
        var result;

        if (options.uploadFiles)
        {
            // Do not pass the data or parameters through so the content type will not be set.
            // For uploads the content type must be set by the http request object.
            result = this.callParent([xhr, options, null, null]);
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    // Performs a form post to the specified url with the given parameters.
    submitForm: function (config)
    {
        if (!this.form)
        {
            this.form = new Ext.form.Panel({ standardSubmit: true });
        }

        config = Ext.apply(config, { clientValidation: false });

        this.form.getForm().submit(config);
    },

    uploadFiles: function (files, progressHandler, completeHandler, batchCompleteHandler, widget, ajaxBatchId, suppressErrors)
    {
        var formData;
        var request;

        if (files.length > 0)
        {
            if (!ajaxBatchId)
            {
                if (widget)
                {
                    ajaxBatchId = Ext.String.format('uploadFiles{0}', widget.name);
                }
                else
                {
                    ajaxBatchId = 'uploadFiles';
                }
            }

            Ext.each(files, function (file, index)
            {
                formData = new FormData();
                formData.append(file.name, file);

                request = Ext.Ajax.request(
                {
                    url: Computronix.Ext.uploadUrl,
                    headers: { 'Content-Type': null },
                    uploadFiles: true,
                    params:
                    {
                        ajaxBatchId: ajaxBatchId,
                        fileIndex: index,
                        fileName: file.name,
                    },
                    suppressError: suppressErrors,
                    rawData: formData,
                    success: completeHandler ? Ext.Function.bind(completeHandler, widget, [file, true], true) : null,
                    failure: completeHandler ? Ext.Function.bind(completeHandler, widget, [file, false], true) : null,
                    scope: widget,
                    type: 'upload'  // Use Computronix.Ext.request.Upload as the request type.
                });

                request.onProgressCallback = progressHandler.bind(widget, file);

            }, this);

            if (batchCompleteHandler)
            {
                Computronix.Ext.Ajax.setSuccessHandler(ajaxBatchId, Ext.Function.bind(batchCompleteHandler, widget, [true], true));
                Computronix.Ext.Ajax.setErrorHandler(ajaxBatchId, Ext.Function.bind(batchCompleteHandler, widget, [false], true));
            }

            Computronix.Ext.Ajax.setBatchComplete(ajaxBatchId);
        }
    }
});

// Custom extension of the AJAX request to provide a hook into the 'progress'
// event of file uploads.
Ext.define('Computronix.Ext.request.Upload',
{
    extend: 'Ext.data.request.Ajax',
    alias: 'request.upload',

    onProgressCallback: null,

    /*
     * VERSION_WARNING 6.0.2
     * getXhrInstance is a private method on Ext.data.request.Ajax. We are
     * overriding it here to get a hook into the xhr so we can bind to the
     * progress event. It must be done here because the progress event binding
     * only works if you bind to it after the xhr is created but before the
     * request is actually sent.
     */
    getXhrInstance: function ()
    {
        var xhr = this.callParent(arguments);

        xhr.upload.addEventListener('progress', this.onXhrProgress.bind(this));

        return xhr;
    },

    onXhrProgress: function (event)
    {
        if (this.onProgressCallback)
        {
            this.onProgressCallback(event);
        }
    }
});


// Manages dynamic script loading.
// ############################################################################################
//
Ext.define('Computronix.Ext.ScriptLoader',
{
    singleton: true,
    mixins: { observable: 'Ext.util.Observable' },

    // Data Members
    //=============================================================================
    //
    busy: false,
    scripts: new Ext.util.HashMap(),
    scriptHost: null,
    loaderUrl: null,
    cacheRequest: false,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function ()
    {
        this.mixins.observable.constructor.call(this);

        if (Computronix.Ext.Ajax.enableRequestCache &&
                Computronix.Ext.Ajax.requestCacheStorage == 'LocalStorage')
        {
            this.cacheRequest = true;
        }

        Ext.Ajax.addListener('requestcomplete', this.onScriptLoaderRequestComplete, this);
        Ext.Ajax.addListener('requestexception', this.onScriptLoaderRequestException, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onScriptLoaderLoad: function (loader, response, options)
    {
        this.scripts.add(options.params.scriptId, { loader: loader, response: response, options: options });
    },

    onScriptLoaderRequestComplete: function (connection, response, options)
    {
        if (options.scripts)
        {
            if (response.responseText.indexOf('Computronix.Ext.ScriptLoader.loadComplete') == -1)
            {
                response.responseText =
                    Ext.String.format('{0}<script type=\'text/javascript\'>Computronix.Ext.ScriptLoader.loadComplete(\'{1}\');</script>',
                    response.responseText, options.params.scriptId);
            }

            this.fireEvent('scriptrequestcomplete', this, connection, response, options);
        }
    },

    onScriptLoaderRequestException: function (connection, response, options)
    {
        if (options.scripts)
        {
            this.busy = false;
        }
    },

    // Methods
    //=============================================================================
    //
    loadComplete: function (scriptId)
    {
        var args;

        if (this.scripts.containsKey(scriptId))
        {
            args = this.scripts.get(scriptId);
            this.fireEvent('scriptload', this, args.loader, args.response, args.options);
            Computronix.Ext.Ajax.removeRequest(args.options, true);

            this.scripts.replace(scriptId, null);
            this.busy = false;
        }
        else
        {
            Computronix.Ext.messageBox.show(
            {
                title: 'Script Loader Error',
                msg: Ext.String.format('Script Id ({0}) completed before it was registered.', scriptId),
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    },

    loadScript: function (params)
    {
        if (!this.scriptHost)
        {
            this.loaderUrl = Computronix.Ext.scriptLoaderUrl;
            this.scriptHost = Ext.get('ComputronixExtScriptHost');
            this.scriptHost.getLoader().addListener('load', this.onScriptLoaderLoad, this);

            // In Ext JS 4.2.3, if we are using ajax caching the method needs to handle the case
            // where active no longer exists.
            this.scriptHost.getLoader().setOptions =
                function (active, options)
                {
                    if (active)
                    {
                        active.rendererScope = options.rendererScope || this.rendererScope || this;
                    }
                }
        }

        if (this.busy)
        {
            Ext.Function.defer(this.loadScript, Computronix.Ext.loadDeferPeriod, this, arguments);
        }
        else
        {
            params = params || {};

            if (typeof params == 'number' || typeof params == 'string')
            {
                params = { scriptId: params };
            }

            if (params.scriptId)
            {
                Ext.applyIf(params,
                {
                    cacheRequest: this.cacheRequest
                });

                if (this.scripts.containsKey(params.scriptId))
                {
                    Computronix.Ext.Ajax.addRequest({ params: params });
                    // fire the requestcomplete and load events if the script already exists.
                    // Have to defer this event so that the code executes in the same sequence as when the script was first loaded.
                    Ext.Function.defer(this.fireEvent, Computronix.Ext.loadEventDeferPeriod, this, ['scriptrequestcomplete', this, null, null, { params: params }]);
                    Ext.Function.defer(this.fireEvent, Computronix.Ext.loadEventDeferPeriod, this, ['scriptload', this, null, null, { params: params }]);
                    Ext.Function.defer(Computronix.Ext.Ajax.removeRequest, Computronix.Ext.loadEventDeferPeriod, Computronix.Ext.Ajax, [{ params: params }, true]);
                }
                else
                {
                    this.busy = true;

                    this.scriptHost.load(
                    {
                        url: this.loaderUrl,
                        scripts: true,
                        params: params
                    });
                }
            }
        }
    },

    loadScriptFile: function (url, successHandler, scope)
    {
        Ext.Loader.loadScript(
        {
            url: url,
            onLoad: successHandler,
            onError: function (message)
            {
                Computronix.Ext.showError(null, message);
                log(message);
            },
            scope: scope
        });
    }
});

// Extension objects.
//=============================================================================
//
// Computronix busy indicator extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.Busy',
{
    extend: 'Ext.toolbar.TextItem',
    padding: '0 0 0 22',
    margin: '0 0 0 4',
    width: 96,
    height: 16,

    // DataMembers
    //=============================================================================
    //
    callStack: 0,
    useCallStack: true,
    message: 'Loading ({0})...',
    _mask: null,
    busyMask: false,
    maskComponent: null,
    maskElement: null,
    maskDeferPeriod: 250,
    maskMessage: null,

    // Methods
    //=============================================================================
    //
    hide: function ()
    {
        this.callStack--;
        this.setText(Ext.String.format(this.message, this.callStack));

        if (!this.useCallStack || this.callStack <= 0)
        {
            this.setText(null);
            this.removeCls('cxbusyicon cxicon');
            this.callStack = 0;

            if (this.busyMask)
            {
                if (this.maskDeferPeriod > 0)
                {
                    Ext.Function.defer(this.hideMask, this.maskDeferPeriod, this);
                }
                else
                {
                    this.hideMask();
                }
            }
        }
    },

    hideMask: function ()
    {
        if (this._mask)
        {
            if (!this.useCallStack || this.callStack <= 0)
            {
                this._mask.hide();
            }
        }
        else
        {
            if (this.maskElement)
            {
                if (!this.useCallStack || this.callStack <= 0)
                {
                    this.maskElement.unmask();
                }
            }
        }
    },

    isBusy: function ()
    {
        return (this.callStack > 0);
    },

    /*
     * VERSION_WARNING
     * In ExtJS v4.1.0 the ancestor method calls doLayout() on its owner
     * container which causes the text update to take about 400 times longer.
     */
    setText: function (text)
    {
        if (this.rendered)
        {
            this.el.update(text);
        }
        else
        {
            this.text = text;
        }
    },

    show: function ()
    {
        this.callStack++;
        this.setText(Ext.String.format(this.message, this.callStack));
        this.addCls('cxbusyicon cxicon');

        if (this.busyMask)
        {
            if (this.maskDeferPeriod > 0)
            {
                Ext.Function.defer(this.showMask, this.maskDeferPeriod, this);
            }
            else
            {
                this.showMask();
            }
        }
    },

    showMask: function ()
    {
        if (!this.useCallStack || this.callStack > 0)
        {
            if (!this._mask)
            {
                if (!this.maskComponent)
                {
                    this.maskComponent = Computronix.Ext.viewport;
                }

                if (this.maskComponent)
                {
                    this._mask = new Ext.LoadMask(
                    {
                        msg: this.maskMessage,
                        target: this.maskComponent,
                        useMsg: !!this.maskMessage,
                    });
                }
            }

            if (this._mask)
            {
                this._mask.show();
            }
            else
            {
                if (!this.maskElement)
                {
                    this.maskElement = Ext.getBody();
                }

                if (this.maskElement)
                {
                    this.maskElement.mask(this.maskMessage);
                }
            }
        }
    }
});

// Computronix button extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.button.Button',
{
    extend: 'Ext.button.Button',
    alias: 'widget.computronixextbutton',
    mixins: { ajaxBatchMask: 'Computronix.Ext.AjaxBatchMask' },

    // Data Members
    //=============================================================================
    //
    toolbarStyle: false,
    enableHiddenText: true,
    textVisible: true,
    initialText: '',
    initialTooltip: '',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        if (this.toolbarStyle && this.ui.indexOf('-toolbar') == -1)
        {
            this.ui += '-toolbar'
        };

        if (this.text)
        {
            this.initialText = this.text;
        }

        if (this.tooltip)
        {
            this.initialTooltip = this.tooltip;
        }

        this.mixins.ajaxBatchMask.constructor.call(this);
    },

    // Methods
    //=============================================================================
    //
    hideText: function ()
    {
        if (this.iconCls && this.iconCls != 'cxblankicon')
        {
            if (this.tooltip)
            {
                if (this.text)
                {
                    this.setTooltip(Ext.String.format('{0}<br/>{1}', this.text, this.tooltip));
                }
            }
            else
            {
                if (this.text)
                {
                    this.setTooltip(this.text);
                }
            }

            this.setText(null);
        }

        this.textVisible = false;
    },

    setAriaLabel: function (ariaLabel)
    {
        this.ariaLabel = ariaLabel;

        if (this.getEl())
        {
            this.getEl().set({ 'aria-label': this.ariaLabel });
        }
    },

    showText: function ()
    {
        if (this.iconCls && this.iconCls != 'cxblankicon')
        {
            this.setText(this.initialText);
            this.setTooltip(this.initialTooltip);
        }

        this.textVisible = true;
    }
});

// Computronix button extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.CollapseButton',
{
    extend: 'Ext.panel.Tool',
    alias: 'widget.computronixextcollapsebutton',

    // Data Members
    //=============================================================================
    //
    panel: null,
    reverseImage: null,
    collapseFocusTarget: null,
    expandFocusTarget: null,
    initialHeight: null,
    animate: false,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (this.panel)
        {
            this.setButton(!this.panel.isHidden());
        }

        this.callParent();

        this.addListener('click', this.onCollapseButtonClick, this);
        this.addListener('render', this.onCollapseButtonRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onCollapseButtonClick: function (item, event)
    {
        this.toggleCollapse();
        // Used to force focus on click events since ExtJS suppresses focus on
        // MouseDown for Ext.panel.Tool. Quoted from v6.0.3 ext-all-debug.js:
        //  // We prevent default action on mousedown to avoid focusing the tool.
        //  // This is consistent with tool behavior in versions prior to 5.5 where
        //  // tools were pointer-interactive only.
        this.focus();
    },

    onCollapseButtonRender: function (item, event)
    {
        this.setAriaLabel();
    },

    // Methods
    //=============================================================================
    //
    collapse: function ()
    {
        if (this.panel)
        {
            if (this.panel.isVisible())
            {
                if (this.animate)
                {
                    this.initialHeight = this.panel.getHeight();
                    this.panel.animate(
                    {
                        to: { height: 0 },
                        listeners: { afteranimate: { fn: function () { this.hide(); }, scope: this.panel } }
                    });
                }
                else
                {
                    this.panel.hide();
                }

                this.setButton(false);

                if (this.collapseFocusTarget)
                {
                    this.collapseFocusTarget.focus();
                }
            }
        }
    },

    expand: function ()
    {
        var height;

        if (this.panel)
        {
            if (!this.panel.isVisible())
            {
                this.panel.show();

                if (this.animate)
                {
                    height = this.panel.getHeight();

                    if (height > this.initialHeight)
                    {
                        this.initialHeight = height;
                    }

                    this.panel.animate(
                    {
                        from: { height: 0 },
                        to: { height: this.initialHeight }
                    });
                }

                this.setButton(true);

                if (this.expandFocusTarget)
                {
                    this.expandFocusTarget.focus();
                }
                else
                {
                    if (this.panel.focusFirst)
                    {
                        this.panel.focusFirst();
                    }
                }
            }
        }
    },

    setAriaLabel: function ()
    {
        var imageEl = this.getEl() && this.getEl().down('img');

        if (imageEl)
        {
            if (this.type === 'up')
            {
                imageEl.set({ 'alt': 'Collapse' });
            }
            else
            {
                imageEl.set({ 'alt': 'Expand' });
            }
        }
    },

    setButton: function (visible)
    {
        if (this.reverseImage)
        {
            visible = !visible;
        }

        if (visible)
        {
            this.setType('up');
        }
        else
        {
            this.setType('down');
        }

        this.setAriaLabel();
    },

    toggleCollapse: function ()
    {
        if (this.panel)
        {
            if (this.panel.isVisible())
            {
                this.collapse();
            }
            else
            {
                this.expand();
            }
        }
    }
});

// Computronix split button extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.button.Split',
{
    extend: 'Ext.button.Split',
    alias: 'widget.computronixextsplitbutton',
    mixins: { ajaxBatchMask: 'Computronix.Ext.AjaxBatchMask' },

    // Data Members
    //=============================================================================
    //
    actionItem: null,
    chooseItem: true,
    toolbarStyle: false,
    enableHiddenText: true,
    textVisible: true,
    useInitialIcon: false,
    initialIcon: '',
    initialIconCls: '',
    initialText: '',
    initialTooltip: '',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        var item;

        this.callParent();

        if (this.toolbarStyle && this.ui.indexOf('-toolbar') == -1)
        {
            this.ui += '-toolbar'
        };

        if (this.text)
        {
            this.initialText = this.text;
        }

        if (this.tooltip)
        {
            this.initialTooltip = this.tooltip;
        }

        if (this.icon)
        {
            this.initialIcon = this.icon;
        }

        if (this.iconCls)
        {
            this.initialIconCls = this.iconCls;
        }

        if (this.chooseItem && this.menu && this.menu.items.length > 0)
        {
            if (!this.text)
            {
                // Use the first enabled and selectable item as the default.
                this.menu.items.each(function (_item)
                {
                    if (!_item.isDisabled() && _item.selectable != false)
                    {
                        item = _item;
                        return false;
                    }
                }, this);

                if (item)
                {
                    this.actionItem = item;
                    this.text = item.text;

                    if (this.text)
                    {
                        this.initialText = this.text;
                    }
                    else
                    {
                        this.initialText = '';
                    }

                    if (item.icon)
                    {
                        this.icon = item.icon;
                    }

                    if (item.iconCls)
                    {
                        this.iconCls = item.iconCls;
                    }

                    if (item.tooltip)
                    {
                        this.tooltip = item.tooltip;
                    }

                    this.ajaxBatchId = item.ajaxBatchId;
                    this.disableMode = item.disableMode;
                    this.maskComponent = item.maskComponent;
                    this.maskMessageFormat = item.maskMessageFormat;

                    this.handler = item.doAction;
                    this.scope = item;
                }
            }

            this.setItemClickHandler();
        }

        this.mixins.ajaxBatchMask.constructor.call(this);
    },

    // Event Handlers
    //=============================================================================
    //
    onButtonSplitMenuItemClick: function (item, event)
    {
        if (this.actionItem != item)
        {
            this.setButton(item);
        }
    },

    // Methods
    //=============================================================================
    //
    hideText: function ()
    {
        if (this.iconCls && this.iconCls != 'cxblankicon')
        {
            if (this.initialTooltip)
            {
                this.setTooltip(Ext.String.format('{0}<br/>{1}', this.text, this.initialTooltip));
            }
            else
            {
                this.setTooltip(this.text);
            }

            this.setText(null);
        }

        this.textVisible = false;
    },

    setButton: function (item)
    {
        if (!item && this.menu && this.menu.items.length > 0)
        {
            // Use the first enabled and selectable item as the default.
            this.menu.items.each(function (_item)
            {
                if (!_item.isDisabled() && _item.selectable != false)
                {
                    item = _item;
                    return false;
                }
            }, this);
        }

        if (item)
        {
            this.actionItem = item;

            if (this.textVisible || !item.iconCls || item.iconCls == 'cxblankicon')
            {
                if (item.text)
                {
                    this.setText(item.text);
                }
                else
                {
                    this.setText('');
                }

                this.initialText = this.text;
            }
            else
            {
                if (item.text)
                {
                    this.initialText = item.text;
                }
                else
                {
                    this.initialText = '';
                }

                this.setText('');
            }

            if (item.icon)
            {
                this.setIcon(item.icon);
            }
            else
            {
                if (this.useInitialIcon)
                {
                    this.setIcon(this.initialIcon);
                }
                else
                {
                    this.setIcon('');
                }
            }

            if (item.iconCls)
            {
                this.setIconCls(item.iconCls);
            }
            else
            {
                if (this.useInitialIcon)
                {
                    this.setIconCls(this.initialIconCls);
                }
                else
                {
                    this.setIconCls('cxblankicon');
                }
            }

            if (item.tooltip)
            {
                this.initialTooltip = item.tooltip;
            }
            else
            {
                this.initialTooltip = '';
            }

            if (this.textVisible || !item.iconCls || item.iconCls == 'cxblankicon')
            {
                this.setTooltip(this.initialTooltip);
            }
            else
            {
                if (item.tooltip)
                {
                    this.setTooltip(Ext.String.format('{0}<br/>{1}', this.initialText, item.tooltip));
                }
                else
                {
                    this.setTooltip(this.initialText);
                }
            }

            this.ajaxBatchId = item.ajaxBatchId;
            this.disableMode = item.disableMode;
            this.maskComponent = item.maskComponent;
            this.maskMessageFormat = item.maskMessageFormat;

            this.setHandler(item.doAction, item);
        }
    },

    setItemClickHandler: function ()
    {
        this.menu.items.each(function (item)
        {
            if (item.selectable != false)
            {
                item.addListener('click', this.onButtonSplitMenuItemClick, this);
                item.splitButton = this;
            }
        }, this);
    },

    loadMenu: function (menu, chooseItem)
    {
        var item;

        this.setMenu(menu, true);

        menu.button = this;

        if (chooseItem != false && this.chooseItem)
        {
            this.setButton();
        }

        if (this.chooseItem)
        {
            this.setItemClickHandler();
        }
    },

    showText: function ()
    {
        if (this.iconCls && this.iconCls != 'cxblankicon')
        {
            this.setText(this.initialText);
            this.setTooltip(this.initialTooltip);
        }

        this.textVisible = true;
    }
});

// Computronix identity generator extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.data.identifier.Generator.None',
{
    extend: 'Ext.data.identifier.Generator',
    alias: 'data.identifier.none',

    generate: function ()
    {
        return null;
    }
});

// Computronix data model extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.data.Model',
{
    extend: 'Ext.data.Model',
    identifier: 'none',

    // Data Members
    //=============================================================================
    //
    displayField: null,
    fieldsByName: null,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function (config)
    {
        var fields = this.getFields();

        this.callParent(arguments);

        this.fieldsByName = new Ext.util.MixedCollection();

        Ext.each(fields, function (field)
        {
            if (field.name != 'id' || this.idProperty == 'id')
            {
                this.fieldsByName.add(field.name, field);
            }
        }, this);

        // This fixes a problem introduced in v4.1.0 that creates an id field of type auto in every model.
        if (fields.length > 0 && fields[0].name == 'id' && this.idProperty != 'id')
        {
            Ext.Array.remove(fields, fields[0]);
        }
    },

    // Methods
    //=============================================================================
    //
    getDisplayField: function ()
    {
        if (this.displayField == null)
        {
            this.fieldsByName.each(function (field)
            {
                if (field.name != this.idProperty &&
                    (field.type == Ext.data.Types.STRING.type || field.type == Ext.data.Types.AUTO.type))
                {
                    this.displayField = field.name;
                    return false;
                }
            }, this);
        }

        return this.displayField;
    },

    privates: {
        // If the values are arrays and they both contain the same values then they are equal.
        isEqual: function (value1, value2, field)
        {
            var result = this.callParent(arguments);

            if (!result && Ext.isArray(value1) && Ext.isArray(value2))
            {
                result = Ext.encode(value1.sort()) == Ext.encode(value2.sort());
            }

            return result;
        }
    }
});

// Computronix json store extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.data.Store',
{
    extend: 'Ext.data.Store',
    alias: 'store.computronixextstore',

    // Data Members
    //=============================================================================
    //
    isComputronixExtStore: true,
    cloneSuffix: null,
    dataId: null,
    paneId: null,
    lookupSuffix: 'Lookup',
    lookupStores: new Ext.util.HashMap(),
    constant: false,
    loadOnce: false,
    ignoreChanges: false,
    includeUnchanged: false,
    lastParams: null,
    ajaxBatchId: null,
    ajaxBatchComplete: false,
    lastResponse: null,
    lastRequestTime: null,

    // Constructor and initialization
    //=============================================================================
    //
    constructor: function (config)
    {
        var lookupStore;
        var lookupStoreId;

        // Configure data Id.
        if (!this.dataId)
        {
            Ext.applyIf(config, { dataId: config.storeId });
        }

        // Configure a default model.
        if (!this.model)
        {
            Ext.applyIf(config, { model: config.dataId });
        }

        // Configure a default proxy.
        if (!this.proxy)
        {
            Ext.applyIf(config, { proxy: {} });

            Ext.applyIf(config.proxy,
            {
                type: 'ajax',
                url: Computronix.Ext.dataHandlerUrl,
                actionMethods:
                {
                    create: 'POST',
                    read: 'POST',
                    update: 'POST',
                    destroy: 'POST',
                },
                extraParams: {},
                reader: {},
            });

            // Configure a default action.
            Ext.applyIf(config.proxy.extraParams,
            {
                action: 'load',
            });

            // Configure a default reader.
            Ext.applyIf(config.proxy.reader,
            {
                type: 'json',
                rootProperty: 'data',
                totalProperty: 'rowCount',
            });
        }

        this.callParent(arguments);

        // In Ext JS 4.2.3, if we are using ajax caching the method needs to handle the case
        // where response is null.
        if (this.proxy && this.proxy.type == 'ajax')
        {
            this.proxy.setException = function (operation, response)
            {
                if (response)
                {
                    operation.setException(
                    {
                        status: response.status,
                        statusText: response.statusText,
                    });
                }
            }
        }

        this.getNewModel().fieldsByName.each(function (field)
        {
            lookupStore = null;
            lookupStoreId = Ext.String.format('{0}{1}', field.name, this.lookupSuffix);

            if (this.storeId != lookupStoreId && Ext.data.StoreManager.containsKey(lookupStoreId))
            {
                lookupStore = Ext.getStore(lookupStoreId);
            }

            if (lookupStore)
            {
                this.setLookupStore(field.name, lookupStore, lookupStore.getNewModel().getDisplayField());
            }
        }, this);
    },

    // Methods
    //=============================================================================
    //
    clear: function ()
    {
        if (!this.constant && !this.loadOnce)
        {
            this.removeAll();
        }
    },

    copyData: function (store)
    {
        var newRecord;

        this.suspendEvents();

        this.removeAll();

        store.each(function (record)
        {
            newRecord = record.copy();
            Ext.data.Model.id(newRecord);
            this.add(newRecord);
        }, this);

        this.resumeEvents();
        this.fireEvent('datachanged', this);
    },

    dataStoreLoadComplete: function (records, operation, success)
    {
        if (operation.initialConfig.params && operation.initialConfig.params.ajaxBatchId &&
            operation.initialConfig.initialParams)
        {
            Computronix.Ext.Ajax.removeRequest(
            {
                params: operation.initialConfig.initialParams,
            }, success);
        }
    },

    getChanges: function (includeOriginalValue)
    {
        var result = null;
        var newRecords = this.getNewRecords();
        var updatedRecords = this.getUpdatedRecords();
        var removedRecords = this.getRemovedRecords();
        var changedFields;
        var data;

        if (newRecords.length > 0 || updatedRecords.length > 0 || removedRecords.length > 0 || this.includeUnchanged)
        {
            result = {};
            result.dataId = this.dataId;

            if (this.paneId)
            {
                result.paneId = this.paneId;
            }

            result.idProperty = this.getNewModel().idProperty;
            result.inserts = [];
            result.updates = [];
            result.deletes = [];

            Ext.each(newRecords, function (record)
            {
                data = {};

                record.fieldsByName.eachKey(function (fieldName, field)
                {
                    if (field.persist)
                    {
                        data[fieldName] = record.get(fieldName);
                    }
                }, this);

                result.inserts.push(data);
            }, this);

            if (this.includeUnchanged)
            {
                this.data.each(function (record)
                {
                    if (!record.phantom)
                    {
                        changedFields = record.getChanges();
                        data = {};

                        if (record.fieldsByName.containsKey(this.getNewModel().idProperty))
                        {
                            data[this.getNewModel().idProperty] = record.getId();
                        }

                        record.fieldsByName.eachKey(function (fieldName, field)
                        {
                            if (field.persist)
                            {
                                data[fieldName] = record.get(fieldName);

                                if (includeOriginalValue != false && changedFields.hasOwnProperty(fieldName))
                                {
                                    data[Ext.String.format('_{0}', fieldName)] = record.modified[fieldName];
                                }
                            }
                        }, this);

                        result.updates.push(data);
                    }
                }, this);
            }
            else
            {
                Ext.each(updatedRecords, function (record)
                {
                    changedFields = record.getChanges();
                    data = {};

                    if (record.fieldsByName.containsKey(this.getNewModel().idProperty))
                    {
                        data[this.getNewModel().idProperty] = record.getId();
                    }

                    for (var field in changedFields)
                    {
                        data[field] = changedFields[field];

                        if (includeOriginalValue != false)
                        {
                            data[Ext.String.format('_{0}', field)] = record.modified[field];
                        }
                    }

                    result.updates.push(data);
                }, this);
            }

            Ext.each(removedRecords, function (record)
            {
                data = {};

                if (record.fieldsByName.containsKey(this.getNewModel().idProperty))
                {
                    data[this.getNewModel().idProperty] = record.getId();
                }

                result.deletes.push(data);
            }, this);
        }

        return result;
    },

    // Get the display value from a lookup store.
    getDisplayValue: function (fieldName, value)
    {
        var result = value;
        var lookup = this.lookupStores.get(fieldName);

        if (lookup)
        {
            result = Computronix.Ext.getDisplayValue(lookup.store, value, lookup.displayField);
        }

        return result;
    },

    getGroupString: function (record)
    {
        var result = null;
        var group = this.groupers.first();
        var fieldName;

        if (group)
        {
            fieldName = group.property;

            if (this.lookupStores.containsKey(fieldName))
            {
                result = this.getDisplayValue(fieldName, record.get(fieldName));
            }
            else
            {
                result = this.callParent(arguments);
            }
        }

        return result;
    },

    getCxAverage: function (records, field)
    {
        var result = null;
        var value;
        var count = 0;

        Ext.each(records, function (record)
        {
            value = record.get(field);

            if (!Ext.isEmpty(value))
            {
                result += value;
                count++;
            }
        }, this);

        return (count > 0 ? result / count : result);
    },

    getCxCount: function (records, field)
    {
        var result = 0;

        Ext.each(records, function (record)
        {
            if (!Ext.isEmpty(record.get(field)))
            {
                result++;
            }
        }, this);

        return result;
    },

    getCxMax: function (records, field)
    {
        var result = '';
        var value;

        Ext.each(records, function (record)
        {
            value = record.get(field);

            if (!Ext.isEmpty(value))
            {
                if (typeof value == 'string')
                {
                    if (Ext.isEmpty(result) || value.toLowerCase() > result.toLowerCase())
                    {
                        result = value;
                    }
                }
                else
                {
                    if (Ext.isEmpty(result) || value > result)
                    {
                        result = value;
                    }
                }
            }
        }, this);

        return result;
    },

    getCxMin: function (records, field)
    {
        var result = '';
        var value;

        Ext.each(records, function (record)
        {
            value = record.get(field);

            if (!Ext.isEmpty(value))
            {
                if (typeof value == 'string')
                {
                    if (Ext.isEmpty(result) || value.toLowerCase() < result.toLowerCase())
                    {
                        result = value;
                    }
                }
                else
                {
                    if (Ext.isEmpty(result) || value < result)
                    {
                        result = value;
                    }
                }
            }
        }, this);

        return result;
    },

    getCxSum: function (records, field)
    {
        return this.sum(field);
    },

    getNewModel: function ()
    {
        var result = null;

        if (this.model)
        {
            result = Ext.create(Ext.data.schema.Schema.lookupEntity(this.model));
        }

        return result;
    },

    getNewRecords: function ()
    {
        var result;
        var source = this.getData().getSource();

        if (source)
        {
            result = source.filterBy(this.filterNew).items;
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    getParams: function (params)
    {
        params = params || {};

        if (typeof params == 'number')
        {
            params = Ext.decode(Ext.String.format('{ "{0}": {1} }',
                this.getNewModel().idProperty, params));
        }

        if (typeof params == 'string')
        {
            params = Ext.decode(Ext.String.format('{ "{0}": "{1}" }',
                this.getNewModel().idProperty, params));
        }

        params = Ext.clone(params);
        Ext.apply(params, { storeId: this.storeId });
        Ext.apply(params, { dataId: this.dataId });

        if (this.ajaxBatchId)
        {
            Ext.applyIf(params, { ajaxBatchId: this.ajaxBatchId });
        }

        if (this.ajaxBatchComplete)
        {
            Ext.applyIf(params, { ajaxBatchComplete: this.ajaxBatchComplete });
        }

        return params;
    },

    getUpdatedRecords: function ()
    {
        var result;
        var source = this.getData().getSource();

        if (source)
        {
            result = source.filterBy(this.filterUpdated).items;
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    hasChanges: function ()
    {
        var result = false;

        if (!this.ignoreChanges)
        {
            result = (this.getNewRecords().length > 0 || this.getUpdatedRecords().length > 0 || this.getRemovedRecords().length > 0)
        }

        return result;
    },

    load: function (params, append)
    {
        var options = null;

        params = params || {};

        if (params.params)
        {
            options = params;
            params = this.setParams(options.params);
            options.params = Ext.clone(params);
        }
        else
        {
            params = this.setParams(params);
        }

        if (!this.loadOnce || !this.isLoaded())
        {
            Computronix.Ext.Ajax.addRequest({ params: params });

            if (options)
            {
                if (append)
                {
                    options.addRecords = true;
                }

                options.initialParams = this.lastParams;

                if (options.callback)
                {
                    options.callback = Ext.Function.createSequence(options.callback, this.dataStoreLoadComplete);
                }
                else
                {
                    options.callback = this.dataStoreLoadComplete;
                }

                this.callParent([options]);
            }
            else
            {
                if (append)
                {
                    this.callParent([
                    {
                        addRecords: true,
                        params: Ext.clone(params),
                        initialParams: this.lastParams,
                        callback: this.dataStoreLoadComplete
                    }]);
                }
                else
                {
                    this.callParent([
                    {
                        params: Ext.clone(params),
                        initialParams: this.lastParams,
                        callback: this.dataStoreLoadComplete
                    }]);
                }
            }
        }
    },

    onProxyLoad: function (operation)
    {
        this.lastResponse = operation._response || { responseText: null };

        if (operation.addRecords || this.lastRequestTime == operation.initialConfig.params.requestTime)
        {
            this.callParent(arguments);
        }
        else
        {
            if (operation.initialConfig.params && operation.initialConfig.params.ajaxBatchId &&
                operation.initialConfig.initialParams)
            {
                Computronix.Ext.Ajax.removeRequest({ params: operation.initialConfig.initialParams }, operation.success);
            }
        }
    },

    reload: function ()
    {
        this.load(this.lastParams);
    },

    removeAll: function ()
    {
        this.callParent(arguments);

        //Ext JS v4.07 does not clear the removed collection when the store is cleared.
        this.removed = [];
    },

    // Adds a lookup store.
    setLookupStore: function (fieldName, lookupStore, displayField)
    {
        var field = this.getNewModel().fieldsByName.get(fieldName);

        if (displayField == null)
        {
            displayField = lookupStore.getNewModel().getDisplayField();
        }

        this.lookupStores.add(fieldName, { store: lookupStore, displayField: displayField });

        eval("field.sortType = function (value) { return Ext.getStore('" + this.storeId + "').getDisplayValue('" + fieldName + "', value);}");
    },

    setParams: function (params)
    {
        params = this.getParams(params);

        this.lastRequestTime = Ext.Date.now();
        params.requestTime = this.lastRequestTime;

        this.lastParams = params;

        return params;
    }
});

// Computronix error indicator extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.MessageTool',
{
    extend: 'Ext.button.Button',
    width: 85,
    iconCls: 'cxblankicon',

    // DataMembers
    //=============================================================================
    //
    message: '{0} Errors',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.addListener('click', this.onErrorClick, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onErrorClick: function (button, event)
    {
        this.update();
    },

    // Methods
    //=============================================================================
    //
    clear: function ()
    {
        Computronix.Ext.setErrors(null, false, false);
        Computronix.Ext.setWarnings(null, false, false);

        this.setButton([]);
    },

    setButton: function (errors)
    {
        if (this.rendered)
        {
            if (errors.length > 0)
            {
                this.setText(Ext.String.format(this.message, errors.length));
                this.setIconCls('cxerroricon');

                if (Computronix.Ext.MessageSummary)
                {
                    if (Computronix.Ext.MessageSummary.instance)
                    {
                        Computronix.Ext.MessageSummary.instance.setup(this, errors);
                    }
                    else
                    {
                        Computronix.Ext.MessageSummary.showMessageSummary(this, errors);
                    }
                }
            }
            else
            {
                this.setText(' ');
                this.setIconCls('cxblankicon');

                if (Computronix.Ext.MessageSummary && Computronix.Ext.MessageSummary.instance)
                {
                    Computronix.Ext.MessageSummary.instance.close();
                }
            }
        }
    },

    update: function ()
    {
        var errors = Computronix.Ext.getErrors();
        var warnings = Computronix.Ext.getWarnings();
        var messages = [];

        Ext.Array.each(errors, function (error)
        {
            if (!Ext.isEmpty(Ext.String.trim(error)))
            {
                messages = Ext.Array.merge(messages, [Ext.String.format('<span class="cxerrormessage">{0}</span>', error)]);
            }
        }, this);

        Ext.Array.each(warnings, function (warning)
        {
            if (!Ext.isEmpty(Ext.String.trim(warning)))
            {
                messages = Ext.Array.merge(messages, [Ext.String.format('<span class="cxwarningmessage">{0}</span>', warning)]);
            }
        }, this);

        this.setButton(messages);

        return errors.length == 0;
    }
});

// Computronix checkbox extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Checkbox',
{
    extend: 'Ext.form.field.Checkbox',
    alias: 'widget.computronixextcheckboxfield',
    mixins: { mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator', microhelp: 'Computronix.Ext.Microhelp' },
    labelClsExtra: 'cxfieldlabel',
    width: 200,
    uncheckedValue: false,

    // Data Members
    //=============================================================================
    //
    align: 'left',
    readOnlyCheckbox: null,
    readOnlyCheckboxChecked: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);

        this.addListener('render', this.onFormFieldCheckboxRender, this);
        this.addListener('destroy', this.onFormFieldCheckboxDestroy, this);
    },

    initEvents: function ()
    {
        this.callParent();

        new Ext.util.KeyNav(
        {
            target: this.inputEl,
            enter: function (e)
            {
                e.preventDefault();
            }
        });
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldCheckboxDestroy: function (field)
    {
        if (this.readOnlyCheckbox)
        {
            this.readOnlyCheckbox.destroy();
        }

        if (this.readOnlyCheckboxChecked)
        {
            this.readOnlyCheckboxChecked.destroy();
        }
    },

    onFormFieldCheckboxRender: function (field)
    {
        this.readOnlyCheckbox = Ext.DomHelper.insertAfter(this.inputEl,
            { tag: 'div', id: this.inputEl.id + 'ro', cls: 'cxreadonlyformcheckbox', html: '&nbsp;' }, true);
        this.readOnlyCheckboxChecked = Ext.DomHelper.insertAfter(this.inputEl,
            { tag: 'div', id: this.inputEl.id + 'roc', cls: 'cxreadonlyformcheckboxchecked', html: '&nbsp;' }, true);

        this.readOnlyCheckbox.setVisibilityMode(Ext.Element.DISPLAY);
        this.readOnlyCheckboxChecked.setVisibilityMode(Ext.Element.DISPLAY);

        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.labelEl, { 'text-align': this.align, 'margin-left': 'auto', 'margin-right': 'auto' });
            Ext.DomHelper.applyStyles(this.readOnlyCheckbox, { 'background-position': this.align, width: '100%' });
            Ext.DomHelper.applyStyles(this.readOnlyCheckboxChecked, { 'background-position': this.align, width: '100%' });
        }

        this.setReadOnly(this.readOnly);
    },

    // Methods
    //=============================================================================
    //
    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        if (this.readOnlyCheckbox && this.readOnlyCheckboxChecked)
        {
            if (this.displayEl)
            {
                if (readOnly)
                {
                    this.displayEl.setVisible(false);

                    if (this.checked)
                    {
                        this.readOnlyCheckbox.setVisible(false);
                        this.readOnlyCheckboxChecked.setVisible(true);
                    }
                    else
                    {
                        this.readOnlyCheckbox.setVisible(true);
                        this.readOnlyCheckboxChecked.setVisible(false);
                    }
                }
                else
                {
                    this.displayEl.setVisible(true);
                    this.readOnlyCheckbox.setVisible(false);
                    this.readOnlyCheckboxChecked.setVisible(false);
                }
            }
        }
    },

    setValue: function (value)
    {
        this.callParent(arguments);

        if (this.readOnlyCheckbox && this.readOnlyCheckboxChecked)
        {
            if (this.readOnly)
            {
                if (this.checked)
                {
                    this.readOnlyCheckbox.setVisible(false);
                    this.readOnlyCheckboxChecked.setVisible(true);
                }
                else
                {
                    this.readOnlyCheckbox.setVisible(true);
                    this.readOnlyCheckboxChecked.setVisible(false);
                }
            }
        }
    }
});

// Computronix combobox extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.ComboBox',
{
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.computronixextcombobox',
    mixins:
    {
        mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator',
        microhelp: 'Computronix.Ext.Microhelp',
        message: 'Computronix.Ext.Message'
    },

    forceSelection: true,
    labelClsExtra: 'cxfieldlabel',
    listConfig: { loadMask: false },
    queryMode: 'local',
    selectOnFocus: true,
    typeAhead: true,
    width: 200,

    // Data Members
    //=============================================================================
    //
    activeField: 'active',
    align: 'left',
    columns: null,
    encodeList: true,
    parentForm: null,
    record: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        var headerTpl = '';
        var columnTpl = '';
        var displayWidth = 0;
        var hasHeader = false;

        // Configure a default store.
        if (this.store)
        {
            this.store = Ext.getStore(this.store);
        }
        else
        {
            if (Ext.data.StoreManager.containsKey(this.name + 'Lookup'))
            {
                this.store = Ext.getStore(this.name + 'Lookup');
            }
        }

        if (this.store)
        {
            if (!this.valueField)
            {
                this.valueField = this.store.getNewModel().idProperty;
            }

            if (this.displayField == 'text')
            {
                this.displayField = this.store.getNewModel().getDisplayField();
                this.displayTpl = null;
                this.setDisplayTpl(this.applyDisplayTpl(this.displayTpl));
            }
        }

        if (this.columns)
        {
            Ext.each(this.columns, function (column)
            {
                if (!Ext.isEmpty(column.text, true))
                {
                    hasHeader = true;
                }

                headerTpl += Ext.String.format('<th class="cxcomboboxcolumnheader" style="text-align: {1}; padding-left: 5px; overflow: hidden; width: {2}px; white-space: nowrap;">{0}</th>',
                        column.text || '', column.align || 'left', column.width);

                if (this.encodeList)
                {
                    columnTpl += Ext.String.format('<td style="text-align: {1}; padding-left: 5px; overflow: hidden; width: {2}px; white-space: nowrap;">{{0}:htmlEncode}</td>', column.name, column.align || 'left', column.width);
                }
                else
                {
                    columnTpl += Ext.String.format('<td style="text-align: {1}; padding-left: 5px; overflow: hidden; width: {2}px; white-space: nowrap;">{{0}}</td>', column.name, column.align || 'left', column.width);
                }

                displayWidth += column.width;
            }, this);

            this.matchFieldWidth = false;

            // Make room for a possible scrollbar.
            this.listConfig = Ext.apply(this.listConfig || {}, { width: displayWidth + 20 });

            if (hasHeader)
            {
                this.tpl =
                    Ext.String.format('<table style="table-layout: fixed; width: {1}px;"><tr>{2}</tr><tpl for="."><tr role="option" class="x-boundlist-item">{0}</tr></tpl></table>',
                    columnTpl, displayWidth, headerTpl);
            }
            else
            {
                this.tpl =
                    Ext.String.format('<table style="table-layout: fixed; width: {1}px;"><tpl for="."><tr role="option" class="x-boundlist-item">{0}</tr></tpl></table>',
                    columnTpl, displayWidth);
            }
        }
        else
        {
            if (this.encodeList)
            {
                this.tpl = '<tpl for="."><div class="x-boundlist-item">{' + this.displayField + ':htmlEncode}</div></tpl>';
            }
        }

        this.callParent();

        if (!this.emptyText && this.fieldLabel)
        {
            this.emptyText = Ext.String.format('Select a {0}...', this.fieldLabel);
        }

        if (this.blankText == 'This field is required' && this.fieldLabel)
        {
            this.blankText = Ext.String.format('{0} is required', this.fieldLabel);
        }

        if (this.minLengthText == 'The minimum length for this field is {0}' && this.fieldLabel)
        {
            this.minLengthText = Ext.String.format('The minimum length for {0} is {1}', this.fieldLabel, this.minLength);
        }

        if (this.maxLengthText == 'The maximum length for this field is {0}' && this.fieldLabel)
        {
            this.maxLengthText = Ext.String.format('The maximum length for {0} is {1}', this.fieldLabel, this.maxLength);
        }

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);
        this.mixins.message.constructor.call(this);

        this.onMessagesChangedCallback = this.onMessagesChanged.bind(this);

        this.addListener('change', this.onFormFieldComboBoxChange, this);
        this.addListener('render', this.onFormFieldComboBoxRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldComboBoxChange: function (field, newValue, oldValue)
    {
        this.clearMessages();
    },

    onFormFieldComboBoxPickerRefresh: function (picker)
    {
        var boundList = this.getPicker();
        var node;

        if (this.encodeList)
        {
            Ext.each(boundList.getNodes(), function (node)
            {
                if (Ext.isEmpty(node.innerHTML))
                {
                    node.innerHTML = '&nbsp;';
                }
            }, this);
        }

        if (this.activeField && this.store && this.store.isComputronixExtStore &&
            this.store.getNewModel().fieldsByName.containsKey(this.activeField))
        {
            this.setActiveList();
        }
    },

    onFormFieldComboBoxRender: function (field)
    {
        if (this.parentForm)
        {
            if (typeof this.parentForm == 'string')
            {
                this.parentForm = Ext.getCmp(this.parentForm);
            }
        }
        else
        {
            this.parentForm = Computronix.Ext.getForm(this);
        }

        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.inputEl, { 'text-align': this.align, 'padding-right': '2px' });
        }

        this.setReadOnly(this.readOnly);

        this.getPicker().addListener('refresh', this.onFormFieldComboBoxPickerRefresh, this);
    },

    onMessagesChanged: function ()
    {
        this.isValid();

        if (this.rendered)
        {
            if (this.hasWarningOnly())
            {
                this.bodyEl.dom.setAttribute('data-warningqtip', this.getActiveError());
                this.addCls('warning-background');
                this.inputWrap.addCls('warning-border');
            }
            else
            {
                this.bodyEl.dom.removeAttribute('data-warningqtip');
                this.removeCls('warning-background');
                this.inputWrap.removeCls('warning-border');
            }
        }
    },

    // Methods
    //=============================================================================
    //
    checkTab: function (combobox, event)
    {
        var value = this.getRawValue();

        // Stop the user from leaving the field if the list has not been constructed yet so the value can be matched.
        if (value && !this.readOnly && !this.disabled && this.forceSelection && this.typeAhead &&
            event.getKey() == event.TAB && !this.findRecordByDisplay(value))
        {
            event.stopEvent();
        }
        else
        {
            this.callParent(arguments);
        }
    },

    getActiveError: function ()
    {
        return this.addWarningMessages(this.callParent(arguments));
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);

        if (this.hasErrorMessages())
        {
            result = result.concat(this.getErrorMessages());
        }

        return result;
    },

    isValid: function ()
    {
        var result = true;

        if (this.readOnly)
        {
            this.clearInvalid();
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    renderActiveError: function ()
    {
        if (!this.hasWarningOnly())
        {
            this.callParent(arguments);
        }
    },

    setActiveList: function ()
    {
        var boundList = this.getPicker();
        var node;

        Ext.each(boundList.getNodes(), function (node)
        {
            if (boundList.getRecord(node).get(this.activeField))
            {
                Ext.fly(node).removeCls('cxinactiveitem');
            }
            else
            {
                Ext.fly(node).addCls('cxinactiveitem');
            }
        }, this);
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        if (this.inputEl)
        {
            if (readOnly)
            {
                this.inputEl.addCls('cxreadonlyformtext');
            }
            else
            {
                this.inputEl.removeCls('cxreadonlyformtext');
            }
        }
    },

    setValue: function (value)
    {
        var record;

        if (this.didValueChange(value, this.lastValue) && !this.multiSelect &&
            this.activeField && this.store && this.store.isComputronixExtStore &&
            this.store.getNewModel().fieldsByName.containsKey(this.activeField))
        {
            this.store.removeFilter('activeFilter');
        }

        this.callParent(arguments);

        if (this.didValueChange(value, this.lastValue) && !this.multiSelect &&
            this.activeField && this.store && this.store.isComputronixExtStore &&
            this.store.getNewModel().fieldsByName.containsKey(this.activeField))
        {
            record = this.record || this.parentForm && this.parentForm.getRecord();

            if (record)
            {
                this.store.filter(
                {
                    id: 'activeFilter',
                    filterFn: function (item)
                    {
                        var result = true;
                        var record = this.record || this.parentForm && this.parentForm.getRecord();

                        if (record)
                        {
                            result = item.get(this.activeField) || item.getId() == record.get(this.name);
                        }

                        return result;
                    },
                    scope: this
                });
            }
        }
    }
});

// Computronix date field extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Date',
{
    extend: 'Ext.form.field.Date',
    alias: 'widget.computronixextdatefield',
    mixins:
    {
        mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator',
        microhelp: 'Computronix.Ext.Microhelp',
        message: 'Computronix.Ext.Message'
    },
    format: Computronix.Ext.dateFormat,
    labelClsExtra: 'cxfieldlabel',
    width: 200,
    minValue: new Date(999, 12, 1),
    formatText: '',

    // Data Members
    //=============================================================================
    //
    align: 'left',
    timeValue: null,
    longFormat: 'l F jS, Y',
    toolTip: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.altFormats =
            'd|M|F|Y|M d|d M|F d|d F|Y d|d Y|Y M|M Y|Y F|F Y|Y M d|Y d M|Y F d|Y d F|M d Y|M Y d|F d Y|F Y d|d M Y|d Y M|d F Y|d Y F' +
            '|M-d|d-M|F-d|d-F|Y-d|d-Y|Y-M|M-Y|Y-F|F-Y|Y-M-d|Y-d-M|Y-F-d|Y-d-F|M-d-Y|M-Y-d|F-d-Y|F-Y-d|d-M-Y|d-Y-M|d-F-Y|d-Y-F' +
            '|M/d|d/M|F/d|d/F|Y/d|d/Y|Y/M|M/Y|Y/F|F/Y|Y/M/d|Y/d/M|Y/F/d|Y/d/F|M/d/Y|M/Y/d|F/d/Y|F/Y/d|d/M/Y|d/Y/M|d/F/Y|d/Y/F' +
            '|M.d|d.M|F.d|d.F|Y.d|d.Y|Y.M|M.Y|Y.F|F.Y|Y.M.d|Y.d.M|Y.F.d|Y.d.F|M.d.Y|M.Y.d|F.d.Y|F.Y.d|d.M.Y|d.Y.M|d.F.Y|d.Y.F' +
            '|M\,d|d\,M|F\,d|d\,F|Y\,d|d\,Y|Y\,M|M\,Y|Y\,F|F\,Y|Y\,M\,d|Y\,d\,M|Y\,F\,d|Y\,d\,F|M\,d\,Y|M\,Y\,d|F\,d\,Y|F\,Y\,d|d\,M\,Y|d\,Y\,M|d\,F\,Y|d\,Y\,F' +
            '|M\\\\d|d\\\\M|F\\\\d|d\\\\F|Y\\\\d|d\\\\Y|Y\\\\M|M\\\\Y|Y\\\\F|F\\\\Y|Y\\\\M\\\\d|Y\\\\d\\\\M|Y\\\\F\\\\d|Y\\\\d\\\\F|M\\\\d\\\\Y|M\\\\Y\\\\d|F\\\\d\\\\Y|F\\\\Y\\\\d|d\\\\M\\\\Y|d\\\\Y\\\\M|d\\\\F\\\\Y|d\\\\Y\\\\F' +
            '|M:d|d:M|F:d|d:F|Y:d|d:Y|Y:M|M:Y|Y:F|F:Y|Y:M:d|Y:d:M|Y:F:d|Y:d:F|M:d:Y|M:Y:d|F:d:Y|F:Y:d|d:M:Y|d:Y:M|d:F:Y|d:Y:F' +
            '|j|M j|j M|F j|j F|Y j|j Y|Y M|M Y|Y F|F Y|Y M j|Y j M|Y F j|Y j F|M j Y|M Y j|F j Y|F Y j|j M Y|j Y M|j F Y|j Y F' +
            '|M-j|j-M|F-j|j-F|Y-j|j-Y|Y-M|M-Y|Y-F|F-Y|Y-M-j|Y-j-M|Y-F-j|Y-j-F|M-j-Y|M-Y-j|F-j-Y|F-Y-j|j-M-Y|j-Y-M|j-F-Y|j-Y-F' +
            '|M/j|j/M|F/j|j/F|Y/j|j/Y|Y/M|M/Y|Y/F|F/Y|Y/M/j|Y/j/M|Y/F/j|Y/j/F|M/j/Y|M/Y/j|F/j/Y|F/Y/j|j/M/Y|j/Y/M|j/F/Y|j/Y/F' +
            '|M.j|j.M|F.j|j.F|Y.j|j.Y|Y.M|M.Y|Y.F|F.Y|Y.M.j|Y.j.M|Y.F.j|Y.j.F|M.j.Y|M.Y.j|F.j.Y|F.Y.j|j.M.Y|j.Y.M|j.F.Y|j.Y.F' +
            '|M\,j|j\,M|F\,j|j\,F|Y\,j|j\,Y|Y\,M|M\,Y|Y\,F|F\,Y|Y\,M\,j|Y\,j\,M|Y\,F\,j|Y\,j\,F|M\,j\,Y|M\,Y\,j|F\,j\,Y|F\,Y\,j|j\,M\,Y|j\,Y\,M|j\,F\,Y|j\,Y\,F' +
            '|M\\\\j|j\\\\M|F\\\\j|j\\\\F|Y\\\\j|j\\\\Y|Y\\\\M|M\\\\Y|Y\\\\F|F\\\\Y|Y\\\\M\\\\j|Y\\\\j\\\\M|Y\\\\F\\\\j|Y\\\\j\\\\F|M\\\\j\\\\Y|M\\\\Y\\\\j|F\\\\j\\\\Y|F\\\\Y\\\\j|j\\\\M\\\\Y|j\\\\Y\\\\M|j\\\\F\\\\Y|j\\\\Y\\\\F' +
            '|M:j|j:M|F:j|j:F|Y:j|j:Y|Y:M|M:Y|Y:F|F:Y|Y:M:j|Y:j:M|Y:F:j|Y:j:F|M:j:Y|M:Y:j|F:j:Y|F:Y:j|j:M:Y|j:Y:M|j:F:Y|j:Y:F';

        if (this.format && (this.format.substr(0, 1).toLowerCase() == 'd' || this.format.substr(0, 1).toLowerCase() == 'j'))
        {
            this.altFormats += '|d m|j n|d-m|j-n|d/m|j/n|d.m|j.n|d\,m|j\,n|d\\\\m|j\\\\n|d:m|j:n|d m Y|j n Y|d-m-Y|j-n-Y|d/m/Y|j/n/Y|d.m.Y|j.n.Y|d\,m\,Y|j\,n\,Y|d\\\\m\\\\Y|j\\\\n\\\\Y|d:m:Y|j:n:Y|Y d m|Y j n|Y-d-m|Y-j-n|Y/d/m|Y/j/n|Y.d.m|Y.j.n|Y\,d\,m|Y\,j\,n|Y\\\\d\\\\m|Y\\\\j\\\\n|Y:d:m|Y:j:n';
        }
        else
        {
            this.altFormats += '|m d|n j|m-d|n-j|m/d|n/j|m.d|n.j|m\,d|n\,j|m\\\\d|n\\\\j|m:d|n:j|m d Y|n j Y|m-d-Y|n-j-Y|m/d/Y|n/j/Y|m.d.Y|n.j.Y|m\,d\,Y|n\,j\,Y|m\\\\d\\\\Y|n\\\\j\\\\Y|m:d:Y|n:j:Y|Y m d|Y n j|Y-m-d|Y-n-j|Y/m/d|Y/n/j|Y.m.d|Y.n.j|Y\,m\,d|Y\,n\,j|Y\\\\m\\\\d|Y\\\\n\\\\j|Y:m:d|Y:n:j';
        }

        if (!this.emptyText)
        {
            this.emptyText = 'Mmm d, yyyy';
        }

        if (this.blankText == 'This field is required' && this.fieldLabel)
        {
            this.blankText = Ext.String.format('{0} is required', this.fieldLabel);
        }

        if (this.invalidText == '{0} is not a valid date - it must be in the format {1}')
        {
            this.invalidText = Ext.String.format('The value for {0} is not a valid date - it must be in the format {1}', this.fieldLabel, this.emptyText)
        }

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);
        this.mixins.message.constructor.call(this);

        this.onMessagesChangedCallback = this.onMessagesChanged.bind(this);

        this.addListener('change', this.onFormFieldDateChange, this);
        this.addListener('expand', this.onFormFieldDateExpand, this);
        this.addListener('render', this.onFormFieldDateRender, this);
        this.addListener('select', this.onFormFieldDateSelect, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldDateChange: function (field, newValue, oldValue)
    {
        this.clearMessages();
    },

    onFormFieldDateRender: function (field)
    {
        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.inputEl, { 'text-align': this.align, 'padding-right': '2px' });
        }

        this.setReadOnly(this.readOnly);

        this.toolTip = new Ext.tip.ToolTip(
        {
            target: this.triggerWrap,
        });

        this.toolTip.addListener('beforeshow', this.onToolTipBeforeShow, this);
    },

    onFormFieldDateExpand: function (field)
    {
        if (this.getValue())
        {
            this.timeValue = Ext.Date.format(this.getValue(), 'H:i:s')
        }
        else
        {
            this.timeValue = null;
        }
    },

    onFormFieldDateSelect: function (field, value)
    {
        if (this.timeValue && Ext.Date.format(value, 'H:i:s') != this.timeValue)
        {
            this.setValue(Ext.Date.parse(Ext.String.format('{0} {1}', Ext.Date.format(value, 'Y-n-j'), this.timeValue), 'Y-n-j H:i:s'));
        }
    },

    onMessagesChanged: function ()
    {
        this.isValid();

        if (this.rendered)
        {
            if (this.hasWarningOnly())
            {
                this.bodyEl.dom.setAttribute('data-warningqtip', this.getActiveError());
                this.addCls('warning-background');
                this.inputWrap.addCls('warning-border');
            }
            else
            {
                this.bodyEl.dom.removeAttribute('data-warningqtip');
                this.removeCls('warning-background');
                this.inputWrap.removeCls('warning-border');
            }
        }
    },

    onToolTipBeforeShow: function (toolTip)
    {
        var result = false;

        if (this.getEl() && this.getValue())
        {
            this.toolTip.update(Ext.Date.format(this.getValue(), this.longFormat));
            result = true;
        }

        return result;
    },

    // Methods
    //=============================================================================
    //
    getActiveError: function ()
    {
        return this.addWarningMessages(this.callParent(arguments));
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);

        if (this.hasErrorMessages())
        {
            result = result.concat(this.getErrorMessages());
        }

        return result;
    },

    isValid: function ()
    {
        var result = true;

        if (this.readOnly)
        {
            this.clearInvalid();
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    renderActiveError: function ()
    {
        if (!this.hasWarningOnly())
        {
            this.callParent(arguments);
        }
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        if (this.inputEl)
        {
            if (readOnly)
            {
                this.inputEl.addCls('cxreadonlyformtext');
            }
            else
            {
                this.inputEl.removeCls('cxreadonlyformtext');
            }
        }
    }
});

// Computronix display field extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Display',
{
    extend: 'Ext.form.field.Display',
    alias: 'widget.computronixextdisplayfield',
    mixins: { mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator', microhelp: 'Computronix.Ext.Microhelp' },
    labelClsExtra: 'cxfieldlabel',
    fieldBodyCls: 'cxdisplayfieldbody',

    // Data Members
    //=============================================================================
    //
    parentForm: null,
    record: null,
    align: 'left',
    format: null,
    colorClsField: null,
    encodeValue: false,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.htmlEncode = this.encodeValue;
        this.callParent();

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);

        this.addListener('afterrender', this.onFormFieldDisplayAfterRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldDisplayAfterRender: function (field)
    {
        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.inputEl, { 'text-align': this.align, 'width': this.bodyEl.dom.style.width });
        }

        if (this.parentForm)
        {
            if (typeof this.parentForm == 'string')
            {
                this.parentForm = Ext.getCmp(this.parentForm);
            }
        }
        else
        {
            this.parentForm = Computronix.Ext.getForm(this);
        }
    },

    // Methods
    //=============================================================================
    //
    clearColor: function ()
    {
        var className;
        var color = /^cxcolor/;

        if (this.inputEl && this.colorClsField)
        {
            className = this.inputEl.dom.className;

            Ext.each(className.split(' '), function (cls)
            {
                if (color.test(cls))
                {
                    this.inputEl.removeCls(cls);
                }
            }, this);
        }
    },

    getFormattedValue: function (value)
    {
        var result = value;
        var formattedChars;
        var index = 0;

        if (value && this.format)
        {
            formattedChars = [];
            value = value.toString();

            Ext.each(this.format.split(''), function (char)
            {
                if (char == '@')
                {
                    formattedChars.push(value.substr(index, 1));
                    index++;
                }
                else
                {
                    formattedChars.push(char)
                }
            }, this);

            result = formattedChars.join('');

            if (index < value.length)
            {
                result += value.substr(index);
            }
        }

        return result;
    },

    // Override the default processing to ignore what was actually rendered into the display field.
    getValue: function ()
    {
        return this.value;
    },

    setColor: function (value)
    {
        if (this.inputEl && value && this.colorClsField && (this.record || this.parentForm && this.parentForm.getRecord()))
        {
            this.clearColor();
            this.inputEl.addCls((this.record || this.parentForm.getRecord()).get(this.colorClsField));
        }
    },

    setValue: function (value)
    {
        var className;
        var color = /^cxcolor/;

        this.callParent(arguments);

        if (this.inputEl && value)
        {
            if (this.colorClsField && (this.record || this.parentForm && this.parentForm.getRecord()))
            {
                this.setColor(value);
            }

            if (this.format)
            {
                value = this.getFormattedValue(value);
            }

            if (this.encodeValue)
            {
                value = Ext.htmlEncode(value);
                value = value.replace(/&lt;br\/&gt;/g, '<br/>');
            }

            value = value.toString().replace(/\n/g, '<br/>');
            // By replacing pairs of blanks we perserve whitespace without stopping word wrapping.
            // This must be executed twice to catch any pairs created by the first replace.
            value = value.toString().replace(/\s\s/g, '&nbsp;&nbsp; ');
            value = value.toString().replace(/\s\s/g, '&nbsp;&nbsp; ');

            this.inputEl.update(value);
        }
    }
});

// Computronix file upload field extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.File',
{
    extend: 'Ext.form.field.File',
    alias: 'widget.computronixextfilefield',
    buttonOnly: true,
    buttonText: null,
    buttonIconCls: 'cxuploadicon',

    // Data Members
    //=============================================================================
    //
    allowMultiple: false,
    accept: null,
    automaticUpload: true,
    currentAjaxBatchNumber: 0,
    disableWhileUploading: true,
    isUploading: false,
    _mask: null,
    title: null,
    focusable: false,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.addListener('change', this.onFormFieldFileChange, this);
        this.addListener('render', this.onFormFieldFileRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldFileChange: function (fileField, fileName)
    {
        if (this.automaticUpload)
        {
            this.uploadFiles(this.fileInputEl.dom.files, fileName);
        }
    },

    onFormFieldFileRender: function (field)
    {
        this.setupInput();
        // In ExtJS v6.0.1 disabling the button causes an error when setting the aria attribute.
        // Setting the role to null stops the attribute from being set.
        this.button.ariaRole = null;
        //this.button.ariaLabel = 'Upload';
        this.button.setIconCls(this.buttonIconCls);
    },

    onFormFieldFileSubmitComplete: function (form, action)
    {
        var size = this.getSize();

        this.isUploading = false;
        this.setMasked(false);
        this.fileInputEl.setSize(size.width, size.height);
        this.fireEvent('submitcomplete', this, action.response, action, true);
    },

    onFormFieldFileSubmitFailure: function (form, action)
    {
        var size = this.getSize();

        this.isUploading = false;
        this.setMasked(false);
        this.fileInputEl.setSize(size.width, size.height);
        this.fireEvent('submitcomplete', this, action.response, action, false);
    },

    onFormFieldFileUploadBatchComplete: function (ajaxBatchId, success)
    {
        this.isUploading = false;
        this.enable();
        this.fireEvent('uploadbatchcomplete', this, ajaxBatchId, success);
    },

    onFormFieldFileUploadComplete: function (response, options, file, success)
    {
        this.fireEvent('uploadcomplete', this, response, options, file, success);
    },

    onFormFieldFileUploadProgress: function (file, event)
    {
        this.fireEvent('uploadprogress', this, event.loaded, event.total, file);
    },

    reset: function ()
    {
        this.callParent(arguments);

        // The reset() function actually creates a new file input, so make sure
        // it's all set up again.
        this.setupInput();
    },

    // Methods
    //=============================================================================
    //
    adjustInput: function ()
    {
        var size;

        // Corrects the size of the input.
        if (this.fileInputEl)
        {
            size = this.getSize();

            if (size.width == 0)
            {
                this.fileInputEl.setStyle({ "font-size": '20px', top: "0px", left: "0px", width: "20px", height: "20px" });
            }
            else if (size.width < 20)
            {
                this.fileInputEl.setStyle({ "font-size": '20px', top: "0px", left: "0px", width: "16px", height: "14px" });
            }
            else
            {
                this.fileInputEl.setStyle({ "font-size": '20px', top: "0px", left: "0px", width: size.width + 'px', height: size.height + 'px' });
            }
        }
    },

    setupInput: function ()
    {
        this.adjustInput();

        if (this.allowMultiple)
        {
            this.fileInputEl.set({ multiple: 'multiple' });
        }

        if (this.accept)
        {
            this.fileInputEl.set({ accept: this.accept });
        }

        if (this.title)
        {
            this.fileInputEl.set({ title: this.title });
        }
    },

    setValue: function ()
    {
        this.callParent(arguments);
        this.adjustInput();
    },

    uploadFiles: function (files, fileName, suppressErrors)
    {
        var ajaxBatchId;
        var form;

        if (files && files.length > 0)
        {
            this.isUploading = true;

            if (this.disableWhileUploading)
            {
                this.disable();
            }

            ajaxBatchId = Ext.String.format('uploadFiles{0}{1}', this.name, this.currentAjaxBatchNumber);
            this.currentAjaxBatchNumber++;

            Computronix.Ext.Ajax.uploadFiles(files, this.onFormFieldFileUploadProgress,
                this.onFormFieldFileUploadComplete, this.onFormFieldFileUploadBatchComplete, this, ajaxBatchId, suppressErrors);
        }
        else
        {
            if (fileName)
            {
                this.isUploading = true;

                this.setMasked(true);

                this.up('form').getForm().submit(
                {
                    url: Computronix.Ext.uploadUrl,
                    clientValidation: false,
                    success: this.onFormFieldFileSubmitComplete,
                    failure: this.onFormFieldFileSubmitFailure,
                    scope: this
                });
            }
        }
    }
});

// Computronix image extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Image',
{
    extend: 'Ext.form.field.Display',
    alias: 'widget.computronixextimage',
    mixins: { mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator', microhelp: 'Computronix.Ext.Microhelp' },
    labelClsExtra: 'cxfieldlabel',

    // Data Members
    //=============================================================================
    //
    parentForm: null,
    record: null,
    tooltip: 'Click to Enlarge',
    target: '_blank',
    thumbField: null,
    imageWidth: 24,
    imageHeight: 24,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);

        this.addListener('afterrender', this.onFormFieldImageAfterRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldImageAfterRender: function (component)
    {
        if (this.parentForm)
        {
            if (typeof this.parentForm == 'string')
            {
                this.parentForm = Ext.getCmp(this.parentForm);
            }
        }
        else
        {
            this.parentForm = Computronix.Ext.getForm(this);
        }

        if (this.value)
        {
            this.setValue(this.value);
        }
    },

    // Methods
    //=============================================================================
    //
    getMarkup: function (value, record)
    {
        var result = null;

        if (record)
        {
            if (value)
            {
                result = Ext.String.format(
                    '<a href="{0}" target="{1}" data-qtip="{2}" onContextMenu="event.cancelBubble=true;"><img width="{3}px" height="{4}px" src="{5}"/></a>',
                    value, this.target, this.tooltip, this.imageWidth, this.imageHeight,
                    (record && this.thumbField && record.get(this.thumbField)) ? record.get(this.thumbField) : value);
            }
        }

        return result;
    },

    setValue: function (value)
    {
        this.callParent(arguments);

        if (this.inputEl)
        {
            this.inputEl.update(this.getMarkup(value,
                (this.record || this.parentForm && this.parentForm.getRecord()) ? this.record || this.parentForm.getRecord() : null));
        }
    }
});

// Computronix link extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Link',
{
    extend: 'Ext.form.field.Display',
    alias: 'widget.computronixextlink',
    mixins: { mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator', microhelp: 'Computronix.Ext.Microhelp' },
    labelClsExtra: 'cxfieldlabel',
    fieldCls: 'cxdisplayfield x-form-display-field',
    fieldBodyCls: 'cxdisplayfieldbody',
    fieldLabel: ' ',
    width: 200,

    // Data Members
    //=============================================================================
    //
    parentForm: null,
    record: null,
    tooltip: 'Click to open in a new window',
    iconCls: null,
    target: '_blank',
    displayValueField: null,
    iconClsField: null,
    accessLevelField: null,
    align: 'left',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        if (Ext.isEmpty(this.tooltip))
        {
            this.tooltip = '';
        }

        this.accessLevelField = Ext.String.format('{0}$AccessLevel', this.name);

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);

        this.addListener('afterrender', this.onFormFieldLinkAfterRender, this);
        this.addListener('resize', this.onFormFieldLinkResize, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldLinkAfterRender: function (component)
    {
        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.inputEl, { 'text-align': this.align, 'width': this.bodyEl.dom.style.width });
        }

        if (this.parentForm)
        {
            if (typeof this.parentForm == 'string')
            {
                this.parentForm = Ext.getCmp(this.parentForm);
            }
        }
        else
        {
            this.parentForm = Computronix.Ext.getForm(this);
        }

        if (this.value)
        {
            this.setValue(this.value);
        }
    },

    onFormFieldLinkResize: function (field, width)
    {
        if (this.inputEl)
        {
            if (this.fieldLabel)
            {
                Ext.DomHelper.applyStyles(this.inputEl, { 'width': Ext.String.format('{0}px', width - this.labelWidth - this.labelPad - 6) });
            }
            else
            {
                Ext.DomHelper.applyStyles(this.inputEl, { 'width': Ext.String.format('{0}px', width - this.labelPad - 1) });
            }
        }
    },

    // Methods
    //=============================================================================
    //
    getMarkup: function (value, record)
    {
        var result = null;

        if (record && record.get(this.accessLevelField) != 'Invisible')
        {
            if (value && record.get(this.accessLevelField) != 'Disabled')
            {
                if (record.get(this.iconClsField) || this.iconCls)
                {
                    result = Ext.String.format(
                        '<a href="{0}" target="{1}" class="cxlink" data-qtip="{2}" onContextMenu="event.cancelBubble=true;"><img class="cxicon {4}" style="margin: 0px 4px 0px 0px" src="Images/Blank.gif"/><span class="cxlink">{3}</span></a>',
                        value, this.target, this.tooltip, this.displayValueField ? record.get(this.displayValueField) : value,
                        record.get(this.iconClsField) || this.iconCls);
                }
                else
                {
                    result = Ext.String.format(
                        '<a href="{0}" target="{1}" class="cxlink" data-qtip="{2}" onContextMenu="event.cancelBubble=true;">{3}</a>',
                        value, this.target, this.tooltip, this.displayValueField ? record.get(this.displayValueField) : value);
                }
            }
            else
            {
                if (record.get(this.iconClsField) || this.iconCls)
                {
                    result = Ext.String.format(
                        '<span class="cxlink" data-qtip="{0}"><img class="cxicon {2}" style="margin: 0px 4px 0px 0px" src="Images/Blank.gif"/><span class="cxlink">{1}</span></span>',
                        this.tooltip, this.displayValueField ? record.get(this.displayValueField) : '',
                        record.get(this.iconClsField) || this.iconCls);
                }
                else
                {
                    if (this.displayValueField)
                    {
                        result = Ext.String.format('<span class="cxlink" data-qtip="{0}">{1}</span>',
                            this.tooltip, record.get(this.displayValueField));
                    }
                }
            }
        }

        return result;
    },

    setValue: function (value)
    {
        this.callParent(arguments);

        if (this.inputEl)
        {
            this.inputEl.update(this.getMarkup(value,
                (this.record || this.parentForm && this.parentForm.getRecord()) ? this.record || this.parentForm.getRecord() : null));
        }
    }
});

// Computronix number field extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Number',
{
    extend: 'Ext.form.field.Number',
    alias: 'widget.computronixextnumberfield',
    mixins:
    {
        mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator',
        microhelp: 'Computronix.Ext.Microhelp',
        message: 'Computronix.Ext.Message'
    },
    validateOnBlur: false,
    stripCharsRe: /[\$,%]/g,
    labelClsExtra: 'cxfieldlabel',
    width: 200,

    // Data Members
    //=============================================================================
    //
    parentForm: null,
    record: null,
    align: 'right',
    stepText: 'The value for this field must be a multiple of {0}',
    tooManySignificantDigitsText:
        'The value for this field may not have more than 14 total digits',
    enforceStep: false,
    format: '0,000.00',
    colorClsField: null,
    isUnconstrained: false,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        var index;
        var decimals;

        this.callParent();

        if (!this.emptyText && this.fieldLabel)
        {
            this.emptyText = Ext.String.format('Enter a {0}...', this.fieldLabel);
        }

        if (this.blankText == 'This field is required' && this.fieldLabel)
        {
            this.blankText = Ext.String.format('{0} is required', this.fieldLabel);
        }

        if (this.minLengthText == 'The minimum length for this field is {0}' && this.fieldLabel)
        {
            this.minLengthText = Ext.String.format('The minimum length for {0} is {1}', this.fieldLabel, this.minLength);
        }

        if (this.maxLengthText == 'The maximum length for this field is {0}' && this.fieldLabel)
        {
            this.maxLengthText = Ext.String.format('The maximum length for {0} is {1}', this.fieldLabel, this.maxLength);
        }

        if (this.minText == 'The minimum value for this field is {0}' && this.fieldLabel)
        {
            this.minText = Ext.String.format('The minimum value for {0} is {1}', this.fieldLabel, this.minValue);
        }

        if (this.maxText == 'The maximum value for this field is {0}' && this.fieldLabel)
        {
            this.maxText = Ext.String.format('The maximum value for {0} is {1}', this.fieldLabel, this.maxValue);
        }

        if (this.stepText == 'The value for this field must be a multiple of {0}' && this.fieldLabel)
        {
            this.stepText = Ext.String.format('The value for {0} must be a multiple of {1}', this.fieldLabel, this.step);
        }

        if (this.tooManySignificantDigitsText ==
                'The value for this field may not have more than 14 total digits'
                && this.fieldLabel)
        {
            this.tooManySignificantDigitsText = Ext.String.format(
                'The value for {0} may not have more than 14 total digits',
                this.fieldLabel);
        }

        if (this.format && this.format.substr(this.format.length - 1, 1) == '0')
        {
            // Enforce the specified decimal precision in the display format.
            index = this.format.indexOf(this.decimalSeparator);

            if (index == -1)
            {
                if (this.decimalPrecision > 0)
                {
                    decimals = [];

                    for (var _index = 0; _index < this.decimalPrecision; _index++)
                    {
                        decimals.push('0');
                    }

                    this.format = Ext.String.format('{0}{1}{2}', this.format, this.decimalSeparator, decimals.join(''));
                }
            }
            else
            {
                if (this.decimalPrecision > 0)
                {
                    if (this.format.substr(index + 1).length != this.decimalPrecision)
                    {
                        decimals = [];

                        for (var _index = 0; _index < this.decimalPrecision; _index++)
                        {
                            decimals.push('0');
                        }

                        this.format = Ext.String.format('{0}{1}', this.format.substring(0, index + 1), decimals.join(''));
                    }
                }
                else
                {
                    this.format = this.format.substring(0, index);
                }
            }
        }

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);
        this.mixins.message.constructor.call(this);

        this.onMessagesChangedCallback = this.onMessagesChanged.bind(this);

        this.addListener('change', this.onFormFieldNumberChange, this);
        this.addListener('render', this.onFormFieldNumberRender, this);
        this.addListener('spin', this.onFormFieldNumberSpin, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldNumberChange: function (field, newValue, oldValue)
    {
        if (newValue == null)
        {
            this.clearColor();
        }

        this.clearMessages();
    },

    onFormFieldNumberRender: function (field)
    {
        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.inputEl, { 'text-align': this.align, 'padding-right': '2px' });
        }

        if (this.parentForm)
        {
            if (typeof this.parentForm == 'string')
            {
                this.parentForm = Ext.getCmp(this.parentForm);
            }
        }
        else
        {
            this.parentForm = Computronix.Ext.getForm(this);
        }

        this.setReadOnly(this.readOnly);
    },

    onFormFieldNumberSpin: function (field, direction)
    {
        var _value;
        var precision;

        if (this.step > 0)
        {
            _value = parseFloat(this.getValue() || 0);
            precision = Math.pow(10, this.decimalPrecision);

            if ((Math.round(_value * precision) % Math.round(this.step * precision)) != 0)
            {

                if (direction == 'up')
                {
                    _value = (Math.floor((Math.round(_value * precision) / Math.round(this.step * precision))) *
                            (this.step * precision)) / precision;
                }
                else
                {
                    _value = (Math.ceil((Math.round(_value * precision) / Math.round(this.step * precision))) *
                            (this.step * precision)) / precision;
                }

                this.setValue(_value);
            }
        }
    },

    onMessagesChanged: function ()
    {
        this.isValid();

        if (this.rendered)
        {
            if (this.hasWarningOnly())
            {
                this.bodyEl.dom.setAttribute('data-warningqtip', this.getActiveError());
                this.addCls('warning-background');
                this.inputWrap.addCls('warning-border');
            }
            else
            {
                this.bodyEl.dom.removeAttribute('data-warningqtip');
                this.removeCls('warning-background');
                this.inputWrap.removeCls('warning-border');
            }
        }
    },

    // Methods
    //=============================================================================
    //
    clearColor: function ()
    {
        var className;
        var color = /^cxcolor/;

        if (this.inputEl && this.colorClsField)
        {
            className = this.inputEl.dom.className;

            Ext.each(className.split(' '), function (cls)
            {
                if (color.test(cls))
                {
                    this.inputEl.removeCls(cls);
                }
            }, this);
        }
    },

    getActiveError: function ()
    {
        return this.addWarningMessages(this.callParent(arguments));
    },

    getErrors: function (value)
    {
        // Note: value may be either string or numeric. When called directly
        // from validateValue, value is passed using getRawValue() and is a
        // string.  When there are errors validateValue will mark invalid errors:
        //   markInvalid -> setActiveErrors -> renderActiveError -> hasWarningOnly
        // hasWarningOnly calls getErrors again using getValue() which returns
        // numeric for valid numbers, string otherwise.

        // If the value is null the base code fails.
        var result = this.callParent([value || '']);
        var tooManySignificantDigits = false;
        var tempStr, tempExp, exponentIndex;
        var decimalIndex, intPartLen, decPartLen;

        if (value && !isNaN(value.toString()))
        {
            if (this.isUnconstrained)
            {
                tempStr = value.toString();
                exponentIndex = tempStr.indexOf("e");

                if (exponentIndex > -1 || tempStr.length > 14)
                {

                    if (exponentIndex > -1)
                    {
                        tempExp = parseInt(tempStr.substr(exponentIndex + 1));
                        tempStr = tempStr.substring(0, exponentIndex);
                    }

                    tempStr = tempStr.replace("-", "");

                    // Strip off leading zeros
                    while (tempStr.substr(0, 1) == '0' && tempStr.length > 1)
                    {
                        tempStr = tempStr.substr(1);
                    }

                    // Strip off trailing zeros for decimals
                    if (tempStr.indexOf(".") > -1)
                    {
                        while (tempStr.substr(tempStr.length - 1) == '0')
                        {
                            tempStr = tempStr.substr(0, tempStr.length - 1);
                        }
                    }

                    if (exponentIndex == -1)
                    {
                        tempStr = tempStr.replace(".", "");

                        if (tempStr.length > 14)
                        {
                            result.push(this.tooManySignificantDigitsText);
                            tooManySignificantDigits = true;
                        }
                    }
                    else if (tempStr == "." || tempStr == "" || tempStr == "0")
                    {
                        // The string will evaluate to zero so don't check further.
                    }
                    else
                    {
                        decimalIndex = tempStr.indexOf(".");

                        if (decimalIndex > -1)
                        {
                            intPartLen = decimalIndex;
                            decPartLen = tempStr.length - decimalIndex - 1;
                        }
                        else
                        {
                            intPartLen = tempStr.length;
                            decPartLen = 0;
                        }

                        if (intPartLen + decPartLen +
                                Math.max(tempExp - decPartLen, 0) +
                                Math.max(-tempExp - intPartLen, 0) > 14)
                        {
                            result.push(this.tooManySignificantDigitsText);
                            tooManySignificantDigits = true;
                        }
                    }
                }
            }

            if (!tooManySignificantDigits && this.enforceStep && this.step > 0 &&
                (Math.round(value * Math.pow(10, this.decimalPrecision)) % Math.round(this.step * Math.pow(10, this.decimalPrecision))) != 0)
            {
                result.push(Ext.String.format(this.stepText, this.step));
            }
        }

        if (this.hasErrorMessages())
        {
            result = result.concat(this.getErrorMessages());
        }

        return result;
    },

    getFormattedValue: function (value)
    {
        var result;
        var index;
        var format;
        var decimals;
        var intPart;

        if (!Ext.isEmpty(value) && this.format)
        {
            var result = value.toString();

            if (this.format.substr(this.format.length - 2) == '.#')
            {
                format = this.format.substr(0, this.format.length - 2);
                index = result.indexOf('.');

                if (index == -1)
                {
                    result = Ext.util.Format.number(parseFloat(result), format);
                }
                else
                {
                    decimals = result.substr(index + 1);
                    intPart = parseFloat(result.substr(0, index));
                    result = Ext.util.Format.number(intPart, format);

                    if (intPart == 0 && value < 0)
                    {
                        result = Ext.String.format('-{0}', result);
                    }

                    result = Ext.String.format('{0}{1}{2}', result, this.decimalSeparator, decimals);
                }
            }
            else
            {
                result = Ext.util.Format.number(parseFloat(result), this.format);
            }
        }

        return result;
    },

    isValid: function ()
    {
        var result = true;

        if (this.readOnly)
        {
            this.clearInvalid();
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    parseValue: function (value)
    {
        if (value && this.stripCharsRe)
        {
            value = value.toString().replace(this.stripCharsRe, '');
        }

        return this.callParent([value]);
    },

    processRawValue: function (value)
    {
        var newValue;

        if (value && this.stripCharsRe)
        {
            newValue = value.toString().replace(this.stripCharsRe, '');

            if (newValue !== value)
            {
                this.rawValue = newValue;
                value = newValue;
            }
        }

        return value;
    },

    renderActiveError: function ()
    {
        if (!this.hasWarningOnly())
        {
            this.callParent(arguments);
        }
    },

    setColor: function (value)
    {
        if (this.inputEl && !Ext.isEmpty(value) && this.colorClsField && (this.record || this.parentForm && this.parentForm.getRecord()))
        {
            this.clearColor();

            this.inputEl.addCls((this.record || this.parentForm.getRecord()).get(this.colorClsField));
        }
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        if (this.inputEl)
        {
            if (readOnly)
            {
                this.inputEl.addCls('cxreadonlyformtext');
            }
            else
            {
                this.inputEl.removeCls('cxreadonlyformtext');
            }
        }
    },

    setValue: function (value)
    {
        this.callParent(arguments);

        if (this.inputEl && !Ext.isEmpty(value) && this.colorClsField && (this.record || this.parentForm && this.parentForm.getRecord()))
        {
            this.setColor(value);
        }

        if (this.inputEl && !Ext.isEmpty(value) && this.format)
        {
            this.inputEl.dom.value = this.getFormattedValue(value);
            this.isValid();
        }
    }
});

// Computronix radiobutton extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Radio',
{
    extend: 'Ext.form.field.Radio',
    alias: 'widget.computronixextradio',
    mixins: { mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator', microhelp: 'Computronix.Ext.Microhelp' },
    boxLabel: null,

    // Data Members
    //=============================================================================
    //
    readOnlyRadio: null,
    readOnlyRadioChecked: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);

        this.addListener('render', this.onFormFieldRadioRender, this);
        this.addListener('destroy', this.onFormFieldRadioDestroy, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldRadioDestroy: function (field)
    {
        if (this.readOnlyRadio)
        {
            this.readOnlyRadio.destroy();
        }

        if (this.readOnlyRadioChecked)
        {
            this.readOnlyRadioChecked.destroy();
        }
    },

    onFormFieldRadioRender: function (field)
    {
        this.inputEl.setVisibilityMode(Ext.Element.DISPLAY);

        this.readOnlyRadio = Ext.DomHelper.insertAfter(this.inputEl, { tag: 'div', id: this.inputEl.id + 'ro', cls: 'cxreadonlyformradio', html: '&nbsp;' }, true);
        this.readOnlyRadio.setVisibilityMode(Ext.Element.DISPLAY);

        this.readOnlyRadioChecked = Ext.DomHelper.insertAfter(this.inputEl,
            { tag: 'div', id: this.inputEl.id + 'roc', cls: 'cxreadonlyformradiochecked', html: '&nbsp;' }, true);
        this.readOnlyRadioChecked.setVisibilityMode(Ext.Element.DISPLAY);

        this.setReadOnly(this.readOnly);
    },

    // Methods
    //=============================================================================
    //
    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        if (this.readOnlyRadio && this.readOnlyRadioChecked)
        {
            if (this.displayEl)
            {
                if (readOnly)
                {
                    this.displayEl.setVisible(false);

                    if (this.checked)
                    {
                        this.readOnlyRadio.setVisible(false);
                        this.readOnlyRadioChecked.setVisible(true);
                    }
                    else
                    {
                        this.readOnlyRadio.setVisible(true);
                        this.readOnlyRadioChecked.setVisible(false);
                    }
                }
                else
                {
                    this.displayEl.setVisible(true);
                    this.readOnlyRadio.setVisible(false);
                    this.readOnlyRadioChecked.setVisible(false);
                }
            }
        }
    },

    setValue: function (value)
    {
        this.callParent(arguments);

        if (this.readOnlyRadio && this.readOnlyRadioChecked)
        {
            if (this.readOnly)
            {
                if (this.checked)
                {
                    this.readOnlyRadio.setVisible(false);
                    this.readOnlyRadioChecked.setVisible(true);
                }
                else
                {
                    this.readOnlyRadio.setVisible(true);
                    this.readOnlyRadioChecked.setVisible(false);
                }
            }
        }
    }
});

// Computronix radiobutton group extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.RadioGroup',
{
    extend: 'Ext.form.RadioGroup',
    alias: 'widget.computronixextradiogroup',
    mixins: { mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator', microhelp: 'Computronix.Ext.Microhelp' },
    width: 200,
    height: 22,
    vertical: true,
    fieldBodyCls: 'cxfieldcontainer',
    labelClsExtra: 'cxfieldlabel',

    // Data Members
    //=============================================================================
    //
    store: null,
    valueField: null,
    displayField: 'text',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        // Configure a default store.
        if (this.store)
        {
            this.store = Ext.getStore(this.store);
        }
        else
        {
            if (Ext.data.StoreManager.containsKey(this.name + 'Lookup'))
            {
                this.store = Ext.getStore(this.name + 'Lookup');
            }
        }

        if (this.store)
        {
            if (!this.valueField)
            {
                this.valueField = this.store.getNewModel().idProperty;
            }

            if (this.displayField == 'text')
            {
                this.displayField = this.store.getNewModel().getDisplayField();
            }
        }

        this.callParent();

        if (this.blankText == 'You must select one item in this group' && this.fieldLabel)
        {
            this.blankText = Ext.String.format('You must select one {0}', this.fieldLabel);
        }

        if (this.store)
        {
            this.store.each(function (record)
            {
                this.add(Ext.create('Computronix.Ext.form.field.Radio',
                {
                    name: this.name,
                    inputValue: record.get(this.valueField),
                    boxLabel: record.get(this.displayField)
                }));
            }, this);
        }

        this.items.each(function (item)
        {
            if (!item.helpText)
            {
                item.helpText = this.helpText;
            }
        }, this);

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);
    },

    // Methods
    //=============================================================================
    //
    focus: function (selectText, delay)
    {
        this.callParent(arguments);

        if (this.items && this.items.getCount() > 0)
        {
            this.items.first().focus(selectText, delay);
        }
    },

    isValid: function ()
    {
        var result = true;

        if (this.readOnly)
        {
            this.clearInvalid();
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    setValue: function (value)
    {
        this.eachBox(function (radio)
        {
            if (radio.inputValue == value)
            {
                radio.setValue(value);
            }
            else
            {
                // Clears the radio buttons if the value does not match any of them.
                radio.setValue(false);
            }
        });
    }
});

// Computronix text field extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Text',
{
    extend: 'Ext.form.field.Text',
    alias: 'widget.computronixexttextfield',
    mixins:
    {
        mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator',
        microhelp: 'Computronix.Ext.Microhelp',
        message: 'Computronix.Ext.Message'
    },

    labelClsExtra: 'cxfieldlabel',
    selectOnFocus: true,
    width: 200,

    // Data Members
    //=============================================================================
    //
    parentForm: null,
    record: null,
    align: 'left',
    dataCase: null,
    format: null,
    formatText: 'The value for this field must fill the format {0}',
    enforceFormat: false,
    colorClsField: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        var stripChars;

        this.callParent();

        if (!this.emptyText && this.fieldLabel && this.inputType != 'password')
        {
            this.emptyText = Ext.String.format('Enter a {0}...', this.fieldLabel);
        }

        if (this.blankText == 'This field is required' && this.fieldLabel)
        {
            this.blankText = Ext.String.format('{0} is required', this.fieldLabel);
        }

        if (this.minLengthText == 'The minimum length for this field is {0}' && this.fieldLabel)
        {
            this.minLengthText = Ext.String.format('The minimum length for {0} is {1}', this.fieldLabel, this.minLength);
        }

        if (this.maxLengthText == 'The maximum length for this field is {0}' && this.fieldLabel)
        {
            this.maxLengthText = Ext.String.format('The maximum length for {0} is {1}', this.fieldLabel, this.maxLength);
        }

        if (this.formatText == 'The value for this field must fill the format {0}' && this.fieldLabel)
        {
            this.formatText = Ext.String.format('The value for {0} must fill the format {1}', this.fieldLabel, this.format);
        }

        if (this.format)
        {
            stripChars = [];

            Ext.each(this.format.split(''), function (char)
            {
                if (char != '@')
                {
                    stripChars.push(Ext.String.format('\\{0}', char));
                }
            }, this);

            if (stripChars.length > 0 && !this.stripCharsRe)
            {
                this.stripCharsRe = new RegExp(Ext.String.format('[{0}]', stripChars.join('')), "g");
            }
        }

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);
        this.mixins.message.constructor.call(this);

        this.onMessagesChangedCallback = this.onMessagesChanged.bind(this);

        this.addListener('blur', this.onFormFieldTextBlur, this);
        this.addListener('change', this.onFormFieldTextChange, this);
        this.addListener('render', this.onFormFieldTextRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldTextBlur: function (field)
    {
        var value = this.parseValue(this.getRawValue());

        if (!Ext.isEmpty(value))
        {
            this.setValue(value);
        }
    },

    onFormFieldTextChange: function (field, newValue, oldValue)
    {
        if (newValue == null)
        {
            this.clearColor();
        }

        this.clearMessages();
    },

    onFormFieldTextRender: function (field)
    {
        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.inputEl, { 'text-align': this.align, 'padding-right': '2px' });
        }

        if (this.parentForm)
        {
            if (typeof this.parentForm == 'string')
            {
                this.parentForm = Ext.getCmp(this.parentForm);
            }
        }
        else
        {
            this.parentForm = Computronix.Ext.getForm(this);
        }

        this.setReadOnly(this.readOnly);
    },

    onMessagesChanged: function ()
    {
        this.isValid();

        if (this.rendered)
        {
            if (this.hasWarningOnly())
            {
                this.bodyEl.dom.setAttribute('data-warningqtip', this.getActiveError());
                this.addCls('warning-background');
                this.inputWrap.addCls('warning-border');
            }
            else
            {
                this.bodyEl.dom.removeAttribute('data-warningqtip');
                this.removeCls('warning-background');
                this.inputWrap.removeCls('warning-border');
            }
        }
    },

    // Methods
    //=============================================================================
    //
    checkChange: function ()
    {
        var value;

        if (!this.suspendCheckChange)
        {
            value = this.getValue();

            if (value && this.dataCase)
            {
                switch (this.dataCase)
                {
                    case 'upper':
                        if (!this.isEqual(value, this.lastValue) && !this.isDestroyed && value != value.toUpperCase())
                        {
                            this.suspendCheckChange++;
                            this.setValue(value.toUpperCase());
                            this.suspendCheckChange--;
                        }
                        break;
                    case 'lower':
                        if (!this.isEqual(value, this.lastValue) && !this.isDestroyed && value != value.toLowerCase())
                        {
                            this.suspendCheckChange++;
                            this.setValue(value.toLowerCase());
                            this.suspendCheckChange--;
                        }
                        break;
                }
            }

            this.callParent(arguments);
        }
    },

    clearColor: function ()
    {
        var className;
        var color = /^cxcolor/;

        if (this.inputEl && this.colorClsField)
        {
            className = this.inputEl.dom.className;

            Ext.each(className.split(' '), function (cls)
            {
                if (color.test(cls))
                {
                    this.inputEl.removeCls(cls);
                }
            }, this);
        }
    },

    getActiveError: function ()
    {
        return this.addWarningMessages(this.callParent(arguments));
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);

        if (value && this.enforceFormat && this.format && this.format.match(/[@]/g).length != value.length)
        {
            result.push(Ext.String.format(this.formatText, this.format));
        }

        if (this.hasErrorMessages())
        {
            result = result.concat(this.getErrorMessages());
        }

        return result;
    },

    getFormattedValue: function (value)
    {
        var result = value;
        var formattedChars;
        var index = 0;

        if (value && this.format)
        {
            formattedChars = [];
            value = value.toString();

            Ext.each(this.format.split(''), function (char)
            {
                if (char == '@')
                {
                    formattedChars.push(value.substr(index, 1));
                    index++;
                }
                else
                {
                    formattedChars.push(char)
                }
            }, this);

            result = formattedChars.join('');

            if (index < value.length)
            {
                result += value.substr(index);
            }
        }

        return result;
    },

    isValid: function ()
    {
        var result = true;

        if (this.readOnly)
        {
            this.clearInvalid();
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    parseValue: function (value)
    {
        if (value && this.stripCharsRe)
        {
            value = value.toString().replace(this.stripCharsRe, '');
        }

        return value;
    },

    processRawValue: function (value)
    {
        var newValue;

        if (value && this.stripCharsRe)
        {
            newValue = value.toString().replace(this.stripCharsRe, '');

            if (newValue !== value)
            {
                this.rawValue = newValue;
                value = newValue;
            }
        }

        return value;
    },

    renderActiveError: function ()
    {
        if (!this.hasWarningOnly())
        {
            this.callParent(arguments);
        }
    },

    setColor: function (value)
    {
        if (this.inputEl && value && this.colorClsField && (this.record || this.parentForm && this.parentForm.getRecord()))
        {
            this.clearColor();

            this.inputEl.addCls((this.record || this.parentForm.getRecord()).get(this.colorClsField));
        }
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        if (this.inputEl)
        {
            if (readOnly)
            {
                this.inputEl.addCls('cxreadonlyformtext');
            }
            else
            {
                this.inputEl.removeCls('cxreadonlyformtext');
            }
        }
    },

    setValue: function (value)
    {
        this.callParent(arguments);

        if (this.inputEl && value && this.colorClsField && (this.record || this.parentForm && this.parentForm.getRecord()))
        {
            this.setColor(value);
        }

        if (this.inputEl && value && this.format)
        {
            this.inputEl.dom.value = this.getFormattedValue(value);
        }
    }
});

// Computronix text area extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.TextArea',
{
    extend: 'Ext.form.field.TextArea',
    alias: 'widget.computronixexttextarea',
    mixins:
    {
        mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator',
        microhelp: 'Computronix.Ext.Microhelp',
        message: 'Computronix.Ext.Message'
    },

    labelClsExtra: 'cxfieldlabel',
    enableKeyEvents: true,
    /*
     * VERSION_WARNING 6.0.2
     *
     * selectOnFocus only works for clicking and not tabbing TextArea in 6.0.2
     * in IE and Edge. This will be resolved with an upgrade to 6.2.0.
     * All other browsers work correct.
     */
    selectOnFocus: true,
    width: 200,

    // Data Members
    //=============================================================================
    //
    align: 'left',
    selection: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        if (!this.emptyText && this.fieldLabel)
        {
            this.emptyText = Ext.String.format('Enter a {0}...', this.fieldLabel);
        }

        if (this.blankText == 'This field is required' && this.fieldLabel)
        {
            this.blankText = Ext.String.format('{0} is required', this.fieldLabel);
        }

        if (this.minLengthText == 'The minimum length for this field is {0}' && this.fieldLabel)
        {
            this.minLengthText = Ext.String.format('The minimum length for {0} is {1}', this.fieldLabel, this.minLength);
        }

        if (this.maxLengthText == 'The maximum length for this field is {0}' && this.fieldLabel)
        {
            this.maxLengthText = Ext.String.format('The maximum length for {0} is {1}', this.fieldLabel, this.maxLength);
        }

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);
        this.mixins.message.constructor.call(this);

        this.onMessagesChangedCallback = this.onMessagesChanged.bind(this);

        this.addListener('change', this.onFormFieldTextAreaChange, this);
        this.addListener('render', this.onFormFieldTextAreaRender, this);

        if (Ext.isIE)
        {
            this.addListener('keyup', this.onFormFieldTextAreaKeyUp, this);
        }
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldTextAreaChange: function (field, newValue, oldValue)
    {
        this.clearMessages();
    },

    onFormFieldTextAreaClick: function (field)
    {
        this.selection = this.getSelectionRange();
    },

    onFormFieldTextAreaKeyUp: function (field)
    {
        this.selection = this.getSelectionRange();
    },

    onFormFieldTextAreaRender: function (field)
    {
        if (Ext.isIE)
        {
            this.inputEl.addListener('click', this.onFormFieldTextAreaClick, this);
        }

        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.inputEl, { 'text-align': this.align, 'padding-right': '2px' });
        }

        this.setReadOnly(this.readOnly);
    },

    onMessagesChanged: function ()
    {
        this.isValid();

        if (this.rendered)
        {
            if (this.hasWarningOnly())
            {
                this.bodyEl.dom.setAttribute('data-warningqtip', this.getActiveError());
                this.addCls('warning-background');
                this.inputWrap.addCls('warning-border');
            }
            else
            {
                this.bodyEl.dom.removeAttribute('data-warningqtip');
                this.removeCls('warning-background');
                this.inputWrap.removeCls('warning-border');
            }
        }
    },

    // Methods
    //=============================================================================
    //
    getActiveError: function ()
    {
        return this.addWarningMessages(this.callParent(arguments));
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);

        if (this.hasErrorMessages())
        {
            result = result.concat(this.getErrorMessages());
        }

        return result;
    },

    getSelectionRange: function ()
    {
        var result = { start: 0, end: 0 };
        var input;

        if (this.inputEl)
        {
            input = this.inputEl.dom;
            input.focus();

            result = { start: input.selectionStart, end: input.selectionEnd };
        }

        return result;
    },

    insertText: function (text)
    {
        var range;
        var value;
        var start;

        if (text && this.inputEl)
        {
            value = this.getValue();

            if (Ext.isIE)
            {
                if (this.selection)
                {
                    this.setValue(Ext.String.format('{0}{1}{2}', value.substr(0, this.selection.start), text, value.substr(this.selection.end)));

                    text = text.replace(/\n/g, '');
                    range = this.inputEl.dom.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', this.selection.start + text.length);
                    range.moveStart('character', this.selection.start + text.length);
                    range.select();

                    this.selection.start += text.length;
                    this.selection.end = this.selection.start;
                }
                else
                {
                    this.setValue(Ext.String.format('{0}{1}', value, text));
                }
            }
            else
            {
                start = this.inputEl.dom.selectionStart;
                this.setValue(Ext.String.format('{0}{1}{2}', value.substr(0, start), text, value.substr(this.inputEl.dom.selectionEnd)));

                text = text.replace(/\n/g, '');
                this.inputEl.dom.setSelectionRange(start + text.length, start + text.length);
            }
        }
    },

    isValid: function ()
    {
        var result = true;

        if (this.readOnly)
        {
            this.clearInvalid();
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    renderActiveError: function ()
    {
        if (!this.hasWarningOnly())
        {
            this.callParent(arguments);
        }
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        if (this.inputEl)
        {
            if (readOnly)
            {
                this.inputEl.addCls('cxreadonlyformtext');
            }
            else
            {
                this.inputEl.removeCls('cxreadonlyformtext');
            }
        }
    }
});

// Computronix Time extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.field.Time',
{
    extend: 'Ext.form.field.Time',
    alias: 'widget.computronixexttime',
    mixins:
    {
        mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator',
        microhelp: 'Computronix.Ext.Microhelp',
        message: 'Computronix.Ext.Message'
    },
    labelClsExtra: 'cxfieldlabel',
    width: 200,
    format: 'H:i:s',

    // Data Members
    //=============================================================================
    //
    align: 'left',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        if (!this.emptyText)
        {
            this.emptyText = 'hh:mm:ss';
        }

        if (this.blankText == 'This field is required' && this.fieldLabel)
        {
            this.blankText = Ext.String.format('{0} is required', this.fieldLabel);
        }

        this.mixins.mandatoryIndicator.constructor.call(this);
        this.mixins.microhelp.constructor.call(this);
        this.mixins.message.constructor.call(this);

        this.addListener('render', this.onFormFieldTimeRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormFieldTimeRender: function (field)
    {
        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.inputEl, { 'text-align': this.align, 'padding-right': '2px' });
        }

        this.setReadOnly(this.readOnly);
    },

    // Methods
    //=============================================================================
    //
    getActiveError: function ()
    {
        return this.addWarningMessages(this.callParent(arguments));
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);

        if (this.hasErrorMessages())
        {
            result = result.concat(this.getErrorMessages());
        }

        return result;
    },

    isValid: function ()
    {
        var result = true;

        if (this.readOnly)
        {
            this.clearInvalid();
        }
        else
        {
            result = this.callParent(arguments);
        }

        return result;
    },

    renderActiveError: function ()
    {
        if (!this.hasWarningOnly())
        {
            this.callParent(arguments);
        }
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        if (this.inputEl)
        {
            if (readOnly)
            {
                this.inputEl.addCls('cxreadonlyformtext');
            }
            else
            {
                this.inputEl.removeCls('cxreadonlyformtext');
            }
        }
    }
});

// Computronix field container extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.FieldContainer',
{
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.computronixextfieldcontainer',
    mixins:
    {
        field: 'Ext.form.field.Field',
        mandatoryIndicator: 'Computronix.Ext.MandatoryIndicator',
        message: 'Computronix.Ext.Message'
    },
    fieldBodyCls: 'cxfieldcontainer',
    labelClsExtra: 'cxfieldcontainerlabel',
    invalidCls: 'x-form-invalid',
    height: 26,

    // Data Members
    //=============================================================================
    //
    allowBlank: true,
    blankText: 'This field is required',
    readOnly: false,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (this.blankText == 'This field is required' && this.fieldLabel)
        {
            this.blankText = Ext.String.format('{0} is required', this.fieldLabel);
        }

        if (this.fieldBodyCls == 'cxfieldcontainer')
        {
            if (this.y > 0)
            {
                this.y -= 1;
            }

            if (this.labelWidth)
            {
                this.labelWidth -= 1;
                this.width += 1;
            }
            else
            {
                if (this.x > 0)
                {
                    this.x -= 1;
                    this.width += 2;
                }
                else
                {
                    this.width += 1;
                }
            }
        }

        this.callParent();

        this.mixins.mandatoryIndicator.constructor.call(this);

        this.initField();
    },

    // Methods
    //=============================================================================
    //
    clearInvalid: function ()
    {
        this.errorMessages = null;
        this.unsetActiveError();
    },

    getActiveError: function ()
    {
        return this.addWarningMessages(this.callParent(arguments));
    },

    getErrors: function (value)
    {
        var result = [];

        if (Ext.isEmpty(this.getValue()) && !this.allowBlank)
        {
            result.push(this.blankText);
        }

        if (this.hasErrorMessages())
        {
            result = result.concat(this.getErrorMessages());
        }

        return result;
    },

    getRawValue: function ()
    {
        return this.getValue();
    },

    getValue: function ()
    {
        return this.mixins.field.getValue.call(this);
    },

    isValid: function ()
    {
        var result;
        var errors = this.getErrors(this.getValue());

        result = this.readOnly || this.disabled || Ext.isEmpty(errors);

        if (result)
        {
            this.clearInvalid();
        }
        else
        {
            this.markInvalid(errors);
        }

        return result;
    },

    markInvalid: function (errors)
    {
        var oldMsg = this.getActiveError();

        this.setActiveErrors(Ext.Array.from(errors));

        if (oldMsg !== this.getActiveError())
        {
            this.updateLayout();
        }
    },

    renderActiveError: function ()
    {
        if (!this.hasWarningOnly())
        {
            this.callParent(arguments);
        }
    },

    setRawValue: function (value)
    {
        this.setValue(value);
    },

    setReadOnly: function (readOnly)
    {
        this.readOnly = readOnly;
    },

    setValue: function (value)
    {
        this.setWarningMessages(null);
        this.mixins.field.setValue.call(this, value);
        this.clearInvalid();
    },

    validate: function ()
    {
        var isValid = this.isValid();

        if (isValid)
        {
            this.unsetActiveError();
        }
        else
        {
            this.setActiveErrors(this.getErrors(this.getValue()));
            this.updateLayout();
        }

        if (isValid !== this.wasValid)
        {
            this.wasValid = isValid;
            this.fireEvent('validitychange', this, isValid);
            this.updateLayout();
        }

        return isValid;
    }
});

// Computronix field set extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.FieldSet',
{
    extend: 'Ext.form.FieldSet',
    alias: 'widget.computronixextfieldset',
    cls: 'cxfieldset'
});

// Computronix label extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.Label',
{
    extend: 'Ext.form.Label',
    alias: 'widget.computronixextlabel',

    // Methods
    //=============================================================================
    //
    disable: function ()
    {
        this.callParent(arguments);

        if (this.el)
        {
            this.el.addCls('cxmask');
        }
    },

    enable: function ()
    {
        this.callParent(arguments);

        if (this.el)
        {
            this.el.removeCls('cxmask');
        }
    }
});

// Computronix form panel extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.Panel',
{
    extend: 'Ext.form.Panel',
    alias: 'widget.computronixextform',
    mixins: { presentationLoader: 'Computronix.Ext.PresentationLoader' },
    frame: true,
    autoScroll: true,

    // Data Members
    //=============================================================================
    //
    isForm: true,
    isLoading: false,
    ignoreError: false,
    focusFirstDeferPeriod: 250,
    focusTarget: null,

    // A reference to the store used to populated the form.
    store: null,

    // The index of the row used to populate the form.
    rowIndex: 0,

    contextMenu: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.setStore(this.store);

        this.mixins.presentationLoader.constructor.call(this);

        if (this.store && typeof this.store == 'object')
        {
            this.stores.push(this.store);
        }

        this.addListener('afterrender', this.onFormPanelAfterRender, this);

        if (this.contextMenu)
        {
            if (typeof this.contextMenu == 'string')
            {
                this.contextMenu = Ext.menu.Manager.get(this.contextMenu);
            }

            this.addListener('contextmenu', this.onFormPanelContextMenu, this);
        }

        this.addManagedListener(Computronix.Ext.ScriptLoader, 'scriptload', this.onFormPanelScriptLoaderScriptLoad, this);
        this.addManagedListener(Computronix.Ext.Ajax, 'batchcomplete', this.onFormPanelBatchComplete, this);
        this.addManagedListener(this.getLoader(), 'load', this.onFormPanelLoad, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onFormPanelAfterRender: function (form)
    {
        if (!this.presentationAjaxBatchId)
        {
            this.loadData();
        }

        if (this.contextMenu)
        {
            this.addManagedListener(this.getEl(),
            {
                'contextmenu': this.onFormPanelElementContextMenu,
                scope: this
            });
        }
    },

    onFormPanelBatchComplete: function (ajax, ajaxBatchId, success)
    {
        if (this.presentationAjaxBatchId == ajaxBatchId && success)
        {
            this.loadData();
        }
    },

    onFormPanelContextMenu: function (form, event)
    {
        event.preventDefault();
        this.contextMenu.showAt(event.getXY());
    },

    onFormPanelDataChanged: function (store)
    {
        this.loadData();
    },

    onFormPanelElementContextMenu: function (event)
    {
        this.fireEvent('contextmenu', this, event);
    },

    onFormPanelLoad: function (loader, response, options)
    {
        if (!this.presentationAjaxBatchId)
        {
            this.loadData();
        }

        if (this.focusTarget)
        {
            this.focus(this.focusTarget);
        }
    },

    onFormPanelScriptLoaderScriptLoad: function (scriptLoader, loader, response, options)
    {
        if (options.params.scriptId == this.scriptId)
        {
            // If script was provided to define the store then once it has been loaded update the store reference
            // on the form.
            this.setStore(this.store);
        }
    },

    // Methods
    //=============================================================================
    //
    clearData: function ()
    {
        this.isLoading = true;

        if (this.store)
        {
            this.store.removeAll();
        }
        else
        {
            this.form.reset();
        }

        this.isLoading = false;
    },

    clearInvalid: function ()
    {
        return this.form.clearInvalid();
    },

    completeEdit: function ()
    {
        var result = true;

        this.getForm().getFields().each(function (field)
        {
            // Only resolve the value if the field is currently visible.
            if (field.assertValue && field.isVisible(true))
            {
                field.assertValue();
            }
        }, this);

        if (this.isValid())
        {
            this.getData();
        }
        else
        {
            result = false;
        }

        return result;
    },

    focus: function (target)
    {
        var fields;
        var targets = Ext.Array.from(target);

        if (targets.length > 0)
        {
            this.focusTarget = targets;

            if (this.getForm().monitor)
            {
                fields = this.getForm().getFields().items;

                Ext.each(fields, function (field)
                {
                    if (field.name == targets[0])
                    {
                        target = targets.slice(1);

                        if (target.length > 0)
                        {
                            field.focus(target);
                        }
                        else
                        {
                            field.focus(false);
                        }

                        return false;
                    }
                }, this);
            }
        }
    },

    // Sets focus to the first field in the form.
    focusFirst: function ()
    {
        Ext.Function.defer(function ()
        {
            var fields;

            if (this.getForm().monitor)
            {
                fields = this.getForm().getFields().items;
                fields = Ext.Array.sort(fields, function (first, second)
                {
                    var result = 0;

                    if (first.tabIndex > second.tabIndex)
                    {
                        result = 1
                    }
                    else if (first.tabIndex < second.tabIndex)
                    {
                        result = -1
                    }

                    return result;
                });

                Ext.each(fields, function (field)
                {
                    if (!field.isDisabled()
                        && !field.isXType('displayfield')
                        && !field.up('toolbar'))
                    {
                        field.focus(false);
                        return false;
                    }
                }, this);
            }
        }, this.focusFirstDeferPeriod, this);
    },

    getButtons: function (item, hashMap)
    {
        var result = null;

        if (!item)
        {
            item = this;
        }

        if (hashMap)
        {
            result = hashMap;
        }
        else
        {
            result = new Ext.util.HashMap();
        }

        item.items.each(function (item)
        {
            if (item.isButton)
            {
                result.add(item.name, item);
            }
            else if (item.isContainer)
            {
                this.getButtons(item, result);
            }
        }, this);

        return result;
    },

    getData: function ()
    {
        if (this.store && Ext.data.StoreManager.containsKey(this.store.storeId) && this.store.getCount() > this.rowIndex)
        {
            this.form.updateRecord(this.store.getAt(this.rowIndex));
        }
    },

    getErrors: function ()
    {
        var result = [];

        // Do not return errors for a form that does not have a record bound to it.
        if (!this.store || this.getRecord() != null)
        {
            if (!this.getForm().isValid())
            {
                this.getForm().getFields().each(function (field)
                {
                    if (!field.isValid())
                    {
                        result = result.concat(field.getErrors(field.getValue()));
                    }
                }, this);
            }
        }

        return result;
    },

    getIsLoading: function (includeParents)
    {
        var result = this.isLoading;
        var parentForm = Computronix.Ext.getForm(this);

        if (includeParents != false && !result)
        {
            while (parentForm)
            {
                if (parentForm.isLoading)
                {
                    result = true;
                    parentForm = null;
                }
                else
                {
                    parentForm = Computronix.Ext.getForm(parentForm.ownerCt);
                }
            }
        }

        return result;
    },

    getWarnings: function ()
    {
        var result = [];

        // Do not return errors for a form that does not have a record bound to it.
        if (!this.store || this.getRecord() != null)
        {
            this.getForm().getFields().each(function (field)
            {
                if (field.hasWarningMessages && field.hasWarningMessages())
                {
                    result = result.concat(field.getWarningMessages());
                }
            }, this);
        }

        return result;
    },

    getRecord: function ()
    {
        var result = null;

        if (this.store && Ext.data.StoreManager.containsKey(this.store.storeId) && this.store.getCount() > this.rowIndex)
        {
            result = this.store.getAt(this.rowIndex);
        }

        return result;
    },

    isValid: function ()
    {
        var result = true;

        // Do not return errors for a form that does not have a record bound to it.
        if (!this.store || this.getRecord() != null)
        {
            result = this.form.isValid();
        }

        return result;
    },

    loadData: function (index)
    {
        var editField;
        var record;
        var accessLevel = /\$AccessLevel$/;
        var errorMessages = /\$ErrorMessages$/;
        var warningMessages = /\$WarningMessages$/;
        var fieldName;
        var buttons;
        var button;

        if (typeof index != 'undefined')
        {
            this.rowIndex = index;
        }

        if (this.store && Ext.data.StoreManager.containsKey(this.store.storeId) && this.store.isLoaded())
        {
            this.isLoading = true;

            if (this.store.getCount() > this.rowIndex)
            {
                record = this.store.getAt(this.rowIndex);
                buttons = this.getButtons();
                this.form.loadRecord(record);

                record.fieldsByName.each(function (field)
                {
                    // Set access level for each field.
                    if (accessLevel.test(field.name))
                    {
                        fieldName = field.name.substr(0, field.name.indexOf('$AccessLevel'));

                        this.setAccessLevel(fieldName, record.get(field.name));

                        if (buttons && buttons.containsKey(fieldName))
                        {
                            button = buttons.get(fieldName);

                            switch (record.get(field.name))
                            {
                                case 'Invisible':
                                    button.enable();
                                    button.hide();
                                    break;
                                case 'Disabled':
                                    button.show();
                                    button.disable();
                                    break;
                                case 'Enterable':
                                    button.show();
                                    button.enable();
                                    break;
                            }
                        }
                    }

                    // Set the error message for each field.
                    if (errorMessages.test(field.name))
                    {
                        editField = this.form.findField(field.name.substr(0, field.name.indexOf('$ErrorMessages')));

                        if (editField && editField.setErrorMessages)
                        {
                            editField.setErrorMessages(record.get(field.name));
                        }
                    }

                    // Set the warning message for each field.
                    if (warningMessages.test(field.name))
                    {
                        editField = this.form.findField(field.name.substr(0, field.name.indexOf('$WarningMessages')));

                        if (editField && editField.setWarningMessages)
                        {
                            editField.setWarningMessages(record.get(field.name));
                        }
                    }
                }, this);
            }
            else
            {
                this.form.reset();
            }

            this.isLoading = false;
            this.fireEvent('afterloaddata', this);
        }
    },

    // Sets the access level for the specified field.
    setAccessLevel: function (fieldName, level)
    {
        var field = this.form.findField(fieldName);

        if (field)
        {
            switch (level)
            {
                case 'Invisible':
                    field.enable();
                    field.setReadOnly(false);
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(false);
                    field.hide();
                    break;
                case 'Disabled':
                    field.show();
                    field.setReadOnly(false);
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(false);
                    field.disable();
                    break;
                case 'ReadOnly':
                    field.show();
                    field.enable();
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(false);
                    field.setReadOnly(true);
                    break;
                case 'Enterable':
                    field.show();
                    field.setReadOnly(false);
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(false);
                    field.enable();
                    break;
                case 'Mandatory':
                    field.show();
                    field.enable();
                    field.setReadOnly(false);
                    field.enforceMandatory = true;
                    field.setMandatoryIndicator(true);
                    break;
                case 'ShowMandatory':
                    field.show();
                    field.enable();
                    field.setReadOnly(false);
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(true);
                    break;
            }
        }
    },

    setStore: function (store)
    {
        var _store;

        if (this.store)
        {
            this.removeManagedListener(this.store, 'datachanged', this.onFormPanelDataChanged, this);
        }

        this.store = store;

        if (this.store)
        {
            _store = Ext.getStore(this.store);

            if (_store)
            {
                this.store = _store;
                this.addManagedListener(this.store, 'datachanged', this.onFormPanelDataChanged, this);

                if (this.store.isLoaded())
                {
                    this.loadData();
                }
            }
        }
    }
});

// Computronix editmask extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.EditMask',
{
    extend: 'Computronix.Ext.form.FieldContainer',
    alias: 'widget.computronixexteditmask',
    columnHeight: null,
    layout: 'fit',
    validateOnChange: false,
    width: 200,

    // Data Members
    //=============================================================================
    //
    parentForm: null,
    record: null,
    align: 'center',
    editMask: null,
    enforceMask: true,
    enforceCompleteMask: true,
    maskText: 'The value does not match mask',
    showMask: false,
    inputs: null,
    delimiterWidth: 8,
    delimiterPadding: 2,
    inputWidth: 15,
    maskContents: null,

    metaChars:
    {
        'x': '\\S{1}',
        '#': '\\d{1}',
        'a': '[A-Za-z0-9]{1}',
        '@': '[A-Za-z]{1}'
    },
    escapeChar: '\\',
    fillerChar: ' ',
    inputContainer: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        var widget;
        var widgets = [];
        var delimiter;
        var segments;
        var index = 0;
        var maxWidth = 0;
        var mask;
        var element;
        var metaChars = '';
        var segmentRe;
        var delimiterRe;

        for (var metaChar in this.metaChars)
        {
            metaChars += metaChar;
        }

        segmentRe = new RegExp(Ext.String.format('([^{0}])', metaChars));
        delimiterRe = new RegExp(Ext.String.format('[^{0}]', metaChars));

        this.inputContainer = new Ext.container.Container(
        {
            layout: 'hbox',
            cls: 'input-container',
        });

        this.items = this.inputContainer;

        // Array of displayed values, placed here for the getFormattedValue function
        this.maskContents = [];

        // Can not be initialized when the property is declared because the array
        // reference would be global.
        this.inputs = [];

        if (this.editMask)
        {
            segments = this.editMask.split(segmentRe);
            segments = Ext.Array.clean(segments);

            Ext.each(segments, function (segment, segmentIndex, segments)
            {
                if (segment)
                {
                    if (segment.search(delimiterRe) > -1)
                    {
                        if (segment == this.escapeChar)
                        {
                            if (segments[segmentIndex + 1].length == 1)
                            {
                                segment = segments[segmentIndex + 1];
                                segments[segmentIndex + 1] = null;
                            }
                            else
                            {
                                segment = segments[segmentIndex + 1].substr(0, 1);
                                segments[segmentIndex + 1] = segments[segmentIndex + 1].substr(1);
                            }
                        }

                        if (segment == ' ')
                        {
                            segment = '&nbsp;';
                        }

                        if (widget && widget.isXType('computronixextdisplayfield'))
                        {
                            widget.setValue(widget.getValue() + segment);
                            widget.width += this.delimiterWidth;
                        }
                        else
                        {
                            // Static text segment
                            widget = new Computronix.Ext.form.field.Display(
                            {
                                value: segment,
                                width: this.delimiterWidth + (this.delimiterPadding * 2),
                                minWidth: this.delimiterWidth + (this.delimiterPadding * 2) || 5,
                                labelPad: 0,
                                align: 'center',
                                fieldStyle: { 'text-overflow': 'clip' }
                            });

                            this.inputContainer.add(widget);
                            this.maskContents.push(widget);
                        }
                    }
                    else
                    {
                        // Input field segment
                        widget = new Computronix.Ext.form.field.Text(
                        {
                            inputIndex: index,
                            readOnly: this.readOnly,
                            align: this.align,
                            maxLength: segment.length,
                            enforceMaxLength: true,
                            dataCase: this.dataCase,
                            mask: this.enforceMask ? new RegExp(this.getMask(segment)) : null,
                            emptyText: this.showMask ? segment : null,
                            helpText: this.helpText,
                            width: Math.max(segment.length * this.inputWidth, 22),
                            height: this.columnHeight,
                            enableKeyEvents: true,
                            listeners:
                            {
                                change: { fn: this.onEditMaskTextFieldChange, scope: this },
                                blur: { fn: this.onEditMaskTextFieldBlur, scope: this },
                                specialkey: { fn: this.onEditMaskTextFieldSpecialKey, scope: this },
                                keyDown: { fn: this.onEditMaskTextFieldKeyDown, scope: this }
                            }
                        });

                        this.inputContainer.add(widget);
                        this.maskContents.push(widget);

                        maxWidth = Math.max(maxWidth, segment.length);

                        this.inputs.push(widget);
                        index++;
                    }
                }
            }, this);

            // Allow the largest input fields to expand to fit the widget width
            Ext.each(this.inputs, function (input)
            {
                if (input.maxLength == maxWidth)
                {
                    input.flex = 1;
                }
            }, this);
        }

        this.height += 2;

        this.callParent();

        if (this.maskText == 'The value does not match mask' && this.editMask)
        {
            this.maskText = Ext.String.format('The value does not match {0}', this.editMask);
        }

        this.onMessagesChangedCallback = this.onMessagesChanged.bind(this);

        this.addListener('afterrender', this.onEditMaskAfterRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onEditMaskAfterRender: function (component)
    {
        if (this.parentForm)
        {
            if (typeof this.parentForm == 'string')
            {
                this.parentForm = Ext.getCmp(this.parentForm);
            }
        }
        else
        {
            this.parentForm = Computronix.Ext.getForm(this);
        }

        if (this.value)
        {
            this.setValue(this.value);
        }

        if (this.columnHeight)
        {
            Ext.each(this.inputs, function (input)
            {
                /* FUTURE_WARNING 6.0.2
                 * This could be done better by creating a ui for grids.
                 * However, we have identified that these kinds of problems
                 * are caused by the fact that we are reusing the form edit mask
                 * mask in a grid. A grid poses a different environment.
                 * In this case, the space around a grid is smaller, so the
                 * inputContainer that displays the error/warnings css has no
                 * natural place to display. We have to reduce the height
                 * of the elements inside the edit mask. We have decided that this
                 * problem should be solved by creating a different edit mask
                 * widget for grids. When that work is done, a ui will be created
                 * to go along.
                 */
                input.inputEl.setHeight(this.columnHeight);
                input.inputEl.setMinHeight(this.columnHeight);
                input.inputWrap.setHeight(this.columnHeight);
                input.inputWrap.setMinHeight(this.columnHeight);
                input.triggerWrap.setHeight(this.columnHeight);
                input.triggerWrap.setMinHeight(this.columnHeight);
            }, this);
        }
    },

    onEditMaskTextFieldBlur: function (field, event)
    {
        this.validateValue();

        Ext.Function.defer(function ()
        {
            var done = false;
            var activeElement = Ext.Element.getActiveElement();

            Ext.each(this.inputs, function (input)
            {
                if (Ext.String.startsWith(activeElement.id, input.id))
                {
                    done = true;
                }
            }, this);

            if (!done)
            {
                this.fireEvent('blur', this, event);
            }
        }, 10, this);
    },

    onEditMaskTextFieldChange: function (field, newValue, oldValue)
    {
        this.clearMessages();
        this.clearInvalid();
    },

    onEditMaskTextFieldKeyDown: function (field, event)
    {
        if (field.maxLength == field.getValue().length && field.inputIndex < this.inputs.length - 1 && !event.isSpecialKey())
        {
            this.moveNext(field);
        }
    },

    onEditMaskTextFieldSpecialKey: function (field, event)
    {
        if (event.keyCode == event.BACKSPACE)
        {
            if (field.getValue().length == 0 && field.inputIndex > 0)
            {
                Ext.Function.defer(this.movePrevious, 10, this, [field]);
            }
        }

        if (event.getKey() != event.TAB || (field == this.inputs[0] && event.shiftKey || field == this.inputs[this.inputs.length - 1] && !event.shiftKey))
        {
            this.fireEvent('specialKey', this, event);
        }
    },

    onMessagesChanged: function ()
    {
        if (this.rendered)
        {
            if (this.hasWarningOnly())
            {
                this.bodyEl.dom.setAttribute('data-warningqtip', this.getActiveError());
                this.inputContainer.addCls('warning-border');
                this.addCls('warning-background');
            }
            else
            {
                this.bodyEl.dom.removeAttribute('data-warningqtip');
                this.inputContainer.removeCls('warning-border');
                this.removeCls('warning-background');
            }
        }
    },

    // Methods
    //=============================================================================
    //
    focus: function (selectText, delay)
    {
        this.callParent(arguments);

        if (this.inputs && this.inputs.length > 0)
        {
            this.inputs[0].focus(selectText, delay);
        }
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);

        var hasValue = false;
        var hasError = false;

        Ext.each(this.inputs, function (input)
        {
            value = input.getValue();

            if (value)
            {
                hasValue = true;
            }

            if (input.mask && this.enforceCompleteMask && !input.mask.test(value) ||
                input.mask && !this.enforceCompleteMask && value && !input.mask.test(value))
            {
                hasError = true;

                if (hasValue)
                {
                    return false;
                }
            }
        }, this);

        if (hasValue && hasError)
        {
            result.push(this.maskText);
        }

        return result;
    },

    getFormattedValue: function (value)
    {
        var result = '';
        var delimiter;

        Ext.each(this.maskContents, function (item)
        {
            if (item.isXType('computronixexttextfield'))
            {
                if (value)
                {
                    result += value.substr(0, item.maxLength)
                    value = value.substr(item.maxLength);
                }
            }
            else if (item.isXType('computronixextdisplayfield'))
            {
                delimiter = item.getValue();

                if (delimiter.length > 0)
                {
                    result += delimiter.replace('&nbsp;', ' ');
                }
            }
        }, this);

        return result;
    },

    getMask: function (segment)
    {
        var result = '';

        Ext.each(segment.split(''), function (element)
        {
            result += this.metaChars[element];
        }, this);

        return result;
    },

    getValue: function ()
    {
        var result = '';
        var hasValue = false;
        var value;

        Ext.each(this.inputs, function (input)
        {
            value = input.getValue();

            if (value)
            {
                hasValue = true;
                return false;
            }
        }, this);

        if (hasValue)
        {
            Ext.each(this.inputs, function (input)
            {
                value = input.getValue() || '';

                if (this.fillerChar)
                {
                    while (value.length < input.maxLength)
                    {
                        value += this.fillerChar;
                    }
                }

                result += value;
            }, this);
        }

        return result;
    },

    moveNext: function (field)
    {
        var targetField = this.inputs[field.inputIndex + 1];

        // Only move the cursore forward if the next field is empty.
        if (!targetField.getValue())
        {
            targetField.focus();
        }
    },

    movePrevious: function (field)
    {
        var targetField = this.inputs[field.inputIndex - 1];
        var range;

        targetField.focus();

        // Ensure the cursor is positioned at the end of the field
        if (Ext.isIE)
        {
            range = document.selection.createRange();
            range.moveStart('character', targetField.maxLength);
            range.moveEnd('character', targetField.maxLength);
            range.select();
        }
        else
        {
            targetField.inputEl.dom.setSelectionRange(targetField.maxLength, targetField.maxLength);
        }
    },

    setMicroHelp: function (microhelp)
    {
        Ext.each(this.inputs, function (input)
        {
            input.setMicroHelp(microhelp);
        }, this);
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);

        Ext.each(this.inputs, function (input)
        {
            input.setReadOnly(readOnly);
        }, this);
    },

    setValue: function (value)
    {
        var segment;
        var record;

        if (value)
        {
            Ext.each(this.inputs, function (input)
            {
                segment = value.substr(0, input.maxLength);
                segment = segment.replace(/\s+$/, '');

                input.setValue(segment);
                value = value.substr(input.maxLength);
            }, this);
        }
        else
        {
            Ext.each(this.inputs, function (input)
            {
                input.setValue(null);
            }, this);
        }
    },

    validateValue: function ()
    {
        var hasValue = true;

        if (!this.validateOnChange)
        {
            Ext.each(this.inputs, function (input)
            {
                value = input.getValue();

                if (!value)
                {
                    hasValue = false;
                    return false;
                }
            }, this);

            if (hasValue)
            {
                this.isValid();
            }
        }
    }
});

// Computronix password entry extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.PasswordEntry',
{
    extend: 'Computronix.Ext.form.FieldContainer',
    alias: 'widget.computronixextpasswordentry',
    width: 200,

    // Data Members
    //=============================================================================
    //
    confirmFieldLabel: 'Confirm:',
    confirmLabel: null,
    confirmPasswordField: null,
    confirmText: 'Both passwords must match',
    emptyText: null,
    inputContainer: null,
    maxLength: Ext.Number.MAX_VALUE,
    maxLengthText: 'The maximum length for this field is {0}',
    minLength: 0,
    minLengthText: 'The minimum length for this field is {0}',
    passwordField: null,
    vertical: false,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        var confirmFieldLabelText = this._resolveLabelText(this.confirmFieldLabel);
        var fieldLabelText = this._resolveLabelText(this.fieldLabel);

        this.passwordField = new Computronix.Ext.form.field.Text(
        {
            allowBlank: this.allowBlank,
            readOnly: this.readOnly,
            emptyText: this.emptyText,
            fieldLabel: null,
            labelWidth: this.labelWidth,
            labelSeparator: this.labelSeparator,
            enableMandatoryIndicator: false,
            inputType: 'password',
            listeners:
            {
                change: this.onPasswordEntryChange.bind(this),
            },
        });

        this.confirmPasswordField = new Computronix.Ext.form.field.Text(
        {
            allowBlank: this.allowBlank,
            readOnly: this.readOnly,
            emptyText: this.emptyText,
            fieldLabel: null,
            labelWidth: this.labelWidth,
            labelSeparator: this.labelSeparator,
            enableMandatoryIndicator: false,
            inputType: 'password',
            margin: 0,
            listeners:
            {
                change: this.onPasswordEntryChange.bind(this),
            },
        });

        if (this.vertical)
        {
            delete this.fieldLabel;
            this.inputContainer = new Ext.container.Container(
            {
                layout: { type: 'vbox', align: 'stretch' },
                cls: 'input-container',
                flex: 1,
                items:
                [
                    this.passwordField,
                    {
                        xtype: 'component',
                        flex: 1,
                    },
                    this.confirmPasswordField,
                ],
            });

            this.items = new Ext.container.Container(
            {
                layout: { type: 'hbox', align: 'stretch' },
                height: this.height,
                width: this.width,
                items:
                [
                    {
                        xtype: 'container',
                        layout: { type: 'vbox', align: 'stretch' },
                        padding: '5 0 5 0',
                        width: this.labelWidth + 4,
                        items:
                        [
                            {
                                xtype: 'label',
                                text: fieldLabelText,
                            },
                            {
                                xtype: 'component',
                                flex: 1,
                            },
                            {
                                xtype: 'label',
                                text: confirmFieldLabelText,
                            },
                        ],
                    },
                    this.inputContainer,
                ],
            });
        }
        else
        {
            if (this.confirmFieldLabel)
            {
                confirmFieldLabelText = this.confirmFieldLabel + this.labelSeparator;
            }
            else
            {
                confirmFieldLabelText = '';
            }

            this.passwordField.flex = 1;
            this.confirmPasswordField.flex = 1;

            this.inputContainer = new Ext.container.Container(
            {
                layout: 'hbox',
                cls: 'input-container',
                items:
                [
                    this.passwordField,
                    {
                        xtype: 'label',
                        margin: '3 3 0 3',
                        text: confirmFieldLabelText,
                    },
                    this.confirmPasswordField,
                ],
            });

            this.items = this.inputContainer;
        }

        this.callParent();

        if (this.minLengthText == 'The minimum length for this field is {0}' && this.fieldLabel)
        {
            this.minLengthText = Ext.String.format('The minimum length for {0} is {1}', this.fieldLabel, this.minLength);
        }

        if (this.maxLengthText == 'The maximum length for this field is {0}' && this.fieldLabel)
        {
            this.maxLengthText = Ext.String.format('The maximum length for {0} is {1}', this.fieldLabel, this.maxLength);
        }

        this.onMessagesChangedCallback = this.onMessagesChanged.bind(this);
    },

    // Event Handlers
    //=============================================================================
    //
    onMessagesChanged: function ()
    {
        this.isValid();

        if (this.rendered)
        {
            if (this.hasWarningOnly())
            {
                this.bodyEl.dom.setAttribute('data-warningqtip', this.getActiveError());
                this.inputContainer.addCls('warning-border');
                this.addCls('warning-background');
            }
            else
            {
                this.bodyEl.dom.removeAttribute('data-warningqtip');
                this.inputContainer.removeCls('warning-border');
                this.removeCls('warning-background');
            }
        }
    },

    onPasswordEntryChange: function (field, newValue, oldValue)
    {
        this.clearMessages();
    },

    // Methods
    //=============================================================================
    //
    _resolveLabelText: function (baseLabelText)
    {
        var resolvedLabelText;

        if (baseLabelText)
        {
            resolvedLabelText = baseLabelText + this.labelSeparator;
        }
        else
        {
            resolvedLabelText = '';
        }

        return resolvedLabelText;
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);

        if (!Ext.isEmpty(this.passwordField.getValue()) && this.passwordField.getValue().length < this.minLength)
        {
            result.push(Ext.String.format(this.minLengthText, this.minLength));
        }

        if (this.passwordField.getValue().length > this.maxLength)
        {
            result.push(Ext.String.format(this.maxLengthText, this.maxLength));
        }

        if (this.passwordField.getValue() != this.confirmPasswordField.getValue())
        {
            result.push(this.confirmText);
        }

        return result;
    },

    getValue: function ()
    {
        return this.passwordField.getValue();
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);
        this.passwordField.setReadOnly(readOnly);
        this.confirmPasswordField.setReadOnly(readOnly);
    },

    setValue: function (value)
    {
        this.callParent(arguments);
        this.passwordField.setValue(value);
        this.confirmPasswordField.setValue(value);
    }
});

// Computronix grid checkbox column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.CheckBox',
{
    extend: 'Ext.grid.column.Check',
    alias: 'widget.computronixextcheckboxcolumn',

    // Data Members
    //=============================================================================
    //
    enforceMandatory: true,
    readOnly: false,
    accessLevelField: null,
    summaryText: '{0}',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = this.dataIndex;
        }

        this.accessLevelField = Ext.String.format('{0}$AccessLevel', this.dataIndex);

        switch (this.summaryType)
        {
            case 'cxcount':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxCount(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmin':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMin(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmax':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMax(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxsum':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxSum(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxaverage':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxAverage(records, '{0}'); }", this.dataIndex));
                break;
        }

        this.callParent(arguments);

        /* VERSION_WARNING 4.1.1
         * Scope is not initialized if a renderer is declared which makes the
         * this reference in the summaryRenderer the summary feature not the
         * column.
         */
        this.scope = this;

        this.addListener('beforecheckchange', this.onCheckBoxColumnBeforeCheckChange, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onCheckBoxColumnBeforeCheckChange: function (column, rowIndex, checked, record, eOpts)
    {
        var accessLevel = record.get(this.accessLevelField);
        var editMode = this.up('grid').editMode;

        return accessLevel != 'Disabled'
            && !this.readOnly
            && accessLevel != 'ReadOnly'
            && (editMode == 'cell' || editMode == 'cellError' || editMode == 'hybrid');
    },

    // Methods
    //=============================================================================
    //
    /* VERSION_WARNING 6.0.2
     * ExtJS currently does not allow a single record in a given column to be
     * disabled. The 'disabled' and 'readOnly' properties apply to the entire
     * column. In order to allow for single records to be disabled or read-only,
     * we override the renderer to allow the access level on the record to be
     * queried in order to determine how the column should be displayed.
     */
    renderer: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        if (!gridView || !record)
        {
            return null;
        }

        var accessLevel = record.get(this.accessLevelField);

        if (accessLevel == 'Invisible' || accessLevel == 'Disabled')
        {
            return null;
        }

        var editMode = gridView.panel.editMode;
        var cls = 'x-grid-checkcolumn';

        if (this.readOnly || accessLevel == 'ReadOnly' ||
            (editMode != 'cell' && editMode != 'cellError' && editMode != 'hybrid'))
        {
            cls += ' x-grid-checkcolumn-readonly';
        }

        if (value)
        {
            cls += ' x-grid-checkcolumn-checked';
        }

        var result = Ext.DomHelper.markup(
        {
            'tag': 'div',
            'cls': cls,
            'role': 'button',
            'tabIndex': 0,
        });

        return result;
    },

    summaryRenderer: function (value, summaryData, field)
    {
        if (Ext.isEmpty(this.summaryType))
        {
            value = '';
        }

        return Ext.String.format('<span class="cxgridsummary">{0}</span>', Ext.String.format(this.summaryText || '{0}', Ext.isEmpty(value) ? '' : value));
    }
});

// Computronix grid text field column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.Column',
{
    extend: 'Ext.grid.column.Column',
    alias: 'widget.computronixexttextcolumn',

    // Data Members
    //=============================================================================
    //
    fieldXType: 'computronixexttextfield',
    displayOnlyFieldXType: 'computronixextdisplayfield',
    textAreaFieldXType: 'computronixexttextarea',
    allowBlank: true,
    autoScroll: true,
    blankText: 'This field is required',
    cellHeight: null,
    colorClsField: null,
    dataCase: null,
    displayOnly: false,
    emptyText: null,
    encodeObject: false,
    encodeValue: false,
    enforceFormat: true,
    enforceMandatory: true,
    enforceMaxLength: false,
    format: null,
    formatText: 'The value for this field must fill the format {0}',
    maxCellHeight: null,
    maxLength: Number.MAX_VALUE,
    maxLengthText: 'The maximum length for this field is {0}',
    minLength: 0,
    minLengthText: 'The minimum length for this field is {0}',
    noWrap: true,
    readOnly: false,
    summaryText: '{0}',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = this.dataIndex;
        }

        if (this.displayOnly)
        {
            Ext.applyIf(this,
            {
                editor:
                {
                    alignment: 't-t',
                    field:
                    {
                        xtype: this.displayOnlyFieldXType,
                        align: this.align,
                        colorClsField: this.colorClsField,
                        dataCase: this.dataCase,
                        encodeValue: this.encodeValue,
                        fieldCls: this.noWrap ? 'x-form-display-field cxgriddisplayfield' : 'x-form-display-field',
                        fieldLabel: this.text,
                        format: this.format,
                        hideLabel: true,
                        labelWidth: 0,
                        selectOnFocus: true,
                    }
                }
            });
        }
        else
        {
            if (this.noWrap)
            {
                Ext.applyIf(this,
                {
                    editor:
                    {
                        alignment: 't-t',
                        field:
                        {
                            xtype: this.fieldXType,
                            align: this.align,
                            allowBlank: this.allowBlank,
                            blankText: this.blankText,
                            colorClsField: this.colorClsField,
                            dataCase: this.dataCase,
                            emptyText: this.emptyText,
                            enforceFormat: this.enforceFormat,
                            enforceMandatory: this.enforceMandatory,
                            enforceMaxLength: this.enforceMaxLength,
                            fieldLabel: this.text,
                            format: this.format,
                            formatText: this.formatText,
                            hideLabel: true,
                            labelWidth: 0,
                            maxLength: this.maxLength,
                            maxLengthText: this.maxLengthText,
                            minLength: this.minLength,
                            minLengthText: this.minLengthText,
                            readOnly: this.readOnly,
                            selectOnFocus: true,
                        }
                    }
                });
            }
            else
            {
                Ext.applyIf(this,
                {
                    editor:
                    {
                        alignment: 't-t',
                        field:
                        {
                            xtype: this.textAreaFieldXType,
                            align: this.align,
                            allowBlank: this.allowBlank,
                            blankText: this.blankText,
                            emptyText: this.emptyText,
                            enforceMandatory: this.enforceMandatory,
                            enforceMaxLength: this.enforceMaxLength,
                            fieldLabel: this.text,
                            height: this.cellHeight,
                            hideLabel: true,
                            labelWidth: 0,
                            maxLength: this.maxLength,
                            maxLengthText: this.maxLengthText,
                            minLength: this.minLength,
                            minLengthText: this.minLengthText,
                            readOnly: this.readOnly,
                            selectOnFocus: true,
                        }
                    }
                });
            }
        }

        switch (this.summaryType)
        {
            case 'cxcount':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxCount(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmin':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMin(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmax':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMax(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxsum':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxSum(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxaverage':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxAverage(records, '{0}'); }", this.dataIndex));
                break;
        }

        this.callParent();

        // in ExtJS v4.1.1 scope is not initialized if a renderer is declared which makes the this reference in the summaryRenderer
        // the summary feature not the column.
        this.scope = this;
    },

    // Methods
    //=============================================================================
    //
    renderer: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        var result = value;
        var column;
        var editorField;
        var messageField;

        if (value && gridView)
        {
            column = this;

            if (column.encodeObject && (Ext.isObject(value) || Ext.isArray(value)))
            {
                result = Ext.htmlEncode(Ext.encode(value));
                metaData.tdAttr = 'data-qtip="' + result + '"';
            }
            else
            {
                if (column.getEditor)
                {
                    editorField = column.getEditor();
                }
                else
                {
                    // Create the field if it does not already exist.
                    editorField = Ext.ComponentManager.create(column.editor.field);
                    column.editor.field = editorField;
                }

                if (editorField.format)
                {
                    result = editorField.getFormattedValue(value);
                }

                if (editorField.colorClsField)
                {
                    if (record)
                    {
                        metaData.tdCls = record.get(editorField.colorClsField);
                    }
                }

                if (column.displayOnly)
                {
                    if (column.encodeValue)
                    {
                        result = Ext.htmlEncode(result);
                        result = result.replace(/&lt;br\/&gt;/g, '<br/>');
                    }

                    result = result.toString().replace(/\n/g, '<br/>');
                    // By replacing pairs of blanks we perserve whitespace without stopping word wrapping.
                    // This must be executed twice to catch any pairs created by the first replace.
                    result = result.toString().replace(/\s\s/g, '&nbsp;&nbsp; ');
                    result = result.toString().replace(/\s\s/g, '&nbsp;&nbsp; ');
                }
                else
                {
                    result = Ext.htmlEncode(result);
                }
            }
        }

        if (!this.noWrap)
        {
            metaData.style = 'white-space: normal; ';
        }

        if (this.cellHeight || this.maxCellHeight)
        {
            if (this.cellHeight)
            {
                metaData.style += Ext.String.format('height: {0}px; ', this.cellHeight);
            }
            else
            {
                metaData.style += Ext.String.format('max-height: {0}px; ', this.maxCellHeight);
            }

            if (this.autoScroll)
            {
                metaData.style += 'overflow: auto; ';
            }
        }

        messageField = Ext.String.format('{0}$ErrorMessages', this.dataIndex);

        if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
        {
            metaData.tdCls += ' cxgridcellerror';
        }
        else
        {
            messageField = Ext.String.format('{0}$WarningMessages', this.dataIndex);

            if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
            {
                metaData.tdCls += ' cxgridcellwarning';
            }
        }

        return result;
    },

    summaryRenderer: function (value, summaryData, field)
    {
        return Ext.String.format('<span class="cxgridsummary">{0}</span>', Ext.String.format(this.summaryText || '{0}', Ext.isEmpty(value) ? '' : value));
    }
});

// Computronix grid combobox column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.ComboBox',
{
    extend: 'Ext.grid.column.Column',
    alias: 'widget.computronixextcomboboxcolumn',

    // Data Members
    //=============================================================================
    //
    fieldXType: 'computronixextcombobox',
    accessLevelField: null,
    activeField: null,
    allowBlank: true,
    blankText: 'This field is required',
    dependentField: null,
    displayField: 'text',
    displayValueField: null,
    emptyText: null,
    enforceMandatory: true,
    forceSelection: true,
    initialData: null,
    multiSelect: false,
    readOnly: false,
    rowDependent: false,
    store: null,
    summaryText: '{0}',
    typeAhead: true,
    valueField: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = this.dataIndex;
        }

        this.accessLevelField = Ext.String.format('{0}$AccessLevel', this.dataIndex);

        Ext.applyIf(this,
        {
            editor:
            {
                alignment: 't-t',
                field:
                {
                    xtype: this.fieldXType,
                    activeField: this.activeField,
                    allowBlank: this.allowBlank,
                    blankText: this.blankText,
                    displayField: this.displayField,
                    emptyText: this.emptyText,
                    enforceMandatory: this.enforceMandatory,
                    fieldLabel: this.text,
                    forceSelection: this.forceSelection,
                    hideLabel: true,
                    labelWidth: 0,
                    multiSelect: this.multiSelect,
                    name: this.dataIndex,
                    readOnly: this.readOnly,
                    selectOnFocus: true,
                    store: this.store,
                    typeAhead: this.typeAhead,
                    valueField: this.valueField,

                    listConfig:
                    {
                        cls: 'grid-comboboxcolumn-dropdown'
                    },
                }
            }
        });

        switch (this.summaryType)
        {
            case 'cxcount':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxCount(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmin':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMin(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmax':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMax(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxsum':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxSum(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxaverage':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxAverage(records, '{0}'); }", this.dataIndex));
                break;
        }

        this.callParent();

        // in ExtJS v4.1.1 scope is not initialized if a renderer is declared which makes the this reference in the summaryRenderer
        // the summary feature not the column.
        this.scope = this;
    },

    // Methods
    //=============================================================================
    //
    getSortParam: function ()
    {
        var result = this.dataIndex;

        if (this.displayValueField)
        {
            result = this.displayValueField;
        }

        return result;
    },

    renderer: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        var result = null;
        var column;
        var editorField;
        var messageField;

        if (gridView)
        {
            column = this;

            // Do not try to resolve the display value if the field is invisible because a null value may map to a value like (None).
            if (record && record.get(column.accessLevelField) != 'Invisible')
            {
                if (column.getEditor)
                {
                    editorField = column.getEditor();
                }
                else
                {
                    // Create the field if it does not already exist.
                    editorField = Ext.ComponentManager.create(column.editor.field);
                    column.editor.field = editorField;
                }

                if (column.displayValueField)
                {
                    result = record.get(column.displayValueField);
                }
                else
                {
                    if (editorField.store)
                    {
                        result = Ext.htmlEncode(Computronix.Ext.getDisplayValue(editorField.store, value, editorField.displayField));
                    }
                }
            }
        }

        messageField = Ext.String.format('{0}$ErrorMessages', this.dataIndex);

        if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
        {
            metaData.tdCls += ' cxgridcellerror';
        }
        else
        {
            messageField = Ext.String.format('{0}$WarningMessages', this.dataIndex);

            if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
            {
                metaData.tdCls += ' cxgridcellwarning';
            }
        }

        return result;
    },

    summaryRenderer: function (value, summaryData, field)
    {
        return Ext.String.format('<span class="cxgridsummary">{0}</span>', Ext.String.format(this.summaryText || '{0}', Ext.isEmpty(value) ? '' : value));
    },

    updateDisplayValue: function ()
    {
        var displayValue;
        var editorField;

        if (this.displayValueField)
        {
            editorField = this.getEditor();
            displayValue = editorField.record.store.getDisplayValue(this.dataIndex, editorField.record.get(this.dataIndex));

            if (this.forceSelection || displayValue)
            {
                editorField.record.set(this.displayValueField, displayValue);
            }
            else
            {
                editorField.record.set(this.displayValueField, editorField.record.get(this.dataIndex));
            }
        }
    }
});

// Computronix grid date column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.Date',
{
    extend: 'Ext.grid.column.Date',
    alias: 'widget.computronixextdatecolumn',
    format: Computronix.Ext.dateFormat,

    // Data Members
    //=============================================================================
    //
    fieldXType: 'computronixextdatefield',
    allowBlank: true,
    blankText: 'This field is required',
    emptyText: null,
    enforceMandatory: true,
    readOnly: false,
    summaryText: '{0}',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = this.dataIndex;
        }

        Ext.applyIf(this,
        {
            editor:
            {
                alignment: 't-t',
                field:
                {
                    xtype: this.fieldXType,
                    allowBlank: this.allowBlank,
                    blankText: this.blankText,
                    emptyText: this.emptyText,
                    enforceMandatory: this.enforceMandatory,
                    fieldLabel: this.text,
                    format: this.format,
                    hideLabel: true,
                    labelWidth: 0,
                    readOnly: this.readOnly,
                    selectOnFocus: true,
                }
            }
        });

        switch (this.summaryType)
        {
            case 'cxcount':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxCount(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmin':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMin(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmax':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMax(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxsum':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxSum(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxaverage':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxAverage(records, '{0}'); }", this.dataIndex));
                break;
        }

        this.callParent();

        // in ExtJS v4.1.1 scope is not initialized if a renderer is declared which makes the this reference in the summaryRenderer
        // the summary feature not the column.
        this.scope = this;
    },

    // Methods
    //=============================================================================
    //
    renderer: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        var result = null;
        var messageField;

        if (Ext.isDate(value) && this.format)
        {
            result = Ext.util.Format.date(value, this.format);
        }

        messageField = Ext.String.format('{0}$ErrorMessages', this.dataIndex);

        if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
        {
            metaData.tdCls += ' cxgridcellerror';
        }
        else
        {
            messageField = Ext.String.format('{0}$WarningMessages', this.dataIndex);

            if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
            {
                metaData.tdCls += ' cxgridcellwarning';
            }
        }

        return result;
    },

    summaryRenderer: function (value, summaryData, record, recordIndex, columnIndex, store)
    {
        if (Ext.isDate(value) && this.format)
        {
            value = Ext.util.Format.date(value, this.format);
        }

        return Ext.String.format('<span class="cxgridsummary">{0}</span>', Ext.String.format(this.summaryText || '{0}', Ext.isEmpty(value) ? '' : value));
    }
});

// Computronix delete action column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.DeleteAction',
{
    extend: 'Ext.grid.column.Action',
    alias: 'widget.computronixextdeleteactioncolumn',
    iconCls: 'cxicon cxdeleteicon',
    disabledIconCls: 'cxicon cxdisableddeleteicon',
    menuDisabled: true,
    hideable: false,
    draggable: false,
    resizable: false,
    align: 'center',
    maxWidth: 29,
    altText: 'Delete',

    handler: function (gridView, rowIndex, columnIndex, action, event, record)
    {
        if (gridView.panel.canDelete(record))
        {
            gridView.panel.deleteRow(gridView, rowIndex, columnIndex, action, event, record);
        }
    },
    scope: this,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = '_deleteAction';
        }

        this.callParent();

        // in ExtJS v4.1.1 scope is not initialized if a renderer is declared which makes the this reference in the summaryRenderer
        // the summary feature not the column.
        this.scope = this;
    },

    // Methods
    //=============================================================================
    //
    getClass: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        if (gridView.panel.canDelete(record))
        {
            result = this.iconCls;
        }
        else
        {
            result = this.disabledIconCls;
        }

        return result;
    }
});

// Computronix edit action column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.EditAction',
{
    extend: 'Ext.grid.column.Action',
    alias: 'widget.computronixexteditactioncolumn',
    menuDisabled: true,
    hideable: false,
    locked: true,
    draggable: false,
    resizable: false,
    align: 'center',
    handler: function (gridView, rowIndex, columnIndex, action, event, record)
    {
        if (gridView.panel.editMode == "rowActionDialog" ||
            gridView.panel.editMode == "hybrid" ||
            gridView.panel.editMode != "cellError" &&
            (!this.canEditField || record.get(this.canEditField)))
        {
            gridView.panel.editRow(gridView, rowIndex, columnIndex, action, event, record);
        }
    },
    scope: this,

    // Data Members
    //=============================================================================
    //
    canEditField: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = '_editAction';
        }

        this.width = 29;

        this.callParent();

        // in ExtJS v4.1.1 scope is not initialized if a renderer is declared which makes the this reference in the summaryRenderer
        // the summary feature not the column.
        this.scope = this;

        this.addListener('afterrender', this.onEditActionColumnAfterRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onEditActionColumnAfterRender: function (column)
    {
        var panel = this.up('gridpanel');

        if (panel && panel.editMode != 'hybrid' && panel.editMode != 'rowActionDialog')
        {
            panel.addListener('itemdblclick', this.onEditActionColumnGridItemDblClick, this);
        }
    },

    onEditActionColumnGridItemDblClick: function (gridView, record, htmlElement, rowIndex, event)
    {
        if (!gridView.panel.editor && gridView.panel.editMode != "cellError" &&
            (!this.canEditField || record.get(this.canEditField)))
        {
            gridView.panel.editRow(gridView, rowIndex, null, this, event, record);
        }
    },

    // Methods
    //=============================================================================
    //
    getClass: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        var result = null;
        var errorMessages = /\$ErrorMessages$/;
        var warningMessages = /\$WarningMessages$/;
        var name;
        var hasRowMessage = false;
        var done = false;

        if (gridView.panel.editMode == "rowActionDialog" ||
            gridView.panel.editMode == "hybrid" ||
            (!this.canEditField || record.get(this.canEditField))
            )
        {
            if (gridView.panel.editMode == "cellError")
            {
                result = 'cxdefault';
            }
            else
            {
                result = 'cxicon cxediticon';
            }

            name = Ext.String.format('{0}$ErrorMessages', record.idProperty);

            if (record.fieldsByName.containsKey(name))
            {
                hasRowMessage = true;

                if (!Ext.isEmpty(record.get(name)))
                {
                    if (gridView.panel.editMode == "cellError")
                    {
                        result = 'cxicon cxerroricon cxdefault';
                    }
                    else
                    {
                        result = 'cxicon cxediterroricon';
                    }

                    done = true;
                }
            }

            if (!done)
            {
                name = Ext.String.format('{0}$WarningMessages', record.idProperty);

                if (record.fieldsByName.containsKey(name))
                {
                    hasRowMessage = true;

                    if (!Ext.isEmpty(record.get(name)))
                    {
                        if (gridView.panel.editMode == "cellError")
                        {
                            result = 'cxicon cxerroricon cxdefault';
                        }
                        else
                        {
                            result = 'cxicon cxediterroricon';
                        }

                        done = true;
                    }
                }

                if (!done && !hasRowMessage)
                {
                    record.fieldsByName.each(function (field)
                    {
                        if (errorMessages.test(field.name))
                        {
                            if (record.get(field.name))
                            {
                                if (gridView.panel.editMode == "cellError")
                                {
                                    result = 'cxicon cxerroricon cxdefault';
                                }
                                else
                                {
                                    result = 'cxicon cxediterroricon';
                                }

                                done = true;

                                return false;
                            }
                        }

                        if (!done && warningMessages.test(field.name))
                        {
                            if (record.get(field.name))
                            {
                                if (gridView.panel.editMode == "cellError")
                                {
                                    result = 'cxicon cxerroricon cxdefault';
                                }
                                else
                                {
                                    result = 'cxicon cxediterroricon';
                                }

                                return false;
                            }
                        }
                    }, this);
                }
            }
        }
        else
        {
            if (gridView.panel.editMode == "cellError")
            {
                result = 'cxdefault';
            }
            else
            {
                result = 'cxicon cxdisabledediticon';
            }
        }

        return result;
    },

    getTip: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        var result = '';
        var errorMessages = /\$ErrorMessages$/;
        var warningMessages = /\$WarningMessages$/;
        var name;
        var messages;
        var hasRowMessage = false;

        if (!this.canEditField || record.get(this.canEditField))
        {
            name = Ext.String.format('{0}$ErrorMessages', record.idProperty);

            if (record.fieldsByName.containsKey(name))
            {
                hasRowMessage = true;
                messages = Ext.String.trim(record.get(name));

                if (!Ext.isEmpty(messages))
                {
                    Ext.each(Ext.Array.from(messages), function (message)
                    {
                        result += Ext.String.format('<li class="cxerrortooltip">{0}</li>', message);
                    }, this);
                }
            }

            name = Ext.String.format('{0}$WarningMessages', record.idProperty);

            if (record.fieldsByName.containsKey(name))
            {
                hasRowMessage = true;
                messages = Ext.String.trim(record.get(name));

                if (!Ext.isEmpty(messages))
                {
                    Ext.each(Ext.Array.from(messages), function (message)
                    {
                        result += Ext.String.format('<li class="cxwarningtooltip">{0}</li>', message);
                    }, this);

                    done = true;
                }
            }

            if (!hasRowMessage)
            {
                record.fieldsByName.each(function (field)
                {
                    if (errorMessages.test(field.name))
                    {
                        messages = Ext.String.trim(record.get(field.name));

                        if (!Ext.isEmpty(messages))
                        {
                            Ext.each(Ext.Array.from(messages), function (message)
                            {
                                result += Ext.String.format('<li class="cxerrortooltip">{0}</li>', message);
                            }, this);
                        }
                    }

                    if (warningMessages.test(field.name))
                    {
                        messages = Ext.String.trim(record.get(field.name));

                        if (!Ext.isEmpty(messages))
                        {
                            Ext.each(Ext.Array.from(messages), function (message)
                            {
                                result += Ext.String.format('<li class="cxwarningtooltip">{0}</li>', message);
                            }, this);
                        }
                    }
                }, this);
            }
        }

        if (result.length > 0)
        {
            result = Ext.String.htmlEncode(Ext.String.format('<ul class="x-list-plain">{0}</ul>', result));
        }

        return result;
    }
});

// Computronix grid editmask field column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.EditMask',
{
    extend: 'Ext.grid.column.Column',
    alias: 'widget.computronixexteditmaskcolumn',

    // Data Members
    //=============================================================================
    //
    fieldXType: 'computronixexteditmask',
    allowBlank: true,
    blankText: 'This field is required',
    dataCase: null,
    displayValueField: null,
    editMask: null,
    emptyText: null,
    enforceCompleteMask: true,
    enforceMandatory: true,
    enforceMask: true,
    maskText: 'The value does not match mask',
    readOnly: false,
    showMask: false,
    summaryText: '{0}',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = this.dataIndex;
        }

        Ext.applyIf(this,
        {
            editor:
            {
                alignment: 't-t',
                field:
                {
                    xtype: this.fieldXType,
                    allowBlank: this.allowBlank,
                    blankText: this.blankText,
                    columnHeight: 17,
                    dataCase: this.dataCase,
                    editMask: this.editMask,
                    emptyText: this.emptyText,
                    enforceCompleteMask: this.enforceCompleteMask,
                    enforceMandatory: this.enforceMandatory,
                    enforceMask: this.enforceMask,
                    fieldLabel: this.text,
                    height: Ext.isIE ? 21 : 20,  // Fit comfortably within the grid column
                    hideLabel: true,
                    labelWidth: 0,
                    margin: '-0.5 0 0 0',
                    maskText: this.maskText,
                    readOnly: this.readOnly,
                    selectOnFocus: true,
                    showMask: this.showMask,
                }
            }
        });

        switch (this.summaryType)
        {
            case 'cxcount':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxCount(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmin':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMin(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmax':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMax(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxsum':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxSum(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxaverage':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxAverage(records, '{0}'); }", this.dataIndex));
                break;
        }

        this.callParent();

        // in ExtJS v4.1.1 scope is not initialized if a renderer is declared which makes the this reference in the summaryRenderer
        // the summary feature not the column.
        this.scope = this;
    },

    // Methods
    //=============================================================================
    //
    getSortParam: function ()
    {
        var result = this.dataIndex;

        if (this.displayValueField)
        {
            result = this.displayValueField;
        }

        return result;
    },

    renderer: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        var result = value;
        var column;
        var editorField;
        var messageField;

        if (value && gridView)
        {
            column = this;

            if (column.getEditor)
            {
                editorField = column.getEditor();
            }
            else
            {
                // Create the field if it does not already exist.
                editorField = Ext.ComponentManager.create(column.editor.field);
                column.editor.field = editorField;
            }

            if (column.displayValueField)
            {
                result = record.get(column.displayValueField);
            }
            else
            {
                if (editorField.editMask)
                {
                    result = editorField.getFormattedValue(value);
                }
            }

            result = Ext.htmlEncode(result);
        }

        messageField = Ext.String.format('{0}$ErrorMessages', this.dataIndex);

        if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
        {
            metaData.tdCls += ' cxgridcellerror';
        }
        else
        {
            messageField = Ext.String.format('{0}$WarningMessages', this.dataIndex);

            if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
            {
                metaData.tdCls += ' cxgridcellwarning';
            }
        }

        return result;
    },

    summaryRenderer: function (value, summaryData, field)
    {
        var editorField;

        if (this.getEditor)
        {
            editorField = this.getEditor();
        }
        else
        {
            // Create the field if it does not already exist.
            editorField = Ext.ComponentManager.create(this.editor.field);
            this.editor.field = editorField;
        }

        if (value && editorField.editMask && (!this.summaryType || this.summaryType.toString().toLowerCase().indexOf('count') == -1))
        {
            value = editorField.getFormattedValue(value);
        }

        return Ext.String.format('<span class="cxgridsummary">{0}</span>', Ext.String.format(this.summaryText || '{0}', Ext.isEmpty(value) ? '' : value));
    },

    updateDisplayValue: function ()
    {
        var editorField;

        if (this.displayValueField)
        {
            editorField = this.getEditor();
            editorField.record.set(this.displayValueField, editorField.getFormattedValue(editorField.record.get(this.dataIndex)));
        }
    }
});

// Computronix image column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.Image',
{
    extend: 'Ext.grid.column.Column',
    alias: 'widget.computronixextimagecolumn',
    groupable: false,
    align: 'center',

    // Data Members
    //=============================================================================
    //
    tooltip: 'Click to Enlarge',
    target: '_blank',
    thumbField: null,
    imageWidth: 16,
    imageHeight: 16,
    summaryText: '{0}',
    displayOnly: true,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = this.dataIndex;
        }

        switch (this.summaryType)
        {
            case 'cxcount':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxCount(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmin':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMin(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmax':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMax(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxsum':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxSum(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxaverage':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxAverage(records, '{0}'); }", this.dataIndex));
                break;
        }

        this.callParent();

        // in ExtJS v4.1.1 scope is not initialized if a renderer is declared which makes the this reference in the summaryRenderer
        // the summary feature not the column.
        this.scope = this;
    },

    // Methods
    //=============================================================================
    //
    renderer: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        var result = null;
        var column;

        if (gridView)
        {
            column = this;

            if (value)
            {
                result = Ext.String.format(
                    '<a href="{0}" target="{1}" tabIndex="-1" data-qtip="{2}" onClick="event.cancelBubble=true;" onContextMenu="event.cancelBubble=true;"><img width="{3}px" height="{4}px" src="{5}"/></a>',
                    value, column.target, column.tooltip, column.imageWidth, column.imageHeight,
                    (record && column.thumbField && record.get(column.thumbField)) ? record.get(column.thumbField) : value);
            }
        }

        return result;
    },

    summaryRenderer: function (value, summaryData, field)
    {
        return Ext.String.format('<span class="cxgridsummary">{0}</span>', Ext.String.format(this.summaryText || '{0}', Ext.isEmpty(value) ? '' : value));
    }
});

// Computronix link column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.Link',
{
    extend: 'Ext.grid.column.Column',
    alias: 'widget.computronixextlinkcolumn',
    text: 'Goto',

    // Data Members
    //=============================================================================
    //
    fieldXType: 'computronixextdisplayfield',
    accessLevelField: null,
    displayOnly: true,
    displayValueField: null,
    groupable: false,
    iconCls: null,
    iconClsField: null,
    summaryText: '{0}',
    target: '_blank',
    tooltip: 'Click to open in a new window',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = this.dataIndex;
        }

        this.accessLevelField = Ext.String.format('{0}$AccessLevel', this.dataIndex);

        Ext.applyIf(this,
        {
            editor:
            {
                alignment: 't-t',
                field:
                {
                    xtype: this.fieldXType,
                    fieldCls: 'x-form-display-field cxgriddisplayfield',
                    fieldLabel: this.text,
                    hideLabel: true,
                    labelWidth: 0,
                    selectOnFocus: true,
                }
            }
        });

        switch (this.summaryType)
        {
            case 'cxcount':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxCount(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmin':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMin(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmax':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMax(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxsum':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxSum(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxaverage':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxAverage(records, '{0}'); }", this.dataIndex));
                break;
        }

        this.callParent();

        // in ExtJS v4.1.1 scope is not initialized if a renderer is declared which makes the this reference in the summaryRenderer
        // the summary feature not the column.
        this.scope = this;

        if (Ext.isEmpty(this.tooltip))
        {
            this.tooltip = '';
        }
    },

    // Methods
    //=============================================================================
    //
    getSortParam: function ()
    {
        var result = this.dataIndex;

        if (this.displayValueField)
        {
            result = this.displayValueField;
        }

        return result;
    },

    renderer: function (value, metaData, record, rowIndex, columnIndex, store, gridView)
    {
        var result = null;
        var column;

        if (gridView)
        {
            column = this;

            if (record && record.get(column.accessLevelField) != 'Invisible')
            {
                if (value && record.get(column.accessLevelField) != 'Disabled')
                {
                    if (record.get(column.iconClsField) || column.iconCls)
                    {
                        result = Ext.String.format(
                            '<a href="{0}" target="{1}" class="cxlink" tabIndex="-1" data-qtip="{2}" onClick="event.cancelBubble=true;" onContextMenu="event.cancelBubble=true;"><img class="cxicon {4}" style="margin: 0px 4px 0px 0px" src="Images/Blank.gif"/><span class="cxlink">{3}</span></a>',
                            value, column.target, column.tooltip, column.displayValueField ? record.get(column.displayValueField) : value, record.get(column.iconClsField) || column.iconCls);
                    }
                    else
                    {
                        result = Ext.String.format(
                            '<a href="{0}" target="{1}" class="cxlink" tabIndex="-1" data-qtip="{2}" onClick="event.cancelBubble=true;" onContextMenu="event.cancelBubble=true;">{3}</a>',
                            value, column.target, column.tooltip, column.displayValueField ? record.get(column.displayValueField) : value);
                    }
                }
                else
                {
                    if (record.get(column.iconClsField) || column.iconCls)
                    {
                        result = Ext.String.format(
                            '<span class="cxlink" data-qtip="{0}"><img class="cxicon {2}" style="margin: 0px 4px 0px 0px" src="Images/Blank.gif"/><span class="cxlink">{1}</span></span>',
                            column.tooltip, column.displayValueField ? record.get(column.displayValueField) : '', record.get(column.iconClsField) || column.iconCls);
                    }
                    else
                    {
                        if (column.displayValueField)
                        {
                            result = Ext.String.format('<span class="cxlink" data-qtip="{0}">{1}</span>',
                                column.tooltip, record.get(column.displayValueField));
                        }
                    }
                }
            }
        }

        return result;
    },

    summaryRenderer: function (value, summaryData, field)
    {
        return Ext.String.format('<span class="cxgridsummary">{0}</span>', Ext.String.format(this.summaryText || '{0}', Ext.isEmpty(value) ? '' : value));
    }
});

// Computronix grid number field column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.column.Number',
{
    extend: 'Ext.grid.column.Number',
    alias: 'widget.computronixextnumbercolumn',
    format: '0,000.00',

    // Data Members
    //=============================================================================
    //
    fieldXType: 'computronixextnumberfield',
    align: 'right',
    allowBlank: true,
    allowDecimals: true,
    blankText: 'This field is required',
    colorClsField: null,
    decimalPrecision: 2,
    emptyText: null,
    enforceMandatory: true,
    enforceMaxLength: false,
    enforceStep: false,
    hideTrigger: false,
    isUnconstrained: false,
    maxLength: Number.MAX_VALUE,
    maxLengthText: 'The maximum length for this field is {0}',
    maxText: 'The maximum value for this field is {0}',
    maxValue: Number.MAX_VALUE,
    minLength: 0,
    minLengthText: 'The minimum length for this field is {0}',
    minText: 'The minimum value for this field is {0}',
    minValue: Number.NEGATIVE_INFINITY,  // FUTURE: -Number.MAX_VALUE?
    readOnly: false,
    step: 1,
    stepText: 'The value for this field must be a multiple of {0}',
    summaryText: '{0}',

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (!this.stateId)
        {
            this.stateId = this.dataIndex;
        }

        Ext.applyIf(this,
        {
            editor:
            {
                alignment: 't-t',
                field:
                {
                    xtype: this.fieldXType,
                    align: this.align,
                    allowBlank: this.allowBlank,
                    allowDecimals: this.allowDecimals,
                    blankText: this.blankText,
                    colorClsField: this.colorClsField,
                    decimalPrecision: this.decimalPrecision,
                    emptyText: this.emptyText,
                    enforceMandatory: this.enforceMandatory,
                    enforceMaxLength: this.enforceMaxLength,
                    enforceStep: this.enforceStep,
                    fieldLabel: this.text,
                    format: this.format,
                    hideLabel: true,
                    hideTrigger: this.hideTrigger,
                    isUnconstrained: this.isUnconstrained,
                    labelWidth: 0,
                    maxLength: this.maxLength,
                    maxLengthText: this.maxLengthText,
                    maxText: this.maxText,
                    maxValue: this.maxValue,
                    minLength: this.minLength,
                    minLengthText: this.minLengthText,
                    minText: this.minText,
                    minValue: this.minValue,
                    negativeText: this.negativeText,
                    readOnly: this.readOnly,
                    selectOnFocus: true,
                    step: this.step,
                    stepText: this.stepText,
                }
            }
        });

        switch (this.summaryType)
        {
            case 'cxcount':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxCount(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmin':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMin(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxmax':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxMax(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxsum':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxSum(records, '{0}'); }", this.dataIndex));
                break;
            case 'cxaverage':
                eval(Ext.String.format("this.summaryType = function (records) { return this.getCxAverage(records, '{0}'); }", this.dataIndex));
                break;
        }

        this.callParent();
    },

    // This executes after initComponent.
    constructor: function (config)
    {
        this.callParent(arguments);

        // This is defined in the constructor because it needs to be created after the ancestor constuctor
        // has run, or it would get replaced by the one added by the ancestor.
        this.renderer = function (value, metaData, record, rowIndex, columnIndex, store, gridView)
        {
            var result = value;
            var column;
            var editorField;
            var messageField;

            if (!Ext.isEmpty(value) && gridView)
            {
                column = this;

                if (column.getEditor)
                {
                    editorField = column.getEditor();
                }
                else
                {
                    // Create the field if it does not already exist.
                    editorField = Ext.ComponentManager.create(column.editor.field);
                    column.editor.field = editorField;
                }

                if (editorField.format)
                {
                    result = editorField.getFormattedValue(value);
                }

                if (editorField.colorClsField)
                {
                    if (!Ext.isEmpty(value) && record)
                    {
                        metaData.tdCls = record.get(editorField.colorClsField);
                    }
                }
            }

            messageField = Ext.String.format('{0}$ErrorMessages', this.dataIndex);

            if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
            {
                metaData.tdCls += ' cxgridcellerror';
            }
            else
            {
                messageField = Ext.String.format('{0}$WarningMessages', this.dataIndex);

                if (record && record.fieldsByName.containsKey(messageField) && record.get(messageField))
                {
                    metaData.tdCls += ' cxgridcellwarning';
                }
            }

            return result;
        };
    },

    // Methods
    //=============================================================================
    //
    summaryRenderer: function (value, summaryData, field)
    {
        var editorField;

        if (this.getEditor)
        {
            editorField = this.getEditor();
        }
        else
        {
            // Create the field if it does not already exist.
            editorField = Ext.ComponentManager.create(this.editor.field);
            this.editor.field = editorField;
        }

        if (!Ext.isEmpty(value) && editorField.format && (!this.summaryType || this.summaryType.toString().toLowerCase().indexOf('count') == -1))
        {
            value = editorField.getFormattedValue(value);
        }

        return Ext.String.format('<span class="cxgridsummary">{0}</span>', Ext.String.format(this.summaryText || '{0}', Ext.isEmpty(value) ? '' : value));
    }
});

Ext.define('Computronix.Ext.ToolbarFieldContainer',
{
    extend: 'Ext.container.Container',
    alias: 'widget.computronixexttoolbarfieldcontainer',

    focusable: true,

    focusCls: 'toolbar-field-container-focus',
    layout: 'fit',

    field: null,

    initComponent: function ()
    {
        this.items =
        [
            this.field,
        ];

        if (this.field)
        {
            this.field.setTabIndex(-1);
        }

        this.callParent(arguments);
    },

    initEvents: function ()
    {
        new Ext.util.KeyNav(
        {
            target: this.getEl(),
            enter: this.onEnterKeyPressed.bind(this),
            esc: this.onEscKeyPressed.bind(this),
        });
    },

    onEnterKeyPressed: function (e)
    {
        if (this.field)
        {
            this.field.focus();
        }
    },

    onEscKeyPressed: function (e)
    {
        var activeElement = Ext.dom.Element.getActiveElement(true);

        if (this.field && this.field.inputEl == activeElement)
        {
            this.focus();
            e.stopEvent();
        }
    },

});

// Computronix grid panel extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.Panel',
{
    extend: 'Ext.grid.Panel',
    alias: 'widget.computronixextgrid',

    bodyStyle: { 'background-color': 'transparent' },
    bufferedRenderer: false,
    columnLines: true,
    viewConfig: null,

    // Data Members
    //=============================================================================
    //
    allowDeselect: false,
    canDeleteFieldSuffix: '$CanDelete',
    canEditField: null,
    canEditFieldSuffix: '$CanEdit',
    checkOnly: true,
    checkboxSelect: false,
    contextMenu: null,
    countLabel: null,
    deleteButton: null,
    dependentLoadMethod: null,
    editActionColumnXType: 'computronixexteditactioncolumn',
    editMask: null,
    editMaskMessage: 'Loading...',
    editMode: null,
    editor: null,
    enableDelete: false,
    enableFilter: false,
    enableGrouping: false,
    enableNew: false,
    enablePrint: false,
    enableSummary: false,
    features: null,
    filterText: null,
    filterTextContainer: null,
    hideGroupedHeader: false,
    iconCls: null,
    ignoreChanges: true,
    ignoreError: false,
    isGrid: true,
    markDirty: false,
    newButton: null,
    newRecord: null,
    paneId: null,
    printButton: null,
    printCssFiles: null,
    printFeatures: 'scrollbars, resizable, toolbar, menubar',
    printHtml: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">{0}</html>',
    printTitle: null,
    singleSelect: true,
    storeId: null,
    titleLabel: null,
    titleText: null,
    toolbar: null,
    toolbarItems: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        var gridView;
        var selectionModel;
        var store;

        // Can not be initialized when the property is declared because the array reference would be global.
        this.features = [];
        this.toolbarItems = this.toolbarItems || [];
        this.printCssFiles = ['Styles/Computronix.Ext.css'];

        this.viewConfig = this.viewConfig || {};
        Ext.applyIf(this.viewConfig,
        {
            loadMask: false,
            stripeRows: true,
        });

        switch (this.editMode)
        {
            case 'cell':
                if (!this.editor)
                {
                    this.editor = Ext.create('Ext.grid.plugin.CellEditing', { clicksToEdit: 1 });
                }

                this.editor.addListener('beforeedit', this.onGridPanelBeforeEdit, this);
                this.editor.addListener('edit', this.onGridPanelEdit, this);
                this.editor.addListener('canceledit', this.onGridPanelCancelEdit, this);

                Ext.applyIf(this, { plugins: [this.editor] });

                // If disableSelection is true then random rows are selected when cells are edited.
                this.disableSelection = false;
                break;
            case 'cellError':
                if (!this.editor)
                {
                    this.editor = Ext.create('Ext.grid.plugin.CellEditing', { clicksToEdit: 1 });
                }

                this.editor.addListener('beforeedit', this.onGridPanelBeforeEdit, this);
                this.editor.addListener('edit', this.onGridPanelEdit, this);
                this.editor.addListener('canceledit', this.onGridPanelCancelEdit, this);

                Ext.applyIf(this, { plugins: [this.editor] });

                // If disableSelection is true then random rows are selected when cells are edited.
                this.disableSelection = false;
                break;
            case 'row':
                if (!this.editor)
                {
                    this.editor = Ext.create('Ext.grid.plugin.RowEditing', { clicksToEdit: 1, messageSummary: false, autoCancel: false });
                }

                this.editor.addListener('beforeedit', this.onGridPanelBeforeEdit, this);
                this.editor.addListener('edit', this.onGridPanelEdit, this);
                this.editor.addListener('canceledit', this.onGridPanelCancelEdit, this);

                Ext.applyIf(this, { plugins: [this.editor] });
                break;
            case 'rowAction':
                if (!this.editor)
                {
                    this.editor = Ext.create('Ext.grid.plugin.RowEditing', { clicksToEdit: 2, errorSummary: false, autoCancel: false });
                }

                this.editor.addListener('beforeedit', this.onGridPanelBeforeEdit, this);
                this.editor.addListener('edit', this.onGridPanelEdit, this);
                this.editor.addListener('canceledit', this.onGridPanelCancelEdit, this);

                Ext.applyIf(this, { plugins: [this.editor] });
                break;
        }

        this.titleLabel = new Computronix.Ext.toolbar.TextItem({ text: this.titleText, flex: 1 });

        if (this.enablePrint)
        {
            this.printButton = this.printButton || new Computronix.Ext.button.Button(
            {
                text: 'Print',
                ariaLabel: 'Print',
                iconCls: 'cxprinticon',
                listeners: { click: { fn: this.onGridPanelPrintButtonClick, scope: this } }
            });

            this.toolbarItems.push(this.printButton);
        }

        if (this.enableNew)
        {
            this.newButton = this.newButton || new Computronix.Ext.button.Button(
            {
                text: 'New',
                ariaLabel: 'New',
                iconCls: 'cxnewicon',
                listeners: { click: { fn: this.onGridPanelNewButtonClick, scope: this } }
            });

            this.toolbarItems.push(this.newButton);
        }

        if (this.enableDelete)
        {
            this.deleteButton = this.deleteButton || new Computronix.Ext.button.Button(
            {
                text: 'Delete',
                ariaLabel: 'Delete',
                iconCls: 'cxdeleteicon',
                disabled: true,
                listeners: { click: { fn: this.onGridPanelDeleteButtonClick, scope: this } }
            });

            this.toolbarItems.push(this.deleteButton);
        }

        if (this.enableFilter)
        {
            this.countLabel = new Ext.toolbar.TextItem(
            {
                padding: '0 4 0 0',
            });

            this.filterText = new Ext.form.field.Text(
            {
                emptyText: 'Text Search...',
                selectOnFocus: true,
                ui: 'toolbar',
                width: 110,

                listeners:
                {
                    change:
                    {
                        fn: this.onGridPanelFilterTextChange,
                        scope: this,
                    },
                },

                triggers:
                {
                    clear:
                    {
                        cls: 'x-form-clear-trigger',
                        handler: function (event)
                        {
                            this.setValue(null);
                        },
                    },
                },
            });

            this.filterTextContainer = new Computronix.Ext.ToolbarFieldContainer(
            {
                field: this.filterText,
            });

            this.toolbarItems = [this.countLabel, this.filterTextContainer].concat(this.toolbarItems);
        }

        if (this.titleText || this.toolbarItems.length > 0)
        {
            this.toolbarItems = [this.titleLabel].concat(this.toolbarItems);
        }

        if (this.iconCls)
        {
            this.toolbarItems = [new Ext.Component({ cls: Ext.String.format('{0} cxicon', this.iconCls) })].concat(this.toolbarItems);

            // iconCls must be removed or the base panel will create a titlebar for it.
            delete this.iconCls;
        }

        this.setToolbar();

        if (this.enableGrouping)
        {
            this.features.push(
            {
                ftype: 'grouping',
                hideGroupedHeader: this.hideGroupedHeader,
                groupByText: 'Group by this column',
                startCollapsed: false,
                groupHeaderTpl: '{name} ({rows.length})',
                enableGroupingMenu: true
            });
        }

        if (this.enableSummary)
        {
            this.features.push(
            {
                ftype: 'summary',
                dock: 'bottom',
            });
        }

        if (!this.disableSelection && this.checkboxSelect)
        {
            this.selType = 'checkboxmodel';
        }

        if (!this.columns)
        {
            this.columns = [{ xtype: 'rownumberer', width: 35 }];
            store = Ext.data.StoreManager.lookup(this.storeId);

            store.getNewModel().fieldsByName.each(function (field)
            {
                switch (field.type)
                {
                    case 'auto':
                        this.columns.push(
                        {
                            text: Ext.String.capitalize(field.name), dataIndex: field.name, width: 150,
                            xtype: 'computronixexttextcolumn', encodeObject: true
                        });
                        break;
                    case 'string':
                        this.columns.push(
                        {
                            text: Ext.String.capitalize(field.name), dataIndex: field.name, width: 150,
                            xtype: 'computronixexttextcolumn'
                        });
                        break;
                    case 'int':
                        this.columns.push(
                        {
                            text: Ext.String.capitalize(field.name), dataIndex: field.name, width: 150,
                            xtype: 'computronixextnumbercolumn', decimalPrecision: 0
                        });
                        break;
                    case 'float':
                        this.columns.push(
                        {
                            text: Ext.String.capitalize(field.name), dataIndex: field.name, width: 150,
                            xtype: 'computronixextnumbercolumn', format: '0,000.000'
                        });
                        break;
                    case 'boolean':
                        this.columns.push(
                        {
                            text: Ext.String.capitalize(field.name), dataIndex: field.name, width: 150,
                            xtype: 'computronixextcheckboxcolumn'
                        });
                        break;
                    case 'date':
                        this.columns.push(
                        {
                            text: Ext.String.capitalize(field.name), dataIndex: field.name, width: 150,
                            xtype: 'computronixextdatecolumn', format: field.dateFormat
                        });
                        break;
                }
            }, this);
        }

        this.callParent(arguments);

        gridView = this.getView();

        gridView.markDirty = this.markDirty;

        if (this.emptyText)
        {
            gridView.emptyText = Ext.String.format('<center><span class="cxemptytext">{0}</span></center>', this.emptyText);
        }

        if (this.editMode == 'rowAction' || this.editMode == 'rowActionDialog' || this.editMode == 'cellError')
        {
            this.insertColumn(0, { xtype: this.editActionColumnXType, canEditField: this.canEditField });
        }

        if (!this.disableSelection)
        {
            selectionModel = this.getSelectionModel();

            if (this.singleSelect)
            {
                selectionModel.setSelectionMode('SINGLE');

                if (this.checkboxSelect)
                {
                    selectionModel.showHeaderCheckbox = false;
                }
            }
            else
            {
                selectionModel.setSelectionMode('SIMPLE');
            }

            selectionModel.allowDeselect = this.allowDeselect;
            selectionModel.checkOnly = this.checkOnly;

            if (!this.ignoreChanges)
            {
                this.addListener('beforeselect', this.onGridPanelBeforeSelect, this);
            }

            this.addListener('selectionchange', this.onGridPanelSelectionChange, this);
        }

        if (this.contextMenu)
        {
            if (typeof this.contextMenu == 'string')
            {
                this.contextMenu = Ext.menu.Manager.get(this.contextMenu);
            }

            this.addListener('itemcontextmenu', this.onGridPanelItemContextMenu, this);
        }

        this.addListener('added', this.onGridPanelAdded, this);
        this.addListener('afterrender', this.onGridPanelAfterRender, this);
        this.addListener('boxready', this.onGridPanelBoxReady, this);
        this.addListener('headerclick', this.onGridPanelHeaderClick, this);
        gridView.addListener('refresh', this.onGridPanelViewRefresh, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onGridPanelAdded: function ()
    {
        this._resolveStore();
        this.addManagedListener(this.store, 'load', this.onGridPanelLoad, this);
    },

    onGridPanelAfterRender: function (grid)
    {
        if (!this.disableSelection && this.checkboxSelect)
        {
            this.headerCt.items.first().stateId = '_checkboxSelect';
        }

        if (Computronix.Ext.showPaneId && Computronix.Ext.SystemMonitor
            && Computronix.Ext.SystemMonitor.instance && this.paneId)
        {
            new Ext.tip.ToolTip(
            {
                target: this.el,
                html: Ext.String.format('Pane Id: [{0}]', this.paneId),
                showDelay: 525,
                listeners:
                {
                    beforeshow:
                    {
                        // Only show the tooltip if the ctrl key is pressed.
                        fn: function (tip)
                        {
                            return Computronix.Ext.keys.ctrl;
                        }
                    }
                }
            });
        }
    },

    onGridPanelBoxReady: function (grid)
    {
        // Do not allow columns to be moved beyond a non-movable column.
        if (this.enableColumnMove)
        {
            this.headerCt.reorderer.dropZone.onNodeOver = Ext.Function.createInterceptor(this.headerCt.reorderer.dropZone.onNodeOver,
            function (node, source, event, data)
            {
                var result = true;

                if (data.dropLocation)
                {
                    if (!data.dropLocation.header.draggable)
                    {
                        this.positionIndicator(data, node, event);
                        result = false;
                    }
                }

                return result;
            }, this.headerCt.reorderer.dropZone, this.dropNotAllowed);

            this.headerCt.reorderer.dropZone.onNodeDrop = Ext.Function.createInterceptor(this.headerCt.reorderer.dropZone.onNodeDrop,
            function (node, source, event, data)
            {
                var result = true;

                if (data.dropLocation)
                {
                    if (!data.dropLocation.header.draggable)
                    {
                        result = false;
                    }
                }

                return result;
            }, this.headerCt.reorderer.dropZone);
        }
    },

    // In v4.1.0 the arguments are reversed so editor is second.
    onGridPanelBeforeEdit: function (editing, editor)
    {
        var result = true;
        var editField;
        var accessLevel = /\$AccessLevel$/;
        var errorMessages = /\$ErrorMessages$/;
        var warningMessages = /\$WarningMessages$/;
        var message;
        var params;
        var spacer;
        var name;
        var ajaxBatchId;
        var fieldName;

        var editorAccessLevel = editor.record.get(
            Ext.String.format('{0}$AccessLevel', editor.column.dataIndex));
        if ((this.editMode == 'cell' || this.editMode == 'cellError')
            && (
                editor.column.displayOnly
                || editorAccessLevel == 'Disabled'
                || (
                    editor.column.isXType('computronixextcheckboxcolumn')
                    && editorAccessLevel == 'ReadOnly'
                )
            ))
        {
            this.newRecord = null;
            return false;
        }

        var cellEditor = this.editor.getEditor(editor.record, editor.column);
        if (cellEditor && cellEditor.isVisible())
        {
            this.newRecord = null;
            return false;
        }

        var canEdit = editor.record.get(Ext.String.format('{0}{1}',
            editor.record.idProperty, this.canEditFieldSuffix));
        if (Ext.isDefined(canEdit) && !canEdit)
        {
            this.newRecord = null;
            return false;
        }

        if (this.editMode == 'cell' || this.editMode == 'cellError')
        {
            if (editor.column.isVisible())
            {
                if (editor.record.get(Ext.String.format('{0}$AccessLevel', editor.column.dataIndex)) != 'Invisible')
                {
                    // Load row dependent combobox lists.
                    if (editor.column.isXType('computronixextcomboboxcolumn') && editor.column.rowDependent)
                    {
                        this.addManagedListener(Computronix.Ext.Ajax, 'batchcomplete', this.onGridPanelEditBatchComplete, this);

                        editField = editor.column.field;
                        editor.column.initialData = Ext.clone(editField.store.data.items);

                        ajaxBatchId = Ext.String.format('edit{0}', this.id);
                        params = { ajaxBatchId: ajaxBatchId };

                        if (editor.column.dependentField)
                        {
                            params[editor.column.dependentField] = editor.record.get(editor.column.dependentField);
                        }
                        else
                        {
                            params[editor.record.idProperty] = editor.record.getId();
                        }

                        editField.store.load(params);
                    }
                }
            }
            else
            {
                if (this.editMode != 'cell' && this.editMode != 'cellError')
                {
                    /* VERSION_WARNING 4.0.7
                     * Items for hidden grid columns still display in the dynamic edit form.
                     */
                    this.editor.getEditor(editor.record, editor.column).items.each(function (item)
                    {
                        if (item.name == column.dataIndex)
                        {
                            Ext.Function.defer(item.hide, 100, item);
                        }
                    }, this);
                }
            }
        }
        else
        {
            Ext.each(this.getColumns(), function (column)
            {
                if (column.isVisible())
                {
                    if (editor.record.get(Ext.String.format('{0}$AccessLevel', column.dataIndex)) != 'Invisible')
                    {
                        // Load row dependent combobox lists.
                        if (column.isXType('computronixextcomboboxcolumn') && column.rowDependent)
                        {
                            if (!this.editMask)
                            {
                                this.editMask = new Ext.LoadMask(
                                {
                                    msg: this.editMaskMessage,
                                    target: this,
                                });
                                this.addManagedListener(Computronix.Ext.Ajax, 'batchcomplete', this.onGridPanelEditBatchComplete, this);
                            }

                            this.editMask.show();

                            editField = column.getEditor();
                            column.initialData = Ext.clone(editField.store.data.items);

                            ajaxBatchId = Ext.String.format('edit{0}', this.id);
                            params = { ajaxBatchId: ajaxBatchId };

                            if (column.dependentField)
                            {
                                params[column.dependentField] = editor.record.get(column.dependentField);
                            }
                            else
                            {
                                params[editor.record.idProperty] = editor.record.getId();
                            }
                            editField.store.load(params);
                        }
                    }
                }
                else
                {
                    if (this.editMode != 'cell' && this.editMode != 'cellError')
                    {
                        /* VERSION_WARNING 4.0.7
                         * Items for hidden grid columns still display in the dynamic edit form.
                         */
                        this.editor.getEditor(editor.record, editor.column).items.each(function (item)
                        {
                            if (item.name == column.dataIndex)
                            {
                                Ext.Function.defer(item.hide, 100, item);
                            }
                        }, this);
                    }
                }
            }, this);
        }

        if (ajaxBatchId)
        {
            Computronix.Ext.Ajax.setBatchComplete(ajaxBatchId, true);
        }

        if (this.editMode == 'cell' || this.editMode == 'cellError')
        {
            fieldName = editor.column.dataIndex;
            editField = editor.column.field;

            if (editField)
            {
                // Create the field if it does not already exist.
                if (!editField.events)
                {
                    editField = Ext.ComponentManager.create(editField);
                }

                if (typeof editField.record !== 'undefined')
                {
                    editField.record = editor.record;
                }

                name = Ext.String.format('{0}$AccessLevel', fieldName);

                if (editor.record.fieldsByName.containsKey(name))
                {
                    this.setAccessLevel(fieldName, editor.record.get(name));
                }

                if (editField.isDisabled())
                {
                    result = false;
                }
                else
                {
                    name = Ext.String.format('{0}$ErrorMessages', fieldName);

                    if (editor.record.fieldsByName.containsKey(name))
                    {
                        if (editField.setErrorMessages)
                        {
                            // Remove any existing errors before the field is bound to the new data.
                            editField.clearInvalid();

                            message = editor.record.get(name);

                            if (message)
                            {
                                // This call is deferred so that the error message is set after the field is bound.
                                Ext.Function.defer(editField.setErrorMessages, 100, editField, [message]);
                            }
                            else
                            {
                                editField.setErrorMessages(null);
                            }
                        }
                    }

                    name = Ext.String.format('{0}$WarningMessages', fieldName);

                    if (editor.record.fieldsByName.containsKey(name))
                    {
                        if (editField.setWarningMessages)
                        {
                            message = editor.record.get(name);

                            if (message)
                            {
                                // This call is deferred so that the error message is set after the field is bound.
                                Ext.Function.defer(editField.setWarningMessages, 100, editField, [message]);
                            }
                            else
                            {
                                editField.setWarningMessages(null);
                            }
                        }
                    }
                }
            }
        }
        else
        {
            editor.record.fieldsByName.each(function (field)
            {
                // Set access level for each field.
                if (accessLevel.test(field.name))
                {
                    this.setAccessLevel(field.name.substr(0, field.name.indexOf('$AccessLevel')), editor.record.get(field.name));
                }

                // Set the error message for each field.
                if (errorMessages.test(field.name))
                {
                    Ext.each(this.getColumns(), function (column)
                    {
                        if ((!column.isXType('computronixextcomboboxcolumn') || !column.rowDependent) &&
                            column.dataIndex == field.name.substr(0, field.name.indexOf('$ErrorMessages')))
                        {
                            editField = column.getEditor();

                            return false;
                        }
                    }, this);

                    if (editField)
                    {
                        // Create the field if it does not already exist.
                        if (!editField.events)
                        {
                            editField = Ext.ComponentManager.create(editField);
                        }

                        if (editField.setErrorMessages)
                        {
                            // Remove any existing errors before the field is bound to the new data.
                            editField.clearInvalid();

                            message = editor.record.get(field.name);

                            if (message)
                            {
                                // This call is deferred so that the error message is set after the field is bound.
                                Ext.Function.defer(editField.setErrorMessages, 100, editField, [message]);
                            }
                            else
                            {
                                editField.setErrorMessages(null);
                            }
                        }
                    }
                }

                // Set the warning message for each field.
                if (warningMessages.test(field.name))
                {
                    Ext.each(this.getColumns(), function (column)
                    {
                        if ((!column.isXType('computronixextcomboboxcolumn') || !column.rowDependent) &&
                            column.dataIndex == field.name.substr(0, field.name.indexOf('$WarningMessages')))
                        {
                            editField = column.getEditor();

                            return false;
                        }
                    }, this);

                    if (editField)
                    {
                        // Create the field if it does not already exist.
                        if (!editField.events)
                        {
                            editField = Ext.ComponentManager.create(editField);
                        }

                        if (editField.setWarningMessages)
                        {
                            message = editor.record.get(field.name);

                            if (message)
                            {
                                // This call is deferred so that the error message is set after the field is bound.
                                Ext.Function.defer(editField.setWarningMessages, 100, editField, [message]);
                            }
                            else
                            {
                                editField.setWarningMessages(null);
                            }
                        }
                    }
                }
            }, this);
        }

        if (this.editMode != 'cell' && this.editMode != 'cellError')
        {
            this.editor.getEditor(editor.record, editor.column).saveBtnText = "OK";

            // Set focus to the first available field.
            this.editor.getEditor(editor.record, editor.column).items.each(function (item)
            {
                if (item.isFormField && !item.isXType('displayfield') && !item.isDisabled())
                {
                    item.focus(true, 500);
                    return false;
                }
            }, this);

            if (this.enableFilter)
            {
                this.filterText.disable();
            }

            if (this.enableNew)
            {
                this.newButton.disable();
            }
        }

        return result;
    },

    onGridPanelBeforeSelect: function (gridView, htmlElement, selections)
    {
        return Computronix.Ext.confirmContinue();
    },

    onGridPanelCancelEdit: function (editor)
    {
        if (this.newRecord)
        {
            this.store.remove(this.newRecord);
            this.newRecord = null;
        }

        this.restoreData();

        if (this.editMask)
        {
            Computronix.Ext.Ajax.abortAll();
            this.editMask.hide();
        }

        if (this.editMode != 'cell' && this.editMode != 'cellError')
        {
            if (this.enableFilter)
            {
                this.filterText.enable();
            }

            if (this.enableNew && this.canAdd())
            {
                this.newButton.enable();
            }
        }
    },

    onGridPanelDeleteButtonClick: function (button, event)
    {
        this.removeSelected();
    },

    // In v4.1.0 the arguments are reversed so editor is second.
    onGridPanelEdit: function (editing, editor)
    {
        var name;
        var displayValue;
        var errorMessages = /\$ErrorMessages$/;
        var warningMessages = /\$WarningMessages$/;
        var hasRowMessage = false;

        if (editor.column.updateDisplayValue)
        {
            editor.column.updateDisplayValue();
        }

        if ((this.editMode == 'row' || this.editMode == 'rowAction' || this.editMode == 'cell' || this.editMode == 'cellError') &&
            !Computronix.Ext.isEqual(editor.value, editor.originalValue))
        {
            name = Ext.String.format('{0}$ErrorMessages', editor.record.idProperty);

            if (editor.record.fieldsByName.containsKey(name))
            {
                hasRowMessage = true;

                if (!Ext.isEmpty(editor.record.get(name)))
                {
                    editor.record.set(name, '');
                }
            }

            name = Ext.String.format('{0}$WarningMessages', editor.record.idProperty);

            if (editor.record.fieldsByName.containsKey(name))
            {
                hasRowMessage = true;

                if (!Ext.isEmpty(editor.record.get(name)))
                {
                    editor.record.set(name, '');
                }
            }

            if (!hasRowMessage)
            {
                editor.record.fieldsByName.each(function (field)
                {
                    if (errorMessages.test(field.name))
                    {
                        if (editor.record.get(field.name))
                        {
                            editor.record.set(field.name, '');
                        }
                    }

                    if (warningMessages.test(field.name))
                    {
                        if (editor.record.get(field.name))
                        {
                            editor.record.set(field.name, '');
                        }
                    }
                }, this);
            }

            name = Ext.String.format('{0}$ErrorMessages', editor.column.dataIndex);

            if (editor.record.fieldsByName.containsKey(name))
            {
                if (!Ext.isEmpty(editor.record.get(name)))
                {
                    editor.record.set(name, '');
                }
            }

            name = Ext.String.format('{0}$WarningMessages', editor.column.dataIndex);

            if (editor.record.fieldsByName.containsKey(name))
            {
                if (!Ext.isEmpty(editor.record.get(name)))
                {
                    editor.record.set(name, '');
                }
            }
        }

        this.newRecord = null;
        this.restoreData();

        if (this.editMode != 'cell' && this.editMode != 'cellError')
        {
            if (this.enableFilter)
            {
                this.filterText.enable();
            }

            if (this.enableNew && this.canAdd())
            {
                this.newButton.enable();
            }
        }
    },

    onGridPanelEditBatchComplete: function (ajax, ajaxBatchId, success)
    {
        var editField;
        var errorMessages = /\$ErrorMessages$/;
        var warningMessages = /\$WarningMessages$/;
        var message;
        var record;

        if (ajaxBatchId == Ext.String.format('edit{0}', this.id))
        {
            record = this.editor.context.record;

            record.fieldsByName.each(function (field)
            {
                editField = null;

                // Set the error message for each field.
                if (errorMessages.test(field.name))
                {
                    Ext.each(this.getColumns(), function (column)
                    {
                        if (column.isXType('computronixextcomboboxcolumn') && column.rowDependent &&
                            column.dataIndex == field.name.substr(0, field.name.indexOf('$ErrorMessages')))
                        {
                            editField = column.getEditor();

                            return false;
                        }
                    }, this);

                    if (editField)
                    {
                        // Create the field if it does not already exist.
                        if (!editField.events)
                        {
                            editField = Ext.ComponentManager.create(editField);
                        }

                        if (editField.setErrorMessages)
                        {
                            // Remove any existing errors before the field is bound to the new data.
                            editField.clearInvalid();

                            message = record.get(field.name);

                            if (message)
                            {
                                // This call is deferred so that the error message is set after the field is bound.
                                Ext.Function.defer(editField.setErrorMessages, 100, editField, [message]);
                            }
                            else
                            {
                                editField.setErrorMessages(null);
                            }
                        }
                    }
                }

                // Set the warning message for each field.
                if (warningMessages.test(field.name))
                {
                    Ext.each(this.getColumns(), function (column)
                    {
                        if (column.isXType('computronixextcomboboxcolumn') && column.rowDependent &&
                            column.dataIndex == field.name.substr(0, field.name.indexOf('$WarningMessages')))
                        {
                            editField = column.getEditor();

                            return false;
                        }
                    }, this);

                    if (editField)
                    {
                        // Create the field if it does not already exist.
                        if (!editField.events)
                        {
                            editField = Ext.ComponentManager.create(editField);
                        }

                        if (editField.setWarningMessages)
                        {
                            message = record.get(field.name);

                            if (message)
                            {
                                // This call is deferred so that the error message is set after the field is bound.
                                Ext.Function.defer(editField.setWarningMessages, 100, editField, [message]);
                            }
                            else
                            {
                                editField.setWarningMessages(null);
                            }
                        }
                    }
                }
            }, this);

            if (this.editMask)
            {
                this.editMask.hide();
            }
        }
    },

    onGridPanelFilterTextChange: function (field, newValue, oldValue)
    {
        this.filterData(newValue);
        this.fireEvent('filtertextchange', this, newValue, oldValue);
    },

    onGridPanelHeaderClick: function ()
    {
        /* VERSION_WARNING 4.2
         * Clicking the header of a column does not cause text or editmask
         * columns to blur and completeEdit.
         */
        if (this.editMode == 'cell' || this.editMode == 'cellError')
        {
            this.completeEdit();
        }
    },

    onGridPanelItemContextMenu: function (gridView, record, htmlElement, rowIndex, event)
    {
        event.preventDefault();
        this.contextMenu.showAt(event.getXY());
    },

    onGridPanelLoad: function (store, records, success)
    {
        if (this.enableFilter)
        {
            this.filterText.setValue(null);
            // If the filter text is already empty, the change event would not
            // be fired; so go ahead and fire it.
            this.filterText.fireEvent('change', null, null);
        }
    },

    onGridPanelNewButtonClick: function (button, event)
    {
        var record = this.store.add({})[0];

        if (this.editor && this.editMode != 'cell' && this.editMode != 'cellError')
        {
            this.newRecord = record;
            this.editRow(this.view, this.store.data.indexOf(this.newRecord), 0, this.getColumns()[0], null, record);
        }
    },

    onGridPanelPrintButtonClick: function (button, event)
    {
        this.print();
    },

    onGridPanelSelectionChange: function (selectionModel, selections)
    {
        if (this.dependentLoadMethod && selections.length == 1)
        {
            this.dependentLoadMethod.call(this, selections[0].getId());
        }

        if (this.enableDelete)
        {
            if (this.canDelete(selections))
            {
                this.deleteButton.enable();
            }
            else
            {
                this.deleteButton.disable();
            }
        }
    },

    onGridPanelViewRefresh: function (gridView)
    {
        this.setRowCount();
    },

    // Methods
    //=============================================================================
    //
    _resolveStore:  function ()
    {
        if (this.storeId)
        {
            store = Ext.data.StoreManager.lookup(this.storeId);
            this.setStore(store);
        }
    },

    addColumn: function (config)
    {
        this.headerCt.add(config);
        this.getView().refresh();
    },

    canAdd: function ()
    {
        return true;
    },

    canDelete: function (records)
    {
        var result = false;
        var value;

        if (!this.editor || this.editMode == 'cell' || this.editMode == 'cellError' || !this.editor.getEditor().isVisible())
        {
            if (Ext.isArray(records))
            {
                if (records.length > 0)
                {
                    result = true;

                    Ext.each(records, function (record)
                    {
                        value = record.get(Ext.String.format('{0}{1}', record.idProperty, this.canDeleteFieldSuffix));

                        if (value == false)
                        {
                            result = false;
                            return false;
                        }
                    }, this);
                }
            }
            else
            {
                if (records)
                {
                    value = records.get(Ext.String.format('{0}{1}', records.idProperty, this.canDeleteFieldSuffix));

                    if (!Ext.isDefined(value) || value)
                    {
                        result = true;
                    }
                }
            }
        }

        return result;
    },

    completeEdit: function ()
    {
        var result = true;

        if (this.editor)
        {
            this.editor.completeEdit();

            // If the editor is still active, then we have an error.
            if (this.editMode != 'cell' && this.editMode != 'cellError')
            {
                result = !this.editor.editing;
            }
        }

        return result;
    },

    deleteRow: function (gridView, rowIndex, columnIndex, action, event, record)
    {
        this.store.remove(record);
    },

    deselect: function (records, suppressEvent)
    {
        this.getSelectionModel().deselect(records, suppressEvent);
    },

    deselectAll: function (suppressEvent)
    {
        this.getSelectionModel().deselectAll(suppressEvent);
    },

    editRow: function (gridView, rowIndex, columnIndex, action, event, record)
    {
        if (this.editor)
        {
            this.editor.startEdit(record, action);
        }
    },

    filterData: function (value)
    {
        this.store.clearFilter();
        if (!Ext.isEmpty(value))
        {
            this.store.filterBy(this.getFilter(value), this);
        }
    },

    focusFirstSelected: function ()
    {
        this.store.data.each(function (record)
        {
            if (this.getSelected().containsKey(record.getId()))
            {
                this.select(record, true, true);
                return false;
            }
        }, this);
    },

    getErrors: function ()
    {
        var result = [];
        var model = this.store.getNewModel();
        var errorMessages = /\$ErrorMessages$/;
        var name;
        var hasRowMessage = false;

        if (this.editMode)
        {
            name = Ext.String.format('{0}$ErrorMessages', model.idProperty);

            if (model.fieldsByName.containsKey(name))
            {
                hasRowMessage = true;

                this.store.data.each(function (record)
                {
                    if (!Ext.isEmpty(record.get(name)))
                    {
                        result = result.concat(Ext.Array.from(record.get(name)));
                    }
                }, this);
            }

            if (!hasRowMessage)
            {
                this.store.data.each(function (record)
                {
                    record.fieldsByName.each(function (field)
                    {
                        if (errorMessages.test(field.name))
                        {
                            if (!Ext.isEmpty(record.get(field.name)))
                            {
                                result = result.concat(Ext.Array.from(record.get(field.name)));
                            }
                        }
                    }, this);
                }, this);
            }

            if (this.editor && this.editMode != 'cell' && this.editMode != 'cellError')
            {
                if (!this.editor.getEditor().getForm().isValid())
                {
                    this.editor.getEditor().getForm().getFields().each(function (field)
                    {
                        if (!field.isValid())
                        {
                            result = result.concat(field.getErrors(field.getValue()));
                        }
                    }, this);
                }
            }
        }

        return result;
    },

    getFilter: function (value)
    {
        var result = null;
        var filters = [];
        var field;
        var matcher;

        if (!Ext.isEmpty(value))
        {
            matcher = Computronix.Ext.getMatcher(value);

            Ext.each(this.getColumns(), function (column)
            {
                if (!Ext.isEmpty(column.dataIndex) && column.isVisible())
                {
                    field = this.store.getNewModel().fieldsByName.get(column.dataIndex);

                    if ((field.type == Ext.data.Types.STRING.type || field.type == Ext.data.Types.AUTO.type) &&
                        column.isXType('computronixexttextcolumn'))
                    {
                        filters.push(function (r)
                        {
                            return matcher.test(r.data[column.dataIndex]);
                        });
                    }
                    else if (column.isXType('computronixextcomboboxcolumn'))
                    {
                        if (column.displayValueField)
                        {
                            filters.push(function (r)
                            {
                                return matcher.test(r.data[column.displayValueField]);
                            });
                        }
                        else
                        {
                            filters.push(function (r)
                            {
                                return matcher.test(this.store.getDisplayValue(column.dataIndex, r.data[column.dataIndex]));
                            });
                        }
                    }
                }

            }, this);

            result = function (record)
            {
                var isMatch = false;

                Ext.each(filters, function (filter)
                {
                    isMatch = isMatch || filter.call(this, record);
                    return !isMatch;
                }, this);

                return isMatch;
            }
        }

        return result;
    },

    getPrintHtml: function ()
    {
        var html = '';
        var rowhtml;
        var value;
        var metaData;
        var cssLinks = '';
        var match;
        var summary;

        this.headerCt.items.each(function (column)
        {
            if (column.dataIndex && column.isVisible() && !column.isXType('actioncolumn') && column.text != '&#160;')
            {
                html += Ext.String.format(
                    '<th class="cxprintgridheader" style="text-align: {1}; width: {2}px;">{0}</th>\n',
                    column.text || '', column.align || 'left', column.getWidth() - 12);
            }
        }, this);

        html = Ext.String.format('<tr>\n{0}</tr>\n', html);

        if (this.store)
        {
            this.store.data.each(function (record, rowIndex)
            {
                rowHtml = '';

                Ext.each(this.getColumns(), function (column, columnIndex)
                {
                    if (column.dataIndex && column.isVisible() && !column.isXType('actioncolumn') && column.text != '&#160;')
                    {
                        metaData = { tdCls: '', style: '' };
                        value = record.get(column.dataIndex);

                        if (column.renderer)
                        {
                            value = column.renderer(value, metaData, record, rowIndex, columnIndex, this.store, this.getView());
                        }

                        value = value || '';

                        rowHtml += Ext.String.format(
                            '<td class="cxprintgriddata {3}" style="text-align: {1}; width: {2}px; {4}">{0}</td>\n',
                            value, column.align || 'left', column.getWidth() - 12, metaData.tdCls, metaData.style);
                    }
                }, this);

                html += Ext.String.format('<tr>\n{0}</tr>\n', rowHtml);
            }, this);
        }

        if (this.enableSummary)
        {
            rowHtml = '';

            Ext.Object.each(this.getView().features, function (key, feature)
            {
                if (feature.ftype == 'summary')
                {
                    summary = feature;
                    return false;
                }
            }, this);

            summary.createSummaryRecord(this.getView());

            Ext.each(this.getColumns(), function (column, columnIndex)
            {
                if (column.dataIndex && column.isVisible() && !column.isXType('actioncolumn') && column.text != '&#160;')
                {
                    value = summary.summaryRecord.get(column.dataIndex)

                    if (column.summaryRenderer)
                    {
                        value = column.summaryRenderer(value, summary.summaryData, column.dataIndex);
                    }

                    rowHtml += Ext.String.format(
                        '<td class="cxprintgriddata" style="text-align: {1}; width: {2}px;">{0}</td>\n',
                        value, column.align || 'left', column.getWidth() - 12);
                }
            }, this);

            html += Ext.String.format('<tr>\n{0}</tr>\n', rowHtml);
        }

        // remove links.
        html = html.replace(/<a [^>]+>|<\/a\s*>/ig, '');

        if (this.printCssFiles)
        {
            Ext.each(this.printCssFiles, function (file)
            {
                cssLinks += Ext.String.format('<link rel="Stylesheet" type="text/css" href="{0}" />\n', file);
            }, this);
        }

        html = Ext.String.format(
            '\n<head>\n<title>{1}</title>\n{2}</head>\n<body class="cxprintgrid">\n<table class="cxprintgridtable" >\n{0}</table>\n</body>\n',
             html, this.printTitle || this.titleText || '', cssLinks);
        html = Ext.String.format(this.printHtml, html);

        return html;
    },

    getWarnings: function ()
    {
        var result = [];
        var model = this.store.getNewModel();
        var warningMessages = /\$WarningMessages$/;
        var name;
        var hasRowMessage = false;

        if (this.editMode)
        {
            name = Ext.String.format('{0}$WarningMessages', model.idProperty);

            if (model.fieldsByName.containsKey(name))
            {
                hasRowMessage = true;

                this.store.data.each(function (record)
                {
                    if (!Ext.isEmpty(record.get(name)))
                    {
                        result = result.concat(Ext.Array.from(record.get(name)));
                    }
                }, this);
            }

            if (!hasRowMessage)
            {
                this.store.data.each(function (record)
                {
                    record.fieldsByName.each(function (field)
                    {
                        if (warningMessages.test(field.name))
                        {
                            if (!Ext.isEmpty(record.get(field.name)))
                            {
                                result = result.concat(Ext.Array.from(record.get(field.name)));
                            }
                        }
                    }, this);
                }, this);
            }
        }

        return result;
    },

    getSelected: function ()
    {
        return this.getSelectionModel().selected;
    },

    insertColumn: function (index, config)
    {
        this.headerCt.insert(index, config);
        this.getView().refresh();
    },

    isSelected: function (record)
    {
        return this.getSelectionModel().isSelected(record);
    },

    print: function ()
    {
        var reportWindow = window.open(Ext.isSafari ? 'about:blank' : '',
            this.paneId || 'printgrid', this.printFeatures);

        html = this.getPrintHtml();
        reportWindow.document.write(html);
        reportWindow.document.close();
        reportWindow.focus();
    },

    removeSelected: function ()
    {
        var selected = this.getSelected();

        selected.each(function (record)
        {
            this.store.remove(record);
        }, this);

        if (!this.checkboxSelect && this.store.getCount() > 0)
        {
            this.getSelectionModel().select(0);
        }

        /*
         * FUTURE
         * setRowCount already checks this.enableFilter and therefore the check
         * here is redundant. We didn't want to incur the regression test to
         * remove the check at this point.
         */
        if (this.enableFilter)
        {
            this.setRowCount();
        }
    },

    restoreData: function ()
    {
        var editField;

        // Restore row dependend combobox lists.
        Ext.each(this.getColumns(), function (column)
        {
            if (column.isXType('computronixextcomboboxcolumn') && column.rowDependent && column.initialData)
            {
                editField = column.getEditor();
                editField.store.loadData(column.initialData);
            }
        }, this);
    },

    select: function (records, keepExisting, suppressEvent)
    {
        this.getSelectionModel().select(records, keepExisting, suppressEvent);
    },

    selectAll: function (suppressEvent)
    {
        this.getSelectionModel().selectAll(suppressEvent);
    },

    // Sets the access level for the specified column.
    setAccessLevel: function (dataIndex, level)
    {
        var field;

        Ext.each(this.getColumns(), function (column)
        {
            if (column.dataIndex == dataIndex)
            {
                field = column.getEditor();

                return false;
            }
        }, this);

        if (field)
        {
            // Create the field if it does not already exist.
            if (!field.events)
            {
                field = Ext.ComponentManager.create(field);
            }

            // Invisible is the same as Disabled because hiding the field causes all of the fields to the right to slide left.
            // The data value for any Invisible field is expected to the blank.
            switch (level)
            {
                case 'Invisible':
                case 'Disabled':
                    field.show();
                    field.setReadOnly(false);
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(false);
                    field.disable();
                    break;
                case 'ReadOnly':
                    field.show();
                    field.enable();
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(false);
                    field.setReadOnly(true);
                    break;
                case 'Enterable':
                    field.show();
                    field.setReadOnly(false);
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(false);
                    field.enable();
                    break;
                case 'Mandatory':
                    field.show();
                    field.enable();
                    field.setReadOnly(false);
                    field.enforceMandatory = true;
                    field.setMandatoryIndicator(true);
                    break;
                case 'ShowMandatory':
                    field.show();
                    field.enable();
                    field.setReadOnly(false);
                    field.enforceMandatory = false;
                    field.setMandatoryIndicator(true);
                    break;
            }
        }
    },

    setRowCount: function ()
    {
        var filteredCount;
        var source;
        var totalCount;

        if (this.enableFilter && this.countLabel && this.countLabel.el && this.store)
        {
            filteredCount = this.store.getCount();
            source = this.store.getData().getSource();

            if (source)
            {
                totalCount = source.getCount();
            }
            else
            {
                totalCount = this.store.getCount();
            }

            this.countLabel.setText(Ext.String.format('{0} of {1}', filteredCount, totalCount));
        }
    },

    setToolbar: function ()
    {
        if (this.toolbarItems.length > 0)
        {
            this.toolbar = new Computronix.Ext.toolbar.Toolbar(
            {
                enableOverflow: true,
                items: this.toolbarItems,
                // Set minimum height of the toolbar to properly accomodate empty toolbars,
                // as encountered with warnings grids. Minimum height is not exposed via
                // css variables so we would have to replicate the styling of the entire
                // toolbar in our scss in order to repair via styling. Conversely, minHeight
                // is a published and documented api endpoint for ExtJS on toolbars so
                // this method should still be somewhat upgrade-proof hopefully
                minHeight: 27
            });

            this.tbar = this.toolbar;
        }
    }
});

// Computronix property grid panel extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.grid.property.Grid',
{
    extend: 'Ext.grid.property.Grid',
    alias: 'widget.computronixextpropertygrid',
    bodyStyle: { 'background-color': 'transparent' },

    // Data Members
    //=============================================================================
    //
    enableCopy: true,
    copyButton: null,
    toolbar: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (this.enableCopy && window.clipboardData)
        {
            this.copyButton = new Computronix.Ext.button.Button(
            {
                text: 'Copy',
                iconCls: 'cxcopyicon',
                listeners: { click: { fn: this.onGridPropertyGridCopyButtonClick, scope: this } }
            });

            this.toolbar = new Computronix.Ext.toolbar.Toolbar(
            {
                enableOverflow: true,
                items:
                [
                    { xtype: 'tbfill' },
                    this.copyButton
                ]
            });

            this.tbar = this.toolbar;
        }

        this.callParent();
    },

    // Event Handlers
    //=============================================================================
    //
    onGridPropertyGridCopyButtonClick: function (editor, event)
    {
        if (window.clipboardData)
        {
            window.clipboardData.setData("Text", Ext.encode(this.source).replace(/},{/g, '},\n{').replace(/,/g, ',\n').replace(/\[{/g, '[\n{'));
        }
    }
});

// Group Header label extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.GroupHeader',
{
    extend: 'Computronix.Ext.form.field.Display',
    alias: 'widget.computronixextgroupheader',
    fieldCls: 'x-form-display-field cxgroupheader',
    width: 200,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (this.enableMandatoryIndicator)
        {
            this.fieldLabel = '&nbsp;';
            this.labelSeparator = '';
            this.labelPad = 0;
            this.labelWidth = 6;
        }

        this.callParent();
    }
});

// Computronix menu item extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.menu.Item',
{
    extend: 'Ext.menu.Item',
    alias: 'widget.computronixextmenuitem',
    mixins: { ajaxBatchMask: 'Computronix.Ext.AjaxBatchMask' },

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.mixins.ajaxBatchMask.constructor.call(this);
    }
});

// Computronix panel extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.panel.Panel',
{
    extend: 'Ext.panel.Panel',
    alias: 'widget.computronixextpanel',
    mixins: { presentationLoader: 'Computronix.Ext.PresentationLoader' },
    bodyStyle: { 'background-color': 'transparent' },
    border: false,
    layout: { type: 'vbox', align: 'stretch' },
    autoScroll: true,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.mixins.presentationLoader.constructor.call(this);
    }
});

// Computronix system monitor tool extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.SystemMonitorTool',
{
    extend: 'Ext.button.Button',
    id: 'computronixExtSystemMonitorTool',
    iconCls: 'cxblankicon',
    border: false,
    disabled: true,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.addListener('click', this.onSystemMonitorToolClick, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onSystemMonitorToolClick: function (button, event)
    {
        if (Computronix.Ext.SystemMonitor && Computronix.Ext.SystemMonitor.instance)
        {
            Computronix.Ext.SystemMonitor.instance.show();
            this.setButton(false);
        }
    },

    // Methods
    //=============================================================================
    //
    setButton: function (show)
    {
        if (show)
        {
            this.setIconCls('cxsystemmonitoricon');
            this.setTooltip('System Monitor');
            this.enable();
        }
        else
        {
            this.setIconCls('cxblankicon');
            this.setTooltip('');
            this.disable();
        }
    }
});

// Computronix tab panel extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.tab.Panel',
{
    extend: 'Ext.tab.Panel',
    alias: 'widget.computronixexttabpanel',
    bodyStyle: { 'background-color': 'transparent' },
    border: false,
    plain: true,
    enableTabScroll: true,
    tabBar:
    {
        // VERSION_WARNING 6.0.2
        // The upgrade to ExtJS 6.0.2 resulted in a peculiar issue in which the
        // tab bar would grow by one pixel when a new tab is selected after the
        // panel initially loads. We hard-code the height here to ensure that
        // the height remains constant.
        height: 24
    },

    // Data Members
    //=============================================================================
    //
    isBindable: true,
    isUpdatable: true,
    ignoreChanges: true,
    focusFirstDeferPeriod: 500,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        if (!this.ignoreChanges)
        {
            this.addListener('beforetabchange', this.onTabPanelBeforeTabChange, this);
        }

        this.addListener('tabchange', this.onTabPanelTabChange, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onTabPanelBeforeTabChange: function (tabPanel, newCard, oldCard)
    {
        var result = true;

        if (!this.ignoreChanges)
        {
            result = Computronix.Ext.confirmContinue();

            if (result && Computronix.Ext.hasChanges())
            {
                oldCard.refreshDeferred = true;
                oldCard.clearStores();
            }
        }

        return result;
    },

    onTabPanelTabChange: function (tabPanel, newCard, oldCard)
    {
        if (newCard.refreshDeferred && newCard.items.getCount() > 0)
        {
            newCard.refreshDeferred = false;
            newCard.refreshStores();
        }
    },

    // Methods
    //=============================================================================
    //
    clearLookupStores: function ()
    {
        this.items.each(function (tab)
        {
            tab.clearLookupStores();
        }, this);
    },

    clearStores: function ()
    {
        this.items.each(function (tab)
        {
            tab.clearStores();
        }, this);
    },

    focusFirst: function ()
    {
        if (this.activeTab.focusFirst)
        {
            Ext.Function.defer(this.activeTab.focusFirst, this.focusFirstDeferPeriod, this.activeTab);
        }
    },

    getChanges: function (completeEdit, changes, hasChanges)
    {
        var result = changes || [];
        var _changes;

        this.items.each(function (tab)
        {
            _changes = tab.getChanges(completeEdit, null, hasChanges || result.length > 0);

            if (_changes.length > 0)
            {
                result = result.concat(_changes);
            }
        }, this);

        return result;
    },

    hasChanges: function (completeEdit)
    {
        var result = false;

        this.items.each(function (tab)
        {
            if (tab.hasChanges(completeEdit))
            {
                result = true;
                return false;
            }
        }, this);

        return result;
    },

    loadLookupStores: function (params)
    {
        this.items.each(function (tab)
        {
            // If the tab is active and presentation exists then load the data stores.
            if (tab.tab.active && tab.items.getCount() > 0)
            {
                tab.loadLookupStores(params);
            }
            else
            {
                if (tab.rendered)
                {
                    tab.clearLookupStores();
                }

                tab.setStoreParams(params);
            }
        }, this);
    },

    loadStores: function (params)
    {
        this.items.each(function (tab)
        {
            // If the tab is active and presentation exists then load the data stores.
            if (tab.tab.active && tab.items.getCount() > 0)
            {
                tab.refreshDeferred = false;
                tab.loadStores(params);
            }
            else
            {
                tab.refreshDeferred = true;

                if (tab.rendered)
                {
                    tab.clearStores();
                }

                tab.setStoreParams(params);
            }
        }, this);
    },

    refreshLookupStores: function (params)
    {
        this.items.each(function (tab)
        {
            if (tab.tab.active && tab.items.getCount() > 0)
            {
                tab.refreshLookupStores();
            }
            else
            {
                if (tab.rendered)
                {
                    tab.clearStores();
                }
            }
        }, this);
    },

    refreshStores: function ()
    {
        this.items.each(function (tab)
        {
            if (tab.tab.active && tab.items.getCount() > 0)
            {
                tab.refreshDeferred = false;
                tab.refreshStores();
            }
            else
            {
                tab.refreshDeferred = true;

                if (tab.rendered)
                {
                    tab.clearStores();
                }
            }
        }, this);
    },

    setActiveTab: function (card)
    {
        this.callParent(arguments);

        if (card && card.refreshDeferred && card.items.getCount() > 0)
        {
            card.refreshDeferred = false;
            card.refreshStores();
        }
    },

    setLookupStoreParams: function (params)
    {
        this.items.each(function (tab)
        {
            tab.setLookupStoreParams(params);
        }, this);
    },

    setStoreParams: function (params)
    {
        this.items.each(function (tab)
        {
            tab.setStoreParams(params);
        }, this);
    }
});

// Computronix toolbar extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.toolbar.Toolbar',
{
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.computronixexttoolbar',

    // Methods
    //=============================================================================
    //
    hideText: function ()
    {
        this.items.each(function (item)
        {
            if (item.isXType('computronixextbutton') || item.isXType('computronixextsplitbutton'))
            {
                if (item.enableHiddenText)
                {
                    item.hideText();
                }
            }
        });
    },

    showText: function ()
    {
        this.items.each(function (item)
        {
            if (item.isXType('computronixextbutton') || item.isXType('computronixextsplitbutton'))
            {
                if (item.enableHiddenText)
                {
                    item.showText();
                }
            }
        });
    }
});

// Computronix toolbar text item extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.toolbar.TextItem',
{
    extend: 'Ext.toolbar.TextItem',
    alias: 'widget.computronixexttbtext',
    overflowText: ' ',

    // Data Members
    //=============================================================================
    //
    initialText: null,
    align: 'left',
    toolTip: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.initialText = this.text;

        this.callParent();

        this.addListener('render', this.onToolbarTextItemRender, this);
        this.addListener('resize', this.onToolbarTextItemResize, this);
    },

    // Event handlers
    //=============================================================================
    //
    onToolbarTextItemRender: function (field)
    {
        if (this.align != 'left')
        {
            Ext.DomHelper.applyStyles(this.el, { 'text-align': this.align });
        }
    },

    onToolbarTextItemResize: function (textItem, width)
    {
        this.adjustText(width);
    },

    // Methods
    //=============================================================================
    //
    getText: function ()
    {
        return this.initialText;
    },

    setText: function (text, adjust)
    {
        this.callParent(arguments);

        if (adjust != false)
        {
            this.initialText = text;
            this.adjustText();
        }
    },

    adjustText: function (width)
    {
        var textWidth = Ext.util.TextMetrics.measure(this.el, this.initialText).width;

        if (width && textWidth > width)
        {
            if (!this.toolTip)
            {
                this.toolTip = new Ext.tip.ToolTip({ target: this.el, html: this.initialText });
            }

            this.setText(Ext.String.ellipsis(this.initialText, Math.round(width / (textWidth / this.initialText.length)), true), false);
        }
        else
        {
            if (this.toolTip)
            {
                this.toolTip.destroy();
                this.toolTip = null;
            }

            this.setText(this.initialText, false);
        }
    }
});

// Computronix microhelp tool extension.
// ############################################################################################
//
// This definition must follow Computronix.Ext.toolbar.TextItem.
Ext.define('Computronix.Ext.MicrohelpTool',
{
    extend: 'Computronix.Ext.toolbar.TextItem',
    cls: 'cxmicrohelp',
    flex: 1,
    height: 16,

    // Data Members
    //=============================================================================
    //
    defaultText: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.text = this.defaultText;

        this.callParent();
    },

    // Methods
    //=============================================================================
    //
    clear: function ()
    {
        if (this.rendered)
        {
            this.setText(this.defaultText);
        }
    },

    updateText: function (message)
    {
        if (this.rendered)
        {
            if (message)
            {
                this.setText(message);
            }
            else
            {
                this.clear();
            }
        }
    }
});

// Computronix tree column extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.tree.Column',
{
    extend: 'Ext.tree.Column',
    alias: 'widget.computronixexttreecolumn',
    flex: 1,
    dataIndex: 'text',

    // Data Members
    //=============================================================================
    //
    cellHeight: null,
    tpl: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.tpl =
            new Ext.Template(Ext.String.format('<span class="cxtreecolumn">{{0}}</span>',
            this.dataIndex), { compile: true });
    },

    // Methods
    //=============================================================================
    //
    renderer: function (value, metaData, record, rowIndex, columnIndex, store, treeView)
    {
        var result = null;
        var column;

        if (treeView)
        {
            column = treeView.panel.columns[columnIndex];

            if (column.tpl)
            {
                if (column.cellHeight)
                {
                    metaData.style += Ext.String.format('height: {0}px; ', column.cellHeight);
                }

                result = column.tpl.applyTemplate(record.data);
            }
            else
            {
                result = value;
            }
        }

        return result;
    }
});

// Computronix tree panel extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.tree.Panel',
{
    extend: 'Ext.tree.Panel',
    alias: 'widget.computronixexttreepanel',
    sortableColumns: false,
    useArrows: true,
    rootVisible: false,
    root:
    {
        expanded: true
    },
    viewConfig: null,

    // Data Members
    //=============================================================================
    //
    editMode: "rowActionDialog",
    contextMenu: null,
    singleSelect: true,
    checkboxSelect: false,
    allowDeselect: false,
    checkOnly: true,
    dependentLoadMethod: null,
    ignoreChanges: true,
    demandStore: null,
    loadOnRender: true,
    loadIncrementally: true,
    parentFieldPrefix: 'Parent',
    parentField: null,
    textField: null,
    iconClsField: null,
    hasChildrenField: null,
    rootParentValue: null,
    titleText: null,
    titleLabel: null,
    enableNew: false,
    newButton: null,
    enableDelete: false,
    deleteButton: null,
    toolbarItems: null,
    toolbar: null,
    canDeleteFieldSuffix: '$CanDelete',
    lastPath: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        var storeId;
        var model;
        var fields;
        var selectionModel;

        // Can not be initialized when the property is declared because the array reference would be global.
        this.toolbarItems = [];

        if (this.titleText)
        {
            this.titleLabel = new Computronix.Ext.toolbar.TextItem({ text: this.titleText, flex: 1 });
            this.toolbarItems.push(this.titleLabel);
        }

        if (this.enableNew)
        {
            this.newButton = this.newButton || new Computronix.Ext.button.Button(
            {
                text: 'New',
                iconCls: 'cxnewicon',
                listeners: { click: { fn: this.onTreePanelNewButtonClick, scope: this } }
            });

            this.toolbarItems.push(this.newButton);
        }

        if (this.enableDelete)
        {
            this.deleteButton = this.deleteButton || new Computronix.Ext.button.Button(
            {
                text: 'Delete',
                iconCls: 'cxdeleteicon',
                disabled: true,
                listeners: { click: { fn: this.onTreePanelDeleteButtonClick, scope: this } }
            });

            this.toolbarItems.push(this.deleteButton);
        }

        if (this.toolbarItems.length > 0)
        {
            this.toolbar = new Computronix.Ext.toolbar.Toolbar(
            {
                enableOverflow: true,
                items: this.toolbarItems
            });

            this.tbar = this.toolbar;
        }

        if (this.checkboxSelect)
        {
            this.selType = 'checkboxmodel';
        }

        if (this.columns && this.columns.length == 1)
        {
            this.hideHeaders = true;
        }

        if (this.demandStore)
        {
            this.demandStore = Ext.getStore(this.demandStore);
            storeId = Ext.String.format('{0}TreeStore', this.demandStore.dataId);
            model = this.demandStore.getNewModel();

            if (!this.textField)
            {
                this.textField = model.getDisplayField();
            }

            if (!this.parentField)
            {
                this.parentField = Ext.String.format('{0}{1}', this.parentFieldPrefix, model.idProperty);
            }

            fields = [];

            model.fieldsByName.each(function (field)
            {
                fields.push(
                {
                    name: field.name,
                    type: field.type,
                    allowNull: field.allowNull
                });
            }, this);

            fields.push({ name: 'text', type: 'string' });

            if (Ext.getStore(storeId))
            {
                this.store = Ext.getStore(storeId);
            }
            else
            {
                Ext.define(storeId,
                {
                    extend: 'Computronix.Ext.data.Model',
                    idProperty: model.idProperty,
                    fields: fields
                });

                this.store = Ext.create('Ext.data.TreeStore',
                {
                    storeId: storeId,
                    model: storeId,
                    proxy:
                    {
                        type: 'memory'
                    },
                    root:
                    {
                        expanded: true
                    }
                });
            }
        }

        this.callParent();

        if (this.demandStore)
        {
            if (this.parentField)
            {
                this.demandStore.group(this.parentField);
            }
        }

        if (!this.disableSelection)
        {
            selectionModel = this.getSelectionModel();

            if (this.singleSelect)
            {
                selectionModel.setSelectionMode('SINGLE');

                if (this.checkboxSelect)
                {
                    selectionModel.showHeaderCheckbox = false;
                }
            }
            else
            {
                selectionModel.setSelectionMode('SIMPLE');
            }

            selectionModel.allowDeselect = this.allowDeselect;
            selectionModel.checkOnly = this.checkOnly;

            if (!this.ignoreChanges)
            {
                this.addListener('beforeselect', this.onTreePanelBeforeSelect, this);
            }

            this.addListener('selectionchange', this.onTreePanelSelectionChange, this);
        }

        if (this.contextMenu)
        {
            if (typeof this.contextMenu == 'string')
            {
                this.contextMenu = Ext.menu.Manager.get(this.contextMenu);
            }

            this.addListener('itemcontextmenu', this.onTreePanelItemContextMenu, this);
        }

        this.addListener('render', this.onTreePanelRender, this);
        this.addListener('beforeitemexpand', this.onTreePanelBeforeItemExpand, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onTreePanelBeforeItemExpand: function (node)
    {
        var result = true;

        if (this.demandStore && (!node.isLoaded() || node.childNodes.length == 0 || !node.childNodes[0].data || !node.childNodes[0].data.text))
        {
            if (this.loadIncrementally)
            {
                this.loadData(node);
                result = false;
            }
            else
            {
                this.populateBranch(node);
            }
        }

        return result;
    },

    onTreePanelBeforeSelect: function (gridView, htmlElement, selections)
    {
        return Computronix.Ext.confirmContinue();
    },

    onTreePanelDeleteButtonClick: function (button, event)
    {
        this.removeSelected();
    },

    onTreePanelItemContextMenu: function (gridView, record, htmlElement, rowIndex, event)
    {
        event.preventDefault();
        this.contextMenu.showAt(event.getXY());
    },

    onTreePanelNewButtonClick: function (button, event)
    {
    },

    onTreePanelRender: function (tree)
    {
        if (this.loadOnRender && this.demandStore)
        {
            this.loadData();
        }
    },

    onTreePanelSelectionChange: function (selectionModel, selections)
    {
        if (this.dependentLoadMethod && selections.length == 1)
        {
            this.dependentLoadMethod.call(this, selections[0].getId());
        }

        if (this.enableDelete)
        {
            if (this.canDelete(selections))
            {
                this.deleteButton.enable();
            }
            else
            {
                this.deleteButton.disable();
            }
        }
    },

    // Methods
    //=============================================================================
    //
    addColumn: function (config)
    {
        this.headerCt.add(config);
    },

    canAdd: function ()
    {
        return true;
    },

    canDelete: function (records)
    {
        var result = false;
        var value;

        if (Ext.isArray(records))
        {
            if (records.length > 0)
            {
                result = true;

                Ext.each(records, function (record)
                {
                    value = record.get(Ext.String.format('{0}{1}', record.idProperty, this.canDeleteFieldSuffix));

                    if (value == false)
                    {
                        result = false;
                        return false;
                    }
                }, this);
            }
        }
        else
        {
            if (records)
            {
                value = records.get(Ext.String.format('{0}{1}', records.idProperty, this.canDeleteFieldSuffix));

                if (!Ext.isDefined(value) || value)
                {
                    result = true;
                }
            }
        }

        return result;
    },

    deleteRow: function (treeView, rowIndex, columnIndex, action, event, record)
    {
        var node = record;
        var nextNode = null;

        if (!this.disableSelection)
        {
            if (node.nextSibling)
            {
                nextNode = node.nextSibling;
            }
            else if (node.previousSibling)
            {
                nextNode = node.previousSibling;
            }
            else if (node.parentNode && node.parentNode.isVisible())
            {
                nextNode = node.parentNode;
            }
        }

        node.remove();

        if (this.demandStore)
        {
            this.demandStore.remove(this.demandStore.getById(node.getId()));
        }

        if (nextNode)
        {
            Ext.Function.defer(this.select, 15, this, [nextNode]);
        }
    },

    deselect: function (records, suppressEvent)
    {
        this.getSelectionModel().deselect(records, suppressEvent);
    },

    deselectAll: function (suppressEvent)
    {
        this.getSelectionModel().deselectAll(suppressEvent);
    },

    getChild: function (id)
    {
        var result = null;
        var rootNode = this.getRootNode();

        if (this.demandStore)
        {
            result = rootNode.findChild(this.demandStore.getNewModel().idProperty, id, true);
        }
        else
        {
            result = rootNode.findChild(rootNode.idProperty, id, true);
        }

        return result;
    },

    getChildAt: function (index)
    {
        var view = this.getView();

        return view.getRecord(view.getNode(index));
    },

    getChildren: function (parentValue)
    {
        var result;

        if (this.demandStore.getCount() > 0)
        {
            result = this.demandStore.getGroups();

            if (result)
            {
                // TODO: Check with Micah about improper parent key values for root items.
                if (parentValue == this.rootParentValue)
                {
                    result = result.getAt(0).items
                }
                else
                {
                    result = result.get(parentValue).items
                }
            }
            else
            {
                result = [];
            }
        }
        else
        {
            result = [];
        }

        return result;
    },

    getParams: function (params)
    {
        return params || {};
    },

    getSelected: function ()
    {
        return this.getSelectionModel().selected;
    },

    insertColumn: function (index, config)
    {
        this.headerCt.insert(index, config);
        this.getView().refresh();
    },

    isSelected: function (record)
    {
        return this.getSelectionModel().isSelected(record);
    },

    loadData: function (node)
    {
        var params = {};

        if (node)
        {
            params[this.parentField] = node.getId();
        }
        else
        {
            params[this.parentField] = this.rootParentValue;
            this.getRootNode().removeAll();
            this.demandStore.clear();
        }

        this.demandStore.load(
        {
            params: this.getParams(params),
            callback: this.treePanelDemandStoreLoadComplete,
            scope: this
        },
        this.loadIncrementally);
    },

    populateBranch: function (node)
    {
        var childNodes;
        var newChild;

        if (this.demandStore && node)
        {
            node.removeAll();

            if (this.demandStore.getCount() > 0)
            {
                if (node == this.getRootNode())
                {
                    childNodes = this.getChildren(this.rootParentValue);
                }
                else
                {
                    childNodes = this.getChildren(node.getId());
                }

                if (childNodes.length > 0)
                {
                    Ext.each(childNodes, function (childNode)
                    {
                        newChild = Ext.clone(childNode.data);

                        if (this.textField)
                        {
                            newChild.text = childNode.get(this.textField);
                        }

                        if (this.iconClsField)
                        {
                            newChild.iconCls = childNode.get(this.iconClsField);
                        }

                        if (this.loadIncrementally)
                        {
                            newChild.leaf = (this.hasChildrenField &&
                                !childNode.get(this.hasChildrenField));
                        }
                        else
                        {
                            newChild.leaf = (this.getChildren(childNode.getId()).length == 0);
                        }

                        this.populateNode(newChild);
                        newChild = node.appendChild(newChild);
                    }, this);

                    if (this.loadIncrementally)
                    {
                        node.expand(false, Ext.Function.pass(this.treePanelPopulateBranchComplete,
                            [node], this), this);
                    }
                    else
                    {
                        Ext.Function.defer(this.treePanelPopulateBranchComplete, 15, this, [node]);
                    }
                }
            }
        }
    },

    // Stub method for adding custom node initialization processing.
    populateNode: function (node)
    {
    },

    pruneChildren: function (parentNode)
    {
        var children;

        if (this.demandStore)
        {
            if (this.demandStore.getCount() > 0)
            {
                children = this.demandStore.getGroups(parentNode.getId());

                if (children)
                {
                    if (Ext.isArray(children))
                    {
                        children = children[0].children;
                    }
                    else
                    {
                        children = children.children;
                    }

                    Ext.each(children, function (node)
                    {
                        node = this.getChildAt(node);
                        this.pruneChildren(node);
                        this.demandStore.remove(node);
                        // We do not what the pruned records to appear as deletes.
                        this.demandStore.removed.pop();
                        node.remove();
                    }, this);
                }
            }
        }

        if (!this.loadIncrementally && (this.getChildren(parentNode.getId()).length == 0))
        {
            parentNode.leaf = true;
        }
        else
        {
            parentNode.leaf = false;
            parentNode.appendChild({});
            parentNode.collapse();
        }

    },

    removeSelected: function ()
    {
        var selected = this.getSelected();

        selected.each(function (node)
        {
            this.deselect(node);
            node.remove();

            if (this.demandStore)
            {
                this.demandStore.remove(node);
            }
        }, this);
    },

    restorePath: function ()
    {
        var selectCallback =
            function(success, lastNode, node)
            {
                var elementList;
                if (success)
                {
                    // Tree is unable to properly show focus on for some reason
                    // so we're using this to force the issue. Focus on the
                    // first cell in row that we've just asked the tree to select.
                    elementList = node.getElementsByTagName("td");
                    if (elementList)
                    {
                        elementList[0].focus();
                        if (lastNode.getPath() == this.lastPath)
                        {
                            this.lastPath = null;
                        }
                    }
                }
            };


        if (this.lastPath)
        {
            this.selectPath(this.lastPath, null, '/', selectCallback);
        }
    },

    select: function (records, keepExisting, suppressEvent)
    {
        this.getSelectionModel().select(records, keepExisting, suppressEvent);
    },

    selectAll: function (suppressEvent)
    {
        this.getSelectionModel().selectAll(suppressEvent);
    },

    setLastPath: function (node)
    {
        if (node)
        {
            this.lastPath = node.getPath();
        }
    },

    treePanelExpandComplete: function (node)
    {
        if (!this.demandStore || !this.loadIncrementally)
        {
            this.restorePath();
        }
    },

    treePanelPopulateBranchComplete: function (node)
    {
        this.restorePath();
    },

    treePanelDemandStoreLoadComplete: function (records, operation, success)
    {
        if (this.destroyed)
        {
            return;
        }

        var node = null;
        var parentId = operation.initialConfig.params[this.parentField];

        if (parentId == this.rootParentValue)
        {
            node = this.getRootNode();
        }
        else
        {
            node = this.getChild(parentId);
        }

        this.populateBranch(node);
    }
});

// Computronix dataview extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.view.View',
{
    extend: 'Ext.view.View',
    alias: 'widget.computronixextdataview',
    itemSelector: '',
    store: '',

    // Data Members
    //=============================================================================
    //
    parentForm: null,

    // The index of the row used to populate the dataview.
    rowIndex: 0,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        Ext.applyIf(this,
        {
            tpl: Ext.String.format('{{0}}', this.name)
        });

        this.callParent();

        this.addListener('afterrender', this.onViewViewAfterRender, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onViewViewAfterRender: function (component)
    {
        if (this.parentForm)
        {
            if (typeof this.parentForm == 'string')
            {
                this.parentForm = Ext.getCmp(this.parentForm);
            }
        }
        else
        {
            this.parentForm = Computronix.Ext.getForm(this);
        }

        if (this.parentForm)
        {
            if (!this.store && this.parentForm)
            {
                this.store = this.parentForm.store;
            }

            // Event handlers are added here because we have to wait until after render for ownerCt to be valid.
            this.addManagedListener(this.parentForm, 'afterloaddata', this.onViewViewFormAfterLoadData, this);
        }

        this.update();
    },

    onViewViewFormAfterLoadData: function (form)
    {
        this.update();
    },

    // Methods
    //=============================================================================
    //
    update: function ()
    {
        if (this.parentForm)
        {
            this.rowIndex = this.parentForm.rowIndex;
        }

        if (this.store && this.store.getCount() > this.rowIndex)
        {
            this.tpl[this.tplWriteMode](this.getContentTarget(), this.store.getAt(this.rowIndex).data);
        }
    }
});

// Computronix message box extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.window.MessageBox',
{
    extend: 'Ext.window.MessageBox',
    alias: 'widget.computronixextmessagebox',

    // Data Members
    //=============================================================================
    //
    lastFocusedElement: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.callParent();

        this.addListener('beforerender', this.onMessageBoxBeforeRender, this);
        this.addListener('beforeshow', this.onMessageBoxBeforeShow, this);
        this.addListener('hide', this.onMessageBoxHide, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onMessageBoxBeforeRender: function (window)
    {
        this.getDockedItems('toolbar[dock="bottom"]')[0].setBorder(false);
    },

    onMessageBoxHide: function (window)
    {
        if (this.lastFocusedElement && !this.lastFocusedElement.destroyed)
        {
            try
            {
                this.lastFocusedElement.focus(100);
            }
            catch (exception)
            {
            }
        }
    },

    onMessageBoxBeforeShow: function (window)
    {
        this.lastFocusedElement = Ext.Element.getActiveElement(true);
    },
});

// Computronix window extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.window.Window',
{
    extend: 'Ext.window.Window',
    alias: 'widget.computronixextwindow',
    constrain: true,
    header: null,

    // Data Members
    //=============================================================================
    //
    closeAnimateTarget: null,
    focusFirst: true,
    restoreFocus: true,
    lastFocusedElement: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (this.restoreFocus && !this.lastFocusedElement)
        {
            this.lastFocusedElement = new Ext.get(Ext.Element.getActiveElement());
        }

        this.callParent();

        this.addListener('show', this.onWindowShow, this);
        this.addListener('close', this.onWindowClose, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onWindowClose: function (window)
    {
        if (this.restoreFocus && this.lastFocusedElement && this.lastFocusedElement.el)
        {
            this.lastFocusedElement.focus(100);
        }
    },

    onWindowShow: function (window)
    {
        var form = this.down('computronixextform');

        this.checkPosition();

        if (this.closeAnimateTarget)
        {
            this.animateTarget = this.closeAnimateTarget;
        }

        if (form)
        {
            if (this.focusFirst)
            {
                form.focusFirst();
            }
        }
    },

    // Methods
    //=============================================================================
    //
    checkPosition: function (center)
    {
        var width, height, adjusted = false;
        var body;

        // If state is being restored then don't center the window.
        if (this.stateful && this.stateId && Ext.state.Manager.get(this.stateId))
        {
            center = false;
            this.setSize(this.width, this.height);
            this.setPosition(this.x, this.y, true);
        }

        if (Computronix.Ext.viewport)
        {
            width = Computronix.Ext.viewport.getWidth();
            height = Computronix.Ext.viewport.getHeight();
        }
        else
        {
            body = Ext.getBody();
            width = body.getWidth();
            height = body.getHeight();
        }

        if (width > 0 && this.getWidth() > width)
        {
            if (this.resizable)
            {
                this.setWidth(width);
            }

            adjusted = true;
        }

        if (height > 0 && this.getHeight() > height)
        {
            if (this.resizable)
            {
                this.setHeight(height);
            }

            adjusted = true;
        }

        if (adjusted || (width > 0 && this.x > width - 20) || this.y < 0 || (height > 0 && this.y > height - 20) || center)
        {
            if (this.resizable || center)
            {
                this.center();
            }
            else
            {
                this.setPosition(0, 0);
            }
        }
    },

    setStateId: function (stateId)
    {
        this.stateful = true;
        this.stateId = stateId;
        this.addStateEvents(['maximize', 'restore', 'resize', 'dragend']);
        this.initState();
    }
});

// Global variable declarations (must follow component declarations).
//=============================================================================
//
// Reference to the busy panel in the footer panel.
Computronix.Ext.busy = new Computronix.Ext.Busy();

// Reference to the global error messages.
Computronix.Ext.errors = [];

// Reference to the message box.
Computronix.Ext.messageBox = new Computronix.Ext.window.MessageBox();

// Reference to the message tool in the footer panel.
Computronix.Ext.messageTool = new Computronix.Ext.MessageTool();

// Reference to the microhelp tool in the footer panel.
Computronix.Ext.microhelpTool = new Computronix.Ext.MicrohelpTool();

// Reference to the system monitor tool in the footer panel.
Computronix.Ext.systemMonitorTool = new Computronix.Ext.SystemMonitorTool();

// Reference to the global warning messages.
Computronix.Ext.warnings = [];

// Development tools.
//=============================================================================
//

// Removes any private values from an object.
cleanObject = function (object)
{
    var result = {}

    for (var member in object)
    {
        try
        {
            if (member.substr(0, 1) == '_')
            {
                result[member] = '[Hidden]';
            }
            else
            {
                result[member] = object[member];
            }
        }
        catch (exception)
        {
            result[member] = '[Not Accessible]';
        }
    }

    return result;
}

function getCallStack(arguments, delimiter)
{
    var result = '';
    var callee = arguments.callee;

    if (!delimiter)
    {
        delimiter = '\n';
    }

    while (callee)
    {
        result += ((callee.$name || 'anonymous') + delimiter);
        callee = callee.caller;
    }

    return result;
}

function getMembers(object, showFunctions, shallow, delimiter, limit)
{
    var message = '';

    if (!delimiter)
    {
        delimiter = ', ';
    }

    if (typeof object == 'string' || typeof object == 'boolean' || typeof object == 'number')
    {
        message = object.toString();
    }
    else
    {
        object = cleanObject(object);

        for (var member in object)
        {
            if (!limit || message.length < limit)
            {
                try
                {
                    if (typeof object[member] == 'string')
                    {
                        message += member + ': \'' + object[member] + '\'' + delimiter;
                    }
                    else if (typeof object[member] == 'function')
                    {
                        if (showFunctions)
                        {
                            message += member + ': function()' + delimiter;
                        }
                    }
                    else if (typeof object[member] == 'object' && shallow == false)
                    {
                        message += member + ': {' + getMembers(object[member], showFunctions, shallow, ', ', 100) + '}' + delimiter;
                    }
                    else
                    {
                        message += member + ': ' + object[member] + delimiter;
                    }
                }
                catch (exception)
                {
                    message += member + ': [Not Accessible]' + delimiter;
                }
            }
            else
            {
                message += '...';
                break;
            }
        }

        if (message.substr(message.length - delimiter.length) == delimiter)
        {
            message = message.substr(0, message.length - delimiter.length);
        }
    }

    return message
}

// Shows the value of an objects members.
function showMembers(object, showFunctions, shallow)
{
    alert('Members of ' + typeof object + '\n' + getMembers(object, showFunctions, shallow, '\n'));
}

// Adds the specified message to the console log.
function log(message)
{
    if (typeof message == 'object')
    {
        message = getMembers(message);
    }
    else
    {
        for (var index = 1; index < arguments.length; index++)
        {
            message = message.replace('{' + (index - 1) + '}', arguments[index]);
        }
    }

    Computronix.Ext.logMessages.add(
        Computronix.Ext.logMessages.getCount(), { timestamp: Ext.Date.now(), message: message });

    if (typeof console != 'undefined')
    {
        console.log(message);
    }
}
