/// External javascript references required for Intellisense. -------------------------------------
/// <reference path='../Ext/ext-all-debug.js' />

Ext.namespace('Computronix.Ext');
Ext.namespace('Computronix.Ext.form.field');
Ext.namespace('Computronix.Ext.grid.column');
Ext.namespace('Computronix.POSSE.PosseInternal');
