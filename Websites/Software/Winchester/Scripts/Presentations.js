Computronix.Ext.renderViewport = function ()
{
    if (Computronix.Ext.viewport == null)
    {
        Computronix.Ext.viewport = Ext.create('Computronix.Ext.Viewport');
    }

    Ext.getCmp('cxLogoffButton').setVisible(Computronix.Ext.showLogoffButton);

    Computronix.Ext.headerPanel = Ext.getCmp('headerPanel');
    Computronix.Ext.centerPanel = Ext.getCmp('centerPanel');
    Computronix.Ext.footerPanel = Ext.getCmp('footerPanel');

    Computronix.POSSE.PosseInternal.loadLogo();
    Ext.getCmp('cxApplicationName').setText(Computronix.Ext.systemConfiguration.ApplicationName);
    Ext.getCmp('cxAboutButton').setText(Ext.String.format('About {0}', Computronix.Ext.systemConfiguration.ApplicationName));
    Ext.getCmp('cxDataBaseName').setText(Computronix.Ext.systemConfiguration.TnsEntry);
    return Computronix.Ext.viewport;
}

// Viewport extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.Viewport',
{
    extend: 'Ext.container.Viewport',
    id: 'viewport',
    layout: 'border',
    items:
    [
        {
            xtype: 'computronixextpanel',
            region: 'north',
            id: 'headerPanel',
            height: 30,
            bbar:
            {
                xtype: 'toolbar',
                ui: 'header',
                enableOverflow: true,
                height: 30,
                items:
                [
                    {
                        xtype: 'tbspacer',
                        width: 5
                    },
                    {
                        xtype: 'button',
                        cls: 'cxapplicationtoolbartext',
                        href: '#home=true',
                        hrefTarget: '',
                        iconCls: 'cxposseicon',
                        id: 'cxApplicationName',
                        keyHandlers: { ENTER: Ext.emptyFn },
                        text: 'POSSE',
                        ui: 'header-toolbar',
                    },
                    {
                        xtype: 'tbspacer',
                        width: 10
                    },
                    {
                        xtype: 'button',
                        disabled: true,
                        href: '#todo=true',
                        hrefTarget: '',
                        iconCls: 'cxtodoicon',
                        id: 'cxToDoButton',
                        keyHandlers: { ENTER: Ext.emptyFn },
                        text: 'To Do List',
                        ui: 'header-toolbar',
                    },
                    {
                        xtype: 'computronixextbutton',
                        disabled: true,
                        hrefTarget: '',
                        iconCls: 'cxprocessicon',
                        id: 'cxCurrentProcessButton',
                        keyHandlers: { ENTER: Ext.emptyFn },
                        text: 'Current Process',
                        ui: 'header-toolbar',
                    },
                    {
                        xtype: 'computronixposseinternalbusinessmenu',
                        disabled: true,
                        id: 'cxMainMenu',
                        ui: 'header-toolbar',
                    },
                    { xtype: 'tbfill' },
                    {
                        xtype: 'computronixposseinternaldelegationlistbutton',
                        hidden: true,
                        id: 'cxDelegationListButton',
                        ui: 'header-toolbar',
                    },
                    {
                        xtype: 'computronixposseinternaluserprofilesplitbutton',
                        disabled: true,
                        id: 'cxHeaderUserName',
                        ui: 'header-toolbar',
                    },
                    {
                        xtype: 'computronixextsplitbutton',
                        id: 'cxHelpButton',
                        text: 'Help',
                        iconCls: 'cxpossehelpicon',
                        ui: 'header-toolbar',
                        chooseItem: false,
                        handler: function (button)
                        {
                            button.showMenu();
                        },
                        menu:
                        {
                            xtype: 'menu',
                            showSeparator: false,
                            cls: 'cxheadermenu',
                            listeners: { beforeshow: Computronix.POSSE.PosseInternal.onHelpMenuBeforeShow },
                            items:
                            [
                                {
                                    xtype: 'computronixextmenuitem',
                                    text: 'General Help',
                                    href: 'Help/Default.htm',
                                    hrefTarget: 'WinchesterHelp',
                                    iconCls: 'cxpossehelpicon',
                                    cls: 'cxheadermenu'
                                },
                                {
                                    xtype: 'computronixextmenuitem',
                                    text: 'Remote Assistance',
                                    iconCls: 'cxremoteassistanceheadermenuicon',
                                    cls: 'cxheadermenu',
                                    listeners: { click: { fn: function () { Computronix.POSSE.PosseInternal.RemoteAssistanceDialog.showRemoteAssistanceDialog(); } } }
                                },
                                {
                                    xtype: 'computronixextmenuitem',
                                    id: 'cxAboutButton',
                                    iconCls: 'cxposseicon',
                                    cls: 'cxheadermenu',
                                    listeners:
                                    {
                                        click: function ()
                                        {
                                            Computronix.POSSE.PosseInternal.AboutDialog.show();
                                        }
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        {
            xtype: 'computronixposseinternalsearchpanel',
            collapsed: true,
            collapseFirst: false,
            collapsible: true,
            frame: true,
            hidden: true,
            id: 'searchPanel',
            region: 'west',
            split: true,
        },
        {
            xtype: 'computronixextpanel',
            region: 'center',
            id: 'centerPanel'
        },
        {
            xtype: 'computronixextpanel',
            region: 'south',
            id: 'footerPanel',
            ui: 'footer',
            tbar:
            {
                padding: '2 0 2 6',
                items:
                [
                    Computronix.Ext.microhelpTool,
                    {
                        xtype: 'computronixexttbtext',
                        id: 'previousLogon',
                        margin: '0 6 0 4',
                    },
                    { xtype: 'tbseparator' },
                    Computronix.POSSE.PosseInternal.remoteAssistanceTool,
                    Computronix.Ext.systemMonitorTool,
                    { xtype: 'tbseparator' },
                    {
                        xtype: 'computronixexttbtext',
                        id: 'cxDataBaseName',
                        margin: '0 6 0 4'
                    },
                    { xtype: 'tbseparator' },
                    Computronix.Ext.messageTool,
                    { xtype: 'tbseparator' },
                    Computronix.Ext.busy
                ]
            }
        }
    ]
});

// Message Summary extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.MessageSummary',
{
    extend: 'Computronix.Ext.window.Window',

    statics:
    {
        instance: null,

        showMessageSummary: function (button, messages)
        {
            if (Computronix.Ext.MessageSummary.instance == null)
            {
                Computronix.Ext.MessageSummary.instance = Ext.create('Computronix.Ext.MessageSummary');

                Computronix.Ext.MessageSummary.instance.setup(button, messages);
                Computronix.Ext.MessageSummary.instance.setSize(300, 250);

                if (Computronix.Ext.viewport)
                {
                    Computronix.Ext.MessageSummary.instance.showAt(
                        Computronix.Ext.viewport.width - 300 - 105,
                        Computronix.Ext.viewport.height - 250 - 32);
                }
                else
                {
                    Computronix.Ext.MessageSummary.instance.show();
                }
            }
        }
    },

    autoScroll: true,
    bodyPadding: 5,
    focusOnToFront: false,
    height: 200,
    iconCls: 'cxerroricon',
    title: 'Message Summary',
    width: 300,

    // Methods
    //=============================================================================
    //
    close: function ()
    {
        this.callParent(arguments);

        Computronix.Ext.MessageSummary.instance = null;
    },

    setup: function (button, messages)
    {
        this.animateTarget = button;
        this.removeAll();

        if (messages.length > 0)
        {
            this.update(messages.join('<br/><br/>'));
        }
    }
});
