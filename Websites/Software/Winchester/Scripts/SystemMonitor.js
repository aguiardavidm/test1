/// External javascript references required for Intellisense. -------------------------------------
/// <reference path='../Ext/ext-all-debug.js' />
/// <reference path='../Scripts/NameSpaces.js' />
/// <reference path='../Scripts/Computronix.Ext.js' />
/// <reference path='../Scripts/Criteria.js' />

Ext.define('systemMonitorStore',
{
    extend: 'Computronix.Ext.data.Model',
    idProperty: 'id',
    identifier: 'uuid',
    fields:
    [
        {
            name: 'objectId',
            type: 'string'
        },
        {
            name: 'type',
            type: 'string'
        },
        {
            name: 'text',
            type: 'string'
        }
    ]
});

Ext.create('Ext.data.TreeStore',
{
    storeId: 'systemMonitorStore',
    model: 'systemMonitorStore',
    proxy:
    {
        type: 'memory'
    },
    root:
    {
        expanded: true
    }
});

Ext.define('Computronix.Ext.SystemMonitor',
{
    extend: 'Computronix.Ext.window.Window',

    statics:
    {
        instance: null,

        renderSystemMonitor: function ()
        {
            if (Computronix.Ext.SystemMonitor.instance == null || Computronix.Ext.SystemMonitor.instance.isDestroyed)
            {
                Computronix.Ext.SystemMonitor.instance = Ext.create('Computronix.Ext.SystemMonitor');
            }

            return Computronix.Ext.SystemMonitor.instance;
        },

        showSystemMonitor: function (minimize)
        {
            var systemMonitor = Computronix.Ext.SystemMonitor.renderSystemMonitor();

            if (minimize)
            {
                systemMonitor.minimize();
            }
            else
            {
                systemMonitor.show();
            }
        }
    },

    title: 'System Monitor',
    iconCls: 'cxsystemmonitoricon',
    layout: 'border',
    width: 1200,
    height: 500,
    plain: true,
    border: false,

    // Data Members
    //=============================================================================
    //
    tree: null,
    panel: null,
    requests: null,
    abortedRequests: null,
    requestTimes: null,
    requestId: 0,
    scripts: null,
    errors: null,
    errorId: 0,
    messageLog: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        if (Computronix.Ext.systemMonitorTool && Computronix.Ext.systemMonitorTool.rendered)
        {
            this.minimizable = true;
            this.animateTarget = Computronix.Ext.systemMonitorTool;
        }

        this.requests = new Ext.util.HashMap();
        this.abortedRequests = new Ext.util.HashMap();
        this.requestTimes = new Ext.util.HashMap();
        this.scripts = new Ext.util.HashMap();
        this.errors = new Ext.util.HashMap();

        this.dockedItems =
        [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                border: false,
                enableOverflow: true,
                items:
                [
                    {
                        xtype: 'computronixexttbtext',
                        text: Ext.String.format('Computronix Ext: v{0}', Computronix.Ext.version)
                    },
                    { xtype: 'tbseparator' },
                    {
                        xtype: 'computronixexttbtext',
                        text: Ext.String.format('Ext JS: v{0}', Ext.getVersion().version)
                    },
                    { xtype: 'tbseparator' },
                    {
                        xtype: 'computronixexttbtext',
                        text: Ext.isIE ? Ext.String.format('IE: v{0}', Ext.ieVersion) :
                        Ext.isChrome ? Ext.String.format('Chrome: v{0}', Ext.chromeVersion) :
                        Ext.isSafari ? Ext.String.format('Safari: v{0}', Ext.safariVersion) :
                        Ext.isGecko ? Ext.String.format('FireFox: v{0}', Ext.firefoxVersion) : null
                    },
                    { xtype: 'tbfill' },
                    {
                        xtype: 'computronixextbutton',
                        id: 'systemMonitorClear',
                        text: 'Clear',
                        iconCls: 'cxclearicon',
                        margin: '6 6 0 0',
                        width: 75,
                        handler: this.clear,
                        scope: this,
                    },
                    {
                        xtype: 'computronixextbutton',
                        id: 'systemMonitorOK',
                        text: 'Refresh',
                        iconCls: 'cxrefreshicon',
                        margin: '6 6 0 0',
                        width: 75,
                        handler: this.refresh,
                        scope: this,
                    },
                    {
                        xtype: 'computronixextbutton',
                        id: 'systemMonitorMinimize',
                        text: 'Minimize',
                        iconCls: 'cxdownicon',
                        margin: '6 6 0 0',
                        width: 75,
                        handler: this.minimize,
                        scope: this,
                    },
                    {
                        xtype: 'computronixextbutton',
                        id: 'systemMonitorClose',
                        text: 'Close',
                        iconCls: 'cxexiticon',
                        margin: '6 6 0 0',
                        width: 75,
                        handler: this.close,
                        scope: this,
                    },
                ],
            },
        ];

        this.tree = new Computronix.Ext.tree.Panel(
        {
            region: 'west',
            title: 'System Resources',
            store: 'systemMonitorStore',
            collapsible: true,
            split: true,
            frame: true,
            width: 400,
            columns:
            [
                {
                    xtype: 'computronixexttreecolumn',
                },
            ],
            listeners:
            {
                itemclick:
                {
                    fn: this.onSystemMonitorTreeItemClick,
                    scope: this,
                },
                beforeitemexpand:
                {
                    fn: this.onSystemMonitorTreeBeforeItemExpand,
                    scope: this,
                },
            },
        });

        this.panel = new Computronix.Ext.panel.Panel(
        {
            region: 'center',
            frame: true,
        });

        this.items =
        [
            this.tree,
            this.panel,
        ];

        this.callParent(arguments);

        this.addListener('show', this.onSystemMonitorShow, this);
        this.addListener('close', this.onSystemMonitorClose, this);
        this.addListener('minimize', this.onSystemMonitorMinimize, this);
        this.addManagedListener(Ext.Ajax, 'beforerequest', this.onSystemMonitorBeforeRequest, this);
        this.addManagedListener(Ext.Ajax, 'requestcomplete', this.onSystemMonitorRequestComplete, this);
        this.addManagedListener(Ext.Ajax, 'requestexception', this.onSystemMonitorRequestException, this);
        this.addManagedListener(Computronix.Ext.Ajax, 'beforerequest', this.onSystemMonitorBeforeRequest, this);
        this.addManagedListener(Computronix.Ext.Ajax, 'requestcomplete', this.onSystemMonitorRequestComplete, this);
        this.addManagedListener(Computronix.Ext.Ajax, 'requestexception', this.onSystemMonitorRequestException, this);
        this.addManagedListener(Computronix.Ext.Ajax, 'batchcomplete', this.onSystemMonitorBatchComplete, this);
        this.addManagedListener(Computronix.Ext.ScriptLoader, 'scriptload', this.onSystemMonitorScriptLoad, this);
    },

    // Event Handlers
    //=============================================================================
    //
    onSystemMonitorBatchComplete: function (ajax, ajaxBatchId, success)
    {
        var request = { time: new Date() };

        request['ajaxBatchId'] = ajaxBatchId;
        request['success'] = success ? 'Completed' : 'Failed';

        this.requests.add(this.requestId, request);
        this.requestId++;
    },

    onSystemMonitorBeforeRequest: function (connection, options)
    {
        var requestKey;

        if (options.params)
        {
            requestKey = Ext.encode({ url: options.url, params: options.params });
        }
        else
        {
            requestKey = Ext.encode(options.url);
        }

        if (this.requestTimes.containsKey(requestKey))
        {
            this.requestTimes.removeAtKey(requestKey);
        }

        this.requestTimes.add(requestKey, new Date());
    },

    onSystemMonitorClose: function (window)
    {
        if (Computronix.Ext.systemMonitorTool && Computronix.Ext.systemMonitorTool.rendered)
        {
            Computronix.Ext.systemMonitorTool.setButton(false);
        }

        Computronix.Ext.SystemMonitor.instance = null
    },

    onSystemMonitorGetMessageLog: function (response, options)
    {
        var selected = this.tree.getSelectionModel().selected;

        this.messageLog = response.responseText;

        if (selected.getCount() > 0 && selected.first().get('type') == 'messageLog')
        {
            this.refreshMessageLog(selected.first());
        }
    },

    onSystemMonitorMinimize: function ()
    {
        if (Computronix.Ext.systemMonitorTool && Computronix.Ext.systemMonitorTool.rendered)
        {
            Computronix.Ext.systemMonitorTool.setButton(true);
            this.hide();
        }
    },

    onSystemMonitorScriptLoad: function (scriptLoader, loader, response, options)
    {
        if (!this.scripts.containsKey(options.params.scriptId) && response)
        {
            this.scripts.add(options.params.scriptId, response.responseText);
        }
    },

    onSystemMonitorShow: function (window)
    {
        this.refresh();
    },

    onSystemMonitorRequestComplete: function (connection, response, options)
    {
        var now;
        var requestKey;
        var duration;
        var request;
        var pos;

        if (options.params)
        {
            requestKey = Ext.encode({ url: options.url, params: options.params });
        }
        else
        {
            requestKey = Ext.encode(options.url);
        }

        now = new Date().getTime();
        request = {};
        pos = options.url.indexOf('?');

        if (this.requestTimes.containsKey(requestKey))
        {
            request['time'] = this.requestTimes.get(requestKey);
            request['duration'] = now - request.time.getTime();
        }

        if (pos > -1)
        {
            request['url'] = options.url.substring(0, options.url.indexOf('?'));
        }
        else
        {
            request['url'] = options.url
        }

        request['options'] = Ext.clone(options);
        request['response'] = response.responseText;
        request['cached'] = response.cached;

        this.requests.add(this.requestId, request);
        this.requestId++;
    },

    onSystemMonitorRequestException: function (connection, response, options)
    {
        var now;
        var requestKey;
        var error;
        var exception;
        var pos;

        if (options.params)
        {
            requestKey = Ext.encode({ url: options.url, params: options.params });
        }
        else
        {
            requestKey = Ext.encode(options.url);
        }

        now = new Date().getTime();
        error = {};
        pos = options.url.indexOf('?');

        if (this.requestTimes.containsKey(requestKey))
        {
            error['time'] = this.requestTimes.get(requestKey);
            error['duration'] = now - error.time.getTime();
        }

        if (pos > -1)
        {
            error['url'] = options.url.substring(0, options.url.indexOf('?'));
        }
        else
        {
            error['url'] = options.url
        }

        if (response.aborted || response.timedout)
        {
            error['message'] = Ext.String.capitalize(response.statusText);
        }
        else
        {
            try
            {
                exception = Ext.decode(response.responseText);
                error['message'] = exception.message;
                error['callStack'] = exception.callStack;
            }
            catch (exception)
            {
                if (response.responseText)
                {
                    error['message'] = response.responseText;
                }
                else
                {
                    error['message'] = 'Unknow Error';
                }
            }
        }

        error['options'] = Ext.clone(options);

        if (response.aborted)
        {
            this.abortedRequests.add(this.errorId, error);
        }
        else
        {
            this.errors.add(this.errorId, error);
        }

        this.errorId++;
    },

    onSystemMonitorTreeBeforeItemExpand: function (node, options)
    {
        var dataStore;
        var model;
        var key;
        var component;
        var name;
        var className;
        var text;
        var iconCls;
        var parentNode;
        var requestBatches;
        var requestBatchId;

        if (!node.isLoaded() || node.childNodes.length == 0 || !node.childNodes[0].data || !node.childNodes[0].data.text)
        {
            node.removeAll();

            switch (node.get('type'))
            {
                case 'stores':
                    node.appendChild(
                    {
                        iconCls: 'cxchangesicon',
                        id: 'changes',
                        leaf: true,
                        text: 'Outstanding Changes',
                        type: 'changes',
                    });

                    Ext.data.StoreManager.each(function (store)
                    {
                        if (store.isComputronixExtStore)
                        {
                            dataStore = node.appendChild({ type: 'store', id: store.storeId, text: Ext.String.format('{0} [{1} rows]',
                                store.storeId, store.getCount()), leaf: false, iconCls: 'cxdatastoreicon'
                            });

                            dataStore.appendChild({
                                type: 'storeChanges', id: Ext.String.format('{0}changes', store.storeId), objectId: store.storeId,
                                text: 'Outstanding Changes', leaf: true, iconCls: 'cxchangesicon'
                            });

                            dataStore.appendChild({
                                type: 'storeComponent', id: Ext.String.format('{0}component', store.storeId), objectId: store.storeId,
                                text: store.storeId, leaf: true, iconCls: 'cxcomponenticon'
                            });

                            model = store.getNewModel();
                            model.fieldsByName.each(function (field)
                            {
                                key = (model.idProperty == field.name);
                                dataStore.appendChild(
                                {
                                    iconCls: key ? 'cxkeycolumnicon' : 'cxcolumnicon',
                                    id: Ext.String.format('{0}{1}', store.storeId, field.name),
                                    key: key,
                                    leaf: true,
                                    objectId: field.name,
                                    text: Ext.String.format('{0} ({1})', field.name, field.type),
                                    type: 'field',
                                });
                            }, this);
                        }
                    });
                    break;
                case 'scripts':
                    this.scripts.each(function (scriptId, script)
                    {
                        node.appendChild(
                        {
                            iconCls: 'cxscripticon',
                            id: Ext.String.format('{0}script', scriptId),
                            leaf: true,
                            objectId: scriptId,
                            text: scriptId,
                            type: 'script',
                        });
                    }, this);
                    break;
                case 'components':
                    Ext.ComponentManager.each(function (componentId)
                    {
                        component = Ext.getCmp(componentId);

                        if (!component.ownerCt &&
                            component.$className != 'Computronix.Ext.SystemMonitor' &&
                            component.title != 'System Monitor' &&
                            !component.isXType('menu') &&
                            component.$className != 'Ext.tip.ToolTip' &&
                            component.$className != 'Ext.tip.QuickTip' &&
                            component.$className != 'Ext.grid.CellEditor')
                        {
                            name = componentId;

                            if (component.hasOwnProperty('name') && component.name)
                            {
                                name = component.name;
                            }
                            else if (component.hasOwnProperty('text') && component.text)
                            {
                                name = component.text;
                            }
                            else if (component.hasOwnProperty('title') && component.title)
                            {
                                name = component.title;
                            }

                            if (component.isViewport)
                            {
                                iconCls = 'cxviewporticon';
                            }
                            else if (component.isGrid)
                            {
                                iconCls = 'cxgridicon';
                            }
                            else if (component.isXType('gridcolumn'))
                            {
                                iconCls = 'cxcolumnicon';
                            }
                            else if (component.isForm)
                            {
                                iconCls = 'cxformicon';
                            }
                            else if (component.isFormField)
                            {
                                iconCls = 'cxfieldicon';
                            }
                            else if (component.isPanel)
                            {
                                iconCls = 'cxpanelicon';
                            }
                            else
                            {
                                iconCls = 'cxcomponenticon';
                            }

                            if (component.hasOwnProperty('paneId'))
                            {
                                className = Ext.String.format('[{0}] {1}', component.paneId, component.$className);
                            }
                            else
                            {
                                className = component.$className;
                            }
                            if (component.items)
                            {
                                node.appendChild(
                                {
                                    iconCls: iconCls,
                                    id: componentId,
                                    text: Ext.String.format('{0} ({1})', name, className),
                                    type: 'container',
                                });
                            }
                            else
                            {
                                node.appendChild(
                                {
                                    iconCls: iconCls,
                                    id: componentId,
                                    leaf: true,
                                    text: Ext.String.format('{0} ({1})', name, className),
                                    type: 'component',
                                });
                            }
                        }
                    }, this);
                    break;
                case 'container':
                    Ext.ComponentManager.each(function (componentId)
                    {
                        component = Ext.getCmp(componentId);

                        if ((component.ownerCt && component.ownerCt.getId() == node.get('id')) ||
                            component.isMenu && Ext.getCmp(node.get('id')) && Ext.getCmp(node.get('id')).menu == component)
                        {
                            name = componentId;

                            if (component.hasOwnProperty('name') && component.name)
                            {
                                name = component.name;
                            }
                            else if (component.hasOwnProperty('text') && component.text)
                            {
                                name = component.text;
                            }
                            else if (component.hasOwnProperty('title') && component.title)
                            {
                                name = component.title;
                            }

                            if (component.isViewport)
                            {
                                iconCls = 'cxviewporticon';
                            }
                            else if (component.isGrid)
                            {
                                iconCls = 'cxgridicon';
                            }
                            else if (component.isXType('gridcolumn'))
                            {
                                iconCls = 'cxcolumnicon';
                            }
                            else if (component.isForm)
                            {
                                iconCls = 'cxformicon';
                            }
                            else if (component.isFormField)
                            {
                                iconCls = 'cxfieldicon';
                            }
                            else if (component.isPanel)
                            {
                                iconCls = 'cxpanelicon';
                            }
                            else
                            {
                                iconCls = 'cxcomponenticon';
                            }

                            if (component.hasOwnProperty('paneId'))
                            {
                                className = Ext.String.format('[{0}] {1}', component.paneId, component.$className);
                            }
                            else
                            {
                                className = component.$className;
                            }

                            if (component.items || component.menu)
                            {
                                node.appendChild(
                                {
                                    iconCls: iconCls,
                                    id: componentId,
                                    text: Ext.String.format('{0} ({1})', name, className),
                                    type: 'container',
                                });
                            }
                            else
                            {
                                node.appendChild(
                                {
                                    iconCls: iconCls,
                                    id: componentId,
                                    leaf: true,
                                    text: Ext.String.format('{0} ({1})', name, className),
                                    type: 'component',
                                });
                            }
                        }
                    }, this);
                    break;
                case 'requests':
                    requestBatches = new Ext.util.HashMap();

                    this.requests.each(function (requestId, request)
                    {
                        text = null;
                        iconCls = null;

                        if (request.ajaxBatchId)
                        {
                            requestBatchId = request.ajaxBatchId;

                            if (requestBatches.containsKey(requestBatchId))
                            {
                                parentNode = requestBatches.get(requestBatchId)
                            }
                            else
                            {
                                parentNode = node.appendChild(
                                {
                                    iconCls: 'cxbatchicon',
                                    id: Ext.String.format('{0}{1}', requestBatchId, requestId),
                                    text: request.ajaxBatchId,
                                    type: 'requestBatch',
                                });
                            }

                            text = Ext.String.format('{0} at {1}', request.success, Ext.Date.format(request.time, 'i:s.u'));
                            iconCls = (request.success == 'Failed') ? 'cxstopicon' : 'cxcompleteicon'
                            parentNode.appendChild(
                            {
                                iconCls: iconCls,
                                id: requestId,
                                leaf: true,
                                text: text,
                                type: 'requestBatch',
                            });

                            requestBatches.removeAtKey(requestBatchId);

                            return true;
                        }
                        else
                        {
                            if (request.options.params && request.options.params.ajaxBatchId)
                            {
                                requestBatchId = request.options.params.ajaxBatchId;

                                if (requestBatches.containsKey(requestBatchId))
                                {
                                    parentNode = requestBatches.get(requestBatchId)
                                }
                                else
                                {
                                    parentNode = node.appendChild(
                                    {
                                        iconCls: 'cxbatchicon',
                                        id: Ext.String.format('{0}{1}', requestBatchId, requestId),
                                        text: request.options.params.ajaxBatchId,
                                        type: 'requestBatch',
                                    });
                                    requestBatches.add(requestBatchId, parentNode);
                                }
                            }
                            else
                            {
                                parentNode = node;
                            }
                        }

                        parentNode.appendChild(
                        {
                            type: 'request',
                            id: requestId,
                            text: this.getText(request),
                            leaf: true,
                            iconCls: request.cached ? 'cxcachedrequesticon' : 'cxrequesticon',
                        });
                    }, this);
                    break;
                case 'cachedRequests':
                    switch (Computronix.Ext.Ajax.requestCacheStorage)
                    {
                        case 'InProcess':
                            Computronix.Ext.Ajax.getRequestCache().each(function (requestCacheKey)
                            {
                                node.appendChild(
                                {
                                    type: 'cachedRequest',
                                    id: requestCacheKey,
                                    text: requestCacheKey,
                                    leaf: true,
                                    iconCls: 'cxcachedrequesticon',
                                });
                            }, this);
                            break;
                        case 'LocalStorage':
                            Ext.each(Computronix.Ext.Ajax.getRequestCache().getKeys(), function (requestCacheKey)
                            {
                                node.appendChild(
                                {
                                    iconCls: 'cxcachedrequesticon',
                                    id: requestCacheKey,
                                    leaf: true,
                                    text: requestCacheKey,
                                    type: 'cachedRequest',
                                });
                            }, this);
                            break;
                    }
                    break;
                case 'abortedRequests':
                    this.abortedRequests.each(function (errorId, error)
                    {
                        node.appendChild(
                        {
                            iconCls: 'cxabortedrequesticon',
                            id: Ext.String.format('{0}error', errorId),
                            leaf: true,
                            objectId: errorId,
                            text: this.getText(error),
                            type: 'abortedRequest',
                        });
                    }, this);
                    break;
                case 'errors':
                    this.errors.each(function (errorId, error)
                    {
                        node.appendChild(
                        {
                            iconCls: 'cxerroricon',
                            id: Ext.String.format('{0}error', errorId),
                            leaf: true,
                            objectId: errorId,
                            text: this.getText(error),
                            type: 'error',
                        });
                    }, this);
                    break;
                case 'scriptErrors':
                    Computronix.Ext.scriptErrors.each(function (errorId, error)
                    {
                        node.appendChild(
                        {
                            iconCls: 'cxerroricon',
                            id: Ext.String.format('{0}scriptError', errorId),
                            leaf: true,
                            objectId: errorId,
                            text: error.message,
                            type: 'scriptError',
                        });
                    }, this);
                    break;
            }
        }
    },

    onSystemMonitorTreeItemClick: function (view, record, htmlElement, index, event, options)
    {
        var store;
        var changes;
        var properties = {};

        switch (record.get('type'))
        {
            case 'store':
                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.grid.Panel(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: 'cxdatastoreicon',
                    flex: 1,
                    storeId: record.get('id'),
                    enableFilter: true
                }));
                break;
            case 'storeChanges':
                store = Ext.getStore(record.get('objectId'));
                changes = store.getChanges();

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.panel.Panel(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: 'cxchangesicon',
                    flex: 1,
                    autoScroll: false,
                    border: true,
                    items:
                    [
                        {
                            xtype: 'computronixexttextarea',
                            flex: 1,
                            readOnly: true,
                            fieldCls: 'x-form-field cxnoborder',
                            value: changes ? Ext.encode(changes).replace(/},{/g, '},\n{').replace(/,/g, ',\n').replace(/\[{/g, '[\n{').replace(/;/g, ';\n').replace(/></g, '>\n<') : null
                        }
                    ]
                }));
                break;
            case 'storeComponent':
                store = Ext.getStore(record.get('objectId'));
                properties = this.getProperties(properties, store);

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.grid.property.Grid(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: 'cxcomponenticon',
                    nameColumnWidth: 250,
                    flex: 1,
                    listeners: { 'beforeedit': function () { return false; } },
                    source: properties,
                }));
                break;
            case 'field':
                store = Ext.getStore(record.parentNode.get('id'));
                properties = this.getProperties(properties, store.getNewModel().fieldsByName.get(record.get('objectId')));

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.grid.property.Grid(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: record.get('key') ? 'cxkeycolumnicon' : 'cxcolumnicon',
                    nameColumnWidth: 250,
                    flex: 1,
                    listeners: { 'beforeedit': function () { return false; } },
                    source: properties,
                }));
                break;
            case 'changes':
                changes = Computronix.Ext.getChanges(true);

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.panel.Panel(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: 'cxchangesicon',
                    flex: 1,
                    autoScroll: false,
                    border: true,
                    items:
                    [
                        {
                            xtype: 'computronixexttextarea',
                            flex: 1,
                            readOnly: true,
                            fieldCls: 'x-form-field cxnoborder',
                            value: changes.length > 0 ? Ext.encode(changes).replace(/},{/g, '},\n{').replace(/,/g, ',\n').replace(/\[{/g, '[\n{').replace(/;/g, ';\n').replace(/></g, '>\n<') : null
                        }
                    ]
                }));
                break;
            case 'script':
                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.panel.Panel(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: 'cxscripticon',
                    flex: 1,
                    autoScroll: false,
                    border: true,
                    items:
                    [
                        {
                            xtype: 'computronixexttextarea',
                            flex: 1,
                            readOnly: true,
                            fieldCls: 'x-form-field cxnoborder',
                            value: this.scripts.get(
                                record.get('objectId')).replace(/},{/g, '},\n{').replace(/,/g, ',\n').replace(/\[{/g, '[\n{').replace(/;/g, ';\n').replace(/></g, '>\n<')
                        }
                    ]
                }));
                break;
            case 'container':
            case 'component':
                properties = this.getProperties(properties, Ext.getCmp(record.get('id')));

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.grid.property.Grid(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: 'cxcomponenticon',
                    nameColumnWidth: 250,
                    flex: 1,
                    listeners: { 'beforeedit': function () { return false; } },
                    source: properties,
                }));
                break;
            case 'request':
                var request = {};

                Ext.apply(request, this.requests.get(record.get('id')));
                delete request.response;
                properties = this.getProperties(properties, request.options);

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.grid.property.Grid(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: request.options.params && request.options.params.ajaxBatchId ? 'cxbatchicon' : 'cxrequesticon',
                    nameColumnWidth: 250,
                    flex: 1,
                    listeners: { 'beforeedit': function () { return false; } },
                    source: properties,
                }));
                this.panel.add(new Computronix.Ext.panel.Panel(
                {
                    title: 'Response',
                    flex: 1,
                    autoScroll: false,
                    border: true,
                    items:
                    [
                        {
                            xtype: 'computronixexttextarea',
                            flex: 1,
                            readOnly: true,
                            fieldCls: 'x-form-field cxnoborder',
                            value: this.requests.get(
                                record.get('id')).response.replace(/},{/g, '},\n{').replace(/,/g, ',\n').replace(/\[{/g, '[\n{').replace(/;/g, ';\n').replace(/></g, '>\n<')
                        }
                    ]
                }));
                break;
            case 'cachedRequest':
                responseText = Computronix.Ext.Ajax.getRequestCacheItem(record.get('id')).response.responseText || '';

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.panel.Panel(
                {
                    title: record.get('text'),
                    iconCls: 'cxcachedrequesticon',
                    flex: 1,
                    autoScroll: false,
                    border: true,
                    items:
                    [
                        {
                            xtype: 'computronixexttextarea',
                            flex: 1,
                            readOnly: true,
                            fieldCls: 'x-form-field cxnoborder',
                            value: responseText.length > 0 ? responseText.replace(/},{/g, '},\n{').replace(/,/g, ',\n').replace(/\[{/g, '[\n{').replace(/;/g, ';\n').replace(/></g, '>\n<') : null
                        }
                    ]
                }));
                break;
            case 'abortedRequest':
                properties = this.getProperties(properties, this.abortedRequests.get(record.get('objectId')).options);

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.grid.property.Grid(
                {
                    flex: 1,
                    iconCls: 'cxabortedrequesticon',
                    listeners:
                    {
                        'beforeedit': function ()
                        {
                            return false;
                        },
                    },
                    nameColumnWidth: 250,
                    source: properties,
                    title: Ext.String.capitalize(record.get('text')),
                }));
                break;
            case 'error':
                properties = this.getProperties(properties, this.errors.get(record.get('objectId')).options);

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.grid.property.Grid(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: 'cxerroricon',
                    nameColumnWidth: 250,
                    flex: 1,
                    listeners: { 'beforeedit': function () { return false; } },
                    source: properties,
                }));
                this.panel.add(new Computronix.Ext.panel.Panel(
                {
                    title: 'Call Stack',
                    flex: 1,
                    autoScroll: false,
                    border: true,
                    items:
                    [
                        {
                            xtype: 'computronixexttextarea',
                            flex: 1,
                            readOnly: true,
                            fieldCls: 'x-form-field cxnoborder',
                            value: this.errors.get(record.get('objectId')).callStack,
                        }
                    ]
                }));
                break;
            case 'scriptError':
                properties = this.getProperties(properties, Computronix.Ext.scriptErrors.get(record.get('objectId')));

                this.panel.removeAll();
                this.panel.add(new Computronix.Ext.grid.property.Grid(
                {
                    title: Ext.String.capitalize(record.get('text')),
                    iconCls: 'cxerroricon',
                    nameColumnWidth: 250,
                    flex: 1,
                    listeners: { 'beforeedit': function () { return false; } },
                    source: properties,
                }));
                break;
            case 'console':
                this.refreshConsole(record);
                break;
            case 'messageLog':
                Ext.Ajax.request(
                {
                    url: 'SysHandler.ashx',
                    params:
                    {
                        action: 'getMessageLog'
                    },
                    success: this.onSystemMonitorGetMessageLog,
                    scope: this
                });
                break;
            default:
                this.panel.removeAll();
                break;
        }
    },

    // Methods
    //=============================================================================
    //
    clear: function ()
    {
        this.requests.clear();
        this.requestTimes.clear();
        this.abortedRequests.clear();
        this.errors.clear();
        Computronix.Ext.scriptErrors.clear();
        Computronix.Ext.logMessages.clear();
        this.panel.removeAll();
        this.requestId = 0;
        this.refresh();
    },

    getProperties: function (properties, object, prefix)
    {
        for (var member in object)
        {
            if (member != 'requires' && member != 'config' && member != 'superclass' && member != 'ownerCt')
            {
                if (typeof object[member] == 'object' && (member == 'type' || member == 'margins' || member == 'initialConfig' ||
                    member == 'proxy' || member == 'params' || member == 'extraParams' || member == 'lastParams'))
                {
                    if (prefix)
                    {
                        properties = this.getProperties(properties, object[member], Ext.String.format('{0}.{1}', prefix, member));
                    }
                    else
                    {
                        properties = this.getProperties(properties, object[member], member);
                    }
                }
                else if (typeof object[member] != 'function')
                {
                    if (member != 'items' && member != 'dockedItems' && object[member] &&
                        typeof object[member] == 'object' &&
                        object[member].$className == 'Ext.util.AbstractMixedCollection')
                    {
                        object[member].eachKey(function (key)
                        {
                            if (prefix)
                            {
                                properties = this.getProperties(properties, object[member].getByKey(key), Ext.String.format('{0}.{1}.{2}', prefix, member, key));
                            }
                            else
                            {
                                properties = this.getProperties(properties, object[member].getByKey(key), Ext.String.format('{0}.{1}', member, key));
                            }
                        }, this);
                    }
                    else
                    {
                        if (member.substr(0, 1) == '_')
                        {
                            if (prefix)
                            {
                                properties[Ext.String.format('{0}.{1}', prefix, member)] = '[Hidden]';
                            }
                            else
                            {
                                properties[member] = '[Hidden]';
                            }
                        }
                        else
                        {
                            if (object[member] && typeof object[member] == 'object')
                            {
                                if (prefix)
                                {
                                    properties[Ext.String.format('{0}.{1}', prefix, member)] =
                                        (object[member].$className || getMembers(object[member], false, true, null, 300)) + '';
                                }
                                else
                                {
                                    properties[member] = (object[member].$className || getMembers(object[member], false, true, null, 300)) + '';
                                }
                            }
                            else
                            {
                                if (prefix)
                                {
                                    properties[Ext.String.format('{0}.{1}', prefix, member)] = object[member] + '';
                                }
                                else
                                {
                                    properties[member] = object[member] + '';
                                }
                            }
                        }
                    }
                }
            }
        }

        return properties;
    },

    getText: function (request)
    {
        var result = null;

        if (request.options.scripts)
        {
            result = Ext.String.format('{0} {1}s at {2} ({3})',
                    request.url,
                    (request.duration || 0) / 1000,
                    Ext.Date.format(request.time, 'i:s.u'),
                    request.options.params.scriptId);
        }
        else
        {
            if (request.options.params && request.options.params.action)
            {
                if (request.options.params.widgetKey)
                {
                    result = Ext.String.format('{0} {1}s at {2} ({3} {4})',
                            request.url,
                            (request.duration || 0) / 1000,
                            Ext.Date.format(request.time, 'i:s.u'),
                            request.options.params.action,
                            request.options.params.widgetKey);
                }
                else
                {
                    if (request.options.params.paneId)
                    {
                        result = Ext.String.format('{0} {1}s at {2} ([{3}] {4})',
                                request.url,
                                (request.duration || 0) / 1000,
                                Ext.Date.format(request.time, 'i:s.u'),
                                request.options.params.paneId,
                                request.options.params.action);
                    }
                    else if (request.options.params.dataId)
                    {
                        result = Ext.String.format('{0} {1}s at {2} ({3} {4})',
                                request.url,
                                (request.duration || 0) / 1000,
                                Ext.Date.format(request.time, 'i:s.u'),
                                request.options.params.action,
                                request.options.params.dataId);
                    }
                    else
                    {
                        result = Ext.String.format('{0} {1}s at {2} ({3})',
                                request.url,
                                (request.duration || 0) / 1000,
                                Ext.Date.format(request.time, 'i:s.u'),
                                request.options.params.action);
                    }
                }
            }
            else
            {
                if (request.options.params && request.options.params.paneId)
                {
                    result = Ext.String.format('{0} {1}s at {2} [{3}]',
                            request.url,
                            (request.duration || 0) / 1000,
                            Ext.Date.format(request.time, 'i:s.u'),
                            request.options.params.paneId);
                }
                else
                {
                    result = Ext.String.format('{0} {1}s at {2}',
                            request.url,
                            (request.duration || 0) / 1000,
                            Ext.Date.format(request.time, 'i:s.u'));
                }
            }
        }

        return result;
    },

    refresh: function ()
    {
        var cachedCount;
        var childText;
        var count;
        var duration = 0;
        var root = this.tree.getRootNode();
        var selectedNode = this.tree.getSelected().first();
        var size = 0;

        if (selectedNode)
        {
            this.tree.setLastPath(selectedNode);
        }

        count = 0;
        Ext.data.StoreManager.each(function (store)
        {
            if (store.isComputronixExtStore)
            {
                count++;
            }
        }, this);

        root.removeAll();
        root.appendChild(
        {
            iconCls: 'cxdatastoreicon',
            id: 'stores',
            text: Ext.String.format('Data Stores [{0}]', count),
            type: 'stores',
        });
        root.appendChild(
        {
            iconCls: 'cxscripticon',
            id: 'scripts',
            text: Ext.String.format('Scripts [{0}]', this.scripts.getCount()),
            type: 'scripts',
        });
        root.appendChild(
        {
            iconCls: 'cxcomponenticon',
            id: 'component',
            text: Ext.String.format('Components [{0}]', Ext.ComponentManager.getCount()),
            type: 'components',
        });

        count = 0;
        cachedCount = 0;
        this.requests.each(function (requestId, request)
        {
            if (!request.ajaxBatchId)
            {
                if (request.cached)
                {
                    cachedCount++;
                }

                count++

                if (request.duration)
                {
                    duration += request.duration;
                }
            }
        }, this);

        childText = Ext.String.format('Requests [{0} Cached {1}] {2}s',
                count, cachedCount, duration / 1000);
        root.appendChild(
        {
            iconCls: 'cxrequesticon',
            id: 'requests',
            text: childText,
            type: 'requests',
        });

        count = 0;

        if (Computronix.Ext.Ajax.enableRequestCache)
        {
            switch (Computronix.Ext.Ajax.requestCacheStorage)
            {
                case 'InProcess':
                    count = Computronix.Ext.Ajax.getRequestCache().getCount();

                    Computronix.Ext.Ajax.getRequestCache().each(function (requestKey, request)
                    {
                        size += request.response.responseText.length;
                    }, this);
                    break;
                case 'LocalStorage':
                    // Need to reparse keys because items may have been added by other page instances.
                    Computronix.Ext.Ajax.getRequestCache()._keys = null;
                    count = Computronix.Ext.Ajax.getRequestCache().getKeys().length;

                    Ext.each(Computronix.Ext.Ajax.getRequestCache().getKeys(), function (requestCacheKey)
                    {
                        size += Computronix.Ext.Ajax.getRequestCacheItem(requestCacheKey).response.responseText.length;
                    }, this);
                    break;
            }

            if (size < 524288)
            {
                size = (Math.round((size / 1024) * 100) / 100) + ' KB';
            }
            else
            {
                size = (Math.round((size / 1048576) * 100) / 100) + ' MB';
            }
        }
        else
        {
            size = '0 KB';
        }

        childText = Ext.String.format('Aborted Requests [{0}]',
                this.abortedRequests.getCount());
        root.appendChild(
        {
            iconCls: 'cxabortedrequesticon',
            id: 'abortedRequests',
            text: childText,
            type: 'abortedRequests',
        });

        childText = Ext.String.format('Cached Requests ({0}) [{1}] {2}',
                Computronix.Ext.Ajax.requestCacheStorage, count, size);
        root.appendChild(
        {
            iconCls: 'cxcachedrequesticon',
            id: 'cachedRequests',
            text: childText,
            type: 'cachedRequests',
        });

        childText = Ext.String.format('Ajax Errors [{0}]', this.errors.getCount());
        root.appendChild(
        {
            iconCls: 'cxerroricon',
            id: 'errors',
            text: childText,
            type: 'errors',
        });

        childText = Ext.String.format('Script Errors [{0}]',
                Computronix.Ext.scriptErrors.getCount())
        root.appendChild(
        {
            iconCls: 'cxerroricon',
            id: 'scriptErrors',
            text: childText,
            type: 'scriptErrors',
        });

        childText = Ext.String.format('Console [{0}]',
                Computronix.Ext.logMessages.getCount());
        root.appendChild(
        {
            iconCls: 'cxlogicon',
            id: 'console',
            leaf: true,
            text: childText,
            type: 'console',
        });

        root.appendChild(
        {
            iconCls: 'cxlogicon',
            id: 'messageLog',
            leaf: true,
            text: 'Message Log',
            type: 'messageLog',
        });

        this.tree.restorePath();
    },

    refreshConsole: function (record)
    {
        var messages = '';
        var textArea;

        Computronix.Ext.logMessages.each(function (key, value)
        {
            messages += Ext.String.format('[{0}] {1}\n', Ext.Date.format(new Date(value.timestamp), 'Y/n/j H:i:s.u'), value.message);
        }, this);

        textArea = new Computronix.Ext.panel.Panel(
        {
            title: Ext.String.capitalize(record.get('text')),
            iconCls: 'cxlogicon',
            flex: 1,
            autoScroll: false,
            border: true,
            items:
            [
                {
                    xtype: 'computronixexttextarea',
                    flex: 1,
                    readOnly: true,
                    fieldCls: 'x-form-field cxnoborder',
                    value: messages
                }
            ]
        })

        this.panel.removeAll();
        this.panel.add(textArea);
    },

    refreshMessageLog: function (record)
    {
        var textArea = new Computronix.Ext.panel.Panel(
        {
            title: Ext.String.capitalize(record.get('text')),
            iconCls: 'cxlogicon',
            flex: 1,
            autoScroll: false,
            border: true,
            items:
            [
                {
                    xtype: 'computronixexttextarea',
                    flex: 1,
                    readOnly: true,
                    fieldCls: 'x-form-field cxnoborder',
                    value: this.messageLog
                }
            ]
        })

        this.panel.removeAll();
        this.panel.add(textArea);
    }
});
