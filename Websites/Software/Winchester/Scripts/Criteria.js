/// External javascript references required for Intellisense. -------------------------------------
/// <reference path='../Ext/ext-all-debug.js' />
/// <reference path='../Scripts/NameSpaces.js' />
/// <reference path='../Scripts/Computronix.Ext.js' />

// Criteria extension objects.
//=============================================================================
//

// Computronix boolean criteria extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.BooleanCriteria',
{
    extend: 'Computronix.Ext.form.field.ComboBox',
    alias: 'widget.computronixextbooleancriteria',
    store: 'booleanCriteriaLookup',
    labelClsExtra: 'cxfieldlabel'
});

// Computronix checkbox criteria extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.CheckboxCriteria',
{
    extend: 'Computronix.Ext.form.FieldContainer',
    alias: 'widget.computronixextcheckboxcriteria',
    layout: 'hbox',

    // Data Members
    //=============================================================================
    //
    nullLabel: 'All',
    tabIndex: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        Ext.apply(this,
        {
            items:
            [
                {
                    xtype: 'computronixextradio',
                    name: this.name,
                    readOnly: this.readOnly,
                    tabIndex: this.tabIndex,
                    boxLabel: 'True',
                    inputValue: true,
                    margin: '0 5 0 0'
                },
                {
                    xtype: 'computronixextradio',
                    name: this.name,
                    readOnly: this.readOnly,
                    tabIndex: this.tabIndex,
                    boxLabel: 'False',
                    inputValue: false,
                    margin: '0 5 0 0'
                },
                {
                    xtype: 'computronixextradio',
                    name: this.name,
                    readOnly: this.readOnly,
                    boxLabel: this.nullLabel,
                    inputValue: null,
                    margin: '0 5 0 0'
                }
            ]
        });

        this.height += 3;

        this.callParent();
    }
});

// Computronix date criteria extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.DateCriteria',
{
    extend: 'Computronix.Ext.form.FieldContainer',
    alias: 'widget.computronixextdatecriteria',
    layout: 'hbox',
    width: 350,

    // Data Members
    //=============================================================================
    //
    fromField: null,
    toField: null,
    compareText: 'From date of {0} must not be greater than to date of {1}',
    emptyText: null,
    format: Computronix.Ext.dateFormat,
    tabIndex: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.fromField = new Computronix.Ext.form.field.Date(
        {
            readOnly: this.readOnly,
            tabIndex: this.tabIndex,
            emptyText: this.emptyText,
            format: this.format,
            flex: 1,
            listeners:
            {
                'change': { fn: this.onDateCriteriaFieldChange, scope: this },
                'expand': { fn: this.onDateCriteriaFieldExpand, scope: this }
            }
        });

        this.toField = new Computronix.Ext.form.field.Date(
        {
            name: Ext.String.format('{0}#To', this.name),
            readOnly: this.readOnly,
            tabIndex: this.tabIndex,
            emptyText: this.emptyText,
            format: this.format,
            flex: 1,
            listeners:
            {
                'change': { fn: this.onDateCriteriaFieldChange, scope: this },
                'expand': { fn: this.onDateCriteriaFieldExpand, scope: this }
            }
        });

        Ext.apply(this,
        {
            items:
            [
                this.fromField,
                {
                    xtype: 'computronixextlabel',
                    text: 'To:',
                    margin: '3 3 0 3'
                },
                this.toField
            ]
        });

        this.height += 3;

        this.callParent();
    },

    // Event Handlers
    //=============================================================================
    //
    onDateCriteriaFieldChange: function (field, newValue, oldValue)
    {
        this.errorMessages = null;
        this.warningMessages = null;

        this.isValid();
    },

    onDateCriteriaFieldExpand: function (field)
    {
        var value;

        if (field == this.fromField)
        {
            value = this.toField.getValue();

            if (Ext.isEmpty(this.fromField.getValue()) && !Ext.isEmpty(value))
            {
                this.fromField.setValue(Ext.Date.add(value, Ext.Date.DAY, -1));
            }
        }

        if (field == this.toField)
        {
            value = this.fromField.getValue();

            if (Ext.isEmpty(this.toField.getValue()) && !Ext.isEmpty(value))
            {
                this.toField.setValue(Ext.Date.add(value, Ext.Date.DAY, +1));
            }
        }
    },

    // Methods
    //=============================================================================
    //
    focus: function (selectText, delay)
    {
        if (this.fromField)
        {
            this.fromField.focus(selectText, delay);
        }
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);
        var fromValue = this.fromField.getValue();
        var toValue = this.toField.getValue();

        if (Ext.isEmpty(fromValue) && Ext.isEmpty(toValue) && !this.allowBlank)
        {
            result.push(this.blankText);
        }

        if (!Ext.isEmpty(fromValue) && !Ext.isEmpty(toValue) && fromValue > toValue)
        {
            result.push(Ext.String.format(this.compareText, Ext.Date.format(fromValue, this.fromField.format),
                Ext.Date.format(toValue, this.toField.format)));
        }

        return result;
    },

    getValue: function ()
    {
        return this.fromField.getValue();
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);
        this.fromField.setReadOnly(readOnly);
        this.toField.setReadOnly(readOnly);
    },

    setValue: function (value)
    {
        this.callParent(arguments);
        this.fromField.setValue(value);
    }
});

// Computronix number criteria extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.NumberCriteria',
{
    extend: 'Computronix.Ext.form.FieldContainer',
    alias: 'widget.computronixextnumbercriteria',
    layout: 'hbox',
    width: 350,

    // Data Members
    //=============================================================================
    //
    align: 'right',
    fromField: null,
    toField: null,
    compareText: 'From value of {0} must not be greater than to value of {1}',
    emptyText: null,
    minValue: Number.MIN_VALUE,
    maxValue: Number.MAX_VALUE,
    allowDecimals: true,
    decimalPrecision: 2,
    format: this.format,
    format: '0,000.00',
    hideTrigger: false,
    tabIndex: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.fromField = new Computronix.Ext.form.field.Number(
        {
            readOnly: this.readOnly,
            tabIndex: this.tabIndex,
            emptyText: this.emptyText,
            minValue: this.minValue,
            maxVaue: this.maxValue,
            allowDecimals: this.allowDecimals,
            decimalPrecision: this.decimalPrecision,
            format: this.format,
            align: this.align,
            hideTrigger: this.hideTrigger,
            flex: 1,
            listeners: { 'change': { fn: this.onNumberCriteriaFieldChange, scope: this} }
        });

        this.toField = new Computronix.Ext.form.field.Number(
        {
            name: Ext.String.format('{0}#To', this.name),
            readOnly: this.readOnly,
            tabIndex: this.tabIndex,
            emptyText: this.emptyText,
            minValue: this.minValue,
            maxVaue: this.maxValue,
            allowDecimals: this.allowDecimals,
            decimalPrecision: this.decimalPrecision,
            format: this.format,
            align: this.align,
            hideTrigger: this.hideTrigger,
            flex: 1,
            listeners: { 'change': { fn: this.onNumberCriteriaFieldChange, scope: this} }
        });

        Ext.apply(this,
        {
            items:
            [
                this.fromField,
                {
                    xtype: 'computronixextlabel',
                    text: 'To:',
                    margin: '3 3 0 3'
                },
                this.toField
            ]
        });

        this.height += 3;

        this.callParent();
    },

    // Event Handlers
    //=============================================================================
    //
    onNumberCriteriaFieldChange: function (field, newValue, oldValue)
    {
        this.errorMessages = null;
        this.warningMessages = null;

        this.isValid();
    },

    // Methods
    //=============================================================================
    //
    focus: function (selectText, delay)
    {
        if (this.fromField)
        {
            this.fromField.focus(selectText, delay);
        }
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);
        var fromValue = this.fromField.getValue();
        var toValue = this.toField.getValue();

        if (Ext.isEmpty(fromValue) && Ext.isEmpty(toValue) && !this.allowBlank)
        {
            result.push(this.blankText);
        }

        if (!Ext.isEmpty(fromValue) && !Ext.isEmpty(toValue) && fromValue > toValue)
        {
            result.push(Ext.String.format(this.compareText, fromValue, toValue));
        }

        return result;
    },

    getValue: function ()
    {
        return this.fromField.getValue();
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);
        this.fromField.setReadOnly(readOnly);
        this.toField.setReadOnly(readOnly);
    },

    setValue: function (value)
    {
        this.callParent(arguments);
        this.fromField.setValue(value);
    }
});


// Computronix text criteria extension.
// ############################################################################################
//
Ext.define('Computronix.Ext.form.TextCriteria',
{
    extend: 'Computronix.Ext.form.FieldContainer',
    alias: 'widget.computronixexttextcriteria',
    layout: 'hbox',
    width: 350,

    // Data Members
    //=============================================================================
    //
    fromField: null,
    toField: null,
    compareText: 'From value of {0} must not be greater than to value of {1}',
    tabIndex: null,

    // Constructor and initialization
    //=============================================================================
    //
    initComponent: function ()
    {
        this.fromField = new Computronix.Ext.form.field.Text(
        {
            readOnly: this.readOnly,
            tabIndex: this.tabIndex,
            flex: 1,
            listeners: { 'change': { fn: this.onTextCriteriaFieldChange, scope: this} }
        });

        this.toField = new Computronix.Ext.form.field.Text(
        {
            name: Ext.String.format('{0}#To', this.name),
            readOnly: this.readOnly,
            tabIndex: this.tabIndex,
            flex: 1,
            listeners: { 'change': { fn: this.onTextCriteriaFieldChange, scope: this} }
        });

        Ext.apply(this,
        {
            items:
            [
                this.fromField,
                {
                    xtype: 'computronixextlabel',
                    text: 'To:',
                    margin: '3 3 0 3'
                },
                this.toField
            ]
        });

        this.height += 3;

        this.callParent();
    },

    // Event Handlers
    //=============================================================================
    //
    onTextCriteriaFieldChange: function (field, newValue, oldValue)
    {
        this.errorMessages = null;
        this.warningMessages = null;

        this.isValid();
    },

    // Methods
    //=============================================================================
    //
    focus: function (selectText, delay)
    {
        if (this.fromField)
        {
            this.fromField.focus(selectText, delay);
        }
    },

    getErrors: function (value)
    {
        var result = this.callParent(arguments);
        var fromValue = this.fromField.getValue();
        var toValue = this.toField.getValue();

        if (Ext.isEmpty(fromValue) && Ext.isEmpty(toValue) && !this.allowBlank)
        {
            result.push(this.blankText);
        }

        if (!Ext.isEmpty(fromValue) && !Ext.isEmpty(toValue) && fromValue > toValue)
        {
            result.push(Ext.String.format(this.compareText, fromValue, toValue));
        }

        return result;
    },

    getValue: function ()
    {
        return this.fromField.getValue();
    },

    setReadOnly: function (readOnly)
    {
        this.callParent(arguments);
        this.fromField.setReadOnly(readOnly);
        this.toField.setReadOnly(readOnly);
    },

    setValue: function (value)
    {
        this.callParent(arguments);
        this.fromField.setValue(value);
    }
});
