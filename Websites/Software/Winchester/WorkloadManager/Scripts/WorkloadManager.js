Ext.define('WorkloadManager.model.Assignee',
{
    extend: 'Ext.data.Model',
    fields:
    [
        // Server set data:
        {
            name: 'PeriodProcessCount',
            type: 'int'
        },
        {
            name: 'UserName',
            type: 'string'
        },
        {
            name: 'UserId',
            type: 'int'
        },
        {
            name: 'TodayProcessCount',
            type: 'int'
        },
        {
            name: 'OverdueProcessCount',
            type: 'int'
        },
        {
            name: 'UnscheduledProcessCount',
            type: 'int'
        },

        // Locally set data:
        {
            name: 'Index',
            type: 'int'
        }
    ],

    idProperty: 'UserId',

    constructor: function ()
    {
        this.callParent(arguments);

        // Make "unassigned" row fixed as last row by giving it a
        // different index than other rows.
        if(this.data.UserId === -1)
        {
            this.set('Index', 1);
        }
        else
        {
            this.set('Index', 0);
        }
    }
});

Ext.define('WorkloadManager.model.AssigneeDay',
{
    extend: 'Ext.data.Model',
    fields:
    [
        // Server set data:
        {
            name: 'ScheduledProcessCount',
            type: 'int'
        },
        {
            name: 'IsInPeriod',
            type: 'boolean'
        },
        {
            name: 'CalendarDate',
            type: 'date',
            dateFormat: 'c'
        },
        {
            name: 'UserId',
            type: 'int'
        },

        // Locally set data:
        {
            name: 'CalendarDay',
            type: 'int'
        },
        {
            name: 'IsToday',
            type: 'boolean'
        },
        {
            name: 'IsWeekend',
            type: 'boolean'
        },
        {
            name: 'Month',
            type: 'string'
        }
    ],

    monthNames:
    [
        'Jan.',
        'Feb.',
        'Mar.',
        'Apr.',
        'May ',
        'June',
        'July',
        'Aug.',
        'Sept.',
        'Oct.',
        'Nov.',
        'Dec.'
    ],

    constructor: function ()
    {
        this.callParent(arguments);

        // Calculate data to put help build the calendar.
        var today = WorkloadManager.getApplication().renderDate;
        var date = this.data.CalendarDate;

        if (today.getDate() === date.getDate() &&
            today.getMonth() === date.getMonth() &&
            today.getYear() === date.getYear())
        {
            this.set('IsToday', true);
        }
        if (date.getDay() === 0 || date.getDay() === 6)
        {
            this.set('IsWeekend', true);
        }

        this.set('Month', this.monthNames[date.getMonth()]);
        this.set('CalendarDay', date.getDate());
    }
});

Ext.define('WorkloadManager.model.Period',
{
    extend: 'Ext.data.Model',
    fields:
    [
        { name: 'Title', type: 'string' },
        { name: 'Value', type: 'int' }
    ]
});

Ext.define('WorkloadManager.store.AssigneeDayList',
{
    extend: 'Ext.data.Store',
    model: 'WorkloadManager.model.AssigneeDay'
});

Ext.define('WorkloadManager.store.AssigneeList',
{
    extend: 'Ext.data.Store',
    model: 'WorkloadManager.model.Assignee',

    sorters:
    [
        {
            property: 'Index',
            direction: 'ASC'
        },
        {
            property: 'UserName',
            direction: 'ASC'
        }
    ]
});

Ext.define('WorkloadManager.store.PeriodList',
{
    extend: 'Ext.data.Store',
    model: 'WorkloadManager.model.Period',
    data:
    [
        {
            Title: '1 day',
            Value: 1
        },
        {
            Title: '2 days',
            Value: 2
        },
        {
            Title: '3 days',
            Value: 3
        },
        {
            Title: '4 days',
            Value: 4
        },
        {
            Title: '5 days',
            Value: 5
        },
        {
            Title: '6 days',
            Value: 6
        },
        {
            Title: '1 week',
            Value: 7
        },
        {
            Title: '2 weeks',
            Value: 14
        }
    ]
});

Ext.define('WorkloadManager.view.AssigneeGrid',
{
    extend: 'Ext.grid.Panel',
    alias: 'widget.assigneegrid',
    store: 'AssigneeList',

    selType: 'checkboxmodel',
    selModel:
    {
        mode: 'SINGLE'
    },

    enableColumnHide: false,
    enableColumnMove: false,
    sortableColumns: false,

    viewConfig:
    {
        stripeRows: false
    },

    summaryData: {},

    initComponent: function ()
    {
        var me = this;

        this.features =
        [
            {
                ftype: 'summary'
            }
        ];

        this.columns =
        [
            {
                align: 'left',
                dataIndex: 'UserName',
                header: 'Staff Name',
                resizable: true,
                width: 91,

                renderer: function (value)
                {
                    // Render value as a link.
                    return Ext.String.format('<a class="grid-data-item" href="javascript:void(0)"><span class="staff-link">{0}</span></a>', value);
                }
            },
            {
                align: 'center',
                dataIndex: 'TodayProcessCount',
                header: 'Today',
                resizable: false,
                width: 50,

                renderer: function (value)
                {
                    if (value !== 0)
                    {
                        // Render value as a link.
                        return Ext.String.format('<a class="grid-data-item" href="javascript:void(0)"><span class="today-link">{0}</span></a>', value);
                    }

                    return '0';
                },

                summaryType: function ()
                {
                    return me.summaryData.todayProcessCount;
                }
            },
            {
                align: 'center',
                dataIndex: 'OverdueProcessCount',
                header: 'Overdue',
                resizable: false,
                width: 55,

                renderer: function (value)
                {
                    if (value !== 0)
                    {
                        // Render value as a link.
                        return Ext.String.format('<a class="grid-data-item" href="javascript:void(0)"><span class="overdue-link">{0}</span></a>', value);
                    }

                    return '0';
                },

                summaryType: function ()
                {
                    return me.summaryData.overdueProcessCount;
                }
            },
            {
                align: 'center',
                dataIndex: 'PeriodProcessCount',
                header: 'Period',
                resizable: false,
                width: 45,

                renderer: function (value)
                {
                    if (value !== 0)
                    {
                        // Render value as a link.
                        return Ext.String.format('<a class="grid-data-item" href="javascript:void(0)"><span class="period-link">{0}</span></a>', value);
                    }

                    return '0';
                },

                summaryType: function ()
                {
                    return me.summaryData.periodProcessCount;
                }
            },
            {
                align: 'center',
                dataIndex: 'UnscheduledProcessCount',
                header: 'Unscheduled',
                resizable: false,
                width: 75,

                renderer: function (value)
                {
                    if (value !== 0)
                    {
                        // Render value as a link.
                        return Ext.String.format('<a class="grid-data-item" href="javascript:void(0)"><span class="unscheduled-link">{0}</span></a>', value);
                    }

                    return '0';
                },

                summaryType: function ()
                {
                    return me.summaryData.unscheduledProcessCount;
                }
            },
            {
                flex: 1,
                resizable: false,

                renderer: function (value)
                {
                    return '<a class="history-link" href="javascript:void(0)">History</a>';
                }
            }
        ];

        this.callParent(arguments);
    }
});

Ext.define('WorkloadManager.view.Calendar',
{
    extend: 'Ext.view.View',
    alias: 'widget.calendar',
    title: 'Calendar',
    store: 'AssigneeDayList',
    itemSelector: '.day-link',
    cls: 'calendar',

    tpl: new Ext.XTemplate(
        // Create week day names.
        '<div class="day day-header">Sun</div>',
        '<div class="day day-header">Mon</div>',
        '<div class="day day-header">Tue</div>',
        '<div class="day day-header">Wed</div>',
        '<div class="day day-header">Thu</div>',
        '<div class="day day-header">Fri</div>',
        '<div class="day day-header">Sat</div>',

        // Create week days.
        '<tpl for=".">',
            '<div class="day ',
            '<tpl if="IsInPeriod"> day-active </tpl>',
            '<tpl if="IsWeekend"> day-weekend </tpl>',
            '<tpl if="IsToday">day-today</tpl> ">',
                '<div class="day-date" ">',
                '<tpl if="CalendarDay == 1 || xindex == 1">',
                    '<span>{Month} </span>',
                '</tpl>',
                '{CalendarDay}</div>',
                '<a href="javascript:void(0)" class="day-link">',
                '<tpl if="ScheduledProcessCount &gt; 0">{ScheduledProcessCount}</tpl>',
               '</a>',
            '</div>',
        '</tpl>'
        ),
    emptyText: 'No images available'
});

Ext.define('WorkloadManager.view.PeriodSelect',
{
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.periodselect',
    store: 'PeriodList',

    autoSelect: true,
    displayField: 'Title',
    editable: false,
    fieldLabel: 'Period:',
    labelWidth: 40,
    maxWidth: 130,
    queryMode: 'local',
    valueField: 'Value'
});

Ext.define('WorkloadManager.view.Viewport',
{
    extend: 'Ext.container.Viewport',

    requires:
    [
        'Ext.layout.container.Fit'
    ],

    layout:
    {
        type: 'hbox',
        align: 'stretch'
    },

    items:
    [
        {
            border: 0,
            flex: 1,
            layout:
            {
                type: 'vbox',
                align: 'stretch'
            },
            items:
            [
                {
                    xtype: 'periodselect',
                    margin: '3 0 11 0',
                },
                {
                    xtype: 'assigneegrid',
                    height: 306
                }
            ]
        },
        {
            border: 0,
            flex: 1,
            margin: '0 0 0 10px',

            layout:
            {
                type: 'vbox',
                align: 'stretch'
            },
            items:
            [
                {
                    xtype: 'component',
                    cls: 'calendar-header',
                    height: 44,
                    padding: '5 0'
                },
                {
                    xtype: 'calendar',
                    flex: 1
                }
            ]
        }
    ]
});

Ext.define('WorkloadManager.controller.Main',
{
    extend: 'Ext.app.Controller',

    views:
    [
        'AssigneeGrid',
        'Calendar',
        'PeriodSelect'
    ],

    models:
    [
        'Assignee',
        'AssigneeDay',
        'Period'
    ],

    stores:
    [
        'AssigneeDayList',
        'AssigneeList',
        'PeriodList'
    ],

    refs:
    [
        {
            ref: 'assigneeGrid',
            selector: 'assigneegrid'
        },
        {
            ref: 'calendar',
            selector: 'calendar'
        },
        {
            ref: 'periodSelect',
            selector: 'periodselect'
        },
        {
            ref: 'viewport',
            selector: 'viewport'
        }
    ],

    isLoading: false,

    init: function ()
    {
        this.control(
        {
            assigneegrid:
            {
                selectionchange: this.onAssigneeGridSelectionChange,
                itemclick: this.onAssigneeGridItemClick
            },
            periodselect:
            {
                change: this.onPeriodSelectChange
            },
            calendar:
            {
                itemclick: this.onCalendarItemClick
            }
        });
    },

    onLaunch: function ()
    {
        PosseApi.addOnLoaded();
        PosseApi.configurationLoaded();
    },

    getDaysToView: function ()
    {
        return this.getPeriodSelect().getValue();
    },

    load: function (useLocalDetails)
    {
        this.getViewport().setLoading(true);
        this.isLoading = true;

        // Get object id from POSSE.
        var objectBuffer = PosseApi.getObjectBuffer();
        if (!objectBuffer || objectBuffer.length === 0)
        {
            throw new Error('No object id found in POSSE object buffer');
        }
        var objectId = objectBuffer[0].objectHandle;

        if (useLocalDetails)
        {
            // Get the selected user id to restore after loading the data.
            var selection = this.getAssigneeGrid().getSelectionModel().getSelection();
            var userIdToSelect;
            if (selection.length > 0)
            {
                userIdToSelect = selection[0].data.UserId;
            }

            this.loadData(userIdToSelect, objectId);
        }
        else
        {
            // Get the DaysToView and AssignToUserId details from the object,
            // and then use those.
            PosseApi.executeAjax(
            {
                url: window.location.pathname + 'AddOnHandler.ashx',
                params:
                {
                    action: 'getDetails',
                    workloadManagerObjectId: objectId,
                },
                scope: this,
                success: function (response, eOpts)
                {
                    var result = Ext.decode(response.responseText);
                    this.getPeriodSelect().setValue(result.DaysToView);

                    this.loadData(result.AssignToUserId, objectId);
                }
            });
        }
    },

    loadData: function (userIdToSelect, objectId)
    {
        PosseApi.executeAjax(
        {
            url: window.location.pathname + 'AddOnHandler.ashx',
            params:
            {
                action: 'getData',
                daysToView: this.getDaysToView(),
                workloadManagerObjectId: objectId
            },
            scope: this,
            success: function (response, eOpts)
            {
                var result = Ext.decode(response.responseText);

                this.getAssigneeGrid().summaryData =
                {
                    todayProcessCount: result.TodayProcessCount,
                    overdueProcessCount: result.OverdueProcessCount,
                    unscheduledProcessCount: result.UnscheduledProcessCount,
                    periodProcessCount: result.PeriodProcessCount
                };

                // Load data into stores.
                Ext.getStore('AssigneeList').loadData(result.AssigneeList);
                Ext.getStore('AssigneeDayList').loadData(result.AssigneeDayList);

                var assigneeToSelect = Ext.getStore('AssigneeList').getById(userIdToSelect);

                if (!assigneeToSelect)
                {
                    assigneeToSelect = Ext.getStore('AssigneeList').getById(-1);
                }

                this.getAssigneeGrid().getSelectionModel().select(assigneeToSelect);
            },
            callback: function ()
            {
                this.getViewport().setLoading(false);
                this.isLoading = false;
            }
        });
    },

    onAssigneeGridItemClick: function (view, record, element, index, e)
    {
        // Populate search.
        var cls = e.getTarget().className;
        var date = WorkloadManager.getApplication().renderDate;

        var to;
        var queryString;

        // Set Assigned to "No" for Unassigned row.
        if (record.getId() === -1)
        {
            queryString = '?Assigned=No';
        }
        else
        {
            queryString = '?AssignedTo=' + encodeURIComponent(record.data.UserName);
        }

        switch (cls)
        {
            case 'staff-link':
                queryString += '&PosseAutoExecute=Y';

                break;

            case 'today-link':
                if (record.data.TodayProcessCount === 0)
                {
                    return false;
                }

                queryString += '&PosseAutoExecute=Y';
                queryString += '&ScheduledStartDate=' + Ext.Date.format(date, 'Y-m-d');
                queryString += '&ScheduledStartDate=' + Ext.Date.format(date, 'Y-m-d');

                break;

            case 'overdue-link':
                if (record.data.OverdueProcessCount === 0)
                {
                    return false;
                }

                to = Ext.Date.subtract(date, 'd', 1);
                queryString += '&PosseAutoExecute=Y';
                queryString += '&ScheduledStartDate=';
                queryString += '&ScheduledStartDate=' + Ext.Date.format(to, 'Y-m-d');

                break;

            case 'period-link':
                if (record.data.PeriodProcessCount === 0)
                {
                    return false;
                }

                to = Ext.Date.add(date, 'd', this.getDaysToView() - 1);
                queryString += '&PosseAutoExecute=Y';
                queryString += '&ScheduledStartDate=' + Ext.Date.format(date, 'Y-m-d');
                queryString += '&ScheduledStartDate=' + Ext.Date.format(to, 'Y-m-d');

                break;

            case 'unscheduled-link':
                if (record.data.UnscheduledProcessCount === 0)
                {
                    return false;
                }

                queryString += '&PosseAutoExecute=Y';
                queryString += '&Scheduled=N';

                break;

            case 'history-link':
                queryString += '&PosseAutoExecute=N';
                queryString += '&ProcessStatus=Complete';
                PosseApi.openSearch('ProcessSearch', queryString);

                return true;

            default :
                // The user selected the row without clicking on the link.
                // Just return.
                return;
        }

        PosseApi.openSearch('PosseWorkloadManagerSearch', queryString);
    },

    onAssigneeGridSelectionChange: function (grid, row)
    {
        // Populate calendar with new user data.
        if (row.length > 0)
        {
            // Filter Store
            var id = row[0].data.UserId;
            var assigneeDayStore = Ext.getStore('AssigneeDayList');

            assigneeDayStore.clearFilter();
            assigneeDayStore.filter(
            [
                {
                    property: 'UserId',
                    value: id,
                    exactMatch: true
                }
            ]);

            this.setDataBuffer();
            this.setCalendarTitle();
        }
    },

    onCalendarItemClick: function (me, record, item, index, e, eOpts)
    {
        var date = record.data.CalendarDate;
        var queryString;
        // Get the name from selected user.
        var selectedRow = this.getAssigneeGrid().selModel.getSelection()[0];

        // Set Assigned to "No" for Unassigned row.
        if (selectedRow.getId() === -1)
        {
            queryString = '?Assigned=No';
        }
        else
        {
            queryString = '?AssignedTo=' + encodeURIComponent(selectedRow.data.UserName);
        }

        queryString += "&PosseAutoExecute=Y";

        // If calendar day is today, set the from value of ScheduledStartDate to
        // blank so that overdue processes are included.
        if (record.data.IsToday)
        {
            queryString += '&ScheduledStartDate=';
        }
        else
        {
            queryString += '&ScheduledStartDate=' + Ext.Date.format(date, 'Y-m-d');
        }
        queryString += '&ScheduledStartDate=' + Ext.Date.format(date, 'Y-m-d');
        PosseApi.openSearch('PosseWorkloadManagerSearch', queryString);
    },

    onPeriodSelectChange: function (scope, newVal, oldVal)
    {
        if (this.isLoading)
        {
            return;
        }

        if (this.getAssigneeGrid().selModel.getSelection())
        {
            this.setDataBuffer();
        }

        this.load(true);
    },

    setCalendarTitle: function ()
    {
        var header = Ext.select('.calendar-header');
        var name = this.getAssigneeGrid().selModel.getSelection()[0].data.UserName;
        header.setHTML('Calendar for ' + name);
    },

    setDataBuffer: function ()
    {
        if (this.isLoading)
        {
            return;
        }

        var data = Ext.encode(
        {
            userId: this.getAssigneeGrid().selModel.getSelection()[0].getId(),
            daysToView: this.getDaysToView()
        });
        PosseApi.setDataBuffer(data);
    }
});

Ext.application(
{
    name: 'WorkloadManager',
    extend: 'Ext.app.Application',

    appFolder: 'Scripts/WorkloadManager',
    autoCreateViewport: true,

    // Capture the current date at load time. That way we can use this for the
    // Today links in the Assignee grid.
    renderDate: new Date(),

    controllers:
    [
        'Main'
    ]
});

// Implement a basic POSSE add-on. Most of the methods don't apply to this
// add-on, but stubs still have to exist for them.
PosseAddOn =
{
    refresh: function () {},
    setReadOnly: function (readOnly)
    {
        PosseApi.setBusy(false);
    },
    showCriteria: function (value) {},
    setConfiguration: function (name) {},

    showData: function ()
    {
        // Load the add-on data.
        WorkloadManager.app.getController('Main').load();
    }
};
