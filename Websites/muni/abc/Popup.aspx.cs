using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class Popup : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			this.LoadPresentation();
			this.RenderErrorMessage(this.pnlPaneBand);

			if (this.HasPresentation)
			{
				this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
				this.RenderHeaderPane(this.pnlHeaderBand, this.pnlTopBand);
				this.RenderTabLabelBand(this.pnlTabLabelBand);
				this.CheckClient();
				this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
				this.RenderPopupFunctionBand(this.pnlBottomFunctionBand, "Close");
			}
		}
		#endregion
	}
}
