
using System;
using System.Web;

namespace Computronix.POSSE.Admin
{
    public class Admin : Computronix.Ext.HandlerBase
    {
        // A generic interface for invoking various the Outrider Admin web API:
        //
        //      .../Admin.ashx?cmd=XXX&option=value&...
        //
        // The absence (or incorrect type) of any option will result in an
        // exception.
        //
        // To add a new Admin.ashx action:
        //
        // 0) Add a new 'if (this["action"] == "SomeAdminCall") {' ... stanza,
        //    wherein you obtain any query options, and invoke the method.
        //
        // 1) Add the new .NET wrapper interface in:
        //     .../dev/HEAD/posse/VS/OutriderNet/OutriderNet/IOutriderNetAdmin.cs
        //
        // 2) Implement the .NET wrapper method, which calls the
        //    (code-generated) Python DLL wrapper, in:
        //
        //     .../dev/HEAD/posse/VS/OutriderNet/OutriderNet/OutriderNet.cs
        //
        //    For the .NET API, we must pass 3 additional parameters (see #4,
        //    below), to force the Python API to implement .NET API behaviour
        //    (default is still COM, for backward compatibility):
        //
        //        return this.Instance.SomeAdminCall(
        //                  ...
        //               // single, dotnet, timeout
        //                  false,  true,   null);
        //
        //
        // 3) Create the configuration for the (code-generated) .NET
        //    wrapper for the Python DLL method invocation, in:
        //
        //     .../dev/HEAD/posse/Outrider/DLL/Outrider.cfg
        //
        //    This will require you to rebuild the generated code in:
        //
        //     .../build/posse.Outrider.DLL/HEAD/win-x86_64/msvc2008-ora102-py27/Outrider.cs
        //    and perhaps also copy it to your personal dev folder:
        //
        //     .../dev/HEAD/posse/VS/OutriderNet/OutriderNet/Outrider.cs
        //
        // 4) Implement the actual Python API, in
        //
        //     .../dev//HEAD/posse/Outrider/DLL/Interface.py
        //
        //    Unfortunately, to retain compatibility with the former COM
        //    interface, we must ensure that the default set of parameters to
        //    any existing Admin API still returns tab+newline encoded data from
        //    a single Outrider Python instance, while the .NET version returns
        //    JSON encoded data from all Outrider Python instances implementing
        //    the system.  Hence, several additional parameters are passed, with
        //    default values that force operation in COM API mode:
        //
        //        single=True  -- collect data from one (or multiple) instances
        //        dotnet=False -- COM interface is still the default
        //        timeout=None -- An optional timeout for collecting data from
        //                        all Outrider instances, in .NET single=False mode
        //
        //    This method invokes the actual implementation in
        //    OutriderCore.Admin in single=True mode.  In single=False mode, it
        //    distributes the command to all known Outrider Python instances,
        //    collects all results, and returns the whole set results encoded as
        //    JSON in, in the following form:
        //
        //        {
        //            "server-id": value,
        //            "server-id": value,
        //            ...
        //        }
        //
        //    Remember to both A) add the Target SomeAdminCall method, and B)
        //    enable remote distribution of the method call via Map/Reduce by
        //    adding a "SomeAdminCall" line, in 'if command in [ ...' test in
        //    the MRSvr.unrecognized_command method.
        //
        // 5) Implement the actual OutriderCore.Admin method, in:
        //
        //        .../dev/HEAD/posse/Outrider/Core/OutriderCore.py
        //
        //    which supports the dotnet=False (COM API) and dotnet=True (.NET
        //    API) return values.  These often simply encode the data values in
        //    tab+newline form for COM using Common.Encode(), and simply return
        //    the raw Python data structures for .NET.
        //
        //    Often, these are thin wrappers around the actual Outrider Python
        //    Trace implementations in:
        //
        //        .../dev/HEAD/posse/Outrider/Core/Trace.py
        //
        public override void ProcessRequest()
        {
            // Create the OutriderNet interface WITHOUT invoking the Python
            // Outrider Initialize()/Dispose(), and without web Hit tracking.

            // The defaults are no cachability, and normally we return
            // Content-Type: application/json, but some (eg. Get...FileContents)
            // return other content types...


            // The default Response.ContentType is "application/json"; by
            // convention, if we receive a JSON encoded raw "string", this
            // indicates that the API detected an Exception, and passed the
            // Exception message to us.  All commands that naturally return just
            // a raw string as a result (GetVerion, Get{Log,Trace}FileContents)
            // set the ContentType to "test/plain;...".

            this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            this.Response.ContentType = "application/json";

        // This OutriderNet instance is created with initialize=false, to
        // avoid creating and initializing a Python Outrider instance; in
        // other words, the .NET<->Python API is set up, but an underlying
        // Outrider instance is NOT created.  Only the Outrider Admin and
        // logging API (which doesn't require a Python Outrider instance)
        // may be used.
            Computronix.POSSE.Outrider.OutriderNet outriderNet
                = new Computronix.POSSE.Outrider.OutriderNet(false);

        //foreach (string key in this.Request)
        //{
        //    Outrider._LogMessage(Outrider.DEBUG, "%s == %s",
        //                 key, self.Request[key]);
        //}

            try {
                string result = "";

                if (this["action"]  == "GetStatistics") {
                    result = outriderNet.GetStatistics(this["since"], this["timeout"]);
                } else if (this["action"]  == "GetVersion") {
                    result = outriderNet.GetVersion(
                                 this["timeout"]);
                    this.Response.ContentType = "text/plain; charset=utf-8";
                } else if (this["action"]  == "GetConfiguration") {
                    bool memory = false;
                    if ( ! String.IsNullOrEmpty(this["memory"])) {
                        try {
                            memory = Convert.ToBoolean(this["memory"]);
                        }
                        catch (FormatException) {
                            memory = Convert.ToBoolean(Convert.ToInt32(this["memory"]));
                        }
                    }
                    result = outriderNet.GetConfiguration(
                                 this["systemKey"], this["debugKey"], memory,
                                 this["timeout"]);
                } else if (this["action"]  == "CreateDebugKey") {
                    bool renderingDebug = false;
                    if ( ! String.IsNullOrEmpty(this["renderingDebug"])) {
                        try {
                            renderingDebug = Convert.ToBoolean(this["renderingDebug"]);
                        }
                        catch (Exception) {
                            renderingDebug = Convert.ToBoolean(Convert.ToInt32(this["renderingDebug"]));
                        }
                    }

                    bool renderLookupIFrame = false;
                    if ( ! String.IsNullOrEmpty(this["renderLookupIFrame"])) {
                        try {
                            renderLookupIFrame = Convert.ToBoolean(this["renderLookupIFrame"]);
                        }
                        catch (FormatException) {
                            renderLookupIFrame = Convert.ToBoolean(Convert.ToInt32(this["renderLookupIFrame"]));
                        }
                    }

                    int level = 0;
                    if ( ! String.IsNullOrEmpty(this["level"])) {
                        level = Convert.ToInt32(this["level"]);
                    }

                    bool dbDebug = false;
                    if ( ! String.IsNullOrEmpty(this["dbDebug"])) {
                        try {
                            dbDebug = Convert.ToBoolean(this["dbDebug"]);
                        }
                        catch (FormatException) {
                            dbDebug = Convert.ToBoolean(Convert.ToInt32(this["dbDebug"]));
                        }
                    }

                    bool traceLines = false;
                    if ( ! String.IsNullOrEmpty(this["traceLines"])) {
                        try {
                            traceLines = Convert.ToBoolean(this["traceLines"]);
                        }
                        catch (FormatException) {
                            traceLines = Convert.ToBoolean(Convert.ToInt32(this["traceLines"]));
                        }
                    }

                    int maxVariableLength = 1000;
                    if ( ! String.IsNullOrEmpty(this["maxVariableLength"])) {
                        maxVariableLength = Convert.ToInt32(this["maxVariableLength"]);
                    }
                    result = outriderNet.CreateDebugKey(
                        this["systemKey"], this["debugKey"], renderingDebug,
                        renderLookupIFrame, level, dbDebug,
                        this["domains"], traceLines, maxVariableLength,
                        this["timeout"]);
                } else if (this["action"]  == "GetDebugKeys") {
                    result = outriderNet.GetDebugKeys(
                                 this["systemKey"],
                                 this["timeout"]);
                } else if (this["action"]  == "DeactivateDebugKey") {
                    result = outriderNet.DeactivateDebugKey(
                                 this["systemKey"], this["debugKey"],
                                 this["timeout"]);
                } else if (this["action"]  == "DeleteDebugKey") {
                    // Normally returns a list of strings (the deleted file
                    // names); could return a single Exception string, though
                    // (not likely)
                    result = outriderNet.DeleteDebugKey(
                                 this["systemKey"], this["debugKey"],
                                 this["timeout"]);
                } else if (this["action"]  == "GetLogFiles") {
                    result = outriderNet.GetLogFiles(this["timeout"]);
                } else if (this["action"]  == "GetLogFileContents") {
                    // Nonexistent files will result in a null being returned,
                    // which will (appropriately) raise an exception
                    result = outriderNet.GetLogFileContents(
                                 this["systemKey"], this["fileName"], this["timeout"]);
                    this.Response.ContentType = "text/plain; charset=utf-8";
                } else if (this["action"]  == "GetTraceFileContents") {
                    // Nonexistent files will result in a null being returned
                    result = outriderNet.GetTraceFileContents(
                                 this["systemKey"], this["fileName"], this["timeout"]);
                    this.Response.ContentType = "text/plain; charset=utf-8";
                } else if (this["action"]  == "DeleteTraceFile") {
                    // Deleting nonexistent trace files is not an exception
                    result = outriderNet.DeleteTraceFile(
                                 this["systemKey"], this["fileName"],
                                 this["timeout"]);
                } else if (this["action"]  == "ReloadConfiguration") {
                    bool minimalOnly = false;
                    if ( ! String.IsNullOrEmpty(this["minimalOnly"])) {
                        try {
                            minimalOnly = Convert.ToBoolean(this["minimalOnly"]);
                        }
                        catch (FormatException) {
                            minimalOnly = Convert.ToBoolean(Convert.ToInt32(this["minimalOnly"]));
                        }
                    }
                    result = outriderNet.ReloadConfiguration(
                                 this["systemKey"], minimalOnly, this["debugKey"],
                                 this["timeout"]);
                    // Normal JSON result is "null" (Python None) on success,
                    // and a string (the text of the exception) on failure.
                } else if (this["action"]  == "SetLoggingLevel") {

                    int level = 30; // Default to WARNING
                    if ( ! String.IsNullOrEmpty(this["level"])) {
                        level = Convert.ToInt32(this["level"]);
                    }

                    result = outriderNet.SetLoggingLevel(
                                 level,
                                 this["timeout"]);
                } else if (this["action"]  == "GetLoggingLevel") {
                    result = outriderNet.GetLoggingLevel(
                                 this["timeout"]);
                } else {
                    throw new Exception(String.Format("Invalid option: action={0}",
                                                      this["action"]));
                }

                if (this.Response.ContentType == "application/json"
                    && ! String.IsNullOrEmpty(result)
                    && result != "null" && result.Length > 0 && result[0] == '"') {
                    // For all commands return JSON, a normal result is JSON
                    // 'null', or other arbitrary JSON encoded data; A single
                    // non-empty JSON encoded string indicates an Exception.
                    string exc = this.JavaScriptSerializer.Deserialize<string>(result);
                    if ( ! String.IsNullOrEmpty(exc)) {
                        throw new Exception(HttpUtility.HtmlEncode(exc));
                    }
                }

                this.Response.AddHeader("Content-Length", result.Length.ToString());
                this.Response.Write( result );
            }
            finally {
                outriderNet.Dispose();
                outriderNet = null;
            }
        }
    }
}
