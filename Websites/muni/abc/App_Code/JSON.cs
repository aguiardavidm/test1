﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Xml;

namespace cxJSON
{

    public class JSON
    {
        public readonly static JSON Instance = new JSON();

        private JSON()
        {
        }
        public bool UseOptimizedDatasetSchema = true;
        public bool UseFastGuid = true;
        public bool UseSerializerExtension = false;
        public bool IndentOutput = false;
        public bool SerializeNullValues = true;

        public object Parse(string json)
        {
            return new JsonParser(json).Decode();
        }
    }
}