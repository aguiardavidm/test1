﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Computronix.Ext
{
    /// <summary>
    /// Base class for all service handlers.
    /// </summary>
    public abstract class HandlerBase : IHttpHandler
    {
        #region Data Members
        protected string this[string key]
        {
            get
            {
                return this.Request[key];
            }
        }

        protected HttpContext Context
        {
            get;
            private set;
        }

        protected HttpRequest Request
        {
            get;
            private set;
        }

        protected HttpResponse Response
        {
            get;
            private set;
        }

        private JavaScriptSerializer javaScriptSerializer = null;
        protected virtual JavaScriptSerializer JavaScriptSerializer
        {
            get
            {
                if (this.javaScriptSerializer == null)
                {
                    this.javaScriptSerializer = new JavaScriptSerializer();
                }

                return this.javaScriptSerializer;
            }
        }
        #endregion

        #region Constructors
        public HandlerBase()
        {
        }
        #endregion

        #region Methods
        public void ProcessRequest(HttpContext context)
        {
            string responseText = null;

            try
            {
                this.Context = context;
                this.Request = context.Request;
                this.Response = context.Response;

                this.Response.Clear();
                this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                this.Response.Cache.SetExpires(DateTime.Today.AddDays(-1));
                this.Response.ContentType = "application/json";

                this.ProcessRequest();
            }
            // If the error was a web request then use the response from the web exception.
            catch (WebException exception)
            {
                this.Response.Clear();
                this.Response.StatusCode = 500;

                if (exception.Response == null)
                {
                    this.Log("{0}\r\n{1}", exception.Message, HandlerBase.Write(exception));
                    this.Response.Write(string.Format("{{StatusCode: {0}, Message: '{1}'}}", this.Response.StatusCode,
                        HandlerBase.EscapeJson(exception.Message)));
                }
                else
                {
                    responseText = HandlerBase.GetResponseText(exception.Response);

                    this.Log("{0}\r\n{1}\r\n{2}", exception.Message, responseText,
                        HandlerBase.Write(exception));
                    this.Response.Write(responseText);
                }

                this.Response.End();
            }
            catch (Exception exception)
            {
                this.Log("{0}\r\n{1}", exception.Message, HandlerBase.Write(exception));
                this.Response.Clear();
                this.Response.StatusCode = 500;
                this.Response.Write(string.Format("{{StatusCode: {0}, Message: '{1}'}}", this.Response.StatusCode,
                    HandlerBase.EscapeJson(exception.Message)));
                this.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public abstract void ProcessRequest();

        /// <summary>
        /// Logs a message to the debug output window.
        /// </summary>
        /// <param name="message">Message text or message format.</param>
        /// <param name="args">Argument values for message format.</param>
        public virtual void Log(object message, params object[] args)
        {
            if (args.Length > 0)
            {
                System.Diagnostics.Debug.WriteLine(string.Format(message.ToString(), args));
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(message);
            }
        }

        /// <summary>
        /// Returns the response text as a string from the specified response.
        /// </summary>
        /// <param name="response">Reference to a web response.</param>
        /// <returns>The response text as a string.</returns>
        public static string GetResponseText(WebResponse response)
        {
            Stream responseStream = response.GetResponseStream();
            string result = null;

            if (responseStream.CanRead)
            {
                result = new StreamReader(responseStream).ReadToEnd();
            }

            responseStream.Close();

            return result;
        }

        /// <summary>
        /// Executes the specified uri and returns the response text as a string.
        /// </summary>
        /// <param name="uri">The uri to execute.</param>
        /// <returns>The response text as a string.</returns>
        public static string GetResponseText(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            return HandlerBase.GetResponseText(request.GetResponse());
        }

        /// <summary>
        /// Executes the specified uri and returns the response as a text file.
        /// </summary>
        /// <param name="uri">The uri to execute.</param>
        /// <returns>The response as a text file.</returns>
        public virtual string GetResponseTextFile(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            WebResponse response = request.GetResponse();

            this.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.txt", this["fileName"]));
            this.Response.AddHeader("Content-Length", response.ContentLength.ToString());
            this.Response.ContentType = response.ContentType;

            return HandlerBase.GetResponseText(response);

        }

        /// <summary>
        /// Writes the specified exception as a string.
        /// </summary>
        /// <param name="exception">A reference to the exception being written.</param>
        /// <returns>The exception as a string.</returns>
        public static string Write(Exception exception)
        {
            string label = "Exception", format = "{0}={1}\r\n";
            StringBuilder result = new StringBuilder();
            ArrayList properties = new ArrayList();
            object value = null;

            while (exception != null)
            {
                properties.Clear();
                properties.AddRange(exception.GetType().GetProperties(
                    BindingFlags.Public |
                    BindingFlags.Instance |
                    BindingFlags.FlattenHierarchy));

                result.AppendFormat(format, label, exception.GetType().FullName);

                if (exception.Data != null)
                {
                    foreach (object key in exception.Data.Keys)
                    {
                        result.AppendFormat(format, "Data[" + key.ToString() + "]", exception.Data[key]);
                    }
                }

                foreach (PropertyInfo property in properties)
                {
                    if (property.Name != "InnerException" && property.Name != "Data")
                    {
                        value = HandlerBase.GetPropertyValue(exception, property.Name);

                        if (value != null)
                        {
                            result.AppendFormat(format, property.Name, value);
                        }
                    }
                }

                exception = exception.InnerException;
                label = "Inner Exception";
            }

            return result.ToString();
        }

        /// <summary>
        /// Returns the current value of the specified property of an object.
        /// </summary>
        /// <param name="target">The object from which to get the property value.</param>
        /// <param name="name">The name of the property value to return.</param>
        /// <returns>The current value of the property.</returns>
        protected static object GetPropertyValue(object target, string name)
        {
            return HandlerBase.InvokeMember(target, name, BindingFlags.GetProperty, new object[] { });
        }

        /// <summary>
        /// Invokes the given member of an object, passing arguments.
        /// </summary>
        /// <param name="target">The object on which to invoke the member.</param>
        /// <param name="name">The name of the member to invoke.</param>
        /// <param name="binding">Indicates the type of binding to perform.</param>
        /// <param name="args">Arguments to be passed to the member.</param>
        /// <returns>The return value of the invoked member.</returns>
        protected static object InvokeMember(object target, string name, BindingFlags binding, object[] args)
        {
            object result;

            try
            {
                if (target is Type)
                {
                    result = ((Type)target).InvokeMember(name,
                        binding | BindingFlags.FlattenHierarchy | BindingFlags.Static | BindingFlags.Public,
                        null, null, args);
                }
                else
                {
                    result = target.GetType().InvokeMember(name, binding, null, target, args);
                }
            }
            catch (MissingMemberException exception)
            {
                throw new InvokeMemberException(target, name, args, exception);
            }

            return result;
        }

        /// <summary>
        /// Determines whether or not the given string can be considered true.
        /// </summary>
        /// <param name="value">The value that is to be interpreted as either true or false.</param>
        /// <returns>True if the lower case of <paramref name="value" /> is considered "true", otherwise false.</returns>
        public static bool IsTrue(string value)
        {
            bool result = false;
            double numericValue;

            if (!string.IsNullOrEmpty(value))
            {
                value = value.ToLower();
                result = ((value == "yes") || (value == "y") || (value == "true") ||
                    (value == "t") || (value == "on") || (value == "ok") || (value == "1"));

                if (!result)
                {
                    if (double.TryParse(value, out numericValue))
                    {
                        result = (numericValue > 0);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Escapes the specified json string.
        /// </summary>
        /// <param name="json">The json string to escape.</param>
        /// <returns>The escaped json string.</returns>
        public static string EscapeJson(string json)
        {
            return json.Replace("'", "\\'");
        }
        #endregion
    }
}