using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class DefaultLite : Computronix.POSSE.Outrider.PageBaseExt
  {
    #region Event Handlers
    /// <summary>
    /// Page load event handler.
    /// </summary>
    /// <param name="sender">A reference to the page.</param>
    /// <param name="e">Event arguments.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      this.HomePageUrl = this.RootUrl + "DefaultLite.aspx";
      this.LoadPresentation();
      this.RenderErrorMessage(this.pnlPaneBand);

      if (this.HasPresentation)
      {
		if (this.Request.QueryString["PosseShowTitle"] == "Yes")
		{
        	this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
		}
        this.RenderHeaderPane(this.pnlHeaderBand, this.pnlTopBand);
        this.RenderTabLabelBand(this.pnlTabLabelBand);
        this.CheckClient();
        this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
		this.RenderTopFunctionBand(this.pnlTopFunctionBand);
        this.RenderBottomFunctionBand(this.pnlBottomFunctionBand);

        // For reports we need to fix the height of the pane so the embedded PDF 
        // fills the page.  Add bottom padding for Firefox.  This assumes that a report
        // pane is named "ReportPane...".
        if (this.PaneName.StartsWith("ReportPane"))
        {
          this.pnlPaneBand.Style["height"] = "600px";
          this.pnlPaneBand.Style["padding-bottom"] = "5px";
        }
      }
      else
      {
        if (!string.IsNullOrEmpty(this.MenuName))
        {
          this.RenderMenuPane(this.pnlPaneBand, this.pnlTitleBand, this.pnlTopBand);
        }
        else
        {
          this.RenderGreeting(this.pnlPaneBand);
        }
      }
    }
    #endregion
  }
}