using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace Computronix.POSSE.Outrider
{
    public partial class _Default : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Ensure the outer ASPX form is never submitted.  Works around browser behaviour that submits single field forms when [Enter] is pressed.
            this.Form.Attributes["onsubmit"] = "return false";

            this.LoadPresentation();
            //this.RenderUserInfo(this.pnlUserInfo);
            this.RenderMenuBand(this.pnlMenuBand);
            this.RenderErrorMessage(this.pnlPaneBand);

            //Start logging to investigate the Menu band randomly disappering.
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            this.pnlMenuBand.RenderControl(hw);

            if (sb.ToString().Length < 200)
            {
                this.OutriderNet.LogMessage(LogLevel.Error, "****************************************************************");
                this.OutriderNet.LogMessage(LogLevel.Error, "Recording Information for Error in Menu Band");
                this.OutriderNet.LogMessage(LogLevel.Error, "URL: " + HttpContext.Current.Request.Url.ToString());
                this.OutriderNet.LogMessage(LogLevel.Error, "Navigation Menu Html Rendering:");
                this.OutriderNet.LogMessage(LogLevel.Error, sb.ToString() + "MenuHtml");
                this.pnlUserInfo.RenderControl(hw);
                this.OutriderNet.LogMessage(LogLevel.Error, "User Information Html Rendering:");
                this.OutriderNet.LogMessage(LogLevel.Error, sb.ToString());
                this.OutriderNet.LogMessage(LogLevel.Error, "Session Id: " + this.SessionId);
                HttpBrowserCapabilities bc = Request.Browser;
                this.OutriderNet.LogMessage(LogLevel.Error, "Browser Info: " + bc.Type + " " + bc.Version);
            }

            //End Logging for the menu disappering.

            // Check if the user needs to update their profile information
            string updateProfile, possePresentation;
            updateProfile = this.UserInformation[0]["CheckMyProfileInformation"].ToString();
            possePresentation = this.Request.QueryString["PossePresentation"];
            if (!this.HasPresentation & !this.IsGuest & updateProfile == "False")
            {
                //throw new Exception(String.Format("\n\n{0}", this.PresentationTitle));
                this.RedirectUrl = String.Format("Default.aspx?PossePresentation=MunicipalUserProfile&PosseObjectId={0}", Server.HtmlEncode((string)this.UserInformation[0]["ObjectId"].ToString()));
            }

            if (this.HasPresentation)
            {
                string redirectUrl;
				string queryArgs = "";
                redirectUrl = this.RedirectUrl;

                // Render a message if the user just changed their password
                // If multiple Success Messages are required we could have a SuccessCode query string parameter instead
                //  and move the logic to decode which message to use to PageBaseExt
                if (this.Request.QueryString["ShowPWSuccess"] == "Y")
                {
                    this.RenderSuccessMessage(this.pnlPaneBand, "Password changed successfully.");
                }
                // Check if the user needs to change their password
                if (this.UserInformation[0]["PasswordChangeRequired"].ToString() == "True"
				    & Request.QueryString["PossePresentation"] != "MunicipalChangePassword")
                {
                    this.RedirectUrl = this.Request.RawUrl;
                    this.SetCookie("PasswordRedirect", "Y");
                    this.ReleaseOutriderNet();
                    Response.Redirect(String.Format("Default.aspx?PossePresentation=MunicipalChangePassword&PosseObjectId={0}",
						Server.HtmlEncode((string)this.UserInformation[0]["ObjectId"].ToString())));
                }
                if (Request.QueryString["PossePresentation"] == "MunicipalChangePasswordSuccess" & this.GetCookie("PasswordRedirect") == "Y")
                {
                    // If the user just reset their password, redirect to where they were trying to go and show a success message
                    this.RemoveCookie("PasswordRedirect");
					// If profile needs to be updated, and we are not on the profile page, go there
                    if (updateProfile == "True" & possePresentation != "PublicUserProfile")
					{
						//this.RedirectUrl = null;
						this.SetCookie("MyProfileRedirect", "Y");
                        this.ReleaseOutriderNet();
						Response.Redirect(String.Format("Default.aspx?PossePresentation=MunicipalUserProfile&PossePane=Details&PosseObjectId={0}{1}",
						    Server.HtmlEncode((string)this.UserInformation[0]["ObjectId"].ToString()),"&ShowPWSuccess=Y"));
					}
                    this.RedirectUrl = null;
                    this.ReleaseOutriderNet();
                    Response.Redirect(string.Format("{0}&ShowPWSuccess=Y",redirectUrl));
                }

                // Verify that the user is logged in as a non-guest user.
                if (!this.IsGuest)
                {
                    // When the user is missing profile information, but the error is null (e.g., upon
                    // logging in), we must generate that error message by attempting to submit
                    if (updateProfile == "True" & string.IsNullOrEmpty(this.ErrorMessage))
                    {
                        try
                        {
                            this.OutriderNet.ProcessFunction(null, this.PaneId, this.PaneId,
                                    RoundTripFunction.Submit, this.GetCacheState(), null);
                        }
                        catch (Exception exception)
                        {
                            this.ProcessError(exception);
                        }
                        this.RenderErrorMessage(this.pnlPaneBand);
                    }
                    // prevent the user from navigating to other pages while mandatory Profile Info
                    // is missing.
                    if (updateProfile == "True" & possePresentation != "MunicipalUserProfile"
					    & possePresentation != "MunicipalChangePassword")
                    {
                        if (this.Request.QueryString["ShowPWSuccess"] == "Y")
                    {
							queryArgs += "&ShowPWSuccess=Y";
						}
                        this.RedirectUrl = this.Request.RawUrl;
                        this.ReleaseOutriderNet();
                        Response.Redirect(String.Format("Default.aspx?PossePresentation=MunicipalUserProfile&PosseObjectId={0}",
							Server.HtmlEncode((string)this.UserInformation[0]["ObjectId"].ToString())));
                    }
                    if (this.RedirectUrl.Contains("MunicipalUserProfile") & updateProfile == "False")
                    {
                        this.RedirectUrl = null;
						this.RemoveCookie("MyProfileRedirect");
                    }
                    else if (possePresentation == "MunicipalUserProfile" & updateProfile == "False")
                    {
                        if (redirectUrl != null & this.GetCookie("MyProfileRedirect") == "Y")
                        {
                            this.RedirectUrl = null;
							this.RemoveCookie("MyProfileRedirect");
                            this.ReleaseOutriderNet();
                            Response.Redirect(redirectUrl);
                        }
                    }
                }

                this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
                this.RenderHelpBand(this.pnlHelpBand);
                this.RenderHeaderPane(this.pnlHeaderBand, this.pnlTopBand);
                if (this.EmbedCriteriaOnSearches)
                {
                    this.RenderSearchCriteriaPane(this.pnlSearchCriteriaBand);
                }
                else
                {
                    this.pnlSearchCriteriaBand.Visible = false;
                    this.RenderTabLabelBand(this.pnlTabLabelBand);
                }

                if (this.PresentationName != "ToDoList") //Don't render the pane if this is a TO DO List
                {
                    this.RenderTopFunctionBand(this.pnlTopFunctionBand);
                    this.CheckClient();
                    this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
                }
                this.RenderBottomFunctionBand(this.pnlBottomFunctionBand);

                // For reports we need to fix the height of the pane so the embedded PDF
                // fills the page.  Add bottom padding for Firefox.  This assumes that a report
                // pane is named "ReportPane...".
                if (this.PaneName.StartsWith("ReportPane"))
                {
                    this.pnlPaneBand.Style["height"] = "600px";
                    this.pnlPaneBand.Style["padding-bottom"] = "5px";
                }
            }
            else
            {
                // Redirect Guest to login
                if (this.IsGuest)
                {
                    //redirect to login
                    this.ShowLogin("");
                }

                if (!string.IsNullOrEmpty(this.MenuName))
                {
                    this.RenderMenuPane(this.pnlPaneBand, this.pnlTitleBand, this.pnlTopBand);
                }
                else
                {
                    this.ReleaseOutriderNet();
                    Response.Redirect(String.Format("Default.aspx?PossePresentation=Default&PosseObjectId={0}", Server.HtmlEncode((string)this.UserInformation[0]["ObjectId"].ToString())));
                }
            }

            if (this.ShowDebugSwitch)
            {
                this.RenderDebugLink(this.pnlDebugLinkBand);
            }
            this.RenderFooterBand(this.pnlFooterBand);
        }
        #endregion

    }
}
