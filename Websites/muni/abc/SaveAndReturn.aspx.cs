using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class SaveAndReturn : Computronix.POSSE.Outrider.PageBaseExt
    {
        #region Event Handlers
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">A reference to the page.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string pageUrl = "SaveAndReturn.aspx";
            this.HomePageUrl = this.RootUrl + pageUrl;
            string cancelUrl = "";

            string breadcrumb = this.GetCookie("Breadcrumb");

            Boolean isSaveReturn = false, isTopLevel = false;

            string returnTo = this.Request.QueryString["ReturnTo"];
            if (returnTo == "home")
            {
                isTopLevel = true;
            }

            if (this.ComesFrom == "posse" && this.RoundTripFunction == RoundTripFunction.Submit && string.IsNullOrEmpty(this.ErrorMessage))
            {
                string saveAndReturnButton = Request.QueryString["SaveReturnButton"];
                isSaveReturn = !string.IsNullOrEmpty(saveAndReturnButton) && saveAndReturnButton == "Y";
            }
            
            this.LoadPresentation(true);

            // if our 'context' object has just been saved for the first time,
            // update the cookie with the actual object Id, then
            // do any required redirecting.
            string contextObjectId = this.GetPaneData(true)[0]["objecthandle"].ToString();

            List<NewObject> newObjects = this.GetNewObjects();
            foreach(NewObject newobj in newObjects)
            {
                string newObjectId = newobj.ObjectId.ToString();
                if (newObjectId == contextObjectId)
                {
                    string objectHandle = newobj.ObjectHandle;

                    if (!string.IsNullOrEmpty(newObjectId))
                    {
                        this.updateCookie(ref breadcrumb, newObjectId);
                    }
                }
            }

	        isTopLevel = this.syncCookie(ref breadcrumb, isTopLevel);
            if (string.IsNullOrEmpty(this.ErrorMessage))
            {
                isTopLevel = this.syncCookie(ref breadcrumb, isTopLevel);
                if (isSaveReturn)
                {
                    if (isTopLevel)
                    {
                        this.Response.Clear();
                        this.Response.Write("<script>opener.PosseNavigate(null); window.close();</script>");
                        this.Response.End();
                    }
                    else
                    {
                    	this.ReleaseOutriderNet();
                        this.Response.Redirect(this.HomePageUrl + "?" + getParentUrlFromCookie(breadcrumb));
                    }
                }
            }

			if (isTopLevel)
            {
                cancelUrl = "javascript:opener.PosseNavigate(null); window.close();";
            }
            else
            {
				string parentURL = getParentUrlFromCookie(breadcrumb);

				if (parentURL.IndexOf("N0", 14) > 0)
				{
					cancelUrl = "javascript:window.close();";
				}
				else
				{
					cancelUrl = string.Format("javascript:window.location='{0}?{1}';", this.HomePageUrl, parentURL);
				}
            }

            this.CheckRedirect();

            this.RenderErrorMessage(this.pnlPaneBand);
            this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
            this.renderBreadcrumb(this.pnlBreadcrumbBand, breadcrumb);
            this.RenderTabLabelBand(this.pnlTabLabelBand);
            this.CheckClient();
            this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
            this.RenderSaveAndReturnFunctionBand(this.pnlBottomFunctionBand, cancelUrl);
        }

        protected string getParentUrlFromCookie(string breadcrumb)
        {
            char[] bang = { '|' };
            char[] dash = { '-' };
            int parentItem = 0;
            StringBuilder outUrl = new StringBuilder();

            string[] items = breadcrumb.Split(bang);

            // if there is a parent item, use it.  Otherwise, use the current item, as the window will
            // be closed before the redirect anyway.
            if (items.Length > 1)
            {
                parentItem = 1;
            }
            string[] data = items[parentItem].Split(dash);
            outUrl.AppendFormat("PosseObjectId={0}&PossePresentation={1}", data[0], data[1]);
            return outUrl.ToString();
        }

        protected void renderBreadcrumb(WebControl container, string breadcrumb)
        {
            StringBuilder outBreadcrumb = new StringBuilder();
            HyperLink link;
            Literal spacer;
            Image chevron;
            Label label;

            char[] bang = { '|' };
            char[] dash = { '-' };

            string[] items = breadcrumb.Split(bang);
            string[] data;

            int loop = items.Length - 1;
            while (loop > 0)
            {
                data = items[loop].Split(dash);
                link = new HyperLink();
                link.CssClass = "breadcrumb";
                link.NavigateUrl = String.Format("{0}?PosseObjectId={1}&PossePresentation={2}", this.HomePageUrl, data[0], data[1]);
                link.Text = data[2];
                link.ToolTip = String.Format("Go to {0}", data[2]);
                container.Controls.Add(link);
                spacer = new Literal();
                spacer.Text = "&nbsp;";
                container.Controls.Add(spacer);

                chevron = new Image();
                chevron.ImageUrl = "images/black_chevron.gif";
                container.Controls.Add(chevron);
                spacer = new Literal();
                spacer.Text = "&nbsp;";
                container.Controls.Add(spacer);
                loop--;
            }

            data = items[0].Split(dash);
            label = new Label();
            label.CssClass = "breadcrumb";
            label.Text = data[2];
            container.Controls.Add(label);
        }

        protected Boolean syncCookie(ref string breadcrumb, Boolean isTopLevel)
        {
            string objectId;
            string PossePane = null;
            string PossePaneParameter = null;
            StringBuilder outCookie = new StringBuilder();
            char[] bang = { '|' };
            char[] dash = { '-' };
            Boolean isNewObject = false;

            if (string.IsNullOrEmpty(breadcrumb))
            {
                isTopLevel = true;
            }

            objectId = this.GetPaneData(true)[0]["objecthandle"].ToString();
            if (char.IsLetter(objectId, 0))
            {
                isNewObject = true;
            }

	    //get the current pane name so we navigate back to it if we came from a presentation with multiple panes
	    if (this.Panes != null)
	    {
            foreach(VisiblePaneInfo pane in this.Panes)
			{
                if (pane.PaneId == this.CurrentPaneId)
                    PossePane = pane.Name;
			}
	    }
	    
	    if (!string.IsNullOrEmpty(PossePane))
	    {
		    PossePaneParameter = "&PossePane=" + PossePane;
	    }

            if (isTopLevel)
            {
                // we're at the top level, so indescriminately set the cookie to contain only the CURRENT object
                outCookie.AppendFormat("{0}-{1}{2}-{3}", objectId, this.PresentationName, PossePaneParameter, this.PresentationTitle);
            }
            else
            {
                if (isNewObject)
                {
                    // the current object is NEW, but we only push it on once (not if we refresh the page)
                    string[] items = breadcrumb.Split(bang);
                    string[] data;
                    data = items[0].Split(dash);
		    
	    
                    //if we've already pushed it, don't push it again
                    if (data[0] == objectId) 
                    {                    
                            outCookie.AppendFormat("{0}", breadcrumb);
                    }
                    else
                    {
                    	    outCookie.AppendFormat("{0}-{1}{2}-{3}|{4}", objectId, this.PresentationName, PossePaneParameter, this.PresentationTitle, breadcrumb);
                    }
                }
                else
                {
                    // Search for the current object id in the cookie, and if not found, push it. Otherwise, throw away any
                    // preceeding elements from the cookie (the user likely used the 'back' button in their browser).
                    // Because we do the preceeding check anyway, we don't need a specific 'pop' method.  This will handle
                    // the pop after a Save and Return anyway.
                    outCookie.AppendFormat("{0}-{1}{2}-{3}", objectId, this.PresentationName, PossePaneParameter, this.PresentationTitle);
                    string[] items = breadcrumb.Split(bang);
                    string[] data;
                    int loop = 0;
                    int startPoint = 0;
                    while (loop < items.Length)
                    {
                        data = items[loop].Split(dash);
                        if (data[0] == objectId) 
                        {
                            startPoint = loop + 1;
                            break;
                        }
                        loop++;
                    }
                    if (startPoint == items.Length) isTopLevel = true;
                    for (int i = startPoint; i < items.Length; i++)
                    {
                        // this if SHOULD be irrelevant, but it will toss any cookie entries to 
                        // 'N' style object handles - they should have been replaced by now with
                        // a real objectId
                        if (char.IsNumber(items[i], 0))
                        {
                            outCookie.AppendFormat("|{0}", items[i]);
                        }
                    }
                }
            }
            breadcrumb = outCookie.ToString();
            this.SetCookie("Breadcrumb", breadcrumb);
            return isTopLevel;
        }

        protected void updateCookie(ref string breadcrumb, string objectId)
        {
            StringBuilder outCookie  = new StringBuilder();
            string[] items, data;
            char[] bang = { '|' };
            char[] dash = { '-' };

            items = breadcrumb.Split(bang);
            data = items[0].Split(dash);

            outCookie.AppendFormat("{0}-{1}-{2}", objectId, data[1], data[2]);
            for (int i = 1; i < items.Length; i++)
            {
                outCookie.AppendFormat("|{0}", items[i]);
            }
            breadcrumb = outCookie.ToString();
            this.SetCookie("Breadcrumb", breadcrumb);
        }

        #endregion
    }
}
