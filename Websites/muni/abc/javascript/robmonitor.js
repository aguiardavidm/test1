    /* These two functions are used to render a dropdown for creating
       new objects off of the search form
    */
    function OpenNewObjectPresentation(vBaseURL, vReference){
      var vPresArray;
      vPresArray = vReference.split(":");
      alert("Create a new \"" + vPresArray[0] + "\" using \"" + 
              vPresArray[1] + "\"");
      document.location = vBaseURL + "?PossePresentation=" + vPresArray[1] +
              "&PosseObjectDef=" + vPresArray[0];
    }
    function MakeNewObjectSelectOptions(vSelectString){
      var i, vSelectObject, vSelectObjects = vSelectString.split(";");
      var vSelectObjectText
      // Netscape 4.79 requires all options to be done the same way
      // so this must written using JavaScript, not directly in the HTML
      document.write('<option value="none">(none)');

      for(i=0; i<vSelectObjects.length; i++){        
        vSelectObject = vSelectObjects[i].split(":");
        // vSelectObject = [objectdef, presentation, display]
        if(vSelectObject.length==2){
          vSelectObject = 
                  Array(vSelectObject[0], vSelectObject[1], vSelectObject[0]);
        }
        document.write("<option value='"+vSelectObject[0]+":" +
                vSelectObject[1]+"'>"+vSelectObject[2]);
      }
    }


  function IsCheckedReturnYes(vCheckbox){
    if (vCheckbox.checked){
      return "yes";
    } else {
      return "no";
    }
  }

 /* This opens a POSSE popup using PosseGlobal.js code */
  function RobPossePopup(aAnchor,aWidth,aHeight,aURL,aFlipName) {
  var aTitle, aComponentId;
  var vRobDBDebuggerCookie = "RobDBDebuggerPipeName";
  var vCookieValue, vSeparator;
    aTitle="Rob's Popup";
    aComponentId=0;

  var vHRef = "";
  if(aURL == "") {
    vHRef = location.href;
    aLookupURL = vHRef.substring(0, vHRef.indexOf("?"));
  }
  vSeparator = "?";
  if (aURL.indexOf("?") > -1){vSeparator="&"}
  vHRef = aURL + vSeparator + "Title="+escape(aTitle);

  if(aComponentId == "1") {
    location.href = vHRef;
  } else {
    var lu = new PossePw();
    lu.xoffset = 0 - (aWidth / 3);
    lu.yoffset = -20;
    lu.width = aWidth;
    lu.height = aHeight;
    lu.href = vHRef;
    // The popup opner has the name hard coded, so we flip the name
    // open the popup, flip the popup name, reset the window
    // name back. If this isn't done a new popup isn't created.
    if(aFlipName){
        alert("RobMonitor.js: Window name switches about to be made " +
              "to avoid name collisions");
        var aWindowNameHolder = window.name;
        window.name = window.name + "dummy";
    }
    lu.openPopup(aAnchor);
    if (aFlipName){
        window.PossePwRef.name = "RobMonitor";
        window.name = aWindowNameHolder;
    }
    lu.reload;
  }
}

  // Returns the value of cookie vCookieName
  function FetchCookie(vCookieName){
    var vCookiePairs, vCookieSplit, i, vReturn;
    vReturn="None";
    vCookiePairs = document.cookie.split(";");

    for(i=0; i<vCookiePairs.length; i++){
      vCookieSplit = vCookiePairs[i].split("=");
      if (RobLTrim(vCookieSplit[0])==vCookieName){if(vCookieSplit.length>1){vReturn=vCookieSplit[1];}}
    }
    return vReturn;
  }

  function ShowCookies(vWindow, vText){
    vHandle = window.open("", "MickeyMouse");
    vHandle.document.write(vText + " cookie="+vWindow.document.cookie + "<br>");
  }

  // posseglobal.js has this function but it is broken in 5.4.6, 5.4.7
  // can remove this function once 5.4.7 is gone
  function RobLTrim(str) {
    var i;
    i = 0;
    while (str.substr(i,1) == " ") {
      i++;
    }
    if (i > 0) {
      str = str.substr(i);
    }
    return str;
  }


  // function to retrieve the page, flipping to a different Tab pointed to by the
  // RobRetrievePaneId cookie
  function FlipToTab(vPaneId){
    if (vPaneId){
      if (!(document.possedocumentchangeform.paneid.value == vPaneId)){
        document.possedocumentchangeform.paneid.value=vPaneId;
        document.possedocumentchangeform.submit();
      }
    }
   }

  // call the WritePopup on the popup window
  function WritePopup(vText){var vTextLocal; vTextLocal=vText; window.PossePwRef.WritePopup(vTextLocal);}