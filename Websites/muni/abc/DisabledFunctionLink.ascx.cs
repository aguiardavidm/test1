using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
	public partial class DisabledFunctionLink : Computronix.POSSE.Outrider.FunctionLinkBase
	{
		#region Event Handlers
		protected void Page_Load(object sender, EventArgs e)
		{
            this.lblBtnLabel.Text = this.Text;
		}
		#endregion
	}
}
