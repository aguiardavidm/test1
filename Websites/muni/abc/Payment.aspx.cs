using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using NJEPayWS;

public partial class Payment : Computronix.POSSE.Outrider.PageBaseExt
{
    protected void Page_Load(object sender, EventArgs e)
    {

		string objectId = Request.QueryString["PosseObjectId"];

        int[] staticIntArray = new int[1] {Convert.ToInt32(objectId)};
        string eComType = Request.QueryString["EComType"];
        string eComTransactionId = CreateEComTransaction(eComType, staticIntArray);

        decimal feeAmount = GetEComTransactionFee(eComType, eComTransactionId);
        string successPaymentUrl = String.Format("http://lpaposseapp01/NJDev/pub/abc/RecordPayment.aspx?PosseObjectId={0}&PossePresentation={1}&PossePane={2}&EComType={3}&SuccessFlag={4}&FeeId={5}", objectId, Request.QueryString["SuccessPresentation"], Request.QueryString["SuccessPane"], eComType, Request.QueryString["SuccessFlag"], Request.QueryString["FeeId"]);
        string failurePaymentUrl = String.Format("http://lpaposseapp01/NJDev/pub/abc/RecordPayment.aspx?PosseObjectId={0}&PossePresentation={1}&PossePane={2}&EComType={3}", objectId, Request.QueryString["FailurePresentation"], Request.QueryString["FailurePane"], eComType);
        string cancelPaymentUrl = Request.UrlReferrer.ToString(); //String.Format("http://lpaposseapp01/NJDev/pub/abc/Default.aspx?{0}", Request.QueryString.ToString());

        this.RenderUserInfo(this.pnlUserInfo);
        this.RenderMenuBand(this.pnlMenuBand);
        
        // Setup the payment and redirect to payment provider
        NJ_NICUSA_WSClient wsClient = new NJ_NICUSA_WSClient();

        njPaymentInfo payInfo = setupPaymentInfo();

        payInfo.AMOUNT = feeAmount.ToString();
        payInfo.LOCALREFID = objectId;
        payInfo.UNIQUETRANSID = eComTransactionId;

        payInfo.HREFSUCCESS = successPaymentUrl;
        payInfo.HREFFAILURE = failurePaymentUrl;
        payInfo.HREFCANCEL = cancelPaymentUrl;        
        njPreparePaymentResult preparePayResult = wsClient.preparePayment(payInfo);

        if (preparePayResult.APPLICATION_REDIRECT_WITH_TOKEN.Length > 0)
        {
            Response.Redirect(preparePayResult.APPLICATION_REDIRECT_WITH_TOKEN);
        }
        else
        {
            // Render the page with an error message
            this.ErrorMessage = "An error has occured in processing your payment. <br/><br/>Error details:<br/>" + preparePayResult.ERRORMESSAGE;
            this.ErrorMessage += String.Format("<br/><br/>AMOUNT: {0}, LOCALREFID: {1}, UNIQUETRANSID: {2}, HREFSUCCESS: {3}, HREFFAILURE: {4}, HREFCANCEL: {5}", 
                payInfo.AMOUNT, payInfo.LOCALREFID, payInfo.UNIQUETRANSID, payInfo.HREFSUCCESS, payInfo.HREFFAILURE, payInfo.HREFCANCEL);
            this.ErrorMessage += String.Format(" STATECD: {0}, MERCHANTID: {1}, MERCHANTKEY: {2}, SERVICECODE: {3}, njApplicationName: {4}", 
                payInfo.STATECD, payInfo.MERCHANTID, payInfo.MERCHANTKEY, payInfo.SERVICECODE, payInfo.njApplicationName);
            this.RenderTitle(this.pnlTitleBand, "Error");
            this.RenderErrorMessage(this.pnlPaneBand);

            //Response.Write("<br/><br/>" + preparePayResult.ERRORMESSAGE);
        }

        if (this.ShowDebugSwitch)
        {
            this.RenderDebugLink(this.pnlDebugLinkBand);
        }
        this.RenderFooterBand(this.pnlFooterBand);
    }

    protected njPaymentInfo setupPaymentInfo()
    {
        njPaymentInfo payInfo = new njPaymentInfo();
        payInfo.STATECD = "NJ";
        payInfo.MERCHANTID = "njlpsabc";
        payInfo.MERCHANTKEY = "bRawus3u";
        payInfo.SERVICECODE = "lpsAbcLicense"; // For License Fees...Use lpsABCFines for Fines, if applicable.
        payInfo.njApplicationName = "LPS_POSSE";

        return payInfo;
    }
}
