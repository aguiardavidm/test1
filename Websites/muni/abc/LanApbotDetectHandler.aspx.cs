using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Lanap.BotDetect;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Computronix.POSSE.Outrider
{
	public partial class LanApbotDetectHandler : Computronix.POSSE.Outrider.PageBaseExt
	{

		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			string command = this.Request["Command"];
			//object captcha;
			//Type type = Type.GetTypeFromProgID("Lanap.BotDetect");
            		List<OrderedDictionary> data;
			StringBuilder xml = new StringBuilder();

			if (command == "CreateImage" || command == "CreateSound")
			{
				this.Response.Buffer = true;
				this.Response.CacheControl = "no-cache";
				this.Response.AddHeader("Pragma", "no-cache");
				this.Response.Expires = -1;

				//captcha = Activator.CreateInstance(type);

				this.StartDialog(string.Format("PosseObjectId={0}&PossePresentation=SessionId", this.ObjectId));

				data = this.GetPaneData(true);

				if (data.Count > 0 && this.SessionId.ToString() != (string)data[0]["SessionId"])
				{
					throw new COMException("Captcha: InvalidSession, An invalid session number was used", -10);
				}

				switch (command)
				{
					case "CreateImage":
						this.Response.ContentType = "image/gif";

						string code = CaptchaCore.GenerateRandomCode(CodeTypeEnum.Alpha, 5);
                        CaptchaCore.GenerateImage(code, TextStyleEnum.Flash,
                            new System.Drawing.Size(238, 50), ImageFormatEnum.Gif,
                            System.Drawing.Color.Beige).WriteTo(this.Response.OutputStream);

                        Response.Write(String.Format("Got Here before the error... xml = {0} <p>", xml.ToString()));
                        Response.End();
						//this.Response.BinaryWrite((byte[])type.InvokeMember("CreateImage", BindingFlags.InvokeMethod, null, captcha, new object[] { }));
						xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", this.ObjectId);
						xml.AppendFormat("<column name=\"CaptchaCode\">{0}</column>", code);
						//xml.AppendFormat("<column name=\"CaptchaHash\">{0}</column>", type.InvokeMember("GetHashValue", BindingFlags.InvokeMethod, null, captcha, new object[] { }));
						xml.Append("</object>");

						this.ProcessXML(xml.ToString());

						break;
					case "CreateSound":
						this.Response.ContentType = "audio/x-wav";
						this.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
						CaptchaCore.GenerateSound((string)data[0]["CaptchaCode"],
                            SoundLanguageEnum.EN, SoundFormatEnum.Wav)
                            .WriteTo(this.Response.OutputStream);
						break;
				}

				this.Response.End();
			}
		}
		#endregion
	}
}
