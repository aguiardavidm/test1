using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using NJEPayWS;

public partial class Payment : Computronix.POSSE.Outrider.PageBaseExt
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string xml;
        int[] staticIntArray = new int[1] {Convert.ToInt32(this.ObjectId)};
        string eComType = Request.QueryString["EComType"];

        string eComTransactionId = CreateEComTransaction(eComType, staticIntArray);

        // Record the payment result
        NJ_NICUSA_WSClient wsClient = new NJ_NICUSA_WSClient();

        njGetPaymentInfoRequest infoRequest = new njGetPaymentInfoRequest();
        infoRequest.token = Request.QueryString["token"];
        wsClient.getPaymentInfo(infoRequest);

        njGetPaymentInfoResponse infoResponse = new njGetPaymentInfoResponse();
        if (infoResponse.FAILCODE == null)
        {
            // Successful Transaction...
            xml = string.Format("<object id=\"{0}\" action=\"Update\">" +
                                "<column name=\"SuccessFlagColumnName\"><![CDATA[{1}]]></column>" +
                                "<column name=\"SourceObjectId\"><![CDATA[{2}]]></column>" +
                                "<column name=\"FeeId\"><![CDATA[{3}]]></column></object>",
                                eComTransactionId, Request.QueryString["SuccessFlag"], this.ObjectId, Request.QueryString["FeeId"]);
        
            this.ProcessXML(xml.ToString());

            string receiptNumber = infoResponse.AUTHCODE == null? string.Empty: infoResponse.AUTHCODE;
            RecordEComTransactionAcceptance(eComType, eComTransactionId, receiptNumber);

            Response.Redirect(String.Format("Default.aspx?{0}", Request.QueryString.ToString()));
        }
        else
        {
            // No Denial process has been configured
            //RecordEComTransactionDenial(eComType, eComTransactionId);
            this.ErrorMessage = infoResponse.FAILMESSAGE;

            Response.Redirect(String.Format("Default.aspx?{0}", Request.QueryString.ToString()));            
        }
    }
}
