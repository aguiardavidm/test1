using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
	public partial class FlushCache : System.Web.UI.Page
	{
		#region Event Handlers
		/// <summary>
		/// Page prerender event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Prerender(object sender, EventArgs e)
		{
			if (this.Cache.Count == 0)
			{
				this.Response.Write("There are no items in the cache.");
			}
			else
			{
				this.Response.Write(string.Format("The following {0} items are in the cache:<br>", this.Cache.Count));

				foreach (DictionaryEntry item in this.Cache)
				{
					this.Response.Write("<br>" + item.Key.ToString());
				}
			}
		}

		/// <summary>
		/// Flush button click event handler.
		/// </summary>
		/// <param name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void btnFlush_Click(object sender, EventArgs e)
		{
			foreach (DictionaryEntry item in this.Cache)
			{
				this.Cache.Remove((string)item.Key);
			}
		}
		#endregion
	}
}
