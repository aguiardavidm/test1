using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Computronix.POSSE.Outrider
{
    public partial class EmbeddedWizard : Computronix.POSSE.Outrider.PageBaseExt
  {
    #region Event Handlers
    /// <summary>
    /// Page load event handler.
    /// </summary>
    /// <param name="sender">A reference to the page.</param>
    /// <param name="e">Event arguments.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      StringBuilder script = new StringBuilder();

      string pageUrl = "EmbeddedWizard.aspx";
      bool isNew = false;

      this.ValidateUrlParameter("FromObjectId", this.FromObjectId);
      this.ValidateUrlParameter("EndPoint", this.EndPoint);

      if (!string.IsNullOrEmpty(this.FromObjectId) && !string.IsNullOrEmpty(this.EndPoint))
      {
        pageUrl += "?FromObjectId=" + this.FromObjectId + "&EndPoint=" + this.EndPoint;
        isNew = true;
      }

      string savePressed = Request.QueryString["SavePressed"];

      this.HomePageUrl = this.RootUrl + pageUrl;
      this.LoadPresentation(true);
      if (this.SaveIsNext && this.RoundTripFunction == RoundTripFunction.Submit && (savePressed != "Y"))
      {
        this.CheckRedirect();
      }
      this.RenderErrorMessage(this.pnlPaneBand);

      if (this.HasPresentation && this.RoundTripFunction == RoundTripFunction.Submit &&
        string.IsNullOrEmpty(this.ErrorMessage) && !this.SaveIsNext && (savePressed == "Y"))
      {
        if (isNew)
        {
          script.AppendFormat("opener.PosseAppendChangesXML(unescape('<object id=\"{0}\" action=\"Update\"><relationship id=\"NEW1\" action=\"Insert\" endpoint=\"{1}\" toobjectid=\"{2}\"/></object>'));",
            this.FromObjectId, this.EndPoint, this.GetPaneData(true)[0]["objecthandle"]);
        }
           script.Append("opener.PosseSubmit(null); window.close();");
           this.ClientScript.RegisterStartupScript(this.GetType(), "Submit", script.ToString(), true);

      }

      if (this.HasPresentation)
      {
        this.RenderTitle(this.pnlTitleBand, this.PresentationTitle);
        this.RenderHeaderPane(this.pnlHeaderBand, this.pnlTopBand);
        this.RenderTabLabelBand(this.pnlTabLabelBand);
        this.CheckClient();
        this.RenderPane(this.pnlPaneBand, this.pnlBottomFunctionBand);
        this.RenderWizardPopFunctionBand(this.pnlBottomFunctionBand);
      }

    }
    #endregion

    protected void RenderWizardPopFunctionBand(WebControl container)
        {
          HtmlTableRow row;
          FunctionLinkBase defaultFunction = null;

          if (container != null)
          {
            row = new HtmlTableRow();

            if (this.PresentationType != PresType.Tabs &&
              this.PresentationType != PresType.List)
            {
              this.RenderFunctionLink(row, "Back", RoundTripFunction.Previous);
            }
            if (this.PresentationType != PresType.Tabs &&
              this.PresentationType != PresType.List)
            {
              this.RenderFunctionLink(row, "Next", RoundTripFunction.Next);
            }

            if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Submit]))
            {
                this.RenderFunctionLink(row, "Save", RoundTripFunction.Submit);
                string delimiter = "?";
                if (this.HomePageUrl.IndexOf('?') >= 0)
                {
                    delimiter = "&";
                }
                string saveUrl = string.Format("javascript:PosseSubmitLinkReturn('{0}{1}SavePressed=Y', {2}, {3});", this.HomePageUrl, delimiter, 2, this.PaneId);
                this.RenderFunctionLink(row, "Save & Close", saveUrl);
            }

            if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.Search]))
            {
                this.RenderFunctionLink(row, "Search Again", RoundTripFunction.Search);
            }

            if (!string.IsNullOrEmpty(this.PaneFunctions[(int)RoundTripFunction.PerformSearch]))
            {
                defaultFunction = this.RenderFunctionLink(row, "Search", RoundTripFunction.PerformSearch);
            }

            if (!this.DisableCancel)
            {
              this.RenderFunctionLink(row, "Cancel", "javascript:window.close();");
            }

            if (defaultFunction != null)
            {
              this.Page.Form.Attributes["onkeypress"] = "javascript:return FireDefaultButton(event, '" +
                defaultFunction.ClientID + "')";
            }
          }
    }
  }
}