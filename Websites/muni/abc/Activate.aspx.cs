using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Computronix.POSSE.Outrider
{
	public partial class Activate : Computronix.POSSE.Outrider.PageBaseExt
	{
		#region Event Handlers
		/// <summary>
		/// Page load event handler.
		/// </summary>
		/// <param   name="sender">A reference to the page.</param>
		/// <param name="e">Event arguments.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            List<OrderedDictionary> data;
            StringBuilder xml = new StringBuilder();
            String LogonId = null, Pwd = null;
            int UserId = 0;

            this.LoadPresentation();
            data = this.GetPaneData(true);
            if (data.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(data[0]["UserAuthenticationName"])))
                {
                    LogonId = (String)data[0]["UserAuthenticationName"];
                }

                if (!string.IsNullOrEmpty(Convert.ToString(data[0]["UserObjectId"])))
                {
                    UserId = (int)data[0]["UserObjectId"];
                }
                if (!string.IsNullOrEmpty(Convert.ToString(data[0]["ActivatePwd"])))
                {
                    Pwd = (String)data[0]["ActivatePwd"];
                    // Set clear text password to null
                    xml.AppendFormat("<object id=\"{0}\" action=\"Update\">", Convert.ToString(this.ObjectId));
                    xml.Append("<column name=\"ActivatePwd\"></column>");
                    xml.Append("</object>");
                    this.ProcessXML(xml.ToString());
                }

                if (!string.IsNullOrEmpty(LogonId) && !string.IsNullOrEmpty(Pwd) && UserId != 0)
                {
                    this.Login(LogonId, Pwd);
                    this.ReleaseOutriderNet();
                    this.Response.Redirect(string.Format("{0}?PossePresentation=Default&PosseObjectId={1}",
                            this.HomePageUrl, UserId));
                }
                else
                {
                    this.ReleaseOutriderNet();
                    this.Response.Redirect("Login.aspx");
                }
            }
        }

        #endregion
	}
}