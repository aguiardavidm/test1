<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DisabledTabLabel.ascx.cs"
	Inherits="Computronix.POSSE.Outrider.DisabledTabLabel" %>
	
<table cellspacing="0" cellpadding="0" border="0" class="tab">
	<tr>
		<td valign="top">
			<asp:Image ID="imgLeft" runat="server" ImageUrl="~/Images/bgMenuLeft.jpg"></asp:Image></td>
		<td align="center" nowrap>
			<asp:Panel ID="pnlTabLabel" runat="server" Height="36px" BackImageUrl="~/Images/bgMenuFill.jpg"
				style="background-repeat:repeat-x;  padding:5px 0px 5px 0px;">
				<asp:Label ID="lblTabLabel" runat="server" CssClass="tabselect"></asp:Label>
			</asp:Panel>
		</td>
		<td valign="top">
			<asp:Image ID="impRight" runat="server" ImageUrl="~/Images/bgMenuRight.jpg">
			</asp:Image></td>
	</tr>
</table>
