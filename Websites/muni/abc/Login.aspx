<%@ Page Language="C#" MasterPageFile="~/OutriderLogin.master" AutoEventWireup="true"
        CodeFile="Login.aspx.cs" Inherits="Computronix.POSSE.Outrider.Login" Title="ABC" %>

<asp:Content ID="cntPaneBand" ContentPlaceHolderID="cphPaneBand" runat="Server">
    <asp:Panel ID="pnlPaneBand" runat="server">
        <form id="posseform" name="posseform">

                                <table style="width: 470px; margin-bottom: 50px;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblEmail" runat="server" Text="User Id:" CssClass="possedetail"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtUserId" TabIndex="1" width="250px" runat="server" CssClass="possedetail"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Hyperlink ID="lnkForgotEmail" TabIndex="3" Width="150px" NavigateUrl="Default.aspx?PossePresentation=RecoverUserId&PosseObjectDef=j_Registration" runat="server" Text="Forgot User Id?" CssClass="smalllink"></asp:Hyperlink></td>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="User Id is required."
                                                ControlToValidate="txtUserId">*</asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPassword" runat="server" Text="Password:" CssClass="possedetail"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtPassword" TabIndex="2" width="250px" runat="server" CssClass="fieldtext" TextMode="Password"></asp:TextBox>
                                        </td>
                                        <td>
                                            <!--<asp:Label ID="lblForgotPwText" Width="170px" runat="server" Text="If you have forgotten or lost your user id or password, please contact the Division of Alcoholic Beverage Control." CssClass="possedetail"></asp:Label>-->
                                            <asp:Hyperlink ID="lnkForgotPass" TabIndex="4" Width="150px" NavigateUrl="Default.aspx?PossePresentation=Reset&PosseObjectDef=j_Registration" runat="server" Text="Forgot password?" CssClass="smalllink"></asp:Hyperlink></td>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Password is required."
                                                ControlToValidate="txtPassword">*</asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Button ID="btnLogin" runat="server" CssClass="loginButton" />
                                        </td>
                                        <td colspan="3">
                                            <asp:ValidationSummary ID="vsmMain" runat="server" CssClass="posseerror" DisplayMode="List"
                                                ForeColor="" />
                                            <asp:Panel ID="pnlErrorBand" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="hidden" id="ComesFrom" name="ComesFrom" value="possesignin">
                                        </td>
                                    </tr>
                                </table>
        </form>
    </asp:Panel>

    <script language="javascript">
    function go_submit( form )
    {
        form.submit();
    }

    function PosseOnRoundTrip() {}
    function PosseRefreshJavaScriptWidgets(){}
    </script>

    <form id="possedocumentchangeform" name="possedocumentchangeform" method="post" onsubmit="return;">
        <input type="hidden" id="currentpaneid" name="currentpaneid" value="">
        <input type="hidden" id="paneid" name="paneid" value="">
        <input type="hidden" id="functiondef" name="functiondef" value="">
        <input type="hidden" id="sortcolumns" name="sortcolumns" value="">
        <input type="hidden" id="datachanges" name="datachanges" value="">
        <input type="hidden" id="Hidden1" name="comesfrom" value="possesignin">
        <input type="hidden" id="changesxml" name="changesxml" value="">
    </form>
</asp:Content>
<asp:Content ID="cntBottomFunctionBand" runat="server" ContentPlaceHolderID="cphBottomFunctionBand">
    <asp:Panel ID="pnlBottomFunctionBand" runat="server" Style="margin-left: 10px">
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntDebugLinkBand" runat="server" ContentPlaceHolderID="cphDebugLinkBand">
    <asp:Panel ID="pnlDebugLinkBand" runat="server">
    </asp:Panel>
</asp:Content>