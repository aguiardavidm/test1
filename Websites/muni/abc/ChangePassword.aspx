<%@ Page Language="C#" MasterPageFile="~/OutriderPopup.master" AutoEventWireup="true"
	CodeFile="ChangePassword.aspx.cs" Inherits="Computronix.POSSE.Outrider.ChangePassword"
	Title="Change Password" %>

<asp:Content ID="cntPaneBand" ContentPlaceHolderID="cphPaneBand" runat="Server">
	<asp:Panel ID="pnlChangePassword" runat="server">
		<table>
			<tr>
				<td align="left" colspan="3">
					<div class="title">Change Password</div></td>
			</tr>
			<tr>
				<td>
					<asp:Label ID="lblPassword" runat="server" Text="Old Password:" CssClass="posselabel possedetail"></asp:Label></td>
				<td>
					<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="fieldtext"></asp:TextBox>
				</td>
				<td>
					<asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Password is required."
						ControlToValidate="txtPassword">*</asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td>
					<asp:Label ID="lblNewPassword" runat="server" Text="New Password:" CssClass="posselabel possedetail"></asp:Label></td>
				<td>
					<asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="fieldtext"></asp:TextBox>
				</td>
				<td>
					<asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" ErrorMessage="New Password is required."
						ControlToValidate="txtNewPassword">*</asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td>
					<asp:Label ID="lblConfirmNewPassword" runat="server" Text="Confirm New Password:"
						CssClass="posselabel possedetail"></asp:Label></td>
				<td>
					<asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password" CssClass="fieldtext"></asp:TextBox>
				</td>
				<td>
					<asp:RequiredFieldValidator ID="rfvConfirmNewPassword" runat="server" ErrorMessage="Confirm New Password is required."
						ControlToValidate="txtConfirmNewPassword">*</asp:RequiredFieldValidator>
					<asp:CompareValidator ID="cpvNewPassword" runat="server" ErrorMessage="New Password and Confirm New Password do not match."
						ControlToCompare="txtConfirmNewPassword" ControlToValidate="txtNewPassword">*</asp:CompareValidator>
					<asp:CompareValidator ID="cpdNewPassword" runat="server" Operator="NotEqual" ErrorMessage="New Password is the same as the old Password."
						ControlToCompare="txtPassword" ControlToValidate="txtNewPassword">*</asp:CompareValidator>
				</td>
			</tr>
			<tr>
				<td align="left" colspan="3">
		             <div class="button">
                        <asp:LinkButton ID="Button1" runat="server" OnClick="btnChangePassword_Click">
                            <span id="changebtnlbl">Change Password</span>
                        </asp:LinkButton>
                        </a>
                    </div>
			    </td>
			</tr>
			<tr>
				<td colspan="3">
					<asp:ValidationSummary ID="vsmMain" runat="server" CssClass="posseerror" DisplayMode="List"
						ForeColor="" />
				</td>
			</tr>
		</table>
	</asp:Panel>
	<asp:Panel ID="pnlContinue" runat="server" Visible="false">
		<table>
			<tr>
				<td align="left">
					<div class="title">Change Password</div>
				</td>
			</tr>
			<tr>
				<td align="left" nowrap>
					<asp:Label ID="lblmessage" runat="server" Text="Your password has been changed."
						CssClass="possedetail"></asp:Label></td>
			</tr>
			<tr>
				<td align="left">
				<div class="button" title="Close"><a onclick="javascript: window.close()"><span>Close Window</span></a></div>
				</td>
			</tr>
		</table>
	</asp:Panel>
</asp:Content>
