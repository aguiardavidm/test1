using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.DirectoryServices;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Reflection;

namespace Computronix.POSSE.RestWordInterface.Services
{
    /// <summary>
    /// Contains service functions for use by the various controllers.
    /// </summary>
    public class Services : WinchesterServiceApiControllerBase
    {

        #region Document Manipulation

        /// <summary>
        /// Saves the Word Interface document into POSSE.  This uses the metadata configured on recordsetName to drive
        /// all the optional processing.  If unsignedDocument is not null and WordInterfaceUnsignedEndPoint is not null,
        /// the unsignedDocument is also saved.
        /// </summary>
        /// <param name="recordsetName">Recordset name containing the Word Interface parameters.</param>
        /// <param name="objectId">POSSE ObjectId of the reference object.</param>
        /// <param name="documentId">DocumentId of this document</param>
        /// <param name="prefix">Prefix to use for this save of the document.</param>
        /// <param name="document">Byte[] containing binary version of document to store.</param>
        /// <param name="unsignedDocument">Byte[] containing binary version of document prior to signing.  Ignored if null.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">current traceKey</param>
        /// <param name="PosseSignIsCompleteOnDocument">Used to set SignIsComplete on the document</param>
        /// <param name="PosseSignIsCompleteOnSource">Used to set SignIsComplete on the source object</param>
		/// <param name="PosseSaveAsPDF">Used to indicate to save as PDF</param>
        /// <param name="userId">POSSE userid to use for Winchester CreateSession.</param>
        /// <param name="password">Password to use for Winchester CreateSession.</param>
        /// <returns>On Success, returns String numeric containing token.  Otherwise, string containing error message (based on value of IsValid).</returns>
        public string SetDocumentViaEndpoint(string recordsetName, string objectId, string documentId, string prefix,
            byte[] document, byte[] unsignedDocument, string sessionToken, string traceKey, string PosseSignIsCompleteOnDocument,
            string PosseSignIsCompleteOnSource, string PosseSaveAsPDF, string userId, string password)
        {
            int wordInterfaceDocumentTypeId;
            String wordInterfaceEndPointName, wordInterfaceDocumentType, wordInterfaceToObjectId,
                wordInterfaceFileName, wordInterfaceFileDescription, wordInterfaceClassification,
                wordInterfaceUnsignedEndPoint;
            String result = objectId;
            StringBuilder processXML;
            List<OrderedDictionary> newObjects;
            OrderedDictionary controlData = null;
            Int32 dataSourceObjectId = int.Parse(objectId);
            String docExtension = "docm";
            Boolean lockRevision = false;
            bool sessionCreated = false;
            string resultString;
            if (!string.IsNullOrEmpty(userId))
            {
                sessionToken = this.CreateSession(userId, password);
                
                if (!string.IsNullOrEmpty(sessionToken))
                {
                    sessionCreated = true;
                }
            }

            if (!string.IsNullOrEmpty(sessionToken))
            {
                resultString = this.GetRecordSet(sessionToken, dataSourceObjectId, recordsetName);

                if (!string.IsNullOrEmpty(resultString))
                {
                    // If the recordset contains a list, then assume it contains only one element.
                    if (resultString.StartsWith("[") && resultString.EndsWith("]"))
                    {
                        resultString = resultString.Substring(1, resultString.Length - 2);
                    }
                    controlData = JsonConvert.DeserializeObject<OrderedDictionary>(resultString);
                }

                wordInterfaceEndPointName = getFieldOrThrowException(controlData, "WordInterfaceEndPointName");

                // If SaveIsValid is null, we can proceed.
                // If it's not null, it contains the error message to return to MSWord
                if (controlData.Contains("SaveIsValid") && controlData["SaveIsValid"] != null &&
                    !String.IsNullOrEmpty(controlData["SaveIsValid"].ToString()))
                {
                    result = controlData["SaveIsValid"].ToString();
                }
                else
                {
                    // Get the all values (or defaults) for the document that will be created
                    wordInterfaceDocumentType = getFieldOrDefault(controlData, "WordInterfaceDocumentType", "d_WebDocument");
                    wordInterfaceFileName = getFieldOrDefault(controlData, "WordInterfaceFileName", "Word Interface");
                    wordInterfaceFileDescription = getFieldOrDefault(controlData, "WordInterfaceFileDescription", "Word Interface");
                    wordInterfaceClassification = getFieldOrDefault(controlData, "WordInterfaceClassification", "Document");
                    wordInterfaceDocumentTypeId = int.Parse(getFieldOrThrowException(controlData, "WordInterfaceDocumentTypeId"));
                    wordInterfaceToObjectId = getFieldOrDefault(controlData, "WordInterfaceToObjectId", objectId);
                    wordInterfaceUnsignedEndPoint = getFieldOrDefault(controlData, "WordInterfaceUnsignedEndPoint", "");

                    // Upload the document to Posse
                    processXML = new StringBuilder();

                    // Check if we're supposed to update the IsComplete flag and lock the document as well
                    if (!string.IsNullOrEmpty(prefix) && controlData.Contains(String.Format("{0}IsComplete", prefix)))
                    {
                        // == CHANGE FROM STANDARD WORD INTERFACE
                        // Set SignIsComplete on source if indicated.  Inside the if is the original code.
                        if (PosseSignIsCompleteOnSource == "Y")
                        {
                            processXML.AppendFormat(String.Concat(
                                "<object id=\"{0}\" action=\"Update\">",
                                    "<column name=\"{1}IsComplete\">Y</column>",
                                "</object>"),
                                wordInterfaceToObjectId, prefix);
                        }
                        // == CHANGE FROM STANDARD WORD INTERFACE
                        // Set SignIsComplete on document if indicated.
                        if (PosseSignIsCompleteOnDocument == "Y")
                        {
                            processXML.AppendFormat(String.Concat(
                                "<object id=\"{0}\" action=\"Update\">",
                                    "<column name=\"{1}IsComplete\">Y</column>",
                                "</object>"),
                                documentId != "0" ? documentId.ToString() : "NEW1", prefix);
                        }
                        if (prefix == "Sign")
                        {
                            docExtension = (PosseSaveAsPDF == "Y") ? "pdf" : "docx";
                            lockRevision = true;
                        }
                    }

                    if (documentId != "NEW")
                    {
                        // Update the existing document
                        processXML.AppendFormat(String.Concat(
                            "<object id=\"{0}\" action=\"Update\">",
                                "<revision basename=\"{1}\" extension=\"{2}\" ",
                                    "forcenewrevision=\"{3}\" lockrevision=\"{4}\">{5}</revision>",
                                "<column name=\"FileExtension\">{2}</column>",
                            "</object>"),
                            documentId, "PosseDocument", docExtension, false, lockRevision,
                            Convert.ToBase64String(document));
                    }
                    else
                    {
                        // Create a new document, set all it's dynamic details and relate it to ToObjectId
                        processXML.AppendFormat(String.Concat(
                            "<object id=\"NEW1\" action=\"Insert\" objectdef=\"{0}\">",
                                "<revision basename=\"{1}\" extension=\"{2}\">{3}</revision>",
                                "<column name=\"FileName\">{4}</column>",
                                "<column name=\"Description\">{5}</column>",
                                "<column name=\"Classification\">{6}</column>",
                                "<column name=\"FileExtension\">{2}</column>",
                                "<column name=\"FileSize\">{7}</column>",
                                "<column name=\"CreatedByWordInterface\">Y</column>",
                            "</object>",
                            "<object id=\"{8}\" action=\"Update\">",
                                "<relationship endpoint=\"{9}\" toobjectid=\"NEW1\" action=\"Insert\" />",
                            "</object>"),
                            wordInterfaceDocumentType, wordInterfaceFileName, docExtension,
                            Convert.ToBase64String(document), wordInterfaceFileName,
                            wordInterfaceFileDescription, wordInterfaceClassification,
                            document.Length, wordInterfaceToObjectId, wordInterfaceEndPointName);
                    }

                    // Check if we also need to save the unsigned version of the document
                    if (unsignedDocument != null && wordInterfaceUnsignedEndPoint.Length > 0)
                    {
                        // Upload the unsigned document to Posse
                        processXML.AppendFormat(string.Concat(
                            "<object id=\"NEW2\" action=\"Insert\" objectdef=\"{0}\">",
                                "<revision basename=\"{1}\" extension=\"docm\">{2}</revision>",
                                "<column name=\"FileName\">{3}</column>",
                                "<column name=\"Description\">{4} (Unsigned)</column>",
                                "<column name=\"Classification\">{5}</column>",
                                "<column name=\"FileExtension\">docm</column>",
                                "<column name=\"FileSize\">{6}</column>",
                                "<column name=\"CreatedByWordInterface\">Y</column>",
                            "</object>",
                            "<object id=\"{7}\" action=\"Update\">",
                                "<relationship endpoint=\"{8}\" toobjectid=\"NEW2\" action=\"Insert\" />",
                            "</object>"),
                            wordInterfaceDocumentType, wordInterfaceFileName,
                            Convert.ToBase64String(unsignedDocument), wordInterfaceFileName,
                            wordInterfaceFileDescription, wordInterfaceClassification,
                            unsignedDocument.Length, wordInterfaceToObjectId, wordInterfaceUnsignedEndPoint);
                    }

                    // Now call ProcessXML to make the document(s) real, and relate them to the appropriate objects
                    resultString = this.ProcessXml(sessionToken, processXML.ToString());

                    // Check if we created a new document (rather than updating the existing one)
                    if (documentId == "NEW")
                    {
                        if (!string.IsNullOrEmpty(resultString))
                        {
                            newObjects = JsonConvert.DeserializeObject<List<OrderedDictionary>>(resultString);

                            // Get the new documentId
                            foreach (OrderedDictionary obj in newObjects)
                            {
                                if ((string)obj["XMLHandle"] == "NEW1")
                                {
                                    // Found the document that was created so set the new token and break out
                                    objectId = obj["ObjectId"].ToString();

                                    break;
                                }
                            }
                        }
                    }

                    result = objectId;
                }

                if (sessionCreated)
                {
                    this.ExpireSession(sessionToken);
                }
            }

            return result;
        }

        #endregion

        #region Helpers

        //
        private String getFieldOrDefault(OrderedDictionary dict, String key, String defaultValue)
        {
            if (dict.Contains(key) && !String.IsNullOrEmpty(dict[key].ToString()))
            {
                return HttpUtility.HtmlEncode(dict[key].ToString());
            }
            else
            {
                return defaultValue;
            }
        }

        private String getFieldOrThrowException(OrderedDictionary dict, String key)
        {
            if (dict.Contains(key) && !String.IsNullOrEmpty(dict[key].ToString()))
            {
                return dict[key].ToString();
            }
            else
            {
                throw new ApplicationException(String.Format("Data does not contain a value for ({0})", key));
            }
        }

        /// <summary>
        /// Gets the user guid.
        /// </summary>
        /// <returns></returns>
        private string GetUserGUID(string userId)
        {
            string result = null;
            int index;
            DirectoryEntry directory = new DirectoryEntry("LDAP://DC=idir,DC=BCGOV");
            DirectorySearcher searcher = new DirectorySearcher(directory);
            SearchResult searchResult;

            if (!string.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["LOGON_USER"]))
            {
                userId = HttpContext.Current.Request.ServerVariables["LOGON_USER"];
            }

            index = userId.LastIndexOf(@"\");

            if (index > 0)
            {
                userId = userId.Substring(index + 1);
            }

            searcher.Filter = string.Format("sAMAccountName={0}", userId);
            searchResult = searcher.FindOne();
            // the "0" index is necessary because there can be multiples for a property
            Object tempResult = searchResult.Properties["bcgovGUID"][0];
            if (tempResult is Byte[])
            {
                result = System.Text.ASCIIEncoding.ASCII.GetString((Byte[])tempResult);
            }
            else
            {
                result = tempResult.ToString();
            }

            result = "S_" + this.b32Encode(result);

            searcher.Dispose();  // at least one of these requires an explicit dispose
            directory.Dispose();
            return result;
        }

        // Functions to provide encoding of strings with Base32.
        // This uses the algorithm from:
        // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/rma/urlformat.asp
        //
        // However, because we actually get a string of hexadecimal
        // characters (e.g. '2ad34f..."), the routine has been modified
        // a bit from strictly following the document.
        private string b32Encode(string strIn)
        {
            int h1, h2, h3, h4, h5, w1, w2, w3, w4;
            string strOut = null;

            // Make sure input string is 35 hex chars long
            strIn = strIn + "00000000000000000000000000000000000".Substring(0, 35 - strIn.Length);

            for (int index = 0; index < strIn.Length; index += 5)
            {
                h1 = this.convertHexChar(strIn.Substring(index, 1));
                h2 = this.convertHexChar(strIn.Substring(index + 1, 1));
                h3 = this.convertHexChar(strIn.Substring(index + 2, 1));
                h4 = this.convertHexChar(strIn.Substring(index + 3, 1));
                h5 = this.convertHexChar(strIn.Substring(index + 4, 1));

                w1 = (h1 * 16 + h2) / 8;
                w2 = ((h2 & 7) * 16 + h3) / 4;
                w3 = ((h3 & 3) * 16 + h4) / 2;
                w4 = (h4 & 1) * 16 + h5;

                strOut += this.mimeEncode(w1) + this.mimeEncode(w2) + this.mimeEncode(w3) + this.mimeEncode(w4);
            }

            return strOut;
        }

        private int convertHexChar(string hexIn)
        {
            int retVal;
            hexIn = hexIn.Substring(0, 1).ToLower();

            if (string.Compare(hexIn, "a") < 0)
            {
                retVal = int.Parse(hexIn);
            }
            else
            {
                retVal = (int)hexIn.ToCharArray()[0] - 87;
            }

            return retVal;
        }

        private string mimeEncode(int intIn)
        {
            string retVal = "";

            if (intIn >= 0)
            {
                retVal = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".Substring(intIn, 1);
            }

            return retVal;
        }

        /// <summary>
        /// Function to get byte array from a file, then delete it.
        /// </summary>
        /// <param name="FileName">File name to get byte array</param>
        /// <returns>Byte Array</returns>
        public byte[] FileToByteArray(string FileName)
        {
            byte[] Buffer = null;

            try
            {
                // Open file for reading
                System.IO.FileStream FileStream = new System.IO.FileStream(FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // attach filestream to binary reader
                System.IO.BinaryReader BinaryReader = new System.IO.BinaryReader(FileStream);

                // get total byte length of the file
                long _TotalBytes = new System.IO.FileInfo(FileName).Length;

                // read entire file into buffer
                Buffer = BinaryReader.ReadBytes((Int32)_TotalBytes);

                // close file reader
                FileStream.Close();
                FileStream.Dispose();
                BinaryReader.Close();
                System.IO.File.Delete(FileName);
            }
            catch (Exception e)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", e.ToString());
            }

            return Buffer;
        }

        #endregion
    }
}