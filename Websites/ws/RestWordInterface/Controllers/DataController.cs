﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Computronix.POSSE.RestWordInterface.Services;
using System.Text;
using System.Xml.Linq;
using System.Collections.Specialized;
using System.Configuration;
using Newtonsoft.Json;
using System.Reflection;

namespace Computronix.POSSE.RestWordInterface.Controllers
{
    public class DictionaryComparer : IComparer<OrderedDictionary>
    {
        public int Compare(OrderedDictionary x, OrderedDictionary y)
        {
            int result = 0;
            string sortColumnName = "POSSESortOrder";

            if (x[sortColumnName] == null)
            {
                if (y[sortColumnName] != null)
                {
                    result = -1;
                }
            }
            else if (y[sortColumnName] == null)
            {
                result = 1;
            }
            else if (x[sortColumnName] is IComparable)
            {
                result = ((IComparable)x[sortColumnName]).CompareTo((IComparable)y[sortColumnName]);
            }
            else
            {
                result = x[sortColumnName].ToString().CompareTo(y[sortColumnName].ToString());
            }

            return result;
        }
    }

    public class DataController : WinchesterServiceApiControllerBase
    {
        /// <summary>
        /// Get data from POSSE.
        /// </summary>
        /// <param name="objectId">The POSSE ObjectId of the reference object.</param>
        /// <param name="recordsetName">The POSSE Recordset for the data to return.</param>
        /// <param name="sessionToken">current session token</param>
        /// <returns>A string containing an XML document with the return data.</returns>
        public List<OrderedDictionary> GetData(string objectId, string recordsetName, string sessionToken)
        {
            return GetData(objectId, recordsetName, sessionToken, null, null, null);
        }        
        
        /// <summary>
        /// Get data from POSSE with tracing set on the query parameters.
        /// </summary>
        /// <param name="objectId">The POSSE ObjectId of the reference object.</param>
        /// <param name="recordsetName">The POSSE Recordset for the data to return.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">current traceKey</param>
        /// <returns>A string containing an XML document with the return data.</returns>
        public List<OrderedDictionary> GetData(string objectId, string recordsetName, string sessionToken, string traceKey)
        {
            return GetData(objectId, recordsetName, sessionToken, traceKey, null, null);
        }

        /// <summary>
        /// Get data from POSSE. (Optionally, UserId and Password can be passed as query parms)
        /// </summary>
        /// <param name="objectId">The POSSE ObjectId of the reference object.</param>
        /// <param name="recordsetName">The POSSE Recordset for the data to return.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">Optional current traceKey</param>
        /// <param name="userId">Optional query parameter containing the authentication UserId</param>
        /// <param name="password">Optional query parameter containing the authentication Password</param>
        /// <returns>A string containing an XML document with the return data.</returns>
        public List<OrderedDictionary> GetData(string objectId, string recordsetName, string sessionToken, string traceKey, string userId, string password)
        {
            List<OrderedDictionary> result = null;
            string resultString;
            bool sessionCreated = false;

            if (!string.IsNullOrEmpty(userId))
            {
                sessionToken = this.CreateSession(userId, password);
                
                if (!string.IsNullOrEmpty(sessionToken))
                {
                    sessionCreated = true;
                }
            }

            if (!string.IsNullOrEmpty(sessionToken))
            {
                resultString = this.GetRecordSet(sessionToken, int.Parse(objectId), recordsetName);
                
                if (!string.IsNullOrEmpty(resultString))
                {
                    if (resultString.StartsWith("["))
                    {
                        result = JsonConvert.DeserializeObject<List<OrderedDictionary>>(resultString);
                    }
                    else
                    {
                        result = new List<OrderedDictionary>();
                        result.Add(JsonConvert.DeserializeObject<OrderedDictionary>(resultString));
                    }
                }

                // Represent Boolean values as strings.
                for (int i = 0; i < result.Count; ++i)
                {
                    for (int j = 0; j < result[i].Count; ++j)
                    {
                        if (result[i][j] != null && result[i][j].GetType() == typeof(bool))
                        {
                            result[i][j] = (bool)result[i][j] ? "Y" : "N";
                        }
                    }
                }

                if (sessionCreated)
                {
                    this.ExpireSession(sessionToken);
                }
            }

            if (result != null)
            {
                result.Sort(new DictionaryComparer());
            }

            return result;
        }
    }
}
