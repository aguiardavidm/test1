﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Computronix.POSSE.RestWordInterface.Services;
using System.Configuration;
using System.Reflection;

namespace Computronix.POSSE.RestWordInterface.Controllers
{
    public class ImageController : WinchesterServiceApiControllerBase
    {
        /// <summary>
        /// Get an Image from POSSE.
        /// </summary>
        /// <param name="imageDocumentId">The DocumentId of the image document to fetch.</param>
        /// <param name="sessionToken">current session token</param>
        /// <returns>A string containing an XML document with the return data.</returns>
        public string GetImage(string imageDocumentId, string sessionToken)
        {
            return GetImage(imageDocumentId, sessionToken, null, null, null);
        }

        /// <summary>
        /// Get an Image from POSSE with a trace key
        /// </summary>
        /// <param name="imageDocumentId">The DocumentId of the image document to fetch.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">current traceKey</param>
        /// <returns>A string containing an XML document with the return data.</returns>
        public string GetImage(string imageDocumentId, string sessionToken, string traceKey)
        {
            return GetImage(imageDocumentId, sessionToken, traceKey, null, null);
        }

        /// <summary>
        /// Get an Image from POSSE. (Optionally, UserId and Password can be passed as query parms)
        /// </summary>
        /// <param name="imageDocumentId">The DocumentId of the image document to fetch.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">current traceKey</param>
        /// <param name="userId">Optional query parameter containing the authentication UserId</param>
        /// <param name="password">Optional query parameter containing the authentication Password</param>
        /// <returns>A string containing an XML document with the return data.</returns>
        public string GetImage(string imageDocumentId, string sessionToken, string traceKey, string userId, string password)
        {
            string result = "";
            byte[] contents;
            bool sessionCreated = false;

            if (!string.IsNullOrEmpty(userId))
            {
                sessionToken = this.CreateSession(userId, password);

                if (!string.IsNullOrEmpty(sessionToken))
                {
                    sessionCreated = true;
                }
            }

            if (!string.IsNullOrEmpty(sessionToken))
            {
                if (!string.IsNullOrEmpty(imageDocumentId))
                {
                    contents = this.GetDocument(sessionToken, int.Parse(imageDocumentId));
                    
                    if (contents.Length > 0)
                    {
                        result = Convert.ToBase64String(contents);
                    }
                }

                if (sessionCreated)
                {
                    this.ExpireSession(sessionToken);
                }
            }

            return result;
        }
    }
}
