using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Computronix.POSSE.RestWordInterface.Services;
using System.Threading.Tasks;


namespace Computronix.POSSE.RestWordInterface.Controllers
{
    public class UnsignedController : Services.Services
    {
        /// <summary>
        /// Post both a signed and unsigned document back to POSSE.
        /// This service expects a multipart-form as input, containing the Prefix, Document, and Unsigned Document.
        /// </summary>
        /// <param name="objectId">The POSSE ObjectId of the reference object.</param>
        /// <param name="documentId">The DocumentId of this document.</param>
        /// <param name="recordsetName">The POSSE Recordset for the document handling parameters.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">current traceKey</param>
        /// <param name="PosseSignIsCompleteOnDocument">Query parameter used to set SignIsComplete on the document</param>
        /// <param name="PosseSignIsCompleteOnSource">Query parameter used to set SignIsComplete on the source object</param>
        /// <returns>The token</returns>
        public async Task<HttpResponseMessage> PostUnsigned(string objectId, string documentId, string recordsetName,
            string sessionToken, string traceKey, string PosseSignIsCompleteOnDocument, string PosseSignIsCompleteOnSource)
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }
                string root = HttpContext.Current.Server.MapPath("~/App_Data");
                var provider = new MultipartFormDataStreamProvider(root);

                try
                {
                    // read the form data
                    await Request.Content.ReadAsMultipartAsync(provider);

                    var docBlob = this.FileToByteArray(provider.FileData[0].LocalFileName);
                    var unsignedDoc = this.FileToByteArray(provider.FileData[1].LocalFileName);

                    var prefix = provider.FormData["prefix"];

                    return Request.CreateResponse(HttpStatusCode.OK, this.SetDocumentViaEndpoint(recordsetName, objectId,
                        documentId, prefix, docBlob, unsignedDoc, sessionToken, traceKey, PosseSignIsCompleteOnDocument,
                        PosseSignIsCompleteOnSource, "N", null, null));
                }
                catch (System.Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(Computronix.Ext.HandlerBase.Write(e));
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                }
            }   
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine(Computronix.Ext.HandlerBase.Write(exception));
                throw;
            }
        }

        /// <summary>
        /// Post both a signed and unsigned document back to POSSE. (Optionally, UserId and Password can be passed as query parms)
        /// This service expects a multipart-form as input, containing the Prefix, Document, and Unsigned Document.
        /// </summary>
        /// <param name="objectId">The POSSE ObjectId of the reference object.</param>
        /// <param name="documentId">The DocumentId of this document.</param>
        /// <param name="recordsetName">The POSSE Recordset for the document handling parameters.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">current traceKey</param>
        /// <param name="PosseSignIsCompleteOnDocument">Query parameter used to set SignIsComplete on the document</param>
        /// <param name="PosseSignIsCompleteOnSource">Query parameter used to set SignIsComplete on the source object</param>
        /// <param name="userId">Optional query parameter containing the authentication UserId</param>
        /// <param name="password">Optional query parameter containing the authentication Password</param>
        /// <returns>The token</returns>
        public async Task<HttpResponseMessage> PostUnsigned(string objectId, string documentId, string recordsetName,
            string sessionToken, string traceKey, string PosseSignIsCompleteOnDocument, string PosseSignIsCompleteOnSource,
            string userId, string password)
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }
                string root = HttpContext.Current.Server.MapPath("~/App_Data");
                var provider = new MultipartFormDataStreamProvider(root);

                try
                {
                    // read the form data
                    await Request.Content.ReadAsMultipartAsync(provider);

                    var docBlob = this.FileToByteArray(provider.FileData[0].LocalFileName);
                    var unsignedDoc = this.FileToByteArray(provider.FileData[1].LocalFileName);

                    var prefix = provider.FormData["prefix"];

                    return Request.CreateResponse(HttpStatusCode.OK, this.SetDocumentViaEndpoint(recordsetName, objectId,
                        documentId, prefix, docBlob, unsignedDoc, sessionToken, traceKey, PosseSignIsCompleteOnDocument,
                        PosseSignIsCompleteOnSource, "N", userId, password));
                }
                catch (System.Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(Computronix.Ext.HandlerBase.Write(e));
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                }
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine(Computronix.Ext.HandlerBase.Write(exception));
                throw;
            }
        }
    }
}
