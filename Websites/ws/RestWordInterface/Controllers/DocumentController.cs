using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Computronix.POSSE.RestWordInterface.Services;
using System.Threading.Tasks;
using System.Configuration;

namespace Computronix.POSSE.RestWordInterface.Controllers
{
    /// <summary>
    /// This is the Document Controller doc.
    /// </summary>
    public class DocumentController : Services.Services
    {
        private static int? sleepInterval;
        protected int SleepInterval
        {
            get
            {
                if (!DocumentController.sleepInterval.HasValue)
                {
                    if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SleepInterval"]))
                    {
                        DocumentController.sleepInterval = 250;
                    }
                    else
                    {
                        DocumentController.sleepInterval =
                            int.Parse(ConfigurationManager.AppSettings["SleepInterval"]);
                    }
                }

                return DocumentController.sleepInterval.GetValueOrDefault();
            }
            set
            {
                DocumentController.sleepInterval = value;
            }
        }

        private static int? timeoutInterval;
        protected int TimeoutInterval
        {
            get
            {
                if (!DocumentController.timeoutInterval.HasValue)
                {
                    if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["TimeoutInterval"]))
                    {
                        DocumentController.timeoutInterval = 30000;
                    }
                    else
                    {
                        DocumentController.timeoutInterval =
                            int.Parse(ConfigurationManager.AppSettings["TimeoutInterval"]);
                    }
                }

                return DocumentController.timeoutInterval.GetValueOrDefault();
            }
            set
            {
                DocumentController.timeoutInterval = value;
            }
        }

        /// <summary>
        /// Post the document back to POSSE.
        /// This service expects a multipart-form as input, containing the Prefix and the Document.
        /// </summary>
        /// <param name="objectId">The POSSE ObjectId of the reference object</param>
        /// <param name="documentId">The DocumentId of this document</param>
        /// <param name="recordsetName">The POSSE Recordset for the document handling parameters.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">current traceKey</param>
        /// <param name="PosseSignIsCompleteOnDocument">Query parameter used to set SignIsComplete on the document</param>
        /// <param name="PosseSignIsCompleteOnSource">Query parameter used to set SignIsComplete on the source object</param>		
		/// <param name="PosseSaveAsPDF">Used to indicate to save as PDF</param>
        /// <returns>The token</returns>
        public async Task<HttpResponseMessage> PostDocument(string objectId, string documentId, string recordsetName,
            string sessionToken, string traceKey, string PosseSignIsCompleteOnDocument, string PosseSignIsCompleteOnSource,
			string PosseSaveAsPDF)
        {
            DateTime requestStart = DateTime.Now;
            bool done = true;
            byte[] docBlob = new byte[0];
            HttpResponseMessage response;

            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                string root = HttpContext.Current.Server.MapPath("~/App_Data");
                var provider = new MultipartFormDataStreamProvider(root);

                //We Should never get this blank error because RESP will be assigned to another response if it works.
                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "");

                try
                {
                    // read the form data
                    await Request.Content.ReadAsMultipartAsync(provider);

                    do
                    {
                        try
                        {
                            docBlob = this.FileToByteArray(provider.FileData[0].LocalFileName);
                            var prefix = provider.FormData["prefix"];
                            response = Request.CreateResponse(HttpStatusCode.OK, this.SetDocumentViaEndpoint(recordsetName,
                                objectId, documentId, prefix, docBlob, null, sessionToken, traceKey, PosseSignIsCompleteOnDocument,
                                PosseSignIsCompleteOnSource, PosseSaveAsPDF, null, null));
                            done = true;
                        }
                        catch (Exception)
                        {
                            if (DateTime.Now < requestStart.Add(new TimeSpan(0, 0, 0, 0, this.TimeoutInterval)))
                            {
                                done = false;
                                System.Threading.Thread.Sleep(this.SleepInterval);
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                    while (!done);
                }
                catch (System.Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(Computronix.Ext.HandlerBase.Write(e));
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine(Computronix.Ext.HandlerBase.Write(exception));
                throw;
            }

            return response;
        }

        /// <summary>
        /// Post the document back to POSSE. (Optionally, UserId and Password can be passed as query parms)
        /// This service expects a multipart-form as input, containing the Prefix and the Document.
        /// </summary>
        /// <param name="objectId">The POSSE ObjectId of the reference object</param>
        /// <param name="documentId">The DocumentId of this document</param>
        /// <param name="recordsetName">The POSSE Recordset for the document handling parameters.</param>
        /// <param name="sessionToken">current session token</param>
        /// <param name="traceKey">current traceKey</param>
        /// <param name="PosseSignIsCompleteOnDocument">Query parameter used to set SignIsComplete on the document</param>
        /// <param name="PosseSignIsCompleteOnSource">Query parameter used to set SignIsComplete on the source object</param>
		/// <param name="PosseSaveAsPDF">Used to indicate to save as PDF</param>
        /// <param name="userId">Optional query parameter containing the authentication UserId</param>
        /// <param name="password">Optional query parameter containing the authentication Password</param>
        /// <returns>The token</returns>
        public async Task<HttpResponseMessage> PostDocument(string objectId, string documentId, string recordsetName,
            string sessionToken, string traceKey, string PosseSignIsCompleteOnDocument, string PosseSignIsCompleteOnSource,
            string PosseSaveAsPDF, string userId, string password)
        {
            Services.Services svc = new Services.Services();
            DateTime requestStart = DateTime.Now;
            bool done = true;
            byte[] docBlob = new byte[0];
            HttpResponseMessage response;

            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                string root = HttpContext.Current.Server.MapPath("~/App_Data");
                var provider = new MultipartFormDataStreamProvider(root);

                //We Should never get this blank error because RESP will be assigned to another response if it works.
                response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "");

                try
                {
                    // read the form data
                    await Request.Content.ReadAsMultipartAsync(provider);

                    do
                    {
                        try
                        {
                            docBlob = svc.FileToByteArray(provider.FileData[0].LocalFileName);
                            var prefix = provider.FormData["prefix"];
                            response = Request.CreateResponse(HttpStatusCode.OK, svc.SetDocumentViaEndpoint(recordsetName,
                                objectId, documentId, prefix, docBlob, null, sessionToken, traceKey, PosseSignIsCompleteOnDocument,
                                PosseSignIsCompleteOnSource, PosseSaveAsPDF, userId, password));
                            done = true;
                        }
                        catch (Exception)
                        {
                            if (DateTime.Now < requestStart.Add(new TimeSpan(0, 0, 0, 0, this.TimeoutInterval)))
                            {
                                done = false;
                                System.Threading.Thread.Sleep(this.SleepInterval);
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                    while (!done);
                }
                catch (System.Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(Computronix.Ext.HandlerBase.Write(e));
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine(Computronix.Ext.HandlerBase.Write(exception));
                throw;
            }

            return response;
        }
    }
}