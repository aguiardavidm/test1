using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Computronix.POSSE.RestWordInterface
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "ImageApi",
                routeTemplate: "image/{imageDocumentId}",
                defaults: new { controller = "Image", traceKey = "", userId = RouteParameter.Optional, password = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DocumentApi",
                routeTemplate: "{controller}/{objectId}/{documentId}/{recordsetName}",
                defaults: new { traceKey = "", posseSignSource = RouteParameter.Optional, userId = RouteParameter.Optional, password = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "data/{objectId}/{recordsetName}",
                defaults: new { controller = "Data", traceKey = "", userId = RouteParameter.Optional, password = RouteParameter.Optional }
            );



            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
        }
    }
}
