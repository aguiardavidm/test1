﻿using System.Web;
using System.Web.Mvc;

namespace Computronix.POSSE.RestWordInterface
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}