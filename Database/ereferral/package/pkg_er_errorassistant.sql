create or replace package PKG_ER_ErrorAssistant is

  -- Author  : Michael Froese
  -- Created : 5-June-2009
  -- Purpose : EReferral Error related proceedures

  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_idlist;

   /*---------------------------------------------------------------------------
   * ReferralRequest_ErrorList()
   *   Returns a list of errors concerning the Referral Request. Used
   * during 'Submit' to confirm that the application has been filled out properly
   * and completely.  Returns an array of error messages that can either be
   * formatted for HTML display, or fed to an error subroutine.
   *-------------------------------------------------------------------------*/
  function ReferralRequest_ErrorList (
    ObjectId                               udt_Id
  ) return udt_stringlist;

  /*---------------------------------------------------------------------------
   * RequestReferral_ErrorsInHTML()
   *   Returns a single VARCHAR2 containing an unordered list in HTML format
   * of the errors returned by ReferralRequest_ErrorList().  For use in displaying all (or
   * as many as possible) errors in the Referral Request process.
   *-------------------------------------------------------------------------*/
  function RequestReferral_ErrorsInHTML (
    ObjectId                               number
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * ValidateReferralAgency()
   * Maintain Referral Agency based on USC-011, USC-017 and DAT-005.
   *-------------------------------------------------------------------------*/
  procedure ValidateReferralAgency (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*---------------------------------------------------------------------------
   * ValidateReferralCenter()
   *-------------------------------------------------------------------------*/
  procedure ValidateReferralCenter (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

end PKG_ER_ErrorAssistant;

 
/

create or replace package body PKG_ER_ErrorAssistant is

  /*--------------------------------------------------------------------------
   * ReferralRequest_ErrorList() -- PUBLIC
   *------------------------------------------------------------------------*/
  function ReferralRequest_ErrorList(
    ObjectId                               udt_Id
  ) return udt_StringList is
    t_DocErrorList                      udt_StringList;
    t_list                              udt_StringList;
    t_Count                             pls_integer;
  begin

    -- Validate Request Method
    if api.pkg_ColumnQuery.Value(objectid, 'RequestMethodEmail') = 'N'
      and api.pkg_ColumnQuery.Value(objectid, 'RequestMethodOnline') = 'N'
      and api.pkg_ColumnQuery.Value(objectid, 'RequestMethodHardcopy') = 'N'
    then
      t_list(t_list.count + 1) := 'At least one request method must be selected';
    end if;
    return t_list;
  end;

  /*--------------------------------------------------------------------------
   * RequestReferral_ErrorsInHTML() -- PUBLIC
   *------------------------------------------------------------------------*/
  function RequestReferral_ErrorsInHTML(
    ObjectId number
  ) return varchar2 is
    output varchar2(4000) := '<ul>';
    tempstring varchar(4000);
    t_list udt_StringList := ReferralRequest_ErrorList(ObjectId);
  begin
    for i in 1..t_list.count loop
       tempstring := '<li>' || t_list(i);
       if length(tempstring) + length(output) < 3991 then
          output := output || tempstring;
       else
          output := output || '<li>....';
          exit;
       end if;
    end loop;

    if output = '<ul>' then output := '[None]'; end if;

    return output;
  end;

  procedure ValidateReferralAgency (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is

  t_Count            pls_Integer;
  t_Ids              udt_IdList;
  t_ReferralCenterId udt_Id;

  begin
    --check for unique referral agency names
    t_ReferralCenterId := api.pkg_columnquery.NumericValue(a_ObjectId,'ReferralCenterObjectId');
    select count(*)
      into t_Count
      from query.r_ER_DefinedInReferralCenter t
    where t.ReferralCenterObjectId = t_ReferralCenterId
      and api.pkg_columnquery.Value(a_ObjectId,'Active') = 'Y'
      and lower(api.pkg_columnquery.Value(a_ObjectId,'Name')) =
          lower(api.pkg_columnquery.Value(t.ReferralAgencyObjectId,'Name')) ;
    if t_Count > 1 then
      api.pkg_errors.RaiseError( -20000, 'The Agency Name must be unique.');
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'ResponseMethod') = 'Guest Web'
      and api.pkg_columnquery.Value(a_ObjectId, 'SelfManaged') = 'Y'
    then
      api.pkg_errors.RaiseError( -20000, 'If the Agency is self managed you cannot select Guest Web as a response method.');
    end if;
    
    if api.pkg_columnquery.Value(a_ObjectId, 'ResponseMethod') = 'Manual to Coordinator'
      and api.pkg_columnquery.Value(a_ObjectId, 'RequestMethodOnline') = 'Y'
    then
      api.pkg_errors.RaiseError( -20000, 'The Request Method cannot be Online for a Manual to Coordinator response method.');
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'ResponseMethod') = 'Guest Web'
      and api.pkg_columnquery.Value(a_ObjectId, 'Type') = 'Internal Agency'
    then
      api.pkg_errors.RaiseError( -20000, 'An Internal Agency may only use the Authenticated Web Response Method.');
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'ResponseMethod') = 'Guest Web'
       and api.pkg_columnquery.Value(a_ObjectId, 'RequestMethodEmail') != 'Y'
    then
       api.pkg_errors.RaiseError( -20000, 'The Request Method must include Email.');
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'ResponseMethod') = 'Authenticated Web'
      and api.pkg_columnquery.Value(a_ObjectId, 'RequestMethodEmail') != 'Y'
      and api.pkg_columnquery.Value(a_ObjectId, 'RequestMethodOnline') != 'Y'
    then
      api.pkg_errors.RaiseError( -20000, 'If Response Method of Authenticated Web is selected, Email or Online must be selected.');
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'RequestMethodEmail') = 'Y'
       or api.pkg_columnquery.Value(a_ObjectId, 'ResponseMethod') = 'Guest Web'
    then
      for i in 1..t_Ids.count
      loop

        if api.pkg_columnquery.Value(t_Ids(i), 'Email') is null
       and api.pkg_columnquery.Value(t_Ids(i), 'PrimaryOrAlternate') = 'Primary'

        then
           api.pkg_errors.RaiseError( -20000, 'Each primary representative must have Email.');
        end if;
      end loop;
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'RequestMethodHardCopy') = 'Y' then
       if api.pkg_columnquery.IsNull(a_ObjectId, 'AddressLine1')
         then
              api.pkg_errors.RaiseError( -20000, 'Before you go on, you must enter a value for Address.');
       end if;

       if api.pkg_columnquery.IsNull(a_ObjectId, 'City')
         then
              api.pkg_errors.RaiseError( -20000, 'Before you go on, you must enter a value for City.');
       end if;

       if api.pkg_columnquery.IsNull(a_ObjectId, 'ProvState')
            then
                 api.pkg_errors.RaiseError( -20000, 'Before you go on, you must enter a value for Province/State.');
       end if;

       if api.pkg_columnquery.IsNull(a_ObjectId, 'PostalCode')
            then
                 api.pkg_errors.RaiseError( -20000, 'Before you go on, you must enter a value for Postal Code.');
       end if;
    end if;

  end ValidateReferralAgency;

  procedure ValidateReferralCenter (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is 
  
  begin
    if api.pkg_ColumnQuery.IsNull(a_ObjectId, 'Name') then
      api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Name.');
    end if;
    if api.pkg_ColumnQuery.IsNull(a_ObjectId, 'Email') then
      api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Email.');
    end if;
    if nvl(api.pkg_columnquery.NumericValue(a_ObjectId, 'InlineAddressCount'), 0) = 0 then
      api.pkg_errors.RaiseError(-20000, 'Before you go on, you must add an Address.');        
    end if;      
  end;
  
end PKG_ER_ErrorAssistant;

/

