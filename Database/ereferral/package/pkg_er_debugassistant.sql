create or replace package           pkg_ER_DebugAssistant is

  -- Author  : MICHAEL.FROESE
  -- Created : 7/13/2009 2:07:16 PM
  -- Purpose : hold a procedure I can use to debug... throw hook it up and use it to dump something into the debug trace

  subtype udt_Id is api.Pkg_Definition.udt_Id;

  procedure ThrowMessage(
    a_ObjectId udt_Id,
    a_AsOfDate date,
    a_DebugMessage varchar2
  );

end pkg_ER_DebugAssistant;

 
/

create or replace package body           pkg_ER_DebugAssistant is

  procedure ThrowMessage(
    a_ObjectId udt_Id,
    a_AsOfDate date,
    a_DebugMessage varchar2
  ) is
  begin
    pkg_Debug.Putline( a_DebugMessage );
  end;

end pkg_ER_DebugAssistant;

/

