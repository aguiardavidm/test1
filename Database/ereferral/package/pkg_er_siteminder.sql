create or replace package           pkg_ER_SiteMinder is

  subtype udt_id is api.pkg_definition.udt_id;
  subtype udt_IdList is api.pkg_definition.udt_IDList;

  /*----------------------------------------------------------------------------------------
    Purpose:  To create/maintain ER IDIR Users and their security.
    this procedure is called on constructor of the SiteMinder ER Request (o_ER_SiteMinderERRequest)
    the SiteMinder ER Request is created by the aspx pages on the External Site every time
    a user creates a new session.
    The procedure looks for any agency representative records that match the
    BCeID username where there is no userid. If it finds such a case, the relationship between
    the user and the rep is made. It also ensures that the user record has recipient access.
  ----------------------------------------------------------------------------------------*/
  procedure MaintainSiteMinderERClient (
    a_ObjectId udt_id,
    a_AsOfDate date
  );

end pkg_ER_SiteMinder;

 
/

create or replace package body           pkg_ER_SiteMinder is

  procedure MaintainSiteMinderERClient (
    a_ObjectId udt_id,
    a_AsOfDate date
  ) is
    t_BCeIDUserName varchar2(100);
    t_ObjectIds     udt_IdList;
    t_EndPointId    udt_Id;
    t_RelId         udt_Id;
    t_UserId        udt_Id;
    t_Count         pls_Integer;
  begin
    t_BCeIDUserName := api.pkg_ColumnQuery.Value( a_ObjectId, 'BCeIDUserName' );

    if t_BCeIDUserName is not null  then
      t_ObjectIds := api.pkg_SimpleSearch.ObjectsByIndex( 'o_ER_ReferralAgencyRep', 'BCeIDUserName', t_BCeIDUserName );
      for c in 1 .. t_ObjectIds.Count loop
        if api.pkg_ColumnQuery.IsNull( t_ObjectIds(c), 'UserId' ) then
          --relate user to agency rep
          t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName( 'o_ER_ReferralAgencyRep', 'User' );
          t_UserId := api.pkg_ColumnQuery.NumericValue( a_ObjectId, 'UserId' );
          t_RelId := api.pkg_RelationshipUpdate.New( t_EndPointId, t_ObjectIds(c), t_UserId );
          --check that the user has the right access group for ereferral
          if not pkg_ER_ReferralRequest.IsInAccessGroup( t_UserId, pkg_ER_ReferralRequest.g_ERRecipientAccessGroupId ) then
            api.pkg_UserUpdate.AddToAccessGroup( t_UserId, pkg_ER_ReferralRequest.g_ERRecipientAccessGroupId );
          end if;
        end if;
      end loop;
    end if;
    -- get rid of the calling object
    api.pkg_ObjectUpdate.Remove(a_ObjectId);
  end MaintainSiteMinderERClient;

end pkg_ER_SiteMinder;

/

