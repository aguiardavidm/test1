create or replace package PKG_ER_SendEmail is

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  -----------------------------------------------------------------------------
  -- IsProd()
  -----------------------------------------------------------------------------
  function IsProd(
    a_Body varchar2
  )
  return varchar2;

  -----------------------------------------------------------------------------
  -- SendEmailReferral()
  -----------------------------------------------------------------------------
  procedure SendEmailReferral (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_CcAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_BodyColName                       varchar2,
    a_DocumentEndPointName              varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  );

  -----------------------------------------------------------------------------
  -- SendEmailReminderOrExtension()
  -----------------------------------------------------------------------------

  procedure SendEmailReminderOrExtension (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- SendEmailSummaryNotif()
  -----------------------------------------------------------------------------
  procedure SendEmailSummaryNotif (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_CcAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_BodyColName                       varchar2,
    a_DocumentEndPointName              varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  );

  -----------------------------------------------------------------------------
  -- SendEmailUserRegistration()
  -----------------------------------------------------------------------------
  procedure SendEmailUserRegistration (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_BodyIsHtml                        varchar2 default 'Y'
  );

  /*--------------------------------------------------------------------------
   * InsertLink() -- PUBLIC
   * Inserts a link for text enclosed in [[ ]] and preserves the text.
   *------------------------------------------------------------------------*/
  function InsertLink (
    a_Body varchar2,
    a_Link varchar2
  )return varchar2;

end PKG_ER_SendEmail;

 
/

create or replace package body PKG_ER_SendEmail is

   subtype udt_ObjectList is api.udt_objectlist;

  -- Globals
  g_IsProd varchar2(1) := extension.Pkg_Sendmail.GetIsProd;

  -----------------------------------------------------------------------------
  -- IsProd()
  -----------------------------------------------------------------------------
  function IsProd(
    a_Body varchar2
  )
  return varchar2 is
    t_body varchar2(32500) := a_Body;
  begin

    if g_IsProd = 'N' then 
      if nvl(Length(t_Body),0) < 32500 then --leave 200 for this html.
        t_body := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This test email is from a non-production system.</span><br><br>' || t_Body;
      else
        t_body := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This test email is from a non-production system.<br><br>Message too long... truncated.</span><br>';
      end if;
    end if;

  return t_body;

  end IsProd;

  -----------------------------------------------------------------------------
  -- SendEmailReferral()
  -----------------------------------------------------------------------------
  procedure SendEmailReferral (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_CcAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_BodyColName                       varchar2,
    a_DocumentEndPointName              varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  ) is
    t_Body                   varchar2(15000) := api.pkg_columnquery.Value(a_ObjectId, 'BodyEmail');
    t_EmailParagraph         varchar2(5000) := api.pkg_columnquery.Value(a_ObjectId, 'LastEmailParagraph');
    t_IntLink                           varchar2(1000) := api.pkg_columnquery.Value(a_ObjectId, 'InternalLink');
    t_ExtLink                           varchar2(1000) := api.pkg_columnquery.Value(a_ObjectId, 'ExternalLink');
    t_GuestLink                           varchar2(1000) := api.pkg_columnquery.Value(a_ObjectId, 'WebGuestLink');
    t_System                           varchar2(10) := api.pkg_columnquery.Value(a_ObjectId, 'SystemForEmail');

  begin

    /*Make sure To is not empty*/
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'EmailToListExp') is null
    then
       return;
    end if;
    if api.pkg_columnquery.Value(a_ObjectId, 'TriggerEmail') = 'Y' then

    -- Replacing substitutions.
    t_IntLink := replace(t_IntLink, '__ObjectId__', a_ObjectId);
    t_ExtLink := replace(t_ExtLink, '__ObjectId__', a_ObjectId);
    t_GuestLink := replace(t_GuestLink, '__ObjectId__', a_ObjectId);
    t_GuestLink := replace(t_GuestLink, '__AuthorizationKey__', api.pkg_ColumnQuery.Value(a_ObjectId, 'AuthorizationKey'));
    t_EmailParagraph := replace(t_EmailParagraph, '__IntLink__', t_IntLink);
    t_EmailParagraph := replace(t_EmailParagraph, '__ExtLink__', t_ExtLink);
    t_EmailParagraph := replace(t_EmailParagraph, '__GuestLink__', t_GuestLink);

    t_body := replace(t_body, '__Recipient__', api.pkg_ColumnQuery.Value(a_ObjectId, 'RecipientOrganizationEmail'));
    t_body := replace(t_body, '__RecipientList__', api.pkg_ColumnQuery.Value(a_ObjectId, 'RecipientListEmail'));
    t_body := replace(t_body, '__ReferralType__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralType'));
    t_body := replace(t_body, '__ReferenceNumber__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferenceNumber'));
    t_body := replace(t_body, '__ReferralNumber__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralNumber'));
    t_body := replace(t_body, '__RequestNumber__', api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestNumber'));
    t_body := replace(t_body, '__SentDate__', api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestSentDateEmail'));
    t_body := replace(t_body, '__DueDate__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ResponseDueDateExp'));
    t_body := replace(t_body, '__ReferralTypeText__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralTextHTML'));
    t_body := replace(t_body, '__ReferralDescription__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralDescription'));
    t_body := replace(t_body, '__EmailParagraph__', t_EmailParagraph);
    t_body := replace(t_body, '__ContactInformation__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ContactInformation'));
    t_body := replace(t_body, '__ResponseOwner__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ResponseOwner'));
    t_body := replace(t_body, '__ReferralCenter__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralCenter'));
    t_body := replace(t_body, '__UserPositionTitle__', api.pkg_ColumnQuery.Value(a_ObjectId, 'UserPositionTitle'));
    t_body := replace(t_body, '__ReferralCenterAddress__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralCenterAddressHTML'));

    --Insert a Link "Manually"
    If t_System = 'Internal' then
      t_Body := InsertLink(t_Body, t_IntLink);
    elsif t_System = 'External' then
      t_Body := InsertLink(t_Body, t_ExtLink);
    elsif t_System = 'Guest' then
      t_Body := InsertLink(t_Body, t_GuestLink);
    end if;

    -- Append a warning to the beginning of the body if this is a non-production environment.
    t_body := IsProd(t_body);

      -- Sending the email.
      extension.pkg_sendmail.SendPosseEmail(a_ObjectId , a_AsOfDate, a_FromAddressColName
          ,a_ToAddressColName, a_CcAddressColName, '', a_SubjectColName,
          t_body, '', a_DocumentEndPointName, a_BodyIsHtml);

    end if;

  end SendEmailReferral;

  procedure SendEmailReminderOrExtension (
    a_ObjectId                       udt_Id,
    a_AsOfDate                       date
  ) is
    t_Objects udt_ObjectList;
    t_UnfilteredObjects              udt_ObjectList;
    t_JobId   udt_Id;
    t_RRId    udt_Id;
  begin

      t_Objects := api.udt_ObjectList();
      t_UnfilteredObjects := api.udt_ObjectList();
      t_JobId := api.pkg_columnquery.Value(a_ObjectId, 'JobId');

      extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_ReferralRequest');
      extension.pkg_CxProceduralSearch.SearchBySystemColumn( 'JobId', t_JobId, t_JobId, false);
      extension.pkg_CxProceduralSearch.PerformSearch( t_UnfilteredObjects, 'and');
      
      for i in 1 .. t_UnfilteredObjects.Count loop
          if api.pkg_ColumnQuery.Value(t_UnfilteredObjects(i).ObjectId, 'ResponseClosed') = 'N'
            and api.pkg_ColumnQuery.Value(t_UnfilteredObjects(i).ObjectId, 'NotExtended') = 'N'
            and api.pkg_ColumnQuery.Value(t_UnfilteredObjects(i).ObjectId, 'RequestMethodEmail') = 'Y' then
            t_Objects.Extend();
            t_Objects(t_Objects.Last) := t_UnfilteredObjects(i);
          end if;
      end loop;
      for i in 1 .. t_UnfilteredObjects.Count loop
        api.Pkg_Columnupdate.RemoveValue(t_UnfilteredObjects(i).ObjectId, 'NotExtended');
      end loop;  
      if t_Objects.count = 0
      then
        null;
      else
        /*Set the email type as a reminder email or an extension one*/
        if api.pkg_columnquery.Value(a_ObjectId,'ObjectDefName') = 'p_ER_SendReminder'
        then
            api.Pkg_Columnupdate.SetValue( t_JobId, 'ReminderOrExtension', '* Reminder' );
            /*Send reminder email to all the recipients*/
            for i in 1..t_Objects.count
            loop
            -- sending the reminder email by setting TriggerEmail to Yes.
            api.Pkg_Columnupdate.SetValue( t_Objects(i).objectid, 'TriggerEmail', 'Y' );
            end loop;
        end if;
        if api.pkg_columnquery.Value(a_ObjectId,'ObjectDefName') = 'p_ER_ExtendReferralPeriod'
        then
            api.Pkg_Columnupdate.SetValue( t_JobId, 'ReminderOrExtension', '* Extension' );
            begin
              select RRId
                into t_RRId
                from query.r_ER_Extensions
               where ERId = a_ObjectId;
               /*Send extension email to only the related recipient*/
               api.Pkg_Columnupdate.SetValue( t_RRId, 'TriggerEmail', 'Y' );
            Exception
              when TOO_MANY_ROWS then
                  /*Send extension email to all the recipients*/
                  for i in 1..t_Objects.count
                  loop
                  -- sending the reminder email by setting TriggerEmail to Yes.
                  api.Pkg_Columnupdate.SetValue( t_Objects(i).objectid, 'TriggerEmail', 'Y' );

                  end loop;
            end;
        end if;

        /*Empty the detail in case it is getting sent from a diferent place.*/
        api.Pkg_Columnupdate.SetValue( t_JobId, 'ReminderOrExtension', '' );
      end if;

    return;
  end;

  procedure SendEmailSummaryNotif (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_CcAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_BodyColName                       varchar2,
    a_DocumentEndPointName              varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  ) is
    t_body                   varchar2(15000) := api.pkg_columnquery.Value(a_ObjectId, 'BodyEmail');
    t_EmailParagraph         varchar2(5000) := api.pkg_columnquery.Value(a_ObjectId, 'EmailParagraph');
    t_IntLink                           varchar2(1000) := api.pkg_columnquery.Value(a_ObjectId, 'InternalLink');
    t_ExtLink                           varchar2(1000) := api.pkg_columnquery.Value(a_ObjectId, 'ExternalLink');
    t_GuestLink                           varchar2(1000) := api.pkg_columnquery.Value(a_ObjectId, 'WebGuestLink');
    t_System                           varchar2(10) := api.pkg_columnquery.Value(a_ObjectId, 'SystemForEmail');
    t_RequestObjectId        udt_Id;
    t_AuthKey                varchar2(20);

  begin

    /*Make sure To is not empty*/
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'EmailToListExp') is null
    then
       return;
    end if;

    /*This is to get the request object id to use in the link sent to the user via email */
    begin
        select RequestProcessId
          into t_RequestObjectId
          from query.r_ReferralRequestSummaryNotif
         where NotificationProcessId = a_ObjectId;
        exception
           when NO_DATA_FOUND then
             return;
        end;

        t_AuthKey := api.pkg_ColumnQuery.Value(a_ObjectId, 'AuthorizationKey');
        if t_AuthKey is null then
          t_AuthKey := dbms_random.string('U', 8 );
          api.pkg_columnupdate.SetValue(a_ObjectId, 'AuthorizationKey', t_AuthKey);
        end if;

    -- Replacing substitutions.
    t_IntLink := replace(t_IntLink, '__ObjectId__', a_ObjectId);
    t_ExtLink := replace(t_ExtLink, '__ObjectId__', a_ObjectId);
    t_GuestLink := replace(t_GuestLink, '__ObjectId__', a_ObjectId);
    t_GuestLink := replace(t_GuestLink, '__AuthorizationKey__', api.pkg_ColumnQuery.Value(a_ObjectId, 'AuthorizationKey'));
    t_EmailParagraph := replace(t_EmailParagraph, '__IntLink__', t_IntLink);
    t_EmailParagraph := replace(t_EmailParagraph, '__ExtLink__', t_ExtLink);
    t_EmailParagraph := replace(t_EmailParagraph, '__GuestLink__', t_GuestLink);

    t_body := replace(t_body, '__Recipient__', api.pkg_ColumnQuery.Value(a_ObjectId, 'RecipientOrganization'));
    t_body := replace(t_body, '__RecipientList__', api.pkg_ColumnQuery.Value(a_ObjectId, 'RecipientListEmail'));
    t_body := replace(t_body, '__ReferralType__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralType'));
    t_body := replace(t_body, '__ReferenceNumber__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferenceNumber'));
    t_body := replace(t_body, '__ReferralNumber__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralNumber'));
    t_body := replace(t_body, '__RequestNumber__', api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestNumber'));
    t_body := replace(t_body, '__RequestSentDate__', api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestSentDateEmail'));
    t_body := replace(t_body, '__DueDate__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ResponseDueDateExp'));
    t_body := replace(t_body, '__ReferralSummary__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralSummary'));
    t_body := replace(t_body, '__EmailParagraph__', t_EmailParagraph);
    t_body := replace(t_body, '__ContactInformation__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ContactInformation'));
    t_body := replace(t_body, '__ResponseOwner__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ResponseOwner'));
    t_body := replace(t_body, '__ReferralCenter__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralCenter'));
    t_body := replace(t_body, '__UserPositionTitle__', api.pkg_ColumnQuery.Value(a_ObjectId, 'UserPositionTitle'));
    t_body := replace(t_body, '__ReferralCenterAddress__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralCenterAddressHTML'));

    --Insert a Link "Manually"
    If t_System = 'Internal' then
      t_Body := InsertLink(t_Body, t_IntLink);
    elsif t_System = 'External' then
      t_Body := InsertLink(t_Body, t_ExtLink);
    elsif t_System = 'Guest' then
      t_Body := InsertLink(t_Body, t_GuestLink);
    end if;

    -- Append a warning to the beginning of the body if this is a non-production environment.
    t_body := IsProd(t_body);

    -- Sending the email.
    extension.pkg_sendmail.SendPosseEmail(a_ObjectId , a_AsOfDate, a_FromAddressColName
        ,a_ToAddressColName, a_CcAddressColName, '', a_SubjectColName,
        t_Body, '', a_DocumentEndPointName, a_BodyIsHtml);

  end SendEmailSummaryNotif;

  -----------------------------------------------------------------------------
  -- SendEmailUserRegistration()
  -----------------------------------------------------------------------------
  procedure SendEmailUserRegistration (
    a_ObjectId                          udt_Id, 
    a_AsOfDate                          date, 
    a_BodyIsHtml                        varchar2 default 'Y'
  ) is
    t_Body                   varchar2(10000) := api.pkg_columnquery.Value(a_ObjectId, 'BodyEmail');
    t_EmailParagraph         varchar2(5000) := api.pkg_columnquery.Value(a_ObjectId, 'EmailParagraph');
    t_FromAddress            varchar2(200) := api.pkg_columnquery.Value(a_ObjectId, 'EmailFrom');
    t_ToAddress                         varchar2(4000) := api.pkg_columnquery.Value(a_ObjectId, 'Email');
    t_CcAddress                         varchar2(4000) := api.pkg_columnquery.Value(a_ObjectId, 'EmailCCList');
    t_Subject                           varchar2(200) := api.pkg_columnquery.Value(a_ObjectId, 'SubjectEmail');
    t_IntLink                           varchar2(1000) := api.pkg_columnquery.Value(a_ObjectId, 'InternalRegistrationLink');
    t_ExtLink                           varchar2(1000) := api.pkg_columnquery.Value(a_ObjectId, 'ExternalRegistrationLink');
    t_AuthKey                varchar2(20);
    t_System                           varchar2(20) := api.pkg_columnquery.Value(a_ObjectId, 'AgencyType');
  begin

    /*Make sure To is not empty*/
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'Email') is null 
    then
        return;
    end if;

    t_AuthKey := to_char(sysdate, 'YYYYMMDD') || dbms_random.string('U', 8 );
    api.pkg_columnupdate.SetValue(a_ObjectId, 'AuthorizationKey', t_AuthKey);

    -- Replacing substitutions.
    t_IntLink := replace(t_IntLink, '__ObjectId__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectId'));
    t_ExtLink := replace(t_ExtLink, '__ObjectId__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectId'));
    t_IntLink := replace(t_IntLink, '__AuthorizationKey__', api.pkg_ColumnQuery.Value(a_ObjectId, 'AuthorizationKey'));
    t_ExtLink := replace(t_ExtLink, '__AuthorizationKey__', api.pkg_ColumnQuery.Value(a_ObjectId, 'AuthorizationKey'));
    t_EmailParagraph := replace(t_EmailParagraph, '__IntLink__', t_IntLink);
    t_EmailParagraph := replace(t_EmailParagraph, '__ExtLink__', t_ExtLink);
    t_body := replace(t_body, '__Name__', api.pkg_ColumnQuery.Value(a_ObjectId, 'Name'));
    t_body := replace(t_body, '__EmailParagraph__', t_EmailParagraph);
    t_body := replace(t_body, '__Agency__', api.pkg_ColumnQuery.Value(a_ObjectId, 'ReferralAgency'));
    t_body := replace(t_body, '__UserOrRepName__', api.pkg_ColumnQuery.Value(a_ObjectId, 'UserOrRepName'));
    t_body := replace(t_body, '__UserPositionTitle__', api.pkg_ColumnQuery.Value(a_ObjectId, 'UserPositionTitle'));
    t_body := replace(t_body, '__AgencyOrReferralCenter__', api.pkg_ColumnQuery.Value(a_ObjectId, 'AgencyOrReferralCenter'));
    t_body := replace(t_body, '__Address__', api.pkg_ColumnQuery.Value(a_ObjectId, 'UserOrRepAddress'));
    t_body := replace(t_body, '__Email__', api.pkg_ColumnQuery.Value(a_ObjectId, 'UserOrRepEmail'));
    t_body := replace(t_body, '__Phone__', api.pkg_ColumnQuery.Value(a_ObjectId, 'FormattedPhone'));

    --Insert a Link "Manually"
  if t_System in ('Internal Agency','Referral Center') then
      t_Body := InsertLink(t_Body, t_IntLink);
  elsif t_System = 'External Agency' then
      t_Body := InsertLink(t_Body, t_ExtLink);
  end if;

    -- Append a warning to the beginning of the body if this is a non-production environment.
    t_body := IsProd(t_body);

    -- Sending the email.  
    extension.pkg_sendmail.SendEmail(a_ObjectId, t_FromAddress, t_ToAddress, t_CcAddress, null, t_Subject, 
        t_Body, null, null, a_BodyIsHtml);
      
    api.pkg_columnupdate.SetValue(a_ObjectId, 'EmailSentDate', sysdate);

  end SendEmailUserRegistration;

  /*--------------------------------------------------------------------------
   * InsertLink() -- PUBLIC
   * Inserts a link for text enclosed in [[ ]] and preserves the text.
   *------------------------------------------------------------------------*/
  function InsertLink (
    a_Body varchar2,
    a_Link varchar2
  )
  return varchar2 is
    t_TextBeforeLink       varchar2(32767);
    t_TextAfterLink        varchar2(32767);
    t_LinkText             varchar2(32767);
    t_Final                varchar2(32767);
  begin
    if instr(a_Body, '[[') > 0 and instr(a_Body, ']]') > 0 then
      t_TextBeforeLink := substr(a_Body, 1, instr(a_Body, '[[') - 1);
      t_TextAfterLink := substr(a_Body, instr(a_Body, ']]') + 2);
      t_LinkText := substr(a_Body, instr(a_Body, '[[') + 2, instr(a_Body, ']]') - instr(a_Body, '[[') - 2);
      t_Final := t_TextBeforeLink || '<a href="' || a_Link || '">' || t_LinkText || '</a>' || t_TextAfterLink;
    return t_Final;
  end if;
  return a_Body;

  end InsertLink;

end PKG_ER_SendEmail;

/

