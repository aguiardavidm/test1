create or replace package pkg_ER_SearchProcedures is

  -- Public type declarations
  /*------------------------------------------------------------------------*
  * Types declaration
  *------------------------------------------------------------------------*/
  subtype udt_ID is api.pkg_Definition.udt_ID;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_Outcome is api.pkg_Definition.udt_Outcome; 
  subtype udt_ObjectTable is extension.pkg_collectionutils.udt_ObjectTable;
  
  procedure ExternalUserSearch(
    a_FirstName varchar2,
    a_LastName  varchar2,
    a_Active    varchar2,
    a_UserType  varchar2,
    a_Objects out nocopy api.udt_ObjectList
    );

  procedure InternalUserSearch(
    a_FirstName varchar2,
    a_LastName  varchar2,
    a_Active    varchar2,
    a_UserType  varchar2,
    a_Objects out nocopy api.udt_ObjectList
    );

  procedure UserSearch(
    a_FirstName varchar2,
    a_LastName  varchar2,
    a_Active    varchar2,
    a_UserType  varchar2,
    a_Objects out nocopy api.udt_ObjectList
    );

  procedure PublicDocumentSearch(
    a_DocumentId         number,
    a_ProcessId          number,
    a_AuthorizationKey   varchar2,
    a_Objects out nocopy api.udt_ObjectList
    );

  procedure ReferralSearch(
    a_ReferralCenter varchar2,
    a_ReferralNumber  varchar2,
    a_ReferenceNumber  varchar2,
    a_ReferralType  varchar2,
    a_FromCreatedDate  date,
    a_ToCreatedDate  date,
    a_Status  varchar2,
    a_ReferralDescription  varchar2,
    a_Objects out nocopy api.udt_ObjectList
    );

  procedure MyReferralsSearch(
    a_SignedUserId          number,
    a_ReferralCenterObjectId number,
    a_ReferralNumber        number,
    a_RequestNumber         varchar2,        
    a_ReferenceNumber       varchar2,
    a_FromRequestSentDate   date,
    a_ToRequestSentDate     date,
    a_FromDueDate           date,
    a_ToDueDate             date,
    a_OpenOnly              varchar2,
    a_IsNew                 varchar2,
    a_IsRead                varchar2,
    a_AssignedTo            varchar2,
    a_ExternalSearch        varchar2,
    a_ProcessObjects out nocopy api.udt_ObjectList
    );

  procedure ReferralRequestSearch(
    a_ReferralCenter        varchar2,
    a_ReferralNumber        number,
    a_RequestNumber         varchar2,
    a_ReferenceNumber       varchar2,
    a_FromRequestSentDate   date,
    a_ToRequestSentDate     date,
    a_OrganizationName      varchar2,
    a_ReferralLevel         varchar2,
    a_RecipientName         varchar2,
    a_JobStatus             varchar2,
    a_UserId                number,
    a_RestrictByAgency      char,
    a_Objects out nocopy api.udt_ObjectList
    );

-- User Lookup Search
  Procedure UserLookupSearch(
    a_FormattedName        varchar2,
    a_UserType             varchar2,
    a_Objects              out nocopy api.udt_ObjectList);

-- Response Assigned To
  procedure ResponseAssignedToList (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out api.udt_ObjectList
  );

  --from extension.pkg_atissearch@moedev
 Procedure ClauseSearchSelect(a_clausetypecode Varchar2, a_Objects out nocopy api.udt_ObjectList);

-- Summary Assigned To
  procedure SummaryAssignedToList (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,  
    a_SearchString           varchar2,          
    a_Objects                out api.udt_ObjectList
  );

-- Referral Type Classifications List for Drop-Down
  procedure TypeClassificationsList (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,  
    a_SearchString           varchar2,          
    a_Objects                out api.udt_ObjectList
  );

-- For Registered statement for Workload Report
  function WorkloadExtractSearch(
    a_RCObjectId            number,
    a_ReferralNumber        number,
    a_ReferenceNumber         varchar2,
    a_ReferralType          varchar2,
    a_RequestOwner          varchar2,
    a_ResponseOwner         varchar2,
    a_AdminRegion           varchar2,
    a_FromCreatedDate       date,
    a_ToCreatedDate         date,
    a_Status                varchar2,
    a_ReferralDesc          varchar2
  ) return api.udt_ObjectList;

end pkg_ER_SearchProcedures;

 
/

grant execute
on pkg_er_searchprocedures
to posseextensions;

create or replace package body pkg_ER_SearchProcedures is

  procedure ExternalUserSearch(
    a_FirstName varchar2,
    a_LastName  varchar2,
    a_Active    varchar2,
    a_UserType  varchar2,
    a_Objects out nocopy api.udt_ObjectList
    )
  is
  begin
    null;
    a_Objects := api.udt_ObjectList();
    UserSearch(a_FirstName, a_LastName, a_Active, 'Referral', a_Objects);
  end ExternalUserSearch;

  procedure InternalUserSearch(
    a_FirstName varchar2,
    a_LastName  varchar2,
    a_Active    varchar2,
    a_UserType  varchar2,
    a_Objects out nocopy api.udt_ObjectList
    )
  is
  begin
    null;
    a_Objects := api.udt_ObjectList();
    UserSearch(a_FirstName, a_LastName, a_Active, 'Internal', a_Objects);
  end InternalUserSearch;

  procedure UserSearch(
    a_FirstName varchar2,
    a_LastName  varchar2,
    a_Active    varchar2,
    a_UserType  varchar2,
    a_Objects out nocopy api.udt_ObjectList
    )
  is
  begin
    a_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('u_Users');
    extension.pkg_CxProceduralSearch.SearchByIndex('FirstName', a_FirstName, a_FirstName, true);
    extension.pkg_CxProceduralSearch.SearchByIndex('LastName', a_LastName, a_LastName, true);
    extension.pkg_CxProceduralSearch.FilterObjects('UserType', a_UserType, a_UserType, false);
    if a_Active = 'Y' then
      extension.pkg_cxProceduralSearch.FilterObjects('Active', 'Y', 'Y', false);
    end if;
    extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and');
  end UserSearch;

  procedure PublicDocumentSearch(
    a_DocumentId         number,
    a_ProcessId          number,
    a_AuthorizationKey   varchar2,
    a_Objects out nocopy api.udt_ObjectList
    )
  is
    t_AuthorizationKey varchar2(8);
    t_Count            pls_integer;
    t_Docs             api.udt_ObjectList;
    t_Found            boolean default false;
  begin
    a_Objects := api.udt_ObjectList();
    --check that the authorization key and process id match
    select AuthorizationKey 
      into t_AuthorizationKey
      from query.p_ER_ReferralRequest p 
     where p.ObjectId = a_ProcessId;
    if t_AuthorizationKey = a_AuthorizationKey then
      --check if doc is related via a stored rel
      select count(1)
        into t_Count
        from query.r_ReferralRequestAttachment r
       where r.ElectronicDocumentId = a_DocumentId;
      if t_Count = 0 then
        --check if doc is related to the job
        pkg_ER_Utils.DocsByPublishingLevel(a_ProcessId,
          api.pkg_ConfigQuery.EndPointIdForName('p_ER_ReferralRequest', 'DocumentsPROC'),
          t_Docs);
        for c in 1 .. t_Docs.Count loop
          if t_Docs(c).ObjectId = a_DocumentId then
            t_Found := True;
            exit;
          end if;
        end loop;
      else
        t_Found := True;
      end if;
      if t_Found then
        a_Objects.Extend();
        a_Objects(1) := api.udt_object(a_DocumentId);
      end if;
      --check if doc has been linked as an attachment by the recipient
      select count(r.ElectronicDocumentId)
        into t_Count
        from query.r_ReferralRequestRecipientAtt r 
       where r.ReferralRequestProcessId = a_ProcessId
         and r.ElectronicDocumentId = a_DocumentId;
      if t_Count = 1 then
        a_Objects.Extend();
        a_Objects(a_Objects.Count()) := api.udt_object(a_DocumentId);
      end if;
    end if;
  end PublicDocumentSearch;

  procedure ReferralSearch(
    a_ReferralCenter varchar2,
    a_ReferralNumber  varchar2,
    a_ReferenceNumber  varchar2,
    a_ReferralType  varchar2,
    a_FromCreatedDate  date,
    a_ToCreatedDate  date,
    a_Status  varchar2,
    a_ReferralDescription  varchar2,
    a_Objects out nocopy api.udt_ObjectList
    )
  is
    t_ToCreatedDate date;
    t_FromCreatedDate date;
    t_StatusTag varchar2(6);
  begin
    if a_ReferralNumber is null and a_ReferenceNumber is null and a_ReferralType is null and
        a_FromCreatedDate is null and a_ToCreatedDate is null and  
        a_ReferralDescription is null and a_Status = 'All' Then
      raise_application_error( -20000, 'You need to specify at least one search criteria.');
    end if;

    if a_Status != 'All' then
      select Tag
        into t_StatusTag
        from api.Statuses s
       where s.Description = a_Status;
    else
      t_StatusTag := null;
    end if;

    /* Dates in Outrider come in as 0000h of the day selected - add 1 day to the
       incoming dates to include the whole day, not just the 1st second of it */

    if a_FromCreatedDate is null then
       t_FromCreatedDate := to_date('1400-01-01', 'YYYY-MM-DD');
    else
      t_FromCreatedDate := a_FromCreatedDate;
    end if;
    if a_toCreatedDate is null then
       t_ToCreatedDate := to_date(to_char(sysdate, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    else
       t_ToCreatedDate := to_date(to_char(a_ToCreatedDate, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;

    -- Sanity check on Dates
    if t_FromCreatedDate > t_ToCreatedDate  then
      raise_application_error( -20000, 'The end of a date range may not be earlier than the start of a date range');
    end if;

    a_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('j_ER_Referral');
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex( 'ReferralCenter', 'Name', a_ReferralCenter, a_ReferralCenter, false);
    extension.pkg_CxProceduralSearch.SearchBySystemColumn( 'JobId', a_ReferralNumber, a_ReferralNumber, true);
    if a_ReferenceNumber is not null
    then 
      if a_ReferenceNumber != '%'  -- Reference Numbers that are null won't show up if we allow '%'-only searches
      then
        extension.pkg_CxProceduralSearch.SearchByIndex( 'ReferenceNumber', a_ReferenceNumber, a_ReferenceNumber, true);
      end if;
    end if;
    if a_ReferralType is not null
    then 
      if a_ReferralType != '%'
      then
        extension.pkg_CxProceduralSearch.SearchByIndex( 'dup_ReferralType', a_ReferralType, a_ReferralType, true);
      end if;
    end if;
    extension.pkg_CxProceduralSearch.SearchBySystemColumn( 'CreatedDate', t_FromCreatedDate, t_ToCreatedDate);
    if a_Status != 'All' then     
      extension.pkg_cxproceduralsearch.SearchBySystemColumn('JobStatus', t_StatusTag, t_StatusTag, false);
    end if;
    extension.pkg_cxproceduralsearch.SearchByKeywords(a_ReferralDescription);
    extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and');
  end ReferralSearch;

  procedure MyReferralsSearch(
    a_SignedUserId            number,         
    a_ReferralCenterObjectId  number,    
    a_ReferralNumber          number,
    a_RequestNumber           varchar2,    
    a_ReferenceNumber         varchar2,
    a_FromRequestSentDate     date,
    a_ToRequestSentDate       date,
    a_FromDueDate             date,
    a_ToDueDate               date,
    a_OpenOnly                varchar2,
    a_IsNew                   varchar2,
    a_IsRead                  varchar2,
    a_AssignedTo              varchar2,
    a_ExternalSearch          varchar2,
    a_ProcessObjects          out nocopy api.udt_ObjectList
    )
  is
    t_ToRequestSentDate       date;
    t_FromRequestSentDate     date;
    t_ToDueDate               date;
    t_FromDueDate             date;
    t_Count                   pls_integer;
    a_Objects                 api.udt_objectList;
    t_TempObjs                api.Udt_Objectlist;
    t_UserAgencyIds           udt_IdList;
    t_ProcessAgencyId         udt_Id;
    
  begin
    if a_ReferralNumber is null and a_RequestNumber is null and a_ReferenceNumber is null and 
        a_FromRequestSentDate is null and a_ToRequestSentDate is null and a_FromDueDate is null and 
        a_ToDueDate is null and nvl(a_OpenOnly, 'N') = 'N' and nvl(a_IsNew, 'N') = 'N' then
      raise_application_error( -20000, 'You need to specify additional search criteria.');
    end if;  
  
    /* Dates in Outrider come in as 0000h of the day selected - add 1 day to the
       incoming dates to include the whole day, not just the 1st second of it */
    if a_FromRequestSentDate is null and a_ToRequestSentDate is not null then
      t_FromRequestSentDate := to_date('1400-01-01', 'YYYY-MM-DD');
    else
      t_FromRequestSentDate := a_FromRequestSentDate;
    end if;
    if a_ToRequestSentDate is not null then
      t_ToRequestSentDate := to_date(to_char(a_ToRequestSentDate, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;

    if a_FromDueDate is null and a_ToDueDate is not null then
      t_FromDueDate := to_date('1400-01-01', 'YYYY-MM-DD');
    else
      t_FromDueDate := a_FromDueDate;
    end if;
    if a_ToDueDate is not null then
      t_ToDueDate := to_date(to_char(a_ToDueDate, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    
    -- Sanity check on Dates
    if t_FromRequestSentDate > t_ToRequestSentDate then
      raise_application_error(-20000, 'The end of a date range may not be earlier than the start of a date range');
    end if;
    if t_FromDueDate > t_ToDueDate then
      raise_application_error(-20000, 'The end of a date range may not be earlier than the start of a date range');
    end if;
    
    --The external 'My Referrals" serach needs to also include Summary Notification processes.
    t_tempObjs := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_SummaryNotification');
    extension.pkg_CxProceduralSearch.SearchBySystemColumn('JobId', a_ReferralNumber, a_ReferralNumber, true);
    if a_RequestNumber != '%' then  -- Request Numbers that are null won't show up if we allow '%'-only searches
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('ReferralRequest', 'RequestNumber', a_RequestNumber, a_RequestNumber, true);
    end if;
    if a_ReferenceNumber is not null then 
      if a_ReferenceNumber != '%' then -- Reference Numbers that are null won't show up if we allow '%'-only searches
        extension.pkg_CxProceduralSearch.SearchByRelatedIndex('Referral', 'ReferenceNumber', a_ReferenceNumber, a_ReferenceNumber, true);
      end if;
    end if;
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex('ReferralRequest', 'RequestSentDate', t_FromRequestSentDate, t_ToRequestSentDate);
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex('ReferralRequest', 'dup_ResponseDueDate', t_FromDueDate, t_ToDueDate);   
    
    if a_IsNew = 'Y' then
      extension.pkg_CxProceduralSearch.SearchByIndex('dup_StatusIsNew', 1, 1, true);    
    end if;
      
    extension.pkg_CxProceduralSearch.PerformSearch(t_tempObjs, 'and');
    
    a_ProcessObjects := api.udt_ObjectList();
    a_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_ReferralRequest');
    extension.pkg_CxProceduralSearch.SearchBySystemColumn('JobId', a_ReferralNumber, a_ReferralNumber, true);
    if a_RequestNumber != '%' then  -- Request Numbers that are null won't show up if we allow '%'-only searches
      extension.pkg_CxProceduralSearch.SearchByIndex('RequestNumber', a_RequestNumber, a_RequestNumber, true);
    end if;
    if a_ReferenceNumber is not null then 
      if a_ReferenceNumber != '%' then -- Reference Numbers that are null won't show up if we allow '%'-only searches
        extension.pkg_CxProceduralSearch.SearchByRelatedIndex('Referral', 'ReferenceNumber', a_ReferenceNumber, a_ReferenceNumber, true);
      end if;
    end if;
    extension.pkg_CxProceduralSearch.SearchByIndex('RequestSentDate', t_FromRequestSentDate, t_ToRequestSentDate);
    extension.pkg_CxProceduralSearch.SearchByIndex('dup_ResponseDueDate', t_FromDueDate, t_ToDueDate);   
  
    if a_OpenOnly = 'Y' then
      extension.pkg_CxProceduralSearch.SearchByIndex('dup_StatusIsOpen', 1, 1, true);    
    end if;

    extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and');
    if t_TempObjs is not null then
      extension.pkg_collectionutils.Append(a_Objects, t_tempObjs);
      t_tempObjs.Delete();
    end if;
    
    --Null is "My Agencies".  I had to add the external search boolean so this wouldn't mess up the internal search
    -- This procedure is used on both the internal and external searches.
    if a_AssignedTo is null and a_ExternalSearch = 'Y' then 
      for i in 1..a_Objects.Count loop
        select sum(c)
        into t_Count
        from (Select count(*) c
        from query.p_Er_Referralrequest p
         join query.r_er_ReferralReqRefAgency r1
          on r1.ReferralRequestId = p.processid
         join query.r_ER_ReferAgencyRefAgenRepres r2
          on r2.ReferralAgencyObjectId = r1.ReferralAgencyObjectId
         join query.r_ER_ReferAgencyRepresentUser r3
          on r3.ReferralAgencyRepObjectId = r2.ReferralAgencyRepObjectId
          and r3.UserId = a_SignedUserId
        where processid = a_Objects(i).Objectid
         and p.RequestSent = 'Y'
      union all
        select count(*) c
        from query.p_Er_Summarynotification p
         join query.r_er_SummaryNotifRefAgency r1
          on r1.SummaryNotificationProcessId = p.ProcessId
         join query.r_ER_ReferAgencyRefAgenRepres r2
          on r2.ReferralAgencyObjectId = r1.ReferralAgencyObjectId
         join query.r_ER_ReferAgencyRepresentUser r3
          on r3.ReferralAgencyRepObjectId = r2.ReferralAgencyRepObjectId
          and r3.UserId = a_SignedUserId
        where processid = a_Objects(i).Objectid);
        
        if t_Count > 0 then 
            a_ProcessObjects.Extend();
            a_ProcessObjects(a_ProcessObjects.Last) := a_Objects(i);
        end if;
      end loop;         
    
    else 
         
      for i in 1 .. a_Objects.Count loop
        select count(*) 
            into t_Count from api.processassignments pa
           where pa.UserId =  a_SignedUserId
             and pa.ProcessId = a_Objects(i).ObjectId ;
        if t_Count > 0 then     
           a_ProcessObjects.Extend();
           a_ProcessObjects(a_ProcessObjects.Last) := a_Objects(i);  
        end if;     
      end loop;
    
    end if;
    
  end MyReferralsSearch;

  procedure ReferralRequestSearch(
    a_ReferralCenter        varchar2,
    a_ReferralNumber        number,
    a_RequestNumber         varchar2,
    a_ReferenceNumber       varchar2,
    a_FromRequestSentDate   date,
    a_ToRequestSentDate     date,
    a_OrganizationName      varchar2,
    a_ReferralLevel         varchar2,
    a_RecipientName         varchar2,
    a_JobStatus             varchar2,
    a_UserId                number,
    a_RestrictByAgency      char,
    a_Objects               out nocopy api.udt_ObjectList
    )
  is
    t_FromRequestSentDate   date;
    t_ToRequestSentDate     date;

  begin
    if a_ReferralNumber is null and a_RequestNumber is null and a_ReferenceNumber is null and
        a_FromRequestSentDate is null and a_ToRequestSentDate is null and a_OrganizationName is null and
        a_ReferralLevel is null and a_RecipientName is null and (a_JobStatus is null or a_JobStatus = 0) Then
      raise_application_error( -20000, 'Before you go on, you must specify at least one search criterion.');
    end if;
pkg_debug.putsingleline('***pbj:  Referral Center: ' || a_ReferralCenter);
pkg_debug.putsingleline('***pbj:  Referral Number: ' || a_ReferralNumber);
pkg_debug.putsingleline('***pbj:  Request Number: ' || a_RequestNumber);
pkg_debug.putsingleline('***pbj:  Reference Number: ' || a_ReferenceNumber);
pkg_debug.putsingleline('***pbj:  From date: ' || to_char(a_FromRequestSentDate, 'FMYYYY Month dd'));
pkg_debug.putsingleline('***pbj:  To date: ' || to_char(a_FromRequestSentDate, 'FMYYYY Month dd'));
pkg_debug.putsingleline('***pbj:  Organization Name: ' || a_OrganizationName);
pkg_debug.putsingleline('***pbj:  Referral Level: ' || a_ReferralLevel);
pkg_debug.putsingleline('***pbj:  Recipient Name: ' || a_RecipientName);
pkg_debug.putsingleline('***pbj:  Job Status: ' || a_JobStatus);
pkg_debug.putsingleline('***pbj:  UserId: ' || a_UserId);
pkg_debug.putsingleline('***pbj:  Restrict by Agency: ' || a_RestrictByAgency);

    /* Dates in Outrider come in as 0000h of the day selected - add 1 day to the
       incoming dates to include the whole day, not just the 1st second of it */
    if a_FromRequestSentDate is null and a_ToRequestSentDate is not null then
       t_FromRequestSentDate := to_date('1400-01-01', 'YYYY-MM-DD');
    else
      t_FromRequestSentDate := a_FromRequestSentDate;
    end if;
    if a_ToRequestSentDate is not null then
       t_ToRequestSentDate := to_date(to_char(a_ToRequestSentDate, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    --api.pkg_errors.RaiseError(20000, 'TCrrrr___' || a_ReferralCenter);
    -- Sanity check on Dates
    if t_FromRequestSentDate > t_ToRequestSentDate  then
      raise_application_error( -20000, 'The end of a date range may not be earlier than the start of a date range');
    end if;

    a_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_ReferralRequest');
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex('Referral', 'dup_ReferralCenter', a_ReferralCenter, a_ReferralCenter, false);
    extension.pkg_CxProceduralSearch.SearchBySystemColumn('JobId', a_ReferralNumber, a_ReferralNumber, true);
    if a_RequestNumber != '%' then-- Request Numbers that are null won't show up if we allow '%'-only searches
      extension.pkg_CxProceduralSearch.SearchByIndex('RequestNumber', a_RequestNumber, a_RequestNumber, true);
    end if;
    if a_ReferenceNumber is not null then
      if a_ReferenceNumber != '%' then  -- Reference Numbers that are null won't show up if we allow '%'-only searches
        extension.pkg_CxProceduralSearch.SearchByRelatedIndex( 'Referral', 'ReferenceNumber', a_ReferenceNumber, a_ReferenceNumber, true);
      end if;
    end if;
    extension.pkg_CxProceduralSearch.SearchByIndex('RequestSentDate', t_FromRequestSentDate, t_ToRequestSentDate);
    extension.pkg_CxProceduralSearch.SearchByIndex('OrganizationName', a_OrganizationName, a_OrganizationName, true);
    extension.pkg_CxProceduralSearch.SearchByIndex('ReferralLevel', a_ReferralLevel, a_ReferralLevel);
    extension.pkg_CxProceduralSearch.SearchByIndex('RecipientName', a_RecipientName, a_RecipientName, true);
    if a_JobStatus != 0 then
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('Referral', 'StatusId', a_JobStatus, a_JobStatus, true);
    end if;
    extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and');

    if a_RestrictByAgency = 'Y' then
      for i in (
            select ra.ReferralAgencyObjectId AgencyId
              from query.r_ER_ReferAgencyRepresentUser rrep
                join query.r_ER_ReferAgencyRefAgenRepres ra
                  on rrep.ReferralAgencyRepObjectId = ra.ReferralAgencyRepObjectId
              where rrep.UserId = a_UserId) loop
pkg_debug.putsingleline('***pbj:  AgencyId: ' || i.Agencyid);
        extension.pkg_CxProceduralSearch.SearchByRelatedIndex('ReferralAgencyRecipient', 'ObjectId', i.AgencyId, i.AgencyId, false);
      end loop;
      extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'or', 'and');
    end if;
  end ReferralRequestSearch;


  /*--------------------------------------------------------------------------------------
   * User Lookup Search
   *
   * This is a procedural search used to find Users.  Access groups can be specified.
   *---------------------------------------------------------------------------------------*/
  Procedure UserLookupSearch(
    a_FormattedName        varchar2,
    a_UserType             varchar2,
    a_Objects              out nocopy api.udt_ObjectList) is

    t_Objects                api.udt_objectlist;
    t_UserId                 udt_Id;
    t_Users                  udt_IdList;
    t_UserList               udt_IdList;
    t_CriteriaSupplied       boolean := false;
    a_UserName               varchar2(4000);
--    t_TempObjs               api.Udt_Objectlist;
  begin
    a_Objects := api.udt_ObjectList();

    extension.pkg_cxProceduralSearch.InitializeSearch('u_Users');
    extension.pkg_cxProceduralSearch.SearchByIndex('dup_UserNameFormat', a_FormattedName, null, true);

    extension.pkg_cxProceduralSearch.FilterObjects('UserType', a_UserType, a_UserType, false);
    extension.pkg_cxProceduralSearch.PerformSearch(a_Objects, 'and');

  end UserLookupSearch;

  /*---------------------------------------------------------------------------
   * ResponseAssignedTo()
   *-------------------------------------------------------------------------*/
  procedure ResponseAssignedToList (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out api.udt_ObjectList
  ) is
    t_EndPointName           varchar2(30);
    t_Users                  udt_IdList;
    t_AccessGroups           varchar2(40);
  begin

    a_Objects := api.udt_ObjectList();
      select u.UserId
      bulk collect into t_Users
        from query.r_ER_ReferAgencyRefAgenRepres ra
        join query.r_er_ReferralReqRefAgency re
          on ra.ReferralAgencyObjectId = re.ReferralAgencyObjectId
        join query.r_ER_ReferAgencyRepresentUser ru
          on ru.ReferralAgencyRepObjectId = ra.ReferralAgencyRepObjectId
        join api.users u
          on u.UserId = ru.UserId
         and u.Active = 'Y'
         and re.ReferralRequestId = a_ObjectId;

    a_Objects.extend(t_Users.count);
    for i in 1..t_Users.count loop
      a_Objects(a_Objects.count-t_Users.count+i) := api.udt_Object(t_Users(i));
    end loop;
  end;
  
  --from extension.pkg_atissearch@moedev
    Procedure ClauseSearchSelect(a_clausetypecode Varchar2, a_Objects out nocopy api.udt_ObjectList) is

    t_TempList          extension.pkg_CxProceduralSearch.udt_ObjectTable;
    t_UnfilteredObjects api.udt_ObjectList;
    t_clausetypecode    varchar2(4000);

  Begin

    a_Objects           := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    extension.pkg_CxProceduralSearch.InitializeSearch('o_ATIS_DefaultClause');
    extension.pkg_CxProceduralSearch.SearchByIndex('dup_ClauseTypeCode',
                                         a_clausetypecode,
                                         a_clausetypecode,
                                         true);

    extension.pkg_CxProceduralSearch.PerformSearch(t_UnfilteredObjects,
                                         'and',
                                         'or');

    extension.pkg_cxproceduralsearch.ReverseResults(t_UnfilteredObjects);

    a_Objects := t_UnfilteredObjects;

    t_UnfilteredObjects.delete;

  End;
  
   /*---------------------------------------------------------------------------
   * SummaryAssignedTo()
   *-------------------------------------------------------------------------*/
  procedure SummaryAssignedToList (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,    
    a_SearchString           varchar2,     
    a_Objects                out api.udt_ObjectList
  ) is
    t_RequestId               udt_Id;
    t_EndPointName           varchar2(30);
    t_Users                  udt_IdList;
    t_AccessGroups           varchar2(40);
  begin
    t_RequestId := api.pkg_columnquery.Value(a_ObjectId, 'RequestId');
    a_Objects := api.udt_ObjectList();
      select u.UserId
      bulk collect into t_Users
        from query.r_ER_ReferAgencyRefAgenRepres ra
        join query.r_er_ReferralReqRefAgency re
          on ra.ReferralAgencyObjectId = re.ReferralAgencyObjectId
        join query.r_ER_ReferAgencyRepresentUser ru
          on ru.ReferralAgencyRepObjectId = ra.ReferralAgencyRepObjectId
        join api.users u 
          on u.UserId = ru.UserId
         and u.Active = 'Y'
         and re.ReferralRequestId = t_RequestId;

    a_Objects.extend(t_Users.count);
    for i in 1..t_Users.count loop
      a_Objects(a_Objects.count-t_Users.count+i) := api.udt_Object(t_Users(i));
    end loop;
  end;

  procedure TypeClassificationsList (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,    
    a_SearchString           varchar2,     
    a_Objects                out api.udt_ObjectList
  ) is
    t_ReferralCenterId       udt_Id;
    t_Classifications        udt_IdList;
  begin
    t_ReferralCenterId := api.pkg_columnquery.Value(a_ObjectId, 'ReferralCenterObjectId');
    a_Objects := api.udt_ObjectList();

       select r.RefTypeClassId
       bulk collect into t_Classifications
         from query.r_ER_RefTypeClassRefCenter r
        where r.RefCenterId = t_ReferralCenterId;
    a_Objects.extend(t_Classifications.count);
    for i in 1..t_Classifications.count loop
      a_Objects(a_Objects.count-t_Classifications.count+i) := api.udt_Object(t_Classifications(i));
    end loop;
  end TypeClassificationsList;

   /*---------------------------------------------------------------------------
   * Referral Extract Report
   *-------------------------------------------------------------------------*/
  function WorkloadExtractSearch(
    a_RCObjectId            number,
    a_ReferralNumber        number,
    a_ReferenceNumber         varchar2,
    a_ReferralType          varchar2,
    a_RequestOwner          varchar2,
    a_ResponseOwner         varchar2,
    a_AdminRegion           varchar2,
    a_FromCreatedDate       date,
    a_ToCreatedDate         date,
    a_Status                varchar2,
    a_ReferralDesc          varchar2
  ) return api.udt_ObjectList is
    t_FromCreatedDate   date;
    t_ToCreatedDate     date;
    t_StatusTag         varchar2(6);
    t_Objects           api.udt_ObjectList;
  begin
    if a_ReferralNumber is null and a_ReferenceNumber is null and a_ReferralType is null and
        a_RequestOwner is null and a_ResponseOwner is null and 
        a_AdminRegion is null and a_FromCreatedDate is null and a_ToCreatedDate is null and
        a_Status = 'All' and a_ReferralDesc is null then
      raise_application_error( -20000, 'You need to specify at least one search criteria.');
    end if;

    if a_Status != 'All' then
      select Tag
        into t_StatusTag
        from api.Statuses s
       where s.Description = a_Status;
    else
      t_StatusTag := null;
    end if;

    /* Dates in Outrider come in as 0000h of the day selected - add 1 day to the
       incoming dates to include the whole day, not just the 1st second of it */
    if a_FromCreatedDate is null and a_ToCreatedDate is not null then
       t_FromCreatedDate := to_date('1400-01-01', 'YYYY-MM-DD');
    else
      t_FromCreatedDate := a_FromCreatedDate;
    end if;
    if a_ToCreatedDate is not null then
       t_ToCreatedDate := to_date(to_char(a_ToCreatedDate, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    --api.pkg_errors.RaiseError(20000, 'TCrrrr___' || a_ReferralCenter); 
    -- Sanity check on Dates
    if t_FromCreatedDate > t_ToCreatedDate  then
      raise_application_error( -20000, 'The end of a date range may not be earlier than the start of a date range');
    end if;

    t_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('j_ER_Referral');

    extension.pkg_CxProceduralSearch.SearchByIndex('dup_ReferralCenterObjectId', a_RCObjectId, a_RCObjectId);
    extension.pkg_CxProceduralSearch.SearchByIndex('ObjectId', a_ReferralNumber, a_ReferralNumber);
    extension.pkg_CxProceduralSearch.SearchByIndex('ReferenceNumber', a_ReferenceNumber, a_ReferenceNumber);
    extension.pkg_CxProceduralSearch.SearchByIndex('dup_ReferralType', a_ReferralType, a_ReferralType);
    extension.pkg_CxProceduralSearch.SearchByIndex('CreatedDate', t_FromCreatedDate, t_ToCreatedDate);
    extension.pkg_CxProceduralSearch.SearchByIndex('dup_AdminRegionCreatedBy', a_AdminRegion, a_AdminRegion);
    extension.pkg_CxProceduralSearch.SearchByKeywords(a_ReferralDesc);
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex('RequestOwner', 'dup_FormattedName2', a_RequestOwner, a_RequestOwner, false);
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex('ResponseOwner', 'dup_FormattedName2', a_ResponseOwner, a_ResponseOwner, false);
    if a_Status != 'All' then     
      extension.pkg_cxproceduralsearch.SearchBySystemColumn('JobStatus', t_StatusTag, t_StatusTag, false);
    end if;

    extension.pkg_CxProceduralSearch.PerformSearch(t_Objects, 'and');

    return t_Objects;
    t_Objects.Delete;

  end WorkloadExtractSearch;

end pkg_ER_SearchProcedures;

/

