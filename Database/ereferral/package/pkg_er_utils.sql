create or replace package pkg_ER_Utils is

  -- Author  : MICHAEL.FROESE
  -- Created : 6/9/2009 4:35:18 PM
  -- Purpose : EReferral Utilities

  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_ObjectList is api.udt_objectlist;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_DateList is api.pkg_Definition.udt_DateList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  
  procedure CopySupportingObjects (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /* Return Mandatory Referral Requests for a Referral */
  procedure MandatoryReferralRequests (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  );

  /* Return Notification Referral Requests for a Referral */
  procedure NotificationReferralRequests (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  );

  /* Return Optional Referral Requests for a Referral */
  procedure OptionalReferralRequests (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  );

  /* Return Summary Only Referral Requests for a Referral */
  procedure SummaryOnlyReferralRequests (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  );

  /* Remove a Referral Request Process from a new Referral
  */
  procedure KillReferralRequest (
    a_ProcessId           udt_Id,
    a_AsOfDate            date
  );

  /*------------------------------------------------------------------------
   * Where possible, cancel all open processes. However there are some situations
   * where we will have to leave processes open, but just unassigned because of
   * mandatory fields, etc
   *------------------------------------------------------------------------*/
  procedure CancelProcesses (
    a_ProcessId          udt_ID,
    a_AsOfDate          date
  );
  
    /*------------------------------------------------------------------------
   * Duplicate of above, except it only closes ReferralResponse and Referral
   * Request processes
   *------------------------------------------------------------------------*/
  procedure CancelRequestProcesses (
    a_ProcessId          udt_ID,
    a_AsOfDate          date
  );
  procedure CancelResponseProcesses (
    a_ProcessId          udt_ID,
    a_AsOfDate          date
  );  
  
  -- sets ProcessSequence on a process from the Referral Job's Visible Process Sequence
  procedure SetProcessSequence (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure CopyDueDate (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /* From the Extend Referral Period process... after it is completed, adjust
     the ScheduledStartDate to be the latest Due Date on any Request
   */
  procedure AdjManageOverdueMandReq (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure DocsByPublishingLevel (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  );

  /* DocsByPubLevelForEmail()*********************************************************************
   Given a processid (of type p_ER_ReferralRequest) and an endpointid, provide all
   [distribution or summary - depending on the endpointid] documents available
   as email attachments for the referral request recipient.
   
   The docs are filtered based on Publishing Level,
   [Include in Distribution or Include in Summary],
   and As Email Attachment.
  ********************************************************************************************** */
  procedure DocsByPubLevelForEmail (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects               out api.udt_ObjectList
  );

  /* AllDocsForSummary()**************************************************************************
   Given a processid (of type p_ER_ReferralRequest), provide all viewable documents for the
   referral request recipient presentation. This includes original docs related to the referral
   job and new docs uploaded by the referral owner at the Summarize Referral stage.
   [Potentially to include selected Response docs as per issue #2152]

   The docs are filtered based on Publishing Level and Include in Summary.
  ********************************************************************************************** */ 
  procedure AllDocsForSummary (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects               out api.udt_ObjectList
  );


  /* *******************************************************************************************
   Given a processid (of type p_ER_ReferralRequest), provide all viewable documents for
   the referral request recipient presentation. This includes docs related to the referral job
   and docs related to the process. Check the doc's publishing level and include in distribution
   attributes.
  **********************************************************************************************  */
  procedure AllDocsForDistribution (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects               out api.udt_ObjectList
  );

  procedure CloseReferralResponsePV (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure CompleteMyself (
    a_ObjectId udt_Id,
    a_AsOfDate date,
    a_Outcome varchar2
  );

  procedure DoExtend (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*
  runs on summary relationship between referral job and web document to set the
  distribution checkbox to false
  */
  procedure RemoveDistributionDefault (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure AssignApplicationOwner (
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  procedure CreateReferralJob (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure CompleteReferralMonitor (
    a_ObjectId udt_Id,
    a_AsOfDate date);
    
  procedure RelateReviewingReferral (
    a_ObjectId udt_Id,
    a_AsOfDate date);

  /* UpdateEmailParagraph()*********************************************************************
   When the Concluding Paragraph Email text is updated on a Referral Center or when a Link
   is updated on System Parameters, this procedure will re-parse the text and insert the html
   link again, and save it to the 'Final' Detail. This is done so that the text does not need
   to be parsed every time an email is sent. --John P.
  ********************************************************************************************** */
/*  procedure UpdateEmailParagraph (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );*/

  /* ManageOverdueMandCleanup()*********************************************************************
   Upon completion of ManageOverdueMandCleanup... some overdue requests may have been flagged as
   "No Response".  For those, set the ResponseClosed to "Y" so the request will complete itself.
   This is to be run on completion of ManageOverdueMandatories process
  ********************************************************************************************** */
  procedure ManageOverdueMandCleanup (
    a_ObjectId udt_id,
    a_AsOfDate date
  ); 

  /**************************************************************************************************
  *  ValidateUserRepresentitive()
  ***************************************************************************************************/
  procedure ValidateUserRepresentative (
    a_ObjectId udt_id,
    a_AsOfDate date
  );

  /**************************************************************************************************
  *  AddClausesToReferral()
  This checks to see if any temp rels have been made (from the Add Clause button) to StandardClauses,
  and if so copies the text from those to ReferralDescription, and dissolves the relationships again.
  ***************************************************************************************************/
  procedure AddClausesToReferral (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /**************************************************************************************************
  *  CRtoHTMLBreak()
  ***************************************************************************************************/
  function CRtoHTMLBreak(
    Text varchar2
    ) return varchar2;

  procedure CreateRelOnReferralSummary (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );
  
  /**************************************************************************************************
  *  Maintain_dup_Classifications() on ReferralType
  Take care of the situation where the dup does not get updated when only an Indirect Rel changes. -JP
  ***************************************************************************************************/
  procedure Maintain_dup_Classifications (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /**************************************************************************************************
  *  CompleteExtendReferral()
  Needed to close this later, after Request details had been updated. -JP
  ***************************************************************************************************/
  procedure CompleteExtendReferral (
    a_ObjectId                     udt_Id,
    a_AsOfDate                     date
  );

  /**************************************************************************************************
  *  SetInitialAgency when a new Referral Center is created. -JP
  ***************************************************************************************************/
/*  procedure SetInitialAgency (
    a_ObjectId               udt_Id, --ReferralCenter
    a_AsOfDate               date
  );*/

  /**************************************************************************************************
  *  Copy Previous Referral Data - RM
  ***************************************************************************************************/
  procedure CopyPreviousReferralData (
    a_ObjectId               udt_Id, --ReferralCenter
    a_AsOfDate               date
  );
end pkg_ER_Utils;

 
/

create or replace package body pkg_ER_Utils is


  function ReferralLevelRequests (
    a_ObjectId              udt_Id,
    a_ReferralLevel         varchar2
  ) return api.udt_ObjectList is
    t_Objects               api.udt_ObjectList;
  begin
    t_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_ReferralRequest');
    extension.pkg_CxProceduralSearch.SearchBySystemColumn('JobId', a_ObjectId, a_ObjectId, false);
    extension.pkg_CxProceduralSearch.SearchByIndex('ReferralLevel', a_ReferralLevel, a_ReferralLevel, false);
    extension.pkg_CxProceduralSearch.PerformSearch(t_Objects, 'and');

    return t_Objects;
  end;

  procedure MandatoryReferralRequests (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  ) is
  begin
    a_Objects := ReferralLevelRequests(a_ObjectId, 'Mandatory');
  end;

  procedure NotificationReferralRequests (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  ) is
  begin
    a_Objects := ReferralLevelRequests(a_ObjectId, 'Notification');
  end;

  procedure OptionalReferralRequests (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  ) is
  begin
    a_Objects := ReferralLevelRequests(a_ObjectId, 'Optional');
  end;

  procedure SummaryOnlyReferralRequests (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  ) is
  begin
    a_Objects := ReferralLevelRequests(a_ObjectId, 'Summary Only');
  end;

  procedure KillReferralRequest (
    a_ProcessId           udt_Id,
    a_AsOfDate            date
  ) is
    t_ReferralProcessType udt_Id := api.Pkg_Configquery.ObjectDefIdForName('p_ER_ReferralRequest');
    t_ReferralResponseProcessType udt_Id := api.Pkg_Configquery.ObjectDefIdForName('p_ER_ReferralResponse');
    t_ProcessTypeId udt_Id;
  begin
    select processtypeid
      into t_ProcessTypeId
      from api.processes
      where processid = a_ProcessId;
    if t_ReferralProcessType = t_ProcessTypeId or t_ReferralResponseProcessType = t_ProcessTypeId
    then
      api.Pkg_Processupdate.Remove(a_ProcessId );
    else
      api.pkg_errors.RaiseError (-20000, 'Can not remove a process that is not a referral request or referral response');
    end if;
  end KillReferralRequest;

  procedure CancelProcesses (
    a_ProcessId          udt_ID,
    a_AsOfDate          date
  ) IS
    t_JobId                udt_ID;
  begin
    select jobid into t_JobId from api.Processes where ProcessId = a_ProcessId;
    for c in (select p.processid, pt.Name, pt.Description, p.ScheduledStartDate
                from api.processes p
                join api.ProcessTypes pt on pt.ProcessTypeId = p.ProcessTypeId
               where p.jobid = t_JobId
                 and p.datecompleted IS NULL
             ) loop
      begin
        if c.Name = 'p_ER_ReferralRequest' or c.Name = 'p_ER_ReferralResponse' then
          api.pkg_columnupdate.SetValue(c.processid, 'ResponseClosed', 'Y');
        end if;
        api.pkg_ProcessUpdate.Complete(c.processid, 'Cancelled');
      end;
    end loop;
  end;


  procedure CancelRequestProcesses (
    a_ProcessId          udt_ID,
    a_AsOfDate          date
  ) IS
    t_JobId                udt_ID;
  begin
    select jobid into t_JobId from api.Processes where ProcessId = a_ProcessId;
    for c in (select p.processid, pt.Name
                from api.processes p
                join api.ProcessTypes pt on pt.ProcessTypeId = p.ProcessTypeId
               where p.jobid = t_JobId
                 and p.datecompleted IS NULL
             ) 
    loop
      if c.Name = 'p_ER_ReferralRequest' then
        api.pkg_columnupdate.SetValue(c.processid, 'ResponseClosed', 'Y');
        api.pkg_ProcessUpdate.Complete(c.processid, 'Cancelled');
      end if;
      --do nothing to other processes
    end loop;
  end;

  procedure CancelResponseProcesses (
    a_ProcessId          udt_ID,
    a_AsOfDate          date
  ) IS
    t_JobId                udt_ID;
  begin
    select jobid into t_JobId from api.Processes where ProcessId = a_ProcessId;
    for c in (select p.processid, pt.Name
                from api.processes p
                join api.ProcessTypes pt on pt.ProcessTypeId = p.ProcessTypeId
               where p.jobid = t_JobId
                 and p.datecompleted IS NULL
             ) 
    loop
      if c.Name = 'p_ER_ReferralResponse' then
        api.pkg_columnupdate.SetValue(c.processid, 'ResponseClosed', 'Y');
        api.pkg_ProcessUpdate.Complete(c.processid, 'Cancelled');
      end if;
      --do nothing to other processes
    end loop;
  end;

  procedure CopySupportingObjects (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_JobId udt_Id;
    t_SummaryRecDefId udt_Id := api.pkg_configquery.ObjectDefIdForName( 'o_ER_SummaryRecommendation');
    t_AnswerDefId udt_Id := api.pkg_configquery.ObjectDefIdForName( 'o_ER_ReferralAnswer');
    t_ReqRecDefId udt_Id := api.pkg_configquery.ObjectDefIdForName( 'o_ER_RequestRecommendation');
    t_Ids udt_IdList;
    t_ObjectId udt_Id;
    t_RelationshipId udt_Id;
    i pls_integer;
  begin
    select jobid into t_JobId
      from api.processes where processid = a_ObjectID;
    t_Ids := extension.pkg_objectquery.RelatedObjects( t_JobId, 'SRType' );
    for i in 1..t_Ids.count
    loop
      if api.pkg_ColumnQuery.Value( t_Ids(i), 'Active') = 'Y'
      then
        t_ObjectId := api.pkg_objectupdate.New( t_SummaryRecDefId );
        api.pkg_ColumnUpdate.SetValue( t_ObjectId,
                                       'Recommendation',
                                       api.pkg_ColumnQuery.Value( t_Ids(i), 'Recommendation')
                                     );
        api.pkg_ColumnUpdate.SetValue(t_ObjectId,
                                       'Sequence',
                                       api.pkg_ColumnQuery.Value(t_Ids(i), 'Sequence')
                                     );
        t_RelationshipId := extension.pkg_relationshipupdate.New(t_JobId, t_ObjectId, 'SummaryRecommendation');
      end if;
    end loop;

    t_Ids.delete;
    t_Ids := extension.pkg_objectquery.RelatedObjects( t_JobID, 'RQType' );
    for i in 1..t_Ids.count
    loop
      if api.pkg_ColumnQuery.Value( t_Ids(i), 'Active') = 'Y'
      then
        t_ObjectId := api.pkg_objectupdate.New( t_AnswerDefId );
        api.pkg_ColumnUpdate.SetValue( t_ObjectId,
                                       'Sequence',
                                       api.pkg_ColumnQuery.Value( t_Ids(i), 'Sequence')
                                     );
        api.pkg_ColumnUpdate.SetValue( t_ObjectId,
                                       'Question',
                                       api.pkg_ColumnQuery.Value( t_Ids(i), 'Question')
                                     );
        api.pkg_ColumnUpdate.SetValue(t_ObjectId,
                                       'Category',
                                       api.pkg_ColumnQuery.Value(t_Ids(i), 'Category')
                                     );
        t_RelationshipId := extension.pkg_relationshipupdate.New(t_JobID, t_ObjectId, 'ReferralAnswer');

      end if;

    end loop;

    t_Ids.delete;
    t_Ids := extension.pkg_objectquery.RelatedObjects( t_JobID, 'RRType' );
    for i in 1..t_Ids.count
    loop
      if api.pkg_ColumnQuery.Value( t_Ids(i), 'Active') = 'Y'
      then
        t_ObjectId := api.pkg_objectupdate.New( t_ReqRecDefId );
        api.pkg_ColumnUpdate.SetValue( t_ObjectId,
                                       'Recommendation',
                                       api.pkg_ColumnQuery.Value( t_Ids(i), 'Recommendation')
                                     );
        api.pkg_ColumnUpdate.SetValue( t_ObjectId,
                                       'ResponseTextRequired',
                                       api.pkg_ColumnQuery.Value( t_Ids(i), 'ResponseTextRequired')
                                     );
        api.pkg_ColumnUpdate.SetValue(t_ObjectId,
                                       'Sequence',
                                       api.pkg_ColumnQuery.Value(t_Ids(i), 'Sequence')
                                     );
        t_RelationshipId := extension.pkg_relationshipupdate.New(t_JobID, t_ObjectId, 'RequestRecommendation');
      end if;
    end loop;
  end;

  procedure SetProcessSequence (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_Sequence varchar2(10);
    t_JobId udt_Id;
  begin
    select jobid into t_JobId
      from api.processes p where processid = a_ObjectId;
    t_Sequence := api.pkg_ColumnQuery.Value( t_JobId, 'NextProcessNumber');
    api.pkg_ColumnUpdate.SetValue( t_JobId, 'VisibleProcessSequence', to_Number(t_Sequence));
    api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'ProcessSequence', t_Sequence);
  end;


  procedure CopyDueDate (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_RequestId udt_Id;
  begin
    select toobjectid
      into t_RequestId
      from api.relationships r
           join api.relationshipdefs rd
             on rd.relationshipdefid = r.relationshipdefid
             and r.ToEndPoint = rd.ToEndPoint
             and rd.FromEndPointName = 'RR'
      where relationshipid = a_ObjectId;
    api.pkg_ColumnUpdate.SetValue( t_RequestId, 'NewDueDate', api.pkg_ColumnQuery.DateValue( a_ObjectId, 'rel_NewDueDate') );
--    raise_application_error(-20000, t_RequestId);
  end;

  procedure AdjManageOverdueMandReq (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_Id udt_Id;
    t_MaxDueDate date;
  begin
    select api.pkg_ColumnQuery.Value( JobId, 'ManageOverdueProcessId')
      into t_Id
      from api.processes
      where processid = a_ObjectId;

    select max( r.rel_NewDueDate )
      into t_MaxDueDate
      from query.r_er_extensions r
           join api.processes p
             on p.processid = r.rrid
           join api.processes p2
             on p2.jobid = p.jobid
             and p2.processid = a_ObjectId;

    if t_Id is null or t_MaxDueDate is null
    then
      null;
    else
      api.pkg_ColumnUpdate.SetValue( t_Id, 'ScheduledStartDate', t_MaxDueDate);
    end if;
  end;

 procedure DocsByPublishingLevel (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects    out nocopy api.udt_ObjectList
  ) is
    t_JobId udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'JobId');
    t_PublishingLevel varchar2(50) := api.pkg_ColumnQuery.Value(a_ObjectId, 'PublishingLevel');
    t_EPName varchar2(30);
    t_PubLevelDetail varchar2(30);
    t_Docs udt_IdList;
    t_SummDocs udt_IdList;
    i pls_integer;
  begin
    a_Objects := api.udt_ObjectList();
    if t_PublishingLevel is null 
    then
      null;
    else
      if t_PublishingLevel = 'Internal Only' then
        t_EPName := 'InternalAttachment';
        t_PubLevelDetail := 'ERToInternalStaff';
      elsif t_PublishingLevel = 'External Agency' then
        t_EPName := 'ExternalAttachment';
        t_PubLevelDetail := 'ERToExternalAgency';
      elsif t_PublishingLevel = 'Public' then
        t_EPName := 'PublicAttachment';
        t_PubLevelDetail := 'ERToPublic';
  /*    else  -- deletes don't work for unsent Referral Jobs if I have this ... I'm not sure why it is calling this procedural rel
        null;
        --api.pkg_errors.RaiseError(-20000, 'Fatal error... no publishing level set on recipient');*/
      end if;

      t_Docs := extension.pkg_objectquery.RelatedObjects(t_JobId, t_EPName );
      
      for i in 1..t_Docs.count loop
        a_Objects.Extend;
        a_Objects(a_Objects.count) := api.udt_object(t_Docs(i) );
      end loop;
      if a_EndPointId = api.pkg_ConfigQuery.EndPointIdForName('p_ER_ReferralRequest', 'SummaryDocsPROC') then
        t_EPName := 'AttachmentSummary';
        t_SummDocs := extension.pkg_objectquery.RelatedObjects(t_JobId, t_EPName );
        for i in 1..t_SummDocs.count loop
          if api.pkg_ColumnQuery.Value(t_SummDocs(i), t_PubLevelDetail) = 'Y' then
            a_Objects.Extend;
            a_Objects(a_Objects.count) := api.udt_object(t_SummDocs(i) );
          end if;
        end loop;
      end if;
      
    end if;
  end DocsByPublishingLevel;

  procedure DocsByPubLevelForEmail (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects               out api.udt_ObjectList
  ) is
    t_Docs                  api.udt_objectList;
  begin
    a_Objects := api.udt_ObjectList();
    if a_EndPointId = api.pkg_ConfigQuery.EndPointIdForName('p_ER_ReferralRequest', 'DocEmailPublicPROC') then
      AllDocsForDistribution(a_ObjectId, a_EndPointId, t_Docs );
    elsif a_EndPointId = api.pkg_ConfigQuery.EndPointIdForName('p_ER_ReferralRequest', 'SummaryDocsPROC') then
      AllDocsForSummary(a_ObjectId, a_EndPointId, t_Docs );
    else
      null;
    end if;

    for i in 1 .. t_Docs.Count loop
      if api.pkg_ColumnQuery.Value(t_Docs(i).ObjectId, 'IncludeAsEmailAttachment') = 'Y' then
        a_Objects.Extend();
        a_Objects(a_Objects.Last) := t_Docs(i);
      end if;
    end loop;
  end DocsByPubLevelForEmail;

  procedure AllDocsForDistribution (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects               out api.udt_ObjectList
  )
  is
    t_Docs                  api.udt_objectList;
  begin
    a_Objects := api.udt_ObjectList();
    DocsByPublishingLevel( a_ObjectId, a_EndPointId, t_Docs );
    for i in 1 .. t_Docs.Count loop
      if api.pkg_ColumnQuery.Value( t_Docs(i).ObjectId, 'IncludeInDistribution' ) = 'Y' then
        a_Objects.Extend();
        a_Objects(a_Objects.Last) := t_Docs(i);
      end if;
    end loop;
    for c in ( select d.ElectronicDocumentId
                 from query.r_ReferralRequestAttachment d
                where d.ReferralRequestId = a_ObjectId ) loop
      if api.pkg_ColumnQuery.Value( c.ElectronicDocumentId, 'IncludeInDistribution' ) = 'Y' then
        a_Objects.Extend();
        a_Objects(a_Objects.Last) := api.udt_Object( c.ElectronicDocumentId );
      end if;
    end loop;

  end AllDocsForDistribution;

  procedure AllDocsForSummary (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_Objects               out api.udt_ObjectList
  )
  is
    t_Docs                  api.udt_objectList;
  begin
    a_Objects := api.udt_ObjectList();
    DocsByPublishingLevel(a_ObjectId, a_EndPointId, t_Docs );
    for i in 1 .. t_Docs.Count loop
      if api.pkg_ColumnQuery.Value(t_Docs(i).ObjectId, 'IncludeInSummary') = 'Y' then
        a_Objects.Extend();
        a_Objects(a_Objects.Last) := t_Docs(i);
      end if;
    end loop;
  end AllDocsForSummary;

  procedure CloseReferralResponsePV (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
  begin 
    --raise_application_error(-20000, 'here');
    if api.pkg_ColumnQuery.Value( a_ObjectId, 'Outcome') is null
    then
      if api.pkg_ColumnQuery.Value( a_ObjectId, 'ForceCloseMe') = 'Y'
      then
        api.pkg_processupdate.Complete( a_ObjectId, 'Closed');
      elsif api.pkg_ColumnQuery.Value( a_ObjectId, 'CloseMe') = 'Y'
      then
        api.pkg_processupdate.Complete( a_ObjectId, 'Closed');
      end if;
    end if;
  end;

  procedure CompleteMyself (
    a_ObjectId udt_Id,
    a_AsOfDate date,
    a_Outcome varchar2
  ) is
  begin
    --ack timing problems... so do it this way.
    if api.pkg_ColumnQuery.Value( a_ObjectId, 'Outcome') is null
    then
      if api.pkg_columnquery.Value(a_ObjectId,'ObjectDefName') = 'p_ER_ExtendReferralPeriod'
      then
      PKG_ER_SendEmail.SendEmailReminderOrExtension(a_ObjectId,
                               a_AsOfDate);
      end if;
    api.pkg_processupdate.Complete( a_ObjectId, a_Outcome );
    end if;
  end;

  procedure DoExtend (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_JobId udt_Id;
    t_ProcessId udt_Id;
    t_RelId udt_Id;
    t_ExtensionProcDefId udt_Id := api.pkg_configquery.ObjectDefIdForName( 'p_ER_ExtendReferralPeriod');
    t_ExtensionDate date;
    t_OriginalDate date;
  begin
    if api.pkg_ColumnQuery.Value( a_Objectid, 'DoExtend') = 'N'
    then
      null;
    else
      api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'DoExtend', 'N');
      t_ExtensionDate := api.pkg_ColumnQuery.DateValue( a_ObjectId, 'DisplayExtensionDate');
      api.pkg_ColumnUpdate.RemoveValue( a_ObjectId, 'DisplayExtensionDate');
      t_OriginalDate := api.pkg_ColumnQuery.DateValue( a_ObjectId, 'ResponseDueDate');
      if t_ExtensionDate is not null
      then
        select JobId
          into t_JobId
          from api.processes
          where processid = a_ObjectId;

        t_ProcessId := api.pkg_ProcessUpdate.New( t_JobId, t_ExtensionProcDefId, '', sysdate, null, null);
        t_RelId := extension.pkg_relationshipupdate.New( t_ProcessId, a_ObjectId, 'Extendable');
        api.pkg_ColumnUpdate.SetValue( t_RelId, 'rel_NewDueDate', t_ExtensionDate);
        api.pkg_ColumnUpdate.SetValue( t_RelId, 'rel_OriginalDueDate', t_OriginalDate);
      else raise_application_error(-20000, 'no date!');
      end if;
    end if;
  end;

  procedure RemoveDistributionDefault (
    a_ObjectId udt_Id,
    a_AsOfDate date
  )
  is
    t_WebDocId udt_Id;
  begin
    select r.ElectronicDocumentId
      into t_WebDocId
      from query.r_er_ReferralSumAttachment r
     where r.RelationshipId = a_ObjectId;
    api.pkg_ColumnUpdate.SetValue( t_WebDocId, 'IncludeInDistribution', 'N' );
  end RemoveDistributionDefault;

  procedure AssignApplicationOwner (
    a_RelationshipId udt_Id,
    a_AsOfDate date
    )
  is
    t_ReferralJobId                             udt_Id;
    t_UserId                                    udt_Id;
    t_ProcessId                                 udt_Id;

  begin

     begin
      select ReferralJobId,
             UserId
        into t_ReferralJobId, --JobId
             t_UserId         --UserId
        from query.r_er_ReferralRequestOwner r
        where r.RelationshipId = a_RelationshipId;
    exception
    when no_data_found then
      return ;
    end;

     begin
      select p.ProcessId
        into t_ProcessId
        from query.p_ER_PrepareReferral p
       where p.JobId = t_ReferralJobId
         and p.outcome is null;
    exception
    when no_data_found then
      null ;
    end;
    if t_ProcessId is null then
       t_ProcessId := api.pkg_ProcessUpdate.New(t_ReferralJobId,
       api.pkg_ConfigQuery.ObjectDefIdForName('p_ER_PrepareReferral'),'Create Prepare Referral', null, null, null);
       api.pkg_ProcessUpdate.Assign(t_ProcessId, api.pkg_ColumnQuery.Value(t_UserId, 'OracleLogonId'));
    else
        api.pkg_ProcessUpdate.Assign(t_ProcessId, api.pkg_ColumnQuery.Value(t_UserId, 'OracleLogonId'));
    end if;
     null;
  end AssignApplicationOwner;

 /*Not part of BEL original - added by Ryan M.  This procedure creates the Referral job and Process monitor from a business job
   that the referral is being created for*/

  procedure CreateReferralJob (
    a_ObjectId udt_Id,
    a_AsOfDate date
  )
    
  is 
    t_RelId number;
    t_BusJobReferralEndPointId number;
    t_BusinessJobDefName varchar2(50);
    t_ReferralJobTypeId number;
    t_BusinessJobId number;
    t_ReferralJobId number;
    t_BusinessExternalFileNum varchar2(200);
    t_RefRefTypeEndPointId number;
    t_ReferralTypeId number;
    t_ProcRefTypeEndPointId number;
    t_ProcessTypeId number;
    t_UserId number;
    t_ReferralCenterObjectId number;
    t_ReferralRefCentEndPointId number;
    t_ReferralMonitorProcessId number;
    t_ReferralMonitorProcessTypeId number;
    t_ReferralMonitorRefEpId number;
    t_BusDocEpId number;
    t_ElectronicDocDefId number;
    t_ReferralAttachmentEp number;
    t_CreatingProcToReferralEpId number;
    t_UpdatedReferralSeqNum number;
    t_ReferenceNumber varchar(30);
    t_ProcessTypeName varchar(50);
    t_PreviousReferralJobId number;
    t_ReferralPreviousRefEPID number;
    t_PreviousSummaryRecommend varchar(150);
    t_PreviousSumisYDocsEPID number;
    
  begin
   --set Business Job variables
   t_ProcessTypeId := api.pkg_columnquery.Value(a_objectid, 'ObjectDefId');
   t_ProcessTypeName := api.pkg_columnquery.Value(a_objectid, 'ObjectDefName');
   t_BusinessJobid := api.pkg_columnquery.Value(a_ObjectId, 'JobId');
   t_BusinessJobDefName := api.pkg_columnquery.Value(t_BusinessJobid, 'ObjectDefName');
   t_ReferralJobTypeId := api.pkg_configquery.ObjectDefIdForName('j_ER_Referral');
   t_BusinessExternalFileNum := api.pkg_columnquery.Value(t_BusinessJobid, 'ExternalFileNum');
   
   t_ProcRefTypeEndPointId := api.pkg_configquery.EndPointIdForName(t_ProcessTypeId, 'ReferralType');
   
   t_BusJobReferralEndPointId := api.pkg_configquery.EndPointIdForName(t_BusinessJobDefName, 'Referral');
   
   t_RefRefTypeEndPointId := api.pkg_configquery.EndPointIdForName('j_ER_Referral', 'ReferralType');
   
   t_CreatingProcToReferralEpId := api.pkg_configquery.EndPointIdForName(t_ProcessTypeId , 'Referral');
   
   --set Referral Monitor variables
   t_ReferralMonitorProcessTypeId := api.pkg_configquery.ObjectDefIdForName('p_ReferralStatusMonitor');
   t_ReferralMonitorRefEpId := api.pkg_configquery.EndPointIdForName(t_ReferralMonitorProcessTypeId, 'Referral');
   
   --Set Document copying variables
   t_BusDocEpId := api.pkg_configquery.EndPointIdForName(t_BusinessJobDefName, 'Document');
   t_ElectronicDocDefId := api.pkg_configquery.ObjectDefIdForName('d_ElectronicDocument');
   t_ReferralAttachmentEp := api.pkg_configquery.EndPointIdForName(t_ReferralJobTypeId, 'Document');
   
   
   
   --Get User Referral Center and Referral Center Rel endpointid
   t_UserId := api.pkg_securityquery.EffectiveUserId;
   t_ReferralCenterObjectId := api.pkg_columnquery.NumericValue(t_UserId, 'ReferralCenterObjectId');
   t_ReferralRefCentEndPointId := api.pkg_configquery.EndPointIdForName('j_ER_Referral', 'ReferralCenter');
   
   --Select the creating process's Referral Type
   select r.toobjectid into t_ReferralTypeId
    from api.relationships r
    where r.FromObjectId = a_ObjectId
    and r.EndPointId = t_ProcRefTypeEndPointId;
   
   --Create Referral Job
   t_ReferralJobid := api.pkg_jobupdate.New(t_ReferralJobTypeId, null, null);
   
   
   --Relate Business Job to Referral Job
   t_RelId := api.pkg_relationshipupdate.New(t_BusJobReferralEndPointId, t_BusinessJobid, t_ReferralJobId);

   --Select Referral Sequence Number + 1 from Business job.
   t_UpdatedReferralSeqNum := 1 + api.pkg_columnquery.NumericValue(t_BusinessJobId, 'ReferralSeqNum');
   
   --Update Business job's Referral Sequence Number
   api.pkg_columnupdate.SetValue(t_BusinessJobId, 'ReferralSeqNum', t_UpdatedReferralSeqNum);
   
   --Set Referral's Business Job Referral Seq Number to the Updated Sequence Number
   api.pkg_columnupdate.SetValue(t_ReferralJobId, 'BusinessJobReferralSeqNum', t_UpdatedReferralSeqNum);

   --Set Referral Number to the Business Job File number - the updated sequece number (zero padded)
   t_ReferenceNumber := t_BusinessExternalFileNum ||'-'|| to_char(t_UpdatedReferralSeqNum, 'FM09');

   --Set Referral Job Reference Number to Business job External File Number
   api.pkg_columnupdate.SetValue(t_ReferralJobid, 'ReferenceNumber', t_ReferenceNumber);

   --Set Referral Job Business Job ObjectId to Business jobid
   api.pkg_columnupdate.SetValue(t_ReferralJobid, 'BusinessJobObjectId', t_BusinessJobId);
   

   --Relate Referral Job to Referral Type
   t_RelId := api.pkg_relationshipupdate.New(t_RefRefTypeEndPointId, t_ReferralJobid, t_ReferralTypeId);
   
   --Relate Referral Job to Referral Center
   t_RelId := api.pkg_relationshipupdate.New(t_ReferralRefCentEndPointId, t_ReferralJobid, t_ReferralCenterObjectId);

   --Relate Referral Job to Creating Process
   t_RelId := api.pkg_relationshipupdate.New(t_CreatingProcToReferralEpId, a_ObjectId, t_ReferralJobid);
   
   --Create Referral Status Monitor Process (to be later completed by CompleteReferralMonitor)
   t_ReferralMonitorProcessId := api.pkg_processupdate.New(t_BusinessJobId, t_ReferralMonitorProcessTypeId, null, null, null, null);
   
   --Relate Referral Status Monitor Process to the Referral Job
   t_RelId := api.pkg_relationshipupdate.New(t_ReferralMonitorRefEpId, t_ReferralMonitorProcessId, t_ReferralJobId);
   
   --Relate Referral Job to Business Job Documents    
       for c in (select r.ToObjectId DocId
               from api.relationships r
               where r.EndPointId = t_BusDocEpId
               and r.FromObjectId = t_BusinessJobId) loop
       
        t_RelId := api.pkg_relationshipupdate.New(t_ReferralAttachmentEp, t_ReferralJobId, c.docid);
       
       end loop;
   
   --Relate Previous Referral
    if t_ProcessTypeName = 'p_PostReferralReview'
     then 
        
        --Relate Previous Referral
        t_ReferralPreviousRefEPID := api.pkg_configquery.EndPointIdForName('j_ER_Referral', 'PreviousReferral');
     
        select max (r.objectid) into t_PreviousReferralJobId
                         from query.j_er_referral r
                         where r.BusinessJobObjectId = t_BusinessJobId
                         and r.objectid < t_ReferralJobId;
                         
        t_RelId := api.pkg_relationshipupdate.New(t_ReferralPreviousRefEPID, t_ReferralJobId, t_PreviousReferralJobId);
     
        --Set Previous Summary Recommendation
        begin 
          select sr.Recommendation into t_PreviousSummaryRecommend
            from query.r_er_ReferralSumRecommendation rsr
            join query.o_er_summaryrecommendation sr on sr.ObjectId = rsr.SummaryRecommendationObjectId
          where rsr.ReferralJobId = t_PreviousReferralJobId
            and rsr.Checked = 'Y';
        
        exception
          when no_data_found then null;
        
          api.pkg_columnupdate.SetValue(t_ReferralJobId, 'PreviousSummaryRecommendation', t_PreviousSummaryRecommend);
        
        end; 
                
        --Relate Previous Referral Documents (Summary docs are an indirect, but original docs that have
        --IncludeInSummary = Y are related here)
        
        t_PreviousSumisYDocsEPID :=  api.pkg_configquery.EndPointIdForName('j_ER_Referral', 'PrevSumisYDocs');
        
        for c in (select r.ElectronicDocumentId 
                   from query.r_Er_Referralattachmentref R
                   join query.d_electronicdocument e on e.ObjectId = r.ElectronicDocumentId
                   where r.ReferralJobId = t_PreviousReferralJobId
                   and e.IncludeInSummary = 'Y') loop
        
          t_RelId := api.pkg_relationshipupdate.New(t_PreviousSumisYDocsEPID, t_ReferralJobId, c.electronicdocumentid);
        end loop;
             
    else null; 
    end if;

  end CreateReferralJob;


 /*Not part of BEL original - added by Ryan M.  This procedure completes the Process monitor of a business job
   that the referral was created for.  This is to be called with the Referral is moved into closed or cancelled status*/
  procedure CompleteReferralMonitor (
    a_ObjectId udt_Id,
    a_AsOfDate date)
    
    is 
    t_ReferralJobId number;
    t_ReferralMonitorProcessId number;
    t_RefReferralMonitorEpId number;
    t_StatusDescription varchar2(50);
    t_ReferralProcessTypeId number;
    t_BusinessJobId number;
    t_ReferralJobTypeId number;
    t_CurrentMonitorOutcome varchar2(50);
    
    begin
    --get status of referral job
    select s.description into t_StatusDescription
      from api.jobs j
      join api.statuses s on s.StatusId = j.StatusId
      where j.JobId = a_ObjectId;

dbms_output.put_line('*************************');
    
    if t_StatusDescription in ('Closed','Cancelled') then 
    
      --t_ReferralJobId := api.pkg_columnquery.Value (a_ObjectId, 'JobId');
      t_ReferralJobTypeId := api.pkg_columnquery.Value(a_ObjectId, 'ObjectDefId');
      t_BusinessJobId := api.pkg_columnquery.Value(a_ObjectId, 'BusinessJobObjectId');
    
      if t_BusinessJobId is not null --If the Referral was directly created by a business job, then complete the Monitor process
       
       then 
         t_ReferralProcessTypeId := api.pkg_configquery.ObjectDefIdForName('p_ReferralStatusMonitor');
       
         t_RefReferralMonitorEpId := api.pkg_configquery.EndPointIdForName(t_ReferralJobTypeId, 'ReferralStatusMonitor');
       
         --get referral process monitor id based on relationship to referral job
         select r.ToObjectId
           into t_ReferralMonitorProcessId
           from api.relationships r
           where r.EndPointId = t_RefReferralMonitorEpId
           and r.FromObjectId = a_ObjectId;
         
         select p.Outcome into t_CurrentMonitorOutcome
          from api.processes p
          where p.ProcessId = t_ReferralMonitorProcessId;
         
         if t_CurrentMonitorOutcome is null then
            --set outcome of referral process monitor to the status of the referral job
            api.pkg_processupdate.Complete(t_ReferralMonitorProcessId, t_StatusDescription);
         
         else null;
         
         end if;
         
       else null;
       end if; --if Referral Created by business job
    
    end if; --if referral in closed or cancelled status
    
    end CompleteReferralMonitor;


 /*This procedure is used by the Referral Status Monitor to relate the Referral Job to the Post Referral Review Process*/
  procedure RelateReviewingReferral (
    a_ObjectId udt_Id,
    a_AsOfDate date)
    
    is 
    
    t_PostRefReviewId number;
    t_PostRefReviewTypeId number;
    t_RelId number;
    t_ReferralJobId number;
    t_MonitorRefRelEpId number;
    t_CreatedProcRefRelEpId number;
    t_MonitorProcessTypeId number;
    t_ReferralTypeId number;
    t_CreatedProcRefTypeRelEpId number;
    t_ReferralRefTypeRelEpId number;
    
    begin
    
    --Get Monitor Proc Type Id
    select p.ProcessTypeId into t_MonitorProcessTypeId
     from api.processes p
     where p.ProcessId = a_ObjectId;
    
    --Get Monitor to Referral Job EndpointId
    t_MonitorRefRelEpId := api.pkg_configquery.EndPointIdForName(t_MonitorProcessTypeId, 'Referral');
    
    --Get Referral Job Id
    select r.ToObjectId into t_ReferralJobId
     from api.relationships r
     where r.EndPointId = t_MonitorRefRelEpId
     and r.FromObjectId = a_ObjectId;
    
    --Get process created by this logical transaction
    select p.objectid, p.ObjectDefId into t_PostRefReviewId, t_PostRefReviewTypeId
     from query.p_postreferralreview p
     where p.ReferralStatusMonitorId = a_ObjectId;

    --PostRefReview rel to Referral Id
    t_CreatedProcRefRelEpId := api.pkg_configquery.EndPointIdForName(t_PostRefReviewTypeId, 'ReviewingReferral');
    
    --Relate Created process to referral
    t_RelId := api.pkg_relationshipupdate.New(t_CreatedProcRefRelEpId , t_PostRefReviewId, t_ReferralJobId);
    
    --Get Referral Referral Type
    t_ReferralRefTypeRelEpId := api.pkg_configquery.EndPointIdForName('j_ER_Referral', 'ReferralType');
    
    select r.toobjectid into t_ReferralTypeId
     from api.relationships r
     where r.FromObjectId = t_ReferralJobId
     and r.EndPointId = t_ReferralRefTypeRelEpId;
    
    --Get Endpoint Id for Created process to Referral Type rel
    t_CreatedProcRefTypeRelEpId := api.pkg_configquery.EndPointIdForName(t_PostRefReviewTypeId, 'ReferralType');
            
    --Relate Created process to Referral Type
    t_RelId := api.pkg_relationshipupdate.New(t_CreatedProcRefTypeRelEpId , t_PostRefReviewId, t_ReferralTypeId);
    
    end RelateReviewingReferral;

/*  procedure UpdateEmailParagraph (
    a_ObjectId udt_Id,
    a_AsOfDate date
  )
  is
    t_ReferralCenters      udt_IdList;
    t_Link                 varchar2(4000);
    t_TextBeforeLink       varchar2(4000);
    t_TextAfterLink        varchar2(4000);
    t_LinkText             varchar2(4000);
    t_Final                varchar2(4000);
    t_LinkInternal         varchar2(4000);
    t_LinkExternal         varchar2(4000);
    t_LinkGuest            varchar2(4000);
    t_SummLinkInt          varchar2(4000);
    t_SummLinkExt          varchar2(4000);
    t_SummLinkGuest        varchar2(4000);    
    t_NotifLinkInt          varchar2(4000);
    t_NotifLinkExt          varchar2(4000);
    t_NotifLinkGuest        varchar2(4000);
    t_RawParagraph         varchar2(4000);
    t_DetailNames          varchar2(4000) := 'RqstEmailVerbiageInt,RqstEmailVerbiageExt,RqstEmailVerbiageGuest,SummEmailVerbiageInt,SummEmailVerbiageExt,SummEmailVerbiageGuest,NotifEmailVerbiageInt,NotifEmailVerbiageExt,NotifEmailVerbiageGuest';
    t_FDetailNames         varchar2(4000) := 'F_RqstEmailVerbiageInt,F_RqstEmailVerbiageExt,F_RqstEmailVerbiageGuest,F_SummEmailVerbiageInt,F_SummEmailVerbiageExt,F_SummEmailVerbiageGuest,F_NotifEmailVerbiageInt,F_NotifEmailVerbiageExt,F_NotifEmailVerbiageGuest';
    t_DetailList           udt_StringList;
    t_FDetailList          udt_StringList;
  begin
    t_DetailList := extension.pkg_Utils.Split(t_DetailNames, ',');
    t_FDetailList := extension.pkg_Utils.Split(t_FDetailNames, ',');

    select api.pkg_ColumnQuery.Value(o.ObjectId, 'InternalWebsiteBaseURL')||'?PossePresentation=InternalReferralResponse&PosseObjectId={ObjectId}', 
           api.pkg_ColumnQuery.Value(o.ObjectId, 'eReferralWebsiteBaseURL')||'?PossePresentation=ReferralResponse&PosseObjectId={ObjectId}', 
           api.pkg_ColumnQuery.Value(o.ObjectId, 'eReferralWebsiteBaseURL')||'?PossePresentation=EReferralRequest&PosseObjectId={ObjectId}&AuthorizationKey={AuthorizationKey}', 
           api.pkg_ColumnQuery.Value(o.ObjectId, 'InternalWebsiteBaseURL')||'?PossePresentation=InternalReferralSummary&PosseObjectId={ObjectId}', 
           api.pkg_ColumnQuery.Value(o.ObjectId, 'eReferralWebsiteBaseURL')||'?PossePresentation=ReferralSummary&PosseObjectId={ObjectId}', 
           api.pkg_ColumnQuery.Value(o.ObjectId, 'eReferralWebsiteBaseURL')||'?PossePresentation=EReferralSummary&PosseObjectId={ObjectId}&AuthorizationKey={AuthorizationKey}',
           api.pkg_ColumnQuery.Value(o.ObjectId, 'InternalWebsiteBaseURL')||'?PossePresentation=InternalReferralResponse&PosseObjectId={ObjectId}', 
           api.pkg_ColumnQuery.Value(o.ObjectId, 'eReferralWebsiteBaseURL')||'?PossePresentation=ReferralResponse&PosseObjectId={ObjectId}', 
           api.pkg_ColumnQuery.Value(o.ObjectId, 'eReferralWebsiteBaseURL')||'?PossePresentation=EReferralRequest&PosseObjectId={ObjectId}&AuthorizationKey={AuthorizationKey}'

      into t_LinkInternal, 
           t_LinkExternal, 
           t_LinkGuest, 
           t_SummLinkInt, 
           t_SummLinkExt, 
           t_SummLinkGuest,
           t_NotifLinkInt,
           t_NotifLinkExt,
           t_NotifLinkGuest
      from
      api.ObjectDefs od
      join api.Objects o
          on o.ObjectDefTypeId = od.ObjectDefTypeId
          and o.ObjectDefId = od.ObjectDefId
    where od.Name = 'o_SystemSettings';

    select ObjectId
    bulk collect into t_ReferralCenters
    from query.o_ER_ReferralCenter;      

    for i in 1..t_ReferralCenters.Count loop
      for j in 1..t_DetailList.Count loop
        select api.pkg_ColumnQuery.Value(rc.ObjectId, t_DetailList(j))
        into t_RawParagraph
        from query.o_ER_Referralcenter rc
        where rc.ObjectId = t_ReferralCenters(i);
        t_Link :=
        case j
          when 1 then t_LinkInternal
          when 2 then t_LinkExternal
          when 3 then t_LinkGuest
          when 4 then t_SummLinkInt
          when 5 then t_SummLinkExt
          when 6 then t_SummLinkGuest
          when 7 then t_NotifLinkInt
          when 8 then t_NotifLinkExt
          when 9 then t_NotifLinkGuest
        end;
        t_TextBeforeLink := substr(t_RawParagraph, 1, instr(t_RawParagraph, '<<') - 1);
        t_TextAfterLink := substr(t_RawParagraph, instr(t_RawParagraph, '>>') + 2);
        t_LinkText := substr(t_RawParagraph, instr(t_RawParagraph, '<<') + 2, instr(t_RawParagraph, '>>') - instr(t_RawParagraph, '<<') - 2);
        t_Final := t_TextBeforeLink || '<a href="' || t_Link || '">' || t_LinkText || '</a>' || t_TextAfterLink;
        api.pkg_ColumnUpdate.SetValue(t_ReferralCenters(i), t_FDetailList(j), t_Final);
      end loop;
     end loop;

  end UpdateEmailParagraph;*/


  procedure ManageOverdueMandCleanup (
    a_ObjectId udt_id,
    a_AsOfDate date
  ) is 
    t_Ids api.pkg_definition.udt_IdList;  
    i pls_integer;
  begin
    --ensure this process is being called from the correct place
    select o.objectdefid 
      into i
      from api.objects o
      join api.objectdefs od
        on od.objectdeftypeid = o.ObjectDefTypeId
        and od.objectdefid = o.objectdefid
        and od.name = 'p_ER_ManageOverdueMandRequests'
      where o.objectid = a_ObjectId;
    t_Ids := extension.pkg_objectquery.RelatedObjects(a_ObjectId, 'Overdue');
    for i in 1..t_Ids.count 
    loop --through all related overdue referral requests and if the NoResponse 
         --flag is set, but the process remains incomplete, then set the
         --ResponseClosed flag which will trigger the closure of the Referral 
         --Request
      if api.pkg_ColumnQuery.Value(t_Ids(i), 'NoResponse') = 'Y' 
        and api.pkg_ColumnQuery.DateValue(t_Ids(i), 'DateCompleted') is null
      then
        --setting ResponseClosed to 'Y' will trigger process completion.
        api.Pkg_ColumnUpdate.SetValue(t_Ids(i), 'ResponseClosed', 'Y');
      end if;
    end loop;
    exception 
      when no_data_found
      then --null; --ie, this isn't called from p_ER_ManageOverdueMandRequests process
           api.pkg_errors.RaiseError(-20000, 'pkg_ER_Utils.ManageOverdueMandatoriesCleanup is being called from an invalid location');
  end;  

  /**************************************************************************************************
  *  ValidateUserRepresentitive()
  ***************************************************************************************************/
  procedure ValidateUserRepresentative (
    a_ObjectId udt_id, --user rep objectid
    a_AsOfDate date
  ) is
    t_AuthKey             varchar2(20);
    t_ValidateAuthKey     varchar2(20);
    t_AuthKeyExpiryDate   date;
    t_DaysToExpire        number;
    t_UserId              udt_Id;
    t_RelId               udt_Id;
    t_UserBelongsToAgency varchar2(1);
    t_TriggerValidation   varchar2(1);
    t_AgencyObjectId      udt_Id;
    t_UserAgencyIds       udt_IdList;
  begin
    t_AuthKey := api.pkg_columnquery.Value(a_ObjectId, 'AuthorizationKey');
    t_ValidateAuthKey := api.pkg_columnquery.Value(a_ObjectId, 'ValidateAuthorizationKey');
    t_AuthKeyExpiryDate := to_date(substr(t_AuthKey, 1, 8), 'yyyymmdd');
    t_UserId := api.pkg_securityquery.EffectiveUserId;
    t_TriggerValidation := api.pkg_columnquery.Value(a_ObjectId, 'TriggerValidation');
    t_DaysToExpire := 30;
    t_AgencyObjectId := extension.pkg_objectquery.RelatedObject(a_ObjectId, 'ReferralAgency');

    Select a.ObjectId
    bulk collect into t_UserAgencyIds
    From query.u_Users u
     join api.relationships r1
      on r1.FromObjectId = u.ObjectId
      and r1.EndPointId = api.pkg_configquery.EndPointIdForName(u.ObjectDefId, 'ReferralAgencyRepresentative')
     join query.o_ER_ReferralAgencyRep rep
      on r1.ToObjectId = rep.ObjectId
     join api.relationships r2
      on r2.FromObjectId = rep.ObjectId
      and r2.EndPointId = api.pkg_configquery.EndPointIdForName(rep.ObjectDefId, 'ReferralAgency')
     join query.o_er_referralagency a
      on r2.ToObjectId = a.ObjectId
    where u.UserId = t_UserId;

    for i in 1..t_UserAgencyIds.count loop
      if t_UserAgencyIds(i) = t_AgencyObjectId then
        t_UserBelongsToAgency := 'Y';
      end if;
    end loop;

    --TriggerValidation is set on first display of the ERLinkInternalUser presentation (or External)
    if t_TriggerValidation = 'Y' then
      --Need to check if the User is not already attached to an agency.  Need to make sure the Auth Key passed
      -- in the email URL matches what is on the Object and make sure it's not expired.
      if t_UserBelongsToAgency is null and t_AuthKey = t_ValidateAuthKey and
          (trunc(sysdate) - t_AuthKeyExpiryDate) < t_DaysToExpire then
        --If all checks are good, create rel between User and Referral Agency Rep.
        api.pkg_columnupdate.SetValue(a_ObjectId, 'IsUnsuccessful', 'N');
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('o_ER_ReferralAgencyRep', 'User'), a_ObjectId, t_UserId);

      else
        --If there was a problem with one of the checks, set this boolean so the correct sceen will appear.
        api.pkg_columnupdate.SetValue(a_ObjectId, 'IsUnsuccessful', 'Y');
      end if;
      --Regardless, set the Trigger back to 'N'.
      api.pkg_columnupdate.SetValue(a_ObjectId, 'TriggerValidation', 'N');
    end if;
  end ValidateUserRepresentative;

  /**************************************************************************************************
  *  AddClausesToReferral()
  ***************************************************************************************************/
  procedure AddClausesToReferral (
    a_ObjectId udt_Id,
    a_AsOfDate date
  )
  is
    t_Objects              udt_IdList;
    t_Rels                 udt_IdList;
    t_ReferralText         varchar2(4000);
    t_Clause               varchar2(4000);
  begin
    select r.ClauseId, r.RelationshipId
     bulk collect into t_Objects, t_Rels
     from query.r_Er_Referral_Standardclause r
     join query.o_Er_Standardclauses sc
       on r.ClauseId = sc.ObjectId
    where r.ReferralId = a_ObjectId
       order by sc.Sequence, sc.ObjectId;

    if t_Objects.Count = 0 then
      return;
    end if;

    t_ReferralText := api.pkg_columnquery.Value(a_ObjectId, 'ReferralDescription');

    for i in 1..t_Objects.Count loop
      select Clause into t_Clause from query.o_Er_Standardclauses where Objectid = t_Objects(i);

      t_Clause := CRtoHTMLBreak(t_Clause);

      if nvl(length(t_ReferralText),0) + length(t_Clause) < 3995 then    
        if nvl(length(t_ReferralText),0) = 0 then
          t_ReferralText := t_Clause;
        else
          t_ReferralText := t_ReferralText || '<br>' || t_Clause;
        end if;
      end if;
    end loop;

    api.pkg_columnupdate.SetValue(a_ObjectId, 'ReferralDescription', t_ReferralText);

    for i in 1 .. t_Rels.count
    loop
      api.Pkg_RelationshipUpdate.Remove(t_Rels(i));
    end loop;

  end AddClausesToReferral;

  /**************************************************************************************************
  *  CRtoHTMLBreak()
  ***************************************************************************************************/
  function CRtoHTMLBreak(
    Text varchar2
  ) return varchar2 is
    t_pos             udt_NumberList;
    t_Sub             varchar2(4000);
    t_Final           varchar2(4000);
    x number := 1;
    y number := 1;
  begin
    loop
      t_pos(x) := instr(Text, chr(13), 1, x);
      if nvl(t_pos(x),0) = 0 then
        t_Sub := substr(Text, y);
        t_Final := t_Final || t_Sub;
        exit;
      end if;
      t_Sub := substr(Text, y, t_pos(x)-y);
      t_Final := t_Final || t_Sub || '<br />';
      y := t_pos(x);
      x := x + 1;
    end loop;
    return t_Final;
  end CRtoHTMLBreak;
  
  /**************************************************************************************************
  *  CreateRelOnReferralSummary()
  ***************************************************************************************************/
  procedure CreateRelOnReferralSummary (
    a_ObjectId udt_Id, --Summary Notification ProcessId
    a_AsOfDate date
  )
  is
    t_RelId        udt_Id;
    t_AgencyId     udt_Id;
  begin
    --Find the agency objectid related to the referral request process.  We want to create a relationship
    -- to the same one.
    Select rr.ReferralAgencyObjectId
    into t_AgencyId
    From query.p_er_summarynotification s
     join api.relationships r
      on r.FromObjectId = s.ObjectId
      and r.EndPointId = api.pkg_configquery.EndPointIdForName(s.ObjectDefId, 'ReferralRequest')
     join query.p_er_referralrequest rr
      on r.ToObjectId = rr.ObjectId
    Where s.ObjectId = a_ObjectId;

    if t_AgencyId is not null then
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('p_ER_SummaryNotification', 'ReferralAgency'), a_ObjectId, t_AgencyId);    
    end if;
  end CreateRelOnReferralSummary;

  /**************************************************************************************************
  *  Maintain dup_Classifications on ReferralType()
  ***************************************************************************************************/
  procedure Maintain_dup_Classifications (
    a_ObjectId udt_Id,
    a_AsOfDate date
  )
  is
    t_UsersRC  udt_Id := api.pkg_columnquery.value(api.pkg_securityquery.effectiveuserid(), 'ReferralCenterObjectId');
  begin
    for i in (select rt.objectid from query.o_Er_Referraltype rt where rt.ReferralCenterObjectId = t_UsersRC)
    loop
      api.pkg_ColumnUpdate.SetValue(i.objectid, 'dup_Classifications', api.pkg_ColumnQuery.Value(i.objectid, 'Classifications'));
    end loop;
  end Maintain_dup_Classifications;

  /**************************************************************************************************
  *  CompleteExtendReferral()
  ***************************************************************************************************/
  procedure CompleteExtendReferral (
    a_ObjectId                     udt_Id,
    a_AsOfDate                     date
  ) is
    t_MaxERProc                    udt_Id;
    t_JobId                        udt_Id;
    t_RRObjectIds                  udt_IdList;
    t_rel_NewDueDates              udt_DateList;
    t_ResponseDueDates             udt_DateList;
    t_Outcomes                     udt_StringList;
    t_Close                        varchar2(1) := 'Y'; 
  begin 
    select max(r.ERId)
      into t_MaxERProc from query.r_Er_Extensions r 
      join query.p_Er_Referralrequest rr
        on r.RRId = rr.ObjectId where rr.ObjectId = a_ObjectId;
    
    select p.JobId into t_JobId from api.processes p where p.ProcessId = a_ObjectId;
    
    select r.rel_NewDueDate, rr.ObjectId, rr.ResponseDueDate, er.Outcome
      bulk collect into t_rel_NewDueDates, t_RRObjectIds, t_ResponseDueDates, t_Outcomes
      from query.r_Er_Extensions r 
      join query.p_Er_Extendreferralperiod er
        on r.ERId = er.ObjectId
      join query.p_Er_Referralrequest rr
        on r.RRId = rr.ObjectId
      join query.j_Er_Referral j
        on j.ObjectId = rr.JobId where rr.JobId = t_JobId and r.ERId = t_MaxERProc and er.Outcome is null;

    for i in 1 .. t_RRObjectIds.Count
    loop
      if t_rel_NewDueDates(i) is not null then
        if t_rel_NewDueDates(i) <> t_ResponseDueDates(i) then
          t_Close := 'N';
        end if;
      else
        api.pkg_columnupdate.SetValue(t_RRObjectIds(i), 'NotExtended', 'Y');
      end if;
    end loop;
    if t_Close = 'Y' then
      CompleteMyself(t_MaxERProc, sysdate, 'Extended');
    end if;

  end CompleteExtendReferral;

  /**************************************************************************************************
  *  SetInitialAgency
  ***************************************************************************************************/
/*  procedure SetInitialAgency (
    a_ObjectId               udt_Id, --ReferralCenter
    a_AsOfDate               date
  )
  is
  t_Name                     varchar2(100);
  t_NewAgencyId              udt_Id;
  t_RelId                    udt_Id;
  begin
    t_Name := api.pkg_columnquery.Value(a_ObjectId, 'Name');
    t_NewAgencyId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ER_Agency'));
    api.pkg_columnupdate.SetValue(t_NewAgencyId, 'AgencyName', t_Name);
    api.pkg_columnupdate.SetValue(t_NewAgencyId, 'IsReferralCenterAgency', 'Y');
    t_RelId := extension.pkg_relationshipupdate.New(a_ObjectId, t_NewAgencyId, 'Agency');

  end SetInitialAgency;*/

  /**************************************************************************************************
  *  CopyPreviousReferralData
  ***************************************************************************************************/
  procedure CopyPreviousReferralData (
    a_ObjectId               udt_Id, --ReferralCenter
    a_AsOfDate               date
  )
  is
    t_PreviousReferralJobId number;
    t_PrevRunOnce varchar2(1);
    t_EndpointId number;
    t_RelId number;
    t_NumericDetail number;
    t_Detail varchar2(4000);
    t_ProcessTypeId number;
    t_NewProcessId number;
  begin
    t_PreviousReferralJobId := api.pkg_columnquery.Value(a_ObjectId, 'PreviousReferralJobId');
    t_PrevRunOnce := api.pkg_columnquery.Value(a_ObjectId, 'PrevRunOnce');

    if t_PreviousReferralJobId is not null and t_PrevRunOnce = 'N'
      then 
        --Set Referral Details Based on Previous Referral
          --Details
            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'MandatoryResponseDays');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'MandatoryResponseDays', t_Detail);
            t_Detail := null;
            
            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'MandatoryReminderDays');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'MandatoryReminderDays', t_Detail);          
            t_Detail := null;
 
            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'MinimumOptionalDays');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'MinimumOptionalDays', t_Detail);
            t_Detail := null;
            
          --Text and Subject
            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'MandatoryResponseReferralText');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'MandatoryResponseReferralText', t_Detail);
            t_Detail := null;

            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'MandatorySubject');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'MandatorySubject', t_Detail);
            t_Detail := null;            

            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'OptionalResponseReferralText');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'OptionalResponseReferralText', t_Detail);
            t_Detail := null;

            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'OptionalSubject');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'OptionalSubject', t_Detail);
            t_Detail := null;

            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'NotificationReferralText');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'NotificationReferralText', t_Detail);
            t_Detail := null;

            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'NotificationSubject');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'NotificationSubject', t_Detail);
            t_Detail := null;

            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'SummaryReferralText');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'SummaryReferralText', t_Detail);
            t_Detail := null;

            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'SummarySubject');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'SummarySubject', t_Detail);
            t_Detail := null;
                        
            --Description
            t_Detail := api.pkg_columnquery.Value(t_PreviousReferralJobId, 'ReferralDescription');
            api.pkg_columnupdate.SetValue(a_ObjectId, 'ReferralDescription', t_Detail);
            t_Detail := null;
                      
          --Set Referral Documents
        --TODO: will it be the same document?  Ack - what about relationship details?
        
        --Set Direct recipient   
        t_ProcessTypeId := api.pkg_configquery.ObjectDefIdForName('p_ER_ReferralRequest');        
        for c in (select p.processId
                   from api.processes p
                   where p.JobId = t_PreviousReferralJobId
                   and p.ProcessTypeId = t_ProcessTypeId) loop            
           
           -- Create Process
           t_NewProcessId := api.pkg_processupdate.New(a_ObjectId, t_ProcessTypeId, null, null, null, null);
           
           --Relate User Recipients
           t_EndpointId := api.pkg_configquery.EndPointIdForName('p_ER_ReferralRequest', 'UserRecipient');  
           for c2 in (select r.ToObjectId
                       from api.relationships r
                       where r.FromObjectId = c.processid
                        and r.EndPointId = t_EndpointId) loop
           
               t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_NewProcessId, c2.toobjectid);
           end loop;

           --Relate Agency Recipients
           t_EndpointId := api.pkg_configquery.EndPointIdForName('p_ER_ReferralRequest', 'ReferralAgencyRecipient');  
           for c2 in (select r.ToObjectId
                       from api.relationships r
                       where r.FromObjectId = c.processid
                        and r.EndPointId = t_EndpointId) loop
           
               t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_NewProcessId, c2.toobjectid);
           end loop;
 
           --Copy Process Details
           t_Detail := api.pkg_columnquery.Value(c.processid, 'ReferralLevel');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'ReferralLevel', t_Detail);
           t_Detail := null;    
        
           t_Detail := api.pkg_columnquery.Value(c.processid, 'AddressLine1');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'AddressLine1', t_Detail);
           t_Detail := null;    
           
           t_Detail := api.pkg_columnquery.Value(c.processid, 'AddressLine2');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'AddressLine2', t_Detail);
           t_Detail := null;    
           
           t_Detail := api.pkg_columnquery.Value(c.processid, 'BusinessTitle');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'BusinessTitle', t_Detail);
           t_Detail := null;    
           
           t_Detail := api.pkg_columnquery.Value(c.processid, 'City');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'City', t_Detail);
           t_Detail := null;    
           
           t_Detail := api.pkg_columnquery.Value(c.processid, 'EmailCCList');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'EmailCCList', t_Detail);
           t_Detail := null;    
           
           t_Detail := api.pkg_columnquery.Value(c.processid, 'EmailToList');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'EmailToList', t_Detail);
           t_Detail := null;    
           
           t_Detail := api.pkg_columnquery.Value(c.processid, 'HardCopyReference');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'HardCopyReference', t_Detail);
           t_Detail := null;    
           
           t_Detail := api.pkg_columnquery.Value(c.processid, 'Legislated');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'Legislated', t_Detail);
           t_Detail := null;      
 
           t_Detail := api.pkg_columnquery.Value(c.processid, 'OrganizationName');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'OrganizationName', t_Detail);
           t_Detail := null;   

           t_Detail := api.pkg_columnquery.Value(c.processid, 'ZipCode');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'ZipCode', t_Detail);
           t_Detail := null;   

           t_Detail := api.pkg_columnquery.Value(c.processid, 'ZipExtension');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'ZipExtension', t_Detail);
           t_Detail := null;   

           t_Detail := api.pkg_columnquery.Value(c.processid, 'State');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'State', t_Detail);
           t_Detail := null;   

           t_Detail := api.pkg_columnquery.Value(c.processid, 'PublishingLevel');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'PublishingLevel', t_Detail);
           t_Detail := null;   

           t_Detail := api.pkg_columnquery.Value(c.processid, 'RecipientList');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'RecipientList', t_Detail);
           t_Detail := null;   

           t_Detail := api.pkg_columnquery.Value(c.processid, 'RecipientName');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'RecipientName', t_Detail);
           t_Detail := null;   

           t_Detail := api.pkg_columnquery.Value(c.processid, 'RecipientTitle');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'RecipientTitle', t_Detail);
           t_Detail := null;   

           t_Detail := api.pkg_columnquery.Value(c.processid, 'RecipientType');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'RecipientType', t_Detail);
           t_Detail := null;           

           t_Detail := api.pkg_columnquery.Value(c.processid, 'SendSummary');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'SendSummary', t_Detail);
           t_Detail := null;      

           t_Detail := api.pkg_columnquery.Value(c.processid, 'RequestMethodEmail');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'RequestMethodEmail', t_Detail);
           t_Detail := null;  

           t_Detail := api.pkg_columnquery.Value(c.processid, 'RequestMethodHardcopy');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'RequestMethodHardcopy', t_Detail);
           t_Detail := null;  

           t_Detail := api.pkg_columnquery.Value(c.processid, 'RequestMethodOnline');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'RequestMethodOnline', t_Detail);
           t_Detail := null;  

           t_Detail := api.pkg_columnquery.Value(c.processid, 'ResponseMethod');
           api.pkg_columnupdate.SetValue(t_NewProcessId, 'ResponseMethod', t_Detail);
           t_Detail := null;  
           
--TODO: Description?
--Relate Documents?


        end loop;
        
      api.pkg_columnupdate.SetValue(a_ObjectId, 'PrevRunOnce', 'Y');
    else null;
    end if;

  end CopyPreviousReferralData;

end pkg_ER_Utils;

/

