create or replace package pkg_ER_Lookup is

  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_definition.udt_StringList;
  -----------------------------------------------------------------------------
  -- SystemParameter()
  -----------------------------------------------------------------------------
  function SystemParameter (
    a_ColumnName      varchar2
  ) return varchar2;

end pkg_ER_Lookup;

 
/

create or replace package body pkg_ER_Lookup is

  -----------------------------------------------------------------------------
  -- SystemParameter()
  -----------------------------------------------------------------------------
  function SystemParameter (
    a_ColumnName      varchar2
  ) return varchar2 is
    t_Value        varchar2(4000);
  begin
    begin
      select api.pkg_ColumnQuery.Value(o.ObjectId, a_ColumnName)
      into t_Value
      from
        api.ObjectDefs od
        join api.Objects o
            on o.ObjectDefTypeId = od.ObjectDefTypeId
            and o.ObjectDefId = od.ObjectDefId
      where od.Name = 'o_ER_SystemParameters';
    exception
    when no_data_found then
      api.pkg_Errors.RaiseError(-20000,
          'No System Parameters (ER) object created.');
    when too_many_rows then
      api.pkg_Errors.RaiseError(-20000,
          'Multiple System Parameters (ER) objects created.');
    end;

    return t_Value;
  end;

end pkg_ER_Lookup;

/

