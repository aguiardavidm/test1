create table provincecodes_t (
  name                            varchar2(100) not null,
  code                            varchar2(2) not null
) tablespace mediumdata;

grant select
on provincecodes_t
to posseextensions;

grant select
on provincecodes_t
to public;

create unique index provincecodes_uk1
on provincecodes_t (
  code
) tablespace mediumdata;

