create table abcta104 (
  detl_amt                        number(8, 2),
  grp_cd                          varchar2(3),
  trans_type_cd                   varchar2(5),
  refund_ind                      varchar2(1),
  adjust_ind                      varchar2(1),
  dt_row_crtd                     date,
  crtd_oper_id                    varchar2(15)
) tablespace conv;

grant alter
on abcta104
to abc;

grant alter
on abcta104
to posseextensions;

grant debug
on abcta104
to abc;

grant debug
on abcta104
to posseextensions;

grant delete
on abcta104
to abc;

grant delete
on abcta104
to posseextensions;

grant flashback
on abcta104
to abc;

grant flashback
on abcta104
to posseextensions;

grant index
on abcta104
to abc;

grant insert
on abcta104
to abc;

grant insert
on abcta104
to posseextensions;

grant on commit refresh
on abcta104
to abc;

grant on commit refresh
on abcta104
to posseextensions;

grant query rewrite
on abcta104
to abc;

grant query rewrite
on abcta104
to posseextensions;

grant references
on abcta104
to abc;

grant select
on abcta104
to abc;

grant select
on abcta104
to posseextensions;

grant update
on abcta104
to abc;

grant update
on abcta104
to posseextensions;

