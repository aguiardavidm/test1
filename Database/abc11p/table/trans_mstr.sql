create table trans_mstr (
  trans_num                       number(8) not null,
  mstr_seq_num                    number(3) not null,
  dt_row_crtd                     date not null,
  oper_id                         varchar2(15) not null,
  dt_funds_rec                    date,
  dt_proposed_deposit             date,
  dt_actual_deposit               date,
  type_pay_cd                     varchar2(2),
  chk_mo_num                      varchar2(15),
  trans_amt                       number(8, 2) not null,
  trans_name                      varchar2(60),
  dt_adjust                       date,
  adjust_reason                   varchar2(5),
  dt_refund                       date,
  njfis_doc_num                   varchar2(7),
  abc11puk                        number
) tablespace data_large;

grant select
on trans_mstr
to abc;

create unique index i1_trans_mstr
on trans_mstr (
  trans_num,
  mstr_seq_num
) tablespace indexes_medium;

create index i2_trans_mstr
on trans_mstr (
  dt_proposed_deposit,
  dt_actual_deposit
) tablespace indexes_medium;

create index i4_trans_mstr
on trans_mstr (
  njfis_doc_num
) tablespace indexes_small;

