Create table abc11p.ONLINEACCESSCODE_XREF as 
select nm1.name, nm1.name_mstr_id_num as OnlineAccessCode, le.objectid as LegalEntityObjectId
from abc11p.name_mstr nm1
join dataconv.o_abc_legalentity le on le.legacykey = to_char(nm1.name_mstr_id_num)
where nm1.name in 
(select  nm.name--, count(*)
  from dataconv.o_abc_legalentity le
  join abc11p.name_mstr nm on to_char( nm.name_mstr_id_num) = le.legacykey
  --join abc11p.lic_mstr lm on lm.cnty_muni_cd || '-' || lm.lic_type_cd ||'-' || lm.muni_ser_num ||'-' || lm.gen_num = nm.abc_num
 where nm.name_type_cd = '2.1-LICENSEE' and nm.name_stat = 'C' and nm.srce_grp_cd = 'LIC'
  -- and nm.name = 'GRAFTON INNS LLC'
 group by nm.name 
 having count(*) > 1)
 and nm1.name_stat = 'C'
  and nm1.name_type_cd = '2.1-LICENSEE'
  order by 1
 
-- Create/Recreate indexes 
create index ABC11P.NAME_IX1 on ABC11P.ONLINEACCESSCODE_XREF (name);
create index ABC11P.ONLINEACCESSCODE_IX2 on ABC11P.ONLINEACCESSCODE_XREF (onlineaccesscode);
create index ABC11P.OBJECTID_IX3 on ABC11P.ONLINEACCESSCODE_XREF (legalentityobjectid);

grant all on abc11p.ONLINEACCESSCODE_XREF to abc;
grant all on abc11p.ONLINEACCESSCODE_XREF to extension;