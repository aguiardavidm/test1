create table trans_detl (
  trans_num                       number(8) not null,
  detl_num                        number(9) not null,
  detl_seq_num                    number(3) not null,
  assoc_bur_cd                    varchar2(4),
  grp_cd                          varchar2(3),
  trans_type_cd                   varchar2(5),
  detl_amt                        number(8, 2) not null,
  dt_row_crtd                     date not null,
  oper_id                         varchar2(15) not null,
  mstr_seq_num                    varchar2(3) not null,
  abc11puk                        number
) tablespace data_large;

grant select
on trans_detl
to abc;

create unique index i1_trans_detl
on trans_detl (
  trans_num,
  mstr_seq_num,
  detl_num,
  detl_seq_num
) tablespace indexes_medium;

create index i2_trans_detl
on trans_detl (
  dt_row_crtd,
  oper_id
) tablespace indexes_medium;

