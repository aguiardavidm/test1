create table abcta104_report (
  totalamount                     number,
  permit                          number,
  license                         number,
  misc                            number,
  trans_type                      varchar2(500),
  legacykey                       number,
  sessionkey                      varchar2(1000)
) tablespace conv;

grant alter
on abcta104_report
to abc;

grant debug
on abcta104_report
to abc;

grant delete
on abcta104_report
to abc;

grant flashback
on abcta104_report
to abc;

grant index
on abcta104_report
to abc;

grant insert
on abcta104_report
to abc;

grant on commit refresh
on abcta104_report
to abc;

grant query rewrite
on abcta104_report
to abc;

grant references
on abcta104_report
to abc;

grant select
on abcta104_report
to abc;

grant update
on abcta104_report
to abc;

create index abcra104_report_ix1
on abcta104_report (
  legacykey
) tablespace conv;

