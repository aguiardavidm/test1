create table abcta104_sum (
  detl_amt                        number(10, 2),
  grp_cd                          varchar2(3),
  trans_type_cd                   varchar2(5),
  refund_ind                      varchar2(1),
  adjust_ind                      varchar2(1),
  dt_row_crtd                     date,
  crtd_oper_id                    varchar2(15)
) tablespace conv;

grant alter
on abcta104_sum
to abc;

grant alter
on abcta104_sum
to posseextensions;

grant debug
on abcta104_sum
to abc;

grant debug
on abcta104_sum
to posseextensions;

grant delete
on abcta104_sum
to abc;

grant delete
on abcta104_sum
to posseextensions;

grant flashback
on abcta104_sum
to abc;

grant flashback
on abcta104_sum
to posseextensions;

grant index
on abcta104_sum
to abc;

grant insert
on abcta104_sum
to abc;

grant insert
on abcta104_sum
to posseextensions;

grant on commit refresh
on abcta104_sum
to abc;

grant on commit refresh
on abcta104_sum
to posseextensions;

grant query rewrite
on abcta104_sum
to abc;

grant query rewrite
on abcta104_sum
to posseextensions;

grant references
on abcta104_sum
to abc;

grant select
on abcta104_sum
to abc;

grant select
on abcta104_sum
to posseextensions;

grant update
on abcta104_sum
to abc;

grant update
on abcta104_sum
to posseextensions;

