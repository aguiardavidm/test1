create or replace package pkg_ExpressionElement as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;

  /*---------------------------------------------------------------------------
   * Value() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function Value (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * numberValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NumberValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return number;

  /*---------------------------------------------------------------------------
   * DateValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DateValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return date;

  /*---------------------------------------------------------------------------
   * BooleanValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function BooleanValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return boolean;

end pkg_ExpressionElement;

/

grant execute
on pkg_expressionelement
to feescheduleplususer;

create or replace package body pkg_ExpressionElement as

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetDataByFeeElementId (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_Data                  out ExpressionElement%rowtype
  ) is
  begin
    select *
      into a_Data
      from ExpressionElement
     where FeeElementId = a_FeeElementId
       and FeeScheduleId = a_FeeScheduleId;
  end;

  /*---------------------------------------------------------------------------
   * Value() -- PUBLIC
   * TODO: add code to handle aggregate bindings
   *-------------------------------------------------------------------------*/
  function Value (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return varchar2 is
    t_ExpressionElement         ExpressionElement%rowtype;
  begin
    GetDataByFeeElementId(a_FeeScheduleId, a_FeeElementId, t_ExpressionElement);

    return pkg_ExpressionColumn.Value(pkg_ExpressionColumn.gc_ExpElementSyntaxString, a_FeeScheduleId, t_ExpressionElement.Syntax, a_PosseObjectId, a_DataSourceId);
  end;

  /*---------------------------------------------------------------------------
   * NumberValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NumberValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return number is
    t_Cursor                    integer;
    t_Count                     number;
    t_Value                     number;
    t_ExpressionElement         ExpressionElement%rowtype;
  begin
    GetDataByFeeElementId(a_FeeScheduleId, a_FeeElementId, t_ExpressionElement);

    return pkg_ExpressionColumn.NumberValue(pkg_ExpressionColumn.gc_ExpElementSyntaxNumber, a_FeeScheduleId, t_ExpressionElement.Syntax, a_PosseObjectId, a_DataSourceId);
  end;

  /*---------------------------------------------------------------------------
   * DateValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DateValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return date is
    t_Cursor                    integer;
    t_Count                     number;
    t_Value                     date;
    t_ExpressionElement         ExpressionElement%rowtype;
  begin
    GetDataByFeeElementId(a_FeeScheduleId, a_FeeElementId, t_ExpressionElement);

    return pkg_ExpressionColumn.DateValue(pkg_ExpressionColumn.gc_ExpElementSyntaxDate, a_FeeScheduleId, t_ExpressionElement.Syntax, a_PosseObjectId, a_DataSourceId);
  end;

  /*---------------------------------------------------------------------------
   * BooleanValue() -- PUBLIC
   *
   * This one is a bit different from the others, since we want to resolve
   *   to a true boolean, not just a "Y" or "N". The actual value returned
   *   will be a string "Y" or "N". Translate this to a boolean.
   *-------------------------------------------------------------------------*/
  function BooleanValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return boolean is
    t_Cursor                    integer;
    t_Count                     number;
    t_Value                     char(1);
    t_ExpressionElement         ExpressionElement%rowtype;
  begin
    GetDataByFeeElementId(a_FeeScheduleId, a_FeeElementId, t_ExpressionElement);

    return pkg_ExpressionColumn.BooleanValue(pkg_ExpressionColumn.gc_ExpElementSyntaxBoolean, a_FeeScheduleId, t_ExpressionElement.Syntax, a_PosseObjectId, a_DataSourceId);
  end;

end pkg_ExpressionElement;

/

