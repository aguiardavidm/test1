create or replace package pkg_Utils is

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *   Utitilies used by the Fee Schedule Plus application
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;

  /*---------------------------------------------------------------------------
   * GetFeeScheduleObjectId()
   *   Given a Fee Schedule ID, returns the ObjectId of the o_FeeSchedule
   *   object associated with that ID. If the Object cannot be found, returns
   *   null.
   *-------------------------------------------------------------------------*/
  function GetFeeScheduleObjectId (
    a_FeeScheduleId                     udt_Id
  ) return udt_PosseId;

end pkg_Utils;

/

create or replace package body pkg_Utils is

  /*---------------------------------------------------------------------------
   * GetFeeScheduleObjectId() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetFeeScheduleObjectId (
    a_FeeScheduleId                     udt_Id
  ) return udt_PosseId is
    t_FeeScheduleId                     varchar2(9);
    t_ObjectDefId                       udt_PosseId;
    t_FeeScheduleObjectId               udt_PosseId;
  begin
    t_ObjectDefId := api.pkg_ConfigQuery.ObjectDefIdForName('o_FeeSchedule');
    t_FeeScheduleId := to_char(a_FeeScheduleId);

    select ObjectId
    into t_FeeScheduleObjectId
    from api.RegisteredExternalObjects
    where ObjectDefId = t_ObjectDefId
      and LinkValue = t_FeeScheduleId;

    return t_FeeScheduleObjectId;
  exception when no_data_found then
    return null;
  end GetFeeScheduleObjectId;

end pkg_Utils;

/

