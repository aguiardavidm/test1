create or replace package pkg_ExpressionColumn as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;

  /*--------------------------------------------------------------------------
   * Constants - PUBLIC
   *------------------------------------------------------------------------*/
  gc_FeeCatCondition                constant pls_integer := 1;
  gc_FeeDefAmount                   constant pls_integer := 2;
  gc_FeeDefCondition                constant pls_integer := 3;
  gc_FeeDefGlAccountNumber          constant pls_integer := 4;
  gc_FeeDefFeeDescription           constant pls_integer := 5;
  gc_ExpElementSyntaxString         constant pls_integer := 6;
  gc_ExpElementSyntaxNumber         constant pls_integer := 7;
  gc_ExpElementSyntaxDate           constant pls_integer := 8;
  gc_ExpElementSyntaxBoolean        constant pls_integer := 9;

  /*---------------------------------------------------------------------------
   * Value() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function Value (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * NnumberValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NumberValue (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return number;

  /*---------------------------------------------------------------------------
   * DateValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DateValue (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return date;

  /*---------------------------------------------------------------------------
   * BooleanValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function BooleanValue (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return boolean;

end pkg_ExpressionColumn;

/

grant execute
on pkg_expressioncolumn
to feescheduleplususer;

create or replace package body pkg_ExpressionColumn as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  type udt_BindRecord is record (
    BindName                   varchar2(30),
    BindType                   varchar2(7),
    StringValue                varchar2(4000),
    DateValue                  date,
    NumberValue                number
  );

  type udt_BindTable is table of udt_BindRecord index by binary_integer;

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_ExpressionColumnId        udt_Id,
    a_Data                  out ExpressionColumn%rowtype
  ) is
  begin
    select *
      into a_Data
      from ExpressionColumn
     where ExpressionColumnId = a_ExpressionColumnId;
  end;

  /*---------------------------------------------------------------------------
   * Syntax() -- PUBLIC
   *-------------------------------------------------------------------------*/
/*  function Syntax (
    a_ExpressionColumn         ExpressionColumn%rowtype,
    a_FeeScheduleId            udt_Id,
    a_PrimaryKeyId             udt_Id
  ) return varchar2 is
    t_SQL                      varchar2(4000);
    t_Value                    varchar2(4000);
  begin
    \* Logic:
         - build dynamic SQL string to retrieve syntax from given column
         - return result
    *\
   t_SQL := 'select ' || a_ExpressionColumn.ColumnName  || ' into :Value from ' || a_ExpressionColumn.TableName || ' where FeeScheduleId = :FeeScheduleId and ' || a_ExpressionColumn.TablePrimaryKeyName || ' = :PrimaryKeyId';
   execute immediate t_SQL using a_FeeScheduleId, a_PrimaryKeyId, out t_Value;

   return t_Value;
  end;*/

  /*--------------------------------------------------------------------------
   * FindInQuotedString()
   *------------------------------------------------------------------------*/
  procedure FindInQuotedString (
    a_String                    varchar2,
    a_FindCharList              varchar2,
    a_StartPos                  pls_integer,
    a_Position             out  varchar2,
    a_CharFound            out  char
  ) is
    t_Length                 pls_integer;
    t_Char                   char(1);
    t_InQuote                char(1);
    t_Increment              pls_integer := 1;
    t_Pos                    pls_integer;
  begin
    --Find the next occurrence of any of the find characters within a_String.
    --Quoted strings don't count when searching for the find characters
    --Inside a quoted string, two quotes together don't count to end the quoted string
    t_Length := nvl(length(a_String), 0);
    if a_StartPos = -1 then
      t_Pos := t_Length;
      t_Increment := -1;
    else
      t_Pos := nvl(a_StartPos, 0);
    end if;
    loop
      if t_Pos > t_Length or t_Pos <= 0 then
        exit;
      end if;
      t_Char := substr(a_String, t_Pos, 1);

      if t_InQuote is null then  -- not in quoted string
        if t_Char = '''' then  -- beginning of quoted string
          t_InQuote := t_Char;
        elsif instr(a_FindCharList, t_Char) > 0 then  -- found char
          a_CharFound := t_Char;
          a_Position := t_Pos;
          exit;
        end if;
      else  -- in quoted string
        if t_Char = '''' then
          if t_Pos + t_Increment > 0 and substr(a_String, t_Pos + t_Increment, 1) = t_Char then  -- doubled quote, doesn't end the quoted string
            t_Pos := t_Pos + t_Increment;
          else  -- end of quoted string
            t_InQuote := null;
          end if;
        end if;
      end if;
      t_Pos := t_Pos + t_Increment;
    end loop;

    if t_InQuote is not null then
      raise_application_error(-20000, 'Missing end-quote in string.  String is: ' || a_String);
    end if;
  end;

  /*--------------------------------------------------------------------------
   * RemoveQuotes()
   *------------------------------------------------------------------------*/
  procedure RemoveQuotes (
    a_String               in out   varchar2
  ) is
    t_StringStart                    char(1);
  begin
    t_StringStart := substr(a_String, 1, 1);
    if t_StringStart in ('''', '"') then
      if substr(a_String, -1, 1) = t_StringStart then
        -- Remove start and end quotes from string and replace doubled quotes in token with single quotes
        a_String := replace(substr(a_String, 2, length(a_String) - 2), t_StringStart || t_StringStart, t_StringStart);
      end if;
    end if;
  end;

  /*--------------------------------------------------------------------------
   * RemoveQuotes()
   *------------------------------------------------------------------------*/
  function RemoveQuotes (
    a_String                            varchar2
  ) return varchar2 is
    t_String                            varchar2(4000);
  begin
    t_String := a_String;
    RemoveQuotes(t_String);
    return t_String;
  end;

  /*--------------------------------------------------------------------------
   * GetNextToken()
   *------------------------------------------------------------------------*/
  procedure GetNextToken (
    a_String                        varchar2,
    a_DelimiterList                 varchar2,
    a_Token               out       varchar2,
    a_RemainingString     out       varchar2,
    a_StartPos                      pls_integer := 1,
    a_NullTokenIfDelimiterNotFound  boolean default false,
    a_RemoveQuotes                  boolean default true
  ) is
    t_Pos                    pls_integer;
    t_CharFound              char(1);
  begin

    FindInQuotedString(a_String, a_DelimiterList, a_StartPos, t_Pos, t_CharFound);

    if t_Pos is null then
      if a_NullTokenIfDelimiterNotFound then
        a_RemainingString := a_String;
      else
        a_Token := a_String;
      end if;
    else
      if a_StartPos = -1 then
        a_Token := trim(substr(a_String, t_Pos + 1));
        a_RemainingString := substr(a_String, 1, t_Pos - 1);
      else
        a_Token := trim(substr(a_String, a_StartPos, t_Pos - 1));
        a_RemainingString := substr(a_String, t_Pos + 1);
      end if;
    end if;

    -- If token starts with a quote, it must end with a quote.  Putting quoted string and literals together in the same token is not supported.
    if a_RemoveQuotes then
      RemoveQuotes(a_Token);
    end if;
  end;

  /*--------------------------------------------------------------------------
   * GetNextToken()
   *   - Overloaded to enable skipping of the a_RemainingString and
   * parameter
   *------------------------------------------------------------------------*/
  procedure GetNextToken (
    a_String                 varchar2,
    a_TokenList              varchar2,
    a_Token            out   varchar2,
    a_StartPos               pls_integer := 1,
    a_NullTokenIfDelimiterNotFound  boolean default false,
    a_RemoveQuotes                  boolean default true
  ) is
    t_Dummy                  varchar2(32767);
  begin
    GetNextToken(a_String, a_TokenList, a_Token, t_Dummy, a_StartPos, a_NullTokenIfDelimiterNotFound, a_RemoveQuotes);
  end;

  /*---------------------------------------------------------------------------
   * NewBindVariable() -- PRIVATE
   *-------------------------------------------------------------------------*/
  function NewBindVariable (
    a_Value                  varchar2,
    a_BindTable       in out udt_BindTable
  ) return varchar2 is
    t_Index                  pls_integer;
  begin
    t_Index := a_BindTable.count + 1;
    a_BindTable(t_Index).BindName := 'var' || t_Index;
    a_BindTable(t_Index).BindType := 'String';
    a_BindTable(t_Index).StringValue := a_Value;
    return a_BindTable(t_Index).BindName;
  end;

  /*---------------------------------------------------------------------------
   * NewBindVariable() -- PRIVATE
   *-------------------------------------------------------------------------*/
  function NewBindVariable (
    a_Value                  number,
    a_BindTable       in out udt_BindTable
  ) return varchar2 is
    t_Index                  pls_integer;
  begin
    t_Index := a_BindTable.count + 1;
    a_BindTable(t_Index).BindName := 'var' || t_Index;
    a_BindTable(t_Index).BindType := 'Number';
    a_BindTable(t_Index).NumberValue := a_Value;
    return a_BindTable(t_Index).BindName;
  end;

  /*---------------------------------------------------------------------------
   * NewBindVariable() -- PRIVATE
   *-------------------------------------------------------------------------*/
  function NewBindVariable (
    a_Value                  date,
    a_BindTable       in out udt_BindTable
  ) return varchar2 is
    t_Index                  pls_integer;
  begin
    t_Index := a_BindTable.count + 1;
    a_BindTable(t_Index).BindName := 'var' || t_Index;
    a_BindTable(t_Index).BindType := 'Date';
    a_BindTable(t_Index).DateValue := a_Value;
    return a_BindTable(t_Index).BindName;
  end;

  /*---------------------------------------------------------------------------
   * InternalValue() -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure InternalValue (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id,
    a_StringValue           out varchar2,
    a_NumberValue           out number,
    a_DateValue             out date,
    a_BooleanValue          out boolean
  ) is
    t_ExpressionColumn           ExpressionColumn%rowtype;
    t_DataType                   FeeElement.DataType%type;
    t_Count                      number;
    t_Position                   pls_integer := 0;
    t_CharFound                  char(1);
    t_InBindVar                  boolean := false;
    t_StartPosition              pls_integer;
    t_Value                      varchar2(32767);
    t_BindVarCount               pls_integer := 0;
    t_Cursor                     integer;
    t_SQL                        varchar2(32767);
    t_RetVal                     varchar2(32767);
    t_BindName                   varchar2(100);
    t_BindDataSourceName         varchar2(30);
    t_DataSourceName             varchar2(30);
    t_FeeElement                 FeeElement%rowtype;
    t_DataSource                 DataSource%rowtype;
    t_BindDataSource             DataSource%rowtype;
    t_BindFeeElement             FeeElement%rowtype;
    t_BindPosseObjectId          udt_PosseId;
    t_SubstitutionString         varchar2(4000);
    t_ParenthStartPos            pls_integer;
    t_ParenthEndPos              pls_integer;
    t_ColPrefix                  varchar2(100);
    t_ColSuffix                  varchar2(100);
    t_BindTable                  udt_BindTable;
    t_BindRetrievalDataType      FeeElement.DataType%type;
    t_FunctionName               varchar2(20);
  begin
    -- Get syntax
    GetData(a_ExpressionColumnId, t_ExpressionColumn);

    if a_Syntax is null then
      return;
    end if;

    -- if syntax is not in expression form, just return it as appropriate data type
    if substr(a_Syntax, 1, 1) != '=' then
      if t_ExpressionColumn.DataType = 'String' then
        a_StringValue := a_Syntax;
      elsif t_ExpressionColumn.DataType = 'Number' then
        a_NumberValue := to_number(a_Syntax);
      elsif t_ExpressionColumn.DataType = 'Date' then
        a_DateValue := to_date(a_Syntax, pkg_FeeSchedulePlus.gc_DateFormat);
      elsif t_ExpressionColumn.DataType = 'Boolean' then
        a_BooleanValue := a_Syntax = 'Y';
      end if;
      return;
    end if;
    --    pkg_FeeElement.GetData(a_ExpressionColumn.FeeElementId, t_FeeElement);

    pkg_DataSource.GetData(a_FeeScheduleId, a_DataSourceId, t_DataSource);

    t_Cursor := dbms_sql.open_cursor;
    begin

      pkg_Debug.PutLine('Evaluating Syntax: ' || a_Syntax);

      t_SQL := substr(a_Syntax, 2);
      loop
        t_BindPosseObjectId := a_PosseObjectId;
        t_SubstitutionString := null;
        t_ColPrefix := null;
        t_ColSuffix := null;

        FindInQuotedString(t_SQL, '{}', t_Position + 1, t_Position, t_CharFound);
        exit when t_Position is null;

        if t_CharFound = '{' then
          if t_InBindVar then
            raise_application_error(-20000, 'Embedded curly braces within expression not permitted.');
          end if;
          t_InBindVar := true;
          t_StartPosition := t_Position;
        else
          if not t_InBindVar then
            raise_application_error(-20000, 'Un-matched close brace in string: ' || t_SQL);
          end if;

          /*
            - for each bind:
              - determine if data source is specified
              - if so and the data source is not the same as the ExpressionColumn data source:
                - find fee element from namespace and generate substitution code for it
                  - determine what relationships exist between the two data sources
                  - if the expression data source is a process and the bound source is a job, resolve values based on the job and use standard binding
                  - if there are POSSE relationships, generate select from api.Relationships
                    - "(select pkg_FeeElement.<DataType>Value(:FeeElementId, re.ToObjectId) from api.Relationships re where re.FromObjectId = :SourcePosseObjectId)"
                  - Generate bind variable for feeelementid
                  - Generate bind variable for ":SourceObjectId" if it doesn't exist
              - otherwise
                - find element with specified name in same data source as ExpressionColumn
                - generate bind variable substitution text using numeric counter
                - generate bind variable
              - substitute the substitution text for the curly-braced section
          */

          -- get binding string
          t_BindName := substr(t_SQL, t_StartPosition + 1, t_Position - t_StartPosition - 1);

          pkg_Debug.PutLine('Found Binding: ' || t_BindName);
          -- if parentheses exist in a binding referencing another data set, include them in the generated sql for aggregate processing
          -- retrieve the string within the parentheses
          t_ParenthStartPos := instr(t_BindName, '(');
          if t_ParenthStartPos > 0 then
            t_ParenthEndPos := instr(t_BindName, ')');
            if t_ParenthEndPos = 0 then
              raise_application_error(-20000, 'Missing end parenthesis in expression.');
            end if;

            t_ColPrefix := substr(t_BindName, 1, t_ParenthStartPos);
            t_ColSuffix := substr(t_BindName, t_ParenthEndPos);
            t_BindName := substr(t_BindName, t_ParenthStartPos + 1, t_ParenthEndPos - t_ParenthStartPos - 1);
          end if;

          GetNextToken(t_BindName, '.', t_BindName, t_BindDataSourceName, -1);

          -- get bind data source
          if t_BindDataSourceName is null then
            t_BindDataSourceName := t_DataSource.PosseViewName;
          end if;

          begin
            pkg_DataSource.GetDataByPosseViewName(a_FeeScheduleId, t_BindDataSourceName, t_BindDataSource);
          exception when no_data_found then
            raise_application_error(-20000, 'No data source with a Posse View Name of ' || t_BindDataSourceName || ' was found in the DataSource table');
          end;

          -- get bind fee element
          begin
            pkg_FeeElement.GetData(a_FeeScheduleId, t_BindDataSource.DataSourceId, t_BindName, t_BindFeeElement);
          exception when no_data_found then
            raise_application_error(-20000, 'No fee element with the name ' || t_BindName || ' and data source: ' || t_BindDataSource.Description || ' was found in the FeeElement table');
          end;

          -- Treat boolean bind variables as strings, get value as 'Y' or 'N', and translate to boolean by adding " = 'Y'" to substitution string
          if t_BindFeeElement.DataType = 'Boolean' then
            t_BindRetrievalDataType := 'String';
          else
            t_BindRetrievalDataType := t_BindFeeElement.DataType;
          end if;

          -- if data source other than the expression data source was specified, generate a sub-select substitution string for it
          if t_BindDataSource.PosseViewName <> t_DataSource.PosseViewName then

            -- determine what kind of relationship to use
            -- if the source data source is a process and the bind data source is a job, just get the job
            pkg_Debug.PutLine('t_DataSource ObjectDefTypeName: ' || pkg_DataSource.PosseObjectDefTypeName(a_FeeScheduleId, t_DataSource.DataSourceId));
            pkg_Debug.PutLine('t_BindDataSource ObjectDefTypeName: ' || pkg_DataSource.PosseObjectDefTypeName(a_FeeScheduleId, t_BindDataSource.DataSourceId));
            if pkg_DataSource.PosseObjectDefTypeName(a_FeeScheduleId, t_DataSource.DataSourceId) = 'ProcessTypes'
             and pkg_DataSource.PosseObjectDefTypeName(a_FeeScheduleId, t_BindDataSource.DataSourceId) = 'JobTypes' then
              select JobId
                into t_BindPosseObjectId
                from api.Processes
               where ProcessId = a_PosseObjectId;
            else
              -- if not a process/job relationship, generate a substitution string based on a select from api.Relationships
              if t_BindRetrievalDataType = 'String' then
                t_FunctionName := 'Value';
              else
                t_FunctionName := t_BindRetrievalDataType || 'Value';
              end if;

              t_SubstitutionString :=
                '(select ' || t_ColPrefix || 'pkg_FeeElement.' || t_FunctionName || '(:'
                  || NewBindVariable(a_FeeScheduleId, t_BindTable) || ', :' || NewBindVariable(t_BindFeeElement.FeeElementId, t_BindTable) || ', re.ToObjectId, :' || NewBindVariable(t_BindDataSource.DataSourceId, t_BindTable)
                  || ')' || t_ColSuffix
                || ' from query.' || t_BindDataSource.PosseViewName || ' ds, api.Relationships re'
                || ' where re.FromObjectId = :' || NewBindVariable(a_PosseObjectId, t_BindTable)
                || '   and ds.ObjectId = re.ToObjectId)';
            end if;
          end if;

          -- if substitution string has not yet been set, generate it as a bind variable
          if t_SubstitutionString is null then

            if t_BindRetrievalDataType = 'String' then
              t_SubstitutionString := ':' || NewBindVariable(pkg_FeeElement.Value(a_FeeScheduleId, t_BindFeeElement.FeeElementId, t_BindPosseObjectId, t_BindDataSource.DataSourceId), t_BindTable);
            elsif t_BindRetrievalDataType = 'Number' then
              t_SubstitutionString := ':' || NewBindVariable(pkg_FeeElement.NumberValue(a_FeeScheduleId, t_BindFeeElement.FeeElementId, t_BindPosseObjectId, t_BindDataSource.DataSourceId), t_BindTable);
            elsif t_BindRetrievalDataType = 'Date' then
              t_SubstitutionString := ':' || NewBindVariable(pkg_FeeElement.DateValue(a_FeeScheduleId, t_BindFeeElement.FeeElementId, t_BindPosseObjectId, t_BindDataSource.DataSourceId), t_BindTable);
            end if;

          end if;

          if t_BindFeeElement.DataType = 'Boolean' then
            t_SubstitutionString := t_SubstitutionString || ' = ''Y''';
          end if;

          -- perform substitution
          t_SQL := substr(t_SQL, 1, t_StartPosition - 1)
                   || t_SubstitutionString
                   || substr(t_SQL, t_Position + 1);

          -- Determine difference in t_Position based on string replacement
          t_Position := t_StartPosition - 1 + length(t_SubstitutionString);
          t_InBindVar := false;
        end if;
      end loop;

      if t_InBindVar then
        raise_application_error(-20000, 'Un-matched close brace in string: ' || a_Syntax);
      end if;

      if t_ExpressionColumn.DataType = 'Boolean' then
        -- if this is a boolean, the expression must be in boolean form. This isn't supported by SQL, so we'll translate the boolean expression to a "Y" or "N" and return as a string
        t_SQL := 'case when ' || t_SQL || ' then ''Y'' else ''N'' end';
      end if;

--dbms_output.put_line('Will it work???');

      t_SQL := 'select ' || t_SQL || ' from dual';

--dbms_output.put_line(t_SQL);

      pkg_Debug.PutLine('Generated SQL: ' || t_SQL);
      dbms_sql.parse(t_Cursor, t_SQL, dbms_sql.native);

--dbms_output.put_line('Yes it did!!!!');
--api.pkg_errors.RaiseError(-20000, 'Whoops!!! Take out this Raise Error');

      for i in 1..t_BindTable.count loop
        if t_BindTable(i).BindType = 'String' then
          pkg_Debug.PutLine(t_BindTable(i).BindName || ': ' || t_BindTable(i).StringValue || ' (String)');
          dbms_sql.bind_variable(t_Cursor, t_BindTable(i).BindName, t_BindTable(i).StringValue);
        elsif t_BindTable(i).BindType = 'Number' then
          pkg_Debug.PutLine(t_BindTable(i).BindName || ': ' || t_BindTable(i).NumberValue || ' (Number)');
          dbms_sql.bind_variable(t_Cursor, t_BindTable(i).BindName, t_BindTable(i).NumberValue);
        elsif t_BindTable(i).BindType = 'Date' then
          pkg_Debug.PutLine(t_BindTable(i).BindName || ': ' || t_BindTable(i).DateValue || ' (Date)');
          dbms_sql.bind_variable(t_Cursor, t_BindTable(i).BindName, t_BindTable(i).DateValue);
        end if;
      end loop;

      if t_ExpressionColumn.DataType = 'String' then
        dbms_sql.define_column(t_Cursor, 1, a_StringValue, 4000);
      elsif t_ExpressionColumn.DataType = 'Number' then
        dbms_sql.define_column(t_Cursor, 1, a_NumberValue);
      elsif t_ExpressionColumn.DataType = 'Date' then
        dbms_sql.define_column(t_Cursor, 1, a_DateValue);
      elsif t_ExpressionColumn.DataType = 'Boolean' then
        dbms_sql.define_column(t_Cursor, 1, a_StringValue, 1);
      end if;

      t_Count := dbms_sql.execute(t_Cursor);

      t_Count := dbms_sql.fetch_rows(t_Cursor);

      if t_ExpressionColumn.DataType = 'String' then
        dbms_sql.column_value(t_Cursor, 1, a_StringValue);
      elsif t_ExpressionColumn.DataType = 'Number' then
        dbms_sql.column_value(t_Cursor, 1, a_NumberValue);
        a_StringValue := a_NumberValue;
      elsif t_ExpressionColumn.DataType = 'Date' then
        dbms_sql.column_value(t_Cursor, 1, a_DateValue);
        a_StringValue := to_char(a_DateValue, pkg_FeeSchedulePlus.gc_DateFormat);
      elsif t_ExpressionColumn.DataType = 'Boolean' then
        dbms_sql.column_value(t_Cursor, 1, a_StringValue);
        a_BooleanValue := a_StringValue = 'Y';
      end if;

      dbms_sql.close_cursor(t_Cursor);
    exception when others then
      dbms_sql.close_cursor(t_Cursor);
      raise;
    end;

  end;

  /*---------------------------------------------------------------------------
   * Value() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function Value (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return varchar2 is
    t_StringValue               varchar2(4000);
    t_DateValue                 date;
    t_NumberValue               number;
    t_BooleanValue              boolean;
  begin
    InternalValue(a_ExpressionColumnId, a_FeeScheduleId, a_Syntax, a_PosseObjectId, a_DataSourceId, t_StringValue, t_NumberValue, t_DateValue, t_BooleanValue);
    return t_StringValue;
  end;

  /*---------------------------------------------------------------------------
   * NumberValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NumberValue (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return number is
    t_StringValue               varchar2(4000);
    t_DateValue                 date;
    t_NumberValue               number;
    t_BooleanValue              boolean;
  begin
    InternalValue(a_ExpressionColumnId, a_FeeScheduleId, a_Syntax, a_PosseObjectId, a_DataSourceId, t_StringValue, t_NumberValue, t_DateValue, t_BooleanValue);
    return t_NumberValue;
  end;

  /*---------------------------------------------------------------------------
   * DateValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DateValue (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return date is
    t_StringValue               varchar2(4000);
    t_DateValue                 date;
    t_NumberValue               number;
    t_BooleanValue              boolean;
  begin
    InternalValue(a_ExpressionColumnId, a_FeeScheduleId, a_Syntax, a_PosseObjectId, a_DataSourceId, t_StringValue, t_NumberValue, t_DateValue, t_BooleanValue);
    return t_DateValue;
  end;

  /*---------------------------------------------------------------------------
   * BooleanValue() -- PUBLIC
   *
   * This one is a bit different from the others, since we want to resolve
   *   to a true boolean, not just a "Y" or "N". The actual value returned
   *   will be a string "Y" or "N". Translate this to a boolean.
   *-------------------------------------------------------------------------*/
  function BooleanValue (
    a_ExpressionColumnId        udt_Id,
    a_FeeScheduleId             udt_Id,
    a_Syntax                    varchar2,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id
  ) return boolean is
    t_StringValue               varchar2(4000);
    t_DateValue                 date;
    t_NumberValue               number;
    t_BooleanValue              boolean;
  begin
    InternalValue(a_ExpressionColumnId, a_FeeScheduleId, a_Syntax, a_PosseObjectId, a_DataSourceId, t_StringValue, t_NumberValue, t_DateValue, t_BooleanValue);
    return t_BooleanValue;
  end;

end pkg_ExpressionColumn;

/

