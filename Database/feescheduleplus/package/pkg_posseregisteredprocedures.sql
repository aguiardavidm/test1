create or replace package pkg_PosseRegisteredProcedures as

  -- Public Type Declarations
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;

  /*---------------------------------------------------------------------------
   * GenerateFees()
   *   Re-Create, adjust and delete any unposted, unpaid, or unadjusted fees that
   * need to be changed.
   *-------------------------------------------------------------------------*/
  procedure GenerateFees (
    a_PosseObjectId                     udt_Id,
    a_AsOfDate                          date,
    a_ProcessBasedFee                   char,
    a_DefinedOnObjectId                 udt_Id default null
  );

end pkg_PosseRegisteredProcedures;
/
grant execute
on pkg_posseregisteredprocedures
to abc;

grant execute
on pkg_posseregisteredprocedures
to bcpdata;

grant execute
on pkg_posseregisteredprocedures
to conversion;

grant execute
on pkg_posseregisteredprocedures
to extension;

grant execute
on pkg_posseregisteredprocedures
to feescheduleplususer;

grant execute
on pkg_posseregisteredprocedures
to posseextensions;

create or replace package body pkg_PosseRegisteredProcedures as

  -- Private Type Declarations
  subtype udt_IdList is pkg_FeeSchedulePlus.udt_IdList;
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;
  subtype udt_FeeDefinitionList is pkg_FeeSchedulePlus.udt_FeeDefinitionList;
  type udt_FeeIdList is table of char(1) index by pls_integer;

  /*---------------------------------------------------------------------------
   * GenerateFees() -- PUBLIC
   *   Re-Create, adjust and delete any unposted, unpaid, or unadjusted fees that
   * need to be changed.
   *-------------------------------------------------------------------------*/
  procedure GenerateFees (
    a_PosseObjectId                     udt_Id,
    a_AsOfDate                          date,
    a_ProcessBasedFee                   char,
    a_DefinedOnObjectId                 udt_Id default null
  ) is
    t_FeeDefinitionList                 udt_FeeDefinitionList;
    t_JobId                             udt_PosseId;
    t_FeeDataSourceObjectId             udt_PosseId;
    t_AdjId                             udt_Id;
    t_FeeIds                            udt_FeeIdList;
    t_FeeId                             udt_Id;
  begin

    t_JobId := nvl(api.pkg_ColumnQuery.NumericValue(a_PosseObjectId, 'JobId'),
        a_PosseObjectId);
    pkg_debug.putline('********t_JobId: ' || t_JobId);

    -- Do not change fees if job is cancelled.
    if api.pkg_ColumnQuery.value(t_JobId, 'StatusDescription') = 'Cancelled' then
      return;
    end if;
      
    if upper(a_ProcessBasedFee) = 'Y' then
      t_FeeDataSourceObjectId := a_PosseObjectId;
    elsif a_DefinedOnObjectId is not null then
      t_FeeDataSourceObjectId := a_DefinedOnObjectId;
    else
      t_FeeDataSourceObjectId := t_JobId;
    end if;
    pkg_debug.putline('FeeDataSourceObjectId: ' || t_FeeDataSourceObjectId);

    t_FeeDefinitionList := pkg_FeeSchedulePlus.DetermineApplicableFees(
        t_FeeDataSourceObjectId);
    pkg_debug.putline('t_FeeDefinitionList.Count: ' || t_FeeDefinitionList.Count);
    for i in 1..t_FeeDefinitionList.count loop
      t_FeeId := pkg_FeeDefinition.GenerateFee(t_FeeDefinitionList(i),
          t_FeeDataSourceObjectId, t_JobId);
      pkg_debug.putline('Generated Fee for: ' || t_FeeId);
      if t_FeeId is not null then
        t_FeeIds(t_FeeId) := 'Y';
      end if;
    end loop;

    -- Now loop through all the system generated fees on this t_JobId and remove
    -- any extra fees
    for i in (
        select
          a.FeeId,
          a.PostedDate,
          a.Amount + a.AdjustedAmount NetAmount,
          a.Amount,
          a.AdjustedAmount,
          a.Description,
          (
            select count(*)
            from api.FeeTransactions b
            where b.FeeId = a.FeeId
          ) TransCount
        from api.Fees a
        where a.JobId = t_JobId
          and a.SystemGenerated = 'Y'
        ) loop
      begin

        if t_FeeIds(i.Feeid) = 'Y' then
          -- Leave fee alone
          null;
        end if;

      exception
        when no_data_found then
          -- Remove fee
          if i.PostedDate is null and i.TransCount = 0 then
            pkg_debug.putline('Remove extra fee: ' || i.FeeId);
            pkg_FeeDefinition.SetSystemGeneratedFlag(i.FeeId, 'N');
            api.pkg_FeeUpdate.Remove(i.FeeId);
          elsif (i.NetAmount <> 0) then
            -- Adjust fee to 0
            pkg_debug.putline('Adjust extra fee to 0: Id:' || i.FeeId || ', Amount:'
                || i.Amount || ', AdjAmount:' || i.AdjustedAmount || ', TransCount:'
                ||i.TransCount);
            t_AdjId := api.pkg_FeeUpdate.Adjust(i.FeeId, i.NetAmount * -1,
                substr(i.Description, 1, 49) || ' Adjustment', sysdate);
            pkg_FeeDefinition.SetSystemGeneratedFlag(t_AdjId);
          end if;

      end;
    end loop;
  end;

end pkg_PosseRegisteredProcedures;
/
