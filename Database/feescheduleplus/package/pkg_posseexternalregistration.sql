create or replace package pkg_PosseExternalRegistration is

  -- Author  : BRAD.VEENSTRA
  -- Created : 08/29/2013 12:51:10 PM
  -- Purpose : Register external objects for FeeSchedulePlus

  /*---------------------------------------------------------------------------
   * RegisterFeeSchedule() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFeeSchedule (
    a_PrimaryKey                 in out varchar2,
    a_Description                       varchar2,
    a_EffectiveStartDate                date,
    a_EffectiveEndDate                  date,
    a_CreatedDate                       date,
    a_CreatedBy                         varchar2,
    a_UpdatedDate                       date,
    a_UpdatedBy                         varchar2
  );

  /*---------------------------------------------------------------------------
   * RegisterDataSource() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterDataSource (
    a_PrimaryKey                 in out varchar2,
    a_Description                       varchar2,
    a_PosseViewName                     varchar2,
    a_FeeScheduleId                     number
  );

  /*---------------------------------------------------------------------------
   * RegisterFeeCategory() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFeeCategory (
    a_PrimaryKey                 in out varchar2,
    a_Condition                         varchar2,
    a_Description                       varchar2,
    a_CreatedDate                       date,
    a_CreatedBy                         varchar2,
    a_UpdatedDate                       date,
    a_UpdatedBy                         varchar2,
    a_FeeScheduleId                     number
  );

  /*---------------------------------------------------------------------------
   * RegisterFeeDefinition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFeeDefinition (
    a_PrimaryKey                 in out varchar2,
    a_Description                       varchar2,
    a_Condition                         varchar2,
    a_Amount                            varchar2,
    a_EffectiveDateColumnName           varchar2,
    a_FeeDescription                    varchar2,
    a_CreatedDate                       date,
    a_CreatedBy                         varchar2,
    a_UpdatedDate                       date,
    a_UpdatedBy                         varchar2,
    a_FeeScheduleId                     number,
    a_DataSourceId                      number,
    a_FeeCategoryId                     number,
    a_GLAccountNumber                   varchar2,
    a_SetResponsibleParty               varchar2,
    a_ResponsiblePartyRel               varchar2
  );

  /*---------------------------------------------------------------------------
   * RegisterFeeElement() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFeeElement (
    a_PrimaryKey                 in out varchar2,
    a_DataType                          varchar2,
    a_FeeElementType                    varchar2,
    a_CreatedDate                       date,
    a_CreatedBy                         varchar2,
    a_UpdatedDate                       date,
    a_UpdatedBy                         varchar2,
    a_IsGlobal                          varchar2,
    a_GlobalName                        varchar2,
    a_FeeScheduleId                     number,
    a_DataSourceId                      number
  );

  /*---------------------------------------------------------------------------
   * RegisterFieldElement() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFieldElement (
    a_PrimaryKey                 in out varchar2,
    a_ColumnName                        varchar2,
    a_FeeScheduleId                     number,
    a_FeeElementId                      number,
    a_CreatedDate                       date default null,
    a_CreatedBy                         varchar2 default null,
    a_UpdatedDate                       date default null,
    a_UpdatedBy                         varchar2 default null
  );

  /*---------------------------------------------------------------------------
   * RegisterExpressionElement() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterExpressionElement (
    a_PrimaryKey                 in out varchar2,
    a_Syntax                            varchar2,
    a_FeeScheduleId                     number,
    a_FeeElementId                      number,
    a_CreatedDate                       date default null,
    a_CreatedBy                         varchar2 default null,
    a_UpdatedDate                       date default null,
    a_UpdatedBy                         varchar2 default null
  );

end pkg_PosseExternalRegistration;

/

create or replace package body pkg_PosseExternalRegistration is

  /*---------------------------------------------------------------------------
   * RegisterFeeSchedule() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFeeSchedule (
    a_PrimaryKey                 in out varchar2,
    a_Description                       varchar2,
    a_EffectiveStartDate                date,
    a_EffectiveEndDate                  date,
    a_CreatedDate                       date,
    a_CreatedBy                         varchar2,
    a_UpdatedDate                       date,
    a_UpdatedBy                         varchar2
  ) is
  begin
    a_PrimaryKey := feescheduleplus_s.nextval;

    insert into feescheduleplus.feeschedule (
      feescheduleid,
      effectivestartdate,
      effectiveenddate,
      description,
      createddate,
      createdby,
      updateddate,
      updatedby
    ) values (
      a_PrimaryKey,
      a_EffectiveStartDate,
      a_EffectiveEndDate,
      a_Description,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      sysdate,
      api.pkg_securityquery.EffectiveUser
    );
  end RegisterFeeSchedule;

  /*---------------------------------------------------------------------------
   * RegisterDataSource() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterDataSource (
    a_PrimaryKey                 in out varchar2,
    a_Description                       varchar2,
    a_PosseViewName                     varchar2,
    a_FeeScheduleId                     number
  ) is
  begin
    a_PrimaryKey := feescheduleplus_s.nextval;

    insert into feescheduleplus.datasource (
      datasourceid,
      feescheduleid,
      posseviewname,
      description,
      createddate,
      createdby,
      updateddate,
      updatedby
    ) values (
      a_PrimaryKey,
      a_FeeScheduleId,
      a_PosseViewName,
      a_Description,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      sysdate,
      api.pkg_securityquery.EffectiveUser
    );
  end RegisterDataSource;

  /*---------------------------------------------------------------------------
   * RegisterFeeCategory() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFeeCategory (
    a_PrimaryKey                 in out varchar2,
    a_Condition                         varchar2,
    a_Description                       varchar2,
    a_CreatedDate                       date,
    a_CreatedBy                         varchar2,
    a_UpdatedDate                       date,
    a_UpdatedBy                         varchar2,
    a_FeeScheduleId                     number
  ) is
  begin
    a_PrimaryKey := feescheduleplus_s.nextval;

    insert into feescheduleplus.feecategory (
      feecategoryid,
      feescheduleid,
      condition,
      description,
      createddate,
      createdby,
      updateddate,
      updatedby
    ) values (
      a_PrimaryKey,
      a_FeeScheduleId,
      a_Condition,
      a_Description,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      sysdate,
      api.pkg_securityquery.EffectiveUser
    );
  end RegisterFeeCategory;

  /*---------------------------------------------------------------------------
   * RegisterFeeDefinition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFeeDefinition (
    a_PrimaryKey                 in out varchar2,
    a_Description                       varchar2,
    a_Condition                         varchar2,
    a_Amount                            varchar2,
    a_EffectiveDateColumnName           varchar2,
    a_FeeDescription                    varchar2,
    a_CreatedDate                       date,
    a_CreatedBy                         varchar2,
    a_UpdatedDate                       date,
    a_UpdatedBy                         varchar2,
    a_FeeScheduleId                     number,
    a_DataSourceId                      number,
    a_FeeCategoryId                     number,
    a_GLAccountNumber                   varchar2,
    a_SetResponsibleParty               varchar2,
    a_ResponsiblePartyRel               varchar2
  ) is
  begin
    a_PrimaryKey := feescheduleplus_s.nextval;

    insert into feescheduleplus.feedefinition (
      feedefinitionid,
      feescheduleid,
      description,
      condition,
      amount,
      effectivedatecolumnname,
      feedescription,
      createddate,
      createdby,
      updateddate,
      updatedby,
      datasourceid,
      feecategoryid,
      GLAccountNumber,
      responsiblepartygenerationtype,
      responsiblepartyrelationship
    ) values (
      a_PrimaryKey,
      a_FeeScheduleId,
      a_Description,
      a_Condition,
      a_Amount,
      a_EffectiveDateColumnName,
      a_FeeDescription,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      a_DataSourceId,
      a_FeeCategoryId,
      a_GLAccountNumber,
      a_SetResponsibleParty,
      a_ResponsiblePartyRel
    );
  end RegisterFeeDefinition;

   /*---------------------------------------------------------------------------
   * RegisterFeeElement() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFeeElement (
    a_PrimaryKey                 in out varchar2,
    a_DataType                          varchar2,
    a_FeeElementType                    varchar2,
    a_CreatedDate                       date,
    a_CreatedBy                         varchar2,
    a_UpdatedDate                       date,
    a_UpdatedBy                         varchar2,
    a_IsGlobal                          varchar2,
    a_GlobalName                        varchar2,
    a_FeeScheduleId                     number,
    a_DataSourceId                      number
  ) is
  begin
    a_PrimaryKey := feescheduleplus_s.nextval;

    insert into feescheduleplus.feeelement (
      feeelementid,
      feescheduleid,
      datasourceid,
      datatype,
      feeelementtype,
      createddate,
      createdby,
      updateddate,
      updatedby,
      isglobal,
      globalname
    ) values (
      a_PrimaryKey,
      a_FeeScheduleId,
      a_DataSourceId,
      a_DataType,
      a_FeeElementType,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      a_IsGlobal,
      a_GlobalName
    );
  end RegisterFeeElement;

  /*---------------------------------------------------------------------------
   * RegisterFieldElement() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterFieldElement (
    a_PrimaryKey                 in out varchar2,
    a_ColumnName                        varchar2,
    a_FeeScheduleId                     number,
    a_FeeElementId                      number,
    a_CreatedDate                       date default null,
    a_CreatedBy                         varchar2 default null,
    a_UpdatedDate                       date default null,
    a_UpdatedBy                         varchar2 default null
  ) is
  begin

    a_PrimaryKey := feescheduleplus_s.nextval;

    insert into feescheduleplus.fieldelement (
      fieldelementid,
      feescheduleid,
      columnname,
      createddate,
      createdby,
      updateddate,
      updatedby,
      feeelementid
    ) values (
      a_PrimaryKey,
      a_FeeScheduleId,
      a_ColumnName,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      a_FeeElementId
    );
  end RegisterFieldElement;

  /*---------------------------------------------------------------------------
   * RegisterExpressionElement() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegisterExpressionElement (
    a_PrimaryKey                 in out varchar2,
    a_Syntax                            varchar2,
    a_FeeScheduleId                     number,
    a_FeeElementId                      number,
    a_CreatedDate                       date default null,
    a_CreatedBy                         varchar2 default null,
    a_UpdatedDate                       date default null,
    a_UpdatedBy                         varchar2 default null
  ) is
  begin
    a_PrimaryKey := feescheduleplus_s.nextval;

    insert into feescheduleplus.expressionelement (
      expressionelementid,
      feescheduleid,
      syntax,
      createddate,
      createdby,
      updateddate,
      updatedby,
      feeelementid
    ) values (
      a_PrimaryKey,
      a_FeeScheduleId,
      a_Syntax,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      sysdate,
      api.pkg_securityquery.EffectiveUser,
      a_FeeElementId
    );
  end RegisterExpressionElement;

end pkg_PosseExternalRegistration;

/

