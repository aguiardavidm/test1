create or replace package pkg_CloneUtilities is

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *   Utilities to facilitate the cloning of a Fee Schedule
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;
  subtype udt_FeeScheduleId is FeeSchedule.FeeScheduleId%type;
  type udt_ClonedObject is record (
    id                                  udt_Id,
    PosseObjectId                       udt_PosseId
  );
  type udt_ClonedObjects is table of udt_ClonedObject index by pls_integer;

  /*---------------------------------------------------------------------------
   * PosseCloneFeeSchedule()
   *   On a new o_FeeSchedule object, clone all of the data and
   *   supporting objects for the fee schedule based on the fee schedule
   *   specified by the "a_FromFeeScheduleId" parameter.
   *-------------------------------------------------------------------------*/
  procedure PosseCloneFeeSchedule (
    a_ObjectId                          udt_PosseId,
    a_AsOfDate                          date,
    a_FromFeeScheduleId                 number,
    a_ToFeeScheduleId                   number,
    a_Description                       varchar2,
    a_EffectiveStartDate                date,
    a_EffectiveEndDate                  date
  );

end pkg_CloneUtilities;

/

create or replace package body pkg_CloneUtilities is

  /*---------------------------------------------------------------------------
   * CloneFeeSchedule() -- PRIVATE
   *   If a new FeeScheduleId has not been provided to the procedure, insert
   *   a new row into the FeeSchedule table. Otherwise, update the new fee
   *   schedule with the arguments to this procedure and values from the old
   *   fee schedule.
   *-------------------------------------------------------------------------*/
  procedure CloneFeeSchedule (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_NewFeeScheduleId           in out udt_FeeScheduleId,
    a_Description                       FeeSchedule.Description%type,
    a_EffectiveStartDate                date,
    a_EffectiveEndDate                  date
  ) is
    t_NewFeeScheduleId                  udt_FeeScheduleId;
  begin
    if a_NewFeeScheduleId is null then
      a_NewFeeScheduleId := FeeSchedulePlus_s.nextval;

      insert into FeeSchedule (
        FeeScheduleId,
        EffectiveStartDate,
        EffectiveEndDate,
        Description,
        PendingChangesSchema,
        createddate,
        createdby,
        updateddate,
        updatedby
      ) select
          a_NewFeeScheduleId,
          a_EffectiveStartDate,
          a_EffectiveEndDate,
          a_Description,
          PendingChangesSchema,
          sysdate,
          api.pkg_securityquery.EffectiveUser,
          sysdate,
          api.pkg_securityquery.EffectiveUser
        from FeeSchedule
        where FeeScheduleId = a_FromFeeScheduleId;
    else
      update FeeSchedule set
        Description = a_Description,
        EffectiveStartDate = a_EffectiveStartDate,
        EffectiveEndDate = a_EffectiveEndDate,
        PendingChangesSchema = (
            select PendingChangesSchema
            from FeeSchedule
            where FeeScheduleId = a_FromFeeScheduleId
            ),
        UpdatedDate = sysdate,
        UpdatedBy = api.pkg_securityquery.EffectiveUser
      where FeeScheduleId = a_NewFeeScheduleId;
    end if;
  end CloneFeeSchedule;

  /*---------------------------------------------------------------------------
   * CloneDataSources() -- PRIVATE
   *   Clone all of the data sources from the source fee schedule to the
   *   destination fee schedule. If a_RegisterPosseObjects is true, it will
   *   register the data sources as Posse objects, creating a relationship to
   *   the Fee Schedule object. Returns a udt_ClonedObjects list of
   *   DataSourceIds/PosseObjectIds.
   *-------------------------------------------------------------------------*/
  function CloneDataSources (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_RegisterPosseObjects              boolean default false
  ) return udt_ClonedObjects is
    t_DataSources                       udt_ClonedObjects;
    t_NewDataSourceId                   udt_FeeScheduleId;
    t_FeeScheduleEndPointId             udt_Id;
    t_PosseObjectId                     udt_Id;
    t_RelId                             udt_Id;
  begin
    if a_RegisterPosseObjects then
      t_FeeScheduleEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_DataSource', 'FeeSchedule');
    end if;

    for ds in (
        select
          DataSourceId,
          PosseViewName,
          Description
        from DataSource
        where FeeScheduleId = a_FromFeeScheduleId
        ) loop
      insert into DataSource(
        DataSourceId,
        FeeScheduleId,
        PosseViewName,
        Description,
        createddate,
        createdby,
        updateddate,
        updatedby
      ) values (
        FeeSchedulePlus_s.nextval,
        a_ToFeeSchedule.id,
        ds.PosseViewName,
        ds.Description,
        sysdate,
        api.pkg_securityquery.EffectiveUser,
        sysdate,
        api.pkg_securityquery.EffectiveUser
      ) returning DataSourceId into t_NewDataSourceId;

      t_DataSources(ds.DataSourceId).id := t_NewDataSourceId;

      if a_RegisterPosseObjects then
        t_PosseObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
            'o_DataSource', t_NewDataSourceId);
        t_DataSources(ds.DataSourceId).PosseObjectId := t_PosseObjectId;
        t_RelId := api.pkg_RelationshipUpdate.New(t_FeeScheduleEndPointId,
            t_PosseObjectId, a_ToFeeSchedule.PosseObjectId);
      end if;
    end loop;

    return t_DataSources;
  end CloneDataSources;

  /*---------------------------------------------------------------------------
   * CloneFeeElements() -- PRIVATE
   *   Clone all of the fee elements from the source fee schedule to the
   *   destination fee schedule. Requires a udt_ClonedObjects list of Data
   *   Sources. If a_RegisterPosseObjects is true, it will register the fee
   *   elements as Posse objects, creating relationships to the Fee Schedule
   *   and data source objects. Returns a udt_ClonedObjects list of
   *   FeeElementIds/PosseObjectIds.
   *-------------------------------------------------------------------------*/
  function CloneFeeElements (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_DataSources                       udt_ClonedObjects,
    a_RegisterPosseObjects              boolean default false
  ) return udt_ClonedObjects is
    t_FeeElements                       udt_ClonedObjects;
    t_NewFeeElementId                   udt_FeeScheduleId;
    t_FeeScheduleEndPointId             udt_Id;
    t_DataSourceEndPointId              udt_Id;
    t_PosseObjectId                     udt_Id;
    t_RelId                             udt_Id;
  begin
    if a_RegisterPosseObjects then
      t_FeeScheduleEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FeeElement', 'FeeSchedule');
      t_DataSourceEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FeeElement', 'DataSource');
    end if;

    for fe in (
        select
          FeeElementId,
          DataSourceId,
          DataType,
          FeeElementType,
          IsGlobal,
          GlobalName
        from FeeElement
        where FeeScheduleId = a_FromFeeScheduleId
        ) loop
      insert into FeeElement(
        FeeElementId,
        FeeScheduleId,
        DataSourceId,
        DataType,
        FeeElementType,
        IsGlobal,
        GlobalName,
        createddate,
        createdby,
        updateddate,
        updatedby
      ) values (
        FeeSchedulePlus_s.nextval,
        a_ToFeeSchedule.id,
        decode(fe.DataSourceId, null, null, a_DataSources(fe.DataSourceId).id),
        fe.DataType,
        fe.FeeElementType,
        fe.IsGlobal,
        fe.GlobalName,
        sysdate,
        api.pkg_securityquery.EffectiveUser,
        sysdate,
        api.pkg_securityquery.EffectiveUser
      ) returning FeeElementId into t_NewFeeElementId;

      t_FeeElements(fe.FeeElementId).id := t_NewFeeElementId;

      if a_RegisterPosseObjects then
        t_PosseObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
            'o_FeeElement', t_NewFeeElementId);
        t_FeeElements(fe.FeeElementId).PosseObjectId := t_PosseObjectId;
        t_RelId := api.pkg_RelationshipUpdate.New(t_FeeScheduleEndPointId,
            t_PosseObjectId, a_ToFeeSchedule.PosseObjectId);
        if fe.DataSourceId is not null then
          t_RelId := api.pkg_RelationshipUpdate.New(t_DataSourceEndPointId,
              t_PosseObjectId, a_DataSources(fe.DataSourceId).PosseObjectId);
        end if;
      end if;
    end loop;

    return t_FeeElements;
  end CloneFeeElements;

  /*---------------------------------------------------------------------------
   * CloneFieldElements() -- PRIVATE
   *   Clone all of the field elements from the source fee schedule to the
   *   destination fee schedule. Requires a udt_ClonedObjects list of Fee
   *   Elements. If a_RegisterPosseObjects is true, it will register the
   *   field elements as Posse objects, creating relationships to the Fee
   *   Schedule and Fee Element objects. Returns a udt_ClonedObjects list of
   *   FieldElementIds/PosseObjectIds.
   *-------------------------------------------------------------------------*/
  function CloneFieldElements (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_FeeElements                       udt_ClonedObjects,
    a_RegisterPosseObjects              boolean default false
  ) return udt_ClonedObjects is
    t_FieldElements                     udt_ClonedObjects;
    t_NewFieldElementId                 udt_FeeScheduleId;
    t_FeeScheduleEndPointId             udt_Id;
    t_FeeElementEndPointId              udt_Id;
    t_PosseObjectId                     udt_Id;
    t_RelId                             udt_Id;
  begin
    if a_RegisterPosseObjects then
      t_FeeScheduleEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FieldElement', 'FeeSchedule');
      t_FeeElementEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FieldElement', 'FeeElement');
    end if;

    for fe in (
        select
          FieldElementId,
          ColumnName,
          FeeElementId
        from FieldElement
        where FeeScheduleId = a_FromFeeScheduleId
        ) loop
      insert into FieldElement(
        FieldElementId,
        FeeScheduleId,
        ColumnName,
        FeeElementId,
        createddate,
        createdby,
        updateddate,
        updatedby
      ) values (
        FeeSchedulePlus_s.nextval,
        a_ToFeeSchedule.id,
        fe.ColumnName,
        decode(fe.FeeElementId, null, null, a_FeeElements(fe.FeeElementId).id),
        sysdate,
        api.pkg_securityquery.EffectiveUser,
        sysdate,
        api.pkg_securityquery.EffectiveUser
      ) returning FieldElementId into t_NewFieldElementId;

      t_FieldElements(fe.FieldElementId).id := t_NewFieldElementId;

      if a_RegisterPosseObjects then
        t_PosseObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
            'o_FieldElement', t_NewFieldElementId);
        t_FieldElements(fe.FieldElementId).PosseObjectId := t_PosseObjectId;
        t_RelId := api.pkg_RelationshipUpdate.New(t_FeeScheduleEndPointId,
            t_PosseObjectId, a_ToFeeSchedule.PosseObjectId);
        if fe.FeeElementId is not null then
          t_RelId := api.pkg_RelationshipUpdate.New(t_FeeElementEndPointId,
              t_PosseObjectId, a_FeeElements(fe.FeeElementId).PosseObjectId);
        end if;
      end if;
    end loop;

    return t_FieldElements;
  end CloneFieldElements;

  /*---------------------------------------------------------------------------
   * CloneFieldElements() -- PRIVATE
   *   Procedural wrapper around the CloneFieldElements function
   *-------------------------------------------------------------------------*/
  procedure CloneFieldElements (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_FeeElements                       udt_ClonedObjects,
    a_RegisterPosseObjects              boolean default false
  ) is
    t_FieldElements                     udt_ClonedObjects;
  begin
    t_FieldElements := CloneFieldElements(a_FromFeeScheduleId, a_ToFeeSchedule,
        a_FeeElements, a_RegisterPosseObjects);
  end CloneFieldElements;

  /*---------------------------------------------------------------------------
   * CloneExpressionElements() -- PRIVATE
   *   Clone all of the expression elements from the source fee schedule to
   *   the destination fee schedule. Requires a udt_ClonedObjects list of Fee
   *   Elements. If a_RegisterPosseObjects is true, it will register the
   *   expression elements as Posse objects, creating relationships to the
   *   Fee Schedule and Fee Element objects. Returns a udt_ClonedObjects list
   *   of ExpressionElementIds/PosseObjectIds.
   *-------------------------------------------------------------------------*/
  function CloneExpressionElements (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_FeeElements                       udt_ClonedObjects,
    a_RegisterPosseObjects              boolean default false
  ) return udt_ClonedObjects is
    t_ExpressionElements                udt_ClonedObjects;
    t_NewExpressionElementId            udt_FeeScheduleId;
    t_FeeScheduleEndPointId             udt_Id;
    t_FeeElementEndPointId              udt_Id;
    t_PosseObjectId                     udt_Id;
    t_RelId                             udt_Id;
  begin
    if a_RegisterPosseObjects then
      t_FeeScheduleEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_ExpressionElement', 'FeeSchedule');
      t_FeeElementEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_ExpressionElement', 'FeeElement');
    end if;

    for ee in (
        select
          ExpressionElementId,
          Syntax,
          FeeElementId
        from ExpressionElement
        where FeeScheduleId = a_FromFeeScheduleId
        ) loop
      insert into ExpressionElement(
        ExpressionElementId,
        FeeScheduleId,
        Syntax,
        FeeElementId,
        createddate,
        createdby,
        updateddate,
        updatedby
      ) values (
        FeeSchedulePlus_s.nextval,
        a_ToFeeSchedule.id,
        ee.Syntax,
        decode(ee.FeeElementId, null, null, a_FeeElements(ee.FeeElementId).id),
        sysdate,
        api.pkg_securityquery.EffectiveUser,
        sysdate,
        api.pkg_securityquery.EffectiveUser
      ) returning ExpressionElementId into t_NewExpressionElementId;

      t_ExpressionElements(ee.ExpressionElementId).id := t_NewExpressionElementId;

      if a_RegisterPosseObjects then
        t_PosseObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
            'o_ExpressionElement', t_NewExpressionElementId);
        t_ExpressionElements(ee.ExpressionElementId).PosseObjectId := t_PosseObjectId;
        t_RelId := api.pkg_RelationshipUpdate.New(t_FeeScheduleEndPointId,
            t_PosseObjectId, a_ToFeeSchedule.PosseObjectId);
        if ee.FeeElementId is not null then
          t_RelId := api.pkg_RelationshipUpdate.New(t_FeeElementEndPointId,
              t_PosseObjectId, a_FeeElements(ee.FeeElementId).PosseObjectId);
        end if;
      end if;
    end loop;

    return t_ExpressionElements;
  end CloneExpressionElements;

  /*---------------------------------------------------------------------------
   * CloneExpressionElements() -- PRIVATE
   *   Procedural wrapper around the CloneExpressionElements function
   *-------------------------------------------------------------------------*/
  procedure CloneExpressionElements (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_FeeElements                       udt_ClonedObjects,
    a_RegisterPosseObjects              boolean default false
  ) is
    t_ExpressionElements                udt_ClonedObjects;
  begin
    t_ExpressionElements := CloneExpressionElements(a_FromFeeScheduleId,
        a_ToFeeSchedule, a_FeeElements, a_RegisterPosseObjects);
  end CloneExpressionElements;

  /*---------------------------------------------------------------------------
   * CloneFeeCategories() -- PRIVATE
   *   Clone all of the fee categories from the source fee schedule to the
   *   destination fee schedule. If a_RegisterPosseObjects is true, it will
   *   register the fee categories as Posse objects, creating a relationship
   *   to the Fee Schedule object. Returns a udt_ClonedObjects list of
   *   FeeCategoryIds/PosseObjectIds.
   *-------------------------------------------------------------------------*/
  function CloneFeeCategories (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_RegisterPosseObjects              boolean default false
  ) return udt_ClonedObjects is
    t_FeeCategories                     udt_ClonedObjects;
    t_NewFeeCategoryId                  udt_FeeScheduleId;
    t_FeeScheduleEndPointId             udt_Id;
    t_PosseObjectId                     udt_Id;
    t_RelId                             udt_Id;
  begin
    if a_RegisterPosseObjects then
      t_FeeScheduleEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FeeCategory', 'FeeSchedule');
    end if;

    for fc in (
        select
          FeeCategoryId,
          Condition,
          Description
        from FeeCategory
        where FeeScheduleId = a_FromFeeScheduleId
        ) loop
      insert into FeeCategory(
        FeeCategoryId,
        FeeScheduleId,
        Condition,
        Description,
        createddate,
        createdby,
        updateddate,
        updatedby
      ) values (
        FeeSchedulePlus_s.nextval,
        a_ToFeeSchedule.id,
        fc.Condition,
        fc.Description,
        sysdate,
        api.pkg_securityquery.EffectiveUser,
        sysdate,
        api.pkg_securityquery.EffectiveUser
      ) returning FeeCategoryId into t_NewFeeCategoryId;

      t_FeeCategories(fc.FeeCategoryId).id := t_NewFeeCategoryId;

      if a_RegisterPosseObjects then
        t_PosseObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
            'o_FeeCategory', t_NewFeeCategoryId);
        t_FeeCategories(fc.FeeCategoryId).PosseObjectId := t_PosseObjectId;
        t_RelId := api.pkg_RelationshipUpdate.New(t_FeeScheduleEndPointId,
            t_PosseObjectId, a_ToFeeSchedule.PosseObjectId);
      end if;
    end loop;

    return t_FeeCategories;
  end CloneFeeCategories;

  /*---------------------------------------------------------------------------
   * CloneFeeDefinitions() -- PRIVATE
   *   Clone all of the fee definitions from the source fee schedule to the
   *   destination fee schedule. Requires udt_ClonedObjects lists of Data
   *   Sources and Fee Categories. If a_RegisterPosseObjects is true, it
   *   will register the fee definitions as Posse objects, creating
   *   relationships to the Fee Schedule, Data Source, Fee Category, and Fee
   *   Type Group objects. Returns a udt_ClonedObjects list of
   *   FeeDefinitionIds/PosseObjectIds.
   *-------------------------------------------------------------------------*/
  function CloneFeeDefinitions (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_DataSources                       udt_ClonedObjects,
    a_FeeCategories                     udt_ClonedObjects,
    a_RegisterPosseObjects              boolean default false
  ) return udt_ClonedObjects is
    t_FeeDefinitions                    udt_ClonedObjects;
    t_NewFeeElementId                   udt_FeeScheduleId;
    t_FeeScheduleEndPointId             udt_Id;
    t_DataSourceEndPointId              udt_Id;
    t_FeeCategoryEndPointId             udt_Id;
    t_FeeTypeGroupEndPointId            udt_Id;
    t_PosseObjectId                     udt_Id;
    t_RelId                             udt_Id;
  begin
    if a_RegisterPosseObjects then
      t_FeeScheduleEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FeeDefinition', 'FeeSchedule');
      t_DataSourceEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FeeDefinition', 'DataSource');
      t_FeeCategoryEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FeeDefinition', 'FeeCategory');
      t_FeeTypeGroupEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          'o_FeeDefinition', 'FeeTypeGroup');
    end if;

    for fd in (
        select
          FeeDefinitionId,
          DataSourceId,
          Description,
          Condition,
          Amount,
          EffectiveDateColumnName,
          FeeCategoryId,
          FeeDescription,
          GLAccountNumber,
          RESPONSIBLEPARTYGENERATIONTYPE,
          ResponsiblePartyRelationship
        from FeeDefinition
        where FeeScheduleId = a_FromFeeScheduleId
        ) loop
      insert into FeeDefinition(
        FeeDefinitionId,
        FeeScheduleId,
        DataSourceId,
        Description,
        Condition,
        Amount,
        EffectiveDateColumnName,
        FeeCategoryId,
        FeeDescription,
        createddate,
        createdby,
        updateddate,
        updatedby,
        GLAccountNumber,
        RESPONSIBLEPARTYGENERATIONTYPE,
        ResponsiblePartyRelationship
      ) values (
        FeeSchedulePlus_s.nextval,
        a_ToFeeSchedule.id,
        a_DataSources(fd.DataSourceId).id,
        fd.Description,
        fd.Condition,
        fd.Amount,
        fd.EffectiveDateColumnName,
        decode(fd.FeeCategoryId, null, null, a_FeeCategories(fd.FeeCategoryId).id),
        fd.FeeDescription,
        sysdate,
        api.pkg_securityquery.EffectiveUser,
        sysdate,
        api.pkg_securityquery.EffectiveUser,
        fd.Glaccountnumber,
        fd.Responsiblepartygenerationtype,
        fd.Responsiblepartyrelationship
      ) returning FeeDefinitionId into t_NewFeeElementId;

      t_FeeDefinitions(fd.FeeDefinitionId).id := t_NewFeeElementId;

      if a_RegisterPosseObjects then
        t_PosseObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
            'o_FeeDefinition', t_NewFeeElementId);
        t_FeeDefinitions(fd.FeeDefinitionId).PosseObjectId := t_PosseObjectId;
        t_RelId := api.pkg_RelationshipUpdate.New(t_FeeScheduleEndPointId,
            t_PosseObjectId, a_ToFeeSchedule.PosseObjectId);
        t_RelId := api.pkg_RelationshipUpdate.New(t_DataSourceEndPointId,
            t_PosseObjectId, a_DataSources(fd.DataSourceId).PosseObjectId);
        if fd.FeeCategoryId is not null then
          t_RelId := api.pkg_RelationshipUpdate.New(t_FeeCategoryEndPointId,
              t_PosseObjectId, a_FeeCategories(fd.FeeCategoryId).PosseObjectId);
        end if;
      end if;
    end loop;

    return t_FeeDefinitions;
  end CloneFeeDefinitions;

  /*---------------------------------------------------------------------------
   * CloneFeeDefinitions() -- PRIVATE
   *   Procedural Wrapper around the CloneFeeDefinitions function
   *-------------------------------------------------------------------------*/
  procedure CloneFeeDefinitions (
    a_FromFeeScheduleId                 udt_FeeScheduleId,
    a_ToFeeSchedule                     udt_ClonedObject,
    a_DataSources                       udt_ClonedObjects,
    a_FeeCategories                     udt_ClonedObjects,
    a_RegisterPosseObjects              boolean default false
  ) is
    t_FeeDefinitions                    udt_ClonedObjects;
  begin
    t_FeeDefinitions := CloneFeeDefinitions(a_FromFeeScheduleId, a_ToFeeSchedule,
        a_DataSources, a_FeeCategories, a_RegisterPosseObjects);
  end CloneFeeDefinitions;

  /*---------------------------------------------------------------------------
   * PosseCloneFeeSchedule() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PosseCloneFeeSchedule (
    a_ObjectId                          udt_PosseId,
    a_AsOfDate                          date,
    a_FromFeeScheduleId                 number,
    a_ToFeeScheduleId                   number,
    a_Description                       varchar2,
    a_EffectiveStartDate                date,
    a_EffectiveEndDate                  date
  ) is
    t_FeeScheduleId                     udt_Id;
    t_FeeSchedule                       udt_ClonedObject;
    t_DataSources                       udt_ClonedObjects;
    t_FeeElements                       udt_ClonedObjects;
    t_FeeCategories                     udt_ClonedObjects;
  begin
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'CloneFeeSchedule') = 'Y' then
      if a_ToFeeScheduleId is null then
        api.pkg_Errors.RaiseError(-20000, 'PosseCloneFeeSchedule must be ' ||
            'called from a new o_FeeSchedule object.');
      else
        t_FeeScheduleId := a_ToFeeScheduleId;
        CloneFeeSchedule(a_FromFeeScheduleId, t_FeeScheduleId, a_Description,
            a_EffectiveStartDate, a_EffectiveEndDate);
      end if;

      t_FeeSchedule.id := t_FeeScheduleId;
      t_FeeSchedule.PosseObjectId := a_ObjectId;

      -- Clone Data Sources
      t_DataSources := CloneDataSources(a_FromFeeScheduleId, t_FeeSchedule, true);

      -- Clone Fee Elements
      t_FeeElements := CloneFeeElements(a_FromFeeScheduleId, t_FeeSchedule,
          t_DataSources, true);

      -- Clone Field Elements
      CloneFieldElements(a_FromFeeScheduleId, t_FeeSchedule, t_FeeElements, true);

      -- Clone Expression Elements
      CloneExpressionElements(a_FromFeeScheduleId, t_FeeSchedule, t_FeeElements, true);

      -- Clone Fee Categories
      t_FeeCategories := CloneFeeCategories(a_FromFeeScheduleId, t_FeeSchedule, true);

      -- Clone Fee Definitions
      CloneFeeDefinitions(a_FromFeeScheduleId, t_FeeSchedule, t_DataSources,
          t_FeeCategories, true);

      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'CloneFeeSchedule', 'N');
    end if;

  end PosseCloneFeeSchedule;

end pkg_CloneUtilities;

/

