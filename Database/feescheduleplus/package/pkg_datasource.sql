create or replace package pkg_DataSource as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;
  subtype udt_PosseObjectDefTypeName is pkg_FeeSchedulePlus.udt_PosseObjectDefTypeName;

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_DataSourceId              udt_Id,
    a_Data                  out DataSource%rowtype
  );

  /*---------------------------------------------------------------------------
   * GetDataByPosseViewName() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetDataByPosseViewName (
    a_FeeScheduleId             udt_Id,
    a_PosseViewName             DataSource.PosseViewName%Type,
    a_Data                  out DataSource%rowtype
  );

  /*---------------------------------------------------------------------------
   * PosseObjectDefType() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function PosseObjectDefTypeName (
    a_FeeScheduleId             udt_Id,
    a_DataSourceId              udt_Id
  ) return udt_PosseObjectDefTypeName;

end pkg_DataSource;

/

grant execute
on pkg_datasource
to feescheduleplususer;

create or replace package body pkg_DataSource as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_DataSourceId              udt_Id,
    a_Data                  out DataSource%rowtype
  ) is
  begin
    select *
      into a_Data
      from DataSource
     where DataSourceId = a_DataSourceId
       and FeeScheduleId = a_FeeScheduleId;
  end;

  /*---------------------------------------------------------------------------
   * GetDataByPosseViewName() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetDataByPosseViewName (
    a_FeeScheduleId             udt_Id,
    a_PosseViewName             DataSource.PosseViewName%Type,
    a_Data                  out DataSource%rowtype
  ) is
  begin
    select *
      into a_Data
      from DataSource
     where PosseViewName = a_PosseViewName
       and FeeScheduleId = a_FeeScheduleId;
  end;

  /*---------------------------------------------------------------------------
   * PosseObjectDefType() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function PosseObjectDefTypeName (
    a_FeeScheduleId             udt_Id,
    a_DataSourceId              udt_Id
  ) return udt_PosseObjectDefTypeName is
    t_ObjectDefTypeName         udt_PosseObjectDefTypeName;
  begin
    begin
      select ot.Name
        into t_ObjectDefTypeName
        from api.ObjectDefTypes ot,
             api.ObjectDefs od,
             DataSource ds
       where ds.DataSourceId = a_DataSourceId
         and ds.FeeScheduleId = a_FeeScheduleId
         and od.Name = ds.PosseViewName
         and ot.ObjectDefTypeId = od.ObjectDefTypeId;
    exception when no_data_found then
      raise_application_error(-20000, 'Data source: ' || a_DataSourceId || ' has an invalid Posse View Name defined');
    end;

    return t_ObjectDefTypeName;
  end;

end pkg_DataSource;

/

