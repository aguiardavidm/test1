create or replace package pkg_FieldElement as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;

  /*---------------------------------------------------------------------------
   * Value() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function Value (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * NumberValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NumberValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId
  ) return number;

  /*---------------------------------------------------------------------------
   * DateValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DateValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId
  ) return date;

  /*---------------------------------------------------------------------------
   * BooleanValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function BooleanValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId
  ) return boolean;

end pkg_FieldElement;

/

grant execute
on pkg_fieldelement
to feescheduleplususer;

create or replace package body pkg_FieldElement as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * ColumnName() -- PRIVATE
   *-------------------------------------------------------------------------*/
  function ColumnName (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id
  ) return varchar2 is
    t_ColumnName                FieldElement.ColumnName%type;
  begin
    select ColumnName
      into t_ColumnName
      from FieldElement
     where FeeElementId = a_FeeElementId
       and FeeScheduleId = a_FeeScheduleId;

    return t_ColumnName;
  end;

  /*---------------------------------------------------------------------------
   * Value() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function Value (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId
  ) return varchar2 is
  begin
    return api.pkg_ColumnQuery.Value(a_PosseObjectId, ColumnName(a_FeeScheduleId, a_FeeElementId));
  end;

  /*---------------------------------------------------------------------------
   * NumberValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NumberValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId
  ) return number is
  begin
    return api.pkg_ColumnQuery.NumericValue(a_PosseObjectId, ColumnName(a_FeeScheduleId, a_FeeElementId));
  end;

  /*---------------------------------------------------------------------------
   * DateValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DateValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId
  ) return date is
  begin
    return api.pkg_ColumnQuery.DateValue(a_PosseObjectId, ColumnName(a_FeeScheduleId, a_FeeElementId));
  end;

  /*---------------------------------------------------------------------------
   * BooleanValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function BooleanValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId
  ) return boolean is
  begin
    return api.pkg_ColumnQuery.Value(a_PosseObjectId, ColumnName(a_FeeScheduleId, a_FeeElementId)) = 'Y';
  end;

end pkg_FieldElement;

/

