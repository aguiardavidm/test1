create or replace package pkg_FeeCategory as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;

  /*---------------------------------------------------------------------------
   * EvaluateCondition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function EvaluateCondition (
    a_FeeScheduleId             udt_Id,
    a_FeeCategoryId             udt_Id,
    a_PosseObjectId             udt_Id,
    a_DataSourceId              udt_Id
  ) return boolean;

end pkg_FeeCategory;

/

grant execute
on pkg_feecategory
to feescheduleplususer;

create or replace package body pkg_FeeCategory as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;

  /*---------------------------------------------------------------------------
   * GetData() -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId            udt_Id,
    a_FeeCategoryId            udt_Id,
    a_Data                  out FeeCategory%rowtype
  ) is
  begin
    select *
      into a_Data
      from FeeCategory
     where FeeCategoryId = a_FeeCategoryId
       and FeeScheduleId = a_FeeScheduleId;
  end;

  /*---------------------------------------------------------------------------
   * EvaluateCondition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function EvaluateCondition (
    a_FeeScheduleId            udt_Id,
    a_FeeCategoryId           udt_Id,
    a_PosseObjectId           udt_Id,
    a_DataSourceId            udt_Id
  ) return boolean is
    t_FeeCategory             FeeCategory%rowtype;
  begin
    GetData(a_FeeScheduleId, a_FeeCategoryId, t_FeeCategory);

    if t_FeeCategory.Condition is null then
      return true;
    else
      return nvl(pkg_ExpressionColumn.BooleanValue(pkg_ExpressionColumn.gc_FeeCatCondition, a_FeeScheduleId, t_FeeCategory.Condition, a_PosseObjectId, a_DataSourceId), true);
    end if;
  end;

end pkg_FeeCategory;

/

