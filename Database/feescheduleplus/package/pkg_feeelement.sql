create or replace package pkg_FeeElement as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;


  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_Data                  out FeeElement%rowtype
  );

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_DataSourceId              udt_Id,
    a_GlobalName                FeeElement.GlobalName%type,
    a_Data                  out FeeElement%rowtype
  );

  /*---------------------------------------------------------------------------
   * Value() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function Value (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id default null
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * NumberValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NumberValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id default null
  ) return number;

  /*---------------------------------------------------------------------------
   * DateValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DateValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id default null
  ) return date;

  /*---------------------------------------------------------------------------
   * BooleanValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function BooleanValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id default null
  ) return boolean;

 /*---------------------------------------------------------------------------
   * Constructor()
   *   Wrapper for the Constructor event on the Fee Element Object.
   *-------------------------------------------------------------------------*/
  procedure Constructor(
    a_ObjectId                          udt_PosseId,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CreateRelToFeeSchedule() -- PUBLIC
   *   Creates a stored relationship between a fee element and fee schedule.
   *-------------------------------------------------------------------------*/
  procedure CreateRelToFeeSchedule(
    a_RelationshipId                    udt_PosseId,
    a_AsOfDate                          date default null,
    a_FeeScheduleObjectId               udt_PosseId default null
  );

  /*---------------------------------------------------------------------------
   * PreVerify()
   *   Wrapper for the Pre Verify event on the Fee Element Object.
   *-------------------------------------------------------------------------*/
  procedure PreVerify (
    a_ObjectId                          udt_PosseId,
    a_AsOfDate                          date
  );

end pkg_FeeElement;

/

grant execute
on pkg_feeelement
to feescheduleplususer;

create or replace package body pkg_FeeElement as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_Data                  out FeeElement%rowtype
  ) is
  begin
    select *
      into a_Data
      from FeeElement
     where FeeElementId = a_FeeElementId
       and FeeScheduleId = a_FeeScheduleId;
  end;

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_DataSourceId              udt_Id,
    a_GlobalName                FeeElement.GlobalName%type,
    a_Data                  out FeeElement%rowtype
  ) is
  begin
    select *
      into a_Data
      from FeeElement
     where GlobalName = a_GlobalName
       and DataSourceId = a_DataSourceId
       and FeeScheduleId = a_FeeScheduleId;
  end;

  /*---------------------------------------------------------------------------
   * GetFeeElementType() -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure GetFeeElementType (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_FeeElementType             out  varchar2
  ) is
  begin
    select FeeElementType
      into a_FeeElementType
      from FeeElement
     where FeeElementId = a_FeeElementId
       and FeeScheduleId = a_FeeScheduleId;
  end;

  /*---------------------------------------------------------------------------
   * Value() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function Value (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id default null
  ) return varchar2 is
    t_FeeElementType            FeeElement.FeeElementType%type;
  begin
    if a_PosseObjectId is null then
      return null;
    end if;

    GetFeeElementType(a_FeeScheduleId, a_FeeElementId, t_FeeElementType);
    if t_FeeElementType = 'Expression' then
      return pkg_ExpressionElement.Value(a_FeeScheduleId, a_FeeElementId,
          a_PosseObjectId, a_DataSourceId);
    elsif t_FeeElementType = 'Field' then
      return pkg_FieldElement.Value(a_FeeScheduleId, a_FeeElementId,
          a_PosseObjectId);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * NumberValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function NumberValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id default null
  ) return number is
    t_FeeElementType            FeeElement.FeeElementType%type;
  begin
    if a_PosseObjectId is null then
      return null;
    end if;

    GetFeeElementType(a_FeeScheduleId, a_FeeElementId, t_FeeElementType);
    if t_FeeElementType = 'Expression' then
      return pkg_ExpressionElement.NumberValue(a_FeeScheduleId, a_FeeElementId,
          a_PosseObjectId, a_DataSourceId);
    elsif t_FeeElementType = 'Field' then
      return pkg_FieldElement.NumberValue(a_FeeScheduleId, a_FeeElementId,
          a_PosseObjectId);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * DateValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DateValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id default null
  ) return date is
    t_FeeElementType            FeeElement.FeeElementType%type;
  begin
    if a_PosseObjectId is null then
      return null;
    end if;

    GetFeeElementType(a_FeeScheduleId, a_FeeElementId, t_FeeElementType);
    if t_FeeElementType = 'Expression' then
      return pkg_ExpressionElement.DateValue(a_FeeScheduleId, a_FeeElementId,
          a_PosseObjectId, a_DataSourceId);
    elsif t_FeeElementType = 'Field' then
      return pkg_FieldElement.DateValue(a_FeeScheduleId, a_FeeElementId,
          a_PosseObjectId);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * BooleanValue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function BooleanValue (
    a_FeeScheduleId             udt_Id,
    a_FeeElementId              udt_Id,
    a_PosseObjectId             udt_PosseId,
    a_DataSourceId              udt_Id default null
  ) return boolean is
    t_FeeElementType            FeeElement.FeeElementType%type;
  begin
    if a_PosseObjectId is null then
      return null;
    end if;

    GetFeeElementType(a_FeeScheduleId, a_FeeElementId, t_FeeElementType);
    if t_FeeElementType = 'Expression' then
      return pkg_ExpressionElement.BooleanValue(a_FeeScheduleId, a_FeeElementId,
          a_PosseObjectId, a_DataSourceId);
    elsif t_FeeElementType = 'Field' then
      return pkg_FieldElement.BooleanValue(a_FeeScheduleId, a_FeeElementId,
          a_PosseObjectId);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * CopyUpdatedElementValues() -- PRIVATE
   *   Copy updated values from a Fee Element to the related Field/Expression
   *   Element
   *-------------------------------------------------------------------------*/
  procedure CopyUpdatedElementValues (
    a_ObjectId                          udt_PosseId,
    a_FeeElementType                    varchar2
  ) is
    t_ObjectIdColumnName                varchar2(30);
    t_UpdateColumnName                  varchar2(30);
    t_RelatedObjectId                   udt_PosseId;
    t_CurrentValue                      varchar2(4000);
    t_RelatedValue                      varchar2(4000);
  begin
    -- Determine the type of element
    if a_FeeElementType = 'Field' then
      t_ObjectIdColumnName := 'FieldElementObjectId';
      t_UpdateColumnName := 'ColumnName';
    elsif a_FeeElementType = 'Expression' then
      t_ObjectIdColumnName := 'ExpressionElementObjectId';
      t_UpdateColumnName := 'Syntax';
    end if;

    t_RelatedObjectId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, t_ObjectIdColumnName);
    t_CurrentValue := api.pkg_ColumnQuery.Value(a_ObjectId, t_UpdateColumnName);
    t_RelatedValue := api.pkg_ColumnQuery.Value(t_RelatedObjectId, t_UpdateColumnName);

    if t_CurrentValue != t_RelatedValue then
      api.pkg_ColumnUpdate.SetValue(t_RelatedObjectId, t_UpdateColumnName, t_CurrentValue);
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, t_UpdateColumnName);
    end if;
  end CopyUpdatedElementValues;

  /*---------------------------------------------------------------------------
   * RegisterElement() -- PRIVATE
   *   Creates an element of the specified ObjectDef, relating it to the
   *   specifed Fee Element Object and Fee Schedule Object via the FeeElement
   *   and FeeSchedule relationships, respectively. Returns the PosseObjectId
   *   of the newly created element.
   *-------------------------------------------------------------------------*/
  function RegisterElement (
    a_FeeElementObjectId                udt_PosseId,
    a_FeeScheduleObjectId               udt_PosseId,
    a_PrimaryKey                        udt_Id,
    a_ObjectDefName                     varchar2
  ) return udt_PosseId is
    t_FeeElementEndPointId              udt_PosseId;
    t_FeeScheduleEndPointId             udt_PosseId;
    t_NewObjectId                       udt_PosseId;
    t_RelId                             udt_PosseId;
  begin
    t_FeeElementEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
        a_ObjectDefName, 'FeeElement');
    t_FeeScheduleEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
        a_ObjectDefName, 'FeeSchedule');

    t_NewObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
        a_ObjectDefName, a_PrimaryKey);
    t_RelId := api.pkg_RelationshipUpdate.New(t_FeeElementEndPointId,
        t_NewObjectId, a_FeeElementObjectId);
    t_RelId := api.pkg_RelationshipUpdate.New(t_FeeScheduleEndPointId,
        t_NewObjectId, a_FeeScheduleObjectId);

    return t_NewObjectId;
  end RegisterElement;

  /*---------------------------------------------------------------------------
   * CreateRelatedExpressionElement() -- PRIVATE
   *   Create an Expression Element and relate it to the Fee Element and Fee
   *   Schedule.
   *-------------------------------------------------------------------------*/
  procedure CreateRelatedExpressionElement (
    a_ObjectId                          udt_PosseId,
    a_FeeElementId                      udt_Id,
    a_FeeScheduleId                     udt_Id,
    a_FeeScheduleObjectId               udt_PosseId
  ) is
    t_Syntax                            varchar2(4000);
    t_PrimaryKey                        varchar2(9);
    t_NewObjectId                       udt_PosseId;
  begin
    t_Syntax := api.pkg_ColumnQuery.Value(a_ObjectId, 'Syntax');
    pkg_PosseExternalRegistration.RegisterExpressionElement(t_PrimaryKey,
        t_Syntax, a_FeeScheduleId, a_FeeElementId);

    t_NewObjectId := RegisterElement(a_ObjectId, a_FeeScheduleObjectId,
        t_PrimaryKey, 'o_ExpressionElement');
  end CreateRelatedExpressionElement;

  /*---------------------------------------------------------------------------
   * CreateRelatedFieldElement() -- PRIVATE
   *   Create a Field Element and relate it to the Fee Element and Fee
   *   Schedule.
   *-------------------------------------------------------------------------*/
  procedure CreateRelatedFieldElement (
    a_ObjectId                          udt_PosseId,
    a_FeeElementId                      udt_Id,
    a_FeeScheduleId                     udt_Id,
    a_FeeScheduleObjectId               udt_PosseId
  ) is
    t_ColumnName                        varchar2(30);
    t_PrimaryKey                        varchar2(9);
    t_NewObjectId                       udt_PosseId;
  begin
    t_ColumnName := api.pkg_ColumnQuery.Value(a_ObjectId, 'ColumnName');
    pkg_PosseExternalRegistration.RegisterFieldElement(t_PrimaryKey,
        t_ColumnName, a_FeeScheduleId, a_FeeElementId);

    t_NewObjectId := RegisterElement(a_ObjectId, a_FeeScheduleObjectId,
        t_PrimaryKey, 'o_FieldElement');
  end CreateRelatedFieldElement;

  /*---------------------------------------------------------------------------
   * CreateRelToFeeSchedule() -- PUBLIC
   *   Creates a stored relationship between a fee element and fee schedule.
   *   Runs on Constructor of rel between DataSource and FeeElement.
   *-------------------------------------------------------------------------*/
  procedure CreateRelToFeeSchedule (
    a_RelationshipId                    udt_PosseId,
    a_AsOfDate                          date default null,
    a_FeeScheduleObjectId               udt_PosseId default null
  ) is
    t_RelId                             udt_PosseId;
    t_FeeScheduleId                     udt_Id;
    t_FeeScheduleObjectId               udt_PosseId;
    t_FeeElementObjectId                udt_Id;
    t_EndPointId                        udt_PosseId;
    t_RelCount                          pls_integer;
  begin
    select r.FeeElementId into t_FeeElementObjectId
      from query.r_Datasource_Feeelement r
     where r.RelationshipId = a_RelationshipId;

    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName('o_FeeElement',
        'FeeSchedule');

    -- Get Fee Schedule Posse ObjectId
    if a_FeeScheduleObjectId is not null then
      t_FeeScheduleObjectId := a_FeeScheduleObjectId;
    else
      t_FeeScheduleId := api.pkg_ColumnQuery.Value(t_FeeElementObjectId, 'FeeScheduleId');

      if t_FeeScheduleId is null then
        api.pkg_Errors.RaiseError(-20000, 'Could not find Fee Schedule Id ' ||
            'related to Fee Element');
      end if;

      t_FeeScheduleObjectId := pkg_Utils.GetFeeScheduleObjectId(t_FeeScheduleId);
    end if;

    if t_FeeScheduleObjectId is not null then
      select count(r.RelationshipId)
        into t_RelCount
        from query.r_FeeSchedule_FeeElement r
       where r.FeeElementObjectId = t_FeeElementObjectId
         and r.FeeScheduleObjectId = t_FeeScheduleObjectId;
      if t_RelCount = 0 then
        t_RelId := api.pkg_RelationshipUpdate.New(t_EndPointId, t_FeeElementObjectId,
            t_FeeScheduleObjectId);
      end if;
    else
      api.pkg_Errors.RaiseError(-20000, 'Could not find Fee Schedule for ' ||
          'the given Fee Schedule Id');
    end if;
  end CreateRelToFeeSchedule;

  /*---------------------------------------------------------------------------
   * Constructor() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure Constructor (
    a_ObjectId                          udt_PosseId,
    a_AsOfDate                          date
  ) is
    t_FeeScheduleId                     udt_Id;
    t_FeeScheduleObjectId               udt_PosseId;
    t_FeeElementId                      udt_Id;
    t_FeeElementType                    varchar2(10);
  begin
    t_FeeScheduleId := api.pkg_ColumnQuery.Value(a_ObjectId, 'FeeScheduleId');
    t_FeeScheduleObjectId := pkg_Utils.GetFeeScheduleObjectId(t_FeeScheduleId);
    t_FeeElementId := api.pkg_ColumnQuery.Value(a_ObjectId, 'FeeElementId');

    -- Relate the Fee Element to the appropriate Fee schedule
    -- t_FeeScheduleObjectId := CreateRelToFeeSchedule(a_ObjectId);
    -- CreateRelToFeeSchedule(a_ObjectId, a_AsOfDate, t_FeeScheduleObjectId);

    -- Create the related Field/Expression Element
    t_FeeElementType := api.pkg_ColumnQuery.Value(a_ObjectId, 'FeeElementType');
    if t_FeeElementType = 'Field' then
      if api.pkg_ColumnQuery.IsNull(a_ObjectId, 'FieldElementObjectId') then
        CreateRelatedFieldElement(a_ObjectId, t_FeeElementId, t_FeeScheduleId,
            t_FeeScheduleObjectId);
      end if;
    elsif t_FeeElementType = 'Expression' then
      if api.pkg_ColumnQuery.IsNull(a_ObjectId, 'ExpressionElementObjectId') then
        CreateRelatedExpressionElement(a_ObjectId, t_FeeElementId, t_FeeScheduleId,
            t_FeeScheduleObjectId);
      end if;
    end if;
  end Constructor;

  /*---------------------------------------------------------------------------
   * PreVerify() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PreVerify (
    a_ObjectId                          udt_PosseId,
    a_AsOfDate                          date
  ) is
    t_FeeElementType                    varchar2(10);
  begin
    t_FeeElementType := api.pkg_ColumnQuery.Value(a_ObjectId, 'FeeElementType');

    -- Copy updated values to related Field/Expressions Element
    CopyUpdatedElementValues(a_ObjectId, t_FeeElementType);
  end PreVerify;

end pkg_FeeElement;

/

