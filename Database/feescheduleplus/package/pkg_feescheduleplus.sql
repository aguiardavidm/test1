create or replace package pkg_FeeSchedulePlus as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  type udt_IdList is table of number(9) index by binary_integer;
  subtype udt_Id is number(9);
  subtype udt_PosseId is api.pkg_Definition.udt_Id;
  subtype udt_PosseIdList is api.pkg_Definition.udt_IdList;
  subtype udt_PosseObjectDefTypeName is api.ObjectDefTypes.Name%type;
  type udt_FeeDefinitionList is table of FeeDefinition%rowtype index by binary_integer;

  /*--------------------------------------------------------------------------
   * Constants -- PUBLIC
   *------------------------------------------------------------------------*/
  gc_DateFormat varchar2(21) := 'YYYY-MM-DD HH24:MI:SS';

  /*---------------------------------------------------------------------------
   * DetermineApplicableFees() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DetermineApplicableFees (
    a_PosseObjectId                   udt_PosseId
  ) return udt_FeeDefinitionList;

end pkg_FeeSchedulePlus;

/

grant execute
on pkg_feescheduleplus
to feescheduleplususer;

create or replace package body pkg_FeeSchedulePlus as

  /*---------------------------------------------------------------------------
   * DetermineApplicableFees() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DetermineApplicableFees (
    a_PosseObjectId                   udt_PosseId
  ) return udt_FeeDefinitionList is
    t_FeeDefinitionList               udt_FeeDefinitionList;
    t_TestNum                         number;
    t_TestNum2                        number;
    t_TestString                      varchar2(4000);
    t_TestString2                     varchar2(4000);
  begin
    /*
      - Logic:
        - find data sources that use given object type
        - get feedefinition objects for the data sources
        - get date column used to determine fee schedule from linked feedefinitions
        - get date value from object
        - filter feedefinitions down to those that are valid for the date
        - for each fee definition, resolve the conditions
        - limit list of fee definitions to those for which the condition holds true
        - build and return list
    */
/*begin
select objectid, od.name
  into t_TestNum, t_TestString
  from api.objects o
  join api.objectdefs od on od.objectdefid = o.objectdefid
  join DataSource ds on ds.Posseviewname = od.Name
 where o.objectid = 6278873;
exception when no_data_found
  then
    api.pkg_errors.RaiseError(-20000, 'We''ve got a problem here...');
end;
*/
--dbms_output.put_line('t_TestNum: ' || t_TestNum || '  t_TestNum2: ' || t_TestNum2 || '  t_TestString: ' || t_TestString || '  t_TestString2: ' || t_TestString2);
--api.pkg_errors.RaiseError(-20000, 'Test5: ' || a_PosseObjectId);
    for c in (select fd.FeeDefinitionId,
                     fd.EffectiveDateColumnName,
                     fs.EffectiveStartDate,
                     fs.EffectiveEndDate,
                     fs.FeeScheduleId
                from FeeSchedule fs,
                     FeeDefinition fd,
                     DataSource ds,
                     api.ObjectDefs od,
                     api.Objects ob
               where ob.ObjectId = a_PosseObjectId
                 and od.ObjectDefId = ob.ObjectDefId
                 and ds.PosseViewName = od.Name
                 and fd.DataSourceId = ds.DataSourceId
                 and fs.FeeScheduleId = fd.FeeScheduleId) loop
                     
--dbms_output.put_line('Inside Loop for Determine applicable Fees');
--dbms_output.put_line('Effective Date: ' || api.pkg_ColumnQuery.DateValue(a_PosseObjectId, c.EffectiveDateColumnName));
      if api.pkg_ColumnQuery.DateValue(a_PosseObjectId, c.EffectiveDateColumnName) between c.EffectiveStartDate and c.EffectiveEndDate then
--dbms_output.put_line('Inside first if');
        if pkg_FeeDefinition.EvaluateCondition(c.FeeScheduleId, c.FeeDefinitionId, a_PosseObjectId) then
--dbms_output.put_line('Inside second if');
          pkg_FeeDefinition.GetData(c.FeeScheduleId, c.FeeDefinitionId, t_FeeDefinitionList(t_FeeDefinitionList.count + 1));
        end if;
      end if;
    end loop;

    return t_FeeDefinitionList;
  end;

end pkg_FeeSchedulePlus;

/

