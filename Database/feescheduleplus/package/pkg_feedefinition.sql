create or replace package pkg_FeeDefinition as

  -- Public Type Declarations
  subtype udt_Id is pkg_FeeSchedulePlus.udt_Id;
  subtype udt_PosseId is pkg_FeeSchedulePlus.udt_PosseId;
  subtype udt_PosseIdList is pkg_FeeSchedulePlus.udt_PosseIdList;

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_FeeDefinitionId           udt_Id,
    a_Data                  out FeeDefinition%rowtype
  );

  /*---------------------------------------------------------------------------
   * GenerateFee()
   *   This function checks if the generated fee already exists. Adjusts the fee
   * if the original amount does not equate to the current amount, sets the new
   * fee Id as SystemGenerated. If the Fee does not currently exist this function
   * will then create it.
   *-------------------------------------------------------------------------*/
  function GenerateFee (
    a_FeeDefinition                     FeeDefinition%rowtype,
    a_PosseObjectId                     udt_Id,
    a_PosseFeeJobId                     udt_Id
  ) return udt_Id;

  /*---------------------------------------------------------------------------
   * SetSystemGeneratedFlag() -- PUBLIC
   * API BREAK!
   * dden 14.09.08 - after discussion with Gord den Otter, agreed to set the
   * "System Generated" flag as "Fee Schedule Plus" in effect acts as a replacement
   * for the GenerateFees toolbox call and needs the flag for exactly the same
   * reason as GenerateFees (i.e. so it can leave manual fees alone).
   * We also need to reset it to 'N' before modifying the fee.
   *-------------------------------------------------------------------------*/
  procedure SetSystemGeneratedFlag (
    a_TransactionId             udt_Id,
    a_SystemGenerated           char default 'Y'
  );

  /*---------------------------------------------------------------------------
   * GenerateFee() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GenerateFee (
    a_FeeDefinition             FeeDefinition%rowtype,
    a_PosseObjectId             udt_Id,
    a_PosseFeeJobId             udt_Id
  );

  /*---------------------------------------------------------------------------
   * EvaluateCondition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function EvaluateCondition (
    a_FeeScheduleId             udt_Id,
    a_FeeDefinitionId           udt_Id,
    a_PosseObjectId             udt_Id
  ) return boolean;

  /*---------------------------------------------------------------------------
   * PreVerify()
   *   Wrapper for the Pre Verify event on the o_LMS_FeeDefinition Posse
   *   ObjectDef
   *-------------------------------------------------------------------------*/
  procedure PreVerify (
    a_ObjectId                          udt_PosseId,
    a_AsOfDate                          date
  );

end pkg_FeeDefinition;
/

grant execute
on pkg_feedefinition
to feescheduleplususer;

create or replace package body pkg_FeeDefinition as

  /*---------------------------------------------------------------------------
   * GetData() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_FeeDefinitionId           udt_Id,
    a_Data                  out FeeDefinition%rowtype
  ) is
  begin
    select *
      into a_Data
      from FeeDefinition
     where FeeDefinitionId = a_FeeDefinitionId
       and FeeScheduleId = a_FeeScheduleId;
  end;

  /*---------------------------------------------------------------------------
   * GenerateFee() -- PUBLIC
   *   This function checks if the generated fee already exists. Adjusts the fee
   * if the original amount does not equate to the current amount, sets the new
   * fee Id as SystemGenerated. If the Fee does not currently exist this function
   * will then create it.
   *-------------------------------------------------------------------------*/
  function GenerateFee (
    a_FeeDefinition                     FeeDefinition%rowtype,
    a_PosseObjectId                     udt_Id,
    a_PosseFeeJobId                     udt_Id
  ) return udt_Id is
    t_ResponsibleObjectId               udt_PosseId;
    t_OldResponsibleObjectId            udt_PosseId;
    t_GLAccountId                       udt_PosseId;
    t_FeeId                             udt_PosseId;
    t_AdjId                             udt_PosseId;
    t_Description                       varchar2(60);
    t_Amount                            number;
    t_OldAmount                         number;
    t_EndPointId                        udt_PosseId;
    t_Relationships                     udt_PosseIdList;
    t_GLAccountNumber                   varchar2(60);
    t_NoManualAdjustment                varchar2(1);
  begin

    pkg_Debug.PutLine('********a_FeeDefinition.ResponsiblePartyGenerationType: '
        || a_FeeDefinition.ResponsiblePartyGenerationType);
    if a_FeeDefinition.ResponsiblePartyGenerationType in ('Default',
        'Relationship') then
      if a_FeeDefinition.ResponsiblePartyGenerationType = 'Default' then
        begin

          select rd.FromEndPointId
          into t_EndPointId
          from
            api.RelationshipDefs rd,
            stage.ResponsibleObjects ro,
            api.Jobs jo
          where (jo.JobId = a_PosseFeeJobId) and (ro.JobTypeId = jo.JobTypeId)
            and (ro.IsResponsible = 'Y') and (ro.IsDefault = 'Y')
            and (rd.FromEndPointId = ro.EndPointId);

        exception
          when no_data_found then
            raise_application_error(-20000, 'Default party relationship for fee: '
                || a_FeeDefinition.Description || ' does not exist');
        end;
      elsif a_FeeDefinition.ResponsiblePartyGenerationType = 'Relationship' then
        begin

          select rd.ToEndPointId
          into t_EndPointId
          from
            api.RelationshipNames rn
            join api.RelationshipDefs rd
                on rn.RelationshipNameId = rd.toRelationshipNameId
            join feescheduleplus.datasource fds
                on fds.datasourceid = a_FeeDefinition.DatasourceId
            join api.objectdefs od
                on od.Name = fds.posseviewname
           where rn.RelationshipName = a_FeeDefinition.ResponsiblePartyRelationship
             and od.objectdefid = rd.FromObjectDefId;

        exception
          when no_data_found then
            raise_application_error(-20000, 'Responsible party relationship ' ||
                a_FeeDefinition.ResponsiblePartyRelationship || ' for fee: ' ||
                a_FeeDefinition.Description || ' does not exist');
        end;
      end if;
      pkg_Debug.PutLine('********EndPointId: ' || t_EndPointId);

      t_Relationships := api.pkg_ObjectQuery.RelatedObjects(a_PosseFeeJobId,
          t_EndPointId);
      pkg_Debug.PutLine('********t_Relatinships.count: ' || t_Relationships.count);
      if t_Relationships.count = 1 then
        t_ResponsibleObjectId := t_Relationships(1);
      elsif t_Relationships.count > 1 then
        raise_application_error(-20000, 'Too many relationships exist for' ||
            'responsible party: ' || a_FeeDefinition.ResponsiblePartyRelationship ||
            ' for fee: ' || a_FeeDefinition.Description);
      end if;
    end if;

    -- Determine General Ledger Account Id
    t_GLAccountNumber := pkg_ExpressionColumn.Value(
        pkg_ExpressionColumn.gc_FeeDefGlAccountNumber,
        a_FeeDefinition.FeeScheduleId, a_FeeDefinition.GLAccountNumber,
        a_PosseObjectId, a_FeeDefinition.DataSourceId);
    begin

      select GLAccountId
        into t_GLAccountId
        from api.GLAccounts
       where GLAccount = t_GLAccountNumber;

    exception
    when no_data_found then
      raise_application_error(-20000, 'Invalid G/L Account: ' || t_GLAccountNumber
          || ' configured on fee: ' || a_FeeDefinition.Description);
    end;

    -- Determine Fee Description
    t_Description := nvl(
        pkg_ExpressionColumn.Value(
          pkg_ExpressionColumn.gc_FeeDefFeeDescription,
          a_FeeDefinition.FeeScheduleId, a_FeeDefinition.FeeDescription,
          a_PosseObjectId, a_FeeDefinition.DataSourceId
        ), a_FeeDefinition.Description);

    -- Determine Fee Amount
    t_Amount := pkg_ExpressionColumn.NumberValue(
        pkg_ExpressionColumn.gc_FeeDefAmount,
        a_FeeDefinition.FeeScheduleId, a_FeeDefinition.Amount, a_PosseObjectId,
        a_FeeDefinition.DataSourceId);

    begin
      -- Check if this fee already exists
      select
        a.FeeId,
        a.Amount + (
            select nvl(sum(Amount), 0)
            from api.FeeTransactions t
            where t.FeeId = a.FeeId
              and t.TransactionType = 'Adj'
              and t.Description != 'Voided'
            ),
        a.ResponsibleObjectId
        into
          t_FeeId,
          t_OldAmount,
          t_OldResponsibleObjectId
        from api.fees a
        where a.JobId = a_PosseFeeJobId
          -- Can't match on ResponsibleObjectId if null - Avoid creating duplicates
          and a.GLAccountId = t_GLAccountId and a.Description = t_Description
          and a.SystemGenerated = 'Y';

      -- Ensure minimum system generated flag is not N, so a Manual Adjustment
      -- occurs.
      select nvl(min(ft.SystemGenerated),'Y')
      into t_NoManualAdjustment
      from api.feetransactions ft
      where ft.FeeId = t_FeeId and ft.TransactionType = 'Adj'
        and ft.Description != 'Voided';

      -- API Break to allow the Responsible Object Id to be updated.
      -- Cannot use api.pkg_feeupdate.ModifyFee since it can't update
      -- SystemGenerated fees.
      if t_ResponsibleObjectId is not null then
        -- Intentionally ONLY update the specific fee in question instead of all
        -- fees on the Job.
        -- NJ-specific fix: Update RP if old RP does not match current one
        if t_OldResponsibleObjectId is null or t_OldResponsibleObjectId != t_ResponsibleObjectId then
          update finance.fees
          set responsibleobjectid = t_ResponsibleObjectId
          where TransactionId = t_FeeId;
        end if;
      end if;

      -- See if this fee needs changed
      if t_OldAmount <> t_Amount and t_NoManualAdjustment = 'Y' then
        -- Adjust the existing fee
        pkg_debug.putline('Adjust fee to '||t_Amount||': ' || t_FeeId);
        t_AdjId := api.pkg_FeeUpdate.Adjust(t_FeeId, (t_OldAmount - t_Amount) * -1,
            substr(t_Description, 1, 49) || ' Adjustment', sysdate);
        SetSystemGeneratedFlag(t_AdjId);
      end if;

    exception
      when too_many_rows then
        raise_application_error(-20000, 'Duplicate fees found for the Job: ' ||
            a_PosseFeeJobId || ', Description: ' || t_Description || ', Account: '
            || t_GLAccountId);
      when no_data_found then
        if t_Amount <> 0 then
          -- Fee doesn't already exist, so create it
          t_FeeId := api.pkg_FeeUpdate.New(a_PosseFeeJobId, t_Amount, t_Description,
              t_GLAccountId, t_ResponsibleObjectId, null, sysdate);
          SetSystemGeneratedFlag(t_FeeId);
          api.pkg_ObjectUpdate.RegisterExternalObject('o_Fee', t_FeeId);
        end if;

    end;

    return t_FeeId;
  end;

  /*---------------------------------------------------------------------------
   * SetSystemGeneratedFlag() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SetSystemGeneratedFlag (
    a_TransactionId             udt_Id,
    a_SystemGenerated           char default 'Y'
  ) is
  begin

    /* Set the SystemGenerated flag for this transaction
     * (Which is equal to the FeeId for a new fee and the AdjustmentId for an adjustment) */
    update finance.transactions a
       set a.SystemGenerated = a_SystemGenerated
     where a.TransactionId = a_TransactionId;

  end;

  /*---------------------------------------------------------------------------
   * GenerateFee() -- PUBLIC
   * Changed By: Joshua Lenon
   * Date: 12/30/2014
   * Description: Changes made by Dave dO for the Fees & Payments tab implementation.
   *     GenerateFees was changed to a function, this procedure has been changed
   *     for any existing instances that may call it, which will be redirected
   *     to the GenerateFees Function above.
   *-------------------------------------------------------------------------*/
  procedure GenerateFee (
    a_FeeDefinition             FeeDefinition%rowtype,
    a_PosseObjectId             udt_Id,
    a_PosseFeeJobId             udt_Id
  ) is
    t_FeeId                     udt_PosseId;

  begin
    t_FeeId := GenerateFee(a_FeeDefinition, a_PosseObjectId, a_PosseFeeJobId);

  end;

  /*---------------------------------------------------------------------------
   * EvaluateCondition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function EvaluateCondition (
    a_FeeScheduleId             udt_Id,
    a_FeeDefinitionId           udt_Id,
    a_PosseObjectId             udt_Id
  ) return boolean is
    t_FeeDefinition             FeeDefinition%rowtype;
    t_CategoryCondition         boolean;
  begin
    GetData(a_FeeScheduleId, a_FeeDefinitionId, t_FeeDefinition);

    if t_FeeDefinition.FeeCategoryId is not null then
      t_CategoryCondition := pkg_FeeCategory.EvaluateCondition(a_FeeScheduleId, t_FeeDefinition.FeeCategoryId, a_PosseObjectId, t_FeeDefinition.DataSourceId);
    else
      t_CategoryCondition := true;
    end if;

    if t_FeeDefinition.Condition is null then
      return t_CategoryCondition;
    else
      return t_CategoryCondition and nvl(pkg_ExpressionColumn.BooleanValue(pkg_ExpressionColumn.gc_FeeDefCondition, a_FeeScheduleId, t_FeeDefinition.Condition, a_PosseObjectId, t_FeeDefinition.DataSourceId), true);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * PreVerify() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PreVerify (
    a_ObjectId                          udt_PosseId,
    a_AsOfDate                          date
  ) is
  begin
    -- Update the External IDs for this Fee Definition
  /*  UpdateExternalIds(a_ObjectId);*/
  null;
  end PreVerify;

end pkg_FeeDefinition;
/

