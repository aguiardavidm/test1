create table fieldelement (
  fieldelementid                  number(9) not null,
  feescheduleid                   number(9) not null,
  columnname                      varchar2(30) not null,
  createddate                     date
    default sysdate not null,
  createdby                       varchar2(30)
    default user not null,
  updateddate                     date
    default sysdate not null,
  updatedby                       varchar2(30)
    default user not null,
  feeelementid                    number(9)
) tablespace smalldata;

grant delete
on fieldelement
to feescheduleplususer;

grant insert
on fieldelement
to feescheduleplususer;

grant select
on fieldelement
to feescheduleplususer;

grant update
on fieldelement
to feescheduleplususer;

alter table fieldelement
add constraint fieldelement_pk
primary key (
  feescheduleid,
  fieldelementid
) using index tablespace smalldata;

alter table fieldelement
add constraint fieldelement_fk1
foreign key (
  feescheduleid
) references feeschedule (
  feescheduleid
);

alter table fieldelement
add constraint fieldelement_fk2
foreign key (
  feescheduleid,
  feeelementid
) references feeelement (
  feescheduleid,
  feeelementid
);

create or replace trigger "FEESCHEDULEPLUS"."FIELDELEMENT_BIR" 
  before insert on FieldElement
  for each row
begin
  if :new.FieldElementId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FieldElementId
      from dual;
  end if;
end fieldelement_bir;
/

create or replace trigger "FEESCHEDULEPLUS"."FIELDELEMENT_BUR" 
  before update on FieldElement
  for each row
begin
  :new.UpdatedDate := sysdate;
  :new.UpdatedBy := user;

  if :new.FieldElementId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FieldElementId
      from dual;
  end if;
end fieldelement_bur;
/

