create table feedefinition (
  feedefinitionid                 number(9) not null,
  feescheduleid                   number(9) not null,
  datasourceid                    number(9) not null,
  description                     varchar2(60) not null,
  condition                       varchar2(4000),
  amount                          varchar2(4000),
  effectivedatecolumnname         varchar2(30) not null,
  feecategoryid                   number(9),
  glaccountnumber                 varchar2(4000) not null,
  feedescription                  varchar2(4000),
  responsiblepartygenerationtype  varchar2(12)
    default 'None' not null,
  responsiblepartyrelationship    varchar2(60),
  createddate                     date
    default sysdate not null,
  createdby                       varchar2(30)
    default user not null,
  updateddate                     date
    default sysdate not null,
  updatedby                       varchar2(30)
    default user not null
) tablespace smalldata;

grant delete
on feedefinition
to feescheduleplususer;

grant insert
on feedefinition
to feescheduleplususer;

grant select
on feedefinition
to feescheduleplususer;

grant update
on feedefinition
to feescheduleplususer;

alter table feedefinition
add constraint feedefinition_pk
primary key (
  feescheduleid,
  feedefinitionid
) using index tablespace smalldata;

alter table feedefinition
add constraint feedefinition_fk1
foreign key (
  feescheduleid
) references feeschedule (
  feescheduleid
);

alter table feedefinition
add constraint feedefinition_fk3
foreign key (
  feescheduleid,
  feecategoryid
) references feecategory (
  feescheduleid,
  feecategoryid
);

alter table feedefinition
add constraint feedefinition_fk4
foreign key (
  feescheduleid,
  datasourceid
) references datasource (
  feescheduleid,
  datasourceid
);

alter table feedefinition
add constraint feedefinition_ck1
check (ResponsiblePartyGenerationType in ('None', 'Default', 'Relationship'));

create or replace trigger "FEESCHEDULEPLUS"."FEEDEFINITION_BIR" 
  before insert on FeeDefinition
  for each row
begin
  if :new.FeeDefinitionId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FeeDefinitionId
      from dual;
  end if;
end feedefinition_bir;
/

create or replace trigger "FEESCHEDULEPLUS"."FEEDEFINITION_BUR" 
  before update on FeeDefinition
  for each row
begin
  :new.UpdatedDate := sysdate;
  :new.UpdatedBy := user;

  if :new.FeeDefinitionId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FeeDefinitionId
      from dual;
  end if;
end feedefinition_bur;
/

