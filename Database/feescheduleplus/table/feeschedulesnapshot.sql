create table feeschedulesnapshot (
  feeschedulesnapshotid           number(9) not null,
  feescheduleid                   number(9) not null,
  data                            clob,
  createddate                     date
    default sysdate not null,
  createdby                       varchar2(30)
    default user not null,
  updateddate                     date
    default sysdate not null,
  updatedby                       varchar2(30)
    default user not null
) tablespace smalldata
  lob (data)
    store as (
      tablespace smalldata
    );

grant delete
on feeschedulesnapshot
to feescheduleplususer;

grant insert
on feeschedulesnapshot
to feescheduleplususer;

grant select
on feeschedulesnapshot
to feescheduleplususer;

grant update
on feeschedulesnapshot
to feescheduleplususer;

alter table feeschedulesnapshot
add constraint feeschedulesnapshot_pk
primary key (
  feescheduleid,
  feeschedulesnapshotid
) using index tablespace smalldata;

alter table feeschedulesnapshot
add constraint feeschedulesnapshot_fk1
foreign key (
  feescheduleid
) references feeschedule (
  feescheduleid
) initially deferred;

create or replace trigger "FEESCHEDULEPLUS"."FEESCHEDULESNAPSHOT_BIR" 
  before insert on feeschedulesnapshot
  for each row
begin
  if :new.feeschedulesnapshotId is null then
    select FeeSchedulePlus_s.nextval
      into :new.feeschedulesnapshotId
      from dual;
  end if;
end feeschedulesnapshot_bir;
/

create or replace trigger "FEESCHEDULEPLUS"."FEESCHEDULESNAPSHOT_BUR" 
  before update on feeschedulesnapshot
  for each row
begin
  :new.UpdatedDate := sysdate;
  :new.UpdatedBy := user;

  if :new.feeschedulesnapshotId is null then
    select FeeSchedulePlus_s.nextval
      into :new.feeschedulesnapshotId
      from dual;
  end if;
end feeschedulesnapshot_bur;
/

