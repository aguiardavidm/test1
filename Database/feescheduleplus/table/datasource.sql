create table datasource (
  datasourceid                    number(9) not null,
  feescheduleid                   number(9) not null,
  posseviewname                   varchar2(60) not null,
  description                     varchar2(60) not null,
  createddate                     date
    default sysdate not null,
  createdby                       varchar2(30)
    default user not null,
  updateddate                     date
    default sysdate not null,
  updatedby                       varchar2(30)
    default user not null
) tablespace smalldata;

grant delete
on datasource
to feescheduleplususer;

grant insert
on datasource
to feescheduleplususer;

grant select
on datasource
to feescheduleplususer;

grant update
on datasource
to feescheduleplususer;

alter table datasource
add constraint datasource_pk
primary key (
  feescheduleid,
  datasourceid
) using index tablespace smalldata;

alter table datasource
add constraint datasource_fk1
foreign key (
  feescheduleid
) references feeschedule (
  feescheduleid
);

create or replace trigger "FEESCHEDULEPLUS"."DATASOURCE_BIR" 
  before insert on datasource
  for each row
begin
  if :new.DataSourceId is null then
    select FeeSchedulePlus_s.nextval
      into :new.DataSourceId
      from dual;
  end if;
end datasource_bir;
/

create or replace trigger "FEESCHEDULEPLUS"."DATASOURCE_BUR" 
  before update on datasource
  for each row
begin
  :new.UpdatedDate := sysdate;
  :new.UpdatedBy := user;
end datasource_bur;
/

