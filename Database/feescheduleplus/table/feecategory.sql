create table feecategory (
  feecategoryid                   number(9) not null,
  feescheduleid                   number(9) not null,
  condition                       varchar2(4000),
  description                     varchar2(60) not null,
  createddate                     date
    default sysdate not null,
  createdby                       varchar2(30)
    default user not null,
  updateddate                     date
    default sysdate not null,
  updatedby                       varchar2(30)
    default user not null
) tablespace smalldata;

grant delete
on feecategory
to feescheduleplususer;

grant insert
on feecategory
to feescheduleplususer;

grant select
on feecategory
to feescheduleplususer;

grant update
on feecategory
to feescheduleplususer;

alter table feecategory
add constraint feecondition_pk
primary key (
  feescheduleid,
  feecategoryid
) using index tablespace smalldata;

alter table feecategory
add constraint feecategory_fk1
foreign key (
  feescheduleid
) references feeschedule (
  feescheduleid
);

create or replace trigger "FEESCHEDULEPLUS"."FEECATEGORY_BIR" 
  before insert on FeeCategory
  for each row
begin
  if :new.FeeCategoryId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FeeCategoryId
      from dual;
  end if;
end feecategory_bir;
/

