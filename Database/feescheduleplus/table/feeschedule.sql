create table feeschedule (
  feescheduleid                   number(9) not null,
  effectivestartdate              date not null,
  effectiveenddate                date not null,
  description                     varchar2(60),
  createddate                     date
    default sysdate not null,
  createdby                       varchar2(30)
    default user not null,
  updateddate                     date
    default sysdate not null,
  updatedby                       varchar2(30)
    default user not null,
  pendingchanges                  clob,
  pendingchangesschema            clob
) tablespace smalldata
  lob (pendingchanges)
    store as (
      tablespace smalldata
    )
  lob (pendingchangesschema)
    store as (
      tablespace smalldata
    );

grant delete
on feeschedule
to feescheduleplususer;

grant insert
on feeschedule
to feescheduleplususer;

grant select
on feeschedule
to feescheduleplususer;

grant update
on feeschedule
to feescheduleplususer;

alter table feeschedule
add constraint feeschedule_pk
primary key (
  feescheduleid
) using index tablespace smalldata;

create or replace trigger "FEESCHEDULEPLUS"."FEESCHEDULE_BIR" 
  before insert on FeeSchedule
  for each row
begin
  if :new.FeeScheduleId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FeeScheduleId
      from dual;
  end if;
end feeschedule_bir;
/

create or replace trigger "FEESCHEDULEPLUS"."FEESCHEDULE_BUR" 
  before update on FeeSchedule
  for each row
begin
  :new.UpdatedDate := sysdate;
  :new.UpdatedBy := user;

  if :new.FeeScheduleId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FeeScheduleId
      from dual;
  end if;
end feeschedule_bur;
/

