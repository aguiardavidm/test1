create table expressioncolumn (
  expressioncolumnid              number(9) not null,
  tablename                       varchar2(30) not null,
  columnname                      varchar2(30) not null,
  datatype                        varchar2(7) not null,
  tableprimarykeyname             varchar2(30) not null
) tablespace smalldata;

grant delete
on expressioncolumn
to feescheduleplususer;

grant insert
on expressioncolumn
to feescheduleplususer;

grant select
on expressioncolumn
to feescheduleplususer;

grant update
on expressioncolumn
to feescheduleplususer;

alter table expressioncolumn
add constraint expressioncolumn_pk
primary key (
  expressioncolumnid
) using index tablespace smalldata;

alter table expressioncolumn
add constraint expressioncolumn_ck1
check (DataType in ('String', 'Date', 'Number', 'Boolean'));

