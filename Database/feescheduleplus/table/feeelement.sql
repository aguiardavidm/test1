create table feeelement (
  feeelementid                    number(9) not null,
  feescheduleid                   number(9) not null,
  datasourceid                    number(9),
  datatype                        varchar2(7) not null,
  feeelementtype                  varchar2(10) not null,
  createddate                     date
    default sysdate not null,
  createdby                       varchar2(30)
    default user not null,
  updateddate                     date
    default sysdate not null,
  updatedby                       varchar2(30)
    default user not null,
  isglobal                        char(1),
  globalname                      varchar2(60)
) tablespace smalldata;

grant delete
on feeelement
to feescheduleplususer;

grant insert
on feeelement
to feescheduleplususer;

grant select
on feeelement
to feescheduleplususer;

grant update
on feeelement
to feescheduleplususer;

alter table feeelement
add constraint feeelement_pk
primary key (
  feescheduleid,
  feeelementid
) using index tablespace smalldata;

alter table feeelement
add constraint feeelement_fk1
foreign key (
  feescheduleid
) references feeschedule (
  feescheduleid
);

alter table feeelement
add constraint feeelement_fk2
foreign key (
  feescheduleid,
  datasourceid
) references datasource (
  feescheduleid,
  datasourceid
);

alter table feeelement
add constraint feeelement_ck1
check (DataType in ('String', 'Date', 'Number', 'Boolean'));

alter table feeelement
add constraint feeelement_ck2
check (FeeElementType in ('Expression', 'Field'));

create or replace trigger "FEESCHEDULEPLUS"."FEEELEMENT_BIR" 
  before insert on FeeElement
  for each row
begin
  if :new.FeeElementId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FeeElementId
      from dual;
  end if;
end feeelement_bir;
/

create or replace trigger "FEESCHEDULEPLUS"."FEEELEMENT_BUR" 
  before update on FeeElement
  for each row
begin
  :new.UpdatedDate := sysdate;
  :new.UpdatedBy := user;

  if :new.FeeElementId is null then
    select FeeSchedulePlus_s.nextval
      into :new.FeeElementId
      from dual;
  end if;

end feeelement_bur;
/

