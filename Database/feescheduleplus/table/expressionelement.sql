create table expressionelement (
  expressionelementid             number(9) not null,
  feescheduleid                   number(9) not null,
  syntax                          varchar2(4000),
  createddate                     date
    default sysdate not null,
  createdby                       varchar2(30)
    default user not null,
  updateddate                     date
    default sysdate not null,
  updatedby                       varchar2(30)
    default user not null,
  feeelementid                    number(9)
) tablespace smalldata;

grant delete
on expressionelement
to feescheduleplususer;

grant insert
on expressionelement
to feescheduleplususer;

grant select
on expressionelement
to feescheduleplususer;

grant update
on expressionelement
to feescheduleplususer;

alter table expressionelement
add constraint expressionelement_pk
primary key (
  feescheduleid,
  expressionelementid
) using index tablespace smalldata;

alter table expressionelement
add constraint expressionelement_fk1
foreign key (
  feescheduleid
) references feeschedule (
  feescheduleid
);

alter table expressionelement
add constraint expressionelement_fk2
foreign key (
  feescheduleid,
  feeelementid
) references feeelement (
  feescheduleid,
  feeelementid
);

create or replace trigger "FEESCHEDULEPLUS"."EXPRESSIONELEMENT_BIR" 
  before insert on ExpressionElement
  for each row
begin
  if :new.ExpressionElementId is null then
    select FeeSchedulePlus_s.nextval
      into :new.ExpressionElementId
      from dual;
  end if;
end expressionelement_bir;
/

create or replace trigger "FEESCHEDULEPLUS"."EXPRESSIONELEMENT_BUR" 
  before update on ExpressionElement
  for each row
begin
  :new.UpdatedDate := sysdate;
  :new.UpdatedBy := user;

  if :new.ExpressionElementId is null then
    select FeeSchedulePlus_s.nextval
      into :new.ExpressionElementId
      from dual;
  end if;
end expressionelement_bur;
/

