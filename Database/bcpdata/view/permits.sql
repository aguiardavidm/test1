create or replace view permits as
select JobId,
       ExternalFileNum,
       JobTypeDescription,
       SiteAddress,
       FeesChargeState,
       S.Description StatusName
  from query.j_BuildingPermit P,
       api.statuses S
 where P.statusid = S.StatusId
Union All
select JobId,
       ExternalFileNum,
       JobTypeDescription,
       SiteAddress,
       FeesChargeState,
       S.Description StatusName
  from query.j_TradePermit P,
       api.statuses S
 where P.statusid = S.StatusId
Union All
select JobId,
       ExternalFileNum,
       JobTypeDescription,
       SiteAddress,
       FeesChargeState,
       S.Description StatusName
  from query.j_GeneralPermit P,
       api.statuses S
 where P.statusid = S.StatusId;

