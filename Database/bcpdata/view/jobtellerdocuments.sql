create or replace view jobtellerdocuments as
select distinct fe.JobId, do.DocumentId
  from api.Fees fe 
  join api.FeeTransactions tr on tr.FeeId = fe.FeeId
  join tellerquery.PaymentDetails pd on pd.PossePaymentTransactionId = tr.TransactionId
  join tellerquery.TellerPayments tp on tp.TellerPaymentId = pd.TellerPaymentId
  join api.Relationships re on re.FromObjectId = tp.PossePaymentObjectId
  join api.Documents do on do.DocumentId = re.ToObjectId;

grant select
on jobtellerdocuments
to posseextensions;

