create or replace view inspectionzones as
select ZoneName,
         ZoneLayer,
         InspectorName
    from InspectionZones_t;

