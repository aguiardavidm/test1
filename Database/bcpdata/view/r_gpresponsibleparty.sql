create or replace view r_gpresponsibleparty as
select GPC.GeneralPermitJobId,
         GPC.ContractorObjectId
    from query.r_GPContractorApplicant GPC,
         query.j_GeneralPermit GP
   where GP.JobId = GPC.GeneralPermitJobId
     and nvl(GP.NonContractorApplicant, 'N') = 'N'
     and exists ( select *
                    from query.o_Contractor C
                   where C.ObjectId = GPC.ContractorObjectId
                     and C.CreditLimit > 0)
  union
  select GPC.GeneralPermitJobId,
         GPC.ContractorObjectId
    from bcpdata.r_GeneralPermitContractor GPC,
         query.j_GeneralPermit GP
   where GP.JobId = GPC.GeneralPermitJobId
     and nvl(GP.NonContractorApplicant, 'N') = 'N'
     and not exists ( select *
                        from query.r_GPContractorApplicant GPCA
                       where GPCA.GeneralPermitJobId = GPC.GeneralPermitJobId)
     and 1 = (select count(*)
                from bcpdata.r_GeneralPermitContractor GPCC
               where GPCC.GeneralPermitJobId = GPC.GeneralPermitJobId)
     and exists ( select *
                    from query.o_Contractor C
                   where C.ObjectId = GPC.ContractorObjectId
                     and C.CreditLimit > 0);

