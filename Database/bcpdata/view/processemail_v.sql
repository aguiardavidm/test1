create or replace view processemail_v as
select processtypeid, name, description
  from api.processtypes pt
 where active = 'Y'
   and description not like 'zzz%';

grant select
on processemail_v
to posseextensions;

