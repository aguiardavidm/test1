create or replace view r_currentassignedinspections as
select IPA.AssignedTo,
       PRO.ProcessId
  from api.IncompleteProcessAssignments IPA,
       api.Processes PRO
 where PRO.ProcessId = IPA.ProcessId
   and trunc(PRO.ScheduledStartDate) = trunc(sysdate);

grant select
on r_currentassignedinspections
to posseextensions;

