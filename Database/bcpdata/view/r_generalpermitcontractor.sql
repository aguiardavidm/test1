create or replace view r_generalpermitcontractor as
select GeneralPermitJobId,
         ContractorObjectId
    from query.r_GPBuildingContractor
  union
  select GeneralPermitJobId,
         ContractorObjectId
    from query.r_GPElectricalContractor
  union
  select GeneralPermitJobId,
         ContractorObjectId
    from query.r_GPMechanicalContractor
  union
  select GeneralPermitJobId,
         ContractorObjectId
    from query.r_GPPlumbingContractor
  union
  select GeneralPermitJobId,
         ContractorObjectId
    from query.r_GPOtherContractor;

grant select
on r_generalpermitcontractor
to posseextensions;

