create or replace view defectcode as
select PosseObjectId,
       CodeId,
       ShortDescription,
       nvl(LongDescription, ShortDescription) LongDescription
  from DefectCode_t;

create or replace trigger "BCPDATA"."DEFECTCODE_IIR" 
  instead of insert on DefectCode
  for each row
declare
  t_LongDescription   varchar2(300);
begin
  if :new.LongDescription = :new.ShortDescription then
    t_LongDescription := null;
  else
    t_LongDescription := :new.LongDescription;
  end if;

  insert into DefectCode_t
  values (:new.PosseObjectId,
          :new.CodeId,
          :new.ShortDescription,
          t_LongDescription
          );
end DefectCode_iur;
/

create or replace trigger "BCPDATA"."DEFECTCODE_IUR" 
  instead of update on DefectCode
  for each row
declare
  t_ShortDescription   varchar2(60);
  t_LongDescription    varchar2(300);
begin
  select ShortDescription,
         LongDescription
    into t_ShortDescription,
         t_LongDescription
    from DefectCode_t
   where PosseObjectId = :new.PosseObjectId;

  if :new.LongDescription = t_ShortDescription and t_LongDescription is null then
    update DefectCode_t
       set CodeId = :new.CodeId,
           ShortDescription = :new.ShortDescription,
           LongDescription = null
     where PosseObjectId = :new.PosseObjectId;
  else
    update DefectCode_t
       set CodeId = :new.CodeId,
           ShortDescription = :new.ShortDescription,
           LongDescription = :new.LongDescription
     where PosseObjectId = :new.PosseObjectId;
  end if;
end DefectCode_iur;
/

