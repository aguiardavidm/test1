create or replace view sdequeries as
select
  EndPointId,
  FromLayerName,
  FromLayerColumnName,
  FromColumnName,
  ToLayerName,
  ToLayerColumnName,
  ToIndexDefId,
  (select od.Name
     from api.ObjectDefs od,
          api.IndexDefs id
    where id.IndexDefid = ToIndexDefId
      and id.ObjectDefId = od.ObjectDefId) IndexObjectDefName,
  (select cd.Name
     from api.ColumnDefs cd,
          api.IndexDefColumns idc
    where idc.IndexDefId = ToIndexDefId
      and idc.ColumnDefId = cd.ColumnDefId) IndexColumnName
from SDEQueries_t;

