create or replace view r_tpresponsibleparty as
select TPC.TradePermitJobId,
         TPC.ContractorObjectId
    from query.r_TPContractorApplicant TPC,
         query.j_TradePermit TP
   where TP.JobId = TPC.TradePermitJobId
     and nvl(TP.NonContractorApplicant, 'N') = 'N'
     and exists ( select *
                    from query.o_Contractor C
                   where C.ObjectId = TPC.ContractorObjectId
                     and C.CreditLimit > 0)
  union
  select TPC.TradePermitJobId,
         TPC.ContractorObjectId
    from bcpdata.r_TradePermitContractor TPC,
         query.j_TradePermit TP
   where TP.JobId = TPC.TradePermitJobId
     and nvl(TP.NonContractorApplicant, 'N') = 'N'
     and not exists ( select *
                        from query.r_TPContractorApplicant TPCA
                       where TPCA.TradePermitJobId = TPC.TradePermitJobId)
     and 1 = (select count(*)
                from bcpdata.r_TradePermitContractor TPCC
               where TPCC.TradePermitJobId = TPC.TradePermitJobId)
     and exists ( select *
                    from query.o_Contractor C
                   where C.ObjectId = TPC.ContractorObjectId
                     and C.CreditLimit > 0);

