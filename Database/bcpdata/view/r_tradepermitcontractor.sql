create or replace view r_tradepermitcontractor as
select TradePermitJobId,
         ContractorObjectId
    from query.r_TPElectricalContractor
  union
  select TradePermitJobId,
         ContractorObjectId
    from query.r_TPMechanicalContractor
  union
  select TradePermitJobId,
         ContractorObjectId
    from query.r_TPPlumbingContractor;

grant select
on r_tradepermitcontractor
to posseextensions;

