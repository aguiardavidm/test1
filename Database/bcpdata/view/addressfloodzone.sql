create or replace view addressfloodzone as
select
  a.posseobjectid,
  max(fz.floodzoneid) FloodZoneId
    from bcpdata.Address a
    join bcpdata.Floodzones fz
      on fz.Firmeffectivedate = a.Firmeffectivedate
     and fz.firmpanel = a.firmpanel
     and fz.floodmapno = a.floodmapno
     and fz.floodzone = a.floodzone
  group by a.posseobjectid;

