create or replace view permitjobtypes as
select "LOGICALTRANSACTIONID","JOBTYPEID","NAME","DESCRIPTION","DISPLAYFORMAT","ISJOBTYPE","PARENTJOBTYPEID","COLOR","BUSINESSDESCRIPTION","EXTERNALFILENUMMASK","EXTERNALFILENUMSEQUENCEID"
    from api.jobtypes j
   where j.name in ('j_BuildingPermit','j_TradePermit','j_GeneralPermit');

grant select
on permitjobtypes
to abc;

grant select
on permitjobtypes
to extension;

