create or replace view r_buildingpermitcontractor as
select BuildingPermitJobId,
         ContractorObjectId
    from query.r_BPBuildingContractor
  union
  select BuildingPermitJobId,
         ContractorObjectId
    from query.r_BPElectricalContractor
  union
  select BuildingPermitJobId,
         ContractorObjectId
    from query.r_BPMechanicalContractor
  union
  select BuildingPermitJobId,
         ContractorObjectId
    from query.r_BPPlumbingContractor;

grant select
on r_buildingpermitcontractor
to posseextensions;

