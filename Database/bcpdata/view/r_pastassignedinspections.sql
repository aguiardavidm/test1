create or replace view r_pastassignedinspections as
select IPA.AssignedTo,
       PRO.ProcessId
  from api.IncompleteProcessAssignments IPA,
       api.Processes PRO
 where PRO.ProcessId = IPA.ProcessId
   and trunc(PRO.ScheduledStartDate) < trunc(sysdate);

