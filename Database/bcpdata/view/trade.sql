create or replace view trade as
select
  posseobjectid,
  description,
  performprocesstypename,
  useassignmentprocedure,
  assignmentprocedurename,
  permittradecolumn
from trade_t;

grant select
on trade
to posseuser;

