create or replace view r_bpresponsibleparty as
select BPC.BuildingPermitJobId,
         BPC.ContractorObjectId
    from query.r_BPBuildingContractor BPC,
         query.j_BuildingPermit BP
   where BP.JobId = BPC.BuildingPermitJobId
     and nvl(BP.NonContractorApplicant, 'N') = 'N'
     and exists ( select *
                    from query.o_Contractor C
                   where C.ObjectId = BPC.ContractorObjectId
                     and C.CreditLimit > 0);

