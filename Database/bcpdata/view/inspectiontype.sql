create or replace view inspectiontype as
select
  posseobjectid,
  description,
  validforutilityconnect,
  final
from inspectiontype_t;

grant select
on inspectiontype
to lms;

grant select
on inspectiontype
to posseuser;

