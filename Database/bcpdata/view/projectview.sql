create or replace view projectview as
select distinct parentjobid, jobid
  from api.jobs

union

--get all of the jobs that are related to a Master Project Object
select r.FromObjectId, r.ToObjectId
  from api.relationships r
  join api.RelationshipDefs rd on rd.RelationshipDefId = r.RelationshipDefId
    and rd.ToEndPointId = r.EndPointId
  join api.ObjectDefs od on od.objectdefid = rd.ToObjectDefId
  join api.ObjectDefTypes ot on ot.ObjectDefTypeId = od.ObjectDefTypeId
   and ot.ObjectDefTypeId = 2
 where rd.FromObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_MasterProject')
   and not exists (select t.jobid from api.jobs t where t.parentjobid is not null and t.jobid = r.ToObjectid)

union

select null, objectid
  from query.o_MasterProject;

grant select
on projectview
to posseextensions;

