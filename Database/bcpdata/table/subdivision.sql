create table subdivision (
  posseobjectid                   number(9) not null,
  name                            varchar2(60) not null
) tablespace mediumdata;

alter table subdivision
add constraint subdivision_pk
primary key (
  posseobjectid
) using index tablespace mediumindexes;

create unique index subdivision_ix1
on subdivision (
  name
) tablespace mediumindexes;

