create table sdequeries_t (
  endpointid                      number(9) not null,
  fromlayername                   varchar2(100) not null,
  fromlayercolumnname             varchar2(30) not null,
  fromcolumnname                  varchar2(30) not null,
  tolayername                     varchar2(100) not null,
  tolayercolumnname               varchar2(30) not null,
  toindexdefid                    number(9)
) tablespace xxsmalltabs;

alter table sdequeries_t
add constraint sdequeries_pk
primary key (
  endpointid
) using index tablespace xxsmalltabs;

