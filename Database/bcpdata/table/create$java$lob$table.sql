create table create$java$lob$table (
  name                            varchar2(700),
  lob                             blob,
  loadtime                        date
) tablespace mediumdata
  lob (lob)
    store as (
      tablespace mediumdata
    );

