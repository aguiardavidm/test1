create table floodzones (
  floodzoneid                     number(10),
  floodmapno                      varchar2(40),
  floodzone                       varchar2(40),
  firmeffectivedate               varchar2(40),
  firmpanel                       number(10)
) tablespace mediumdata;

