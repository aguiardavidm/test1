create table inspectiontype_t (
  posseobjectid                   number not null,
  description                     varchar2(80) not null,
  validforutilityconnect          char(1),
  final                           char(1)
) tablespace mediumdata;

alter table inspectiontype_t
add constraint inspectiontype_pk
primary key (
  posseobjectid
) using index tablespace mediumindexes;

