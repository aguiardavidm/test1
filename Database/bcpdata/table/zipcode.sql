create table zipcode (
  posseobjectid                   number(9) not null,
  zipcode                         varchar2(5) not null
) tablespace mediumdata;

alter table zipcode
add constraint zipcode_pk
primary key (
  posseobjectid
) using index tablespace mediumindexes;

create unique index zipcode_ix1
on zipcode (
  zipcode
) tablespace mediumindexes;

