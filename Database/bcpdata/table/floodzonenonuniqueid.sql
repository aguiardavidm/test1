create table floodzonenonuniqueid (
  firmeffectivedate               varchar2(40),
  firmpanel                       number(10),
  floodmapno                      varchar2(40),
  floodzone                       varchar2(40),
  floodzoneid                     number
) tablespace mediumdata;

