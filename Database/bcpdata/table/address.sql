create table address (
  posseobjectidold                number(9),
  taxparcelnumber                 varchar2(30),
  addresshousenumber              number(5),
  addresssuite                    varchar2(6),
  lotnumber                       varchar2(10),
  blocknumber                     number(3),
  landarea                        number,
  zipcode                         varchar2(5),
  zipextension                    varchar2(4),
  externallinkvalue               varchar2(50),
  firmeffectivedate               varchar2(40),
  firmpanel                       number(10),
  floodmapno                      varchar2(40),
  floodzone                       varchar2(40),
  posseobjectid                   number(9) not null,
  building                        varchar2(30)
) tablespace mediumdata;

alter table address
add constraint posseobjectid_pk1
primary key (
  posseobjectid
) using index tablespace mediumdata;

create index address_ix1
on address (
  taxparcelnumber
) tablespace mediumindexes;

