create table parcel (
  legal_descr                     varchar2(4000),
  state_parcel_no                 varchar2(500) not null,
  account_no                      varchar2(50),
  total_net_acres                 number
) tablespace mediumdata;

alter table parcel
add constraint sys_c0014120
primary key (
  state_parcel_no
) using index tablespace mediumdata;

