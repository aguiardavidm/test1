create table processassignmentaudit_t (
  logicaltransactionid            number(9) not null,
  processid                       number(9) not null,
  userid                          number(9) not null,
  assignedto                      varchar2(30)
) tablespace mediumdata;

grant select
on processassignmentaudit_t
to abc;

