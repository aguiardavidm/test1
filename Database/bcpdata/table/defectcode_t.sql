create table defectcode_t (
  posseobjectid                   number(9) not null,
  codeid                          varchar2(5) not null,
  shortdescription                varchar2(60) not null,
  longdescription                 varchar2(300)
) tablespace mediumdata;

alter table defectcode_t
add constraint defectcode_pk
primary key (
  posseobjectid
) using index tablespace mediumindexes;

create unique index defectcode_ix_1
on defectcode_t (
  codeid
) tablespace mediumindexes;

