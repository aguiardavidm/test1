create table addressingobjecttemp (
  addressobjectid                 number(9) not null,
  apistreetid                     number(9) not null,
  externaladdressid               number(9) not null,
  addressnumber                   number(9) not null,
  suite                           varchar2(20)
) tablespace mediumdata;

