create table streettype (
  streettypeobjectid              number(9),
  name                            varchar2(15) not null,
  oldstreettypeobjectid           number(9)
) tablespace mediumdata;

create unique index streettype_ix1
on streettype (
  name
) tablespace mediumindexes;

create unique index streettype_pk
on streettype (
  streettypeobjectid
) tablespace mediumindexes;

