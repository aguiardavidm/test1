create table trade_t (
  posseobjectid                   number not null,
  description                     varchar2(40) not null,
  performprocesstypename          varchar2(30),
  useassignmentprocedure          char(1),
  assignmentprocedurename         varchar2(80),
  permittradecolumn               varchar2(30)
) tablespace mediumdata;

alter table trade_t
add constraint inspectiondiscipline_pk
primary key (
  posseobjectid
) using index tablespace mediumindexes;

create unique index inspectiondiscipline_ix_1
on trade_t (
  description
) tablespace mediumindexes;

