create table addresslinkobjectid (
  objectid                        number(9) not null,
  concatlist                      varchar2(393),
  linkvalue                       varchar2(500)
) tablespace mediumdata;

create index addresslinkobjectid_ix1
on addresslinkobjectid (
  objectid
) tablespace mediumdata;

create index addresslinkobjectid_ix2
on addresslinkobjectid (
  concatlist
) tablespace mediumdata;

create index addresslinkobjectid_ix3
on addresslinkobjectid (
  linkvalue
) tablespace mediumdata;

