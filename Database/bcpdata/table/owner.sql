create table owner (
  posseobjectid                   number(9),
  owner_name                      varchar2(4000),
  tax_district_no                 number,
  property_account_no             varchar2(30),
  first_name                      varchar2(50),
  last_name                       varchar2(50)
) tablespace mediumdata;

grant select
on owner
to posseextensions;

grant select
on owner
to possesys;

