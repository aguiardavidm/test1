create table city (
  posseobjectid                   number(9) not null,
  name                            varchar2(30) not null
) tablespace mediumdata;

alter table city
add constraint city_pk
primary key (
  posseobjectid
) using index tablespace mediumindexes;

create unique index city_ix1
on city (
  name
) tablespace mediumindexes;

