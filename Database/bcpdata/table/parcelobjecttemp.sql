create table parcelobjecttemp (
  parcelobjectid                  number(9) not null,
  externalid                      number(9) not null,
  parceldefid                     number(9) not null,
  netarces                        number(12, 2)
) tablespace mediumdata;

