create table parcelfloodzoneiterim_t (
  state_parcel_no                 varchar2(500),
  firmeffectivedate               varchar2(8),
  floodmapno                      varchar2(12),
  firmpanel                       varchar2(5),
  floodzone                       varchar2(4)
) tablespace mediumdata;

