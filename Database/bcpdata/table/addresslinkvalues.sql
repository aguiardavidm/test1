create table addresslinkvalues (
  adr                             number,
  concatlist                      varchar2(204)
) tablespace mediumdata;

create index addresslinkvalues_ix1
on addresslinkvalues (
  adr
) tablespace mediumdata;

create index addresslinkvalues_ix2
on addresslinkvalues (
  concatlist
) tablespace mediumdata;

