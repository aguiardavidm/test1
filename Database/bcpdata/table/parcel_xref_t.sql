create table parcel_xref_t (
  xrefid                          number,
  parentid                        varchar2(500) not null,
  childid                         varchar2(500) not null
) tablespace mediumdata;

grant select
on parcel_xref_t
to posseextensions;

grant select
on parcel_xref_t
to posseuser;

alter table parcel_xref_t
add constraint pk_xref
primary key (
  parentid,
  childid
) using index tablespace mediumdata;

alter table parcel_xref_t
add constraint fk_child_id
foreign key (
  childid
) references parcel (
  state_parcel_no
);

alter table parcel_xref_t
add constraint fk_parent_id
foreign key (
  parentid
) references parcel (
  state_parcel_no
);

