create table street (
  shortname                       varchar2(30) not null,
  streetobjectid                  number(9) not null,
  predirection                    varchar2(2),
  postdirection                   varchar2(2),
  formattedname                   varchar2(40),
  possestreetnameid               number(9)
) tablespace mediumdata;

alter table street
add constraint street_pk
primary key (
  streetobjectid
) using index tablespace mediumindexes;

create index street_ix1
on street (
  formattedname
) tablespace mediumindexes;

create index street_ix2
on street (
  possestreetnameid
) tablespace mediumdata;

