create table mappoint (
  posseobjectid                   number(9) not null,
  locationx                       varchar2(100),
  locationy                       varchar2(100),
  linkposseobjectid               number(9),
  linkposseobjectdefid            number(9)
) tablespace mediumdata;

grant select
on mappoint
to posseextensions;

alter table mappoint
add constraint mappoint_pk01
primary key (
  posseobjectid
) using index tablespace mediumdata;

create index mappoint_uk01
on mappoint (
  locationx
) tablespace mediumdata;

create index mappoint_uk02
on mappoint (
  locationy
) tablespace mediumdata;

create index mappoint_uk03
on mappoint (
  linkposseobjectid
) tablespace mediumdata;

create index mappoint_uk04
on mappoint (
  linkposseobjectdefid
) tablespace mediumdata;

