create or replace package pkg_Inspections as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  -- MM: SDE Query test. Remove this subtype when SDEQuery is fixed
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  /*--------------------------------------------------------------------------
   * CreateInspections - PUBLIC
   *------------------------------------------------------------------------*/
  procedure CreateInspections (
    a_ProcessId                   udt_Id,
    a_AsOfDate                    date,
    a_JobId                       udt_Id
  );
  
  -- MM: SDE Query test. Remove this function when SDE Query is fixed
  function SDEQueryTest(a_SearchLayerName                   varchar2,
                        a_SearchLayerColumnName             varchar2,
                        a_SearchValue                       varchar2,
                        a_ReturnLayerName                   varchar2,
                        a_ReturnLayerColumnName             varchar2
    ) return udt_StringList;

  function NextWorkDate(
    a_Date                        date default null
  ) return date;

  /*--------------------------------------------------------------------------
   * InspectionCompletion - PUBLIC
   *------------------------------------------------------------------------*/
  procedure InspectionCompletion (
    a_ProcessId                   udt_Id,
    a_AsOfDate                    date,
    a_Outcome                     varchar2
  );

  /*--------------------------------------------------------------------------
   * MonitorCompletionCondition - PUBLIC
   *------------------------------------------------------------------------*/
  function MonitorCompletionCondition (
    a_JobId                       udt_Id
  ) return varchar2;

  /*--------------------------------------------------------------------------
   * Find a zone based on a tax parcel number - PUBLIC
   *------------------------------------------------------------------------*/
  /*function FindZone (
    a_TaxParcelNumber             varchar2,
    a_ZoneDesc                    varchar2
  ) return varchar2;*/

  /*---------------------------------------------------------------------------
  * CanInspectionBeDone() - PUBLIC
  *--------------------------------------------------------------------------*/
  procedure CanInspectionBeCompleted(a_objectid     udt_id,
                                     a_asofdate     date
  );

  /*---------------------------------------------------------------------------
  * GenerateInspections() - PUBLIC
  *--------------------------------------------------------------------------*/
  procedure RelateDefaultInspTypesToJob(a_objectid     udt_id,
                                        a_asofdate     date);

  /****************************************************************************
  * CopyDefaultInspToRequest() - PUBLIC
  ****************************************************************************/
  procedure CopyDefaultInspToRequest(a_objectid     udt_id,
                                     a_asofdate     date);

  /****************************************************************************
  * CopyDefaultInspToRequest() - PUBLIC
  ****************************************************************************/
  procedure GenerateInspRequestRels(a_objectid     udt_id,
                                    a_asofdate     date);

  /*---------------------------------------------------------------------------
  * GenerateInspectionsChecklist() - PUBLIC
  *--------------------------------------------------------------------------*/
  procedure GenerateInspectionsChecklist(a_objectid     udt_id,
                                         a_asofdate     date,
                                         a_ObjectDefID  udt_id);

  /*---------------------------------------------------------------------------
   * DeleteIncompleteReqInspProc() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure DeleteIncompleteReqInspProc(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );

end pkg_Inspections;

 
/

grant debug
on pkg_inspections
to possedba;

grant execute
on pkg_inspections
to mhwyinterface;

grant execute
on pkg_inspections
to posseextensions;

create or replace package body pkg_Inspections as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  subtype udt_Boolean is api.pkg_Definition.udt_Boolean;
  subtype udt_IDList is api.pkg_Definition.udt_IDList;
  subtype udt_Outcome is api.pkg_Definition.udt_Outcome;
  subtype udt_StaffMember is api.pkg_Definition.udt_StaffMember;
  -- MM: SDE Query test. Uncomment this line when SDE Query is fixed
  --subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_EndPoint is api.pkg_Definition.udt_EndPoint;
  subtype udst_StringList is extension.udst_StringList;
  subtype udt_ObjectList is api.udt_ObjectList;
  gc_Package               constant varchar2(50) := 'bcpdata.pkg_Inspections.';

  /*--------------------------------------------------------------------------
   * CreateInspections - PUBLIC
   *------------------------------------------------------------------------*/
  procedure CreateInspections (
    a_ProcessId                   udt_Id,
    a_AsOfDate                    date,
    a_JobId                       udt_Id
  ) is
    t_CommercialInspection        varchar2(1);
    t_CommercialOrResidential     varchar2(20) := api.pkg_ColumnQuery.Value(a_JobId,'CommercialOrResidential');
    t_ConfirmationNumber          varchar2(20);
    t_DummyId                     udt_Id;
    t_EndPointId                  udt_Id;
    t_Failover                    varchar2(1) := 'N';
    t_GISInspectors               udt_ObjectList;
    t_GISQueryColumn              varchar2(4000);
    t_GISQueryLayer               varchar2(4000);
    t_GISSearchValue              varchar2(4000);
    t_Inspector                   varchar2(4000);
    t_InspectorId                 udt_Id;
    t_InspectorLoad               number;
    t_Inspectors                  udt_ObjectList;
    t_IsReInspection              varchar2(1) := api.pkg_Columnquery.value(a_Processid, 'IsReinspection');
    t_JobTypeID                   udt_Id;
    t_LayerName                   varchar2(50);
    t_MinInspectorLoad            number;
    t_ProcessId                   udt_Id;
    t_ProcessTypeId               udt_Id;
    t_ReinspectionDate            Date;
    t_RelationshipId              udt_Id;
    t_ResidentialInspection       varchar2(1);
    t_ScheduleDate                date;
    t_SeqNum                      number;
    t_TempInspectors              udt_ObjectList;
    t_Zones                       udst_StringList;
    t_ZoneStringList              udt_StringList;
    procedure SetErrorArguments is
    begin
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('ProcessId', a_ProcessId);
      api.pkg_Errors.SetArgValue('AsOfDate', a_AsOfDate);
    end;
  begin
    t_Inspectors := api.udt_ObjectList();
    t_GISInspectors := api.udt_ObjectList();
    t_TempInspectors := api.udt_ObjectList();
  
    -- future:
      -- validate that procedure is being run on correct process type (maybe)
      -- error if no inspection types are found
      -- error if multiple inspection type relationships are found (maybe)
      -- error if other data is invalid
      -- Perhaps we should determine some naming standards for "system table" objects
      -- should probably set the scheduled start date to something

    -- We may want to move this to a different expression or something, that gets passed in as a parameter
    if t_IsReInspection = 'Y' then
      Select pb.ReInspectionDate
        into t_ReinspectionDate
        from query.p_performbuildinginspection pb
       where pb.JobId = a_JobId
         and pb.DateCompleted = (Select Max(p.DateCompleted)
                                   from query.p_Performbuildinginspection p
                                  where p.JobId = a_JobId);
      if  t_ReinspectionDate is not null then
        t_ScheduleDate := t_ReinspectionDate;
      end if;
    else
      t_ScheduleDate := NextWorkDate();
    end if;

    for c in (select pr.JobId,
                     t.PerformProcessTypeName,
                     it.Description,
                     it.ObjectId InspectionTypeObjectId,
                     it.ObjectDefid InspectionTypeDefId,
                     t.ObjectId TradeObjectId
                from api.Processes pr
                join api.Relationships re on re.FromObjectId = pr.ProcessId
                join query.r_InspectionTypeRequestType ri on ri.RequestInspectionTypeObjectId = re.ToObjectId
                join query.o_InspectionType it on it.ObjectId = ri.InspectionTypeObjectId
                join query.r_InspectionTypeTrade td on td.InspectionTypeObjectId = it.ObjectId
                join query.o_Trade t on t.ObjectId = td.TradeObjectId
               where pr.ProcessId = a_ProcessId) loop
      -- Get process type and job information
      t_ProcessId := null;
      t_ProcessTypeId := api.pkg_ConfigQuery.ObjectDefIdForName(c.PerformProcessTypeName);
      if t_CommercialOrResidential = 'Commercial' then
        t_CommercialInspection := 'Y';
        t_ResidentialInspection := 'N';
      elsif t_CommercialOrResidential = 'Residential' then
        t_ResidentialInspection := 'Y';
        t_CommercialInspection := 'N';
      end if;
      
      begin
        -- Check whether process already exists, in an unassigned and unscheduled state
        select ProcessId
          into t_ProcessId
          from api.Relationships re
          Join api.Processes pr
            on re.FromObjectId = pr.ProcessId
         where pr.JobId = c.JobId
           and pr.ProcessTypeId = t_ProcessTypeId
           and pr.Outcome is null
           and pr.ScheduledStartDate is null
           and pr.AssignedStaff is null
           and re.ToObjectId = c.InspectionTypeObjectId;
      exception when no_data_found then
        null;
      end;

      if t_ProcessId is null then
        -- Create process, setting description to inspection type
        t_ProcessId := api.pkg_ProcessUpdate.New(c.JobId, t_ProcessTypeId, c.Description, t_ScheduleDate, null, null);

        --Set the Requested By On the new inspection
        api.pkg_columnupdate.SetValue(t_ProcessId,'RequestedBy', nvl(api.pkg_columnquery.value(api.pkg_securityquery.EffectiveUserId(), 'BusinessName'), api.pkg_columnquery.value(api.pkg_securityquery.EffectiveUserId(), 'FormattedName')));

        -- Relate the new inspection to the inspection type that it was generated for
        select ToEndPointId
          into t_EndPointId
          from api.RelationshipDefs rd
         where FromObjectDefId = t_ProcessTypeId
           and ToObjectDefId = c.InspectionTypeDefId;

        t_RelationshipId := api.pkg_RelationshipUpdate.New(t_EndPointId, t_ProcessId, c.InspectionTypeObjectId);

        -- Set Confirmation Number on generated Process
        t_ConfirmationNumber := api.Pkg_ColumnQuery.Value(c.JobId, 'ExternalFileNum');

        begin
          -- select the max number of ExternalFileNum starting at second '-' and add 1
          select nvl(max(to_number(substr(ExternalFileNum, instr(ExternalFileNum, '-', 1, 2) + 1))), 0) + 1
            into t_SeqNum
            from api.Processes pr
            join api.ProcessTypes pt
              on pt.Processtypeid = pr.Processtypeid
            join Trade tr
              on tr.performprocesstypename = pt.name
           where pr.JobId = c.JobId;
        exception when no_data_found then
          t_SeqNum := 1;
        end;

        api.pkg_ProcessUpdate.SetExternalFileNum(t_ProcessId, t_ConfirmationNumber || '-' || trim(to_char(t_SeqNum, '9900')));
        t_DummyId := pkg_Utils.RelationshipNew(a_ProcessId, t_ProcessId);
      else
        api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ScheduledStartDate', t_ScheduleDate);
      end if;

      -- Copy matching details to new process
      toolbox.pkg_Util.CopyObjectDetails(a_SourceObjectId => a_ProcessId, a_TargetObjectId => t_ProcessId, a_AsOfDate => a_AsOfDate);

      -- Get Inspectors based on Trade and Residential/Commercial permit type
      select api.udt_Object(u.ObjectId)
        bulk collect into t_Inspectors
        from query.r_UserTrade ut
        join query.u_Users u on u.UserId = ut.UserObjectId
       where ut.TradeObjectId = c.TradeObjectId
         and (u.CommercialInspector = t_CommercialInspection
          or u.ResidentialInspector = t_ResidentialInspection);

      if t_Inspectors.Count > 0 then
        -- GIS Routing search
        for d in (select i.ObjectId RoutingObjectId,
                         i.LayerName,
                         i.LayerReturnColumn,
                         i.QueryLayer
                    from query.r_InspectorRoutingTrade r
                    join query.o_InspectorRouting i on i.ObjectId = r.InspectorRoutingObjectId
                   where r.TradeObjectId = c.TradeObjectId
                     and (i.Commercial = t_CommercialInspection
                      or i.Residential = t_ResidentialInspection)) loop
          -- Get the GIS search layer and column name from the System Settings object
          if d.QueryLayer = 'Address' then
            t_GISQueryLayer := pkg_BCPSetting.GetSetting('GISAddressLayer');
            t_GISQueryColumn := pkg_BCPSetting.GetSetting('GISAddressColumn');
            t_GISSearchValue := api.pkg_ColumnQuery.Value(api.pkg_ColumnQuery.Value(c.JobId, 'AddressObjectId'), 'FormattedAddress');
          elsif d.QueryLayer = 'Parcel' then
            t_GISQueryLayer := pkg_BCPSetting.GetSetting('GISParcelLayer');
            t_GISQueryColumn := pkg_BCPSetting.GetSetting('GISParcelColumn');
            t_GISSearchValue := api.pkg_ColumnQuery.Value(api.pkg_ColumnQuery.Value(c.JobId, 'AddressObjectId'), 'TaxParcelNumber');
          end if;

          begin
            -- MM: Using SDEQueryTest till we get SDE query working
            --t_ZoneStringList := pkg_SDE.PerformQuery(t_GISQueryLayer, t_GISQueryColumn, t_GISSearchValue, d.LayerName, d.LayerReturnColumn);
            t_ZoneStringList := SDEQueryTest(t_GISQueryLayer, t_GISQueryColumn, t_GISSearchValue, d.LayerName, d.LayerReturnColumn);
            t_Zones := extension.pkg_Utils.ConvertToCastableStringList(t_ZoneStringList);
          exception when others then
            api.pkg_Errors.RaiseError(-20000,'Problem with GIS query for Inspector Routings. '||SQLERRM);
          end;
          
          -- Get Inspectors related to the Inspector Routing Assigment Object
          select api.udt_Object(x.ObjectId)
            bulk collect into t_TempInspectors
            from (select unique ir.InspectorObjectId ObjectId
                    from query.r_IRAInspectorRouting ar
                    join query.o_InspectorRoutingAssignment a on a.ObjectId = ar.InspRoutingAssignmentObjectId
                    join table(cast(t_Zones as extension.udst_stringlist)) t on t.Column_Value = a.InspectorRoutingLinkData
                    join query.r_IRAInspector ir on ir.InspRoutingAssignmentObjectId = a.ObjectId
                   where ar.InspectorRoutingObjectId = d.RoutingObjectId) x;
            
          -- Append each Inspector list from each Inspector Routing to one main list
          extension.pkg_CollectionUtils.Append(t_GISInspectors, t_TempInspectors);
          t_TempInspectors.Delete;
        end loop;
        -- Combine the list of Trade inspectors and GIS zone inspectors
        extension.pkg_CollectionUtils.Join(t_Inspectors, t_GISInspectors);
        
        if t_Inspectors.Count = 1 then
          -- Assign the inspection as there is no need to check the inspector load
          api.pkg_ProcessUpdate.Assign(t_ProcessId, t_Inspectors(1).ObjectId);
        else
          t_JobTypeId := api.pkg_ConfigQuery.ObjectDefIdForName('j_BuildingPermit');
          -- Assign the inspection based on Trade and GIS routing search to the Inspector with the lowest Load Complexity
          for x in 1..t_Inspectors.Count loop
            select nvl(sum(api.pkg_ColumnQuery.NumericValue(p.ProcessId,'Complexity')),0)
              into t_InspectorLoad
              from api.ProcessAssignments pa
              join api.Processes p on p.ProcessId = pa.ProcessId
              join api.Jobs j on j.JobId = p.JobId
              join api.JobTypes jt on jt.JobTypeId = j.JobTypeId
             where pa.UserId = t_Inspectors(x).ObjectId
               and p.Outcome is null
               and p.ScheduledStartDate = t_ScheduleDate
               and jt.JobTypeId = t_JobTypeId;
            
            -- Get the inspector with the lowest load
            if t_InspectorLoad = 0 then
              t_InspectorId := t_Inspectors(x).ObjectId;
              exit;
            else
              if t_MinInspectorLoad is null then
                t_MinInspectorLoad := t_InspectorLoad;
                t_InspectorId := t_Inspectors(x).ObjectId;
              elsif t_InspectorLoad < t_MinInspectorLoad then
                t_MinInspectorLoad := t_InspectorLoad;
                t_InspectorId := t_Inspectors(x).ObjectId;
              end if;
            end if;
          end loop;
          
          if t_InspectorId is not null then
            -- Assign to the inspector with the lowest load
            api.pkg_ProcessUpdate.Assign(t_ProcessId, t_InspectorId);
          else
            t_Failover := 'Y';
          end if;
        end if;
      end if;
      
      if t_Inspectors.Count = 0 or t_Failover = 'Y' then
        -- If there were no inspectors found for either the Trade or GIS routing search assign to the failover access group
        for e in (select u.UserId
                    from api.AccessGroups a
                    join api.AccessGroupUsers u on u.AccessGroupId = a.AccessGroupId
                   where a.Description = 'Inspector Assignment Coordinator') loop
          api.pkg_ProcessUpdate.Assign(t_ProcessId, e.UserId);
        end loop;
      end if;
      
      -- Clear values from Inspector lists
      t_Inspectors.Delete;
      t_GISInspectors.Delete;
      
    -- Loop through the next inspection
    end loop;
  end CreateInspections;
  
  -- MM: This is for testing GIS routing assignments until we get the SDE query working
  function SDEQueryTest(a_SearchLayerName                   varchar2,
                        a_SearchLayerColumnName             varchar2,
                        a_SearchValue                       varchar2,
                        a_ReturnLayerName                   varchar2,
                        a_ReturnLayerColumnName             varchar2
    ) return udt_StringList is
    t_StringList         udt_StringList;
    t_Index              number := 0;
  begin
    if a_ReturnLayerName = 'SDE.FRES_INSPZONECOM' then
      if substr(a_SearchValue, 1, 4) between 0 and 1000 then
        select 'south' bulk collect into t_StringList from dual;
      elsif substr(a_SearchValue, 1, 4) between 1001 and 2000 then
        select 'west' bulk collect into t_StringList from dual;
      elsif substr(a_SearchValue, 1, 4) between 2001 and 3000 then
        select 'east' bulk collect into t_StringList from dual;
      else
        select 'north' bulk collect into t_StringList from dual;
      end if;
    elsif a_ReturnLayerName = 'SDE.FRES_INSPZONERES' then
      if substr(a_SearchValue, 1, 4) between 1 and 1000 then
        select '1' bulk collect into t_StringList from dual;
      elsif substr(a_SearchValue, 1, 4) between 1001 and 2000 then
        select '2' bulk collect into t_StringList from dual;
      elsif substr(a_SearchValue, 1, 4) between 2001 and 3000 then
        select '3' bulk collect into t_StringList from dual;
      else
        select '4' bulk collect into t_StringList from dual;
      end if;
    end if;
    return t_StringList;
  end SDEQueryTest;

  function NextWorkDate(
    a_Date                        date default null
  ) return date is
    t_ScheduleDate                date;
    t_Date                        date;
    t_CutoffDate                  date;
    t_Time                        number;
  begin
    -- If before the cutoff time, schedule for next work date, otherwise, schedule for 2 work days out
    if a_date is null then
      t_Date := trunc(sysdate, 'MI');
    else
      t_Date := a_Date;
    end if;

    -- Get cutoff time
    t_Time := pkg_BCPSetting.GetSetting('InspectionCutoffTime');

    t_CutoffDate := to_date(to_char(trunc(t_Date), 'MMDDYYYY') || lpad(t_Time, 4, '0'), 'MMDDYYYYHH24MI');

    if t_Date <= t_CutoffDate then
      t_ScheduleDate := api.pkg_Utils.DatePlusWorkDays(t_Date, 1);
    else
      t_ScheduleDate := api.pkg_Utils.DatePlusWorkDays(t_Date, 2);
    end if;

    return t_ScheduleDate;
  end;


  /*--------------------------------------------------------------------------
   * InspectionCompletion - PUBLIC
   *------------------------------------------------------------------------*/
  procedure InspectionCompletion (
    a_ProcessId                   udt_Id,
    a_AsOfDate                    date,
    a_Outcome                     varchar2
  ) is
    t_ObjectId                    udt_Id;
    t_ObjectDefId                 udt_Id := toolbox.pkg_util.GetIdForName('o_CompletedInspection');
    t_RelationshipId              udt_Id;
  begin

    -- Check to See if inspection can be completed
    if a_Outcome is not null and a_Outcome <> 'Cancelled' then
      CanInspectionBeCompleted(a_ProcessId, a_asofdate);
    end if;

    -- Create CompletedInspections object if outcome set to something other than 'Cancelled'
    if a_Outcome is not null and a_Outcome <> 'Cancelled' then
      t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InspectorName', api.pkg_ColumnQuery.Value(a_ProcessId, 'InspectorName'));
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'CompletedDate', api.pkg_ColumnQuery.Value(a_ProcessId, 'DateCompleted'));
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InspectionType', api.pkg_ColumnQuery.Value(api.pkg_ColumnQuery.Value(a_ProcessId, 'InspectionTypeObjectId'), 'Description'));
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Trade', api.pkg_ColumnQuery.Value(api.pkg_ColumnQuery.Value(a_ProcessId, 'InspectionTypeObjectId'), 'TradeDescription'));
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InspectorComments', api.pkg_ColumnQuery.Value(a_ProcessId, 'InspectorComments'));
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InspectionNumber', api.pkg_ColumnQuery.Value(a_ProcessId, 'ExternalFileNum'));
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Outcome', api.pkg_ColumnQuery.Value(a_ProcessId, 'Outcome'));
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'ReinspectionFee', api.pkg_ColumnQuery.Value(a_ProcessId, 'ChargeReinspectionFee'));
      api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'PermitNumber', api.pkg_ColumnQuery.Value(a_ProcessId, 'JobExternalFileNum'));
    end if;

    -- Relate object to inspection process
    t_RelationshipId := pkg_Utils.RelationshipNew(a_ProcessId, t_ObjectId, 'Completed Inspection Object:');
  end;

  /*--------------------------------------------------------------------------
   * MonitorCompletionCondition - PUBLIC
   *------------------------------------------------------------------------*/
  function MonitorCompletionCondition (
    a_JobId                       udt_Id
  ) return varchar2 is
    t_Count                       pls_integer;
    t_ReturnValue                 varchar2(1);
  begin
    -- For each necessary trade, find if the permit has in inspection completed with the Final Inspection Performed outcome
    for c in (select tr.PosseObjectId,
                     tr.PermitTradeColumn,
                     tr.PerformProcessTypeName
                from Trade tr) loop
      if api.pkg_ColumnQuery.Value(a_JobId, c.PermitTradeColumn) = 'Y' then
        -- Main Trades
        select count(*)
          into t_Count
          from api.Processes pr
          join api.ProcessTypes pt
            on pr.ProcessTypeId = pt.ProcessTypeId
          join api.Relationships re
            on re.FromObjectId = pr.ProcessId
          join InspectionType it
            on it.PosseObjectId = re.ToObjectId
          join query.r_InspectionTypeTrade td
            on td.InspectionTypeObjectId = it.PosseObjectId
         where pt.Name = c.PerformProcessTypeName
           and pr.JobId = a_JobId
           and (pr.Outcome = 'Final Inspection Performed'
               or (pr.Outcome = 'Passed' and api.pkg_ColumnQuery.Value(pr.ProcessId, 'FinalInspection') = 'Y' ))
--           and td.TradeObjectId = c.PosseObjectId
;

        if t_Count = 0 then
          -- Additional Trades
          select count(*)
            into t_Count
            from api.Relationships re
            join api.Processes pr
              on re.FromObjectId = pr.ProcessId
            join InspectionType it
              on it.PosseObjectId = re.ToObjectId
            join query.r_InspectionTypeAddTrade td
              on td.InspectionTypeObjectId = it.PosseObjectId
           where pr.JobId = a_JobId
             and (pr.Outcome = 'Final Inspection Performed'
                 or (pr.Outcome = 'Passed' and api.pkg_ColumnQuery.Value(pr.ProcessId, 'FinalInspection') = 'Y' ))
             and td.TradeObjectId = c.PosseObjectId;
          if t_Count = 0 then
            return 'N';
          end if;

        end if;

      end if;
    end loop;

    return 'Y';
  end;



  /*--------------------------------------------------------------------------
   * Find a zone based on a tax parcel number - PUBLIC
   *------------------------------------------------------------------------*/
  function FindZone (
    a_TaxParcelNumber             varchar2,
    a_ZoneDesc                    varchar2
  ) return varchar2 is
    t_ZoneTypeObjectId            udt_Id;
    t_Zone                        varchar2(4000);
    t_ZoneLayer                   varchar2(100);
  procedure SetErrorArguments is
    begin
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('ProcessId', a_TaxParcelNumber);
      api.pkg_Errors.SetArgValue('AsOfDate', a_ZoneDesc);
    end;
  begin
    t_ZoneTypeObjectId := api.pkg_SimpleSearch.ObjectByIndex('o_InspectorRouting', 'Description', a_ZoneDesc);
    t_ZoneLayer := api.pkg_ColumnQuery.Value(t_ZoneTypeObjectId, 'LayerName');

      /*begin
        if pkg_SDE.IsSDEConnected = 'N' then
          pkg_SDE.ConnectSDE('gis1', 5151, 'gis_data', 'posse', 'tronix');
          --return 'Connecting';
        end if;
        t_Zone := pkg_SDE.PerformJavaQuery('GIS_DATA.SDE.PARCELS_PROVAL', 'ACCTNO', a_TaxParcelNumber, t_ZoneLayer, 'ZONE');
      exception when others then
        SetErrorArguments;
        api.pkg_Errors.RaiseError(-20000, substr(sqlerrm, 12)); --Don't show "ORA-20000: " in message
      end;

      -- Fix the error with more than 24 errors in a single stream.
      if (t_Zone = 'NETWORK I/O OPERATION FAILED.') then
        begin
          pkg_SDE.ConnectSDE('gis1', 5151, 'gis_data', 'posse', 'tronix');
          t_Zone := pkg_SDE.PerformJavaQuery('GIS_DATA.SDE.PARCELS_PROVAL', 'ACCTNO', a_TaxParcelNumber, t_ZoneLayer, 'ZONE');
        exception when others then
          SetErrorArguments;
          api.pkg_Errors.RaiseError(-20000, substr(sqlerrm, 12)); --Don't show "ORA-20000: " in message
        end;
      end if;

    if (t_Zone = 'java.lang.Exception: No Rows were returned') then
      t_Zone := null;
    end if;*/

    return t_Zone;
  end;


  /*--------------------------------------------------------------------------
   * FindInspectionTypes - PUBLIC
   *------------------------------------------------------------------------*/
   procedure FindInspectionTypes(a_Object            udt_ID,
                                 a_RelationshipDef   udt_ID,
                                 a_EndPoint          udt_EndPoint,
                                 a_SearchString      varchar2,
                                 a_Objects       out api.udt_ObjectList) is

  t_ReqInsTypeToReqInsProcEPId   udt_ID := api.pkg_configquery.endpointidforname('o_RequestInspectionType', 'RequestInspection');
  t_RequestInspBPEPId            udt_ID := api.pkg_configquery.endpointidforname('o_RequestInspectionType', 'BuildingPermit');
  t_RequestInspGPEPId            udt_ID := api.pkg_configquery.endpointidforname('o_RequestInspectionType', 'GeneralPermit');
  t_RequestInspTPEPId            udt_ID := api.pkg_configquery.endpointidforname('o_RequestInspectionType', 'TradePermit');
  t_PermitDefId                  udt_ID;
  t_PermitId                     udt_ID;

  begin

    select r2.toobjectid, e.ToObjectDefId
      into t_PermitId, t_PermitDefId
      from api.relationships r
      join api.relationships r2 on r2.fromobjectid = r.toobjectid
       and r2.endpointid in (t_RequestInspBPEPId, t_RequestInspGPEPId, t_RequestInspTPEPId)
      join api.endpoints e on e.endpointid = r2.endpointid
     where r.fromobjectid = a_Object
       and r.endpointid = t_ReqInsTypeToReqInsProcEPId
;

null;
  end;


  /*---------------------------------------------------------------------------
  * CanInspectionBeDone() - PUBLIC
  *--------------------------------------------------------------------------*/
  procedure CanInspectionBeCompleted(a_objectid     udt_id,
                                     a_asofdate     date)is

  t_Count                  udt_id := 0;
  t_FinalInspection        varchar2(1);
  t_FinalTrade             varchar2(1);
  t_JobId                  udt_id;
  t_ProcessId              udt_id;
  t_ProcessName            varchar2(30);

  begin

    t_ProcessId       := a_objectid ;
    t_FinalInspection := api.pkg_columnquery.Value(t_ProcessId, 'FinalInspection');
    t_FinalTrade      := api.pkg_columnquery.Value(t_ProcessId, 'FinalTrade');
    t_JobId           := api.pkg_columnquery.Value(t_ProcessId, 'JobId');
    t_ProcessName     := api.pkg_columnquery.Value(t_ProcessId, 'ObjectDefName');


    if t_FinalTrade = 'Y' then

        select count(*)
          into t_Count
          from api.processes p
          join api.processtypes pt on p.ProcessTypeId = pt.ProcessTypeId
        where p.JobId = t_JobId
          and pt.Name in ('p_PerformBuildingInspection',
                          'p_PerformElectricalInspection',
                          'p_PerformMechanicalInspection',
                          'p_PerformPlumbingInspection')
          and p.Outcome is null
          and p.ProcessId != t_ProcessId
          and pt.Name = t_ProcessName
          and api.Pkg_Columnquery.Value(p.ProcessId,'FinalTrade') = 'N'
          and api.pkg_columnquery.Value(p.ProcessId,'FinalInspection') = 'N';

      else if t_FinalInspection = 'Y' then

        select count(*)
          into t_Count
          from api.processes p
          join api.processtypes pt on p.ProcessTypeId = pt.ProcessTypeId
        where p.JobId = t_JobId
          and pt.Name in ('p_PerformBuildingInspection',
                          'p_PerformElectricalInspection',
                          'p_PerformMechanicalInspection',
                          'p_PerformPlumbingInspection')
          and p.Outcome is null
          and p.ProcessId != t_ProcessId;

      end if;

    end if;

    if t_Count > 0 then

      api.pkg_errors.RaiseError(-20001,'This inspection cannot be completed there are other outstanding inspections');

    end if;

  end CanInspectionBeCompleted;

  /*---------------------------------------------------------------------------
  * RelateDefaultInspTypesToJob() - PUBLIC
  *--------------------------------------------------------------------------*/
  procedure RelateDefaultInspTypesToJob(a_objectid     udt_id,
                                        a_asofdate     date)is

  t_JobId                  udt_id:= api.pkg_columnquery.Value(a_objectid,'JobId');
  t_JobName                varchar2(30);
  t_ConditionsFailed       number :=0;
  t_CurrentCondition       number :=0;
  t_NewRel                 number;
  t_RelEndpointId          number;

  begin
    t_JobName     := api.pkg_columnquery.Value(t_JobId,'ObjectDefName');
    t_RelEndpointId := api.pkg_configquery.EndPointIdForName(t_JobName, 'InspectionType');

    --loop through the inspection groups realted to my job type
    for c in(select ig.ObjectId
               from query.o_InspectionGroup ig
               join query.r_PermitJobTypeInspectionGroup pig on pig.InspectionGroup = ig.ObjectId
               join query.o_PermitJobTypes pjt on pjt.ObjectId = pig.PermitJobTypes
              where pjt.Name = t_JobName)loop
      dbms_output.put_line(c.objectid);
      --Loop through conditions for current inspection group and determine if all conditions are met.
      t_ConditionsFailed := 0;
      for c1 in (select igc.FieldName
                      , igc.InspectionGroupOperator
                      , igc.Condition
                   from query.o_Inspectiongroupcondition igc
                   join api.relationships r on r.FromObjectId = igc.ObjectId
                   join query.o_inspectiongroup ig on ig.ObjectId = r.ToObjectId
                  where ig.ObjectId = c.objectid) loop
        dbms_output.put_line(c1.fieldname);
        dbms_output.put_line('Verifying conditions');
        begin
          if (lower(c1.condition) like '%null%' and c1.inspectiongroupoperator = 'is') then
            execute immediate 'select 0 from dual where api.pkg_columnquery.value(' || t_JobId || ', ''' || c1.fieldname || ''') ' || c1.InspectionGroupOperator || ' ' || c1.condition into t_CurrentCondition;
          else
            execute immediate 'select 0 from dual where api.pkg_columnquery.value(' || t_JobId || ', ''' || c1.fieldname || ''') ' || c1.InspectionGroupOperator || ' ''' || c1.condition || '''' into t_CurrentCondition;
          end if;
        exception when NO_DATA_FOUND then
          t_CurrentCondition := null;
        end;
        --Null means the condition failed, so we add one to the counter for failed conditions.
        if t_CurrentCondition is null then
          t_ConditionsFailed := 1;
        else
          t_ConditionsFailed := 0;
        end if;
        --Exit loop if a condition has failed. No need to perform any more checks for this inspection group.
        exit when t_ConditionsFailed > 0;
      end loop;
      --Make sure all conditions were met before creating processes on the job.
      if t_ConditionsFailed > 0 then
        dbms_output.put_line('A condition is false, moving to next Inspection Group');
      else
        --Loop through all Inspection Types in Group that are NOT already related to the job
        for c2 in (select it.objectid
                     from query.o_inspectiongroup ig
                     join query.r_inspectiontypeinspectiongrou itig on itig.InspectionGroupObjectId = ig.ObjectId
                     join query.o_inspectiontype it on it.ObjectId = itig.InspectionTypeObjectId
                    where ig.ObjectId = c.objectid
                      and it.ObjectId not in (select it2.ObjectId
                                                from api.jobs j
                                                join api.relationships r on r.FromObjectId = j.JobId
                                                join query.o_inspectiontype it2 on it2.ObjectId = r.ToObjectId
                                               where j.JobId = t_JobId)) loop
          --Relate Inspection Types to Job
          t_NewRel := api.pkg_relationshipupdate.New(t_RelEndpointId, t_JobId, c2.objectid);
          --dbms_output.put_line('Make this relationship: ' || c2.objectid || ' on job: ' || t_JobId);
        end loop;
      end if;
    end loop;
    dbms_output.put_line('Done!');
--  exception when others then
--    dbms_output.put_line('Something broke');
  end RelateDefaultInspTypesToJob;

  /****************************************************************************
  * CopyDefaultInspToRequest() - PUBLIC
  ****************************************************************************/
  procedure CopyDefaultInspToRequest(a_objectid     udt_id,
                                     a_asofdate     date) is

    t_ProcessName        varchar2(4000) := api.pkg_columnquery.Value(a_objectid, 'ObjectDefName');
    t_RelEndpointId      number := api.pkg_configquery.EndPointIdForName(t_ProcessName, 'InspectionType');
    t_RelId              number;

  begin
    for c in (select it.objectid
                from api.relationships r
                join query.o_inspectiontype it on it.ObjectId = r.ToObjectId
               where r.FromObjectId = api.pkg_columnquery.Value(a_objectid, 'JobId')) loop
      dbms_output.put_line('Default Inspection Types: ' || c.objectid);
      t_RelId := api.pkg_relationshipupdate.new(t_RelEndpointId, a_objectid, c.objectid);
    end loop;
  end CopyDefaultInspToRequest;

  /****************************************************************************
  * GenerateInspRequestRels() - PUBLIC
  ****************************************************************************/
  procedure GenerateInspRequestRels(a_objectid     udt_id,
                                    a_asofdate     date) is
    t_ReqInspTypeDefId                             udt_id := api.pkg_configquery.ObjectDefIdForName('o_RequestInspectionType');
    t_ProcessName                                  varchar2(4000) := api.pkg_columnquery.Value(a_objectid, 'ObjectDefName');
    t_ReqInspTypeRelId                             number := api.pkg_configquery.EndPointIdForName(t_ProcessName, 'RequestInspectionType');
    t_InspTypeRelId                                udt_id;
    t_NewRel                                       udt_id;
    t_NewObj                                       udt_id;

  begin
    for c in (select riit.InspTypeId
                from query.p_requestinspection ri
                join query.r_RequestInsp_InspType riit on riit.RequestInspId = ri.ObjectId
               where riit.RequestThisInspection = 'Y'
                 and ri.ObjectId = a_objectid) loop
      t_NewObj := api.pkg_objectupdate.new(t_ReqInspTypeDefId);
      t_NewRel := api.pkg_relationshipupdate.New(t_ReqInspTypeRelId, a_objectid, t_NewObj);
      t_ProcessName := api.pkg_columnquery.value(t_NewObj, 'ObjectDefName');
      t_InspTypeRelId := api.pkg_configquery.EndPointIdForName(t_ProcessName, 'InspectionType');
      t_NewRel := api.pkg_relationshipupdate.New(t_InspTypeRelId, t_NewObj, c.insptypeid);
    end loop;
  end GenerateInspRequestRels;

  /*---------------------------------------------------------------------------
  * GenerateInspectionsChecklist() - PUBLIC
  *--------------------------------------------------------------------------*/
  procedure GenerateInspectionsChecklist(a_objectid     udt_id,
                                         a_asofdate     date,
                                         a_ObjectDefID  udt_id)is

  t_EndpointId                    udt_Id;
  t_DefToDocEndpointId            udt_id;
  t_NewRelID                      udt_id;
  t_InspectionTypeObjectId        udt_id;
  t_PreviousInspectionId          udt_id;
  t_DeficiencyDefId               udt_id;
  t_NewDeficiencyId               udt_id;
  t_JobId                         udt_id;
  t_NewDocRelId                   udt_id;

  begin

    --Find the endpoint id to use based on which Perform Inspection we are running this off.
    t_Endpointid := api.pkg_configquery.EndPointIdForName(a_ObjectDefID, 'ep_PerformChecklist');

    t_InspectionTypeObjectId := api.pkg_columnquery.value(a_objectid, 'InspectionTypeObjectId');
    t_DeficiencyDefId := api.pkg_configquery.ObjectDefIdForName('o_Deficiency');
    t_JobId := api.pkg_columnquery.NumericValue(a_objectid, 'JobId');
    t_DefToDocEndpointId := api.pkg_configquery.EndPointIdForName(t_DeficiencyDefId, 'Document');
    --Get the last process of the this inspection type to create a reinspection checklist
    select max(p.ProcessId)
      into t_PreviousInspectionId
      from api.processes p
      join api.processtypes pt on pt.ProcessTypeId = p.ProcessTypeId
     where p.JobId = t_JobId
       and pt.ProcessTypeId = a_ObjectDefId
       and api.pkg_columnquery.NumericValue(p.ProcessId, 'InspectionTypeObjectId') = t_InspectionTypeObjectId
       and p.ProcessId != a_objectid;

    --if this is not a reinspection, grab the default checklist
    if t_PreviousInspectionId is null then
      for c in (select cgr.ChecklistReqObjId ChecklistReqObjId
                  from query.r_ITChecklistGroup ig
                  join query.r_ChecklistGroupReq cgr on cgr.ChecklistGroupObjId = ig.ChecklistGroupObjectId
                 where ig.InspectionTypeId = t_InspectionTypeObjectId
                 )loop

        t_NewRelID := api.pkg_relationshipupdate.New(t_EndpointId, a_Objectid, c.ChecklistReqObjId);
      end loop;
    --otherwise relate the items from the previous checklist that failed and copy the deficiencies
    else
      for d in (select r.ToObjectId ChecklistReqObjId, r.RelationshipId
                  from api.relationships r
                 where r.FromObjectId = t_PreviousInspectionId
                   and r.EndPointId = t_EndPointId
                   and api.pkg_columnquery.Value(r.RelationshipId, 'PassFail') = 'Fail') loop

        t_NewRelID := api.pkg_relationshipupdate.New(t_EndpointId, a_Objectid, d.ChecklistReqObjId);

        --Make a copy of the old deficiencies and relate them
        for def in (select def.objectid,
                         api.pkg_columnquery.Value(def.objectid, 'Title') Title,
                         api.pkg_columnquery.Value(def.objectid, 'Comments') Comments,
                         api.pkg_columnquery.DateValue(def.objectid, 'ResolvedDate') ResolvedDate
                    from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_Deficiency',
                                                                                'InspChecklistReqRelId',
                                                                                d.Relationshipid) as
                                    api.udt_ObjectList)) def) loop
          if def.resolveddate is null then
            t_NewDeficiencyId := api.pkg_objectupdate.New(t_DeficiencyDefId);
            api.pkg_columnupdate.SetValue(t_NewDeficiencyId, 'CreateFromDeficiencyId', def.objectid);
            api.pkg_columnupdate.SetValue(t_NewDeficiencyId, 'InspChecklistReqRelId', t_NewRelID);
            api.pkg_columnupdate.SetValue(t_NewDeficiencyId, 'Title', def.Title);
            api.pkg_columnupdate.SetValue(t_NewDeficiencyId, 'Comments', def.Comments);

            --Relate the documents attached to the previous deficiency to the new one
            for doc in (select r.ToObjectId docid
                          from api.relationships r
                         where r.FromObjectId = def.Objectid
                           and r.EndPointId = t_DefToDocEndpointId) loop
              t_NewDocRelId := api.pkg_relationshipupdate.New(t_DefToDocEndpointId, t_NewDeficiencyId, doc.docid);

            end loop;
          end if;
        end loop;
      end loop;
    end if;

  end GenerateInspectionsChecklist;

  /*---------------------------------------------------------------------------
   * DeleteIncompleteReqInspProc() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure DeleteIncompleteReqInspProc(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    t_JobId             udt_Id;
  begin
    select JobId
      into t_JobId
      from api.processes p
     where p.ProcessId = a_ObjectId;

    -- Future TODO: Allow for comma separated list
    for c in (select p.ProcessId
                from api.Processes p
               where p.JobId = t_JobId
                 and p.ProcessTypeId = api.Pkg_ConfigQuery.ObjectDefIdForName('p_RequestInspection')
                 and p.CreatedByUserId = api.pkg_SecurityQuery.EffectiveUserId
                 and p.Outcome is null)loop
      api.pkg_ObjectUpdate.Remove(c.ProcessId, sysdate);
    end loop;
  end;
end pkg_Inspections;

/

