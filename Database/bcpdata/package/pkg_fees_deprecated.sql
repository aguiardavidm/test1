create or replace PACKAGE pkg_Fees_deprecated is

  /*--------------------------------------------------------------------------
   * ALBERT - Deprecated since there is a package in extension that is the same
   * and appears to be used. This one is not referenced anywhere.
   *------------------------------------------------------------------------*/

  -- Author  : TREVOR.HAMM
  -- Created : 1/21/2007 8:45:59 PM
  -- Purpose : Online fee payment

  -- Public type declarations
  subtype udt_id is api.pkg_definition.udt_Id;
  subtype udt_idlist is api.pkg_definition.udt_IdList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_ptdef is api.pkg_definition.udt_ProcessType;
  subtype udt_EndPoint is api.pkg_definition.udt_EndPoint;
  subtype udt_ObjectList is api.udt_ObjectList;

  /*--------------------------------------------------------------------------
   * Initialize()
   * Sets up the Proposed Payment job by copying all the fees related to the
   * a_FromObjectId via the endpoint named 'Fee' and defaulting the AmountToPay
   * to the TotalOwing from the fee.
   *------------------------------------------------------------------------*/
  Procedure Initialize (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_FromObjectId              udt_Id
  );

  /*--------------------------------------------------------------------------
   * VoidPayment()
   * If a_IsVoidedFlag = 'N' and a_ReasonForVoid is not null, then this
   * procedure voids a_PaymentId
   *------------------------------------------------------------------------*/
  Procedure VoidPayment (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_PaymentId                 udt_Id,
    a_IsVoidedFlag              varchar2,
    a_ReasonForVoid             varchar2,
    a_DateVoided                date
  );

  /*--------------------------------------------------------------------------
   * AdjustFee()
   * If a_AdjustAmountBy is not null and a_AdjustAmountBy is <> 0, this
   * procedure processes an adjustment on a_FeeId
   *------------------------------------------------------------------------*/
  Procedure AdjustFee (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_FeeId                     udt_Id,
    a_AdjustAmountBy            number,
    a_AdjustmentReason          varchar2,
    a_AdjustmentDate            date
  );

  /*--------------------------------------------------------------------------
   * ProcessProposedPayment()
   * This routine prepares a proposed payment for processing.  This includes
   * a bunch of error checking.
   *------------------------------------------------------------------------*/
  Procedure PrepareProposedPayment (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
   * ProcessProposedPayment()
   * This routine processes a proposed payment, turning it into an actual
   * payment.
   *
   * NOTE:
   * If this is a manual payment, then this routine does all of the payment
   * processing.
   * If this is an ECom transaction, then the Prepare Payment process has
   * already kicked off the call to BCExpressPay and this routine is being
   * called by PayDone.aspx to complete the second half of the payment
   * ( Which is actually done by calling pkg_ECom.Approved() )
   *------------------------------------------------------------------------*/
  Procedure ProcessProposedPayment (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*---------------------------------------------------------------------------
   * GetPaymentsForRefund()
   * Returns all of the payments related to the job refund process a_ObjectId
   * belongs to.
   *
   * NOTE:
   * This is expected to be run as a procedural relationship from POSSE
   *-------------------------------------------------------------------------*/
  procedure GetPaymentsForRefund (
    a_ObjectId				             udt_Id,
    a_EndPointId			             udt_Id,
    a_RelatedObjectIds			   out api.udt_ObjectList
  );

  /*--------------------------------------------------------------------------
   * PreparRefund()
   * This routine prepares a refund for processing.  This includes
   * a bunch of error checking.
   *------------------------------------------------------------------------*/
  Procedure PrepareRefund (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
   * ProcessRefund()
   * Processes a refund for payment a_PaymentId, distributing a_Amount across
   * a_PaymentId's transaction evenly.
   *------------------------------------------------------------------------*/
  Procedure ProcessRefund (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*---------------------------------------------------------------------------
   * GetSelectablePaymentMethods() -- PUBLIC
   *
   * If a_SearchString is NULL, then this is being called to populate a
   * drop down list. Otherwise, it is being called from an edit style lookup
   * where the user as entered some information.  Use the information to
   * search for payment methods that are active and user selectable
   *-------------------------------------------------------------------------*/
  procedure GetSelectablePaymentMethods (
    a_Object                    udt_ID,
    a_RelationshipDef           udt_ID,
    a_EndPoint                  udt_EndPoint,
    a_SearchString              varchar2,
    a_Objects               out udt_ObjectList
  );

 /* ------------------------------------------------------------
  * PayExternalFees() -- PUBLIC
  * 
  * This procedure blindly pays all fees for the job that is
  * related to the eCom job.  Used for demo purposes, will need
  * to change.  This procedure is called from the Accept Payment
  * process on the eCom job.
  * ------------------------------------------------------------*/
  procedure PayExternalFees(a_ObjectId  udt_Id, 
                            a_AsOfDate  date);

end pkg_Fees_deprecated;

 
/

create or replace PACKAGE BODY pkg_Fees_deprecated is

  /*-------------------------------------------------------------------------
   * Initialize() - Public
   *------------------------------------------------------------------------*/
  Procedure Initialize (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_FromObjectId              udt_Id
  ) is
    t_DestEndPointId            udt_Id;
    t_MethodDestEndPointId      udt_Id;
    t_PayDestEndPointId         udt_Id;
    t_DestObjectDefId           udt_Id;
    t_Fee                       pls_integer;
    t_Payment                   pls_integer;
    t_Method                    pls_integer;
    t_Fees                      udt_IdList;
    t_Payments                  udt_IdList;
    t_Methods                   udt_IdList;
    t_FromEndPointId            udt_Id;
    t_PayFromEndPointId         udt_Id;
    t_MethodFromEndPointId      udt_Id;
    t_FromObjectDefId           udt_Id;
    t_RelId                     udt_Id;
    t_PaymentObjectDefId        udt_Id;
    t_ProvideEndPointId         udt_Id;
    t_ProposedPaymentDefId      pls_integer;
    t_EP                        varchar2(2);
    t_ApplicationEndPointId     pls_integer;
    t_FinalFees                 api.udt_objectlist;
    
  begin
    t_FinalFees := api.udt_objectlist();
    /* Get Config id's */
    if a_FromObjectId is null then
      api.pkg_errors.RaiseError(-20001,
          'a_FromObjectId must contain a valid ObjectId.');
    end if;


    /* Create relationship to Application Job */ -- added by MF 2008-Aug-12
        /* The relationship existed already.. but wasn't being populated 
           I need it to be able to display declined transactions
        */
    t_ProposedPaymentDefId := api.pkg_configquery.ObjectDefIdForName('j_ProposedPayment');

    case api.pkg_ColumnQuery.Value( a_FromObjectid, 'ObjectDefName')
      when 'j_SubdivisionApplication' then t_EP := 'SA';
      when 'j_ApprovalApplication'    then t_EP := 'AA';
      when 'j_PermitApplication'      then t_EP := 'PA';
      else t_EP := null;
    end case;

    if t_EP is not null then
      t_ApplicationEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId, t_EP);
      t_RelId := api.pkg_RelationshipUpdate.New(t_ApplicationEndPointId, a_ObjectId, a_FromObjectId);
    end if;
    /**/    
    

    t_PaymentObjectDefId := api.pkg_configquery.ObjectDefIdForName('o_Payment');

    begin
      select a.ObjectDefId
        into t_DestObjectDefId
        from api.Objects a
       where a.ObjectId = a_ObjectId;
    exception
      when no_data_found then
        api.pkg_errors.RaiseError(-20001,
          'Cannot determine ObjectDefId for Proposed Payment Job [' ||
          a_ObjectId || '].');
    end;

    begin
      select a.ObjectDefId
        into t_FromObjectDefId
        from api.Objects a
        where a.ObjectId = a_FromObjectId;
    exception
    when no_data_found then
      api.pkg_errors.RaiseError(-20001,
          'Cannot determine ObjectDefId for From Object [' ||
          a_FromObjectId || '].');
    end;

    --Is a refund?
    if t_FromObjectDefId = api.pkg_configquery.ObjectDefIdForName('p_SA_ProvideRefund') then
      t_PayFromEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_FromObjectDefId, 'Payment');
      if t_PayFromEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'From object [' || a_FromObjectId || '] does not have a relationship named ''Payment''.');
      end if;

      t_PayDestEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_DestObjectDefId, 'Payment');
      if t_PayDestEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || a_ObjectId || '] does not have a relationship named ''Payment''.');
      end if;

      t_ProvideEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_DestObjectDefId, 'ProvideRefund');
      if t_PayDestEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || a_ObjectId || '] does not have a relationship named ''ProvideRefund''.');
      end if;
      t_RelId := api.pkg_RelationshipUpdate.New(t_ProvideEndPointId, a_ObjectId, a_FromObjectId);

      --Create a relationship to all the Payments on a_FromObjectId
      t_Payments := api.pkg_ObjectQuery.RelatedObjects(a_FromObjectId, t_PayFromEndPointId);
      for t_Payment in 1..t_Payments.count loop
        t_RelId := api.pkg_RelationshipUpdate.New(t_PayDestEndPointId, a_ObjectId, t_Payments(t_Payment));
      end loop;

      t_MethodFromEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_FromObjectDefId, 'PaymentMethod');
      if t_MethodFromEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'From object [' || a_FromObjectId || '] does not have a relationship named ''PaymentMethod''.');
      end if;

      t_MethodDestEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_DestObjectDefId, 'PaymentMethod');
      if t_MethodDestEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || a_ObjectId || '] does not have a relationship named ''PaymentMethod''.');
      end if;

      --Create a relationship to the Payment Methond on a_FromObjectId
      t_Methods := api.pkg_ObjectQuery.RelatedObjects(a_FromObjectId, t_MethodFromEndPointId);
      for t_Method in 1..t_Methods.count loop
        t_RelId := api.pkg_RelationshipUpdate.New(t_MethodDestEndPointId, a_ObjectId, t_Methods(t_Method));
      end loop;

      --Default details from a_FromObjectId
      api.pkg_columnupdate.setvalue(a_ObjectId,'TotalPayment',api.pkg_columnquery.value(a_FromObjectId,'Amount'));
      api.pkg_columnupdate.setvalue(a_ObjectId,'PaymentDate',api.pkg_columnquery.value(a_FromObjectId,'RefundDate'));
      api.pkg_columnupdate.setvalue(a_ObjectId,'PayeeReference',api.pkg_columnquery.value(a_FromObjectId,'ReferenceNumber'));
      api.pkg_columnupdate.setvalue(a_ObjectId,'ReceiptNumber',api.pkg_columnquery.value(a_FromObjectId,'ReceiptNumber'));
      api.pkg_columnupdate.setvalue(a_ObjectId,'ReasonForRefund',api.pkg_columnquery.value(a_FromObjectId,'ReasonForRefund'));

    --Is a Payment?
    else
      t_FromEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_FromObjectDefId, 'Fee');
      if t_FromEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'From object [' || a_FromObjectId || '] does not have a relationship named ''Fee''.');
      end if;

      t_DestEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_DestObjectDefId, 'Fee');
      if t_DestEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || a_ObjectId || '] does not have a relationship named ''Fee''.');
      end if;

      --Create a relationship to all the unpaid Fees on a_FromObjectId
     /* Select fq.ObjectId
        Bulk collect into t_Fees
        from api.fees f
        join query.o_contractor c on c.objectid = f.ResponsibleObjectId
        join query.o_fee fq on fq.FeeId = f.feeid 
        where (f.Amount + f.AdjustedAmount + f.PaidAmount) > 0
        and f.ResponsibleObjectId = a_FromObjectId;*/
      t_Fees := api.pkg_ObjectQuery.RelatedObjects(a_FromObjectId, t_FromEndPointId);
      
      for c in 1..t_Fees.Count loop
        if api.pkg_columnquery.numericvalue(t_Fees(c), 'TotalOwing') > 0
          then
            t_FinalFees.Extend(1);
            t_FinalFees(t_FinalFees.Count) := api.udt_object(t_Fees(c));
        end if;
      end loop;
      
      
      for x in 1..t_FinalFees.count loop
        t_RelId := api.pkg_RelationshipUpdate.New(t_DestEndPointId, a_ObjectId, t_FinalFees(x).objectid);
        --Default the amount to pay to the amount owing on the fee
        api.pkg_ColumnUpdate.SetValue(t_RelId, 'AmountToPay', api.pkg_ColumnQuery.Value(t_FinalFees(x).objectid, 'TotalOwing'));
      end loop;
    end if;
  end Initialize;


  /*-------------------------------------------------------------------------
   * VoidPayment() - Public
   *------------------------------------------------------------------------*/
  Procedure VoidPayment (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_PaymentId                 udt_Id,
    a_IsVoidedFlag              varchar2,
    a_ReasonForVoid             varchar2,
    a_DateVoided                date
  ) is
  begin
    if nvl(a_IsVoidedFlag, 'N') = 'N' and a_ReasonForVoid is not null then
      /* Void this payment */
      api.pkg_PaymentUpdate.Void(a_PaymentId, a_DateVoided);

      /* Now 'freeze' data voided */
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'DateVoided', a_DateVoided - 1);
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'DateVoided', a_DateVoided);
    end if;
  end VoidPayment;

  /*-------------------------------------------------------------------------
   * AdjustFee() - Public
   *------------------------------------------------------------------------*/
  Procedure AdjustFee (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_FeeId                     udt_Id,
    a_AdjustAmountBy            number,
    a_AdjustmentReason          varchar2,
    a_AdjustmentDate            date
  ) is
    t_Transaction               udt_Id;
  begin
    if a_AdjustAmountBy is not null and a_AdjustAmountBy <> 0 then
      if a_AdjustmentReason is null then
        api.pkg_errors.RaiseError(-20001,
            'Please enter a reason for this adjustment.');
      end if;
      t_Transaction := api.pkg_FeeUpdate.Adjust(a_FeeId, a_AdjustAmountBy,
                           a_AdjustmentReason, a_AdjustmentDate);

      /* Reset the flags */
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'AdjustAmountBy');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'AdjustmentReason');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'AdjustmentDate');
    end if;
  end AdjustFee;

  /*-------------------------------------------------------------------------
   * PrepareProposedPayment() - Public
   *------------------------------------------------------------------------*/
  Procedure PrepareProposedPayment (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_EComFlag                  boolean;
    t_JobId                     udt_Id;
    t_PaymentDefId              udt_Id;
    t_PaymentMethod             varchar2(100);
    t_PaymentMethodId           udt_Id;
    t_ProcessPaymentDefId       udt_Id;
    t_ProposedPaymentDefId      udt_Id;
    t_PayeeReference            varchar2(60);
    t_ReceiptNumber             varchar2(60);
    t_ReceiptNumberLabel        varchar2(100);
    t_ReferenceLabel            varchar2(100);
    t_ReferenceRequiredFlag     boolean;
    t_TempIds                   udt_IdList;
    t_TotalAmountsToPay         number;
    t_TotalPayment              number;

     begin
       
   
    /* Get DefId's and Proposed Payment Info */
    select a.JobId,
           b.TotalPayment,
           b.ReceiptNumber,
           b.PayeeReference
      into t_JobId,
           t_TotalPayment,
           t_ReceiptNumber,
           t_PayeeReference
      from api.Processes a
      join query.j_ProposedPayment b
        on b.JobId = a.JobId
      where a.ProcessId = a_ProcessId;
    t_ProcessPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                                 'p_ProcessPayment');
    if t_ProcessPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                                 'Cannot get the DefId for p_ProcessPayment.');
    end if;
    t_ProposedPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                                 'j_ProposedPayment');
    if t_ProposedPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                                 'Cannot get the DefId for j_ProposedPayment.');
    end if;
    t_PaymentDefId := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    if t_PaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                                 'Cannot get the DefId for o_Payment.');
    end if;

    /* Get Payment Method related info */
    t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
        api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId,
                                 'PaymentMethod'));
    if t_TempIds.count != 1 then
      api.pkg_errors.RaiseError(-20001, 'Please choose a Payment Method.');
    end if;
    t_PaymentMethodId := t_TempIds(1);
    t_PaymentMethod := api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'Name');
    t_ReceiptNumberLabel := api.pkg_ColumnQuery.Value(t_PaymentMethodId,
                                 'ReceiptNumberLabel');
    t_ReferenceLabel := api.pkg_ColumnQuery.Value(t_PaymentMethodId,
                                 'ReferenceLabel');
    t_ReferenceRequiredFlag := case when api.pkg_ColumnQuery.Value(
                                 t_PaymentMethodId, 'ReferenceRequiredFlag') =
                                 'Y' then true else false end;
    t_EComFlag := case when api.pkg_ColumnQuery.Value(t_PaymentMethodId,
                                 'EComFlag') = 'Y' then true else false end;

    /* Check for required data */
    if t_TotalPayment is null or t_TotalPayment = 0 then
      api.pkg_errors.RaiseError(-20001,
                                 'Please enter the Total Amount to pay.');
    elsif t_TotalPayment < 0 then
      api.pkg_errors.RaiseError(-20001,
                                 'Total Amount to pay must be positive.');
    end if;

    /* Set the payment date */
    if api.pkg_ColumnQuery.DateValue(t_JobId, 'PaymentDate') is null then
      api.pkg_ColumnUpdate.SetValue(t_JobId, 'PaymentDate', trunc(sysdate));
    end if;

    if t_ReferenceRequiredFlag and t_PayeeReference is null then
      api.pkg_errors.RaiseError(-20001,
                     'A Payment Method of ' || t_PaymentMethod ||
                     ' requires that you enter a ' || t_ReferenceLabel || '.');
    end if;

    /* Sum up the amounts to pay and make sure they're all valid */
    t_TotalAmountsToPay := 0;
    for c in (select api.pkg_ColumnQuery.Value(b.ToObjectId, 'FeeId') FeeId,
                     api.pkg_ColumnQuery.Value(b.RelationshipId, 'AmountToPay')
                         AmountToPay
                from api.relationshipdefs a
                     join api.Relationships b
                       on b.FromObjectId = t_JobId
                      and b.EndPointId = a.FromEndPointId
                where a.FromEndPointName = 'Fee') loop

      if c.AmountToPay is null then
        api.pkg_errors.RaiseError(-20001,
                       'Please enter the amount to pay for each fee.');
      elsif c.AmountToPay < 0 then
        api.pkg_errors.RaiseError(-20001,
                       'Each amount to pay must be positive.');
      end if;

      t_TotalAmountsToPay := t_TotalAmountsToPay + c.AmountToPay;

    end loop;

    if t_TotalPayment <> t_TotalAmountsToPay then
      api.pkg_errors.RaiseError(-20001, 'Total Amount to pay (' ||
          to_char(t_TotalPayment, 'FM$999,999,999,999,990.00') ||
          ') must equal to the sum of all the individual amounts (' ||
          to_char(t_TotalAmountsToPay, 'FM$999,999,999,999,990.00') || ').');
    end if;

    if not t_EComFlag then

      if t_ReceiptNumber is null then
        api.pkg_errors.RaiseError(-20001, 'Please enter a receipt number.');
                       --t_ReceiptNumberLabel || ' from the POS receipt.');
      end if;

    end if;

  end PrepareProposedPayment;


  /*-------------------------------------------------------------------------
   * ProcessProposedPayment() - Public
   *------------------------------------------------------------------------*/
  Procedure ProcessProposedPayment (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_AmountsToPay              api.pkg_definition.udt_NumberList;
    t_CardTypeCode              varchar2(2);
    t_EComFlag                  char(1);
    t_FeeCount                  pls_integer;
    t_FeeIds                    udt_IdList;
    t_JobId                     udt_Id;
    t_ToJobId                   udt_Id;
    t_ProcessId                 udt_Id;
    t_PayeeReference            varchar2(60);
    t_PaymentDate               date;
    t_PaymentDefId              udt_Id;
    t_PaymentId                 udt_Id;
    t_PaymentMethodId           udt_Id;
    t_PaymentObjectId           udt_Id;
    t_ProcessPaymentDefId       udt_Id;
    t_ProposedPaymentDefId      udt_Id;
    t_PublicReferenceNumber     varchar2(60);
    t_ReceiptNumber             varchar2(60);
    t_RelId                     udt_Id;
    t_SequenceId                udt_Id;
    t_TempIds                   udt_IdList;
    t_TotalAmountsToPay         number;
    t_TotalPayment              number;
  begin
     
    /* Get DefId's */
    select a.JobId,
           b.TotalPayment,
           b.ReceiptNumber,
           b.PayeeReference,
           b.PaymentDate,
           b.EComFlag,
           b.PublicReferenceNumber
      into t_JobId,
           t_TotalPayment,
           t_ReceiptNumber,
           t_PayeeReference,
           t_PaymentDate,
           t_EComFlag,
           t_PublicReferenceNumber
      from api.Processes a
      join query.j_ProposedPayment b
        on b.JobId = a.JobId
      where a.ProcessId = a_ProcessId;
--raise_application_error( -20000, to_char( t_PaymentDate, 'yyyy/mm/dd hh24:mi:ss'));
    t_ProcessPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                                'p_ProcessPayment');
    if t_ProcessPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                                'Cannot get the DefId for p_ProcessPayment.');
    end if;
    t_ProposedPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                                'j_ProposedPayment');
    if t_ProposedPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                                'Cannot get the DefId for j_ProposedPayment.');
    end if;
    t_PaymentDefId := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    if t_PaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for o_Payment.');
    end if;

    if t_EComFlag <> 'Y' then
      /* Get Payment Method related info */
      t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
          api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId,
                                                'PaymentMethod'));
      if t_TempIds.count != 1 then
        api.pkg_errors.RaiseError(-20001, 'Please choose a Payment Method.');
      end if;
      t_PaymentMethodId := t_TempIds(1);

    else

      /* Get Payment Method for method used by BCExpressPay + use it instead */
      t_CardTypeCode := Rtrim(api.pkg_ColumnQuery.Value(t_JobId,
                                'ECMCardTypeCode'));
      if t_CardTypeCode is not null then

        select max(ObjectId)
          into t_PaymentMethodId
          from query.o_PaymentMethod
          where BCExpressPayCardTypeCode = t_CardTypeCode;
        if t_PaymentMethodId is null then
          api.pkg_Errors.RaiseError(-20001,
              'Cannot find Payment Method for BCExpressPayCardType [' ||
              t_CardTypeCode || ']');
        end if;

      end if;

      /* For ECOM, we have to generate the receipt number */
      begin
        select SequenceId into t_SequenceId
          from api.Sequences where Description = 'Receipt Number';
      exception when no_data_found then
        api.pkg_errors.RaiseError(-20001,
                       'Cannot get sequence id for "Receipt Number" sequence.');
      end;
      t_ReceiptNumber := api.pkg_Utils.GenerateExternalFileNum('[9999999]',
                                       t_SequenceId);
      t_PayeeReference := t_PublicReferenceNumber;

    end if;

    /* Sum up the amounts to pay and make sure they're all valid */
    t_TotalAmountsToPay := 0;
    t_FeeCount := 0;
    for c in (select api.pkg_ColumnQuery.Value(b.ToObjectId, 'FeeId') FeeId,
                     api.pkg_ColumnQuery.Value(b.RelationshipId, 'AmountToPay')
                         AmountToPay
                from api.relationshipdefs a
                     join api.Relationships b
                       on b.FromObjectId = t_JobId
                      and b.EndPointId = a.FromEndPointId
                where a.FromEndPointName = 'Fee'
                  and api.pkg_ColumnQuery.Value(b.RelationshipId, 'AmountToPay') > 0) loop

      if c.AmountToPay is null then
        api.pkg_errors.RaiseError(-20001,
            'Please enter the amount to pay for each fee.');
      elsif c.AmountToPay < 0 then
        api.pkg_errors.RaiseError(-20001,
                                  'Each amount to pay must be positive.');
      end if;

      t_FeeCount := t_FeeCount + 1;
      t_FeeIds(t_FeeCount) := c.FeeId;
      t_AmountsToPay(t_FeeCount) := c.AmountToPay;
      t_TotalAmountsToPay := t_TotalAmountsToPay + c.AmountToPay;

    end loop;

    /* Pay the POSSE Fees */
    begin
      t_PaymentId := api.pkg_PaymentUpdate.New(t_PaymentDate, t_FeeIds,
                                               t_AmountsToPay);
      api.pkg_PaymentUpdate.Modify(t_PaymentId, t_PayeeReference,
                                   t_ReceiptNumber);
    exception
    when no_data_found then
      api.pkg_errors.RaiseError(-20001,
                     'Create payment could not find one of the fees.');
    end;

    /* Register the Financial Transactions that Posse created */
    t_PaymentObjectId := api.pkg_objectupdate.RegisterExternalObject(
                             'o_Payment', t_PaymentId);
    for i in (select transactionId from api.paymenttransactions
               where paymentId = t_PaymentId) loop
      api.pkg_objectupdate.RegisterExternalObject('o_PaymentTransactions',
                                                  i.transactionid);
      api.pkg_objectupdate.RegisterExternalObject('o_FeeTransactions',
                                                  i.transactionid);
    end loop;

    /* Update the Payment Method */
    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'PaymentMethod'),
                            t_PaymentObjectId, t_PaymentMethodId);

    /* Create a relationship to the new payment object */
    t_RelId := api.pkg_RelationshipUpdate.New(
                   api.pkg_ConfigQuery.EndPointIdForName(
                                       t_ProposedPaymentDefId,'Payment'),
                   t_JobId, t_PaymentObjectId);

    /* Create a "Check Submission" process
       and put "Payment received" in ReasonForUpdate  */
    /*select jobid into t_toJobId from api.fees where feeid = t_FeeIds(1);
    t_processid := api.pkg_processupdate.new( t_toJobId,
                       api.pkg_objectdefquery.idforname('p_CheckSubmission'),
                       null, null, null, null );
    api.pkg_columnupdate.setvalue(t_processid, 'ReasonForUpdate',
                                  'Payment received');*/

  end ProcessProposedPayment;


  /*---------------------------------------------------------------------------
   * GetPaymentsForRefund() - public
   *-------------------------------------------------------------------------*/
  procedure GetPaymentsForRefund (
    a_ObjectId                                 udt_Id,
    a_EndPointId                            udt_Id,
    a_RelatedObjectIds                  out api.udt_ObjectList
  ) is
    t_EndPoints                    udt_IdList;
    t_JobId                        udt_Id;
    t_Payments                     udt_IdList;
    t_VoidedPayment                pls_integer;
    t_PaymentId                    udt_Id;
    t_PaymentCnt                   pls_integer;
  begin
    a_RelatedObjectIds := api.udt_ObjectList();

    /* Get the EndPointId for the Payment relationship */
    select r.ToEndPointId
      bulk collect into t_EndPoints
      from api.relationshipdefs r
      join api.jobtypes t
        on t.JobTypeId = r.FromObjectDefId
      join api.ObjectDefs p
        on p.ObjectDefId = r.ToObjectDefId
      where t.name in('j_SubdivisionApplication', 'j_PermitApplication',
                      'j_ApprovalApplication')
        and p.Name = 'o_Payment';

    /* Get our job id */
    select a.JobId
      into t_JobId
      from api.Processes a
      where a.ProcessId = a_ObjectId;

    t_Payments := api.pkg_ObjectQuery.RelatedObjects(t_JobId, t_EndPoints);

    for i in 1..t_Payments.count loop
      t_PaymentId := api.pkg_columnquery.value(t_Payments(i),'PaymentId');

      select count(*)
        into t_voidedpayment
        from api.paymenttransactions pt
       where pt.paymentid = t_PaymentId
         and pt.VoidedByTransactionId is not null;

      if t_voidedpayment = 0 then
        a_RelatedObjectIds.extend(1);
        t_PaymentCnt := nvl(t_PaymentCnt,0) + 1;
              a_RelatedObjectIds(t_PaymentCnt) := api.udt_Object(t_Payments(i));
      end if;
    end loop;

  end;

  /*-------------------------------------------------------------------------
   * PrepareRefund() - Public
   *------------------------------------------------------------------------*/
  Procedure PrepareRefund (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_JobId                     udt_Id;
    t_PaymentDefId              udt_Id;
    t_PaymentMethod             varchar2(100);
    t_PaymentMethodId           udt_Id;
    t_ProcessPaymentDefId       udt_Id;
    t_ProposedPaymentDefId      udt_Id;
    t_PayeeReference            varchar2(60);
    t_ReceiptNumber             varchar2(60);
    t_ReceiptNumberLabel        varchar2(100);
    t_ReferenceLabel            varchar2(100);
    t_ReferenceRequiredFlag     boolean;
    t_TempIds                   udt_IdList;
    t_TotalAmountsToRefund      number;
    t_TotalPayment              number(15,2);
    t_Transactions              udt_IdList;
  begin
    /* Get DefId's and Proposed Payment Info */
    select a.JobId,
           b.TotalPayment,
           b.ReceiptNumber,
           b.PayeeReference
      into t_JobId,
           t_TotalPayment,
           t_ReceiptNumber,
           t_PayeeReference
      from api.Processes a
      join query.j_ProposedPayment b
        on b.JobId = a.JobId
      where a.ProcessId = a_ProcessId;

    t_ProcessPaymentDefId := api.pkg_ObjectDefQuery.IdForName('p_ProcessPayment');
    if t_ProcessPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for p_ProcessPayment.');
    end if;
    t_ProposedPaymentDefId := api.pkg_ObjectDefQuery.IdForName('j_ProposedPayment');
    if t_ProposedPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for j_ProposedPayment.');
    end if;
    t_PaymentDefId := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    if t_PaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for o_Payment.');
    end if;

    /* Get Payment Method related info */
    t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
        api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId, 'PaymentMethod'));
    if t_TempIds.count != 1 then
      api.pkg_errors.RaiseError(-20001, 'Please choose a Payment Method.');
    end if;
    t_PaymentMethodId := t_TempIds(1);
    t_PaymentMethod := api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'Name');
    t_ReceiptNumberLabel := api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'ReceiptNumberLabel');
    t_ReferenceLabel := api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'ReferenceLabel');
    t_ReferenceRequiredFlag := case when api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'ReferenceRequiredFlag') = 'Y' then true else false end;

    /* Check for required data */
    if t_TotalPayment is null or t_TotalPayment = 0 then
      api.pkg_errors.RaiseError(-20001, 'Please enter the Total Refund Amount.');
    elsif t_TotalPayment < 0 then
      api.pkg_errors.RaiseError(-20001, 'Total Refund Amount must be positive.');
    end if;

    /* Set the payment date */
    if api.pkg_ColumnQuery.DateValue(t_JobId, 'PaymentDate') is null then
      api.pkg_ColumnUpdate.SetValue(t_JobId, 'PaymentDate', trunc(sysdate));
    end if;

    if t_ReferenceRequiredFlag and t_PayeeReference is null then
      api.pkg_errors.RaiseError(-20001, 'A Payment Method of ' || t_PaymentMethod || ' requires that you enter a ' || t_ReferenceLabel || '.');
    end if;

    --Sum up the amounts to pay and make sure they're all valid
    t_TotalAmountsToRefund := 0;
    for c in (select ft.feeid,
                     api.pkg_ColumnQuery.Value(api.pkg_simplesearch.ObjectByIndex('o_FeeTransactions','TransactionID',ft.transactionid),
                                               'AmountToRefund') AmountToRefund
                from api.relationshipdefs a
                join api.Relationships b on b.FromObjectId = t_JobId
                                         and b.EndPointId = a.FromEndPointId
                join api.paymenttransactions p on p.paymentid = api.pkg_columnquery.value(b.ToObjectId,'PaymentId')
                join api.feetransactions ft on ft.transactionid = p.TransactionId
                where a.FromEndPointName = 'Payment') loop

      if c.AmountToRefund is null then
        api.pkg_errors.RaiseError(-20001, 'Please enter the amount to refund for each fee.');
      elsif c.AmountToRefund < 0 then
        api.pkg_errors.RaiseError(-20001, 'Each amount to refund must be positive.');
      end if;

      t_TotalAmountsToRefund := t_TotalAmountsToRefund + c.AmountToRefund;
    end loop;

    if t_TotalPayment <> t_TotalAmountsToRefund then
      api.pkg_errors.RaiseError(-20001, 'Total Refund Amount (' || to_char(t_TotalPayment, 'FM$999,999,999,999,990.00') || ') must equal to the sum of all the individual amounts (' || to_char(t_TotalAmountsToRefund, 'FM$999,999,999,999,990.00') || ').');
    end if;

    if t_ReceiptNumber is null then
      api.pkg_errors.RaiseError(-20001, 'Please enter a receipt number.');--' || t_ReceiptNumberLabel || ' from the POS receipt.');
    end if;

  end PrepareRefund;


  /*-------------------------------------------------------------------------
   * ProcessRefund() - Public
   *------------------------------------------------------------------------*/
  Procedure ProcessRefund (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_AmountsToRefund           api.pkg_definition.udt_NumberList;
    t_FeeCount                  pls_integer;
    t_FeeIds                    udt_IdList;
    t_JobId                     udt_Id;
    t_PaymentDefId              udt_Id;
    t_PaymentMethodId           udt_Id;
    t_ProcessPaymentDefId       udt_Id;
    t_ProposedPaymentDefId      udt_Id;
    t_RelId                     udt_Id;
    t_TempIds                   udt_IdList;
    t_Payments                  udt_IdList;
    t_TotalAmountsToRefund      number;
    t_RefundId                  udt_Id;
    t_RefundObjectId            udt_Id;
    t_RefundDate                date;
    t_PayeeReference            varchar2(60);
    t_ReceiptNumber             varchar2(60);
    t_Reason                    varchar2(4000);
    t_PayEndPointId             udt_Id;
    t_RefundEndPointId          udt_Id;
  begin
    /* Get DefId's */
    select a.JobId,
           b.PaymentDate,
           b.PayeeReference,
           b.ReceiptNumber,
           b.ReasonForRefund
      into t_JobId,
           t_RefundDate,
           t_PayeeReference,
           t_ReceiptNumber,
           t_Reason
      from api.Processes a
      join query.j_ProposedPayment b
        on b.JobId = a.JobId
      where a.ProcessId = a_ProcessId;
    t_ProcessPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                     'p_ProcessPayment');
    if t_ProcessPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                     'Cannot get the DefId for p_ProcessPayment.');
    end if;
    t_ProposedPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                     'j_ProposedPayment');
    if t_ProposedPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                     'Cannot get the DefId for j_ProposedPayment.');
    end if;
    t_PaymentDefId := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    if t_PaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                     'Cannot get the DefId for o_Payment.');
    end if;

    t_PayEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
                     t_ProposedPaymentDefId, 'Payment');
    if t_PayEndPointId is null then
      api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || t_JobId ||
                     '] does not have a relationship named ''Payment''.');
    end if;

    t_RefundEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
                     t_ProposedPaymentDefId, 'Refund');
    if t_PayEndPointId is null then
      api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || t_JobId ||
                     '] does not have a relationship named ''Refund''.');
    end if;

    t_Payments := api.pkg_ObjectQuery.RelatedObjects(t_JobId, t_PayEndPointId);

    /* Get Payment Method related info */
    t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
        api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId,
                                 'PaymentMethod'));
    if t_TempIds.count != 1 then
      api.pkg_errors.RaiseError(-20001, 'Please choose a Payment Method.');
    end if;
    t_PaymentMethodId := t_TempIds(1);

    /* Sum up the amounts to pay and make sure they're all valid */
    t_TotalAmountsToRefund := 0;
    t_FeeCount := 0;

    for c in (select ft.feeid,
                     api.pkg_ColumnQuery.Value(
                       api.pkg_simplesearch.ObjectByIndex('o_FeeTransactions',
                                            'TransactionID',ft.transactionid),
                       'AmountToRefund') AmountToRefund
                from api.relationshipdefs a
                join api.Relationships b
                  on b.FromObjectId = t_JobId and b.EndPointId=a.FromEndPointId
                join api.paymenttransactions p
                  on p.paymentid = api.pkg_columnquery.value(b.ToObjectId,
                                                             'PaymentId')
                join api.feetransactions ft
                  on ft.transactionid = p.TransactionId
                where a.FromEndPointName = 'Payment') loop

      if c.AmountToRefund is null then
        api.pkg_errors.RaiseError(-20001,
            'Please enter the amount to refund for each fee.');
      elsif c.AmountToRefund < 0 then
        api.pkg_errors.RaiseError(-20001,
            'Each amount to refund must be positive.');
      end if;

      t_FeeCount := t_FeeCount + 1;
      t_FeeIds(t_FeeCount) := c.FeeId;
      t_AmountsToRefund(t_FeeCount) := c.AmountToRefund * -1;
      t_TotalAmountsToRefund := t_TotalAmountsToRefund + c.AmountToRefund;

    end loop;

    /* Create the refund (negative payment as POSSE finance doesn't actually
       support refunds) */
    begin
      t_RefundId := api.pkg_PaymentUpdate.New(t_RefundDate, t_FeeIds,
                                              t_AmountsToRefund);
      api.pkg_PaymentUpdate.Modify(t_RefundId, t_PayeeReference,
                                   t_ReceiptNumber);
    exception
    when no_data_found then
      api.pkg_errors.RaiseError(-20001,
                     'Create refund could not find one of the fees.');
    end;

    /* A search from here won't do the dynamic registration, so register
       the new payment manually */
    t_RefundObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
                            'o_Payment', t_RefundId);

    /* Now fill in all the additional details */
    api.pkg_ColumnUpdate.SetValue(t_RefundObjectId, 'IsRefund', 'Y');
    api.pkg_ColumnUpdate.SetValue(t_RefundObjectId, 'ReasonForRefund',t_Reason);

    /* Update the Payment Method */
    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'PaymentMethod'),
                            t_RefundObjectId, t_PaymentMethodId);

    /* Relate the refund to the payment */
    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'Refund'),
                            t_Payments(1), t_RefundObjectId);

    /* Relate the refund to the payment */
    t_RelId := api.pkg_RelationshipUpdate.New(
                   api.pkg_ConfigQuery.EndPointIdForName(
                                       t_ProposedPaymentDefId, 'Refund'),
                   t_JobId, t_RefundObjectId);

    --complete the Provid Refund process
    t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
        api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId,
                                              'ProvideRefund'));
    api.pkg_processupdate.complete(t_TempIds(1),'Refund Provided');
  end;

  /*---------------------------------------------------------------------------
   * GetSelectablePaymentMethods() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetSelectablePaymentMethods (
    a_Object                    udt_ID,
    a_RelationshipDef           udt_ID,
    a_EndPoint                  udt_EndPoint,
    a_SearchString              varchar2,
    a_Objects               out udt_ObjectList
  ) is
    t_Active                    char(1);
    t_Count                     pls_integer;
    t_SelectablePaymentMethods  udt_IDList;
  begin
    a_Objects := api.udt_ObjectList();

    /* Search for all of the selectable payment methods */
    t_SelectablePaymentMethods := api.pkg_simplesearch.ObjectsByIndex(
                                  'o_PaymentMethod', 'UserSelectable', 'Y');

    /* Now put all the active ones into a_Objects */
    t_Count := t_SelectablePaymentMethods.count;
    for i in 1..t_Count loop
      select a.ActiveFlag
        into t_Active
        from query.o_PaymentMethod a
        where a.ObjectId = t_SelectablePaymentMethods(i);
      if t_Active = 'Y' then
        a_Objects.extend(1);
        a_Objects(a_Objects.count) := api.udt_Object(
                                          t_SelectablePaymentMethods(i));
      end if;
    end loop;
  end;


 /* ------------------------------------------------------------
  * PayExternalFees() -- PUBLIC
  * 
  * This procedure blindly pays all fees for the job that is
  * related to the eCom job.  Used for demo purposes, will need
  * to change.  This procedure is called from the Accept Payment
  * process on the eCom job.
  * ------------------------------------------------------------*/
  procedure PayExternalFees(a_ObjectId  udt_Id, 
                            a_AsOfDate  date) is

    t_FeeIDs                udt_IdList;
    t_AmountsToPay          api.pkg_definition.udt_NumberList;
    t_eComJobId             udt_Id;
    t_eComBPEndPointId      udt_Id;
    t_PaymentId             udt_Id;
    t_PaymentObjectId       udt_Id;
    t_FeeCount              number := 0;
    
  begin

  --get the EndPointId
    t_eComBPEndPointId := api.pkg_configquery.endpointidforname('j_eCom', 'BuildingPermit');

  --get the eComJobId
    select jobid
      into t_eComJobId
      from api.processes p
     where p.processid = a_ObjectId;

  --loop through the fees and add them with their amounts to the arrays
    for c in (select f.feeid, f.Amount
                from api.relationships r
                join api.fees f on f.jobid = r.toobjectid
               where r.fromobjectid = t_eComJobId
                 and r.endpointid = t_eComBPEndPointId)loop

      t_FeeCount := t_FeeCount + 1;
      t_FeeIds(t_FeeCount) := c.FeeId;
      t_AmountsToPay(t_FeeCount) := c.Amount;

    end loop; --c

  --make the payment for all of the fees
    t_PaymentId := api.pkg_paymentupdate.New(sysdate, t_FeeIds, t_AmountsToPay);

  --Register the Financial Objects that Posse created
    t_PaymentObjectId := api.pkg_objectupdate.RegisterExternalObject(
                             'o_Payment', t_PaymentId);
    for i in (select transactionId from api.paymenttransactions
               where paymentId = t_PaymentId) loop
      api.pkg_objectupdate.RegisterExternalObject('o_PaymentTransactions',
                                                  i.transactionid);
      api.pkg_objectupdate.RegisterExternalObject('o_FeeTransactions',
                                                  i.transactionid);
    end loop; --i

  end PayExternalFees;

end pkg_Fees_deprecated;

/

