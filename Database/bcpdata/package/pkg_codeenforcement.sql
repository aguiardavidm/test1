create or replace package pkg_CodeEnforcement is

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * CompleteComplaints() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CompleteComplaints(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * ResolveViolation() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  ResolveViolation(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );
  
  /*---------------------------------------------------------------------------
   * GenerateFines() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  GenerateFines(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * FindAddressOccupancy() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  FindAddressOccupancy(
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_AddressObjectId           udt_Id,
    a_PeriodicInspectionJobId   udt_Id
  );

  /*---------------------------------------------------------------------------
   * MaintainOccupancy() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  MaintainOccupancy(
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date
  );

end pkg_CodeEnforcement;

 
/

grant execute
on pkg_codeenforcement
to posseextensions;

create or replace package body pkg_CodeEnforcement is

  /*---------------------------------------------------------------------------
   * CompleteComplaints() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CompleteComplaints(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  )is

  t_CompleteInvDefId    udt_Id := api.pkg_configquery.objectdefidforname('p_CompleteInvestigation');
  t_JobId               udt_Id;

  begin

    select JobId
      into t_JobId
      from api.processes p
     where p.processid = a_ObjectId;

    for c in (select p.processid
                from api.jobs j
                join api.processes p on p.jobid = j.jobid
                 and p.processtypeid = t_CompleteInvDefId
               where j.parentjobid = t_JobId)loop
      api.pkg_processupdate.Complete(c.ProcessId, 'Complete');
    end loop;
  end CompleteComplaints;

  /*---------------------------------------------------------------------------
   * ResolveViolation() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  ResolveViolation(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  )is
  
  begin
/*What if they are changing the Resolved Date??? Do we want to reset these things?  AWP 2/25/2011*/
    if api.pkg_columnquery.datevalue(a_ObjectId, 'ResolvedDate') is not null
      then
        api.pkg_columnupdate.setvalue(a_ObjectId, 'DailyFine', 0);
    end if;
  end ResolveViolation;

  /*---------------------------------------------------------------------------
   * GenerateFines() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  GenerateFines(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  )is
    t_ViolationEndPointId udt_Id := api.pkg_configquery.endpointidforname('j_Violation', 'Violation');
  begin
    --loop through the Violation objects
    for c in (select r.toobjectid, 
                     api.pkg_columnquery.datevalue(r.toobjectid, 'ResolutionDate') ResolutionDate,
                     api.pkg_columnquery.value(r.toobjectid, 'FeesAssessed') FeesAssessed
                from api.relationships r
               where r.fromobjectid = a_ObjectId
                 and r.endpointid = t_ViolationEndpointId)loop
--      --if the violation has been resolved
      if c.FeesAssessed = 'N' and c.ResolutionDate is not null
        then
--dbms_output.put_line('Violation ObjectId: ' || c.ToObjectId);
          feescheduleplus.pkg_posseregisteredprocedures.GenerateFees(a_ObjectId, Sysdate, 'N', c.ToObjectId);
          api.pkg_columnupdate.SetValue(c.ToObjectId, 'FeesAssessed', 'Y');
      end if;
    end loop;
  end GenerateFines;

  /*---------------------------------------------------------------------------
   * IssueNOV() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  IssueNOV(
    a_ObjectId                    udt_Id,
    a_AsOfDate                    date
  )is
    t_InvestigationJobId          udt_Id;
    t_ViolationJobId              udt_Id;
    t_ViolationsEndPointId        udt_Id;
    t_AddressEndPointId           udt_Id;
    t_NewProcessId                udt_Id;
    t_NewRelationshipId           udt_Id;
    t_InvestgationJobId           udt_Id;
    t_NewProcessTypeId            udt_Id;
    t_ViolationJobTypeId          udt_Id         := api.pkg_ConfigQuery.ObjectDefIdForName('j_Violation');
    t_IssueNOVProcessTypeId       udt_Id         := api.pkg_ConfigQuery.ObjectDefIdForName('p_IssueNOV');
    t_PerfViolInspTypeId          udt_Id         := api.pkg_ConfigQuery.ObjectDefIdForName('p_PerformViolationInspection');
    t_IssueNOVViolationsEPID      udt_Id         := api.pkg_ConfigQuery.EndPointIdForName('p_IssueNOV', 'Violations');
    t_PerfViolInspViolationsEPId  udt_Id         := api.pkg_ConfigQuery.EndPointIdForName('p_PerformViolationInspection', 'Violations');
--    t_IssueNOVAddressEPID         udt_Id         := api.pkg_ConfigQuery.EndPointIdForName('p_IssueNOV', 'Address');
    t_PerfViolInspAddressEPId     udt_Id         := api.pkg_ConfigQuery.EndPointIdForName('p_PerformViolationInspection', 'Address');
    t_PerfInvViolationsEPId       udt_Id         := api.pkg_ConfigQuery.EndPointIdForName('p_PerformInvestigation', 'Violations');
--    t_InvAddressEPId              udt_Id         := api.pkg_ConfigQuery.EndPointIdForName('j_Investigation', 'Address');
    t_Outcome                     varchar2(4000) := api.pkg_ColumnQuery.Value(a_ObjectId, 'Outcome');
  begin

    select jobid
      into t_InvestigationJobId
      from api.processes p
     where p.processid = a_ObjectId;

    select j.jobid
      into t_ViolationJobId
      from api.jobs j
     where j.parentjobid = t_InvestigationJobId
       and j.jobtypeid = t_ViolationJobTypeId;

    if t_Outcome = 'Issue Violation'
      then
        t_NewProcessTypeId           := t_IssueNOVProcessTypeId;
        t_ViolationsEndPointId       := t_IssueNOVViolationsEPID;
--        t_AddressEndPointId          := t_IssueNOVAddressEPID;
      else
        t_NewProcessTypeId           := t_PerfViolInspTypeId;
        t_ViolationsEndPointId       := t_PerfViolInspViolationsEPId;
--        t_AddressEndPointId          := t_PerfViolInspAddressEPId;
    end if;
    
    t_NewProcessId := api.pkg_processupdate.New(t_ViolationJobId,
                                                t_NewProcessTypeId,
                                                null,
                                                null, --may want a scheduled start date...
                                                null,
                                                null);
--use indirect rels instead?
    --copy Violation relationships to process
/*    for v in(select r.ToObjectId ObjectId
               from api.Relationships r
              where r.FromObjectId = a_ObjectId
                and r.EndPointId = t_PerfInvViolationsEPId)loop
      t_NewRelationshipId := api.pkg_relationshipupdate.new(t_ViolationsEndPointId, t_NewProcessId, v.ObjectId);
    end loop;
--
    --copy Address relationship to process
    for a in(select r.ToObjectId ObjectId
               from api.Relationships r
              where r.FromObjectId = t_InvestgationJobId
                and r.EndPointId = t_InvAddressEPId)loop
      t_NewRelationshipId := api.pkg_relationshipupdate.new(t_AddressEndPointId, t_newProcessId, a.ObjectId);
    end loop;
*/
    --anything else to copy?
    --details?
    --?

  end IssueNOV;

  /*---------------------------------------------------------------------------
   * FindAddressOccupancy() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  FindAddressOccupancy(
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_AddressObjectId           udt_Id,
    a_PeriodicInspectionJobId   udt_Id
  )is
    t_AddressBPEPId             udt_Id := api.pkg_ConfigQuery.EndpointIdForName('o_Address', 'AddressBuildingPermit');
    t_BPOccupancyEPId           udt_Id := api.pkg_ConfigQuery.EndpointIdForName('j_BuildingPermit', 'Occupancy');
    t_InspectionOccEPId         udt_Id := api.pkg_ConfigQuery.EndpointIdForName('j_PeriodicInspection', 'Occupancy');
    t_BuildingPermit            udt_Id;
    t_NewRelId                  udt_Id;
    t_Occupancy                 api.udt_ObjectList;
  begin

    t_Occupancy := api.udt_ObjectList();

    --first, remove existing occupancies that are related to the periodic inspection
    for a in (select r.RelationshipId
                from api.relationships r
               where r.fromobjectid = a_PeriodicInspectionJobId
                 and r.endpointid = t_InspectionOccEPId) loop
      api.pkg_relationshipupdate.remove(a.Relationshipid);
    end loop;

    begin
      --get the most recent building permit
      select max(r.toobjectid)
        into t_BuildingPermit
        from api.relationships r
        join api.objects o on o.objectid = r.toobjectid
        join api.logicaltransactions lt on lt.logicaltransactionid = o.CreatedLogicalTransactionId
       where r.fromobjectid = a_AddressObjectId
         and r.endpointid = t_AddressBPEPId;

      --grab all of the permit's occupancies
      select api.udt_Object(r.toobjectid)
        bulk collect into t_Occupancy
        from api.relationships r
       where r.fromobjectid = t_BuildingPermit
         and r.endpointid = t_BPOccupancyEPId;

      --loop through the occupancies and relate them to the Periodic Inspection
      for b in 1.. t_Occupancy.Count loop
        t_NewRelId := api.pkg_RelationshipUpdate.New(t_InspectionOccEPId, a_PeriodicInspectionJobId, t_Occupancy(b).ObjectId);
      end loop;

    exception when no_data_found then
      null;
    end;
  end;

  /*---------------------------------------------------------------------------
   * MaintainOccupancy() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  MaintainOccupancy(
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date
  )is
    t_JobOccEPId                udt_Id := api.pkg_ConfigQuery.EndPointIdForName('j_PeriodicInspection', 'Occupancy');
    t_ProcessOccEPId            udt_Id := api.pkg_ConfigQuery.EndPointIdForName('p_PerformPeriodicInspection', 'Occupancy');
    t_PerformPeriodicInspId     udt_Id := api.pkg_configquery.ObjectDefIdForName('p_PerformPeriodicInspection');
    t_DefId                     udt_Id;
    t_NewRelId                  udt_Id;
    t_ProcessId                 udt_Id;
    t_JobId                     udt_Id;
    t_RelId                     udt_Id;

  begin
    --get the objectid
    select ObjectDefId
      into t_DefId
      from api.objects
     where objectid = a_ObjectId;

    if t_DefId = api.pkg_configquery.ObjectDefIdForName('j_PeriodicInspection')
      then
        --add occupancies that are on job and missing on process
        begin
          --get the most recent incomplete process
          select processid
            into t_ProcessId
            from api.processes
           where jobid = a_ObjectId
             and outcome is null
             and processtypeid = t_PerformPeriodicInspId;

          --loop through new Occupancies, compared to most recent periodic Inspection process that hasn't been completed
          for a in (select r.toobjectid
                      from api.relationships r
                     where r.fromobjectid = a_ObjectId
                       and r.endpointid = t_JobOccEPId
                    minus
                    select r.ToObjectId
                      from api.processes p
                      join api.relationships r on r.fromobjectid = p.processid
                       and r.endpointid = t_ProcessOccEPId
                     where p.jobid = a_ObjectId
                       and p.outcome is null
                       and p.processid = t_ProcessId) loop
            --relate the occupancies to the process
            t_NewRelId := api.pkg_RelationshipUpdate.New(t_ProcessOccEPId, t_ProcessId, a.Toobjectid);
          end loop; --a loop

        exception when too_many_rows then
          api.pkg_errors.raiseerror(-20000, 'There is more than one open Perfom Periodic Inspection process on this job, please delete one before continuing.');
        when no_data_found then
          null;
        end;

        --remove occupancies from process that aren't on job
        for c in (select relationshipid
                    from api.relationships r
                   where r.fromobjectid = t_ProcessId
                     and r.EndPointId = t_ProcessOccEPId
                     and r.toobjectid = (select r.toobjectid
                                           from api.relationships r
                                          where r.fromobjectid = t_ProcessId
                                            and r.endpointid = t_ProcessOccEPId
                                         minus
                                         select r.toobjectid
                                           from api.relationships r
                                          where r.fromobjectid = a_ObjectId
                                            and r.endpointid = t_JobOccEPId)) loop

          api.pkg_RelationshipUpdate.Remove(c.RelationshipId);

        end loop; --c loop

    elsif t_DefId = t_PerformPeriodicInspId
      then
          --get the job
          select JobId
            into t_JobId
            from api.processes
           where ProcessId = a_ObjectId;

          --loop through new Occupancies, subtracting the ones from the job
          for d in (select r.toobjectid
                      from api.relationships r
                     where r.fromobjectid = a_ObjectId
                       and r.endpointid = t_ProcessOccEPId
                    minus
                    select r.ToObjectId
                      from api.processes p
                      join api.relationships r on r.fromobjectid = p.JobId
                       and r.endpointid = t_JobOccEPId
                     where p.ProcessId = a_ObjectId) loop
            --relate the occupancies to the Job
            t_NewRelId := api.pkg_RelationshipUpdate.New(t_JobOccEPId, t_JobId, d.ToObjectId);
          end loop; --d loop


        --remove occupancies from job that aren't on process
        for e in (select relationshipid
                    from api.relationships r
                   where r.fromobjectid = t_JobId
                     and r.EndPointId = t_JobOccEPId
                     and r.toobjectid = (select r.toobjectid
                                           from api.relationships r
                                          where r.fromobjectid = t_JobId
                                            and r.endpointid = t_JobOccEPId
                                         minus
                                         select r.toobjectid
                                           from api.relationships r
                                          where r.fromobjectid = a_ObjectId
                                            and r.endpointid = t_ProcessOccEPId)) loop

          api.pkg_RelationshipUpdate.Remove(e.RelationshipId);

        end loop; --e loop

    end if; --if def is...
  end MaintainOccupancy;

end pkg_CodeEnforcement;

/

