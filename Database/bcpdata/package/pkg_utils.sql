create or replace package pkg_utils as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.Pkg_Definition.udt_IdList;
  subtype udt_ObjectList is api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * AddSection()
   *   Add a section to a string, separating the section from the remainder
   * of the string, if necessary.
   *-------------------------------------------------------------------------*/
  procedure AddSection (
    a_String        in out varchar2,
    a_Section       varchar2,
    a_Separator       varchar2
  );

  /*---------------------------------------------------------------------------
   * Split ()
   *  Return a list of the words in a_String that are separated by a_Separator.
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                    varchar2,
    a_Separator                 varchar2
  ) return udt_StringList;

  /*---------------------------------------------------------------------------
   * IsNumber()
   *   Returns a logical indicating if a string is a number.
   *-------------------------------------------------------------------------*/
  function IsNumber (
    a_String        varchar2
  ) return boolean;

  /*--------------------------------------------------------------------------
   * NumericPrefix()
   *   Return the numeric prefix of a string.  For example, '23abc' would
   * return 23.
   *------------------------------------------------------------------------*/
  function NumericPrefix (
    a_String                 varchar2
  ) return number;

  /*--------------------------------------------------------------------------
   * StringSuffix()
   *   Return the section of a string that comes after the numeric prefix.
   * For example, '23abc123' would return 'abc123'.
   *------------------------------------------------------------------------*/
  function StringSuffix (
    a_String                 varchar2
  ) return varchar2;

  /*--------------------------------------------------------------------------
   * SplitLines()
   *  Split the text into an array, with one entry for each line in the string.
   * This is usefull for taking a very long block of text and using the
   * table(cast(... as stalbert.udt_StingList)) in a SQL statement to get a row
   * for each line of text.
   *------------------------------------------------------------------------*/
  function SplitLines (
    a_String         varchar2
  ) return udst_StringList;

  /*--------------------------------------------------------------------------
   * SplitNote()
   *  SplitNote does the same thing as SplitLines, except it takes a note id as
   * an argument and splits the note text.
   *------------------------------------------------------------------------*/
  function SplitNote (
    a_NoteId         udt_Id
  ) return udst_StringList;

  /*--------------------------------------------------------------------------
   * RelationshipNew() -- PUBLIC
   *------------------------------------------------------------------------*/
  function RelationshipNew (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id,
    a_RelationshipName                 varchar2 default null,
    a_EffectiveDate                    date default null
  ) return udt_Id;

 /*--------------------------------------------------------------------------
   * RelationshipRemove() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelationshipRemove (
    a_ObjectId                         udt_Id,
    a_RelationshipName                 varchar2,
    a_EffectiveDate                    date default null
  );

  /*--------------------------------------------------------------------------
   * RelationshipRemove() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelationshipRemove (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id,
    a_RelationshipName                 varchar2 default null,
    a_EffectiveDate                    date default null
  );

  /*--------------------------------------------------------------------------
   * RelationshipCopy() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelationshipCopy (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id,
    a_FromRelationshipName             varchar2,
    a_ToRelationshipName               varchar2,
    a_EffectiveDate                    date default null
  );

  /*--------------------------------------------------------------------------
   * RelationshipReplace() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelationshipReplace (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id,
    a_FromRelationshipName             varchar2,
    a_ToRelationshipName               varchar2,
    a_EffectiveDate                    date default null
  );

  /*--------------------------------------------------------------------------
   * IsRelated() -- PUBLIC
   *------------------------------------------------------------------------*/
  function IsRelated (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id
  ) return boolean;
  /*--------------------------------------------------------------------------
   * IsRelatedToDef() -- PUBLIC
   *------------------------------------------------------------------------*/
  function IsRelatedToDef (
    a_FromObjectId                     udt_Id,
    a_RelationshipName                 varchar2
  ) return boolean;

  /*--------------------------------------------------------------------------
   * GetRelationships() -- PUBLIC
   *------------------------------------------------------------------------*/
  function GetRelationships (
    a_ObjectId                         udt_Id,
    a_RelationshipName                 varchar2
  ) return udt_IdList;

  /*--------------------------------------------------------------------------
   * GetRelationshipObjects() -- PUBLIC
   *------------------------------------------------------------------------*/
  function GetRelationshipObjects (
    a_ObjectId                         udt_Id,
    a_RelationshipName                 varchar2
  ) return udt_ObjectList;

  function ReferenceIfNotReferenced (
    a_BlockId                udt_Id,
    a_ObjectId               udt_Id
  ) return udt_Id;

  /*---------------------------------------------------------------------------
   * PostTransactionForAMonth() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PostPaidTransactions(
    a_Date              date,
    a_Format            varchar2 default 'MM'
  );
  
  /*---------------------------------------------------------------------------
   * SetJobExternalFileNum() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SetJobExternalFileNum (
    a_JobId                 udt_Id,
    a_AsOfDate              date,
    a_ExternalFileNum       varchar2
  );


 /*---------------------------------------------------------------------------
  * GetParameter()
  *  Returns the ObjectId of a System Parameter object given its
  * System Reference
  *-------------------------------------------------------------------------*/
/*  function GetParameterObjectId(
    a_SystemReference                   varchar2
  ) return udt_Id;


 /*---------------------------------------------------------------------------
  * GetTextValue()
  *  Returns the Text value from a System Parameter object given its
  * System Reference
  *-------------------------------------------------------------------------*/
/*  function GetTextValue (
    a_SystemReference                   varchar2
  ) return varchar2;


 /*---------------------------------------------------------------------------
  * ParseTextValue()
  *  From the System Parameter with the specified System Reference 
  *  treat each values contained in braces ('{' and '}') as a
  *  column name on the specified object and return its value.
  *-------------------------------------------------------------------------*/
  function ParseTextValue (
    a_ObjectId                          udt_id,
    a_TextToParse                       varchar2
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * AddUserToAccessGroup()
   *-------------------------------------------------------------------------*/
  procedure AddUserToAccessGroup (
    a_RelationshipId        udt_Id,
    a_AsOfDate              date,
    a_AccessGroupId         udt_Id,
    a_UserId                udt_Id
  );

  /*---------------------------------------------------------------------------
   * RemoveUserFromAccessGroup()
   *-------------------------------------------------------------------------*/
  procedure RemoveUserFromAccessGroup (
    a_RelationshipId        udt_Id,
    a_AsOfDate              date,
    a_AccessGroupId         udt_Id,
    a_UserId                udt_Id);
    
  ------------------------------------------------------------------------------
  -- WordTemplateToSystemSettings
  --  Relate the Word Interface Template to the System Settings object if it is
  -- not already.
  ------------------------------------------------------------------------------
  procedure WordTemplateToSystemSettings(
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

end pkg_utils;



 
/

grant execute
on pkg_utils
to conversion;

grant execute
on pkg_utils
to mhwyinterface;

create or replace package body pkg_utils as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;

  /*---------------------------------------------------------------------------
   * AddSection()
   *-------------------------------------------------------------------------*/
  procedure AddSection (
    a_String        in out varchar2,
    a_Section       varchar2,
    a_Separator       varchar2
  ) is
  begin
    if a_Section is not null then
      if a_String is null then
        a_String := a_Section;
      else
        a_String := a_String || a_Separator || a_Section;
      end if;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * Split ()
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                    varchar2,
    a_Separator                 varchar2
  ) return udt_StringList is
    t_List                      udt_StringList;
    t_Pos                       number;
    t_Index                     number;
  begin
    t_Pos := 0;
    while t_Pos <= length(a_String) loop
      t_Index := instr(a_String, a_Separator, t_Pos+1);
      if t_Index > 0 then
        t_List(t_List.count+1) := substr(a_String, t_Pos+1, t_Index-t_Pos-1);
        t_Pos := t_Index;
      else
        t_List(t_List.count+1) := substr(a_String, t_Pos+1);
        t_Pos := length(a_String) + 1;
      end if;
    end loop;

    return t_List;
  end;

  /*---------------------------------------------------------------------------
   * IsNumber() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function IsNumber (
    a_String        varchar2
  ) return boolean is
    t_Temp        number;
  begin
    t_Temp := a_String;
    return true;
  exception
  when value_error then
    return false;
  end;

  /*--------------------------------------------------------------------------
   * NumericPrefix()
   *------------------------------------------------------------------------*/
  function NumericPrefix (
    a_String                 varchar2
  ) return number is
    t_Number                 number;
  begin
    if a_String is null then
      return null;
    end if;

    for i in 1..length(a_String) loop
      if substr(a_String, i, 1) between '0' and '9' then
        t_Number := to_number(substr(a_String, 1, i));
      else
        exit;
      end if;
    end loop;

    return t_Number;
  end;

  /*--------------------------------------------------------------------------
   * StringSuffix()
   *------------------------------------------------------------------------*/
  function StringSuffix (
    a_String                 varchar2
  ) return varchar2 is
    t_Suffix                 varchar2(1000);
    t_Index                  pls_integer;
  begin
    if a_String is null then
      return null;
    end if;

    for i in 1..length(a_String) loop
      if substr(a_String, i, 1) not between '0' and '9' then
        t_Index := i;
        exit;
      end if;
    end loop;

    if t_Index is not null then
      t_Suffix := substr(a_String, t_Index);
    end if;

    return t_Suffix;
  end;

  /*--------------------------------------------------------------------------
   * SplitLines() -- PUBLIC
   *------------------------------------------------------------------------*/
  function SplitLines (
    a_String         varchar2
  ) return udst_StringList is
    t_Text varchar2(32760);
    t_TextList udst_StringList;
    t_IndexCR number;
    t_IndexLF number;
    t_Index number;
    t_NextIndex number;
    t_Position number;
    t_Continue boolean;
  begin
    t_TextList := udst_StringList();
    t_Position := 1;
    t_Text := a_String;

    if t_Text is not null then
      t_Continue := true;
      while t_Continue loop
        t_IndexCR := instr(t_Text, chr(13), t_Position);
        t_IndexLF := instr(t_Text, chr(10), t_Position);

        if t_IndexLF > 0 and t_IndexCR = 0 then
          t_Index := t_IndexLF;
          t_NextIndex := t_Index + 1;
        elsif t_IndexCR > 0 and t_IndexLF = 0 then
          t_Index := t_IndexLF;
          t_NextIndex := t_Index + 1;
        elsif t_IndexCR > 0 and t_IndexLF > 0 then
          if t_IndexLF < t_IndexCR then
            t_Index := t_IndexLF;
            if t_IndexCR = t_IndexLF + 1 then
              t_NextIndex := t_Index + 2;
            else
              t_NextIndex := t_Index + 1;
            end if;
          else
            t_Index := t_IndexCR;
            if t_IndexLF = t_IndexCR + 1 then
              t_NextIndex := t_Index + 2;
            else
              t_NextIndex := t_Index + 1;
            end if;
          end if;
        else
          t_Continue := false;
          t_Index := length(t_Text)+1;
        end if;

        t_TextList.extend(1);
        t_TextList(t_TextList.count) :=
            substr(t_Text, t_Position, t_Index-t_Position);
        t_Position := t_NextIndex;
      end loop;
    end if;

    return t_TextList;
  end;

  /*--------------------------------------------------------------------------
   * SplitNote() -- PUBLIC
   *------------------------------------------------------------------------*/
  function SplitNote (
    a_NoteId         udt_Id
  ) return udst_StringList is
    t_Text           varchar2(32760);
  begin
    select Text
      into t_Text
      from api.Notes
     where NoteId = a_NoteId;

    return SplitLines(t_Text);
  end;

  function GetObjectDefId (
    a_ObjectId                         udt_Id
  ) return udt_Id is
    t_ObjectDefId                      udt_Id;
  begin
    select ObjectDefId
      into t_ObjectDefId
      from api.Objects
     where ObjectId = a_ObjectId;
    return t_ObjectDefId;
  exception
  when no_data_found then
    raise_application_error( -20000, 'Could not find ObjectId (' || a_ObjectId || ')');
  end GetObjectDefId;

  function GetRelationshipNameId (
    a_RelationshipName                 varchar2
  ) return udt_Id is
    t_RelNameId                        udt_Id;
  begin
    if a_RelationshipName is null then
      return null;
    end if;
    --
    select RelationshipNameId
      into t_RelNameId
      from api.RelationshipNames
     where RelationshipName = a_RelationshipName;
    return t_RelNameId;
  exception
  when no_data_found then
    raise_application_error( -20000, 'Relationship Name (' || a_RelationshipName || ') not found in Relationship Names system table.');
  end GetRelationshipNameId;

  function GetObjectDefName (
    a_ObjectDefId                      udt_Id
  ) return varchar2 is
    t_ObjectDefName                    varchar2(200);
  begin
    select Description
      into t_ObjectDefName
      from api.ObjectDefs
     where ObjectDefId = a_ObjectDefId;
    --
    return t_ObjectDefName;
  exception
  when no_data_found then
    return null;
  end GetObjectDefName;

  function GetRelationshipEndpointId (
    a_FromObjectDefId                  udt_Id,
    a_ToObjectDefId                    udt_Id,
    a_RelationshipName                 varchar2 default null
  ) return udt_Id is
    t_RelNameId                        udt_Id;
    t_ToEndpointId                     udt_Id;
    t_FromObjectDefName                varchar2(200);
    t_ToObjectDefName                  varchar2(200);
  begin
    --
    -- Lookup RelDefEnd from ObjectIds
    t_RelNameId := GetRelationshipNameId( a_RelationshipName);
    begin
      --
      -- Use ToRelationshipNameId for the "Going Out" side of the relationship
      select ToEndPointId
        into t_ToEndpointId
        from api.RelationshipDefs rd
       where FromObjectDefId = a_FromObjectDefId
         and ToObjectDefId = a_ToObjectDefId
         and (ToRelationshipNameId = t_RelNameId or
              a_RelationshipName is null)
         and Stored = 'Y';
    exception
    when no_data_found then
      raise_application_error( -20000, 'No stored RelationshipDef found for Defs (' || a_FromObjectDefId || ' - ' || GetObjectDefName( a_FromObjectDefId) || '), (' || a_ToObjectDefId || ' - ' || GetObjectDefName( a_ToObjectDefId) || '), (' || a_RelationshipName || ')');
    when too_many_rows then
      raise_application_error( -20000, 'More than one stored RelationshipDef found for Defs (' || a_FromObjectDefId || ' - ' || GetObjectDefName( a_FromObjectDefId) || '), (' || a_ToObjectDefId || ' - ' || GetObjectDefName( a_ToObjectDefId) || '), (' || a_RelationshipName || ')');
    end;
    --
    return t_ToEndpointId;
  end GetRelationshipEndpointId;

  /*--------------------------------------------------------------------------
   * RelationshipNew() -- PUBLIC
   *------------------------------------------------------------------------*/
  function RelationshipNew (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id,
    a_RelationshipName                 varchar2 default null,
    a_EffectiveDate                    date default null
  ) return udt_Id is
    t_FromObjectDefId                  udt_Id;
    t_ToObjectDefId                    udt_Id;
    t_ToEndpointId                     udt_Id;
    t_RelId                            udt_Id;
  begin
    -- Lookup RelDefEnd from ObjectIds
    t_FromObjectDefId := GetObjectDefId( a_FromObjectId);
    t_ToObjectDefId := GetObjectDefId( a_ToObjectId);
    t_ToEndpointId := GetRelationshipEndpointId( t_FromObjectDefId, t_ToObjectDefId, a_RelationshipName);

    t_RelId := api.pkg_RelationshipUpdate.New( t_ToEndpointId,
                                               a_FromObjectId,
                                               a_ToObjectId,
                                               a_EffectiveDate);

    return t_RelId;
  end RelationshipNew;

 /*--------------------------------------------------------------------------
   * RelationshipRemove() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelationshipRemove (
    a_ObjectId                         udt_Id,
    a_RelationshipName                 varchar2,
    a_EffectiveDate                    date default null
  ) is
    t_RelNameId                        udt_Id;
    t_RemovedRelSum                    number;
  begin
    t_RelNameId := GetRelationshipNameId( a_RelationshipName);
    for c in (select re.RelationshipId
                from api.RelationshipDefs rd,
                     api.Relationships re
               where re.FromObjectId = a_ObjectId
                 and re.relationshipdefid = rd.relationshipdefid
                 and ToRelationshipNameId = t_RelNameId
                 and Stored = 'Y') loop
      api.pkg_RelationshipUpdate.Remove(c.RelationshipId, a_EffectiveDate);
      t_RemovedRelSum := t_RemovedRelSum + 1;
    end loop;

    if t_RemovedRelSum = 0 then
      raise_application_error(-20000, 'No Relationships were found to remove.');
    end if;
  end RelationshipRemove;

  /*--------------------------------------------------------------------------
   * RelationshipRemove() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelationshipRemove (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id,
    a_RelationshipName                 varchar2 default null,
    a_EffectiveDate                    date default null
  ) is
    t_RelNameId                        udt_Id;
    t_RemovedRelSum                    number;
  begin
    t_RelNameId := GetRelationshipNameId( a_RelationshipName);
    -- Lookup RelDefEnd from ObjectIds

    for c in (select distinct re.RelationshipId
                from api.RelationshipDefs rd,
                     api.Relationships re
               where re.FromObjectId = a_FromObjectId
                 and re.ToObjectId = a_ToObjectId
                 and re.relationshipdefid = rd.relationshipdefid
                 and (ToRelationshipNameId = t_RelNameId
                     or t_RelNameId is null)
                 and Stored = 'Y') loop
    api.pkg_RelationshipUpdate.Remove( c.RelationshipId, a_EffectiveDate);
    t_RemovedRelSum := t_RemovedRelSum + 1;
  end loop;

  if t_RemovedRelSum = 0 then
    raise_application_error(-20000, 'No Relationships were found to remove.');
  end if;

  end RelationshipRemove;

  /*--------------------------------------------------------------------------
   * RelationshipCopy() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelationshipCopy (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id,
    a_FromRelationshipName             varchar2,
    a_ToRelationshipName               varchar2,
    a_EffectiveDate                    date default null
  ) is
    t_FromObjectDefId                  udt_Id;
    t_ToObjectDefId                    udt_Id;
    t_ToEndpointId                     udt_Id;
    t_RelId                            udt_Id;
    t_FromRelatedObjects               udt_IdList;
  begin
    -- Lookup RelDefEnd from ObjectIds
    t_ToObjectDefId := GetObjectDefId( a_ToObjectId);

    t_FromRelatedObjects := GetRelationships(a_FromObjectId, a_FromRelationshipName);
    for i in 1..t_FromRelatedObjects.count loop
      t_FromObjectDefId := GetObjectDefId(t_FromRelatedObjects(i));
      t_ToEndpointId := GetRelationshipEndpointId( t_ToObjectDefId, t_FromObjectDefId, a_ToRelationshipName);
      t_RelId := api.pkg_RelationshipUpdate.New( t_ToEndpointId,
                                                 a_ToObjectId,
                                                 t_FromRelatedObjects(i),
                                                 a_EffectiveDate);
    end loop;

  end RelationshipCopy;

  /*--------------------------------------------------------------------------
   * RelationshipReplace() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelationshipReplace (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id,
    a_FromRelationshipName             varchar2,
    a_ToRelationshipName               varchar2,
    a_EffectiveDate                    date default null
  ) is
  begin
    RelationshipRemove(a_ToObjectId, a_ToRelationshipName);
    RelationshipCopy(a_FromObjectId, a_ToObjectId, a_FromRelationshipName, a_ToRelationshipName, a_EffectiveDate);
  end RelationshipReplace;

  /*--------------------------------------------------------------------------
   * IsRelated() -- PUBLIC NOT WORKING
   *------------------------------------------------------------------------*/
  function IsRelated (
    a_FromObjectId                     udt_Id,
    a_ToObjectId                       udt_Id
  ) return boolean is
  t_Related                            number := 0;
  begin
    select 1
      into t_Related
      from dual
     where exists (select 1
                     from api.Relationships
                    where FromObjectId = a_FromObjectId
                      and ToObjectId = a_ToObjectId);
    if t_Related = 1 then
      return true;
    else
      return false;
    end if;
  exception when too_many_rows then
    return true;
            when no_data_found then
    return false;
  end IsRelated;

  /*--------------------------------------------------------------------------
   * IsRelatedToDef() -- PUBLIC NOT WORKING
   *------------------------------------------------------------------------*/
  function IsRelatedToDef (
    a_FromObjectId                     udt_Id,
    a_RelationshipName                 varchar2
  ) return boolean is
  t_Related                            number := 0;
  begin
    select 1
      into t_Related
      from dual
     where exists (select 1
                     from api.Relationships re,
                          api.RelationshipDefs rd,
                          api.RelationshipNames rn
                    where FromObjectId = a_FromObjectId
                      and re.RelationshipDefId = rd.RelationshipDefId
                      and rn.RelationshipNameId = rd.ToRelationshipNameId
                      and rn.RelationshipName = a_RelationshipName
                    );
    if t_Related = 1 then
      return true;
    else
      return false;
    end if;
  exception when too_many_rows then
    return true;
            when no_data_found then
    return false;
  end IsRelatedToDef;

  /*--------------------------------------------------------------------------
   * GetRelationships() -- PUBLIC
   *------------------------------------------------------------------------*/
  function GetRelationships (
    a_ObjectId                         udt_Id,
    a_RelationshipName                 varchar2
  ) return udt_IdList is
    t_RelIds                           udt_IdList;
  begin
    select re.ToObjectId
      bulk collect into t_RelIds
      from api.Relationships re,
           api.RelationshipDefs rd,
           api.RelationshipNames rn,
           api.Objects ob
      where ob.ObjectId = a_ObjectId
        and rn.RelationshipName = a_RelationshipName
        and rd.FromObjectDefId = ob.ObjectDefId
        and rd.ToRelationshipNameId = rn.RelationshipNameId
        and re.FromObjectId = ob.ObjectId
        and re.EndPointId = rd.ToEndPointId
      order by re.ToObjectId;

    return t_RelIds;
  end;

  /*--------------------------------------------------------------------------
   * GetRelationshipObjects() -- PUBLIC
   *------------------------------------------------------------------------*/
  function GetRelationshipObjects (
    a_ObjectId                         udt_Id,
    a_RelationshipName                 varchar2
  ) return udt_ObjectList is
    t_RelIds                           udt_IdList;
    t_RelIdsObjects                    udt_ObjectList;
  begin
    t_RelIds := GetRelationships(a_ObjectId, a_RelationshipName);
    toolbox.pkg_Util.ObjectListFromIdList(t_RelIds, t_RelIdsObjects);
    return t_RelIdsObjects;
  end;

  function ReferenceIfNotReferenced (
    a_BlockId                udt_Id,
    a_ObjectId               udt_Id
  ) return udt_Id is
    t_BlockObjectId          udt_Id;
    t_Count                  number;
  begin
    select BlockObjectId
      into t_BlockObjectId
      from api.BlockObjects
     where BlockId = a_BlockId
       and ObjectId = a_ObjectId;

    return t_BlockObjectId;

  exception when no_data_found then
    select count(*)
      into t_Count
      from api.objects ob,
           api.objectdefs od,
           api.objectdeftypes ot
     where ob.ObjectId = a_ObjectId
       and ob.objectdefid = od.objectdefid
       and od.objectdeftypeid = ot.objectdeftypeid
       and ot.name = 'StoredRelationshipDefs';

    if t_Count > 0 then
      t_BlockObjectId := api.Pkg_BlockRelationshipUpdate.Reference(a_BlockId, a_ObjectId);
    else
      t_BlockObjectId := api.pkg_BlockObjectUpdate.ReferenceObject(a_BlockId, a_ObjectId);
    end if;

    return t_BlockObjectId;
  end;

  function ObjectDefIdForName (
    a_ObjectDefName          api.ObjectDefs.Name%type
  ) return udt_Id is
    t_ObjectDefId            udt_Id := api.pkg_ObjectDefQuery.IdForName(a_ObjectDefName);
  begin
    if t_ObjectDefId is null then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Object Def Name', a_ObjectDefName);
--      api.pkg_Errors.Raise();
    end if;

    return t_ObjectDefId;
  end;

  /*---------------------------------------------------------------------------
   * PostTransactionForAMonth() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PostPaidTransactions(
    a_Date              date,
    a_Format            varchar2 default 'MM'
  ) is
  begin
    for c in (select TransactionId
                from api.FeeTransactions
               where PostedDate is null
                 and trunc(TransactionDate, a_Format) = trunc(a_Date, a_Format)
                 and TransactionType = 'Pay') loop
      ToolboxNew.pkg_Finance.PostTransaction(c.TransactionId, a_Date);
    end loop;
  end;
  
  /*---------------------------------------------------------------------------
   * SetJobExternalFileNum() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SetJobExternalFileNum (
    a_JobId                 udt_Id,
    a_AsOfDate              date,
    a_ExternalFileNum       varchar2
  ) is
  begin
    api.pkg_JobUpdate.SetExternalFileNum(a_JobId, a_ExternalFileNum);
  end;


 /*---------------------------------------------------------------------------
  * GetParameter()
  *-------------------------------------------------------------------------*/
/*  function GetParameterObjectId (
    a_SystemReference                   varchar2
  ) return udt_Id is
    t_SysParmIndex                      varchar2(200) := trim(a_SystemReference);
    t_SysParmObjectId                   udt_Id;
    
  begin
    -- Check values passed in
    if t_SysParmIndex is null then
      api.pkg_errors.RaiseError(-20000, 'System Parameter Error: SystemReference argument must be supplied.');
    end if;
    
    -- Retrieve objectid of the System Parameter given the arguments
    begin
      select sp.ObjectId
        into t_SysParmObjectId
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_SystemParameter',
                                                                    'SystemReference',
                                                                    t_SysParmIndex) as api.udt_objectlist)) spt
          join query.o_SystemParameter sp
            on sp.objectid = spt.objectid
        where (sp.SysRef_EffectiveStartDate is null or t_Date >= trunc(sp.SysRef_EffectiveStartDate))
          and (sp.SysRef_EffectiveEndDate is null or t_Date < trunc(sp.SysRef_EffectiveEndDate + 1))
          and sp.ObjectDefTypeId = 1;
    exception
      when no_data_found then
        api.pkg_errors.RaiseError(-20000, 'System Parameter Error: Object for '||t_SysParmIndex|| ' not found.');
      when too_many_rows then
        api.pkg_errors.RaiseError(-20000, 'System Parameter Error: Too many objects found for '||t_SysParmIndex||'.');
    end;
   
    return t_SysParmObjectId;
  end GetParameterObjectId;
*/


 /*---------------------------------------------------------------------------
  * GetTextValue()
  *-------------------------------------------------------------------------*/
/*  function GetTextValue (
    a_SystemReference                   varchar2
  ) return varchar2 is
    t_SysParmObjectId                   udt_Id := GetParameterObjectId(a_SystemReference);
    
  begin
    if t_SysParmObjectId is null then
      api.pkg_errors.RaiseError(-20000, 'Text System Parameter Error: System Parameter object not found.');
    end if;
    return api.pkg_ColumnQuery.Value(t_SysParmObjectId, 'TextValue');
  end GetTextValue;
*/

 /*---------------------------------------------------------------------------
  * ColumnExistsOnObject()
  *-------------------------------------------------------------------------*/  
  function ColumnExistsOnObject(
    a_ObjectId                          udt_id,
    a_ColumnName                        varchar2
  ) return boolean is
    t_Count                             pls_integer;
    
  begin
    select count(*)
      into t_Count
      from api.objects o
      where o.ObjectId = a_ObjectId
        and exists (
          select 1
            from api.ColumnDefs cd
            where cd.ObjectDefId = o.ObjectDefId
              and lower(cd.Name) = lower(a_ColumnName));
    
    return t_Count <> 0;
  end ColumnExistsOnObject;



 /*---------------------------------------------------------------------------
  * ParseTextValue()
  *-------------------------------------------------------------------------*/  
  function ParseTextValue (
    a_ObjectId                          udt_id,
    a_TextToParse                       varchar2
  ) return varchar2 is
    t_ColumnName                        varchar2(4000);
    t_First                             varchar2(4000);
    t_Index                             pls_integer;
    t_Rest                              varchar2(4000); --:= GetTextValue(a_SystemReference);
    
  begin
    t_Rest := a_TextToParse;
    loop
      -- find beginning of column name
      t_Index := instr(t_Rest, '{');
      exit when t_Index = 0 or t_Index is null;
      t_First := t_First || substr(t_Rest, 0, t_Index - 1);
      t_Rest := substr(t_Rest, t_Index);

      -- find end of column name
      t_Index := instr(t_Rest, '}');
      exit when t_Index = 0 or t_Index is null;
      t_ColumnName := substr(t_Rest, 2, t_Index - 2);
      if ColumnExistsOnObject(a_ObjectId, t_ColumnName) then
        t_First := t_First || api.pkg_ColumnQuery.Value(a_ObjectId, t_ColumnName);
      else
        t_First := t_First || '{' || t_ColumnName || '}';
      end if;
      t_Rest := substr(t_Rest, t_Index + 1);
    end loop;

    return t_First || t_Rest;
  end ParseTextValue;


  /*---------------------------------------------------------------------------
   * AddUserToAccessGroup()
   *-------------------------------------------------------------------------*/
  procedure AddUserToAccessGroup (
    a_RelationshipId        udt_Id,
    a_AsOfDate              date,
    a_AccessGroupId         udt_Id,
    a_UserId                udt_Id) is

    t_AccessGroupObjectId   udt_Id;
    t_UserObjectId          udt_Id;
    t_Temp                  number;

  begin
    select 1
      into t_Temp
      from api.accessgroupusers ag
     where ag.UserId = a_UserId
       and ag.AccessGroupId = a_AccessGroupId;
    exception when no_data_found then
      api.pkg_userupdate.AddToAccessGroup(a_UserId, a_AccessGroupId);
  end AddUserToAccessGroup;


  /*---------------------------------------------------------------------------
   * RemoveUserFromAccessGroup()
   *-------------------------------------------------------------------------*/
  procedure RemoveUserFromAccessGroup (
    a_RelationshipId        udt_Id,
    a_AsOfDate              date,
    a_AccessGroupId         udt_Id,
    a_UserId                udt_Id) is

    t_AccessGroupObjectId   udt_Id;
    t_UserObjectId          udt_Id;
    t_Temp                  number;

  begin
    select 1
      into t_Temp
      from api.accessgroupusers ag
     where ag.UserId = a_UserId
       and ag.AccessGroupId = a_AccessGroupId;

    api.pkg_userupdate.RemoveFromAccessGroup(a_UserId, a_AccessGroupId);

    exception when no_data_found then
      null;
  end RemoveUserFromAccessGroup;

  ------------------------------------------------------------------------------
  -- WordTemplateToSystemSettings
  --  Relate the Word Interface Template to the System Settings object if it is
  -- not already.
  ------------------------------------------------------------------------------
  procedure WordTemplateToSystemSettings(
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_Count                             number;
    t_SystemSettingsObjectId            udt_Id;
    t_WordTemplateDocumentId            udt_Id;

  begin
    select o.ObjectId
      into t_SystemSettingsObjectId
      from query.o_SystemSettings o
      where rownum <= 1;

   select rptwt.WordTemplateDocumentId
     into t_WordTemplateDocumentId
     from query.r_JTPT_WordInterfaceTemplate rptwt
     where rptwt.RelationshipId = a_RelationshipId;

    select count(*)
      into t_Count
      from query.r_SystemSettingsLetterTemplate rsslt
      where rsslt.SystemSettingsObjectId = t_SystemSettingsObjectId
        and rsslt.WordTemplateDocumentId = t_WordTemplateDocumentId;

    if t_Count = 0 then
      extension.pkg_RelationshipUpdate.New(t_SystemSettingsObjectId, t_WordTemplateDocumentId, 'LetterTemplate');
    end if;
  end WordTemplateToSystemSettings;

end pkg_utils;



/

