create or replace package pkg_BCPAddressing_DEPRECATED is

/*
  Albert - renaming to deprecated since I can't find references to
  this package and it doesn't compile anyways.
  This package has not been added to CVS.
*/

  /*---------------------------------------------------------------------
  * NewAddress() -- PUBLIC
  *-------------------------------------------------------------------*/
  procedure NewAddressObject(a_PosseObjectId       in out number,
                             a_TaxParcelNumber     varchar2,
                             a_AddressHouseNumber  number,
                             a_AddressSuite        varchar2,
                             a_LotNumber           varchar2,
                             a_BlockNumber         number,
                             a_LandArea            number,
                             a_ZipCode             varchar2,
                             a_ZipExtension        varchar2);

  /*---------------------------------------------------------------------
  * NewStreetObject() -- PUBLIC
  *-------------------------------------------------------------------*/
  procedure NewStreetObject(a_StreetObjectId       in out number,
                      a_ShortName            varchar2,
                      a_PreDirection         varchar2,
                      a_PostDirection        varchar2,
                      a_FormattedName        varchar2,
                      a_PosseStreetNameId    number);

end pkg_BCPAddressing_DEPRECATED;

 

/

create or replace package body pkg_BCPAddressing_DEPRECATED is


  /*---------------------------------------------------------------------
  * NewAddressObject() -- PUBLIC
  *-------------------------------------------------------------------*/
  procedure NewAddressObject(a_PosseObjectId       in out number,
                             a_TaxParcelNumber     varchar2,
                             a_AddressHouseNumber  number,
                             a_AddressSuite        varchar2,
                             a_LotNumber           varchar2,
                             a_BlockNumber         number,
                             a_LandArea            number,
                             a_ZipCode             varchar2,
                             a_ZipExtension        varchar2) is

  begin

    select bcpdata.AddressingSeq_s.NextVal
      into a_PosseObjectId
      from dual;

    insert into bcpdata.address values (a_PosseObjectId,
                                        a_TaxParcelNumber,
                                        a_AddressHouseNumber,
                                        a_AddressSuite,
                                        a_LotNumber,
                                        a_BlockNumber,
                                        a_LandArea,
                                        a_ZipCode,
                                        a_ZipExtension,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null);

  end NewAddressObject;


  /*---------------------------------------------------------------------
  * NewStreetObject() -- PUBLIC
  *-------------------------------------------------------------------*/
  procedure NewStreetObject(a_StreetObjectId       in out number,
                      a_ShortName            varchar2,
                      a_PreDirection         varchar2,
                      a_PostDirection        varchar2,
                      a_FormattedName        varchar2,
                      a_PosseStreetNameId    number) is

  begin

    select bcpdata.AddressingSeq_s.NextVal
      into a_StreetObjectId
      from dual;

    insert into bcpdata.Street values (a_ShortName,
                                       a_StreetObjectId,
                                       a_PreDirection,
                                       a_PostDirection,
                                       a_FormattedName,
                                       a_PosseStreetNameId);

  end NewStreetObject;

end pkg_BCPAddressing_DEPRECATED;

/

