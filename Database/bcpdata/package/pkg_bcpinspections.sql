create or replace package pkg_BCPInspections is

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*--------------------------------------------------------------------------
   * RelateInspectors() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelateInspector (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
   * OverrideUtility() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure OverrideUtility (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
   * RelateInspections() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelateInspections (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_ProcessViewName           varchar2,
    a_AllowComplete             varchar2 default 'N',
    a_IncludeDate             date default null
  );

  /*--------------------------------------------------------------------------
   * CopyInspection() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure CopyInspection (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
   * ReAssignInspector() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure ReAssignInspector (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
  * CreateReInspectionProcess() -- PUBLIC
  *------------------------------------------------------------------------*/
  procedure CreateReInspectionProcess(
    a_ProcessId                          udt_Id,
    a_AsOfDate                           date,
    a_ReInspectionDate                   Date
  );

  /*--------------------------------------------------------------------------
  * CreateRequestInspectionProcess() -- PUBLIC
  *------------------------------------------------------------------------*/
  procedure CreateRequestInspectionProcess(
    a_ProcessId                          udt_Id,
    a_AsOfDate                           date,
    a_InspectionTypeId                   Varchar2,
    a_ReInspectionDate                   Date
  );
end pkg_BCPInspections;
 
/

grant execute
on pkg_bcpinspections
to possesys;

grant execute
on pkg_bcpinspections
to posseuser;

create or replace package body pkg_BCPInspections is

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_ObjectList is api.udt_ObjectList;

  /*--------------------------------------------------------------------------
   * RelateInspectors() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelateInspector (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_CompletedUserId           udt_Id;
    t_InspectorUserId           udt_Id;
    t_RelId                     udt_Id;
    t_IsInspector               varchar2(1);
    t_HasInspector              varchar2(1);
    procedure SetErrorArguments is
      begin
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('ProcessId', a_ProcessId);
        api.pkg_Errors.SetArgValue('As Of Date', a_AsOfDate);
      end;
  begin
    begin
      select us.UserId
        into t_CompletedUserId
        from api.Processes pr
        join api.users us
          on us.OracleLogonId = pr.CompletedBy
       where ProcessId = a_ProcessId;
    exception when no_data_found then
      return;
    end;

    -- Check if the user completing the process is api.Notes no inspector
    begin
      select 'Y'
        into t_IsInspector
        from api.accessgroupUsers agu
        join api.accessgroups ag
          on agu.AccessGroupId = ag.AccessGroupId
       where ag.Description = 'Inspector'
         and agu.UserId = t_CompletedUserId;
    exception when no_data_found then
      t_IsInspector := 'N';
    end;

    --Check if only 1 inspector is assigned to the process
    begin
      select 'Y',
             pa.UserId
        into t_HasInspector,
             t_InspectorUserId
        from api.AccessGroupUsers agu
        join api.AccessGroups ag
          on agu.AccessGroupId = ag.AccessGroupId
        join api.ProcessAssignments pa
          on agu.UserId = pa.UserId
       where ag.Description = 'Inspector'
         and pa.ProcessId = a_ProcessId;
    exception when no_data_found then
      t_HasInspector := 'N';
              when too_many_rows then
      t_HasInspector := 'N';
    end;

    if t_IsInspector = 'Y' and api.pkg_columnquery.Value(a_ProcessId, 'InspectorName') is null then
      t_RelId := pkg_Utils.RelationshipNew(a_ProcessId, t_CompletedUserId, 'Inspector:');
    elsif t_HasInspector = 'Y' and api.pkg_columnquery.Value(a_ProcessId, 'InspectorName') is null then
      t_RelId := pkg_Utils.RelationshipNew(a_ProcessId, t_InspectorUserId, 'Inspector:');
    else
      begin
        select UserId
          into t_InspectorUserId
          from api.Users
         where Name = api.pkg_columnquery.Value(a_ProcessId, 'InspectorName');
      exception when no_data_found then
        SetErrorArguments;
        api.pkg_Errors.RaiseError(-20000, 'Please set the Inspector');
      end;

      --t_RelId := pkg_Utils.RelationshipNew(a_ProcessId, t_InspectorUserId);
    end if;
  end;

  /*--------------------------------------------------------------------------
   * OverrideUtility() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure OverrideUtility (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_JobId                     udt_Id;
    t_GasRelId                  udt_Id;
    t_JobGasRelId               udt_Id;
    t_GasObjectId               udt_Id;
    t_PowerRelId                udt_Id;
    t_JobPowerRelId             udt_Id;
    t_PowerObjectId             udt_Id;
    procedure SetErrorArguments is
      begin
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('ProcessId', a_ProcessId);
        api.pkg_Errors.SetArgValue('As Of Date', a_AsOfDate);
      end;
  begin
    --Get the Job ID
    begin
      select JobId
        into t_JobId
        from api.processes
       where processId = a_ProcessId;
    exception when no_data_found then
      return;
    end;

    --Try to get the relationship from the process to the gas utility
    begin
      select GAS.ObjectId
        into t_GasObjectId
        from api.Relationships REL
        join query.o_GasUtility GAS
          on GAS.ObjectId = Rel.ToObjectId
       where REL.FromObjectId = a_ProcessId;
    exception when no_data_found then
      -- If we get here, it really is not an error - just that there is no utility
      null;
    end;
    --See if a relationship to a gas utility object from the process exists
    if t_GasObjectId is not null then
      --Try to get the relationship from the job to the gas utility
      begin
        select REL.RelationshipId
          into t_JobGasRelId
          from api.Relationships REL
          join query.o_GasUtility GAS
            on GAS.ObjectId = Rel.ToObjectId
         where REL.FromObjectId = t_JobId;
      exception when no_data_found then
        -- If we get here, it really is not an error - just that there is no utility
        null;
      end;
      if t_JobGasRelId is not null then
        --If one exists, delete it
        api.pkg_RelationshipUpdate.Remove(t_JobGasRelId, null);
      end if;

      --Create a relationship to the new gas utility on the job
      t_JobGasRelId := pkg_Utils.RelationshipNew(t_GasObjectId, t_JobId);
    end if;



    --Try to get the relationship from the process to the power utility
    begin
      select PWR.ObjectId
        into t_PowerObjectId
        from api.Relationships REL
        join query.o_PowerUtility PWR
          on PWR.ObjectId = Rel.ToObjectId
       where REL.FromObjectId = a_ProcessId;
    exception when no_data_found then
      -- If we get here, it really is not an error - just that there is no utility
      null;
    end;
    --See if a relationship to a power utility object from the process exists
    if t_PowerObjectId is not null then
      --Try to get the relationship from the job to the power utility
      begin
        select REL.RelationshipId
          into t_JobPowerRelId
          from api.Relationships REL
          join query.o_PowerUtility PWR
            on PWR.ObjectId = Rel.ToObjectId
         where REL.FromObjectId = t_JobId;
      exception when no_data_found then
        -- If we get here, it really is not an error - just that there is no utility
        null;
      end;
      if t_JobPowerRelId is not null then
        --If one exists, delete it
        api.pkg_RelationshipUpdate.Remove(t_JobPowerRelId, null);
      end if;

      --Create a relationship to the new power utility on the job
      t_JobPowerRelId := pkg_Utils.RelationshipNew(t_PowerObjectId, t_JobId);
    end if;
  end;


  /*--------------------------------------------------------------------------
   * RelateInspectors() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelateInspections (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_ProcessViewName           varchar2,
    a_AllowComplete             varchar2 default 'N',
    a_IncludeDate               date default null
  ) is
    --t_ProcessTypeId             udt_Id;
    t_JobTypeId                 udt_Id;
    t_JobId                     udt_Id;
    t_RelId                     udt_Id;
    t_ProcessTypes              udt_ObjectList;
    procedure SetErrorArguments is
      begin
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('JobId', t_JobId);
        api.pkg_Errors.SetArgValue('As Of Date', a_AsOfDate);
        api.pkg_Errors.SetArgValue('Process View Name', a_ProcessViewName);
      end;
  begin
    -- If we are on a process get the JobId
    begin
      select PRO.JobId
        into t_JobId
        from api.processes PRO
       where PRO.ProcessId = a_ObjectId;
    exception when no_data_found then
      -- Then we must be running this on the job
      t_JobId := a_ObjectId;
    end;

    --api.pkg_Errors.RaiseError(-20000, a_IncludeDate);

    -- Get JobTypeId
    select JobTypeId
      into t_JobTypeId
      from api.jobs
     where JobId = t_JobId;

    -- Get the ProcessTypeIds
    toolbox.pkg_parse.DefIDListFromString(a_ProcessViewName, ',', t_ProcessTypes, false);

    -- Get all process of the specified type that are not related to a job of the type passed in
    if(a_AllowComplete = 'N') then
      for c in (select INS.ProcessId
                  from api.processes INS
                  join table(cast(t_ProcessTypes as api.udt_objectlist)) PT
                    on INS.ProcessTypeId = PT.ObjectId
                 where INS.Outcome is null
                   and (INS.ScheduledStartDate < a_IncludeDate + 1
                       or a_IncludeDate is null)
                 minus
                select P.ProcessId
                  from table(cast(t_ProcessTypes as api.udt_objectlist)) PT
                  join api.processes P
                    on P.ProcessTypeId = PT.ObjectId
                  join api.relationships REL
                    on P.ProcessId = REL.FromObjectId
                  join api.Jobs JOB
                    on REL.ToObjectId = JOB.JobId
                 where JOB.JobTypeId = t_JobTypeId
                   and (P.ScheduledStartDate < a_IncludeDate + 1
                       or a_IncludeDate is null)
                   and P.Outcome is null) loop
        -- Relate them to this job
        begin
          t_RelId := pkg_Utils.RelationshipNew(t_JobId, c.ProcessId);
        exception when others then
          SetErrorArguments;
          api.pkg_Errors.RaiseError(-20000, 'Unable to relate job ' || t_JobId || ' and process ' || c.ProcessId);
        end;
      end loop;
    else
      for c in (select INS.ProcessId
                  from api.processes INS
                  join table(cast(t_ProcessTypes as api.udt_objectlist)) PT
                    on INS.ProcessTypeId = PT.ObjectId
                   and (INS.ScheduledStartDate < a_IncludeDate + 1
                       or a_IncludeDate is null)
                 minus
                 select P.ProcessId
                   from table(cast(t_ProcessTypes as api.udt_objectlist)) PT
                   join api.processes P
                     on P.ProcessTypeId = PT.ObjectId
                   join api.relationships REL
                     on P.ProcessId = REL.FromObjectId
                   join api.Jobs JOB
                     on REL.ToObjectId = JOB.JobId
                  where JOB.JobTypeId = t_JobTypeId
                    and (P.ScheduledStartDate < a_IncludeDate + 1
                        or a_IncludeDate is null))loop
        -- Relate them to this job
        begin
          t_RelId := pkg_Utils.RelationshipNew(t_JobId, c.ProcessId);
        exception when others then
          SetErrorArguments;
          api.pkg_Errors.RaiseError(-20000, 'Unable to relate job ' || t_JobId || ' and process ' || c.ProcessId);
        end;
      end loop;
    end if;
  end;

  /*--------------------------------------------------------------------------
   * CopyInspection() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure CopyInspection (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date
  ) is
  t_ProcessTypeId               udt_Id := api.pkg_COnfigQuery.ObjectDefIdForName('p_RequestInspection');
  t_RequestProcessId            udt_Id;
  t_ObjectLogicalTransaction    udt_Id;
  t_DummyId                     udt_Id;
  begin
    pkg_Debug.PutLine('*** Start of CopyInspection ***');
    select CreatedLogicalTransactionId
      into t_ObjectLogicalTransaction
      from api.Objects ob
     where ObjectId = a_ObjectId;

    if api.pkg_LogicalTransactionUpdate.CurrentTransaction !=  t_ObjectLogicalTransaction then
      raise_application_error(-20100, 'This Inspection has been requested already. You cannot request it again.');
    end if;

    if api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestProcessId') is null then

      t_RequestProcessId := api.Pkg_ProcessUpdate.New(api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'PermitJobId'),
                                                      t_ProcessTypeId, null, null, null, null);
      api.pkg_ColumnUpdate.SetValue(t_RequestProcessId, 'RequestedBy', api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestedBy'));
      api.pkg_ColumnUpdate.SetValue(t_RequestProcessId, 'RequestorComments', api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestorComments'));
      api.pkg_ColumnUpdate.SetValue(t_RequestProcessId, 'ContactPhoneNumber', api.pkg_ColumnQuery.Value(a_ObjectId, 'ContactPhoneNumber'));
      api.pkg_ColumnUpdate.SetValue(t_RequestProcessId, 'RequestedTime', api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestedTime'));
      api.pkg_ColumnUpdate.SetValue(t_RequestProcessId, 'RequestedDate', api.pkg_ColumnQuery.Value(a_ObjectId, 'RequestedDate'));
      t_DummyId := pkg_utils.RelationshipNew(a_ObjectId, t_RequestProcessId);
      toolbox.pkg_util.CopyObjectRelationships(a_ObjectId, null, t_RequestProcessId, null, sysdate, true);
      api.Pkg_ProcessUpdate.Complete(t_RequestProcessId,  'Requested');
    end if;

  end;

  /*--------------------------------------------------------------------------
   * ReAssignInspector() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure ReAssignInspector (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
  t_PerformProcesses            udt_IdList;
  t_UserLogon                   varchar2(40);
  t_ReAssignTo                  varchar2(100);
  t_UserId                      udt_Id;
  begin

    if api.pkg_ColumnQuery.Value(a_ProcessId, 'ReAssignTo') is null then
      return;
    end if;


    t_ReAssignTo := api.pkg_ColumnQuery.Value(a_ProcessId, 'ReAssignTo');

    for d in (select us.OracleLogonId,
                     ipa.ProcessId
                from api.Users us
                join api.IncompleteProcessAssignments ipa
                  on ipa.AssignedTo = us.UserId
               where ipa.ProcessId = a_ProcessId) loop
      api.pkg_ProcessUpdate.UnAssign(d.ProcessId, d.OracleLogonId);
    end loop;

    select OracleLogonId, UserId
      into t_UserLogon, t_UserId
      from api.users
     where name = t_ReAssignTo;
    api.Pkg_ProcessUpdate.Assign(a_ProcessId, t_UserLogon);

    pkg_utils.RelationshipRemove(a_ProcessId, t_UserId, 'Reassigned Inspector:');

  end;

 /*--------------------------------------------------------------------------
  * CreateReInspectionProcess() -- PUBLIC
  *------------------------------------------------------------------------*/
  procedure CreateReInspectionProcess(
    a_ProcessId                          udt_Id,
    a_AsOfDate                           date,
    a_ReInspectionDate                   Date
  ) is
  t_NewProcessId                         udt_Id;
  t_ProcessTypeId                        udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('p_PerformViolationInspection');
  t_JobId                                udt_id;
  begin

    select p.jobid
      into t_JobId
      from query.p_PerformViolationInspection p
     where p.ProcessId = a_ProcessId;

    t_NewProcessId := api.pkg_processupdate.New(t_JobId,t_ProcessTypeId,null, a_ReInspectionDate, null, null);

end CreateReInspectionProcess;

 /*--------------------------------------------------------------------------
  * CreateRequestInspectionProcess() -- PUBLIC
  *------------------------------------------------------------------------*/
  procedure CreateRequestInspectionProcess(
    a_ProcessId                          udt_Id,
    a_AsOfDate                           date,
    a_InspectionTypeId                   Varchar2,
    a_ReInspectionDate                   Date
  ) is
  t_EndPointId                           udt_Id := api.pkg_configquery.EndPointIdForName(api.pkg_configquery.ObjectDefIdForName('p_RequestInspection'),'RequestInspectionType');
  t_RequestInspEndPointId                udt_Id := api.pkg_configquery.EndPointIdForName(api.pkg_configquery.ObjectDefIdForName('o_requestinspectiontype'),'InspectionType');
  t_NewProcessId                         udt_Id;
  t_ProcessTypeId                        udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('p_RequestInspection');
  t_RequestObjectDefId                   udt_Id := api.pkg_configquery.ObjectDefIdForName('o_requestinspectiontype');
  t_JobId                                udt_id;
  t_ProcessId                            udt_id;
  t_NewRel                               udt_id;
  t_NewRel2                              udt_id;
  t_NewObjectId                          udt_id;
  t_RecentRequestInspection              udt_id;
  begin
    select p.jobid
      into t_JobId
      from query.p_PerformBuildingInspection p
     where p.ProcessId = a_ProcessId;

    --Find the most recent Request Inspection Process
    begin
      Select max(i.ObjectId)
      into t_RecentRequestInspection
      From query.p_requestinspection i
      where i.JobId = t_JobId;
    exception when no_data_found then
      t_RecentRequestInspection := null;
    end;

    t_NewProcessId := api.pkg_processupdate.New(t_JobId,t_ProcessTypeId,null, sysdate, null, null);

    if a_InspectionTypeId is not null then
      --Create New Request Inspection Type ObjectId
      t_NewObjectId := api.pkg_objectupdate.New(t_RequestObjectDefId);
      t_NewRel := api.pkg_Relationshipupdate.New(t_EndPointId,t_NewProcessId, t_NewObjectId);
      t_NewRel2 := api.pkg_Relationshipupdate.New(t_RequestInspEndPointId,t_NewObjectId, a_InspectionTypeId);
   end if;

   --Find information off of the most recent Request Inspection Process and
   --copy it to the new process
   if t_RecentRequestInspection is not null then
      api.pkg_columnupdate.SetValue(t_NewProcessId, 'RequestedBy',
         api.pkg_columnquery.Value(t_RecentRequestInspection, 'RequestedBy'));
      api.pkg_columnupdate.SetValue(t_NewProcessId, 'ContactPhoneNumber',
         api.pkg_columnquery.Value(t_RecentRequestInspection, 'ContactPhoneNumber'));
      api.pkg_columnupdate.SetValue(t_NewProcessId, 'RequestedDate',
         api.pkg_columnquery.Value(t_RecentRequestInspection, 'RequestedDate'));
      api.pkg_columnupdate.SetValue(t_NewProcessId, 'RequestedTime',
         api.pkg_columnquery.Value(t_RecentRequestInspection, 'RequestedTime'));
      api.pkg_columnupdate.SetValue(t_NewProcessId, 'RequestorComments',
         api.pkg_columnquery.Value(t_RecentRequestInspection, 'RequestorComments'));
   end if;

    --api.pkg_ColumnUpdate.SetValue(t_NewProcessId, 'InspectionTypeObjectIds', a_InspectionTypeId);
    api.pkg_ColumnUpdate.SetValue(t_NewProcessId, 'IsReinspection', 'Y');
    api.pkg_processupdate.Complete(t_NewProcessId, 'Requested');



  end CreateRequestInspectionProcess;

end pkg_BCPInspections;
/

