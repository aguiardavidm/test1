create or replace package pkg_relationships as
  /*--------------------------------------------------------------------------
  * Types -- PUBLIC
  *------------------------------------------------------------------------*/

  SUBTYPE udt_ID IS api.pkg_Definition.udt_ID;
  SUBTYPE udt_IdList IS api.pkg_definition.udt_IdList;
  /*---------------------------------------------------------------------------
  * ContractorBuildingPermits() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure ContractorBuildingPermits(a_objectid   udt_id,
                                      a_EndpointId udt_id,
                                      a_objectlist out api.udt_objectlist);

  /*---------------------------------------------------------------------------
  * ContractorGeneralPermits() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure ContractorGeneralPermits(a_objectid   udt_id,
                                     a_EndpointId udt_id,
                                     a_objectlist out api.udt_objectlist);
  /*---------------------------------------------------------------------------
  * ContractorTradePermits() -- PUBLIC
  *-------------------------------------------------------------------------*/

  procedure ContractorTradePermits(a_objectid   udt_id,
                                   a_EndpointId udt_id,
                                   a_objectlist out api.udt_objectlist);
  /*---------------------------------------------------------------------------
  * BuildingInspections() -- PUBLIC
  *-------------------------------------------------------------------------*/
                                   
  procedure BuildingInspections(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist);                                   

  /*---------------------------------------------------------------------------
  * BuildingInspections() -- PUBLIC
  *-------------------------------------------------------------------------*/
                                   
  procedure MechanicalInspections(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist); 
                                
  /*---------------------------------------------------------------------------
  * BuildingInspections() -- PUBLIC
  *-------------------------------------------------------------------------*/
                                   
  procedure ElectricalInspections(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist);
  /*---------------------------------------------------------------------------
  * BuildingInspections() -- PUBLIC
  *-------------------------------------------------------------------------*/
                                   
  procedure PlumbingInspections(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist);                                                                  
  /*---------------------------------------------------------------------------
  * RezoningParcelCurrentZoning() -- PUBLIC
  *-------------------------------------------------------------------------*/

  procedure RezoningParcelCurrentZoning(a_objectid udt_id, a_asofdate date);

  /*--------------------------------------------------------------------------------------------
  * RezoningPropertyParcelRel() -- PUBLIC
  *------------------------------------------------------------------------------------------*/

  procedure RezoningPropertyParcelRel(a_objectid udt_Id, a_asofdate date);
  /*--------------------------------------------------------------------------------------------
  * PlanningPropertyParcelRel() -- PUBLIC
  *------------------------------------------------------------------------------------------*/

  procedure PlanningPropertyParcelRel(a_objectid udt_Id, a_asofdate date);
  /*--------------------------------------------------------------------------------------------
  * RezoningPropertyCreation() -- PUBLIC
  *------------------------------------------------------------------------------------------*/

  procedure PlanningPropertyCreation(a_objectid udt_id, a_asofdate date);

  /*---------------------------------------------------------------------------
  * RezoningPropertyUpdate() --PUBLIC
  *---------------------------------------------------------------------------*/
  procedure PlanningPropertyUpdate(a_objectid udt_id, a_asofdate date);

  /*---------------------------------------------------------------------------
  * UserMileageLog() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure UserMileageLog(a_objectid   udt_id,
                           a_EndpointId udt_id,
                           a_objectlist out api.udt_objectlist);

  /*---------------------------------------------------------------------------
  * UserTimeLog() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure UserTimeLog(a_objectid   udt_id,
                        a_EndpointId udt_id,
                        a_objectlist out api.udt_objectlist);

  /*---------------------------------------------------------------------------
  * UserPerformBuilding() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformBuilding(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist);

  /*---------------------------------------------------------------------------
  * UserPerformElectrical() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformElectrical(a_objectid   udt_id,
                                  a_EndpointId udt_id,
                                  a_objectlist out api.udt_objectlist);

  /*---------------------------------------------------------------------------
  * UserPerformMechanical() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformMechanical(a_objectid   udt_id,
                                  a_EndpointId udt_id,
                                  a_objectlist out api.udt_objectlist);

  /*---------------------------------------------------------------------------
  * UserPerformPlumbing() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformPlumbing(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist);

  /*---------------------------------------------------------------------------
  * ProjectHierarchy() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure ProjectHierarchy(a_ObjectId         udt_Id,
                             a_EndPointId       udt_Id,
                             a_RelatedObjectIds out api.udt_ObjectList);

  /*---------------------------------------------------------------------------
  * GetMasterProject() -- PUBLIC
  *-------------------------------------------------------------------------*/
  function GetMasterProject(a_JobId udt_Id) return udt_Id;

  /*---------------------------------------------------------------------------
  * GetHierarchy() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure GetHierarchy(a_TopJob udt_Id, a_Objects in out api.udt_ObjectList);

  /*---------------------------------------------------------------------------
  * SubPermitHierarchy() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure SubPermitHierarchy(a_ObjectId         udt_Id,
                               a_EndPointId       udt_Id,
                               a_RelatedObjectIds out api.udt_ObjectList); 

  /*---------------------------------------------------------------------------
  * GetHierarchyOneLevel() -- PUBLIC
  *-------------------------------------------------------------------------*/
--  procedure GetHierarchyOneLevel(a_TopJob udt_Id, 
--                                 a_Objects in out api.udt_ObjectList);

  /*---------------------------------------------------------------------------
  * HierarchySort() -- PUBLIC
  *-------------------------------------------------------------------------*/
--  function HierarchySort(a_JobId udt_Id) return number;

 /*---------------------------------------------------------------------------
  * MasterProjectProperty() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure MasterProjectProperty(a_objectid   udt_id,
                                  a_EndpointId udt_id,
                                  a_objectlist out api.udt_objectlist);

  /*---------------------------------------------------------------------------
  * GetViolations() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure GetViolations(a_ObjectId         udt_Id,
                          a_EndPointId       udt_Id,
                          a_RelatedObjectIds out api.udt_ObjectList);
                          
  /*---------------------------------------------------------------------------
  * UserPerformViolation() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformViolation(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist);                          


  /*---------------------------------------------------------------------------
  * UserPerformInvestigation() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformInvestigation(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist);
  /*---------------------------------------------------------------------------
  * AddressRelateFloodPlain() -- PUBLIC - Relates a flood zone to a Permit
  * if the permit is on a flood zone
  *-------------------------------------------------------------------------*/
  procedure AddressRelateFloodZone(a_RelationshipId   udt_id,
                                    a_AsOfDate         date);  
                                     
/*---------------------------------------------------------------------------
 * DefaultPermitOwner() -- PUBLIC - Defaults the owner on the permit
 *-------------------------------------------------------------------------*/
  procedure DefaultPermitOwner(a_RelationshipId    udt_id,
                                a_AsOfDate          date);                            
                      
 /*---------------------------------------------------------------------------
  * ConditionsForJob() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure ConditionsForJob(a_objectid   udt_id,
                            a_EndpointId udt_id,
                            a_objectlist out api.udt_objectlist);
                            
  procedure OtherOpenInvestigations (
    a_objectid   udt_id,
    a_EndpointId udt_id,
    a_objectlist out api.udt_objectlist
  );         
  
 /*---------------------------------------------------------------------------
   * GetPrimaryAddress() - public
   *-------------------------------------------------------------------------*/
  procedure GetPrimaryAddress (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjectIds out  api.udt_ObjectList); 
    
  /*---------------------------------------------------------------------------
   * GetRelatedTimeLogs() - public
   *-------------------------------------------------------------------------*/
  procedure GetRelatedTimeLogs(
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjects out    api.udt_ObjectList);
    
  /*---------------------------------------------------------------------------
   * GetAllInspectionTypes() - public
   *-------------------------------------------------------------------------*/
  procedure GetAllInspectionTypes(
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjects out    api.udt_ObjectList);
 
  /*---------------------------------------------------------------------------
   * GetOrderJobViolations() - public
   *-------------------------------------------------------------------------*/
  procedure GetOrderJobViolations(
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjects out    api.udt_ObjectList);
   
  /*---------------------------------------------------------------------------
   * GetParcelOwnerAsDefendant() - public
   *-------------------------------------------------------------------------*/
  procedure GetParcelOwnerAsDefendant (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjects out    api.udt_ObjectList
  );
  
  /*---------------------------------------------------------------------------
   * CopyViolationsToNOV() - public
   *  Creating relationships to Violations from a Case File that have not been 
   *  previously related to an NOV
   *-------------------------------------------------------------------------*/
  procedure CopyViolationsToNOV (
    a_RelationshipId        udt_Id,
    a_AsOfDate              date
  ); 
  
  /*---------------------------------------------------------------------------
   * CopyViolationTypeToViolation() - public
   *  Copy the details from the Violation Type to the Violation.
   * This is designed to run on constructor of the rel between Violation Type
   * and Violation.
   *-------------------------------------------------------------------------*/
  procedure CopyViolationTypeToViolation (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );
  
  /*---------------------------------------------------------------------------
   * CopyCSTemplateToCodeSection() - public
   *  Copy the details from the Code Section Template to the Code Section.
   * This is designed to run on constructor of the rel between Code Section
   * Template and Code Section.
   *-------------------------------------------------------------------------*/
  procedure CopyCSTemplateToCodeSection (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );
    
END pkg_relationships;

 

/

grant debug
on pkg_relationships
to keaton;

grant execute
on pkg_relationships
to keaton;

grant execute
on pkg_relationships
to posseextensions;

create or replace package body pkg_relationships as

g_count  number := 0;

  /*---------------------------------------------------------------------------
  * ContractorBuildingPermits() -- PUBLIC
  *-------------------------------------------------------------------------*/

  procedure ContractorBuildingPermits(a_objectid   udt_id,
                                      a_EndpointId udt_id,
                                      a_objectlist out api.udt_objectlist) is
  
    t_ContractorEndpointId        udt_id := api.pkg_configquery.EndPointIdForName('u_Users',
                                                                                  'Contractor');
    t_BPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorElectrical');
    t_BPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorBuilding');
    t_BPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorMech');
    t_BPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorPlumbing');
    t_BPEndpointId                udt_id := api.pkg_configquery.EndPointIdForName('o_Users',
                                                                                  'BuildingPermit');
    t_ObjectList                  api.udt_objectlist;
  
  begin
    t_ObjectList := api.udt_objectlist();
    a_ObjectList := api.udt_objectList();
  
    select api.udt_object(ObjectId) bulk collect
      into t_ObjectList
      from (select distinct r1.ToObjectId ObjectId
              from api.relationships r
              join api.relationships r1
                on r1.FromObjectId = r.ToObjectId
              join api.jobs j
                on j.jobid = r1.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_ObjectId
               and r1.endpointid in
                   (t_BPContractorElecEndpointId,
                    t_BPContractorBldgEndpointId,
                    t_BPContractorMechEndpointId,
                    t_BPContractorPlumbEndpointId)
               and r.EndPointId = t_ContractorEndpointId
               and s.Tag <> 'COMP');
  
    dbms_output.put_line(a_ObjectList.count);
    dbms_output.put_line(t_ObjectList.count);
  
    select api.udt_object(ObjectId) bulk collect
      into a_ObjectList
      from (select distinct r.ToObjectId ObjectId
              from api.relationships r
              join api.jobs j
                on j.jobid = r.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_Objectid
               and r.endpointid = t_BPEndpointId
               and s.Tag <> 'COMP');
  
    dbms_output.put_line(a_ObjectList.count);
  
    extension.Pkg_Collectionutils.Append(a_objectlist, t_objectlist);
  
  end;

  /*---------------------------------------------------------------------------
  * ContractorGeneralPermits() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure ContractorGeneralPermits(a_objectid   udt_id,
                                     a_EndpointId udt_id,
                                     a_objectlist out api.udt_objectlist) is
  
    t_ContractorEndpointId        udt_id := api.pkg_configquery.EndPointIdForName('u_Users',
                                                                                  'Contractor');
    t_GPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorElectrical');
    t_GPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorBuilding');
    t_GPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorMech');
    t_GPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorPlumbing');
    t_GPContractorOtherEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorOther');
    t_GPEndpointId                udt_id := api.pkg_configquery.EndPointIdForName('o_Users',
                                                                                  'GeneralPermit');
    t_ObjectList                  api.udt_objectlist;
  
  begin
    t_ObjectList := api.udt_objectlist();
    a_ObjectList := api.udt_objectList();
  
    select api.udt_object(ObjectId) bulk collect
      into t_ObjectList
      from (select distinct r1.ToObjectId ObjectId
              from api.relationships r
              join api.relationships r1
                on r1.FromObjectId = r.ToObjectId
              join api.jobs j
                on j.jobid = r1.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_ObjectId
               and r1.endpointid in
                   (t_GPContractorElecEndpointId,
                    t_GPContractorBldgEndpointId,
                    t_GPContractorMechEndpointId,
                    t_GPContractorPlumbEndpointId,
                    t_GPContractorOtherEndpointId)
               and r.EndPointId = t_ContractorEndpointId
               and s.Tag <> 'COMP');
  
    dbms_output.put_line(a_ObjectList.count);
    dbms_output.put_line(t_ObjectList.count);
  
    select api.udt_object(ObjectId) bulk collect
      into a_ObjectList
      from (select distinct r.ToObjectId ObjectId
              from api.relationships r
              join api.jobs j
                on j.jobid = r.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_Objectid
               and r.endpointid = t_GPEndpointId
               and s.Tag <> 'COMP');
  
    dbms_output.put_line(a_ObjectList.count);
  
    extension.Pkg_Collectionutils.Append(a_objectlist, t_objectlist);
  
  end;

  /*---------------------------------------------------------------------------
  * ContractorTradePermits() -- PUBLIC
  *-------------------------------------------------------------------------*/

  procedure ContractorTradePermits(a_objectid   udt_id,
                                   a_EndpointId udt_id,
                                   a_objectlist out api.udt_objectlist) is
  
    t_ContractorEndpointId        udt_id := api.pkg_configquery.EndPointIdForName('u_Users',
                                                                                  'Contractor');
    t_TPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorElectrical');
    t_TPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorBuilding');
    t_TPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorMech');
    t_TPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorPlumbing');
    t_TPEndpointId                udt_id := api.pkg_configquery.EndPointIdForName('o_Users',
                                                                                  'TradePermit');
    t_ObjectList                  api.udt_objectlist;
  
  begin
    t_ObjectList := api.udt_objectlist();
    a_ObjectList := api.udt_objectList();
  
    select api.udt_object(ObjectId) bulk collect
      into t_ObjectList
      from (select distinct r1.ToObjectId ObjectId
              from api.relationships r
              join api.relationships r1
                on r1.FromObjectId = r.ToObjectId
              join api.jobs j
                on j.jobid = r1.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_ObjectId
               and r1.endpointid in
                   (t_TPContractorElecEndpointId,
                    t_TPContractorBldgEndpointId,
                    t_TPContractorMechEndpointId,
                    t_TPContractorPlumbEndpointId)
               and r.EndPointId = t_ContractorEndpointId
               and s.Tag <> 'COMP');
  
    dbms_output.put_line(a_ObjectList.count);
    dbms_output.put_line(t_ObjectList.count);
  
    select api.udt_object(ObjectId) bulk collect
      into a_ObjectList
      from (select distinct r.ToObjectId ObjectId
              from api.relationships r
              join api.jobs j
                on j.jobid = r.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_Objectid
               and r.endpointid = t_TPEndpointId
               and s.Tag <> 'COMP');
  
    dbms_output.put_line(a_ObjectList.count);
  
    extension.Pkg_Collectionutils.Append(a_objectlist, t_objectlist);
  
  end;
  
 /*---------------------------------------------------------------------------
  * BuildingInspections() -- PUBLIC
  *-------------------------------------------------------------------------*/

  procedure BuildingInspections(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist) is  

    t_ContractorEndpointId        udt_id := api.pkg_configquery.EndPointIdForName('u_Users',
                                                                                  'Contractor');
    t_BPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorElectrical');
    t_BPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorBuilding');
    t_BPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorMech');
    t_BPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorPlumbing');   
    t_GPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorElectrical');
    t_GPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorBuilding');
    t_GPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorMech');
    t_GPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorPlumbing');
    t_GPContractorOtherEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorOther');    
    t_TPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorElectrical');
    t_TPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorBuilding');
    t_TPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorMech');
    t_TPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorPlumbing');  
    t_ObjectList                  api.udt_objectlist;
    t_TempResults1                api.udt_objectlist;
  
  begin
    t_ObjectList   := api.udt_objectlist();
    a_ObjectList   := api.udt_objectList();
    t_TempResults1 := api.udt_objectList();
    
      select api.udt_object(ObjectId) bulk collect
      into t_ObjectList
      from (select distinct r1.ToObjectId ObjectId
              from api.relationships r
              join api.relationships r1
                on r1.FromObjectId = r.ToObjectId
              join api.jobs j
                on j.jobid = r1.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_ObjectId
               and r1.endpointid in
                   (t_BPContractorElecEndpointId,
                    t_BPContractorBldgEndpointId,
                    t_BPContractorMechEndpointId,
                    t_BPContractorPlumbEndpointId,
                    t_GPContractorElecEndpointId,
                    t_GPContractorBldgEndpointId,
                    t_GPContractorMechEndpointId,
                    t_GPContractorPlumbEndpointId,
                    t_GPContractorOtherEndpointId,
                    t_TPContractorElecEndpointId,
                    t_TPContractorBldgEndpointId,
                    t_TPContractorMechEndpointId,
                    t_TPContractorPlumbEndpointId)
               and r.EndPointId = t_ContractorEndpointId
               and s.Tag <> 'COMP');      
               
    for c in 1..t_objectlist.Count loop
      for d in (select api.udt_object(p.processid) objectid
                  from api.processes p
                where p.JobId = t_objectlist(c).objectid
                  and p.ProcessTypeId = api.pkg_configquery.ObjectDefIdForName('p_PerformBuildingInspection')
                  and api.pkg_columnquery.Value(p.ProcessId, 'RelateInspection') = 'Y') loop
        t_TempResults1.Extend();          
        t_TempResults1(t_TempResults1.Last) := d.objectid;
      end loop; 
    end loop;           
  
    
    a_objectlist := t_TempResults1;
  
  end BuildingInspections;

 /*---------------------------------------------------------------------------
  * ElectricalInspections() -- PUBLIC
  *-------------------------------------------------------------------------*/

  procedure ElectricalInspections(a_objectid   udt_id,
                                  a_EndpointId udt_id,
                                  a_objectlist out api.udt_objectlist) is  

    t_ContractorEndpointId        udt_id := api.pkg_configquery.EndPointIdForName('u_Users',
                                                                                  'Contractor');
    t_BPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorElectrical');
    t_BPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorBuilding');
    t_BPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorMech');
    t_BPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorPlumbing');   
    t_GPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorElectrical');
    t_GPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorBuilding');
    t_GPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorMech');
    t_GPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorPlumbing');
    t_GPContractorOtherEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorOther');    
    t_TPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorElectrical');
    t_TPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorBuilding');
    t_TPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorMech');
    t_TPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorPlumbing');  
    t_ObjectList                  api.udt_objectlist;
    t_TempResults1                api.udt_objectlist;
  
  begin
    t_ObjectList   := api.udt_objectlist();
    a_ObjectList   := api.udt_objectList();
    t_TempResults1 := api.udt_objectList();
    
      select api.udt_object(ObjectId) bulk collect
      into t_ObjectList
      from (select distinct r1.ToObjectId ObjectId
              from api.relationships r
              join api.relationships r1
                on r1.FromObjectId = r.ToObjectId
              join api.jobs j
                on j.jobid = r1.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_ObjectId
               and r1.endpointid in
                   (t_BPContractorElecEndpointId,
                    t_BPContractorBldgEndpointId,
                    t_BPContractorMechEndpointId,
                    t_BPContractorPlumbEndpointId,
                    t_GPContractorElecEndpointId,
                    t_GPContractorBldgEndpointId,
                    t_GPContractorMechEndpointId,
                    t_GPContractorPlumbEndpointId,
                    t_GPContractorOtherEndpointId,
                    t_TPContractorElecEndpointId,
                    t_TPContractorBldgEndpointId,
                    t_TPContractorMechEndpointId,
                    t_TPContractorPlumbEndpointId)
               and r.EndPointId = t_ContractorEndpointId
               and s.Tag <> 'COMP');      
               
    for c in 1..t_objectlist.Count loop
      for d in (select api.udt_object(p.processid) objectid
                  from api.processes p
                where p.JobId = t_objectlist(c).objectid
                  and p.ProcessTypeId = api.pkg_configquery.ObjectDefIdForName('p_PerformElectricalInspection')
                  and api.pkg_columnquery.Value(p.ProcessId, 'RelateInspection') = 'Y') loop
        t_TempResults1.Extend();          
        t_TempResults1(t_TempResults1.Last) := d.objectid;
      end loop; 
    end loop;           
  
    
    a_objectlist := t_TempResults1;
  
  end ElectricalInspections;
  
  /*---------------------------------------------------------------------------
  * MechanicalInspections() -- PUBLIC
  *-------------------------------------------------------------------------*/

  procedure MechanicalInspections(a_objectid   udt_id,
                                  a_EndpointId udt_id,
                                  a_objectlist out api.udt_objectlist) is  

    t_ContractorEndpointId        udt_id := api.pkg_configquery.EndPointIdForName('u_Users',
                                                                                  'Contractor');
    t_BPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorElectrical');
    t_BPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorBuilding');
    t_BPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorMech');
    t_BPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorPlumbing');   
    t_GPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorElectrical');
    t_GPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorBuilding');
    t_GPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorMech');
    t_GPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorPlumbing');
    t_GPContractorOtherEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorOther');    
    t_TPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorElectrical');
    t_TPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorBuilding');
    t_TPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorMech');
    t_TPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorPlumbing');  
    t_ObjectList                  api.udt_objectlist;
    t_TempResults1                api.udt_objectlist;
  
  begin
    t_ObjectList   := api.udt_objectlist();
    a_ObjectList   := api.udt_objectList();
    t_TempResults1 := api.udt_objectList();
    
      select api.udt_object(ObjectId) bulk collect
      into t_ObjectList
      from (select distinct r1.ToObjectId ObjectId
              from api.relationships r
              join api.relationships r1
                on r1.FromObjectId = r.ToObjectId
              join api.jobs j
                on j.jobid = r1.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_ObjectId
               and r1.endpointid in
                   (t_BPContractorElecEndpointId,
                    t_BPContractorBldgEndpointId,
                    t_BPContractorMechEndpointId,
                    t_BPContractorPlumbEndpointId,
                    t_GPContractorElecEndpointId,
                    t_GPContractorBldgEndpointId,
                    t_GPContractorMechEndpointId,
                    t_GPContractorPlumbEndpointId,
                    t_GPContractorOtherEndpointId,
                    t_TPContractorElecEndpointId,
                    t_TPContractorBldgEndpointId,
                    t_TPContractorMechEndpointId,
                    t_TPContractorPlumbEndpointId)
               and r.EndPointId = t_ContractorEndpointId
               and s.Tag <> 'COMP');      
               
    for c in 1..t_objectlist.Count loop
      for d in (select api.udt_object(p.processid) objectid
                  from api.processes p
                where p.JobId = t_objectlist(c).objectid
                  and p.ProcessTypeId = api.pkg_configquery.ObjectDefIdForName('p_PerformMechanicalInspection')
                  and api.pkg_columnquery.Value(p.ProcessId, 'RelateInspection') = 'Y') loop
        t_TempResults1.Extend();          
        t_TempResults1(t_TempResults1.Last) := d.objectid;
      end loop; 
    end loop;           
  
    
    a_objectlist := t_TempResults1;
  
  end MechanicalInspections;

  /*---------------------------------------------------------------------------
  * PlumbingInspections() -- PUBLIC
  *-------------------------------------------------------------------------*/

  procedure PlumbingInspections(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist) is  

    t_ContractorEndpointId        udt_id := api.pkg_configquery.EndPointIdForName('u_Users',
                                                                                  'Contractor');
    t_BPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorElectrical');
    t_BPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorBuilding');
    t_BPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorMech');
    t_BPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'BPContractorPlumbing');   
    t_GPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorElectrical');
    t_GPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorBuilding');
    t_GPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorMech');
    t_GPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorPlumbing');
    t_GPContractorOtherEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'GPContractorOther');    
    t_TPContractorElecEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorElectrical');
    t_TPContractorBldgEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorBuilding');
    t_TPContractorMechEndpointId  udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorMech');
    t_TPContractorPlumbEndpointId udt_id := api.pkg_configquery.EndPointIdForName('o_Contractor',
                                                                                  'TPContractorPlumbing');  
    t_ObjectList                  api.udt_objectlist;
    t_TempResults1                api.udt_objectlist;
  
  begin
    t_ObjectList   := api.udt_objectlist();
    a_ObjectList   := api.udt_objectList();
    t_TempResults1 := api.udt_objectList();
    
      select api.udt_object(ObjectId) bulk collect
      into t_ObjectList
      from (select distinct r1.ToObjectId ObjectId
              from api.relationships r
              join api.relationships r1
                on r1.FromObjectId = r.ToObjectId
              join api.jobs j
                on j.jobid = r1.toobjectid
              join api.statuses s
                on s.StatusId = j.statusid
             where r.FromObjectId = a_ObjectId
               and r1.endpointid in
                   (t_BPContractorElecEndpointId,
                    t_BPContractorBldgEndpointId,
                    t_BPContractorMechEndpointId,
                    t_BPContractorPlumbEndpointId,
                    t_GPContractorElecEndpointId,
                    t_GPContractorBldgEndpointId,
                    t_GPContractorMechEndpointId,
                    t_GPContractorPlumbEndpointId,
                    t_GPContractorOtherEndpointId,
                    t_TPContractorElecEndpointId,
                    t_TPContractorBldgEndpointId,
                    t_TPContractorMechEndpointId,
                    t_TPContractorPlumbEndpointId)
               and r.EndPointId = t_ContractorEndpointId
               and s.Tag <> 'COMP');      
               
    for c in 1..t_objectlist.Count loop
      for d in (select api.udt_object(p.processid) objectid
                  from api.processes p
                where p.JobId = t_objectlist(c).objectid
                  and p.ProcessTypeId = api.pkg_configquery.ObjectDefIdForName('p_PerformPlumbingInspection')
                  and api.pkg_columnquery.Value(p.ProcessId, 'RelateInspection') = 'Y') loop
        t_TempResults1.Extend();          
        t_TempResults1(t_TempResults1.Last) := d.objectid;
      end loop; 
    end loop;           
  
    
    a_objectlist := t_TempResults1;
  
  end PlumbingInspections;

  /*---------------------------------------------------------------------------
  * RezoningParcelCurrentZoning() -- PUBLIC
  * This procedure is used to default the Current Zoning Relationship (Zoning Original Detail) on the Rezoning job when the primary parcel is selected
  * on the New Rezoning wizard.  It is run on the stored relationship from Rezoning to Parcel.
  *-------------------------------------------------------------------------*/

  procedure RezoningParcelCurrentZoning(a_objectid udt_id, a_asofdate date) is
  
    t_ContractorEndpointId       udt_id;
    t_RezoningParcelEndpointId   udt_id := api.pkg_configquery.EndPointIdForName('j_Rezoning',
                                                                                 'Parcel');
    t_RezoningJobid              number;
    t_ParcelObjectId             udt_id;
    t_NewZoningObjectId          number;
    t_OldZoningObjectId          number;
    t_ParcelZoningEndpointId     udt_id;
    t_CurrentZoningRelEndpointId number;
    t_Parcelrelcount             number;
    t_OldCurrentZoningRelId      number;
    t_NewRelId                   number;
  
  begin
    --Get Rezoning JobId
    select rj.FromObjectId
      into t_RezoningJobid
      from api.relationships rj
     where rj.EndPointId = t_RezoningParcelEndpointId
       and rj.RelationshipId = a_objectid;
  
    --Get count of parcels directly related to the Job.      
    select count(1)
      into t_Parcelrelcount
      from api.relationships r
     where r.FromObjectId = t_RezoningJobid
       and r.EndPointId = t_RezoningParcelEndpointId;
  
    --This should only be done if there is only one parcel.  Otherwise, the current zoning should stay as selected on the window (there would be no
    --obvious one to select anyway.
    --if t_Parcelrelcount > 1(Do to timing this has been commented out)
  
    --then null;
  
    --else 
  
    --Find Parcel objectid
    select r.ToObjectId
      into t_ParcelObjectId
      from api.relationships r
     where r.RelationshipId = a_objectid
       and r.EndPointId = t_RezoningParcelEndpointId;
  
    begin
    
      --Get ParcelZoning Endpoint
      t_ParcelZoningEndpointId := api.pkg_configquery.EndPointIdForName('o_Parcel',
                                                                        'ZoningCode');
    
      --Get New Zoning ObjectId                          
      select r2.ToObjectId
        into t_NewZoningObjectId
        from api.relationships r2
       where r2.FromObjectId = t_ParcelObjectId
         and r2.EndPointId = t_ParcelZoningEndpointId;
    
    exception
      when no_data_found then
        null;
      
    end;
  
    --Get RezoningCurrent(Original)Zoning EndpointId
    t_CurrentZoningRelEndpointId := api.pkg_configquery.EndPointIdForName('j_Rezoning',
                                                                          'CurrentZoningCode');
  
    --Get Old Zoning Object Id (for if there's already a property on the Rezoning job)
    begin
    
      select r.ToObjectId
        into t_OldZoningObjectId
        from api.relationships r
       where r.FromObjectId = t_RezoningJobid
         and r.EndPointId = t_CurrentZoningRelEndpointId;
    
    exception
      when no_data_found then
        t_OldZoningObjectId := null;
      
    end;
  
    --Replace Relationship if new is different than old
    if t_OldZoningObjectId = t_NewZoningObjectId or
       t_NewZoningObjectId is null
    
     then
      null;
    
    else
      --Delete old rel if exists
      if t_OldZoningObjectId is not null then
      
        select r.RelationshipId
          into t_OldCurrentZoningRelId
          from api.relationships r
         where r.FromObjectId = t_RezoningJobid
           and r.EndPointId = t_CurrentZoningRelEndpointId;
      
        --Remove old relationship to Current Zoning
        api.pkg_relationshipupdate.Remove(t_OldCurrentZoningRelId);
      
      else
        null;
      
      end if; --end if there was an old rel to delete
    
      --Create New Current Zoning Relationship based on existing parcel                                  
      t_NewRelId := api.pkg_relationshipupdate.new(t_CurrentZoningRelEndpointId,
                                                   t_RezoningJobid,
                                                   t_NewZoningObjectId);
    
    end if; --end if new zoning is different than old zoning
  
    --end if;  --end if only one relationship exists
    null;
  end;

  /*--------------------------------------------------------------------------------------------
  * This procedure is run on contructor of the parcel to rezoning Job relationship. It performs
  * the function of relating all parcels on a property to the job.
  *------------------------------------------------------------------------------------------*/

  procedure RezoningPropertyParcelRel(a_objectid udt_Id, a_asofdate date) is
  
    t_NewRelId                 udt_id;
    t_Parcelrelcount           number;
    t_PropertyObjectId         udt_id;
    t_ParcelObjectId           udt_id;
    t_PropertyEndPoint         udt_id := api.pkg_configquery.EndPointIdForName('j_Rezoning',
                                                                               'Property');
    t_PropertyParcelEndPointId udt_id := api.pkg_configquery.EndPointIdForName('o_Parcel',
                                                                               'Property');
    t_RezoningParcelEndpointId udt_id := api.pkg_configquery.EndPointIdForName('j_Rezoning',
                                                                               'Parcel');
    t_RezoningJobid            udt_id;
  
    t_OwnerName         varchar2(400);
    t_AddressLine1      varchar2(400);
    t_OwnerCity         varchar2(400);
    t_OwnerState        varchar2(400);
    t_OwnerZipCode      varchar2(400);
    t_OwnerZipExtension varchar2(400);
    t_OwnerPhoneNumber  varchar2(400);
    t_OwnerEmailAddress varchar2(400);
  
  begin
  
    --Get Rezoning JobId
    select rj.FromObjectId
      into t_RezoningJobid
      from api.relationships rj
     where rj.EndPointId = t_RezoningParcelEndpointId
       and rj.RelationshipId = a_objectid;
  
    --Get count of parcels directly related to the Job.      
    select count(1)
      into t_Parcelrelcount
      from api.relationships r
     where r.FromObjectId = t_RezoningJobid
       and r.EndPointId = t_RezoningParcelEndpointId;
  
    --Find Parcel objectid
    select r.ToObjectId
      into t_ParcelObjectId
      from api.relationships r
     where r.RelationshipId = a_objectid
       and r.EndPointId = t_RezoningParcelEndpointId;
  
    --Get the parcel owner information.
    begin
      select o.OwnerName,
             o.AddressLine1,
             o.AddressCity,
             o.State,
             o.ZipCode,
             o.ZipExtension,
             o.PhoneNumber,
             o.EmailAddress
        into t_OwnerName,
             t_AddressLine1,
             t_OwnerCity,
             t_OwnerState,
             t_OwnerZipCode,
             t_OwnerZipExtension,
             t_OwnerPhoneNumber,
             t_OwnerEmailAddress
        from query.o_owner o
       where o.ObjectId =
             (select max(o2.objectid)
                from query.r_ownerparcel op
                join query.o_owner o2
                  on o2.ObjectId = op.OwnerId
               where op.ParcelId = t_ParcelObjectId);
    
    exception
      when no_data_found then
        null;
    end;
  
    --Default the Owner details on the Application.
    api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                  'OwnerNameStored',
                                  t_OwnerName);
    api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                  'OwnerMailingAddressStored',
                                  t_AddressLine1);
    api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                  'OwnerCityStored',
                                  t_OwnerCity);
    api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                  'OwnerStateStored',
                                  t_OwnerState);
    api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                  'OwnerZipCodeStored',
                                  t_OwnerZipCode);
    api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                  'OwnerZipExtensionStored',
                                  t_OwnerZipExtension);
    api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                  'OwnerPhoneNumberStored',
                                  t_OwnerPhoneNumber);
    api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                  'OwnerEmailAddressStored',
                                  t_OwnerEmailAddress);
  
    --This should only be done if there is only one parcel. So as to not overwrite the Users addition of parcels.  
    if t_Parcelrelcount != 1
    
     then
      null;
    
    else
    
      --Find the most recently created property's objectid
      select max(r.ToObjectId)
        into t_PropertyObjectId
        from api.relationships r
       where r.FromObjectId = t_ParcelObjectId
         and r.EndPointId = t_PropertyParcelEndPointId;
    
      if t_PropertyObjectId is not null then
      
        --Loop Through all parcels related to the property and relate them to the rezoning job.
        for c in (select pp.ParcelObjectId
                    from query.r_propertyparcel pp
                   where pp.PropertyObjectId = t_PropertyObjectId
                     and pp.ParcelObjectId != t_ParcelObjectId) loop
        
          t_NewRelId := api.pkg_relationshipupdate.New(t_RezoningParcelEndpointId,
                                                       t_RezoningJobid,
                                                       c.ParcelObjectId);
        
        end loop;
      
        --Relate the existing property to the rezoning job
        t_NewRelId := api.pkg_relationshipupdate.New(t_PropertyEndPoint,
                                                     t_RezoningJobid,
                                                     t_PropertyObjectId);
      
        --Set Property Exists checkbox to skip the property creation screen.
        api.pkg_columnupdate.SetValue(t_RezoningJobid,
                                      'PropertyExists',
                                      'Y');
      
      end if;
    
    end if;
  
  end RezoningPropertyParcelRel;

  /*--------------------------------------------------------------------------------------------
  * This procedure is run on contructor of the parcel to planning Job relationship. It performs
  * the function of relating all parcels on a property to the job.
  *------------------------------------------------------------------------------------------*/

  procedure PlanningPropertyParcelRel(a_objectid udt_Id, a_asofdate date) is
  
    t_AddressLine1             varchar2(400);
    t_NewRelId                 udt_id;
    t_ObjectDefName            varchar2(30);
    t_OwnerName                varchar2(400);
    t_OwnerCity                varchar2(400);
    t_OwnerState               varchar2(400);
    t_OwnerZipCode             varchar2(400);
    t_OwnerZipExtension        varchar2(400);
    t_OwnerPhoneNumber         varchar2(400);
    t_OwnerEmailAddress        varchar2(400);
    t_Parcelrelcount           number;
    t_PropertyObjectId         udt_id;
    t_ParcelObjectId           udt_id;
    t_PlanningJobid            udt_id;
    t_PropertyEndPoint         udt_id;
    t_PropertyParcelEndPointId udt_id;
    t_PlanningParcelEndpointId udt_id;
  
  begin
  
    --Get Planning JobId
    select rj.FromObjectId
      into t_PlanningJobid
      from api.relationships rj
      join api.jobs j
        on rj.FromObjectId = j.JobId
     where rj.RelationshipId = a_objectid;
  
    t_ObjectDefName            := api.pkg_columnquery.Value(t_PlanningJobid,
                                                            'ObjectDefName');
    t_PropertyEndPoint         := api.pkg_configquery.EndPointIdForName(t_ObjectDefName,
                                                                        'Property');
    t_PropertyParcelEndPointId := api.pkg_configquery.EndPointIdForName('o_Parcel',
                                                                        'Property');
    t_PlanningParcelEndpointId := api.pkg_configquery.EndPointIdForName(t_ObjectDefName,
                                                                        'Parcel');
  
    --Get count of parcels directly related to the planning job.      
    select count(1)
      into t_Parcelrelcount
      from api.relationships r
     where r.FromObjectId = t_PlanningJobid
       and r.EndPointId = t_PlanningParcelEndpointId;
  
    --Find Parcel objectid
    select r.ToObjectId
      into t_ParcelObjectId
      from api.relationships r
     where r.RelationshipId = a_objectid
       and r.EndPointId = t_PlanningParcelEndpointId;
  
    --Get the parcel owner information.
    begin
      select o.OwnerName,
             o.AddressLine1,
             o.AddressCity,
             o.State,
             o.ZipCode,
             o.ZipExtension,
             o.PhoneNumber,
             o.EmailAddress
        into t_OwnerName,
             t_AddressLine1,
             t_OwnerCity,
             t_OwnerState,
             t_OwnerZipCode,
             t_OwnerZipExtension,
             t_OwnerPhoneNumber,
             t_OwnerEmailAddress
        from query.o_owner o
       where o.ObjectId =
             (select max(o2.objectid)
                from query.r_ownerparcel op
                join query.o_owner o2
                  on o2.ObjectId = op.OwnerId
               where op.ParcelId = t_ParcelObjectId);
    
    exception
      when no_data_found then
        null;
    end;
    
    if api.pkg_columnquery.Value(t_PlanningJobid, 'OwnerNameStored') is null  then
      --Default the Owner details on the Application.
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'OwnerNameStored',
                                    t_OwnerName);
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'OwnerMailingAddressStored',
                                    t_AddressLine1);
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'OwnerCityStored',
                                    t_OwnerCity);
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'OwnerStateStored',
                                    t_OwnerState);
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'OwnerZipCodeStored',
                                    t_OwnerZipCode);
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'OwnerZipExtensionStored',
                                    t_OwnerZipExtension);
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'OwnerPhoneNumberStored',
                                    t_OwnerPhoneNumber);
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'OwnerEmailAddressStored',
                                    t_OwnerEmailAddress);
                                  
    end if;                              
  
    --This should only be done if there is only one parcel. So as to not overwrite the Users addition of parcels.  
    if t_Parcelrelcount != 1
    
     then
      null;
    
    else
    
      --Find the most recently created property's objectid
      select max(r.ToObjectId)
        into t_PropertyObjectId
        from api.relationships r
       where r.FromObjectId = t_ParcelObjectId
         and r.EndPointId = t_PropertyParcelEndPointId;
    
      if t_PropertyObjectId is not null then
      
        --Loop Through all parcels related to the property and relate them to the rezoning job.
        for c in (select pp.ParcelObjectId
                    from query.r_propertyparcel pp
                   where pp.PropertyObjectId = t_PropertyObjectId
                     and pp.ParcelObjectId != t_ParcelObjectId) loop
        
          t_NewRelId := api.pkg_relationshipupdate.New(t_PlanningParcelEndpointId,
                                                       t_PlanningJobid,
                                                       c.ParcelObjectId);
        
        end loop;
      
        --Relate the existing property to the rezoning job
        t_NewRelId := api.pkg_relationshipupdate.New(t_PropertyEndPoint,
                                                     t_PlanningJobid,
                                                     t_PropertyObjectId);
      
        --Set Property Exists checkbox to skip the property creation screen.
        api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                      'PropertyExists',
                                      'Y');
      
      end if;
    
    end if;
  
  end PlanningPropertyParcelRel;

  /*---------------------------------------------------------------------------
  * PlanningPropertyCreation() --PUBLIC
  *---------------------------------------------------------------------------*/
  procedure PlanningPropertyCreation(a_objectid udt_id, a_asofdate date) is
  
    t_NewRelId                 udt_id;
    t_NewPropertyId            udt_id;
    t_ObjectDefName            varchar2(30);
    t_PropertyDefId            udt_id := api.pkg_configquery.ObjectDefIdForName('o_Property');
    t_ParcelEndpointId         udt_id := api.pkg_configquery.EndPointIdForName('o_Property',
                                                                               'Parcel');
    t_PlanningParcelEndpointId udt_id;
    t_PropertyEndPoint         udt_id;
    t_PlanningJobid            udt_id;
  
  begin
  
    t_PlanningJobid            := a_objectid;
    t_ObjectDefName            := api.pkg_columnquery.Value(a_objectid,
                                                            'ObjectDefName');
    t_PropertyEndPoint         := api.pkg_configquery.EndPointIdForName(t_ObjectDefName,
                                                                        'Property');
    t_PlanningParcelEndpointId := api.pkg_configquery.EndPointIdForName(t_ObjectDefName,
                                                                        'Parcel');
  
    if api.pkg_columnquery.Value(a_objectid, 'PropertyDoesNotExist') = 'Y' then
      --Create the New property object to relate to the parcels  
      t_NewPropertyId := api.pkg_objectupdate.New(t_PropertyDefId);
    
      --Relate the parcels from the planning job to the property
      for c in (select pj.ToObjectId ParcelObjectId
                  from api.relationships pj
                 where pj.EndPointId = t_PlanningParcelEndpointId
                   and pj.FromObjectId = t_PlanningJobid) loop
      
        t_NewRelId := api.pkg_relationshipupdate.New(t_ParcelEndpointId,
                                                     t_NewPropertyId,
                                                     c.ParcelObjectId);
      
      end loop;
    
      --Relate the new property to the planning job
      t_NewRelId := api.pkg_relationshipupdate.New(t_PropertyEndPoint,
                                                   t_PlanningJobid,
                                                   t_NewPropertyId);
    
      --Default the property details based on the planning job information
      api.pkg_columnupdate.SetValue(t_NewPropertyId,
                                    'PropertyName',
                                    api.pkg_columnquery.Value(t_PlanningJobid,
                                                              'MarketingName'));
    
      api.pkg_columnupdate.SetValue(t_NewPropertyId,
                                    'PropertyDescription',
                                    api.pkg_columnquery.Value(t_PlanningJobid,
                                                              'PDescription'));
    
      --Set property does not exist checkbox to no and the propert exists check box to yes
      api.pkg_columnupdate.SetValue(t_PlanningJobid,
                                    'PropertyDoesNotExist',
                                    'N');
      api.pkg_columnupdate.SetValue(t_PlanningJobid, 'PropertyExists', 'Y');
    
    end if;
  
  end PlanningPropertyCreation;

  /*---------------------------------------------------------------------------
  * PlanningPropertyUpdate() --PUBLIC
  *---------------------------------------------------------------------------*/
  procedure PlanningPropertyUpdate(a_objectid udt_id, a_asofdate date) is
  
    t_NewRelId                 udt_id;
    t_ParcelObjectId           udt_id;
    t_PropertyId               udt_id;
    t_PropertyParcelEndpointId udt_id;
    t_PropertyEndPointId       udt_id;
    t_PlanningParcelEndpointId udt_id;
    t_PlanningJobid            udt_id;
    t_OldRelId                 udt_id;
    t_ObjectDefName            varchar2(30);
  
  begin
  
    --Get Planning JobId
    select rj.FromObjectId
      into t_PlanningJobid
      from api.relationships rj
      join api.jobs j
        on rj.FromObjectId = j.JobId
     where rj.RelationshipId = a_objectid;
  
    t_ObjectDefName            := api.pkg_columnquery.Value(t_PlanningJobid,
                                                            'ObjectDefName');
    t_PropertyEndPointId       := api.pkg_configquery.EndPointIdForName(t_ObjectDefName,
                                                                        'Property');
    t_PropertyParcelEndpointId := api.pkg_configquery.EndPointIdForName('o_Property',
                                                                        'Parcel');
    t_PlanningParcelEndpointId := api.pkg_configquery.EndPointIdForName(t_ObjectDefName,
                                                                        'Parcel');
  
    --Get the Parcel Id
    select rj.ToObjectId
      into t_ParcelObjectId
      from api.relationships rj
     where rj.FromObjectId = t_PlanningJobid
       and rj.RelationshipId = a_objectid;
  
    if api.pkg_columnquery.Value(t_PlanningJobid, 'ReviewApplication') = 'Y' then
    
      --Find the Propertyid
    
      select pj.ToObjectId
        into t_PropertyId
        from api.relationships pj
       where pj.EndPointId = t_PropertyEndPointId
         and pj.FromObjectId = t_PlanningJobid;
    
      --Find out if the Parcel is already related to the property
      begin
        select pp.RelationshipId
          into t_OldRelId
          from api.relationships pp
         where pp.EndpointId = t_PropertyParcelEndpointId
           and pp.FromObjectId = t_PropertyId
           and pp.ToObjectId = t_ParcelObjectId;
      
      exception
        when no_data_found then
          null;
      end;
    
      if t_OldRelId is null then
        --Relate the new parcel to the Property
        t_NewRelId := api.pkg_relationshipupdate.New(t_PropertyParcelEndpointId,
                                                     t_PropertyId,
                                                     t_ParcelObjectId);
      
      else
        --When the relationship exists remove it From the Property
        api.pkg_relationshipupdate.Remove(t_OldRelId, sysdate);
      end if;
    
    end if;
  
  end PlanningPropertyUpdate;

  /*---------------------------------------------------------------------------
  * RezoningPropertyCreation() --PUBLIC
  *---------------------------------------------------------------------------*/
  /*procedure RezoningPropertyCreation(
              a_objectid    udt_id,
              a_asofdate    date
              )is
  
  t_NewRelId             udt_id;            
  t_NewPropertyId        udt_id;
  t_PropertyDefId        udt_id := api.pkg_configquery.ObjectDefIdForName('o_Property');
  t_ParcelEndpointId     udt_id := api.pkg_configquery.EndPointIdForName('o_Property','Parcel');
  t_PropertyEndPoint     udt_id := api.pkg_configquery.EndPointIdForName('j_Rezoning', 'Property');
  t_RezoningJobid        udt_id;
  
  begin
  
  t_RezoningJobid := a_objectid;
  
  if api.pkg_columnquery.Value(a_objectid,'PropertyDoesNotExist') = 'Y'
    then 
  --Create the New property object to relate to the parcels  
  t_NewPropertyId := api.pkg_objectupdate.New(t_PropertyDefId); 
  
  --Relate the parcels from the rezoning job to the property
  for c in (select rjp.ParcelObjectId
              from query.r_rezoningparcelstored rjp
            where rjp.RezoningJobId = t_RezoningJobid
            ) loop
            
    t_NewRelId := api.pkg_relationshipupdate.New(t_ParcelEndpointId,t_NewPropertyId,c.ParcelObjectId);  
  
  end loop;
  
  --Relate the new property to the Rezoning job
  t_NewRelId := api.pkg_relationshipupdate.New(t_PropertyEndPoint,t_RezoningJobid,t_NewPropertyId);
  
  --Default the property details based on the rezoning job information
  api.pkg_columnupdate.SetValue(t_NewPropertyId,
                                'PropertyName',
                                api.pkg_columnquery.Value(t_RezoningJobid,'MarketingName'));
  
  api.pkg_columnupdate.SetValue(t_NewPropertyId,
                                'PropertyDescription',
                                api.pkg_columnquery.Value(t_RezoningJobid,'RezoningDescription')); 
                                
  --Set property does not exist checkbox to no and the propert exists check box to yes
  api.pkg_columnupdate.SetValue(t_RezoningJobid, 'PropertyDoesNotExist', 'N');    
  api.pkg_columnupdate.SetValue(t_RezoningJobid, 'PropertyExists', 'Y'); 
                                  
  end if;                                                        
  
  end;
  
  /*---------------------------------------------------------------------------
  * RezoningPropertyUpdate() --PUBLIC
  *---------------------------------------------------------------------------*/
  /* procedure RezoningPropertyUpdate(
              a_objectid    udt_id,
              a_asofdate    date
              )is
  
  t_NewRelId                    udt_id;  
  t_ParcelObjectId              udt_id;          
  t_PropertyId                  udt_id;
  t_PropertyParcelEndpointId    udt_id := api.pkg_configquery.EndPointIdForName('o_Property','Parcel');
  t_RezoningPropertyEndPointId  udt_id := api.pkg_configquery.EndPointIdForName('j_Rezoning', 'Property');
  t_RezoningParcelEndpointId    udt_id := api.pkg_configquery.EndPointIdForName('j_Rezoning', 'Parcel');
  t_RezoningJobid               udt_id;
  t_OldRelId                    udt_id;
  
  begin
  
    --Get Rezoning JobId
    select rj.FromObjectId ,
           rj.ToObjectId
           into 
           t_RezoningJobid,
           t_ParcelObjectId
      from api.relationships rj
    where rj.EndPointId = t_RezoningParcelEndpointId
      and rj.RelationshipId = a_objectid;
      
    if api.pkg_columnquery.Value(t_RezoningJobid,'ReviewApplication') ='Y' then
      --Find the Propertyid
        
      select pj.ToObjectId
             into
             t_PropertyId
        from api.relationships pj
      where pj.EndPointId = t_RezoningPropertyEndPointId
        and pj.FromObjectId = t_RezoningJobid;
      
      --Find out if the Parcel is already related to the property
      begin  
        select pp.RelationshipId
               into
               t_OldRelId
          from api.relationships pp
        where pp.EndpointId = t_PropertyParcelEndpointId
          and pp.FromObjectId = t_PropertyId
          and pp.ToObjectId = t_ParcelObjectId;
        
      exception when no_data_found then
        null; 
      end;                       
      
      if t_OldRelId is null then
        --Relate the new parcel to the Property
        t_NewRelId := api.pkg_relationshipupdate.New(t_PropertyParcelEndpointId,t_PropertyId,t_ParcelObjectId);
        
      else
        --When the relationship exists remove it From the Property
        api.pkg_relationshipupdate.Remove(t_OldRelId,sysdate);
      end if; 
        
    end if;
                                                                            
    
  end;*/

  /*---------------------------------------------------------------------------
  * UserMileageLog() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure UserMileageLog(a_objectid   udt_id,
                           a_EndpointId udt_id,
                           a_objectlist out api.udt_objectlist) is
  
  begin
  
    select api.udt_object(l.userlogid) bulk collect
      into a_objectlist
      from query.l_mileagelog l
     where l.UserId = api.pkg_securityquery.EffectiveUserId();
  
  end UserMileageLog;

  /*---------------------------------------------------------------------------
  * UserTimeLog() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure UserTimeLog(a_objectid   udt_id,
                        a_EndpointId udt_id,
                        a_objectlist out api.udt_objectlist) is
  
  begin
  
    select api.udt_object(l.userlogid) bulk collect
      into a_objectlist
      from query.l_timelog l
     where l.UserId = api.pkg_securityquery.EffectiveUserId();
  
  end UserTimeLog;

  /*---------------------------------------------------------------------------
  * UserPerformBuilding() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformBuilding(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist) is
  
    t_SearchingUserId number(9);
    t_ProcessTypeId   number(9);
  
    --set user list to search based on entered user criteria or on the current user when no user criteria is entered.
    cursor c_CurrentUser is
      select users.UserId
        from api.users users
       where users.userid = t_SearchingUserId;
  begin
    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();
    a_objectlist      := api.udt_objectlist();
    t_ProcessTypeId   := api.pkg_configquery.ObjectDefIdForName('p_PerformBuildingInspection');


    --Return todo list.
    for p in (select pr.processid
                from api.processassignments pa
                join api.processes pr
                  on pr.processid = pa.processid
                join api.processtypes pt
                  on pt.processtypeid = pr.processtypeid
                join api.jobs jobs
                  on jobs.jobid = pr.jobid
               where pa.UserId = t_SearchingUserId
                 and pr.ProcessTypeId = t_ProcessTypeId
                 and pr.Outcome is null) loop
      a_objectlist.extend(1);
      a_objectlist(a_objectlist.count) := api.udt_object(p.processid);
    end loop; --to get User's todo list
  
  end UserPerformBuilding;

  /*---------------------------------------------------------------------------
  * UserPerformElectrical() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformElectrical(a_objectid   udt_id,
                                  a_EndpointId udt_id,
                                  a_objectlist out api.udt_objectlist) is
  
    t_SearchingUserId number(9);
    t_ProcessTypeId   number(9);
  
    --set user list to search based on entered user criteria or on the current user when no user criteria is entered.
    cursor c_CurrentUser is
      select users.UserId
        from api.users users
       where users.userid = t_SearchingUserId;
  begin

    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();
    a_objectlist      := api.udt_objectlist();
    t_ProcessTypeId   := api.pkg_configquery.ObjectDefIdForName('p_PerformElectricalInspection');
  
    --Return todo list.
    for p in (select pr.processid
                from api.processassignments pa
                join api.processes pr
                  on pr.processid = pa.processid
                join api.processtypes pt
                  on pt.processtypeid = pr.processtypeid
                join api.jobs jobs
                  on jobs.jobid = pr.jobid
               where pa.UserId = t_SearchingUserId
                 and pr.ProcessTypeId = t_ProcessTypeId
                 and pr.Outcome is null) loop
      a_objectlist.extend(1);
      a_objectlist(a_objectlist.count) := api.udt_object(p.processid);
    end loop; --to get User's todo list
  
  end UserPerformElectrical;

  /*---------------------------------------------------------------------------
  * UserPerformMechanical() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformMechanical(a_objectid   udt_id,
                                  a_EndpointId udt_id,
                                  a_objectlist out api.udt_objectlist) is
  
    t_SearchingUserId number(9);
    t_ProcessTypeId   number(9);
  
    --set user list to search based on entered user criteria or on the current user when no user criteria is entered.
    cursor c_CurrentUser is
      select users.UserId
        from api.users users
       where users.userid = t_SearchingUserId;
  begin

    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();
    a_objectlist      := api.udt_objectlist();
    t_ProcessTypeId   := api.pkg_configquery.ObjectDefIdForName('p_PerformMechanicalInspection');
  
    --Return todo list.
    for p in (select pr.processid
                from api.processassignments pa
                join api.processes pr
                  on pr.processid = pa.processid
                join api.processtypes pt
                  on pt.processtypeid = pr.processtypeid
                join api.jobs jobs
                  on jobs.jobid = pr.jobid
               where pa.UserId = t_SearchingUserId
                 and pr.ProcessTypeId = t_ProcessTypeId
                 and pr.Outcome is null) loop
      a_objectlist.extend(1);
      a_objectlist(a_objectlist.count) := api.udt_object(p.processid);
    end loop; --to get User's todo list
  
  end UserPerformMechanical;

  /*---------------------------------------------------------------------------
  * UserPerformPlumbing() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformPlumbing(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist) is
  
    t_SearchingUserId number(9);
    t_ProcessTypeId   number(9);
  
    --set user list to search based on entered user criteria or on the current user when no user criteria is entered.
    cursor c_CurrentUser is
      select users.UserId
        from api.users users
       where users.userid = t_SearchingUserId;
  begin

    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();
    a_objectlist      := api.udt_objectlist();
    t_ProcessTypeId   := api.pkg_configquery.ObjectDefIdForName('p_PerformPlumbingInspection');
  
    --Return todo list.
    for p in (select pr.processid
                from api.processassignments pa
                join api.processes pr
                  on pr.processid = pa.processid
                join api.processtypes pt
                  on pt.processtypeid = pr.processtypeid
                join api.jobs jobs
                  on jobs.jobid = pr.jobid
               where pa.UserId = t_SearchingUserId
                 and pr.ProcessTypeId = t_ProcessTypeId
                 and pr.Outcome is null) loop
      a_objectlist.extend(1);
      a_objectlist(a_objectlist.count) := api.udt_object(p.processid);
    end loop; --to get User's todo list
  
  end UserPerformPlumbing;

  /*---------------------------------------------------------------------------
  * ProjectHierarchy() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure ProjectHierarchy(a_ObjectId         udt_Id,
                             a_EndPointId       udt_Id,
                             a_RelatedObjectIds out api.udt_ObjectList) is
    t_ParentBuildingPermit udt_Id := api.pkg_configquery.endpointidforname('o_ProjectView',
                                                                           'ParentBuildingPermit');
    t_ChildReview          udt_Id := api.pkg_configquery.endpointidforname('o_ProjectView',
                                                                           'ChildReview');
    t_ProjectViewDefId      udt_Id := api.pkg_configquery.objectdefidforname('o_ProjectView');
    t_ProcessViewId         udt_Id;
    t_MasterProjectEPId     udt_Id;
    t_MasterProjectObjectId udt_Id;
    t_DefName               varchar2(200);
    t_MasterProjectDefId    udt_Id := api.pkg_configquery.objectdefidforname('o_MasterProject');
    t_TopJob                udt_Id;
    t_JobIds                api.udt_ObjectList;
  
  begin
    a_RelatedObjectIds := api.udt_ObjectList();
    t_JobIds           := api.udt_ObjectList();

    --find the highest job
    t_TopJob := GetMasterProject(a_ObjectId);
    
    --get everything below the top job
    GetHierarchy(t_TopJob, t_JobIds);

    --add the top job to the array
    t_JobIds.extend(1);
    t_JobIds(t_JobIds.Count) := api.udt_Object(t_TopJob);

--dbms_output.put_line('t_JobIds.Count: ' ||t_JobIds.Count);
    for a in 1.. t_JobIds.Count loop
      begin
--dbms_output.put_line('JobId: ' || t_JobIds(a).ObjectId);
        select objectid
          into t_ProcessViewId
          from api.registeredexternalobjects reo
         where reo.linkvalue = t_JobIds(a).ObjectId
           and reo.ObjectDefId = t_ProjectViewDefId;
      exception
        when no_data_found then
          --register it
          api.pkg_objectupdate.RegisterExternalObject('o_ProjectView', t_JobIds(a).ObjectId);
          begin
            select objectid
              into t_ProcessViewId
              from api.registeredexternalobjects reo
             where reo.linkvalue = t_JobIds(a).ObjectId
               and reo.ObjectDefId = t_ProjectViewDefId;
          exception
            when no_data_found then
              null;
          end;
      end;
      dbms_output.put_line(t_ProcessViewId);
      --add the Master Project Project view object to the array
      a_RelatedObjectIds.extend(1);
      a_RelatedObjectIds(a_RelatedObjectIds.Count) := api.udt_Object(t_ProcessViewId);
    end loop;
    
   
  end ProjectHierarchy;

  /*---------------------------------------------------------------------------
  * GetMasterProject() -- PUBLIC
  *-------------------------------------------------------------------------*/
  function GetMasterProject(a_JobId udt_Id) return udt_Id is
    t_ParentJobId   udt_Id;
    t_TopJob        udt_Id;
  begin
    --set the top job as myself
    t_TopJob := a_JobId;
    --see if there is anything higher
    select parentjobid
      into t_ParentJobId
      from bcpdata.projectview
     where jobid = a_JobId
       and parentjobid is not null;
    --if we get to here there is something higher so we find out if there is anything even higher
    t_TopJob := GetMasterProject(t_ParentJobId);
    --return the one that is the highest
    return t_TopJob;
  --if we don't find one higher return myself as the highest job
  exception when no_data_found then
    return t_TopJob;
  end;

  /*---------------------------------------------------------------------------
  * GetHierarchy() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure GetHierarchy(
    a_TopJob  udt_Id, 
    a_Objects in out api.udt_ObjectList
    ) is
    t_TempObjects       api.udt_ObjectList;
    t_ReviewJobDefId    number(9);
  begin
   
    t_ReviewJobDefId := api.pkg_configquery.ObjectDefIdForName('j_Review');
  
    for i in (select pj.jobid
                from bcpdata.projectview pj
                join api.jobs j on j.JobId = pj.jobid
               where pj.parentjobid = a_TopJob
                 and j.jobtypeid <> t_ReviewJobDefId) loop
      t_TempObjects := api.udt_ObjectList();
      a_Objects.Extend(1);
      a_Objects(a_Objects.Count) := api.udt_Object(i.jobid);
      
      
               
      select api.udt_Object(jobid)
        bulk collect into t_TempObjects
        from api.jobs
        where jobtypeid <> t_ReviewJobDefId /* Remove review jobs from project tab */
        start with jobid = i.JobId
        connect by prior jobid = parentjobid;
        
      extension.pkg_collectionutils.Append(a_Objects, t_TempObjects);
    end loop;
 
  end GetHierarchy;

  /*---------------------------------------------------------------------------
  * GetHierarchyOneLevel() -- PUBLIC
  *-------------------------------------------------------------------------*/
/*  procedure GetHierarchyOneLevel(a_TopJob udt_Id, 
                                 a_Objects in out api.udt_ObjectList) is
    t_TempObjects       api.udt_ObjectList;
  begin
    t_TempObjects := api.udt_ObjectList();

    select api.udt_Object(jobid)
      bulk collect into t_TempObjects
      from bcpdata.projectview
     where parentjobid = a_TopJob
     order by jobid;

    extension.pkg_collectionutils.Append(a_Objects, t_TempObjects);
  end;
*/
  /*---------------------------------------------------------------------------
  * HierarchySort() -- PUBLIC
  *-------------------------------------------------------------------------*/
/*  function HierarchySort(a_JobId udt_Id) return number is
    t_JobId        udt_Id;
    t_TopJob       udt_Id;
    t_DefId        udt_Id := api.pkg_ConfigQuery.ObjectDefIdforName('o_ProjectView');
    t_Jobs         api.udt_ObjectList;
    t_MaxJobId     udt_Id;
    --t_Count        number := 0;
  begin
--return 1;
    t_Jobs := api.udt_ObjectList();

    select distinct getmasterproject(jobid), jobid
      into t_TopJob, t_JobId
      from bcpdata.Projectview
     where JobId = a_JobId;

    if t_TopJob = t_JobId then
      --t_TopJob := t_JobId;
      g_Count := 1;
      return g_Count;
    else

      GetHierarchyOneLevel(t_TopJob, t_Jobs);

      for a in (select api.pkg_ColumnQuery.DateValue(x.ObjectId, 'CreatedDate') CreatedDate, x.objectid JobId
                  from table(t_Jobs) x) loop
        dbms_output.put_line('JobId: ' || a.jobid || ' and ' || a.createddate);

        if t_JobId = a.JobId then
          g_Count := g_Count + 1;
          return g_Count;
        else
          g_Count := HierarchySort(a.JobId);
          return g_Count;
        end if;

      end loop;
      
    end if;
  end;
*/
/*---------------------------------------------------*/

  /*---------------------------------------------------------------------------
  * SubPermitHierarchy() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure SubPermitHierarchy(a_ObjectId         udt_Id,
                               a_EndPointId       udt_Id,
                               a_RelatedObjectIds out api.udt_ObjectList) is
    t_ParentBuildingPermit udt_Id := api.pkg_configquery.endpointidforname('o_ProjectView',
                                                                           'ParentBuildingPermit');
    t_ChildReview          udt_Id := api.pkg_configquery.endpointidforname('o_ProjectView',
                                                                           'ChildReview');
    t_ProjectViewDefId      udt_Id := api.pkg_configquery.objectdefidforname('o_ProjectView');
    t_ProcessViewId         udt_Id;
    t_MasterProjectEPId     udt_Id;
    t_MasterProjectObjectId udt_Id;
    t_DefName               varchar2(200);
    t_MasterProjectDefId    udt_Id := api.pkg_configquery.objectdefidforname('o_MasterProject');
    t_TopJob                udt_Id;
    t_JobIds                api.udt_ObjectList;
    t_MasterId              udt_Id;
  
  begin
    a_RelatedObjectIds := api.udt_ObjectList();
    t_JobIds           := api.udt_ObjectList();

    --find the highest job
    t_MasterId := GetMasterProject(a_ObjectId);
    t_JobIds.extend(1);
    t_JobIds(t_JobIds.Count) := api.udt_Object(t_MasterId);

    t_TopJob := api.pkg_columnquery.NumericValue(a_ObjectId, 'PermitApplicationObjectId');
    --add the top job to the array
    if t_TopJob is not null then
      t_JobIds.extend(1);
      t_JobIds(t_JobIds.Count) := api.udt_Object(t_TopJob);   
    --get everything below the top job
      GetHierarchy(t_TopJob, t_JobIds);
    else
      t_JobIds.extend(1);
      t_JobIds(t_JobIds.Count) := api.udt_Object(a_ObjectId);
    end if;

    for a in 1.. t_JobIds.Count loop
      begin
        select objectid
          into t_ProcessViewId
          from api.registeredexternalobjects reo
         where reo.linkvalue = t_JobIds(a).ObjectId
           and reo.ObjectDefId = t_ProjectViewDefId;
      exception
        when no_data_found then
          --register it
          api.pkg_objectupdate.RegisterExternalObject('o_ProjectView', t_JobIds(a).ObjectId);
          begin
            select objectid
              into t_ProcessViewId
              from api.registeredexternalobjects reo
             where reo.linkvalue = t_JobIds(a).ObjectId
               and reo.ObjectDefId = t_ProjectViewDefId;
          exception
            when no_data_found then
              null;
          end;
      end;
      dbms_output.put_line(t_ProcessViewId);
      --add the Master Project Project view object to the array
      a_RelatedObjectIds.extend(1);
      a_RelatedObjectIds(a_RelatedObjectIds.Count) := api.udt_Object(t_ProcessViewId);
    end loop;
  end SubPermitHierarchy;

procedure MasterProjectProperty(a_objectid   udt_id,
                                      a_EndpointId udt_id,
                                      a_objectlist out api.udt_objectlist) is
  
    t_ObjectList                  api.udt_objectlist;
  
  begin
    a_ObjectList := api.udt_objectList();
  
    select api.udt_object(o.ObjectId) bulk collect
      into a_ObjectList
      from api.relationships r
      join api.jobs j on j.JobId = r.ToObjectId
      join api.relationships r2 on r2.FromObjectId = j.JobId
      join api.objects o on o.ObjectId = r2.ToObjectId
      join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
     where r.FromObjectId = a_ObjectId
       and od.Name = 'o_Property';
  
  end;


  /*---------------------------------------------------------------------------
  * GetViolations() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure GetViolations(a_ObjectId         udt_Id,
                          a_EndPointId       udt_Id,
                          a_RelatedObjectIds out api.udt_ObjectList) is

    t_ViolationJobId                         udt_Id;
    t_ViolationViolationsEPId                udt_Id := api.pkg_configquery.EndPointIdForName('j_Violation', 'Violation');
    t_PerfViolInvViolationsEPId              udt_Id := api.pkg_configquery.EndPointIdForName('p_PerformViolationInspection', 'Violations');

  begin
    select JobId
      into t_ViolationJobId
      from api.Processes p
     where p.ProcessId = a_ObjectId;

    select api.udt_Object(r.ToObjectId)
      bulk collect into a_RelatedObjectIds
      from api.relationships r
      join query.o_violations v on v.ObjectId = r.ToObjectId
     where r.fromobjectid = t_ViolationJobId
       and r.endpointid = t_ViolationViolationsEPId
       and v.IsResolved = 'N'
       and r.toobjectid not in (select re.ToObjectId
                                  from api.Relationships re
                                 where re.FromObjectId = a_ObjectId
                                   and re.EndPointId = t_PerfViolInvViolationsEPId);
    
  end;

 /*---------------------------------------------------------------------------
  * UserPerformViolation() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformViolation(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist) is
  
    t_SearchingUserId number(9);
    t_ProcessTypeId   number(9);
  
    --set user list to search based on entered user criteria or on the current user when no user criteria is entered.
    cursor c_CurrentUser is
      select users.UserId
        from api.users users
       where users.userid = t_SearchingUserId;
  begin
  
    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();
    a_objectlist      := api.udt_objectlist();
    t_ProcessTypeId   := api.pkg_configquery.ObjectDefIdForName('p_PerformViolationInspection');
  
    --Return todo list.
    for p in (select pr.processid
                from api.processassignments pa
                join api.processes pr
                  on pr.processid = pa.processid
                join api.processtypes pt
                  on pt.processtypeid = pr.processtypeid
                join api.jobs jobs
                  on jobs.jobid = pr.jobid
               where pa.UserId = t_SearchingUserId
                 and pr.ProcessTypeId = t_ProcessTypeId
                 and pr.Outcome is null) loop
      a_objectlist.extend(1);
      a_objectlist(a_objectlist.count) := api.udt_object(p.processid);
    end loop; --to get User's todo list
  
  end UserPerformViolation;

  /*---------------------------------------------------------------------------
  * UserPerformInvestigation() -- PUBLIC -Generates the User To Do List of Insepctions on
  * the inspectors website
  *-------------------------------------------------------------------------*/
  procedure UserPerformInvestigation(a_objectid   udt_id,
                                a_EndpointId udt_id,
                                a_objectlist out api.udt_objectlist) is
  
    t_SearchingUserId number(9);
    t_ProcessTypeId   number(9);
  
    --set user list to search based on entered user criteria or on the current user when no user criteria is entered.
    cursor c_CurrentUser is
      select users.UserId
        from api.users users
       where users.userid = t_SearchingUserId;
  begin
  
    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();
    a_objectlist      := api.udt_objectlist();
    t_ProcessTypeId   := api.pkg_configquery.ObjectDefIdForName('p_PerformInvestigation');
  
    --Return todo list.
    for p in (select pr.processid
                from api.processassignments pa
                join api.processes pr
                  on pr.processid = pa.processid
                join api.processtypes pt
                  on pt.processtypeid = pr.processtypeid
                join api.jobs jobs
                  on jobs.jobid = pr.jobid
               where pa.UserId = t_SearchingUserId
                 and pr.ProcessTypeId = t_ProcessTypeId
                 and pr.Outcome is null) loop
      a_objectlist.extend(1);
      a_objectlist(a_objectlist.count) := api.udt_object(p.processid);
    end loop; --to get User's todo list
  
  end UserPerformInvestigation;
  
  /*---------------------------------------------------------------------------
  * AddressRelateFloodPlain() -- PUBLIC - Relates a flood zone to a Permit
  * if the permit is on a flood zone
  *-------------------------------------------------------------------------*/
  procedure AddressRelateFloodZone
  (a_RelationshipId    udt_id,
   a_AsOfDate         date
  ) is
   
    t_AddressEndPointId        udt_id;
    t_AddressObjectId          udt_id;
    t_BuildingPermitObjectId   udt_id;
    t_FloodZoneEndPointId      udt_id;
    t_FloodZoneId              udt_id;
    t_FloodZoneObjectId        udt_id;
    t_RelationshipId           udt_id;      
                                
  begin
    
    null;
    -- Dalainya - Mar 20, 2015
    -- Removed the code below during NJ Development, bcpdata.ext_floodplains does not exist
    -- Not sure if this procedure is needed in ABC
    
    /*t_AddressEndPointId   := api.pkg_configquery.EndPointIdForName('j_BuildingPermit', 'BuildingPermitAddress');
    t_FloodZoneEndPointId := api.pkg_configquery.EndPointIdForName('j_BuildingPermit', 'FloodZone');
    
    select 
    max(alo.objectid),
    max(fp.flood01_)
    into
    t_AddressObjectId,
    t_FloodZoneId
      from bcpdata.Addresslinkobjectid alo 
      join bcpdata.ext_addrattr at 
        on at.adr_id = alo.linkvalue
      join bcpdata.ext_floodplains fp 
        on fp.firm_mapno = at.firm_mapno
       and fp.firm_panel = at.firm_panel
       and fp.firm_effdt = at.firm_effdt
       and fp.firm_zone  = at.firm_zone
     where alo.objectid = (select r.ToObjectId
                             from api.relationships r 
                           where r.RelationshipId = a_RelationshipId
                             and r.EndPointId = t_AddressEndPointId);
                             
    select r.FromObjectId
    into 
    t_BuildingPermitObjectId
      from api.relationships r 
     where r.RelationshipId = a_RelationshipId
      and r.EndPointId = t_AddressEndPointId;
                               
    select re.ObjectId
    into 
    t_FloodZoneObjectId
      from bcpdata.floodzones fz 
      join api.registeredexternalobjects re on re.LinkValue = to_char(fz.floodzoneid)
      where fz.floodzoneid = t_FloodZoneId;
      
    t_RelationshipId := api.pkg_relationshipupdate.New(t_FloodZoneEndPointId,t_BuildingPermitObjectId,t_FloodZoneObjectId);      */                   
      
  end AddressRelateFloodZone;       
 
/*---------------------------------------------------------------------------
 * DefaultPermitOwner() -- PUBLIC - Defaults the owner on the permit
 *-------------------------------------------------------------------------*/
  procedure DefaultPermitOwner(a_RelationshipId    udt_id,
                                a_AsOfDate          date) is
  begin
    --look for the owner given the relationship between the job and address object
    --owner is related to parcel which is related to address
    --  Job --> address --> parcel --> owner
    for c in (select ow.OwnerName    OwnerName,
                     ow.AddressLine1 AddressLine1,
                     ow.AddressCity  AddressCity,
                     ow.ZipCode      ZipCode,
                     ow.ZipExtension ZipExtension,
                     ow.PhoneNumber  PhoneNumber,
                     ow.State        State,
                     ow.EmailAddress EmailAddress,
                     ba.FromObjectId PermitJobId
                from api.Relationships ba
                join query.r_ParcelAddress pa on ba.ToObjectId = pa.AddressId 
                join query.r_OwnerParcel op on op.ParcelId = pa.ParcelId
                join query.o_Owner ow on ow.ObjectId = op.OwnerId 
               where ba.RelationshipId = a_RelationshipId and rownum < 2
               order by ba.FromObjectId ) loop
      api.pkg_columnUpdate.SetValue(c.PermitJobId, 'ApplicantNameStored', c.OwnerName);
      api.pkg_columnUpdate.SetValue(c.PermitJobId, 'ApplicantMailingAddressStored', c.AddressLine1);
      api.pkg_columnUpdate.SetValue(c.PermitJobId, 'ApplicantCityStored', c.AddressCity);
      api.pkg_columnUpdate.SetValue(c.PermitJobId, 'ApplicantZipCodeStored', c.ZipCode);
      api.pkg_columnUpdate.SetValue(c.PermitJobId, 'ApplicantZipExtensionStored', c.ZipExtension);
      api.pkg_columnUpdate.SetValue(c.PermitJobId, 'ApplicantPhoneNumberStored', c.PhoneNumber);
      api.pkg_columnUpdate.SetValue(c.PermitJobId, 'ApplicantStateStored', c.State);
      api.pkg_columnUpdate.SetValue(c.PermitJobId, 'ApplicantEmailAddress', c.EmailAddress);
      
    end loop;                        
  end DefaultPermitOwner;                                    

 /*---------------------------------------------------------------------------
  * ConditionsForJob() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure ConditionsForJob(a_objectid   udt_id,
                            a_EndpointId udt_id,
                            a_objectlist out api.udt_objectlist) is
  t_ObjectDefId      udt_Id;
  t_ParcelEndpointId udt_Id;
  t_ParcelList       udt_IdList;
  t_RelCount         integer;

  begin
    a_ObjectList := api.udt_objectList();
    t_ObjectDefId := api.pkg_columnquery.NumericValue(a_ObjectId, 'ObjectDefId');
    t_ParcelEndpointId := api.pkg_configquery.EndPointIdForName(t_ObjectDefId, 'ParcelIND');

    t_ParcelList := api.pkg_objectquery.RelatedObjects(a_ObjectId, t_ParcelEndpointId);

    for i in 1..t_ParcelList.count() loop
      for c in (select r.ConditionObjectId
                  from query.r_ParcelCondition r
                 where r.ParcelObjectId = t_ParcelList(i)) loop
        select count(rel.RelationshipId)
          into t_RelCount
          from api.relationships rel
         where rel.FromObjectId = a_ObjectId
           and rel.ToObjectId = c.conditionobjectid;

        --If the condition has not been resolve and this job didn't create the condition, add it
        if api.pkg_columnquery.Value(c.conditionobjectid, 'ConditionResolved') != 'Y'
          and t_RelCount < 1 then
          a_objectlist.extend(1);
          a_objectlist(a_objectlist.count) := api.udt_object(c.conditionobjectid);
        end if;
      end loop;
    end loop;

  end ConditionsForJob;

  procedure OtherOpenInvestigations (
    a_objectid   udt_id,
    a_EndpointId udt_id,
    a_objectlist out api.udt_objectlist
  ) is
    t_Address          udt_Id;
    t_Investigation    udt_Id;
    t_Investigations   udt_IdList;
    t_TempList         udt_IdList;--api.udt_ObjectList;

  begin
    a_objectlist := api.udt_ObjectList();
    begin
      select addressobjectid
      into t_Address
      from query.r_ComplaintAddress
      where complaintjobid = a_objectid
      and rownum <= 1;
    exception
      when no_data_found then
        null;
    end; 
    
    t_Investigation := api.pkg_columnquery.numericvalue(a_ObjectId, 'ParentJobId');
    
    select r.JobId
    bulk collect into t_Investigations
    from query.r_InvestigationAddress r
    join api.jobs j on j.jobid = r.JobId
    where r.AddressId = t_Address
    and j.StatusId in (select statusid
                         from api.statuses
                         where tag in ('VIOL', 'UNDINV'))
    and r.jobid != t_Investigation;
    
    a_objectlist.extend(t_Investigations.count);
    for i in 1..t_Investigations.count loop
      a_objectlist(i) := api.udt_Object(t_Investigations(i));
    end loop;
    
  end OtherOpenInvestigations;

  /*---------------------------------------------------------------------------
   * GetPrimaryAddress() - public
   *-------------------------------------------------------------------------*/
  procedure GetPrimaryAddress (
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjectIds out  api.udt_ObjectList
  ) is
    t_AddressIds         udt_IdList;

  begin
    select r.ToObjectId
      bulk collect into t_AddressIds
      from api.relationships r
      where r.FromObjectId = a_ObjectId
        and api.pkg_ColumnQuery.Value(r.RelationshipId, 'IsPrimaryAddress') = 'Y';
    
    a_RelatedObjectIds := extension.pkg_utils.ConvertToObjectList(t_AddressIds);
  end GetPrimaryAddress;

  /*---------------------------------------------------------------------------
   * GetRelatedTimeLogs() - public
   *-------------------------------------------------------------------------*/
  procedure GetRelatedTimeLogs(
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjects out    api.udt_ObjectList
  ) is
    t_ObjectList            udt_IdList;
    
  begin
    select t.ObjectId
      bulk collect into t_ObjectList
      from api.Processes p
      join api.Relationships r on r.FromObjectId = p.ProcessId
      join query.l_TimeLog t on t.ObjectId = r.ToObjectId
     where p.JobId = a_ObjectId;
     
    a_RelatedObjects := extension.pkg_utils.ConvertToObjectList(t_ObjectList);
  end GetRelatedTimeLogs;

  /*---------------------------------------------------------------------------
   * GetAllInspectionTypes() - public
   *-------------------------------------------------------------------------*/
  procedure GetAllInspectionTypes(
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjects out    api.udt_ObjectList
  ) is
    t_ObjectList            udt_IdList;
    
  begin
    select ObjectId
      bulk collect into t_ObjectList
      from query.o_InspectionType;
     
    a_RelatedObjects := extension.pkg_utils.ConvertToObjectList(t_ObjectList);
  end GetAllInspectionTypes;
   
  /*---------------------------------------------------------------------------
   * GetOrderJobViolations() - public
   *-------------------------------------------------------------------------*/
  procedure GetOrderJobViolations(
    a_ObjectId              udt_Id,
    a_EndPointId            udt_Id,
    a_RelatedObjects out    api.udt_ObjectList
  ) is
    t_ObjectList            udt_IdList;
    
  begin
     Select r.ViolationObjectId
      bulk collect into t_ObjectList
      from query.r_CaseFileViolations r
      join query.o_Violations v
        on v.ObjectId = r.ViolationObjectId
     where r.CaseFileJobId = a_ObjectId
       and v.ResolutionDate is null
       and v.OrderDate is null
       and v.NoticeDate is not null;
     
    a_RelatedObjects := extension.pkg_utils.ConvertToObjectList(t_ObjectList);
  end GetOrderJobViolations;
  
   
  /*---------------------------------------------------------------------------
   * GetParcelOwnerAsDefendant() - public
   *  Returns the parcel owner only if he/she is also the defendant.
   *-------------------------------------------------------------------------*/
  procedure GetParcelOwnerAsDefendant (
    a_ObjectId                          udt_Id,
    a_EndPointId                        udt_Id,
    a_RelatedObjects out                api.udt_ObjectList
  ) is
    t_ObjectList                        udt_IdList;
    
  begin
    select rop.OwnerId
      bulk collect into t_ObjectList
      from query.r_LMS_CaseFileParcelAddress rcfa
        join query.r_ParcelAddress rpa
          on rpa.AddressId = rcfa.AddressObjectId
        join query.r_OwnerParcel rop
          on rop.ParcelId = rpa.ParcelId
      where rcfa.CaseFileJobId = a_ObjectId
        and api.pkg_ColumnQuery.Value(rcfa.CaseFileJobId, 'ParcelOwnerIsDefendant') = 'Y';
        
    a_RelatedObjects := extension.pkg_utils.ConvertToObjectList(t_ObjectList);
  end GetParcelOwnerAsDefendant;
  
  
  /*---------------------------------------------------------------------------
   * CopyViolationsToNOV() - public
   *  Creating relationships to Violations from a Case File that have not been 
   *  previously related to an NOV
   *-------------------------------------------------------------------------*/
  procedure CopyViolationsToNOV (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_RelId                             udt_Id;
    t_EndpointId                        udt_Id := api.pkg_configquery.EndPointIdForName('o_LMS_NoticeOfViolation', 'Violations'); 
    
    cursor c_ViolationsToAdd is
      select rNC.NoticeOfViolationObjectId, o.ObjectId
        from query.r_NoticeofViolationCaseFile rNC
          join query.r_CaseFileViolations rCV
            on rCV.CaseFileJobId = rNC.CaseFileJobId
          join query.o_violations o
            on o.ObjectId = rCV.ViolationObjectId
        where rNC.RelationshipId = a_RelationshipId
          and o.ResolutionDate is null
          and o.NoticeDate is null;
          
  begin
    for v in c_ViolationsToAdd loop
      t_RelId := api.pkg_relationshipUpdate.New(t_EndpointId, v.Noticeofviolationobjectid, v.objectid);
    end loop;
  end CopyViolationsToNOV;
  
  
  /*---------------------------------------------------------------------------
   * CopyColumn() - private
   *  Copy a column from the source object to the target object.  There are
   * procedures for strings, dates, and numbers, and an override for each
   * procedure.
   *-------------------------------------------------------------------------*/
  procedure CopyColumn(
    a_SourceObjectId                    udt_Id,
    a_SourceColumnName                  varchar2,
    a_TargetObjectId                    udt_Id,
    a_TargetColumnName                  varchar2 default null,
    a_ForceCopy                         boolean default false
  ) is
    t_TargetColumnName                  varchar2(30);
    
  begin
    if a_TargetColumnName is null then
      t_TargetColumnName := a_SourceColumnName;
    else
      t_TargetColumnName := a_TargetColumnName;
    end if;
    
    if a_ForceCopy or api.pkg_ColumnQuery.Value(a_TargetObjectId, t_TargetColumnName) is null then
      api.pkg_ColumnUpdate.SetValue(a_TargetObjectId, t_TargetColumnName, api.pkg_ColumnQuery.Value(a_SourceObjectId, a_SourceColumnName));
    end if;
  end CopyColumn;
  
  
  procedure CopyColumn(
    a_SourceObjectId                    udt_Id,
    a_SourceColumnName                  varchar2,
    a_TargetObjectId                    udt_Id,
    a_ForceCopy                         boolean
  ) is
  begin
    CopyColumn(a_SourceObjectId, a_SourceColumnName, a_TargetObjectId, a_SourceColumnName, a_ForceCopy);
  end;
  
  
  procedure CopyDateColumn(
    a_SourceObjectId                    udt_Id,
    a_SourceColumnName                  varchar2,
    a_TargetObjectId                    udt_Id,
    a_TargetColumnName                  varchar2 default null,
    a_ForceCopy                         boolean default false
  ) is
    t_TargetColumnName                  varchar2(30);
    
  begin
    if a_TargetColumnName is null then
      t_TargetColumnName := a_SourceColumnName;
    else
      t_TargetColumnName := a_TargetColumnName;
    end if;
    
    if a_ForceCopy or api.pkg_ColumnQuery.DateValue(a_TargetObjectId, t_TargetColumnName) is null then
      api.pkg_ColumnUpdate.SetValue(a_TargetObjectId, t_TargetColumnName, api.pkg_ColumnQuery.DateValue(a_SourceObjectId, a_SourceColumnName));
    end if;
  end CopyDateColumn;
  
  
  procedure CopyDateColumn(
    a_SourceObjectId                    udt_Id,
    a_SourceColumnName                  varchar2,
    a_TargetObjectId                    udt_Id,
    a_ForceCopy                         boolean
  ) is
  begin
    CopyDateColumn(a_SourceObjectId, a_SourceColumnName, a_TargetObjectId, a_SourceColumnName, a_ForceCopy);
  end;
  
  
  procedure CopyNumericColumn(
    a_SourceObjectId                    udt_Id,
    a_SourceColumnName                  varchar2,
    a_TargetObjectId                    udt_Id,
    a_TargetColumnName                  varchar2 default null,
    a_ForceCopy                         boolean default false
  ) is
    t_TargetColumnName                  varchar2(30);
    
  begin
    if a_TargetColumnName is null then
      t_TargetColumnName := a_SourceColumnName;
    else
      t_TargetColumnName := a_TargetColumnName;
    end if;
    
    if a_ForceCopy or api.pkg_ColumnQuery.NumericValue(a_TargetObjectId, t_TargetColumnName) is null then
      api.pkg_ColumnUpdate.SetValue(a_TargetObjectId, t_TargetColumnName, api.pkg_ColumnQuery.NumericValue(a_SourceObjectId, a_SourceColumnName));
    end if;
  end CopyNumericColumn;
  
  
  procedure CopyNumericColumn(
    a_SourceObjectId                    udt_Id,
    a_SourceColumnName                  varchar2,
    a_TargetObjectId                    udt_Id,
    a_ForceCopy                         boolean
  ) is
  begin
    CopyNumericColumn(a_SourceObjectId, a_SourceColumnName, a_TargetObjectId, a_SourceColumnName, a_ForceCopy);
  end;
  
  
  /*---------------------------------------------------------------------------
   * CopyViolationTypeToViolation() - public
   *  Copy the details from the Violation Type to the Violation.
   * This is designed to run on constructor of the rel between Violation Type
   * and Violation.
   *-------------------------------------------------------------------------*/
  procedure CopyViolationTypeToViolation (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_CSObjectDefId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('o_CodeSection');
    t_EndpointId                        udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_Violations', 'CodeSection');
    t_RelId                             udt_Id;
    t_TargetCSObjectId                  udt_Id;
    t_ViolationsObjectId                udt_Id;
    t_ViolationTypeObjectId             udt_Id;
    
    cursor c_CodeSectionTemplates is
      select r.CodeSectionTemplateObjectId SourceCSObjectId
        from query.r_LMS_ViolTypeCodeSectionTemp r
        where r.ViolationTypeObjectId = t_ViolationTypeObjectId;
  
  begin
    select r.ViolationTypeId, r.ViolationsId
      into t_ViolationTypeObjectId, t_ViolationsObjectId
      from query.r_ViolationsViolationType r
      where r.RelationshipId = a_RelationshipId;
    
    --CopyColumn(t_ViolationTypeObjectId, 'Description', t_ViolationsObjectId);
    CopyColumn(t_ViolationTypeObjectId, 'AssessFineOnNOV', t_ViolationsObjectId);
    CopyNumericColumn(t_ViolationTypeObjectId, 'GracePeriodNOV', t_ViolationsObjectId);
    CopyColumn(t_ViolationTypeObjectId, 'FrequencyNOV', t_ViolationsObjectId);
    CopyColumn(t_ViolationTypeObjectId, 'FinePerPeriodNOV', t_ViolationsObjectId);
    CopyColumn(t_ViolationTypeObjectId, 'AssessFineOnOrder', t_ViolationsObjectId);
    CopyNumericColumn(t_ViolationTypeObjectId, 'GracePeriodOrder', t_ViolationsObjectId);
    CopyColumn(t_ViolationTypeObjectId, 'FrequencyOrder', t_ViolationsObjectId);
    CopyColumn(t_ViolationTypeObjectId, 'FinePerPeriodOrder', t_ViolationsObjectId);
    
    for c in c_CodeSectionTemplates loop
      t_TargetCSObjectId := api.pkg_ObjectUpdate.New(t_CSObjectDefId);
      CopyColumn(c.SourceCSObjectId, 'CodeSection', t_TargetCSObjectId);
      CopyColumn(c.SourceCSObjectId, 'Title', t_TargetCSObjectId);
      CopyColumn(c.SourceCSObjectId, 'Text', t_TargetCSObjectId);
      t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, t_ViolationsObjectId, t_TargetCSObjectId);
    end loop;
  end CopyViolationTypeToViolation;
  
  
  /*---------------------------------------------------------------------------
   * CopyCSTemplateToCodeSection() - public
   *  Copy the details from the Code Section Template to the Code Section.
   * This is designed to run on constructor of the rel between Code Section
   * Template and Code Section.
   *-------------------------------------------------------------------------*/
  procedure CopyCSTemplateToCodeSection (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_CodeSectionObjectId               udt_Id;
    t_CSTemplateObjectId                udt_Id;
    
  begin
    select r.CodeSectionTemplateObjectId, r.CodeSectionObjectId
      into t_CSTemplateObjectId, t_CodeSectionObjectId
      from query.r_LMS_CodeSectionCSTemplate r
      where r.RelationshipId = a_RelationshipId;
      
    CopyColumn(t_CSTemplateObjectId, 'CodeSection', t_CodeSectionObjectId, True);
    CopyColumn(t_CSTemplateObjectId, 'Title', t_CodeSectionObjectId, True);
    CopyColumn(t_CSTemplateObjectId, 'Text', t_CodeSectionObjectId, True);
  end CopyCSTemplateToCodeSection;
  
END pkg_relationships;

/

