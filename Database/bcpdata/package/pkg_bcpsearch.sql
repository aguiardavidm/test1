create or replace package         pkg_BCPSearch as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_EndPoint is api.pkg_Definition.udt_EndPoint;
  subtype udt_ObjectList is api.udt_ObjectList;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_stringList is api.pkg_Definition.udt_StringList;

  /*---------------------------------------------------------------------------
   * FindStreetName() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindStreetName (
    a_Object            udt_ID,
    a_RelationshipDef   udt_ID,
    a_EndPoint          udt_EndPoint,
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindStreetName() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindStreetName (
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindInspections() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspections (
    a_CompletedDateFrom         date,
    a_CompletedDateTo           date,
    a_Inspector                 varchar2,
    a_InspectionType            varchar2,
    a_Trade                     varchar2,
    a_Outcome                   varchar2,
    a_ObjectList         out    api.udt_ObjectList
  );
  
   /*---------------------------------------------------------------------------
   * FindInspections2() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspections2 (
    a_InspectionNumber          VarChar2,
    a_BuildingPermit            VarChar2,
    a_GeneralPermit             VarChar2,
    a_TradePermit               VarChar2,
    a_StreetName                VarChar2,
    a_housenumber               VarChar2,
    a_fromhousenumber           VarChar2,
    a_tohousenumber             VarChar2,
    a_suite                     VarChar2,
    a_FromCompletedDate         date,
    a_ToCompletedDate           date,
    a_IssuedStartDate           date,
    a_IssuedEndDate             date,
    a_Outcome                   varchar2,
    a_ObjectList         out    api.udt_ObjectList
  );
  
  /*---------------------------------------------------------------------------
   * FindInspections3() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspections3 (
    a_InspectionNumber          VarChar2,
    a_BuildingPermit            VarChar2,
    a_GeneralPermit             VarChar2,
    a_TradePermit               VarChar2,
    a_StreetName                VarChar2,
    a_housenumber               VarChar2,
    a_fromhousenumber           VarChar2,
    a_tohousenumber             VarChar2,
    a_suite                     VarChar2,
    a_FromCompletedDate         date,
    a_ToCompletedDate           date,
    a_ToRequestedDate           date,
    a_FromRequestedDate             date,
    a_Outcome                   varchar2,
    a_InspectionStatus          VarChar2,
    a_InspectionType            Varchar2,
    a_Inspector                 Varchar2,
    a_RequestedBy               Varchar2,
    a_PermitNumber              Varchar2,
    a_TypeOfWork                Varchar2,
    a_Applicant                 VarChar2, 
    a_Contractor                VarChar2,
    a_CommercialOrResidential   VarChar2,
    a_ObjectList         out    api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindInspectionRoutingAssignmen() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspectionRoutingAssignmen (
    a_InspectorRouting          number,
    a_InspectorRoutingLinkData  varchar2,
    a_ObjectList                out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindContractors() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindContractors (
    a_BusinessName              varchar2,
    a_PrimaryContactName        varchar2,
    a_PrimaryPhoneNumber        varchar2,
    a_AllStateLicenseNumbers        varchar2,
    a_ObjectList         out    api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindContractorsLookup() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindContractorsLookup(
    a_BusinessName              varchar2,
    a_ObjectList         out    api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindContractorsForBilling() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindContractorsForBilling (
    a_AsOfDate                  date,
    a_BillType                  varchar2,
    a_ObjectList         out    api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindPropertyByAddress() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindAddress (
    a_ObjectId                  udt_ID,
    a_RelationshipDefId         udt_ID,
    a_ToEndPointId              udt_EndPoint,
    a_AddressString             varchar2,
    a_Results               out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindAddress() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindAddress (
    a_AddressString             varchar2,
    a_Results               out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindProject() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindProject (
    a_ProjectNumber             varchar2,
    a_Results               out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindSystemSettings() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindSystemSettings (
    a_Results               out api.udt_ObjectList
  );


  /*---------------------------------------------------------------------------
   * FindPermit() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermit (
    a_PermitNumber               varchar2,
    a_BuildingPermit             varchar2,
    a_TradePermit                varchar2,
    a_GeneralPermit              varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
    a_IssuedStartDate            date,
    a_IssuedEndDate              date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_PermitApplication          varchar2 default 'N', -- Remove the default when we add in the Permit Application
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Applicant                  varchar2 default null,
    a_ExpirationStartDate         date,
    a_ExpirationEndDate           date,
    a_ParcelNumberList            varchar2 default null,
    a_UseMapCriteria              varchar2,
    a_Results               out  api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindPermit2() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermit2 (
    a_PermitNumber               varchar2,
    a_BuildingPermit             varchar2,
    a_TradePermit                varchar2,
    a_GeneralPermit              varchar2,
    a_Results               out  api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindPermit3() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermit3 (
    a_PermitNumber               varchar2,
    a_ProjectName                varchar2,
    a_Results               out  api.udt_ObjectList);

  /*---------------------------------------------------------------------------
   * FindPermitByNumber() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermitByNumber (
    a_PermitNumber               varchar2,
    a_Results               out  api.udt_ObjectList
  );
  
  /*---------------------------------------------------------------------------
   * FindPermitByContractor() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermitByContractor (
    a_ContractorName        varchar2,
    a_PermitType            varchar2,    
    a_Results               out  api.udt_ObjectList
  ); 
  
  /*---------------------------------------------------------------------------
   * FindPermitByAddress2() -- PUBLIC
   *------------------------------------------------------------------------- */
   procedure FindPermitByAddress2 (
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_PermitType       varchar2 default null,
    a_Results           out api.udt_ObjectList 
  ); 
  
  /*---------------------------------------------------------------------------
   * FindPermitByDate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermitByDate (
    a_PermitType            varchar2,
    a_CreatedDateFrom       date,
    a_CreatedDateTo         date,    
    a_IssuedDateFrom        date,
    a_IssuedDateTo          date,
    a_Results               out  api.udt_ObjectList
  );  

  /*---------------------------------------------------------------------------
   * FindInspectors() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspectors (
    a_Dummy                      int,
    a_Results               out  api.udt_ObjectList
  );

   /*---------------------------------------------------------------------------
   * FindInspectionSettings() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspectionSettings (
    a_Results               out api.udt_ObjectList
  );

  /*-------------------------------------------------------------------------------
   * Procedure FindUserProcesses()
   *   Purpose: This procedure will return a list of incompleted processes assigned to
   *            a current login user for the purpose of filling a To-Do-List. You have
   *            the option of either displaying current processes(i.e. the scheduledstartdate
   *            is null or it is less than the sysdate) or all processes.
   *-----------------------------------------------------------------------------*/
  Procedure FindUserProcesses(
                a_User                   varchar2,
                a_ProcessType            varchar2,
                a_JobExternalFileNum     varchar2,
                a_DateCompletedFrom      date,
                a_DateCompletedTo        date,
                a_RelatedObjectIds	 out nocopy api.udt_ObjectList
                );
                
  /*---------------------------------------------------------------------------
   * FindPlanningJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPlanningJob (
    a_FileNumber               varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results               out  api.udt_ObjectList
  );   
    
   /*---------------------------------------------------------------------------
   * FindPlanningByNumber() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPlanningByNumber (
    a_PlanningNumber               varchar2,
    a_Results               out  api.udt_ObjectList
  );  
  
    /*---------------------------------------------------------------------------
   * FindPlannigByDate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPlanningByDate (
    a_CreatedDateFrom       date,
    a_CreatedDateTo         date,    
    a_IssuedDateFrom        date,
    a_IssuedDateTo          date,
    a_Results               out  api.udt_ObjectList
  );  
  /*---------------------------------------------------------------------------
   * FindPlanningByAddress() -- PUBLIC
   *------------------------------------------------------------------------- */
   procedure FindPlanningByAddress (
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_Results           out api.udt_ObjectList 
  );              
    
    /*---------------------------------------------------------------------------
   * FindPermitByAddress() -- PUBLIC
   *------------------------------------------------------------------------- */
   procedure FindPermitByAddress (
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_ExactMatch        varchar2 default null,
    a_Results           out api.udt_ObjectList 
  );
  
   /*---------------------------------------------------------------------------
   * FindPermitPublic() -- PUBLIC
   *-------------------------------------------------------------------------*/

 procedure FindPermitPublic (
    a_ExternalFileNum            varchar2,
    a_Description                varchar2 default null,
    a_BuildingPermit             varchar2 default null,
    a_TradePermit                varchar2 default null,
    a_GeneralPermit              varchar2 default null,
    a_AllContractorsName         varchar2 default null,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_ExactMatch                 varchar2 default null,
    a_Results               out  api.udt_ObjectList
  );


   /*---------------------------------------------------------------------------
   * FindProcessEmail() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindProcessEmail (
    a_Description           varchar2,
    a_Results               out api.udt_ObjectList
  );

   /*---------------------------------------------------------------------------
   * FindComplaint() -- PUBLIC
   *-------------------------------------------------------------------------*/
   procedure FindComplaint (
    a_ComplaintNumber           varchar2,
    a_CreatedStartDate          date,
    a_CreatedEndDate            date,
    a_CompletedStartDate        date,
    a_CompletedEndDate          date,
    a_Status                    varchar2,
    a_ComplaintDescription      varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results               out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindInvestigation() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInvestigation (
    a_InvestigationNumber       varchar2,
    a_CreatedStartDate          date,
    a_CreatedEndDate            date,
    a_CompletedStartDate        date,
    a_CompletedEndDate          date,
    a_Status                    varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results               out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindViolationTypes() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindViolationTypes (
    a_Title               varchar2,
    a_Results               out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FindViolations() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindViolations (
    a_ExternalFileNumber         varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results             out api.udt_ObjectList
  );
  
  /*---------------------------------------------------------------------------
   * FindCaseFileViolations() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindCaseFileViolations (
    a_JobId               number,
    a_Results             out api.udt_ObjectList
  );
  /*---------------------------------------------------------------------------
   * GetStatusNameForDescription() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetStatusNameForDescription (
    a_StatusDescription         varchar2,
    a_JobTypeId                 udt_Id
  ) return varchar2;
  
  /*---------------------------------------------------------------------------
   * FindContacts() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindContacts (
    a_Name              varchar2,
    a_LastName          varchar2,
    a_ObjectList         out    api.udt_ObjectList
  );

   /*---------------------------------------------------------------------------
   * ToDoList() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ToDoList (
    a_Results               out api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- ProcessTypeSearch
  --  Search for Process Types.
  -----------------------------------------------------------------------------
  procedure LMS_ProcessTypeSearch(
    a_Description                       varchar2,
    a_Name                              varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- LetterTemplateSearch
  --  Return Letter Templates.
  -----------------------------------------------------------------------------
  procedure LetterTemplateSearch (
    a_WordTemplateName                  varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- JobTypeSearch
  --  Search for Job Types.
  -----------------------------------------------------------------------------
  procedure LMS_JobTypeSearch(
    a_Description                       varchar2,
    a_Name                              varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  );
  
  -----------------------------------------------------------------------------
  -- JobTypeProcessTypesSearch
  --  Search for Job Type Process Types.
  -----------------------------------------------------------------------------
  procedure JobTypeProcessTypeSearch(
    a_JobTypeId                         number,
    a_ObjectList                        out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- LMSPersonSearch
  --  Search for o_LMS_Person.
  -----------------------------------------------------------------------------
  procedure LMSPersonSearch(
    a_FormattedName                 varchar2,
    a_PhoneNumber                   varchar2,
    a_FaxNumber                     varchar2,
    a_EmailAddress                  varchar2,
    a_Objects                       out nocopy udt_ObjectList
  );
    
  /*---------------------------------------------------------------------------
   * CandEJobSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure CandEJobSearch (
    a_FileNumber                 varchar2,
    a_Complaint                  varchar2,
    a_CaseFile                   varchar2,
    a_Order                      varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_LocationDescription        varchar2 default null,
    a_Results                    out  api.udt_ObjectList
  );  

  /*---------------------------------------------------------------------------
   * InvestigationSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure InvestigationSearch (
    a_CompletedBy                number,
    a_InvestigationResult        number,
    a_Outcome                    varchar2,
    a_InvestigationStartDate     date,
    a_InvestigationEndDate       date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results                    out  api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * CandEMapSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure CandEMapSearch (
   a_ExtentMinX                 varchar2,
   a_ExtentMinY                 varchar2,
   a_ExtentMaxX                 varchar2,
   a_ExtentMaxY                 varchar2,
   a_Results                    out  api.udt_ObjectList
  );  
 
end pkg_BCPSearch;



 
/

create or replace package body         pkg_BCPSearch as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * FindStreetName() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindStreetName (
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  ) is
    t_SearchString      varchar2(4000);
    t_StreetDefId       number := api.pkg_ConfigQuery.ObjectDefIdForName('o_Street');
  begin
api.pkg_errors.RaiseError(-20000, 'Test 1');
    pkg_Debug.PutLine('a_SearchString: "' || a_SearchString || '"');
    t_SearchString := a_SearchString;
    if substr(t_SearchString, length(t_SearchString), 1) != '%' then
      t_SearchString := t_SearchString || '%';
    end if;
    pkg_Debug.PutLine('t_SearchString: "' || t_SearchString || '"');
    a_Objects := api.udt_ObjectList();
api.pkg_errors.raiseerror(-20000, 'Count: ' || a_Objects.Count);
    for c in (select reo.ObjectId PosseObjectId, st.Formattedname
                from api.StreetNames sn
                join Street st on st.formattedname = sn.Name
                join api.registeredexternalobjects reo on reo.LinkValue = st.streetobjectid
                 and reo.objectdefid = t_StreetDefId
               where (sn.Name like upper(t_SearchString)
                  or sn.StreetName like upper(t_SearchString))) loop
      pkg_Debug.PutLine('Found street: ' || c.FormattedName || ', PosseObjectId: ' || c.PosseObjectId);
      a_Objects.extend(1);
      a_Objects(a_Objects.count) := api.udt_Object(c.PosseObjectId);
    end loop;
    for i in 1..a_Objects.Count loop
      app.pkg_Debug.PutLine('a_Objects(' || i || ').ObjectId: ' || a_Objects(i).ObjectId);
    end loop;

  end;

  /*---------------------------------------------------------------------------
   * FindStreetName() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindStreetName (
    a_Object            udt_ID,
    a_RelationshipDef   udt_ID,
    a_EndPoint          udt_EndPoint,
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  ) is
  begin
    FindStreetName(a_SearchString, a_Objects);
  end;

  /*---------------------------------------------------------------------------
   * FindInspections() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspections (
    a_CompletedDateFrom         date,
    a_CompletedDateTo           date,
    a_Inspector                 varchar2,
    a_InspectionType            varchar2,
    a_Trade                     varchar2,
    a_Outcome                   varchar2,
    a_ObjectList         out    api.udt_ObjectList
  ) is
  begin
    app.pkg_debug.putline('a_CompletedDateFrom: ' || to_char(a_CompletedDateFrom) || ', a_CompletedDateTo: ' || to_char(a_CompletedDateTo) || ', a_Inspector: ' || a_Inspector || ', a_InspectionType: ' || a_InspectionType || ', a_Trade: ' || a_Trade || ', a_Outcome: ' || a_Outcome);
    pkg_Search.InitializeSearch('o_CompletedInspection');
    pkg_Search.AddDateIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'CompletedDate', a_CompletedDateFrom, a_CompletedDateTo);
    pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'Inspector', a_Inspector, true);
    pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'InspectionType', a_InspectionType, true);
    pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'Trade', a_Trade, true);
    pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'Outcome', a_Outcome, true);
    pkg_Search.PerformSearch(a_ObjectList);
  end;
  
  /*---------------------------------------------------------------------------
   * FindInspections2() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspections2 (
    a_InspectionNumber          VarChar2,
    a_BuildingPermit            VarChar2,
    a_GeneralPermit             VarChar2,
    a_TradePermit               VarChar2,
    a_StreetName                VarChar2,
    a_housenumber               VarChar2,
    a_fromhousenumber           VarChar2,
    a_tohousenumber             VarChar2,
    a_suite                     VarChar2,
    a_FromCompletedDate         date,
    a_ToCompletedDate           date,
    a_IssuedStartDate           date,
    a_IssuedEndDate             date,
    a_Outcome                   varchar2,
    a_ObjectList         out    api.udt_ObjectList
  ) is

  t_BuildingPermitTypeId api.pkg_definition.UDT_Id :=0;
  t_GeneralPermitTypeId api.pkg_definition.UDT_Id :=0;
  t_TradePermitTypeId api.pkg_definition.UDT_Id :=0;
  t_jobList api.udt_ObjectList;
  t_jobList2 api.pkg_Definition.udt_IdList;
  t_JobObjectList api.udt_ObjectList;
  t_IdList        api.pkg_Definition.udt_IdList;
  t_MechInspectionTypeId api.pkg_definition.UDT_Id;
  t_ElectInspectionTypeId api.pkg_definition.UDT_Id;
  t_BuildInspectionTypeId api.pkg_definition.UDT_Id;
  t_PlumbInspectionTypeId api.pkg_definition.UDT_Id;
  t_SearchByInspection Varchar2(1) := 'N';
  t_SearchByAddress Varchar2(1) := 'N';

  Begin
  
  t_jobList := api.udt_ObjectList();
  t_JobObjectList := api.udt_ObjectList();
    --Find out what type of Permit job I am searching for
    if a_BuildingPermit ='Y' then
       t_BuildingPermitTypeId := api.Pkg_Configquery.ObjectDefIdForName('j_buildingpermit');
    end if;
    if a_GeneralPermit ='Y' then
       t_GeneralPermitTypeId := api.Pkg_Configquery.ObjectDefIdForName('j_GeneralPermit');
    end if;
    if a_TradePermit ='Y' then
      t_TradePermitTypeId := api.Pkg_Configquery.ObjectDefIdForName('j_TradePermit');
    end if;  
    
    --if we are searching by address
      if a_StreetName is not null
      or a_housenumber is not null
      or a_fromhousenumber is not null
      or a_tohousenumber is not null
      or a_suite is not null then

      --check that we have valid search criterion
        if trim(a_StreetName) is null then
          if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
            api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
          else
            api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
          end if;
        end if;
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          null;
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
        end if;
        --Use FindPermit to get a permit from an address
         t_SearchByAddress := 'Y';
         pkg_BCPSearch.FindPermit(null,a_BuildingPermit,a_TradePermit,a_GeneralPermit,null,null,null,null,null,null,null,null,a_StreetName,a_HouseNumber,a_FromHouseNumber,a_ToHouseNumber,a_suite,null,null,null,null,'N',t_JobList);
      end if; --if we are searching by address
             
    if a_InspectionNumber is not null
       or a_FromCompletedDate is not null
       or a_ToCompletedDate is not null
       or a_Outcome is not null then
       --The User is searching by Inspection
       t_SearchByInspection := 'Y';
      -- Do a search for permits based on Inspections
      --Grab me all of the ProcessTypeIds for the processes that I am going to be searching by
      t_MechInspectionTypeId  := api.Pkg_Configquery.ObjectDefIdForName('p_performmechanicalinspection');
      t_ElectInspectionTypeId := api.Pkg_Configquery.ObjectDefIdForName('p_performelectricalinspection');
      t_BuildInspectionTypeId := api.Pkg_Configquery.ObjectDefIdForName('p_performbuildinginspection');
      t_PlumbInspectionTypeId := api.Pkg_Configquery.ObjectDefIdForName('p_performplumbinginspection');
      Select distinct p.jobid
        bulk collect into t_JobList2
        from (Select p1.*, api.pkg_columnquery.DateValue(p1.ProcessId, 'RequestedDate') RequestedDate
                from api.processes p1
               where p1.processtypeId in (t_MechInspectionTypeId,t_ElectInspectionTypeId,t_BuildInspectionTypeId,t_PlumbInspectionTypeId)
               )p
        join api.jobs j
          on p.jobId = j.Jobid
         and j.jobTypeId in (t_BuildingPermitTypeId,t_GeneralPermitTypeId,t_TradePermitTypeId)
       where (a_InspectionNumber is null or p.ExternalFileNum = a_InspectionNumber)
         and ((a_FromCompletedDate is null and a_ToCompletedDate is null)
              or(a_FromCompletedDate is null and p.DateCompleted <= a_ToCompletedDate)
              or (a_ToCompletedDate is null and p.DateCompleted >= a_FromCompletedDate)
              or (p.DateCompleted between a_FromCompletedDate and a_ToCompletedDate))
         and ((a_IssuedStartDate is null and a_IssuedEndDate is null)
              or(a_IssuedStartDate is null and p.RequestedDate <= a_IssuedEndDate)
              or (a_IssuedEndDate is null and p.RequestedDate >= a_IssuedStartDate)
              or (p.RequestedDate between a_IssuedStartDate and a_IssuedEndDate))
         and (a_Outcome is null or p.Outcome = a_Outcome);
               

     end if;
     
     --Convert the UDTID list to an ObjectList
     t_JobObjectList := extension.pkg_utils.ConvertToObjectList(t_JobList2);
     --raise_application_Error(-20000, t_SearchByAddress || ' '||t_JobList.count || ' '|| t_SearchByInspection || ' ' ||  t_JobList2.count);
     if t_SearchByAddress = 'Y' and t_SearchByInspection = 'Y' then
        select ObjectId bulk collect into t_IdList
          from table (t_JobList)
       intersect
         select ObjectId
           from table (t_JobObjectList);
        a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);
      elsif t_SearchByInspection = 'Y' then
        a_ObjectList := t_JobObjectList;
      else
        a_Objectlist := t_JobList;
      end if;
    
     
end FindInspections2;

/*---------------------------------------------------------------------------
   * FindInspections3() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspections3 (
    a_InspectionNumber          VarChar2,
    a_BuildingPermit            VarChar2,
    a_GeneralPermit             VarChar2,
    a_TradePermit               VarChar2,
    a_StreetName                VarChar2,
    a_housenumber               VarChar2,
    a_fromhousenumber           VarChar2,
    a_tohousenumber             VarChar2,
    a_suite                     VarChar2,
    a_FromCompletedDate         date,
    a_ToCompletedDate           date,
    a_ToRequestedDate           date,
    a_FromRequestedDate             date,
    a_Outcome                   varchar2,
    a_InspectionStatus          VarChar2,
    a_InspectionType            Varchar2,
    a_Inspector                 Varchar2,
    a_RequestedBy               Varchar2,
    a_PermitNumber              Varchar2,
    a_TypeOfWork                Varchar2,
    a_Applicant                 VarChar2, 
    a_Contractor                VarChar2,
    a_CommercialOrResidential   VarChar2,
    a_ObjectList         out    api.udt_ObjectList
  ) is

  t_BuildingPermitTypeId api.pkg_definition.UDT_Id :=0;
  t_GeneralPermitTypeId api.pkg_definition.UDT_Id :=0;
  t_TradePermitTypeId api.pkg_definition.UDT_Id :=0;
  t_ProcessList api.pkg_Definition.udt_IdList;
  t_ProcessList2 api.pkg_Definition.udt_IdList;
  t_ProcessObjectList api.udt_ObjectList;
  t_ProcessObjectList2 api.udt_ObjectList;
  t_IdList        api.pkg_Definition.udt_IdList;
  t_MechInspectionTypeId api.pkg_definition.UDT_Id;
  t_ElectInspectionTypeId api.pkg_definition.UDT_Id;
  t_BuildInspectionTypeId api.pkg_definition.UDT_Id;
  t_PlumbInspectionTypeId api.pkg_definition.UDT_Id;
--  t_ContractorDefId := api.pkg_configquery.ObjectDefIdForName ('o_Contractor');
  t_SearchByInspection Varchar2(1) := 'N';
  t_SearchByAddress Varchar2(1) := 'N';

  Begin
  
  t_ProcessObjectList2 := api.udt_ObjectList();
  t_ProcessObjectList := api.udt_ObjectList();
  t_MechInspectionTypeId  := api.Pkg_Configquery.ObjectDefIdForName('p_performmechanicalinspection');
  t_ElectInspectionTypeId := api.Pkg_Configquery.ObjectDefIdForName('p_performelectricalinspection');
  t_BuildInspectionTypeId := api.Pkg_Configquery.ObjectDefIdForName('p_performbuildinginspection');
  t_PlumbInspectionTypeId := api.Pkg_Configquery.ObjectDefIdForName('p_performplumbinginspection');
    --Find out what type of Permit job I am searching for
    if a_BuildingPermit ='Y' then
       t_BuildingPermitTypeId := api.Pkg_Configquery.ObjectDefIdForName('j_buildingpermit');
    end if;
    if a_GeneralPermit ='Y' then
       t_GeneralPermitTypeId := api.Pkg_Configquery.ObjectDefIdForName('j_GeneralPermit');
    end if;
    if a_TradePermit ='Y' then
      t_TradePermitTypeId := api.Pkg_Configquery.ObjectDefIdForName('j_TradePermit');
    end if;  
    
    --if we are searching by address
      if a_StreetName is not null
      or a_housenumber is not null
      or a_fromhousenumber is not null
      or a_tohousenumber is not null
      or a_suite is not null then

      --check that we have valid search criterion
        if trim(a_StreetName) is null then
          if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
            api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
          else
            api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
          end if;
        end if;
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          null;
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
        end if;
        --Find all the inspections with that address or address range
         t_SearchByAddress := 'Y';
          Select distinct p.processid
                  bulk collect into t_ProcessList
                  from (Select p1.*,
                               api.pkg_columnquery.DateValue(p1.ProcessId, 'RequestedDate') RequestedDate,
                               api.pkg_columnquery.value (p1.processid, 'HouseNumber') HouseNumber,
                               api.pkg_columnquery.value (p1.processid, 'StreetName') StreetName,
                               api.pkg_columnquery.value (p1.processid, 'Suite') Suite
                          from api.processes p1
                         where p1.processtypeId in (t_MechInspectionTypeId,t_ElectInspectionTypeId,t_BuildInspectionTypeId,t_PlumbInspectionTypeId)
                         )p
                 where ((a_HouseNumber is null or a_HouseNumber = p.housenumber)
                          or (a_tohousenumber is null and a_fromhousenumber is null or p.HouseNumber between a_FromHouseNumber and a_ToHouseNumber))
                   and upper(p.StreetName) like (upper(a_StreetName) || '%')
                   and (a_suite is null or UPPER(p.suite) = UPPER(a_suite));

                     end if; --if we are searching by address
             
    if a_InspectionNumber is not null
       or a_FromCompletedDate is not null
       or a_ToCompletedDate is not null
       or a_FromRequestedDate is not null
       or a_ToRequestedDate is not null
       or a_RequestedBy is not null
       or a_PermitNumber is not null then
       
       --The User is searching by Inspection
       t_SearchByInspection := 'Y';
      -- Do a search for permits based on Inspections
      --Grab me all of the ProcessTypeIds for the processes that I am going to be searching by
      
      
      Select distinct p.processid
        bulk collect into t_ProcessList2
        from (Select p1.*,
                     j.ExternalFileNum JobFileNumber,
                     api.pkg_columnquery.value (p1.processid, 'ObjectDefDescription') InspectionType,
                     api.pkg_columnquery.value (p1.processid, 'InspectorName') Inspector,
                     api.pkg_columnquery.value (p1.processid, 'RequestedBy') RequestedBy,
                     api.pkg_columnquery.DateValue(p1.ProcessId, 'RequestedDate') RequestedDate,
                     api.pkg_columnquery.value (p1.jobid, 'TypeOfWork') TypeOfWork,
                     api.pkg_columnquery.value (p1.jobid, 'CommercialOrResidential') CommercialOrResidential,
                     api.pkg_columnquery.value (p1.jobid, 'ApplicantNameStored') Applicant
                from api.processes p1
                join api.jobs j
                  on p1.jobId = j.Jobid
                 and j.jobTypeId in (t_BuildingPermitTypeId,t_GeneralPermitTypeId,t_TradePermitTypeId)
               where p1.processtypeId in (t_MechInspectionTypeId,t_ElectInspectionTypeId,t_BuildInspectionTypeId,t_PlumbInspectionTypeId)
               )p
       where (a_InspectionNumber is null or p.ExternalFileNum like (a_InspectionNumber || '%'))
         and ((a_FromCompletedDate is null and a_ToCompletedDate is null)
              or(a_FromCompletedDate is null and p.DateCompleted <= a_ToCompletedDate)
              or (a_ToCompletedDate is null and p.DateCompleted >= a_FromCompletedDate)
              or (p.DateCompleted between a_FromCompletedDate and a_ToCompletedDate))
         and ((a_FromRequestedDate is null and a_ToRequestedDate is null)
              or(a_FromRequestedDate is null and p.RequestedDate <= a_ToRequestedDate)
              or (a_ToRequestedDate is null and p.RequestedDate >= a_FromRequestedDate)
              or (p.RequestedDate between a_FromRequestedDate and a_ToRequestedDate))
         and (a_Outcome is null or p.Outcome = a_Outcome)
         and (a_TypeOfWork is null or p.TypeOfWork = a_TypeOfWork)
         and (a_CommercialOrResidential is null or p.CommercialOrResidential = a_CommercialOrResidential)
         and (a_Applicant is null or upper(p.Applicant) like (upper(a_Applicant) || '%'))
         and (a_InspectionStatus is null or a_InspectionStatus = p.ProcessStatus)
         and (a_InspectionType is null or a_InspectionType = p.InspectionType)
         and (a_Inspector is null or upper(p.Inspector) like (upper(a_Inspector) || '%'))
         and (a_RequestedBy is null or (Select upper(u.name)
                                          from query.u_users u
                                          where u.OracleLogonId = p.RequestedBy) like (upper(a_RequestedBy) || '%'))
         and (a_PermitNumber is null or p.JobFileNumber like (a_PermitNumber || '%'))
         and (Exists (Select 1
                        from api.processes p2 
                        join api.jobs j1 on j1.jobid = p2.JobId
                        join api.relationships r on r.FromObjectId = j1.jobid
                        join query.o_contractor c on c.objectid = r.ToObjectId
                        where upper(c.BusinessName) like (upper(a_Contractor) || '%')) or a_Contractor is null);

         
         elsif (a_housenumber is null and a_StreetName is null)
               or (a_tohousenumber is null and a_fromhousenumber is null and a_StreetName is null) then
               
           api.pkg_errors.RaiseError (-20000, 'You must enter one or more of the following to perform this search: <br/>' || 
                                              '*Inspection Number <br/>' ||
                                              '*Requested Date(s) <br/>' || 
                                              '*Completed Date(s) <br/>' ||
                                              '*Requested By <br/>' || 
                                              '*House Number and Street Name <br/>' || 
                                              '*Permit Number');    

     end if;

     
     --Convert the UDTID list to an ObjectList
     t_ProcessObjectList := extension.pkg_utils.ConvertToObjectList(t_ProcessList);
     t_ProcessObjectList2 := extension.pkg_utils.ConvertToObjectList(t_ProcessList2);
     --raise_application_Error(-20000, t_SearchByAddress || ' '||t_ProcessList.count || ' '|| t_SearchByInspection || ' ' ||  t_ProcessList2.count);
     if t_SearchByAddress = 'Y' and t_SearchByInspection = 'Y' then
        select ObjectId bulk collect into t_IdList
          from table (t_ProcessObjectList)
       intersect
         select ObjectId
           from table (t_ProcessObjectList2);

        a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);
      elsif t_SearchByInspection = 'Y' then
        a_ObjectList := t_ProcessObjectList2;

      else
        a_Objectlist := t_ProcessObjectList;

      end if;

    
     
end FindInspections3;

  /*---------------------------------------------------------------------------
   * FindInspectionRoutingAssignmen() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspectionRoutingAssignmen (
    a_InspectorRouting          number,
    a_InspectorRoutingLinkData  varchar2,
    a_ObjectList                out api.udt_ObjectList
  ) is
  begin
    a_ObjectList := api.udt_ObjectList();
    extension.pkg_CXProceduralSearch.InitializeSearch('o_InspectorRoutingAssignment');
    extension.pkg_CXProceduralSearch.SearchByRelatedIndex('InspectorRouting', 'ObjectId', a_InspectorRouting, null, true);
    extension.pkg_CXProceduralSearch.SearchByIndex('InspectorRoutingLinkData', a_InspectorRoutingLinkData, null, true);
    extension.pkg_CXProceduralSearch.PerformSearch(a_ObjectList, 'and');
  end FindInspectionRoutingAssignmen;

  /*---------------------------------------------------------------------------
   * FindContractors() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindContractors (
    a_BusinessName              varchar2,
    a_PrimaryContactName        varchar2,
    a_PrimaryPhoneNumber        varchar2,
    a_AllStateLicenseNumbers        varchar2,
    a_ObjectList         out    api.udt_ObjectList
  ) is
  begin
    a_ObjectList := api.udt_ObjectList();
    app.pkg_debug.putline('a_BusinessName: ' || a_BusinessName ||
                        ', a_PrimaryContactName: ' || a_PrimaryContactName ||
                        ', a_PrimaryPhoneNumber: ' || a_PrimaryPhoneNumber ||
                        ', a_AllStateLicenseNumbers: ' || a_AllStateLicenseNumbers);
    extension.pkg_cxproceduralsearch.InitializeSearch('o_Contractor');
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ContractorRegistration', 'ExternalFileNum', a_AllStateLicenseNumbers, null, true);
    extension.pkg_cxproceduralsearch.SearchByIndex('BusinessName', a_BusinessName, null, true);
    extension.pkg_cxproceduralsearch.SearchByIndex('PrimaryPhoneNumber', a_PrimaryPhoneNumber ,null,true); 
    extension.pkg_cxproceduralsearch.SearchByIndex('PrimaryContactName', a_PrimaryContactName, null, true); 
    extension.pkg_cxproceduralsearch.PerformSearch(a_ObjectList, 'and');                 
    /*pkg_Search.InitializeSearch('o_Contractor');
    pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'dup_AllStateLicenseNumbers', a_AllStateLicenseNumbers, true);
    pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'BusinessName', a_BusinessName, true );
    pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'PrimaryPhoneNumber', a_PrimaryPhoneNumber, true );
    pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'PrimaryContactName', a_PrimaryContactName, true);
    pkg_Search.PerformSearch(a_ObjectList);*/
  end;

  /*---------------------------------------------------------------------------
   * FindContractorsLookup() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindContractorsLookup(
    a_BusinessName              varchar2,
    a_ObjectList         out    api.udt_ObjectList
  ) is
  begin
    if a_BusinessName is null
      then
        api.pkg_errors.raiseerror(-20000, 'Please enter the name of the contractor.');
    else
      pkg_Search.InitializeSearch('o_Contractor');
      pkg_Search.AddStringIndexSearchPath(pkg_Search.g_RestrictSearchMethod, 'BusinessName', a_BusinessName, true );
      pkg_Search.PerformSearch(a_ObjectList);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * FindContractorsForBilling() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindContractorsForBilling (
    a_AsOfDate                  date,
    a_BillType                  varchar2,
    a_ObjectList         out    api.udt_ObjectList
  ) is
  begin
    a_ObjectList := api.udt_ObjectList();
    if a_BillType = 'Fees Paid' or a_BillType = 'All' or a_BillType is null then
      for c in (select distinct co.ObjectId
                  from query.o_Contractor co,
                       api.Fees fe
                 where fe.ResponsibleObjectId = co.ObjectId
                   and fe.TransactionDate > a_AsOfDate
                   and fe.Amount + fe.TaxAmount + (select nvl(sum(ft.Amount + ft.TaxAmount),0)
                                                     from api.FeeTransactions ft
                                                    where ft.Feeid = fe.FeeId) <= 0) loop
        a_ObjectList.extend(1);
        a_ObjectList(a_ObjectList.count) := api.udt_Object(c.ObjectId);
      end loop;
    end if;
    if a_BillType = 'Fees Due' or a_BillType = 'All' or a_BillType is null then
      for c in (select co.ObjectId
                  from query.o_Contractor co,
                       api.Fees fe
                 where fe.ResponsibleObjectId = co.ObjectId
                   and fe.Amount + fe.TaxAmount + (select nvl(sum(ft.Amount + ft.TaxAmount),0)
                                                     from api.FeeTransactions ft
                                                    where ft.Feeid = fe.FeeId) > 0) loop
        a_ObjectList.extend(1);
        a_ObjectList(a_ObjectList.count) := api.udt_Object(c.ObjectId);
      end loop;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * FindAddress() -- PUBLIC
   *
   * Kevin Nanson
   *   Modified the Procedure FindAddress(a_AddressString, a_Results) that used to 
   *   call this procedure, to use the extension.pkg_cxProceduralSearch   
   *
   *   This has been left in (but commented out) to ensure we identify
   *   any functions that are calling this. 
   *-------------------------------------------------------------------------*/
  procedure FindAddress (
    a_ObjectId                  udt_ID,
    a_RelationshipDefId         udt_ID,
    a_ToEndPointId              udt_EndPoint,
    a_AddressString             varchar2,
    a_Results               out api.udt_ObjectList
  ) is
    t_SpacePos                  number;
    t_DashPos                   number;
    t_HouseNumberFrom           number;
    t_HouseNumberTo             number;
    t_HouseNumberString         varchar2(60);
    t_StreetName                varchar2(60);
    t_ObjectDefId               number := api.pkg_ObjectDefQuery.IdForName('o_Address');
    cursor c_Addresses (a_HouseNumberFrom number, a_HouseNumberTo number, a_StreetName varchar2) is
      select oa.ObjectId
        from api.ObjectAddresses oa,
             api.StreetNames sn,
             api.Objects ob
       where ob.ObjectId = oa.ObjectId
         and ob.ObjectDefId = t_ObjectDefId
         and oa.HouseNum between a_HouseNumberFrom and a_HouseNumberTo
         and sn.StreetNameId = oa.StreetNameId
         and sn.Name like '%' || upper(a_StreetName) || '%'
       order by sn.Name, oa.HouseNum;
    procedure SetErrorArguments is
      begin
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('a_ObjectId', a_ObjectId);
        api.pkg_Errors.SetArgValue('a_RelationshipDefId', a_RelationshipDefId);
        api.pkg_Errors.SetArgValue('a_ToEndPointId', a_ToEndPointId);
        api.pkg_Errors.SetArgValue('a_AddressString', a_AddressString);
      end;

  begin
    -- Break apart input string
    -- Expect house number, followed by a space, followed by the street name
    /*
    t_SpacePos := instr(a_AddressString, ' ');
    if t_SpacePos = 0 or t_SpacePos is null then
      SetErrorArguments;
      api.pkg_Errors.RaiseError(-20000, '{a_AddressString} is invalid. Please input the address in the format: HouseNumber[-HouseNumberTo] StreetName. For example: 1234 Neighborhood Ln.');
    end if;

    t_HouseNumberString := trim(substr(a_AddressString, 1, t_SpacePos - 1));

    t_DashPos := instr(t_HouseNumberString, '-');
    if t_DashPos > 0 then
      begin
        t_HouseNumberFrom := to_number(trim(substr(t_HouseNumberString, 1, t_DashPos - 1)));
        t_HouseNumberTo := to_number(trim(substr(t_HouseNumberString, t_DashPos + 1)));

      exception when value_error then
        SetErrorArguments;
        api.pkg_Errors.SetArgValue('HouseNumberString', t_HouseNumberString);
        api.pkg_Errors.SetArgValue('Resolving range', 'True');
        api.pkg_Errors.RaiseError(-20000, '{a_AddressString} does not contain a numeric house number. Please input the address in the format: HouseNumber[-HouseNumberTo] StreetName. For example: 1234 Neighborhood Ln.');
      end;

    else

      begin
        t_HouseNumberFrom := to_number(trim(substr(a_AddressString, 1, t_SpacePos - 1)));
        t_HouseNumberTo := t_HouseNumberFrom;
      exception when value_error then
        SetErrorArguments;
        api.pkg_Errors.SetArgValue('HouseNumberString', t_HouseNumberString);
        api.pkg_Errors.RaiseError(-20000, '{a_AddressString} does not contain a numeric house number. Please input the address in the format: HouseNumber[-HouseNumberTo] StreetName. For example: 1234 Neighborhood Ln.');
      end;

    end if;

    t_StreetName := trim(substr(a_AddressString, t_SpacePos + 1));

    a_Results := api.udt_ObjectList();
    for c in c_Addresses (t_HouseNumberFrom, t_HouseNumberTo, t_StreetName) loop
      a_Results.extend(1);
      a_Results(a_Results.count) := api.udt_Object(c.ObjectId);
    end loop;
    */
    
    --Raise error so we can identify any functions/procedures that use this
    api.pkg_Errors.RaiseError(-20000, 'FindAddress(,,,,) is no longer in use.  Use FindAddress(,) Instead');
  end;

  /*---------------------------------------------------------------------------
   * FindAddress() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindAddress (
    a_AddressString             varchar2,
    a_Results               out api.udt_ObjectList
  ) is
    t_SpacePos                  number;
    t_DashPos                   number;
    t_StreetPos                 number;
    t_HouseNumberFrom           varchar2(100);
    t_HouseNumberTo             varchar2(100);
    t_HouseNumberString         varchar2(60);
    t_StreetName                varchar2(60);
    t_AddressStringList         udt_StringList;
    
  begin
    -- Break apart input string
    -- Expect house number, followed by a space, followed by the street name
    t_AddressStringList := extension.pkg_Utils.Split(a_AddressString, ' '); 

    if t_AddressStringList.count = 1 then
      api.pkg_Errors.RaiseError(-20000, a_AddressString || ' is invalid. Please input the address in the format: HouseNumber[-HouseNumberTo] StreetName. For example: 1234 Neighborhood Ln.');
    end if;

    t_StreetPos := 2;
    -- Check for the address range....
    if instr(t_AddressStringList(1), '-', 1) > 1 OR
       t_AddressStringList(2) = '-' then
       
       if instr(t_AddressStringList(1), '-', 1) > 1 then
         t_DashPos := instr(t_AddressStringList(1), '-');
         t_HouseNumberFrom := substr(t_AddressStringList(1), 1, t_DashPos - 1);
         t_HouseNumberTo   := substr(t_AddressStringList(1), t_DashPos + 1);
         t_StreetPos       := 2;                 
       else
         t_HouseNumberFrom := t_AddressStringList(1);
         t_HouseNumberTo   := t_AddressStringList(3);
         t_StreetPos       := 4;   
       end if;
    else
       t_HouseNumberFrom := t_AddressStringList(1);
       t_HouseNumberTo   := null;    
    end if;    
  
    --we are assuming that the remainder of the string is all the street name
    t_StreetName := substr(a_AddressString, instr(a_AddressString, ' ',1, t_StreetPos - 1) + 1);

    extension.pkg_cxproceduralsearch.InitializeSearch('o_Address');
    extension.pkg_cxproceduralsearch.SearchByAddress(t_HouseNumberFrom, t_HouseNumberTo, null, t_StreetName, null, null);
    a_Results := api.udt_ObjectList();
    extension.pkg_cxproceduralsearch.PerformSearch(a_Results);

  end;

  /*---------------------------------------------------------------------------
   * FindProject() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindProject (
    a_ProjectNumber             varchar2,
    a_Results               out api.udt_ObjectList
  ) is
  begin
    a_Results := api.udt_ObjectList();
    for c in (select PRO.ProcessId
                from api.processes PRO,
                     api.jobs JOB
               where JOB.ProjectId = a_ProjectNumber
                 and PRO.JobId = JOB.JobId) loop

      a_Results.extend(1);
      a_Results(a_Results.count) := api.udt_Object(c.ProcessId);
    end loop;
  end;

   /*---------------------------------------------------------------------------
   * FindSystemSettings() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindSystemSettings (
    a_Results               out api.udt_ObjectList
  ) is
  begin
    a_Results := api.udt_ObjectList();

    a_Results.extend(1);
    a_Results(a_Results.count) := api.udt_Object(bcpdata.Pkg_BCPSetting.SystemSettingsObjectId('o_SystemSettings'));
  end;


  /*---------------------------------------------------------------------------
   * FindPermit() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermit (
    a_PermitNumber               varchar2,
    a_BuildingPermit             varchar2,
    a_TradePermit                varchar2,
    a_GeneralPermit              varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
    a_IssuedStartDate            date,
    a_IssuedEndDate              date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_PermitApplication          varchar2 default 'N', -- Remove the default when we add in the Permit Application
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Applicant                  varchar2 default null,
    a_ExpirationStartDate         date,
    a_ExpirationEndDate           date,
    a_ParcelNumberList            varchar2 default null,
    a_UseMapCriteria              varchar2,
    a_Results               out  api.udt_ObjectList
  ) is
  t_TempResults             api.udt_ObjectList;
  t_TempResults2            api.udt_ObjectList;
  t_MapCriteriaResults      api.udt_ObjectList;
  t_Status                  varchar2(500);
  t_BuildingPermitTypeId    number := api.pkg_configquery.objectdefidforname('j_BuildingPermit');
  t_GeneralPermitTypeId     number := api.pkg_configquery.objectdefidforname('j_GeneralPermit');
  t_TradePermitTypeId       number := api.pkg_configquery.objectdefidforname('j_TradePermit');
  t_PermitBundleTypeId      number := api.pkg_configquery.objectdefidforname('j_PermitBundle');
  t_MapParcelNumbers        api.pkg_Definition.udt_StringList;
  t_AndOr                   varchar2(10);

  begin
    a_Results            := api.udt_ObjectList();
    t_TempResults        := api.udt_ObjectList();
    t_TempResults2       := api.udt_ObjectList();
    t_MapCriteriaResults := api.udt_ObjectList();


  --if we are searching by address
  
  --api.pkg_Errors.RaiseError (-20000, a_StreetName || ' ' || a_HouseNumber || ' ' || a_FromHouseNumber || ' ' || a_ToHousenumber || ' ' || a_suite);
    if a_StreetName is not null
    or a_housenumber is not null
    or a_fromhousenumber is not null
    or a_tohousenumber is not null
    or a_suite is not null then

    --check that we have valid search criterion
      if trim(a_StreetName) is null then
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;

    end if; --if we are searching by address

    if (a_BuildingPermit = 'Y') then


  -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
    t_Status := GetStatusNameForDescription(a_Status, t_BuildingPermitTypeId);


        extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
        extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_PermitNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     a_IssuedStartDate,
                                                     a_IssuedEndDate,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       extension.pkg_cxproceduralsearch.SearchByIndex ('ExpirationDate', a_ExpirationStartDate, a_ExpirationEndDate);

        if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;
        
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
       
       /* we need to add an AND (applicant OR contractor = <name>) to the end of this search */
       if t_TempResults.Count > 0 then
         t_AndOr := 'and';
       else
         if extension.pkg_cxproceduralsearch.SearchPerformed then
           /* Doing this search would be pointless */
           t_AndOr := 'skip';
         else
           t_AndOr := 'or';
         end if;
       end if;

       If a_Applicant is not null and t_AndOr <> 'skip' then

          extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
          extension.pkg_cxproceduralsearch.SearchByIndex('ApplicantNameStored',a_Applicant,a_Applicant,true);
          extension.pkg_cxproceduralsearch.SearchByIndex('dup_AllContractorsName',a_Applicant,a_Applicant,true);
          extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults2, 'or');

          /* Now AND these results with the previous search */
          if extension.pkg_cxproceduralsearch.SearchPerformed then
            if t_AndOr = 'and' then
              extension.pkg_collectionutils.join(t_TempResults, t_TempResults2);
            else
              extension.pkg_collectionutils.Append(t_TempResults, t_TempResults2);
            end if;
          end if;
          t_TempResults2 := api.udt_objectlist();
          
        end if;

       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
       
    end if;
    
    if (a_GeneralPermit = 'Y') then


  -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
    t_Status := GetStatusNameForDescription(a_Status, t_GeneralPermitTypeId);


        extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
        extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_PermitNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     a_IssuedStartDate,
                                                     a_IssuedEndDate,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       extension.pkg_cxproceduralsearch.SearchByIndex ('ExpirationDate', a_ExpirationStartDate, a_ExpirationEndDate);
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);

       /* we need to add an AND (applicant OR contractor = <name>) to the end of this search */
       if t_TempResults.Count > 0 then
         t_AndOr := 'and';
       else
         if extension.pkg_cxproceduralsearch.SearchPerformed then
           /* Doing this search would be pointless */
           t_AndOr := 'skip';
         else
           t_AndOr := 'or';
         end if;
       end if;

       If a_Applicant is not null and t_AndOr <> 'skip' then

          extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
          extension.pkg_cxproceduralsearch.SearchByIndex('ApplicantNameStored',a_Applicant,a_Applicant,true);
          extension.pkg_cxproceduralsearch.SearchByIndex('dup_AllContractorsName',a_Applicant,a_Applicant,true);
          extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults2, 'or');

          /* Now AND these results with the previous search */
          if extension.pkg_cxproceduralsearch.SearchPerformed then
            if t_AndOr = 'and' then
              extension.pkg_collectionutils.join(t_TempResults, t_TempResults2);
            else
              extension.pkg_collectionutils.Append(t_TempResults, t_TempResults2);
            end if;
          end if;
          t_TempResults2 := api.udt_objectlist();
          
        end if;

       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
    end if;
    if (a_TradePermit = 'Y') then

  -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
    t_Status := GetStatusNameForDescription(a_Status, t_TradePermitTypeId);


        extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
        extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_PermitNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     a_IssuedStartDate,
                                                     a_IssuedEndDate,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       extension.pkg_cxproceduralsearch.SearchByIndex ('ExpirationDate', a_ExpirationStartDate, a_ExpirationEndDate);
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);

       /* we need to add an AND (applicant OR contractor = <name>) to the end of this search */
       if t_TempResults.Count > 0 then
         t_AndOr := 'and';
       else
         if extension.pkg_cxproceduralsearch.SearchPerformed then
           /* Doing this search would be pointless */
           t_AndOr := 'skip';
         else
           t_AndOr := 'or';
         end if;
       end if;

       If a_Applicant is not null and t_AndOr <> 'skip' then

          extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
          extension.pkg_cxproceduralsearch.SearchByIndex('ApplicantNameStored',a_Applicant,a_Applicant,true);
          extension.pkg_cxproceduralsearch.SearchByIndex('dup_AllContractorsName',a_Applicant,a_Applicant,true);
          extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults2, 'or');

          /* Now AND these results with the previous search */
          if extension.pkg_cxproceduralsearch.SearchPerformed then
            if t_AndOr = 'and' then
              extension.pkg_collectionutils.join(t_TempResults, t_TempResults2);
            else
              extension.pkg_collectionutils.Append(t_TempResults, t_TempResults2);
            end if;
          end if;
          t_TempResults2 := api.udt_objectlist();
          
        end if;

       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
    end if;
    if (a_PermitApplication = 'Y') then


  -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
    t_Status := GetStatusNameForDescription(a_Status, t_PermitBundleTypeId);
        extension.pkg_cxproceduralsearch.InitializeSearch('j_PermitBundle');
        extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_PermitNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     a_IssuedStartDate,
                                                     a_IssuedEndDate,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);
       /* we need to add an AND (applicant OR contractor = <name>) to the end of this search */
       if t_TempResults.Count > 0 then
         t_AndOr := 'and';
       else
         if extension.pkg_cxproceduralsearch.SearchPerformed then
           /* Doing this search would be pointless */
           t_AndOr := 'skip';
         else
           t_AndOr := 'or';
         end if;
       end if;

       If a_Applicant is not null and t_AndOr <> 'skip' then

          extension.pkg_cxproceduralsearch.InitializeSearch('j_PermitBundle');
          extension.pkg_cxproceduralsearch.SearchByIndex('ApplicantNameStored',a_Applicant,a_Applicant,true);
          extension.pkg_cxproceduralsearch.SearchByIndex('dup_AllContractorsName',a_Applicant,a_Applicant,true);
          extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults2, 'or');

          /* Now AND these results with the previous search */
          if extension.pkg_cxproceduralsearch.SearchPerformed then
            if t_AndOr = 'and' then
              extension.pkg_collectionutils.join(t_TempResults, t_TempResults2);
            else
              extension.pkg_collectionutils.Append(t_TempResults, t_TempResults2);
            end if;
          end if;
          t_TempResults2 := api.udt_objectlist();
          
        end if;
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
       
    end if;
    
    --Check if we should include map criteria
    if (a_UseMapCriteria = 'Y') then
      if a_ParcelNumberList is not null              
        then
          t_MapParcelNumbers := extension.pkg_Utils.Split(a_ParcelNumberList, ',');
          
          for i in 1..t_MapParcelNumbers.count loop
            for a in (select j.JobId
                        from api.Jobs j
                        join api.Relationships r on r.FromObjectId = j.JobId
                        join bcpdata.Address a on a.PosseObjectId = r.ToObjectId 
                        join api.JobTypes jt on jt.JobTypeId = j.JobTypeId
                        where a.TaxParcelNumber = t_MapParcelNumbers(i)
                        and jt.Name in ('j_BuildingPermit',
                                        'j_GeneralPermit',
                                        'j_TradePermit')) loop
              t_MapCriteriaResults.extend(1);
              t_MapCriteriaResults(t_MapCriteriaResults.count) := api.udt_Object(a.JobId);       
            end loop;
          end loop;
      end if;
      extension.pkg_collectionutils.Join(a_Results, t_MapCriteriaResults);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * FindPermit2() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermit2 (
    a_PermitNumber               varchar2,
    a_BuildingPermit             varchar2,
    a_TradePermit                varchar2,
    a_GeneralPermit              varchar2,
    a_Results               out  api.udt_ObjectList
  ) is
  begin
    a_Results       := api.udt_ObjectList();
    if (a_BuildingPermit = 'Y') then
        for c in (select j.JobID
                    from query.j_BuildingPermit j
                   where j.ExternalFileNum like a_PermitNumber) loop

          a_Results.extend(1);
          a_Results(a_Results.count) := api.udt_Object(c.JobId);
        end loop;
    end if;
    if (a_GeneralPermit = 'Y') then
        for c in (select j.JobID
                    from query.j_GeneralPermit j
                   where j.ExternalFileNum like a_PermitNumber) loop

          a_Results.extend(1);
          a_Results(a_Results.count) := api.udt_Object(c.JobId);
        end loop;
    end if;
    if (a_TradePermit = 'Y') then
        for c in (select j.JobID
                    from query.j_TradePermit j
                   where j.ExternalFileNum like a_PermitNumber) loop

          a_Results.extend(1);
          a_Results(a_Results.count) := api.udt_Object(c.JobId);
        end loop;
    end if;
  end;


  /*---------------------------------------------------------------------------
   * FindPermit3() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermit3 (
    a_PermitNumber               varchar2,
    a_ProjectName                varchar2,
    a_Results               out  api.udt_ObjectList
  ) is
    t_TempResults                api.udt_ObjectList;

  begin
    a_Results := api.udt_ObjectList();
    t_TempResults   := api.udt_ObjectList();

    extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PermitNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.SearchByKeywords(a_ProjectName);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();

    extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PermitNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.SearchByKeywords(a_ProjectName);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();

    extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PermitNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.SearchByKeywords(a_ProjectName);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();

  end FindPermit3;

  /*---------------------------------------------------------------------------
   * FindPermitByNumber() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermitByNumber (
    a_PermitNumber               varchar2,
    a_Results               out  api.udt_ObjectList
  ) is
    t_TempResults                api.udt_ObjectList;

  begin
    a_Results := api.udt_ObjectList();
    t_TempResults   := api.udt_ObjectList();


    --If the first occurance of the percent sign is before the forth character is supplied raise an error
    if  REGEXP_INSTR(a_PermitNumber,'%',1,1,0) = 1 or REGEXP_INSTR(a_PermitNumber,'_',1,1,0) = 1 then
      api.pkg_errors.RaiseError(-20000,'The first character supplied must be an exact match');
    end if;  

    extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PermitNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();

    extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PermitNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();

    extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PermitNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();

  end FindPermitByNumber;
  
  /*---------------------------------------------------------------------------
   * FindPermitByContractor() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermitByContractor (
    a_ContractorName        varchar2,
    a_PermitType            varchar2,
    a_Results               out  api.udt_ObjectList
  ) is
    t_TempResults                api.udt_ObjectList;

  begin
    a_Results := api.udt_ObjectList();
    t_TempResults   := api.udt_ObjectList();
    
    app.pkg_Debug.PutLine('Contractor Name: ' || a_ContractorName);
    app.pkg_Debug.PutLine('Permit Type: ' || a_PermitType);
    
    --If the first occurance of the percent sign is before the forth character is supplied raise an error
    if  REGEXP_INSTR(a_ContractorName,'%',1,1,0) = 1 or REGEXP_INSTR(a_ContractorName,'_',1,1,0) = 1 then
      api.pkg_errors.RaiseError(-20000,'The first character supplied must be an exact match');
    end if;  
    
    if a_PermitType is null or a_PermitType = 'Building Permit' then
      extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('BuildingPermitContractor','BusinessName',a_ContractorName,a_ContractorName,true);
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
      extension.pkg_collectionutils.append(a_Results, t_TempResults);
      t_TempResults := api.udt_ObjectList();
    end if;
    
    if a_PermitType is null or a_PermitType = 'General Permit' then
      extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('GeneralPermitContractor','BusinessName',a_ContractorName,a_ContractorName,true);      
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
      extension.pkg_collectionutils.append(a_Results, t_TempResults);
      t_TempResults := api.udt_ObjectList();
    end if;
    
    if a_PermitType is null or a_PermitType = 'Trade Permit' then
      extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('TradePermitContractor','BusinessName',a_ContractorName,a_ContractorName,true);
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
      extension.pkg_collectionutils.append(a_Results, t_TempResults);
      t_TempResults := api.udt_ObjectList();
    end if;  

  end FindPermitByContractor;
  
  /*---------------------------------------------------------------------------
   * FindPermitByAddress2() -- PUBLIC
   *------------------------------------------------------------------------- */
   procedure FindPermitByAddress2 (
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_PermitType       varchar2 default null,
    a_Results           out api.udt_ObjectList 
  )is
     t_TempResults                api.udt_ObjectList;
  
  begin
    
  a_Results := api.udt_ObjectList();
    t_TempResults   := api.udt_ObjectList();
    
    if trim(a_StreetName) is null then
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
      end if;
    end if;
    if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
      null;
    else
      api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
    end if;
    
    if a_PermitType is null or a_PermitType = 'Building Permit' then    
      extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
      if a_HouseNumber is not null then
        extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
      else
        extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
      end if;
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
      extension.pkg_collectionutils.append(a_Results, t_TempResults);
      t_TempResults := api.udt_ObjectList();
    end if;  
    
    if a_PermitType is null or a_PermitType = 'General Permit' then
      extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
      if a_HouseNumber is not null then
        extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
      else
        extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
      end if;
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
      extension.pkg_collectionutils.append(a_Results, t_TempResults);
      t_TempResults := api.udt_ObjectList();
    end if;
    
    if a_PermitType is null or a_PermitType = 'Trade Permit' then 
      extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
      if a_HouseNumber is not null then
        extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
      else
        extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
      end if;
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
      extension.pkg_collectionutils.append(a_Results, t_TempResults);
      t_TempResults := api.udt_ObjectList();
    end if;  
    
  end FindPermitByAddress2;    
  
  /*---------------------------------------------------------------------------
   * FindPermitByDate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPermitByDate (
    a_PermitType            varchar2,
    a_CreatedDateFrom       date,
    a_CreatedDateTo         date,    
    a_IssuedDateFrom        date,
    a_IssuedDateTo          date,
    a_Results               out  api.udt_ObjectList
  ) is
    t_TempResults                api.udt_ObjectList;

  begin
    a_Results := api.udt_ObjectList();
    t_TempResults   := api.udt_ObjectList();
    
    app.pkg_Debug.PutLine('Created DateFrom: ' || a_CreatedDateFrom);
    app.pkg_Debug.PutLine('Created DateTo: ' || a_CreatedDateTo);
    app.pkg_Debug.PutLine('Issued DateFrom: ' || a_IssuedDateFrom);
    app.pkg_Debug.PutLine('Issued DateTo: ' || a_IssuedDateTo);
    app.pkg_Debug.PutLine('Permit Type: ' || a_PermitType);    

    if a_PermitType is null or a_PermitType = 'Building Permit' then
          extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
          extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                       null,
                                                       null,
                                                       null,
                                                       a_CreatedDateFrom,
                                                       a_CreatedDateTo,
                                                       a_IssuedDateFrom,
                                                       a_IssuedDateTo,
                                                       null,
                                                       null);
          extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
          extension.pkg_collectionutils.append(a_Results, t_TempResults);
          t_TempResults := api.udt_ObjectList();

    End if;
    
    if a_PermitType is null or a_PermitType = 'General Permit' then
          extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
          extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                       null,
                                                       null,
                                                       null,
                                                       a_CreatedDateFrom,
                                                       a_CreatedDateTo,
                                                       a_IssuedDateFrom,
                                                       a_IssuedDateTo,
                                                       null,
                                                       null);
          extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
          extension.pkg_collectionutils.append(a_Results, t_TempResults);
          t_TempResults := api.udt_ObjectList();

    End If;

    if a_PermitType is null or a_PermitType = 'Trade Permit' then
          extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
          extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                       null,
                                                       null,
                                                       null,
                                                       a_CreatedDateFrom,
                                                       a_CreatedDateTo,
                                                       a_IssuedDateFrom,
                                                       a_IssuedDateTo,
                                                       null,
                                                       null);
          extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
          extension.pkg_collectionutils.append(a_Results, t_TempResults);
          t_TempResults := api.udt_ObjectList();
    End If;
    
  end FindPermitByDate;  

  /*---------------------------------------------------------------------------
   * FindInspectors() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspectors (
    a_Dummy                      int,
    a_Results               out api.udt_ObjectList
  ) is
  begin
    a_Results := api.udt_ObjectList();

    for c in (select USR.UserId
                from api.Users USR,
                     api.AccessGroupUsers AGU,
                     api.AccessGroups AG
               where AG.Description = 'Inspector'
                 and AGU.AccessGroupId = AG.AccessGroupId
                 and USR.UserId = AGU.UserId) loop

      a_Results.extend(1);
      a_Results(a_Results.count) := api.udt_Object(c.UserId);
    end loop;
  end;

   /*---------------------------------------------------------------------------
   * FindInspectionSettings() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspectionSettings (
    a_Results               out api.udt_ObjectList
  ) is
  begin
    a_Results := api.udt_ObjectList();

    a_Results.extend(1);
    a_Results(a_Results.count) := api.udt_Object(bcpdata.Pkg_BCPSetting.InspectionSettingsObjectId('o_InspectionSettings'));
  end FindInspectionSettings;

  /*---------------------------------------------------------------------------
   * FindUserProcesses() -- PUBLIC
   *-------------------------------------------------------------------------*/

  procedure FindUserProcesses(
	  a_User               varchar2,
    a_ProcessType        varchar2,
--    a_Investigations     varchar2,
    a_JobExternalFileNum varchar2,
    a_DateCompletedFrom  date,
    a_DateCompletedTo    date,
    a_RelatedObjectIds   out nocopy api.udt_ObjectList
  ) is
    t_TodoListUserId         number(9);
    t_SearchingUserId        number(9);
    t_DateCompletedFrom      date := a_DateCompletedFrom;
    t_DateCompletedTo        date := a_DateCompletedTo;
--    t_ViewableOfficeIdList   api.pkg_definition.udt_IdList;
--    t_OfficeFilteredUserList udt_ObjectList;
    t_FinalUserList          udt_ObjectList;
    t_LoopCount              number;
    t_InvalidLoopCount       number;

    --set user list to search based on entered user criteria or on the current user when no user criteria is entered.
    cursor c_CurrentUser is
      select users.UserId
        from api.users users
       where (Lower(users.Name) like Lower(a_User || '%')
			   and a_User is not null)
          or (users.userid = t_SearchingUserId
			   and a_user is null);
  begin

    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();

    --Set date range when none is specified.
    if a_DateCompletedFrom is not null
		and a_DateCompletedTo is null then
      t_DateCompletedTo := sysdate;
    elsif a_DateCompletedFrom is null
		and a_DateCompletedTo is not null then
      t_DateCompletedFrom := to_date('01/01/1900', 'mm/dd/yyyy');
    end if;

    a_RelatedObjectIds := api.udt_objectlist();
    t_FinalUserList    := api.udt_ObjectList();

    --if User criteria is specified in the search and the user's access group might allow seeing other users
    --then set the list of acceptible resulting users accordingly.  If those conditions are not met, then set the
    --allowed users to only include the logged in user.
--    if a_User is not null
--		and api.pkg_columnquery.value(t_SearchingUserId, 'AccessGroupRankNumber') <> 4 then
      --when user might be able to view more than it's own todo list, then:
      --Get list of offices that the searching user can see results for.
--      FindUserOffices(t_SearchingUserId, t_ViewableOfficeIdList);

--      t_OfficeFilteredUserList := api.udt_ObjectList();

      --Create idlist of all users in the searching user's valid offices.
/*      for c in 1 .. t_ViewableOfficeIdList.count loop
        for d in (select u.userid UserId
                    from query.users u
                   where u.OfficeIdList like '%' || t_ViewableOfficeIdList(c) || '%') loop
          t_OfficeFilteredUserList.extend(1);
          t_OfficeFilteredUserList(t_OfficeFilteredUserList.Count) := api.udt_Object(d.UserId);
        end loop;
      end loop;
*/
      --Limit above idlist to only those users that the searching user has accessgroup security to see.
/*      for c2 in 1 .. t_OfficeFilteredUserList.count loop
        if api.pkg_columnquery.value(t_OfficeFilteredUserList(c2).objectid, 'AccessGroupRankNumber') >=
           api.pkg_columnquery.value(t_SearchingUserId, 'AccessGroupRankNumber') then
          t_FinalUserList.extend(1);
          t_FinalUserList(t_FinalUserList.Count) := api.udt_Object(t_OfficeFilteredUserList(c2).objectid);
        else
          null;
        end if;
      end loop; --final valid user idlist.
      
    else
      --when user can only see own todo list or when no User criteria is entered, then:
      t_FinalUserList.extend(1);
      t_FinalUserList(t_FinalUserList.Count) := api.udt_Object(t_SearchingUserId); --final valid user idlist (set to current user.).
    end if;
*/
    --these details are later used to raise error
    t_LoopCount        := 0;
    t_InvalidLoopCount := 0;

    --loop through users from search criteria
    for r in c_CurrentUser loop
      t_TodoListUserId := r.userid;

      --Verify that user is in valid user list
--      for c3 in 1 .. t_FinalUserList.count loop
--        if t_TodoListUserId = t_FinalUserList(c3).objectid then
          --If searched todo list is okay for user to see, return todo list.
          for p in (select pr.processid
                      from api.processassignments pa
                      join api.processes pr on pr.processid = pa.processid
                      join api.processtypes pt on pt.processtypeid = pr.processtypeid
                      join api.jobs jobs on jobs.jobid = pr.jobid
                     where pa.UserId = t_TodoListUserId
                       and pr.outcome is null
                       and (a_ProcessType is null
--											 and nvl(a_Investigations, 'N') = 'N'
											  or (a_ProcessType is not null
                       and pt.Description = a_ProcessType)
--											  or (a_Investigations = 'Y'
--											 and pt.processTypeId in (api.pkg_configquery.ObjectDefIdForName('p_ConductEnforcementInv'),
--                                                api.pkg_configquery.ObjectDefIdForName('p_ConductLicenseInvestigation'),
--                                                api.pkg_configquery.ObjectDefIdForName('p_ConductAdminInvestigation'),
--                                                api.pkg_configquery.ObjectDefIdForName('p_ConductAppInvestigation'))))
                       and (jobs.ExternalFileNum like a_JobExternalFileNum || '%'
											  or a_JobExternalFileNum is null)
                       and (pr.DateCompleted >= t_DateCompletedFrom
											  or (pr.DateCompleted is null
											 and t_DateCompletedFrom is null))
                       and (pr.DateCompleted < t_DateCompletedTo + 1
											  or (pr.DateCompleted is null
											 and t_DateCompletedTo is null)))) loop
            a_RelatedObjectIds.extend(1);
            a_RelatedObjectIds(a_RelatedObjectIds.count) := api.udt_object(p.processid);
          end loop; --to get User's todo list
--        else
          --Count each time the loop is done when the searching user is not allowed to see a user from the criteria
--          t_InvalidLoopCount := t_InvalidLoopCount + 1;
--        end if;
        t_LoopCount := t_LoopCount + 1; --Count each time the loop is done for all users from the criteria
      end loop;
--    end loop; --for each user from search criteria

    -- If valid users where entered in the search criteria (the loop was run) but the searching user was not allowed to see any of them, raise error.
    if t_LoopCount = t_InvalidLoopCount and t_LoopCount <> 0 then
      api.pkg_errors.RaiseError(-20000, 'You do not have security to view the To-Do List of the user(s) you have searched for.');
    end if;
  end FindUserProcesses;
  
 /*---------------------------------------------------------------------------
   * FindPlanningJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPlanningJob (
    a_FileNumber               varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results               out  api.udt_ObjectList
  ) is
  t_TempResults             api.udt_ObjectList;
  t_Status                  varchar2(500);
  t_RezoningJobId    number := api.pkg_configquery.objectdefidforname('j_Rezoning');

  begin
    a_Results     := api.udt_ObjectList();
    t_TempResults := api.udt_ObjectList();


  -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
    t_Status := GetStatusNameForDescription(a_Status, t_RezoningJobId);
/*    if a_Status is not null then
      begin
        select s.tag
          into t_Status
          from api.statuses s
          join api.jobstatuses js 
            on js.StatusId = s.StatusId
            and js.JobTypeId = t_RezoningJobId
         where s.Description = a_Status;
      exception when too_many_rows then
        api.pkg_errors.RaiseError(-20000, 'Multiple statuses defined in configuration for description ' || a_Status);
      when no_data_found then
        api.pkg_errors.RaiseError(-20000, '''' || a_Status || ''' is not a valid status for this search.');
      end;
    end if;*/
    
          --if we are searching by address
    if a_StreetName is not null
    or a_housenumber is not null
    or a_fromhousenumber is not null
    or a_tohousenumber is not null
    or a_suite is not null then

    --check that we have valid search criterion
      if trim(a_StreetName) is null then
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;

    end if; --if we are searching by address

        extension.pkg_cxproceduralsearch.InitializeSearch('j_Rezoning');
        extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_FileNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     null,
                                                     null,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
        if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
       
       
       extension.pkg_cxproceduralsearch.InitializeSearch('j_Subdivision');
       extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_FileNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     null,
                                                     null,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;                                                     
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
       
       extension.pkg_cxproceduralsearch.InitializeSearch('j_UseBySpecialReview');
       extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_FileNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     null,
                                                     null,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
        if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
       
       extension.pkg_cxproceduralsearch.InitializeSearch('j_Presubmittal');
       extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_FileNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     null,
                                                     null,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;                                                     
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
       
       extension.pkg_cxproceduralsearch.InitializeSearch('j_VarianceAndAppeals');
       extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_FileNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     null,
                                                     null,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;                                                     
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
       
       extension.pkg_cxproceduralsearch.InitializeSearch('j_SiteImprovementPlan');
       extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_FileNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     null,
                                                     null,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;                                                     
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist(); 
       
       extension.pkg_cxproceduralsearch.InitializeSearch('j_DevelopmentPermit');
        extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_FileNumber,
                                                     t_Status,
                                                     a_CreatedStartDate,
                                                     a_CreatedEndDate,
                                                     null,
                                                     null,
                                                     a_CompletedStartDate,
                                                     a_CompletedEndDate);
       if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;                                                     
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist(); 

  end;
  
   /*---------------------------------------------------------------------------
   * FindPermitByNumber() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPlanningByNumber (
    a_PlanningNumber               varchar2,
    a_Results               out  api.udt_ObjectList
  ) is
    t_TempResults                api.udt_ObjectList;

  begin
    a_Results := api.udt_ObjectList();
    t_TempResults   := api.udt_ObjectList();


    --If the first occurance of the percent sign is before the forth character is supplied raise an error
    if  REGEXP_INSTR(a_PlanningNumber,'%',1,1,0) = 1 or REGEXP_INSTR(a_PlanningNumber,'_',1,1,0) = 1 then
      api.pkg_errors.RaiseError(-20000,'The first character supplied must be an exact match');
    end if;  

    extension.pkg_cxproceduralsearch.InitializeSearch('j_Rezoning');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PlanningNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_Subdivision');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PlanningNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_UseBySpecialReview');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PlanningNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_Presubmittal');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PlanningNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_VarianceAndAppeals');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PlanningNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_SiteImprovementPlan');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_PlanningNumber,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();

  end FindPlanningByNumber;
  
/*---------------------------------------------------------------------------
   * FindPlanningByDate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindPlanningByDate (
    a_CreatedDateFrom       date,
    a_CreatedDateTo         date,    
    a_IssuedDateFrom        date,
    a_IssuedDateTo          date,
    a_Results               out  api.udt_ObjectList
  ) is
    t_TempResults                api.udt_ObjectList;

  begin
    a_Results := api.udt_ObjectList();
    t_TempResults   := api.udt_ObjectList();   

    extension.pkg_cxproceduralsearch.InitializeSearch('j_Rezoning');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 null,
                                                 null,
                                                 a_CreatedDateFrom,
                                                 a_CreatedDateTo,
                                                 a_IssuedDateFrom,
                                                 a_IssuedDateTo,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_Subdivision');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 null,
                                                 null,
                                                 a_CreatedDateFrom,
                                                 a_CreatedDateTo,
                                                 a_IssuedDateFrom,
                                                 a_IssuedDateTo,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_UseBySpecialReview');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 null,
                                                 null,
                                                 a_CreatedDateFrom,
                                                 a_CreatedDateTo,
                                                 a_IssuedDateFrom,
                                                 a_IssuedDateTo,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_Presubmittal');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 null,
                                                 null,
                                                 a_CreatedDateFrom,
                                                 a_CreatedDateTo,
                                                 a_IssuedDateFrom,
                                                 a_IssuedDateTo,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_VarianceAndAppeals');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 null,
                                                 null,
                                                 a_CreatedDateFrom,
                                                 a_CreatedDateTo,
                                                 a_IssuedDateFrom,
                                                 a_IssuedDateTo,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_SiteImprovementPlan');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 null,
                                                 null,
                                                 a_CreatedDateFrom,
                                                 a_CreatedDateTo,
                                                 a_IssuedDateFrom,
                                                 a_IssuedDateTo,
                                                 null,
                                                 null);
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();

  end FindPlanningByDate;  
  
  /*---------------------------------------------------------------------------
   * FindPlanningByAddress() -- PUBLIC
   *------------------------------------------------------------------------- */
   procedure FindPlanningByAddress (
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_Results           out api.udt_ObjectList 
  )is
   t_TempResults                api.udt_ObjectList;
   t_TempResults2               api.pkg_definition.Udt_Idlist;
   t_TempResults3               api.pkg_definition.Udt_Idlist;
   t_TempResults4               api.pkg_definition.udt_IdList;
  
  begin
    
  a_Results := api.udt_ObjectList();
  t_TempResults   := api.udt_ObjectList();
    
    if trim(a_StreetName) is null then
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
      end if;
    end if;
    if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
      null;
    else
      api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
    end if;
    
    
      extension.pkg_cxproceduralsearch.InitializeSearch('o_Address');
      if a_HouseNumber is not null then
        extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
      else
        extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
      end if;
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
      
      for c in 1..t_TempResults.Count loop
        t_TempResults2 := extension.pkg_cxproceduralsearch.RelatedObjects(t_TempResults(c).objectid, api.pkg_configquery.EndPointIdForName('o_Address','Parcel'));
        for d in 1..t_TempResults2.Count loop
          t_TempResults3(nvl(t_TempResults3.Last+1,1)) := t_TempResults2(d);
        end loop;
      end loop;
      
      for e in 1..t_TempResults3.Count loop
        t_TempResults2 := extension.pkg_cxproceduralsearch.RelatedObjects(t_TempResults3(e), api.pkg_configquery.EndPointIdForName('o_Parcel', 'UseBySpecialReview'));
        for f in 1..t_TempResults2.Count loop
          t_TempResults4(nvl(t_TempResults4.Last+1,1)) := t_TempResults2(f);
        end loop;
      end loop;
      
      for g in 1..t_TempResults3.Count loop
        t_TempResults2 := extension.pkg_cxproceduralsearch.RelatedObjects(t_TempResults3(g), api.pkg_configquery.EndPointIdForName('o_Parcel', 'Rezoning'));
        for h in 1..t_TempResults2.Count loop
          t_TempResults4(nvl(t_TempResults4.Last+1,1)) := t_TempResults2(h);
        end loop;
      end loop;
      
      for i in 1..t_TempResults3.Count loop
        t_TempResults2 := extension.pkg_cxproceduralsearch.RelatedObjects(t_TempResults3(i), api.pkg_configquery.EndPointIdForName('o_Parcel', 'Subdivision'));
        for j in 1..t_TempResults2.Count loop
          t_TempResults4(nvl(t_TempResults4.Last+1,1)) := t_TempResults2(j);
        end loop;
      end loop;
      
      t_TempResults := api.udt_ObjectList();
      for k in 1..t_TempResults4.Count loop
        t_TempResults.Extend();
        t_TempResults(t_TempResults.Last) := api.udt_Object(t_TempResults4(k));  
      end loop;
     
     extension.pkg_collectionutils.append(a_Results, t_TempResults);
      
    
  end FindPlanningByAddress;        
  
  /*---------------------------------------------------------------------------
   * FindPermitByAddress() -- PUBLIC
   *------------------------------------------------------------------------- */
   procedure FindPermitByAddress (
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_ExactMatch        varchar2 default null,
    a_Results           out api.udt_ObjectList 
  )is
     t_TempResults                api.udt_ObjectList;
  
  begin
    
  a_Results := api.udt_ObjectList();
    t_TempResults   := api.udt_ObjectList();
    
    if trim(a_StreetName) is null then
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
      end if;
    end if;
    if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
      null;
    else
      api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
    end if;
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
    if a_HouseNumber is not null then
      extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    else
      extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    end if;
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
    if a_HouseNumber is not null then
      extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    else
      extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    end if;
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
    if a_HouseNumber is not null then
      extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    else
      extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    end if;
    extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults, 'and');
    extension.pkg_collectionutils.append(a_Results, t_TempResults);
    t_TempResults := api.udt_ObjectList();
    
    

    
  end;
  /*---------------------------------------------------------------------------
   * FindPermitPublic() -- PUBLIC
   *-------------------------------------------------------------------------*/

  procedure FindPermitPublic (
    a_ExternalFileNum            varchar2,
    a_Description                varchar2 default null,
    a_BuildingPermit             varchar2 default null,
    a_TradePermit                varchar2 default null,
    a_GeneralPermit              varchar2 default null,
    a_AllContractorsName         varchar2 default null,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_ExactMatch                 varchar2 default null,
    a_Results               out  api.udt_ObjectList
  ) is
  t_TempResults             api.udt_ObjectList;


  begin
    a_Results     := api.udt_ObjectList();
    t_TempResults := api.udt_ObjectList();
    
    if a_ExternalFileNum is null and a_Description is null and a_AllContractorsName is null
      then bcpdata.pkg_bcpsearch.FindPermitByAddress (a_StreetName, a_HouseNumber, a_FromHouseNumber, a_ToHouseNumber, a_Suite, a_ExactMatch, a_Results);
     

   elsif (a_BuildingPermit = 'Y') then

        extension.pkg_cxproceduralsearch.InitializeSearch('j_BuildingPermit');
        extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_ExternalFileNum,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null);
       extension.pkg_cxproceduralsearch.SearchByIndex('Description',a_Description,a_Description);
       extension.pkg_cxproceduralsearch.SearchByIndex('dup_AllContractorsName',a_AllContractorsName,a_AllContractorsName);
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
end if;

if (a_GeneralPermit = 'Y') then

        extension.pkg_cxproceduralsearch.InitializeSearch('j_GeneralPermit');
        extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_ExternalFileNum,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null);
       extension.pkg_cxproceduralsearch.SearchByIndex('Description',a_Description,a_Description);   
       extension.pkg_cxproceduralsearch.SearchByIndex('dup_AllContractorsName',a_AllContractorsName,a_AllContractorsName);                                           
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
 end if;
 
 if (a_TradePermit = 'Y')then

       extension.pkg_cxproceduralsearch.InitializeSearch('j_TradePermit');
       extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                     null,
                                                     a_ExternalFileNum,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null,
                                                     null);
       extension.pkg_cxproceduralsearch.SearchByIndex('Description',a_Description,a_Description);  
       extension.pkg_cxproceduralsearch.SearchByIndex('dup_AllContractorsName',a_AllContractorsName,a_AllContractorsName);                                            
       extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);
       extension.pkg_collectionutils.Append(a_Results, t_TempResults);
       t_TempResults := api.udt_objectlist();
 end if;
  
 end;


   /*---------------------------------------------------------------------------
   * FindProcessEmail() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindProcessEmail (
    a_Description           varchar2,
    a_Results               out api.udt_ObjectList
  ) is
  begin
    a_Results := api.udt_ObjectList();

    extension.pkg_cxproceduralsearch.InitializeSearch('o_ProcessEmail');

    if a_Description is null 
    or a_Description = '%'
      then
        extension.pkg_cxproceduralsearch.SearchByIndex('Name', '%', null, True);
      else
        extension.pkg_cxproceduralsearch.SearchByKeywords(a_Description);
    end if;

    extension.pkg_cxproceduralsearch.PerformSearch(a_Results);

--    a_Results.extend(1);
--    a_Results(a_Results.count) := api.udt_Object(bcpdata.Pkg_BCPSetting.SystemSettingsObjectId('o_SystemSettings'));
  end;


  /*---------------------------------------------------------------------------
   * FindComplaint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindComplaint (
    a_ComplaintNumber            varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_ComplaintDescription       varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results                    out api.udt_ObjectList
  )is
    t_Status           varchar2(4000);
    t_JobTypeId        udt_Id := api.pkg_configquery.objectdefidforname('j_LMS_Complaint');
    t_FirstResults     api.udt_ObjectList;

  begin
    t_Status         := pkg_BCPSearch.GetStatusNameForDescription(a_Status, t_JobTypeId);
    a_Results        := api.udt_ObjectList();
    t_FirstResults   := api.udt_ObjectList();
    
    
      --if we are searching by address
    if a_StreetName is not null
    or a_housenumber is not null
    or a_fromhousenumber is not null
    or a_tohousenumber is not null
    or a_suite is not null then

    --check that we have valid search criterion
      if trim(a_StreetName) is null then
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;

    end if; --if we are searching by address

  --search by keyword
    extension.pkg_cxproceduralsearch.InitializeSearch('j_LMS_Complaint');
    extension.pkg_cxproceduralsearch.SearchByKeywords(a_ComplaintDescription);
    extension.pkg_cxproceduralsearch.PerformSearch(t_FirstResults);

  --search by standard indexes
    extension.pkg_cxproceduralsearch.InitializeSearch('j_LMS_Complaint');
    extension.pkg_cxproceduralsearch.SearchByJob(null, 
                                                 null, 
                                                 a_ComplaintNumber, 
                                                 t_Status, 
                                                 a_CreatedStartDate, 
                                                 a_CreatedEndDate, 
                                                 null, 
                                                 null, 
                                                 a_CompletedStartDate,
                                                 a_CompletedEndDate);
        if a_HouseNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
        else
          extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
        end if;
        
       extension.pkg_cxproceduralsearch.PerformSearch(a_Results, 'and');
    
    

  --join the two lists where there the results from both arrays are the same
    if t_FirstResults.Count > 0 and a_Results.Count > 0
      then
        extension.pkg_collectionutils.Join(a_Results, t_FirstResults);
      else
        extension.pkg_collectionutils.Append(a_Results, t_FirstResults);
    end if;

  end FindComplaint;


 /*---------------------------------------------------------------------------
   * FindInvestigation() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInvestigation (
    a_InvestigationNumber       varchar2,
    a_CreatedStartDate          date,
    a_CreatedEndDate            date,
    a_CompletedStartDate        date,
    a_CompletedEndDate          date,
    a_Status                    varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results               out api.udt_ObjectList
  )is
    t_Status  varchar2(4000);
    t_JobTypeId  udt_Id := api.pkg_configquery.objectdefidforname('j_Investigation');
  begin
    t_Status := pkg_BCPSearch.GetStatusNameForDescription(a_Status, t_JobTypeId);
    a_Results := api.udt_ObjectList();
    
    --if we are searching by address
    if a_StreetName is not null
    or a_housenumber is not null
    or a_fromhousenumber is not null
    or a_tohousenumber is not null
    or a_suite is not null then

    --check that we have valid search criterion
      if trim(a_StreetName) is null then
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;

    end if; --if we are searching by address
    
    extension.pkg_cxproceduralsearch.InitializeSearch('j_Investigation');
    extension.pkg_cxproceduralsearch.SearchByJob(null, 
                                                 null, 
                                                 a_InvestigationNumber, 
                                                 t_Status, 
                                                 a_CreatedStartDate, 
                                                 a_CreatedEndDate, 
                                                 null, 
                                                 null, 
                                                 a_CompletedStartDate,
                                                 a_CompletedEndDate);
/*    extension.pkg_cxproceduralsearch.SearchByIndex('dup_ComplaintAddress', 
                                                   a_Address, 
                                                   null, 
                                                   true);*/
                                                   
    if a_HouseNumber is not null then
      extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
    else
      extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
     end if;
     
    extension.pkg_cxproceduralsearch.PerformSearch(a_Results, 'and');
    
  end FindInvestigation;

  /*---------------------------------------------------------------------------
   * FindViolationTypes() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindViolationTypes (
    a_Title               varchar2,
    a_Results               out api.udt_ObjectList
  )is
  begin

    a_Results := api.udt_ObjectList();
    extension.pkg_cxproceduralsearch.InitializeSearch('o_ViolationType');
    
    if a_Title is not null
      then
        extension.pkg_cxproceduralsearch.SearchByKeywords(a_Title);
    else
      extension.pkg_cxproceduralsearch.SearchByIndex('Title', '%', null);
    end if;
    extension.pkg_cxproceduralsearch.PerformSearch(a_Results);
  end FindViolationTypes;

  /*---------------------------------------------------------------------------
   * FindViolations() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindViolations (
    a_ExternalFileNumber         varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
--    a_IssuedStartDate            date,
--    a_IssuedEndDate              date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results             out api.udt_ObjectList
  )is

  t_Status      varchar2(4000);
  t_JobTypeId   udt_Id := api.pkg_configquery.ObjectDefIdForName('j_Violation');

  begin

    t_Status := pkg_BCPSearch.GetStatusNameForDescription(a_Status, t_JobTypeId);

    --if we are searching by address
    if a_StreetName is not null
    or a_housenumber is not null
    or a_fromhousenumber is not null
    or a_tohousenumber is not null
    or a_suite is not null then

    --check that we have valid search criterion
      if trim(a_StreetName) is null then
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;

    end if; --if we are searching by address

    a_Results := api.udt_ObjectList();
    extension.pkg_cxproceduralsearch.InitializeSearch('j_Violation');
    extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                 null,
                                                 a_ExternalFileNumber,
                                                 t_Status,
                                                 a_CreatedStartDate,
                                                 a_CreatedEndDate,
                                                 null, --a_IssuedStartDate,
                                                 null, --a_IssuedEndDate,
                                                 a_CompletedStartDate,
                                                 a_CompletedEndDate);

    if a_HouseNumber is not null then
      extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
    else
      extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
    end if;
        
       extension.pkg_cxproceduralsearch.PerformSearch(a_Results, 'and');

    --extension.pkg_cxproceduralsearch.PerformSearch(a_Results);
  end FindViolations;

  /*---------------------------------------------------------------------------
   * FindCaseFileViolations() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindCaseFileViolations (
    a_JobId               number,
    a_Results             out api.udt_ObjectList
  )is

  begin

    a_Results := api.udt_ObjectList();
     
     Select api.udt_Object(r.ViolationObjectId)
      bulk collect into a_Results
      from query.r_CaseFileViolations r
      join query.o_Violations v
        on v.ObjectId = r.ViolationObjectId
     where r.CaseFileJobId = a_JobId
       and v.ResolutionDate is null
       and v.OrderDate is null;
        
  end FindCaseFileViolations;

  /*---------------------------------------------------------------------------
   * GetStatusNameForDescription() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetStatusNameForDescription (
    a_StatusDescription         varchar2,
    a_JobTypeId                 udt_Id
  ) return varchar2 is

  t_StatusName  varchar2(4000);

  begin
  -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
    if a_StatusDescription is not null then
      begin
        select s.tag
          into t_StatusName
          from api.statuses s
          join api.jobstatuses js 
            on js.StatusId = s.StatusId
            and js.JobTypeId = a_JobTypeId
         where s.Description = a_StatusDescription;
      exception when too_many_rows then
        api.pkg_errors.RaiseError(-20000, 'Multiple statuses defined in configuration for description ' || a_StatusDescription);
      when no_data_found then
        api.pkg_errors.RaiseError(-20000, '''' || a_StatusDescription || ''' is not a valid status for this search.');
      end;
    end if;
    
    return t_StatusName;
  end GetStatusNameForDescription;
  
  /*---------------------------------------------------------------------------
   * FindContacts() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindContacts (
    a_Name              varchar2,
    a_LastName          varchar2,
    a_ObjectList         out    api.udt_ObjectList
  ) is
  begin
    a_Objectlist := api.udt_objectlist();
    extension.pkg_cxproceduralsearch.InitializeSearch('o_Contact');
    extension.pkg_cxproceduralsearch.SearchByIndex ('Name', a_Name, null, true);
    extension.pkg_cxproceduralsearch.SearchByIndex ('LastName', a_LastName, null, true);
    extension.pkg_cxproceduralsearch.PerformSearch (a_ObjectList, 'or');
  end FindContacts;

   /*---------------------------------------------------------------------------
   * ToDoList() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ToDoList (
    a_Results               out api.udt_ObjectList
  ) is
    t_IdList                    udt_IdList;
    t_SearchingUserId        number(9);
  begin
    a_Results := api.udt_ObjectList();

    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();

    select pa.ProcessId bulk collect into t_IdList
      from api.incompleteprocessassignments pa
      where pa.AssignedTo = t_SearchingUserId;

    a_Results := extension.pkg_Utils.ConvertToObjectList(t_IdList);

  end;

  -----------------------------------------------------------------------------
  -- ProcessTypeSearch
  --  Search for Process Types.
  -----------------------------------------------------------------------------
  procedure LMS_ProcessTypeSearch(
    a_Description                       varchar2,
    a_Name                              varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is

  t_IdList                              udt_IdList;

  begin

    if a_Description is null and a_Name is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    select pt.objectid
      bulk collect into t_IdList
      from query.o_processtype pt
     where (lower(pt.Description) like lower(a_description)||'%' or a_description is null)
       and (lower(pt.Name) like lower(a_name)||'%' or a_name is null)
       and lower(pt.Name) like 'p_%';
    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);

  end LMS_ProcessTypeSearch;

  -----------------------------------------------------------------------------
  -- LetterTemplateSearch
  --  Return Letter Templates.
  -----------------------------------------------------------------------------
  procedure LetterTemplateSearch (
    a_WordTemplateName                  varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is
    t_DocumentIdList                    udt_IdList;

  begin
    a_ObjectList := api.udt_ObjectList();

    select r.WordTemplateDocumentId
      bulk collect into t_DocumentIdList
      from query.r_SystemSettingsLetterTemplate r
        join query.d_WordInterfaceTemplate d
          on r.WordTemplateDocumentId = d.ObjectId
      where d.Name like a_WordTemplateName;

    if t_DocumentIdList.count > 0 then
      a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_DocumentIdList);
    end if;
  end LetterTemplateSearch;

  -----------------------------------------------------------------------------
  -- JobTypeSearch
  --  Search for Job Types.
  -----------------------------------------------------------------------------
  procedure LMS_JobTypeSearch(
    a_Description                       varchar2,
    a_Name                              varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is

  t_IdList                              udt_IdList;

  begin

    if a_Description is null and a_Name is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    select jt.objectid
      bulk collect into t_IdList
      from query.o_JobTypes jt
     where (lower(jt.Description) like lower(a_description)||'%' or a_description is null)
       and (lower(jt.Name) like lower(a_name)||'%' or a_name is null)
       and jt.IsLMS = 'Y';
    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);

  end LMS_JobTypeSearch;
  
  -----------------------------------------------------------------------------
  -- JobTypeProcessTypeSearch
  --  Search for Job Type Process Types.
  -----------------------------------------------------------------------------
  procedure JobTypeProcessTypeSearch(
    a_JobTypeId                         number,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is

  t_IdList                              udt_IdList;

  begin
    if a_JobTypeId is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    select jtpt.ObjectId
    bulk collect into t_IdList
    from query.o_JobTypeProcessTypes jtpt
    where jtpt.JobTypeId = a_JobTypeId;
     
    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);

  end JobTypeProcessTypeSearch;


  -----------------------------------------------------------------------------
  -- LMSPersonSearch
  --  Search for o_LMS_Person.
  -----------------------------------------------------------------------------
  procedure LMSPersonSearch(
    a_FormattedName                 varchar2,
    a_PhoneNumber                   varchar2,
    a_FaxNumber                     varchar2,
    a_EmailAddress                  varchar2,
    a_Objects                       out nocopy udt_ObjectList
  ) is
    t_FormattedName                 varchar2(500);
  begin
    a_Objects := api.udt_ObjectList();
    t_FormattedName := a_FormattedName;
    
    extension.pkg_cxproceduralsearch.InitializeSearch('o_LMS_Person');
    extension.pkg_cxproceduralsearch.SearchByIndex('dup_FormattedName', t_FormattedName, t_FormattedName, true);
    extension.pkg_cxproceduralsearch.SearchByIndex('PhoneNumber', a_PhoneNumber, a_PhoneNumber, false);
    extension.pkg_cxproceduralsearch.SearchByIndex('FaxNumber', a_FaxNumber, a_FaxNumber, false);
    extension.pkg_cxproceduralsearch.SearchByIndex('EmailAddress', a_EmailAddress, a_EmailAddress, false);
    
    extension.pkg_cxproceduralsearch.PerformSearch(a_Objects, 'and', 'none');
    
  end LMSPersonSearch;

   /*---------------------------------------------------------------------------
   * CandEJobSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
   procedure CandEJobSearch (
    a_FileNumber                 varchar2,
    a_Complaint                  varchar2,
    a_CaseFile                   varchar2,
    a_Order                      varchar2,
    a_CreatedStartDate           date,
    a_CreatedEndDate             date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_Status                     varchar2,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_LocationDescription        varchar2 default null,
    a_Results                    out  api.udt_ObjectList
  ) is
    t_TempResults             api.udt_ObjectList;
    t_Status                  varchar2(500);
    t_ComplaintTypeId         number := api.pkg_configquery.objectdefidforname('j_LMS_Complaint');
    t_CaseFileTypeId          number := api.pkg_configquery.objectdefidforname('j_LMS_CaseFile');
    t_OrderTypeId             number := api.pkg_configquery.objectdefidforname('j_LMS_Order');

  begin
    a_Results      := api.udt_ObjectList();
    t_TempResults  := api.udt_ObjectList();
    
    --If at least one of the Job Types is selected, other search criteria must also be selected
    if a_Complaint = 'Y' or 
       a_CaseFile  = 'Y' or 
       a_Order = 'Y' then
      
      if a_FileNumber is null and
         a_CreatedStartDate is null and
         a_CreatedEndDate is null and
         a_CompletedStartDate is null and
         a_CompletedEndDate is null and
         a_Status is null and
         a_StreetName is null and
         a_HouseNumber is null and
         a_FromHouseNumber is null and
         a_ToHouseNumber is null and
         a_Suite is null and
         a_LocationDescription is null then
             
         api.pkg_Errors.RaiseError(-20000, 'If a Job Type is selected, other search criteria must also be selected.');
      end if;
    end if;

    --if we are searching by address
    if a_StreetName is not null
    or a_housenumber is not null
    or a_fromhousenumber is not null
    or a_tohousenumber is not null
    or a_suite is not null then

      --check that we have valid search criterion
      if trim(a_StreetName) is null then
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
      
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;

    end if; --if we are searching by address

    if (a_Complaint = 'Y') then
      -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
      t_Status := GetStatusNameForDescription(a_Status, t_ComplaintTypeId);


      extension.pkg_cxproceduralsearch.InitializeSearch('j_LMS_Complaint');
      extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                   null,
                                                   a_FileNumber,
                                                   t_Status,
                                                   a_CreatedStartDate,
                                                   a_CreatedEndDate,
                                                   null,
                                                   null,
                                                   a_CompletedStartDate,
                                                   a_CompletedEndDate);
      
      if a_HouseNumber is not null then
        extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
      else
        extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
      end if;
              
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);
      
      extension.pkg_collectionutils.Append(a_Results, t_TempResults);
      t_TempResults := api.udt_objectlist();
 
    end if;
    
    if (a_CaseFile = 'Y') then

      -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
      t_Status := GetStatusNameForDescription(a_Status, t_CaseFileTypeId);

      extension.pkg_cxproceduralsearch.InitializeSearch('j_LMS_CaseFile');
      extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                   null,
                                                   a_FileNumber,
                                                   t_Status,
                                                   a_CreatedStartDate,
                                                   a_CreatedEndDate,
                                                   null,
                                                   null,
                                                   a_CompletedStartDate,
                                                   a_CompletedEndDate);
      
      if a_HouseNumber is not null then
        extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
      else
        extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
      end if;
      
      if a_LocationDescription is not null then
        extension.pkg_cxproceduralsearch.SearchByKeywords(a_LocationDescription);
      end if;
                                                   
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);

      extension.pkg_collectionutils.Append(a_Results, t_TempResults);
      t_TempResults := api.udt_objectlist();
      
    end if;
    
    if (a_Order = 'Y') then

      -- User needs to select Status Description, but SearchByJob procedure only takes StatusName.
      t_Status := GetStatusNameForDescription(a_Status, t_OrderTypeId);


      extension.pkg_cxproceduralsearch.InitializeSearch('j_LMS_Order');
      extension.pkg_cxproceduralsearch.SearchByJob(null,
                                                   null,
                                                   a_FileNumber,
                                                   t_Status,
                                                   a_CreatedStartDate,
                                                   a_CreatedEndDate,
                                                   null,
                                                   null,
                                                   a_CompletedStartDate,
                                                   a_CompletedEndDate);
      
      if a_HouseNumber is not null then
        extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
      else
        extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
      end if;
                                    
      extension.pkg_cxproceduralsearch.PerformSearch(t_TempResults);

      extension.pkg_collectionutils.Append(a_Results, t_TempResults);
      
    end if;

  end CandEJobSearch;

  /*---------------------------------------------------------------------------
   * InvestigationSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure InvestigationSearch (
    a_CompletedBy                number,
    a_InvestigationResult        number,
    a_Outcome                    varchar2,
    a_InvestigationStartDate     date,
    a_InvestigationEndDate       date,
    a_CompletedStartDate         date,
    a_CompletedEndDate           date,
    a_StreetName                 varchar2,
    a_HouseNumber                number default null,
    a_FromHouseNumber            number default null,
    a_ToHouseNumber              number default null,
    a_Suite                      varchar2 default null,
    a_Results                    out  api.udt_ObjectList
  ) is
    t_TempResults                api.udt_ObjectList;
    t_InvestigationEndDate       date;
    t_CompletedEndDate           date;
    t_CompletedBy                varchar2(500);

  begin
    a_Results      := api.udt_ObjectList();
    t_TempResults  := api.udt_ObjectList();

    if a_CompletedBy is not null then
      Select Name into t_CompletedBy from query.u_users where ObjectId = a_CompletedBy;
    end if;

    --if we are searching by address
    if a_StreetName is not null
    or a_housenumber is not null
    or a_fromhousenumber is not null
    or a_tohousenumber is not null
    or a_suite is not null then

      --check that we have valid search criterion
      if trim(a_StreetName) is null then
        if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        else
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
      
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;

    end if; --if we are searching by address
    
    if a_InvestigationEndDate is not null then
      t_InvestigationEndDate := to_date(to_char(a_InvestigationEndDate, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    
     if a_CompletedEndDate is not null then
      t_CompletedEndDate := to_date(to_char(a_CompletedEndDate, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    
    extension.pkg_cxproceduralsearch.InitializeSearch('p_LMS_PerformInvestigation');
    extension.pkg_cxproceduralsearch.SearchBySystemColumn('CreatedDate', a_InvestigationStartDate, t_InvestigationEndDate);
    extension.pkg_cxproceduralsearch.SearchBySystemColumn('DateCompleted', a_CompletedStartDate, t_CompletedEndDate);
    extension.pkg_cxproceduralsearch.SearchByIndex('Outcome', a_Outcome, null, false);
    extension.pkg_cxproceduralsearch.SearchByIndex('dup_CompletedByUserName', t_CompletedBy, null, false);
    
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('InvestigationResult', 'ObjectId', a_InvestigationResult, null, false);
   
    if a_HouseNumber is not null then
      extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, null);
    else
      extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, null);
    end if;
    
    extension.pkg_cxproceduralsearch.PerformSearch(a_Results, 'and', 'none');
      
  end InvestigationSearch;
  
  /*---------------------------------------------------------------------------
   * CandEMapSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
   procedure CandEMapSearch (
    a_ExtentMinX                 varchar2,
    a_ExtentMinY                 varchar2,
    a_ExtentMaxX                 varchar2,
    a_ExtentMaxY                 varchar2,
    a_Results                    out  api.udt_ObjectList
  ) is
    t_CaseFileTypeId          number := api.pkg_configquery.objectdefidforname('j_LMS_CaseFile');

  begin
    a_Results      := api.udt_ObjectList();
    
    for c in (select mp.LinkPosseObjectId
                from bcpdata.MapPoint mp
                join api.Jobs j on mp.LinkPosseObjectId = j.JobId
                join api.JobStatuses s on s.StatusId = j.StatusId and s.JobTypeId = j.JobTypeId
               where to_number(mp.LocationX) between to_number(a_ExtentMinX) and to_number(a_ExtentMaxX)
                 and to_number(mp.LocationY) between to_number(a_ExtentMinY) and to_number(a_ExtentMaxY)
                 and j.JobTypeId = t_CaseFileTypeId
                 and s.StatusType not in ('X','C')) loop
      a_Results.extend(1);
      a_Results(a_Results.count) := api.udt_Object(c.LinkPosseObjectId);
    end loop;

  end CandEMapSearch;

end pkg_BCPSearch;


/

