create or replace package pkg_Contractor as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*--------------------------------------------------------------------------
   * SetExternalFileNumber - PUBLIC
   * This procedure was meant to be run from the location of the Contractor
   * Registration Job.
   *------------------------------------------------------------------------*/
  procedure SetContractorFileNumber (
    a_ObjectId                    udt_Id,
    a_AsOfDate                    date,
    a_JobId                       udt_Id
  );

  /*--------------------------------------------------------------------------
   * RelateRenewalBatchCandidates - PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelateRenewalBatchCandidates (
    a_ObjectId                    udt_Id,
    a_AsOfDate                    date,
    a_BatchType                   varchar2,
    a_RunDate                     date,
    a_GetCandidates               char
  );

end pkg_Contractor;



 
/

grant execute
on pkg_contractor
to posseextensions;

create or replace package body pkg_Contractor as

  /*--------------------------------------------------------------------------
   * SetContractorFileNumber - PUBLIC
   *------------------------------------------------------------------------*/
  procedure SetContractorFileNumber (
    a_ObjectId                    udt_Id,
    a_AsOfDate                    date,
    a_JobId                       udt_Id
  ) is
      t_StateLicenseNumber varchar2(50) := api.pkg_columnquery.Value(a_JobId, 'StateLicenseNumber');
      t_RegistrationCategory varchar2(200) := api.pkg_columnquery.Value(a_JobId, 'RegistrationCategory');

  begin
    if t_StateLicenseNumber is not null then
      api.pkg_jobupdate.SetExternalFileNum(a_JobId, t_StateLicenseNumber);
    else
      toolbox.pkg_counter.SetExternalFileNum(a_JobId,
                                             a_AsOfDate,
                                             substr(t_RegistrationCategory, 1, 1) || '-[000000]',
                                             'Contractor Registration Number');
    end if;
  end;

  /*--------------------------------------------------------------------------
   * RelateRenewalBatchCandidates - PUBLIC
   *------------------------------------------------------------------------*/
  procedure RelateRenewalBatchCandidates (
    a_ObjectId                    udt_Id,
    a_AsOfDate                    date,
    a_BatchType                   varchar2,
    a_RunDate                     date,
    a_GetCandidates               char
  ) is
      t_SendNoticeProcessId udt_Id;
      t_ExpireRegistrationProcessId udt_Id;
      t_NewRelationship udt_Id;

  begin
  if a_GetCandidates != 'Y' then
    return;
  end if;
    if a_BatchType = 'Renewal Notices' then
    
    --Get all of the Contractor Registrations that need a notice
      for c in (select cr.ObjectId
        from query.j_contractorregistration cr
        join query.r_contracregistregisttype rt on rt.ContractorRegistrationJobId = cr.JobId
        join query.o_ContractorRegistrationType crt on crt.ObjectId = rt.RegistrationTypeObjectId
       where a_RunDate >= cr.ExpirationDate - nvl(crt.AdvanceRenewalNotice, 0)
         and cr.StatusName = 'ACTIVE'
         and not exists (select 1
                           from query.p_sendrenewalnotice sn
                          where sn.JobId = cr.JobId
                            and sn.Outcome is null)) loop

       t_SendNoticeProcessId := api.pkg_processupdate.New(c.objectid, api.pkg_configquery.ObjectDefIdForName('p_SendRenewalNotice'), null, null, null, null);
       t_NewRelationship := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName('j_ContractorRenewalBatch', 'SendRenewalNotice'), a_objectid, t_SendNoticeProcessId);
      end loop;
    elsif a_BatchType = 'Expire Registrations' then

    --Get all of the Contractor Registrations past their expiration grace period
      for e in (select cr.ObjectId
        from query.j_contractorregistration cr
        join query.r_contracregistregisttype rt on rt.ContractorRegistrationJobId = cr.JobId
        join query.o_ContractorRegistrationType crt on crt.ObjectId = rt.RegistrationTypeObjectId
       where a_RunDate >= cr.ExpirationDate + nvl(crt.ExpirationGracePeriod, 0)
         and cr.StatusName in ('ACTIVE', 'RENPEN')
         and not exists (select 1
                           from query.p_ExpireRegistration sn
                          where sn.JobId = cr.JobId
                            and sn.Outcome is null)) loop
         
       t_SendNoticeProcessId := api.pkg_processupdate.New(e.objectid, api.pkg_configquery.ObjectDefIdForName('p_ExpireRegistration'), null, null, null, null);
       t_NewRelationship := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName('j_ContractorRenewalBatch', 'ExpireRegistration'), a_objectid, t_SendNoticeProcessId);
      end loop;
    end if;
    api.pkg_columnupdate.SetValue(a_ObjectId, 'GetCandidates', 'N');
  end;

end pkg_Contractor;


/

