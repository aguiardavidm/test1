create or replace package PKG_BCPCorral is

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * NotifyCorralSynchronize() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure NotifyCorralSynchronize (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );
end PKG_BCPCorral;



 
/

create or replace package body PKG_BCPCorral is

  /*---------------------------------------------------------------------------
   * NotifyCorralSynchronize() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure NotifyCorralSynchronize (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    procedure SetErrorArguments is
      begin
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('ObjectId', a_ObjectId);
        api.pkg_Errors.SetArgValue('As Of Date', a_AsOfDate);
      end;
  begin
    for c in (select jo.JobId ObjectToNotify
                from api.Relationships re,
                     api.Jobs jo
               where re.FromObjectId = a_ObjectId
                 and re.ToObjectId = jo.JobId) loop
      api.Pkg_ObjectUpdate.NotifyExternalObjectModified(c.ObjectToNotify);
    end loop;
  end;
end PKG_BCPCorral;



/

