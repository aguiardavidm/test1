create or replace package LMS is

  subtype udt_Id is api.pkg_Definition.udt_Id;
  procedure AttachPermitTypesFromQuestions(
    a_ObjectId              udt_Id,
    a_AsOfDate              date
  );
  procedure CreateBundleSubJobs (
    a_Objectid              udt_Id,
    a_AsOfDate              date
  );
  procedure CreatePermitAppFromProject (
    a_Objectid              udt_Id,
    a_AsOfDate              date
  );  


end LMS;

 
/

create or replace package body LMS is

  /*
    Define types and Global Variables
  */
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  
  g_BuildingPermitDef     udt_Id := api.pkg_configquery.ObjectDefIdForName('j_BuildingPermit');
  g_TradePermitDef        udt_Id := api.pkg_configquery.ObjectDefIdForName('j_TradePermit');
  g_GeneralPermitDef      udt_Id := api.pkg_configquery.ObjectDefIdForName('j_GeneralPermit');
  g_GPEndPt               udt_Id := api.pkg_configquery.EndPointIdForName('j_GeneralPermit', 'PermitType');
  g_BundlePTypeEndPt      udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'PermitType');
  g_PermitBundleDef       udt_Id := api.pkg_configquery.ObjectDefIdForName('j_PermitBundle');
  g_ProjectPermitAppEndPt udt_Id := api.pkg_configquery.EndPointIdForName('o_MasterProject', 'PermitApplication');
  g_BldgContractEndPt     udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'GPBuildingContractor');
  g_ElecContractEndPt     udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'GPElectricalContractor');
  g_MechContractEndPt     udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'GPMechContractor');
  g_PlumContractEndPt     udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'GPPlumbingContractor');
  g_OthrContractEndPt     udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'GPOtherContractor');
  g_GasUtilEndPt          udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'GasUtility');
  g_PwrUtilEndPt          udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'PowerUtility');
  g_ZngJurEndPt           udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'ZoningJurisdiction');  
  g_AddressEndPt          udt_Id := api.pkg_configquery.EndPointIdForName('j_PermitBundle', 'Address');
  
  /*
    Function and Procedure Implementations
  */
  function SubjobExists(
    a_ParentJobId           udt_Id,
    a_JobTypeId             udt_Id,
    a_CheckColumn           varchar2,
    a_CheckValue            varchar2
  ) return boolean is
    t_Exists                boolean := false;
    t_JobValue              varchar2(100);
  begin
    for r in (
      select jobid
        from api.jobs
       where parentjobid = a_ParentJobId
         and jobtypeid = a_JobTypeId
    ) loop
      if a_CheckColumn is null then
        t_Exists := true;
        exit;
      else
        t_JobValue := api.pkg_ColumnQuery.Value(r.jobid, a_CheckColumn);
        if t_JobValue = a_CheckValue then
          t_Exists := true;
          exit;
        end if;
      end if;
    end loop;
    return t_Exists;
  end SubjobExists;
  
  function RelExists(
    a_FromObjectId          udt_Id,
    a_ToObjectId            udt_Id,
    a_EndPointId            udt_Id
  ) return boolean is
    t_RelCount              number;
  begin
    select count(1)
      into t_RelCount
      from api.relationships
     where fromobjectid = a_FromObjectId
       and toobjectid = a_ToObjectId
       and endpointid = a_EndPointId;
    return (t_RelCount > 0);
  end RelExists;

  procedure AttachPermitTypesFromQuestions(
    a_ObjectId              udt_Id,
    a_AsOfDate              date
  ) is
    t_QuestionRelsEndPt     udt_Id := api.Pkg_Configquery.EndPointIdForName('j_PermitBundle', 'PermitQuestion');          
    t_PermitTypeEndPt       udt_Id := api.Pkg_Configquery.EndPointIdForName('o_PermitQuestion', 'PermitType');
    t_RequiredPermitTypes   udt_IdList;
    t_RelId                 udt_Id;
  begin
    for r in (
      select relationshipid,
             toobjectid,
             api.pkg_ColumnQuery.Value(relationshipid, 'AppliesToPermit') Applies,
             api.pkg_ColumnQuery.Value(toobjectid, 'Sequence') seq,
             api.pkg_ColumnQuery.Value(toobjectid, 'Question') thequestion
        from api.relationships rel
       where rel.fromobjectid = a_ObjectId
         and rel.endpointid = t_QuestionRelsEndPt
    ) loop
      If r.Applies = 'Y' then
        t_RequiredPermitTypes := api.pkg_ObjectQuery.RelatedObjects(r.toobjectid, t_PermitTypeEndPt);
        for r1 in 1..t_RequiredPermitTypes.count loop
          if not RelExists(a_ObjectId, t_RequiredPermitTypes(r1), g_BundlePTypeEndPt) then
            t_RelId := api.pkg_RelationshipUpdate.New(g_BundlePTypeEndPt, a_ObjectId, t_RequiredPermitTypes(r1), sysdate);
          end if;
        end loop;
        -- Clear the flag on the question so that if the permittype is removed from teh bundle it is not immediately related again
        api.pkg_ColumnUpdate.SetValue(r.relationshipid, 'AppliesToPermit', 'N');
      End If;
    end loop;
  end AttachPermitTypesFromQuestions;
  
  procedure CreateBundleSubJobs (
    a_Objectid              udt_Id,
    a_AsOfDate              date
  ) is
    t_Job                   udt_Id;
    t_PermitType            varchar2(100);
    t_Rel                   udt_Id;
    t_SubJob                udt_Id;
  begin
    null;
    /*t_Job := api.pkg_columnquery.NumericValue(a_ObjectId, 'JobId');
    pkg_Debug.putline('bcpdata.pkg_Vancouver.CreateBundleSubJobs - JobId: ' || t_Job);
    -- Ensure that all the PermitType objects have been related
    AttachPermitTypesFromQuestions(t_Job, a_AsOfDAte);
    pkg_Debug.putline('returned from bcpdata.pkg_Vancouver.AttachPermitTypesFromQuestions');
    
    -- Loop through the attached Permit Types and create subjobs if they don't already exist.
    for i in (select r.PermitTypeObjectId
              , api.pkg_columnquery.Value(r.PermitTypeObjectId, 'PermitType') PermitType
              , relationshipid
              , api.pkg_columnquery.Value(r.relationshipid, 'SubjobCreated') SubjobCreated
        from query.r_PermitBundlePermitType r
              where r.PermitBundleJobId = t_Job)
    loop
      if i.SubjobCreated <> 'Y' then 
        pkg_Debug.putline('bcpdata.pkg_Vancouver.AttachPermitTypesFromQuestions IN THE LOOP');
        if i.permittype like 'Building%' then  -- Building Permit
          --if not SubjobExists(t_Job, g_BuildingPermitDef, null, null) then
            t_SubJob := api.pkg_jobupdate.New(g_BuildingPermitDef, null, null, t_Job);
            pkg_Debug.putline('bcpdata.pkg_Vancouver.CreateBundleSubJobs - New Subjob: ' || t_SubJob);
          --end if;
        elsif i.permittype like 'Trade%' then  -- Trade Permit
          if i.permittype = 'Trade - Electrical' then
            t_PermitType := 'Electrical';
          elsif i.permittype = 'Trade - Mechanical' then
            t_PermitType := 'Mechanical';
          else
            t_PermitType := 'Plumbing';
          end if;
          --if not SubjobExists(t_Job, g_TradePermitDef, 'Trade', t_PermitType) then
            t_SubJob := api.pkg_jobupdate.New(g_TradePermitDef, null, null, t_Job);
            pkg_Debug.putline('bcpdata.pkg_Vancouver.CreateBundleSubJobs - New Subjob: ' || t_SubJob);
            api.pkg_columnupdate.SetValue(t_SubJob, 'Trade', t_PermitType);
          --end if;
        else -- General Permit
          --if not SubjobExists(t_Job, g_GeneralPermitDef, 'PermitType', t_PermitType) then
            t_SubJob := api.pkg_jobupdate.New(g_GeneralPermitDef, null, null, t_Job);
            pkg_Debug.putline('bcpdata.pkg_Vancouver.CreateBundleSubJobs - New Subjob: ' || t_SubJob);
            t_Rel := api.pkg_Relationshipupdate.New(g_GPEndPt, t_SubJob, i.permittypeobjectid);
          --end if;
        end if;
      end if;
      api.pkg_ColumnUpdate.SetValue(i.relationshipid, 'SubjobCreated', 'Y');
    end loop;
    
    for i in ( select r.relationshipid
               from query.r_PermitBundlePermitType r
               where r.PermitBundleJobId = t_job
             ) loop
      api.pkg_relationshipupdate.Remove(i.relationshipid, null);
    end loop;*/
  end CreateBundleSubJobs;
  
  procedure CreatePermitAppFromProject (
    a_Objectid              udt_Id,
    a_AsOfDate              date
  ) is
    t_Job                   udt_Id;
    t_Rel                   udt_Id;
    t_BldgContr             udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'BuildingContractorObjectId');
    t_ElecContr             udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'ElectricalContractorObjectId');
    t_MechContr             udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'MechanicalContractorObjectId');
    t_PlumContr             udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'PlumbingContractorObjectId');
    t_OthrContr             udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'OtherContractorObjectId');
    t_GasObjectId           udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'GasUtilityObjectId');
    t_PowerObjectId         udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'PowerUtilityObjectId');
    t_ZoningObjectId        udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'ZoningJurisdictionObjectId');
    t_AddressObjectId       udt_Id := api.pkg_columnquery.NumericValue(a_Objectid, 'AddressObjectId');
    
    
  begin  
    if api.pkg_columnquery.Value(a_Objectid, 'CreateSubPermitApp') = 'Y' then

       -- copying details
       t_Job := api.pkg_objectupdate.New(g_PermitBundleDef);
       api.pkg_columnupdate.SetValue(t_Job, 'CommercialOrResidential', api.pkg_columnquery.Value(a_Objectid, 'CommercialOrResidential'));
       api.pkg_columnupdate.SetValue(t_Job, 'WorkDescription', api.pkg_columnquery.Value(a_Objectid, 'WorkDescription'));
       api.pkg_columnupdate.SetValue(t_Job, 'BuildingContractValue', api.pkg_columnquery.Value(a_Objectid, 'BuildingContractValue'));
       api.pkg_columnupdate.SetValue(t_Job, 'BuildingFinalRequired', api.pkg_columnquery.Value(a_Objectid, 'BuildingFinalRequired'));
       api.pkg_columnupdate.SetValue(t_Job, 'ElectricalContractValue', api.pkg_columnquery.Value(a_Objectid, 'ElectricalContractValue'));
       api.pkg_columnupdate.SetValue(t_Job, 'ElectricalFinalRequired', api.pkg_columnquery.Value(a_Objectid, 'ElectricalFinalRequired'));
       api.pkg_columnupdate.SetValue(t_Job, 'MechanicalContractValue', api.pkg_columnquery.Value(a_Objectid, 'MechanicalContractValue'));
       api.pkg_columnupdate.SetValue(t_Job, 'MechanicalFinalRequired', api.pkg_columnquery.Value(a_Objectid, 'MechanicalFinalRequired'));
       api.pkg_columnupdate.SetValue(t_Job, 'PlumbingContractValue', api.pkg_columnquery.Value(a_Objectid, 'PlumbingContractValue'));
       api.pkg_columnupdate.SetValue(t_Job, 'PlumbingFinalRequired', api.pkg_columnquery.Value(a_Objectid, 'PlumbingFinalRequired'));
       api.pkg_columnupdate.SetValue(t_Job, 'OtherContractValue', api.pkg_columnquery.Value(a_Objectid, 'OtherContractValue'));
           
       api.pkg_columnupdate.SetValue(t_Job, 'ApplicantCityStored', api.pkg_columnquery.Value(a_Objectid, 'ApplicantCityStored'));
       api.pkg_columnupdate.SetValue(t_Job, 'ApplicantMailingAddressStored', api.pkg_columnquery.Value(a_Objectid, 'ApplicantMailingAddressStored'));
       api.pkg_columnupdate.SetValue(t_Job, 'ApplicantNameStored', api.pkg_columnquery.Value(a_Objectid, 'ApplicantNameStored'));
       api.pkg_columnupdate.SetValue(t_Job, 'ApplicantPhoneNumberStored', api.pkg_columnquery.Value(a_Objectid, 'ApplicantPhoneNumberStored'));
       api.pkg_columnupdate.SetValue(t_Job, 'ApplicantStateStored', api.pkg_columnquery.Value(a_Objectid, 'ApplicantStateStored'));
       api.pkg_columnupdate.SetValue(t_Job, 'ApplicantZipCodeStored', api.pkg_columnquery.Value(a_Objectid, 'ApplicantZipCodeStored'));
       api.pkg_columnupdate.SetValue(t_Job, 'NonContractorApplicant', api.pkg_columnquery.Value(a_Objectid, 'NonContractorApplicant'));   
       
       api.pkg_columnupdate.SetValue(t_Job, 'WaterSupplyType', api.pkg_columnquery.Value(a_Objectid, 'WaterSupplyType'));
       api.pkg_columnupdate.SetValue(t_Job, 'SewerType', api.pkg_columnquery.Value(a_Objectid, 'SewerType'));
       api.pkg_columnupdate.SetValue(t_Job, 'SepticPermitNumber', api.pkg_columnquery.Value(a_Objectid, 'SepticPermitNumber'));
       api.pkg_columnupdate.SetValue(t_Job, 'NewGasService', api.pkg_columnquery.Value(a_Objectid, 'NewGasService'));
       api.pkg_columnupdate.SetValue(t_Job, 'NewPowerService', api.pkg_columnquery.Value(a_Objectid, 'NewPowerService'));
       api.pkg_columnupdate.SetValue(t_Job, 'CountyZoning', api.pkg_columnquery.Value(a_Objectid, 'CountyZoning'));
       api.pkg_columnupdate.SetValue(t_Job, 'ZoningApproved', api.pkg_columnquery.Value(a_Objectid, 'ZoningApproved'));
       api.pkg_columnupdate.SetValue(t_Job, 'InFloodZone', api.pkg_columnquery.Value(a_Objectid, 'InFloodZone'));
       
       --contractor rels
       if t_BldgContr is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_BldgContractEndPt, t_Job, t_BldgContr);
       end if;
       if t_ElecContr is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_ElecContractEndPt, t_Job, t_ElecContr);
       end if;
       if t_MechContr is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_MechContractEndPt, t_Job, t_MechContr);
       end if;
       if t_PlumContr is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_PlumContractEndPt, t_Job, t_PlumContr);
       end if;
       if t_OthrContr is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_OthrContractEndPt, t_Job, t_OthrContr);
       end if;      
       
       --other rels                      
       if t_AddressObjectId is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_AddressEndPt, t_Job, t_AddressObjectId);
       end if;
       if t_GasObjectId is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_GasUtilEndPt, t_Job, t_GasObjectId);
       end if;
       if t_PowerObjectId is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_PwrUtilEndPt, t_Job, t_PowerObjectId);
       end if;
       if t_ZoningObjectId is not null then
          t_Rel := api.pkg_relationshipupdate.New(g_ZngJurEndPt, t_Job, t_ZoningObjectId);
       end if;              
       
       --relate to Project
       t_Rel := api.pkg_relationshipupdate.New(g_ProjectPermitAppEndPt, a_Objectid, t_Job);
       
       --reset checkbox
       api.pkg_columnupdate.SetValue(a_Objectid, 'CreateSubPermitApp', 'N');
    end if;
    
     
  end CreatePermitAppFromProject;
  
end LMS;

/

