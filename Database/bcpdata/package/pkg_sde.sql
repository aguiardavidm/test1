create or replace package pkg_SDE as

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *   Routines used to query an SDE listener.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_ObjectList is api.udt_ObjectList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  /*---------------------------------------------------------------------------
   * Merge()
   *   Perform a merge of the items which match the input criteria, creating
   * a new item on the specified layer with the data attributes being set.
   *-------------------------------------------------------------------------*/
  Procedure Merge (
    a_SourceLayerName                   varchar2,
    a_SourceLayerColumnName             varchar2,
    a_SourceValues                      udt_StringList,
    a_ResultLayerName                   varchar2,
    a_ResultColumnNames                 udt_StringList,
    a_ResultValues                      udt_StringList
  );

  /*---------------------------------------------------------------------------
   * PerformQuery()
   *   Perform a spatial query using the SDE listener and the parameters stored
   * in the table SDEQueries indexed by EndPointId. The objects found are
   * placed in the provided array.
   *-------------------------------------------------------------------------*/
  procedure PerformQuery (
    a_ObjectId                          udt_Id,
    a_EndPointId                        udt_Id,
    a_RelatedObjects                    out nocopy udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PerformQuery()
   *   Perform a spatial query using the SDE listener the specified parameters.
   * The objects found are returned in a udt_StringArray.
   *-------------------------------------------------------------------------*/
  function PerformQuery (
    a_SearchLayerName                   varchar2,
    a_SearchLayerColumnName             varchar2,
    a_SearchValue                       varchar2,
    a_ReturnLayerName                   varchar2,
    a_ReturnLayerColumnName             varchar2
  ) return udt_StringList;

  /*---------------------------------------------------------------------------
   * ReturnQueryResults()
   *   Perform a spatial query using the SDE listener the specified parameters.
   * The objects found are returned in a udt_StringArray.
   *-------------------------------------------------------------------------*/
  Procedure ReturnQueryResultCount (
    a_SearchLayerName                   varchar2,
    a_SearchLayerColumnName             varchar2,
    a_SearchValue                       varchar2,
    a_ReturnLayerName                   varchar2,
    a_ReturnLayerColumnName             varchar2,
    a_Count                         out number
  );

  /*---------------------------------------------------------------------------
   * SetPipeName()
   *   Set the pipe name to which SDE Listener requests will be sent.
   *-------------------------------------------------------------------------*/
  procedure SetPipeName (
    a_PipeName                          varchar2
  );

  /*---------------------------------------------------------------------------
   * SetTimeOut()
   *   Set the time in seconds to wait for a response from the SDE Listener.
   *-------------------------------------------------------------------------*/
  procedure SetTimeOut (
    a_TimeOut                           number
  );

  /*---------------------------------------------------------------------------
   * Shutdown()
   *   Shutdowns down the SDE Listener.
   *-------------------------------------------------------------------------*/
  procedure Shutdown ;

end;



 
/

grant execute
on pkg_sde
to public;

create or replace package body pkg_SDE wrapped
0
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
3
b
9200000
1
4
0
b7
2 :e:
1PACKAGE:
1BODY:
1PKG_SDE:
1SUBTYPE:
1UDT_IDLIST:
1API:
1PKG_DEFINITION:
1UDT_RAWQUERYVALUELIST:
1GC_PROTOCOLVERSION:
1CONSTANT:
1VARCHAR2:
130:
13:
1GC_MAXMESSAGESIZE:
1NUMBER:
14000:
1G_TIMEOUT:
120:
1G_PIPENAME:
1SDEListener:
1G_MESSAGESIZE:
1G_REQUESTID:
1MESSAGESEND:
10:
1DBMS_PIPE:
1SEND_MESSAGE:
1!=:
1RAISE_APPLICATION_ERROR:
1-:
120000:
1Unable to send message to SDE listener!:
1RAISEERROR:
1A_ERRORTEXT:
1PKG_DEBUG:
1PUTLINE:
1SDEListener Error:: ":
1||:
1".:
1PKG_ERRORS:
1SETARGVALUE:
1SDEListenerError:
1MESSAGEADD:
1A_VALUE:
1+:
126:
1>:
1PACK_MESSAGE:
17:
1LENGTH:
1FUNCTION:
1MESSAGEGETBUFFER:
1A_PIPENAME:
1A_REQUESTID:
1A_BUFFERSTOTAL:
1OUT:
1PLS_INTEGER:
1A_BUFFERSVALID:
1RETURN:
1T_REQUESTID:
1T_MESSAGECOUNT:
1LOOP:
1RECEIVE_MESSAGE:
1Unable to receive message from SDE listener!:
11:
1UNPACK_MESSAGE:
1=:
1Buffer:
1TO_CHAR:
1Processing request :
1 valid buffer :
1 with :
1 values.:
1EXIT:
1Skipping result buffer from request :
1 values:
1MESSAGEGETRESPONSE:
1UDT_STRINGLIST:
1T_TYPE:
1T_NUMBERVALUE:
1T_STRINGVALUE:
1T_VALUES:
1T_BUFFERSTOTAL:
1T_BUFFERSVALID:
1CLEAR:
1PipeName:
1RequestId:
1I:
1NEXT_ITEM_TYPE:
16:
1COUNT:
1ELSIF:
19:
1Invalid data type (:
1) of returned response.:
1MESSAGESTART:
1A_TYPE:
1T_PIPENAME:
160:
1T_PREFIX:
1SDE$LSNR:
1IS NULL:
1UNIQUE_SESSION_NAME:
1MERGESEND:
1A_SOURCELAYERNAME:
1A_SOURCELAYERCOLUMNNAME:
1A_SOURCEVALUES:
1A_RESULTLAYERNAME:
1A_RESULTLAYERCOLUMNNAMES:
1A_RESULTLAYERCOLUMNVALUES:
1Merge:
1QUERYSEND:
1A_SEARCHLAYERNAME:
1A_SEARCHLAYERCOLUMNNAME:
1A_SEARCHVALUE:
1A_RETURNLAYERNAME:
1A_RETURNLAYERCOLUMNNAME:
1Query:
1MERGE:
1A_RESULTCOLUMNNAMES:
1A_RESULTVALUES:
1PERFORMQUERY:
1A_OBJECTID:
1UDT_ID:
1A_ENDPOINTID:
1A_RELATEDOBJECTS:
1UDT_OBJECTLIST:
1T_FROMLAYERNAME:
1SDEQUERIES:
1FROMLAYERNAME:
1TYPE:
1T_FROMLAYERCOLUMNNAME:
1FROMLAYERCOLUMNNAME:
1T_FROMCOLUMNNAME:
1FROMCOLUMNNAME:
1T_TOLAYERNAME:
1TOLAYERNAME:
1T_TOLAYERCOLUMNNAME:
1TOLAYERCOLUMNNAME:
1T_DATATYPE:
1COLUMNDEFS:
1DATATYPE:
1T_OBJECTID:
1T_OBJECTIDS:
1T_OBJECTDEFNAME:
1OBJECTDEFS:
1NAME:
1T_COLUMNDEFNAME:
1SQ:
1INDEXOBJECTDEFNAME:
1INDEXCOLUMNNAME:
1ENDPOINTID:
1SELECT:n        sq.FromLayerName,:n        sq.FromLayerColumnName,:n        s+
1q.FromColumnName,:n        sq.ToLayerName,:n        sq.ToLayerColumnName,:n  +
1      sq.IndexObjectDefName,:n        sq.IndexColumnName:n      into:n       +
1 t_FromLayerName,:n        t_FromLayerColumnName,:n        t_FromColumnName,+
1:n        t_ToLayerName,:n        t_ToLayerColumnName,:n        t_ObjectDefNa+
1me,:n        t_ColumnDefName:n      from SDEQueries sq:n      where sq.EndPoi+
1ntId = a_EndPointId:
1NO_DATA_FOUND:
1No SDE query defined for end point :
1C:
1OBJECTS:
1O:
1OBJECTID:
1OBJECTDEFID:
1SELECT c.DataType:n        into t_DataType:n        from:n          api.Colum+
1nDefs c,:n          api.Objects o:n        where o.ObjectId = a_ObjectId:n   +
1       and c.ObjectDefId = o.ObjectDefId:n          and c.Name = t_FromColumn+
1Name:
1No column named ":
1" on the from object.:
1char:
1PKG_COLUMNQUERY:
1VALUE:
1number:
1NUMERICVALUE:
1Column ":
1" must be a string or a number.:
1TO_NUMBER:
1IS NOT NULL:
1EXTEND:
1UDT_OBJECT:
1PKG_SIMPLESEARCH:
1OBJECTSBYINDEX:
1J:
1RETURNQUERYRESULTCOUNT:
1A_COUNT:
1SETPIPENAME:
1SETTIMEOUT:
1A_TIMEOUT:
1SHUTDOWN:
1Shutdown:
0
0
0
692
2
0 :2 a0 97 a0 9b :2 a0 6b a0
6b 1c 70 a0 9b :2 a0 6b a0
6b 1c 70 87 :2 a0 51 a5 1c
6e 1b b0 87 :2 a0 1c 51 1b
b0 a3 a0 1c 51 81 b0 a3
a0 51 a5 1c 6e 81 b0 a3
a0 1c 81 b0 a3 a0 1c 81
b0 9a b4 55 6a a0 51 d
:2 a0 6b :2 a0 a5 b 7e 51 b4
2e a0 7e 51 b4 2e 6e a5
57 b7 19 3c b7 a4 b1 11
68 4f 9a 8f a0 b0 3d b4
55 6a :2 a0 6b 6e 7e a0 b4
2e 7e 6e b4 2e a5 57 :2 a0
6b a0 6b 6e a0 a5 57 :2 a0
6b a0 6b 7e 51 b4 2e a0
a5 57 b7 a4 b1 11 68 4f
9a 8f a0 b0 3d b4 55 6a
:2 a0 7e 51 b4 2e d :2 a0 7e
b4 2e a0 b4 57 a0 51 d
b7 19 3c :2 a0 6b a0 a5 57
b7 a4 b1 11 68 4f 9a 8f
a0 b0 3d b4 55 6a :2 a0 7e
51 b4 2e 7e :2 a0 a5 b b4
2e d :2 a0 7e b4 2e a0 b4
57 a0 51 7e :2 a0 a5 b b4
2e d b7 19 3c :2 a0 6b a0
a5 57 b7 a4 b1 11 68 4f
a0 8d 8f a0 b0 3d 8f a0
b0 3d 90 :2 a0 b0 3f 90 :2 a0
b0 3f b4 :2 a0 2c 6a a3 a0
1c 81 b0 a3 a0 1c 81 b0
:3 a0 6b :2 a0 a5 b 7e 51 b4
2e a0 6e a5 57 b7 19 3c
:2 a0 7e 51 b4 2e d :2 a0 6b
a0 a5 57 :2 a0 6b a0 a5 57
:2 a0 7e b4 2e :2 a0 7e 51 b4
2e d :2 a0 6b a0 6b 6e 7e
:2 a0 a5 b b4 2e 6e 7e :2 a0
a5 b b4 2e 7e 6e b4 2e
7e :2 a0 a5 b b4 2e 7e 6e
b4 2e 7e :2 a0 a5 b b4 2e
7e 6e b4 2e a5 57 a0 2b
b7 19 3c :2 a0 6b a0 6b 6e
7e :2 a0 a5 b b4 2e 6e 7e
:2 a0 a5 b b4 2e 7e 6e b4
2e 7e :2 a0 a5 b b4 2e 7e
6e b4 2e a5 57 b7 a0 47
:2 a0 65 b7 a4 b1 11 68 4f
a0 8d 8f a0 b0 3d b4 :2 a0
2c 6a a3 a0 1c 81 b0 a3
a0 1c 81 b0 a3 a0 1c 81
b0 a3 a0 51 a5 1c 81 b0
a3 a0 1c 81 b0 a3 a0 1c
81 b0 a3 a0 1c 81 b0 :2 a0
6b a0 6b b4 57 :2 a0 6b a0
6b 6e a0 a5 57 :2 a0 6b a0
6b 6e a0 a5 57 a0 51 d
a0 51 d :7 a0 a5 b d :2 a0
7e 51 b4 2e 2b 91 51 :2 a0
63 37 :3 a0 6b b4 2e d a0
7e 51 b4 2e :2 a0 6b a0 a5
57 a0 :2 7e 51 b4 2e b4 2e
5a a0 7e 51 b4 2e 5a a
10 a0 7e 51 b4 2e 5a a
10 :2 a0 6b a0 a5 57 :2 a0 a5
57 b7 19 3c :3 a0 6b 7e 51
b4 2e a5 b :2 a0 a5 b d
a0 b7 a0 7e 51 b4 2e :2 a0
6b a0 a5 57 :3 a0 6b 7e 51
b4 2e a5 b a0 d b7 19
a0 6e 7e :2 a0 a5 b b4 2e
7e 6e b4 2e a5 57 b7 :2 19
3c b7 a0 47 b7 a0 47 :2 a0
6b a0 6b b4 57 :2 a0 65 b7
a4 b1 11 68 4f a0 8d 8f
a0 b0 3d b4 :2 a0 2c 6a a3
a0 51 a5 1c 81 b0 a3 a0
51 a5 1c 6e 81 b0 a0 7e
b4 2e a0 51 d b7 :2 a0 7e
51 b4 2e d b7 :2 19 3c :2 a0
7e :2 a0 6b b4 2e b4 2e d
:2 a0 6b a0 a5 57 :2 a0 6b a0
a5 57 :2 a0 6b a0 a5 57 :2 a0
6b a0 a5 57 a0 51 7e 51
b4 2e 7e :2 a0 a5 b b4 2e
7e 51 b4 2e 7e :2 a0 a5 b
b4 2e d :2 a0 65 b7 a4 b1
11 68 4f a0 8d 8f a0 b0
3d 8f a0 b0 3d 8f a0 b0
3d 8f a0 b0 3d 8f a0 b0
3d 8f a0 b0 3d b4 :2 a0 2c
6a a3 a0 51 a5 1c 81 b0
:2 a0 6e a5 b d :2 a0 a5 57
:2 a0 a5 57 :3 a0 6b a5 57 :2 a0
a5 57 :3 a0 6b a5 57 :3 a0 6b
a5 57 91 51 :2 a0 6b a0 63
37 :3 a0 a5 b a5 57 b7 a0
47 91 51 :2 a0 6b a0 63 37
:3 a0 a5 b a5 57 b7 a0 47
91 51 :2 a0 6b a0 63 37 :3 a0
a5 b a5 57 b7 a0 47 a0
b4 57 :2 a0 65 b7 a4 b1 11
68 4f a0 8d 8f a0 b0 3d
8f a0 b0 3d 8f a0 b0 3d
8f a0 b0 3d 8f a0 b0 3d
b4 :2 a0 2c 6a a3 a0 51 a5
1c 81 b0 :2 a0 6e a5 b d
:2 a0 a5 57 :2 a0 a5 57 :2 a0 a5
57 :2 a0 a5 57 :2 a0 a5 57 a0
b4 57 :2 a0 65 b7 a4 b1 11
68 4f 9a 8f a0 b0 3d 8f
a0 b0 3d 8f a0 b0 3d 8f
a0 b0 3d 8f a0 b0 3d 8f
a0 b0 3d b4 55 6a a3 a0
1c 81 b0 a3 a0 51 a5 1c
81 b0 :8 a0 a5 b d :3 a0 a5
b d a0 65 b7 a4 b1 11
68 4f a0 8d 8f a0 b0 3d
8f a0 b0 3d 8f a0 b0 3d
8f a0 b0 3d 8f a0 b0 3d
b4 :2 a0 2c 6a a3 a0 1c 81
b0 a3 a0 51 a5 1c 81 b0
:7 a0 a5 b d :3 a0 a5 b d
:2 a0 65 b7 a4 b1 11 68 4f
9a 8f a0 b0 3d 8f a0 b0
3d 96 :2 a0 b0 54 b4 55 6a
a3 :2 a0 6b :2 a0 f 1c 81 b0
a3 :2 a0 6b :2 a0 f 1c 81 b0
a3 :2 a0 6b :2 a0 f 1c 81 b0
a3 :2 a0 6b :2 a0 f 1c 81 b0
a3 :2 a0 6b :2 a0 f 1c 81 b0
a3 :2 a0 6b a0 6b :2 a0 f 1c
81 b0 a3 a0 1c 81 b0 a3
a0 1c 81 b0 a3 a0 1c 81
b0 a3 a0 51 a5 1c 81 b0
a3 :2 a0 6b a0 6b :2 a0 f 1c
81 b0 a3 :2 a0 6b a0 6b :2 a0
f 1c 81 b0 a3 a0 51 a5
1c 81 b0 :3 a0 6b b4 2e d
:1a a0 12a b7 :2 a0 7e 51 b4 2e
6e 7e :2 a0 a5 b b4 2e a5
57 b7 a6 9 a4 b1 11 4f
:13 a0 12a b7 :2 a0 7e 51 b4 2e
6e 7e a0 b4 2e 7e 6e b4
2e a5 57 b7 a6 9 a4 b1
11 4f a0 7e 6e b4 2e :3 a0
6b a0 6b :2 a0 a5 b d a0
b7 a0 7e 6e b4 2e :4 a0 6b
a0 6b :2 a0 a5 b a5 b d
b7 19 a0 7e 51 b4 2e 6e
7e a0 b4 2e 7e 6e b4 2e
a5 57 b7 :2 19 3c a0 7e b4
2e a0 65 b7 19 3c :7 a0 a5
b d :3 a0 a5 b d 91 51
:2 a0 6b a0 63 37 a0 7e b4
2e :4 a0 a5 b a5 b d a0
7e b4 2e :2 a0 6b 51 a5 57
:3 a0 6b a5 b :2 a0 6b a0 a5
b d b7 19 3c b7 :3 a0 6b
a0 6b :4 a0 a5 b a5 b d
91 51 :2 a0 6b a0 63 37 :2 a0
a5 b 7e b4 2e :2 a0 6b 51
a5 57 :3 a0 6b a5 b :2 a0 6b
:2 a0 a5 b a5 b d b7 19
3c b7 a0 47 b7 :2 19 3c b7
a0 47 b7 a4 b1 11 68 4f
9a 8f a0 b0 3d 8f a0 b0
3d 8f a0 b0 3d 8f a0 b0
3d 8f a0 b0 3d 96 :2 a0 b0
54 b4 55 6a :7 a0 a5 b a0
6b d b7 a4 b1 11 68 4f
9a 8f a0 b0 3d b4 55 6a
:2 a0 d b7 a4 b1 11 68 4f
9a 8f a0 b0 3d b4 55 6a
:2 a0 d b7 a4 b1 11 68 4f
9a b4 55 6a a3 a0 51 a5
1c 81 b0 :2 a0 6e a5 b d
a0 b4 57 b7 a4 b1 11 68
4f b1 b7 a4 11 b1 56 4f
1d 17 b5
692
2
0 3 7 b 15 19 1d 21
25 28 2c 2f 37 3a 3e 42
46 4a 4d 51 54 5c 80 63
67 6b 6e 6f 77 7c 62 9f
8b 8f 93 5f 9b 8a bb aa
ae 87 b6 a9 dd c6 a6 ca
cb d3 d8 c5 f9 e8 ec f4
c2 111 100 104 10c e7 118 e4
12c 130 134 138 13b 13f 143 147
14a 14e 152 153 155 158 15b 15c
161 165 168 16b 16c 171 176 177
17c 17e 182 185 187 18b 18d 199
19d 19f 1bb 1b7 1b6 1c3 1b3 1c8
1cc 1d0 1d4 1d8 1db 1e0 1e3 1e7
1e8 1ed 1f0 1f5 1f6 1fb 1fc 201
205 209 20c 210 213 218 21c 21d
222 226 22a 22d 231 234 237 23a
23b 240 244 245 24a 24c 250 252
25e 262 264 280 27c 27b 288 278
28d 291 295 299 29d 2a0 2a3 2a4
2a9 2ad 2b1 2b5 2b8 2b9 2be 2c2
2c3 2c8 2cc 2cf 2d3 2d5 2d9 2dc
2e0 2e4 2e7 2eb 2ec 2f1 2f3 2f7
2f9 305 309 30b 327 323 322 32f
31f 334 338 33c 340 344 347 34a
34b 350 353 357 35b 35c 35e 35f
364 368 36c 370 373 374 379 37d
37e 383 387 38a 38d 391 395 396
398 399 39e 3a2 3a4 3a8 3ab 3af
3b3 3b6 3ba 3bb 3c0 3c2 3c6 3c8
3d4 3d8 3da 3de 3fa 3f6 3f5 402
40f 40b 3f2 417 424 41c 420 40a
42b 43c 434 438 407 443 433 448
44c 450 454 46d 45c 460 468 430
485 474 478 480 45b 48c 490 494
458 498 49c 4a0 4a1 4a3 4a6 4a9
4aa 4af 4b3 4b8 4b9 4be 4c0 4c4
4c7 4cb 4cf 4d2 4d5 4d6 4db 4df
4e3 4e7 4ea 4ee 4ef 4f4 4f8 4fc
4ff 503 504 509 50d 511 514 515
51a 51e 522 525 528 529 52e 532
536 53a 53d 541 544 549 54c 550
554 555 557 558 55d 562 565 569
56d 56e 570 571 576 579 57e 57f
584 587 58b 58f 590 592 593 598
59b 5a0 5a1 5a6 5a9 5ad 5b1 5b2
5b4 5b5 5ba 5bd 5c2 5c3 5c8 5c9
5ce 5d2 5d8 5da 5de 5e1 5e5 5e9
5ec 5f0 5f3 5f8 5fb 5ff 603 604
606 607 60c 611 614 618 61c 61d
61f 620 625 628 62d 62e 633 636
63a 63e 63f 641 642 647 64a 64f
650 655 656 65b 65d 661 668 66c
670 674 676 67a 67c 688 68c 68e
692 6ae 6aa 6a9 6b6 6a6 6bb 6bf
6c3 6c7 6e0 6cf 6d3 6db 6ce 6fc
6eb 6ef 6f7 6cb 714 703 707 70f
6ea 731 71f 6e7 723 724 72c 71e
74d 73c 740 748 71b 765 754 758
760 73b 781 770 774 77c 738 76c
788 78c 78f 793 796 797 79c 7a0
7a4 7a7 7ab 7ae 7b3 7b7 7b8 7bd
7c1 7c5 7c8 7cc 7cf 7d4 7d8 7d9
7de 7e2 7e5 7e9 7ed 7f0 7f4 7f8
7fc 800 804 808 80c 810 811 813
817 81b 81f 822 825 826 82b 831
835 838 83c 840 844 846 84a 84e
852 855 856 85b 85f 863 866 869
86a 86f 873 877 87a 87e 87f 884
888 88b 88e 891 892 897 898 89d
8a0 8a4 8a7 8aa 8ab 8b0 1 8b3
8b8 8bc 8bf 8c2 8c3 8c8 1 8cb
8d0 8d4 8d8 8db 8df 8e0 8e5 8e9
8ed 8ee 8f3 8f5 8f9 8fc 900 904
908 90b 90e 911 912 917 918 91a
91e 922 923 925 929 92d 92f 933
936 939 93a 93f 943 947 94a 94e
94f 954 958 95c 960 963 966 969
96a 96f 970 972 976 97a 97c 980
984 989 98c 990 994 995 997 998
99d 9a0 9a5 9a6 9ab 9ac 9b1 9b3
9b7 9bb 9be 9c0 9c4 9cb 9cd 9d1
9d8 9dc 9e0 9e3 9e7 9ea 9eb 9f0
9f4 9f8 9fc 9fe a02 a04 a10 a14
a16 a1a a36 a32 a31 a3e a2e a43
a47 a4b a4f a6c a57 a5b a5e a5f
a67 a56 a8e a77 a53 a7b a7c a84
a89 a76 a95 a73 a99 a9a a9f aa3
aa6 aaa aac ab0 ab4 ab7 aba abb
ac0 ac4 ac6 aca ace ad1 ad5 ad9
adc ae0 ae4 ae7 ae8 aed aee af3
af7 afb aff b02 b06 b07 b0c b10
b14 b17 b1b b1c b21 b25 b29 b2c
b30 b31 b36 b3a b3e b41 b45 b46
b4b b4f b52 b55 b58 b59 b5e b61
b65 b69 b6a b6c b6d b72 b75 b78
b79 b7e b81 b85 b89 b8a b8c b8d
b92 b96 b9a b9e ba2 ba4 ba8 baa
bb6 bba bbc bc0 bdc bd8 bd7 be4
bf1 bed bd4 bf9 c02 bfe bec c0a
c17 c13 be9 c1f c28 c24 c12 c30
c3d c39 c0f c45 c38 c4a c4e c52
c56 c70 c5e c35 c62 c63 c6b c5d
c77 c7b c7f c5a c84 c86 c8a c8e
c92 c93 c98 c9c ca0 ca1 ca6 caa
cae cb2 cb5 cb6 cbb cbf cc3 cc4
cc9 ccd cd1 cd5 cd8 cd9 cde ce2
ce6 cea ced cee cf3 cf7 cfa cfe
d02 d05 d09 d0d d0f d13 d17 d1b
d1c d1e d1f d24 d26 d2a d31 d35
d38 d3c d40 d43 d47 d4b d4d d51
d55 d59 d5a d5c d5d d62 d64 d68
d6f d73 d76 d7a d7e d81 d85 d89
d8b d8f d93 d97 d98 d9a d9b da0
da2 da6 dad db1 db2 db7 dbb dbf
dc3 dc5 dc9 dcb dd7 ddb ddd de1
dfd df9 df8 e05 e12 e0e df5 e1a
e23 e1f e0d e2b e38 e34 e0a e40
e49 e45 e33 e51 e30 e56 e5a e5e
e62 e7f e6a e6e e71 e72 e7a e69
e86 e8a e8e e66 e93 e95 e99 e9d
ea1 ea2 ea7 eab eaf eb0 eb5 eb9
ebd ebe ec3 ec7 ecb ecc ed1 ed5
ed9 eda edf ee3 ee4 ee9 eed ef1
ef5 ef7 efb efd f09 f0d f0f f2b
f27 f26 f33 f40 f3c f23 f48 f51
f4d f3b f59 f66 f62 f38 f6e f77
f73 f61 f7f f8c f88 f5e f94 f87
f99 f9d fb6 fa5 fa9 fb1 f84 fd2
fbd fc1 fc4 fc5 fcd fa4 fd9 fdd
fe1 fe5 fe9 fed ff1 ff5 fa1 ff9
ffb fff 1003 1007 100b 100c 100e 1012
1016 101a 101c 1020 1022 102e 1032 1034
1038 1054 1050 104f 105c 1069 1065 104c
1071 107a 1076 1064 1082 108f 108b 1061
1097 10a0 109c 108a 10a8 1087 10ad 10b1
10b5 10b9 10d2 10c1 10c5 10cd 10c0 10ef
10dd 10bd 10e1 10e2 10ea 10dc 10f6 10fa
10fe 1102 1106 110a 110e 10d9 1112 1114
1118 111c 1120 1124 1125 1127 112b 112f
1133 1137 1139 113d 113f 114b 114f 1151
116d 1169 1168 1175 1182 117e 1165 118a
1197 118f 1193 117d 119e 117a 11a3 11a7
11d4 11af 11b3 11b7 11ba 11be 11c2 11c7
11cf 11ae 1201 11df 11e3 11ab 11e7 11eb
11ef 11f4 11fc 11de 122e 120c 1210 11db
1214 1218 121c 1221 1229 120b 125b 1239
123d 1208 1241 1245 1249 124e 1256 1238
1288 1266 126a 1235 126e 1272 1276 127b
1283 1265 12bc 1293 1297 1262 129b 129f
12a2 12a6 12aa 12af 12b7 1292 12d8 12c7
12cb 12d3 128f 12f0 12df 12e3 12eb 12c6
130c 12fb 12ff 1307 12c3 1328 1313 1317
131a 131b 1323 12fa 135c 1333 1337 12f7
133b 133f 1342 1346 134a 134f 1357 1332
1390 1367 136b 132f 136f 1373 1376 137a
137e 1383 138b 1366 13ad 139b 1363 139f
13a0 13a8 139a 13b4 13b8 13bc 1397 13c0
13c1 13c6 13ca 13ce 13d2 13d6 13da 13de
13e2 13e6 13ea 13ee 13f2 13f6 13fa 13fe
1402 1406 140a 140e 1412 1416 141a 141e
1422 1426 142a 142e 1432 143e 1440 1444
1448 144b 144e 144f 1454 1459 145c 1460
1464 1465 1467 1468 146d 146e 1473 1475
1476 147b 147f 1481 148d 148f 1493 1497
149b 149f 14a3 14a7 14ab 14af 14b3 14b7
14bb 14bf 14c3 14c7 14cb 14cf 14d3 14d7
14db 14e7 14e9 14ed 14f1 14f4 14f7 14f8
14fd 1502 1505 1509 150a 150f 1512 1517
1518 151d 151e 1523 1525 1526 152b 152f
1531 153d 153f 1543 1546 154b 154c 1551
1555 1559 155d 1560 1564 1567 156b 156f
1570 1572 1576 157a 157c 1580 1583 1588
1589 158e 1592 1596 159a 159e 15a1 15a5
15a8 15ac 15b0 15b1 15b3 15b4 15b6 15ba
15bc 15c0 15c4 15c7 15ca 15cb 15d0 15d5
15d8 15dc 15dd 15e2 15e5 15ea 15eb 15f0
15f1 15f6 15f8 15fc 1600 1603 1607 160a
160b 1610 1614 1618 161a 161e 1621 1625
1629 162d 1631 1635 1639 163d 163e 1640
1644 1648 164c 1650 1651 1653 1657 165b
165e 1662 1666 1669 166d 1671 1673 1677
167a 167b 1680 1684 1688 168c 1690 1691
1693 1694 1696 169a 169e 16a1 16a2 16a7
16ab 16af 16b2 16b5 16b6 16bb 16bf 16c3
16c7 16ca 16cb 16cd 16d1 16d5 16d8 16dc
16dd 16df 16e3 16e5 16e9 16ec 16ee 16f2
16f6 16fa 16fd 1701 1704 1708 170c 1710
1714 1715 1717 1718 171a 171e 1722 1725
1729 172d 1730 1734 1738 173a 173e 1742
1743 1745 1748 1749 174e 1752 1756 1759
175c 175d 1762 1766 176a 176e 1771 1772
1774 1778 177c 177f 1783 1787 1788 178a
178b 178d 1791 1793 1797 179a 179c 17a0
17a7 17a9 17ad 17b1 17b4 17b6 17ba 17c1
17c3 17c7 17c9 17d5 17d9 17db 17f7 17f3
17f2 17ff 180c 1808 17ef 1814 181d 1819
1807 1825 1832 182e 1804 183a 1843 183f
182d 184b 185c 1854 1858 182a 1863 1853
1868 186c 1870 1874 1878 187c 1880 1884
1888 1850 188c 188e 1892 1895 1899 189b
189f 18a1 18ad 18b1 18b3 18cf 18cb 18ca
18d7 18c7 18dc 18e0 18e4 18e8 18ec 18f0
18f2 18f6 18f8 1904 1908 190a 1926 1922
1921 192e 191e 1933 1937 193b 193f 1943
1947 1949 194d 194f 195b 195f 1961 1975
1976 197a 1997 1982 1986 1989 198a 1992
1981 199e 19a2 19a6 197e 19ab 19ad 19b1
19b5 19b6 19bb 19bd 19c1 19c3 19cf 19d3
19d5 19d7 19d9 19dd 19e9 19eb 19ee 19f0
19f1 19fa
692
2
0 1 9 e 3 b 19 1d
19 2c :2 19 :2 3 b 24 28 24
37 :2 24 :2 3 20 29 32 31 29
39 20 :2 3 20 :2 29 33 20 :2 3
:2 29 33 29 :2 3 29 32 31 29
39 29 :2 3 :3 29 :2 3 :3 29 3 d
0 :2 3 5 16 5 8 :2 12 1f
2b :2 8 36 39 :2 36 7 1f 20
:2 1f b :2 7 3b :2 5 :6 3 d 5
29 :2 5 18 :2 3 5 :2 f 17 2e
31 :2 17 3d 40 :2 17 :3 5 :2 9 :2 14
20 34 :3 5 :2 9 :2 14 1f 20 :2 1f
27 :2 5 :6 3 d 5 29 :2 5 18
:2 3 5 16 24 26 :2 16 5 8
18 :3 16 :4 7 18 7 2a :3 5 :2 f
1c :2 5 :6 3 d 5 29 :2 5 18
:2 3 5 16 24 26 :2 16 28 2a
31 :2 2a :2 16 5 8 18 :3 16 :4 7
18 1a 1c 23 :2 1c :2 18 7 2a
:3 5 :2 f 1c :2 5 :7 3 c 5 29
:3 5 29 :3 5 25 29 :3 5 25 29
:2 5 1d 5 c :2 3 5 :3 29 :2 5
:3 29 :2 5 a :2 14 24 30 :2 a 3b
3e :2 3b 9 14 :2 9 40 :3 7 19
28 2a :2 19 :2 7 :2 11 20 :3 7 :2 11
20 :2 7 a 18 :3 16 9 1b 2a
2c :2 1b :2 9 :2 d :2 18 24 2d 30
38 :2 30 :2 24 f 25 28 30 :2 28
:3 f 12 :2 f 23 26 2e :2 26 :3 f
12 :2 f 1b 1e 26 :2 1e :2 f 36
39 :2 f :4 9 24 :3 7 :2 b :2 16 22
2b 2e 36 :2 2e :2 22 b c f
17 :2 f :2 b c f :2 b 18 1b
23 :2 1b :2 b 33 36 :2 b :2 7 5
9 3 5 c 5 :7 3 c 5
29 :2 5 1f 5 c :2 3 5 :3 29
:2 5 :3 29 :2 5 :3 29 :2 5 29 32 31
:2 29 :2 5 :3 29 :2 5 :3 29 :2 5 :3 29 :2 5
:2 9 :2 14 :3 5 :2 9 :2 14 20 2c :3 5
:2 9 :2 14 20 2d :3 5 17 :2 5 17
:2 5 7 19 2a 36 b 1b :2 19
:2 7 11 20 22 :2 20 7 b 10
13 22 10 7 9 13 :2 1d :2 13
9 c 13 15 :2 13 b :2 15 24
:2 b f 1d 1f 20 :2 1f :2 1d e
28 37 39 :2 37 27 :2 e 41 43
45 :2 43 40 :2 e d :2 17 26 :3 d
18 :2 d 48 :3 b 14 :2 1d 23 25
:2 14 :2 b 2b 33 :2 2b b 9 17
f 16 18 :2 16 b :2 15 24 :3 b
14 :2 1d 23 25 :2 14 :2 b 2b b
1a 17 b 16 2b 2e 36 :2 2e
:2 16 3e f :2 16 :2 b :4 9 22 b
7 5 9 3 5 :2 9 :2 14 :3 5
c 5 :7 3 c 5 29 :2 5 19
5 c :2 3 5 29 32 31 :2 29
:2 5 29 32 31 29 39 29 5
:4 8 7 16 7 1c 7 16 22
24 :2 16 7 :5 5 13 1c 1f :2 29
:2 1f :2 13 :2 5 :2 f 1c :3 5 :2 f 1c
:3 5 :2 f 1c :3 5 :2 f 1c :3 5 16
19 1b :2 16 1d 1f 26 :2 1f :2 16
32 34 :2 16 36 38 3f :2 38 :2 16
:2 5 c 5 :7 3 c 5 29 :3 5
29 :3 5 29 :3 5 29 :3 5 29 :3 5
29 :2 5 16 5 c :2 3 5 29
32 31 :2 29 :2 5 13 20 :2 13 :2 5
10 :3 5 10 :3 5 10 :2 1f :3 5 10
:3 5 10 :2 29 :3 5 10 :2 2a :2 5 9
e 11 :2 20 26 e 5 7 12
21 :2 12 :2 7 26 9 5 9 e
11 :2 2a 30 e 5 7 12 2b
:2 12 :2 7 30 9 5 9 e 11
:2 2b 31 e 5 7 12 2c :2 12
:2 7 31 9 :5 5 c 5 :7 3 c
5 29 :3 5 29 :3 5 29 :3 5 29
:3 5 29 :2 5 16 5 c :2 3 5
29 32 31 :2 29 :2 5 13 20 :2 13
:2 5 10 :3 5 10 :3 5 10 :3 5 10
:3 5 10 :6 5 c 5 :6 3 d 5
29 :3 5 29 :3 5 29 :3 5 29 :3 5
29 :3 5 29 :2 5 13 :2 3 5 :3 29
:2 5 29 32 31 :2 29 :2 5 13 1d
30 11 21 34 11 :2 13 :2 5 11
24 :2 11 :3 5 :7 3 c 5 29 :3 5
29 :3 5 29 :3 5 29 :3 5 29 :2 5
19 5 c :2 3 5 :3 29 :2 5 29
32 31 :2 29 :2 5 13 1d 30 11
20 33 :2 13 :2 5 11 24 :2 11 :2 5
c 5 :6 3 d 5 29 :3 5 29
:3 5 24 28 :2 5 1a :2 3 5 29
34 29 :2 42 :3 29 :2 5 29 34 29
:2 48 :3 29 :2 5 29 34 29 :2 43 :3 29
:2 5 29 34 29 :2 40 :3 29 :2 5 29
34 29 :2 46 :3 29 :2 5 29 2d 29
38 29 :2 41 :3 29 :2 5 :3 29 :2 5 :3 29
:2 5 :3 29 :2 5 29 32 31 :2 29 :2 5
29 2d 29 38 29 :2 3d :3 29 :2 5
29 2d 29 38 29 :2 3d :3 29 :2 5
29 32 31 :2 29 :2 5 19 :2 1d :2 19
5 9 c 9 c 9 c 9
c 9 c 9 c 9 c :7 9
c 17 d 10 1d fffa 5 a
7 1f 20 :2 1f b 31 34 3c
:2 34 :2 b :2 7 18 :3 5 :3 3 e 10
e b f 1a b f 17 f
11 1c f 11 1f 21 f 11
18 7 5 a 7 1f 20 :2 1f
b 1f 22 :2 b 33 36 :2 b :2 7
18 :3 5 :3 3 8 13 15 :2 13 7
18 :2 1c :2 2c 32 3e :2 18 7 5
1c b 16 18 :2 16 7 18 20
:2 24 :2 34 41 b :2 20 :2 18 7 21
1c 7 1f 20 :2 1f b 16 19
:2 b 2a 2d :2 b :2 7 :4 5 :4 8 :2 7
1e :3 5 13 1d 2e 11 20 2f
:2 13 :2 5 11 24 :2 11 5 9 e
11 :2 1a 20 e 5 :4 a 9 17
21 2a :2 21 :2 17 9 :4 c b :2 1c
23 :3 b 1c :2 2d :2 b 37 :2 3b 46
:2 37 b 23 :2 9 22 9 18 :2 1c
:2 2d 3c d 1e 27 :2 1e :2 18 9
d 12 15 :2 21 27 12 9 e
1a :5 e d :2 1e 25 :3 d 1e :2 2f
:2 d 39 :2 3d 48 54 :2 48 :2 39 d
29 :2 b 27 d 9 :4 7 20 9
5 :6 3 d 5 29 :3 5 29 :3 5
29 :3 5 29 :3 5 29 :3 5 25 29
:2 5 24 :2 3 5 10 1d 30 9
18 2b :2 10 :2 44 5 :6 3 d 5
29 :2 5 19 :2 3 5 13 5 :6 3
d 5 29 :2 5 18 :2 3 5 12
5 :6 3 d 0 :2 3 5 29 32
31 :2 29 :2 5 13 20 :2 13 :4 5 :a 3
:6 1
692
4
0 :3 1 :9 6 :9 7
:9 c :7 d :6 e :8 f
:5 10 :5 11 16 0
:2 16 :3 19 :b 1a :5 1b
1c :2 1b :3 1a :2 18
:4 16 23 :4 24 :3 23
:e 27 :9 28 :c 29 :2 26
:4 23 2f :4 30 :3 2f
:7 33 :5 34 :3 35 :3 36
:3 34 :6 38 :2 32 :4 2f
3e :4 3f :3 3e :e 42
:5 43 :3 44 :a 45 :3 43
:6 47 :2 41 :4 3e :2 4d
:4 4e :4 4f :5 50 :5 51
4d :2 52 :2 4d :5 53
:5 54 57 :b 5a :4 5b
:3 5a :7 5e :6 62 :6 63
:5 65 :7 66 :d 67 :8 68
:2 69 :2 68 :5 69 :2 68
:2 6a :2 68 :5 6a :2 68
:2 6a :2 68 :2 67 :2 6b
:3 65 :d 6e 6f :5 70
:2 6f :2 71 :2 6f :5 71
:2 6f :2 71 :2 6f :2 6e
57 73 55 :3 75
:2 55 :4 4d :2 7c :4 7d
7c :2 7e :2 7c :5 7f
:5 80 :5 81 :7 82 :5 83
:5 84 :5 85 :7 89 :9 8a
:9 8b :3 8d :3 8e 90
:4 93 :2 94 :3 93 :7 95
:6 98 :7 99 :5 9a :6 9b
:19 9f :6 a0 :4 a1 :3 9f
:f a3 a4 9a :5 a4
:6 a5 :c a6 a4 9a
:a a8 a9 :4 a8 a7
:3 9a 98 ab 98
90 ad 86 :7 b0
:3 b2 :2 86 :4 7c :2 b8
:4 b9 b8 :2 ba :2 b8
:7 bb :8 bc :4 be :3 bf
be :7 c1 c0 :3 be
:b c3 :6 c4 :6 c5 :6 c6
:6 c7 :19 c8 :3 c9 :2 bd
:4 b8 :2 cf :4 d0 :4 d1
:4 d2 :4 d3 :4 d4 :4 d5
cf :2 d6 :2 cf :7 d7
:6 da :4 db :4 dc :6 dd
:4 de :6 df :6 e0 :8 e1
:7 e2 e1 e3 e1
:8 e4 :7 e5 e4 e6
e4 :8 e7 :7 e8 e7
e9 e7 :3 ea :3 eb
:2 d8 :4 cf :2 f1 :4 f2
:4 f3 :4 f4 :4 f5 :4 f6
f1 :2 f7 :2 f1 :7 f8
:6 fb :4 fc :4 fd :4 fe
:4 ff :4 100 :3 101 :3 102
:2 f9 :4 f1 108 :4 109
:4 10a :4 10b :4 10c :4 10d
:4 10e :3 108 :5 110 :7 111
:4 114 :3 115 116 :3 114
:6 119 :2 11b :2 112 :4 108
:2 121 :4 122 :4 123 :4 124
:4 125 :4 126 121 :2 127
:2 121 :5 128 :7 129 :4 12c
:3 12d :3 12c :6 130 :3 131
:2 12a :4 121 137 :4 138
:4 139 :5 13a :3 137 :a 13c
:a 13d :a 13e :a 13f :a 140
:c 141 :5 142 :5 143 :5 144
:7 145 :c 146 :c 147 :7 148
:7 14b :2 150 :2 151 :2 152
:2 153 :2 154 :2 155 :2 156
158 159 15a 15b
15c 15d 15e :2 15f
:3 160 150 14e 162
:5 163 :8 164 :2 163 :3 162
161 :3 149 :2 169 16a
:3 16c :3 16d :3 16e :4 16f
:3 170 169 168 172
:5 173 :9 174 :2 173 :3 172
171 :3 149 :5 178 :b 179
17a 178 :5 17a :8 17b
17c :5 17b 17a 178
:5 17e :9 17f :2 17e 17d
:3 178 :4 181 :2 182 :3 181
:4 186 :3 187 :3 186 :6 188
:8 18b :4 18d :9 18e :4 18f
:6 190 :d 191 :3 18f 18d
:7 194 :5 195 :3 194 :8 196
:7 197 :6 198 :10 199 :3 197
196 19b 196 193
:3 18d 18b 19e 18b
:2 149 :4 137 1a5 :4 1a6
:4 1a7 :4 1a8 :4 1a9 :4 1aa
:5 1ab :3 1a5 :4 1ae :3 1af
:2 1ae :2 1af 1ae :2 1ad
:4 1a5 1b5 :4 1b6 :3 1b5
:3 1b9 :2 1b8 :4 1b5 1bf
:4 1c0 :3 1bf :3 1c3 :2 1c2
:4 1bf 1c9 0 :2 1c9
:7 1cb :6 1cd :3 1ce :2 1cc
:4 1c9 :4 16 :6 1
19fc
4
:3 0 1 :3 0 2
:3 0 3 :6 0 1
:2 0 4 :3 0 5
:3 0 6 :3 0 7
:2 0 4 6 7
0 5 :2 0 4
8 9 0 a
:7 0 5 b 68c
4 :3 0 8 :3 0
6 :3 0 7 :2 0
4 f 10 0
8 :2 0 4 11
12 0 13 :7 0
e 14 68c 10
:2 0 5 a :3 0
b :3 0 c :2 0
3 18 1a :6 0
d :4 0 1e 1b
1c 68c 9 :6 0
12 :2 0 7 a
:3 0 f :3 0 21
:7 0 25 22 23
68c e :6 0 c
:2 0 9 f :3 0
27 :7 0 2b 28
29 68c 0 11
:6 0 f e4 0
d b :3 0 b
2d 2f :6 0 14
:4 0 33 30 31
68c 0 13 :9 0
11 f :3 0 35
:7 0 38 36 0
68c 0 15 :6 0
f :3 0 3a :7 0
3d 3b 0 68c
0 16 :6 0 17
:a 0 5f 2 :7 0
3f :2 0 5f 3e
40 :2 0 15 :3 0
18 :2 0 42 43
0 5b 19 :3 0
1a :3 0 45 46
0 13 :3 0 11
:3 0 13 47 4a
1b :2 0 18 :2 0
18 4c 4e :3 0
1c :3 0 1d :2 0
1e :2 0 1b 51
53 :3 0 1f :4 0
1d 50 56 :2 0
58 20 59 4f
58 0 5a 22
0 5b 24 5e
:3 0 5e 0 5e
5d 5b 5c :6 0
5f 1 0 3e
40 5e 68c :2 0
20 :a 0 90 3
:7 0 29 :2 0 27
b :3 0 21 :7 0
64 63 :3 0 66
:2 0 90 61 67
:2 0 22 :3 0 23
:3 0 69 6a 0
24 :4 0 25 :2 0
21 :3 0 2b 6d
6f :3 0 25 :2 0
26 :4 0 2e 71
73 :3 0 31 6b
75 :2 0 8c 6
:3 0 27 :3 0 77
78 0 28 :3 0
79 7a 0 29
:4 0 21 :3 0 33
7b 7e :2 0 8c
6 :3 0 27 :3 0
80 81 0 20
:3 0 82 83 0
1d :2 0 1e :2 0
36 85 87 :3 0
21 :3 0 38 84
8a :2 0 8c 3b
8f :3 0 8f 0
8f 8e 8c 8d
:6 0 90 1 0
61 67 8f 68c
:2 0 2a :a 0 b9
4 :7 0 41 :2 0
3f f :3 0 2b
:7 0 95 94 :3 0
97 :2 0 b9 92
98 :2 0 15 :3 0
15 :3 0 2c :2 0
2d :2 0 43 9c
9e :3 0 9a 9f
0 b5 15 :3 0
e :3 0 2e :2 0
48 a3 a4 :3 0
17 :4 0 a6 a7
:2 0 ac 15 :3 0
2d :2 0 a9 aa
0 ac 4b ad
a5 ac 0 ae
4e 0 b5 19
:3 0 2f :3 0 af
b0 0 2b :3 0
50 b1 b3 :2 0
b5 52 b8 :3 0
b8 0 b8 b7
b5 b6 :6 0 b9
1 0 92 98
b8 68c :2 0 2a
:a 0 f0 5 :7 0
58 :2 0 56 b
:3 0 2b :7 0 be
bd :3 0 c0 :2 0
f0 bb c1 :2 0
15 :3 0 15 :3 0
2c :2 0 30 :2 0
5a c5 c7 :3 0
2c :2 0 31 :3 0
2b :3 0 5d ca
cc 5f c9 ce
:3 0 c3 cf 0
ec 15 :3 0 e
:3 0 2e :2 0 64
d3 d4 :3 0 17
:4 0 d6 d7 :2 0
e3 15 :3 0 30
:2 0 2c :2 0 31
:3 0 2b :3 0 67
dc de 69 db
e0 :3 0 d9 e1
0 e3 6c e4
d5 e3 0 e5
6f 0 ec 19
:3 0 2f :3 0 e6
e7 0 2b :3 0
71 e8 ea :2 0
ec 73 ef :3 0
ef 0 ef ee
ec ed :6 0 f0
1 0 bb c1
ef 68c :2 0 32
:3 0 33 :a 0 1ad
6 :7 0 79 407
0 77 b :3 0
34 :7 0 f6 f5
:3 0 7d 430 0
7b f :3 0 35
:7 0 fa f9 :3 0
37 :3 0 38 :3 0
36 :6 0 ff fe
:3 0 84 458 0
7f 37 :3 0 38
:3 0 39 :6 0 104
103 :3 0 3a :3 0
f :3 0 106 108
0 1ad f3 109
:2 0 116 117 0
86 f :3 0 10c
:7 0 10f 10d 0
1ab 0 3b :6 0
f :3 0 111 :7 0
114 112 0 1ab
0 3c :6 0 3d
:3 0 19 :3 0 3e
:3 0 34 :3 0 11
:3 0 88 118 11b
1b :2 0 18 :2 0
8d 11d 11f :3 0
20 :3 0 3f :4 0
90 121 123 :2 0
125 92 126 120
125 0 127 94
0 1a3 36 :3 0
36 :3 0 2c :2 0
40 :2 0 96 12a
12c :3 0 128 12d
0 1a3 19 :3 0
41 :3 0 12f 130
0 3b :3 0 99
131 133 :2 0 1a3
19 :3 0 41 :3 0
135 136 0 3c
:3 0 9b 137 139
:2 0 1a3 3b :3 0
35 :3 0 42 :2 0
9f 13d 13e :3 0
39 :3 0 39 :3 0
2c :2 0 40 :2 0
a2 142 144 :3 0
140 145 0 17a
6 :3 0 27 :3 0
147 148 0 28
:3 0 149 14a 0
43 :4 0 25 :2 0
44 :3 0 36 :3 0
a5 14e 150 a7
14d 152 :3 0 45
:4 0 25 :2 0 44
:3 0 16 :3 0 aa
156 158 ac 155
15a :3 0 25 :2 0
46 :4 0 af 15c
15e :3 0 25 :2 0
44 :3 0 39 :3 0
b2 161 163 b4
160 165 :3 0 25
:2 0 47 :4 0 b7
167 169 :3 0 25
:2 0 44 :3 0 3c
:3 0 ba 16c 16e
bc 16b 170 :3 0
25 :2 0 48 :4 0
bf 172 174 :3 0
c2 14b 176 :2 0
17a 49 :8 0 17a
c5 17b 13f 17a
0 17c c9 0
1a3 6 :3 0 27
:3 0 17d 17e 0
28 :3 0 17f 180
0 43 :4 0 25
:2 0 44 :3 0 36
:3 0 cb 184 186
cd 183 188 :3 0
4a :4 0 25 :2 0
44 :3 0 3b :3 0
d0 18c 18e d2
18b 190 :3 0 25
:2 0 47 :4 0 d5
192 194 :3 0 25
:2 0 44 :3 0 3c
:3 0 d8 197 199
da 196 19b :3 0
25 :2 0 4b :4 0
dd 19d 19f :3 0
e0 181 1a1 :2 0
1a3 e3 1a5 3d
:4 0 1a3 :4 0 1a9
3a :3 0 3c :3 0
1a7 :2 0 1a9 ea
1ac :3 0 1ac ed
1ac 1ab 1a9 1aa
:6 0 1ad 1 0
f3 109 1ac 68c
:2 0 32 :3 0 4c
:a 0 29e 8 :7 0
f2 :2 0 f0 b
:3 0 34 :7 0 1b3
1b2 :3 0 3a :3 0
4d :3 0 1b5 1b7
0 29e 1b0 1b8
:2 0 f6 6e7 0
f4 f :3 0 1bb
:7 0 1be 1bc 0
29c 0 3c :6 0
10 :2 0 f8 f
:3 0 1c0 :7 0 1c3
1c1 0 29c 0
4e :6 0 f :3 0
1c5 :7 0 1c8 1c6
0 29c 0 4f
:6 0 fe 738 0
fc b :3 0 fa
1ca 1cc :6 0 1cf
1cd 0 29c 0
50 :6 0 102 76c
0 100 4d :3 0
1d1 :7 0 1d4 1d2
0 29c 0 51
:6 0 38 :3 0 1d6
:7 0 1d9 1d7 0
29c 0 52 :6 0
6 :3 0 38 :3 0
1db :7 0 1de 1dc
0 29c 0 53
:6 0 27 :3 0 1df
1e0 0 54 :3 0
1e1 1e2 :2 0 1e3
1e4 :2 0 29a 6
:3 0 27 :3 0 1e6
1e7 0 28 :3 0
1e8 1e9 0 55
:4 0 13 :3 0 104
1ea 1ed :2 0 29a
6 :3 0 27 :3 0
1ef 1f0 0 28
:3 0 1f1 1f2 0
56 :4 0 16 :3 0
107 1f3 1f6 :2 0
29a 52 :3 0 18
:2 0 1f8 1f9 0
29a 53 :3 0 18
:2 0 1fb 1fc 0
29a 3d :3 0 3c
:3 0 33 :3 0 34
:3 0 16 :3 0 52
:3 0 53 :3 0 10a
200 205 1ff 206
0 28d 49 :3 0
3c :3 0 42 :2 0
18 :2 0 111 20a
20c :4 0 20d :3 0
28d 57 :3 0 40
:2 0 3c :3 0 3d
:3 0 210 211 :2 0
20f 213 4e :3 0
19 :3 0 58 :3 0
216 217 :2 0 218
219 :3 0 215 21a
0 28a 4e :3 0
42 :2 0 59 :2 0
116 21d 21f :3 0
19 :3 0 41 :3 0
221 222 0 4f
:3 0 119 223 225
:2 0 25d 4f :3 0
42 :2 0 1d :2 0
40 :2 0 11b 229
22b :3 0 11f 228
22d :3 0 22e :2 0
53 :3 0 42 :2 0
40 :2 0 124 231
233 :3 0 234 :2 0
22f 236 235 :2 0
57 :3 0 42 :2 0
40 :2 0 129 239
23b :3 0 23c :2 0
237 23e 23d :2 0
19 :3 0 41 :3 0
240 241 0 50
:3 0 12c 242 244
:2 0 24a 20 :3 0
50 :3 0 12e 246
248 :2 0 24a 130
24b 23f 24a 0
24c 133 0 25d
51 :3 0 51 :3 0
5a :3 0 24e 24f
0 2c :2 0 40
:2 0 135 251 253
:3 0 138 24d 255
44 :3 0 4f :3 0
13a 257 259 256
25a 0 25d 5b
:3 0 13c 287 4e
:3 0 42 :2 0 5c
:2 0 142 25f 261
:3 0 19 :3 0 41
:3 0 263 264 0
50 :3 0 145 265
267 :2 0 275 51
:3 0 51 :3 0 5a
:3 0 26a 26b 0
2c :2 0 40 :2 0
147 26d 26f :3 0
14a 269 271 50
:3 0 272 273 0
275 14c 276 262
275 0 289 20
:3 0 5d :4 0 25
:2 0 44 :3 0 4e
:3 0 14f 27a 27c
151 279 27e :3 0
25 :2 0 5e :4 0
154 280 282 :3 0
157 277 284 :2 0
286 159 288 220
25d 0 289 0
286 0 289 15b
0 28a 15f 28c
3d :3 0 214 28a
:4 0 28d 162 28f
3d :4 0 28d :4 0
29a 6 :3 0 27
:3 0 290 291 0
54 :3 0 292 293
:2 0 294 295 :2 0
29a 3a :3 0 51
:3 0 298 :2 0 29a
166 29d :3 0 29d
16f 29d 29c 29a
29b :6 0 29e 1
0 1b0 1b8 29d
68c :2 0 32 :3 0
5f :a 0 310 b
:7 0 179 :2 0 177
b :3 0 60 :7 0
2a4 2a3 :3 0 3a
:3 0 b :3 0 2a6
2a8 0 310 2a1
2a9 :2 0 c :2 0
17d b :3 0 62
:2 0 17b 2ac 2ae
:6 0 2b1 2af 0
30e 0 61 :6 0
65 :2 0 181 b
:3 0 17f 2b3 2b5
:6 0 64 :4 0 2b9
2b6 2b7 30e 0
63 :6 0 16 :3 0
183 2bb 2bc :3 0
16 :3 0 40 :2 0
2be 2bf 0 2c1
185 2ca 16 :3 0
16 :3 0 2c :2 0
40 :2 0 187 2c4
2c6 :3 0 2c2 2c7
0 2c9 18a 2cb
2bd 2c1 0 2cc
0 2c9 0 2cc
18c 0 30c 61
:3 0 63 :3 0 25
:2 0 19 :3 0 66
:3 0 2d0 2d1 :2 0
2d2 2d3 :3 0 18f
2cf 2d5 :3 0 2cd
2d6 0 30c 19
:3 0 2f :3 0 2d8
2d9 0 9 :3 0
192 2da 2dc :2 0
30c 19 :3 0 2f
:3 0 2de 2df 0
61 :3 0 194 2e0
2e2 :2 0 30c 19
:3 0 2f :3 0 2e4
2e5 0 60 :3 0
196 2e6 2e8 :2 0
30c 19 :3 0 2f
:3 0 2ea 2eb 0
16 :3 0 198 2ec
2ee :2 0 30c 15
:3 0 2d :2 0 2c
:2 0 30 :2 0 19a
2f2 2f4 :3 0 2c
:2 0 31 :3 0 61
:3 0 19d 2f7 2f9
19f 2f6 2fb :3 0
2c :2 0 30 :2 0
1a2 2fd 2ff :3 0
2c :2 0 31 :3 0
60 :3 0 1a5 302
304 1a7 301 306
:3 0 2f0 307 0
30c 3a :3 0 61
:3 0 30a :2 0 30c
1aa 30f :3 0 30f
1b3 30f 30e 30c
30d :6 0 310 1
0 2a1 2a9 30f
68c :2 0 32 :3 0
67 :a 0 39c c
:7 0 1b8 be9 0
1b6 b :3 0 68
:7 0 316 315 :3 0
1bc c0f 0 1ba
b :3 0 69 :7 0
31a 319 :3 0 4d
:3 0 6a :7 0 31e
31d :3 0 1c0 c35
0 1be b :3 0
6b :7 0 322 321
:3 0 4d :3 0 6c
:7 0 326 325 :3 0
62 :2 0 1c2 4d
:3 0 6d :7 0 32a
329 :3 0 3a :3 0
b :3 0 32c 32e
0 39c 313 32f
:2 0 1cd :2 0 1cb
b :3 0 1c9 332
334 :6 0 337 335
0 39a 0 61
:6 0 61 :3 0 5f
:3 0 6e :4 0 339
33b 338 33c 0
398 2a :3 0 68
:3 0 1cf 33e 340
:2 0 398 2a :3 0
69 :3 0 1d1 342
344 :2 0 398 2a
:3 0 6a :3 0 5a
:3 0 347 348 0
1d3 346 34a :2 0
398 2a :3 0 6b
:3 0 1d5 34c 34e
:2 0 398 2a :3 0
6c :3 0 5a :3 0
351 352 0 1d7
350 354 :2 0 398
2a :3 0 6d :3 0
5a :3 0 357 358
0 1d9 356 35a
:2 0 398 57 :3 0
40 :2 0 6a :3 0
5a :3 0 35e 35f
0 3d :3 0 35d
360 :2 0 35c 362
2a :3 0 6a :3 0
57 :3 0 1db 365
367 1dd 364 369
:2 0 36b 1df 36d
3d :3 0 363 36b
:4 0 398 57 :3 0
40 :2 0 6c :3 0
5a :3 0 370 371
0 3d :3 0 36f
372 :2 0 36e 374
2a :3 0 6c :3 0
57 :3 0 1e1 377
379 1e3 376 37b
:2 0 37d 1e5 37f
3d :3 0 375 37d
:4 0 398 57 :3 0
40 :2 0 6d :3 0
5a :3 0 382 383
0 3d :3 0 381
384 :2 0 380 386
2a :3 0 6d :3 0
57 :3 0 1e7 389
38b 1e9 388 38d
:2 0 38f 1eb 391
3d :3 0 387 38f
:4 0 398 17 :4 0
392 393 :2 0 398
3a :3 0 61 :3 0
396 :2 0 398 1ed
39b :3 0 39b 1fa
39b 39a 398 399
:6 0 39c 1 0
313 32f 39b 68c
:2 0 32 :3 0 6f
:a 0 3e4 10 :7 0
1fe e0a 0 1fc
b :3 0 70 :7 0
3a2 3a1 :3 0 202
e30 0 200 b
:3 0 71 :7 0 3a6
3a5 :3 0 b :3 0
72 :7 0 3aa 3a9
:3 0 206 :2 0 204
b :3 0 73 :7 0
3ae 3ad :3 0 b
:3 0 74 :7 0 3b2
3b1 :3 0 3a :3 0
b :3 0 3b4 3b6
0 3e4 39f 3b7
:2 0 210 :2 0 20e
b :3 0 62 :2 0
20c 3ba 3bc :6 0
3bf 3bd 0 3e2
0 61 :6 0 61
:3 0 5f :3 0 75
:4 0 3c1 3c3 3c0
3c4 0 3e0 2a
:3 0 70 :3 0 212
3c6 3c8 :2 0 3e0
2a :3 0 71 :3 0
214 3ca 3cc :2 0
3e0 2a :3 0 72
:3 0 216 3ce 3d0
:2 0 3e0 2a :3 0
73 :3 0 218 3d2
3d4 :2 0 3e0 2a
:3 0 74 :3 0 21a
3d6 3d8 :2 0 3e0
17 :4 0 3da 3db
:2 0 3e0 3a :3 0
61 :3 0 3de :2 0
3e0 21c 3e3 :3 0
3e3 225 3e3 3e2
3e0 3e1 :6 0 3e4
1 0 39f 3b7
3e3 68c :2 0 76
:a 0 425 11 :7 0
229 f38 0 227
b :3 0 68 :7 0
3e9 3e8 :3 0 22d
f5e 0 22b b
:3 0 69 :7 0 3ed
3ec :3 0 4d :3 0
6a :7 0 3f1 3f0
:3 0 231 f84 0
22f b :3 0 6b
:7 0 3f5 3f4 :3 0
4d :3 0 77 :7 0
3f9 3f8 :3 0 23a
fa1 0 233 4d
:3 0 78 :7 0 3fd
3fc :3 0 3ff :2 0
425 3e6 400 :2 0
240 :2 0 23e 4d
:3 0 403 :7 0 406
404 0 423 0
51 :6 0 b :3 0
62 :2 0 23c 408
40a :6 0 40d 40b
0 423 0 61
:6 0 61 :3 0 67
:3 0 68 :3 0 69
:3 0 6a :3 0 6b
:3 0 77 :3 0 78
:3 0 40f 416 40e
417 0 421 51
:3 0 4c :3 0 61
:3 0 247 41a 41c
419 41d 0 421
3a :6 0 421 249
424 :3 0 424 24d
424 423 421 422
:6 0 425 1 0
3e6 400 424 68c
:2 0 32 :3 0 79
:a 0 465 12 :7 0
252 1061 0 250
b :3 0 70 :7 0
42b 42a :3 0 256
1087 0 254 b
:3 0 71 :7 0 42f
42e :3 0 b :3 0
72 :7 0 433 432
:3 0 25a :2 0 258
b :3 0 73 :7 0
437 436 :3 0 b
:3 0 74 :7 0 43b
43a :3 0 3a :3 0
4d :3 0 43d 43f
0 465 428 440
:2 0 62 :2 0 260
4d :3 0 443 :7 0
446 444 0 463
0 51 :6 0 266
:2 0 264 b :3 0
262 448 44a :6 0
44d 44b 0 463
0 61 :6 0 61
:3 0 6f :3 0 70
:3 0 71 :3 0 72
:3 0 73 :3 0 74
:3 0 44f 455 44e
456 0 461 51
:3 0 4c :3 0 61
:3 0 26c 459 45b
458 45c 0 461
3a :3 0 51 :3 0
45f :2 0 461 26e
464 :3 0 464 272
464 463 461 462
:6 0 465 1 0
428 440 464 68c
:2 0 79 :a 0 61b
13 :7 0 277 117a
0 275 7b :3 0
7a :7 0 46a 469
:3 0 27b :2 0 279
7b :3 0 7c :7 0
46e 46d :3 0 37
:3 0 7e :3 0 7d
:6 0 473 472 :3 0
475 :2 0 61b 467
476 :2 0 483 484
0 27f 80 :3 0
81 :2 0 4 479
47a 0 82 :3 0
82 :2 0 1 47b
47d :3 0 47e :7 0
481 47f 0 619
0 7f :6 0 48d
48e 0 281 80
:3 0 84 :2 0 4
82 :3 0 82 :2 0
1 485 487 :3 0
488 :7 0 48b 489
0 619 0 83
:6 0 497 498 0
283 80 :3 0 86
:2 0 4 82 :3 0
82 :2 0 1 48f
491 :3 0 492 :7 0
495 493 0 619
0 85 :6 0 4a1
4a2 0 285 80
:3 0 88 :2 0 4
82 :3 0 82 :2 0
1 499 49b :3 0
49c :7 0 49f 49d
0 619 0 87
:6 0 4ab 4ac 0
287 80 :3 0 8a
:2 0 4 82 :3 0
82 :2 0 1 4a3
4a5 :3 0 4a6 :7 0
4a9 4a7 0 619
0 89 :6 0 28b
12c3 0 289 6
:3 0 8c :2 0 4
8d :2 0 4 4ad
4ae 0 82 :3 0
82 :2 0 1 4af
4b1 :3 0 4b2 :7 0
4b5 4b3 0 619
0 8b :6 0 28f
12f7 0 28d 4d
:3 0 4b7 :7 0 4ba
4b8 0 619 0
51 :6 0 7b :3 0
4bc :7 0 4bf 4bd
0 619 0 8e
:6 0 4cd 4ce 0
293 5 :3 0 4c1
:7 0 4c4 4c2 0
619 0 8f :6 0
b :3 0 62 :2 0
291 4c6 4c8 :6 0
4cb 4c9 0 619
0 61 :6 0 4d9
4da 0 295 6
:3 0 91 :2 0 4
92 :2 0 4 4cf
4d0 0 82 :3 0
82 :2 0 1 4d1
4d3 :3 0 4d4 :7 0
4d7 4d5 0 619
0 90 :6 0 10
:2 0 297 6 :3 0
8c :2 0 4 92
:2 0 4 4db 4dc
0 82 :3 0 82
:2 0 1 4dd 4df
:3 0 4e0 :7 0 4e3
4e1 0 619 0
93 :6 0 4ec 4ed
0 29b b :3 0
299 4e5 4e7 :6 0
4ea 4e8 0 619
0 50 :6 0 7d
:3 0 6 :3 0 7e
:4 0 4ee 4ef :3 0
4eb 4f0 0 617
94 :3 0 81 :3 0
94 :3 0 84 :3 0
94 :3 0 86 :3 0
94 :3 0 88 :3 0
94 :3 0 8a :3 0
94 :3 0 95 :3 0
94 :3 0 96 :3 0
7f :3 0 83 :3 0
85 :3 0 87 :3 0
89 :3 0 90 :3 0
93 :3 0 80 :3 0
94 :3 0 94 :3 0
97 :3 0 7c :4 0
98 1 :8 0 50d
29d 523 99 :3 0
1c :3 0 1d :2 0
1e :2 0 29f 510
512 :3 0 9a :4 0
25 :2 0 44 :3 0
7c :3 0 2a1 516
518 2a3 515 51a
:3 0 2a6 50f 51c
:2 0 51e 2a9 520
2ab 51f 51e :2 0
521 2ad :2 0 523
0 523 522 50d
521 :6 0 617 13
:3 0 9b :3 0 8d
:3 0 8b :3 0 6
:3 0 8c :3 0 9b
:3 0 6 :3 0 9c
:3 0 9d :3 0 9d
:3 0 9e :3 0 7a
:3 0 9b :3 0 9f
:3 0 9d :3 0 9f
:3 0 9b :3 0 92
:3 0 85 :4 0 a0
1 :8 0 539 2af
550 99 :3 0 1c
:3 0 1d :2 0 1e
:2 0 2b1 53c 53e
:3 0 a1 :4 0 25
:2 0 85 :3 0 2b3
541 543 :3 0 25
:2 0 a2 :4 0 2b6
545 547 :3 0 2b9
53b 549 :2 0 54b
2bc 54d 2be 54c
54b :2 0 54e 2c0
:2 0 550 0 550
54f 539 54e :6 0
617 13 :3 0 8b
:3 0 42 :2 0 a3
:4 0 2c4 553 555
:3 0 50 :3 0 6
:3 0 a4 :3 0 558
559 0 a5 :3 0
55a 55b 0 7a
:3 0 85 :3 0 2c7
55c 55f 557 560
0 563 5b :3 0
2ca 58a 8b :3 0
42 :2 0 a6 :4 0
2ce 565 567 :3 0
50 :3 0 44 :3 0
6 :3 0 a4 :3 0
56b 56c 0 a7
:3 0 56d 56e 0
7a :3 0 85 :3 0
2d1 56f 572 2d4
56a 574 569 575
0 577 2d6 578
568 577 0 58c
1c :3 0 1d :2 0
1e :2 0 2d8 57a
57c :3 0 a8 :4 0
25 :2 0 85 :3 0
2da 57f 581 :3 0
25 :2 0 a9 :4 0
2dd 583 585 :3 0
2e0 579 587 :2 0
589 2e3 58b 556
563 0 58c 0
589 0 58c 2e5
0 617 50 :3 0
65 :2 0 2e9 58e
58f :3 0 3a :6 0
593 2eb 594 590
593 0 595 2ed
0 617 61 :3 0
6f :3 0 7f :3 0
83 :3 0 50 :3 0
87 :3 0 89 :3 0
2ef 597 59d 596
59e 0 617 51
:3 0 4c :3 0 61
:3 0 2f5 5a1 5a3
5a0 5a4 0 617
57 :3 0 40 :2 0
51 :3 0 5a :3 0
5a8 5a9 0 3d
:3 0 5a7 5aa :2 0
5a6 5ac 90 :3 0
65 :2 0 2f7 5af
5b0 :3 0 8e :3 0
aa :3 0 51 :3 0
57 :3 0 2f9 5b4
5b6 2fb 5b3 5b8
5b2 5b9 0 5d5
8e :3 0 ab :2 0
2fd 5bc 5bd :3 0
7d :3 0 ac :3 0
5bf 5c0 0 40
:2 0 2ff 5c1 5c3
:2 0 5d2 7d :3 0
7d :3 0 5a :3 0
5c6 5c7 0 301
5c5 5c9 6 :3 0
ad :3 0 5cb 5cc
0 8e :3 0 303
5cd 5cf 5ca 5d0
0 5d2 305 5d3
5be 5d2 0 5d4
308 0 5d5 30a
611 8f :3 0 6
:3 0 ae :3 0 5d7
5d8 0 af :3 0
5d9 5da 0 90
:3 0 93 :3 0 51
:3 0 57 :3 0 30d
5de 5e0 30f 5db
5e2 5d6 5e3 0
610 b0 :3 0 40
:2 0 8f :3 0 5a
:3 0 5e7 5e8 0
3d :3 0 5e6 5e9
:2 0 5e5 5eb 8f
:3 0 b0 :3 0 313
5ed 5ef ab :2 0
315 5f1 5f2 :3 0
7d :3 0 ac :3 0
5f4 5f5 0 40
:2 0 317 5f6 5f8
:2 0 60a 7d :3 0
7d :3 0 5a :3 0
5fb 5fc 0 319
5fa 5fe 6 :3 0
ad :3 0 600 601
0 8f :3 0 b0
:3 0 31b 603 605
31d 602 607 5ff
608 0 60a 31f
60b 5f3 60a 0
60c 322 0 60d
324 60f 3d :3 0
5ec 60d :4 0 610
326 612 5b1 5d5
0 613 0 610
0 613 329 0
614 32c 616 3d
:3 0 5ad 614 :4 0
617 32e 61a :3 0
61a 337 61a 619
617 618 :6 0 61b
1 0 467 476
61a 68c :2 0 b1
:a 0 64a 18 :7 0
347 1804 0 345
b :3 0 70 :7 0
620 61f :3 0 34b
182a 0 349 b
:3 0 71 :7 0 624
623 :3 0 b :3 0
72 :7 0 628 627
:3 0 34f 1850 0
34d b :3 0 73
:7 0 62c 62b :3 0
b :3 0 74 :7 0
630 62f :3 0 358
:2 0 351 37 :3 0
f :3 0 b2 :6 0
635 634 :3 0 637
:2 0 64a 61d 638
:2 0 b2 :3 0 79
:3 0 70 :3 0 71
:3 0 72 :3 0 73
:3 0 74 :3 0 63b
641 5a :3 0 642
643 0 63a 644
0 646 35e 649
:3 0 649 0 649
648 646 647 :6 0
64a 1 0 61d
638 649 68c :2 0
b3 :a 0 65b 19
:7 0 362 :2 0 360
b :3 0 34 :7 0
64f 64e :3 0 651
:2 0 65b 64c 652
:2 0 13 :3 0 34
:3 0 654 655 0
657 364 65a :3 0
65a 0 65a 659
657 658 :6 0 65b
1 0 64c 652
65a 68c :2 0 b4
:a 0 66c 1a :7 0
368 :2 0 366 f
:3 0 b5 :7 0 660
65f :3 0 662 :2 0
66c 65d 663 :2 0
11 :3 0 b5 :3 0
665 666 0 668
36a 66b :3 0 66b
0 66b 66a 668
669 :6 0 66c 1
0 65d 663 66b
68c :2 0 b6 :a 0
686 1b :8 0 66f
:2 0 686 66e 670
:2 0 370 :2 0 36e
b :3 0 62 :2 0
36c 673 675 :6 0
678 676 0 684
0 61 :6 0 61
:3 0 5f :3 0 b7
:4 0 67a 67c 679
67d 0 682 17
:4 0 67f 680 :2 0
682 372 685 :3 0
685 375 685 684
682 683 :6 0 686
1 0 66e 670
685 68c :3 0 68b
0 68b :3 0 68b
68c 689 68a :6 0
68d :2 0 377 0
3 68b 690 :3 0
68f 68d 691 :8 0
390
4
:3 0 1 19 1
16 1 1f 1
26 1 2e 1
2c 1 34 1
39 2 48 49
1 4d 2 4b
4d 1 52 2
54 55 1 57
1 59 2 44
5a 1 62 1
65 2 6c 6e
2 70 72 1
74 2 7c 7d
1 86 2 88
89 3 76 7f
8b 1 93 1
96 2 9b 9d
1 a2 2 a1
a2 2 a8 ab
1 ad 1 b2
3 a0 ae b4
1 bc 1 bf
2 c4 c6 1
cb 2 c8 cd
1 d2 2 d1
d2 1 dd 2
da df 2 d8
e2 1 e4 1
e9 3 d0 e5
eb 1 f4 1
f8 1 fc 1
101 4 f7 fb
100 105 1 10b
1 110 2 119
11a 1 11e 2
11c 11e 1 122
1 124 1 126
2 129 12b 1
132 1 138 1
13c 2 13b 13c
2 141 143 1
14f 2 14c 151
1 157 2 154
159 2 15b 15d
1 162 2 15f
164 2 166 168
1 16d 2 16a
16f 2 171 173
2 153 175 3
146 177 179 1
17b 1 185 2
182 187 1 18d
2 18a 18f 2
191 193 1 198
2 195 19a 2
19c 19e 2 189
1a0 6 127 12e
134 13a 17c 1a2
2 1a5 1a8 2
10e 113 1 1b1
1 1b4 1 1ba
1 1bf 1 1c4
1 1cb 1 1c9
1 1d0 1 1d5
1 1da 2 1eb
1ec 2 1f4 1f5
4 201 202 203
204 1 20b 2
209 20b 1 21e
2 21c 21e 1
224 1 22a 1
22c 2 227 22c
1 232 2 230
232 1 23a 2
238 23a 1 243
1 247 2 245
249 1 24b 2
250 252 1 254
1 258 3 226
24c 25b 1 260
2 25e 260 1
266 2 26c 26e
1 270 2 268
274 1 27b 2
278 27d 2 27f
281 1 283 1
285 3 287 276
288 2 21b 289
3 207 20e 28c
8 1e5 1ee 1f7
1fa 1fd 28f 296
299 7 1bd 1c2
1c7 1ce 1d3 1d8
1dd 1 2a2 1
2a5 1 2ad 1
2ab 1 2b4 1
2b2 1 2ba 1
2c0 2 2c3 2c5
1 2c8 2 2ca
2cb 2 2ce 2d4
1 2db 1 2e1
1 2e7 1 2ed
2 2f1 2f3 1
2f8 2 2f5 2fa
2 2fc 2fe 1
303 2 300 305
8 2cc 2d7 2dd
2e3 2e9 2ef 308
30b 2 2b0 2b8
1 314 1 318
1 31c 1 320
1 324 1 328
6 317 31b 31f
323 327 32b 1
333 1 331 1
33a 1 33f 1
343 1 349 1
34d 1 353 1
359 1 366 1
368 1 36a 1
378 1 37a 1
37c 1 38a 1
38c 1 38e c
33d 341 345 34b
34f 355 35b 36d
37f 391 394 397
1 336 1 3a0
1 3a4 1 3a8
1 3ac 1 3b0
5 3a3 3a7 3ab
3af 3b3 1 3bb
1 3b9 1 3c2
1 3c7 1 3cb
1 3cf 1 3d3
1 3d7 8 3c5
3c9 3cd 3d1 3d5
3d9 3dc 3df 1
3be 1 3e7 1
3eb 1 3ef 1
3f3 1 3f7 1
3fb 6 3ea 3ee
3f2 3f6 3fa 3fe
1 402 1 409
1 407 6 410
411 412 413 414
415 1 41b 3
418 41e 420 2
405 40c 1 429
1 42d 1 431
1 435 1 439
5 42c 430 434
438 43c 1 442
1 449 1 447
5 450 451 452
453 454 1 45a
3 457 45d 460
2 445 44c 1
468 1 46c 1
470 3 46b 46f
474 1 478 1
482 1 48c 1
496 1 4a0 1
4aa 1 4b6 1
4bb 1 4c0 1
4c7 1 4c5 1
4cc 1 4d8 1
4e6 1 4e4 1
50c 1 511 1
517 2 514 519
2 513 51b 1
51d 1 50e 1
520 1 538 1
53d 2 540 542
2 544 546 2
53f 548 1 54a
1 53a 1 54d
1 554 2 552
554 2 55d 55e
1 561 1 566
2 564 566 2
570 571 1 573
1 576 1 57b
2 57e 580 2
582 584 2 57d
586 1 588 3
58a 578 58b 1
58d 1 592 1
594 5 598 599
59a 59b 59c 1
5a2 1 5ae 1
5b5 1 5b7 1
5bb 1 5c2 1
5c8 1 5ce 2
5c4 5d1 1 5d3
2 5ba 5d4 1
5df 3 5dc 5dd
5e1 1 5ee 1
5f0 1 5f7 1
5fd 1 604 1
606 2 5f9 609
1 60b 1 60c
2 5e4 60f 2
611 612 1 613
8 4f1 523 550
58c 595 59f 5a5
616 d 480 48a
494 49e 4a8 4b4
4b9 4be 4c3 4ca
4d6 4e2 4e9 1
61e 1 622 1
626 1 62a 1
62e 1 632 6
621 625 629 62d
631 636 5 63c
63d 63e 63f 640
1 645 1 64d
1 650 1 656
1 65e 1 661
1 667 1 674
1 672 1 67b
2 67e 681 1
677 18 c 15
1d 24 2a 32
37 3c 5f 90
b9 f0 1ad 29e
310 39c 3e4 425
465 61b 64a 65b
66c 686
1
4
0
690
0
1
28
1b
68
0 1 1 1 1 1 6 1
8 9 1 1 c c c 1
1 1 1 13 13 13 16 1
1 1 1 0 0 0 0 0
0 0 0 0 0 0 0 0
1ba 8 0
110 6 0
10b 6 0
26 1 0
f3 1 6
64c 1 19
66e 1 1b
672 1b 0
4c5 13 0
482 13 0
447 12 0
407 11 0
3eb 11 0
3b9 10 0
331 c 0
318 c 0
2ab b 0
3fb 11 0
3f3 11 0
320 c 0
1b0 1 8
61 1 3
101 6 0
4bb 13 0
fc 6 0
3e6 1 11
4c0 13 0
467 1 13
428 1 12
1f 1 0
313 1 c
16 1 0
5 1 0
4a0 13 0
324 c 0
bc 5 0
93 4 0
328 c 0
4cc 13 0
39f 1 10
3ef 11 0
31c c 0
f8 6 0
4d8 13 0
3e 1 2
61d 1 18
3 0 1
622 18 0
42d 12 0
3a4 10 0
1c4 8 0
2c 1 0
62 3 0
bb 1 5
92 1 4
2a2 b 0
48c 13 0
4e4 13 0
1c9 8 0
1bf 8 0
34 1 0
65e 1a 0
1da 8 0
61e 18 0
429 12 0
3a0 10 0
2b2 b 0
1d5 8 0
478 13 0
3e7 11 0
314 c 0
5a6 16 0
380 f 0
36e e 0
35c d 0
20f a 0
5e5 17 0
470 13 0
2a1 1 b
4aa 13 0
3f7 11 0
65d 1 1a
62e 18 0
439 12 0
3b0 10 0
64d 19 0
62a 18 0
435 12 0
3ac 10 0
1b1 8 0
f4 6 0
632 18 0
626 18 0
496 13 0
431 12 0
3a8 10 0
46c 13 0
39 1 0
4b6 13 0
442 12 0
402 11 0
1d0 8 0
468 13 0
e 1 0
0



/

