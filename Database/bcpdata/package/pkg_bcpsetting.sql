create or replace package pkg_BCPSetting as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*--------------------------------------------------------------------------
   * InspectionSettingsObjectId() - PRIVATE
   * Gets the inspection settings objectId
   *------------------------------------------------------------------------*/

  function InspectionSettingsObjectId (
    a_ObjectTypeName               varchar2
  )return udt_Id;

  /*--------------------------------------------------------------------------
   * SystemSettingsObjectId() - PRIVATE
   *   Gets the system settings objectId
   *------------------------------------------------------------------------*/
  function SystemSettingsObjectId (
    a_ObjectTypeName            varchar2
  )return udt_Id;

  /*--------------------------------------------------------------------------
   * GetSetting() - PRIVATE
   * Gets a setting from the system settings object.
   *------------------------------------------------------------------------*/
  function GetSetting (
    a_SettingName                 varchar2
  )return varchar2;

end pkg_BCPSetting;



 
/

grant execute
on pkg_bcpsetting
to mhwyinterface;

grant execute
on pkg_bcpsetting
to posseextensions;

create or replace package body pkg_BCPSetting as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  gc_SettingsObjectId              udt_Id := SystemSettingsObjectId('o_SystemSettings');

  /*--------------------------------------------------------------------------
   * InspectionSettingsObjectId() - PRIVATE
   * Gets the inspection settings objectId
   *------------------------------------------------------------------------*/

  function InspectionSettingsObjectId (
    a_ObjectTypeName               varchar2
  )return udt_Id is
    t_ObjectId                     udt_Id;
  begin
    t_ObjectId := api.Pkg_SimpleSearch.ObjectByIndex(a_ObjectTypeName, 'InspectionSettingsNumber', 1);

    return t_ObjectId;
  end;

  /*--------------------------------------------------------------------------
   * SystemSettingsObjectId() - PRIVATE
   * Gets the system settings objectId
   *------------------------------------------------------------------------*/

  function SystemSettingsObjectId (
    a_ObjectTypeName               varchar2
  )return udt_Id is
    t_ObjectId                     udt_Id;
  begin
    t_ObjectId := api.Pkg_SimpleSearch.ObjectByIndex(a_ObjectTypeName, 'SystemSettingsNumber', 1);

    return t_ObjectId;
  end;

  /*--------------------------------------------------------------------------
   * GetSetting() - PRIVATE
   * Gets a setting from the system settings object.
   *------------------------------------------------------------------------*/
  function GetSetting (
    a_SettingName                 varchar2
  )return varchar2 is
  begin
    return api.pkg_Columnquery.Value(gc_SettingsObjectId, a_SettingName);
  end;

end pkg_BCPSetting;



/

