create or replace package pkg_Processes is

  -- Public type declarations
  subtype udt_Id is api.pkg_Definition.udt_Id;

  -- Function and procedure implementations

  ------------------------------------------------------------------------------
  -- CopyChecklistTemplateToChecklist
  --  Copies a checklist template from the process type to a new checklist on
  -- the process.
  ------------------------------------------------------------------------------
  procedure CopyTemplateToChecklist(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_ProcessTypeId                     udt_Id
  );

  ------------------------------------------------------------------------------
  -- CreateReminder
  --  Create a reminder process on the job for the specified date.  This code
  -- assumes the Reminder process is added to the job.
  ------------------------------------------------------------------------------
  procedure CreateReminder(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_JobId                             udt_Id,
    a_ReminderDate                      date,
    a_Note                              varchar2
  );

  ------------------------------------------------------------------------------
  -- VerifyChecklist
  --  Ensures that all checklist items that are mandatory for this outcome are
  -- checked.
  ------------------------------------------------------------------------------
  procedure VerifyChecklist(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_Outcome                           varchar
  );
  
  -----------------------------------------------------------------------------
  -- CreateAndRelateNewNOVtoProcess()
  --  Create a new NOV and relate it to the Perform Investigation process.  
  --  This will also grab all violations not previously included on an NOV 
  --  and relate those to the new NOV
  -----------------------------------------------------------------------------
   procedure CreateAndRelateNewNOVtoProcess(
    a_ProcessId               udt_id,
    a_AsOfDate                date               
  );

end pkg_Processes;

 
/

create or replace package body pkg_Processes is

  ------------------------------------------------------------------------------
  -- CopyTemplateToChecklist
  --  Copies a checklist template from the process type to a new checklist on
  -- the process.
  ------------------------------------------------------------------------------
  procedure CopyTemplateToChecklist(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_ProcessTypeId                     udt_Id
  ) is
    t_EndpointId                        udt_Id;
    t_ObjectDefId                       udt_Id;
    t_ObjectId                          udt_Id;
    t_JobId                             udt_Id;
    t_JobTypeObjectId                   udt_Id;
    t_JTPTObjectId                      udt_Id;
    t_PermitTypeObjectId                udt_Id;
    t_RelId                             udt_Id;
    t_Include                             boolean;

    cursor c_ChecklistTemplate is
      select o.ObjectId, o.SortOrder, o.Name, o.Description, o.Mandatory, o.AllOrSomePermitTypes
        from query.r_JTPT_ChecklistTemplate r
          join query.o_ABC_ChecklistTemplate o
            on r.ChecklistTemplateObjectId = o.ObjectId
        where r.JTPTObjectId = t_JTPTObjectId
          and o.Active = 'Y';

  begin
  /*pkg_debug.enable;
  pkg_debug.setpipename('craig');
  pkg_debug.suspend;
  pkg_debug.putsingleline('***STARTING***');
  pkg_debug.putsingleline('Process Id: ' || a_ProcessId);
  pkg_debug.putsingleline('Process TypeId (input): ' || a_ProcessTypeId);*/

  
    if nvl(api.pkg_ColumnQuery.Value(a_ProcessId, 'IsSystemProcess'), 'N') = 'N' then
      t_EndpointId := api.pkg_ConfigQuery.EndPointIdForName(a_ProcessTypeId, 'Checklist');
      t_ObjectDefId := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_Checklist'); 
      t_JobId := api.pkg_columnquery.NumericValue(a_ProcessId, 'JobId');
 -- pkg_debug.putsingleline('JobId: ' || t_JobId);
   /*   Select jt.JobTypeId into t_JobTypeObjectId 
      from api.Jobs j join api.JobTypes jt on jt.JobTypeId = j.JobTypeId 
      where j.JobId = t_JobId;*/
      
      --t_JobTypeObjectId := api.pkg_columnquery.NumericValue(t_JobId, 'JobTypeId');
 -- pkg_debug.putsingleline('Job TypeId: ' || t_JobTypeObjectId);
      
      --Get the Job Type Process Type ObjectId
     -- Select ObjectId into t_JTPTObjectId from query.o_JobTypeProcessTypes where JobTypeId = t_JobTypeObjectId and ProcessTypeId = a_ProcessTypeId;
      t_PermitTypeObjectId := api.pkg_columnquery.NumericValue(api.pkg_columnquery.NumericValue(a_ProcessId, 'JobId'), 'PermitTypeObjectId');
  --pkg_debug.putsingleline('PermitType Id: ' || t_PermitTypeObjectId);
  
      for c in c_ChecklistTemplate loop
        t_Include := false;
        for x in (select rr.PermitTypeObjectId from query.r_PermitTypeChecklistTemplate rr where rr.ChecklistTemplateObjectId = c.Objectid) loop
         pkg_debug.putsingleline('PermitType Id From DB: ' || x.PermitTypeObjectId); 
          if x.PermitTypeObjectId = t_PermitTypeObjectId then
            t_Include := true;
          end if;
        end loop;
        if c.AllOrSomePermitTypes = 'All' or t_Include then
          t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'SortOrder', c.SortOrder);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Name', c.Name);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Description', c.Description);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Mandatory', c.Mandatory);
          t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, a_ProcessId, t_ObjectId);
        end if;
      end loop;
    end if;
  end CopyTemplateToChecklist;


  ------------------------------------------------------------------------------
  -- VerifyChecklist
  --  Ensures that all checklist items that are mandatory for this outcome are
  -- checked.
  ------------------------------------------------------------------------------
  procedure VerifyChecklist(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_Outcome                           varchar
  ) is
    t_Count                             number;
    t_EndpointId                        udt_Id;
    t_ProcessOutcomeObjectId            udt_Id;
    t_ProcessTypeId                     udt_Id;

  begin
    if a_Outcome is not null then
      select p.ProcessTypeId, api.pkg_SimpleSearch.ObjectByIndex('o_ProcessOutcome', 'ProcessOutcomeId', po.ProcessOutcomeId)
        into t_ProcessTypeId, t_ProcessOutcomeObjectId
        from api.processes p
          join api.ProcessOutcomes po
            on p.JobTypeProcessTypeId = po.JobTypeProcessTypeId
              and po.Outcome = a_Outcome
        where p.ProcessId = a_ProcessId;

      t_EndpointId := api.pkg_ConfigQuery.EndPointIdForName(t_ProcessTypeId, 'Checklist');

      for x in (select r.ToObjectId from api.relationships r
          where r.FromObjectId = a_ProcessId
            and r.EndPointId = t_EndpointId) loop
        if api.pkg_columnquery.Value(x.ToObjectId, 'Completed') = 'Y' then
          api.pkg_columnupdate.SetValue(x.ToObjectId, 'CompletedDate', sysdate);
        else
          api.pkg_columnupdate.RemoveValue(x.ToObjectId, 'CompletedDate');
        end if;
      end loop;

      if api.pkg_ColumnQuery.Value(t_ProcessOutcomeObjectId, 'MandatoryChecklistsApplicable') = 'Y' then
        select count(*)
          into t_Count
          from api.relationships r
          where r.FromObjectId = a_ProcessId
            and r.EndPointId = t_EndpointId
            and exists (
              select 1
                from query.o_ABC_Checklist o
                where r.ToObjectId = o.ObjectId
                  and o.Mandatory = 'Y'
                  and o.CompletedDate is null);

        if t_Count > 0 then
          api.pkg_Errors.RaiseError(-20000, 'Before you go on, please ensure that all mandatory checklist items are completed.');
        end if;
      end if;


      select count(*)
        into t_Count
        from api.relationships r
        where r.FromObjectId = a_ProcessId
          and r.EndPointId = t_EndpointId
          and exists (
            select 1
              from query.o_ABC_Checklist o
              where r.ToObjectId = o.ObjectId
                and trunc(o.CompletedDate) > sysdate);

      if t_Count > 0 then
        api.pkg_Errors.RaiseError(-20000, 'Before you go on, please ensure that checklist items do not have a Completed Date in the future.');
      end if;
    end if;
  end VerifyChecklist;


  ------------------------------------------------------------------------------
  -- CreateReminder
  --  Create a reminder process on the job for the specified date.  This code
  -- assumes the Reminder process is added to the job.
  ------------------------------------------------------------------------------
  procedure CreateReminder(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_JobId                             udt_Id,
    a_ReminderDate                      date,
    a_Note                              varchar2
  ) is
    t_ProcessId                         udt_Id;
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('p_ABC_Reminder');

  begin
    t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, null, a_ReminderDate, to_date(null), to_date(null));
    api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'Note', a_Note);
    api.pkg_ProcessUpdate.Assign(t_ProcessId, api.pkg_SecurityQuery.EffectiveUserId());
  end CreateReminder;
  
  -----------------------------------------------------------------------------
  -- CreateAndRelateNewNOVtoProcess()
  --  Create a new NOV and relate it to the Perform Investigation process.  
  --  This will also grab all violations not previously included on an NOV 
  --  and relate those to the new NOV
  -----------------------------------------------------------------------------
  procedure CreateAndRelateNewNOVtoProcess(
    a_ProcessId               udt_id,
    a_AsOfDate                date
  ) is 
    t_NOVObjectDefId          udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('o_LMS_NoticeOfViolation');
    t_NewNOVObjectId          udt_Id;
    t_NOVPIEndpointId         udt_Id := api.pkg_ConfigQuery.EndPointIdForName('p_LMS_PerformInvestigation', 'NoticeOfViolation');
    t_NOVPIRelId              udt_Id; 
    t_ViolNOVEndpointId       udt_Id := api.pkg_configquery.EndPointIdForName('o_LMS_NoticeOfViolation', 'Violations');
    t_ViolNOVRelId            udt_Id;
  
    
    cursor c_ViolationsToAdd is
      select rNP.NoticeOfViolationObjectId, o.ObjectId
        from query.r_LMS_PerfInvestNoticeViol rNP
        join query.r_ViolationsPerfInv rPV
          on rPV.PerformInvestigationId = rNP.PerformInvestigationProcessId
        join query.o_violations o
          on o.ObjectId = rPV.Violationsid
       where rNP.RelationshipId = t_NOVPIRelId
         and o.ResolutionDate is null
         and o.NoticeDate is null;
          
  begin
   -- raise_application_error(-20000, 'Got In! NOV object def id: ' || t_NOVObjectDefId || ' NOVPIEndPointId: ' || t_NOVPIEndpointId || ' ');
    if (api.Pkg_Columnquery.Value(a_ProcessId, 'NOVCreated') = 'Y') then
      --create a new Notice of Violation object and relate it to the current job
      t_NewNOVObjectId := api.pkg_ObjectUpdate.New(t_NOVObjectDefId);
      t_NOVPIRelId := api.pkg_RelationshipUpdate.New(t_NOVPIEndpointId, a_ProcessId, t_NewNOVObjectId);
      
      --create a rel from the violations to the new Notice of Violation
      for v in c_ViolationsToAdd loop
        t_ViolNovRelId := api.pkg_relationshipUpdate.New(t_ViolNOVEndpointId, t_NewNOVObjectId, v.objectid);
      end loop;
    
      api.pkg_columnupdate.SetValue(a_ProcessId, 'NOVCreated', 'N');
    end if;
  end CreateAndRelateNewNOVtoProcess;

end pkg_Processes;

/

