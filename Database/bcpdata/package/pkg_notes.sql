create or replace package pkg_Notes is

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;


/*---------------------------------------------------------------------------
 * NewNote() -- PUBLIC
 *-------------------------------------------------------------------------*/
  procedure NewNote (a_ObjectId  udt_Id,
                     a_AsOfDate  date,
                     a_Text      varchar2,
                     a_NoteDefId udt_Id,
                     a_Lock      varchar2
  );
  
/*---------------------------------------------------------------------------
 *ModifyNote() change note text from an outrider presentation
 *-------------------------------------------------------------------------*/
  procedure ModifyNote (a_ObjectId udt_Id,
                        a_AsOfDate date,
                        a_Text     long,
                        a_Lock     varchar2
  );  

end  pkg_Notes;

 
/

create or replace package body pkg_Notes is

/*---------------------------------------------------------------------------
 *NewNote() create a note from an outrider presentation
 *-------------------------------------------------------------------------*/
  procedure NewNote (a_ObjectId  udt_Id,
                     a_AsOfDate  date,
                     a_Text      varchar2,
                     a_NoteDefId udt_Id,
                     a_Lock      varchar2) is
  
  t_NoteId integer;
  
  begin                       
    
    
      t_NoteId := api.pkg_noteupdate.New(a_ObjectId, a_NoteDefId, a_Lock, a_Text);   
                 
                         
  end NewNote;
  
/*---------------------------------------------------------------------------
 *ModifyNote() change note text from an outrider presentation
 *-------------------------------------------------------------------------*/
  procedure ModifyNote (a_ObjectId udt_Id,
                        a_AsOfDate date,
                        a_Text     long,
                        a_Lock     varchar2) is
  
  t_NoteId integer;
  t_NoteText long;
  
  begin                       
  
    --Find the Note ID attached to the object
    select n.noteId
      into t_NoteId
    from api.notes n
      where n.ObjectId = a_ObjectId;
    
    --Find the current Note text attached to the note
    select n.Text
      into  t_NoteText
    from api.notes n 
      where n.NoteId = t_NoteId;      
      
    --Append the Clause to the Note if text is to large throw an error    
    t_NoteText := t_NoteText || a_Text;       
  
    
      api.pkg_noteupdate.Modify(t_NoteId,a_Lock,t_NoteText);  
                  
                         
  end ModifyNote; 
  
/*---------------------------------------------------------------------------
 *AttachClause() attach a clause to a note from an outrider presentation
 *-------------------------------------------------------------------------*/
  procedure AttachClause (a_ObjectId  udt_Id,
                          a_Text      long,
                          a_NoteDefId udt_Id,
                          a_ClauseId  udt_Id,
                          a_Lock      varchar2) is
  
  t_NoteId       integer;
  t_NoteText     long;
  t_ClauseText   long;
  
  begin                       
    
    --Find the Note ID attached to the object
    select n.noteId
      into t_NoteId
    from api.notes n
      where n.ObjectId = a_ObjectId;
    
    --Find the current Note text attached to the note
    select n.Text
      into  t_NoteText
    from api.notes n 
      where n.NoteId = t_NoteId;
  
    --Find the clause text to add to the note
    select c.Text
      into  t_ClauseText
    from api.clauses c
      where c.NoteDefId = a_NoteDefId
        and c.ClauseId = a_ClauseId; 
    
    --Append the Clause to the Note if text is to large throw an error    
    t_NoteText := t_NoteText || a_Text || t_ClauseText;     
    
      api.pkg_noteupdate.Modify(t_NoteId,a_Lock,t_NoteText);               
                         
  end AttachClause;   

end pkg_Notes;

/

