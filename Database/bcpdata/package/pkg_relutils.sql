create or replace package pkg_RelUtils is

  -----------------------------------------------------------------------------
  -- Types
  -----------------------------------------------------------------------------
  subtype udt_Id is api.pkg_Definition.udt_Id;

  -----------------------------------------------------------------------------
  -- AssignedStaffNames()
  -----------------------------------------------------------------------------
  function AssignedStaffNames (
    a_ProcessId				udt_Id
  ) return varchar2;

  -----------------------------------------------------------------------------
  -- EndPoint()
  --   Gets the endpoint for the objectdef and name and caches the id in
  --    package state.
  -----------------------------------------------------------------------------
  function EndPoint (
    a_ObjectDefName                  varchar2,
    a_Name                           varchar2,
    a_Optional                       boolean default false
  ) return udt_Id;

  -----------------------------------------------------------------------------
  -- RelateCurrentUser()
  -----------------------------------------------------------------------------
  procedure RelateCurrentUser (
    a_ObjectId				udt_Id,
    a_AsofDate				date,
    a_CurrentUser			varchar2
  );

  -----------------------------------------------------------------------------
  -- RelExists()
  -----------------------------------------------------------------------------
  function RelExists (
    a_ObjectId				pls_integer,
    a_EndPointName			varchar2
  ) return varchar2;

  -----------------------------------------------------------------------------
  -- RemoveRels()
  -----------------------------------------------------------------------------
  procedure RemoveRels (
    a_ObjectId				pls_integer,
    a_AsOfDate				date,
    a_ObjectDefName			varchar2,
    a_EndPointNames			varchar2
  );

  -----------------------------------------------------------------------------
  -- VerifyRelPairs()
  -----------------------------------------------------------------------------
/*  procedure VerifyRelPairs (
    a_ObjectId				pls_integer,
    a_AsOfDate				date,
    a_JobColumnName			varchar2
  );
*/
end pkg_RelUtils;


 
/

create or replace package body pkg_RelUtils is

  -----------------------------------------------------------------------------
  -- Types
  -----------------------------------------------------------------------------
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  type udt_IdByName is table of udt_Id index by varchar2(70);
  g_IdsByName                        udt_IdByName;

  -----------------------------------------------------------------------------
  -- AssignedStaffNames() -- PUBLIC
  --   Return a comma and space separated list of staff assigned to the process
  -----------------------------------------------------------------------------
  function AssignedStaffNames (
    a_ProcessId				udt_Id
  ) return varchar2 is
    t_StaffList				varchar2(4000);

    cursor c_Assignments is
    select u.Name
      from api.Users u
      join api.ProcessAssignments pa
        on pa.UserId = u.UserId
     where pa.ProcessId = a_ProcessId
    order by u.Name;
  begin
    for a in c_Assignments loop
      extension.pkg_Utils.AddSection(t_StaffList, a.Name, ', ');
    end loop;

    return t_StaffList;
  end AssignedStaffNames;

  -----------------------------------------------------------------------------
  -- EndPoint()
  -----------------------------------------------------------------------------
  function EndPoint (
    a_ObjectDefName                  varchar2,
    a_Name                           varchar2,
    a_Optional                       boolean default false
  ) return udt_Id is
    t_Name                           varchar2(70);
    t_EndPoint                       udt_Id;
  begin
    pkg_debug.putline('Determining EndPoint '||a_Name||' for '||a_ObjectDefName);
    t_Name := upper(a_ObjectDefName) || ':' || upper(a_Name);
    if g_IdsByName.exists(t_Name) then
      t_EndPoint := g_IdsByName(t_Name);
    else
      t_EndPoint := api.pkg_ConfigQuery.EndPointIdForName(a_ObjectDefName,
          a_Name);
      g_IdsByName(t_Name) := t_EndPoint;
    end if;
    if t_EndPoint is null and not a_Optional then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('ObjectDefName', a_ObjectDefName);
      api.pkg_Errors.SetArgValue('EndPointName', a_Name);
      api.pkg_Errors.RaiseError(-20100,
          '{ObjectDefName} has no endpoint named {EndPointName}.');
    end if;
    return t_EndPoint;
  end;

  -----------------------------------------------------------------------------
  -- RelateCurrentUser()
  -----------------------------------------------------------------------------
  procedure RelateCurrentUser (
    a_ObjectId				udt_Id,
    a_AsofDate				date,
    a_CurrentUser			varchar2
  ) is
    t_CurrentUserObjectId		udt_Id;
    t_Dummy				udt_Id;

    cursor existinguserrels is
      select r.relationshipid
        from
          api.relationships r
          join api.relationshipdefs rd
              on r.relationshipdefid = rd.relationshipdefid
        where r.fromobjectid = a_ObjectId
          and r.toobjectid = t_CurrentUserObjectId
          and rd.ToEndPointName = 'User';

  begin
    -- Find the current user
    select ObjectId
      into t_CurrentUserObjectId
      from query.u_Users u
      where u.OracleLogonId = a_CurrentUser;

    -- remove any existing rels if they exist
    for i in existinguserrels loop
      api.pkg_relationshipupdate.Remove(i.relationshipid,sysdate);
    end loop;

    -- relate the current user to the object
    t_Dummy := extension.pkg_RelationshipUpdate.New(a_ObjectId, t_CurrentUserObjectId,
        'User');

  end RelateCurrentUser;

  -----------------------------------------------------------------------------
  -- RelExists() -- PRIVATE
  -----------------------------------------------------------------------------
  function RelExists (
    a_ObjectId				pls_integer,
    a_ObjectDefId			pls_integer,
    a_EndPointName			varchar2
  ) return boolean is
    t_EndPointId			pls_integer;
    t_Rels				udt_IdList;

  begin
    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(a_ObjectDefId,
        a_EndPointName);

    if t_EndPointId is null then
      api.pkg_Errors.SetArgValue('EPName', a_EndPointName);
      api.pkg_Errors.SetArgValue('ObjDef', a_objectDefId);
      api.pkg_Errors.RaiseError(-20000,
          'Invalid end point name "{EPName}" for object def: {ObjDef}.');
    end if;

    t_Rels := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId, t_EndPointId);

    return t_Rels.count > 0;
  end RelExists;

  -----------------------------------------------------------------------------
  -- RelExists()
  -----------------------------------------------------------------------------
  function RelExists (
    a_ObjectId				pls_integer,
    a_EndPointName			varchar2
  ) return varchar2 is
  begin
    if RelExists(a_ObjectId, extension.pkg_ObjectQuery.ObjectDefId(a_ObjectId),
        a_EndPointName) then
      return 'Y';
    else
      return 'N';
    end if;
  end RelExists;

  -----------------------------------------------------------------------------
  -- RemoveRels()
  --   Remove all relationships for the given object def name and the list of
  -- EndPoint names specified.
  -----------------------------------------------------------------------------
  procedure RemoveRels (
    a_ObjectId				pls_integer,
    a_AsOfDate				date,
    a_ObjectDefName			varchar2,
    a_EndPointNames			varchar2
  ) is
    t_ObjectId				pls_integer;
    t_ObjectDefId			pls_integer;
    t_EndPoints				udt_StringList;
    t_EPIx				pls_integer;
    t_Rels				udt_IdList;
    t_RelIx				pls_integer;
  begin
    begin
      select
        rel.FromObjectId,
        obj.ObjectDefId
      into
        t_ObjectId,
        t_ObjectDefId
      from
        api.Relationships rel
        join api.Objects obj
            on obj.ObjectId = rel.FromObjectId
        join api.ObjectDefs od
            on od.ObjectDefId = obj.ObjectDefId
            and od.Name = a_ObjectDefName
      where rel.RelationshipId = a_ObjectId;
    exception
    when no_data_found then
      api.pkg_Errors.SetArgValue('RelId', a_ObjectId);
      api.pkg_Errors.SetArgValue('DefName', a_ObjectDefName);
      api.pkg_Errors.RaiseError(-20000,
          'Invalid relationship id: {RelId} and object def name "{DefName}" pair.');
    end;

    t_EndPoints := extension.pkg_Utils.Split(a_EndPointNames, ',');

    for t_EPIx in 1..t_EndPoints.count loop
      select RelationshipId
        bulk collect into t_Rels
        from api.Relationships
        where FromObjectId = t_ObjectId
          and EndPointId = api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
              t_EndPoints(t_EPIx));

      for t_RelIx in 1..t_Rels.count loop
        api.pkg_RelationshipUpdate.Remove(t_Rels(t_RelIx));
      end loop;
    end loop;
  end RemoveRels;

  -----------------------------------------------------------------------------
  -- VerifyRelPairs()
  -----------------------------------------------------------------------------
/*  procedure VerifyRelPairs (
    a_ObjectId				pls_integer,
    a_AsOfDate				date,
    a_JobColumnName			varchar2
  ) is
    t_JobId				pls_integer;
    t_ObjectDefId			pls_integer;
    t_EndPoints				udt_StringList;
    t_Index				pls_integer;
    t_MissingRels			varchar2(4000);
  begin
    -- a job or process id may be passed in
    t_JobId := extension.pkg_Common.JobIdForProcess(a_ObjectId);
    if t_JobId is null then
      t_JobId := a_ObjectId;
    end if;

    t_ObjectDefId := extension.pkg_ObjectQuery.ObjectDefId(t_JobId);
    t_EndPoints := extension.pkg_Utils.Split(
        api.pkg_ColumnQuery.Value(t_JobId, a_JobColumnName), ',');

    -- should have even number of end points in the list
    if mod(t_EndPoints.count, 2) <> 0 then
      api.pkg_Errors.RaiseError(-20000,
          'The client end point list does not have an even number of items.');
    end if;

    t_Index := 1;
    while t_Index < t_EndPoints.count loop
      if RelExists(t_JobId, t_ObjectDefId, t_EndPoints(t_Index)) then
        if not RelExists(t_JobId, t_ObjectDefId, t_EndPoints(t_Index + 1)) then
          extension.pkg_Utils.AddSection(t_MissingRels, t_EndPoints(t_Index + 1), ', ');
        end if;
      end if;
      -- go to the next pair
      t_Index := t_Index + 2;
    end loop;

    if length(t_MissingRels) > 0 then
      api.pkg_Errors.RaiseError(-20000, t_MissingRels || ' client(s) not created.');
    end if;
  end VerifyRelPairs;*/

end pkg_RelUtils;


/

