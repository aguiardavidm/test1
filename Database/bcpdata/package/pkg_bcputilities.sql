create or replace package         pkg_BCPUtilities is

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * RelateUtilityConnects() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RelateUtilityConnects (
    a_JobId             udt_Id,
    a_AsOfDate          date,
    a_ProcessViewName   varchar2,
    a_ProcessOutcome    varchar2
  );

  /*---------------------------------------------------------------------------
   * SetExpirationDate() -- PUBLIC
   *-------------------------------------------------------------------------*/

  procedure SetExpirationDate (
    a_ProcessId             udt_Id,
    a_AsOfDate          date,
    a_DateColumnName    varchar2,
    a_NumberOfMonths    number
  );

  /*---------------------------------------------------------------------------
   * CreateUtilityConnects() -- PUBLIC
   *-------------------------------------------------------------------------*/

  procedure CreateUtilityConnects (
    a_ProcessId             udt_Id,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * SetDocumentExternalFileNum() -- PUBLIC
   *-------------------------------------------------------------------------*/

  procedure SetDocumentExternalFileNum (
    a_DocumentId            udt_Id,
    a_AsOfDate              date
  );

  /*---------------------------------------------------------------------
  * DeleteRelatedObjects() -- PUBLIC
  * This procedure deletes all related Objects for a given ObjectDefName
  *-------------------------------------------------------------------*/
  procedure DeleteRelatedObjects(a_ObjectId         number,
                                 a_AsOfDate         date,
                                 a_EndPointName     varchar2);

  /*---------------------------------------------------------------------
  * DeleteRelsForEndPointNames() -- PUBLIC
  * This procedure deletes all related Objects for a list of 
  * comma-seperated End Point Names
  *-------------------------------------------------------------------*/
  procedure DeleteRelsForEndPointNames(a_ObjectId         number,
                                       a_AsOfDate         date,
                                       a_EndPointNames   varchar2);

  /*------------------------------------------------------------------
  * SetNullTimeValue()
  ------------------------------------------------------------------*/
  procedure MaintainTimeLogValues(a_ObjectId   udt_Id,
                                  a_AsOfDate   date,
                                  a_Hours      number,
                                  a_Minutes    number);

end pkg_BCPUtilities;

 

/

create or replace package body         pkg_BCPUtilities is

  /*---------------------------------------------------------------------------
   * RelateUtilityConnects() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RelateUtilityConnects (
    a_JobId             udt_Id,
    a_AsOfDate          date,
    a_ProcessViewName   varchar2,
    a_ProcessOutcome    varchar2
  ) is
    t_ProcessTypeId     udt_Id;
    t_RelId             udt_Id;
    procedure SetErrorArguments is
      begin
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('JobId', a_JobId);
        api.pkg_Errors.SetArgValue('As Of Date', a_AsOfDate);
        api.pkg_Errors.SetArgValue('Process View Name', a_ProcessViewName);
        api.pkg_Errors.SetArgValue('Process Outcome', a_ProcessOutcome);
      end;
  begin
    t_ProcessTypeId := toolbox.pkg_Util.GetIdForName(a_ProcessViewName);

    for c in (select pr.ProcessId
                from api.Processes pr
                join api.jobs j on j.JobId = pr.JobId
               where pr.ProcessTypeId = t_ProcessTypeId
                 and pr.Outcome is null
                 and j.JobStatus <> 'READY') loop
      begin
        t_RelId := pkg_Utils.RelationshipNew(a_JobId, c.ProcessId);
      exception when others then
        SetErrorArguments;
        api.pkg_Errors.RaiseError(-20000, 'Unable to relate job ' || a_JobId || ' and process ' || c.ProcessId);
      end;

      begin
        api.pkg_ProcessUpdate.Complete(c.ProcessId, a_ProcessOutcome);
      exception when others then
        SetErrorArguments;
        api.pkg_Errors.RaiseError(-20000, 'Unable to complete process ' || c.ProcessId || ' with outcome of ' ||  a_ProcessOutcome);
      end;

    end loop;
  end;

  /*---------------------------------------------------------------------------
   * SetExpirationDate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SetExpirationDate (
    a_ProcessId         udt_Id,
    a_AsOfDate          date,
    a_DateColumnName    varchar2,
    a_NumberOfMonths    number
  ) is
    t_ExpirationDate    date default sysdate;
    t_StartDate         date default sysdate;
    t_JobId             udt_Id;
  begin
    select PRO.JobId
      into t_JobId
      from api.processes PRO
     where PRO.ProcessId = a_ProcessId;

    if a_DateColumnName is not null then
      t_StartDate := api.pkg_columnQuery.dateValue(t_JobId, a_DateColumnName);
    end if;
    t_ExpirationDate := add_Months(t_StartDate, a_NumberOfMonths);

    api.pkg_columnupdate.setValue(t_JobId, 'ExpirationDate', t_ExpirationDate);
  end;

  /*---------------------------------------------------------------------------
   * CreateUtilityConnects() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure CreateUtilityConnects (
    a_ProcessId             udt_Id,
    a_AsOfDate              date
  ) is
    t_jobId                 udt_Id;
    t_processesExist        int;
    t_processId             udt_Id;
    t_GasProcessTypeId      udt_Id;
    t_PowerProcessTypeId    udt_Id;
  begin
    t_GasProcessTypeId := toolbox.pkg_Util.GetIdForName('p_RequestGasConnect');
    t_PowerProcessTypeId := toolbox.pkg_Util.GetIdForName('p_RequestPowerConnect');

    select jobId
      into t_jobId
      from api.processes
     where processId = a_ProcessId;

    if (api.pkg_columnQuery.value(t_jobId, 'NewGasService') = 'Y') then
      select count(*)
        into t_processesExist
        from query.p_RequestGasConnect RGC
       where RGC.JobId = t_JobId
         and RGC.TemporaryPermanent = 'Permanent';
      if (t_processesExist = 0) then
        t_processId := api.pkg_processupdate.new(t_JobId, t_GasProcessTypeId, null, null, null, null);
        api.pkg_columnUpdate.setvalue(t_processId, 'TemporaryPermanent', 'Permanent');
      end if;
    end if;

    if (api.pkg_columnQuery.value(t_jobId, 'NewPowerService') = 'Y') then
      select count(*)
        into t_processesExist
        from query.p_RequestPowerConnect RGC
       where RGC.JobId = t_JobId
         and RGC.TemporaryPermanent = 'Permanent';
      if (t_processesExist = 0) then
        t_processId := api.pkg_processupdate.new(t_JobId, t_PowerProcessTypeId, null, null, null, null);
        api.pkg_columnUpdate.setvalue(t_processId, 'TemporaryPermanent', 'Permanent');
      end if;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * SetDocumentExternalFileNum() -- PUBLIC
   *-------------------------------------------------------------------------*/

  procedure SetDocumentExternalFileNum (
    a_DocumentId            udt_Id,
    a_AsOfDate              date
  ) is
    t_NextValue             int;
  begin
    select bcpdata.ElectronicDocumentNumber.nextVal
      into t_NextValue
      from dual;

    api.pkg_objectupdate.setExternalFileNum(a_DocumentId, lpad(t_NextValue, 8, '0'));
  end;

  /*---------------------------------------------------------------------
  * DeleteRelatedObjects() -- PUBLIC
  * This procedure deletes all related Objects for a given ObjectDefName
  *-------------------------------------------------------------------*/
  procedure DeleteRelatedObjects(a_ObjectId         number,
                                 a_AsOfDate         date,
                                 a_EndPointName    varchar2) is
    
	t_FromObjectDefName   varchar2(4000);
  t_EndPointId          number;
  
  begin 

    select od.Name
      into t_FromObjectDefName
      from api.objects o
      join api.objectdefs od on od.objectdefid = o.objectdefid
     where o.objectid = a_ObjectId;

    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_FromObjectDefName, a_EndPointName);

        for c in (select r.relationshipid
                    from api.relationships r
                    join api.endpoints e on e.endpointid = r.endpointid
                   where r.FromObjectId = a_ObjectId
                     and e.EndPointId = t_EndPointId)loop

             api.pkg_relationshipupdate.remove(c.relationshipid, null);

     end loop;

  end DeleteRelatedObjects;

  /*---------------------------------------------------------------------
  * DeleteRelsForEndPointNames() -- PUBLIC
  * This procedure deletes all related Objects for a list of 
  * comma-seperated End Point Names
  *-------------------------------------------------------------------*/
  procedure DeleteRelsForEndPointNames(a_ObjectId         number,
                                       a_AsOfDate         date,
                                       a_EndPointNames   varchar2) is
    
	t_FromObjectDefName   varchar2(4000);
  t_EndPointId          number;
  t_EndPointNames       api.pkg_Definition.udt_StringList;

  begin 

    select od.Name
      into t_FromObjectDefName
      from api.objects o
      join api.objectdefs od on od.objectdefid = o.objectdefid
     where o.objectid = a_ObjectId;

    t_EndPointNames := extension.pkg_utils.split(replace(a_EndPointNames, ' ', null), ',');

    for a in 1 .. t_EndPointNames.Count loop

      t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_FromObjectDefName, t_EndPointNames(a));

        for c in (select r.relationshipid
                    from api.relationships r
                    join api.endpoints e on e.endpointid = r.endpointid
                   where r.FromObjectId = a_ObjectId
                     and e.EndPointId = t_EndPointId)loop

             api.pkg_relationshipupdate.remove(c.relationshipid, null);

        end loop; --c
    end loop; --a

  end DeleteRelsForEndPointNames;

  /*------------------------------------------------------------------
  * SetNullTimeValue()
  ------------------------------------------------------------------*/
  procedure MaintainTimeLogValues(a_ObjectId   udt_Id,
                                  a_AsOfDate   date,
                                  a_Hours      number,
                                  a_Minutes    number) is
  begin
    if a_Hours is null then
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'Hours', 0);
    end if;
    if a_Minutes is null then
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'Minutes', 0);
    end if;
    if a_Minutes = 60 then
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'Minutes', 0);
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'Hours', nvl(a_Hours,0)+1);
    end if;
  end MaintainTimeLogValues;

end pkg_BCPUtilities;

/

