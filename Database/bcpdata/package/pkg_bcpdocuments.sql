create or replace package pkg_BCPDocuments  is

  /*--------------------------------------------------------------------------
   * CopyDocumentsToReview - PUBLIC
   *------------------------------------------------------------------------*/
  procedure CopyDocumentsToReview(
    a_ObjectIdId                  api.pkg_definition.udt_Id,
    a_AsOfDate                    date
  );

end pkg_BCPDocuments;

 
/

create or replace package body pkg_BCPDocuments is

  /*--------------------------------------------------------------------------
   * CopyDocumentsToReview - PUBLIC
   *------------------------------------------------------------------------*/
    procedure CopyDocumentsToReview(
      a_ObjectIdId                  api.pkg_definition.udt_Id,
      a_AsOfDate                    date
    ) is
    
    begin
      null;
    end;


end pkg_BCPDocuments;

/

