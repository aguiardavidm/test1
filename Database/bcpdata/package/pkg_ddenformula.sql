create or replace package pkg_ddenFormula as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   * ALBERT - this needs to get into a UTIL type package, 
   *  although I question the usage when we've got sql that can use
   *  case structures or nvl2 or translate.
   * note that this package is only referenced by a global expression
   * which in turn has no references by deputy.
   *------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------
   * IsValueInList()
   *   Return 'Y' if a_Value is in a_List, where a_List is a comma separated
   *   list of values.
   *
   * Usage: Formula--SQL:
   *   :RetVal := sitespecific.pkg_ddenFormula.IsValueInList(:List, :Value);
   *
   *------------------------------------------------------------------------*/
  function IsValueInList (
    a_List                      varchar2,
    a_Value                     varchar2
  ) return varchar2;

  /*--------------------------------------------------------------------------
   * FormatDisplayString()
   *   Basically accepts a string just like the "Display Format" and returns
   *   the same result as the Display Format would, Plus the following useful
   *   additions:
   *
   * Special handling of [ {} ]
   *   [string {column} string]|[otherstring]:
   *     If value of column is not null: Returns string value string
   *     If value of column is null: Returns otherstring
   *     eg Status[ - Rank: {RankNumber}] -- {Status}
   *       Returns Status - Rank: 1 -- Complete
   *       or
   *       Returns Status -- Complete
   *     eg Status[ - Rank: {RankNumber}]|[ - No Rank] -- {Status}
   *       Returns Status - Rank: 1 -- Complete
   *       or
   *       Returns Status - No Rank -- Complete
   *   *Note - if [ ] does not contain a {}, it is returned exactly as is
   *     eg Status [Rank] -- {Status}
   *       Returns Status [Rank] -- Complete
   *
   * Special handling of ~
   *   ~n - turns into line feed
   *   ~r - turns into carriage return
   *   ~t - turns into tab
   *   ~~ - turns into ~
   *   ~{ - turns into {
   *   ~[ - turns into [
   *   *Note - Status [ string ~[{column}~] string]|[otherstring ~[{column2}~]]
   *           will return string [value] string or otherstring [value2]
   *           but this only works for ONE nest level!
   *
   * Usage: Formula--SQL:
   *   :RetVal := sitespecific.pkg_ddenFormula.DisplayFormat(:FormatString);
   *
   *------------------------------------------------------------------------*/
  function FormatDisplayString (
    a_ObjectID                  number,
    a_FormatString              varchar2
  ) return varchar2;

  function FormatDisplayStringFeeType (
    a_ObjectID                  number,
    a_FormatString              varchar2
  ) return varchar2;

end pkg_ddenFormula;



 
/

create or replace package body pkg_ddenFormula as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------
   * IsValueInList() -- Public
   *------------------------------------------------------------------------*/
  function IsValueInList (
    a_List                      varchar2,
    a_Value                     varchar2
  ) return varchar2 is
    t_List                      varchar2(32767);
    t_Value                     varchar2(32767);
  begin
    t_List := ',' || a_List || ',';
    t_Value := ',' || a_Value || ',';
    if instr(t_Value, t_List) > 0 then
      return 'Y';
    else
      return 'N';
    end if;
  end;

  /*--------------------------------------------------------------------------
   * TildeHack() -- Private
   *  This is here 'cause I need to re-write FormatDisplayString properly
   *------------------------------------------------------------------------*/
  function TildeHack (
    a_String                    varchar2
  ) return varchar2 is
    t_End                       number;
    t_NextChar                  char(1);
    t_Result                    varchar2(4000);
    t_Start                     number;
  begin

    /* Prime the pump */
    t_Start := 1;
    t_End := instr(a_String, '~');

    /* Parse ~'s */
    while t_End > 0 loop

      /* Add straight text to result */
      t_Result := t_Result || substr(a_String, t_Start, t_End - t_Start);

      t_Start := t_End + 2;
      t_NextChar := substr(a_String, t_End + 1, 1);
      if t_NextChar = '[' or t_NextChar = ']' or
         t_NextChar = '{' or t_NextChar = '}' or
         t_NextChar = '~' then

        /* Turn it into whatever we found */
        t_Result := t_Result || t_NextChar;

      elsif t_NextChar = 'n' or t_NextChar = 'N' then

        /* Turn it into a line feed */
        t_Result := t_Result || chr(10);

      elsif t_NextChar = 'r' or t_NextChar = 'R' then

        /* Turn it into a carrage return */
        t_Result := t_Result || chr(13);

      elsif t_NextChar = 't' or t_NextChar = 'T' then

        /* Turn it into a tab */
        t_Result := t_Result || chr(9);

      else

        /* Leave it alone */
        t_Result := t_Result || '~' || t_NextChar;

      end if;

      t_End := instr(a_String, '~', t_Start);

   end loop;

   t_Result := t_Result || substr(a_String, t_Start);
   return(t_Result);

  end;


  /*--------------------------------------------------------------------------
   * FormatDisplayString()
   *   Basically accepts a string just like the "Display Format" and returns
   *   the same result as the Display Format would, Plus the following useful
   *   additions:
   *
   * Special handling of [ {} ]
   *   [string {column} string]|[otherstring]:
   *     If value of column is not null: Returns string value string
   *     If value of column is null: Returns otherstring
   *     eg Status[ - Rank: {RankNumber}] -- {Status}
   *       Returns Status - Rank: 1 -- Complete
   *       or
   *       Returns Status -- Complete
   *     eg Status[ - Rank: {RankNumber}]|[ - No Rank] -- {Status}
   *       Returns Status - Rank: 1 -- Complete
   *       or
   *       Returns Status - No Rank -- Complete
   *   *Note - if [ ] does not contain a {}, it is returned exactly as is
   *     eg Status [Rank] -- {Status}
   *       Returns Status [Rank] -- Complete
   *
   * Special handling of ~
   *   ~n - turns into line feed
   *   ~r - turns into carriage return
   *   ~t - turns into tab
   *   ~~ - turns into ~
   *   ~{ - turns into {
   *   ~[ - turns into [
   *   *Note - Status [ string ~[{column}~] string]|[otherstring ~[{column2}~]]
   *           will return string [value] string or otherstring [value2]
   *           but this only works for ONE nest level!
   *
   * Usage: Formula--SQL:
   *   :RetVal := sitespecific.pkg_ddenFormula.DisplayFormat(FormatString);
   *
   *------------------------------------------------------------------------*/
  function FormatDisplayString (
    a_ObjectID                  number,
    a_FormatString              varchar2
  ) return varchar2 is
    t_Column                    varchar2(30);
    t_ColumnValue               varchar2(4000);
    t_End                       number;
    t_End2                      number;
    t_InOr                      boolean;
    t_Result                    varchar2(4000);
    t_SpecialChar               char(1);
    t_Start                     number;
    t_Start2                    number;
  begin

    /* Prime the pump */
    t_Start := 1;
    t_InOr := false;
    t_End := instr(a_FormatString, '[');
    t_End2 := instr(a_FormatString, '{');
    if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
      t_End := t_End2;
    end if;
    t_End2 := instr(a_FormatString, '~');
    if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
      t_End := t_End2;
    end if;

    /* Parse []'s {}'s and ~'s */
    while t_End > 0 loop

      /* Add straight text to result */
      t_Result := t_Result || substr(a_FormatString, t_Start, t_End - t_Start);

      /* Check what kind of special character this was ([{~) */
      t_SpecialChar := substr(a_FormatString, t_End, 1);
      if t_SpecialChar = '~' then

        /* Parsing a ~ */
        t_Start := t_End + 2;
        t_SpecialChar := substr(a_FormatString, t_End + 1, 1);
        if t_SpecialChar = '[' or t_SpecialChar = ']' or
           t_SpecialChar = '{' or t_SpecialChar = '}' or
           t_SpecialChar = '~' then

          /* Turn it into whatever we found */
          t_Result := t_Result || t_SpecialChar;

        elsif t_SpecialChar = 'n' or t_SpecialChar = 'N' then

          /* Turn it into a line feed */
          t_Result := t_Result || chr(10);

        elsif t_SpecialChar = 'r' or t_SpecialChar = 'R' then

          /* Turn it into a carrage return */
          t_Result := t_Result || chr(13);

        elsif t_SpecialChar = 't' or t_SpecialChar = 'T' then

          /* Turn it into a tab */
          t_Result := t_Result || chr(9);

        else

          /* Leave it alone */
          t_Result := t_Result || '~' || t_SpecialChar;

        end if;

      elsif t_SpecialChar = '[' then

        /* Parsing a [] */
        t_Start := t_End + 1;
        t_End := instr(a_FormatString, ']', t_Start);

        /* Check if the ']' is escaped (~]) */
        if t_End > 0 then
          if substr(a_FormatString, t_End - 1, 1) = '~' then
            t_End := instr(a_FormatString, ']', t_End + 1);
          end if;
        end if;
        if t_End > 0 then

          /* Parse the {} */
          t_Start2 := instr(a_FormatString, '{', t_Start);
          if t_Start2 > 0 and t_Start2 < t_End then

            t_End2 := instr(a_FormatString, '}', t_Start2);
            if t_End2 > 0 and t_End2 < t_End then

              /* Parse column */
              t_Column := substr(a_FormatString, t_Start2 + 1, t_End2 - t_Start2 - 1);
              t_ColumnValue := api.pkg_ColumnQuery.Value(a_ObjectID, t_Column);
              if t_ColumnValue is not null then

                /* Include everything between the []'s */
                t_ColumnValue := TildeHack(substr(a_FormatString, t_Start, t_Start2 - t_Start))
                    || t_ColumnValue || TildeHack(substr(a_FormatString, t_End2 + 1, t_End - t_End2 - 1));

                /* And now for a little hack because I didn't write a full parser --
                 * Check if there are any ~['s or ~]'s and clean'em up before adding
                 * this to the result string.  This only works for ONE nest level
                 * (yeah well, thats why it's a hack)
                 */
                t_Start2 := instr(t_ColumnValue, '~[');
                if t_Start2 > 0 then
                  t_ColumnValue := substr(t_ColumnValue, 1, t_Start2 - 1) ||
                      substr(t_ColumnValue, t_Start2 + 1);
                end if;
                t_Start2 := instr(t_ColumnValue, '~]');
                if t_Start2 > 0 then
                  t_ColumnValue := substr(t_ColumnValue, 1, t_Start2 - 1) ||
                      substr(t_ColumnValue, t_Start2 + 1);
                end if;

                t_Result := t_Result || t_ColumnValue;

                /* Check if an 'or' side exists */
                if substr(a_FormatString, t_End + 1, 2) = '|[' then
                  t_End2 := instr(a_FormatString, ']', t_End + 2);

                  /* Check if the ']' is escaped (~]) */
                  if t_End2 > 0 then
                    if substr(a_FormatString, t_End2 - 1, 1) = '~' then
                      t_End2 := instr(a_FormatString, ']', t_End2 + 1);
                    end if;
                  end if;
                  if t_End2 > 0 then

                    /* Skip the 'or' side */
                    t_End := t_End2;

                  end if;

                end if;

              else

                /* Check if an 'or' side exists */
                if substr(a_FormatString, t_End + 1, 2) = '|[' then

                  /* Let's make sure it ends too! */
                  t_End2 := instr(a_FormatString, ']', t_End + 2);

                  /* Check if the ']' is escaped (~]) */
                  if t_End2 > 0 then
                    if substr(a_FormatString, t_End2 - 1, 1) = '~' then
                      t_End2 := instr(a_FormatString, ']', t_End2 + 1);
                    end if;
                  end if;
                  if t_End2 > 0 then
                    t_InOR := true;
                    t_End := t_End + 1;
                  end if;

                end if;

              end if;
              t_Start := t_End + 1;

            else

            /* Must just be a plain old [] */
            t_Result := t_Result || substr(a_FormatString, t_Start - 1, 1);

            end if;

          else

            /* Must just be a plain old [] */
            if not t_InOr then
              t_Result := t_Result || substr(a_FormatString, t_Start - 1, 1);
            end if;

          end if;

        else

          /* Badly formed format ([ without a ]) -- treat like straight text */
          t_Result := t_Result || substr(a_FormatString, t_Start - 1, 1);

        end if;

      elsif t_SpecialChar = ']' then

        /* Finish off the "or" side of the [ ]|[ ] construct */
        t_Start := t_End + 1;
        t_InOR := false;

      else

        /* Parsing a {} */
        t_Start := t_End + 1;
        t_End := instr(a_FormatString, '}', t_Start);
        if t_End > 0 then

          /* Parse column */
          t_Column := substr(a_FormatString, t_Start, t_End - t_Start);
          t_Result := t_Result || api.pkg_ColumnQuery.Value(a_ObjectID, t_Column);
          t_Start := t_End + 1;

        else

          /* Badly formed format ({ without a }) -- treat like straight text */
          t_Result := t_Result || substr(a_FormatString, t_Start - 1, 1);

        end if;

      end if;

      /* Get next special character */
      t_End := instr(a_FormatString, '[', t_Start);
      t_End2 := instr(a_FormatString, '{', t_Start);
      if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
        t_End := t_End2;
      end if;
      t_End2 := instr(a_FormatString, '~', t_Start);
      if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
        t_End := t_End2;
      end if;
      if t_InOr then
        t_End2 := instr(a_FormatString, ']', t_Start);
        if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
          t_End := t_End2;
        end if;
      end if;

    end loop;

    t_Result := t_Result || substr(a_FormatString, t_Start);
    return(t_Result);

  end;
  
    function FormatDisplayStringFeeType (
    a_ObjectID                  number,
    a_FormatString              varchar2
  ) return varchar2 is
    t_Column                    varchar2(30);
    t_ColumnValue               varchar2(4000);
    t_End                       number;
    t_End2                      number;
    t_InOr                      boolean;
    t_Result                    varchar2(4000);
    t_SpecialChar               char(1);
    t_Start                     number;
    t_Start2                    number;
  begin

    /* Prime the pump */
    t_Start := 1;
    t_InOr := false;
    t_End := instr(a_FormatString, '[');
    t_End2 := instr(a_FormatString, '{');
    if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
      t_End := t_End2;
    end if;
    t_End2 := instr(a_FormatString, '~');
    if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
      t_End := t_End2;
    end if;

    /* Parse []'s {}'s and ~'s */
    while t_End > 0 loop

      /* Add straight text to result */
      t_Result := t_Result || substr(a_FormatString, t_Start, t_End - t_Start);

      /* Check what kind of special character this was ([{~) */
      t_SpecialChar := substr(a_FormatString, t_End, 1);
      if t_SpecialChar = '~' then

        /* Parsing a ~ */
        t_Start := t_End + 2;
        t_SpecialChar := substr(a_FormatString, t_End + 1, 1);
        if t_SpecialChar = '[' or t_SpecialChar = ']' or
           t_SpecialChar = '{' or t_SpecialChar = '}' or
           t_SpecialChar = '~' then

          /* Turn it into whatever we found */
          t_Result := t_Result || t_SpecialChar;

        elsif t_SpecialChar = 'n' or t_SpecialChar = 'N' then

          /* Turn it into a line feed */
          t_Result := t_Result || chr(10);

        elsif t_SpecialChar = 'r' or t_SpecialChar = 'R' then

          /* Turn it into a carrage return */
          t_Result := t_Result || chr(13);

        elsif t_SpecialChar = 't' or t_SpecialChar = 'T' then

          /* Turn it into a tab */
          t_Result := t_Result || chr(9);

        else

          /* Leave it alone */
          t_Result := t_Result || '~' || t_SpecialChar;

        end if;

      elsif t_SpecialChar = '[' then

        /* Parsing a [] */
        t_Start := t_End + 1;
        t_End := instr(a_FormatString, ']', t_Start);

        /* Check if the ']' is escaped (~]) */
        if t_End > 0 then
          if substr(a_FormatString, t_End - 1, 1) = '~' then
            t_End := instr(a_FormatString, ']', t_End + 1);
          end if;
        end if;
        if t_End > 0 then

          /* Parse the {} */
          t_Start2 := instr(a_FormatString, '{', t_Start);
          if t_Start2 > 0 and t_Start2 < t_End then

            t_End2 := instr(a_FormatString, '}', t_Start2);
            if t_End2 > 0 and t_End2 < t_End then

              /* Parse column */
              t_Column := substr(a_FormatString, t_Start2 + 1, t_End2 - t_Start2 - 1);
              t_ColumnValue := api.pkg_ColumnQuery.Value(a_ObjectID, t_Column);
              if t_ColumnValue is not null then

                /* Include everything between the []'s */
                t_ColumnValue := TildeHack(substr(a_FormatString, t_Start, t_Start2 - t_Start))
                    || t_ColumnValue || TildeHack(substr(a_FormatString, t_End2 + 1, t_End - t_End2 - 1));

                /* And now for a little hack because I didn't write a full parser --
                 * Check if there are any ~['s or ~]'s and clean'em up before adding
                 * this to the result string.  This only works for ONE nest level
                 * (yeah well, thats why it's a hack)
                 */
                t_Start2 := instr(t_ColumnValue, '~[');
                if t_Start2 > 0 then
                  t_ColumnValue := substr(t_ColumnValue, 1, t_Start2 - 1) ||
                      substr(t_ColumnValue, t_Start2 + 1);
                end if;
                t_Start2 := instr(t_ColumnValue, '~]');
                if t_Start2 > 0 then
                  t_ColumnValue := substr(t_ColumnValue, 1, t_Start2 - 1) ||
                      substr(t_ColumnValue, t_Start2 + 1);
                end if;

                t_Result := t_Result || t_ColumnValue;

                /* Check if an 'or' side exists */
                if substr(a_FormatString, t_End + 1, 2) = '|[' then
                  t_End2 := instr(a_FormatString, ']', t_End + 2);

                  /* Check if the ']' is escaped (~]) */
                  if t_End2 > 0 then
                    if substr(a_FormatString, t_End2 - 1, 1) = '~' then
                      t_End2 := instr(a_FormatString, ']', t_End2 + 1);
                    end if;
                  end if;
                  if t_End2 > 0 then

                    /* Skip the 'or' side */
                    t_End := t_End2;

                  end if;

                end if;

              else

                /* Check if an 'or' side exists */
                if substr(a_FormatString, t_End + 1, 2) = '|[' then

                  /* Let's make sure it ends too! */
                  t_End2 := instr(a_FormatString, ']', t_End + 2);

                  /* Check if the ']' is escaped (~]) */
                  if t_End2 > 0 then
                    if substr(a_FormatString, t_End2 - 1, 1) = '~' then
                      t_End2 := instr(a_FormatString, ']', t_End2 + 1);
                    end if;
                  end if;
                  if t_End2 > 0 then
                    t_InOR := true;
                    t_End := t_End + 1;
                  end if;

                end if;

              end if;
              t_Start := t_End + 1;

            else

            /* Must just be a plain old [] */
            t_Result := t_Result || substr(a_FormatString, t_Start - 1, 1);

            end if;

          else

            /* Must just be a plain old [] */
            if not t_InOr then
              t_Result := t_Result || substr(a_FormatString, t_Start - 1, 1);
            end if;

          end if;

        else

          /* Badly formed format ([ without a ]) -- treat like straight text */
          t_Result := t_Result || substr(a_FormatString, t_Start - 1, 1);

        end if;

      elsif t_SpecialChar = ']' then

        /* Finish off the "or" side of the [ ]|[ ] construct */
        t_Start := t_End + 1;
        t_InOR := false;

      else

        /* Parsing a {} */
        t_Start := t_End + 1;
        t_End := instr(a_FormatString, '}', t_Start);
        if t_End > 0 then

          /* Parse column */
          t_Column := substr(a_FormatString, t_Start, t_End - t_Start);
          t_Result := t_Result || api.pkg_ColumnQuery.Value(a_ObjectID, t_Column);
          t_Start := t_End + 1;

        else

          /* Badly formed format ({ without a }) -- treat like straight text */
          t_Result := t_Result || substr(a_FormatString, t_Start - 1, 1);

        end if;

      end if;

      /* Get next special character */
      t_End := instr(a_FormatString, '[', t_Start);
      t_End2 := instr(a_FormatString, '{', t_Start);
      if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
        t_End := t_End2;
      end if;
      t_End2 := instr(a_FormatString, '~', t_Start);
      if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
        t_End := t_End2;
      end if;
      if t_InOr then
        t_End2 := instr(a_FormatString, ']', t_Start);
        if (t_End2 > 0 and t_End = 0) or (t_End2 > 0 and t_End2 < t_End) then
          t_End := t_End2;
        end if;
      end if;

    end loop;

    t_Result := t_Result ||substr(a_FormatString, t_Start);
    return(t_Result);

  end;

end pkg_ddenFormula;



/

