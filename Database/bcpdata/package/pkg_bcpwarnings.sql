create or replace package pkg_BCPWarnings as
  
  /*
    ColorWarningTab() -- Alan den Otter, Jan 19, 2012.
      This function deals with the fact that a Warnings tab
      may display not only Warnings related to the current object,
      but also warnings on objects related to the current object.
      Additionally, we need to consider that a Warning has a Released
      Date, but that Date is in the future. 
      
      This function looks at all Warnings displayed on the tab, 
      checks the Released Dates, and returns a Y or N depending 
      on if the Warning tab needs to be colored.
      
      The function assumes that the current object is related to 
      another object that serves as a "Warning" object. A
      "no_data_found" error will be raised if this is not the case.
      
      Referenced in expression column named WarningExist on j_BuildingPermit
      and on other columns I'm sure (Albert).
  */
  function ColorWarningTab (
    a_ObjectId             number,
    a_TargetObjectDefName  varchar2
  ) return char;
end;

 
/

grant execute
on pkg_bcpwarnings
to posseextensions;

create or replace package body pkg_BCPWarnings as

  function ColorWarningTab (
    a_ObjectId             number,
    a_TargetObjectDefName  varchar2
  ) return char as
    t_sourcedefid  number(9);
    t_targetdefid  number(9) := api.pkg_configquery.ObjectDefIdForName(a_TargetObjectDefName);
    t_warnings     api.pkg_definition.udt_IdList;
    t_warnendpoints  api.pkg_definition.udt_IdList;
    t_colortab     char(1) := 'N';
    t_count        number(9) := 0;
  begin
    select o.objectdefid
    into t_sourcedefid
    from api.objects o
    where o.objectid = a_objectid;
    
    begin
      select e.EndPointId
      bulk collect into t_warnendpoints
      from api.endpoints e
      where e.FromObjectDefId = t_sourcedefid
        and e.ToObjectDefId = t_targetdefid;
    exception
      when no_data_found then
        api.pkg_errors.RaiseError(-20000, 'This procedure is being use in an invalid ' ||
          'configuration. Please see the procedure comments for more information.');
    end;

    t_warnings := api.pkg_objectquery.RelatedObjects(a_objectid, t_warnendpoints);

    -- If a condition that would color the tab is found, don't bother checking any more
    -- warning objects.
    begin
      for a in 1..t_warnings.count loop
        t_count := t_count + 1;
        if api.pkg_columnquery.DateValue(t_warnings(a), 'ReleasedDate') is null or
          api.pkg_columnquery.DateValue(t_warnings(a), 'ReleasedDate') >= trunc(sysdate) + 1 then
          t_colortab := 'Y';
          exit;
        end if;
      end loop;
    end;
    
    return t_colortab;
  end;

end;

/

