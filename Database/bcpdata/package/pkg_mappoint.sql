create or replace package pkg_MapPoint is

/*
  Albert - renaming to deprecated since I can't find references to
  this package and it doesn't compile anyways.
  This package has not been added to CVS.
*/

  /*---------------------------------------------------------------------
  * UpdateMapPoint() -- PUBLIC
  *-------------------------------------------------------------------*/
  procedure UpdateMapPoint(a_ObjectId                number,
                           a_AsOfDate                date,
                           a_LinkPosseObjectId       number,
                           a_LinkPosseObjectDefId    number,
                           a_LocationXY              varchar2);

end pkg_MapPoint;

 
/

grant execute
on pkg_mappoint
to posseextensions;

create or replace package body pkg_MapPoint is


  /*---------------------------------------------------------------------
  * UpdateMapPoint() -- PUBLIC
  *-------------------------------------------------------------------*/
  procedure UpdateMapPoint(a_ObjectId                number,
                           a_AsOfDate                date,
                           a_LinkPosseObjectId       number,
                           a_LinkPosseObjectDefId    number,
                           a_LocationXY              varchar2
  ) is
    t_LocationX               varchar2(100);
    t_LocationY               varchar2(100);
    t_PosseObjectId           number;
    t_MapPointObjectTypeId    number := api.pkg_ConfigQuery.ObjectDefIdForName('o_MapPoint');
    t_EndPointId              number;
    t_NewRelationshipId       number;

  begin
    if instr(a_LocationXY, ',') > 0 and a_LocationXY is not null
      then  
        t_LocationX := substr(a_LocationXY, 1, instr(a_LocationXY, ',') - 1);
        t_LocationY := substr(a_LocationXY, instr(a_LocationXY, ',') + 1);
        
        for c in (select mp.PosseObjectId
                    from bcpdata.MapPoint mp
                   where mp.LinkPosseObjectId = a_LinkPosseObjectId) loop
          t_PosseObjectId := c.PosseObjectId;
        end loop;
        
        if t_PosseObjectId is not null and t_PosseObjectId <> 0
          then
            api.pkg_ColumnUpdate.SetValue(t_PosseObjectId, 'LocationX', t_LocationX);
            api.pkg_ColumnUpdate.SetValue(t_PosseObjectId, 'LocationY', t_LocationY);
          else
            --Create Object
            t_PosseObjectId := api.pkg_ObjectUpdate.New(t_MapPointObjectTypeId);
            api.pkg_ColumnUpdate.SetValue(t_PosseObjectId, 'LocationX', t_LocationX);
            api.pkg_ColumnUpdate.SetValue(t_PosseObjectId, 'LocationY', t_LocationY);
            api.pkg_ColumnUpdate.SetValue(t_PosseObjectId, 'LinkPosseObjectId', a_LinkPosseObjectId);
            api.pkg_ColumnUpdate.SetValue(t_PosseObjectId, 'LinkPosseObjectDefId', a_LinkPosseObjectDefId);
            --Create Relationship
            t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(a_LinkPosseObjectDefId, 'MapPoint');
            t_NewRelationshipId := api.Pkg_RelationshipUpdate.New(t_EndPointId, a_LinkPosseObjectId, t_PosseObjectId);
        end if;
    end if;
  end UpdateMapPoint;


end pkg_MapPoint;

/

