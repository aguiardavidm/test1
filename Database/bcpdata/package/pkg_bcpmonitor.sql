create or replace package pkg_BCPMonitor as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*--------------------------------------------------------------------------
   * MonitorConstructor()
   *   If used on constructor of a Monitor process, will auto-complete the
   * process if there is nothing to monitor.  This is handy so that you can
   * create the whole set of processes conditionally, and if there is nothing
   * to monitor in the end, the monitor process will be completed and workflow
   * will proceed.
   *------------------------------------------------------------------------*/
  procedure MonitorConstructor (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
   * MonitoredConstructor()
   *   If used on the constructor even of a monitored process, will create
   * the Monitor process if it doesn't exists, as well as move the process
   * that it is called on before the Monitor process.  This way, the
   * monitored processes will stay grouped before the Monitor process that is
   * monitoring them, even if a monitored process is created after the Monitor
   * process (i.e. re-review where the second review keeps the monitor open).
   * Also, you can set up a monitor without having to specifically create a
   * Monitor process.
   *------------------------------------------------------------------------*/
/*  procedure MonitoredConstructor (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date,
    a_MonitorProcessName        varchar2
  );
*/
  /*--------------------------------------------------------------------------
   * MonitorProcessGroup()
   *
   * How to use:
   *   1) Setup all processes to be monitored ensuring that each has a detail
   *      named 'MonitorProcessName' bound to constant text containing the
   *      name of the monitoring process.
   *   2) Add this procedure as an activity to run on each process from step 1
   *   3) Setup the monitor process such that when the last of the group of
   *      monitored processes completes, the job will be in a status whereby
   *      there is only
   *      1 possible outcome for the monitor process.
   *   4) If you want to enforce that all the processes from step 1 are
   *      completed before the monitor process is completed, add this
   *      procedure as an activity to run on the monitor process as well.
   *      In this case, DO not have the 'MonitorProcessName' detail on the
   *      monitor process.
   *      *** note ***
   *      If you do not do this step, this process can be completed manually
   *      before all of the processes in the group from step 1 have been
   *      completed.
   *
   *------------------------------------------------------------------------*/
  procedure MonitorProcessGroup(
    a_ProcessId         udt_Id,
    a_AsOfDate          date,
    a_Outcome           varchar2
  );

end pkg_BCPMonitor;



 
/

grant execute
on pkg_bcpmonitor
to posseextensions;

create or replace package body pkg_BCPMonitor as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  gc_Package               constant varchar2(50) := 'bcpdata.pkg_BCPMonitor.';

  /*--------------------------------------------------------------------------
   * Package variable declarations -- PRIVATE
   * g_LastJobId is used to determine whether the CreateProcessWhenAllComplete
   *   procedure has been run yet for this job.
   *------------------------------------------------------------------------*/
  g_LastJobId udt_Id;

  /*--------------------------------------------------------------------------
   * CompleteMonitorProcess() -- PRIVATE
   *------------------------------------------------------------------------*/
  procedure CompleteMonitorProcess (
    a_MonitorProcessId          udt_Id,
    a_MonitorProcessTypeId      udt_Id,
    a_JobId                     udt_Id,
    a_MonitorProcessName        api.ProcessTypes.Description%type,
    a_JobTypeId                 udt_Id,
    a_JobStatus                 api.Jobs.JobStatus%type
  ) is
    t_Outcome                   api.ProcessOutcomes.Outcome%type;
    t_Count                     pls_integer;
    t_MonitorCompletionCondition varchar2(1);
  begin

    toolbox.pkg_Util.TraceMethodEntry(gc_Package || 'CompleteMonitorProcess',
      'a_MonitorProcessId',       a_MonitorProcessId,
      'a_MonitorProcessTypeId',   a_MonitorProcessTypeId,
      'a_JobId',                  a_JobId,
      'a_MonitorProcessName',     a_MonitorProcessName,
      'a_JobTypeId',              a_JobTypeId,
      'a_JobStatus',              a_JobStatus);

    t_MonitorCompletionCondition := api.pkg_ColumnQuery.Value(a_MonitorProcessId, 'MonitorCompletionCondition');

    if t_MonitorCompletionCondition is null or t_MonitorCompletionCondition = 'Y' then
      /* Check to see if there are any uncompleted processes in this job which
         reference the monitoring process */
      select count(*)
        into t_Count
        from api.Processes
       where JobId = a_JobId
         and DateCompleted is null
         and api.pkg_ColumnQuery.Value(ProcessId, 'MonitorProcessName',
             sysdate) = a_MonitorProcessName
         and ProcessId <> a_MonitorProcessId;

      /* If there are no incompleted monitored processes in job, automatically
         update monitor */
      if t_Count = 0 then
        /* determine the outcome to put on the process - there should be only
           1 possibility at this from status */
        begin
          select o.Outcome
            into t_Outcome
            from api.ProcessOutcomes o
            join api.StatusChanges s
              on o.ProcessOutcomeId = s.ProcessOutcomeId
            join api.statuses st
              on s.StatusChangeId = st.StatusId
           where o.ProcessTypeId = a_MonitorProcessTypeId
             and st.Tag = a_JobStatus
             and o.JobTypeId = a_JobTypeId
             and st.Tag = a_JobStatus;

        exception when no_data_found then
          begin
            select o.Outcome
              into t_Outcome
              from api.ProcessOutcomes o
              join api.StatusChanges s
                on o.ProcessOutcomeId = s.ProcessOutcomeId
              join api.statuses st
                on s.StatusChangeId = st.StatusId
             where o.ProcessTypeId = a_MonitorProcessTypeId
               and st.Tag = a_JobStatus
               and o.JobTypeId = a_JobTypeId;
               --and s.FromStatus is null;
          exception when others then
            toolbox.pkg_Util.RaiseError(-20000, 'There are no outcomes possible for '
              || 'the monitoring process at this job status ('
              || a_JobStatus || ')');
          end;
        when others then
          toolbox.pkg_Util.RaiseError(-20000, 'There must be 1 and only 1 outcome '
            || 'possible for the monitoring process at this job status');
        end;

        /* Complete this last process */
        api.pkg_ProcessUpdate.Complete(a_MonitorProcessId, t_Outcome);
      end if;
    end if;

    toolbox.pkg_Util.TraceMethodExit;
  end;

  /*--------------------------------------------------------------------------
   * ReorderProcesses() -- PRIVATE
   *------------------------------------------------------------------------*/
/*  procedure ReorderProcesses (
    a_ProcessId                 udt_Id,
    a_SeqNumAbove               api.Processes.SeqNum%type
  ) is
    t_SeqNum                    api.Processes.SeqNum%type;
  begin
    toolbox.pkg_Util.TraceMethodEntry(gc_Package || 'ReorderProcesses',
                       'a_ProcessId',     a_ProcessId,
                       'a_SeqNumAbove',   a_SeqNumAbove);

    t_SeqNum := 1;
    for c in (select pr2.ProcessId,
                     pr2.SeqNum
                from api.Processes pr
                join api.Processes pr2
                  on pr2.JobId = pr.JobId
               where pr.ProcessId = a_ProcessId
            order by decode(pr2.ProcessId, a_ProcessId, a_SeqNumAbove + .5
                    , pr2.SeqNum)) loop
      if c.SeqNum <> t_SeqNum then
        -- api doesn't support resequence
        workflow.pkg_ProcessUpdate.Resequence(c.ProcessId, t_SeqNum);
        workflow.
      end if;
      t_SeqNum := t_SeqNum + 1;
    end loop;

    toolbox.pkg_Util.TraceMethodExit;
  end;
*/
  /*--------------------------------------------------------------------------
   * MonitorConstructor() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure MonitorConstructor (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_ProcessTypeId             udt_Id;
    t_JobId                     udt_Id;
    t_Outcome                   api.ProcessOutcomes.Outcome%type;
    t_MonitorProcessName        api.ProcessTypes.Description%type;
    t_JobTypeId                 udt_Id;
    t_JobStatus                 api.Jobs.JobStatus%type;
    t_Count                     pls_integer;
  begin
    toolbox.pkg_Util.TraceMethodEntry(gc_Package || 'MonitorConstructor',
                         'a_ProcessId',   a_ProcessId,
                         'a_AsOfDate',    toolbox.pkg_Util.FormatValue(a_AsOfDate));

    begin
      select pr.ProcessTypeId,
             pr.JobId,
             pr.Outcome,
             pt.Description,
             jo.JobTypeId,
             jo.JobStatus
        into t_ProcessTypeId,
             t_JobId,
             t_Outcome,
             t_MonitorProcessName,
             t_JobTypeId,
             t_JobStatus
        from api.Jobs jo
        join api.Processes pr
         on jo.JobId = pr.JobId
        join api.ProcessTypes pt
          on pt.ProcessTypeId = pr.ProcessTypeId
       where pr.ProcessId = a_ProcessId;
    exception when no_data_found then
      toolbox.pkg_Util.RaiseError(-20000,'Object ' || a_ProcessId
        || ' is not a Process.');
    end;

    /* Check if this process is already complete.  This is possible in certain
     * situations, where something being monitored completes in the same
     * logical transaction as the construction of this monitor process.  In
     * that case the completion of this process will actually occur before
     * the constructor is called.  (Just imagine how long it took to figure
     * out what in the world was happening.
     */
    if t_Outcome is null then
      CompleteMonitorProcess(a_ProcessId, t_ProcessTypeId, t_JobId,
          t_MonitorProcessName, t_JobTypeId, t_JobStatus);
    end if;

    toolbox.pkg_Util.TraceMethodExit;
  end;

  /*--------------------------------------------------------------------------
   * MonitoredConstructor() -- PUBLIC
   *------------------------------------------------------------------------*/
/*  procedure MonitoredConstructor (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date,
    a_MonitorProcessName        varchar2
  ) is
    t_JobId                     udt_Id;
    t_MonitorProcessId          udt_Id;
    t_MonitorProcessTypeId      udt_Id;
    t_LowestMonitoredProcessId  udt_Id;
    t_MonitorSeqNum             api.Processes.SeqNum%type;

  begin
    toolbox.pkg_Util.TraceMethodEntry(gc_Package || 'MonitoredConstructor',
                     'a_ProcessId',           a_ProcessId,
                     'a_AsOfDate',            toolbox.pkg_Util.FormatValue(a_AsOfDate),
                     'a_MonitorProcessName',  a_MonitorProcessName);

    begin
      select JobId
        into t_JobId
        from api.Processes
       where ProcessId = a_ProcessId;
    exception when no_data_found then
      toolbox.pkg_Util.RaiseError(-20000, 'Invalid process: ' || a_ProcessId);
    end;

    begin
      select ProcessTypeId
        into t_MonitorProcessTypeId
        from api.ProcessTypes
       where Description = a_MonitorProcessName;
    exception when no_data_found then
      toolbox.pkg_Util.RaiseError(-20000, 'Process type: ' || a_MonitorProcessName
        || ' does not exist.');
    end;

    /* Find process with the highest sequence number for re-sequence.
       This query also tells us whether a monitor process should be created
       if it doesn't exist. If a monitor already exists, move this new process
       after the last monitored process If no active monitor exists, create
       one and move it after the last monitored process
       (probably this process) */

/*    begin
      select ProcessId,
             SeqNum
        into t_MonitorProcessId,
             t_MonitorSeqNum
        from api.Processes pr
       where pr.JobId = t_JobId
         and pr.ProcessTypeId = t_MonitorProcessTypeId
         and pr.DateCompleted is null;

      -- if monitor exists, move me to the position before the monitor
      ReorderProcesses(a_ProcessId, t_MonitorSeqNum - 1);
    exception
      when no_data_found then
        t_MonitorProcessId := api.pkg_ProcessUpdate.New(t_JobId,
            t_MonitorProcessTypeId, null, null, null, null);
      when others then
        toolbox.pkg_Util.RaiseError(-20000, 'Problem found (possibly multiple?) '
          || 'with monitoring process.');
    end;

    toolbox.pkg_Util.TraceMethodExit;
  end;
*/
  /*--------------------------------------------------------------------------
   * MonitorProcessGroup() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure MonitorProcessGroup (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date,
    a_Outcome                   varchar2
  ) is
    t_ProcessNames              varchar2(2000);
    t_Count                     number(9);
    t_Outcome                   api.ProcessOutcomes.Outcome%TYPE;
    t_MonitorProcessId          udt_Id;
    t_MonitorProcessTypeId      udt_Id;
    t_MonitorProcessName        api.Processes.Description%TYPE;
    t_MonitorProcessViewName    api.ObjectDefs.Name%type;
    t_MonitorProcessDescription api.ObjectDefs.Description%type;
    t_ProcessTypeId             udt_Id;
    t_JobId                     udt_Id;
    t_JobTypeId                 udt_Id;
    t_JobStatus                 api.Jobs.JobStatus%TYPE;
    t_MonitorCompletionCondition varchar2(1);

    cursor c_Uncompleted (
      a_JobId                     udt_Id,
      a_MonitorProcessViewName    api.ObjectDefs.Name%type,
      a_MonitorProcessDescription api.ObjectDefs.Description%type default null
    ) is
      select t.Description, count(*) count
        from api.Processes p
        join api.ProcessTypes t
          on p.ProcessTypeId = t.ProcessTypeId
       where JobId = a_JobId
         and DateCompleted is null
         and api.pkg_ColumnQuery.Value(ProcessId, 'MonitorProcessName',
             sysdate) in (a_MonitorProcessViewName,
             a_MonitorProcessDescription)
       group by t.Description;

  begin
    toolbox.pkg_Util.TraceMethodEntry(gc_Package || 'MonitorProcessGroup',
                         'a_ProcessId',   a_ProcessId,
                         'a_AsOfDate',    toolbox.pkg_Util.FormatValue(a_AsOfDate),
                         'a_Outcome',     a_Outcome);

     /* If this process isn't complete yet, we're done */
    if a_Outcome is null then
      toolbox.pkg_Util.TraceMethodExit;
      return;
    end if;

    /* Get JobId, JobTypeId, jobstatus, ProcessTypeId, monitorprocessname */
    begin
      select b.JobId,
             b.JobTypeId,
             b.JobStatus,
             a.ProcessTypeId,
             api.pkg_ColumnQuery.Value(a.ProcessId, 'MonitorProcessName',
                sysdate)
        into t_JobId, t_JobTypeId, t_JobStatus, t_ProcessTypeId,
             t_MonitorProcessName
        from api.Processes a
        join api.Jobs b
          on a.JobId = b.JobId
       where a.ProcessId = a_ProcessId;
    exception when no_data_found then
      toolbox.pkg_Util.RaiseError(-20000,
          'Process or related job does not exist');
    end;

    /* If there is no MonitorProcessName on this process, then this must BE
       the monitor */
    if t_MonitorProcessName is null then
      /* get the name of this process type */
      select Description,
             Name
        into t_MonitorProcessDescription,
             t_MonitorProcessViewName
        from api.Processtypes
       where ProcessTypeId = t_ProcessTypeId;

    /* check for incomplete processes being monitored by this one */
      select count(*)
        into t_Count
        from api.Processes
       where JobId = t_JobId
         and DateCompleted is null
         and api.pkg_ColumnQuery.Value(ProcessId, 'MonitorProcessName',
             sysdate) in (t_MonitorProcessViewName,
             t_MonitorProcessDescription);

      if t_Count > 0 then
        t_ProcessNames := '' ;
        for c in c_Uncompleted (t_JobId, t_MonitorProcessViewName,
         t_MonitorProcessDescription) loop
          t_ProcessNames := t_ProcessNames || ', ' || ltrim(c.Description);
          if c.count > 1 then
            t_ProcessNames := t_ProcessNames || '(x' || to_char(c.count)
              || ')';
          end if;
        end loop;

        toolbox.pkg_Util.RaiseError(-20000,
          'Cannot complete this process until the following processes '
          || 'are complete: ' || ltrim(t_ProcessNames, ', '));
      end if;
      /* if we got to here, we are allowed to simply continue without any
         further processing as the monitoring process is already
         complete - and that's OK */

      toolbox.pkg_Util.TraceMethodExit;
      return;
    end if;

    /* Get the ProcessTypeId for the named monitoring process */
    begin
      select ProcessTypeId
        into t_MonitorProcessTypeId
        from api.ProcessTypes
       where Description = t_MonitorProcessName
          or Name = t_MonitorProcessName;
    exception when no_data_found then
      toolbox.pkg_Util.RaiseError(-20000, 'Process of type: ' || t_MonitorProcessName
        || ' does not exist.');
    end;

    /* get the ProcessId for the monitoring process.  If no incomplete
       monitoring process exists, then simply exit the procedure without
       an error. */
    begin
      select ProcessId
        into t_MonitorProcessId
        from api.Processes
       where JobId = t_JobId
         and ProcessTypeId = t_MonitorProcessTypeId
         and DateCompleted is null;
    exception
      when no_data_found then

        toolbox.pkg_Util.TraceMethodExit;
        return;
      when others then
        toolbox.pkg_Util.RaiseError(-20000, 'Problem found (possibly multiple?) '
          || 'with monitoring process.');
    end;

    /* Check to see if there are any OTHER uncompleted processes in this job
       which reference the monitoring process */
    select count(*)
      into t_Count
      from api.Processes
     where JobId = t_JobId
       and DateCompleted is null
       and api.pkg_ColumnQuery.Value(ProcessId, 'MonitorProcessName',
           sysdate) = t_MonitorProcessName
       and ProcessId <> a_ProcessId;

    /* If there are no incompleted monitored processes in job, automatically
       update monitor */
    if t_Count = 0 then
      /* determine the outcome to put on the process - there should be only
         1 possibility at this from status */
      begin
        select o.Outcome
          into t_Outcome
          from api.ProcessOutcomes o
          join api.StatusChanges s
            on o.ProcessOutcomeId = s.ProcessOutcomeId
          join api.statuses st
            on s.StatusChangeId = st.StatusId
           --and o.ProcessTypeId = a_MonitorProcessTypeId
         where st.Tag = t_JobStatus
           and o.JobTypeId = t_JobTypeId
           and st.Tag = t_JobStatus;

      exception when no_data_found then
        begin
          select o.Outcome
            into t_Outcome
            from api.ProcessOutcomes o
            join api.StatusChanges s
              on o.ProcessOutcomeId = s.ProcessOutcomeId
            join api.statuses st
              on s.StatusChangeId = st.StatusId
           where st.Tag = t_JobStatus
             --and o.ProcessTypeId = a_MonitorProcessTypeId 
             and o.JobTypeId = t_JobTypeId
             and s.FromStatusId is null;
        exception when others then
          toolbox.pkg_Util.RaiseError(-20000, 'There are no outcomes possible for '
            || 'the monitoring process at this job status');
        end;
      when others then
        toolbox.pkg_Util.RaiseError(-20000, 'There must be 1 and only 1 outcome '
          || 'possible for the monitoring process at this job status');
      end;

      t_MonitorCompletionCondition := api.pkg_ColumnQuery.Value(t_MonitorProcessId, 'MonitorCompletionCondition');

      if t_MonitorCompletionCondition is null or t_MonitorCompletionCondition = 'Y' then
        /* Complete this last process */
        api.pkg_ProcessUpdate.Complete(t_MonitorProcessId, t_Outcome);
      end if;
    end if;

    toolbox.pkg_Util.TraceMethodExit;
  end;


  /*--------------------------------------------------------------------------
   * MonitorProcesses() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure MonitorProcesses (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date,
    a_Outcome                   varchar2
  ) is
    t_ProcessNames              varchar2(2000);
    t_Count                     number(9);
    t_Outcome                   api.ProcessOutcomes.Outcome%TYPE;
    t_MonitorProcessId          udt_Id;
    t_MonitorProcessTypeId      udt_Id;
    t_MonitorProcessName        api.Processes.Description%TYPE;
    t_MonitorProcessViewName    api.ObjectDefs.Name%type;
    t_MonitorProcessDescription api.ObjectDefs.Description%type;
    t_ProcessTypeId             udt_Id;
    t_JobId                     udt_Id;
    t_JobTypeId                 udt_Id;
    t_JobStatus                 api.Jobs.JobStatus%TYPE;
    t_MonitorCompletionCondition varchar2(1);

    cursor c_Uncompleted (
      a_JobId                     udt_Id,
      a_MonitorProcessViewName    api.ObjectDefs.Name%type,
      a_MonitorProcessDescription api.ObjectDefs.Description%type default null
    ) is
      select t.Description, count(*) count
        from api.Processes p
        join api.ProcessTypes t
          on p.ProcessTypeId = t.ProcessTypeId
       where JobId = a_JobId
         and DateCompleted is null
         and api.pkg_ColumnQuery.Value(ProcessId, 'MonitorProcessName',
             sysdate) in (a_MonitorProcessViewName,
             a_MonitorProcessDescription)
       group by t.Description;

  begin
    toolbox.pkg_Util.TraceMethodEntry(gc_Package || 'MonitorProcesses',
                         'a_ProcessId',   a_ProcessId,
                         'a_AsOfDate',    toolbox.pkg_Util.FormatValue(a_AsOfDate),
                         'a_Outcome',     a_Outcome);

     /* If this process isn't complete yet, we're done */
    if a_Outcome is null then
      toolbox.pkg_Util.TraceMethodExit;
      return;
    end if;

    /* Get JobId, JobTypeId, jobstatus, ProcessTypeId, monitorprocessname */
    begin
      select b.JobId,
             b.JobTypeId,
             b.JobStatus,
             a.ProcessTypeId,
             api.pkg_ColumnQuery.Value(a.ProcessId, 'MonitorProcessName',
                sysdate)
        into t_JobId, t_JobTypeId, t_JobStatus, t_ProcessTypeId,
             t_MonitorProcessName
        from api.Processes a
        join api.Jobs b
          on a.JobId = b.JobId
       where a.ProcessId = a_ProcessId;
    exception when no_data_found then
      toolbox.pkg_Util.RaiseError(-20000,
          'Process or related job does not exist');
    end;

    /* If there is no MonitorProcessName on this process, then this must BE
       the monitor */
    if t_MonitorProcessName is null then
      /* get the name of this process type */
      select Description,
             Name
        into t_MonitorProcessDescription,
             t_MonitorProcessViewName
        from api.Processtypes
       where ProcessTypeId = t_ProcessTypeId;

    /* check for incomplete processes being monitored by this one */
      select count(*)
        into t_Count
        from api.Processes
       where JobId = t_JobId
         and DateCompleted is null
         and api.pkg_ColumnQuery.Value(ProcessId, 'MonitorProcessName',
             sysdate) in (t_MonitorProcessViewName,
             t_MonitorProcessDescription);

      if t_Count > 0 then
        t_ProcessNames := '' ;
        for c in c_Uncompleted (t_JobId, t_MonitorProcessViewName,
         t_MonitorProcessDescription) loop
          t_ProcessNames := t_ProcessNames || ', ' || ltrim(c.Description);
          if c.count > 1 then
            t_ProcessNames := t_ProcessNames || '(x' || to_char(c.count)
              || ')';
          end if;
        end loop;

        toolbox.pkg_Util.RaiseError(-20000,
          'Cannot complete this process until the following processes '
          || 'are complete: ' || ltrim(t_ProcessNames, ', '));
      end if;
      /* if we got to here, we are allowed to simply continue without any
         further processing as the monitoring process is already
         complete - and that's OK */

      toolbox.pkg_Util.TraceMethodExit;
      return;
    end if;

    /* Get the ProcessTypeId for the named monitoring process */
    begin
      select ProcessTypeId
        into t_MonitorProcessTypeId
        from api.ProcessTypes
       where Description = t_MonitorProcessName
          or Name = t_MonitorProcessName;
    exception when no_data_found then
      toolbox.pkg_Util.RaiseError(-20000, 'Process of type: ' || t_MonitorProcessName
        || ' does not exist.');
    end;

    /* get the ProcessId for the monitoring process.  If no incomplete
       monitoring process exists, then simply exit the procedure without
       an error. */
    begin
      select ProcessId
        into t_MonitorProcessId
        from api.Processes
       where JobId = t_JobId
         and ProcessTypeId = t_MonitorProcessTypeId
         and DateCompleted is null;
    exception
      when no_data_found then

        toolbox.pkg_Util.TraceMethodExit;
        return;
      when others then
        toolbox.pkg_Util.RaiseError(-20000, 'Problem found (possibly multiple?) '
          || 'with monitoring process.');
    end;

    /* Check to see if there are any OTHER uncompleted processes in this job
       which reference the monitoring process */
    select count(*)
      into t_Count
      from api.Processes
     where JobId = t_JobId
       and DateCompleted is null
       and api.pkg_ColumnQuery.Value(ProcessId, 'MonitorProcessName',
           sysdate) = t_MonitorProcessName
       and ProcessId <> a_ProcessId;

    /* If there are no incompleted monitored processes in job, automatically
       update monitor */
    if t_Count = 0 then
      /* determine the outcome to put on the process - there should be only
         1 possibility at this from status */
      begin
        select o.Outcome
          into t_Outcome
          from api.ProcessOutcomes o
          join api.StatusChanges s
            on o.ProcessOutcomeId = s.ProcessOutcomeId
          join api.statuses st
            on s.StatusChangeId = st.StatusId
           --and o.ProcessTypeId = a_MonitorProcessTypeId
         where st.Tag = t_JobStatus
           and o.JobTypeId = t_JobTypeId
           and st.Tag = t_JobStatus;

      exception when no_data_found then
        begin
          select o.Outcome
            into t_Outcome
            from api.ProcessOutcomes o
            join api.StatusChanges s
              on o.ProcessOutcomeId = s.ProcessOutcomeId
            join api.statuses st
              on s.StatusChangeId = st.StatusId
             --and o.ProcessTypeId = a_MonitorProcessTypeId  
           where st.Tag = t_JobStatus
             and o.JobTypeId = t_JobTypeId
             and s.FromStatusId is null;
        exception when others then
          toolbox.pkg_Util.RaiseError(-20000, 'There are no outcomes possible for '
            || 'the monitoring process at this job status');
        end;
      when others then
        toolbox.pkg_Util.RaiseError(-20000, 'There must be 1 and only 1 outcome '
          || 'possible for the monitoring process at this job status');
      end;

      t_MonitorCompletionCondition := api.pkg_ColumnQuery.Value(t_MonitorProcessId, 'MonitorCompletionCondition');

      if t_MonitorCompletionCondition is null or t_MonitorCompletionCondition = 'Y' then
        /* Complete this last process */
        api.pkg_ProcessUpdate.Complete(t_MonitorProcessId, t_Outcome);
      end if;
    end if;

    toolbox.pkg_Util.TraceMethodExit;
  end;

end pkg_BCPMonitor;



/

