create or replace package pkg_Email is

  -- Author  : Craig Porter
  -- Created : 04/27/2012
  -- Purpose : Used to call possesendmail from any object

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  -----------------------------------------------------------------------------
  -- SendTestNoteEmail()
  -----------------------------------------------------------------------------
  procedure SendTestNoteEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_TestType                          varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  );

  -----------------------------------------------------------------------------
  -- SendTestEmail()
  -----------------------------------------------------------------------------
  procedure SendTestEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_TestType                          varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  );

  /*--------------------------------------------------------------------------
   * SendInvEmailNotification() -- PUBLIC
   * This notification happens on the Case File and Order.  The Complainants 
   *   for Complaints related to a given Case File who requested notifications 
   *   will get notifications on either a Violation Found or a No Violation 
   *   Found outcome on a Perform Investigation process.
   *------------------------------------------------------------------------*/
  procedure SendInvEmailNotification (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );

  /*--------------------------------------------------------------------------
   * InsertLink() -- PUBLIC
   * Inserts a link for text enclosed in [[ ]] and preserves the text.
   *------------------------------------------------------------------------*/
  function InsertLink (
    a_Body varchar2,
    a_Link varchar2
  )return varchar2;

end pkg_Email;

 
/

create or replace package body pkg_Email is

  -----------------------------------------------------------------------------
  -- SendTestNoteEmail()
  -----------------------------------------------------------------------------
  procedure SendTestNoteEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_TestType                          varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  ) is
    t_Body                   varchar2(32767);

  begin

  /*Make sure To is not empty*/
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'DevelopmentEmailAddresses') is null
    then
       return;
    end if;
    if api.pkg_columnquery.Value(a_ObjectId, 'TriggerNoteEmail') = 'Y' then
      -- Get the Note Text
      begin
      select n.Text into t_Body from api.notes n
        join api.notedefs nd on n.NoteDefId = nd.NoteDefId
        where n.ObjectId = a_ObjectId and nd.Tag = a_TestType;
      exception
        when too_many_rows then
          api.pkg_errors.raiseerror(-20000, 'You have too many Editors.');
        when no_data_found then
          api.pkg_errors.raiseerror(-20000, 'Test email content not found.');
      end;
      -- Make substitutions
      t_body := replace(t_Body, '{EmailHeader}', api.pkg_ColumnQuery.Value(a_ObjectId, 'EmailHeader'));
      t_body := replace(t_Body, '{WelcomeText}', api.pkg_ColumnQuery.Value(a_ObjectId, 'WelcomeText'));

      -- Append a warning to the beginning of the body if this is a non-production environment.
      if api.pkg_columnquery.Value(a_ObjectId, 'IsProduction') = 'N' then
        if Length(t_Body) < 32300 then --leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This Test Email is from a non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This Test Email is from a non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;

      -- Sending the email.
      extension.pkg_sendmail.SendPosseEmail(a_ObjectId , a_AsOfDate, a_FromAddressColName
          ,a_ToAddressColName, '', '', a_SubjectColName,
          t_Body, '', '', a_BodyIsHtml);

    end if;
  end SendTestNoteEmail;

  -----------------------------------------------------------------------------
  -- SendTestEmail()
  -----------------------------------------------------------------------------
  procedure SendTestEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_TestType                          varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  ) is
    t_Body                   varchar2(32767);
  begin

  /*Make sure To is not empty*/
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'DevelopmentEmailAddresses') is null
    then
       return;
    end if;
    if api.pkg_columnquery.Value(a_ObjectId, 'TriggerEmail') = 'Y' then

    if a_TestType = 'REGEML' then
      t_Body := api.pkg_columnquery.Value(a_ObjectId, 'RegEmailPreamble');
      t_Body := t_Body || api.pkg_columnquery.Value(a_ObjectId, 'RegEmailClosing');
    elsif a_TestType = 'PWREML' then
      t_Body := api.pkg_columnquery.Value(a_ObjectId, 'ResetEmailPreamble')
       || api.pkg_columnquery.Value(a_ObjectId, 'ResetEmailClosing');
    end if;
      -- Make substitutions
      t_Body := replace(t_Body, '{EmailHeader}', api.pkg_ColumnQuery.Value(a_ObjectId, 'EmailHeader'));
      t_body := replace(t_Body, '{WelcomeText}', api.pkg_ColumnQuery.Value(a_ObjectId, 'WelcomeText'));

      -- Sending the email.
      extension.pkg_sendmail.SendPosseEmail(a_ObjectId , a_AsOfDate, a_FromAddressColName
          ,a_ToAddressColName, '', '', a_SubjectColName,
          t_Body, '', '', a_BodyIsHtml);

    end if;
  end SendTestEmail;

  /*--------------------------------------------------------------------------
   * SendInvEmailNotification() -- PUBLIC
   * This notification happens on the Case File and Order.  The Complainants 
   *   for Complaints related to a given Case File who requested notifications 
   *   will get notifications on either a Violation Found or a No Violation 
   *   Found outcome on a Perform Investigation process.
   *------------------------------------------------------------------------*/
  procedure SendInvEmailNotification (
    a_ObjectId          udt_Id,  --Process Id
    a_AsOfDate          date
  ) is
  t_SysObjectId         udt_Id;
  t_EmailType           varchar2(6) := 'INPEML';
  t_Body                varchar2(32767);
  t_TemplateBody        varchar2(32767);
  t_IsProd              varchar2(1);
  t_JobId               udt_Id := api.pkg_columnquery.Value(a_ObjectId, 'JobId');
  t_OrderJobDefId       udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('j_LMS_Order');

 -- this cursor finds all of the complaints related to the Case File Job and returns only
 -- those complainants that requested notifications
  cursor c_RelatedComplaints is
    select c.ObjectId, c.FormattedComplainantName, c.ExternalFileNum
      from query.r_ComplaintCaseFile rcc
        join query.j_Lms_Complaint c
          on c.ObjectId = rcc.ComplaintJobId
     where c.SendEmailNotificationOfInspect = 'Y'
       and rcc.CaseFileJobId = t_JobId;
  
  begin
    select o.ObjectId, o.IsProduction into t_SysObjectId, t_IsProd
      from query.o_SystemSettings o
      where o.ObjectDefTypeId = 1
        and rownum = 1;

    -- get the email template from the Admin site (system settings object)
    select n.Text into t_TemplateBody from api.notes n
      join api.notedefs nd on n.NoteDefId = nd.NoteDefId
      where n.ObjectId = t_SysObjectId and nd.Tag = t_EmailType;

    -- we need to deternine if this is a case file job or an order job
    if api.pkg_columnquery.Value(t_JobId, 'objectdefid') = t_OrderJobDefId then
      --this is an order job so get the related case file job
      select cfo.CaseFileJobId 
      into t_JobId
      from query.r_CaseFile_Order cfo 
      where cfo.OrderJobId = t_JobId;
    end if;

    for c in c_RelatedComplaints loop
      -- each time we send the email, ensure we are working with the template
      t_Body := t_TemplateBody;
      -- make substitutions into a copy of the template
      t_Body := replace(t_Body, '{ComplainantName}', c.formattedcomplainantname);
      t_Body := replace(t_Body, '{ComplaintExternalFileNum}', c.externalfilenum);
      t_Body := replace(t_Body, '{CaseFileAddress}', api.pkg_ColumnQuery.Value(t_JobId, 'ParcelAddress'));
      t_Body := replace(t_Body, '{CompletedDate}', 
         to_char(api.pkg_ColumnQuery.DateValue(a_ObjectId, 'DateCompleted'), 'Mon DD, YYYY'));
      t_Body := replace(t_Body, '{Outcome}', api.pkg_ColumnQuery.Value(a_ObjectId, 'Outcome'));

      -- Append a warning to the beginning of the body if this is a non-production environment.
      --Commented out because I don't think we want this for Demo's (Craig Porter -- May 2, 2012)
      /*if t_IsProd = 'N' then
        if Length(t_Body) < 32300 then --leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This Test Email is from a non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This Test Email is from a non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;*/

      -- Sending the email.
      extension.pkg_SendMail.SendPosseEmail(c.objectid, sysdate, 'FromEmailAddress', 'FormattedComplainantEmail',
        null, null, 'EmailSubject', t_Body,
        null, null, 'Y');
    end loop;

  end SendInvEmailNotification;

  /*--------------------------------------------------------------------------
   * InsertLink() -- PUBLIC
   * Inserts a link for text enclosed in [[ ]] and preserves the text.
   *------------------------------------------------------------------------*/
  function InsertLink (
    a_Body varchar2,
    a_Link varchar2
  )
  return varchar2 is
    t_TextBeforeLink       varchar2(32767);
    t_TextAfterLink        varchar2(32767);
    t_LinkText             varchar2(32767);
    t_Final                varchar2(32767);
  begin
    if instr(a_Body, '[[') > 0 and instr(a_Body, ']]') > 0 then
      t_TextBeforeLink := substr(a_Body, 1, instr(a_Body, '[[') - 1);
      t_TextAfterLink := substr(a_Body, instr(a_Body, ']]') + 2);
      t_LinkText := substr(a_Body, instr(a_Body, '[[') + 2, instr(a_Body, ']]') - instr(a_Body, '[[') - 2);
      t_Final := t_TextBeforeLink || '<a href="' || a_Link || '">' || t_LinkText || '</a>' || t_TextAfterLink;
    return t_Final;
  end if;
  return a_Body;

  end InsertLink;

end pkg_Email;

/

