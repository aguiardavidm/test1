create or replace package pkg_Lookup as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_ID is api.pkg_Definition.udt_ID;
  subtype udt_EndPoint is api.pkg_Definition.udt_EndPoint;
  subtype udt_ObjectList is api.udt_ObjectList;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;


  /*---------------------------------------------------------------------------
   * FindInspectionTypes() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindInspectionTypes (
    a_Object            udt_ID,
    a_RelationshipDef   udt_ID,
    a_EndPoint          udt_EndPoint,
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  );
  
  /**************************************************************************************************
  *  ActiveComplaintTypes()
  ***************************************************************************************************/
  procedure ActiveComplaintTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );
  
 /**************************************************************************************************
  *  ActiveViolationTypes()
  ***************************************************************************************************/
  procedure ActiveViolationTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

end pkg_Lookup;




 
/

grant execute
on pkg_lookup
to posseextensions;

create or replace package body pkg_Lookup as

  /*---------------------------------------------------------------------------
   * FindInspectionTypes() -- PUBLIC
   * This procedure finds the valid Inspection Types for the Request Inspection
   * process.
   *-------------------------------------------------------------------------*/
  procedure FindInspectionTypes (
    a_Object            udt_ID,
    a_RelationshipDef   udt_ID,
    a_EndPoint          udt_EndPoint,
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  ) is

    t_RequestInsTypeReqInsEPId  udt_Id := api.pkg_configquery.EndPointIdForName('o_RequestInspectionType', 'RequestInspection');
    t_JobTypeId                 udt_Id;
--    t_JobId                     udt_Id;
--    t_Trade                     varchar2(50);

  begin

    a_Objects := api.udt_ObjectList();

      select j.JobTypeId--, JobId
        into t_JobTypeId--, t_JobId
        from api.relationships r
        join api.processes p on p.processid = r.toobjectid
        join api.jobs j on j.jobid = p.jobid
       where r.fromobjectid = a_Object
         and r.EndPointId = t_RequestInsTypeReqInsEPId;

    if t_JobTypeId = api.pkg_configquery.ObjectDefIdForName('j_BuildingPermit')
      then
        select --+ CARDINALITY(cs 50)
               api.udt_Object(x.ObjectId)
          bulk collect into a_Objects
          from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_InspectionType', 'AllowOnBuildingPermit', 'Y') as api.udt_ObjectList)) x;

    elsif t_JobTypeId = api.pkg_configquery.ObjectDefIdForName('j_GeneralPermit')
      then
        select --+ CARDINALITY(cs 50)
               api.udt_Object(x.ObjectId)
          bulk collect into a_Objects
          from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_InspectionType', 'AllowOnGeneralPermit', 'Y') as api.udt_ObjectList)) x;

    elsif t_JobTypeId = api.pkg_configquery.ObjectDefIdForName('j_TradePermit')
      then
        select --+ CARDINALITY(cs 50)
               api.udt_Object(x.ObjectId) ObjectId
          bulk collect into a_Objects
          from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_InspectionType', 'AllowOnTradePermit', 'Y') as api.udt_ObjectList)) x;
    end if;
  end;

  /**************************************************************************************************
  *  ActiveComplaintTypes()
  ***************************************************************************************************/
  procedure ActiveComplaintTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_LMS_ComplaintType
     where Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveComplaintTypes;


 /**************************************************************************************************
  *  ActiveViolationTypes()
  ***************************************************************************************************/
  procedure ActiveViolationTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_ViolationType
     where Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveViolationTypes;
end pkg_Lookup;

/

