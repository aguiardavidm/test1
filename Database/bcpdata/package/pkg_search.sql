create or replace package pkg_Search is

  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  type udt_Cursor is ref cursor;

  g_AddSearchMethod        constant pls_integer := 10;
  g_RestrictSearchMethod   constant pls_integer := 11;

  procedure InitializeSearch (
    a_ObjectDefName             varchar2,
    a_ReportMode                boolean default false
  );

  procedure AddStringIndexSearchPath (
    a_SearchMethod              pls_integer,
    a_ColumnName                varchar2,
    a_IndexValue                varchar2,
    a_PerformWildCardSearch     boolean     default false,
    a_MinValueLength            pls_integer default null
  );

  procedure AddNumberIndexSearchPath (
    a_SearchMethod              pls_integer,
    a_ColumnName                varchar2,
    a_LowIndexValue             number,
    a_HighIndexValue            number
  );

  procedure AddDateIndexSearchPath (
    a_SearchMethod              pls_integer,
    a_ColumnName                varchar2,
    a_LowIndexValue             date,
    a_HighIndexValue            date
  );

  procedure AddRelationshipSearchPath (
    a_SearchMethod              pls_integer,
    a_RelatedObjectId           udt_Id,
    a_RelationshipName          varchar2
  );

  procedure AddLookupSearchPath (
    a_SearchMethod              pls_integer,
    a_RelationshipName          varchar2,
    a_ColumnName                varchar2,
    a_SearchValue               varchar2,
    a_PerformWildCardSearch     boolean     default false,
    a_MinValueLength            pls_integer default null
  );

  procedure PerformSearch (
    a_ObjectList            out udt_IdList
  );

  procedure PerformSearch (
    a_Objects               out api.udt_ObjectList
  );

  procedure PerformSearch (
    a_Cursor                out udt_Cursor
  );

  function ObjectListByIndex (
    a_ObjectDefName             varchar2,
    a_ColumnName                varchar2,
    a_LowValue                  varchar2,
    a_HighValue                 varchar2 default null,
    a_AsOfDate                  varchar2 default null
  ) return api.udt_ObjectList;
  
   procedure WebFindAddress (
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_ExactMatch        varchar2 default null,
    a_GISKeys           varchar2 default null,
    a_AddressObjects    out api.udt_ObjectList 
  );
  
  procedure WebFindAddressLookup (
    a_Address           varchar2,
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_ExactMatch        varchar2 default null,
    a_GISKeys           varchar2 default null,
    a_AddressObjects    out api.udt_ObjectList 
  );

   procedure MapSelectAddressSearch (
    a_GISKeys           varchar2 default null,
    a_AddressObjects    out api.udt_ObjectList 
  );

   procedure MapSelectParcelSearch (
    a_GISKeys           varchar2 default null,
    a_ParcelObjects     out api.udt_ObjectList 
  );

   procedure MapSelectPermitSearch (
    a_GISKeys           varchar2 default null,
    a_Jobs              out api.udt_ObjectList 
  );

   procedure MapSelectPlanningSearch (
    a_GISKeys           varchar2 default null,
    a_Jobs              out api.udt_ObjectList 
  );

   procedure MapSelectCodeEnfSearch (
    a_GISKeys           varchar2 default null,
    a_Jobs              out api.udt_ObjectList 
  );
  
  -----------------------------------------------------------------------------
  -- UsersSearch
  --  Search for Internal Users.
  -----------------------------------------------------------------------------
  procedure UserSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_OfficeId                          udt_Id,
    a_Active                            char,
    a_UserType                          varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  );
  
  -----------------------------------------------------------------------------
  -- DefendantSearch
  --  Search for a Defendant.
  -----------------------------------------------------------------------------
  procedure DefendantSearch(
    a_Name                              varchar2,
    a_PhoneNumber                       varchar2,
    a_Address                           varchar2,
    a_StateLicenseNumber                varchar2,
    a_Keyword                           varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  );
  
  -----------------------------------------------------------------------------
  -- ContractorSearch
  --  Search for a Contractor.
  -----------------------------------------------------------------------------
  procedure ContractorSearch(
    a_Name                              varchar2,
    a_PhoneNumber                       varchar2,
    a_Address                           varchar2,
    a_StateLicenseNumber                varchar2,
    a_Keyword                           varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  );
  
  -----------------------------------------------------------------------------
  -- Owner Search
  --  Search for an Owner.
  -----------------------------------------------------------------------------
  procedure OwnerSearch(
    a_Name                              varchar2,
    a_PhoneNumber                       varchar2,
    a_Address                           varchar2,
    a_Keyword                           varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  );
  
  -----------------------------------------------------------------------------
  -- PersonSearch
  --  Search for a Person.
  -----------------------------------------------------------------------------
  procedure PersonSearch(
    a_Name                              varchar2,
    a_PhoneNumber                       varchar2,
    a_Address                           varchar2,
    a_Keyword                           varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  );
  
end pkg_Search;



 
/

create or replace package body pkg_Search is

  type udt_StringIndexSearchPath is record (
    ColumnName                  varchar2(60),
    IndexValue                  varchar2(4000),
    PerformWildCardSearch       boolean,
    MinValueLength              pls_integer
  );

  type udt_NumberIndexSearchPath is record (
    ColumnName                  varchar2(30),
    LowIndexValue               number,
    HighIndexValue              number
  );

  type udt_DateIndexSearchPath is record (
    ColumnName                  varchar2(30),
    LowIndexValue               date,
    HighIndexValue              date
  );

  type udt_RelationshipSearchPath is record (
    RelationshipNameId          udt_Id,
    RelatedObjectId             udt_Id
  );

  type udt_LookupSearchPath is record (
    RelationshipNameId          udt_Id,
    ColumnName                  varchar2(30),
    SearchValue                 varchar2(4000),
    PerformWildCardSearch       boolean,
    MinValueLength              pls_integer
  );

  type udt_SearchPath is record (
    SearchType                  pls_integer,
    SearchPathIndex             number,
    SearchMethod                pls_integer
  );

  type udt_StringIndexSearchPathList is table of udt_StringIndexSearchPath
  index by binary_integer;

  type udt_NumberIndexSearchPathList is table of udt_NumberIndexSearchPath
  index by binary_integer;

  type udt_DateIndexSearchPathList is table of udt_DateIndexSearchPath
  index by binary_integer;

  type udt_RelationshipSearchPathList is table of udt_RelationshipSearchPath
  index by binary_integer;

  type udt_LookupSearchPathList is table of udt_LookupSearchPath
  index by binary_integer;

  type udt_SearchPathList is table of udt_SearchPath
  index by binary_integer;

  type udt_IntegerList is table of pls_integer
  index by binary_integer;
  
  subtype udt_StringList is api.pkg_Definition.udt_StringList; 

  g_StringIndexSearchPath  constant pls_integer := 1;
  g_NumberIndexSearchPath  constant pls_integer := 2;
  g_DateIndexSearchPath    constant pls_integer := 3;
  g_RelationshipSearchPath constant pls_integer := 4;
  g_LookupSearchPath       constant pls_integer := 5;

  g_StringIndexSearchPaths      udt_StringIndexSearchPathList;
  g_NumberIndexSearchPaths      udt_NumberIndexSearchPathList;
  g_DateIndexSearchPaths        udt_DateIndexSearchPathList;
  g_RelationshipSearchPaths     udt_RelationshipSearchPathList;
  g_LookupSearchPaths           udt_LookupSearchPathList;

  g_SearchPaths                 udt_SearchPathList;
  g_ObjectDefId                 udt_Id;
  g_ObjectDefTypeId             udt_Id;
  g_ObjectDefName               varchar2(30);
  g_ObjectIds                   udt_IdList;
  g_ReportMode                  boolean;

  procedure AddSearchPath (
    a_SearchType                pls_integer,
    a_SearchPathIndex           number,
    a_SearchMethod              pls_integer
  ) is
  begin
    g_SearchPaths(g_SearchPaths.count + 1).SearchType := a_SearchType;
    g_SearchPaths(g_SearchPaths.count).SearchPathIndex := a_SearchPathIndex;
    g_SearchPaths(g_SearchPaths.count).SearchMethod := a_SearchMethod;
  end;

  procedure AddStringIndexSearchPath (
    a_SearchMethod              pls_integer,
    a_ColumnName                varchar2,
    a_IndexValue                varchar2,
    a_PerformWildCardSearch     boolean     default false,
    a_MinValueLength            pls_integer default null
  ) is
  begin
    -- If in report mode, ignore strings that are only spaces
    if a_IndexValue is not null and (trim(a_IndexValue) is not null or not g_ReportMode) then
      g_StringIndexSearchPaths(g_StringIndexSearchPaths.count + 1).ColumnName := a_ColumnName;
      g_StringIndexSearchPaths(g_StringIndexSearchPaths.count).IndexValue := a_IndexValue;
      g_StringIndexSearchPaths(g_StringIndexSearchPaths.count).PerformWildCardSearch := a_PerformWildCardSearch;
      g_StringIndexSearchPaths(g_StringIndexSearchPaths.count).MinValueLength := a_MinValueLength;
      AddSearchPath(g_StringIndexSearchPath, g_StringIndexSearchPaths.count, a_SearchMethod);
    end if;
  end;

  procedure AddNumberIndexSearchPath (
    a_SearchMethod              pls_integer,
    a_ColumnName                varchar2,
    a_LowIndexValue             number,
    a_HighIndexValue            number
  ) is
  begin
    if a_LowIndexValue is not null or a_HighIndexValue is not null then
      g_NumberIndexSearchPaths(g_NumberIndexSearchPaths.count + 1).ColumnName := a_ColumnName;
      g_NumberIndexSearchPaths(g_NumberIndexSearchPaths.count).LowIndexValue := a_LowIndexValue;
      g_NumberIndexSearchPaths(g_NumberIndexSearchPaths.count).HighIndexValue := a_HighIndexValue;
      AddSearchPath(g_NumberIndexSearchPath, g_NumberIndexSearchPaths.count, a_SearchMethod);
    end if;
  end;

  procedure AddDateIndexSearchPath (
    a_SearchMethod              pls_integer,
    a_ColumnName                varchar2,
    a_LowIndexValue             date,
    a_HighIndexValue            date
  ) is
    t_LowIndexValue             date;
    t_HighIndexValue            date;
  begin
    -- If in report mode, ignore Jan 01, 1900 dates
    if g_ReportMode then
      if trunc(a_LowIndexValue) = to_date('01-01-1900', 'MM-DD-YYYY') then
        t_LowIndexValue := null;
      else
        t_LowIndexValue := a_LowIndexValue;
      end if;

      if trunc(a_HighIndexValue) = to_date('01-01-1900', 'MM-DD-YYYY') then
        t_HighIndexValue := null;
      else
        t_HighIndexValue := a_HighIndexValue;
      end if;
    else
      t_LowIndexValue := a_LowIndexValue;
      t_HighIndexValue := a_HighIndexValue;
    end if;

    if t_LowIndexValue is null then
      t_LowIndexValue := to_date('01-01-1900', 'MM-DD-YYYY');
    end if;

    if t_HighIndexValue is null then
      t_HighIndexValue := to_date('12-31-3999', 'MM-DD-YYYY');
    end if;

    if t_LowIndexValue is not null or t_HighIndexValue is not null then
      g_DateIndexSearchPaths(g_DateIndexSearchPaths.count + 1).ColumnName := a_ColumnName;
      g_DateIndexSearchPaths(g_DateIndexSearchPaths.count).LowIndexValue := trunc(t_LowIndexValue);
      g_DateIndexSearchPaths(g_DateIndexSearchPaths.count).HighIndexValue := trunc(t_HighIndexValue);
      AddSearchPath(g_DateIndexSearchPath, g_DateIndexSearchPaths.count, a_SearchMethod);
    end if;
  end;

  procedure AddRelationshipSearchPath (
    a_SearchMethod              pls_integer,
    a_RelatedObjectId           udt_Id,
    a_RelationshipNameId        udt_Id
  ) is
  begin
    -- If in report mode, ignore 0 values
    if a_RelatedObjectId is not null and (a_RelatedObjectId <> 0 or not g_ReportMode) then
      g_RelationshipSearchPaths(g_RelationshipSearchPaths.count + 1).RelationshipNameId := a_RelationshipNameId;
      g_RelationshipSearchPaths(g_RelationshipSearchPaths.count).RelatedObjectId := a_RelatedObjectId;
      AddSearchPath(g_RelationshipSearchPath, g_RelationshipSearchPaths.count, a_SearchMethod);
    end if;
  end;

  function GetRelationshipNameId (
    a_RelationshipName          varchar2
  ) return udt_Id is
    t_RelationshipNameId        udt_Id;
  begin
    begin
      select RelationshipNameId
        into t_RelationshipNameId
        from api.RelationshipNames
       where RelationshipName = a_RelationshipName;
    exception when no_data_found then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('RelationshipName', a_RelationshipName);
      api.pkg_Errors.RaiseError(-20000, 'Invalid relationship name: {RelationshipName} specified when preparing search.');
    end;

    return t_RelationshipNameId;
  end;

  procedure AddRelationshipSearchPath (
    a_SearchMethod              pls_integer,
    a_RelatedObjectId           udt_Id,
    a_RelationshipName          varchar2
  ) is
  begin
    AddRelationshipSearchPath(a_SearchMethod, a_RelatedObjectId, GetRelationshipNameId(a_RelationshipName));
  end;

  procedure AddLookupSearchPath (
    a_SearchMethod              pls_integer,
    a_RelationshipNameId        udt_Id,
    a_ColumnName                varchar2,
    a_SearchValue               varchar2,
    a_PerformWildCardSearch     boolean     default false,
    a_MinValueLength            pls_integer default null
  ) is
  begin
    -- If in report mode, ignore strings that are only spaces
    if a_SearchValue is not null and (trim(a_SearchValue) is not null or not g_ReportMode) then
      g_LookupSearchPaths(g_LookupSearchPaths.count + 1).RelationshipNameId := a_RelationshipNameId;
      g_LookupSearchPaths(g_LookupSearchPaths.count).ColumnName := a_ColumnName;
      g_LookupSearchPaths(g_LookupSearchPaths.count).SearchValue := a_SearchValue;
      g_LookupSearchPaths(g_LookupSearchPaths.count).PerformWildCardSearch := a_PerformWildCardSearch;
      g_LookupSearchPaths(g_LookupSearchPaths.count).MinValueLength := a_MinValueLength;
      AddSearchPath(g_LookupSearchPath, g_LookupSearchPaths.count, a_SearchMethod);
    end if;
  end;

  procedure AddLookupSearchPath (
    a_SearchMethod              pls_integer,
    a_RelationshipName          varchar2,
    a_ColumnName                varchar2,
    a_SearchValue               varchar2,
    a_PerformWildCardSearch     boolean     default false,
    a_MinValueLength            pls_integer default null
  ) is
  begin
    AddLookupSearchPath(
      a_SearchMethod,
      GetRelationshipNameId(a_RelationshipName),
      a_ColumnName,
      a_SearchValue,
      a_PerformWildCardSearch,
      a_MinValueLength);
  end;

  procedure ClearVariables is
    t_StringIndexSearchPathList  udt_StringIndexSearchPathList;
    t_DateIndexSearchPathList    udt_DateIndexSearchPathList;
    t_NumberIndexSearchPathList  udt_NumberIndexSearchPathList;
    t_RelationshipSearchPathList udt_RelationshipSearchPathList;
    t_SearchPathList             udt_SearchPathList;
    t_IdList                     udt_IdList;
  begin
    g_StringIndexSearchPaths  := t_StringIndexSearchPathList;
    g_NumberIndexSearchPaths  := t_NumberIndexSearchPathList;
    g_DateIndexSearchPaths    := t_DateIndexSearchPathList;
    g_RelationshipSearchPaths := t_RelationshipSearchPathList;
    g_SearchPaths             := t_SearchPathList;
    g_ObjectDefId             := null;
    g_ObjectDefName           := null;
    g_ObjectIds               := t_IdList;
    g_ReportMode              := false;
  end;

  /*--------------------------------------------------------------------------
   * GetIndexDefID() -- Private
   *------------------------------------------------------------------------*/
  function GetIndexDefID (
    a_ObjectDef		udt_ID,
    a_ColumnName	varchar2
  ) return udt_ID is
    t_IndexDefID	udt_ID;
  begin
    select Min(ic.IndexDefId)
      into t_IndexDefId
      from api.IndexDefColumns  ic,
           api.ColumnDefs cd
     where ic.ColumnDefId = cd.ColumnDefId
       and cd.Name = a_ColumnName
       and cd.ObjectDefId = a_ObjectDef;
    if t_IndexDefId is null then
      raise_application_error (-20000, 'No index exists on column: ' || a_ColumnName || '.');
    else
      return t_IndexDefID;
    end if;
  end;

  function SearchByIndex (
    a_ColumnName                varchar2,
    a_LowIndexValue             varchar2,
    a_HighIndexValue            varchar2,
    a_PerformWildCardSearch     boolean,
    a_ObjectDefName             varchar2
  ) return udt_IdList is
  begin

    if a_PerformWildcardSearch then
      return api.pkg_SimpleSearch.ObjectsByIndex(a_ObjectDefName, a_ColumnName, a_LowIndexValue || '%', a_HighIndexValue);
    else
      return api.pkg_SimpleSearch.ObjectsByIndex(a_ObjectDefName, a_ColumnName, a_LowIndexValue, a_HighIndexValue);
    end if;

  end;

  function SearchByStringIndex (
    a_ColumnName                varchar2,
    a_IndexValue                varchar2,
    a_PerformWildCardSearch     boolean,
    a_MinValueLength            pls_integer,
    a_ObjectDefName             varchar2 default null
  ) return udt_IdList is
    t_ResultsList               udt_IdList;
    t_WildCard                  varchar(1);
  begin

    if a_MinValueLength is not null then
      if nvl(length(replace(a_IndexValue,'%','')),0) < a_MinValueLength then
        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('ColumnName', a_ColumnName);
        api.pkg_Errors.RaiseError(-20000, 'Please enter a longer string for {ColumnName}.');
      end if;
    end if;

    if a_PerformWildCardSearch then
      t_WildCard := '%';
    end if;

    if a_ColumnName = 'StatusDescription' and g_ObjectDefTypeId = 2 then
      select JobId
        bulk collect into t_ResultsList
        from api.Jobs jo,
             api.Statuses st
       where jo.StatusId = st.StatusId
         and st.Description like a_IndexValue || t_WildCard;
    elsif a_ColumnName = 'StatusName' and g_ObjectDefTypeId = 2 then
      select JobId
        bulk collect into t_ResultsList
        from api.Jobs jo,
             api.Statuses st
       where jo.StatusId = st.StatusId
         and st.Tag like a_IndexValue || t_WildCard;
    else
      t_ResultsList := SearchByIndex (
        a_ColumnName,
        a_IndexValue,
        a_IndexValue,
        a_PerformWildCardSearch,
        nvl(a_ObjectDefName, g_ObjectDefName)
      );
    end if;

    return t_ResultsList;
  end;

  function SearchByNumberIndex (
    a_ColumnName                varchar2,
    a_LowIndexValue             number,
    a_HighIndexValue            number
  ) return udt_IdList is
  begin
    return SearchByIndex (
      a_ColumnName,
      a_LowIndexValue,
      a_HighIndexValue,
      false,
      g_ObjectDefName
    );
  end;

  function SearchByDateIndex (
    a_ColumnName                varchar2,
    a_LowIndexValue             Date,
    a_HighIndexValue            Date
  ) return udt_IdList is
  begin
    return SearchByIndex (
      a_ColumnName,
      to_char(a_LowIndexValue, api.pkg_Definition.gc_DateFormat),
      to_char(a_HighIndexValue, api.pkg_Definition.gc_DateFormat),
      false,
      g_ObjectDefName
    );
  end;

  function GetRelatedObjects (
    a_RelationshipNameId        udt_Id,
    a_RelatedObjectId           udt_Id,
    a_SourceObjectDefId         udt_Id
  ) return udt_IdList is
    t_EndPointId                udt_Id;
    t_ResultsList               udt_IdList;
  begin
    -- Determine EndPointId
    begin
      select ToEndPointId
        into t_EndPointId
        from api.RelationshipDefs
       where ToObjectDefId = a_SourceObjectDefId
         and FromRelationshipNameId = a_RelationshipNameId;
    exception when no_data_found then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('RelationshipNameId', a_RelationshipNameId);
      api.pkg_Errors.SetArgValue('ObjectDefId', g_ObjectDefId);
      api.pkg_Errors.RaiseError(-20000, 'No relationship found from object def: {ObjectDefId} using relationship name: {RelationshipNameId}');
    end;
    -- Perform search
    t_ResultsList := api.pkg_ObjectQuery.RelatedObjects(a_RelatedObjectId, t_EndPointId);

    return t_ResultsList;
  end;

  function SearchByRelationship (
    a_RelationshipNameId        udt_Id,
    a_RelatedObjectId           udt_Id
  ) return udt_IdList is
  begin
    return GetRelatedObjects(a_RelationshipNameId, a_RelatedObjectId, g_ObjectDefId);
  end;

  function GetLookupCandidates (
    a_RelationshipNameId        udt_Id,
    a_ColumnName                varchar2,
    a_SearchValue               varchar2,
    a_PerformWildCardSearch     boolean,
    a_MinValueLength            pls_integer
  ) return udt_IdList is
    t_ObjectDefName             varchar2(30);
  begin
    begin
      select od.Name
        into t_ObjectDefName
        from api.RelationshipDefs rd,
             api.ObjectDefs od
       where rd.FromObjectDefId = g_ObjectDefId
         and rd.ToRelationshipNameId = a_RelationshipNameId
         and od.ObjectDefId = rd.ToObjectDefId;

    exception when no_data_found then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('RelationshipNameId', a_RelationshipNameId);
      api.pkg_Errors.SetArgValue('ObjectDefId', g_ObjectDefId);
      api.pkg_Errors.RaiseError(-20000, 'No relationship found from object def: {ObjectDefId} using relationship name: {RelationshipNameId}');
    end;

    return SearchByStringIndex(a_ColumnName, a_SearchValue, a_PerformWildCardSearch, a_MinValueLength, t_ObjectDefName);
  end;

  function SearchByLookup (
    a_RelationshipNameId        udt_Id,
    a_ColumnName                varchar2,
    a_SearchValue               varchar2,
    a_PerformWildCardSearch     boolean,
    a_MinValueLength            pls_integer
  ) return udt_IdList is
    t_Candidates                udt_IdList;
    t_ResultsList               udt_IdList;
    t_RelatedObjects            udt_IdList;
  begin
    -- Determine candidates for lookup
    t_Candidates := GetLookupCandidates(a_RelationshipNameId, a_ColumnName, a_SearchValue, a_PerformWildCardSearch, a_MinValueLength);

    -- For each candidate, do a search by relationship
    for i in 1..t_Candidates.count loop
      t_RelatedObjects := GetRelatedObjects(a_RelationshipNameId, t_Candidates(i), g_ObjectDefId);
      for j in 1..t_RelatedObjects.count loop
        t_ResultsList(t_ResultsList.count + 1) := t_RelatedObjects(j);
      end loop;
    end loop;

    return t_ResultsList;
  end;

  procedure FilterByStringIndex (
    a_ColumnName                varchar2,
    a_IndexValue                varchar2,
    a_PerformWildCardSearch     boolean,
    a_MinValueLength            pls_integer
  ) is
    t_ObjectId                  udt_Id;
    t_WildCard                  varchar2(1);
  begin

    if a_MinValueLength is not null then
      if nvl(length(replace(a_IndexValue,'%','')),0) < a_MinValueLength then
        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('ColumnName', a_ColumnName);
        api.pkg_Errors.RaiseError(-20000, 'Please enter a longer string for {ColumnName}.');
      end if;
    end if;

    if a_PerformWildCardSearch then
      t_WildCard := '%';
    end if;

    t_ObjectId := g_ObjectIds.first;
    while t_ObjectId is not null loop
      if lower(api.pkg_ColumnQuery.Value(t_ObjectId, a_ColumnName)) not like lower(a_IndexValue) || t_WildCard then
        g_ObjectIds.delete(t_ObjectId);
      end if;
      t_ObjectId := g_ObjectIds.next(t_ObjectId);
    end loop;

  end;

  procedure FilterByNumberIndex (
    a_ColumnName                varchar2,
    a_LowIndexValue             number,
    a_HighIndexValue            number
  ) is
    t_ObjectId                  udt_Id;
    t_NumberValue               number;
  begin
    t_ObjectId := g_ObjectIds.first;
    while t_ObjectId is not null loop
      t_NumberValue := api.pkg_ColumnQuery.NumericValue(t_ObjectId, a_ColumnName);
      if not ((t_NumberValue >= a_LowIndexValue or a_LowIndexValue is null)
          and (t_NumberValue <= a_HighIndexValue or a_HighIndexValue is null)) then
        g_ObjectIds.delete(t_ObjectId);
      end if;
      t_ObjectId := g_ObjectIds.next(t_ObjectId);
    end loop;
  end;

  procedure FilterByDateIndex (
    a_ColumnName                varchar2,
    a_LowIndexValue             date,
    a_HighIndexValue            date
  ) is
    t_ObjectId                  udt_Id;
    t_DateValue                 date;
  begin
    t_ObjectId := g_ObjectIds.first;
    while t_ObjectId is not null loop
--      raise_application_error(-20000, 'Test ' || a_LowIndexValue || ', ' || a_HighIndexValue || ', ' || t_DateValue);
      t_DateValue := api.pkg_ColumnQuery.DateValue(t_ObjectId, a_ColumnName);
      if not t_DateValue between a_LowIndexValue and a_HighIndexValue then
--      if not ((t_DateValue is not null)
--          and (t_DateValue >= a_LowIndexValue or a_LowIndexValue is null)
--          and (t_DateValue <= a_HighIndexValue or a_HighIndexValue is null)) then
        g_ObjectIds.delete(t_ObjectId);
      end if;
      t_ObjectId := g_ObjectIds.next(t_ObjectId);
    end loop;
  end;

  procedure FilterByRelationship (
    a_RelationshipNameId        udt_Id,
    a_RelatedObjectIds          udt_IdList,
    a_SourceObjectDefId         udt_Id
  ) is
    t_RelatedObjectDefId        udt_Id;
    t_ObjectId                  udt_Id;
    t_ObjectList                udt_IdList;
    t_Found                     boolean := false;
    t_EndPointId                udt_Id;
  begin
    if a_RelatedObjectIds.count = 0 then
      return;
    end if;

    -- For the filter relationship search, perform a related object search
      -- from the target object to the related object and determine whether the related object is included in the results.

    -- Assume that the object def id for all of the objects is the same
    t_RelatedObjectDefId := toolbox.pkg_Util.GetObjectDefId(a_RelatedObjectIds(1));

    -- Determine EndPointId
    begin
      select ToEndPointId
        into t_EndPointId
        from api.RelationshipDefs
       where FromObjectDefId = a_SourceObjectDefId
         and ToObjectDefId = t_RelatedObjectDefId
         and ToRelationshipNameId = a_RelationshipNameId;
    exception when no_data_found then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('RelationshipNameId', a_RelationshipNameId);
      api.pkg_Errors.SetArgValue('RelatedObjectId', a_RelatedObjectIds(1));
      api.pkg_Errors.SetArgValue('SearchObjectDefId', a_SourceObjectDefId);
      api.pkg_Errors.RaiseError(-20000, 'Related Object: {RelatedObjectId} does not have a relationship def defined to object def {SearchObjectDefId} using relationship name: {RelationshipNameId}.');
    end;

    t_ObjectId := g_ObjectIds.first;
    while t_ObjectId is not null loop
      t_Found := false;
      t_ObjectList := api.pkg_ObjectQuery.RelatedObjects(t_ObjectId, t_EndPointId);

      for i in 1..t_ObjectList.count loop
        for j in 1..a_RelatedObjectIds.count loop
          if t_ObjectList(i) = a_RelatedObjectIds(j) then
            t_Found := true;
            exit;
          end if;
        end loop;

        if t_Found then
          exit;
        end if;
      end loop;

      if not t_Found then
        g_ObjectIds.delete(t_ObjectId);
      end if;
      t_ObjectId := g_ObjectIds.next(t_ObjectId);
    end loop;

  end;

  procedure FilterByRelationship (
    a_RelationshipNameId        udt_Id,
    a_RelatedObjectId           udt_Id
  ) is
    t_RelatedObjectIds          udt_IdList;
  begin
    t_RelatedObjectIds(1) := a_RelatedObjectId;
    FilterByRelationship(a_RelationshipNameId, t_RelatedObjectIds, g_ObjectDefId);
  end;

  procedure FilterByLookup (
    a_RelationshipNameId        udt_Id,
    a_ColumnName                varchar2,
    a_SearchValue               varchar2,
    a_PerformWildCardSearch     boolean,
    a_MinValueLength            pls_integer
  ) is
    t_Candidates                udt_IdList;
    t_ResultsList               udt_IdList;
    t_RelatedObjects            udt_IdList;
  begin
    -- Determine candidates for lookup
    t_Candidates := GetLookupCandidates(a_RelationshipNameId, a_ColumnName, a_SearchValue, a_PerformWildCardSearch, a_MinValueLength);

    -- Do a reverse search by relationship for each candidate
    FilterByRelationship(a_RelationshipNameId, t_Candidates, g_ObjectDefId);

  end;

  procedure DoSearch (
    a_SearchPathType            pls_integer,
    a_SearchPathIndex           pls_integer
  ) is
    t_ResultsList               udt_IdList;
  begin
    if a_SearchPathType = g_StringIndexSearchPath then
      t_ResultsList := SearchByStringIndex (
        g_StringIndexSearchPaths(a_SearchPathIndex).ColumnName,
        g_StringIndexSearchPaths(a_SearchPathIndex).IndexValue,
        g_StringIndexSearchPaths(a_SearchPathIndex).PerformWildCardSearch,
        g_StringIndexSearchPaths(a_SearchPathIndex).MinValueLength
      );
    elsif a_SearchPathType = g_NumberIndexSearchPath then
      t_ResultsList := SearchByNumberIndex (
        g_NumberIndexSearchPaths(a_SearchPathIndex).ColumnName,
        g_NumberIndexSearchPaths(a_SearchPathIndex).LowIndexValue,
        g_NumberIndexSearchPaths(a_SearchPathIndex).HighIndexValue
      );
    elsif a_SearchPathType = g_DateIndexSearchPath then
      t_ResultsList := SearchByDateIndex (
        g_DateIndexSearchPaths(a_SearchPathIndex).ColumnName,
        g_DateIndexSearchPaths(a_SearchPathIndex).LowIndexValue,
        g_DateIndexSearchPaths(a_SearchPathIndex).HighIndexValue
      );
    elsif a_SearchPathType = g_RelationshipSearchPath then
      t_ResultsList := SearchByRelationship (
        g_RelationshipSearchPaths(a_SearchPathIndex).RelationshipNameId,
        g_RelationshipSearchPaths(a_SearchPathIndex).RelatedObjectId
      );
    elsif a_SearchPathType = g_LookupSearchPath then
      t_ResultsList := SearchByLookup (
        g_LookupSearchPaths(a_SearchPathIndex).RelationshipNameId,
        g_LookupSearchPaths(a_SearchPathIndex).ColumnName,
        g_LookupSearchPaths(a_SearchPathIndex).SearchValue,
        g_LookupSearchPaths(a_SearchPathIndex).PerformWildCardSearch,
        g_LookupSearchPaths(a_SearchPathIndex).MinValueLength
      );
    end if;

    for i in 1..t_ResultsList.count loop
      g_ObjectIds(t_ResultsList(i)) := t_ResultsList(i);
    end loop;
  end;

  procedure DoFilter (
    a_SearchPathType            pls_integer,
    a_SearchPathIndex           pls_integer
  ) is
  begin
    if a_SearchPathType = g_StringIndexSearchPath then
      FilterByStringIndex(
        g_StringIndexSearchPaths(a_SearchPathIndex).ColumnName,
        g_StringIndexSearchPaths(a_SearchPathIndex).IndexValue,
        g_StringIndexSearchPaths(a_SearchPathIndex).PerformWildCardSearch,
        g_StringIndexSearchPaths(a_SearchPathIndex).MinValueLength
      );
    elsif a_SearchPathType = g_NumberIndexSearchPath then
      FilterByNumberIndex(
        g_NumberIndexSearchPaths(a_SearchPathIndex).ColumnName,
        g_NumberIndexSearchPaths(a_SearchPathIndex).LowIndexValue,
        g_NumberIndexSearchPaths(a_SearchPathIndex).HighIndexValue
      );
    elsif a_SearchPathType = g_DateIndexSearchPath then
      FilterByDateIndex(
        g_DateIndexSearchPaths(a_SearchPathIndex).ColumnName,
        g_DateIndexSearchPaths(a_SearchPathIndex).LowIndexValue,
        g_DateIndexSearchPaths(a_SearchPathIndex).HighIndexValue
      );
    elsif a_SearchPathType = g_RelationshipSearchPath then
      FilterByRelationship(
        g_RelationshipSearchPaths(a_SearchPathIndex).RelationshipNameId,
        g_RelationshipSearchPaths(a_SearchPathIndex).RelatedObjectId
      );
    elsif a_SearchPathType = g_LookupSearchPath then
      FilterByLookup(
        g_LookupSearchPaths(a_SearchPathIndex).RelationshipNameId,
        g_LookupSearchPaths(a_SearchPathIndex).ColumnName,
        g_LookupSearchPaths(a_SearchPathIndex).SearchValue,
        g_LookupSearchPaths(a_SearchPathIndex).PerformWildCardSearch,
        g_LookupSearchPaths(a_SearchPathIndex).MinValueLength
      );
    end if;

  end;

  procedure InitializeSearch (
    a_ObjectDefName             varchar2,
    a_ReportMode                boolean default false
  ) is
  begin
    ClearVariables;
    g_ObjectDefName := a_ObjectDefName;
    g_ObjectDefId := toolbox.pkg_Util.GetIdForName(g_ObjectDefName, true);

    select ObjectDefTypeId
      into g_ObjectDefTypeId
      from api.ObjectDefs od
     where od.ObjectDefId = g_ObjectDefId;

    g_ReportMode := a_ReportMode;
  end;

  procedure PerformSearch is
  begin
    -- Start at the beginning of the search paths - the first path is an actual search, the rest become filters unless the search method is add
    if g_SearchPaths.count = 0 then
      raise_application_error(-20000, 'At least one item of criteria must be specified in order to perform a search.');
    else
      DoSearch(g_SearchPaths(1).SearchType, g_SearchPaths(1).SearchPathIndex);
      if g_SearchPaths.count > 1 then
        for i in 2..g_SearchPaths.count loop
          if g_SearchPaths(i).SearchMethod = g_RestrictSearchMethod then
            DoFilter(g_SearchPaths(i).SearchType, g_SearchPaths(i).SearchPathIndex);
          else
            DoSearch(g_SearchPaths(i).SearchType, g_SearchPaths(i).SearchPathIndex);
          end if;
        end loop;
      end if;
    end if;
  end;

  procedure PerformSearch (
    a_ObjectList            out udt_IdList
  ) is
  begin
    PerformSearch;
    a_ObjectList := g_ObjectIds;
  end;

/*
  create table jkl_MessageLog (Message varchar2(4000)) tablespace mediumdata;

  procedure LogMessage (
    a_Message varchar2
  ) is
    pragma autonomous_transaction;
  begin
    insert into jkl_MessageLog values (a_Message);
    commit;
  end;
*/

  procedure PerformSearch (
    a_Objects               out api.udt_ObjectList
  ) is
    t_ObjectId              udt_Id;
    t_Index                 pls_integer := 0;
  begin
    PerformSearch;

    a_Objects := api.udt_ObjectList();
    a_Objects.extend(g_ObjectIds.count);

    t_ObjectId := g_ObjectIds.first;
    while t_ObjectId is not null loop
      t_Index := t_Index + 1;
      a_Objects(t_Index) := api.udt_Object(t_ObjectId);
      t_ObjectId := g_ObjectIds.next(t_ObjectId);
    end loop;

--    LogMessage('Search performed. Rows returned: ' || g_ObjectIds.count);
  end;

  procedure PerformSearch (
    a_Cursor                out udt_Cursor
  ) is
    t_ObjectList                api.udt_ObjectList;
  begin
    PerformSearch(t_ObjectList);
    open a_Cursor for
      select ObjectId
        from table(cast(t_ObjectList as api.udt_ObjectList));
  end;

  function ObjectListByIndex (
    a_ObjectDefName             varchar2,
    a_ColumnName                varchar2,
    a_LowValue                  varchar2,
    a_HighValue                 varchar2 default null,
    a_AsOfDate                  varchar2 default null
  ) return api.udt_ObjectList is
    t_ObjectIdLIst              udt_IdList;
    t_ReturnList                api.udt_ObjectList;
  begin
    t_ObjectIdList := api.pkg_SimpleSearch.ObjectsByIndex(a_ObjectDefName, a_ColumnName, a_LowValue, a_HighValue, a_AsOfDate);
    toolbox.pkg_Util.ObjectListFromIdList(t_ObjectIdList, t_ReturnList);
    return t_ReturnList;
  end;

  procedure DropDownListOfObjects (
    a_Cursor                out udt_Cursor,
    a_ObjectDefName             varchar2,
    a_IndexedColumnName         varchar2,
    a_DisplayColumnName         varchar2,
    a_NullDisplayValue          varchar2 default null
  ) is
    a_ObjectList                api.udt_ObjectList;
  begin
    InitializeSearch(a_ObjectDefName);
    AddStringIndexSearchPath(g_RestrictSearchMethod, a_IndexedColumnName, '%');
    PerformSearch(a_ObjectList);
    open a_Cursor for
      select to_number(null) ObjectId,
             a_NullDisplayValue DisplayValue
        from dual
       where a_NullDisplayValue is not null
      union all
      select ob.ObjectId,
             api.pkg_ColumnQuery.Value(ob.ObjectId, a_DisplayColumnName) DisplayValue
        from table(cast(a_ObjectList as api.udt_ObjectList)) ob
       order by DisplayValue;
  end;

 /*--------------------------------------------------------------------------
   * WebFindAddress -- PUBLIC
   *   For contractor web address search.
   *------------------------------------------------------------------------*/
  procedure WebFindAddress (
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_ExactMatch        varchar2 default null,
    a_GISKeys           varchar2 default null,
    a_AddressObjects    out api.udt_ObjectList 
  )is
    t_ParcelNumbers     udt_StringList;
    t_ParcelTemp        udt_IdList;
    t_AddressObjectIds  udt_IdList;
    t_MapResultObjects  api.udt_ObjectList;
  begin
    if trim(a_StreetName) is null then
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        if a_GISKeys is null then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        end if;  
      else
        if a_GISKeys is null then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
    end if;
    if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
      null;
    else
      if a_GISKeys is null then
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;
    end if;
    
    extension.pkg_cxproceduralsearch.InitializeSearch('o_Address');
    if a_HouseNumber is not null then
      extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    else
      extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    end if;
    
    a_AddressObjects := api.udt_ObjectList();
    extension.pkg_cxproceduralsearch.PerformSearch(a_AddressObjects);
    
    -- Add the GIS addresses in to the search results. This will make it an additive search
    
    if a_GISKeys is not null then
      t_ParcelNumbers := extension.pkg_Utils.Split(a_GISKeys, ',');       
      for i in 1..t_ParcelNumbers.count loop
        t_ParcelTemp := api.pkg_SimpleSearch.ObjectsByIndex('o_Address', 'TaxParcelNumber', t_ParcelNumbers(i));
        for j in 1..t_ParcelTemp.count loop
          t_AddressObjectIds(t_ParcelTemp(j)) := t_ParcelTemp(j);
pkg_Debug.PutSingleLine('Adding Address ObjectId ' || to_char(t_ParcelTemp(j)));         
        end loop;
      end loop;

      t_MapResultObjects := extension.pkg_Utils.ConvertToObjectList(t_AddressObjectIds);
      extension.pkg_CollectionUtils.Append(a_AddressObjects, t_MapResultObjects);
    end if;
    
  end WebFindAddress;
  
   /*--------------------------------------------------------------------------
   * WebFindAddressLookup -- PUBLIC
   *   For contractor web address search.
   *------------------------------------------------------------------------*/
  procedure WebFindAddressLookup (
    a_Address           varchar2,
    a_StreetName        varchar2,
    a_HouseNumber       number default null,
    a_FromHouseNumber   number default null,
    a_ToHouseNumber     number default null,
    a_Suite             varchar2 default null,
    a_ExactMatch        varchar2 default null,
    a_GISKeys           varchar2 default null,
    a_AddressObjects    out api.udt_ObjectList 
  )is
    t_ParcelNumbers     udt_StringList;
    t_ParcelTemp        udt_IdList;
    t_AddressObjectIds  udt_IdList;
    t_MapResultObjects  api.udt_ObjectList;
  begin
    if a_Address is not null then
      pkg_bcpsearch.FindAddress(a_Address, a_AddressObjects);
      return;
    end if;
    if trim(a_StreetName) is null then
      if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
        if a_GISKeys is null then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a Street Name.');
        end if;  
      else
        if a_GISKeys is null then
          api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #, and a Street Name.');
        end if;
      end if;
    end if;
    if trim(a_HouseNumber) is not null or (trim(a_FromHouseNumber) is not null and trim(a_ToHouseNumber) is not null) then
      null;
    else
      if a_GISKeys is null then
        api.pkg_Errors.RaiseError(-20000, 'Please enter a range of House #s, or an exact House #.');
      end if;
    end if;
    
    extension.pkg_cxproceduralsearch.InitializeSearch('o_Address');
    if a_HouseNumber is not null then
      extension.pkg_cxproceduralsearch.SearchByAddress(a_HouseNumber, a_HouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    else
      extension.pkg_cxproceduralsearch.SearchByAddress(a_FromHouseNumber, a_ToHouseNumber, null, a_StreetName, a_Suite, a_ExactMatch);
    end if;
    
    a_AddressObjects := api.udt_ObjectList();
    extension.pkg_cxproceduralsearch.PerformSearch(a_AddressObjects);
    
    -- Add the GIS addresses in to the search results. This will make it an additive search
    
    if a_GISKeys is not null then
      t_ParcelNumbers := extension.pkg_Utils.Split(a_GISKeys, ',');       
      for i in 1..t_ParcelNumbers.count loop
        t_ParcelTemp := api.pkg_SimpleSearch.ObjectsByIndex('o_Address', 'TaxParcelNumber', t_ParcelNumbers(i));
        for j in 1..t_ParcelTemp.count loop
          t_AddressObjectIds(t_ParcelTemp(j)) := t_ParcelTemp(j);
pkg_Debug.PutSingleLine('Adding Address ObjectId ' || to_char(t_ParcelTemp(j)));         
        end loop;
      end loop;

      t_MapResultObjects := extension.pkg_Utils.ConvertToObjectList(t_AddressObjectIds);
      extension.pkg_CollectionUtils.Append(a_AddressObjects, t_MapResultObjects);
    end if;
    
  end WebFindAddressLookup;
  
 /*--------------------------------------------------------------------------
   * MapSelectAddressSearch -- PUBLIC
   *   For returning a list of Address objects from objects selected on Matthew's nifty map
   *------------------------------------------------------------------------*/
  procedure MapSelectAddressSearch (
    a_GISKeys           varchar2 default null,
    a_AddressObjects    out api.udt_ObjectList 
  )is
    t_ParcelNumbers     udt_StringList;
    t_ParcelTemp        udt_IdList;
    t_AddressObjectIds  udt_IdList;
    t_MapResultObjects  api.udt_ObjectList;
  begin
    if a_GISKeys is not null then
      t_ParcelNumbers := extension.pkg_Utils.Split(a_GISKeys, ',');       
      for i in 1..t_ParcelNumbers.count loop
        t_ParcelTemp := api.pkg_SimpleSearch.ObjectsByIndex('o_Address', 'TaxParcelNumber', t_ParcelNumbers(i));
        for j in 1..t_ParcelTemp.count loop
          t_AddressObjectIds(t_ParcelTemp(j)) := t_ParcelTemp(j);
          --pkg_Debug.PutSingleLine('Adding Address ObjectId ' || to_char(t_ParcelTemp(j)));         
        end loop;
      end loop;

      a_AddressObjects := extension.pkg_Utils.ConvertToObjectList(t_AddressObjectIds);
    else
      api.pkg_Errors.RaiseError(-20000, 'No objects were found to have been selected on the map.');
    end if;
  end MapSelectAddressSearch;
  
 /*--------------------------------------------------------------------------
   * MapSelectParcelSearch -- PUBLIC
   *   For returning a list of Parcel objects from objects selected on Matthew's nifty map
   *------------------------------------------------------------------------*/
  procedure MapSelectParcelSearch (
    a_GISKeys           varchar2 default null,
    a_ParcelObjects    out api.udt_ObjectList 
  )is
    t_ParcelNumbers     udt_StringList;
    t_ParcelTemp        udt_IdList;
    t_ParcelObjectIds   udt_IdList;
    t_MapResultObjects  api.udt_ObjectList;
  begin
    if a_GISKeys is not null then
      t_ParcelNumbers := extension.pkg_Utils.Split(a_GISKeys, ',');       
      for i in 1..t_ParcelNumbers.count loop
        t_ParcelTemp := api.pkg_SimpleSearch.ObjectsByIndex('o_Parcel', 'SPN', t_ParcelNumbers(i));
        for j in 1..t_ParcelTemp.count loop
          t_ParcelObjectIds(t_ParcelTemp(j)) := t_ParcelTemp(j);
          --pkg_Debug.PutSingleLine('Adding Address ObjectId ' || to_char(t_ParcelTemp(j)));         
        end loop;
      end loop;

      a_ParcelObjects := extension.pkg_Utils.ConvertToObjectList(t_ParcelObjectIds);
    else
      api.pkg_Errors.RaiseError(-20000, 'No objects were found to have been selected on the map.');
    end if;
  end MapSelectParcelSearch;
  
 /*--------------------------------------------------------------------------
   * MapSelectPermitSearch -- PUBLIC
   *   For returning a list of all Jobs from objects selected on Matthew's nifty map
   *------------------------------------------------------------------------*/
  procedure MapSelectPermitSearch (
    a_GISKeys           varchar2 default null,
    a_Jobs    out api.udt_ObjectList 
  )is
    t_ParcelNumbers     udt_StringList;
    t_Jobs              udt_IdList;
    t_MapResultObjects  api.udt_ObjectList;
  begin
    --api.pkg_errors.RaiseError (-20000, a_GISKeys);
    if a_GISKeys is not null then
      t_ParcelNumbers := extension.pkg_Utils.Split(a_GISKeys, ',');       
      --api.pkg_errors.RaiseError (-20000, t_ParcelNumbers(1));
      for i in 1..t_ParcelNumbers.count loop
        for a in (Select j.Jobid
          from api.jobs j
          join api.relationships r on r.FromObjectId = j.JobId
          join api.registeredexternalobjects re on re.ObjectId = r.ToObjectId 
          join bcpdata.address a on to_char(a.PosseObjectId) = re.LinkValue
          join api.jobtypes jt on jt.jobtypeid = j.JobTypeId
          where a.taxparcelnumber = t_parcelnumbers(i)
          and jt.name in ('j_BuildingPermit',
                          'j_GeneralPermit',
                          'j_TradePermit')) loop
          t_Jobs(a.Jobid) := a.JobId;
          --pkg_Debug.PutSingleLine('Adding Address ObjectId ' || to_char(t_ParcelTemp(j)));         
        end loop;
      end loop;

      a_Jobs := extension.pkg_Utils.ConvertToObjectList(t_Jobs);
    else
      api.pkg_Errors.RaiseError(-20000, 'No objects were found to have been selected on the map.');
    end if;
  end MapSelectPermitSearch;
  
 /*--------------------------------------------------------------------------
   * MapSelectPlanningSearch -- PUBLIC
   *   For returning a list of all Jobs from objects selected on Matthew's nifty map
   *------------------------------------------------------------------------*/
  procedure MapSelectPlanningSearch (
    a_GISKeys           varchar2 default null,
    a_Jobs    out api.udt_ObjectList 
  )is
    t_ParcelNumbers     udt_StringList;
    t_Jobs              udt_IdList;
    t_MapResultObjects  api.udt_ObjectList;
  begin
    if a_GISKeys is not null then
      t_ParcelNumbers := extension.pkg_Utils.Split(a_GISKeys, ',');       
      for i in 1..t_ParcelNumbers.count loop
        for a in (Select j.Jobid
                    from api.jobs j
                    join api.relationships r on r.FromObjectId = j.JobId
                    join query.o_property p1 on p1.ObjectId = r.ToObjectId
                    join api.relationships r2 on r2.FromObjectId = p1.objectid
                    join api.registeredexternalobjects re on re.ObjectId = r2.ToObjectId
                    join bcpdata.parcel pr2 on pr2.state_parcel_no = re.LinkValue
                    join api.jobtypes jt on jt.jobtypeid = j.JobTypeId
                    where pr2.State_Parcel_No = t_ParcelNumbers(i)
                    and jt.name in ('j_Rezoning',
                                    'j_Subdivision',
                                    'j_Presubmittal',
                                    'j_UseBySpecialReview', 
                                    'j_VarianceAndAppeals', 
                                    'j_SiteImprovementPlan')) loop
                    t_Jobs(a.Jobid) := a.JobId;
          --pkg_Debug.PutSingleLine('Adding Address ObjectId ' || to_char(t_ParcelTemp(j)));         
        end loop;
      end loop;

      a_Jobs := extension.pkg_Utils.ConvertToObjectList(t_Jobs);
    else
      api.pkg_Errors.RaiseError(-20000, 'No objects were found to have been selected on the map.');
    end if;
  end MapSelectPlanningSearch;

/*--------------------------------------------------------------------------
   * MapSelectCodeEnfSearch -- PUBLIC
   *   For returning a list of all Jobs from objects selected on Matthew's nifty map
   *------------------------------------------------------------------------*/
  procedure MapSelectCodeEnfSearch (
    a_GISKeys           varchar2 default null,
    a_Jobs    out api.udt_ObjectList 
  )is
    t_ParcelNumbers     udt_StringList;
    t_Jobs              udt_IdList;
    t_MapResultObjects  api.udt_ObjectList;
  begin
    if a_GISKeys is not null then
      t_ParcelNumbers := extension.pkg_Utils.Split(a_GISKeys, ',');       
      for i in 1..t_ParcelNumbers.count loop
        for a in (Select j.Jobid
          from api.jobs j
          join api.relationships r on r.FromObjectId = j.JobId
          join api.registeredexternalobjects re on re.ObjectId = r.ToObjectId 
          join bcpdata.address a on to_char(a.PosseObjectId) = re.LinkValue
          join api.jobtypes jt on jt.jobtypeid = j.JobTypeId
          where a.taxparcelnumber = t_parcelnumbers(i)
          and jt.name in ('j_Complaint',
                          'j_Violation',
                          'j_Investiagtion')) loop
          t_Jobs(a.Jobid) := a.JobId;
          --pkg_Debug.PutSingleLine('Adding Address ObjectId ' || to_char(t_ParcelTemp(j)));         
        end loop;
      end loop;

      a_Jobs := extension.pkg_Utils.ConvertToObjectList(t_Jobs);
    else
      api.pkg_Errors.RaiseError(-20000, 'No objects were found to have been selected on the map.');
    end if;
  end MapSelectCodeEnfSearch;
  
  -----------------------------------------------------------------------------
  -- UsersSearch
  --  Search for Users.
  -----------------------------------------------------------------------------
  procedure UserSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_OfficeId                          udt_Id,
    a_Active                            char,
    a_UserType                          varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
  begin
    if a_FirstName is null and a_LastName is null and a_OfficeId is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    a_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('u_Users');
    extension.pkg_CxProceduralSearch.SearchByIndex('FirstName', a_FirstName, null, true);
    extension.pkg_CxProceduralSearch.SearchByIndex('LastName', a_LastName, null, true);
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex('Office', 'ObjectId', a_OfficeId, null, false);
    extension.pkg_CxProceduralSearch.SearchByIndex('UserType', a_UserType, a_UserType, false);
    if a_Active = 'Y' then
      extension.pkg_CxProceduralSearch.FilterObjects('Active', a_Active, null, False);
    end if;
    extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and');
  end UserSearch;
  
  
  -----------------------------------------------------------------------------
  -- DefendantSearch
  --  Search for a Defendant.
  -----------------------------------------------------------------------------
  procedure DefendantSearch(
    a_Name                              varchar2,
    a_PhoneNumber                       varchar2,
    a_Address                           varchar2,
    a_StateLicenseNumber                varchar2,
    a_Keyword                           varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
  begin
    if a_Name is null and a_PhoneNumber is null and a_Address is null and
        a_StateLicenseNumber is null and a_Keyword is null then
      raise_application_error(-20000, 'You must enter a some search criteria.');
    end if;
    
    a_Objects := api.udt_ObjectList();
    if a_Name is not null or a_PhoneNumber is not null or a_Address is not null or a_StateLicenseNumber is not null then
      extension.pkg_CXProceduralSearch.InitializeSearch('o_Contractor');
      extension.pkg_CXProceduralSearch.SearchByIndex('BusinessName', a_Name, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PrimaryPhoneNumber', a_PhoneNumber, null, false);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedMailingAddress', a_Address, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_AllStateLicenseNumbers', a_StateLicenseNumber, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and');
    
      extension.pkg_CXProceduralSearch.InitializeSearch('o_Owner');
      extension.pkg_CXProceduralSearch.SearchByIndex('OwnerName', a_Name, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PhoneNumber', a_PhoneNumber, null, false);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedAddress', a_Address, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and', 'or');
      
      extension.pkg_CXProceduralSearch.InitializeSearch('o_LMS_Person');
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedName', a_Name, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PhoneNumber', a_PhoneNumber, null, false);
      extension.pkg_CXProceduralSearch.SearchByRelatedIndex('Mailing', 'dup_FormattedDisplay', a_Address, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and', 'or');
    else
      extension.pkg_CXProceduralSearch.InitializeSearch('o_Contractor');
      extension.pkg_CXProceduralSearch.SearchByIndex('BusinessName', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PrimaryPhoneNumber', a_Keyword, null, false);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedMailingAddress', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_AllStateLicenseNumbers', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'or');
    
      extension.pkg_CXProceduralSearch.InitializeSearch('o_Owner');
      extension.pkg_CXProceduralSearch.SearchByIndex('OwnerName', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PhoneNumber', a_Keyword, null, false);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedAddress', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'or', 'or');
      
      extension.pkg_CXProceduralSearch.InitializeSearch('o_LMS_Person');
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedName', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PhoneNumber', a_Keyword, null, false);
      extension.pkg_CXProceduralSearch.SearchByRelatedIndex('Mailing', 'dup_FormattedDisplay', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'or', 'or');
    end if;
  end DefendantSearch;
  
  
  -----------------------------------------------------------------------------
  -- ContractorSearch
  --  Search for a Contractor.
  -----------------------------------------------------------------------------
  procedure ContractorSearch(
    a_Name                              varchar2,
    a_PhoneNumber                       varchar2,
    a_Address                           varchar2,
    a_StateLicenseNumber                varchar2,
    a_Keyword                           varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
  begin
    if a_Name is null and a_PhoneNumber is null and a_Address is null and a_Keyword is null then
      raise_application_error(-20000, 'You must enter a some search criteria.');
    end if;
    
    a_Objects := api.udt_ObjectList();
    if a_Name is not null or a_PhoneNumber is not null or a_Address is not null or a_StateLicenseNumber is not null then
      extension.pkg_CXProceduralSearch.InitializeSearch('o_Contractor');
      extension.pkg_CXProceduralSearch.SearchByIndex('BusinessName', a_Name, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PrimaryPhoneNumber', a_PhoneNumber, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedMailingAddress', a_Address, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_AllStateLicenseNumbers', a_StateLicenseNumber, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and');
    else
      extension.pkg_CXProceduralSearch.InitializeSearch('o_Contractor');
      extension.pkg_CXProceduralSearch.SearchByIndex('BusinessName', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PrimaryPhoneNumber', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedMailingAddress', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_AllStateLicenseNumbers', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'or');
    end if;
  end ContractorSearch;
  
  
  -----------------------------------------------------------------------------
  -- OwnerSearch
  --  Search for an Owner.
  -----------------------------------------------------------------------------
  procedure OwnerSearch(
    a_Name                              varchar2,
    a_PhoneNumber                       varchar2,
    a_Address                           varchar2,
    a_Keyword                           varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
  begin
    if a_Name is null and a_PhoneNumber is null and a_Address is null and a_Keyword is null then
      raise_application_error(-20000, 'You must enter a some search criteria.');
    end if;
    
    a_Objects := api.udt_ObjectList();
    if a_Name is not null or a_PhoneNumber is not null or a_Address is not null then
      extension.pkg_CXProceduralSearch.InitializeSearch('o_Owner');
      extension.pkg_CXProceduralSearch.SearchByIndex('OwnerName', a_Name, a_Name, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PhoneNumber', a_PhoneNumber, a_PhoneNumber, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedAddress', a_Address, a_Address, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and'/*, 'or'*/);
    else
      extension.pkg_CXProceduralSearch.InitializeSearch('o_Owner');
      extension.pkg_CXProceduralSearch.SearchByIndex('OwnerName', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PhoneNumber', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedAddress', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects/*, 'or', 'or'*/);
    end if;
  end OwnerSearch;
  
  
  -----------------------------------------------------------------------------
  -- PersonSearch
  --  Search for a Person.
  -----------------------------------------------------------------------------
  procedure PersonSearch(
    a_Name                              varchar2,
    a_PhoneNumber                       varchar2,
    a_Address                           varchar2,
    a_Keyword                           varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
  begin
    if a_Name is null and a_PhoneNumber is null and a_Address is null and a_Keyword is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    
    a_Objects := api.udt_ObjectList();
    if a_Name is not null or a_PhoneNumber is not null or a_Address is not null then
      extension.pkg_CXProceduralSearch.InitializeSearch('o_LMS_Person');
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedName', a_Name, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PhoneNumber', a_PhoneNumber, null, true);
      extension.pkg_CXProceduralSearch.SearchByRelatedIndex('Mailing', 'dup_FormattedDisplay', a_Address, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and'/*, 'or'*/);
    else
      extension.pkg_CXProceduralSearch.InitializeSearch('o_LMS_Person');
      extension.pkg_CXProceduralSearch.SearchByIndex('dup_FormattedName', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByIndex('PhoneNumber', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.SearchByRelatedIndex('Mailing', 'dup_FormattedDisplay', a_Keyword, null, true);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects/*, 'or', 'or'*/);
    end if;
  end PersonSearch;
  
end pkg_Search;

/

