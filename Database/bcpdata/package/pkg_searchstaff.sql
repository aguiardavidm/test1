create or replace package pkg_SearchStaff as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_ID is api.pkg_Definition.udt_ID;
  subtype udt_EndPoint is api.pkg_Definition.udt_EndPoint;
  subtype udt_ObjectList is api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * ListStaffInAccessGroup() -- PUBLIC
   *
   * This procedure is used as a method of finding values for a lookup.  It will
   *   return all of the staff members in the specified access group(s).  It
   *   can be used to filter a drop-down list of staff based on a list of
   *   access groups.
   *
   * How to use:
   *   1. Configure a relationship to staff and a corresponding lookup detail.
   *   2. On the lookup detail configuration, specify
   *     "POSSEToolbox.pkg_Search.ListStaffInAccessGroup" in the "Procedure"
   *     field under "Search By:"
   *   3. Configure a constant-type column called "ListAccessGroupDescription",
   *     bound to a comma-delimited list of the access group descriptions of
   *     the access groups that you want to list staff for.
   *-------------------------------------------------------------------------*/
  procedure ListStaffInAccessGroup (
    a_Object            udt_ID,
    a_RelationshipDef   udt_ID,
    a_EndPoint          udt_EndPoint,
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * ListInspectors() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ListInspectors (
    a_Object            udt_ID,
    a_RelationshipDef   udt_ID,
    a_EndPoint          udt_EndPoint,
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  );

end pkg_SearchStaff;


 

/

create or replace package body pkg_SearchStaff as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_StaffMember is api.pkg_Definition.udt_StaffMember;
  gc_Package               constant varchar2(50) := 'toolbox.pkg_Search.';

  /*---------------------------------------------------------------------------
   * FindCalculatedValue() -- PRIVATE
   *-------------------------------------------------------------------------*/
  function FindCalculatedValue (
    a_ObjectDef         udt_ID,
    a_ColumnName        varchar2
  ) return varchar2 is
    t_Value             varchar2(4000);
    t_ColumnDefID       udt_ID;
  begin
    select ColumnDefID
      into t_ColumnDefID
      from api.ColumnDefs
     where ObjectDefID = a_ObjectDef
       and Name = a_ColumnName;

    select BoundConstantValue
      into t_Value
      from objmodelphys.CalculatedColumnBindings
     where ColumnDefID = t_ColumnDefID;

    return t_Value;

  exception when others then
    return null;
  end;

  /*---------------------------------------------------------------------------
   * ListStaffInAccessGroup() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ListStaffInAccessGroup (
    a_Object            udt_ID,
    a_RelationshipDef   udt_ID,
    a_EndPoint          udt_EndPoint,
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  ) is
    t_AccessGroupDescription   varchar2(2000);
    t_AccessGroupDescriptions  udt_StringList;
    t_ObjectDefID              udt_ID;
    t_AccessGroupID            udt_ID;
  begin

    toolbox.pkg_Util.TraceMethodEntry(gc_Package || 'ListStaffInAccessGroup',
                       'a_Object',            a_Object,
                       'a_RelationshipDef',   a_RelationshipDef,
                       'a_EndPoint',          a_EndPoint,
                       'a_SearchString',      a_SearchString);

    t_AccessGroupDescription := api.pkg_ColumnQuery.Value(a_Object, 'ListAccessGroupDescription');

    select FromObjectDefID
      into t_ObjectDefID
      from api.RelationshipDefs
     where RelationshipDefID = a_RelationshipDef
       and ToEndPoint = a_EndPoint;

    a_Objects := api.udt_ObjectList();
    if t_AccessGroupDescription is null then
      t_AccessGroupDescription := FindCalculatedValue(t_ObjectDefID, 'ListAccessGroupDescription');
    end if;

    if t_AccessGroupDescription is not null then
      t_AccessGroupDescriptions := toolbox.pkg_Parse.ListFromString(t_AccessGroupDescription);

      for i in 1..t_AccessGroupDescriptions.count loop
        begin
          select AccessGroupID
            into t_AccessGroupID
            from api.AccessGroups
           where Description = t_AccessGroupDescriptions(i);

        exception when no_data_found then
          raise_application_error(-20000, 'Invalid Access Group: ' || t_AccessGroupDescriptions(i));
        end;

        for c in (select userid
                    from api.AccessGroupUsers
                   where AccessGroupID = t_AccessGroupID) loop
          a_Objects.extend(1);
          a_Objects(a_Objects.count) := api.udt_Object(c.UserID);
        end loop;
      end loop;
    end if;

    toolbox.pkg_Util.TraceMethodExit;

  end;

  /*---------------------------------------------------------------------------
   * ListInspectors() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ListInspectors (
    a_Object            udt_ID,
    a_RelationshipDef   udt_ID,
    a_EndPoint          udt_EndPoint,
    a_SearchString      varchar2,
    a_Objects       out udt_ObjectList
  ) is
    t_AccessGroupDescription   varchar2(2000);
    t_AccessGroupDescriptions  udt_StringList;
    t_ObjectDefID              udt_ID := api.pkg_configquery.objectdefidforname('o_ReportCriteria');
    t_AccessGroupID            udt_ID;
  begin

/*    toolbox.pkg_Util.TraceMethodEntry(gc_Package || 'ListStaffInAccessGroup',
                       'a_Object',            a_Object,
                       'a_RelationshipDef',   a_RelationshipDef,
                       'a_EndPoint',          a_EndPoint,
                       'a_SearchString',      a_SearchString);*/

    t_AccessGroupDescription := 'Inspector';  --api.pkg_ColumnQuery.Value(a_Object, 'ListAccessGroupDescription');

/*    select FromObjectDefID
      into t_ObjectDefID
      from api.RelationshipDefs
     where RelationshipDefID = a_RelationshipDef
       and ToEndPoint = a_EndPoint;*/

    a_Objects := api.udt_ObjectList();
--    if t_AccessGroupDescription is null then
--      t_AccessGroupDescription := FindCalculatedValue(t_ObjectDefID, 'ListAccessGroupDescription');
--    end if;

    if t_AccessGroupDescription is not null then
      t_AccessGroupDescriptions := toolbox.pkg_Parse.ListFromString(t_AccessGroupDescription);

      for i in 1..t_AccessGroupDescriptions.count loop
        begin
          select AccessGroupID
            into t_AccessGroupID
            from api.AccessGroups
           where Description = t_AccessGroupDescriptions(i);

        exception when no_data_found then
          raise_application_error(-20000, 'Invalid Access Group: ' || t_AccessGroupDescriptions(i));
        end;

        for c in (select userid
                    from api.AccessGroupUsers
                   where AccessGroupID = t_AccessGroupID) loop
          a_Objects.extend(1);
          a_Objects(a_Objects.count) := api.udt_Object(c.UserID);
        end loop;
      end loop;
    end if;

    toolbox.pkg_Util.TraceMethodExit;

  end;

end pkg_SearchStaff;


/

