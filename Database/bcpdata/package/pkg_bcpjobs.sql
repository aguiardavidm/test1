create or replace package PKG_BCPJOBS is
  /*--------------------------------------------------------------------------
   * ALBERT - The ClonePermit procedure in this packages breaks API. DO NOT USE.
   * Need to do more work to determine whether the procedure is used in LMS,
   * in preparation of removal or fixing.
   * I broke the procedure to raise an error.
   *------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * PostPayment() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  PostPayment(
    a_ObjectId             udt_Id,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * AssignProcessToAccessGroup() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  AssignProcessToAccessGroup(
    a_ObjectId          udt_Id,
    a_AsOfDate          date,
    a_ConditionColumn   string,
    a_AccessGroup       string
  );
  
  /*---------------------------------------------------------------------------
   * CreateReviews() -- PUBLIC
   *   Run on constructor of the Review Job to create Review Processes based 
   *   on the Review Matrix object. 
   *-------------------------------------------------------------------------*/
  procedure  CreateReviews(
    a_ObjectId             udt_Id,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * CreateTradeReview() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CreateTradeReview(a_ObjectId    udt_Id,
             	                 a_AsOfDate    date);
                               
  /*---------------------------------------------------------------------------
   * CreateSubsequentReviews() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CreateSubsequentReviews(
    a_ObjectId          udt_Id,
    a_AsOfDate          date,
    a_JobId             udt_Id,
    a_Department        varchar2 
  );                             

  /*---------------------------------------------------------------------------
   * RelateAsParentJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  RelateAsParentJob(
    a_ObjectId          udt_Id,
    a_AsOfDate          date,
    a_ColumnForParentJobId       varchar2
  );

  /*---------------------------------------------------------------------------
   * CreateParentJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CreateParentJob(
    a_ObjectId          udt_Id,
    a_AsOfDate          date,
    a_ParentJobName     varchar2
  );

  /*---------------------------------------------------------------------------
   * CompleteComplaints() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CompleteComplaints(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );
  
  /*---------------------------------------------------------------------------
   * ClonePermit() -- PUBLIC
   * This procedure clones objects and rels from the source permit to the
   * target permit.  It clones what the toolbox misses or what doesn't work
   * because of data model architecture
   *-------------------------------------------------------------------------*/
  procedure  ClonePermit(a_ObjectId          udt_Id,
                         a_AsOfDate          date);
  
end PKG_BCPJOBS;



 
/

create or replace package body PKG_BCPJOBS is

  /*---------------------------------------------------------------------------
   * PostPayment() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  PostPayment(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
  t_JobTypeId           udt_Id;
  t_JobId               udt_Id;
  t_PostedTime          date;
  begin
    t_JobId := toolbox.pkg_Util.GetJobId(a_ObjectId);
    t_JobTypeId := toolbox.pkg_Util.GetObjectDefId(t_JobId);

    select max(to_date(nvl(api.Pkg_ColumnQuery.Value(x.ObjectId, 'PostingTime'), '00:00:00'), 'hh24:mi:ss'))
      into t_PostedTime
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('j_DailyDeposit', 'PostingDate', trunc(api.Pkg_ColumnQuery.DateValue(t_JobId, 'PostingDate'))) as api.udt_ObjectList)) x;

    api.pkg_ColumnUpdate.SetValue(t_JobId, 'PostingTime', to_char(t_PostedTime + 1/1440, 'hh24:mi:ss'));
    pkg_utils.PostPaidTransactions(to_date(api.Pkg_ColumnQuery.Value(t_JobId, 'PostingDateAndTime'), 'dd-mon-yy hh24:mi:ss'), 'MM');
  exception when no_data_found then
    api.pkg_ColumnUpdate.SetValue(t_JobId, 'PostingTime', '00:00:00');
    pkg_utils.PostPaidTransactions(to_date(api.Pkg_ColumnQuery.Value(t_JobId, 'PostingDateAndTime'), 'dd-mon-yy hh24:mi:ss'), 'MM');
  end;

  /*---------------------------------------------------------------------------
   * AssignProcessToAccessGroup() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  AssignProcessToAccessGroup(
    a_ObjectId          udt_Id,
    a_AsOfDate          date,
    a_ConditionColumn   string,
    a_AccessGroup       string
  ) is
    t_Run               varchar(1) default 'N';
    t_JobId             udt_Id;
  begin
    -- Evaluate whether the process is completed
    if api.pkg_ProcessQuery.IsCompleted( a_ObjectId ) = 'Y' then
      return;
    end if;
    -- Evaluate if we even want to run based on a condition on the process
    if (api.pkg_ColumnQuery.Value(a_ObjectId, a_ConditionColumn) = 'Y') then
      t_Run := 'Y';
    else -- Try to fid a condition on the job if the condition on the process returns null
      if(api.pkg_ColumnQuery.Value(a_ObjectId, a_ConditionColumn) is null) then
        select JobId
          into t_JobId
          from api.Processes
         where processId = a_ObjectId;
        if (api.pkg_ColumnQuery.Value(t_JobId, a_ConditionColumn) = 'Y') then
          t_Run := 'Y';
        end if;
      end if;
    end if;

    -- Ok, now if Run = Y then we are good to go
    if (t_Run = 'Y') then
      -- Assign the process to all members in the specified access group
      for c in (select u.oraclelogonid
                  from api.accessgroups ag
                  join api.accessgroupusers agu
                    on agu.accessgroupid = ag.accessgroupid
                  join api.users u
                    on u.userid = agu.userid
                 where ag.description = a_AccessGroup) loop
        api.Pkg_ProcessUpdate.Assign(a_ObjectId, c.OracleLogonId);
      end loop;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * CreateReviews() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CreateReviews(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
  t_JobTypeName         varchar2(30);
  t_ReviewProcessTypeId number := api.pkg_ConfigQuery.ObjectDefIdForName('p_PerformReview');
  t_ProcessId           udt_Id;
  begin
    -- Get Job Type Name for this Job
    select jt.Name
      into t_JobTypeName
      from api.Jobs jo 
      join api.Jobs pjo on pjo.JobId = jo.ParentJobId
      join api.JobTypes jt on jt.JobTypeId = pjo.JobTypeId 
     where jo.JobId = a_ObjectId;
      
    -- Search for Review Matrix Objects with same job type as this one
    for c in (select Department, AssignTo
                from query.o_ReviewMatrix
               where JobTypeName = t_JobTypeName) loop
      -- Create Review Processes for each Review Matrix Object
      t_ProcessId := api.pkg_ProcessUpdate.New(a_ObjectId, t_ReviewProcessTypeId, 'Perform ' || c.Department || ' Review', null, null, null);
      api.Pkg_Columnupdate.SetValue(t_ProcessId, 'Department', c.Department);
      for d in (select us.OracleLogonId
                  from api.AccessGroups ag
                  join api.AccessGroupUsers agu on ag.AccessGroupId = agu.AccessGroupId
                  join api.Users us on us.UserId = agu.UserId
                 where ag.Description = c.AssignTo) loop
pkg_debug.putline('##### t_ProcessId: ' || t_ProcessId || '   d.OracleLogonId: ' || d.OracleLogonId || '    c.AssignTo: ' || c.assignto);
        api.pkg_ProcessUpdate.Assign(t_ProcessId, d.OracleLogonId);
      end loop;           
    end loop;
  end;

  /*---------------------------------------------------------------------------
   * CreateTradeReview() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CreateTradeReview(a_ObjectId    udt_Id,
             	                 a_AsOfDate    date) is

  t_Create                         varchar2(1);
  t_GenerateFeesProcTypeId         udt_Id;
  t_IssueTradePermitProcTypeId     udt_Id;
  t_GenerateFeesNewProcId          udt_Id;
  t_IssueTradePermitNewProcId      udt_Id;
  t_JobId                          udt_Id;
  begin

    t_Create := api.pkg_processquery.JobValue(a_ObjectId, 'CreateReview');
    t_GenerateFeesProcTypeId := api.pkg_configquery.ObjectDefIdForName ('p_GenerateFees');
    t_IssueTradePermitProcTypeId := api.pkg_configquery.ObjectDefIdForName ('p_IssueTradePermit');
     Select p.JobId
       into t_JobId
       from api.processes p
       where p.processid = a_ObjectId;
    
    if t_Create = 'Y'
      then
        
        toolbox.pkg_workflow.CreateSubJob(a_ObjectId, null, 'j_Review');
        
        elsif
        t_create = 'N'
        then 
          t_GenerateFeesNewProcId := api.pkg_processupdate.new(t_JobId, t_GenerateFeesProcTypeId, null, null ,null ,null);
          api.pkg_processupdate.Complete (t_GenerateFeesNewProcId, 'Generated');
          t_IssueTradePermitNewProcId := api.pkg_processupdate.new(t_JobId, t_IssueTradePermitProcTypeId , null, null ,null ,null);
    end if;

  end;

/*---------------------------------------------------------------------------
   * CreateSubsequentReviews() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CreateSubsequentReviews(
    a_ObjectId          udt_Id,
    a_AsOfDate          date,
    a_JobId             udt_Id,
    a_Department        varchar2
  ) is
  t_JobTypeName         varchar2(30);
  t_ReviewProcessTypeId number := api.pkg_ConfigQuery.ObjectDefIdForName('p_PerformReview');
  t_ProcessId           udt_Id;
  t_Department          varchar2(30);
  t_AssignTo            varchar2(30);
  begin
    -- Get Job Type Name for this Job
    select jt.Name
      into t_JobTypeName
      from api.Jobs jo 
      join api.Jobs pjo on pjo.JobId = jo.ParentJobId
      join api.JobTypes jt on jt.JobTypeId = pjo.JobTypeId 
     where jo.JobId = a_JobId;
      
    -- Search for Review Matrix Objects with same job type as this one
        select Department, AssignTo
          into t_Department, t_AssignTo
          from query.o_ReviewMatrix
          where JobTypeName = t_JobTypeName
          and Department = a_Department;
          
      -- Create the Review Process and Assign it.
      t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ReviewProcessTypeId, 'Perform ' || t_Department || ' Review', null, null, null);
      api.Pkg_Columnupdate.SetValue(t_ProcessId, 'Department', t_Department);
      for d in (select us.OracleLogonId
                  from api.AccessGroups ag
                  join api.AccessGroupUsers agu on ag.AccessGroupId = agu.AccessGroupId
                  join api.Users us on us.UserId = agu.UserId
                 where ag.Description = t_AssignTo) loop

        api.pkg_ProcessUpdate.Assign(t_ProcessId, d.OracleLogonId);
      end loop;           
    
  end;

  /*---------------------------------------------------------------------------
   * RelateAsParentJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  RelateAsParentJob(
    a_ObjectId          udt_Id,
    a_AsOfDate          date,
    a_ColumnForParentJobId       varchar2
  ) is
    t_ParentJobId    number;
    t_ChildJobId     number;
    t_EndPointId     number := api.pkg_configquery.EndPointIdForName('p_ComplaintReview', 'Investigation');
  begin

    select jobid
      into t_ChildJobId
      from api.processes p
     where p.processid = a_ObjectId;

  --Get the Related job
--    begin
      select r.toobjectid
        into t_ParentJobId
        from api.relationships r
       where r.fromobjectid = a_ObjectId
         and r.endpointid = t_EndPointId;
--    exception when not_data_found then
--      api.pkg_errors.RaiseError(-20000, 'There is no Investigation job related to this ');
--    end;


  --set the calling job as a sub-job
    api.pkg_jobupdate.Modify(t_ChildJobId, null, null, t_ParentJobId);
  end;


  /*---------------------------------------------------------------------------
   * CreateParentJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CreateParentJob(
    a_ObjectId          udt_Id,
    a_AsOfDate          date,
    a_ParentJobName     varchar2
  ) is
    t_ParentJobDefId number := api.pkg_configquery.ObjectDefIdForName(a_ParentJobName);
    t_ParentJobId    number;
    t_ChildJobId     number;

  begin
  --get the Child Job Id
    select jobid
      into t_ChildJobId
      from api.processes p
     where p.processid = a_ObjectId;

  --create the new job
    t_ParentJobId := api.pkg_jobupdate.new(t_ParentJobDefId, null, null);

  --set the calling job as a sub-job
    api.pkg_jobupdate.Modify(t_ChildJobId, null, null, t_ParentJobId);
  end;


  /*---------------------------------------------------------------------------
   * CompleteComplaints() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CompleteComplaints(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  )is

  t_CompleteInvDefId    udt_Id := api.pkg_configquery.objectdefidforname('p_CompleteInvestigation');
  t_JobId               udt_Id;
  
  begin

    select JobId
      into t_JobId
      from api.processes p
     where p.processid = a_ObjectId;

    for c in (select p.processid
                from api.jobs j
                join api.processes p on p.jobid = j.jobid
                 and p.processtypeid = t_CompleteInvDefId
               where j.parentjobid = t_JobId)loop
      api.pkg_processupdate.Complete(c.ProcessId, 'Complete');
    end loop;
  end;


  /*---------------------------------------------------------------------------
   * ClonePermit() -- PUBLIC
   * This procedure clones objects and rels from the source permit to the
   * target permit.  It clones what the toolbox misses or what doesn't work
   * because of data model architecture.  AWP 9/9/2011
   *-------------------------------------------------------------------------*/
  procedure  ClonePermit(a_ObjectId    udt_Id,
                         a_AsOfDate    date)is

    t_SourceJobId                      udt_Id;
    t_NewObjectId                      udt_Id;
    t_PermitDefId                      udt_Id;
    t_NewRelId                         udt_Id;
    t_NewPermitId                      udt_Id;
    t_PermitEndPointId                 udt_Id;
    t_CopyObjectList                   varchar(4000) := 
        lower(api.pkg_ColumnQuery.Value(a_ObjectId,'IncludeCopyPermitObjects'));
    t_ExcludeRelObjectList             varchar(4000) := 
        lower(api.pkg_ColumnQuery.Value(a_ObjectId,'ExcludeRelatedPermitObjects'));
    t_DestinationObjectDefName         varchar2(100);
    t_CopyPermitColName                varchar2(100);
    t_ObjectDefName                    varchar2(100);
  begin

    select objs.objectdefid, objdefs.name
      into t_PermitDefId, t_ObjectDefName
      from api.objects objs
      join api.objectdefs objdefs on
        objs.objectdefid = objdefs.ObjectDefId
      where objectid = a_ObjectId;
      
    t_SourceJobId := a_ObjectId;

    if t_ObjectDefName = 'j_BuildingPermit' then
      t_CopyPermitColName := 'CopyBuildingPermit';
    else
      t_CopyPermitColName := 'CopyTradePermit';   
    end if;

    
    if api.pkg_ColumnQuery.Value(t_SourceJobId, t_CopyPermitColName) = 'N' then
      return;
    end if;

    -- Set the CopyBuildingPermit to N
    api.pkg_ColumnUpdate.SetValue(t_SourceJobId, t_CopyPermitColName, 'N');

    -- Create a New Building Permit Job
    t_NewPermitId := null;
    extension.pkg_ObjectUpdate.CopyObject(a_ObjectId, t_NewPermitId);
    
    t_PermitEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_PermitDefId, 'SourceCopyDataPermit');

  -- return;
  --loop through the related objects on the source permit (Exclude the SourceCopyDataPermit relationship)
    for a in (select r.ToObjectId,
                     r.EndPointId,
                     defs.toObjectDefId
                from api.Relationships r
                join api.relationshipdefs defs on
                     r.endpointid = defs.toendpointid
                where r.FromObjectId = t_SourceJobId
                and defs.fromobjectdefid = t_PermitDefId
                and defs.FromEndPointId != api.pkg_ConfigQuery.EndPointIdForName(t_PermitDefId, 'SourceCopyDataPermit')
              )loop

      select lower(name)
        into t_DestinationObjectDefName
        from api.objectdefs
        where objectdefid = a.toobjectdefid;

      /*
        If we have an object type in the list, copy the actual record and create the new object
      */  
      if instr(t_CopyObjectList, t_DestinationObjectDefName, 1) > 0 then
      
        --call a procedure that copys that object to a new object and returns the new objectid
        t_NewObjectId := null;
        extension.pkg_ObjectUpdate.CopyObject(a.ToObjectId, t_NewObjectId);

        --relate the new object to the target permit
        t_NewRelId := api.pkg_RelationshipUpdate.New(a.EndPointId, t_NewPermitId, t_NewObjectId);       
      /*
        If we have an object type not in the copy list AND
        a object not in the EXCLUDE list, create a new relationship only
      */ 
      elsif instr(t_ExcludeRelObjectList, t_DestinationObjectDefName, 1) <= 0 then
 
      -- Create the relationship to the existing object 
        t_NewRelId := api.pkg_RelationshipUpdate.New(a.EndPointId, t_NewPermitId, a.toObjectId);

      end if;


    end loop; --a
    
    -- create the relationship to the parent permit
    
    t_NewRelId := api.pkg_RelationshipUpdate.New(t_PermitEndPointId, t_SourceJobId, t_NewPermitId);

  end ClonePermit;

end PKG_BCPJOBS;



/

