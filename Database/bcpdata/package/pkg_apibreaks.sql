create or replace package pkg_APIBreaks is

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;


  /*---------------------------------------------------------------------------
   * GetUserManSchemeId() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetUserManSchemeId (a_SchemeName varchar2) return udt_id;

  /*---------------------------------------------------------------------------
   * GetUserManSchemeId() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetSchemeIdForUserId (a_UserId udt_Id) return udt_id;

  /*---------------------------------------------------------------------------
   * GetUsersInManScheme() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetUsersInManScheme(a_ManagementScheme varchar2) return api.udt_ObjectList;

end pkg_APIBreaks;

 

/

grant execute
on pkg_apibreaks
to abc;

grant execute
on pkg_apibreaks
to extension;

grant execute
on pkg_apibreaks
to posseextensions;

create or replace package body pkg_APIBreaks is

  /*---------------------------------------------------------------------------
   * GetUserManSchemeId() -- PUBLIC
   * Needs 
   * grant select on admin.usermanagementschemes to bcpdata; 
   * grant execute on bcpdata.pkg_apibreaks to posseextensions;
   * grant execute on bcpdata.pkg_apibreaks to extension;
   * to work
   *-------------------------------------------------------------------------*/
  function GetUserManSchemeId (a_SchemeName varchar2) return udt_id is

    t_SchemeId udt_id;

    begin
      select ums.UserManagementSchemeId
        into t_SchemeId
        from admin.usermanagementschemes ums
       where ums.name = a_SchemeName;
    return t_SchemeId;
  end GetUserManSchemeId;


  /*---------------------------------------------------------------------------
   * GetUserManSchemeId() -- PUBLIC
   * Needs 
   * grant select on common.users to bcpdata; 
   * grant execute on bcpdata.pkg_apibreaks to posseextensions;
   * grant execute on bcpdata.pkg_apibreaks to extension;
   * to work
   *-------------------------------------------------------------------------*/
  function GetSchemeIdForUserId (a_UserId udt_Id) return udt_id is

    t_SchemeId udt_id;

    begin
      select u.UserManagementSchemeId
        into t_SchemeId
        from common.users u
       where u.userid = a_UserId;
    return t_SchemeId;

  end GetSchemeIdForUserId;


  /*---------------------------------------------------------------------------
   * GetUsersInManScheme() -- PUBLIC
   * Needs 
   * grant select on common.usermanagementschemes to bcpdata; 
   * grant select on common.users to bcpdata;
   * grant execute on bcpdata.pkg_apibreaks to posseextensions;
   * to work
   *-------------------------------------------------------------------------*/
  function GetUsersInManScheme(a_ManagementScheme varchar2) return api.udt_ObjectList is

    t_UserIds api.udt_ObjectList;

    begin
      t_UserIds := api.udt_ObjectList();

      select api.udt_Object(u.userid)
        bulk collect into t_UserIds
        from common.users u
        join common.usermanagementschemes um on um.UserManagementSchemeId = u.UserManagementSchemeId
         and um.name = a_ManagementScheme;
    return t_UserIds;

  end GetUsersInManScheme;

end pkg_APIBreaks;

/

