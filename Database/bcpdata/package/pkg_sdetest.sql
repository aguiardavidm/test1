create or replace package pkg_SDETest is
function TestSDEConnection
  return varchar2;

procedure Test(a_Test number);
end pkg_SDETest;



 
/

create or replace package body pkg_SDETest is

function TestSDEConnection return varchar2
  as language java
  name 'TestConnection.makeConn() return String';

procedure Test(a_Test number)
  as language java
  name 'TestConnection.Test(int)';
end pkg_SDETest;



/

