create or replace package         PKG_BCPJOBS_RYAN is
  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * CreateReviews() -- PUBLIC
   *   Run on constructor of the Review Job to create Review Processes based 
   *   on the Review Matrix object. 
   *-------------------------------------------------------------------------*/
  procedure  CreateReviews(
    a_ObjectId             udt_Id,
    a_AsOfDate          date
  );

end PKG_BCPJOBS_RYAN;



 
/

create or replace package body         PKG_BCPJOBS_RYAN is

  /*---------------------------------------------------------------------------
   * CreateReviews() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CreateReviews(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
  begin
    null;
    -- Search for Review Matrix Objects with asame job type as this one
    -- Create Review Processes for each Review Matrix Object
  end;

end PKG_BCPJOBS_RYAN;



/

