create or replace package pkg_Conditions_deprecated is

  /*---------------------------------------------------------------------------
   *   Albert - I've deprecated this package as we've reworked how conditions
   * are managed in the system.
   *-------------------------------------------------------------------------*/



  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

/*---------------------------------------------------------------------------
 * CreateConditionToParcelRel() -- PUBLIC
 * When a condition is created from a job this procedure runs to create
 * a stored relationship between the condition and each of the parcel
 * related (indirectly) to the creating job.
 *-------------------------------------------------------------------------*/
  procedure CreateConditionToParcelRel(a_ObjectId  udt_Id,
                                       a_AsOfDate  date);
  
end  pkg_Conditions_deprecated;

 
/

create or replace package body pkg_Conditions_deprecated is

/*---------------------------------------------------------------------------
 * CreateConditionToParcelRel() -- PUBLIC
 * When a condition is created from a job this procedure runs to create
 * a stored relationship between the condition and each of the parcel
 * related (indirectly) to the creating job.
 *-------------------------------------------------------------------------*/
  procedure CreateConditionToParcelRel(a_ObjectId  udt_Id,
                                       a_AsOfDate  date) is
  
  t_NewRelId                    udt_Id;
  t_CreatingJobId               udt_Id;
  t_CreatingJobObjectDefId      udt_Id;
  t_ParcelEndpointId            udt_Id;
  t_ParcelList                  udt_IdList;
  t_ConditionToParcelEndpointId udt_Id;

  begin
    t_ConditionToParcelEndpointId := api.pkg_configquery.EndPointIdForName('o_Condition', 'Parcel');

    begin
      select j.jobid
        into t_CreatingJobId
        from api.relationships r
        join api.jobs j on j.JobId = r.ToObjectId
       where r.FromObjectId = a_ObjectId;

    --If no job found, this must be created from the parcel and this procedure isn't necessary
    exception when no_data_found then
      return;
    end;

    t_CreatingJobObjectDefId := api.pkg_columnquery.NumericValue(t_CreatingJobId, 'ObjectDefId');
    t_ParcelEndpointId := api.pkg_configquery.EndPointIdForName(t_CreatingJobObjectDefId, 'ParcelIND');

    t_ParcelList := api.pkg_objectquery.RelatedObjects(t_CreatingJobId, t_ParcelEndpointId);

    for i in 1..t_ParcelList.count() loop
      t_NewRelId := api.pkg_relationshipupdate.New(t_ConditionToParcelEndpointId, a_ObjectId, t_ParcelList(i));
    end loop;

  end CreateConditionToParcelRel;
  


end pkg_Conditions_deprecated;

/

