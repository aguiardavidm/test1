create or replace package         pkg_CaseFile is

  -- Author  : BRUCE.JAKEWAY
  -- Created : 4/26/2012 4:53:54 PM
  -- Purpose : Case file procedures and functions
  
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_idlist;
  
  -----------------------------------------------------------------------------
  -- CreateAndRelatedOrderJob()
  --  This package creates an Order Job and relates all the selected violations.
  -----------------------------------------------------------------------------
  procedure CreateAndRelateOrderJob (
    a_JobId				     udt_Id,
    a_AsOfDate				 date
  );
  
  -----------------------------------------------------------------------------
  -- CopyProcessInvestigatorToJob()
  --  Copy the Default Investigator to the Proposed Investigator.
  -----------------------------------------------------------------------------
  procedure CopyProcessInvestigatorToJob (
    a_ProcessId        udt_Id,
    a_AsOfDate         date
  );
  
  -----------------------------------------------------------------------------
  -- EnsureUniqueDefendant()
  --  This procedure ensures that there is only one defendant.
  -----------------------------------------------------------------------------
  procedure EnsureUniqueDefendant (
    a_ObjectId         udt_Id,
    a_AsOfDate			   date
  );
  
  -----------------------------------------------------------------------------
  -- CreateAndRelateNewNOV()
  --  Create a new NOV and relate it to the case file job.  This will also grab
  --  all violations not previously included on an NOV and relate those to the 
  --  new NOV
  -----------------------------------------------------------------------------
  procedure CreateAndRelateNewNOV(
    a_JobId            udt_Id,
    a_AsOfDate         date
  );
end pkg_CaseFile;

 
/

create or replace package body         pkg_CaseFile is

  -- Function and procedure implementations
  
  -----------------------------------------------------------------------------
  -- EnsureUniqueDefendant()
  --  This procedure ensures that there is only one defendant.
  -----------------------------------------------------------------------------
  procedure EnsureUniqueDefendant (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
    t_CaseFileJobId                         udt_Id;
    t_RelId                                 udt_Id;
    
  begin
    begin
      select r.FromObjectId
        into t_CaseFileJobId
        from api.relationships r
          join query.j_LMS_CaseFile j
            on j.JobId = r.FromObjectId
        where r.RelationshipId = a_ObjectId;
    exception
      when no_data_found then  -- assuming we're coming from a Case File Job.
        if api.pkg_ColumnQuery.Value(a_ObjectId, 'ParcelOwnerIsDefendant') = 'N' then
          -- When the checkbox ParcelOwnerIsDefendant is being set to no, don't bother verifying what relationships are set
          -- as no defendant relationships should exist.
          return;
        else
          t_CaseFileJobId := a_ObjectId;
        end if;
    end;
     /* DM May 16 
    --this is not necessary at the moment.  I want to use this procedure on the case file job
    --to create the parcel owner as a defendant (checkbox).  But I do not want to remove previous relationships 
    
    -- remove an exising contractor
    begin
      select r.RelationshipId
        into t_RelId
        from query.r_LMS_CaseFileContractor r
        where r.RelationshipId <> a_ObjectId
          and r.CaseFileJobId = t_CaseFileJobId;
        
      api.pkg_RelationshipUpdate.Remove(t_RelId);
    exception
      when no_data_found then
        null;
    end;
   
    -- remove an exising owner
    begin
      select r.RelationshipId
        into t_RelId
        from query.r_LMS_CaseFileOwner r
        where r.RelationshipId <> a_ObjectId
          and r.CaseFileJobId = t_CaseFileJobId;
        
      api.pkg_RelationshipUpdate.Remove(t_RelId);
    exception
      when no_data_found then
        null;
    end;
    
    -- remove an exising person
    begin
      select r.RelationshipId
        into t_RelId
        from query.r_LMS_CaseFilePerson r
        where r.RelationshipId <> a_ObjectId
          and r.CaseFileJobId = t_CaseFileJobId;
        
      api.pkg_RelationshipUpdate.Remove(t_RelId);
    exception
      when no_data_found then
        null;
    end;*/
  end EnsureUniqueDefendant;
  
  
  -----------------------------------------------------------------------------
  -- CreateAndRelatedOrderJob()
  -- This package creates an Order Job and relates all the selected violations with no resolved date
  -- and with a Notice Date and no Order Date.
  -----------------------------------------------------------------------------
  procedure CreateAndRelateOrderJob (
    a_JobId				                udt_Id,
    a_AsOfDate				            date
  ) is
    t_RelId                       udt_Id;
    t_OrderJobId                  udt_Id;
    t_InspectorId                 udt_Id;
    t_OrderViolations             udt_IdList;
    
  begin  
    t_OrderJobId := api.pkg_objectupdate.New(api.pkg_objectdefquery.IdForName('j_LMS_Order'));
    t_RelId := extension.pkg_relationshipupdate.New(a_JobId,t_OrderJobId,'Order');
    t_OrderViolations := extension.pkg_objectquery.RelatedObjects(a_JobId,'OrderJobViolations');
    t_InspectorId := extension.pkg_objectquery.RelatedObject(a_JobId,'Investigator');
    
    if t_InspectorId is not null then
      t_RelId := extension.pkg_relationshipupdate.New(t_OrderJobId,t_InspectorId,'Investigator');
    end if;
    
    for i in 1..t_OrderViolations.count loop
      t_RelId := extension.pkg_relationshipupdate.New(t_OrderJobId,t_OrderViolations(i),'Violations');
    end loop;
  end CreateAndRelateOrderJob;
  
  
  -----------------------------------------------------------------------------
  -- CopyProcessInvestigatorToJob()
  --  Copy the Investigator from the Assign Investigation process
  --  to the Proposed Investigator on the job.  The Perform Investigation process
  --  is then assigned to this Proposed Investigator.
  -----------------------------------------------------------------------------
  procedure CopyProcessInvestigatorToJob (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_InvestigatorUserId                udt_Id;
    t_ProposedInvestigatorUserId        udt_Id;
    t_JobId                             udt_Id := api.pkg_ColumnQuery.NumericValue(a_ProcessId, 'JobId');
    t_RelId                             udt_Id;
  
  begin
    t_ProposedInvestigatorUserId := extension.pkg_ObjectQuery.RelatedObject(t_JobId, 'ProposedInvestigator');
    t_InvestigatorUserId := extension.pkg_ObjectQuery.RelatedObject(a_ProcessId, 'Investigator');
  
    if t_ProposedInvestigatorUserId is not null then
      extension.pkg_RelationshipUpdate.Remove(t_JobId,t_ProposedInvestigatorUserId, 'ProposedInvestigator');
    end if;
    if t_InvestigatorUserId is not null then 
      t_RelId := extension.pkg_RelationshipUpdate.New(t_JobId, t_InvestigatorUserId, 'ProposedInvestigator');
    end if;
  end CopyProcessInvestigatorToJob;
  
  
  -----------------------------------------------------------------------------
  -- CreateAndRelateNewNOV()
  --  Create a new NOV and relate it to the case file job.  This will also grab
  --  all violations not previously included on an NOV and relate those to the 
  --  new NOV
  -----------------------------------------------------------------------------
  procedure CreateAndRelateNewNOV(
    a_JobId                   udt_Id,
    a_AsOfDate                date
  ) is 
    t_NOVObjectDefId          udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('o_LMS_NoticeOfViolation');
    t_NewNOVObjectId          udt_Id;
    t_NOVCFEndpointId         udt_Id := api.pkg_ConfigQuery.EndPointIdForName('j_LMS_CaseFile', 'NoticeOfViolation');
    t_NOVCFRelId              udt_Id; 
    t_ViolNOVEndpointId       udt_Id := api.pkg_configquery.EndPointIdForName('o_LMS_NoticeOfViolation', 'Violations');
    t_ViolNOVRelId            udt_Id;
  
    
    cursor c_ViolationsToAdd is
      select rNC.NoticeOfViolationObjectId, o.ObjectId
        from query.r_NoticeofViolationCaseFile rNC
        join query.r_CaseFileViolations rCV
          on rCV.CaseFileJobId = rNC.CaseFileJobId
        join query.o_violations o
          on o.ObjectId = rCV.ViolationObjectId
       where rNC.RelationshipId = t_NOVCFRelId
         and o.ResolutionDate is null
         and o.NoticeDate is null;
          
  begin
    if (api.Pkg_Columnquery.Value(a_JobId, 'NOVCreated') = 'Y') then
      --create a new Notice of Violation object and relate it to the current job
      t_NewNOVObjectId := api.pkg_ObjectUpdate.New(t_NOVObjectDefId);
      t_NOVCFRelId := api.pkg_RelationshipUpdate.New(t_NOVCFEndpointId, a_JobId, t_NewNOVObjectId);
      
      --create a rel from the violations to the new Notice of Violation
      for v in c_ViolationsToAdd loop
        t_ViolNovRelId := api.pkg_relationshipUpdate.New(t_ViolNOVEndpointId, t_NewNOVObjectId, v.objectid);
      end loop;
    
      api.pkg_columnupdate.SetValue(a_JobId, 'NOVCreated', 'N');
    end if;
  end CreateAndRelateNewNOV;
  
end pkg_CaseFile;

/

