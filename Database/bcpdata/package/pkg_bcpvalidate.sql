create or replace package pkg_BCPValidate as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * ValidateJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure VerifyJob (
    a_ProcessId         udt_Id,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * ValidateIssue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ValidateIssue (
    a_ProcessId         udt_Id,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * SetResponsibleParty() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SetResponsibleParty (
    a_ProcessId         udt_Id,
    a_AsOfDate          date
  );
  
  /*---------------------------------------------------------------------------
   * ValidatePrimaryAddress() - public
   *-------------------------------------------------------------------------*/
  procedure ValidatePrimaryAddress (
    a_ObjectId            udt_Id,
    a_AsOfDate            date,
    a_ObjectDefName       varchar2,
    a_EndPointName        varchar2
  );
end pkg_BCPValidate;



 
/

create or replace package body pkg_BCPValidate as

  /*---------------------------------------------------------------------------
   * ValidateJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure VerifyJob (
    a_ProcessId         udt_Id,
    a_AsOfDate          date
  ) is
  begin
    api.pkg_ObjectUpdate.Verify(toolbox.pkg_Util.GetJobId(a_ProcessId));
  end;

  /*---------------------------------------------------------------------------
   * ValidateIssue() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ValidateIssue (
    a_ProcessId         udt_Id,
    a_AsOfDate          date
  ) is
    t_JobId             udt_Id;
    t_ResponsibleParty  udt_Id;
    t_BondLimitUsed     number(13, 2);
    t_BondLimit         number(13, 2);
    t_AmountToBeCharged number(13, 2);
    procedure SetErrorArguments is
      begin
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('ProcessId', a_ProcessId);
        api.pkg_Errors.SetArgValue('As Of Date', a_AsOfDate);
      end;
  begin
    t_JobId := toolbox.pkg_Util.GetJobId(a_ProcessId);

    begin
      select distinct ResponsibleObjectId
        into t_ResponsibleParty
        from api.Fees fe
       where JobId = t_JobId
         and fe.Amount + fe.TaxAmount + (select nvl(sum(ft.Amount + ft.TaxAmount),0)
                                           from api.FeeTransactions ft
                                          where ft.Feeid = fe.FeeId) > 0;
    exception when too_many_rows then
      SetErrorArguments;
      api.pkg_Errors.RaiseError(-20000, 'Only one responsible party may be on this job.');
              when no_data_found then
      t_ResponsibleParty := null;
    end;

    if t_ResponsibleParty is not null then
      t_BondLimit := nvl(api.pkg_ColumnQuery.NumericValue(t_ResponsibleParty, 'CreditLimit'), 0);

      -- possibly create as a dynamic detail?
      select sum(nvl(Amount, 0) + nvl(TaxAmount, 0) + nvl((select sum(nvl(ft.Amount, 0) + nvl(ft.TaxAmount,0))
                                         from api.FeeTransactions ft
                                        where ft.Feeid = fe.FeeId), 0))
        into t_BondLimitUsed
        from api.Fees fe
       where fe.ResponsibleObjectId = t_ResponsibleParty;

      --t_AmountToBeCharged := api.pkg_JobQuery.Balance(t_JobId);

      if t_BondLimit - t_BondLimitUsed/* - t_AmountToBeCharged*/ < 0 then
        SetErrorArguments;
        api.pkg_Errors.RaiseError(-20000, 'Charging this permit to the contractor''s bond account will exceed contractor''s bond limit by ' || -(t_BondLimit - t_BondLimitUsed/* - t_AmountToBeCharged*/) );
      end if;
    elsif api.pkg_JobQuery.Balance(t_JobId) > 0 then
      SetErrorArguments;
      api.pkg_Errors.RaiseError(-20000, 'Fees must be paid before issuing this permit.');
    end if;
  end;

  /*---------------------------------------------------------------------------
   * SetResponsibleParty() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SetResponsibleParty (
    a_ProcessId         udt_Id,
    a_AsOfDate          date
  ) is
    t_JobId             udt_Id;
    t_ResponsibleParty  udt_Id;
    procedure SetErrorArguments is
      begin
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('ProcessId', a_ProcessId);
        api.pkg_Errors.SetArgValue('As Of Date', a_AsOfDate);
      end;
  begin
    t_JobId := toolbox.pkg_Util.GetJobId(a_ProcessId);

    t_ResponsibleParty := api.pkg_ColumnQuery.Value(t_JobId, 'ApplicantObjectId');

    if t_ResponsibleParty is not null then
      if nvl(api.pkg_ColumnQuery.NumericValue(t_ResponsibleParty, 'CreditLimit'), 0) > 0 then

        for c in (select fe.FeeId,
                         fe.Amount,
                         fe.Description,
                         fe.GLAccountId,
                         fe.TaxRateId
                    from api.Fees fe
                   where fe.JobId = t_JobId) loop
          api.pkg_FeeUpdate.Modify(c.FeeId, c.Amount, c.Description, c.GLAccountId, t_ResponsibleParty, c.TaxRateId);
        end loop;
      end if;
    end if;
  end;
  
  /*---------------------------------------------------------------------------
   * ValidatePrimaryAddress() - public
   * need to ensure only one primary address exists
   *-------------------------------------------------------------------------*/
  procedure ValidatePrimaryAddress (
    a_ObjectId            udt_Id,
    a_AsOfDate            date,
    a_ObjectDefName       varchar2,
    a_EndPointName        varchar2
  ) is
    t_Count                pls_integer;
    t_ObjId          udt_Id;
    t_EndpointId       udt_Id;
    t_PAdrCount             pls_integer;
    t_NeedsPrimaryContact  char(1);
    t_SourceEndPointId     udt_Id;

  begin
 
    t_EndpointId := api.pkg_configquery.EndPointIdForName(a_ObjectDefName,a_EndPointName);
    
    begin
    -- we came from a rel and need to return only one object
       select distinct r.fromobjectid
         into t_ObjId
         from api.RELATIONSHIPS r
         where r.relationshipid = a_ObjectId
           and r.EndPointId = t_EndpointId;
       
     -- if this is the first address rel, set it as primary
     select count(*)
     into t_Count
     from api.relationships 
     where fromobjectid = t_ObjId
     and endpointid = t_EndpointId;
     --api.pkg_errors.RaiseError(-20000, 't_Count: '||t_Count);
     if t_Count = 1 then
       api.pkg_columnupdate.setvalue(a_ObjectId, 'IsPrimaryAddress', 'Y');
     end if;
   exception
   --we came from an object and there is no relationship data
     when no_data_found then
       t_ObjId := a_objectId;
   end;  

   t_NeedsPrimaryContact := nvl(api.pkg_ColumnQuery.Value(t_ObjId, 'NeedsPrimaryAddress'), 'N');

   if t_NeedsPrimaryContact = 'Y' then
     raise_application_error(-20001, 'Must have a single primary address.');
   end if;

 end ValidatePrimaryAddress;
end pkg_BCPValidate;



/

