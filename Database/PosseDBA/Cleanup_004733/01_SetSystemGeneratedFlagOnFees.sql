/* Set's system generated flag on all non-manual fees
 * RUNAS: POSSEDBA
 */
declare
  t_GLAccountId number;
begin
  select max(g.GLAccountId)
    into t_GLAccountId
    from api.glaccounts g
   where upper(g.Description) = 'MISC. FEE';
   
   update finance.transactions a
      set a.SystemGenerated = 'Y'
    where a.feeid in
        ( select distinct b.TransactionId
            from finance.fees b
            where b.GLAccountId <> t_GLAccountId )
      and a.SystemGenerated = 'N';

   update finance.transactions a
      set a.SystemGenerated = 'Y'
    where a.TransactionId in
        ( select distinct b.TransactionId
            from finance.fees b
            where b.GLAccountId <> t_GLAccountId )
      and a.SystemGenerated = 'N';
end;
/
