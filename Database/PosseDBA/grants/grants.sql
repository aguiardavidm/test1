grant select, update on finance.transactions to feescheduleplus;
grant select on stage.ResponsibleObjects to feescheduleplus;
grant select on stage.ResponsibleObjects to extension;
grant create any sequence to posseextensions;


-- API BREAK: Needed by feescheduleplus.pkg_FeeDefinition.GenerateFee()
grant UPDATE on FINANCE.FEES to FEESCHEDULEPLUS;
grant SELECT on FINANCE.FEES to FEESCHEDULEPLUS;


