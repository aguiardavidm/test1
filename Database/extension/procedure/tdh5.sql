create or replace procedure           tdh5 (
  aObjList  in out api.pkg_definition.udt_objectlist ) is
begin
  select objectid bulk collect into aObjList
    from api.objects
   where objectdeftypeid = 4 and rownum < 3;
end;

 
/

