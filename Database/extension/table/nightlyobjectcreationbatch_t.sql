create table nightlyobjectcreationbatch_t (
  batchid                         number(9) not null,
  batchdate                       date,
  errormessage                    varchar2(4000)
) tablespace mediumdata;

alter table nightlyobjectcreationbatch_t
add constraint nightlyobjectcreationbatch_pk
primary key (
  batchid
) using index tablespace mediumindexes;

create index nightlyobjectcreationbatc_ix_1
on nightlyobjectcreationbatch_t (
  batchdate
) tablespace mediumindexes;

