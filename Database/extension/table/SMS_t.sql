-- Create table
create table SMS_T
(
  objectid        NUMBER(9) not null,
  fromnumber      VARCHAR2(4000) not null,
  tonumber        VARCHAR2(4000) not null,
  message         VARCHAR2(4000) not null,
  sentdate        DATE,
  errormessage    VARCHAR2(4000),
  createddate     DATE,
  relatedobjectid NUMBER(9)
);
-- Grant/Revoke object privileges 
grant select, insert on SMS_T to POSSEEXTENSIONS, ABCRW;

alter table SMS_T
  add constraint SMS_PK primary key (OBJECTID)
  using index;

CREATE index SMS_IX_1 on SMS_T (tonumber);

CREATE index SMS_IX_2 on SMS_T (sentdate);

CREATE index SMS_IX_3 on SMS_T (createddate);

CREATE index SMS_IX_4 on SMS_T (relatedobjectid);
