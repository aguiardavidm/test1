create table processassignmentaudit_t (
  logicaltransactionid            number(9) not null,
  processid                       number(9) not null,
  userid                          number(9) not null,
  assignedto                      varchar2(30)
) tablespace largedata;

create index processid_ix
on processassignmentaudit_t (
  processid
) tablespace largedata;

