create table rtftopdf_t (
  documentid                      number(9) not null,
  document                        blob
) tablespace system
  lob (document)
    store as (
      tablespace system
    );

grant insert
on rtftopdf_t
to public;

