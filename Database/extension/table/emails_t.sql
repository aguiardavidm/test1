create table emails_t (
  objectid                        number(9) not null,
  fromaddress                     varchar2(4000) not null,
  toaddress                       varchar2(4000) not null,
  subject                         varchar2(4000) not null,
  body                            clob not null,
  ccaddress                       varchar2(4000),
  bccaddress                      varchar2(4000),
  sentdate                        date,
  errormessage                    varchar2(4000),
  createddate                     date,
  relatedobjectid                 number(9),
  message                         clob
) tablespace largedata
  lob (body)
    store as (
      tablespace largedata
    )
  lob (message)
    store as (
      tablespace largedata
    );

alter table emails_t
add constraint emails_pk
primary key (
  objectid
) using index tablespace largeindexes;

create index emails_ix_1
on emails_t (
  sentdate
) tablespace largeindexes;

create index emails_ix_2
on emails_t (
  relatedobjectid
) tablespace largeindexes;

