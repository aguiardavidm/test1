create table emails_old_t (
  emailid                         number(9) not null,
  fromaddress                     varchar2(4000) not null,
  toaddress                       varchar2(4000) not null,
  subject                         varchar2(4000) not null,
  body                            clob not null,
  ccaddress                       varchar2(4000),
  bccaddress                      varchar2(4000),
  sentdate                        date,
  errormessage                    varchar2(4000),
  createddate                     date,
  relatedobjectid                 number(9),
  message                         varchar2(4000)
) tablespace largedata
  lob (body)
    store as (
      tablespace largedata
    );

alter table emails_old_t
add constraint emails_old_pk
primary key (
  emailid
) using index tablespace largeindexes;

create index emails_old_ix_1
on emails_old_t (
  sentdate
) tablespace largeindexes;

create index emails_old_ix_2
on emails_old_t (
  relatedobjectid
) tablespace largeindexes;

