create table pdfbatchtoimport_t (
  fullfilename                    varchar2(4000) not null,
  reportname                      varchar2(1000),
  documentdefid                   number(9) not null,
  objectid                        number(9) not null,
  endpointid                      number(9) not null,
  processid                       number(9) not null
) tablespace system;

