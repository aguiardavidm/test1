create table objectauditrelationshipdata_t (
  objectauditid                   number(9),
  toendpointid                    number(9),
  relationshipid                  number(9),
  toobjectid                      number(9),
  flag                            char(1)
) tablespace largedata;

alter table objectauditrelationshipdata_t
add constraint objectauditreldata_fk1
foreign key (
  objectauditid
) references objectaudit_t (
  objectauditid
);

create index objectauditreldata_ix1
on objectauditrelationshipdata_t (
  objectauditid,
  toobjectid
) tablespace largeindexes;

create index objectauditreldata_ix2
on objectauditrelationshipdata_t (
  flag
) tablespace largeindexes;

