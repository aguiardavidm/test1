create table sequenceaudit_t (
  sequenceauditid                 number not null,
  sequenceid                      number(9) not null,
  maskedvalue                     varchar2(60) not null,
  referenceobjectdefid            number(9) not null,
  referenceobjectid               number(9) not null,
  logicaltransactionid            number(9) not null,
  createddate                     date not null,
  createdby                       number(9) not null,
  realuser                        varchar2(30)
) tablespace largedata;

alter table sequenceaudit_t
add constraint sequenceaudit_pk
primary key (
  sequenceauditid
) using index tablespace largeindexes;

