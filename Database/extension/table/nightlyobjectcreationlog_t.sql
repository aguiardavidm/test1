create table nightlyobjectcreationlog_t (
  batchid                         number(9),
  sourceobjectid                  number(9),
  targetobjectdef                 varchar2(60),
  createdobjectid                 number(9),
  errormessage                    varchar2(4000)
) tablespace mediumdata;

alter table nightlyobjectcreationlog_t
add constraint nightlyobjectcreationlog_fk_1
foreign key (
  batchid
) references nightlyobjectcreationbatch_t (
  batchid
);

