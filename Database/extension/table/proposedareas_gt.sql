create global temporary table proposedareas_gt (
  meridian                        number(1),
  range                           number(2),
  township                        number(3),
  section                         number(2),
  lsd                             number(2),
  hrv                             number(1),
  category                        varchar2(10)
) on commit delete rows;

