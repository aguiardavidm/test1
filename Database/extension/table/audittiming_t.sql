create table audittiming_t (
  objectauditid                   number(9),
  auditstart                      date,
  auditend                        date
) tablespace largedata;

create unique index audittiming_uk1
on audittiming_t (
  objectauditid
) tablespace largedata;

