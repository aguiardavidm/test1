create table emaildocuments_t (
  emailid                         number(9) not null,
  documentid                      number(9) not null
) tablespace largedata;

alter table emaildocuments_t
add constraint emaildocuments_pk
primary key (
  emailid,
  documentid
) using index tablespace largeindexes;

