create table jobstatuschange_t (
  jobstatuschangeid               number(9) not null,
  jobid                           number(9),
  statusname                      varchar2(6),
  effectivestartdate              date,
  effectiveenddate                date,
  mostrecent                      char(1),
  statustype                      varchar2(1),
  effectiveuser                   varchar2(30),
  statusdate                      date
) tablespace largedata;

grant alter
on jobstatuschange_t
to bcpdata;

grant debug
on jobstatuschange_t
to bcpdata;

grant delete
on jobstatuschange_t
to bcpdata;

grant flashback
on jobstatuschange_t
to bcpdata;

grant index
on jobstatuschange_t
to bcpdata;

grant insert
on jobstatuschange_t
to bcpdata;

grant on commit refresh
on jobstatuschange_t
to bcpdata;

grant query rewrite
on jobstatuschange_t
to bcpdata;

grant references
on jobstatuschange_t
to bcpdata;

grant select
on jobstatuschange_t
to bcpdata;

grant select
on jobstatuschange_t
to posseextensions;

grant select
on jobstatuschange_t
to public;

grant update
on jobstatuschange_t
to bcpdata;

alter table jobstatuschange_t
add constraint jobstatuschange_pk
primary key (
  jobstatuschangeid
) using index tablespace largeindexes;

create index jobstatuschange_ix_1
on jobstatuschange_t (
  jobid
) tablespace largeindexes;

