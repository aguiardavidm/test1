create table objectauditdata_t (
  objectauditid                   number(9),
  columnname                      varchar2(30),
  oldcolumnvalue                  varchar2(4000),
  newcolumnvalue                  varchar2(4000),
  columndefid                     number(9)
) tablespace largedata;

alter table objectauditdata_t
add constraint objectauditdata_fk1
foreign key (
  objectauditid
) references objectaudit_t (
  objectauditid
);

create index objectauditdata_ix1
on objectauditdata_t (
  objectauditid
) tablespace largeindexes;

