create table documentblobs_t (
  blobid                          number(9) not null,
  value                           blob,
  documentid                      number(9)
) tablespace mediumdata
  lob (value)
    store as (
      tablespace mediumdata
    );

grant delete
on documentblobs_t
to posseextensions;

grant insert
on documentblobs_t
to posseextensions;

grant select
on documentblobs_t
to posseextensions;

grant update
on documentblobs_t
to posseextensions;

alter table documentblobs_t
add constraint pk_blobid
primary key (
  blobid
) using index tablespace mediumindexes;

create index documentblobs_ix_1
on documentblobs_t (
  documentid
) tablespace mediumindexes;

