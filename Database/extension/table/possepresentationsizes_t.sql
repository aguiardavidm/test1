create table possepresentationsizes_t (
  objectid                        number(9) not null,
  objectdefname                   varchar2(30) not null,
  presentationname                varchar2(30) not null,
  width                           number(9) not null,
  height                          number(9) not null,
  systemname                      varchar2(30) not null
) tablespace xxsmalltabs;

alter table possepresentationsizes_t
add constraint possepresentationsizes_pk
primary key (
  objectid
) using index tablespace xxsmalltabs;

alter table possepresentationsizes_t
add constraint possepresentationsizes_uk_1
unique (
  objectdefname,
  presentationname
) using index tablespace xxsmalltabs;

