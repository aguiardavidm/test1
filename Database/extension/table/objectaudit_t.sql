create table objectaudit_t (
  objectauditid                   number(9) not null,
  audittype                       char(1),
  objectdefname                   varchar2(30),
  objectid                        number(9),
  asofdate                        date,
  externalfilenum                 varchar2(40),
  logicaltransactionid            number(9),
  auditdate                       date,
  effectiveuser                   varchar2(30),
  objectdefid                     number(9)
) tablespace largedata;

grant alter
on objectaudit_t
to bcpdata;

grant debug
on objectaudit_t
to bcpdata;

grant delete
on objectaudit_t
to bcpdata;

grant flashback
on objectaudit_t
to bcpdata;

grant index
on objectaudit_t
to bcpdata;

grant insert
on objectaudit_t
to bcpdata;

grant on commit refresh
on objectaudit_t
to bcpdata;

grant query rewrite
on objectaudit_t
to bcpdata;

grant references
on objectaudit_t
to bcpdata;

grant select
on objectaudit_t
to bcpdata;

grant update
on objectaudit_t
to bcpdata;

alter table objectaudit_t
add constraint objectaudit_pk
primary key (
  objectauditid
) using index tablespace largeindexes;

create index objectaudit_ix1
on objectaudit_t (
  objectid
) tablespace largeindexes;

create index objectaudit_ix2
on objectaudit_t (
  auditdate
) tablespace largeindexes;

