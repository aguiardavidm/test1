create table histresources91_t (
  meridian                        number(1) not null,
  range                           number(2) not null,
  township                        number(3) not null,
  section                         number(2) not null,
  lsd                             number(2) not null,
  hrv                             number(1) not null,
  category                        varchar2(10) not null,
  bordenno                        varchar2(10),
  sitename                        varchar2(250) not null
) tablespace system;

create index histresources_ix_1
on histresources91_t (
  meridian,
  range,
  township,
  section,
  lsd
) tablespace system;

