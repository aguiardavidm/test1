create or replace TYPE             "UDST_STRINGLIST"                                          as table of varchar2(4000);
/

grant execute
on udst_stringlist
to posseextensions;

grant execute
on udst_stringlist
to public;

