create or replace TYPE             "UDT_AGGRSTRING"                                          as object
(
  Value varchar2(4000),
  Separator varchar2(4000)
);
/

grant execute
on udt_aggrstring
to public;

