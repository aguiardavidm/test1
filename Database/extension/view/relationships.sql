create or replace view relationships as
select
  r."LOGICALTRANSACTIONID",r."RELATIONSHIPID",r."ENDPOINTID",r."FROMOBJECTID",r."TOOBJECTID",r."RELATIONSHIPDEFID",r."TOENDPOINT",
  d.ToEndPointName EndPointName
from
  api.Relationships r,
  api.RelationshipDefs d
where d.RelationshipDefId = r.RelationshipDefId
  and d.ToEndPointId = r.EndPointId;

