create or replace view emaildocuments as
select
  EmailId,
  DocumentId
from EmailDocuments_t;

grant select
on emaildocuments
to posseextensions;

