create or replace view possepresentationsizes as
select
  ObjectId,
  ObjectDefName,
  SystemName,
  PresentationName,
  Width,
  Height
from PossePresentationSizes_t;

grant delete
on possepresentationsizes
to posseextensions;

grant insert
on possepresentationsizes
to posseextensions;

grant select
on possepresentationsizes
to posseextensions;

grant update
on possepresentationsizes
to posseextensions;

