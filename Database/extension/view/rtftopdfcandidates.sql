create or replace view rtftopdfcandidates as
select (api.pkg_ColumnQuery.NumericValue( p.ObjectId, 'JobId')) as JobId,
       (api.pkg_ColumnQuery.value( api.pkg_ColumnQuery.NumericValue( p.ObjectId, 'JobId'), 'ReportToGenerate' )) as ReportToGenerate,
       p.ObjectId as ProcessId
  from (table(cast(api.pkg_SimpleSearch.CastableObjectsByIndex( 'p_GenerateDocument', 'GenerateRTFtoPDFDocument', 'Y' ) as api.udt_objectlist ))) p
  order by jobid
--grant select on extension.RTFTOPDFCANDIDATES to POSSEEXTENSIONS
--grant select on extension.RTFTOPDFCANDIDATES to PUBLIC;

grant select
on rtftopdfcandidates
to posseextensions;

grant select
on rtftopdfcandidates
to public;

