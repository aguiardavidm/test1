create or replace view jobstatuschange as
select
  jobstatuschangeid ,
  jobid,
  statusname,
  statustype,
  effectivestartdate,
  effectiveenddate,
  mostrecent,
	effectiveuser,
  statusdate
from jobstatuschange_t;

grant select
on jobstatuschange
to public;

