create or replace view objectaudit as
select ObjectAuditId,
       AuditType,
       ObjectDefName,
			 ObjectDefId,
       ObjectId,
       AsOfDate,
       ExternalFileNum,
       LogicalTransactionId,
       AuditDate,
       EffectiveUser
  from ObjectAudit_t;

grant select
on objectaudit
to posseextensions;

