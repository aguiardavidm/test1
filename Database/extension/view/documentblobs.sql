create or replace view documentblobs as
select
  blobid,
  value,
  documentid
from extension.documentblobs_t;

grant delete
on documentblobs
to posseextensions;

grant insert
on documentblobs
to posseextensions;

grant select
on documentblobs
to posseextensions;

grant update
on documentblobs
to posseextensions;

