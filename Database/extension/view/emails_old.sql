create or replace view emails_old as
select
  EmailId,
  FromAddress,
  ToAddress,
  Subject,
  Body,
  CCAddress,
  BccAddress,
  SentDate,
  ErrorMessage,
  CreatedDate,
  RelatedObjectID,
  Message
from Emails_Old_t;

