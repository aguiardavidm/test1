create or replace view nightlyobjectcreationbatch as
select
  BatchId,
  BatchDate,
  ErrorMessage
from NightlyObjectCreationBatch_t;

