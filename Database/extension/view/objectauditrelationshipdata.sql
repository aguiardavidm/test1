create or replace view objectauditrelationshipdata as
select ObjectAuditId,
       ToEndPointId,
       RelationshipId,
       ToObjectId,
       decode(Flag, 'E', 'Exists', 'N', 'New', 'D', 'Deleted') Flag
 from objectauditrelationshipdata_t;

grant select
on objectauditrelationshipdata
to posseextensions;

