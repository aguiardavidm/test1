create or replace view emails32 as
select
  EmailId,
  FromAddress,
  ToAddress,
  Subject,
  Body,
  CCAddress,
  BccAddress,
  SentDate,
  ErrorMessage,
  CreatedDate,
  RelatedObjectID,
  Message
from Emails32_t;

