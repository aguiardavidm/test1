create or replace view nightlyobjectcreationlog as
select
  BatchId,
  SourceObjectId,
  TargetObjectDef,
  CreatedObjectId,
  ErrorMessage
from NightlyObjectCreationLog_t;

