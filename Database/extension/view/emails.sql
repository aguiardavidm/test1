create or replace view emails as
select
  ObjectId,
  FromAddress,
  ToAddress,
  Subject,
  Body,
  CCAddress,
  BccAddress,
  SentDate,
  ErrorMessage,
  CreatedDate,
  RelatedObjectID,
  Message
from Emails_t;

grant select
on emails
to bcpdata;

grant select
on emails
to posseextensions;

