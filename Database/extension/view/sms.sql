create or replace view sms as
select 
  t.objectid,
  t.fromnumber,
  t.tonumber,
  t.message,
  t.sentdate,
  t.errormessage,
  t.createddate,
  t.relatedobjectid
from sms_t t;

grant select
on sms
to posseextensions;

