create or replace view objectauditdata as
select ObjectAuditId,
       ColumnName,
       ColumnDefId,
       OldColumnValue,
       NewColumnValue
  from ObjectAuditData_t;

grant select
on objectauditdata
to posseextensions;

