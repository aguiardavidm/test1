create or replace function           ConcatList2 (
  /* ConcatList2 is a more flexible version of ConcatList.  It allows the
    separator to be specified through the udt_AggrString type.  Example:

    select ConcatList2(udt_AggrString(some_value, ';'))
    from some_table;

    This approach had to be taken because Oracle does not allow multi-argument
    aggregate functions.  While it would have been ideal to be able to call:
    "ConcatList(some_value, ';')", this isn't possible.  Instead, the aggregate
    function takes udt_AggrString as the argument. */
  input udt_AggrString
) return varchar2
PARALLEL_ENABLE AGGREGATE USING udt_ConcatList;

 
/

grant execute
on concatlist2
to public;

