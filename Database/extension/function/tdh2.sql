create or replace function           tdh2
  return api.udt_objectlist is
  aObjList  api.udt_objectlist;
begin
  aObjList := api.udt_objectlist();
  for x in (select objectid from api.objects
             where objectdeftypeid = 4 and rownum < 3) loop
    aObjList.extend;
    aObjList(aObjList.last) := api.udt_object(x.objectid);
  end loop;
  return aObjList;
end;

 
/

