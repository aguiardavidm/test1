create or replace function           ConcatList (
  /* ConcatList is an aggregate function that will concatenate a list of values
    into a single comma separated value. It will ignore null values. Example:

    select ConcatList(some_value)
    from some_table;

    This will return something like 'value1, value2, value3'.
  */
  input varchar2
) return varchar2
PARALLEL_ENABLE AGGREGATE USING udt_ConcatList;

 
/

grant execute
on concatlist
to public;

