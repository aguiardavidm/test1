create or replace function           tdh3
  return api.pkg_definition.udt_idlist is
  aObjList  api.pkg_definition.udt_objectlist;
begin
  for x in (select objectid from api.objects
             where objectdeftypeid = 4 and rownum < 3) loop
    aObjList(aObjList.count+1) := x.objectid ;
  end loop;
  return aObjList;
end;

 
/

