create or replace package pkg_RelationshipUpdate as

  /*---------------------------------------------------------------------------
   * This package contains procedures used to create or assist in the creation
   * of relationships.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * NewRelationship()
   *   Create a relationship between two objects using the endpoint name
   * specified.
   *-------------------------------------------------------------------------*/
  function New (
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_EndPointName                      varchar2,
    a_AsOfDate                          date default sysdate
  ) return udt_Id;

  procedure New (
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_EndPointName                      varchar2,
    a_AsOfDate                          date default sysdate
  );
  
  procedure New (
    a_EndpointId                        udt_Id,
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_AsOfDate                          date default sysdate
  );

  /*---------------------------------------------------------------------------
   * Remove()
   *   Remove the relationship between two objects for the endpoint name
   * specified.
   *-------------------------------------------------------------------------*/
  procedure Remove (
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_EndPointName                      varchar2
  );

end pkg_RelationshipUpdate;

 
/

grant execute
on pkg_relationshipupdate
to abc;

grant execute
on pkg_relationshipupdate
to bcpdata;

grant execute
on pkg_relationshipupdate
to conversion;

grant execute
on pkg_relationshipupdate
to ereferral;

grant execute
on pkg_relationshipupdate
to lms;

create or replace package body pkg_RelationshipUpdate as

  /*---------------------------------------------------------------------------
   * New() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function New (
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_EndPointName                      varchar2,
    a_AsOfDate                          date default sysdate
  ) return udt_Id is
    t_ObjectDefId                       udt_Id;
    t_EndPointId                        udt_Id;
  begin
    begin
      select ObjectDefId
      into t_ObjectDefId
      from api.Objects
      where ObjectId = a_FromObjectId;
    exception
    when no_data_found then
      api.pkg_Errors.RaiseError(-20000, 'Invalid object id: '||a_FromObjectId);
    end;

    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
        a_EndPointName);
    if t_EndPointId is null then
      api.pkg_Errors.RaiseError(-20000, 'End Point Name "'||a_EndPointName||
          '" is not valid for object '||a_FromObjectId||' ['||t_ObjectDefId||
          ']');
    end if;

    return api.pkg_RelationshipUpdate.New(t_EndPointId, a_FromObjectId, a_ToObjectId, a_AsOfDate);
  end New;
  
  
  procedure New (
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_EndPointName                      varchar2,
    a_AsOfDate                          date default sysdate
  ) is
    t_RelId                             udt_Id;
  begin
    t_RelId := New(a_FromObjectId, a_ToObjectId, a_EndPointName, a_AsOfDate);
  end New;
  
  
  procedure New (
    a_EndpointId                        udt_Id,
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_AsOfDate                          date default sysdate
  ) is
    t_RelId                             udt_Id;
  begin
    t_RelId := api.pkg_RelationshipUpdate.New(a_EndpointId, a_FromObjectId, a_ToObjectId, a_AsOfDate);
  end New;


  /*---------------------------------------------------------------------------
   * Remove() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure Remove (
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_EndPointName                      varchar2
  ) is
    t_ObjectDefId                       udt_Id;
    t_RelId                             udt_Id;
  begin
    begin
      select ObjectDefId
      into t_ObjectDefId
      from api.Objects
      where ObjectId = a_FromObjectId;
    exception
    when no_data_found then
      api.pkg_Errors.RaiseError(-20000, 'Invalid object id: '||a_FromObjectId);
    end;

    begin
      select RelationshipId
      into t_RelId
      from api.Relationships
      where FromObjectId = a_FromObjectId
        and ToObjectId = a_ToObjectId
        and EndPointId = api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
                a_EndPointName);
      api.pkg_RelationshipUpdate.Remove(t_RelId);
    exception
    when no_data_found then
      null;
    end;
  end Remove;
  
end pkg_RelationshipUpdate;

/

