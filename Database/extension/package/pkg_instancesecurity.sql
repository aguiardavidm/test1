create or replace package pkg_InstanceSecurity as

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *   Routines used to manipulate instance security.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * AdjustByTemplates()
   *   This takes on parameter which is the name of the endpoint of the
   * relationship which defines the security to use via a number of objects.
   *
   *   These objects needs to be configured as follows:
   *
   *     AccessGroupId        Key of the AccessGroup which is being granted
   *                          privileged.
   *     Can...               Boolean column which if Y indicates that this
   *                          privilege is to be granted.  (Actual privilege
   *                          is the Label of the columndef).
   *
   * The code first reverts to default security, then selects all the objects
   * and applies the security as it is encountered.
   *-------------------------------------------------------------------------*/
  procedure AdjustByTemplates (
    a_ObjectId				udt_Id,
    a_AsOfDate  			date,
    a_EndPointName			varchar2
  );

  /*---------------------------------------------------------------------------
   * AdjustByColumns()
   *   This takes two parameters both of which are a list of access group
   * descriptions.  The first list will be given read access to the current
   * object.  While the second lilst will be given modify access to the current
   * object.
   *
   * The code first reverts to default security, then selects all the objects
   * and applies the security as it is encountered.
   *-------------------------------------------------------------------------*/
  procedure AdjustByColumn (
    a_ObjectId				udt_Id,
    a_AsOfDate  			date,
    a_ReadAccessGroups		varchar2,
    a_ModifyAccessGroups		varchar2
  );


end pkg_InstanceSecurity;
 
/

grant execute
on pkg_instancesecurity
to posseextensions;

create or replace package body pkg_InstanceSecurity as

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_IdList is api.pkg_Definition.udt_IdList;


  /*---------------------------------------------------------------------------
   * Adjust() -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure Adjust (
    a_ObjectId				udt_Id,
    a_AccessGroups		varchar2,
    a_Privilege   		varchar2
  ) is
    t_AccessGroup                       api.AccessGroups.Description%type;
    t_AccessGroups                      varchar2(4000);
    t_Pos                               number;
    t_AccessGroupId                     udt_Id;
  begin
--pkg_debug.putline('Adjust1');
    t_AccessGroups := a_AccessGroups || ';';

    t_Pos := instr(t_AccessGroups, ';');
    while t_Pos > 0 loop
      t_AccessGroup := substr(t_AccessGroups, 1, t_Pos - 1);
      t_AccessGroups := substr(t_AccessGroups, t_Pos + 1);
--pkg_debug.putline('Adjust2');
      begin
        select AccessGroupId
          into t_AccessGroupId
          from api.AccessGroups
         where Description = t_AccessGroup;
      exception
        when no_data_found then
          api.pkg_Errors.Clear();
          api.pkg_Errors.SetArgValue('ObjectId', a_ObjectId);
          api.pkg_Errors.SetArgValue('AccessGroup',t_AccessGroup);
          api.pkg_Errors.SetArgValue('AccessGroups', a_AccessGroups);
          api.pkg_Errors.RaiseError(-20100, 'Unable to find Access Group "{AccessGroup}".');
      end ;
      api.pkg_ObjectUpdate.GrantPrivilege(a_ObjectId, t_AccessGroupId, a_Privilege);
      t_Pos := instr(t_AccessGroups, ';');
    end loop;

  end;


  /*---------------------------------------------------------------------------
   * AdjustByTemplates() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure AdjustByTemplates (
    a_ObjectId				udt_Id,
    a_AsOfDate  			date,
    a_EndPointName			varchar2
  ) is
    t_ObjectDefId			udt_Id;
    t_ObjectDefTypeId			udt_Id;
    t_Objects                           udt_IdList;
    t_EndPointId                        udt_Id;
    t_AccessGroupId                     udt_Id;
  begin

    select ObjectDefId,   ObjectDefTypeId
      into t_ObjectDefId, t_ObjectDefTypeId
      from api.Objects
     where ObjectId = a_ObjectId;

    if t_ObjectDefTypeId not in (1,2) then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('ObjectId', a_ObjectId);
      api.pkg_Errors.SetArgValue('ObjectDefTypeId', t_ObjectDefTypeId);
      api.pkg_Errors.RaiseError(-20100, 'Invalid ObjectDefTypeId "{ObjectDefTypeId}".');
    end if;

    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
        a_EndPointName);

    if t_EndPointId is null then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('ObjectId', a_ObjectId);
      api.pkg_Errors.SetArgValue('EndPointName', a_EndPointName);
      api.pkg_Errors.RaiseError(-20100,
          'EndPoint "{EndPointName}" does not exist for Object.');
    end if;

    api.pkg_ObjectUpdate.RevertToDefaultSecurity(a_ObjectId);

    t_Objects := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId, t_EndPointId);

    for i in 1..t_Objects.count loop
      t_AccessGroupId := api.pkg_ColumnQuery.NumericValue(t_Objects(i),
          'AccessGroupId');
      if t_AccessGroupid is null then
        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('ObjectId', a_ObjectId);
        api.pkg_Errors.SetArgValue('InstanceSecurityTemplate',t_Objects(i));
        api.pkg_Errors.RaiseError(-20100, 'Missing AccessGroupId.');
      end if;
      for c in (select cd.Label Privilege
                  from api.ColumnDefs cd,
                       api.Objects o
                 where o.ObjectId = t_Objects(i)
                   and cd.ObjectDefId = o.ObjectDefId
                   and lower(cd.Name) like 'can%'
                   and cd.DataType = 'boolean'
                   and api.pkg_ColumnQuery.Value(o.ObjectId, cd.Name) = 'Y') loop
        api.pkg_ObjectUpdate.GrantPrivilege(a_ObjectId, t_AccessGroupId, c.Privilege);
      end loop;
    end loop;

  end;


  /*---------------------------------------------------------------------------
   * AdjustByColumn() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure AdjustByColumn (
    a_ObjectId				udt_Id,
    a_AsOfDate  			date,
    a_ReadAccessGroups		varchar2,
    a_ModifyAccessGroups		varchar2
  ) is
  begin

    api.pkg_ObjectUpdate.RevertToDefaultSecurity(a_ObjectId);
    Adjust(a_ObjectId, a_ReadAccessGroups, 'Read');
    Adjust(a_ObjectId, a_ModifyAccessGroups, 'Modify');
    Adjust(a_ObjectId, a_ModifyAccessGroups, 'Delete');
    Adjust(a_ObjectId, a_ModifyAccessGroups, 'Secure');
    Adjust(a_ObjectId, a_ModifyAccessGroups, 'ManualFees');
    Adjust(a_ObjectId, a_ModifyAccessGroups, 'Pay');
    Adjust(a_ObjectId, a_ModifyAccessGroups, 'Void');

  end;


end pkg_InstanceSecurity;
/

