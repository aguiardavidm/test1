create or replace package pkg_collectionutils as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  type udt_ObjectTable is table of varchar2(1) index by binary_integer;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  /*--------------------------------------------------------------------------
   * Join() - PUBLIC
   *   Merges a_SecondList into a_MainList (only values in both are left)
   *------------------------------------------------------------------------*/
  procedure Join (
    a_MainList                          in out nocopy udt_ObjectTable,
    a_SecondList                        udt_ObjectTable
  );

  /*--------------------------------------------------------------------------
   * Join() - PUBLIC
   *   Merges a_SecondList into a_MainList (only values in both are left)
   *------------------------------------------------------------------------*/
  procedure Join (
    a_MainList                          in out nocopy udt_ObjectTable,
    a_SecondList                        udt_IdList
  );


  /*--------------------------------------------------------------------------
   * Join() - PUBLIC
   *   Merges a_SecondList into a_MainList (only values in both are left)
   *------------------------------------------------------------------------*/
  procedure Join (
    a_MainList                          in out nocopy api.udt_objectlist,
    a_SecondList                        udt_ObjectTable
  );

  /*--------------------------------------------------------------------------
   * Join() - PUBLIC
   *   Merges a_SecondList into a_MainList (only values in both are left)
   *------------------------------------------------------------------------*/
  procedure Join (
    a_MainList                          in out nocopy api.udt_objectlist,
    a_SecondList                        api.udt_objectlist
  );

 /*--------------------------------------------------------------------------
  * Minus() -- Public
  * Returns the values in the first list if they are not also in the second
  *--------------------------------------------------------------------------*/
  procedure Subtract (
    a_MainList                          in out nocopy udt_IdList,
    a_SecondList                        udt_IdList
  );

  /*--------------------------------------------------------------------------
   * Append() - PUBLIC
   *   Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_IdList,
    a_SecondList                        udt_IdList
  );

  /*--------------------------------------------------------------------------
   * Append() - PUBLIC
   *   Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_ObjectTable,
    a_SecondList                        udt_IdList
  );

  /*--------------------------------------------------------------------------
   * Append() - PUBLIC
   *   Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_ObjectTable,
    a_SecondList                        udt_ObjectTable
  );

  /*--------------------------------------------------------------------------
   * Append() - PUBLIC
   *   Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy api.udt_objectlist,
    a_SecondList                        api.udt_objectlist
  );

  /*--------------------------------------------------------------------------
   * Append() - PUBLIC
   *   Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy api.udt_ObjectList,
    a_SecondList                        udt_ObjectTable
  );

end pkg_collectionutils;

 
/

grant execute
on pkg_collectionutils
to abc;

grant execute
on pkg_collectionutils
to bcpdata;

grant execute
on pkg_collectionutils
to conversion;

grant execute
on pkg_collectionutils
to ereferral;

create or replace package body pkg_collectionutils as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------
   * Join() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure Join (
    a_MainList                          in out nocopy udt_ObjectTable,
    a_SecondList                        udt_ObjectTable
  ) is
    t_TempList                          udt_ObjectTable;
    t_Index                             number;
  begin
    t_Index := a_SecondList.first;
    while t_Index is not null loop
      if a_MainList.exists(t_Index) then
        t_TempList(t_Index) := 'Y';
      end if;
      t_Index := a_SecondList.next(t_Index);
    end loop;

    a_MainList.delete;
    a_MainList := t_TempList;
    t_TempList.delete;
  end;


 /*--------------------------------------------------------------------------
  * Join() -- PUBLIC
  *   Merges a_SecondList into a_MainList (only values in both are left)
  *------------------------------------------------------------------------*/
  procedure Join (
    a_MainList                          in out nocopy udt_ObjectTable,
    a_SecondList                        udt_IdList
  ) is
    t_TempList                          udt_ObjectTable;
    t_Count                             number;
  begin
    t_Count := a_SecondList.count;
    for i in 1 .. t_Count loop
      if a_MainList.exists(a_SecondList(i))then
        t_TempList(a_SecondList(i)) := 'Y';
      end if;
    end loop;

    a_MainList.delete;
    a_MainList := t_TempList;
    t_TempList.delete;
  end;


 /*--------------------------------------------------------------------------
  * Join() -- PUBLIC
  *   Merges a_SecondList into a_MainList (only values in both are left)
  *------------------------------------------------------------------------*/
  procedure Join (
    a_MainList                          in out nocopy api.udt_ObjectList,
    a_SecondList                        udt_ObjectTable
  ) is
    t_MainList                          udt_ObjectTable;
    t_TempList                          udt_ObjectTable;
    t_Index                             number;
  begin
    /* Load a_MainList into an Object Table */
    t_Index := a_MainList.count;
    for i in 1..t_Index loop
      t_MainList(a_MainList(i).ObjectId) := 'Y';
    end loop;

    if t_MainList.count > 0 then
      t_Index := a_SecondList.first;
      while t_Index is not null loop
        if t_MainList.exists(t_Index) then
          t_TempList(t_Index) := 'Y';
        end if;
        t_Index := a_SecondList.next(t_Index);
      end loop;
    end if;

    a_MainList.delete;
    Append(a_MainList, t_TempList);
    t_TempList.delete;
  end;


 /*--------------------------------------------------------------------------
  * Join() -- PUBLIC
  *   Merges a_SecondList into a_MainList (only values in both are left)
  *------------------------------------------------------------------------*/
  procedure Join (
    a_MainList                          in out nocopy api.udt_ObjectList,
    a_SecondList                        api.udt_ObjectList
  ) is
    t_Count                             pls_integer;
    t_Index                             number;
    t_MainList                          udt_ObjectTable;
    t_TempList                          udt_ObjectTable;
  begin
    /* Load a_MainList into an Object Table */
    t_Count := a_MainList.count;
    for i in 1..t_Count loop
      t_MainList(a_MainList(i).ObjectId) := 'Y';
    end loop;

    /* Now merge with a_SecondList */
    if t_MainList.count > 0 then
      t_Count := a_SecondList.count;
      for i in 1..t_Count loop
        t_Index := a_SecondList(i).ObjectId;
        if t_MainList.exists(t_Index) then
          t_TempList(t_Index) := 'Y';
        end if;
      end loop;
    end if;

    a_MainList.delete;
    Append(a_MainList, t_TempList);
    t_TempList.delete;
  end;

 /*--------------------------------------------------------------------------
  * Minus() -- Public
  * Returns the values in the first list if they are not also in the second
  *    Assumes both variables are arrays.
  *--------------------------------------------------------------------------*/
  procedure Subtract (
    a_MainList                          in out nocopy udt_IdList,
    a_SecondList                        udt_IdList
  ) is
    t_TempList                          udt_IdList;
    t_Common                            boolean;
  begin
    for x in 1..a_MainList.count loop
      t_Common := false;
      for y in 1..a_SecondList.count loop
        if a_MainList(x) = a_SecondList(y) then t_Common := true; end if;
      end loop;
      if not t_Common then
        t_TempList(t_TempList.count + 1) := a_MainList(x);
      end if;
    end loop;
    a_MainList := t_TempList;
    t_TempList.delete;
  end Subtract;

 /*--------------------------------------------------------------------------
  * Append() -- PUBLIC
  * Adds all the values in a_SecondList to a_MainList
  *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_IdList,
    a_SecondList                        udt_IdList
  ) is
    t_TempList                          udt_ObjectTable;
    t_Count                             number;
    t_Index                             number;
  begin
    /* Combine all the distinct values from both lists */
    t_Count := a_MainList.count;
    for i in 1 .. t_Count loop
      t_TempList(a_MainList(i)) := 'Y';
    end loop;
    t_Count := a_SecondList.count;
    for i in 1 .. t_Count loop
      t_TempList(a_SecondList(i)) := 'Y';
    end loop;
    a_MainList.delete;

    /* Now turn back into an IdList */
    t_Index := t_TempList.first;
    t_Count := 0;
    while t_Index is not null loop
      t_Count := t_Count + 1;
      a_MainList(t_Count) := t_Index;
      t_Index := t_TempList.next(t_Index);
    end loop;

    t_TempList.delete;
  end;


  /*--------------------------------------------------------------------------
   * Append() -- PUBLIC
   * Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_ObjectTable,
    a_SecondList                        udt_IdList
  ) is
    t_Count                             number;
  begin
    t_Count := a_SecondList.count;
    for i in 1 .. t_Count loop
      a_MainList(a_SecondList(i)) := 'Y';
    end loop;
  end;


  /*--------------------------------------------------------------------------
   * Append() -- PUBLIC
   * Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_ObjectTable,
    a_SecondList                        udt_ObjectTable
  ) is
    t_Index                             number;
  begin
    t_Index := a_SecondList.first;
    while t_Index is not null loop
      a_MainList(t_Index) := 'Y';
      t_Index := a_SecondList.next(t_Index);
    end loop;
  end;


  /*--------------------------------------------------------------------------
   * Append() -- PUBLIC
   * Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy api.udt_ObjectList,
    a_SecondList                        api.udt_ObjectList
  ) is
    t_Count                             pls_integer;
    t_Index                             number;
    t_TempList                          udt_ObjectTable;
  begin
    /* Load a_MainList into an Object Table */
    t_Count := a_MainList.count;
    for i in 1..t_Count loop
      t_TempList(a_MainList(i).ObjectId) := 'Y';
    end loop;

    /* Now append the second list */
    t_Count := a_SecondList.count;
    for i in 1..t_Count loop
      t_Index := a_SecondList(i).ObjectId;
      t_TempList(t_Index) := 'Y';
    end loop;

    a_MainList.delete;
    Append(a_MainList, t_TempList);
    t_TempList.delete;
  end;


  /*--------------------------------------------------------------------------
   * Append() -- PUBLIC
   *   Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy api.udt_ObjectList,
    a_SecondList                        udt_ObjectTable
  ) is
    t_Count                             pls_integer;
    t_Index                             number;
    t_TempList                          udt_ObjectTable;
  begin
    if a_MainList.count > 0 then
      /* Load a_MainList into an Object Table */
      t_Count := a_MainList.count;
      for i in 1..t_Count loop
        t_TempList(a_MainList(i).ObjectId) := 'Y';
      end loop;

      /* Now append the second list */
      t_Index := a_SecondList.first;
      while t_Index is not null loop
        t_TempList(t_Index) := 'Y';
        t_Index := a_SecondList.next(t_Index);
      end loop;
    else
      t_TempList := a_SecondList;
    end if;

    /* Copy the temp list into the output list */
    a_MainList.delete;
    a_MainList.extend(t_TempList.count);
    t_Index := t_TempList.first;
    t_Count := 1;
    while t_Index is not null loop
      a_MainList(t_Count) := api.udt_Object(t_Index);
      t_Index := t_TempList.next(t_Index);
      t_Count := t_Count + 1;
    end loop;
    t_TempList.delete;
  end;

end pkg_collectionutils;

/

