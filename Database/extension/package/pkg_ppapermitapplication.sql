create or replace package           Pkg_PPAPermitApplication is

  -- Author  : HUSSEIN.HUSSEIN
  -- Created : 11/28/2008 11:29:46 AM
  -- Purpose : Main package for the j_PPAPermit Object (Parks Permit)
  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
   subtype udt_StringList is api.pkg_Definition.udt_StringList;
  /*---------------------------------------------------------------------------
   * Search - PPA Permit Object Search
   *-------------------------------------------------------------------------*/
  Procedure PPAApplicationSearch (a_ApplicationNumber      udt_Id,
                                 a_LastName                varchar2,
                                 a_FirstName               varchar2,
                                 a_Affiliation             varchar2,
                                 a_PPAName                 varchar2,
                                 a_PPAType                 varchar2,
                                 a_PPAManagementArea       varchar2,
                                 a_Activity                varchar2,
                                 a_Research                varchar2,
                                 a_FromSubmittedDate       Date,
                                 a_ToSubmittedDate         Date,
                                 a_FromAcceptedDate        Date,
                                 a_ToAcceptedDate          Date,
                                 a_FromCompletedDate       Date,
                                 a_ToCompletedDate         Date,
                                 a_StatusDescription       varchar2,
                                 a_OutstandingOnly         varchar2,
                                 a_ApplicationType         varchar2,
                                 a_Objects      out nocopy api.udt_ObjectList );

 /*---------------------------------------------------------------------------
  * CreatePCOwnerRelOnJob - Create a PC Owner relationship on the Job.
  *-------------------------------------------------------------------------*/
 Procedure CreatePCOwnerRelOnJob(a_ObjectId                udt_Id,
                                 a_AsOfDate                 date);

 /*-----------------------------------------------------------------------------
 * SendEmail - Create a send email process and populate the necessary
 *   fields.
 *----------------------------------------------------------------------------*/
  procedure SendEmail(a_ProcessId              udt_Id,
                      a_AsOfDate               date);
 /*-----------------------------------------------------------------------------
 * CreateManagementAreasRel - Create relationships to all
 * Management Areas.
 *----------------------------------------------------------------------------*/
  Procedure CreateManagementAreasRel(a_ObjectId       udt_Id,
                                     a_AsOfDate       date );
 /*-----------------------------------------------------------------------------
 * ManagementAreasRequireReview - Create Relationship to all Management Areas that
 * require review.
 *----------------------------------------------------------------------------*/
  Procedure ManagementAreasRequireReview(a_ObjectId                 udt_Id,
                                         a_EndPointId               udt_Id,
                                         a_ReviewManagementAreaList out nocopy api.udt_ObjectList);

 /*-----------------------------------------------------------------------------
  * CopyConditionsToJob - Copy all the final conditions on the Prepare Parks Permit
  * Application process to Job
  *----------------------------------------------------------------------------*/
  Procedure CopyConditionsToJob(a_ObjectId       udt_Id,
                                a_AsOfDate       date );

 /*---------------------------------------------------------------------------
 * AssignScreenProcess() - Assign the Screen Parks Permit Application process
 * according to USC-051
 *-------------------------------------------------------------------------*/
   Procedure AssignScreenProcess(a_ObjectId    udt_Id,
                                  a_AsOfDate    date);

 /*---------------------------------------------------------------------------
 * CreateRelToPPA() - Creates a Procedural relationship between the Review process and Parks
 * Protected Areas
 *-------------------------------------------------------------------------*/
   Procedure CreateRelToPPA(a_ObjectId      udt_Id,
                             a_EndPointId   udt_Id,
                             a_JobPPAList   out nocopy api.udt_ObjectList);

 /*---------------------------------------------------------------------------
 * PermitToAmendProcedureLookup - Creates a drop down for all the Permit that can
 * be amended by the Applicant
 *-------------------------------------------------------------------------*/

   Procedure PermitToAmendProcedureLookup(a_ObjectId              udt_Id,
                                          a_RelationshipDefId     udt_Id,
                                          a_EndPoint              varchar2,
                                          a_SearchString          varchar2,
                                          a_Objects               out api.udt_ObjectList);

  /*---------------------------------------------------------------------------
 * PermitToRenewProcedureLookup - Creates a drop down for all the Permits that can
 * be reneweds by the Applicant
 *-------------------------------------------------------------------------*/

   Procedure PermitToRenewProcedureLookup(a_ObjectId              udt_Id,
                                          a_RelationshipDefId     udt_Id,
                                          a_EndPoint              varchar2,
                                          a_SearchString          varchar2,
                                          a_Objects               out api.udt_ObjectList);

 /*---------------------------------------------------------------------------
 * CreateConcernReviewer() - Creates the User who entered the Concerns for
 * the Review Parks Permit Application
 *-------------------------------------------------------------------------*/
   Procedure CreateConcernReviewer(a_ObjectId               udt_Id,
                                   a_AsOfDate               date);

  /*---------------------------------------------------------------------------
 * CreateConditionsReviewer() - Creates the User who entered the Conditions for
 * the Review Parks Permit Application
 *-------------------------------------------------------------------------*/
   Procedure CreateConditionReviewer(a_ObjectId               udt_Id,
                                     a_AsOfDate               date);
  /*---------------------------------------------------------------------------
 *  CreateReferralEmail() - Creates the Emails for each referral displaying:
 *  - To Referrals
 *  - Additional Contents:
 *    Parks and Protected Areas (PPA Name)
 *    Field Operations, Commencement Date, Termination Date
 *  - May Include Concerns
 *  - May Include Conditions
 *  - May Include Attachments
 *  - Only Display Links for Internal Staff in Emails
 *-------------------------------------------------------------------------*/
   Procedure CreateReferralEmail (a_ObjectId udt_Id,
                                  a_AsOfDate date,
                                  a_EndPointName varchar2 default '',
                                  a_SendEmailId udt_Id default 0);
 /*---------------------------------------------------------------------------
 * StartSubmissionAudit() - This is to run after the Audit constructor.
 *    The assumptions is that the permit is being copied which puts items into
 *    the audit log that the submission report does not care about.  This
 *    procedure marks the point at which that is done and the point at which
 *    the user has started to make their changes.
 *    ReportDriverColumn is the name of the column used to drive the submission
 *    report.  ie SubmissionReportNumber or RevisionNumber
 *---------------------------------------------------------------------------*/
  procedure StartSubmissionAudit(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_ReportDriverColumn                varchar2,
    a_ValueToStart                      varchar2 default '1'
  );


  /*---------------------------------------------------------------------------
   * ReleasePermitFromAmendmentorRenewal() - This is to run when an application is
   * deleted before it is initally submitted. The permit is released from Amendment
   * or Renewal.
   *---------------------------------------------------------------------------*/
    procedure RelPermitFromAmendorRenewal(
      a_ObjectId                          udt_Id,
      a_AsOfDate                          date
    );
end Pkg_PPAPermitApplication;

 
/

grant execute
on pkg_ppapermitapplication
to posseextensions;

