create or replace package           pkg_HistResources is

  -- Author  : CARL.BRUINSMA
  -- Created : 11/7/2008 17:06:40
  -- Purpose : Transfer the list of Historical Resources from the draft
  --    staging area transfer.histresources to extension.histresources where
  --    the data will be used a reference table for the OPAC system.

  subtype udt_Id is api.pkg_definition.udt_Id;

 /*-----------------------------------------------------------------------------
  * Transfer() - Replace extension.histresources_t with transfer.histresources_t
  *---------------------------------------------------------------------------*/
  procedure Transfer(
      a_ObjectId        udt_Id,
      a_AsOfDate        date
  );

end pkg_HistResources;

 
/

grant execute
on pkg_histresources
to posseextensions;

