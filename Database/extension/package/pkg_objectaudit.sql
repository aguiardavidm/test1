create or replace package pkg_ObjectAudit is

  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * AuditConstructor() -- PUBLIC
   *   Attach this to the constructor of an objecttype
   *-------------------------------------------------------------------------*/
  procedure AuditConstructor (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date
  );

  /*---------------------------------------------------------------------------
   * AuditVerify() -- PUBLIC
   *   Attach this to the Post Verify of an objecttype
   * arguments -  a_ObjectID
   *              a_AsOfDate
   *              a_AuditColumns - a comma concatenated list of column names
   *                               properly capitalized
   *                          or - % which will prompt the procedure to
   *                               return all ENTERABLE column names
   *                          or NULL for no column logging
   *              a_AuditRelationships - a comma concatenated list of relationship
   *                                     view names properly capitalized
   *                                or - % which will prompt the procedure to
   *                                     return all stored relationship ToEndPointIds
   *                                or NULL for no relationship logging
   *              a_OmitPrefix - will allow relationship labels and column names matching
   *                             a certain prefix to be omitted
   *                           - this is not case sensitive
   *                           - no action taken if NULL
   *                           - example 'zap' or 'zzz'
   *
   *   **NOTE** If both the a_AuditColumns and a_AuditRelationships **NOTE**
   *   **NOTE** are NULL then NO LOGGING will take place!           **NOTE**
   *
   *   **NOTE** The object type being logged must have a view name  **NOTE**
   *-------------------------------------------------------------------------*/
  procedure AuditVerify (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_AuditColumns              varchar2,
    a_AuditRelationshipNames    varchar2,
    a_OmitPrefix                varchar2 default null
  );

  /*---------------------------------------------------------------------------
   * AuditDestructor() -- PUBLIC
   *   Attach this to the Destructor of an objecttype
   *   Uuses same arguments as AuditVerify Procedure above.
   *-------------------------------------------------------------------------*/
  procedure AuditDestructor (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_AuditColumns              varchar2,
    a_AuditRelationshipNames    varchar2,
    a_OmitPrefix                varchar2 default null
  );

  /*---------------------------------------------------------------------------
   * Audit() -- PUBLIC
   * oversees the details of the audit
   *-------------------------------------------------------------------------*/

  procedure Audit (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_AuditColumns              varchar2,
    a_AuditRelationshipNames    varchar2,
    a_Omittal                   varchar2,
    a_AuditType                 pls_integer
  );

  /*---------------------------------------------------------------------------
   * CleanupOldData() -- PUBLIC
   *   Cleans up data from the audit tables based on the retention days in the constants
   * table
  procedure CleanupOldData;
   *-------------------------------------------------------------------------*/

end;
 
/

grant execute
on pkg_objectaudit
to bcpdata;

create or replace package body pkg_ObjectAudit is

  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;

  TYPE r_Rel IS RECORD (
    EndPointId          udt_Id,
    ToObjectId          udt_Id,
    Flag                char(1) /* 'E'xists, 'N'ew, 'D'eleted */
  );
  TYPE tr_Rel IS TABLE OF r_Rel  INDEX BY BINARY_INTEGER;

  g_ConstructorLogicalTransIds  udt_IdList;

  g_AuditNew            constant pls_integer := 1;
  g_AuditChange         constant pls_integer := 2;
  g_AuditRemove         constant pls_integer := 3;

  /*--------------------------------------------------------------------------
   * InsertRelData() -- private
   *------------------------------------------------------------------------*/
  procedure InsertRelData (
    a_RelationshipId  udt_Id,
    a_EndPointId        udt_Id,
    a_ToObjectId        udt_Id,
    a_Flag              char,
    a_Rel_tr            in out nocopy tr_Rel
  ) is
    t_Rel_r r_Rel;
  begin
    t_Rel_r.EndPointId := a_EndPointId;
    t_Rel_r.ToObjectId := a_ToObjectId;
    t_Rel_r.Flag       := a_Flag;
    a_Rel_tr(a_RelationshipId) := t_Rel_r;
  end InsertRelData;

  /*--------------------------------------------------------------------------
   * CompareRels() -- private
   *------------------------------------------------------------------------*/
  procedure CompareRels (
    a                   tr_Rel,
    b                   in out nocopy tr_Rel,
    a_Changed           in out nocopy boolean
  ) is
    i pls_integer;
    t boolean;
    u boolean;
  begin
    u := a.count != b.count;
    i := a.first;
    while i is not null loop
      t := b.exists(i);
      if t
        then b(i).flag := 'E'; --Exists
        else b(i) := a(i);
      end if;
      i := a.next(i);
    end loop;
    /* test for changes.
      If we come in and leave with the counts being identical then nothing has changed
    */
    a_Changed := (a.count != b.count) != u;
  end CompareRels;

  /*--------------------------------------------------------------------------
   * ListFromString()
   *------------------------------------------------------------------------*/
  function ListFromString (
    a_String                        varchar2,
    a_Delimiter                     varchar2 default ','
  ) return udt_StringList is
    t_Counter                       number := 0;
    t_StringList                    udt_StringList;
    t_String                        varchar2(32700); /* allow some room for a 67 character delimitter... ridiculous but why not :) */
    t_String2                       varchar2(32767);
    t_Value                         varchar2(32767);
  begin
    if substr(a_String, -1) <> a_Delimiter then
      t_String := a_String || a_Delimiter;
    else
      t_String := a_String;
    end if;

    while t_String is not null loop
      ToolBox.pkg_Parse.GetNextToken(t_String, a_Delimiter, t_Value, t_String);
      if t_Value is not null then
        t_Counter := t_Counter + 1;
        t_StringList(t_Counter) := t_Value;
      end if;
    end loop;

    return t_StringList;
  end;

  /*---------------------------------------------------------------------------
   * ToEndPointIdfromName() -- private
   * returns the ToEndpointId for a given relationship name
   *-------------------------------------------------------------------------*/
  function ToEndPointIdfromName (
    a_RelName varchar2,
    a_ObjectDefId number
  ) return number is
      t_Id number(9);
    begin
      select ToEndPointId
        into t_Id
        from api.relationshipdefs
       where name = a_RelName
         and fromobjectdefid=a_ObjectDefId;
      return (t_Id);
    end;

  /*---------------------------------------------------------------------------
   * GetRelValues() -- private
   *-------------------------------------------------------------------------*/
  procedure GetRelValues (
    a_ObjectId                  udt_Id,
    a_ObjectDefId               udt_Id,
    a_AuditRelationships        varchar2,
    a_Rel_tr                    in out nocopy tr_Rel
  ) is
    t_ToEndpointIdList          udt_NumberList;
  begin
    if a_AuditRelationships = '%' then
      select rd.ToEndPointId
        bulk collect
        into t_ToEndpointIdList
        from api.ObjectDefs od
        join api.RelationshipDefs rd
          on rd.RelationshipDefId = od.ObjectDefid
       where od.ObjectDefTypeid = 4
         and rd.FromObjectDefid = a_ObjectDefId;
    else
      select rd.ToEndPointId
      bulk collect into t_ToEndpointIdList
      from
        api.ObjectDefs od
        join api.RelationshipDefs rd
          on rd.RelationshipDefId = od.ObjectDefid
      where od.ObjectDefTypeid = 4
        and rd.FromObjectDefid = a_ObjectDefId
        and instr(a_AuditRelationships, rd.name ) > 0;
    end if;

    for i in 1..t_ToEndpointIdList.count loop
      for r in (select r.RelationshipId, r.ToObjectId
                  from api.Relationships r
                 where r.FromObjectid = a_ObjectId
                   and r.EndPointId = t_ToEndpointIdList(i)) loop
        InsertRelData(r.RelationshipId, t_ToEndpointIdList(i), r.ToObjectId, 'N', a_Rel_tr);
      end loop;
    end loop;
  end;

  /*---------------------------------------------------------------------------
   * GetOldRelValues() -- private
   *-------------------------------------------------------------------------*/
  procedure GetOldRelValues (
    a_ObjectId                  udt_Id,
    a_ObjectDefId               udt_Id,
    a_AuditRelationships        varchar2,
    a_Rel_tr                    in out nocopy tr_Rel
  ) is
    t_ToEndpointIdList          udt_NumberList;

  begin
    if a_AuditRelationships = '%' then
      select rd.ToEndPointId
        bulk collect
        into t_ToEndpointIdList
        from api.ObjectDefs od
        join api.RelationshipDefs rd
          on rd.RelationshipDefId = od.ObjectDefid
       where od.ObjectDefTypeid = 4
         and rd.FromObjectDefid = a_ObjectDefId;
    else
      select rd.ToEndPointId
      bulk collect into t_ToEndpointIdList
      from
        api.ObjectDefs od
        join api.RelationshipDefs rd
          on rd.RelationshipDefId = od.ObjectDefid
      where od.ObjectDefTypeid = 4
        and rd.FromObjectDefid = a_ObjectDefId
        and instr(a_AuditRelationships, rd.name ) > 0;
    end if;

    for i in 1..t_ToEndpointIdList.count loop
      for r in (select od.RelationshipId, od.ToObjectId, rd.FromEndPointId
                  from extension.objectauditrelationshipdata_t od
                  join extension.ObjectAudit oa
                    on od.ObjectAuditId = oa.ObjectAuditId
                  join api.relationshipdefs rd on rd.ToEndPointId = t_ToEndPointIdList(i)
                 where od.ToEndPointId = t_ToEndPointIdList(i)
                   and oa.ObjectAuditId = (select max(oa2.ObjectAuditId)
                                             from ObjectAudit oa2
                                            where oa2.ObjectId = a_ObjectId)
                   and od.flag in ('N','E')) loop
        InsertRelData(r.RelationshipId, t_ToEndpointIdList(i), r.ToObjectId, 'D', a_Rel_tr);
       end loop;
    end loop;

  end;

  /*---------------------------------------------------------------------------
   * AuditConstructor() -- PUBLIC
   *   Attach this to the constructor of an objecttype
   *-------------------------------------------------------------------------*/
  procedure AuditConstructor (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date
  ) is
  begin
    g_ConstructorLogicalTransIds(a_ObjectId) := api.pkg_LogicalTransactionUpdate.CurrentTransaction;
  end;

  /*---------------------------------------------------------------------------
   * GetValues() -- private
   * returns the current Column Values for the given column names in a_ColumnDefList
   *-------------------------------------------------------------------------*/
  function GetValues (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_ColumnDefList             udt_StringList
  ) return udt_StringList is
    t_ValueList                 udt_StringList;
  begin
    for i in 1..a_ColumnDefList.count loop
      t_ValueList(i) := api.pkg_ColumnQuery.Value(a_ObjectId, a_ColumnDefList(i), a_AsOfDate);
    end loop;
    return t_ValueList;
  end;

  /*---------------------------------------------------------------------------
   * GetOldValues() -- private
   * returns the logged Column Values for the given column names in a_ColumnDefList
   *-------------------------------------------------------------------------*/
  function GetOldValues (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_ColumnDefList             udt_StringList
  ) return udt_StringList is
    t_ValueList                 udt_StringList;
  begin
    for i in 1..a_ColumnDefList.count loop
      begin
        select od.NewColumnValue
          into t_ValueList(i)
          from ObjectAuditData od
          join ObjectAudit oa
            on od.ObjectAuditId = oa.ObjectAuditId
         where oa.ObjectAuditId = (select max(oa2.ObjectAuditId) from ObjectAudit oa2 where oa2.ObjectId = a_ObjectId)
           and od.ColumnName = a_ColumnDefList(i);
      exception when no_data_found then
        t_ValueList(i) := null;
      end;
    end loop;
    return t_ValueList;
  end;

  /*---------------------------------------------------------------------------
   * GetColumnDefs() -- private
   *-------------------------------------------------------------------------
  function GetColumnDefs (
    a_ObjectDefId               udt_Id,
    a_AuditColumns              varchar2,
    a_Omittal                   varchar2
  ) return udt_StringList is
    t_ColumnDefList             udt_StringList;
    t_AuditColumnList           udt_StringList;
    t_j                         udt_Id;
  begin
    if a_AuditColumns = '%'  then
      select Name
        bulk collect into t_ColumnDefList
        from api.ColumnDefs
       where ObjectDefId = a_ObjectDefId
--         and Enterable = 'Y'  enterable is no longer a reliable indicator as of POSSE V6 used for now so official doc flag did not need to be set
         and PrintOnOfficialDocument = 'Y'
         and ( (lower(label) not like lower(a_Omittal)||'%'
                  and
                a_Omittal is not null
                )
               or a_Omittal is null
             );
    else
      t_ColumnDefList := ListFromString(a_AuditColumns);
      for i in 1..t_ColumnDefList.count loop
        begin
          select columndefid into t_j
            from api.columndefs
           where objectdefid=a_ObjectDefId
             and name=t_ColumndefList(i);
        exception when no_data_found
            then begin
              api.pkg_Errors.Clear;
              api.pkg_Errors.SetArgValue('ColName', t_ColumndefList(i));
              api.pkg_Errors.SetArgValue('ObjectDefId', a_ObjectDefId);
              api.pkg_Errors.SetArgValue('ProcedureArgument', a_AuditColumns);
              api.pkg_Errors.RaiseError(-20000, 'Invalid column name configured on audit procedure: {ColName} for {ObjectDefId}');
            end;
        end;
      end loop;
    end if;
    return t_ColumnDefList;
  end;
  */
  -- new 21/12/2006
  procedure GetColumnDefs (
    a_ObjectDefId               udt_Id,
    a_AuditColumns              varchar2,
    a_Omittal                   varchar2,
    a_ColumnDefList             out nocopy udt_StringList,
    a_ColumnDefIDList           out nocopy udt_NumberList
  )  is
    t_j                         udt_Id;
  begin
    if a_AuditColumns = '%' then
      select Name,columndefid
        bulk collect into a_ColumnDefList,a_ColumnDefIDList
        from api.ColumnDefs
       where ObjectDefId = a_ObjectDefId
--         and Enterable = 'Y' --enterable is no longer a reliable indicator as of POSSE V6
         and PrintOnOfficialDocument = 'Y'
         and ( (lower(label) not like lower(a_Omittal)||'%'
                  and
                a_Omittal is not null
                )
               or a_Omittal is null
             );
    else
      a_ColumnDefList := ListFromString(a_AuditColumns);
      for i in 1..a_ColumnDefList.count loop
        begin
          select columndefid into t_j
            from api.columndefs
           where objectdefid=a_ObjectDefId
             and name=a_ColumndefList(i);
           a_ColumnDefIDList(i) := t_j;
        exception when no_data_found
            then begin
              api.pkg_Errors.Clear;
              api.pkg_Errors.SetArgValue('ColName', a_ColumndefList(i));
              api.pkg_Errors.SetArgValue('ObjectDefId', a_ObjectDefId);
              api.pkg_Errors.SetArgValue('ProcedureArgument', a_AuditColumns);
              api.pkg_Errors.RaiseError(-20000, 'Invalid column name configured on audit procedure: {ColName} for {ObjectDefId}');
            end;
        end;
      end loop;
    end if;

  end;

  /*---------------------------------------------------------------------------
   * WriteColumnData() -- private
   * writes the column audit data to the appropriate table
   *-------------------------------------------------------------------------*/
  procedure WriteColumnData (
    a_AuditType                 pls_integer,
    a_ObjectAuditId             udt_Id,
    a_ColumnDefs                udt_StringList,
    a_ColumnDefIDs              udt_NumberList, -- new 21/12/2006
    a_NewValues                 udt_StringList,
    a_OldValues                 udt_StringList
  ) is
  begin
    if a_AuditType in (g_AuditNew, g_AuditChange) then
      for i in 1..a_NewValues.count loop
        if a_AuditType = g_AuditNew then
          insert into ObjectAuditData (
            ObjectAuditId,
            ColumnName,
            ColumnDefId, -- new 21/12/2006
            NewColumnValue
          ) values (
            a_ObjectAuditId,
            a_ColumnDefs(i),
            a_ColumnDefIDs(i),
            a_NewValues(i)
          );
        elsif a_AuditType = g_AuditChange  then
          insert into ObjectAuditData (
            ObjectAuditId,
            ColumnName,
            ColumnDefId, -- new 21/12/2006
            OldColumnValue,
            NewColumnValue
          ) values (
            a_ObjectAuditId,
            a_ColumnDefs(i),
            a_ColumnDefIDs(i),
            a_OldValues(i),
            a_NewValues(i)
          );
        end if;
      end loop;
    elsif a_AuditType = g_AuditRemove then
      for i in 1..a_OldValues.count loop
        insert into ObjectAuditData (
          ObjectAuditId,
          ColumnName,
          ColumnDefId, -- new 21/12/2006
          OldColumnValue
        ) values (
          a_ObjectAuditId,
          a_ColumnDefs(i),
          a_ColumnDefIDs(i),
          a_OldValues(i)
        );
      end loop;
    end if;
  end;


  /*---------------------------------------------------------------------------
   * WriteRelationshipData() -- private
   * writes the relatinoship audit data to the appropriate table
   *-------------------------------------------------------------------------*/
  procedure WriteRelationshipData(
    a_ObjectAuditId             udt_Id,
    a_Rel_tr                    tr_Rel
  ) is
    i pls_integer;
  begin
    i := a_Rel_tr.first;
    while i is not null loop

      insert into ObjectAuditRelationshipData_t (
        ObjectAuditId,
        RelationshipId,
        ToEndpointId,
        ToObjectId,
        Flag
      ) values (
        a_ObjectAuditId,
        i,
        a_Rel_tr(i).EndpointId,
        a_Rel_tr(i).ToObjectId,
        a_Rel_tr(i).Flag
      );

      i := a_Rel_tr.next(i);

    end loop;
  end;

  /*---------------------------------------------------------------------------
   * Audit() -- PUBLIC
   * oversees the details of the audit
   *-------------------------------------------------------------------------*/
  procedure Audit (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_AuditColumns              varchar2,
    a_AuditRelationshipNames    varchar2,
    a_Omittal                   varchar2,
    a_AuditType                 pls_integer
  ) is
    t_OldValues                 udt_StringList;
    t_NewValues                 udt_StringList;
    t_ColumnDefs                udt_StringList;
    t_ColumnDefIDs              udt_NumberList;
    t_Changed                   boolean := false;
    t_ObjectAuditId             udt_Id;
    t_ObjectDefId               udt_Id;
    t_ObjectDefName             varchar2(30);
    t_ExternalFileNum           varchar2(40);
    t_OldRel_tr                 tr_Rel;
    t_CurrRel_tr                tr_Rel;
    t_startaudit                date;
    t_endaudit                  date;
  begin
/* This next line can be deleted when the need to time audits is over*/
 --   select sysdate into t_StartAudit from dual;
/* The previous line can be deleted when the need to time audits is over*/
    begin
      select ObjectDefId,
             ExternalFileNum
        into t_ObjectDefId,
             t_ExternalFileNum
        from api.Objects
       where ObjectId = a_ObjectId; /*should we limit this to objectdeftypeid 1,2,3? */
    exception when no_data_found then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('ObjectId', a_ObjectId);
      api.pkg_Errors.RaiseError(-20000, 'Invalid Object Id: {ObjectId} encountered attempting to perform object auditing.');
    end;
    /* Retrieve column names  @pkg_objectquery.sql; */
--    t_ColumnDefs := GetColumnDefs(t_ObjectDefId, a_AuditColumns, a_Omittal);
    GetColumnDefs(t_ObjectDefId, a_AuditColumns, a_Omittal, t_ColumnDefs, t_ColumnDefIDs);

    /* Get New (or current) Values for columns and relationships */
    if a_AuditType in (g_AuditNew, g_AuditChange) then
      t_NewValues := GetValues(a_ObjectId, a_AsOfDate, t_ColumnDefs);
      GetRelValues(a_ObjectId, t_ObjectDefId, a_AuditRelationshipNames, t_CurrRel_tr);
    end if;

    /* Get Old (or logged) Values for columns and relationships */
    if a_AuditType in (g_AuditChange, g_AuditRemove) then
      t_OldValues := GetOldValues(a_ObjectId, a_AsOfDate, t_ColumnDefs);
      GetOldRelValues(a_ObjectId, t_ObjectDefId, a_AuditRelationshipNames, t_OldRel_tr);
    end if;

    /* Create Composite Rel record and determine if rels changed */
    CompareRels(t_CurrRel_tr, t_OldRel_tr, t_Changed);

    if a_AuditType = g_AuditChange then
      -- Determine if any columns have actually changed
      for i in 1..t_OldValues.count loop
        if (t_OldValues(i) <> t_NewValues(i))
           or (t_OldValues(i) is null and t_NewValues(i) is not null)
           or (t_NewValues(i) is null and t_OldValues(i) is not null) then
          t_Changed := true;
          exit;
        end if;
      end loop;
    end if;

    select Name
      into t_ObjectDefName
      from api.ObjectDefs
     where ObjectDefId = t_ObjectDefId;
    if t_Changed or a_AuditType in (g_AuditNew, g_AuditRemove) then
      select ObjectAudit_s.nextval
        into t_ObjectAuditId
        from dual;
      if t_ObjectDefName is null then
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('ObjectDefId', t_ObjectDefId);
        api.pkg_Errors.RaiseError(-20000, 'No view name specified for object def: {ObjectDefId}. A view name must be specified for Object Auditing.');
      end if;
      insert into ObjectAudit (
        ObjectAuditId,
        AuditType,
        ObjectDefName,
        Objectdefid, -- new 21/12/2006
        ObjectId,
        AsOfDate,
        ExternalFileNum,
        LogicalTransactionId,
        AuditDate,
        EffectiveUser
      ) values (
        t_ObjectAuditId,
        decode(a_AuditType, g_AuditNew, 'N', g_AuditChange, 'C', g_AuditRemove, 'R'),
        t_ObjectDefName,
        t_ObjectDefId,
        a_ObjectId,
        a_AsOfDate,
        t_ExternalFileNum,
        api.pkg_LogicalTransactionUpdate.CurrentTransaction,
        sysdate,
        api.pkg_SecurityQuery.EffectiveUser
      );
      /* Skip the unecessary operation if applicable */
      if a_AuditColumns is not null
        then WriteColumnData(a_AuditType, t_ObjectAuditId, t_ColumnDefs, t_ColumnDefIDs, t_NewValues, t_OldValues);
      end if;
      if a_AuditRelationshipNames is not null
        then WriteRelationshipData(t_ObjectAuditId, t_OldRel_tr);
      end if;
    end if;
/* The next 2 lines can be deleted when the need to time audits is over*/
--    select sysdate into t_EndAudit from dual;
--    insert into audittiming_t (ObjectAuditId, AuditSTart, AuditEnd) values (t_ObjectAuditId, t_StartAudit, t_EndAudit);
/* The previous 2 lines can be deleted when the need to time audits is over
   As well... remove the table AuditTiming_t
*/

  end;

  /*---------------------------------------------------------------------------
   * AuditVerify() -- PUBLIC
   *   Attach this to the Post Verify of an objecttype
   *-------------------------------------------------------------------------*/
  procedure AuditVerify (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_AuditColumns              varchar2,
    a_AuditRelationshipNames    varchar2,
    a_OmitPrefix                varchar2 default null
  ) is
    t_AuditType                 pls_integer := g_AuditChange;
  begin
    if a_AuditRelationshipNames is null and a_AuditColumns is null then return;
       /* I don't know why we are calling this procedure if the above is true! */
    end if;
    if g_ConstructorLogicalTransIds.exists(a_ObjectId) then
      if g_ConstructorLogicalTransIds(a_ObjectId) = api.pkg_LogicalTransactionUpdate.CurrentTransaction then
        t_AuditType := g_AuditNew;
        -- clear the constructo logical transaction id for this objectid as we don't want to do
        -- mulitple New entries as may happen under the Outrider COM
        g_ConstructorLogicalTransIds.delete(a_ObjectId);
      end if;
    end if;
    Audit(a_ObjectId, a_AsOfDate, a_AuditColumns,a_AuditRelationshipNames, a_OmitPrefix, t_AuditType);
  end;

  /*---------------------------------------------------------------------------
   * AuditDestructor() -- PUBLIC
   *   Attach this to the Destructor of an objecttype
   *-------------------------------------------------------------------------*/
  procedure AuditDestructor (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_AuditColumns              varchar2,
    a_AuditRelationshipNames    varchar2,
    a_OmitPrefix                varchar2 default null
  ) is
  begin
    if a_AuditRelationshipNames is null and a_AuditColumns is null then return;
       /* I don't know why we are calling this procedure if the above is true! */
    end if;
    Audit(a_ObjectId, a_AsOfDate, a_AuditColumns,a_AuditRelationshipNames,a_OmitPrefix,g_AuditRemove);
  end;

  /*---------------------------------------------------------------------------
   * CleanupOldData() -- PUBLIC
   *   Cleans up data from the audit tables based on the retention days in the constants
   * table
   *-------------------------------------------------------------------------*/
  procedure CleanupOldData is
    t_RetentionDays integer;
    begin
      t_RetentionDays := api.pkg_constantquery.Value('OBJECTLOGGING_RETENTION_DAYS');
      if t_RetentionDays is not null then
        for c in (select objectauditid from objectaudit where auditdate < trunc(sysdate) - t_Retentiondays)
          loop
            delete from objectauditdata where objectauditid = c.objectauditid;
            delete from objectauditrelationshipdata where objectauditid = c.objectauditid;
            delete from objectaudit where objectauditid = c.objectauditid;
        end loop;
      end if;
    end;
end;
/

