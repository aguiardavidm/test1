create or replace package           PKG_HRM is

  -- Author  : Trevor Hamm
  -- Created : 9/16/2008
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_definition.udt_StringList;
  subtype udt_NumberList is api.pkg_definition.udt_NumberList;
  type udt_Cursor is ref cursor;
  type udt_StringAssocArray is table of api.pkg_definition.udt_Id index by varchar2(4000);
  /*--------------------------------------------------------------------------
  * ProcessListedLands()
  *     Creates and Relates Listed Lands objects based on records found in the
  *     HistResources table matching the Proposed Development Area on the
  *     source object.
  *------------------------------------------------------------------------*/
procedure ProcessListedLands (
  a_ObjectId       udt_Id,
  a_AsOfDate       date
);

  /*--------------------------------------------------------------------------
  * CopyMatchingColumns()
  *     Copies column values from a source object to a target object.
  *     Columns are stored (non-system), non-dupe, and non-'zzz'd
  *------------------------------------------------------------------------*/
  procedure CopyMatchingColumns(a_SourceObjectId     pls_integer,
                                a_TargetObjectId     pls_integer
  );

  /*--------------------------------------------------------------------------
  * CopyDetail()- Copy the value found in ColumnNameA of ObjectAId
  *     to a_ObjectBId.  a_ColumnNameB may be specified if the column on B
  *     is named differently.
  *------------------------------------------------------------------------*/
  procedure CopyDetail(
    a_ObjectAId                         udt_Id,
    a_ColumnNameA                       varchar2,
    a_ObjectBId                         udt_Id,
    a_ColumnNameB                       varchar2 default null
  );

 /*--------------------------------------------------------------------------
  * CloneRelsToRelatedObjects() - Clone all the relationships found
  *     at a_EndPointNameA of a_ObjectAId to a_ObjectBId.  If the end point
  *     is named differently on a_ObjectBId, then a_EndPointNameB may be
  *     specified.  Otherwise it will be assumed the endpointname is the same
  *     on both (i.e. a_EndPointNameA).
  *------------------------------------------------------------------------*/
  procedure CloneRelsToRelatedObjects(
    a_ObjectAId                         udt_Id,
    a_EndPointNameA                     varchar2,
    a_ObjectBId                         udt_Id,
    a_EndPointNameB                     varchar2 default null
  );

 /*--------------------------------------------------------------------------
  * CloneRelsIfNew() - Essentially the same as CloneRelsToRelatedObjects
  *     except it will not create relationships to objects that are already
  *     related to ObjectBId at EndPointNameB.
  *------------------------------------------------------------------------*/
  procedure CloneRelsIfNew(
    a_ObjectAId                         udt_Id,
    a_EndPointNameA                     varchar2,
    a_ObjectBId                         udt_Id,
    a_EndPointNameB                     varchar2 default null
  );
 /*--------------------------------------------------------------------------
  * CloneRelatedObjects() - Clone all the related objects found at
  *     EndPointNameA of ObjectAId and relate them a_ObjectBId.  If the end
  *     point is named differently on a_ObjectBId, then a_EndPointNameB may be
  *     specified.  Otherwise it will be assumed the endpointname is the same
  *     on both (i.e. a_EndPointNameA).
  *------------------------------------------------------------------------*/
  procedure CloneRelatedObjects(
    a_ObjectAId                         udt_Id,
    a_EndPointNameA                     varchar2,
    a_ObjectBId                         udt_Id,
    a_EndPointNameB                     varchar2 default null
  );

 /*--------------------------------------------------------------------------
  * CloneNewRelatedObjects()
  *     Copy the only the objects related to ObjectIdA at EndpointNameA that
  *  don't yet exist at EndPointNameB of ObjectIdB where existance is
  *  determined by an object existing with the same value in a given column.
  *  If you wish to compare multiple columns then concatenate them.
  *------------------------------------------------------------------------*/
  procedure CloneNewRelatedObjects(
    a_ObjectAId                         udt_Id,
    a_EndPointNameA                     varchar2,
    a_ObjectBId                         udt_Id,
    a_EndPointNameB                     varchar2,
    a_ColumnName                        varchar2
  );

end PKG_HRM;

 
/

grant execute
on pkg_hrm
to posseextensions;

