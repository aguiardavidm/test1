create or replace package pkg_DashboardDefinition is

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_NumberList               is api.pkg_Definition.udt_NumberList;
  subtype udt_StringList               is api.pkg_Definition.udt_StringList;

  type udt_CorralTable is record (
    schema                              varchar2(30),
    name                                varchar2(30),
    IdName                              varchar2(30)
  );

  type udt_Gauge is record (
    min                                number(12,1) := 0,
    max                                number(12,1) := 0,
    alert                              pls_integer  := 0, --{0,1,2}
    pct                                varchar2(5)  := 'false'
  );

  type udt_Data is record (
    x                                  varchar2(100),
    y                                  udt_NumberList
  );

  type udt_DataList                    is table of udt_Data index by binary_integer;

  type udt_UrlParameters               is table of varchar2(4000) index by varchar2(4000);

  type udt_Widget is record (
    widgetid                           pls_integer,
    widgettype                         varchar2(200),
    title                              varchar2(400),
    displaytype                        varchar2(100),
    accessgroup                        varchar2(200),
    xaxislabel                         varchar2(100),
    yaxislabel                         varchar2(100),
    defaultperiod                      varchar2(100),
    timeperioddetail                   varchar2(100),
    period                             varchar2(100),
    fromdate_                          varchar2(10), --'mm/dd/yyyy'
    todate_                            varchar2(10), --'mm/dd/yyyy'
    fromdate                           date,
    todate                             date,
    view_                              varchar2(30),
    users_                             varchar2(4000),
    subgroup                           varchar2(4000),
    grouplabels                        udt_StringList,
    groupvalues                        udt_StringList,
    displaytwogroups                   char(1),
    maxyvalue                          number,
    drilldownurl                       varchar2(4000),
    fieldnames                         udt_StringList,
    serieslabels                       udt_StringList,
    gauge                              udt_Gauge,
    data                               udt_DataList,
    dashboardwidgetxrefid              pls_integer,
    category                           varchar2(60),
    visible                            varchar2(1)
  );

  type udt_WarningLevel is record (
    key                                number,
    val                                varchar2(100)
  );

  type tbl_WarningLevel is table of udt_WarningLevel;

  type udt_NumberListByString is table of number index by varchar2(100);

  type udt_DataMatrix is table of udt_NumberListByString index by varchar2(100);

  type udt_PeriodCalc is record (
    PeriodName                          varchar2(30),
    Value                               number
  );

  type udt_PeriodCalcList is table of udt_PeriodCalc index by binary_integer;

  /*--------------------------------------------------------------------------
   * Global Constants
   *------------------------------------------------------------------------*/
  -- values for udt_Widget.displaytype
  gc_Timeline				constant varchar2(10) := 'Timeline';
  gc_BarGraph				constant varchar2(10) := 'Bar Graph';
  gc_Gauge				constant varchar2(10) := 'Gauge';

  -- values for udt_Widget.view_
  gc_Compare                            constant varchar2(10) := 'Compare';
  gc_Detailed                           constant varchar2(10) := 'Detailed';
  gc_Summary                            constant varchar2(10) := 'Summary';

  -- values for Function Name
  gc_Count                              constant varchar2(10) := 'Count';
  gc_Sum                                constant varchar2(10) := 'Sum';
  gc_Average                            constant varchar2(10) := 'Average';

end pkg_DashboardDefinition;
 
/

