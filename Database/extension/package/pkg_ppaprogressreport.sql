create or replace package           pkg_PPAProgressReport is

  -- Author  : CARL.BRUINSMA
  -- Created : 11/27/2008 16:18:48
  -- Purpose : Procedures needed for the Parks Progress Report job

  subtype udt_Id is api.pkg_Definition.udt_Id;

/*-----------------------------------------------------------------------------
 * validateProgressReport - Ensure all mandatory columns have values and a
 *   permit is related
 *----------------------------------------------------------------------------*/
  procedure validateProgressReport(
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

/*-----------------------------------------------------------------------------
 * SendEmail - Create a send email process and populate the necessary
 *   fields.
 *----------------------------------------------------------------------------*/
  procedure SendEmail(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );
end pkg_PPAProgressReport;

 
/

grant execute
on pkg_ppaprogressreport
to posseextensions;

