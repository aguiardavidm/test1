create or replace package pkg_SecurityUtils as

  -- Author  : RYANM
  -- Created : 1/20/2011 4:17:13 PM
  -- Purpose : Security Utility
  
  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * IsMemberOfAccessGroup() -- PUBLIC
   *   Return whether or not the given User is in the given AccessGroup.
   *-------------------------------------------------------------------------*/
  function IsMemberOfAccessGroup(
    a_AccessGroupId     udt_Id,
    a_UserId            udt_Id
  ) return boolean;

  /*---------------------------------------------------------------------------
   * IsMemberOfAccessGroup() -- PUBLIC
   *   Return whether or not the given User is in the given AccessGroup.
   * This overload lets you specify an AccessGroup's name rather than its Id.
   *-------------------------------------------------------------------------*/
  function IsMemberOfAccessGroup(
    a_AccessGroupName   varchar2,
    a_UserId            udt_Id
  ) return boolean;

end pkg_SecurityUtils;

 
/

create or replace package body pkg_SecurityUtils as

  /*---------------------------------------------------------------------------
   * IsMemberOfAccessGroup() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function IsMemberOfAccessGroup(
    a_AccessGroupId     udt_Id,
    a_UserId            udt_Id
  ) return boolean is
    t_Count             number(9);
  begin
    select count(UserId)
      into t_Count
      from api.AccessGroupUsers
      where AccessGroupId = a_AccessGroupId
        and UserId = a_UserId;
    return t_Count > 0;
  end IsMemberOfAccessGroup;

  /*---------------------------------------------------------------------------
   * IsMemberOfAccessGroup() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function IsMemberOfAccessGroup(
    a_AccessGroupName   varchar2,
    a_UserId            udt_Id
  ) return boolean is
    t_AccessGroupId     udt_Id;
  begin
    begin
      select AccessGroupId
        into t_AccessGroupId
        from api.AccessGroups
        where Description = a_AccessGroupName;
    exception
    when no_data_found then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('AccessGroup', a_AccessGroupName);
      api.pkg_Errors.RaiseError(-20000,
          'The access group "{AccessGroup}" does not exist.');
    end;
    return IsMemberOfAccessGroup(t_AccessGroupId, a_UserId);
  end IsMemberOfAccessGroup;

end pkg_SecurityUtils;


/

