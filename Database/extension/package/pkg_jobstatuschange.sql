create or replace package pkg_JobStatusChange is

  /*--------------------------------------------------------------------------
  * Description
  *   Create a history of when a job changes status. Unassign assigned
  * processes when a job enters a cancelled status. Configured as a procedure
  * to run on post verify of all jobs.
  *------------------------------------------------------------------------*/

  /*--------------------------------------------------------------------------
  * Types
  *------------------------------------------------------------------------*/
  subtype udt_id is api.pkg_Definition.udt_Id;

  /*--------------------------------------------------------------------------
  * StatusChange()
  *------------------------------------------------------------------------*/
  procedure StatusChange(a_JobId      udt_Id,
                         a_AsOfDate   date,
                         a_StatusName varchar2,
                         a_StatusDate  date);


  /*--------------------------------------------------------------------------
  * StatusDateChange() -- PUBLIC
  *------------------------------------------------------------------------*/
  procedure StatusDateChange(a_JobId      udt_Id,
                             a_AsOfDate   date,
                             a_StatusName varchar2,
                             a_StatusDate date);

end pkg_JobStatusChange;
 
/

create or replace package body pkg_JobStatusChange is

  /*--------------------------------------------------------------------------
  * StatusChange() -- PUBLIC
  *------------------------------------------------------------------------*/
  procedure StatusChange(a_JobId      udt_Id,
                         a_AsOfDate   date,
                         a_StatusName varchar2,
                         a_StatusDate date) is
    t_CurrentStatusName varchar2(6);
    t_JobStatusChangeId number;
    t_JobStatusType     char(1);
    t_JobTypeId         udt_id;
    t_Count             pls_integer;
  begin
    begin
      select jsc.StatusName,
             jsc.JobStatusChangeId,
             js.StatusType,
             j.JobTypeId
        into t_CurrentStatusName,
             t_JobStatusChangeId,
             t_JobStatusType,
             t_JobTypeId
        from JobStatusChange jsc, api.Jobs j, api.Jobstatuses js
       where jsc.Jobid = a_JobId
         and jsc.MostRecent = 'Y'
         and j.JobId = a_JobId
         and j.JobTypeId = js.JobTypeId
         and j.StatusId = js.StatusId;

      if a_StatusName <> t_CurrentStatusName then
        insert into JobStatusChange
          (JobStatusChangeId,
           JobId,
           StatusName,
           StatusType,
           EffectiveStartDate,
           EffectiveEndDate,
           MostRecent,
					 EffectiveUser,
           StatusDate)
        values
          (JobStatusChange_s.NextVal,
           a_jobid,
           a_StatusName,
           t_JobStatusType,
           sysdate,
           null,
           'Y',
					 api.pkg_SecurityQuery.EffectiveUser,
           a_StatusDate);

        update jobstatuschange
           set effectiveenddate = sysdate, mostrecent = 'N'
         where jobstatuschangeid = t_JobStatusChangeId;

         --Set Effective Date (Status Date) to the sysdate
     if api.pkg_columnquery.Value(a_JobId,'ObjectDefName') = 'j_LicenseType' then
         api.pkg_columnupdate.SetValue(a_Jobid,'StatusDate',sysdate);
     end if;
        /* Clear the completed date when a job changes from completed or
        canceled type to an non completed type */
        if t_JobStatusType in ('I', 'O') then
          select count(*)
            into t_Count
            from api.JobStatuses js, api.Statuses s
           where s.Tag = t_CurrentStatusName
             and js.JobTypeId = t_JobTypeId
             and js.StatusId = s.StatusId
             and js.StatusType in ('X', 'C');

          if t_Count > 0 then
            api.pkg_ColumnUpdate.RemoveValue(a_JobId, 'CompletedDate');
          end if;
        end if;
      end if;
    exception
      when no_data_found then
        select jsta.StatusType
          into t_JobStatusType
          from api.jobstatuses jsta
          join api.statuses sta on jsta.StatusId = sta.StatusId
         where sta.Tag = a_StatusName
         and jsta.JobTypeId = (select JobTypeId from api.jobs where JobId = a_JobId);
        insert into JobStatusChange
          (JobStatusChangeId,
           JobId,
           StatusName,
           StatusType,
           EffectiveStartDate,
           EffectiveEndDate,
           MostRecent,
					 EffectiveUser,
           StatusDate)
        values
          (JobStatusChange_s.NextVal,
           a_jobid,
           a_StatusName,
           t_JobStatusType,
           sysdate,
           null,
           'Y',
					 api.pkg_SecurityQuery.EffectiveUser,
           a_StatusDate);

         --Set Effective Date (Status Date) to the sysdate
       if api.pkg_columnquery.Value(a_JobId,'ObjectDefName') = 'j_LicenseType' then
         api.pkg_columnupdate.SetValue(a_Jobid,'StatusDate',sysdate);
      end if;
    end;

  end;

  /*--------------------------------------------------------------------------
  * StatusDateChange() -- PUBLIC
  *------------------------------------------------------------------------*/
  procedure StatusDateChange(a_JobId      udt_Id,
                             a_AsOfDate   date,
                             a_StatusName varchar2,
                             a_StatusDate date) is

  t_EffectiveStartDate       date;
  t_StatusChangeId           udt_Id;
  t_StatusType               varchar2(1);

  begin
    begin
      select jsc.JobStatusChangeId,
             jsc.StatusType,
             jsc.EffectiveStartDate
        into t_StatusChangeId,
             t_StatusType,
             t_EffectiveStartDate
        from JobStatusChange jsc
       where jsc.JobId = a_JobId
         and jsc.MostRecent = 'Y';

      -- if the Status is changed then update the
      --if a_StatusName != t_StatusName then
      /*--This line of Code is not needed anymore since we are updating the Most recent
        --with the Current Effective Date (Status Date) everytime this procedure runs. 5/26/2010
      insert into JobStatusChange_t(JobStatusChangeId,
                                    JobId,
                                    StatusName,
                                    EffectiveStartDate,
                                    EffectiveEndDate,
                                    MostRecent,
                                    StatusType,
                                    EffectiveUser,
                                    StatusDate)
                             values(JobStatusChange_s.NextVal,
                                    a_JobId,
                                    a_StatusName,
                                    --t_EffectiveStartDate,
                                    sysdate,
                                    null,
                                    'Y',
                                    t_StatusType,
                                    api.pkg_SecurityQuery.EffectiveUser,
                                    a_StatusDate);*/

      update JobStatusChange_t
         --set EffectiveEndDate = t_EffectiveStartDate, MostRecent = 'N'
         --set EffectiveEndDate = sysdate, MostRecent = 'N'
         set StatusDate = a_StatusDate
       where JobStatusChangeId = t_StatusChangeId;

    exception
    when no_data_found then

      insert into JobStatusChange
          (JobStatusChangeId,
           JobId,
           StatusName,
           StatusType,
           EffectiveStartDate,
           EffectiveEndDate,
           MostRecent,
					 EffectiveUser,
           StatusDate)
        values
          (JobStatusChange_s.NextVal,
           a_jobid,
           a_StatusName,
           'I',
           sysdate,
           null,
           'Y',
					 api.pkg_SecurityQuery.EffectiveUser,
           a_StatusDate);
      when others then
      api.pkg_Errors.RaiseError(-20000, SQLERRM||'. Problem with selecting from JobStatusChange table.');
    end;
  end StatusDateChange;


end pkg_JobStatusChange;
/

