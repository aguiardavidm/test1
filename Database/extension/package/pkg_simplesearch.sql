create or replace package pkg_SimpleSearch as

  /*------------------------------------------------------------------------*
   * Types declaration
   *------------------------------------------------------------------------*/
  subtype udt_ID      is api.pkg_Definition.udt_ID;

  /*-------------------------------------------------------------------------------------
  * ObjectsByIndex() -- PUBLIC
  * Arguments --
  * a_ObjectDefName: the name of the object to be searched, eg. 'o_Course'
  * a_ColumnName: the name of the indexed column to search on: eg. 'Name'
  * a_LowValue: the lower limit of the range to search in.
  * a_High Value: the upper limit of the range to search in. Defaults to null.
  * a_AsOfDate: not used. Required in POSSE. Defaults to sysdate.
  * Returns --
  * api.udt_ObjectList
  *
  * Performs an api.pkg_SimpleSearch.ObjectsByIndex, and loads the values in the
  * returned udt_IdList into an udt_ObjectList, which can be used in simple SQL.
  *-----------------------------------------------------------------------------------*/
  function ObjectsByIndex(
                a_ObjectDefName varchar2,
                a_ColumnName varchar2,
                a_LowValue varchar2,
                a_HighValue varchar2 default null,
                a_AsOfDate date default sysdate
   ) return api.udt_ObjectList;

end pkg_SimpleSearch;
 
/

grant execute
on pkg_simplesearch
to bcpdata;

create or replace package body pkg_SimpleSearch is
  /*------------------------------------------
  * ObjectsByIndex() -- PUBLIC
  * Full documentation in package header.
  *----------------------------------------*/
  function ObjectsByIndex(
                a_ObjectDefName varchar2,
                a_ColumnName varchar2,
                a_LowValue varchar2,
                a_HighValue varchar2 default null,
                a_AsOfDate date default sysdate
  ) return api.udt_ObjectList is
    t_IdList api.pkg_Definition.udt_IdList;
    t_ObjectList api.udt_ObjectList;
  begin
    t_IdList := api.pkg_SimpleSearch.ObjectsByIndex(a_ObjectDefName, a_ColumnName, a_LowValue, a_HighValue, a_AsOfDate);
    t_ObjectList := api.udt_ObjectList();
    t_ObjectList.extend(t_IdList.count());
    for t_Index in 1..t_IdList.count() loop
      t_ObjectList(t_Index) := api.udt_Object(t_IdList(t_Index));
    end loop;
    return t_ObjectList;
  end ObjectsByIndex;
end pkg_SimpleSearch;
/

