create or replace package pkg_HRMClearanceJob is

  -- Author  : HUSSEIN.HUSSEIN
  -- Created : 9/10/2008 3:24:14 PM
  -- Purpose : This package will include all the procedures
  --           and functions that are included on HRM Clearance Application Job

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_ObjectTable is pkg_CollectionUtils.udt_ObjectTable;
  subtype udt_ExternalFileNum is api.pkg_Definition.udt_ExternalFileNum;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*--------------------------------------------------------------------------
  This procedure searches for HRM Clearance Application Jobs
  *------------------------------------------------------------------------*/

  procedure ApplicationSearch (
       a_StatusDescription                  varchar2,
       a_FromSubmittedDate                  date,
       a_ToSubmittedDate                    date,
       a_FromAcceptedDate                   date,
       a_ToAcceptedDate                     date,
       a_OutstandingOnly                    varchar2,
       a_ApplFirstName                      varchar2,
       a_ApplLastName                       varchar2,
       a_ApplAffiliation                    varchar2,
       a_HRMProjectNumber                   varchar2,
       a_LandAppliedForMER                  varchar2,
       a_LandAppliedForRGE                  varchar2,
       a_LandAppliedForTWP                  varchar2,
       a_LandAppliedForSEC                  varchar2,
       a_ObjectId                           number,
       a_ApplReferenceNumber                varchar2,
       a_StoredHRMPermit                    varchar2,
       a_Objects                 out nocopy api.udt_ObjectList
      );

  /*--------------------------------------------------------------------------
   This procedure generates HRM Project Number if
   Application Purpose is "All New lands" when no
   entry exists.
   *------------------------------------------------------------------------*/

  procedure GenerateHRMProjectNumber(
       a_ObjectId                udt_Id,
       a_AsOfDate                date
       );

  /*--------------------------------------------------------------------------
   This procedure checks for the required fields when the job is already saved.
   *------------------------------------------------------------------------*/

  procedure CheckRequiredFields(
       a_ObjectId                udt_Id,
       a_AsOfDate                date
       );

  /*--------------------------------------------------------------------------
   This procedure sends email.
   *------------------------------------------------------------------------*/
  procedure HRMSendEmail (
       a_ObjectId                udt_Id,
       a_AsOfDate                date
       );

  /*--------------------------------------------------------------------------
   This procedure checks for the required fields when the Conduct Clearance
   RTMP Review process is completed with Reviewed outcome.
   *------------------------------------------------------------------------*/

  procedure CheckRTMPTabRequiredFields(
       a_ObjectId                udt_Id,
       a_AsOfDate                date
       );
  /*--------------------------------------------------------------------------
   This procedure checks for the required fields when the Conduct Clearance
   ARCH Review process is completed with Reviewed outcome.
   *------------------------------------------------------------------------*/

  procedure CheckARCHTabRequiredFields(
       a_ObjectId                udt_Id,
       a_AsOfDate                date
       );
  /*--------------------------------------------------------------------------
   This procedure creates relations HRM Report Summary which are added on the screen process
   and are requested for clearance.
   *------------------------------------------------------------------------*/
  Procedure CreateRelToReportSummary(
       a_ObjectId    udt_Id,
       a_EndPointId  udt_Id,
       a_ReportSummaryList out nocopy api.udt_ObjectList
       );

end pkg_HRMClearanceJob;

 
/

grant execute
on pkg_hrmclearancejob
to posseextensions;

