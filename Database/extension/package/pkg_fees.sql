CREATE OR REPLACE PACKAGE pkg_Fees is

  -- Author  : TREVOR.HAMM
  -- Created : 1/21/2007 8:45:59 PM
  -- Purpose : Online fee payment

  -- Public type declarations
  subtype udt_id is api.pkg_definition.udt_Id;
  subtype udt_idlist is api.pkg_definition.udt_IdList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_ptdef is api.pkg_definition.udt_ProcessType;
  subtype udt_EndPoint is api.pkg_definition.udt_EndPoint;
  subtype udt_ObjectList is api.udt_ObjectList;

  /*--------------------------------------------------------------------------
   * Initialize()
   * Sets up the Proposed Payment job by copying all the fees related to the
   * a_FromObjectId via the endpoint named 'Fee' and defaulting the AmountToPay
   * to the TotalOwing from the fee.
   *------------------------------------------------------------------------*/
  Procedure Initialize (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_FromObjectId              udt_Id
  );

  /*--------------------------------------------------------------------------
   * VoidPayment()
   * If a_IsVoidedFlag = 'N' and a_ReasonForVoid is not null, then this
   * procedure voids a_PaymentId
   *------------------------------------------------------------------------*/
  Procedure VoidPayment (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_PaymentId                 udt_Id,
    a_IsVoidedFlag              varchar2,
    a_ReasonForVoid             varchar2,
    a_DateVoided                date
  );

  /*--------------------------------------------------------------------------
   * AdjustFee()
   * If a_AdjustAmountBy is not null and a_AdjustAmountBy is <> 0, this
   * procedure processes an adjustment on a_FeeId
   *------------------------------------------------------------------------*/
  Procedure AdjustFee (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_FeeId                     udt_Id,
    a_AdjustAmountBy            number,
    a_AdjustmentReason          varchar2,
    a_AdjustmentDate            date
  );

  /*--------------------------------------------------------------------------
   * ProcessProposedPayment()
   * This routine prepares a proposed payment for processing.  This includes
   * a bunch of error checking.
   *------------------------------------------------------------------------*/
  Procedure PrepareProposedPayment (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
   * ProcessProposedPayment()
   * This routine processes a proposed payment, turning it into an actual
   * payment.
   *
   * NOTE:
   * If this is a manual payment, then this routine does all of the payment
   * processing.
   * If this is an ECom transaction, then the Prepare Payment process has
   * already kicked off the call to BCExpressPay and this routine is being
   * called by PayDone.aspx to complete the second half of the payment
   * ( Which is actually done by calling pkg_ECom.Approved() )
   *------------------------------------------------------------------------*/
  Procedure ProcessProposedPayment (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*---------------------------------------------------------------------------
   * GetPaymentsForRefund()
   * Returns all of the payments related to the job refund process a_ObjectId
   * belongs to.
   *
   * NOTE:
   * This is expected to be run as a procedural relationship from POSSE
   *-------------------------------------------------------------------------*/
  procedure GetPaymentsForRefund (
    a_ObjectId                     udt_Id,
    a_EndPointId                   udt_Id,
    a_RelatedObjectIds         out api.udt_ObjectList
  );

  /*--------------------------------------------------------------------------
   * PreparRefund()
   * This routine prepares a refund for processing.  This includes
   * a bunch of error checking.
   *------------------------------------------------------------------------*/
  Procedure PrepareRefund (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*--------------------------------------------------------------------------
   * ProcessRefund()
   * Processes a refund for payment a_PaymentId, distributing a_Amount across
   * a_PaymentId's transaction evenly.
   *------------------------------------------------------------------------*/
  Procedure ProcessRefund (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  );

  /*---------------------------------------------------------------------------
   * GetSelectablePaymentMethods() -- PUBLIC
   *
   * If a_SearchString is NULL, then this is being called to populate a
   * drop down list. Otherwise, it is being called from an edit style lookup
   * where the user as entered some information.  Use the information to
   * search for payment methods that are active and user selectable
   *-------------------------------------------------------------------------*/
  procedure GetSelectablePaymentMethods (
    a_Object                    udt_ID,
    a_RelationshipDef           udt_ID,
    a_EndPoint                  udt_EndPoint,
    a_SearchString              varchar2,
    a_Objects               out udt_ObjectList
  );

 /* ------------------------------------------------------------
  * PayExternalFees() -- PUBLIC
  *
  * This procedure blindly pays all fees for the job that is
  * related to the eCom job.  Used for demo purposes, will need
  * to change.  This procedure is called from the Accept Payment
  * process on the eCom job.
  * ------------------------------------------------------------*/
  procedure PayExternalFees(a_ObjectId  udt_Id,
                            a_AsOfDate  date);

   /* ------------------------------------------------------------
  * RecordPaymentMade
  *  This procedure sets the PaymentMade boolean on the Source
  * Object, if the Source Object has a boolean Allow Record
  * Payment flag set to True.
  * ------------------------------------------------------------*/
  procedure RecordPaymentMade (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_SourceObjectId                    udt_Id
  );

   /* ------------------------------------------------------------
  * CreateManualFee
  *  Triggered from the o_ManualFee object
  * ------------------------------------------------------------*/
  procedure CreateManualFee (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
    
    
    
  );

   /* ------------------------------------------------------------
  * ReGenerateFees
  *  This will regenerate all fees, as defined by FeeSchedulePlus, on the job
  * ------------------------------------------------------------*/
  procedure ReGenerateFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_fees;

 
/
grant execute
on pkg_fees
to abc;

grant execute
on pkg_fees
to posseextensions;

create or replace PACKAGE BODY           pkg_Fees is

  /*-------------------------------------------------------------------------
   * Initialize() - Public
   *------------------------------------------------------------------------*/
  Procedure Initialize (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_FromObjectId              udt_Id
  ) is
    t_DestEndPointId            udt_Id;
    t_MethodDestEndPointId      udt_Id;
    t_PayDestEndPointId         udt_Id;
    t_DestObjectDefId           udt_Id;
    t_Fee                       pls_integer;
    t_Payment                   pls_integer;
    t_Method                    pls_integer;
    t_Fees                      udt_IdList;
    t_Payments                  udt_IdList;
    t_Methods                   udt_IdList;
    t_FromEndPointId            udt_Id;
    t_PayFromEndPointId         udt_Id;
    t_MethodFromEndPointId      udt_Id;
    t_FromObjectDefId           udt_Id;
    t_RelId                     udt_Id;
    t_PaymentObjectDefId        udt_Id;
    t_ProvideEndPointId         udt_Id;
    t_ProposedPaymentDefId      pls_integer;
    t_EP                        varchar2(2);
    t_ApplicationEndPointId     pls_integer;
    t_FinalFees                 api.udt_objectlist;
    t_AvailableToRefund         number;
    t_PaymentId                 number;
    t_FeeId                     number;

  begin

    t_FinalFees := api.udt_objectlist();
    /* Get Config id's */
    if a_FromObjectId is null then
      api.pkg_errors.RaiseError(-20001,
          'a_FromObjectId must contain a valid ObjectId.');
    end if;


    /* Create relationship to Application Job */ -- added by MF 2008-Aug-12
        /* The relationship existed already.. but wasn't being populated
           I need it to be able to display declined transactions
        */
    t_ProposedPaymentDefId := api.pkg_configquery.ObjectDefIdForName('j_ProposedPayment');

    case api.pkg_ColumnQuery.Value( a_FromObjectid, 'ObjectDefName')
      when 'j_SubdivisionApplication' then t_EP := 'SA';
      when 'j_ApprovalApplication'    then t_EP := 'AA';
      when 'j_PermitApplication'      then t_EP := 'PA';
      else t_EP := null;
    end case;

    if t_EP is not null then
      t_ApplicationEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId, t_EP);
      t_RelId := api.pkg_RelationshipUpdate.New(t_ApplicationEndPointId, a_ObjectId, a_FromObjectId);
    end if;
    /**/

    t_PaymentObjectDefId := api.pkg_configquery.ObjectDefIdForName('o_Payment');

    begin
      select a.ObjectDefId
        into t_DestObjectDefId
        from api.Objects a
       where a.ObjectId = a_ObjectId;
    exception
      when no_data_found then
        api.pkg_errors.RaiseError(-20001,
          'Cannot determine ObjectDefId for Proposed Payment Job [' ||
          a_ObjectId || '].');
    end;

    begin
      select a.ObjectDefId
        into t_FromObjectDefId
        from api.Objects a
        where a.ObjectId = a_FromObjectId;
    exception
    when no_data_found then
      api.pkg_errors.RaiseError(-20001,
          'Cannot determine ObjectDefId for From Object [' ||
          a_FromObjectId || '].');
    end;

    --Is a refund?
    if api.pkg_columnquery.Value(a_ObjectId, 'PayType') = 'Refund' then

      --if it is a Refund, we assume we are coming from a Payment (the FromObjectId is a Payment)
      --create a rel from the Proposed Payment job to the Payment we are going to refund
      t_RelId := api.pkg_relationshipupdate.New(api.pkg_ConfigQuery.EndPointIdForName('j_ProposedPayment', 'RefundPayment'), a_ObjectId, a_FromObjectId);

      --Set some details
      api.pkg_columnupdate.setvalue(a_ObjectId,'PaymentDate',trunc(sysdate));

      t_FromEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_FromObjectDefId, 'Fee');
      if t_FromEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'From object [' || a_FromObjectId || '] does not have a relationship named ''Fee''.');
      end if;

      t_DestEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_DestObjectDefId, 'Fee');
      if t_DestEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || a_ObjectId || '] does not have a relationship named ''Fee''.');
      end if;

      --Create a relationship to all the Fees on a_FromObjectId
      t_Fees := api.pkg_ObjectQuery.RelatedObjects(a_FromObjectId, t_FromEndPointId);

      for c in 1..t_Fees.Count loop
          t_FinalFees.Extend(1);
          t_FinalFees(t_FinalFees.Count) := api.udt_object(t_Fees(c));
      end loop;
      
      --Get PaymentId for Use in finding payment refund amount.
      select to_number(reo.LinkValue) 
        into t_Paymentid
        from api.registeredexternalobjects reo 
       where reo.ObjectId = a_FromObjectId;

      for x in 1..t_FinalFees.count loop
         
        --Find Fee Id for the getting fee amount.
        select to_number(reo.LinkValue) 
        into t_FeeId
        from api.registeredexternalobjects reo 
       where reo.ObjectId = t_FinalFees(x).objectid;
        
        --finding how much is available to refund (from the payment on the fee in question)
        select max(ft.Amount) * -1
          into t_AvailableToRefund
          from api.paymenttransactions pt
          join api.feetransactions ft on ft.TransactionId = pt.TransactionId
       --   join query.o_payment p on p.objectid = (select x.objectid from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_Payment', 'PaymentId', pt.PaymentId)as api.udt_ObjectList)) x)
         -- join query.o_fee f on f.ObjectId = (select xf.objectid from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_Fee', 'FeeId', ft.FeeId)as api.udt_ObjectList)) xf)
         where ft.feeid = t_FeeId--f.ObjectId = t_FinalFees(x).objectid
           and pt.paymentid = t_Paymentid;--p.objectid = a_FromObjectId;

        api.pkg_columnupdate.SetValue(t_FinalFees(x).objectid, 'AmountAvailableToRefund', t_AvailableToRefund);

        t_RelId := api.pkg_RelationshipUpdate.New(t_DestEndPointId, a_ObjectId, t_FinalFees(x).objectid);
        api.pkg_ColumnUpdate.SetValue(t_FinalFees(x).objectid, 'AmountToRefund', 0);
      end loop;


    --Is a Payment?
    else
      t_FromEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_FromObjectDefId, 'Fee');
      if t_FromEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'From object [' || a_FromObjectId || '] does not have a relationship named ''Fee''.');
      end if;

      t_DestEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_DestObjectDefId, 'Fee');
      if t_DestEndPointId is null then
        api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || a_ObjectId || '] does not have a relationship named ''Fee''.');
      end if;

      --Create a relationship to all the unpaid Fees on a_FromObjectId
     /* Select fq.ObjectId
        Bulk collect into t_Fees
        from api.fees f
        join query.o_contractor c on c.objectid = f.ResponsibleObjectId
        join query.o_fee fq on fq.FeeId = f.feeid
        where (f.Amount + f.AdjustedAmount + f.PaidAmount) > 0
        and f.ResponsibleObjectId = a_FromObjectId;*/
      t_Fees := api.pkg_ObjectQuery.RelatedObjects(a_FromObjectId, t_FromEndPointId);

      for c in 1..t_Fees.Count loop
        if api.pkg_columnquery.numericvalue(t_Fees(c), 'TotalOwing') > 0
          then
            t_FinalFees.Extend(1);
            t_FinalFees(t_FinalFees.Count) := api.udt_object(t_Fees(c));
        end if;
      end loop;


      for x in 1..t_FinalFees.count loop
        t_RelId := api.pkg_RelationshipUpdate.New(t_DestEndPointId, a_ObjectId, t_FinalFees(x).objectid);
        --Default the amount to pay to the amount owing on the fee
        api.pkg_ColumnUpdate.SetValue(t_RelId, 'AmountToPay', api.pkg_ColumnQuery.Value(t_FinalFees(x).objectid, 'TotalOwing'));
      end loop;
    end if;
  end Initialize;


  /*-------------------------------------------------------------------------
   * VoidPayment() - Public
   *------------------------------------------------------------------------*/
  Procedure VoidPayment (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_PaymentId                 udt_Id,
    a_IsVoidedFlag              varchar2,
    a_ReasonForVoid             varchar2,
    a_DateVoided                date
  ) is
  begin
    if nvl(a_IsVoidedFlag, 'N') = 'N' and a_ReasonForVoid is not null then
       pkg_debug.putline('######## VoidPayment:'||a_ObjectId||' ########');
      /* Void this payment */
      api.pkg_PaymentUpdate.Void(a_PaymentId, a_DateVoided);

      /* Now 'freeze' data voided */
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'DateVoided', a_DateVoided - 1);
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'DateVoided', a_DateVoided);
      
      /* Set the Void boolean on the payment object to Y */
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'IsVoid', 'Y');     
    end if;
  end VoidPayment;

  /*-------------------------------------------------------------------------
   * AdjustFee() - Public
   *------------------------------------------------------------------------*/
  Procedure AdjustFee (
    a_ObjectId                  udt_Id,
    a_AsOfDate                  date,
    a_FeeId                     udt_Id,
    a_AdjustAmountBy            number,
    a_AdjustmentReason          varchar2,
    a_AdjustmentDate            date
  ) is
    t_Transaction               udt_Id;
  begin
    if a_AdjustAmountBy is not null and a_AdjustAmountBy <> 0 then
      if a_AdjustmentReason is null then
        api.pkg_errors.RaiseError(-20001,
            'Please enter a reason for this adjustment.');
      end if;

      if api.pkg_columnquery.NumericValue(a_ObjectId, 'TotalFeeAmount') + a_AdjustAmountBy < 0 then
        api.pkg_errors.RaiseError(-20001, 'You may not adjust the fee such that the fee balance is below zero.');
      end if;

      pkg_debug.putline('######## AdjustFee:'||a_ObjectId||'/'||a_FeeId||' ########');
      t_Transaction := api.pkg_FeeUpdate.Adjust(a_FeeId, a_AdjustAmountBy,
                           a_AdjustmentReason, a_AdjustmentDate);

      /* Reset the flags */
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'AdjustAmountBy');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'AdjustmentReason');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'AdjustmentDate');
    end if;
  end AdjustFee;

  /*-------------------------------------------------------------------------
   * PrepareProposedPayment() - Public
   *------------------------------------------------------------------------*/
  Procedure PrepareProposedPayment (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_EComFlag                  boolean;
    t_JobId                     udt_Id;
    t_PaymentDefId              udt_Id;
    t_PaymentMethod             varchar2(100);
    t_PaymentMethodId           udt_Id;
    t_ProcessPaymentDefId       udt_Id;
    t_ProposedPaymentDefId      udt_Id;
    t_PayeeReference            varchar2(60);
    t_ReceiptNumber             varchar2(60);
    t_ReceiptNumberLabel        varchar2(100);
    t_ReferenceLabel            varchar2(100);
    t_ReferenceRequiredFlag     boolean;
    t_TempIds                   udt_IdList;
    t_TotalAmountsToPay         number;
    t_TotalPayment              number;

     begin


    /* Get DefId's and Proposed Payment Info */
    select a.JobId,
           b.TotalPayment,
           b.ReceiptNumber,
           b.PayeeReference
      into t_JobId,
           t_TotalPayment,
           t_ReceiptNumber,
           t_PayeeReference
      from api.Processes a
      join query.j_ProposedPayment b
        on b.JobId = a.JobId
      where a.ProcessId = a_ProcessId;
    t_ProcessPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                                 'p_ProcessPayment');
    if t_ProcessPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                                 'Cannot get the DefId for p_ProcessPayment.');
    end if;
    t_ProposedPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                                 'j_ProposedPayment');
    if t_ProposedPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                                 'Cannot get the DefId for j_ProposedPayment.');
    end if;
    t_PaymentDefId := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    if t_PaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                                 'Cannot get the DefId for o_Payment.');
    end if;

    /* Get Payment Method related info */
    t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
        api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId,
                                 'PaymentMethod'));
    if t_TempIds.count != 1 then
      api.pkg_errors.RaiseError(-20001, 'Please choose a Payment Method.');
    end if;
    t_PaymentMethodId := t_TempIds(1);
    t_PaymentMethod := api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'Name');
    t_ReceiptNumberLabel := api.pkg_ColumnQuery.Value(t_PaymentMethodId,
                                 'ReceiptNumberLabel');
    t_ReferenceLabel := api.pkg_ColumnQuery.Value(t_PaymentMethodId,
                                 'ReferenceLabel');
    t_ReferenceRequiredFlag := case when api.pkg_ColumnQuery.Value(
                                 t_PaymentMethodId, 'ReferenceRequiredFlag') =
                                 'Y' then true else false end;
    t_EComFlag := case when api.pkg_ColumnQuery.Value(t_PaymentMethodId,
                                 'EComFlag') = 'Y' then true else false end;

    /* Check for required data */
    if t_TotalPayment is null or t_TotalPayment = 0 then
      api.pkg_errors.RaiseError(-20001,
                                 'Please enter the Total Amount to pay.');
    elsif t_TotalPayment < 0 then
      api.pkg_errors.RaiseError(-20001,
                                 'Total Amount to pay must be positive.');
    end if;

    /* Set the payment date */
    if api.pkg_ColumnQuery.DateValue(t_JobId, 'PaymentDate') is null then
      api.pkg_ColumnUpdate.SetValue(t_JobId, 'PaymentDate', trunc(sysdate));
    end if;

    if t_ReferenceRequiredFlag and t_PayeeReference is null then
      api.pkg_errors.RaiseError(-20001,
                     'A Payment Method of ' || t_PaymentMethod ||
                     ' requires that you enter a ' || t_ReferenceLabel || '.');
    end if;

    /* Sum up the amounts to pay and make sure they're all valid */
    t_TotalAmountsToPay := 0;
    for c in (select api.pkg_ColumnQuery.Value(b.ToObjectId, 'FeeId') FeeId,
                     api.pkg_ColumnQuery.Value(b.RelationshipId, 'AmountToPay') AmountToPay,
                     api.pkg_columnquery.NumericValue(b.ToObjectId, 'TotalFeeAmount') TotalFeeAmount
                from api.relationshipdefs a
                     join api.Relationships b
                       on b.FromObjectId = t_JobId
                      and b.EndPointId = a.FromEndPointId
                where a.FromEndPointName = 'Fee') loop

      if c.AmountToPay is null then
        api.pkg_errors.RaiseError(-20001, 'Please enter the amount to pay for each fee.');
      elsif c.AmountToPay < 0 then
        api.pkg_errors.RaiseError(-20001, 'Each amount to pay must be positive.');
      elsif c.TotalFeeAmount < c.AmountToPay then
        api.pkg_errors.RaiseError(-20001, 'You cannot pay more than the balance of the fee.');
      end if;

      t_TotalAmountsToPay := t_TotalAmountsToPay + c.AmountToPay;

    end loop;

    if t_TotalPayment <> t_TotalAmountsToPay then
      api.pkg_errors.RaiseError(-20001, 'Total Amount to pay (' ||
          to_char(t_TotalPayment, 'FM$999,999,999,999,990.00') ||
          ') must equal to the sum of all the individual amounts (' ||
          to_char(t_TotalAmountsToPay, 'FM$999,999,999,999,990.00') || ').');
    end if;

    if not t_EComFlag then

      if t_ReceiptNumber is null then
        api.pkg_errors.RaiseError(-20001, 'Please enter a receipt number.');
                       --t_ReceiptNumberLabel || ' from the POS receipt.');
      end if;

    end if;

  end PrepareProposedPayment;


  /*-------------------------------------------------------------------------
   * ProcessProposedPayment() - Public
   *------------------------------------------------------------------------*/
  Procedure ProcessProposedPayment (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_AmountsToPay              api.pkg_definition.udt_NumberList;
    t_CardTypeCode              varchar2(2);
    t_EComFlag                  char(1);
    t_FeeCount                  pls_integer;
    t_FeeIds                    udt_IdList;
    t_JobId                     udt_Id;
    t_ToJobId                   udt_Id;
    t_ProcessId                 udt_Id;
    t_PayeeReference            varchar2(60);
    t_Payer                     varchar2(60);
    t_PaymentDate               date;
    t_PaymentDefId              udt_Id;
    t_PaymentId                 udt_Id;
    t_PaymentMethodId           udt_Id;
    t_PaymentObjectId           udt_Id;
    t_ProcessPaymentDefId       udt_Id;
    t_ProposedPaymentDefId      udt_Id;
    t_PublicReferenceNumber     varchar2(60);
    t_ReceiptNumber             varchar2(60);
    t_RelId                     udt_Id;
    t_SequenceId                udt_Id;
    t_TempIds                   udt_IdList;
    t_TotalAmountsToPay         number;
    t_TotalPayment              number;
    
  begin
    /* Get DefId's */
    select a.JobId,
           b.TotalPayment,
           b.ReceiptNumber,
           b.PayeeReference,
           b.PaymentDate,
           b.EComFlag,
           b.PublicReferenceNumber
      into t_JobId,
           t_TotalPayment,
           t_ReceiptNumber,
           t_PayeeReference,
           t_PaymentDate,
           t_EComFlag,
           t_PublicReferenceNumber
      from api.Processes a
      join query.j_ProposedPayment b
        on b.JobId = a.JobId
      where a.ProcessId = a_ProcessId;

--raise_application_error(-20000, to_char( t_PaymentDate, 'yyyy/mm/dd hh24:mi:ss'));
    t_ProcessPaymentDefId := api.pkg_ObjectDefQuery.IdForName('p_ProcessPayment');
    if t_ProcessPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for p_ProcessPayment.');
    end if;

    t_ProposedPaymentDefId := api.pkg_ObjectDefQuery.IdForName('j_ProposedPayment');
    if t_ProposedPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for j_ProposedPayment.');
    end if;

    t_PaymentDefId := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    if t_PaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for o_Payment.');
    end if;

        /* Get Payment Method related info */
    t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
        api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId, 'PaymentMethod'));
    if t_TempIds.count != 1 then
      api.pkg_errors.RaiseError(-20001, 'Please choose a Payment Method.');
    end if;
    t_PaymentMethodId := t_TempIds(1);

    /* Sum up the amounts to pay and make sure they're all valid */
    t_TotalAmountsToPay := 0;
    t_FeeCount := 0;
    for c in (select api.pkg_ColumnQuery.Value(b.ToObjectId, 'FeeId') FeeId,
                     api.pkg_ColumnQuery.Value(b.RelationshipId, 'AmountToPay') AmountToPay,
                     api.pkg_columnquery.NumericValue(b.ToObjectId, 'TotalFeeAmount') TotalFeeAmount
                from api.relationshipdefs a
                     join api.Relationships b
                       on b.FromObjectId = t_JobId
                      and b.EndPointId = a.FromEndPointId
                where a.FromEndPointName = 'Fee'
                  and api.pkg_ColumnQuery.Value(b.RelationshipId, 'AmountToPay') > 0) loop

      if c.AmountToPay is null then
        api.pkg_errors.RaiseError(-20001, 'Please enter the amount to pay for each fee.');
      elsif c.AmountToPay < 0 then
        api.pkg_errors.RaiseError(-20001, 'Each amount to pay must be positive.');
      elsif c.TotalFeeAmount < c.AmountToPay then
        api.pkg_errors.RaiseError(-20001, 'You cannot pay more than the balance of the fee.');
      end if;

      t_FeeCount := t_FeeCount + 1;
      t_FeeIds(t_FeeCount) := c.FeeId;
      t_AmountsToPay(t_FeeCount) := c.AmountToPay;
      t_TotalAmountsToPay := t_TotalAmountsToPay + c.AmountToPay;
    end loop;

    /* Pay the POSSE Fees */
    begin
      t_PaymentId := api.pkg_PaymentUpdate.New(t_PaymentDate, t_FeeIds, t_AmountsToPay);
      api.pkg_PaymentUpdate.Modify(t_PaymentId, t_PayeeReference, t_ReceiptNumber);
    exception
      when no_data_found then
        api.pkg_errors.RaiseError(-20001, 'Create payment could not find one of the fees.');
    end;

    /* Register the Financial Transactions that Posse created */
    t_PaymentObjectId := api.pkg_objectupdate.RegisterExternalObject('o_Payment', t_PaymentId);
    for i in (select transactionId from api.paymenttransactions
               where paymentId = t_PaymentId) loop
      api.pkg_objectupdate.RegisterExternalObject('o_PaymentTransactions', i.transactionid);
      api.pkg_objectupdate.RegisterExternalObject('o_FeeTransactions', i.transactionid);
    end loop;

    /* Update the Payment Method */
    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'PaymentMethod'),
                            t_PaymentObjectId, t_PaymentMethodId);

    /* Create a relationship to the new payment object */
    t_RelId := api.pkg_RelationshipUpdate.New(
                   api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId,'Payment'),
                   t_JobId, t_PaymentObjectId);

    /* Create a "Check Submission" process
       and put "Payment received" in ReasonForUpdate  */
    /*select jobid into t_toJobId from api.fees where feeid = t_FeeIds(1);
    t_processid := api.pkg_processupdate.new( t_toJobId,
                       api.pkg_objectdefquery.idforname('p_CheckSubmission'),
                       null, null, null, null );
    api.pkg_columnupdate.setvalue(t_processid, 'ReasonForUpdate',
                                  'Payment received');*/
                                  
                                  
  end ProcessProposedPayment; 


  /*---------------------------------------------------------------------------
   * GetPaymentsForRefund() - public
   *-------------------------------------------------------------------------*/
  procedure GetPaymentsForRefund (
    a_ObjectId                                 udt_Id,
    a_EndPointId                            udt_Id,
    a_RelatedObjectIds                  out api.udt_ObjectList
  ) is
    t_EndPoints                    udt_IdList;
    t_JobId                        udt_Id;
    t_Payments                     udt_IdList;
    t_VoidedPayment                pls_integer;
    t_PaymentId                    udt_Id;
    t_PaymentCnt                   pls_integer;
  begin
    a_RelatedObjectIds := api.udt_ObjectList();

    /* Get the EndPointId for the Payment relationship */
    select r.ToEndPointId
      bulk collect into t_EndPoints
      from api.relationshipdefs r
      join api.jobtypes t
        on t.JobTypeId = r.FromObjectDefId
      join api.ObjectDefs p
        on p.ObjectDefId = r.ToObjectDefId
      where t.name in('j_SubdivisionApplication', 'j_PermitApplication',
                      'j_ApprovalApplication')
        and p.Name = 'o_Payment';

    /* Get our job id */
    select a.JobId
      into t_JobId
      from api.Processes a
      where a.ProcessId = a_ObjectId;

    t_Payments := api.pkg_ObjectQuery.RelatedObjects(t_JobId, t_EndPoints);

    for i in 1..t_Payments.count loop
      t_PaymentId := api.pkg_columnquery.value(t_Payments(i),'PaymentId');

      select count(*)
        into t_voidedpayment
        from api.paymenttransactions pt
       where pt.paymentid = t_PaymentId
         and pt.VoidedByTransactionId is not null;

      if t_voidedpayment = 0 then
        a_RelatedObjectIds.extend(1);
        t_PaymentCnt := nvl(t_PaymentCnt,0) + 1;
              a_RelatedObjectIds(t_PaymentCnt) := api.udt_Object(t_Payments(i));
      end if;
    end loop;

  end;

  /*-------------------------------------------------------------------------
   * PrepareRefund() - Public
   *------------------------------------------------------------------------*/
  Procedure PrepareRefund (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_JobId                     udt_Id;
    t_PaymentDefId              udt_Id;
    t_PaymentMethod             varchar2(100);
    t_PaymentMethodId           udt_Id;
    t_ProcessPaymentDefId       udt_Id;
    t_ProposedPaymentDefId      udt_Id;
    t_PayeeReference            varchar2(60);
    t_ReceiptNumber             varchar2(60);
    t_ReceiptNumberLabel        varchar2(100);
    t_ReferenceLabel            varchar2(100);
    t_ReferenceRequiredFlag     boolean;
    t_TempIds                   udt_IdList;
    t_TotalAmountsToRefund      number;
    t_TotalPayment              number(15,2);
    t_Transactions              udt_IdList;
  begin
    /* Get DefId's and Proposed Payment Info */
    select a.JobId,
           b.TotalPayment,
           b.ReceiptNumber,
           b.PayeeReference
      into t_JobId,
           t_TotalPayment,
           t_ReceiptNumber,
           t_PayeeReference
      from api.Processes a
      join query.j_ProposedPayment b
        on b.JobId = a.JobId
      where a.ProcessId = a_ProcessId;

    t_ProcessPaymentDefId := api.pkg_ObjectDefQuery.IdForName('p_ProcessPayment');
    if t_ProcessPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for p_ProcessPayment.');
    end if;
    t_ProposedPaymentDefId := api.pkg_ObjectDefQuery.IdForName('j_ProposedPayment');
    if t_ProposedPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for j_ProposedPayment.');
    end if;
    t_PaymentDefId := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    if t_PaymentDefId is null then
      api.pkg_errors.RaiseError(-20001, 'Cannot get the DefId for o_Payment.');
    end if;

    /* Get Payment Method related info */
    t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
        api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId, 'PaymentMethod'));
    if t_TempIds.count != 1 then
      api.pkg_errors.RaiseError(-20001, 'Please choose a Refund Method.');
    end if;
    t_PaymentMethodId := t_TempIds(1);
    t_PaymentMethod := api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'Name');
    t_ReceiptNumberLabel := api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'ReceiptNumberLabel');
    t_ReferenceLabel := api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'ReferenceLabel');
    t_ReferenceRequiredFlag := case when api.pkg_ColumnQuery.Value(t_PaymentMethodId, 'ReferenceRequiredFlag') = 'Y' then true else false end;

    /* Check for required data */
    if t_TotalPayment is null or t_TotalPayment = 0 then
      api.pkg_errors.RaiseError(-20001, 'Please enter the Total Refund Amount.');
    elsif t_TotalPayment < 0 then
      api.pkg_errors.RaiseError(-20001, 'Total Refund Amount must be positive.');
    end if;

    /* Set the payment date */
    if api.pkg_ColumnQuery.DateValue(t_JobId, 'PaymentDate') is null then
      api.pkg_ColumnUpdate.SetValue(t_JobId, 'PaymentDate', trunc(sysdate));
    end if;

    -- Commented out to avoid enforcing Payee Reference for refunds since
    -- staff do not know what check number treasury will assign to the refund.
    /*if t_ReferenceRequiredFlag and t_PayeeReference is null then
      api.pkg_errors.RaiseError(-20001, 'A Refund Method of ' || t_PaymentMethod || ' requires that you enter a ' || t_ReferenceLabel || '.');
    end if;*/

    --Sum up the amounts to pay and make sure they're all valid
    t_TotalAmountsToRefund := 0;
    for c in (select f.feeid
                   , f.AmountToRefund
                   , f.AmountAvailableToRefund
                from query.r_ProposedPaymentFee r
                join query.o_fee f on f.ObjectId = r.FeeObjectId
                where r.ProposedPaymentObjectId = t_JobId) loop

      if c.AmountToRefund is null then
        api.pkg_errors.RaiseError(-20001, 'Please enter the amount to refund for each fee.');
      elsif c.AmountToRefund < 0 then
        api.pkg_errors.RaiseError(-20001, 'Each amount to refund must be positive.');
      elsif c.amountavailabletorefund < c.amounttorefund then
        api.pkg_errors.RaiseError(-20001, 'You cannot refund more than the amount that was paid..');
      end if;

      t_TotalAmountsToRefund := t_TotalAmountsToRefund + c.AmountToRefund;
    end loop;

    if t_TotalPayment <> t_TotalAmountsToRefund then
      api.pkg_errors.RaiseError(-20001, 'Total Refund Amount (' || to_char(t_TotalPayment, 'FM$999,999,999,999,990.00') || ') must equal to the sum of all the individual amounts (' || to_char(t_TotalAmountsToRefund, 'FM$999,999,999,999,990.00') || ').');
    end if;

    if t_ReceiptNumber is null then
      api.pkg_errors.RaiseError(-20001, 'Please enter a receipt number.');--' || t_ReceiptNumberLabel || ' from the POS receipt.');
    end if;

  end PrepareRefund;


  /*-------------------------------------------------------------------------
   * ProcessRefund() - Public
   *------------------------------------------------------------------------*/
  Procedure ProcessRefund (
    a_ProcessId                 udt_Id,
    a_AsOfDate                  date
  ) is
    t_AmountsToRefund           api.pkg_definition.udt_NumberList;
    t_FeeCount                  pls_integer;
    t_FeeIds                    udt_IdList;
    t_JobId                     udt_Id;
    t_PaymentDefId              udt_Id;
    t_PaymentMethodId           udt_Id;
    t_ProcessPaymentDefId       udt_Id;
    t_ProposedPaymentDefId      udt_Id;
    t_RelId                     udt_Id;
    t_TempIds                   udt_IdList;
    t_Payments                  udt_IdList;
    t_TotalAmountsToRefund      number;
    t_RefundId                  udt_Id;
    t_RefundObjectId            udt_Id;
    t_RefundDate                date;
    t_PayeeReference            varchar2(60);
    t_ReceiptNumber             varchar2(60);
    t_Reason                    varchar2(4000);
    t_PayEndPointId             udt_Id;
    t_RefundEndPointId          udt_Id;
    t_FromObjectId              udt_Id;
  begin
    /* Get DefId's */
    select a.JobId,
           b.PaymentDate,
           b.PayeeReference,
           b.ReceiptNumber,
           b.ReasonForRefund,
           b.FromObjectId
      into t_JobId,
           t_RefundDate,
           t_PayeeReference,
           t_ReceiptNumber,
           t_Reason,
           t_FromObjectId
      from api.Processes a
      join query.j_ProposedPayment b
        on b.JobId = a.JobId
      where a.ProcessId = a_ProcessId;
    t_ProcessPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                     'p_ProcessPayment');
    if t_ProcessPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                     'Cannot get the DefId for p_ProcessPayment.');
    end if;
    t_ProposedPaymentDefId := api.pkg_ObjectDefQuery.IdForName(
                     'j_ProposedPayment');
    if t_ProposedPaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                     'Cannot get the DefId for j_ProposedPayment.');
    end if;
    t_PaymentDefId := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    if t_PaymentDefId is null then
      api.pkg_errors.RaiseError(-20001,
                     'Cannot get the DefId for o_Payment.');
    end if;

    t_PayEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
                     t_ProposedPaymentDefId, 'Payment');
    if t_PayEndPointId is null then
      api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || t_JobId ||
                     '] does not have a relationship named ''Payment''.');
    end if;

    t_RefundEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
                     t_ProposedPaymentDefId, 'Refund');
    /*if t_RefundEndPointId is null then
      api.pkg_errors.RaiseError(-20001, 'Proposed Payment job [' || t_JobId ||
                     '] does not have a relationship named ''Refund''.');
    end if;*/

    t_Payments := api.pkg_ObjectQuery.RelatedObjects(t_JobId, t_PayEndPointId);

    /* Get Payment Method related info */
    t_TempIds := api.pkg_ObjectQuery.RelatedObjects(t_JobId,
        api.pkg_ConfigQuery.EndPointIdForName(t_ProposedPaymentDefId,
                                 'PaymentMethod'));
    if t_TempIds.count != 1 then
      api.pkg_errors.RaiseError(-20001, 'Please choose a Payment Method.');
    end if;
    t_PaymentMethodId := t_TempIds(1);

    /* Sum up the amounts to pay and make sure they're all valid */
    t_TotalAmountsToRefund := 0;
    t_FeeCount := 0;

    for c in (select f.feeid
                   , f.AmountToRefund
                from query.r_ProposedPaymentFee r
                join query.o_fee f on f.ObjectId = r.FeeObjectId
                where r.ProposedPaymentObjectId = t_JobId) loop

      if c.AmountToRefund is null then
        api.pkg_errors.RaiseError(-20001,
            'Please enter the amount to refund for each fee.');
      elsif c.AmountToRefund < 0 then
        api.pkg_errors.RaiseError(-20001,
            'Each amount to refund must be positive.');
      end if;

      t_FeeCount := t_FeeCount + 1;
      t_FeeIds(t_FeeCount) := c.FeeId;
      t_AmountsToRefund(t_FeeCount) := c.AmountToRefund * -1;
      t_TotalAmountsToRefund := t_TotalAmountsToRefund + c.AmountToRefund;

    end loop;

    /* Create the refund (negative payment as POSSE finance doesn't actually
       support refunds) */
    begin
      t_RefundId := api.pkg_PaymentUpdate.New(t_RefundDate, t_FeeIds,
                                              t_AmountsToRefund);

      api.pkg_PaymentUpdate.Modify(t_RefundId, t_PayeeReference,
                                   t_ReceiptNumber);

    exception
    when no_data_found then
      api.pkg_errors.RaiseError(-20001,
                     'Create refund could not find one of the fees.');
    end;



    /* A search from here won't do the dynamic registration, so register
       the new payment manually */
    t_RefundObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
                            'o_Payment', t_RefundId);

    /* Now fill in all the additional details */
    api.pkg_ColumnUpdate.SetValue(t_RefundObjectId, 'IsRefund', 'Y');
    api.pkg_ColumnUpdate.SetValue(t_RefundObjectId, 'ReasonForRefund',t_Reason);



    /* Update the Payment Method */
    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'PaymentMethod'),
                            t_RefundObjectId, t_PaymentMethodId);

    /* Relate the refund to the payment */
    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'Refund'),
                            t_FromObjectId, t_RefundObjectId);

    /* Relate the refund to the proposed payment */
    t_RelId := api.pkg_RelationshipUpdate.New(
                   api.pkg_ConfigQuery.EndPointIdForName(
                                       t_ProposedPaymentDefId, 'Payment'),
                   t_JobId, t_RefundObjectId);

  end;

  /*---------------------------------------------------------------------------
   * GetSelectablePaymentMethods() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GetSelectablePaymentMethods (
    a_Object                    udt_ID,
    a_RelationshipDef           udt_ID,
    a_EndPoint                  udt_EndPoint,
    a_SearchString              varchar2,
    a_Objects               out udt_ObjectList
  ) is
    t_Active                    char(1);
    t_Count                     pls_integer;
    t_SelectablePaymentMethods  udt_IDList;
  begin
    a_Objects := api.udt_ObjectList();

    /* Search for all of the selectable payment methods */
    t_SelectablePaymentMethods := api.pkg_simplesearch.ObjectsByIndex(
                                  'o_PaymentMethod', 'UserSelectable', 'Y');

    /* Now put all the active ones into a_Objects */
    t_Count := t_SelectablePaymentMethods.count;
    for i in 1..t_Count loop
      select a.ActiveFlag
        into t_Active
        from query.o_PaymentMethod a
        where a.ObjectId = t_SelectablePaymentMethods(i);
      if t_Active = 'Y' then
        a_Objects.extend(1);
        a_Objects(a_Objects.count) := api.udt_Object(
                                          t_SelectablePaymentMethods(i));
      end if;
    end loop;
  end;

/* ------------------------------------------------------------
  * PayExternalFeesLMS() -- PRIVATE
  * 
  * !!!!DEMO CODE !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  
  *  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE 
  *   !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE  !!! DEMO CODE 
  *
  * TODO: Rework this code, consider PayExternalFeesABC as a good starting point.
  *
  * This procedure blindly pays all fees for the LMS job(s) that are
  * related to the eCom job.  Used for demo purposes, will need
  * to change.  This procedure is called from the PayExernalFees
  * procedure in this package.
  * ------------------------------------------------------------*/
  procedure PayExternalFeesLMS(a_EcomJobId  udt_Id) is
    t_FeeIDs                udt_IdList;
    t_AmountsToPay          api.pkg_definition.udt_NumberList;
    t_eComBPEndPointId      udt_Id;
    t_eComGPEndPointId      udt_Id;
    t_eComTPEndPointId      udt_Id;
    t_eComPAEndPointId      udt_Id;
    t_eComZOEndPointId      udt_Id;
    t_PaymentId             udt_Id;
    t_PaymentObjectId       udt_Id;
    t_SequenceId            udt_Id;
    t_ReceiptNumber         varchar2(15);
    t_FeeCount              number := 0;
    t_IsTradePermit         boolean default false;
    t_JobId                 udt_Id;
    t_ProcessId             udt_Id;
  begin
    api.pkg_errors.RaiseError(-20000,'Not Implemented Exception');

    pkg_debug.putline('######## Inside the PayExternalFeesLMS procedure ########');

    --get the EndPointId for the relationship between the ecom job and the LMS jobs
    --this list may not be complete!
    t_eComBPEndPointId := api.pkg_configquery.endpointidforname('j_eCom', 'BuildingPermit');
    t_eComGPEndPointId := api.pkg_configquery.endpointidforname('j_eCom', 'GeneralPermit');
    t_eComTPEndPointId := api.pkg_configquery.endpointidforname('j_eCom', 'TradePermit');
    t_eComPAEndPointId := api.pkg_configquery.endpointidforname('j_eCom', 'Presubmittal');
    t_eComZOEndPointId := api.pkg_configquery.endpointidforname('j_eCom', 'Rezoning');

    --loop through the fees and add them with their amounts to the arrays
    for c in (select f.feeid,
                     r.endpointid,
                     f.jobid,
                     (f.Amount + f.AdjustedAmount + f.PaidAmount) Amount
                from api.relationships r
                join api.fees f on f.jobid = r.toobjectid
               where r.fromobjectid = a_EcomJobId
                 and r.endpointid in (t_eComBPEndPointId, t_eComGPEndPointId, t_eComTPEndPointId, t_eComPAEndPointId, t_eComZOEndPointId)
                 and (f.Amount + f.AdjustedAmount + f.PaidAmount) > 0) loop
      pkg_debug.putline('########  Looping through the fee ID: ' || c.feeid || ' Amount: ' || c.Amount);
      t_FeeCount := t_FeeCount + 1;
      t_FeeIds(t_FeeCount) := c.FeeId;
      t_AmountsToPay(t_FeeCount) := c.Amount;
      if c.endpointid = t_eComTPEndPointId then
        --this is a bit of a hack for trade permits ... this doesn't account for the
        --eventual possibility of a shopping cart
        t_IsTradePermit := True;
        t_JobId := c.jobid;
      end if;
    end loop;

    --loop through and collect fees on subjobs - VANCOUVER added by AWP
    for d in (select f.feeid,
                     (f.Amount + f.AdjustedAmount + f.PaidAmount) Amount
                from api.relationships r
                join api.jobs j on j.parentjobid = r.toobjectid
                join api.fees f on f.jobid = j.jobid
               where r.fromobjectid = a_eComJobId
                 and r.endpointid = t_eComPAEndPointId
                and (f.Amount + f.AdjustedAmount + f.PaidAmount) > 0) loop
      pkg_debug.putline('########  Looping through the SUBJOB fee ID: ' || d.feeid || ' Amount: ' || d.Amount);
      t_FeeCount := t_FeeCount + 1;
      t_FeeIds(t_FeeCount) := d.FeeId;
      t_AmountsToPay(t_FeeCount) := d.Amount;
    end loop;

    --make the payment for all of the fees
    t_PaymentId := api.pkg_paymentupdate.New(sysdate, t_FeeIds, t_AmountsToPay);

    --generate the Receipt Number
    begin
      select SequenceId
        into t_SequenceId
        from api.Sequences
       where Description = 'Receipt Number';
    exception when no_data_found then
      api.pkg_errors.RaiseError(-20001, 'Cannot get sequence id for "Receipt Number" sequence.');
    end;
    t_ReceiptNumber := api.pkg_Utils.GenerateExternalFileNum('[9999999]', t_SequenceId);

    --set the details of the payment
    api.pkg_PaymentUpdate.Modify(t_PaymentId, 'Online Payment', t_ReceiptNumber);


    --Register the Financial Objects that Posse created
    t_PaymentObjectId := api.pkg_objectupdate.RegisterExternalObject('o_Payment', t_PaymentId);
    for i in (select transactionId from api.paymenttransactions
               where paymentId = t_PaymentId) loop
      api.pkg_objectupdate.RegisterExternalObject('o_PaymentTransactions', i.transactionid);
      api.pkg_objectupdate.RegisterExternalObject('o_FeeTransactions', i.transactionid);
    end loop;

    pkg_debug.putline('######## PayExternalFeesLMS procedure Complete ########');
    --If this is a trade permit payment, create and complete issue permit process
    if t_IsTradePermit then
      t_ProcessId := api.pkg_ProcessUpdate.New(t_JobId,
                                               api.pkg_ConfigQuery.ObjectDefIdForName('p_IssueTradePermit'),
                                               'Issued by Public website',
                                               to_date(null), to_date(null), sysdate);
      api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Issued Online');
    end if;


  end PayExternalFeesLMS;

 /* ------------------------------------------------------------
  * PayExternalFeesABC() -- PRIVATE
  *
  * This procedure is called from the Accept Payment
  * process on the eCom job and ONLY pays the fees that the eCom
  * sent to be paid.
  * ------------------------------------------------------------*/
  procedure PayExternalFeesABC(
    a_EcomJobId                               udt_Id,
    a_SourceObjectDefName                     varchar2
  ) is
    t_FeeIDs                udt_IdList;
    t_AmountsToPay          api.pkg_definition.udt_NumberList;
    t_Payor                 varchar2(150);
    t_PaymentId             udt_Id;
    t_PaymentObjectId       udt_Id;
    t_FeeCount              number := 0;
    t_SequenceId            number;
    t_ReceiptNumber         varchar2(60);
    t_PayeeReference        varchar2(60) := 'Online Payment';
    t_PaymentDefId          udt_Id := api.pkg_ObjectDefQuery.IdForName('o_Payment');
    t_PaymentMethodId       udt_Id;
    t_RelId                 udt_Id;
    t_SuccessFlagColumnName   varchar2(60);
    t_EcomMethod            varchar2(20);
    t_JobDefName            varchar2(30);
  begin
    --pkg_debug.enable('AVO');  
  
    pkg_debug.putline('######## Inside the PayExternalFeesABC procedure ########');

    -- Pay the Fees based what was related to the eCom job at the time of payment.
    -- This guarantees that only those fees that were paid will be marked as paid.
    for c in (select f.FeeId,
                     api.pkg_columnquery.NumericValue(r.RelationshipId,
                                                      'FeeAmountToPay') AmountToPay
                from query.j_ecom e
                join api.relationships r
                  on r.FromObjectId = e.ObjectId
                 and r.EndPointId =
                     api.pkg_configquery.EndPointIdForName('j_eCom',
                                                           'FeeEndPoint')
                join query.o_fee f
                  on f.ObjectId = r.ToObjectId
               where e.ObjectId = a_EcomJobId) loop
      pkg_debug.putline('########  Looping through the fee ID: ' ||
                        c.feeid || ' Amount: ' || c.AmountToPay);
      t_FeeCount := t_FeeCount + 1;
      t_FeeIds(t_FeeCount) := c.FeeId;
      t_AmountsToPay(t_FeeCount) := c.AmountToPay;
    end loop;

    -- generate the Receipt Number
    begin
      select SequenceId into t_SequenceId
        from api.Sequences where Description = 'Receipt Number';
    exception when no_data_found then
      api.pkg_errors.RaiseError(-20001, 'Cannot get sequence id for "Receipt Number" sequence.');
    end;
    t_ReceiptNumber := api.pkg_Utils.GenerateExternalFileNum('[9999999]', t_SequenceId);

    --make the payment for all of the fees
    t_PaymentId := api.pkg_paymentupdate.New(sysdate, t_FeeIds, t_AmountsToPay);
    api.pkg_PaymentUpdate.Modify(t_PaymentId, t_PayeeReference, t_ReceiptNumber);

    --Register the Financial Objects that Posse created
    t_PaymentObjectId := api.pkg_objectupdate.RegisterExternalObject(
                             'o_Payment', t_PaymentId);
    api.pkg_columnupdate.SetValue(t_PaymentObjectId, 'IsOnlinePayment', 'Y');
    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'OnlineUser'),
                            t_PaymentObjectId, api.pkg_securityquery.EffectiveUserId());

    for i in (select transactionId from api.paymenttransactions
               where paymentId = t_PaymentId) loop
      api.pkg_objectupdate.RegisterExternalObject('o_PaymentTransactions',
                                                  i.transactionid);
      api.pkg_objectupdate.RegisterExternalObject('o_FeeTransactions',
                                                  i.transactionid);
    end loop; --i

    -- Create Rel from Payment to eCom job for information purposes.
    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'eCom'),
        t_PaymentObjectId, a_EcomJobId);    

    --set the Payment method
/*
    select max(ObjectId)
      into t_PaymentMethodId
      from query.o_PaymentMethod p
     where p.EComAndActive = 'Y';
*/

    t_EcomMethod := api.pkg_columnquery.Value(a_EcomJobId, 'PaymentMethod');
    
    for m in ( select pm.ObjectId, pm.ECOMCreditCardFlag, pm.eCheckFlag
                 from query.o_PaymentMethod pm
                 where pm.EComAndActive = 'Y'
                 order by 1 ) loop
                 
      --testing t_EcomMethod for null is to support the in progress eCom jobs that didn't have the opportunity
      --to have the PaymentMethod set from the updated payment.aspx.cs file. 
      --the expected timeframe for this scenario is 24 hours. This check could be removed after that
      --this will be implemented in July/Aug 2015
      if t_EcomMethod is null then
        if m.ECOMCreditCardFlag = 'Y' then
          t_PaymentMethodId := m.ObjectId;
        end if;
      elsif t_EcomMethod = 'Credit Card' then
        if m.ECOMCreditCardFlag = 'Y' then
          t_PaymentMethodId := m.ObjectId;
        end if;
      elsif t_EcomMethod = 'eCheck' then
        if m.echeckflag = 'Y' then
          t_PaymentMethodId := m.ObjectId;
        end if;
      end if;
      
    end loop;

    if t_PaymentMethodId is null then
      api.pkg_Errors.RaiseError(-20001,
          'Cannot find Payment Method for ECom transaction');
    end if;

    t_RelId := api.pkg_RelationshipUpdate.New(
        api.pkg_ConfigQuery.EndPointIdForName(t_PaymentDefId, 'PaymentMethod'),
                            t_PaymentObjectId, t_PaymentMethodId);

  
    --Set the payer of the fee to the name of the registered online user or
    --if the user is a system user get the PaymentProviderPayeeName from the eCom job
    
    if api.pkg_securityquery.EffectiveUser in ('POSSESYS','RON') then
      t_Payor := api.pkg_columnquery.Value(a_eComJobId, 'PaymentProviderPayeeName');
    else
      t_Payor := api.pkg_columnquery.Value(t_PaymentObjectId, 'OnlineUserName');
    end if;
    api.pkg_columnupdate.SetValue(t_PaymentObjectId, 'Payor', t_Payor);
    
  
    --set the Success Flag on the job related to the eCom job
    t_SuccessFlagColumnName := api.pkg_columnquery.Value(a_eComJobId, 'SuccessFlagColumnName');
    if t_SuccessFlagColumnName is not null and a_SourceObjectDefName != 'o_ABC_LegalEntity' then
      api.pkg_columnupdate.SetValue(api.pkg_columnquery.NumericValue(a_eComJobId, 'SourceObjectId'),
                                    t_SuccessFlagColumnName,
                                    'Y');
    elsif t_SuccessFlagColumnName is not null 
        and a_SourceObjectDefName = 'o_ABC_LegalEntity' then
      -- We are coming from Pay All Fees and need to submit all draft jobs paid by this transaction
      for i in (
         select j.JobId
           from api.paymenttransactions pt
           join api.feetransactions ft
               on ft.TransactionId = pt.TransactionId
           join api.fees f
               on f.FeeId = ft.FeeId
           join api.jobs j
               on j.jobid = f.jobid
          where pt.PaymentId = t_PaymentId
            and j.JobStatus = 'NEW'
            and api.pkg_ColumnQuery.value(j.JobId, 'AddToPayAllFees') = 'Y'
        ) loop
        api.pkg_columnupdate.SetValue(i.jobid, t_SuccessFlagColumnName,'Y');
        api.pkg_columnupdate.SetValue(i.jobid, 'ApplicationReceivedDate', sysdate);
        
        t_JobDefName := api.pkg_ColumnQuery.Value(i.JobID, 'ObjectDefName');
        
        if t_JobDefName = 'j_ABC_PermitRenewal' or t_JobDefName = 'j_ABC_PermitApplication' then 
          api.pkg_columnupdate.SetValue(i.jobid, 'SubmittedDate', sysdate);
        end if;
        
        if t_JobDefName = 'j_ABC_PRRenewal' then
          api.pkg_columnupdate.SetValue(i.jobid, 'SetOnlineDetails', 'Y');
        end if;
      end loop;
    end if;

  end PayExternalFeesABC;


/* ------------------------------------------------------------
  * PayExternalFees() -- PUBLIC
  *
  * This procedure figures out whether this is an LMS or ABC
  * payment, then call the appropriate procedure.
  * ------------------------------------------------------------*/
  procedure PayExternalFees(a_ObjectId  udt_Id,
                            a_AsOfDate  date) is
    t_eComJobId             udt_Id;
    t_eComNAEndPointId      udt_Id;
    t_eComRLEndPointId      udt_Id;
    t_Count                 pls_Integer;
    t_SourceObjectDefName   varchar2(30);
    t_SourceObjectId        number;
    t_IsABC                 boolean;

  begin

    pkg_debug.putline('######## Inside the PayExternalFees procedure ########');

    --get the eComJobId
    select jobid
      into t_eComJobId
      from api.processes p
     where p.processid = a_ObjectId;

    t_SourceObjectId := api.pkg_columnquery.NumericValue(t_eComJobId, 'SourceObjectId');

    select max(od.Name)
      into t_SourceObjectDefName
      from api.objects o
      join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
     where o.ObjectId = t_SourceObjectId;

    if t_SourceObjectDefName in ('o_ABC_LegalEntity',
                                 'j_ABC_NewApplication', 
                                 'j_ABC_AmendmentApplication', 
                                 'j_ABC_Reinstatement', 
                                 'j_ABC_RenewalApplication', 
                                 'j_ABC_Accusation',
                                 'j_ABC_PRApplication',
                                 'j_ABC_PRRenewal',
                                 'j_ABC_PRAmendment',
                                 'j_ABC_Petition') then
      PayExternalFeesABC(t_eComJobId, t_SourceObjectDefName);
    else
      PayExternalFeesABC(t_eComJobId, t_SourceObjectDefName);
    end if;

    pkg_debug.putline('######## Exiting the PayExternalFees procedure ########');

  end PayExternalFees;

  /* ------------------------------------------------------------
  * RecordPaymentMade
  *  This procedure sets the PaymentMade boolean on the Source
  * Object to indicate a payment has been made.
  * ------------------------------------------------------------*/
  procedure RecordPaymentMade (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_SourceObjectId                    udt_Id
  ) is
  begin
    api.pkg_ColumnUpdate.SetValue(a_SourceObjectId, 'PaymentMade', 'Y');
  exception
    when others then
      null;
  end RecordPaymentMade;


  /* ------------------------------------------------------------
  * CreateManualFee
  * ------------------------------------------------------------*/
  procedure CreateManualFee (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
    
  ) is
    t_FromObjectId                      number := api.pkg_columnquery.NumericValue(a_ObjectId, 'FromObjectId');
    t_FeeId                             number;
    t_Amount                            number := api.pkg_columnquery.NumericValue(a_ObjectId, 'Amount');
    t_FeeType                           varchar2(100) := api.pkg_columnquery.Value(a_ObjectId, 'FeeType');
    t_GLAccountId                       number;
    t_Relationships                     udt_IdList;
    t_ResponsibleObjectId               number;
    t_EndPointId                        number;
    t_ObjectId                          varchar2(30);
    t_TempObjectId                      number;
    t_FromObjectDefId                   number;
    t_AccusationJobDefId                number := api.pkg_configquery.ObjectDefIdForName('j_ABC_Accusation');
    t_GLDescription                     varchar2(100);


  begin
    if api.pkg_columnquery.Value(a_ObjectId, 'FeeCreated') = 'N' then
      
      --the GL Account is not currently implemented properly - just hard coding for the time being.
      
    select o.ObjectDefId
     into t_FromObjectDefId
     from api.objects o
    where o.objectid = t_FromObjectId;

    select m.GLAccountDescription
     into t_GLDescription
     from query.o_manualfee m
    where m.objectid = a_objectid;
             
    if t_GLDescription is null then
      raise_application_error(-20000, 'GL Account missing - please contact your system administrator.');
    end if;
            
     select gl.GLAccountId
      into t_GLAccountId
      from api.glaccounts gl
     where t_GLDescription = gl.Description;
       
     --Responsible Party
      select max(rd.FromEndPointId)
        into t_EndPointId
        from api.RelationshipDefs rd
        join stage.ResponsibleObjects ro on ro.EndPointId = rd.FromEndPointId
        join api.jobs jo on jo.JobTypeId = ro.JobTypeId
       where jo.JobId = t_FromObjectId
         and ro.IsResponsible = 'Y'
         and ro.IsDefault = 'Y';
         
       select o.ObjectDefId
           into t_FromObjectDefId
           from api.objects o
          where o.objectid = t_FromObjectId;

      if t_EndPointId is null then
        raise_application_error(-20000, 'Responsible Party endpoint missing.');
      end if;

      t_Relationships := api.pkg_ObjectQuery.RelatedObjects(t_FromObjectId, t_EndPointId);

      if t_Relationships.count = 1 then
         t_ResponsibleObjectId := t_Relationships(1);
      elsif t_Relationships.count > 1 then
         raise_application_error(-20000, 'Too many relationships exist for responsible party.');
      elsif (t_Relationships.count = 0 and t_FromObjectDefId != t_AccusationJobDefId) then          
         raise_application_error(-20000, 'The fee cannot be created as no responsible party has been selected for this job.');
      end if;

      --create the fee (no tax rate)
      t_FeeId := api.pkg_feeupdate.new(t_FromObjectId, t_Amount, t_FeeType, t_GLAccountId, t_ResponsibleObjectId, null, sysdate);
      api.pkg_ObjectUpdate.RegisterExternalObject('o_Fee', t_FeeId);

      --indicate that the fee has been created, so we don't create another one by accident
      api.pkg_columnupdate.SetValue(a_ObjectId, 'FeeCreated', 'Y');
    end if;
  end CreateManualFee;

   /* ------------------------------------------------------------
  * ReGenerateFees
  *  This will regenerate all fees, as defined by FeeSchedulePlus, on the job
  * ------------------------------------------------------------*/
  procedure ReGenerateFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_FeeTxnCount                       number;

  begin
   
--api.pkg_errors.RaiseError(-20000, api.pkg_columnquery.value(a_ObjectId, 'ReGenerateFees'));
/*    --first we need to delete the old fees on the job, but only if they are no transactions against them
    for i in (select feeid from api.fees f where f.JobId = a_ObjectId) loop
      select count(1)
        into t_FeeTxnCount
        from api.feetransactions ft
       where ft.FeeId = i.feeid;

      if t_FeeTxnCount > 0 then
        raise_application_error(-20000, 'Fee recalculation failed: fee transactions exist');
      else
        api.pkg_feeupdate.Remove(i.feeid);
      end if;
    end loop;*/

    -- regenerate the fees using FeeSchedulePlus
    pkg_debug.putline('######## ReGenerateFees procedure ########');
    feescheduleplus.pkg_posseregisteredprocedures.GenerateFees(a_ObjectId, sysdate, 'N');
    

  end ReGenerateFees;
end pkg_fees;
/
