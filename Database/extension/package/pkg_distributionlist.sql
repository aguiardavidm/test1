create or replace package pkg_DistributionList is

  -- Author  : ALBERTD
  -- Created : 26/08/2004 1:08:14 PM
  -- Purpose : This package is intended to be used to
  --           enhance the functionality of distribution lists in POSSE

  -- Public type declarations
  subtype udt_id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;


  -- Public function and procedure declarations
  /************************************************************************
  * CreateDistributionRecipients is a procedure run at post-verify of
  * the relationship between the process and the Distribution list
  * object. For each Distribution Client or User related to the
  * Distribution List, it will create a new relationship between the
  * process and to the user and/or client.
  **************************************************************************/
  procedure CreateDistributionRecipients
    ( a_RelationshipId udt_Id,
      a_AsOfDate       Date );

  /************************************************************************
  * On a distribution list, POSSE can't create assignments based on an
  * indirect relationship to the staff object, so this procedure is required.
  * On post-verify (would be better on constructor, but the info isn't
  * available yet) of the review process attach this procedure. Configure
  * two columns on the process, a_User is a lookup to the OracleLogonId and
  * RunOnce is a boolean stored detail, RunOnce is used to tell
  * this procedure whether it has already run.
  * Logic: if there is a user supplied, and this procedure hasn't already
  * run, unassign everyone, and assign it to the a_User argument, then set
  * the RunOnce flag.
  **************************************************************************/
  /*
  procedure AssignRoutingProcess
    ( a_ProcessId udt_id,
      a_AsOfDate  Date,
      a_User      varchar2,
      a_RunOnce   char );
*/
/*
  procedure AssociateDistributionList (
    a_ObjectId               udt_Id,
    a_AsOfDate               date,
    a_RelationshipDefName    varchar2,
    a_DistributionListName   varchar2
  );
*/

  /*---------------------------------------------------------------------------
   * CreateRoutingProcesses()
   *-------------------------------------------------------------------------*/
 /*
  procedure CreateRoutingProcesses (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_DefaultProcessType                varchar2
  );
*/


end pkg_DistributionList;

 
/

create or replace package body pkg_DistributionList is

  procedure CreateDistributionRecipients
    ( a_RelationshipId udt_Id,
      a_AsOfDate       Date )
  is
    t_ProcessTypeId          udt_Id;
    t_ProcessId              udt_Id;
    t_ObjectTypeId           udt_Id;
    t_NewRel                 udt_id;
    t_ClientSelected         udt_StringList; --array to hold "Selected" attributes for client relationship
    t_ClientProcess          udt_StringList; --array to hold "CreateProcessType" attributes for client relationship
    t_UserSelected           udt_StringList; --array to hold "Selected" attributes for user relationship
    t_UserProcess            udt_StringList; --array to hold "CreateProcessType" attributes for user relationship
    t_Pointer                udt_id; -- pointer used to cycle through arrays


    begin
    --major assumption: the Distribution List functionality is on a PROCESS!
    select p.ProcessTypeId,
           p.ProcessId
      into t_ProcessTypeId,
           t_ProcessId
      from api.processes p,
           api.relationships r
     where r.relationshipid = a_RelationshipId
       and r.ToObjectId = p.ProcessId;

--FIRST TIME through
--selecting attributes and ToObjectID for relationship from <DistList> to <Client>/<User>
    for c in (Select api.pkg_columnquery.Value(r2.RelationshipId,'SelectedbyDefault') SelectedbyDefault,
                     api.pkg_columnquery.Value(r2.RelationshipId,'CreateProcessType') CreateProcessType,
                     r2.ToObjectId
                from query.o_ER_distributionlist o,
                     api.relationships r,
                     api.relationships r2
               where r.ToObjectId = o.Objectid
                 and r.RelationshipId = a_RelationshipId
                 and r.EndPointId = api.pkg_ConfigQuery.EndPointIdForName(api.pkg_objectdefquery.IdForName('p_SGDistListProcess'),'Distlist')
                 and o.Objectid = r2.FromObjectId
                 and r2.EndPointId in (api.pkg_ConfigQuery.EndPointIdForName(api.pkg_objectdefquery.IdForName('o_DistributionList'),'Client'),
                                       api.pkg_ConfigQuery.EndPointIdForName(api.pkg_objectdefquery.IdForName('o_DistributionList'),'Users'))
              )Loop

      --determining the type of the object: either Client or User
      select api.pkg_objectdefquery.IdForName(od.Name)
       into t_ObjectTypeId
       from api.objectdefs od,
            api.objects o
      where o.objectid = c.ToObjectid
        and o.ObjectDefId = od.ObjectDefId;


--creating the arrays that will be populated with the flags --> arrayname(ObjectID)
           --Clients
      Case when t_ObjectTypeId = api.pkg_objectdefquery.IdForName('o_Client') then

              t_ClientSelected(c.ToObjectid) := c.SelectedbyDefault;
              t_ClientProcess(c.ToObjectid) := c.CreateProcessType;

           --Users
           when t_ObjectTypeId = api.pkg_objectdefquery.IdForName('Users') then

              t_UserSelected(c.ToObjectid) := c.SelectedbyDefault;
              t_UserProcess(c.ToObjectid) := c.CreateProcessType;

      end case;

   end loop;

--SECOND TIME through
--selecting attributes and ToObjectID for relationship from <DistList> to <Roles> to <Client>/<User>
  for d in (Select api.pkg_columnquery.Value(r2.RelationshipId,'SelectedbyDefault') SelectedbyDefault,
                     api.pkg_columnquery.Value(r2.RelationshipId,'CreateProcessType') CreateProcessType,
                     r3.ToObjectId
                from query.o_ER_distributionlist o,
                     api.relationships r,
                     api.relationships r2,
                     api.relationships r3
               where r.ToObjectId = o.Objectid
                 and r.RelationshipId = a_RelationshipId
                 and o.Objectid = r2.FromObjectId
                 and r2.ToObjectId = r3.FromObjectId
                 and r2.EndPointId = api.pkg_ConfigQuery.EndPointIdForName(api.pkg_objectdefquery.IdForName('o_DistributionList'),'Roles')
                 and r3.EndPointId in (api.pkg_ConfigQuery.EndPointIdForName(api.pkg_objectdefquery.IdForName('o_Roles'),'Client'),
                                       api.pkg_ConfigQuery.EndPointIdForName(api.pkg_objectdefquery.IdForName('o_Roles'),'Users'))

  )Loop


      --determining the type of the object: either Client or User
      select api.pkg_objectdefquery.IdForName(od.Name)
       into t_ObjectTypeId
       from api.objectdefs od,
            api.objects o
      where o.objectid = d.ToObjectid
        and o.ObjectDefId = od.ObjectDefId;


--creating the arrays that will be populated with the flags --> arrayname(ObjectID)
           --Clients
      Case when t_ObjectTypeId = api.pkg_objectdefquery.IdForName('o_Client') then
            --"Selected" attribute
              --checks to see if the Client is already there (in the distribution list)
              if t_ClientSelected.exists(d.ToObjectid) then
                --if it does already exist, then only change the "Selected" attribute if the new value is 'Y'
                if d.selectedByDefault = 'Y' then
                   t_ClientSelected(d.ToObjectid) := d.selectedByDefault;
                end if;
              --if the client does not exist, then set the value
              else
                 t_ClientSelected(d.ToObjectid) := d.SelectedbyDefault;
              end if;

            --"CreateProcessType" attribute
              --if it doesn't exist, then set new value, otherwise do nothing
              if t_ClientProcess.exists(d.ToObjectid)=false then
                t_ClientProcess(d.ToObjectid) := d.CreateProcessType;
              end if;

           --Users
           when t_ObjectTypeId = api.pkg_objectdefquery.IdForName('Users') then
            --"Selected" attribute
              --checks to see if the User is already there (in the distribution list)
              if t_UserSelected.exists(d.ToObjectid) then
                --if it does already exist, then only change the "Selected" attribute if the new value is 'Y'
                if d.selectedByDefault = 'Y' then
                   t_UserSelected(d.ToObjectid) := d.selectedByDefault;
                end if;
              --if the user does not exist, then set the value
              else
                 t_UserSelected(d.ToObjectid) := d.SelectedbyDefault;
              end if;
            --"CreateProcessType" attribute
              --if it doesn't exist, then set new value, otherwise do nothing
              if t_UserProcess.exists(d.ToObjectid)=false then
                 t_UserProcess(d.ToObjectid) := d.CreateProcessType;
              end if;

      end case;

   end loop;

--creating the relationships and setting the attributes (using the arrays that were created above)
         --CLIENTS
           --setting the pointer to point to the first value in the array
           t_Pointer := t_ClientSelected.first;
           --loop thru all values in array, and create relationships and copy attributes to new relationship
           while t_Pointer is not null loop
                 t_NewRel := extension.pkg_relationshipupdate.New(t_ProcessId, t_Pointer, 'ProcessDistributionClient');
                 api.pkg_ColumnUpdate.SetValue( t_NewRel, 'Selected', t_ClientSelected(t_Pointer));
                 api.pkg_ColumnUpdate.SetValue(t_NewRel, 'CreateProcessType', t_ClientProcess(t_Pointer));
                 t_Pointer := t_ClientSelected.next( t_Pointer );
           end loop;

         --USERS
           --setting the pointer to point to the first value in the array
           t_Pointer := t_UserSelected.first;
           --loop thru all values in array, and create relationships and copy attributes to new relationship
           while t_Pointer is not null loop
                 t_NewRel := extension.pkg_relationshipupdate.New(t_ProcessId, t_Pointer, 'ProcessDistributionUser');
                 api.pkg_ColumnUpdate.SetValue( t_NewRel, 'Selected', t_UserSelected(t_Pointer));
                 api.pkg_ColumnUpdate.SetValue(t_NewRel, 'CreateProcessType', t_UserProcess(t_Pointer));
                 t_Pointer := t_UserSelected.next( t_Pointer );
           end loop;

      /*
      if c.LinkedCustomerObjectId is not null then
        --this will trigger another procedure, to set the user relationship to the current user
        t_RelId := api.pkg_RelationshipUpdate.New( t_DRCustomerEndPoint, t_DRObjectId, c.LinkedCustomerObjectId );
      else
        t_RelId := api.pkg_RelationshipUpdate.New( t_DRUserEndPoint, t_DRObjectId, c.LinkedUserId );
      end if;
      t_RelId := api.pkg_RelationshipUpdate.New( t_ProcessDREndPoint, t_ProcessId, t_DRObjectId );
      */

  end CreateDistributionRecipients;

end pkg_DistributionList;

/

