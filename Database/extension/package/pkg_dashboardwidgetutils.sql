create or replace package pkg_DashboardWidgetUtils is

  /*---------------------------------------------------------------------------
   * Constants - time period hack to get decent date for a demo
   *-------------------------------------------------------------------------*/
  gc_TimePeriodOffset constant number := 0;


  -- Author  : MICHAEL.FROESE
  -- Created : 7/21/2011 2:26:03 PM
  -- Purpose : Utils for Dashboard Widgets

  subtype udt_Id                        is api.pkg_definition.udt_Id;
  subtype udt_IdList                    is api.pkg_definition.udt_IdList;
  subtype udt_StringList                is api.pkg_definition.udt_StringList;
  subtype udt_NumberList                is api.pkg_definition.udt_NumberList;
  subtype udt_ObjectList                is api.udt_objectlist;
  subtype udt_DataMatrix                is extension.pkg_dashboarddefinition.udt_DataMatrix;
  subtype udt_UrlParameters             is extension.pkg_dashboarddefinition.udt_UrlParameters;
  subtype udt_Gauge                     is extension.pkg_dashboarddefinition.udt_Gauge;
  subtype udt_Data                      is extension.pkg_dashboarddefinition.udt_Data;
  subtype udt_DataList                  is extension.pkg_dashboarddefinition.udt_DataList;
  subtype udt_Widget                    is extension.pkg_dashboarddefinition.udt_Widget;
  subtype udt_WarningLevel              is extension.pkg_dashboarddefinition.udt_WarningLevel;
  subtype tbl_WarningLevel              is extension.pkg_dashboarddefinition.tbl_WarningLevel;
  subtype udt_NumberListByString        is extension.pkg_dashboarddefinition.udt_NumberListByString;

  gc_NullStringList			udt_StringList;

  /*   NewWidget()
   *
   *   Returns udt_Widget pre-set with details both from the o_DashboardWidget
   *     corresponding to a_WidgetId and the passed in Args.
   */
  function NewWidget(
    a_WidgetId                          pls_integer,
    a_WidgetArgs                        varchar2
  ) return udt_Widget;

  /*---------------------------------------------------------------------------
   * UserObjectList()
   * Returns a list of user objects.
   *-------------------------------------------------------------------------*/
  function UserObjectList(
    a_users                         varchar2
  ) return udt_ObjectList;
  /*---------------------------------------------------------------------------
   * GetUserDropDownListJSON()
   * Retuns users JSON result.
   *-------------------------------------------------------------------------*/
  function GetUserDropDownListJSON(
    a_AccessGroupName                  varchar2 default null,
    a_users                            varchar2 default null
    ) return clob;

  /*   toJSON()
   *
   *   Returns a JSON object as a CLOB for the udt_Widget provided.
   *     Optionally specify true for the second argument to obtain a pretty-
   *     printed CLOB
   */
  function ToJSON (
    a_Widget                            in udt_Widget,
    a_PrettyPrint                       boolean default false
  ) return clob;


  /*   ApplyDataMatrixToWidget()
   *   Takes four arguments, a_Matrix (which is the result of
   *     pkg_DashboardUtils.Group%), a_Widget, a_xList and a_sgList.
   *     The procedure processes the matrix and autosets the fieldnames,
   *     serieslabels, and data with the contents of the matrix.
   */
  procedure ApplyDataMatrixToWidget(
    a_Matrix                            in udt_DataMatrix,
    a_Widget                            in out nocopy udt_Widget,
    a_xList                             in udt_NumberListByString,
    a_sgList                            in udt_NumberListByString
  );

  /* WarningLevelStringList()
   *   Returns Widget-configured WarningLevel values in a udt_StringList
   */
  function WarningLevelStringList(
    a_WidgetId                          udt_Id
  ) return udt_StringList;

  /* WarningLevelTable()
   *   Returns columns key, value in a tbl_WarningLevel which can be cast
   *     into a table
   */
  function WarningLevelTable(
    a_WidgetId                          udt_Id
  ) return extension.pkg_DashboardDefinition.tbl_WarningLevel PIPELINED;

  /* StatusObjectList()
   *   Returns a udt_ObjectList of the statusids related to the
   *     widget depending on the Endpoint provided
   *   This is useful for joining to extensioncorral.jobs
       e.g.
        select jobid
          from extensioncorral.jobs j
          join table(extension.pkg_dashboardwidgetutils.StatusObjectList(
            a_WidgetId, 'StartStatus')) S
            on S.ObjectId = j.StatusId;
   */
  function StatusObjectList(
    a_WidgetId                          udt_Id,
    a_StatusEP                          varchar2
  ) return udt_ObjectList;

  /* DefIdObjectList()
   *   Returns a udt_ObjectList of the jobtypes or processtypes related to the
   *     widget depending on the Endpoint provided
   */
  function DefIdObjectList(
    a_WidgetId                          udt_Id,
    a_Endpoint                          varchar2
  ) return udt_ObjectList;

  /* DefIdList()
   *   Returns a udt_IdList of the jobtypes or processtypes related to the
   *     widget depending on the Endpoint provided
   */
  function DefIdList(
    a_WidgetId                          udt_Id,
    a_Endpoint                          varchar2
  ) return udt_IdList;

  /*---------------------------------------------------------------------------
   * SetGaugeValues()
   *-------------------------------------------------------------------------*/
  procedure SetGaugeValues (
    a_Widget				                    in out nocopy udt_Widget,
    a_Value				                      number,
    a_MinValue				                  number default null,
    a_MaxValue				                  number default null,
    a_Threshold1                        number default null,
    a_Threshold2                        number default null
  );

  /*---------------------------------------------------------------------------
   * SetSingleValue()
   *-------------------------------------------------------------------------*/
  procedure SetSingleValue (
    a_Widget				in out nocopy udt_Widget,
    a_Value				number,
    a_xLabel                            varchar2
  );

  -- for testing
  procedure DumpMatrix( a_Matrix in udt_DataMatrix);
end pkg_DashboardWidgetUtils;
 
/

grant execute
on pkg_dashboardwidgetutils
to posseextensions;

create or replace package body pkg_DashboardWidgetUtils is

  /*---------------------------------------------------------------------------
   * Constants
   *-------------------------------------------------------------------------*/
  gc_Gauge				constant varchar2(10) := pkg_DashboardDefinition.gc_Gauge;

  procedure CalculateTimePeriod(
    a_Widget                            in out nocopy udt_Widget
  ) is
  begin
    a_Widget.todate := trunc(sysdate);
    case
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Current week') or a_Widget.period = 'Current week' then
        a_Widget.fromdate := trunc(sysdate, 'IW');
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Current month') or a_Widget.period = 'Current month' then
        a_Widget.fromdate := trunc(sysdate, 'MM');
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Current quarter') or a_Widget.period = 'Current quarter' then
        a_Widget.fromdate := trunc(sysdate, 'Q');
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Current year') or a_Widget.period = 'Current year' then
        a_Widget.fromdate := trunc(sysdate, 'YY');
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Current year to date') or a_Widget.period = 'Current year to date' then
        a_Widget.fromdate := trunc(sysdate, 'YY');
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Previous week') or a_Widget.period = 'Previous week' then
        a_Widget.fromdate := trunc(sysdate - 7, 'IW');
        a_Widget.todate := a_Widget.fromdate + 6;
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Previous month') or a_Widget.period = 'Previous month' then
        a_Widget.fromdate := trunc(add_months(sysdate, -1), 'MM');
        a_Widget.todate := trunc(sysdate, 'MM') - 1;
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Previous quarter') or a_Widget.period = 'Previous quarter' then
        a_Widget.fromdate := trunc(add_months(sysdate,-3), 'Q');
        a_Widget.todate := trunc(sysdate, 'Q') - 1;
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Previous year') or a_Widget.period = 'Previous year' then
        a_Widget.fromdate := trunc(add_months(sysdate, -12), 'YY');
        a_Widget.todate := trunc(sysdate, 'YY') - 1;
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Last 3 months') or a_Widget.period = 'Last 3 months' then
        a_Widget.fromdate := trunc(add_months(sysdate,-3), 'MM');
        a_Widget.todate := trunc(sysdate, 'MM')-1;
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Last 6 months') or a_Widget.period = 'Last 6 months' then
        a_Widget.fromdate := trunc(add_months(sysdate,-6), 'MM');
        a_Widget.todate := trunc(sysdate, 'MM')-1;
      when (a_Widget.period is null and a_Widget.defaultperiod = 'Last 12 months') or a_Widget.period = 'Last 12 months' then
        a_Widget.fromdate := trunc(add_months(sysdate,-12), 'MM');
        a_Widget.todate := trunc(sysdate, 'MM')-1;
      when a_Widget.period = 'Custom' then
        null; --this is already taken care of in ApplyArgs
      else
        null;
    end case;

    a_Widget.fromdate_ := to_char(a_Widget.fromdate, 'mm/dd/yyyy');
    a_Widget.todate_   := to_char(a_Widget.todate, 'mm/dd/yyyy');
  end;

  procedure CalculateGaugeTimePeriod(
    a_Widget                            in out nocopy udt_Widget
  )  is
    t_CurrPrev                          varchar2(30) := api.pkg_ColumnQuery.Value( a_Widget.widgetId, 'GaugeCalculationCurrPrev');
    t_TimePeriod                        varchar2(50) := api.pkg_ColumnQuery.Value( a_Widget.widgetId, 'GaugeCalculationTimePeriod');
    t_delta                             pls_integer  := api.pkg_ColumnQuery.Value( a_Widget.widgetId, 'GaugeCalculationPrevXDays');
    t_CalcName                          varchar2(100):= api.pkg_ColumnQuery.Value( a_Widget.widgetId, 'GaugeCalculationName');
    t_PrevFromDate                      date;
    t_TruncBy                           varchar2(10);
  begin
    -- exit the procedure if the time period does not apply
    if a_Widget.widgettype = 'WarningGaugeCustom' then
      return;
    end if;

    t_PrevFromDate := sysdate;

    if t_TimePeriod = 'Day' then
      t_TruncBy := 'DD';
      t_PrevFromDate := t_PrevFromDate + (t_delta * -1);
    elsif t_TimePeriod = 'Week' then
      t_TruncBy := 'IW';
      t_PrevFromDate := t_PrevFromDate + (t_delta * -7);
    elsif t_TimePeriod = 'Month' then
      t_TruncBy := 'MM';
      t_PrevFromDate := add_months(t_PrevFromDate,(t_delta * -1));
    elsif t_TimePeriod = 'Quarter' then
      t_TruncBy := 'Q';
      t_PrevFromDate := add_months(t_PrevFromDate,(t_delta * -3));
    elsif t_TimePeriod = 'Year' then
      t_TruncBy := 'YYYY';
      t_PrevFromDate := add_months(t_PrevFromDate,(t_delta * -12));
    end if;

    if t_CurrPrev = 'Current' then
      a_Widget.ToDate := trunc(sysdate);
      a_Widget.FromDate := trunc(sysdate, t_TruncBy);
    else
      a_Widget.ToDate := trunc(sysdate,t_TruncBy)-1;
      a_Widget.FromDate := trunc(t_PrevFromDate, t_TruncBy);
    end if;

    a_Widget.fromdate_ := to_char(a_Widget.fromdate, 'mm/dd/yyyy');
    a_Widget.todate_   := to_char(a_Widget.todate, 'mm/dd/yyyy');
  end;

  procedure ApplyArgs(
    a_Widget                            in out nocopy udt_Widget,
    a_WidgetArgs                        varchar2
  ) is
    t_Args                              udt_URLParameters;
  begin
    t_Args := extension.pkg_dashboardutils.ParseQueryString(a_WidgetArgs);
    if t_Args.exists('GROUP1VALUE') then
      a_Widget.groupvalues(1) := replace(t_Args('GROUP1VALUE'), '+', ' ');
    end if;
    if t_Args.exists('GROUP2VALUE') then
      a_Widget.groupvalues(2) := replace(t_Args('GROUP2VALUE'), '+', ' ');
    end if;
    if t_Args.exists('GROUP3VALUE') then
      a_Widget.groupvalues(3) := replace(t_Args('GROUP3VALUE'), '+', ' ');
    end if;
    if t_Args.exists('GROUP4VALUE') then
      a_Widget.groupvalues(4) := replace(t_Args('GROUP4VALUE'), '+', ' ');
    end if;

    if t_Args.exists('PERIOD') then
      a_Widget.period := replace(t_Args('PERIOD'),'+', ' ');
    end if;

    if t_Args.exists('USERS') then
      a_Widget.users_ := t_Args('USERS');
    end if;

    if t_Args.exists('VIEW') then
      a_Widget.view_ := t_Args('VIEW');
    end if;

    if a_Widget.period = 'Custom' then
      if t_Args.exists('FROMDATE') then
        a_Widget.fromdate_ := t_Args('FROMDATE');
        a_Widget.fromdate := to_date(a_Widget.fromdate_, 'mm/dd/yyyy');
      end if;

      if t_Args.exists('TODATE') then
        a_Widget.todate_ := t_Args('TODATE');
        a_Widget.todate := to_date(a_Widget.todate_, 'mm/dd/yyyy');
      end if;

    elsif a_Widget.period = 'All' then
      null;
    elsif a_Widget.displaytype = gc_Gauge then
      CalculateGaugeTimePeriod( a_Widget );
    else
      CalculateTimePeriod( a_Widget );
    end if;
  end;

  function NewWidget(
    a_WidgetId                           pls_integer,
    a_WidgetArgs                         varchar2
  ) return udt_Widget is
    t_Widget                             udt_Widget;
    t_GroupExpression                    varchar2(4000);
    t_GroupLabel                         varchar2(4000);
  begin
    t_Widget.widgetid        := a_WidgetId;
    t_Widget.widgettype      := api.pkg_columnquery.Value(a_WidgetId, 'WidgetTypeName');
    t_Widget.title           := api.pkg_columnquery.Value(a_WidgetId, 'Title');
    t_Widget.displaytype     := api.pkg_columnquery.Value(a_WidgetId, 'DisplayTypeName');
    t_Widget.accessgroup     := api.pkg_columnquery.Value(a_WidgetId, 'AccessGroup');
    t_Widget.xaxislabel      := api.pkg_columnquery.Value(a_WidgetId, 'XAxisLabel');
    t_Widget.yaxislabel      := api.pkg_columnquery.Value(a_WidgetId, 'YAxisLabel');
    t_Widget.defaultperiod   := api.pkg_columnquery.Value(a_WidgetId, 'DefaultTimePeriod');
    t_Widget.timeperioddetail   := api.pkg_columnquery.Value(a_WidgetId, 'TimePeriodDetail');
    --t_Widget.subgroup        := nvl(api.pkg_columnquery.Value(a_WidgetId, 'Group1Expression'),api.pkg_columnquery.Value(a_WidgetId, 'GroupDetail'));
    t_Widget.subgroup        := api.pkg_columnquery.Value(a_WidgetId, 'GroupDetail');
    t_Widget.drilldownurl    := api.pkg_columnquery.Value(a_WidgetId, 'DrillDownURL');

    -- Grouping
    for i in 1..4 loop
      t_GroupExpression := api.pkg_columnquery.Value(a_WidgetId, 'Group'||i||'Expression');
      if t_GroupExpression is not null then
        t_GroupLabel := api.pkg_columnquery.Value(a_WidgetId, 'Group'||i||'Label');
        if t_GroupLabel is null then
          api.pkg_Errors.RaiseError(-20000, 'A label must be configured from Group '||i||' on widget '||a_WidgetId);
        end if;
        t_Widget.grouplabels(i) := t_GroupLabel;
      end if;
    end loop;
    t_Widget.displaytwogroups := api.pkg_columnquery.Value(a_WidgetId, 'DisplayTwoGroups');

    -- Gauges
    t_Widget.maxyvalue       := 0;
    if t_Widget.displaytype = gc_Gauge then
      t_Widget.gauge.min := api.pkg_columnquery.Value(a_WidgetId, 'GaugeMin');
      t_Widget.gauge.max := api.pkg_columnquery.Value(a_WidgetId, 'GaugeMax');
      if api.pkg_columnquery.Value(a_WidgetId, 'DisplayAsPercentage') = 'Y' then
        t_Widget.gauge.pct := 'true';
      end if;
    end if;
    ApplyArgs( t_Widget, a_WidgetArgs);


    /* Time period hack for demo */
/*    if t_Widget.FromDate is not null then
      t_Widget.FromDate := add_months(t_Widget.FromDate, gc_TimePeriodOffset);
    end if;
    if t_Widget.ToDate is not null then
      t_Widget.ToDate := add_months(t_Widget.ToDate, gc_TimePeriodOffset);
    end if;*/

    return t_Widget;
  end;

  function collapse(
    a_StringList                       udt_StringList
  ) return clob is
    t_Clob                             clob;
    i                                  pls_integer;
  begin
    for i in 1..a_StringList.count loop
      if i = 1 then
        t_Clob := a_StringList(1);
      else
        t_Clob := t_Clob || ','||a_StringList(i);
      end if;
    end loop;
    return t_Clob;
  end collapse;

  /*---------------------------------------------------------------------------
   * UserObjectList()
   *-------------------------------------------------------------------------*/
 function UserObjectList(
    a_users                         varchar2
  ) return udt_ObjectList is
    t_ObjectList                       udt_ObjectList := api.udt_objectlist();
    t_users                            udt_StringList;
  begin
    t_users := extension.pkg_Utils.Split(a_users, ',');
    for i in 1..t_users.count loop
      t_ObjectList.Extend();
      t_ObjectList(t_ObjectList.count) := api.udt_object(t_users(i));
    end loop;
    return t_ObjectList;
  end;

  /*---------------------------------------------------------------------------
   * GetUserDropDownListJSON()
   *-------------------------------------------------------------------------*/
  function GetUserDropDownListJSON(
    a_AccessGroupName                  varchar2 default null,
    a_users                            varchar2 default null
  ) return clob is
    t_JSON                             clob;
    t_UserIds                          udt_IdList;
  begin
    if a_AccessGroupName is null then
       select u.UserId
         bulk collect into t_UserIds
         from api.users u
        where u.Active = 'Y'
          order by u.Name;
    else
       if a_users is not null then
         select u.UserId
           bulk collect into t_UserIds
           from api.users u
           join table(extension.pkg_DashboardWidgetUtils.UserObjectList(a_users)) argusers
             on argusers.ObjectId = u.UserId
           join api.accessgroupusers agu
             on agu.UserId = argusers.ObjectId
            and u.Active = 'Y'
           join api.accessgroups ag
             on ag.AccessGroupId = agu.AccessGroupId
          where upper(ag.Description) = upper(a_AccessGroupName)
          order by u.Name;
        else
         select u.UserId
           bulk collect into t_UserIds
           from api.users u
           join api.accessgroupusers agu
             on agu.UserId = u.UserId
            and u.Active = 'Y'
           join api.accessgroups ag
             on ag.AccessGroupId = agu.AccessGroupId
          where upper(ag.Description) = upper(a_AccessGroupName)
          order by u.Name;
        end if;
     end if;

     t_JSON :=
       ' users: [';
       for i in 1..t_UserIds.count loop
          t_JSON := concat(t_JSON, '{id: '|| t_UserIds(i)||', name: "'||api.pkg_columnquery.Value(t_UserIds(i),'Name')||'"},');

       end loop;

       if t_UserIds.count > 0 then
          t_JSON := concat(substr(t_JSON, 1, length(t_JSON)-1), ']');
       else
          t_JSON := concat(t_JSON,']');
       end if;

    return t_JSON;
  end;

  function ToJSON (
    a_Widget                        in udt_Widget,
    a_PrettyPrint                      boolean default false
  ) return clob is
    t_JSON                             clob;
    t_JSON_users                       clob;
    t_JSON_tmp                         varchar2(32766);
    t_CR                               varchar2(1) := chr(13);
    t_Tab                              varchar2(1) := chr(9);
    i                                  pls_integer;
    j                                  pls_integer;
    t_userId                           udt_Id := api.pkg_securityquery.EffectiveUserId();
  begin
    if not a_PrettyPrint then
      t_CR := '';
      t_Tab := '';
    end if;
    /*  Note, that || is a string operator and is limited to 32k ... so when we
     *  build the data, we will use a temp string and concat() it to the clob
     */
    --Build JSON clob
    t_JSON := '{'    ||t_CR;
    t_JSON := t_JSON || t_Tab || 'widgetid: '       || a_Widget.widgetid      ||','  || t_CR;
    t_JSON := t_JSON || t_Tab || 'widgettype: "'    || a_Widget.widgettype    ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'dashboardwidgetxrefid: ' || nvl(a_Widget.dashboardwidgetxrefid,0)      ||',' || t_CR;
    t_JSON := t_JSON || t_Tab || 'visible: '       || case when a_Widget.visible = 'N' then 'false' else 'true' end       ||',' || t_CR;
    t_JSON := t_JSON || t_Tab || 'category: "'      || a_Widget.category      ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'title: "'         || a_Widget.title         ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'displaytype: "'   || a_Widget.displaytype   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'accessgroup: "'   || a_Widget.accessgroup   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'xaxislabel: "'    || a_Widget.xaxislabel    ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'yaxislabel: "'    || a_Widget.yaxislabel    ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'defaultperiod: "' || a_Widget.defaultperiod ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'timeperioddetail: "' || a_Widget.timeperioddetail ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'period: "'        || a_Widget.period        ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'fromdate: "'      || a_Widget.fromdate_     ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'todate: "'        || a_Widget.todate_       ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'view: "'          || a_Widget.view_         ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'user: "'          || t_userId               ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'subgroup: "'      || a_Widget.subgroup      ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'maxyvalue: "'     || a_Widget.maxyvalue     ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'drilldownurl: "'  || a_Widget.drilldownurl  ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'fieldnames: "'    || collapse(a_Widget.fieldnames)   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'serieslabels: "'  || collapse(a_Widget.serieslabels) ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'displaytwogroups: "'|| a_Widget.displaytwogroups      ||'",' || t_CR;
    -- Group labels
    for i in 1..4 loop
      if a_Widget.grouplabels.exists(i) then
        t_JSON := t_JSON || t_Tab || 'group'||i||'label: "'  || a_Widget.grouplabels(i) ||'",' || t_CR;
      end if;
    end loop;

    -- Group Values
    for i in 1..4 loop
      if a_Widget.groupvalues.exists(i) then
        t_JSON := t_JSON || t_Tab || 'group'||i||'value: "'  || a_Widget.groupvalues(i) ||'",' || t_CR;
      end if;
    end loop;

    --Gauge object
    t_JSON := t_JSON || t_Tab || 'gauge: '          || '{'                  || t_CR;
    t_JSON := t_JSON || t_Tab || t_Tab || 'min: '   || nvl(a_Widget.gauge.min,0)   ||','  || t_CR;
    t_JSON := t_JSON || t_Tab || t_Tab || 'max: '   || nvl(a_Widget.gauge.max,1)   ||','  || t_CR;
    t_JSON := t_JSON || t_Tab || t_Tab || 'alert: ' || a_Widget.gauge.alert ||','  || t_CR;
    t_JSON := t_JSON || t_Tab || t_Tab || 'pct: '   || a_Widget.gauge.pct   || t_CR;
    t_JSON := t_JSON || t_Tab || '},'  || t_CR;
    --Data object array
    t_JSON := t_JSON || t_Tab || 'data: ' || '[' || t_CR;
    for i in 1..a_Widget.data.count loop
      t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab || '{' || t_CR;
      t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab || t_Tab|| 'x: "' || a_Widget.data(i).x ||'",' || t_CR;
      for j in 1..a_Widget.data(i).y.count loop
        if j = a_Widget.data(i).y.count then
          if a_Widget.data(i).y(j) is not null then
            t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab || t_Tab|| 'y'||j||': ' || a_Widget.data(i).y(j) || t_CR;
          end if;
        else
          if a_Widget.data(i).y(j) is not null then
            --dbms_output.put_line(i||'-'||j||': '||a_Widget.data(i).y(j)||':::'||length(t_JSON_tmp));
            t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab || t_Tab|| 'y'||j||': ' || a_Widget.data(i).y(j) ||',' || t_CR;
          end if;
        end if;
      end loop;
      if i = a_Widget.data.count then
        t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab|| '}' || t_CR;
      else
        t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab|| '},'|| t_CR;
      end if;
      t_JSON := concat( t_JSON, t_JSON_tmp );
      t_JSON_tmp := '';
    end loop;

    t_JSON := concat( t_JSON, t_Tab || ']');

    if a_Widget.widgettype = 'ToDoListSummary' then
       t_JSON_users := GetUserDropDownListJSON(a_Widget.accessgroup,a_Widget.users_);
       t_JSON := concat(t_JSON,','||t_JSON_users);
    end if;

    t_JSON := concat( t_JSON, t_CR || '}');

    return t_JSON;
  end;

  procedure DumpMatrix( a_Matrix in udt_DataMatrix) is
    t_x             varchar2(1000);
    t_y             varchar2(1000);
  begin
    t_x := a_Matrix.first();
    while t_x is not null loop
      dbms_output.put_line(t_x);
      t_y := a_Matrix(t_x).first();
      while t_y is not null loop
        dbms_output.put_line('  ' || t_y || ':' || a_Matrix(t_x)(t_y));
        t_y := a_Matrix(t_x).next(t_y);
      end loop;
      t_x := a_Matrix.next(t_x);
    end loop;
  end;

  /*   AutoSetFieldNames()
   *
   *   Auto sets the fieldnames attribute of a widget based on how many
   *     serieslabels are present
   */
  procedure AutoSetFieldNames(
    a_Widget                           in out nocopy udt_Widget
  ) is
    t_StringList                       udt_StringList;
    i                                  pls_integer;
  begin
    a_Widget.fieldnames(1) := 'x';
    if a_Widget.serieslabels.count = 0 then
      a_Widget.fieldnames(2) := 'y1';
    else
      for i in 1..a_Widget.serieslabels.count loop
        a_Widget.fieldnames(i+1) := 'y'||i;
      end loop;
    end if;
  end;

  procedure ApplyDataMatrixToWidget(
    a_Matrix                            in udt_DataMatrix,
    a_Widget                            in out nocopy udt_Widget,
    a_xList                             in udt_NumberListByString,
    a_sgList                            in udt_NumberListByString
  ) is
    t_x             varchar2(100);
    t_sg            varchar2(100);
  begin
    -- Note that xList and sgList are sparse arrays indexed by the group or
    -- sub-group value with the seqnum being the data portion
    -- e.g. ['East' : 2, 'North' : 1, 'South' : 3, 'West' : 4]

    -- define the series labels on the Widget when more than one
    if a_sgList.count > 1 then
      t_sg := a_sgList.first();
      while t_sg is not null loop
        -- when done this array should be dense
        a_Widget.SeriesLabels(a_sgList(t_sg)) := t_sg;
        t_sg := a_sgList.next(t_sg);
      end loop;
      end if;

    -- load the widget data structure from the Matrix using the Orderd x and sg
    -- lists to indicate the array positions
    t_x := a_xList.first();
      while t_x is not null loop
      t_sg := a_sgList.first();
      while t_sg is not null loop
        a_Widget.Data(a_xList(t_x)).x := t_x;
        a_Widget.Data(a_xList(t_x)).y(a_sgList(t_sg)) := a_Matrix(t_x)(t_sg);
        a_Widget.MaxYValue := greatest(a_Widget.MaxYValue, a_Matrix(t_x)(t_sg));
        t_sg := a_sgList.next(t_sg);
      end loop;
      t_x := a_xList.next(t_x);
        end loop;

    AutoSetFieldNames( a_Widget );

  end ApplyDataMatrixToWidget;

/*  function WarningLevelKeyList(
    a_WidgetId                         udt_Id
  ) return udt_NumberList is
    t_NumberList                       udt_NumberList;
    t_IdList                           udt_IdList;
    i                                  pls_integer;
  begin
    t_IdList := extension.pkg_ObjectQuery.RelatedObjects(a_WidgetId, 'WarningLevel');
    for i in 1..t_IdList.count loop
      t_NumberList(i) := api.pkg_ColumnQuery.NumericValue( t_IdList(i), 'Value');
    end loop;
    return t_NumberList;
  end;
*/
  function WarningLevelStringList(
    a_WidgetId                         udt_Id
  ) return udt_StringList is
    t_StringList                       udt_StringList;
    t_IdList                           udt_IdList;
    i                                  pls_integer;
  begin
    t_IdList := extension.pkg_ObjectQuery.RelatedObjects(a_WidgetId,
      'WarningLevel');
    for i in 1..t_IdList.count loop
      t_StringList(api.pkg_ColumnQuery.NumericValue( t_IdList(i), 'Value')) :=
        api.pkg_ColumnQuery.Value( t_IdList(i), 'Label');
    end loop;
    return t_StringList;
  end;

  function WarningLevelTable(
    a_WidgetId                         udt_Id
  ) return extension.pkg_DashboardDefinition.tbl_WarningLevel PIPELINED is
    i                                  pls_integer;
    t_WarningLevel                     udt_WarningLevel;
    t_IdList                           udt_IdList;
  begin
    t_IdList := extension.pkg_ObjectQuery.RelatedObjects(a_WidgetId,
      'WarningLevel');
    for i in 1..t_IdList.count loop
      t_WarningLevel.key := api.pkg_ColumnQuery.NumericValue( t_IdList(i),
        'Value');
      t_WarningLevel.val := api.pkg_ColumnQuery.Value( t_IdList(i), 'Label');
      PIPE ROW(t_WarningLevel);
    end loop;
  end;

  function StatusObjectList(
    a_WidgetId                         udt_Id,
    a_StatusEP                         varchar2
  ) return udt_ObjectList is
    t_ObjectList                       udt_ObjectList := api.udt_objectlist();
    t_IdList                           udt_IdList;
    i                                  pls_integer;
    t_StatusId                         udt_Id;
  begin
    t_IdList := extension.pkg_ObjectQuery.RelatedObjects(a_WidgetId,
      a_StatusEP);
    for i in 1..t_IdList.count loop
      select statusid into t_StatusId
        from api.statuses s
        where s.Tag = api.pkg_ColumnQuery.Value( t_IdList(i), 'StatusName');
      t_ObjectList.Extend();
      t_ObjectList(t_ObjectList.count) := api.udt_object(t_StatusId);
    end loop;
    return t_ObjectList;
  end;

  function DefIdObjectList(
    a_WidgetId                         udt_Id,
    a_Endpoint                         varchar2
  ) return udt_ObjectList is
    t_ObjectList                       udt_ObjectList := api.udt_objectlist();
    t_IdList                           udt_IdList;
    i                                  pls_integer;
  begin
    t_IdList := extension.pkg_ObjectQuery.RelatedObjects(a_WidgetId, a_EndPoint);
    for i in 1..t_IdList.count loop
      t_ObjectList.Extend();
      t_ObjectList(t_ObjectList.count) := api.udt_object(
        api.pkg_configquery.ObjectDefIdForName(api.pkg_ColumnQuery.Value(
        t_IdList(i), 'Dashboard_ObjectDefName')));
    end loop;
    return t_ObjectList;
  end;

  function DefIdList(
    a_WidgetId                         udt_Id,
    a_Endpoint                         varchar2
  ) return udt_IdList is
    t_IdList                           udt_IdList;
    t_DefIdList                        udt_IdList;
    i                                  pls_integer;
  begin
    t_IdList := extension.pkg_ObjectQuery.RelatedObjects(a_WidgetId,
      a_EndPoint);
    for i in 1..t_IdList.count loop
      t_DefIdList(i) := api.pkg_configquery.ObjectDefIdForName(
        api.pkg_ColumnQuery.Value(t_IdList(i), 'Dashboard_ObjectDefName'));
    end loop;
    return t_DefIdList;
  end;

  /*---------------------------------------------------------------------------
   * SetGaugeValues()
   *-------------------------------------------------------------------------*/
  procedure SetGaugeValues (
    a_Widget				                    in out nocopy udt_Widget,
    a_Value				                      number,
    a_MinValue				                  number default null,
    a_MaxValue				                  number default null,
    a_Threshold1                        number default null,
    a_Threshold2                        number default null
  ) is
    t_Threshold1                        number;
    t_Threshold2                        number;
    t_Value                             number;
    t_Alert				number := 0;
  begin
    t_Threshold1 := nvl(a_Threshold1, nvl(api.pkg_ColumnQuery.Value(a_Widget.WidgetId,
        'GaugeLevel1Threshold'), 0));
    t_Threshold2 := nvl(a_Threshold2, nvl(api.pkg_ColumnQuery.Value(a_Widget.WidgetId,
        'GaugeLevel2Threshold'), 0));

    a_Widget.fieldnames(1) := 'x';
    a_Widget.fieldnames(2) := 'y1';
    a_Widget.data(1).x := 'Needle';

    if a_Widget.gauge.pct = 'true' then
      a_Widget.gauge.min := 0;
      a_Widget.gauge.max := 100;
      if a_Value = 0 then
        a_Widget.data(1).y(1) := 0;
      else
        a_Widget.data(1).y(1) := round(
            ((a_Value - a_MinValue)/(a_MaxValue - a_MinValue))*100, 0);
      end if;
    else
      a_Widget.gauge.min := nvl(a_MinValue, a_Widget.gauge.min);
      a_Widget.gauge.max := nvl(a_MaxValue, a_Widget.gauge.max);
      a_Widget.gauge.max := greatest(a_Widget.gauge.max, a_Value);
      a_Widget.data(1).y(1) := a_Value;
    end if;
    t_Value := a_Widget.data(1).y(1);

    -- ensure the gauge max is at least 1
    a_Widget.gauge.max := greatest(a_Widget.gauge.max, 1);

    -- set Alert value
    if t_Threshold1 is not null and t_Threshold1 < t_Threshold2 then
      if t_Value < t_Threshold1 then
        t_Alert := 0;
      elsif t_Value < t_Threshold2 then
        t_Alert := 1;
      else
        t_Alert := 2;
      end if;
    elsif t_Threshold1 is not null then
      if t_Value < t_Threshold2 then
        t_Alert := 2;
      elsif t_Value < t_Threshold1 then
        t_Alert := 1;
      else
        t_Alert := 0;
      end if;
    end if;
    a_Widget.gauge.alert := t_Alert;
  end SetGaugeValues;

  /*---------------------------------------------------------------------------
   * SetSingleValue()
   *-------------------------------------------------------------------------*/
  procedure SetSingleValue (
    a_Widget				in out nocopy udt_Widget,
    a_Value				number,
    a_xLabel                            varchar2
  ) is
  begin
    a_Widget.Data(1).x := a_xLabel;
    a_Widget.Data(1).y(1) := a_Value;
    a_Widget.MaxYValue := a_Value;
    AutoSetFieldNames(a_Widget);
  end SetSingleValue;

end pkg_DashboardWidgetUtils;
/

