create or replace package pkg_AuditTool is

  /*---------------------------------------------------------------------------
   * Types -- PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * DetermineChangedColumns() -- PUBLIC
   *   Returns a list of column names whose value has changed
   *-------------------------------------------------------------------------*/
  function DetermineChangedColumns (
    a_ObjectAuditId             udt_Id
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * DetermineChangedRelationships() -- PUBLIC
   *   Returns a list of relationship labels where associated
   *   relationships have changed
   *-------------------------------------------------------------------------*/
  function DetermineChangedRelationships (
    a_ObjectAuditId             udt_Id
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * FormatColumn() -- PUBLIC
   *   Formats the value of an audited columns.  Currently this only reformats
   * date time columns from Posse's internal format to a more readable format.
   *-------------------------------------------------------------------------*/
  function FormatColumn (
    a_Value                             varchar2
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * FormatColumn() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function FormatColumn (
    a_Value                             date
  ) return varchar2;

end pkg_AuditTool;
 
/

grant execute
on pkg_audittool
to posseextensions;

create or replace package body pkg_AuditTool is

  /*---------------------------------------------------------------------------
   * Global Constants -- PRIVATE
   *-------------------------------------------------------------------------*/
  gc_AuditDateFormat constant varchar2(50) := 'Mon dd, yyyy hh:mi:ss AM';

  /*---------------------------------------------------------------------------
   * DetermineChangedColumns() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DetermineChangedColumns (
    a_ObjectAuditId             udt_Id
  ) return varchar2 is
    t_ChangedColumns            varchar2(4000);
  begin
    for c in (select OldColumnValue,
                     NewColumnValue,
                     ColumnName
                from ObjectAuditData oud,
                     ObjectAudit     ou
               where ou.ObjectAuditId = a_ObjectAuditId
                 and ou.AuditType = 'C'
                 and oud.ObjectAuditId = ou.ObjectAuditId) loop
      if c.OldColumnValue <> c.NewColumnValue or c.OldColumnValue is null and c.NewColumnValue is not null or c.NewColumnValue is null and c.OldColumnValue is not null then
        t_ChangedColumns := t_ChangedColumns || ', ' || c.ColumnName;
      end if;
    end loop;
    return substr(t_ChangedColumns, 3);
  end;

  /*---------------------------------------------------------------------------
   * DetermineChangedRelationships() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DetermineChangedRelationships (
    a_ObjectAuditId             udt_id
  ) return varchar2 is
    t_ChangedRelationships            varchar2(4000);
  begin
    for c in (select unique rn.RelationshipName
                from api.relationshipnames rn,
                     api.relationshipdefs rd,
                     objectauditrelationshipdata D
               where d.ObjectAuditId = a_ObjectAuditId
                 and d.flag in ('New', 'Deleted')
                 and d.toendpointid = rd.ToEndPointId
                 and rn.relationshipnameid = rd.fromrelationshipnameid) loop
        t_ChangedRelationships := t_ChangedRelationships || ', ' || c.RelationshipName;
    end loop;
    return substr(t_ChangedRelationships, 3);
  end;

  /*---------------------------------------------------------------------------
   * FormatColumn() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function FormatColumn (
    a_Value                             varchar2
  ) return varchar2 is
    t_Date                              date;
    t_FormattedValue                    varchar2(4000);
  begin
    t_FormattedValue := a_Value;
    begin
      t_Date := to_date(a_Value, api.pkg_Definition.gc_DateFormat);
      t_FormattedValue := to_char(t_Date, gc_AuditDateFormat);
    exception
    when others then
      null;
    end;

    return t_FormattedValue;
  end;

  /*---------------------------------------------------------------------------
   * FormatColumn() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function FormatColumn (
    a_Value                             date
  ) return varchar2 is
  begin
    return to_char(a_Value, gc_AuditDateFormat);
  end;

end pkg_AuditTool;
/

