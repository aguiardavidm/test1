create or replace package           pkg_RelUtils is

  -----------------------------------------------------------------------------
  -- Types
  -----------------------------------------------------------------------------
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_EndPointList is api.pkg_definition.udt_EndPointList;
  subtype udt_EndPoint is api.pkg_definition.udt_EndPoint;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  -----------------------------------------------------------------------------
  -- AssignedStaffNames()
  -----------------------------------------------------------------------------
  function AssignedStaffNames(a_ProcessId udt_Id) return varchar2;

  -----------------------------------------------------------------------------
  -- RelateCurrentUser()
  -----------------------------------------------------------------------------
  procedure RelateCurrentUser(a_ObjectId    udt_Id,
                              a_AsofDate    date,
                              a_CurrentUser varchar2);

  -----------------------------------------------------------------------------
  -- RelExists()
  -----------------------------------------------------------------------------
  function RelExists(a_ObjectId pls_integer, a_EndPointName varchar2)
    return varchar2;

  -----------------------------------------------------------------------------
  -- StoredRelExistsViaEPNames()
  -----------------------------------------------------------------------------
  function StoredRelExistsViaEPNames (
    a_ObjectId                          pls_integer,
    a_EndPointNameList                  varchar2
  ) return char;

  -----------------------------------------------------------------------------
  -- RemoveRels()
  -----------------------------------------------------------------------------
  procedure RemoveRels(a_ObjectId      pls_integer,
                       a_AsOfDate      date,
                       a_ObjectDefName varchar2,
                       a_EndPointNames varchar2);

  -----------------------------------------------------------------------------
  -- RemoveExistingRelsFromObject()
  --   Remove all relationships for the given object def name and the list of
  -- EndPoint names specified. Where RemoveRels is called by the 'From' object
  -- RemoveRelsToObject is called by the 'To' object.
  -----------------------------------------------------------------------------
  procedure RemoveExistingRelsFromObject(a_ObjectId      pls_integer,
                                         a_AsOfDate      date,
                                         a_FromObjectId  pls_integer,
                                         a_EndPointNames varchar2);

  /********************************************************************************************
  * RemoveOlderRels() -- PUBLIC
  * Author:  Greg Hassan
  * Created: 2007.12.17
  * Purpose: RemoveOlderRels removes all but the newest relationship for a given
  *          set of EndPoints.
  ********************************************************************************************/
  procedure RemoveOlderRels(a_ObjectId           pls_integer,
                            a_AsOfDate           date,
                            a_EndPointNames      varchar2,
                            a_ShouldDeleteObject varchar2 default 'N');

  /********************************************************************************************
  * EndPointIdsFromNames() -- PUBLIC
  * Author:  Greg Hassan
  * Created: 2007.12.17
  * Purpose: Returns a list of End Point Ids for a given list of End Point Names
  ********************************************************************************************/
  function EndPointIdsFromNames(t_ObjectDefId   pls_integer,
                                a_EndPointNames varchar2) return udt_IdList;

  -----------------------------------------------------------------------------
  -- VerifyRelPairs()
  -----------------------------------------------------------------------------
  procedure VerifyRelPairs(a_ObjectId      pls_integer,
                           a_AsOfDate      date,
                           a_JobColumnName varchar2);

  -----------------------------------------------------------------------------
  --RemoveRelsFromCheckbox()
  -----------------------------------------------------------------------------
  procedure RemoveRelsFromCheckbox(a_ObjectId      udt_Id,
                                   a_AsOfDate      date,
                                   a_ObjectDefName varchar2,
                                   a_EndPointNames varchar2,
                                   a_Column        varchar2);

  ----------------------------------------------------------------------------
  --Remove Rels On Process Complete()
  -----------------------------------------------------------------------------
  procedure RemoveRelsOnProcessComplete(a_ObjectId      udt_Id,
                                        a_AsOfDate      date,
                                        a_ObjectDefName varchar2,
                                        a_EndPointNames varchar2);

  /*******************************************************************
  *  CopyRelsFromLatestProcess()
  * Copies specified rels (by comma-separated list of endpoint names)
  * from the latest process of a specified type. Can also specify
  * whether or not to only copy the latest rel for each endpoint.
  *******************************************************************/
  Procedure CopyRelsFromLatestProcess(
    a_Objectid           udt_id,
    a_asofdate           date,
    a_FromProcessType    varchar2,
    a_IncludeIncomplete  varchar2 default 'N',
    a_EndPointList       varchar2,
    a_OnlyMostRecent     varchar2 default 'N'
  );

end pkg_RelUtils;

 
/

grant execute
on pkg_relutils
to abc;

grant execute
on pkg_relutils
to ereferral;

grant execute
on pkg_relutils
to posseextensions;

create or replace package body           pkg_RelUtils is

  -----------------------------------------------------------------------------
  -- Types
  -----------------------------------------------------------------------------
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  -----------------------------------------------------------------------------
  -- AssignedStaffNames() -- PUBLIC
  --   Return a comma and space separated list of staff assigned to the process
  -----------------------------------------------------------------------------
  function AssignedStaffNames(a_ProcessId udt_Id) return varchar2 is
    t_StaffList varchar2(4000);

    cursor c_Assignments is
    select u.Name
      from api.Users u
      join api.ProcessAssignments pa
        on pa.UserId = u.UserId
     where pa.ProcessId = a_ProcessId
    order by u.Name;
  begin
    for a in c_Assignments loop
      extension.pkg_Utils.AddSection(t_StaffList, a.Name, ', ');
    end loop;

    return t_StaffList;
  end AssignedStaffNames;

  -----------------------------------------------------------------------------
  -- RelateCurrentUser()
  -----------------------------------------------------------------------------
  procedure RelateCurrentUser(a_ObjectId    udt_Id,
                              a_AsofDate    date,
                              a_CurrentUser varchar2) is
    t_CurrentUserObjectId udt_Id;
    t_Dummy               udt_Id;

    cursor existinguserrels is
      select r.relationshipid
        from api.relationships r
        join api.relationshipdefs rd on r.relationshipdefid =
                                        rd.relationshipdefid
       where r.fromobjectid = a_ObjectId
         and r.toobjectid = t_CurrentUserObjectId
         and rd.ToEndPointName = 'User';

  begin
    -- Find the current user
    select ObjectId
      into t_CurrentUserObjectId
      from query.u_Users u
     where u.OracleLogonId = a_CurrentUser
       and u.ObjectDefTypeId = 24;

    -- remove any existing rels if they exist
    for i in existinguserrels loop
      api.pkg_relationshipupdate.Remove(i.relationshipid, sysdate);
    end loop;

    -- relate the current user to the object
    t_Dummy := extension.pkg_RelationshipUpdate.New(a_ObjectId,
                                                    t_CurrentUserObjectId,
                                                    'User');

  end RelateCurrentUser;

  -----------------------------------------------------------------------------
  -- EndPointIds() -- PRIVATE
  -----------------------------------------------------------------------------
  function EndPointIds (
    a_ObjectDefId                       pls_integer,
    a_EndPointName                      varchar2
  ) return udt_IdList is
    t_EndPointNames                     udt_StringList;
    t_EndPointIds                       udt_IdList;
  begin
    t_EndPointNames := extension.pkg_Utils.Split(a_EndPointName, ',');
    for i in 1..t_EndPointNames.count loop
      t_EndPointIds(i) := api.pkg_ConfigQuery.EndPointIdForName(a_ObjectDefId,
          t_EndPointNames(i));

      if t_EndPointIds(i) is null then
        api.pkg_Errors.SetArgValue('EPName', t_EndPointNames(i));
        api.pkg_Errors.SetArgValue('ObjDef', a_ObjectDefId);
        api.pkg_Errors.RaiseError(-20000,
            'Invalid end point name "{EPName}" for object def: {ObjDef}.');
      end if;
    end loop;

    return t_EndPointIds;
  end EndPointIds;

  -----------------------------------------------------------------------------
  -- RelExists() -- PRIVATE
  -----------------------------------------------------------------------------
  function RelExists (
    a_ObjectId                          pls_integer,
    a_ObjectDefId                       pls_integer,
    a_EndPointName                      varchar2
  ) return boolean is
    t_EndPointIds                       udt_IdList;
    t_Rels                              udt_IdList;
  begin
    t_EndPointIds := EndPointIds(a_ObjectDefId, a_EndPointName);
    for i in 1..t_EndPointIds.count loop
      t_Rels := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId, t_EndPointIds(i));
      if t_Rels.count > 0 then
        return true;
      end if;
    end loop;

    return false;
  end RelExists;

  -----------------------------------------------------------------------------
  -- RelExists()
  -----------------------------------------------------------------------------
  function RelExists(
    a_ObjectId pls_integer,
    a_EndPointName varchar2
  ) return varchar2 is
  begin
    if a_ObjectId is not null then
      if RelExists(a_ObjectId, extension.pkg_ObjectQuery.ObjectDefId(a_ObjectId),
                   a_EndPointName) then
        return 'Y';
      else
        return 'N';
      end if;
    else
      return 'N';
    end if;
  end RelExists;

  -----------------------------------------------------------------------------
  -- StoredRelExistsViaEPNames()
  --  This is a replacement for the toolbox.pkg_Formula.RelationshipExists
  --  that uses the programming end point names instead of the colon names.
  --  A list of end point names can be specified or a % for any relationships
  --  but NOTE that it only works for stored rels.
  -----------------------------------------------------------------------------
  function StoredRelExistsViaEPNames (
    a_ObjectId                          pls_integer,
    a_EndPointNameList                  varchar2
  ) return char is
    t_ObjectDefId                       pls_integer;
    t_EndPointIds                       udt_IdList;
    t_Count                             pls_integer;
  begin
    if a_ObjectId is null then
      -- this was the behavior of the original function in pkg_Formula
      return 'N';
    end if;

    if a_EndPointNameList = '%' then
      select count(*)
        into t_Count
        from api.Relationships
        where FromObjectId = a_ObjectId
          and rownum <= 1;
    else
      t_ObjectDefId := extension.pkg_ObjectQuery.ObjectDefId(a_ObjectId);
      t_EndPointIds := EndPointIds(t_ObjectDefId, a_EndPointNameList);

      for i in 1..t_EndPointIds.count loop
        select count(*)
          into t_Count
          from api.Relationships
          where FromObjectId = a_ObjectId
            and EndPointId = t_EndPointIds(i)
            and rownum <= 1;

        exit when t_Count > 0;
      end loop;
    end if;

    if t_Count > 0 then
      return 'Y';
    else
      return 'N';
    end if;
  end StoredRelExistsViaEPNames;

  -----------------------------------------------------------------------------
  -- RemoveRels()
  --   Remove all relationships for the given object def name and the list of
  -- EndPoint names specified.
  -----------------------------------------------------------------------------
  procedure RemoveRels(a_ObjectId      pls_integer,
                       a_AsOfDate      date,
                       a_ObjectDefName varchar2,
                       a_EndPointNames varchar2) is
    t_ObjectId    pls_integer;
    t_ObjectDefId pls_integer;
    t_EndPoints   udt_StringList;
    t_EPIx        pls_integer;
    t_Rels        udt_IdList;
    t_RelIx       pls_integer;
  begin

    t_EndPoints := extension.pkg_Utils.Split(a_EndPointNames, ',');

    for t_EPIx in 1 .. t_EndPoints.count loop
      select RelationshipId bulk collect
        into t_Rels
        from api.Relationships
       where FromObjectId = a_ObjectId
         and EndPointId =
             api.pkg_ConfigQuery.EndPointIdForName(a_ObjectDefName,
                                                   t_EndPoints(t_EPIx));

      for t_RelIx in 1 .. t_Rels.count loop
        api.pkg_RelationshipUpdate.Remove(t_Rels(t_RelIx));
      end loop;
    end loop;
  end RemoveRels;

  -----------------------------------------------------------------------------
  -- RemoveExistingRelsFromObject()
  --   Remove all relationships for the given object def name and the list of
  -- EndPoint names specified. Where RemoveRels is called by the 'From' object
  -- RemoveRelsToObject is called by the 'To' object.
  -----------------------------------------------------------------------------
  procedure RemoveExistingRelsFromObject(a_ObjectId      pls_integer,
                                         a_AsOfDate      date,
                                         a_FromObjectId  pls_integer,
                                         a_EndPointNames varchar2) is
    t_ObjectDefName varchar2(30);

  begin

    select od.name
      into t_ObjectDefName
      from api.objectdefs od, api.objects o
     where od.ObjectDefId = o.ObjectDefId
       and o.objectid = a_FromObjectId;

    extension.pkg_RelUtils.RemoveRels(a_FromObjectId,
                                      a_AsOfDate,
                                      t_ObjectDefName,
                                      a_EndPointNames);

  end RemoveExistingRelsFromObject;

  /********************************************************************************************
  * NewestRelatedObject() -- PRIVATE
  * Author:  Greg Hassan
  * Created: 2007.12.17
  * Purpose: NewestRelatedObject returns the ObjectId of the newest related item
  ********************************************************************************************/
  function NewestRelatedObject(a_ObjectId   pls_integer,
                               a_EndPointId pls_integer) return Pls_Integer is
    t_RelatedObjects udt_IdList;
    t_NewestObjDate  date;
    t_NewestObject   pls_integer;

    t_CurrentRelObject pls_integer;
    t_CurrentObjDate   date;

  begin
    t_RelatedObjects := extension.pkg_cxproceduralsearch.RelatedObjects(a_ObjectId,
                                                           a_EndPointId);

    for t_CurrentRelObject in 1 .. t_RelatedObjects.count loop

      select lt.createddate
        into t_CurrentObjDate
        from api.objects o, api.logicaltransactions lt
       where lt.LogicalTransactionId = o.LogicalTransactionId
         and o.objectid = t_RelatedObjects(t_CurrentRelObject);

      -- Check if current object is newer than previous
      if ((t_NewestObjDate is null) or
         (t_NewestObjDate - t_CurrentObjDate < 0)) then
        t_NewestObject  := t_CurrentRelObject;
        t_NewestObjDate := t_CurrentObjDate;
      end if;
    end loop;

    return t_RelatedObjects(t_NewestObject);
  end NewestRelatedObject;

  /********************************************************************************************
  * RemoveOlderRels() -- PUBLIC
  * Author:  Greg Hassan
  * Created: 2007.12.17
  * Purpose: RemoveOlderRels removes all but the newest relationship for a given
  *          set of EndPoints.
  ********************************************************************************************/
  procedure RemoveOlderRels(a_ObjectId           pls_integer,
                            a_AsOfDate           date,
                            a_EndPointNames      varchar2,
                            a_ShouldDeleteObject varchar2 default 'N') is
    t_EndPointIds     udt_IdList;
    t_CurrentEndPoint pls_integer;
    t_RelatedObjects   udt_IdList;
    t_CurrentRelObject pls_integer;
    t_RelationshipObjectId pls_integer;
    t_NewestRelatedObject pls_integer;
    t_ObjectDefId         pls_integer;
  begin
    t_ObjectDefId := extension.pkg_ObjectQuery.ObjectDefId(a_ObjectId);
    t_EndPointIds := EndPointIdsFromNames(t_ObjectDefId, a_EndPointNames);
    -- No matching EndPoints exist, so raise error
    if t_EndPointIds.count < 1 then
      api.pkg_Errors.SetArgValue('ObjId', a_objectId);
      api.pkg_Errors.RaiseError(-20000,
                                'RemoveOlderRels(): No matching endpoints defined for object. Expected ' ||
                                a_EndPointNames || '.');
    else
      -- For each endpoint
      for t_CurrentEndPoint in 1 .. t_EndPointIds.count loop
        -- Get list of related objects
        t_RelatedObjects := extension.pkg_cxproceduralsearch.RelatedObjects(a_ObjectId,
                                                               t_EndPointIds(t_CurrentEndPoint));
        if t_RelatedObjects.count > 1 then
          -- Determine the 'newest' related object
          t_NewestRelatedObject := NewestRelatedObject(a_ObjectId,
                                                       t_EndPointIds(t_CurrentEndPoint));
          -- Delete all relationships except the newest
          for t_CurrentRelObject in 1 .. t_RelatedObjects.count loop
            if (t_RelatedObjects(t_CurrentRelObject) != t_NewestRelatedObject) then
              -- Delete object or Disolve relationship?
              if a_ShouldDeleteObject = 'N' then
                -- Delete rel
                select r.relationshipid
                  into t_RelationshipObjectId
                  from api.relationships r
                 where r.ToObjectId = t_RelatedObjects(t_CurrentRelObject)
                   and r.EndPointId = t_EndPointIds(t_CurrentEndPoint);
                api.pkg_relationshipupdate.Remove(t_RelationshipObjectId);
              elsif a_ShouldDeleteObject = 'Y' then
                -- Delete object
                api.pkg_objectupdate.Remove(t_RelatedObjects(t_CurrentRelObject));
              else
                api.pkg_errors.RaiseError(-20000, 'Procedural error: Could not determine whether the '||
                   'object or the relationship should be deleted.');
              end if;
            end if;
          end loop;
        end if;
      end loop;
    end if;
  end RemoveOlderRels;

  /********************************************************************************************
  * EndPointIdsFromNames() -- PUBLIC
  * Author:  Greg Hassan
  * Created: 2007.12.17
  * Purpose: Returns a list of End Point Ids for a given list of End Point Names
  ********************************************************************************************/
  function EndPointIdsFromNames(t_ObjectDefId   pls_integer,
                                a_EndPointNames varchar2) return udt_IdList is
    t_EndPointIds     udt_IdList;
    t_EndPointNames   udt_StringList;
    t_CurrentEndPoint pls_integer;

  begin
    t_EndPointNames := extension.pkg_Utils.Split(a_EndPointNames, ',');

    for t_CurrentEndPoint in 1 .. t_EndPointNames.count loop

      t_EndPointIds(t_CurrentEndPoint) := api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
                                                                                trim(t_EndPointNames(t_CurrentEndPoint)));

    end loop;
    return t_EndPointIds;

  end EndPointIdsFromNames;

  -----------------------------------------------------------------------------
  -- VerifyRelPairs()
  -----------------------------------------------------------------------------
  procedure VerifyRelPairs(a_ObjectId      pls_integer,
                           a_AsOfDate      date,
                           a_JobColumnName varchar2) is
    t_JobId       pls_integer;
    t_ObjectDefId pls_integer;
    t_EndPoints   udt_StringList;
    t_Index       pls_integer;
    t_MissingRels varchar2(4000);
  begin
    -- a job or process id may be passed in
    t_JobId := extension.pkg_Common.JobIdForProcess(a_ObjectId);
    if t_JobId is null then
      t_JobId := a_ObjectId;
    end if;

    t_ObjectDefId := extension.pkg_ObjectQuery.ObjectDefId(t_JobId);
    t_EndPoints   := extension.pkg_Utils.Split(api.pkg_ColumnQuery.Value(t_JobId,
                                                                         a_JobColumnName),
                                               ',');

    -- should have even number of end points in the list
    if mod(t_EndPoints.count, 2) <> 0 then
      api.pkg_Errors.RaiseError(-20000,
                                'The client end point list does not have an even number of items.');
    end if;

    t_Index := 1;
    while t_Index < t_EndPoints.count loop
      if RelExists(t_JobId, t_ObjectDefId, t_EndPoints(t_Index)) then
        if not RelExists(t_JobId, t_ObjectDefId, t_EndPoints(t_Index + 1)) then
          extension.pkg_Utils.AddSection(t_MissingRels,
                                         t_EndPoints(t_Index + 1),
                                         ', ');
        end if;
      end if;
      -- go to the next pair
      t_Index := t_Index + 2;
    end loop;

    if length(t_MissingRels) > 0 then
      api.pkg_Errors.RaiseError(-20000,
                                t_MissingRels || ' client(s) not created.');
    end if;
  end VerifyRelPairs;

  /********************************************************************************************
  * Remove Rels from Checkbox() -- PUBLIC
  * Author: Conni Pohl
  * Created: 12/05/2006
  * Purpose: Run from a checkbox on a Job or Object, if detail is unchecked, remove
  *          Relationship(s) specified in arguments.
  ********************************************************************************************/

  procedure RemoveRelsFromCheckbox(a_ObjectId      udt_Id,
                                   a_AsOfDate      date,
                                   a_ObjectDefName varchar2,
                                   a_EndPointNames varchar2,
                                   a_Column        varchar2) is
    t_Column Char(1);

  Begin

    --determine the value of the checkbox
    t_Column := api.pkg_ColumnQuery.value(a_ObjectId, a_Column);

    if t_Column = 'N' then
      extension.pkg_RelUtils.RemoveRels(a_ObjectId,
                                        a_AsOfDate,
                                        a_ObjectDefName,
                                        a_EndPointNames);
    end if;

  end RemoveRelsFromCheckbox;

  /********************************************************************************************
  * Remove Rels On Process Complete() -- PUBLIC
  * Author: Conni Pohl
  * Created: 12/05/2006
  * Purpose: Run from a process on a Job to remove
  *          Relationship(s) on the job specified in arguments.
  ********************************************************************************************/
  procedure RemoveRelsOnProcessComplete(a_ObjectId      udt_Id,
                                        a_AsOfDate      date,
                                        a_ObjectDefName varchar2,
                                        a_EndPointNames varchar2) is
    t_JobId udt_Id;

  Begin
    --determine jobid
    t_JobId := extension.pkg_Common.JobIdForProcess(a_ObjectId);

    --Remove Relationships
    extension.pkg_RelUtils.RemoveRels(t_JobId,
                                      a_AsOfDate,
                                      a_ObjectDefName,
                                      a_EndPointNames);

  end RemoveRelsOnProcessComplete;

    /*******************************************************************
    *  CopyRelsFromLatestProcess()
    *******************************************************************/
    Procedure CopyRelsFromLatestProcess(
    a_Objectid           udt_id,
    a_asofdate           date,
    a_FromProcessType    varchar2,
    a_IncludeIncomplete  varchar2 default 'N',
    a_EndPointList       varchar2,
    a_OnlyMostRecent     varchar2 default 'N'
    ) is
      t_EndPointList        udt_StringList;
      t_JobId               udt_ID;
      t_FromProcessId       udt_ID;
      t_FromProcessTypeId   udt_ID;
      t_RelObjectId         udt_ID;
      t_EndpointId          udt_ID;
      t_ToEndpointId        udt_ID;
      t_ToProcessDefName    varchar2(60);
      t_relId               udt_ID;
    begin
      t_JobId := api.pkg_columnquery.NumericValue(a_Objectid,'JobId');
      t_FromProcessTypeId := api.pkg_configquery.ObjectDefIdForName(a_FromProcessType);
      t_EndPointList := extension.pkg_Utils.Split(a_EndPointList, ',');
      t_ToProcessDefName := api.pkg_columnquery.Value(a_Objectid,'ObjectDefName');
      begin
        select max(processid)
          into t_FromProcessId
          from api.processes
         where jobid = t_JobId
           and processtypeid = t_FromProcessTypeId
           and (datecompleted is not null or a_IncludeIncomplete = 'Y');
      exception
        when no_data_found then
          api.pkg_errors.RaiseError(-20000,'Configuration error: No processes of type '||a_FromProcessType||
                                           ' found on current job. Please contact a System Administrator.');
      end;

      for EP in 1..t_EndPointList.count loop

        t_EndpointId := api.pkg_configquery.EndPointIdForName(a_FromProcessType,t_EndPointList(EP));
        if t_EndpointId is null then
             raise_application_error(-20000, 'Invalid Endpoint Name specified for Copy Rels From Latest Process Procedure');
        end if;

        t_ToEndpointId := api.pkg_configquery.EndPointIdForName(t_ToProcessDefName,t_EndPointList(EP));
        if t_ToEndpointId is null then
          raise_application_error(-20000, 'Invalid Endpoint Name specified for Copy Rels From Latest Process Procedure');
        end if;

        if a_OnlyMostRecent = 'Y' then
          select max(r.toobjectid)
            into t_RelObjectId
            from api.relationships r
           where r.FromObjectId = t_FromProcessId
             and r.EndPointId = t_EndpointId;
          if t_RelObjectId is not null then
            t_relId := api.pkg_relationshipupdate.New(t_ToEndpointId,a_ObjectId,t_RelObjectId);
          end if;
        else
          for i in (select r.toobjectid
                      from api.relationships r
                     where r.FromObjectId = t_FromProcessId
                       and r.EndPointId = t_EndpointId) loop
            t_relId := api.pkg_relationshipupdate.New(t_ToEndpointId,a_ObjectId,i.toobjectid);
          end loop;
        end if;

      end loop;

    end;

end pkg_RelUtils;

/

