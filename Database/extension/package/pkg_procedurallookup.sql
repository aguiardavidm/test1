create or replace package pkg_ProceduralLookup is

  /*---------------------------------------------------------------------------
   * Description
   *   The package is a container for various procedural generic procedural
   * lookups.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;


  /*---------------------------------------------------------------------------
   * UsersInAccessGroup()
   *   This procedure is intended to filter a drop down list of users to only
   * include the users in a specific access group.  To use do the following:
   *
   * 1. Configure an expression dynamic detail named {EndPointName}AccessGroup
   * (where {EndPointName} is the end point name of the stored relationship that
   * this procedure is going to be used with). The expression can store one or
   * more access group names.  If multiple access groups are specified then
   * separate them with the characters "<>".
   *
   * 2. On the lookup detail in the Search By Procedure secion, specify
   * extension.pkg_ProceduralLookup.UsersInAccessGroup as the procedure to use.
   *-------------------------------------------------------------------------*/

  procedure UsersInAccessGroup (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString          varchar2,
    a_Objects                out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * ObjectsByIndex()
   *   This procedure is meant to be used as a drop down procedural lookup. It
   * will filter the drop down to include only the objects returned by a simple
   * index based seach.  To use do the following:
   *
   * 1. Configure an expression dynamic detail named {EndPointName}LookupArgs
   * (where {EndPointName} is the end point name of the stored relationship that
   * this procedure is going to be used with). The expression will store the
   * three arguments to use in api.pkg_SimpleSearch.ObjectsByIndex() in the
   * format "{ObjectDefName},{ColumnName},{Value}".
   *
   * 2. On the lookup detail in the Search By Procedure secion, specify
   * extension.pkg_ProceduralLookup.ObjectsByIndex as the procedure to use.
   *-------------------------------------------------------------------------*/
  procedure ObjectsByIndex (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out api.udt_ObjectList
  );

end pkg_ProceduralLookup;
 
/

create or replace package body pkg_ProceduralLookup is

  /*---------------------------------------------------------------------------
   * Types - PRIVATE
   *-------------------------------------------------------------------------*/
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  /*---------------------------------------------------------------------------
   * UsersInAccessGroup()
   *-------------------------------------------------------------------------*/
  procedure UsersInAccessGroup (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out api.udt_ObjectList
  ) is
    t_EndPointName           varchar2(30);
    t_Users                  udt_IdList;
    t_AccessGroups           udt_StringList;
  begin
    select ToEndPointName
    into t_EndPointName
    from api.RelationshipDefs
    where RelationshipDefId = a_RelationshipDefId
      and ToEndPoint = a_EndPoint;

    t_AccessGroups := extension.pkg_Utils.Split(api.pkg_ColumnQuery.Value(a_ObjectId,
        t_EndPointName || 'AccessGroup'), '<>');

    a_Objects := api.udt_ObjectList();
    for i in 1..t_AccessGroups.count loop
      select au.UserId
      bulk collect into t_Users
      from
        api.AccessGroupUsers au,
        api.AccessGroups a
     where a.Description = t_AccessGroups(i)
        and au.AccessGroupId = a.AccessGroupId;

      a_Objects.extend(t_Users.count);
      for i in 1..t_Users.count loop
        a_Objects(a_Objects.count-t_Users.count+i) := api.udt_Object(t_Users(i));
      end loop;
    end loop;
  end;


  /*---------------------------------------------------------------------------
   * ObjectsByIndex()
   *-------------------------------------------------------------------------*/
  procedure ObjectsByIndex (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out api.udt_ObjectList
  ) is
    t_EndPointName           varchar2(30);
    t_Objects                udt_IdList;
    t_Args                   udt_StringList;
  begin
    select ToEndPointName
    into t_EndPointName
    from api.RelationshipDefs
    where RelationshipDefId = a_RelationshipDefId
      and ToEndPoint = a_EndPoint;

    t_Args := pkg_Utils.Split(api.pkg_ColumnQuery.Value(a_ObjectId,
        t_EndPointName || 'LookupArgs'), ',');
    t_Objects := api.pkg_SimpleSearch.ObjectsByIndex(t_Args(1), t_Args(2),
        t_Args(3));

    a_Objects := api.udt_ObjectList();
    a_Objects.extend(t_Objects.count);
    for i in 1..t_Objects.count loop
      a_Objects(i) := api.udt_Object(t_Objects(i));
    end loop;
  end;

end pkg_ProceduralLookup;
/

