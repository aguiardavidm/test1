create or replace package pkg_ProcessUpdate as

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_ObjectList is api.pkg_Definition.udt_ObjectList;

  /*----------------------------------------------------------------------------
   * AssignToAccessGroup()
   *   This procedure is designed to assign each member of a specified Access
   * Group to a specified process.  It will unassign any users who are not members
   * of the access group.
   *--------------------------------------------------------------------------*/
  procedure AssignToAccessGroup (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_AccessGroup                       varchar2
  );
  
  /*---------------------------------------------------------------------------
  * New
  *  Create a new process, specifying a process type id or a process type name.
  * Functions return the process id, procedures do not.
  *--------------------------------------------------------------------------*/
  function New (
    a_JobId                             udt_Id,
    a_ProcessTypeId                     udt_Id
  ) return udt_Id;
  
  function New (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2,
    a_Description                       varchar2,
    a_ScheduledStartDate                date,
    a_ScheduledEndDate                  date,
    a_ActualStartDate                   date
  ) return udt_Id;
  
  function New (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2
  ) return udt_Id;
  
  procedure New (
    a_JobId                             udt_Id,
    a_ProcessTypeId                     udt_Id,
    a_Description                       varchar2,
    a_ScheduledStartDate                date,
    a_ScheduledEndDate                  date,
    a_ActualStartDate                   date
  );
  
  procedure New (
    a_JobId                             udt_Id,
    a_ProcessTypeId                     udt_Id
  );
  
  procedure New (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2,
    a_Description                       varchar2,
    a_ScheduledStartDate                date,
    a_ScheduledEndDate                  date,
    a_ActualStartDate                   date
  );
  
  procedure New (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2
  );
  
  procedure NewComplete (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2,
    a_Outcome                           varchar2
  );

  /*---------------------------------------------------------------------------
   * ReassignProcessType()
   *  Reassign all incomplete processes of the given type for a given job type
   * to the specified access group.
   *-------------------------------------------------------------------------*/
  procedure ReassignProcessType (
    a_JobTypeName                       varchar2,
    a_ProcessTypeName                   varchar2,
    a_AccessGroup                       varchar2
  );
  
end pkg_ProcessUpdate;

 
/

grant execute
on pkg_processupdate
to abc;

create or replace package body pkg_ProcessUpdate as

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * AssignToAccessGroup() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure AssignToAccessGroup (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_AccessGroup                       varchar2
  ) is
    t_AccessGroupId                     udt_Id;

    cursor c_AssignUsers Is
      select u.OracleLogonId
      from
        api.Accessgroupusers AGU
        join api.Users U
            on U.UserId = AGU.UserId
      where AGU.AccessGroupId = t_AccessGroupId
      minus
      select AssignedTo
      from api.ProcessAssignments
      where ProcessId = a_ProcessId;

    cursor c_UnassignUsers Is
      select AssignedTo
      from api.ProcessAssignments
      where ProcessId = a_ProcessId
      minus
      select u.OracleLogonId
      from
        api.Accessgroupusers AGU
        join api.Users U
            on U.UserId = AGU.UserId
      where AGU.AccessGroupId = t_AccessGroupId;
  begin
    select AccessGroupId
    into t_AccessGroupId
    from api.AccessGroups
    where Description = a_AccessGroup;

    for i in c_AssignUsers loop
      api.pkg_Processupdate.Assign(a_ProcessId, i.OracleLogonId);
    end loop;

    for i in c_UnAssignUsers loop
      api.pkg_Processupdate.UnAssign(a_ProcessId, i.AssignedTo);
    end loop;
  end AssignToAccessGroup;

  /*---------------------------------------------------------------------------
   * ReassignProcessType() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ReassignProcessType (
    a_JobTypeName                       varchar2,
    a_ProcessTypeName                   varchar2,
    a_AccessGroup                       varchar2
  ) is
    t_AccessGroupId                     udt_Id;

    cursor c_AssignProcesses is
      select
        p.ProcessId,
        u.OracleLogonId
      from
        api.Objects o
        join api.Processes p
            on p.ProcessId = o.ObjectId
        join api.Jobs j
            on j.JobId = p.JobId
        join api.Accessgroupusers au
            on au.AccessGroupId = t_AccessGroupId
        join api.Users u
            on u.UserId = au.UserId
      where o.ObjectDefId = api.pkg_ConfigQuery.ObjectDefIdForName(a_ProcessTypeName)
        and o.ObjectDefTypeId = 3
        and p.DateCompleted is null
        and j.JobTypeId = api.pkg_ConfigQuery.ObjectDefIdForName(a_JobTypeName)
        and not exists (
          select *
          from api.ProcessAssignments pa
          where pa.ProcessId = p.ProcessId
            and pa.UserId = au.UserId);

    cursor c_UnAssignProcesses is
      select
        p.ProcessId,
        pa.AssignedTo
      from
        api.Objects o
        join api.Processes p
            on p.ProcessId = o.ObjectId
        join api.Jobs j
            on j.JobId = p.JobId
        join api.ProcessAssignments pa
            on pa.ProcessId = p.ProcessId
      where o.ObjectDefId = api.pkg_ConfigQuery.ObjectDefIdForName(a_ProcessTypeName)
        and o.ObjectDefTypeId = 3
        and p.DateCompleted is null
        and j.JobTypeId = api.pkg_ConfigQuery.ObjectDefIdForName(a_JobTypeName)
        and not exists (
          select *
          from api.AccessGroupUsers au
          where au.UserId = pa.UserId
            and au.AccessGroupId = t_AccessGroupId);
  begin
    select AccessGroupId
    into t_AccessGroupId
    from api.AccessGroups
    where Description = a_AccessGroup;

    for p in c_AssignProcesses loop
      api.pkg_Processupdate.Assign(p.ProcessId, p.OracleLogonId);
    end loop;

    for p in c_UnAssignProcesses loop
      api.pkg_Processupdate.UnAssign(p.ProcessId, p.AssignedTo);
    end loop;
  end;
  
    
  /*---------------------------------------------------------------------------
  * New
  *  Create a new process, specifying a process type id or a process type name.
  * Functions return the process id, procedures do not.
  *--------------------------------------------------------------------------*/
  function New (
    a_JobId                             udt_Id,
    a_ProcessTypeId                     udt_Id
  ) return udt_Id is
  begin
    return api.pkg_ProcessUpdate.New(a_JobId, a_ProcessTypeId, null, null, null, null);
  end New;
  
  function New (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2,
    a_Description                       varchar2,
    a_ScheduledStartDate                date,
    a_ScheduledEndDate                  date,
    a_ActualStartDate                   date
  ) return udt_Id is
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(a_ProcessTypeName);
  begin
    return api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, a_Description, a_ScheduledStartDate, a_ScheduledEndDate, a_ActualStartDate);
  end New;
  
  function New (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2
  ) return udt_Id is
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(a_ProcessTypeName);
  begin
    return api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, null, null, null, null);
  end New;
  
  procedure New (
    a_JobId                             udt_Id,
    a_ProcessTypeId                     udt_Id,
    a_Description                       varchar2,
    a_ScheduledStartDate                date,
    a_ScheduledEndDate                  date,
    a_ActualStartDate                   date
  ) is
    t_ProcessId                         udt_Id;
  begin
    t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, a_ProcessTypeId, a_Description, a_ScheduledStartDate, a_ScheduledEndDate, a_ActualStartDate);
  end New;
  
  procedure New (
    a_JobId                             udt_Id,
    a_ProcessTypeId                     udt_Id
  ) is
    t_ProcessId                         udt_Id;
  begin
    t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, a_ProcessTypeId, null, null, null, null);
  end New;
  
  procedure New (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2,
    a_Description                       varchar2,
    a_ScheduledStartDate                date,
    a_ScheduledEndDate                  date,
    a_ActualStartDate                   date
  ) is
    t_ProcessId                         udt_Id;
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(a_ProcessTypeName);
  begin
    t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, a_Description, a_ScheduledStartDate, a_ScheduledEndDate, a_ActualStartDate);
  end New;
  
  procedure New (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2
  ) is
    t_ProcessId                         udt_Id;
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(a_ProcessTypeName);
  begin
    t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, null, null, null, null);
  end New;
  
  procedure NewComplete (
    a_JobId                             udt_Id,
    a_ProcessTypeName                   varchar2,
    a_Outcome                           varchar2
  ) is
    t_ProcessId                         udt_Id;
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(a_ProcessTypeName);
  begin
    t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, null, null, null, null);
    api.pkg_ProcessUpdate.Complete(t_ProcessId, a_Outcome);
  end NewComplete;
  

end pkg_ProcessUpdate;

/

