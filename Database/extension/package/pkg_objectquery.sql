create or replace package           pkg_ObjectQuery is

  /*---------------------------------------------------------------------------
   * Description
   *   The package contains various generic functions for querying objects.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * DistinctRelatedObjectValues()
   *   Return a distinct list of the specified column value for objects related
   * by the given end point name.
   *-------------------------------------------------------------------------*/
  function DistinctRelatedObjectValues (
    a_ObjectId				udt_Id,
    a_EndPointName			varchar2,
    a_ColumnName			varchar2,
    a_Delimiter				varchar2 default ',',
    a_UseOtherEndPoint			varchar2 default 'N'
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * ObjectDefId()
   *   Return the ObjectDefId for the given object.
   *-------------------------------------------------------------------------*/
  function ObjectDefId (
    a_ObjectId                          udt_Id
  ) return udt_Id;

  /*-------------------------------------------------------------------------
   * RelatedObjectValue()
   *  This procedure will perform a lookup across a relationship. This does
   * essentially the same thing as a regular POSSE lookup except that it
   * avoids having to unnecessarily create a lookup column.  It will also
   * work on group relationships which lookups can't handle.
   *-----------------------------------------------------------------------*/
  function RelatedObjectValue (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_ColumnName                        varchar2,
    a_Delimiter                         varchar2 default ', '
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * RelatedObjects ()
   *   Return list of related objects for the given object and end point name.
   *-------------------------------------------------------------------------*/
  function RelatedObjects (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return udt_IdList;

  /*---------------------------------------------------------------------------
   * RelatedObjectsForOtherEndPoint()
   *   Return list of related objects for the given object and the end point
   * opposite to that of the given end point name.
   *-------------------------------------------------------------------------*/
  function RelatedObjectsForOtherEndPoint (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return udt_IdList;

  /*---------------------------------------------------------------------------
   * RelatedObject()
   *   Return the related object for the given object and end point name.
   * Will raise an error if more than one object is found.
   *-------------------------------------------------------------------------*/
  function RelatedObject (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return udt_Id;

  /*---------------------------------------------------------------------------
   * RelatedObjectForOtherEndPoint()
   *   Return the related object for the given object and the end point
   * opposite to that of the given end point name.
   * Will raise an error if more than one object is found.
   *-------------------------------------------------------------------------*/
  function RelatedObjectForOtherEndPoint (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return udt_Id;

  /*---------------------------------------------------------------------------
   * CastableRelatedObjects ()
   *   Calls RelatedObjects() and returns the list as a api.udt_ObjectList.
   *-------------------------------------------------------------------------*/
  function CastableRelatedObjects (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return api.udt_ObjectList;
  
  /*---------------------------------------------------------------------------
   * CastableRelatedObjects ()
   *   Calls RelatedObjects() and returns the list as a api.udt_ObjectList.
   *   This version can be used in SQL instead of just PL/SQL.
   *-------------------------------------------------------------------------*/
  function CastableRelatedObjects (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  varchar2 
  ) return api.udt_ObjectList;  

  /*---------------------------------------------------------------------------
   * CastableObjectsByIndexRange()
   *   The same as api.pkg_SimpleSearch.CastableObjectsByIndexRange() except it
   * takes date parameters.
   *-------------------------------------------------------------------------*/
  function CastableObjectsByIndexRange (
    a_ObjectDefName                     varchar2,
    a_ColumnName                        varchar2,
    a_LowValue                          date,
    a_HighValue                         date,
    a_AsOfDate                          date default sysdate
  ) return api.udt_ObjectList;

  /*-------------------------------------------------------------------------
  * RelatedObjectValueWithLimit() -- PUBLIC
  * --MF
  *-----------------------------------------------------------------------*/
  FUNCTION RelatedObjectValueWithLimit
  (
    a_ObjectId     udt_Id,
    a_EndPointName VARCHAR2,
    a_ColumnName   VARCHAR2,
    a_MaxLength    NUMBER DEFAULT 4000,
    a_Delimiter    VARCHAR2 DEFAULT ', ',
    a_TooLong      VARCHAR2 DEFAULT '...',
    a_AsOfDate     DATE DEFAULT SYSDATE
  ) RETURN VARCHAR2 ;

  /*--------------------------------------------------------------------------
   * StoredRelExists()
   *  Return a boolean to indicate of a stored relationship exists between
   * the two objects.
   *------------------------------------------------------------------------*/
  function StoredRelExists (
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_EndPointName                      varchar2
  ) return boolean;

end pkg_ObjectQuery;

 

/

grant execute
on pkg_objectquery
to abc;

grant execute
on pkg_objectquery
to bcpdata;

grant execute
on pkg_objectquery
to conversion;

grant execute
on pkg_objectquery
to ereferral;

grant execute
on pkg_objectquery
to lms;

grant execute
on pkg_objectquery
to posseextensions;

create or replace package body           pkg_ObjectQuery is

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  type udt_StringIndexedList is table of boolean index by varchar2(4000);

  /*---------------------------------------------------------------------------
   * DistinctRelatedObjectValues() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DistinctRelatedObjectValues (
    a_ObjectId				udt_Id,
    a_EndPointName			varchar2,
    a_ColumnName			varchar2,
    a_Delimiter				varchar2 default ',',
    a_UseOtherEndPoint			varchar2 default 'N'
  ) return varchar2 is
    t_RelatedObjects			udt_IdList;
    t_Value				varchar2(8000);
    t_StringList			udt_StringIndexedList;
    t_String				varchar2(4000);
  begin
    if a_UseOtherEndPoint = 'Y' then
      t_RelatedObjects := RelatedObjectsForOtherEndPoint(a_ObjectId,
          a_EndPointName);
    else
      t_RelatedObjects := RelatedObjects(a_ObjectId, a_EndPointName);
    end if;

    if t_RelatedObjects.count = 1 then
      t_Value := api.pkg_ColumnQuery.Value(t_RelatedObjects(1), a_ColumnName);
    else
      for r in 1..t_RelatedObjects.count loop
        t_Value := api.pkg_ColumnQuery.Value(t_RelatedObjects(r), a_ColumnName);
        if t_Value is not null then
          t_StringList(t_Value) := null;
        end if;
      end loop;

      t_Value := null;
      t_String := t_StringList.first;
      while t_String is not null loop
        pkg_Utils.AddSection(t_Value, t_String, a_Delimiter);
        exit when length(t_Value) > 4000;
        t_String := t_StringList.next(t_String);
      end loop;
    end if;

    if length(t_Value) > 4000 then
      t_Value := substr(t_Value, 1, 3997) || '...';
    end if;

    return t_Value;
  end DistinctRelatedObjectValues;

  /*---------------------------------------------------------------------------
   * ObjectDefId() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function ObjectDefId (
    a_ObjectId                          udt_Id
  ) return udt_Id is
    t_ObjectDefId			udt_Id;
  begin
    select ObjectDefId
      into t_ObjectDefId
      from api.Objects
      where ObjectId = a_ObjectId;

    return t_ObjectDefId;
  exception
  when no_data_found then
    api.pkg_Errors.SetArgValue('objId', a_ObjectId);
    api.pkg_Errors.RaiseError(-20000, 'Invalid object Id: {objId}.');
  end ObjectDefId;

  /*-------------------------------------------------------------------------
   * RelatedObjectValue() -- PUBLIC
   *-----------------------------------------------------------------------*/
  function RelatedObjectValue (
    a_ObjectId               udt_Id,
    a_EndPointName           varchar2,
    a_ColumnName             varchar2,
    a_Delimiter              varchar2 default ', '
  ) return varchar2 is
    t_ObjectDefId            udt_Id;
    t_EndPointId             udt_Id;
    t_Objects                udt_IdList;
    t_ColumnValue            varchar2(4000);
    t_Value                  varchar2(4000);
  begin
    t_ObjectDefId := ObjectDefId(a_ObjectId);

    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
        a_EndPointName);
    if t_EndPointId is null then
      return null;
    end if;
    t_Objects := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId, t_EndPointId);
    
    for i in 1..t_Objects.count loop
      t_ColumnValue := api.pkg_ColumnQuery.Value(t_Objects(i), a_ColumnName);
      pkg_Utils.AddSection(t_Value, t_ColumnValue, a_Delimiter);
    end loop;

    return t_Value;
  end RelatedObjectValue;

  /*---------------------------------------------------------------------------
   * RelatedObjects() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function RelatedObjects (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return udt_IdList is
    t_EndPointId			udt_Id;
    t_RelatedObjects			udt_IdList;
  begin
    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(
        ObjectDefId(a_ObjectId), a_EndPointName);
    if t_EndPointId is null and a_FailIfNoEndPoint then
      api.pkg_Errors.RaiseError(-20000, 'Object Def '||
          api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectDefDescription') ||
          ' has no end point named '||a_EndPointName);
    elsif t_EndPointId is not null then
      t_RelatedObjects := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId,
          t_EndPointId);
    end if;

    return t_RelatedObjects;
  end RelatedObjects;

  /*---------------------------------------------------------------------------
   * RelatedObjectsForOtherEndPoint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function RelatedObjectsForOtherEndPoint (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return udt_IdList is
    t_EndPointId			udt_Id;
    t_RelatedObjects			udt_IdList;
  begin
    t_EndPointId := api.pkg_ConfigQuery.OtherEndPointIdForName(
        ObjectDefId(a_ObjectId), a_EndPointName);

    if t_EndPointId is null and a_FailIfNoEndPoint then
      api.pkg_Errors.RaiseError(-20000, 'Object Def '||
          api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectDefDescription') ||
          ' has no end point named '||a_EndPointName);
    elsif t_EndPointId is not null then
      t_RelatedObjects := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId,
          t_EndPointId);
    end if;

    return t_RelatedObjects;
  end RelatedObjectsForOtherEndPoint;

  /*---------------------------------------------------------------------------
   * RelatedObject() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function RelatedObject (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return udt_Id is
    t_RelatedObjects			udt_IdList;
  begin
    t_RelatedObjects := RelatedObjects(a_ObjectId, a_EndPointName,
        a_FailIfNoEndPoint);

    if t_RelatedObjects.count = 0 then
      return to_number(null);
    elsif t_RelatedObjects.count = 1 then
      return t_RelatedObjects(1);
    elsif t_RelatedObjects.count > 1 then
      api.pkg_Errors.SetArgValue('ObjId', a_ObjectId);
      api.pkg_Errors.SetArgValue('EPName', a_EndPointName);
      api.pkg_Errors.RaiseError(-20000, 'Multiple related objects found' ||
          ' for object id: {ObjId} and End Point Name "{EPName}".');
    end if;
  end RelatedObject;

  /*---------------------------------------------------------------------------
   * RelatedObjectForOtherEndPoint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function RelatedObjectForOtherEndPoint (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return udt_Id is
    t_RelatedObjects			udt_IdList;
  begin
    t_RelatedObjects := RelatedObjectsForOtherEndPoint(a_ObjectId,
        a_EndPointName, a_FailIfNoEndPoint);

    if t_RelatedObjects.count = 0 then
      return to_number(null);
    elsif t_RelatedObjects.count = 1 then
      return t_RelatedObjects(1);
    elsif t_RelatedObjects.count > 1 then
      api.pkg_Errors.SetArgValue('ObjId', a_ObjectId);
      api.pkg_Errors.SetArgValue('EPName', a_EndPointName);
      api.pkg_Errors.RaiseError(-20000, 'Multiple related objects found' ||
          ' for object id: {ObjId} and End Point opposite of End Point' ||
          ' Name "{EPName}".');
    end if;
  end RelatedObjectForOtherEndPoint;

  /*---------------------------------------------------------------------------
   * CastableRelatedObjects() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function CastableRelatedObjects (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  boolean default true
  ) return api.udt_ObjectList is
  begin
    return extension.pkg_Utils.ConvertToObjectList(RelatedObjects(
                 a_ObjectId, a_EndPointName, a_FailIfNoEndPoint));
  end CastableRelatedObjects;

  /*---------------------------------------------------------------------------
   * CastableRelatedObjects() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function CastableRelatedObjects (
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_FailIfNoEndPoint                  varchar2 
  ) return api.udt_ObjectList is
    t_FailIfNoEndPoint     boolean;
  begin
    if a_FailIfNoEndPoint = 'Y' then
      t_FailIfNoEndPoint := true;
    else
      t_FailIfNoEndPoint := false;
    end if;
    return extension.pkg_Utils.ConvertToObjectList(RelatedObjects(
                 a_ObjectId, a_EndPointName, t_FailIfNoEndPoint));
  end CastableRelatedObjects;

  /*---------------------------------------------------------------------------
   * CastableObjectsByIndexRange()
   *   The same as api.pkg_SimpleSearch.CastableObjectsByIndexRange() except it
   * takes date parameters.
   *-------------------------------------------------------------------------*/
  function CastableObjectsByIndexRange (
    a_ObjectDefName                     varchar2,
    a_ColumnName                        varchar2,
    a_LowValue                          date,
    a_HighValue                         date,
    a_AsOfDate                          date default sysdate
  ) return api.udt_ObjectList is
    t_ObjectList                        udt_IdList;
    t_Objects                           api.udt_ObjectList;
  begin
    t_ObjectList := api.pkg_SimpleSearch.ObjectsByIndex(a_ObjectDefName,
        a_ColumnName, to_char(a_LowValue, api.pkg_Definition.gc_DateFormat),
        to_char(a_HighValue, api.pkg_Definition.gc_DateFormat), a_AsOfDate);

    t_Objects := api.udt_ObjectList();
    t_Objects.extend(t_ObjectList.count);
    for i in 1..t_ObjectList.count loop
      t_Objects(i) := api.udt_Object(t_ObjectList(i));
    end loop;

    return t_Objects;
  end;

  /*-------------------------------------------------------------------------
  * RelatedObjectValueWithLimit() -- PUBLIC
  * --MF
  *-----------------------------------------------------------------------*/
  FUNCTION RelatedObjectValueWithLimit
  (
    a_ObjectId     udt_Id,
    a_EndPointName VARCHAR2,
    a_ColumnName   VARCHAR2,
    a_MaxLength    NUMBER DEFAULT 4000,
    a_Delimiter    VARCHAR2 DEFAULT ', ',
    a_TooLong      VARCHAR2 DEFAULT '...',
    a_AsOfDate     DATE DEFAULT SYSDATE
  ) RETURN VARCHAR2 IS
    t_ObjectDefId udt_Id;
    t_EndPointId  udt_Id;
    t_Objects     udt_IdList;
    t_ColumnValue VARCHAR2(4000);
    t_Value       VARCHAR2(4000);
    t_Proceed     BOOLEAN := TRUE;
  BEGIN
    t_ObjectDefId := extension.pkg_objectquery.ObjectDefId(a_ObjectId);

    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
                                                          a_EndPointName);
    IF t_EndPointId IS NULL THEN
      RETURN NULL;
    END IF;
    t_Objects := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId,
                                                    t_EndPointId,
                                                    a_AsOfDate);

    FOR i IN 1 .. t_Objects.COUNT LOOP

      IF t_Proceed THEN
        t_ColumnValue := api.pkg_ColumnQuery.VALUE(t_Objects(i),
                                                   a_ColumnName);

        IF length(t_Value || a_Delimiter || t_ColumnValue) >=
            (a_MaxLength - length(a_TooLong) - length(a_Delimiter)) THEN
          t_Proceed     := FALSE;
          t_ColumnValue := a_TooLong;
        END IF;

        extension.pkg_Utils.AddSection(t_Value, t_ColumnValue, a_Delimiter);
        IF t_Proceed = FALSE THEN
          EXIT;
        END IF;
      END IF;

    END LOOP;

    RETURN t_Value;
  END RelatedObjectValueWithLimit;

  /*--------------------------------------------------------------------------
   * StoredRelExists() -- PUBLIC
   *------------------------------------------------------------------------*/
  function StoredRelExists (
    a_FromObjectId                      udt_Id,
    a_ToObjectId                        udt_Id,
    a_EndPointName                      varchar2
  ) return boolean is
    t_Count                             number;
    t_ObjectDefId                       udt_Id;
  begin
    select ObjectDefId
    into t_ObjectDefId
    from api.Objects
    where ObjectId = a_FromObjectId;

    select count(*)
    into t_Count
    from api.Relationships r
    where FromObjectId = a_FromObjectId
      and EndPointId = api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
              a_EndPointName)
      and to_number(ToObjectId) = a_ToObjectId;

    return t_Count > 0;
  end;

end pkg_ObjectQuery;

/

