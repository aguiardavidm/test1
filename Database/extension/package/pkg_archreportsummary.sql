create or replace package           pkg_ArchReportSummary is
  /*---------------------------------------------------------------------------
   * Description
   *   Main Procedure for the HRM Archaeological Report Summary
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * ValidateReportSummary()
   * Checks that all required fields are filled in on the Report Summary.
   *  ReportType, PermitNumber, Report Title, ProjectName (at least one)
   *  For Report Type 'Final'
   *     Volumes, Folded Maps, Pocket Maps & Primary Author
   *  Validates the Covered Projects with the HRM Permit Number in case the
   *  the Applicant changes which HRM Permit Number he is using after Project
   *  selection.
   * Called from either the Job or Process
   *-------------------------------------------------------------------------*/
  procedure ValidateReportSummary ( a_ObjectId udt_Id,
                                    a_AsOfDate date );
  /*---------------------------------------------------------------------------
   * SubmitReportSummary()
   * Creates the Report Summary Process upon Submit with Outcome
   *  Sets the RevisionNumber
   *  Sets the Submitted Date on the Job
   *  Outcomes the existing Submit Summary Report
   *
   *-------------------------------------------------------------------------*/
  procedure SubmitReportSummary (a_ObjectId udt_Id,
                                 a_AsOfDate date );

  /*---------------------------------------------------------------------------
   * ArchSendEmail()
   * Container Procedure which manages Send Emails and some workflow stuff.
   *
   *-------------------------------------------------------------------------*/
  procedure ArchSendEmail ( a_ObjectId            udt_Id,
                            a_AsOfDate            date  );
   /*---------------------------------------------------------------------------
   * ProjectSearch()
   * Permit Project Search by ProjectName with Filter on Removed.
   *
   *-------------------------------------------------------------------------*/
  procedure ProjectSearch (a_PermitNumber   varchar2,
                           a_ProjectName    varchar2,
                           a_Objects        out api.udt_objectList);

 /*---------------------------------------------------------------------------
 * InitialAssignments()
 * Must unAssign Processes when there is not an attached Interim Report
 *
 *-------------------------------------------------------------------------*/
  procedure InitialAssignments (a_ObjectId   udt_Id,
                                a_AsOfDate   date);

 /*---------------------------------------------------------------------------
 * Assign()
 * Assigns users based on the AccessGroup pass in
 *
 *-------------------------------------------------------------------------*/
  procedure Assign ( a_ObjectId    udt_Id,
                     a_AsOfDate    date,
                     a_AccessGroup varchar2);

  /*---------------------------------------------------------------------------
 * AssignTo()
 *-------------------------------------------------------------------------*/
  procedure AssignTo ( a_ObjectId    udt_Id,
                       a_AsOfDate    Date,
                       a_User        varchar2);

 /*---------------------------------------------------------------------------
 * CancelJob()
 * Cancels the Report Summary Job:
 * If all clearances related to it are Cancelled.
 * Overload of pkg_Common.CancelJob.
 *-------------------------------------------------------------------------*/
  Procedure CancelJob(a_ObjectId  udt_Id,
                      a_AsOfDate  date);
/*---------------------------------------------------------------------------
 * SendPublishedDocs()
 *-------------------------------------------------------------------------*/
  Procedure SendPublishedDocs(a_ObjectId  udt_Id,
                               a_AsOfDate  date);
/*---------------------------------------------------------------------------
 * GetValidPermits()
 *-------------------------------------------------------------------------*/
  Procedure GetValidPermits( a_ObjectId               udt_Id,
                              a_RelationshipDefId      udt_Id,
                              a_EndPoint               varchar2,
                              a_SearchString           varchar2,
                              a_Objects                out api.udt_ObjectList );


/*---------------------------------------------------------------------------
 * GetProjectNumbers()
 *-------------------------------------------------------------------------*/
 /* Procedure GetProjectNumbers( a_ObjectId udt_Id,
                               a_RelationshipDefId      udt_Id,
                               a_EndPoint               varchar2,
                               a_SearchString           varchar2,
                               a_Objects                out api.udt_ObjectList );        */

 /*--------------------------------------------------------------------------
  * HRMReportSearch () - HRM Archaeological Report Summary Search
  *------------------------------------------------------------------------*/
  procedure HRMReportSearch(
    a_ApplicationNumber        udt_Id,
    a_FromSubmittedDate        Date,
    a_ToSubmittedDate          Date,
    a_FromAcceptedDate         Date,
    a_ToAcceptedDate           Date,
    a_OutstandingOnly          varchar2,
    a_PermitHolderFirstName    varchar2,
    a_PermitHolderLastName     varchar2,
    a_PermitHolderAffiliation  varchar2,
    a_PermitNumber             varchar2,
    a_StatusName               varchar2,
    a_Objects            out nocopy api.udt_ObjectList);

end pkg_ArchReportSummary;

 
/

grant execute
on pkg_archreportsummary
to posseextensions;

