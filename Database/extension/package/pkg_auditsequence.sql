create or replace package pkg_AuditSequence as

   subtype udt_Id is api.pkg_Definition.udt_Id;

  /*-------------------------------------------------------------------------------
   * Fucntion:   GenerateSequence
   *    Author:  Dave den Otter
   *    Created: 04/04/2009
   *    Purpose: Generates a new sequence value for a_SequenceName.  If the
   *             sequences is audited, a_ReferenceObjectId must be passed in.
   *             If a_Mask is supplied, it will be applied to the value that is
   *             returned.
   *    Returns: Sequence Value (with a_Mask applied if not null)
   *-----------------------------------------------------------------------------*/
  function GenerateSequence (
    a_SequenceName              varchar2,
    a_ReferenceObjectId         udt_Id    default null,
    a_Mask                      varchar2  default null
  ) return varchar2;


  /*-------------------------------------------------------------------------------
   * Procedure:  SetColumnWithSequence
   *    Author:  Ron Hagens
   *    Created: Jun/02/2009
   *    Purpose: Uses GenerateSequence to generate the next sequence and
   *             sets the value to the specified column.
   *             Generates a new sequence value for a_SequenceName.
   *-----------------------------------------------------------------------------*/
  Procedure SetColumnWithSequence(
    a_ObjectId in udt_Id,
    a_AsOfDate in date,
    a_SequenceName in varchar2,
    a_ColumnName in varchar2,
    a_Mask in varchar2 );

end pkg_AuditSequence;

 
/

create or replace package body pkg_AuditSequence as

  /*-------------------------------------------------------------------------------
   * Function:   GenerateSequence
   *    Author:  Dave den Otter
   *    Created: 04/04/2009
   *    Purpose: Generates a new sequence value for a_SequenceName.  If the
   *             sequences is audited, a_ReferenceObjectId must be passed in.
   *             If a_Mask is supplied, it will be applied to the value that is
   *             returned.
   *    Returns: Sequence Value (with a_Mask applied if not null)
   *-----------------------------------------------------------------------------*/
  function GenerateSequence (
    a_SequenceName              varchar2,
    a_ReferenceObjectId         udt_Id,
    a_ReferenceObjectDefId      udt_Id,
    a_LogicalTransactionId      udt_Id,
    a_CreatedBy                 udt_Id,
    a_RealUser                  varchar,
    a_Mask                      varchar2
  ) return varchar2 is

    PRAGMA AUTONOMOUS_TRANSACTION;

    t_CreatedDate               date;
    t_MaskedValue               varchar2(60);
    t_SequenceId                udt_Id;

  begin
    begin
      select SequenceId
        into t_SequenceId
        from api.Sequences
       where Description = a_SequenceName;
    exception when no_data_found then
      rollback;
      api.pkg_Errors.RaiseError(-20002, 'Invalid Sequence Name: ' || a_SequenceName);
    end;

    /* Get the object metadata */
    if a_ReferenceObjectId is not null then

      t_MaskedValue := api.pkg_Utils.GenerateExternalFileNum(a_Mask, t_SequenceId);
      t_CreatedDate := sysdate;

      /* Audit the sequence */
      insert into extension.SequenceAudit_t (
        SequenceAuditId,
        SequenceId,
        MaskedValue,
        ReferenceObjectDefId,
        ReferenceObjectId,
        LogicalTransactionId,
        CreatedDate,
        CreatedBy,
        RealUser
      ) values (
        extension.SequenceAudit_s.nextval,
        t_SequenceId,
        t_MaskedValue,
        a_ReferenceObjectDefId,
        a_ReferenceObjectId,
        a_LogicalTransactionId,
        t_CreatedDate,
        a_CreatedBy,
        a_RealUser
      );

    else
      rollback;
      api.pkg_errors.RaiseError(-20002, 'ReferencedValue cannot be null for audited sequence ' || a_SequenceName);
    end if;

    commit;
    return t_MaskedValue;

  end GenerateSequence;


  /*-------------------------------------------------------------------------------
   * Function:  GenerateSequence (public)
   *-----------------------------------------------------------------------------*/
  function GenerateSequence (
    a_SequenceName              varchar2,
    a_ReferenceObjectId         udt_Id,
    a_Mask                      varchar2
  ) return varchar2 is

    t_ReferenceObjectDefId      udt_Id;
    t_LogicalTransactionId      udt_Id;
    t_CreatedBy                 udt_Id;
    t_RealUser                  varchar2(30);

  begin

    /* Get the metadata that won't be visible inside the autonomous transaction */
    if a_ReferenceObjectId is not null then

      begin

        select a.ObjectDefId
          into t_ReferenceObjectDefId
          from api.Objects a
          where a.ObjectId = a_ReferenceObjectId;

      exception
      when no_data_found then
        api.pkg_errors.RaiseError(-20002, 'Cannot determine ObjectDefId for ObjectId ' || to_char(a_ReferenceObjectId));
      end;

      /* Get user information */
      begin

        t_LogicalTransactionId := api.pkg_LogicalTransactionUpdate.CurrentTransaction;
        select a.CreatedBy,
               a.RealUser
          into t_CreatedBy,
               t_RealUser
          from api.LogicalTransactions a
          where a.LogicalTransactionId = t_LogicalTransactionId;

      exception
      when no_data_found then
        api.pkg_errors.RaiseError(-20002, 'Cannot determine user information for audited sequence ' || a_SequenceName);
      end;

    end if;

    return GenerateSequence(a_SequenceName, a_ReferenceObjectId, t_ReferenceObjectDefId, t_LogicalTransactionId, t_CreatedBy, t_RealUser, a_Mask);

  end GenerateSequence;

  /*-------------------------------------------------------------------------------
   * Procedure:  SetColumnWithSequence (public)
   *-----------------------------------------------------------------------------*/
  Procedure SetColumnWithSequence(
    a_ObjectId in udt_Id,
    a_AsOfDate in date,
    a_SequenceName in varchar2,
    a_ColumnName in varchar2,
    a_Mask in varchar2 ) is

    t_CurrentValue    varchar2(40);
    t_NextSeq         varchar2(40);

  begin

    t_CurrentValue := api.pkg_columnquery.Value( a_ObjectId, a_ColumnName, a_AsOfDate );

    if t_CurrentValue is null then

      t_NextSeq := extension.pkg_auditsequence.GenerateSequence(a_SequenceName, a_ObjectId, a_Mask);

      if a_ColumnName = 'ExternalFileNum' then
        api.pkg_jobupdate.SetExternalFileNum( a_ObjectId, t_NextSeq );
      else
        api.pkg_columnupdate.SetValue(a_ObjectId, a_ColumnName, t_NextSeq, a_AsOfDate);
      end if;

    end if;

  end SetColumnWithSequence;

end pkg_AuditSequence;

/

