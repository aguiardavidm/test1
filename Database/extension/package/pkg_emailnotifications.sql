create or replace PACKAGE pkg_EmailNotifications is

  -- Public type declarations
  subtype udt_id is api.pkg_definition.udt_Id;
  subtype udt_idlist is api.pkg_definition.udt_IdList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_ptdef is api.pkg_definition.udt_ProcessType;
  subtype udt_EndPoint is api.pkg_definition.udt_EndPoint;
  subtype udt_ObjectList is api.udt_ObjectList;

  /*--------------------------------------------------------------------------
   * RelateExtUsersToProcessEmail()
   * This procedure will run when the ShowOnUserPrefsExternal boolean is
   * changed.  If the value of that boolean = 'Y' then the procedure will
   * relate the current o_ProcessEmail object to all of the external users
   *------------------------------------------------------------------------*/
   procedure RelateExtUsersToProcessEmail(a_ObjectId udt_Id,
                                          a_AsOfDate date);

  /*-------------------------------------------------------------------------
   * SendExternalNotification() - Public
   * This procedure will send the External Notification emails to the
   * contractors that are related to the job
   *------------------------------------------------------------------------*/
   procedure SendExternalNotification(a_ObjectId udt_Id,
                                      a_AsOfDate date);

  /*-------------------------------------------------------------------------
   * ExternalSelectDeselect() - Public
   * This procedure is used to select and deselect all of the External
   * checkboxes on the relationships
   *------------------------------------------------------------------------*/
   procedure ExternalSelectDeselect(a_ObjectId udt_Id,
                                    a_AsOfDate date);

  /*--------------------------------------------------------------------------
   * RelateIntUsersToProcessEmail()
   * This procedure will run when the ShowOnUserPrefsAssignments boolean is
   * changed.  If the value of that boolean = 'Y' then the procedure will
   * relate the current o_ProcessEmail object to all of the internal users
   *------------------------------------------------------------------------*/
   procedure RelateIntUsersToProcessEmail(a_ObjectId udt_Id,
                                          a_AsOfDate date);

  /*-------------------------------------------------------------------------
   * SendAssignmentNotification() - Public
   * This procedure will send the Assignment Notification email to the
   * assigned-to user when their prefs say to send it.
   *------------------------------------------------------------------------*/
   procedure SendAssignmentNotification(a_ObjectId udt_Id,
                                        a_AsOfDate date);

  /*-------------------------------------------------------------------------
   * SendAssignmentNotification() - Public
   * This procedure will send the Assignment Notification email to the
   * assigned-to user when their prefs say to send it.
   *------------------------------------------------------------------------*/
   procedure SendAssignmentNotification32(a_ObjectId udt_Id,
                                        a_AsOfDate date);

  /*-------------------------------------------------------------------------
   * AssignmentSelectDeselect() - Public
   * This procedure is used to select and deselect all of the
   * checkboxes on the relationships
   *------------------------------------------------------------------------*/
   procedure AssignmentSelectDeselect(a_ObjectId udt_Id,
                                      a_AsOfDate date);

  /*-------------------------------------------------------------------------
   * RelateNewUserToProcessEmail() - Public
   * This procedure is used on constructor of the user object to relate all
   * of the valid Process Email objects to the user
   *------------------------------------------------------------------------*/
   procedure RelateNewUserToProcessEmail(a_ObjectId udt_Id,
                                         a_AsOfDate date);

end pkg_EmailNotifications;

 
/
create or replace PACKAGE BODY pkg_EmailNotifications is

  /*-------------------------------------------------------------------------
   * RelateExtUsersToProcessEmail() - Public
   * This procedure is used to relate the External Users to all of the Process
   * Email Objects
   *------------------------------------------------------------------------*/
   procedure RelateExtUsersToProcessEmail(a_ObjectId udt_Id,
                                          a_AsOfDate date) is

     t_UserIds       udt_ObjectList;
     t_NewRelId      udt_Id;
     t_EndPointId    udt_Id := api.pkg_configquery.endpointidforname('o_ProcessEmail', 'Users');

   begin

--api.pkg_errors.raiseerror(-20000, 'TEST');

   --get all of the External users
     select api.udt_Object(u.UserIds)
       bulk collect into t_UserIds
       from (select u.userid UserIds
               from api.users u
             minus
             select x.objectId
               from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('u_Users', 'UserType', 'Internal') as api.udt_ObjectList)) x
            ) u;

   --if the checkbox = 'Y' then we will relate this object to all of the external users
     if api.pkg_columnquery.value(a_ObjectId, 'ShowOnUserPrefsExternal') = 'Y'
       then
       --loop through the users and relate them to this object
         for c in 1.. t_UserIds.count loop
         --create the relationship
           t_NewRelId := api.pkg_relationshipupdate.new(t_EndPointId, a_ObjectId, t_UserIds(c).ObjectId);
         --do not default to send the emails on Completion
           --api.pkg_columnupdate.setvalue(t_NewRelId, 'EmailExternalNotification', 'Y');
         end loop; --c

     else --delete the relationships where the users are the ones that are External
       for d in (select r.relationshipid
                   from query.r_ABCprocessemailuser r
                   join table(t_UserIds) c on c.objectid = r.UserId
                  where r.ProcessEmailObjectId = a_ObjectId)loop
         api.pkg_relationshipupdate.Remove(d.relationshipid);
       end loop; --d
     end if; --if checkbox = 'Y'
   end;


  /*-------------------------------------------------------------------------
   * SendExternalNotification() - Public
   * needs grant execute on extension.pkg_sendmail to bcpdata;
   * This procedure is used to send the external notifications.  It is called
   * on every Status Change activity in the database.  (at least when the config
   * is kept up-to-date)
   *------------------------------------------------------------------------*/
   procedure SendExternalNotification(a_ObjectId udt_Id,
                                      a_AsOfDate date) is

     t_ProcessTypeId                  udt_Id;
     t_UserId                         udt_Id;
     t_JobId                          udt_Id;
     t_ContractorDefId                udt_Id := api.pkg_configquery.objectdefidforname('o_Contractor');
     t_UserDefId                      udt_Id := api.pkg_configquery.objectdefidforname('u_Users');
     t_UserProcessEmailEPId           udt_Id := api.pkg_configquery.endpointidforname('u_Users', 'ProcessEmail');
     t_ContractorUserEPId             udt_Id := api.pkg_configquery.endpointidforname('o_Contractor', 'Users');
     t_EmailNotifications             varchar2(1);
     t_Outcome                        varchar2(500);
     t_EmailAddress                   varchar2(500);
     t_FromEmailAddress               varchar2(500);
     t_SendEmail                      varchar2(1) := 'N';
     t_EmailExternalNotification      varchar2(1) := 'N';
     t_EmailSubject                   varchar2(4000);
     t_EmailPreamble                  varchar2(1000);
     t_EmailLinkDestination           varchar2(4000);
     t_EmailLink                      varchar2(250);
     t_FinalEmailLink                 varchar2(250);
     t_EmailBody                      varchar2(2750);
     t_EmailMessage                   varchar2(4000);
     t_AttachAllDocs                  varchar2(1);
     t_Active                         varchar2(1);

   begin
     select jobid, processtypeid
       into t_JobId, t_ProcessTypeId
       from api.processes p
      where p.processid = a_ObjectId;


   --this needs to be coded generically enough that it can find the contractors/applicants from other jobs too
     for c in (
               /*USERS RELATED TO JOB*/
               select r.toobjectid UserId, api.pkg_columnquery.value(r.ToObjectId, 'EmailAddress') EmailAddress
                 from api.processes p
                 join api.relationships r on r.FromObjectId = p.jobid

               --the following join would get all users that have a stored rel to the job
                 join api.endpoints e on e.endpointid = r.endpointid
                  and e.ToObjectDefId = t_UserDefId

               --this would then join from the user to the external object thingy based on the processtype we originated from
                 join api.relationships r3 on r3.fromobjectid = r.toobjectid
                  and r3.endpointid = t_UserProcessEmailEPId
                where p.processid = a_ObjectId

               union --all

               /*CONTRACTORS RELATED TO JOB*/
               select r2.toobjectid UserId, api.pkg_columnquery.value(r2.ToObjectId, 'EmailAddress') EmailAddress
                 from api.processes p
                 join api.relationships r on r.FromObjectId = p.jobid

               --the following join would get all contractors that have a stored rel to the job
                 join api.endpoints e on e.endpointid = r.endpointid
                  and e.toobjectdefid = t_ContractorDefId

               --this then joins from those entities, (if a contractor) to the user object
                 join api.relationships r2 on r2.fromobjectid = r.toobjectid
                  and r2.endpointid = t_ContractorUserEPId

               --this would then join from the user to the external object thingy based on the processtype we originated from
                 join api.relationships r3 on r3.fromobjectid = r2.toobjectid
                  and r3.endpointid = t_UserProcessEmailEPId
                where p.processid = a_ObjectId)loop

       t_EmailAddress         := c.EmailAddress;
       t_UserId               := c.UserId;
       t_Outcome              := api.pkg_columnquery.value(a_ObjectId, 'Outcome');
       t_EmailNotifications   := api.pkg_columnquery.value(t_UserId, 'ExternalSelectDeselectAll');
       t_Active               := api.pkg_columnquery.value(t_UserId, 'Active');

       begin
       --select the email information from the Process Email object
         select --+ CARDINALITY(cs 1)
                r.EmailExternalNotification,
                api.pkg_columnquery.value(x.ObjectId, 'ShowOnUserPrefsExternal') SendEmail,
                api.pkg_columnquery.value(x.ObjectId, 'ExternalEmailSubject') Subject,
                api.pkg_columnquery.value(x.ObjectId, 'ExternalEmailPreamble') Preamble,
                api.pkg_columnquery.value(x.ObjectId, 'ExternalLinkDestination') LinkDestination,
                api.pkg_columnquery.value(x.ObjectId, 'ExternalEmailLink') Link,
                api.pkg_columnquery.value(x.ObjectId, 'ExternalEmailBody') EmailBody,
                api.pkg_columnquery.value(x.ObjectId, 'FromEmailAddress'),
                api.pkg_columnquery.value(x.ObjectId, 'IncludeProcessDocsAsAttachment') AttachAllDocs
           into t_EmailExternalNotification,
                t_SendEmail,
                t_EmailSubject,
                t_EmailPreamble,
                t_EmailLinkDestination,
                t_EmailLink,
                t_EmailBody,
                t_FromEmailAddress,
                t_AttachAllDocs
           from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ProcessEmail', 'ProcessTypeId', t_ProcessTypeId) as api.udt_ObjectList)) x
           join query.r_ABCprocessemailuser r on r.processemailobjectid = x.objectid and r.userid = t_UserId;

       --if the settings allow us to and we have an email address... send the email
         if t_EmailExternalNotification = 'Y'
        and t_SendEmail = 'Y'
        and t_EmailAddress is not null
        and t_EmailNotifications = 'Y'
        and t_Active = 'Y'
           then
           --Build the real link
             if t_EmailLinkDestination = 'Job'
               then
                 t_FinalEmailLink := t_EmailLink || t_JobId;
             elsif t_EmailLinkDestination = 'Process'
               then
                 t_FinalEmailLink := t_EmailLink || a_ObjectId;
             elsif t_EmailLinkDestination = 'Dashboard'
               then
                 t_FinalEmailLink := t_EmailLink || t_UserId;
             end if;

           --Build the Email Message
             t_EmailMessage := t_EmailPreamble || chr(13) || chr(10) || t_FinalEmailLink || chr(13) || chr(10) || t_EmailBody;

           --Parse for the Outcome in the Message and the Subject
             t_EmailSubject := extension.pkg_utils.ParseTextValue(a_ObjectId, t_EmailSubject);
             t_EmailMessage := extension.pkg_utils.ParseTextValue(a_ObjectId, t_EmailMessage);

           --Send the Email
             extension.pkg_SendMail.SendEmail(a_ObjectId,
                                              t_FromEmailAddress,
                                              t_EmailAddress,
                                              null,
                                              null,
                                              t_EmailSubject,
                                              t_EmailMessage,
                                              null,
                                              null,
                                              'N',
                                              t_AttachAllDocs);

         end if; --t_EmailExternalNotification
       exception when no_data_found --if no data is found then the related contractor/user doesn't have a relationship to the process
                                     --email object, therefore we don't want to send an email
         then
           null;
       end;
     end loop;
   end;


  /*-------------------------------------------------------------------------
   * ExternalSelectDeselect() - Public
   * This procedure is used to select or deselect all checkboxes on the external
   * user prefs for the Process Email relationship from the user
   *------------------------------------------------------------------------*/
   procedure ExternalSelectDeselect(a_ObjectId udt_Id,
                                    a_AsOfDate date) is

   begin
   --loop through the relationships for this user
     for c in (select RelationshipId
                 from query.r_ABCprocessemailuser
                where userid = a_ObjectId)loop
     --set the detail on the relationship equal to what was set to on the user
       api.pkg_columnupdate.setvalue(c.relationshipid, 'EmailExternalNotification', api.pkg_columnquery.value(a_ObjectId, 'ExternalSelectDeselectAll'));
     end loop;
   end;


  /*-------------------------------------------------------------------------
   * RelateIntUsersToProcessEmail() - Public
   *------------------------------------------------------------------------*/
   procedure RelateIntUsersToProcessEmail(a_ObjectId udt_Id,
                                          a_AsOfDate date) is

     t_UserIds       api.udt_ObjectList;
     t_NewRelId      udt_Id;
     t_EndPointId    udt_Id := api.pkg_configquery.endpointidforname('o_ProcessEmail', 'Users');

   begin
     t_UserIds := api.udt_ObjectList();

   --get all of the Internal users
     select api.udt_Object(x.objectId)
       bulk collect into t_UserIds
       from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('u_Users', 'UserType', 'Internal') as api.udt_ObjectList)) x;

   --if the checkbox = 'Y' then we will relate this object to all of the internal users
     if api.pkg_columnquery.value(a_ObjectId, 'ShowOnUserPrefsAssignments') = 'Y'
       then

       --loop through the users and relate them to this object
         for c in 1.. t_UserIds.count loop
         --create the relationship
           t_NewRelId := api.pkg_relationshipupdate.new(t_EndPointId, a_ObjectId, t_UserIds(c).ObjectId);
         --do not default to send the emails on Assignment
           --api.pkg_columnupdate.setvalue(t_NewRelId, 'EmailAssignments', 'Y');
         end loop; --c

     else --delete the relationships where the users are Internal
       for d in (select r.relationshipid
                   from query.r_ABCprocessemailuser r
                   join table(t_UserIds) c on c.objectid = r.UserId
                  where r.processemailObjectId = a_ObjectId)loop
         api.pkg_relationshipupdate.Remove(d.relationshipid);
       end loop; --d
     end if; --if checkbox = 'Y'

   end;


  /*-------------------------------------------------------------------------
   * SendAssignmentNotification() - Public
   * needs grant execute on extension.pkg_sendmail to bcpdata;
   *------------------------------------------------------------------------*/
   procedure SendAssignmentNotification(a_ObjectId udt_Id,
                                        a_AsOfDate date) is

     t_ProcessTypeId          udt_Id;
     t_AssignedUserId         udt_Id;
     t_JobId                  udt_Id;
     t_Outcome                varchar2(500);
     t_EmailAddress           varchar2(500);
     t_FromEmailAddress       varchar2(500);
     t_SendEmail              varchar2(1) := 'N';
     t_EmailAssignments       varchar2(1) := 'N';
     t_EmailSubject           varchar2(4000);
     t_EmailPreamble          varchar2(1000);
     t_EmailLinkDestination   varchar2(4000);
     t_EmailLink              varchar2(250);
     t_FinalEmailLink         varchar2(250);
     t_EmailBody              varchar2(2750);
     t_EmailMessage           varchar2(4000);

   begin

   --get the users that have not yet been added to the audit log
     for c in (select pa.UserId
                 from api.processassignments pa
                where pa.processid = a_ObjectId
               minus
               select p.UserId
                 from extension.ProcessAssignmentAudit_t p
                where processid = a_ObjectId)loop

     --first of all, add this user to the audit log
       insert into extension.Processassignmentaudit_t (LogicalTransactionId,
                                                     ProcessId,
                                                     UserId,
                                                     AssignedTo)
         select LogicalTransactionId,
                ProcessId,
                UserId,
                AssignedTo
           from api.processassignments
          where processid = a_ObjectId
            and UserId = c.UserId;

     --if the user is an Internal user
       if api.pkg_columnquery.value(c.UserId, 'UserType') = 'Internal'
         then

         --get the other data...
           t_AssignedUserId  := c.UserId;
           t_EmailAddress    := api.pkg_columnquery.value(c.userid, 'EmailAddress');
           select processtypeid, jobid, Outcome
             into t_ProcessTypeId, t_JobId, t_Outcome
             from api.processes p
            where p.processid = a_ObjectId;

         --if the process is being completed in this transaction, don't send an email
           if t_Outcome is not null
             then null;
           else
             begin
             --select the email information from the Process Email object
               select --+ CARDINALITY(cs 1)
                      r.EmailAssignments,
                      api.pkg_columnquery.value(x.ObjectId, 'ShowOnUserPrefsAssignments') SendEmail,
                      api.pkg_columnquery.value(x.ObjectId, 'AssignmentEmailSubject') Subject,
                      api.pkg_columnquery.value(x.ObjectId, 'AssignmentEmailPreamble') Preamble,
                      api.pkg_columnquery.value(x.ObjectId, 'InternalLinkDestination') LinkDestination,
                      api.pkg_columnquery.value(x.ObjectId, 'AssignmentEmailLink') Link,
                      api.pkg_columnquery.value(x.ObjectId, 'AssignmentEmailBody') EmailBody,
                      api.pkg_columnquery.value(x.ObjectId, 'FromEmailAddress') FromEmailAddress
                 into t_EmailAssignments,
                      t_SendEmail,
                      t_EmailSubject,
                      t_EmailPreamble,
                      t_EmailLinkDestination,
                      t_EmailLink,
                      t_EmailBody,
                      t_FromEmailAddress
                 from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ProcessEmail', 'ProcessTypeId', t_ProcessTypeId) as api.udt_ObjectList)) x
                 join query.r_ABCprocessemailuser r on r.processemailobjectid = x.objectid and r.userid = t_AssignedUserId;

             --if the settings allow us to and if we have an email address... send the email
               if t_EmailAssignments = 'Y' and t_SendEmail = 'Y' and t_EmailAddress is not null
                 then
                 --Build the real link
                   if t_EmailLinkDestination = 'Job'
                     then
                       t_FinalEmailLink := t_EmailLink || t_JobId;
                   elsif t_EmailLinkDestination = 'Process'
                     then
                       t_FinalEmailLink := t_EmailLink || a_ObjectId;
                   elsif t_EmailLinkDestination = 'To-Do List'
                     then
                       t_FinalEmailLink := t_EmailLink;
                   end if;

                 --Build the Email Message
                   t_EmailMessage := t_EmailPreamble || chr(13) || chr(10) || t_FinalEmailLink ||chr(13) || chr(10) || t_EmailBody;

                 --Send the Email
                   extension.pkg_sendmail_old.SendEmail(a_ObjectId,
                                                    t_FromEmailAddress,
                                                    t_EmailAddress,
                                                    null,
                                                    null,
                                                    t_EmailSubject,
                                                    t_EmailMessage,
                                                    null,
                                                    null,
                                                    'N');

               end if; --t_EmailAssignments
             exception when no_data_found --if no data is found then the assigned user doesn't have a relationship to the process email object
             --therefore we don't want to send an email
               then null;
             end;
           end if; --t_Outcome
         end if; --if Internal or External
     end loop; --c

   --get the users that removed from the assignment and remove them from the Audit Table
     for d in (select p.UserId
                 from extension.ProcessAssignmentAudit_t p
                where p.processid = a_ObjectId
               minus
               select pa.userid
                 from api.processassignments pa
                where pa.processid = a_ObjectId)loop
       delete from extension.processassignmentaudit_t where userid = d.UserId and ProcessId = a_ObjectId;
     end loop; --d
   end;

  /*-------------------------------------------------------------------------
   * SendAssignmentNotification32() - Public
   *------------------------------------------------------------------------*/
   procedure SendAssignmentNotification32(a_ObjectId udt_Id,
                                        a_AsOfDate date) is

     t_ProcessTypeId          udt_Id;
     t_AssignedUserId         udt_Id;
     t_JobId                  udt_Id;
     t_ProcEmailId            udt_Id;
     t_Outcome                varchar2(500);
     t_EmailAddress           varchar2(500);
     t_FromEmailAddress       varchar2(500);
     t_SendEmail              varchar2(1) := 'N';
     t_EmailAssignments       varchar2(1) := 'N';
     t_EmailSubject           varchar2(4000);
     t_EmailPreamble          varchar2(4000);
     t_EmailLinkDestination   varchar2(4000);
     t_LinkDescription        varchar2(4000);
     t_FirstName              varchar2(4000);
     t_LastName               varchar2(4000);
     t_EmailLink              varchar2(250);
     t_FinalEmailLink         varchar2(250);
     t_EmailBody              varchar2(4000);
     t_EmailMessage           varchar2(32767);
     t_IsProd                 varchar2(1);

   begin

   --get the users that have not yet been added to the audit log
     for c in (select pa.UserId
                 from api.processassignments pa
                where pa.processid = a_ObjectId
               minus
               select p.UserId
                 from extension.ProcessAssignmentAudit_t p
                where processid = a_ObjectId)loop

     --first of all, add this user to the audit log
       insert into extension.Processassignmentaudit_t (LogicalTransactionId,
                                                     ProcessId,
                                                     UserId,
                                                     AssignedTo)
         select LogicalTransactionId,
                ProcessId,
                UserId,
                AssignedTo
           from api.processassignments
          where processid = a_ObjectId
            and UserId = c.UserId;

     --if the user is an Internal user
       if api.pkg_columnquery.value(c.UserId, 'UserType') = 'Internal'
         then

         --get the other data...
           t_AssignedUserId := c.UserId;
           t_EmailAddress := api.pkg_columnquery.value(c.userid, 'EmailAddress');
           t_FirstName := api.pkg_columnquery.value(c.userid, 'FirstName');
           t_LastName := api.pkg_columnquery.value(c.userid, 'LastName');
           select processtypeid, jobid, Outcome
             into t_ProcessTypeId, t_JobId, t_Outcome
             from api.processes p
            where p.processid = a_ObjectId;

         --if the process is being completed in this transaction, don't send an email
           if t_Outcome is not null
             then null;
           else
             begin
             --select the email information from the Process Email object
               select --+ CARDINALITY(cs 1)
                      r.EmailAssignments,
                      api.pkg_columnquery.value(x.ObjectId, 'ObjectId') ProcEmailId,
                      api.pkg_columnquery.value(x.ObjectId, 'ShowOnUserPrefsAssignments') SendEmail,
                      api.pkg_columnquery.value(x.ObjectId, 'AssignmentEmailSubject') Subject,
                      api.pkg_columnquery.value(x.ObjectId, 'AssignmentEmailPreamble') Preamble,
                      api.pkg_columnquery.value(x.ObjectId, 'InternalLinkDestination') LinkDestination,
                      api.pkg_columnquery.value(x.ObjectId, 'AssignmentEmailLink') Link,
                      api.pkg_columnquery.value(x.ObjectId, 'AssignmentEmailBody') EmailBody,
                      api.pkg_columnquery.value(x.ObjectId, 'FromEmailAddress') FromEmailAddress,
                      api.pkg_columnquery.value(x.ObjectId, 'InternalDescription') LinkDescription
                 into t_EmailAssignments,
                      t_ProcEmailId,
                      t_SendEmail,
                      t_EmailSubject,
                      t_EmailPreamble,
                      t_EmailLinkDestination,
                      t_EmailLink,
                      t_EmailBody,
                      t_FromEmailAddress,
                      t_LinkDescription
                 from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ProcessEmail', 'ProcessTypeId', t_ProcessTypeId) as api.udt_ObjectList)) x
                 join query.r_ABCprocessemailuser r on r.processemailobjectid = x.objectid and r.userid = t_AssignedUserId;

             --if the settings allow us to and if we have an email address... send the email
               if t_EmailAssignments = 'Y' and t_SendEmail = 'Y' and t_EmailAddress is not null
                 then
                 --Build the real link
                   if t_EmailLinkDestination = 'Job'
                     then
                       t_FinalEmailLink := t_EmailLink || t_JobId;
                   elsif t_EmailLinkDestination = 'Process'
                     then
                       t_FinalEmailLink := t_EmailLink || a_ObjectId;
                   elsif t_EmailLinkDestination = 'To-Do List'
                     then
                       t_FinalEmailLink := t_EmailLink;
                   end if;

                   select o.IsProduction into t_IsProd
                    from query.o_SystemSettings o
                   where o.ObjectDefTypeId = 1
                     and rownum = 1;

                 --Build the Email Message
                   --t_EmailMessage := t_EmailPreamble || t_EmailBody;
                   select n.Text into t_EmailMessage from api.notes n
                    join api.notedefs nd on n.NoteDefId = nd.NoteDefId
                   where n.ObjectId = t_ProcEmailId and nd.Tag = 'PROEML';

                   -- Append a warning to the beginning of the body if this is a non-production environment.
                   if t_IsProd = 'N' then
                     if Length(t_EmailMessage) < 32300 then
                       t_EmailMessage := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This Test Email is from a non-production system.</span><br><br>' || t_EmailMessage;
                     else
                       t_EmailMessage := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This Test Email is from a non-production system.<br><br>Message too long... truncated.</span><br>';
                     end if;
                   end if;

                   t_EmailMessage := replace(t_EmailMessage, '{Link}', t_FinalEmailLink);
                   t_EmailMessage := replace(t_EmailMessage, '{LinkDescription}', t_LinkDescription);
                   t_EmailMessage := replace(t_EmailMessage, '{FirstName}', t_FirstName);
                   t_EmailMessage := replace(t_EmailMessage, '{LastName}', t_LastName);

                 --Send the Email
                   extension.pkg_sendmail.SendEmail(a_ObjectId,
                                                    t_FromEmailAddress,
                                                    t_EmailAddress,
                                                    null,
                                                    null,
                                                    t_EmailSubject,
                                                    t_EmailMessage,
                                                    null,
                                                    null,
                                                    'Y');

               end if; --t_EmailAssignments
             exception when no_data_found --if no data is found then the assigned user doesn't have a relationship to the process email object
             --therefore we don't want to send an email
               then null;
             end;
           end if; --t_Outcome
         end if; --if Internal or External
     end loop; --c

   --get the users that removed from the assignment and remove them from the Audit Table
     for d in (select p.UserId
                 from extension.ProcessAssignmentAudit_t p
                where p.processid = a_ObjectId
               minus
               select pa.userid
                 from api.processassignments pa
                where pa.processid = a_ObjectId)loop
       delete from extension.processassignmentaudit_t where userid = d.UserId and ProcessId = a_ObjectId;
     end loop; --d
   end SendAssignmentNotification32;

  /*-------------------------------------------------------------------------
   * AssignmentSelectDeselect() - Public
   *------------------------------------------------------------------------*/
   procedure AssignmentSelectDeselect(a_ObjectId udt_Id,
                                      a_AsOfDate date) is

   begin
   --loop through the relationships for this user
     for c in (select RelationshipId
                 from query.r_ABCprocessemailuser
                where userid = a_ObjectId
                union
                select RelationshipId
                 from query.r_ABCprocessemailuser
                where userid = a_ObjectId)loop
     --set the detail on the relationship equal to what was set to on the user
       api.pkg_columnupdate.setvalue(c.relationshipid, 'EmailAssignments', api.pkg_columnquery.value(a_ObjectId, 'AssignmentsSelectDeselectAll'));
     end loop;
   end;


  /*-------------------------------------------------------------------------
   * RelateNewUserToProcessEmail() - Public
   *------------------------------------------------------------------------*/
   procedure RelateNewUserToProcessEmail(a_ObjectId udt_Id,
                                         a_AsOfDate date) is
     t_EndPointId             udt_Id := api.pkg_configquery.endpointidforname('o_ProcessEmail', 'Users');
     t_NewRelId               udt_Id;
     t_ABCEndPointId          udt_Id := api.pkg_configquery.endpointidforname('o_ProcessEmail', 'ABCInternalUser');
     t_ABCNewRelId            udt_Id;
     t_UserType               varchar2(30) := api.pkg_columnquery.value(a_ObjectId, 'UserType');

   begin

   --if the user is Internal
     if t_UserType  = 'Internal'

       then
       --loop through the Process Email objects that have the Assignment Notifications box checked
         for d in(select x.objectid
                    from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ProcessEmail', 'ShowOnUserPrefsAssignments', 'Y') as api.udt_ObjectList)) x)loop
         --create the relationship
           t_NewRelId := api.pkg_relationshipupdate.new(t_EndPointId, d.ObjectId, a_ObjectId);
         --Do not default the value to send the Assignment Emails
           --api.pkg_columnupdate.setvalue(t_NewRelId,
           --                              'EmailAssignments',
            --                             'Y');
         end loop; --d
         
         --check for ABC specific processes and create the rel for the ABCInternal User's profile
           for p in(select pr.objectid
                    from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ProcessEmail', 'isABC', 'Y') as api.udt_ObjectList)) pr)loop
               t_ABCNewRelId := api.pkg_relationshipupdate.new(t_ABCEndPointId, p.ObjectId, a_ObjectId);
           end loop; --p

   --otherwise if the user is External
     else
       --loop through the Process Email objects that have the External Notification box checked
         for c in(select x.objectid
                    from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ProcessEmail', 'ShowOnUserPrefsExternal', 'Y') as api.udt_ObjectList)) x)loop
         --create the relationship
           t_NewRelId := api.pkg_relationshipupdate.new(t_EndPointId, c.ObjectId, a_ObjectId);
         --Default the value to the user's preference that was selected
           api.pkg_columnupdate.setvalue(t_NewRelId,
                                         'EmailExternalNotification',
                                         api.pkg_columnquery.value(a_ObjectId, 'ExternalSelectDeselectAll'));
         end loop; --c
     end if; --If Internal or External
   end;

end pkg_EmailNotifications;
/
