create or replace package           pkg_CrossJobMonitor is
  /*-----------------------------------------------------------------------------------------
   * Description
   * Monitors Clearance information & creating the appropriate Processes
   * on the related Archaeological Report Summary Or HRM Clearance.
   * Called From:
   *     HRM Clearance - Review/Prepare Schedule B on Review Clarification Request
   *       - Set all clearances related to the Report Summary to Waiting for Report
   *       - Create Conduct Report Review and assign to that jobs Archaeologist
   *         if the Report Summary is in a status of InReview and Incompleted
   *         Conduct Report Review does not already exist (USC-033)
   *
   *     HRM Clearance - Approved with Requirements on Approved with Requirements
   *       - Check the status of all Related Report Summary Jobs where the Clearance
   *         Applications are online, if all the clearances related to the report summary
   *         are approved, the System must create and assign Coordinator Report Completion Task (USC-035)
   *
   *     Arch Report Summary - Coordinate Permit Report Completion on Proceed with Clearance
   *       - System must create task for clearance preparation to the planner for each online
   *         clearance identified in Covered Projects where the clearance status is Clearance
   *         Requested (USC-019)
   *-----------------------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  --
  Procedure Monitor( a_ObjectId udt_Id,
                     a_AsOfDate date );
  --
  /*
   *  Clearances Approved
   *  Identifies which Report Summaries which have all clearances in a status of Approved.
   *  Return: 0 - means that all clearances are either Approved or Cancelled
   *  Return: 1 - means that at least one clearance was found with a status other than Approved or Cancelled
   */
  Function ClearancesApproved (a_ObjectId udt_Id,
                               a_ClearanceId udt_Id) return udt_Id;
  --
   /*
  *  ClearancesCancelled
  *  Identifies which Report Summaries which have all clearances in a status of Cancelled.
  *  Return: 0 - means that all clearances are Cancelled
  *  Return: 1 - means that at least one clearance was found with a status other than Cancelled
  */
  Function ClearancesCancelled (a_ObjectId udt_Id) return udt_Id;
  --

end pkg_CrossJobMonitor;

 
/

grant execute
on pkg_crossjobmonitor
to posseextensions;

