create or replace package pkg_Dashboard is

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_Widget is extension.pkg_dashboardwidgetutils.udt_Widget;
  subtype udt_Data is extension.pkg_dashboardwidgetutils.udt_Data;
  subtype udt_DateList is api.pkg_Definition.udt_DateList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_DataMatrix is pkg_DashboardUtils.udt_DataMatrix;
  subtype udt_DataList is pkg_DashboardWidgetUtils.udt_DataList;
  subtype udt_CorralTable is pkg_DashboardDefinition.udt_CorralTable;

  g_NullNumberList                      udt_NumberList;
  g_NullStringList                      udt_StringList;
  /*---------------------------------------------------------------------------
   * BuiltInGauges()
   *-------------------------------------------------------------------------*/
  function BuiltInGauges (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * CustomGauge()
   *-------------------------------------------------------------------------*/
  function CustomGauge (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * JobIntakeRate()
   *-------------------------------------------------------------------------*/
  function JobIntakeRate (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * JobSummary()
   *-------------------------------------------------------------------------*/
  function JobSummary (
    a_WidgetId                         udt_Id,
    a_WidgetArgs                       varchar2
  ) return clob ;

  /*---------------------------------------------------------------------------
   * ProcessInWarningState()
   *-------------------------------------------------------------------------*/
  function ProcessInWarningState(
    a_WidgetId                         udt_Id,
    a_WidgetArgs                       varchar2
  ) return clob ;

  /*---------------------------------------------------------------------------
   * ProcessCompletionWarningGauge()
   *-------------------------------------------------------------------------*/
  function ProcessCompletionWarningGauge(
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob;

 /*---------------------------------------------------------------------------
  * ToDoListSummary()
  *-------------------------------------------------------------------------*/
  function ToDoListSummary (
     a_WidgetId                         udt_Id,
     a_WidgetArgs                       varchar2
   ) return clob;

 /*---------------------------------------------------------------------------
  * JobsInStatus()
  *-------------------------------------------------------------------------*/
  function JobsInStatus (
    a_WidgetId                         udt_Id,
    a_WidgetArgs                       varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * DaysToCompleteByStaff()
   *-------------------------------------------------------------------------*/
  function DaysToCompleteByStaff (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob;
  /*---------------------------------------------------------------------------
   * DaysToCompleteSummary()
   *-------------------------------------------------------------------------*/
  function DaysToCompleteSummary (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob;
end pkg_Dashboard;

 
/

grant execute
on pkg_dashboard
to posseextensions;

create or replace package body pkg_Dashboard as

  /*---------------------------------------------------------------------------
   * Subtypes
   *-------------------------------------------------------------------------*/
  subtype udt_ObjectList is api.udt_ObjectList;
  subtype udt_BindVarList is pkg_DashboardSql.udt_BindVarList;
  subtype udt_ResultRecord is pkg_DashboardSql.udt_ResultRecord;

  /*---------------------------------------------------------------------------
   * Constants
   *-------------------------------------------------------------------------*/
  gc_Timeline                           constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Timeline;
  gc_BarGraph                           constant varchar2(10) :=
      pkg_DashboardDefinition.gc_BarGraph;
  gc_Gauge                              constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Gauge;
  gc_Compare                            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Compare;
  gc_Detailed                           constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Detailed;
  gc_Summary                            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Summary;
  gc_CRLF                               constant varchar2(2) :=
      chr(13) || chr(10);
  gc_Count                              constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Count;
  gc_Sum                                constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Sum;
  gc_Average                            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Average;
  gc_String                             constant varchar2(10) :=
      pkg_DashboardSql.gc_String;
  gc_Number                             constant varchar2(10) :=
      pkg_DashboardSql.gc_Number;
  gc_Date                               constant varchar2(10) :=
      pkg_DashboardSql.gc_Date;

  /*---------------------------------------------------------------------------
   * DashboardObjectCount()
   *-------------------------------------------------------------------------*/
  function DashboardObjectCount
  return pls_integer is
    t_Count                             pls_integer;
  begin
    select count(*)
      into t_Count
      from extension.DashboardObjects_gt;
    return t_Count;
  end DashboardObjectCount;

  /*---------------------------------------------------------------------------
   * BuiltInGauges()
   *-------------------------------------------------------------------------*/
  function BuiltInGauges (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob is
    t_Type                              varchar2(100);
  begin
    t_Type := api.pkg_ColumnQuery.Value(a_WidgetId, 'GaugeCalculationName');

    case t_Type
      when 'JobIntakeRate' then
        return JobIntakeRate(a_WidgetId, a_WidgetArgs);
      when 'JobSummary' then
        return JobSummary(a_WidgetId, a_WidgetArgs);
      when 'DaysToCompleteSummary' then
        return DaysToCompleteSummary(a_WidgetId, a_WidgetArgs);
      when 'DaysToCompleteByStaff' then
        return DaysToCompleteByStaff(a_WidgetId, a_WidgetArgs);
      else
        api.pkg_Errors.RaiseError(-20000, 'Invalid Gauge Calculation "'
            || t_Type || '" for widget id: ' || a_WidgetId);
    end case;

  end BuiltInGauges;

  /*---------------------------------------------------------------------------
   * CustomGauge()
   *-------------------------------------------------------------------------*/
  function CustomGauge (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob is
    t_Widget                            udt_Widget;
    t_SQL                               varchar2(4000);
    t_Value                             number;
    t_Min                               number;
    t_Max                               number;
    t_Threshold1                        number;
    t_Threshold2                        number;
  begin
    t_SQL := api.pkg_ColumnQuery.Value(a_WidgetId, 'GaugeSQL');
    t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
        a_WidgetArgs);

    execute immediate t_SQL
    into t_Value, t_Min, t_Max, t_Threshold1, t_Threshold2;

    pkg_DashboardWidgetUtils.SetGaugeValues(t_Widget, t_Value, t_Min, t_Max,
        t_Threshold1, t_Threshold2);
    return pkg_dashboardWidgetUtils.ToJSON(t_Widget);
  end CustomGauge;
 /*---------------------------------------------------------------------------
   * JobIntakeRate()
   *-------------------------------------------------------------------------*/
  function JobIntakeRate (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob is
    t_PreviousYears                     pls_integer := 0;
    t_ExcludeCancelledJoinClause        varchar2(1000);
    t_FromDate                          date;
    t_ToDate                            date;
    t_JobTypeIds                        udt_IdList;
    t_JobTypeId                         udt_Id;
    t_Dates                             udt_DateList;
    t_Widget                            udt_Widget;
    t_Sql                               varchar2(4000);
    t_Columns                           varchar2(4000);
    t_Data                              udt_NumberList;
    t_GroupData                         udt_StringList;
    t_AllData                           udt_NumberList;
    t_AllGroupData                      udt_StringList;
    t_AllDates                          udt_DateList;
    t_Group1                            varchar2(4000);
    t_Group2                            varchar2(4000);
    t_WhereClause                       varchar2(4000);

  begin
    t_JobTypeIds := pkg_DashboardWidgetUtils.DefIdList(a_WidgetId, 'JobType');
    t_JobTypeId := t_JobTypeIds(1);

    t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
        a_WidgetArgs);

    t_Group1 := api.pkg_ColumnQuery.Value(a_WidgetId, 'Group1Expression');

    if t_Widget.view_ in (gc_Detailed, gc_Compare) then
      if t_Group1 is null then
      api.pkg_Errors.RaiseError(-20000,
          'Group 1 Expression not configured for ' || t_Widget.Title);
      end if;
    end if;

    if t_Widget.view_ = gc_Compare then
      if t_Widget.ToDate - t_Widget.FromDate > 365 then
        api.pkg_Errors.RaiseError(-20000,
            'Compare Previous Years date range must be a year or less.');
      end if;
      t_PreviousYears := 2;
    end if;

    if api.pkg_ColumnQuery.Value(a_WidgetId, 'IncludeCancelled') <> 'Y' then
      t_ExcludeCancelledJoinClause :=
          '  join api.JobStatuses js' || gc_CRLF ||
          '    on js.JobTypeId = j.ObjectDefId' || gc_CRLF ||
          '    and js.StatusId = j.StatusId' || gc_CRLF ||
          '    and js.StatusType <> ''X''' || gc_CRLF;
    end if;
  -- Handle time period criteria
    if t_Widget.TimePeriodDetail is not null then
      t_Columns := 'j.JobId' || ',' || gc_CRLF ||t_Widget.TimePeriodDetail;
      t_WhereClause := 'and '||t_Widget.TimePeriodDetail||' >= :2' || gc_CRLF ||
                       'and '||t_Widget.TimePeriodDetail||' < :3 + 1' || gc_CRLF;
    else
      t_Columns := 'j.JobId' || ',' || gc_CRLF ||'j.CreatedDate';
      t_WhereClause := 'and j.CreatedDate >= :2' || gc_CRLF ||
                       'and j.CreatedDate < :3 + 1';
    end if;

    if t_Group1 is not null and t_Widget.view_ in (gc_Detailed, gc_Compare) then
      t_Columns := t_Columns || ',' || gc_CRLF || t_Group1;
      t_WhereClause := t_WhereClause || gc_CRLF ||'and '|| t_Group1 || ' is not null';
    end if;

    t_Sql :=
      'select ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      '  from extensioncorral.Jobs j' || gc_CRLF ||
      pkg_DashboardUtils.FilterJoinClause(t_Widget, 'j.JobId',true) ||
      t_ExcludeCancelledJoinClause ||
      '  where j.ObjectDefId = :1' || gc_CRLF ||
      t_WhereClause ;

    for i in 0..t_PreviousYears loop
      t_FromDate := add_months(t_Widget.FromDate, i * -12);
      t_ToDate := add_months(t_Widget.ToDate, i * -12);

      if t_Group1 is not null and t_Widget.view_ in (gc_Detailed, gc_Compare) then
        execute immediate t_Sql
        bulk collect into t_Data, t_Dates, t_GroupData
        using t_JobTypeId, t_FromDate, t_ToDate;
      else
        execute immediate t_Sql
        bulk collect into t_Data, t_Dates
        using t_JobTypeId, t_FromDate, t_ToDate;
      end if;

      Pkg_Dashboardutils.Append(t_AllData,t_Data);
      Pkg_Dashboardutils.Append(t_AllDates,t_Dates);
      Pkg_Dashboardutils.Append(t_AllGroupData,t_GroupData);
    end loop;

    -- creating Timeline graph
    if t_Widget.DisplayType = gc_Timeline then
      pkg_DashboardUtils.GroupByTimeline(t_Widget, t_AllDates, t_AllGroupData);

    -- creating Bar graph
    elsif t_Widget.DisplayType = gc_BarGraph then
      if t_Widget.View_ = gc_Compare then
        pkg_DashboardUtils.GroupByValues(t_Widget, t_AllGroupData, t_AllDates);
      else
        if t_AllGroupData.count > 0 then
          pkg_DashboardUtils.GroupByValues(t_Widget, t_AllGroupData);
        else
          -- a single group only so just use the count
          pkg_DashboardWidgetUtils.SetSingleValue(t_Widget,
              t_AllData.Count, 'All');
        end if;
      end if;

    -- creating Gauge graph
    elsif t_Widget.DisplayType = gc_Gauge then
      pkg_DashboardWidgetUtils.SetGaugeValues(t_Widget, t_AllData.Count);
    end if;
    t_Data.delete;
    t_Dates.delete;
    t_GroupData.delete;
    t_AllDates.delete;
    t_AllData.delete;
    t_AllGroupData.delete;

    return pkg_dashboardWidgetUtils.ToJSON(t_Widget);
  end JobIntakeRate;

  /*---------------------------------------------------------------------------
   * JobSummary()
   *-------------------------------------------------------------------------*/
  function JobSummary (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob is
    t_Widget                            udt_Widget;
    t_JobTypeId                         udt_Id :=
      api.pkg_configquery.ObjectDefIdForName(api.pkg_columnquery.Value(
        extension.pkg_ObjectQuery.RelatedObject(a_WidgetId, 'JobType'),
        'Dashboard_ObjectDefName'));
    t_GroupCols                         udt_StringList;
    t_ShowValues                        udt_StringList;
    t_SubGroupShowValues                udt_StringList;
    t_GroupLevel                        pls_integer;
    t_Columns                           varchar2(4000);
    t_BindVars                          udt_BindVarList;
    t_Sql                               varchar2(4000);
    t_WhereClause                       varchar2(4000);
    t_StartStatusIds                    api.udt_ObjectList;
    t_StatusJoinClause                  varchar2(100);
    t_ColumnCount                       pls_integer;
    t_BindName                          varchar2(30);
    t_ResultRecord                      udt_ResultRecord;
    t_ColumnTypes                       udt_StringList;
    t_Calculation                       varchar2(10);
    t_CalcExp                           varchar2(4000);
  begin
    -- Create Widget so we can use its preset attributes
    t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
      a_WidgetArgs);

    -- Handle time period criteria
    if t_Widget.TimePeriodDetail is null and (t_Widget.fromdate_ is not null
        or t_Widget.todate_ is not null ) then
      api.pkg_Errors.RaiseError(-20000,
          'Detail to use for time period is not specified for ' ||
          t_Widget.Title);
    end if;

    if t_Widget.FromDate is not null then
      t_WhereClause := t_WhereClause || ' and ' || t_Widget.TimePeriodDetail ||
          ' >= :FromDate' || gc_CRLF;
      pkg_DashboardSql.AddBindVar('FromDate', t_Widget.FromDate, t_BindVars);
    end if;
    if t_Widget.ToDate is not null then
      t_WhereClause := t_WhereClause || ' and '|| t_Widget.TimePeriodDetail ||
          ' < :ToDate + 1' || gc_CRLF;
      pkg_DashboardSql.AddBindVar('ToDate', t_Widget.ToDate, t_BindVars);
    end if;

    if t_Widget.DisplayType = gc_Gauge then
      t_GroupCols(1) := 'j.ObjectDefId';
      t_ShowValues(1) := null;
    else
      pkg_DashboardUtils.GetGroupConfig(a_WidgetId, t_GroupCols, t_ShowValues);
    end if;

    if api.pkg_ColumnQuery.Value(a_WidgetId, 'ShowTwoGroups') = 'Y' then
      t_ColumnCount := 2;
    else
      t_ColumnCount := 1;
    end if;

    if t_GroupCols(1) is null then
      api.pkg_Errors.RaiseError(-20000,
          'Group 1 Expression not configured for ' || t_Widget.Title);
    end if;

    t_GroupLevel := t_Widget.GroupValues.count + 1;
    -- if a drill down is requested that goes beyond the configured group
    -- expressions all drill down values will be removed (better than an error)
    -- The front end should not make such a call but this is just in case.
    if t_GroupLevel > 4 or t_GroupCols(t_GroupLevel) is null then
      t_Widget.GroupValues.delete;
      t_GroupLevel := 0;
    end if;

    -- add sections to where clause for drill down (group) values passed in
    if t_Widget.DisplayType = gc_Gauge then
      t_ColumnCount := 1;
      t_GroupLevel := 1;
    else
      for i in 1..t_GroupLevel - 1 loop
        t_BindName := ':Group' || to_char(i);
        t_WhereClause := t_WhereClause || ' and ' || t_GroupCols(i) ||
            ' = ' || t_BindName || gc_CRLF;
        pkg_DashboardSql.AddBindVar(t_BindName, t_Widget.GroupValues(i),
            t_BindVars);
      end loop;
    end if;

    -- add columns and where clause sections based on level of drill down
    for i in t_GroupLevel..t_GroupLevel + t_ColumnCount - 1 loop
      if t_GroupCols(i) is null then
        -- if ShowTwoGroups is true but only three group columns (expressions)
        -- are configured then there will be no sub-group for the drill down
        t_ColumnCount := t_ColumnCount - 1;
      else
        pkg_DashboardSql.AddColumnType(gc_String, t_ColumnTypes);
        pkg_Utils.AddSection(t_Columns, t_GroupCols(i), ',' || gc_CRLF);
        t_WhereClause := t_WhereClause || ' and ' || t_GroupCols(i) ||
            ' is not null' || gc_CRLF;
      end if;
    end loop;

    -- add calculation expression if required
    t_Calculation := nvl(api.pkg_ColumnQuery.Value(a_WidgetId, 'Calculation'),
        gc_Count);
    t_CalcExp := api.pkg_ColumnQuery.Value(a_WidgetId, 'CalculationDetail');
    if t_Calculation in (gc_Sum, gc_Average) and t_CalcExp is not null then
      pkg_DashboardSql.AddColumnType(gc_Number, t_ColumnTypes);
      pkg_Utils.AddSection(t_Columns, t_CalcExp, ',' || gc_CRLF);
    end if;

    -- limit by a list of status ids if any are configured
    -- this should be acceptable as it is expected to be a short list
    t_StartStatusIds := extension.pkg_dashboardwidgetutils.StatusObjectList(
        a_WidgetId, 'StartStatus');
    if t_StartStatusIds.count > 0 then
      t_WhereClause := t_WhereClause ||
          ' and j.StatusId in (';
      for i in 1..t_StartStatusIds.count loop
        if i > 1 then
          t_WhereClause := t_WhereClause || ', ';
        end if;
        t_WhereClause := t_WhereClause ||
            to_char(t_StartStatusIds(i).ObjectId);
      end loop;
      t_WhereClause := t_WhereClause || ')';
    end if;

    -- construct the sql statement
    t_Sql :=
      'select ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      '  from extensioncorral.Jobs j' || gc_CRLF ||
      t_StatusJoinClause ||
      pkg_DashboardUtils.FilterJoinClause(t_Widget, 'j.JobId', true) ||
      '  where j.ObjectDefId = :ObjectDefId' || gc_CRLF ||
      t_WhereClause;
    pkg_DashboardSql.AddBindVar('ObjectDefId', t_JobTypeId, t_BindVars);

    -- execute the query and get the rows back
    pkg_DashboardSql.ExecuteInto(t_Sql, t_BindVars, t_ColumnTypes,
        t_ResultRecord);

    if t_Widget.DisplayType = gc_Gauge then
      --pkg_DashboardWidgetUtils.SetGaugeValues(t_Widget,
      --    t_ResultRecord.Strings1.count);
      extension.pkg_dashboardutils.GroupByValues(t_Widget,
          t_ResultRecord.Strings1, t_ResultRecord.Strings2,
          pkg_Utils.Split(t_ShowValues(t_GroupLevel), ','),
          g_NullStringList,
          nvl(api.pkg_ColumnQuery.Value(a_WidgetId, 'Calculation'), gc_Count),
          t_ResultRecord.Numbers);
      pkg_DashboardWidgetUtils.SetGaugeValues(t_Widget, t_Widget.data(1).y(1));
    else
      if t_GroupLevel < 4 then
        t_SubGroupShowValues :=
            pkg_Utils.Split(t_ShowValues(t_GroupLevel + 1), ',');
      else
        t_SubGroupShowValues := g_NullStringList;
      end if;

      extension.pkg_dashboardutils.GroupByValues(t_Widget,
          t_ResultRecord.Strings1, t_ResultRecord.Strings2,
          pkg_Utils.Split(t_ShowValues(t_GroupLevel), ','),
          t_SubGroupShowValues,
          nvl(api.pkg_ColumnQuery.Value(a_WidgetId, 'Calculation'), gc_Count),
          t_ResultRecord.Numbers);
    end if;

    return extension.pkg_dashboardwidgetutils.toJSON(t_Widget);
  end JobSummary;

  /*---------------------------------------------------------------------------
   * ProcessInWarningState()
   *-------------------------------------------------------------------------*/
  function ProcessInWarningState(
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob is
    t_Widget                            udt_Widget;
    t_ProcessTypeIds                    udt_ObjectList;
    t_JobTypeIds                        udt_ObjectList;
    t_Group1                            varchar2(4000);
    t_Group2                            varchar2(4000);
    t_Columns                           varchar2(4000);
    t_Sql                               varchar2(4000);
    t_WhereClause                       varchar2(4000);
    t_JobJoinClause                     varchar2(200);
    t_Data                              udt_StringList;
    t_GroupData                         udt_StringList;

  begin
    t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
      a_WidgetArgs);

    t_ProcessTypeIds := extension.pkg_dashboardwidgetutils.DefIdObjectList(
        a_WidgetId, 'ProcessType');
    t_JobTypeIds := extension.pkg_dashboardwidgetutils.DefIdObjectList(
        a_WidgetId, 'JobType');

    t_Group1 := api.pkg_ColumnQuery.Value(a_WidgetId, 'Group1Expression');
    if t_Group1 is null then
      api.pkg_Errors.RaiseError(-20000,
          'Group 1 Expression not configured for ' || t_Widget.Title);
    end if;

    if t_Widget.DisplayType <> gc_Gauge then
      t_Group2 := api.pkg_ColumnQuery.Value(a_WidgetId, 'Group2Expression');
    end if;

    t_Columns := t_Group1;
    t_WhereClause := 'and ' || t_Group1 || ' is not null' || gc_CRLF;

    if t_Group2 is not null then
      t_Columns := t_Columns || ',' || gc_CRLF || t_Group2;
      t_WhereClause := t_WhereClause ||
          'and ' || t_Group2 || ' is not null' || gc_CRLF;
    end if;

    if t_JobTypeIds.count > 0 then
      t_JobJoinClause :=
        '  join extensioncorral.Jobs j' || gc_CRLF ||
        '    on j.JobId = p.JobId' || gc_CRLF ||
        '  join table(:JobTypes) jt' || gc_CRLF ||
        '    on jt.ObjectId = j.ObjectDefId' || gc_CRLF;
    end if;

-- FIX ME - may not have to join to the corral table if only extensioncorral
-- tables are referenced, but how to tell that is the case?
    t_Sql :=
      'select /*+ cardinality (pt 1) */ ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      '  from table(:ProcessTypes) pt' || gc_CRLF ||
      '  join extensioncorral.Processes p' || gc_CRLF ||
      '    on p.ObjectDefId = pt.ObjectId' || gc_CRLF ||
      t_JobJoinClause ||
      pkg_DashboardUtils.FilterJoinClause(t_Widget, 'p.ProcessId', true) ||
      '  where 1 = 1' || gc_CRLF ||
      t_WhereClause;

    -- execute the sql with the correct arguments
    if t_JobJoinClause is null then
      if t_Group2 is null then
        execute immediate t_Sql
        bulk collect into t_Data
        using t_ProcessTypeIds;
      else
        execute immediate t_Sql
        bulk collect into t_Data, t_GroupData
        using t_ProcessTypeIds;
      end if;
    else
      if t_Group2 is null then
        execute immediate t_Sql
        bulk collect into t_Data
        using t_ProcessTypeIds, t_JobTypeIds;
      else
        execute immediate t_Sql
        bulk collect into t_Data, t_GroupData
        using t_ProcessTypeIds, t_JobTypeIds;
      end if;
    end if;

    if t_Widget.DisplayType = gc_Gauge then
      pkg_DashboardWidgetUtils.SetGaugeValues(t_Widget, t_Data.count);
    else
      extension.pkg_dashboardutils.GroupByValues(t_Widget, t_Data, t_GroupData);
    end if;

    return extension.pkg_dashboardwidgetutils.toJSON(t_Widget);
  end ProcessInWarningState;

  /*---------------------------------------------------------------------------
   * ProcessCompletionWarningGauge()
   *-------------------------------------------------------------------------*/
  function ProcessCompletionWarningGauge(
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob is
    t_Widget                            udt_Widget;
    t_ProcessTypeId                     udt_Id :=
      api.pkg_configquery.ObjectDefIdForName(api.pkg_columnquery.Value(
        extension.pkg_ObjectQuery.RelatedObject(a_WidgetId, 'ProcessType'),
        'Dashboard_ObjectDefName'));
    t_Candidates                        udt_IdList;
    t_ObjectList                        udt_ObjectList := api.udt_objectlist();
    t_Count                             pls_integer;
    t_OnlyIncludeScheduled              boolean := api.pkg_ColumnQuery.Value(
      a_WidgetId, 'OnlyIncludeScheduled') = 'Y';

  begin
    t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
      a_WidgetArgs);

    if t_OnlyIncludeScheduled then
      --gather candidates
      select cp.processid
        bulk collect into t_Candidates
        from extensioncorral.process cp
        join api.processes p
          on p.processid = cp.processid
          and p.ScheduledStartDate >= t_Widget.fromdate
          and p.ScheduledStartDate < t_Widget.todate + 1--is already truncated
        where cp.ObjectDefId = t_ProcessTypeId;
    else
      select cp.processid
        bulk collect into t_Candidates
        from extensioncorral.process cp
        join api.processes p
          on p.processid = cp.processid
          and (
                (     p.ScheduledStartDate >= t_Widget.fromdate
                  and p.ScheduledStartDate < t_Widget.todate + 1--is already truncated
                )
                or
                (     p.DateCompleted >= t_Widget.fromdate
                  and p.DateCompleted < t_Widget.todate + 1--is already truncated
                )
               )
        where cp.ObjectDefId = t_ProcessTypeId;
    end if;
    --convert to ObjectList and filter
    t_ObjectList := extension.pkg_utils.ConvertToObjectList( t_Candidates );
    t_Candidates.delete;

    extension.pkg_dashboardutils.ApplyWidgetFilters( t_ObjectList,
      t_ProcessTypeId, a_WidgetId );

    select count(*)
      into t_Count
      from table(t_ObjectList) X
      join extensioncorral.process p
        on p.PROCESSID = x.objectid
      where p.DateCompleted is not null;

    pkg_DashboardWidgetUtils.SetGaugeValues(t_Widget, t_Count, 0,
        t_ObjectList.count);
    t_ObjectList.delete;

    return extension.pkg_dashboardwidgetutils.toJSON(t_Widget);
  end ProcessCompletionWarningGauge;

  /*---------------------------------------------------------------------------
   * ToDoListSummary()
   *-------------------------------------------------------------------------*/
  function ToDoListSummary (
    a_WidgetId                         udt_Id,
    a_WidgetArgs                       varchar2
  ) return clob is
     t_ProcessIds                 udt_IdList;
     t_ObjectList                 api.udt_ObjectList;
     t_Widget                     udt_Widget;
     t_ProcessDescription         udt_StringList;
     t_AccessGroup                varchar2(100);
     t_UserId                     udt_Id;

   begin
     t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
        a_WidgetArgs);
     t_AccessGroup := api.pkg_columnquery.value(a_WidgetId,'AccessGroup');

     if t_AccessGroup is null then
        if t_Widget.users_ is not null then
           t_UserId := to_number(t_Widget.users_);
        else
           t_UserId := api.pkg_securityquery.EffectiveUserId();
        end if;
         --gather candidates
         select ipa.ProcessId
           bulk collect into t_ProcessIds
           from api.incompleteprocessassignments ipa
           join api.objects o
             on o.objectid = ipa.processid
            and o.objectdeftypeid = 3
           join api.objectdefs od
             on od.objectdefid = o.objectdefid
          where ipa.AssignedTo = t_UserId;
      else
       --gather candidates
        if t_Widget.users_ is not null then
         select ipa.ProcessId
           bulk collect into t_ProcessIds
           from table(extension.pkg_DashboardWidgetUtils.UserObjectList(t_Widget.users_)) argusers
           join api.incompleteprocessassignments ipa
             on ipa.AssignedTo = argusers.ObjectId
           join api.objects o
             on o.objectid = ipa.processid
            and o.objectdeftypeid = 3
           join api.objectdefs od
             on od.objectdefid = o.objectdefid
           join api.accessgroupusers agu
             on agu.UserId = ipa.AssignedTo
           join api.accessgroups ag
             on ag.AccessGroupId = agu.AccessGroupId
            and ag.Description = t_AccessGroup;
        else
         select ipa.ProcessId
           bulk collect into t_ProcessIds
           from api.incompleteprocessassignments ipa
           join api.objects o
             on o.objectid = ipa.processid
            and o.objectdeftypeid = 3
           join api.objectdefs od
             on od.objectdefid = o.objectdefid
           join api.accessgroupusers agu
             on agu.UserId = ipa.AssignedTo
           join api.accessgroups ag
             on ag.AccessGroupId = agu.AccessGroupId
            and ag.Description = t_AccessGroup;
        end if;
        end if;

     --convert candidate list to ObjectList for filtering
     t_ObjectList := extension.pkg_utils.ConvertToObjectList(t_ProcessIds);
     t_ProcessIds.delete;

      select p.ObjectDefDescription
        bulk collect into t_ProcessDescription
        from table(t_ObjectList) t
        join extensioncorral.process p
          on p.ProcessId = t.ObjectId;

     extension.pkg_dashboardutils.GroupByValues(t_Widget,t_ProcessDescription);

     return extension.pkg_dashboardwidgetutils.toJSON(t_Widget);

  end;

  /*---------------------------------------------------------------------------
   * JobsInStatus()
   *-------------------------------------------------------------------------*/
  function JobsInStatus (
    a_WidgetId                          udt_Id,
    a_WidgetArgs                        varchar2
  ) return clob is
    t_JobTypeIds                        udt_IdList;
    t_JobTypeId				udt_Id;
    t_Widget                            udt_Widget;
    t_StatusDescriptions                udt_StringList;
    t_Sql                               varchar2(4000);

  begin
    t_JobTypeIds := pkg_DashboardWidgetUtils.DefIdList(a_WidgetId, 'JobType');
    t_JobTypeId := t_JobTypeIds(1);
    t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
        a_WidgetArgs);

    t_Sql :=
      'insert into extension.DashboardObjects_gt' || gc_CRLF ||
      '(select j.JobId' || gc_CRLF ||
      '  from extensioncorral.Jobs j' || gc_CRLF ||
      '  join table(extension.pkg_DashboardWidgetUtils.StatusObjectList(:1,' ||
      '      ''StartStatus'')) s' || gc_CRLF ||
      '    on s.ObjectId = j.StatusId' || gc_CRLF ||
      pkg_DashboardUtils.FilterJoinClause(t_Widget, 'j.JobId') ||
      '  where j.ObjectDefId = :2)';

    execute immediate t_Sql
    using a_WidgetId, t_JobTypeId;

    select s.Tag
      bulk collect into t_StatusDescriptions
      from extension.DashboardObjects_gt do
      join extensioncorral.jobs j
        on j.JobId = do.ObjectId
      join api.Statuses s
        on s.StatusId = j.StatusId;

    extension.pkg_DashboardUtils.GroupByValues(t_Widget, t_StatusDescriptions);

    return extension.pkg_DashboardWidgetUtils.toJSON(t_Widget);
  end JobsInStatus;

  /*---------------------------------------------------------------------------
   * DaysToCompleteByStaff()
   *-------------------------------------------------------------------------*/
   function DaysToCompleteByStaff (
     a_WidgetId                    udt_Id,
     a_WidgetArgs                  varchar2
   ) return clob is
    t_PreviousYears                     pls_integer := 0;
    t_AccessGroupNameJoinClause         varchar2(1000);
    t_ProcessTypeJoinClause             varchar2(1000);
    t_Dates                             udt_DateList;
    t_FromDate                          date;
    t_ToDate                            date;
    t_ProcessTypeIds                    udt_IdList;
    t_Widget                            udt_Widget;
    t_Sql                               varchar2(4000);
    t_AccessGroup                       varchar2(100);
    t_WorkingOrCalendarDays             varchar2(200);
    t_Columns                           varchar2(4000);
    t_Data                              udt_NumberList;
    t_GroupData                         udt_StringList;
    t_AllData                           udt_NumberList;
    t_AllGroupData                      udt_StringList;
    t_AllDates                          udt_DateList;

  begin
    t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
        a_WidgetArgs);
    t_ProcessTypeIds := pkg_DashboardWidgetUtils.DefIdList(a_WidgetId, 'ProcessType');
    t_AccessGroup := api.pkg_columnquery.value(a_WidgetId,'AccessGroup');
    t_WorkingOrCalendarDays := api.pkg_columnquery.value(a_WidgetId,'WorkingOrCalendarDays');

    if t_WorkingOrCalendarDays = 'Working Days' then
       t_Columns := 'case
                      when p.ScheduledStartDate is not null then
                        extension.pkg_utils.WorkingDaysBetween(p.ScheduledStartDate,p.DateCompleted)
                     else
                        extension.pkg_utils.WorkingDaysBetween(l.CreatedDate,p.DateCompleted)
                     end';
    else
       t_Columns := 'case
                      when p.ScheduledStartDate is not null then
                        trunc(p.DateCompleted) - trunc(p.ScheduledStartDate)
                     else
                        trunc(p.DateCompleted) - trunc(l.CreatedDate)
                     end';
    end if;

    t_Columns := t_Columns || ',' || gc_CRLF ||'p.DateCompleted' || ',' || gc_CRLF ||'u.Name';
    if t_AccessGroup is not null then
       t_AccessGroupNameJoinClause :=
          '  join api.accessgroupusers agu' || gc_CRLF ||
          '    on agu.UserId = u.UserId' || gc_CRLF ||
          '  join api.accessgroups ag' || gc_CRLF ||
          '    on ag.AccessGroupId = agu.AccessGroupId' || gc_CRLF ||
          '   and ag.Description = :2'|| gc_CRLF;
     end if;

    if t_ProcessTypeIds.Count > 0 then
       t_ProcessTypeJoinClause :=
         '    and o.objectdefid = :1' || gc_CRLF;
    end if;

    if t_Widget.view_ = gc_Compare then
      if t_Widget.ToDate - t_Widget.FromDate > 365 then
        api.pkg_Errors.RaiseError(-20000,
            'Compare Previous Years date range must be a year or less.');
      end if;
      t_PreviousYears := 2;
    end if;

    t_Sql :=
      'select ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      '   from workflow.processes p' || gc_CRLF ||
      '   join objmodelphys.Objects o' || gc_CRLF ||
      '     on o.ObjectId = p.ProcessId' || gc_CRLF ||
      t_ProcessTypeJoinClause ||
      '   join api.logicaltransactions l' || gc_CRLF ||
      '     on l.LogicalTransactionId = o.CreatedLogicalTransactionId' || gc_CRLF ||
      '   join api.logicaltransactions l2' || gc_CRLF ||
      '     on l2.LogicalTransactionId = p.CompletedByLogicalTransId' || gc_CRLF ||
      '   join api.users u' || gc_CRLF ||
      '     on u.UserId = l2.CreatedBy' || gc_CRLF ||
      t_AccessGroupNameJoinClause ||
      '  where p.DateCompleted >= :3' || gc_CRLF ||
      '    and p.DateCompleted < :4 + 1' || gc_CRLF ||
      '    and p.Outcome is not null';

    for i in 0..t_PreviousYears loop
      t_FromDate := add_months(t_Widget.FromDate, i * -12);
      t_ToDate := add_months(t_Widget.ToDate, i * -12);

      if t_ProcessTypeIds.Count > 0 then
       for j in 1..t_ProcessTypeIds.Count loop
        if t_AccessGroup is not null then
            execute immediate t_Sql
            bulk collect into t_Data, t_Dates, t_GroupData
            using t_ProcessTypeIds(j), t_AccessGroup, t_FromDate, t_ToDate;
        else
            execute immediate t_Sql
            bulk collect into t_Data, t_Dates, t_GroupData
            using t_ProcessTypeIds(j), t_FromDate, t_ToDate;
        end if;
       end loop;
      else
        if t_AccessGroup is not null then
            execute immediate t_Sql
            bulk collect into t_Data, t_Dates, t_GroupData
            using t_AccessGroup, t_FromDate, t_ToDate;
        else
            execute immediate t_Sql
            bulk collect into t_Data, t_Dates, t_GroupData
            using t_FromDate, t_ToDate;
        end if;
      end if;
      Pkg_Dashboardutils.Append(t_AllData,t_Data);
      Pkg_Dashboardutils.Append(t_AllDates,t_Dates);
      Pkg_Dashboardutils.Append(t_AllGroupData,t_GroupData);
    end loop;

    -- creating Timeline graph
    if t_Widget.DisplayType = gc_Timeline and t_Widget.view_ in (gc_Detailed, gc_Compare) then
      pkg_DashboardUtils.GroupByTimeline(t_Widget, t_AllDates, t_AllGroupData, gc_Average,t_AllData);
    else
      t_AllGroupData.delete;
      pkg_DashboardUtils.GroupByTimeline(t_Widget, t_AllDates, t_AllGroupData, gc_Average,t_AllData);
    end if;
    t_Dates.delete;
    t_Data.delete;
    t_GroupData.delete;
    t_AllDates.delete;
    t_AllData.delete;
    t_AllGroupData.delete;

    return pkg_DashboardWidgetUtils.ToJSON(t_Widget);

end DaysToCompleteByStaff;

   function FormatStatusList (
     a_Statuses                    varchar2
   ) return varchar2 is
     t_StatusList                  udt_StringList;
     t_Statuses                    varchar2(4000);
   begin
     t_StatusList := extension.pkg_utils.Split(a_Statuses, ',');
     for i in 1..t_StatusList.count loop
       extension.pkg_utils.AddSection(t_Statuses, '''' || t_StatusList(i) || '''', ',');
     end loop;

     return t_Statuses;
   end;

  /*---------------------------------------------------------------------------
   * DaysToCompleteSummary()
   *-------------------------------------------------------------------------*/
   function DaysToCompleteSummary (
     a_WidgetId                    udt_Id,
     a_WidgetArgs                  varchar2
   ) return clob is
    t_PreviousYears                     pls_integer := 0;
    t_ExcludeCancelledJoinClause        varchar2(1000);
    t_FromDate                          date;
    t_ToDate                            date;
    t_JobTypeIds                        udt_IdList;
    t_JobTypeId                         udt_Id;
    t_Dates                             udt_DateList;
    t_Widget                            udt_Widget;
    t_Sql                               varchar2(4000);
    t_WorkingOrCalendarDays             varchar2(200);
    t_Group1                            varchar2(4000);
    t_Group2                            varchar2(4000);
    t_Columns                           varchar2(4000);
    t_WhereClause                       varchar2(4000);
    t_Data                              udt_NumberList;
    t_GroupData                         udt_StringList;
    t_ExcludeJoinClause                 varchar2(100);
    t_StatusChangeJoinClause            varchar2(500);
    t_StatusChangeGroupByClause         varchar2(500);
    t_ExcludeStatuses                   varchar2(4000);
    t_StartStatus                       varchar2(4000);
    t_EndStatus                         varchar2(4000);
    t_StartDateDetail                   varchar2(500);
    t_EndDateDetail                     varchar2(500);
    t_AllData                           udt_NumberList;
    t_AllGroupData                      udt_StringList;
    t_AllDates                          udt_DateList;


    t_Jobs                              udt_IdList;
    t_StartDates                        udt_DateList;
    t_EndDates                          udt_DateList;
    t_DaysToComplete                    number;
    t_StartDate                         date;
    t_EndDate                           date;
    t_PrevEndDate                       date;
    cursor c_ExcludeStatuses(a_JobId number, a_StartDate date, a_EndDate date) is
      select
        EffectiveStartDate,
        EffectiveEndDate
      from extension.JobStatusChange
      where JobId = a_JobId
        and instr(t_ExcludeStatuses, StatusName) > 0
        and EffectiveStartDate <= a_EndDate
        and EffectiveEndDate > a_StartDate
      order by EffectiveStartDate;

  begin
    t_Widget := extension.pkg_dashboardwidgetutils.NewWidget(a_WidgetId,
        a_WidgetArgs);
    t_JobTypeIds := pkg_DashboardWidgetUtils.DefIdList(a_WidgetId, 'JobType');
    t_JobTypeId := t_JobTypeIds(1);
    t_WorkingOrCalendarDays := api.pkg_columnquery.value(a_WidgetId,'WorkingOrCalendarDays');
    t_StartStatus := api.pkg_ColumnQuery.Value(a_WidgetId, 'StartStatus');
    t_EndStatus := api.pkg_ColumnQuery.Value(a_WidgetId, 'EndStatus');
    t_StartDateDetail := api.pkg_ColumnQuery.Value(a_WidgetId, 'StartDateDetail');
    t_EndDateDetail := api.pkg_ColumnQuery.Value(a_WidgetId, 'EndDateDetail');

    if t_Widget.DisplayType = gc_Gauge then
      t_Group1 := 'j.ObjectDefId';
    else
      t_Group1 := api.pkg_ColumnQuery.Value(a_WidgetId, 'Group1Expression');
    end if;

    if t_Group1 is null then
      api.pkg_Errors.RaiseError(-20000,
          'Group 1 Expression not configured for ' || t_Widget.Title);
    end if;

    if t_StartStatus is null and t_StartDateDetail is null then
      api.pkg_Errors.RaiseError(-20000,
          'Start statuses or start date columns not configured for ' || t_Widget.Title);
    end if;
    if t_EndStatus is null and t_EndDateDetail is null then
      api.pkg_Errors.RaiseError(-20000,
          'End statuses or end date columns not configured for ' || t_Widget.Title);
    end if;
    if t_StartStatus is not null and t_StartDateDetail is not null then
      api.pkg_Errors.RaiseError(-20000,
          'Either start statuses or start date columns must be configured for ' || t_Widget.Title||' but not both');
    end if;
    if t_EndStatus is not null and t_EndDateDetail is not null then
      api.pkg_Errors.RaiseError(-20000,
          'Either end statuses or end date columns must be configured for ' || t_Widget.Title||' but not both');
    end if;

    if t_Widget.DisplayType <> gc_Gauge then
      t_Group2 := api.pkg_ColumnQuery.Value(a_WidgetId, 'Group2Expression');
    end if;

    t_Columns := 'j.JobId';
    if t_StartStatus is not null and t_EndStatus is not null then
      t_StatusChangeJoinClause :=
          '  join extension.jobstatuschange jsc' || gc_CRLF ||
          '    on jsc.JobId = j.JobId' || gc_CRLF ||
          '    and jsc.StatusName in (' || FormatStatusList(t_EndStatus) || ')' || gc_CRLF;
      t_StatusChangeGroupByClause := 'group by j.JobId';
      if t_Group1 is not null then
        t_StatusChangeGroupByClause := t_StatusChangeGroupByClause || ', '||t_Group1;
      end if;
      if t_Group2 is not null then
        t_StatusChangeGroupByClause := t_StatusChangeGroupByClause || ', '||t_Group2;
      end if;
      t_StatusChangeGroupByClause := t_StatusChangeGroupByClause || gc_CRLF ||
          'having max(jsc.effectivestartdate) >= trunc(:StartDate)' || gc_CRLF ||
          '  and max(jsc.effectivestartdate) < trunc(:EndDate) + 1' || gc_CRLF;
      t_Columns := t_Columns || ', ' || gc_CRLF ||
          '  (select min(jsc2.effectivestartdate)' || gc_CRLF ||
          'from extension.jobstatuschange jsc2' || gc_CRLF ||
          'where jsc2.statusname IN (' || FormatStatusList(t_StartStatus) || ')' || gc_CRLF ||
          '  and jsc2.jobid = j.jobid' || gc_CRLF ||
          ') StartDate,' || gc_CRLF ||
          'max(jsc.effectivestartdate) EndDate';
       t_WhereClause :=
           '  and exists (' || gc_CRLF ||
           '    select *' || gc_CRLF ||
           '    from extension.jobstatuschange jsc2' || gc_CRLF ||
           '    where jobid = jsc.jobid' || gc_CRLF ||
           '      and jsc2.statusname in (' || FormatStatusList(t_StartStatus) || ')' || gc_CRLF ||
           '      and jsc2.effectivestartdate < jsc.effectivestartdate)' || gc_CRLF;
    elsif t_StartDateDetail is not null and t_EndDateDetail is not null then
      t_Columns := t_Columns || ', ' || gc_CRLF ||
          t_StartDateDetail || ' StartDate,' || gc_CRLF ||
          t_EndDateDetail || ' EndDate';
      t_WhereClause :=
          ' and ' || t_EndDateDetail || ' >= trunc(:StartDate)' || gc_CRLF ||
          ' and ' || t_EndDateDetail || ' < trunc(:EndDate) + 1' || gc_CRLF ||
          ' and ' || t_StartDateDetail || ' is not null' || gc_CRLF;
    else
      api.pkg_Errors.RaiseError(-20000,
          'Either start/end statuses or start/end date columns must be configured for ' || t_Widget.Title||' but not both');
    end if;

    if t_Group1 is not null then
      t_Columns := t_Columns || ',' || gc_CRLF || t_Group1;
      t_WhereClause := t_WhereClause || 'and ' || t_Group1 || ' is not null' || gc_CRLF;
    end if;

    if t_Group2 is not null then
      t_Columns := t_Columns || ',' || gc_CRLF || t_Group2;
      t_WhereClause := t_WhereClause ||
          'and ' || t_Group2 || ' is not null' || gc_CRLF;
    end if;

    if t_Widget.view_ = gc_Compare then
      if t_Widget.ToDate - t_Widget.FromDate > 365 then
        api.pkg_Errors.RaiseError(-20000,
            'Compare Previous Years date range must be a year or less.');
      end if;
      t_PreviousYears := 2;
    end if;

    if api.pkg_ColumnQuery.Value(a_WidgetId, 'IncludeCancelled') <> 'Y' then
      t_ExcludeCancelledJoinClause :=
          '  join api.JobStatuses js' || gc_CRLF ||
          '    on js.JobTypeId = j.ObjectDefId' || gc_CRLF ||
          '    and js.StatusId = j.StatusId' || gc_CRLF ||
          '    and js.StatusType <> ''X''' || gc_CRLF;
    end if;

    t_ExcludeStatuses := api.pkg_columnquery.value(a_WidgetId,'ExcludeStatus');

     t_Sql :=
      'select ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      '  from extensioncorral.Jobs j' || gc_CRLF ||
      t_StatusChangeJoinClause ||
      t_ExcludeJoinClause ||
      t_ExcludeCancelledJoinClause ||
      pkg_DashboardUtils.FilterJoinClause(t_Widget, 'j.JobId', true) ||
      '  where j.ObjectDefId = :ObjectDefId' || gc_CRLF ||
      t_WhereClause ||
      t_StatusChangeGroupByClause;

    for i in 0..t_PreviousYears loop
      t_FromDate := add_months(t_Widget.FromDate, i * -12);
      t_ToDate := add_months(t_Widget.ToDate, i * -12);

      execute immediate t_SQL
      bulk collect into t_Jobs, t_StartDates, t_EndDates, t_GroupData
      using t_JobTypeId, t_FromDate, t_ToDate;

      /* Calculate the days to complete for each job */
      t_Data.delete;
      for j in 1..t_Jobs.count loop
        if t_WorkingOrCalendarDays = 'Working Days' then
          t_DaysToComplete := extension.pkg_utils.WorkingDaysBetween(t_StartDates(j), t_EndDates(j));
        else
          t_DaysToComplete := trunc(t_EndDates(j)) - trunc(t_StartDates(j));
        end if;

        /* Exclude any time periods in an excluded status */
        for s in c_ExcludeStatuses(t_Jobs(j), trunc(t_StartDates(j)), trunc(t_EndDates(j))) loop
          t_StartDate := trunc(greatest(s.EffectiveStartDate, t_StartDates(j)));
          t_EndDate := trunc(least(s.EffectiveEndDate, t_EndDates(j)));
          /* If the current status occurred immediately after the previous one, then ensure
           that the start date is not double counted */
          if t_StartDate = t_PrevEndDate then
            t_StartDate := t_StartDate + 1;
          end if;

          if t_WorkingOrCalendarDays = 'Working Days' then
            t_DaysToComplete := t_DaysToComplete - extension.pkg_utils.WorkingDaysBetween(t_StartDate, t_EndDate);
          else
            t_DaysToComplete := t_DaysToComplete - (trunc(t_EndDate) - trunc(t_StartDate));
          end if;
          t_PrevEndDate := t_EndDate;
        end loop;

        t_Data(j) := t_DaysToComplete;
      end loop;
       Pkg_Dashboardutils.Append(t_AllData,t_Data);
       Pkg_Dashboardutils.Append(t_AllDates,t_EndDates);
       Pkg_Dashboardutils.Append(t_AllGroupData,t_GroupData);
    end loop;

    -- creating Timeline graph
    if t_Widget.DisplayType = gc_Timeline and t_Widget.view_ in (gc_Detailed, gc_Compare) then
      pkg_DashboardUtils.GroupByTimeline(t_Widget, t_AllDates, t_AllGroupData, gc_Average,t_AllData);
    elsif t_Widget.DisplayType = gc_Timeline then
      t_AllGroupData.delete;
      pkg_DashboardUtils.GroupByTimeline(t_Widget, t_AllDates, t_AllGroupData, gc_Average,t_AllData);
    -- creating Bar graph
    elsif t_Widget.DisplayType = gc_BarGraph then
      pkg_DashboardUtils.GroupByValues(t_Widget, t_AllGroupData,g_NullStringList,g_NullStringList,g_NullStringList, gc_Average, t_AllData);
    elsif t_Widget.DisplayType = gc_Gauge then
      pkg_DashboardUtils.GroupByValues(t_Widget, t_AllGroupData,g_NullStringList,g_NullStringList,g_NullStringList, gc_Average, t_AllData);
      pkg_DashboardWidgetUtils.SetGaugeValues(t_Widget, t_Widget.data(1).y(1));
    end if;
    t_Dates.delete;
    t_Data.delete;
    t_GroupData.delete;
    t_AllDates.delete;
    t_AllData.delete;
    t_AllGroupData.delete;

    return pkg_DashboardWidgetUtils.ToJSON(t_Widget);

end DaysToCompleteSummary;
end pkg_Dashboard;

/

