create or replace package pkg_SendMail_old as

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *   Routines to support sending email messages.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  /*---------------------------------------------------------------------------
   * Send()
   *   Sends an email message.
   *-------------------------------------------------------------------------*/
  function Send (
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_Subject                           varchar2,
    a_Body                              varchar2,
    a_MimeType                          varchar2 default 'text/plain',
    a_Priority                          pls_integer default null,
    a_CcAddress                         varchar2 default null,
    a_BccAddress                        varchar2 default null,
    a_Message                           varchar2 default null,
    a_RelatedObjectID                   number default null
  ) return udt_Id;

  /*---------------------------------------------------------------------------
   * SendWithAttachment()
   *   Sends an email message with a binary attachment.
   *-------------------------------------------------------------------------*/
  function SendWithAttachment (
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_Subject                           varchar2,
    a_Body                              varchar2,
    a_Attachment                        blob,
    a_MimeType                          varchar2 default 'multipart/mixed',
    a_Priority                          pls_integer default null,
    a_CcAddress                         varchar2 default null,
    a_BccAddress                        varchar2 default null,
    a_AttachMimeType                    varchar2 default 'application/octet',
    a_AttachInLine                      boolean default false,
    a_AttachFileName                    varchar2 default null,
    a_Message                           varchar2 default null,
    a_RelatedObjectId                   number default null
  ) return udt_Id;

  /*---------------------------------------------------------------------------
   * SendWithAttachment()
   *   Sends an email message with a text attachment.
   *-------------------------------------------------------------------------*/
  function SendWithAttachment (
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_Subject                           varchar2,
    a_Body                              varchar2,
    a_Attachment                        clob,
    a_MimeType                          varchar2 default 'multipart/mixed',
    a_Priority                          pls_integer default null,
    a_CcAddress                         varchar2 default null,
    a_BccAddress                        varchar2 default null,
    a_AttachMimeType                    varchar2 default 'text/plain',
    a_AttachInLine                      boolean default false,
    a_AttachFileName                    varchar2 default null,
    a_Message                           varchar2 default null,
    a_RelatedObjectId                   number default null
  ) return udt_Id;

  /*---------------------------------------------------------------------------
   * BeginMail()
   *  Start an email message.  Subsequent calls need to be made to WriteText(),
   * AttachText(), or AttachBase64().
   *-------------------------------------------------------------------------*/
  function BeginMail (
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_Subject                           varchar2,
    a_MimeType                          varchar2,
    a_Priority                          pls_integer,
    a_CcAddress                         varchar2,
    a_BccAddress                        varchar2,
    a_Message                           varchar default null,
    a_RelatedObjectId                   varchar default null
  ) return udt_Id;

  /*---------------------------------------------------------------------------
   * EndMail()
   *  Uses a dbms job to call the finalize email procedure.
   *-------------------------------------------------------------------------*/
  procedure EndMail;

  /*---------------------------------------------------------------------------
   * WriteText()
   *  Write text to the body of the email message.
   *-------------------------------------------------------------------------*/
  procedure WriteText (
    a_Data                              varchar2
  );

  /*---------------------------------------------------------------------------
   * AttachText()
   *  Attach a text attachment to the email message.
   *-------------------------------------------------------------------------*/
  procedure AttachText (
    a_Data                              varchar2,
    a_MimeType                          varchar2 default 'text/plain',
    a_InLine                            boolean default true,
    a_FileName                          varchar2 default null,
    a_Last                              boolean default false
  );

  /*---------------------------------------------------------------------------
   * AttachBase64()
   *  Attach a binary attachment to the email message.
   *-------------------------------------------------------------------------*/
  procedure AttachBase64 (
    a_Blob                              blob,
    a_MimeType                          varchar2 default 'application/octet',
    a_InLine                            boolean default true,
    a_FileName                          varchar2 default null,
    a_Last                              boolean default false
  );

  /*---------------------------------------------------------------------------
   * SendPosseEmail()
   *  Procedure called from Posse.
   *-------------------------------------------------------------------------*/
  procedure SendPosseEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_CcAddressColName                  varchar2,
    a_BccAddressColName                 varchar2,
    a_SubjectColName                    varchar2,
    a_BodyColName                       varchar2,
    a_DocumentIdColName                 varchar2,
    a_DocumentEndPointName              varchar2,
    a_BodyIsHtml                        varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * SendEmail()
   *-------------------------------------------------------------------------*/
  procedure SendEmail (
    a_ObjectId                          udt_Id,
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_CcAddress                         varchar2,
    a_BccAddress                        varchar2,
    a_Subject                           varchar2,
    a_Body                              varchar2,
    a_DocumentId                        varchar2,
    a_DocumentEndPointName              varchar2,
    a_BodyIsHtml                        varchar2 default 'N',
    a_AttachAllDocs                     varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * FinalizeEmail()
   *  Sends out the emails from the emails_t table
   *-------------------------------------------------------------------------*/
  procedure FinalizeEmail;

end pkg_SendMail_old;

 
/

grant execute
on pkg_sendmail_old
to abc;

create or replace package body pkg_SendMail_old as

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  type udt_CharList is table of varchar2(1) index by binary_integer;
  type udt_StringIndexedList is table of boolean index by varchar2(260);

  /*---------------------------------------------------------------------------
   * Globals Constants
   *-------------------------------------------------------------------------*/
  gc_Newline constant char(2) := chr(13)||chr(10);
  MAILER_ID constant varchar2(256) := 'Mailer by Oracle UTL_SMTP';

  -- A unique string that demarcates boundaries of parts in a multi-part email
  -- The string should not appear inside the body of any part of the email.
  -- Customize this if needed or generate this randomly dynamically.
  BOUNDARY constant varchar2(256) := '-----7D81B75CCC90D2974F7A1CBD';
  FIRST_BOUNDARY constant varchar2(256) := '--' || BOUNDARY || gc_NewLine;
  LAST_BOUNDARY constant varchar2(256) := '--' || BOUNDARY || '--' || gc_NewLine;

  MAX_BASE64_ENCODED_LINE_WIDTH constant pls_integer := 76;
  MAX_BASE64_LINE_WIDTH constant pls_integer := MAX_BASE64_ENCODED_LINE_WIDTH / 4 * 3;
  MAX_BASE64_LINES constant pls_integer := floor(30000 / (MAX_BASE64_ENCODED_LINE_WIDTH+2));
  LOB_MAX_PIECE_SIZE constant pls_integer := MAX_BASE64_LINE_WIDTH * MAX_BASE64_LINES;

  -- A MIME type that denotes multi-part email (MIME) messages.
  MULTIPART_MIME_TYPE constant varchar2(256) := 'multipart/mixed; boundary="'||
                                                  BOUNDARY || '"';

  /*---------------------------------------------------------------------------
   * Globals
   *-------------------------------------------------------------------------*/
  g_Body clob;
  g_Map udt_CharList;

  /*---------------------------------------------------------------------------
   * WriteBoundary()
   *-------------------------------------------------------------------------*/
  procedure WriteBoundary (
    a_Last                              boolean default false
  ) as
  begin
    if a_Last then
      WriteText(LAST_BOUNDARY);
    else
      WriteText(FIRST_BOUNDARY);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * BeginAttachment()
   *-------------------------------------------------------------------------*/
  procedure BeginAttachment (
    a_MimeType                          varchar2 default 'text/plain',
    a_InLine                            boolean default true,
    a_FileName                          varchar2 default null,
    a_TransferEnc                       varchar2 default null
  ) is
  begin
    WriteBoundary();
    WriteText('Content-Type: '||a_MimeType||gc_NewLine);

    if a_FileName is not null then
      if a_InLine then
        WriteText('Content-Disposition: inline; filename="'||a_FileName||'"'||
            gc_NewLine);
      else
        WriteText('Content-Disposition: attachment; filename="'||
            a_Filename||'"'||gc_NewLine);
      end if;
    end if;

    if a_TransferEnc is not null then
      WriteText('Content-Transfer-Encoding: '||a_TransferEnc||gc_NewLine);
    end if;

    WriteText(gc_NewLine);
  end;

  /*---------------------------------------------------------------------------
   * EndAttachment()
   *-------------------------------------------------------------------------*/
  procedure EndAttachment (
    a_Last                              boolean default false
  ) is
  begin
    WriteText(gc_NewLine);
    if a_Last then
      WriteBoundary(a_Last);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * BeginMail() - PUBLIC
   *-------------------------------------------------------------------------*/
  function BeginMail (
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_Subject                           varchar2,
    a_MimeType                          varchar2,
    a_Priority                          pls_integer,
    a_CcAddress                         varchar2,
    a_BccAddress                        varchar2,
    a_Message                           varchar default null,
    a_RelatedObjectId                   varchar default null
  ) return udt_Id is
    t_ToAddress                         varchar2(32767);
    t_CcAddress                         varchar2(32767);
    t_BccAddress                        varchar2(32767);
    t_EmailId                           udt_Id;
    t_DateString                        varchar2(100);
    t_MimeType                          varchar2(500);
    t_Addresses                         udt_StringList;
    t_Recipients                        varchar2(32767);
    t_CCRecipients                      varchar2(32767);
    t_BccRecipients                     varchar2(32767);
  begin
    /* We must have a from and to address */
    if a_FromAddress is null then
      api.pkg_errors.RaiseError(-20000, 'You must supply a "From" email address.');
    end if;

    if a_ToAddress is null and a_CcAddress is null and a_BccAddress is null then
      api.pkg_errors.RaiseError(-20000, 'You must provide a recipient (e.g. "To", "Cc", "Bcc") email address.');
    end if;

    /* Determine all the recipients from the To, CC, and Bcc parameters */
    t_ToAddress := replace(a_ToAddress, ' ', ',');
    t_ToAddress := replace(t_ToAddress, ';', ',');
    t_CcAddress := replace(a_CcAddress, ' ', ',');
    t_CcAddress := replace(t_CcAddress, ';', ',');
    t_BccAddress := replace(a_BccAddress, ' ', ',');
    t_BccAddress := replace(t_BccAddress, ';', ',');

    t_Addresses := pkg_Utils.Split(t_ToAddress, ',');
    for i in 1..t_Addresses.count loop
      pkg_Utils.AddSection(t_Recipients, t_Addresses(i), ',');
    end loop;

    t_Addresses := pkg_Utils.Split(t_CcAddress, ',');
    for i in 1..t_Addresses.count loop
      pkg_Utils.AddSection(t_CCRecipients, t_Addresses(i), ',');
    end loop;

    t_Addresses := pkg_Utils.Split(t_BccAddress, ',');
    for i in 1..t_Addresses.count loop
      pkg_Utils.AddSection(t_BccRecipients, t_Addresses(i), ',');
    end loop;

    t_emailid := api.pkg_objectupdate.new(api.pkg_configquery.objectdefidforname('o_Email'));

    insert into Emails_old (
      EmailID,
      FromAddress,
      ToAddress,
      Subject,
      Body,
      CCAddress,
      BCCAddress,
      Message,
      RelatedObjectID,
      CreatedDate
    ) values (
      t_EmailID,
      a_FromAddress,
      t_Recipients,
      a_Subject,
      empty_clob(),
      t_CCRecipients,
      t_BccRecipients,
      a_Message,
      a_RelatedObjectID,
      sysdate
    ) returning body into g_Body;

    /* Format the date and time to the GMT time zone */
    execute immediate 'alter session set time_zone=''GMT''';
    execute immediate 'select to_char(current_date, ''dd Mon yy hh24:mi:ss'') '||
        '|| '' GMT'' from dual' into t_DateString;
    execute immediate 'alter session set time_zone=dbtimezone';

    /* Create the email header */
    WriteText('Date: '||t_DateString||gc_Newline);
    WriteText('From: '||a_FromAddress||gc_Newline);
    WriteText('Reply-To: '||gc_Newline);   -- Reply-To address is always empty.
    WriteText('To: '||a_ToAddress||gc_Newline);
    if a_CcAddress is not null then
      WriteText('Cc: '||a_CcAddress||gc_Newline);
    end if;
    WriteText('Subject: '||a_Subject||gc_Newline);
    WriteText('X-Mailer: '||MAILER_ID||gc_Newline);

    /* Set "Content-Type" MIME header */
    if lower(a_MimeType) like 'multipart/mixed%' then
      t_MimeType := MULTIPART_MIME_TYPE;
    else
      t_MimeType := a_MimeType;
    end if;
    WriteText('Content-Type: '||t_MimeType||gc_Newline);

    /* Set priority:
       High      Normal       Low
       1     2     3     4     5 */
    if (a_Priority is not null) then
      WriteText('X-Priority: ' || a_Priority);
    end if;

    /* Send an empty line to denote the end of the header and the beginning of
     the message body. */
    WriteText(gc_Newline);

    if (lower(a_MimeType) like 'multipart/mixed%') then
      WriteText('This is a multi-part message in MIME format.'||gc_NewLine);
    end if;

    return t_EmailId;
  end;

  /*---------------------------------------------------------------------------
   * EndMail() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure EndMail is
    jobno                               binary_integer;
  begin
    sys.dbms_job.submit(jobno, 'extension.pkg_SendMail_Old.FinalizeEmail();', sysdate,
        null);
  end;

  /*---------------------------------------------------------------------------
   * WriteText() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure WriteText (
    a_Data                              varchar2
  ) is
  begin
    if a_Data is not null then
      dbms_lob.writeappend(g_Body, length(a_Data), a_Data);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * AttachText() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure AttachText (
    a_Data                              varchar2,
    a_MimeType                          varchar2 default 'text/plain',
    a_InLine                            boolean default true,
    a_FileName                          varchar2 default null,
    a_Last                              boolean default false
  ) is
  begin
    BeginAttachment(a_MimeType, a_InLine, a_FileName);
    WriteText(a_Data);
    EndAttachment(a_Last);
  end;

  /*---------------------------------------------------------------------------
   * AttachBase64() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure AttachBase64 (
    a_Blob                              blob,
    a_MimeType                          varchar2 default 'application/octet',
    a_InLine                            boolean default true,
    a_FileName                          varchar2 default null,
    a_Last                              boolean default false
  ) is
    i   pls_integer;
    j   pls_integer;
    len pls_integer;
    rawlen pls_integer;
    amount pls_integer;
    t_Data raw(32767);
    t_LineData raw(32767);
    t_Buffer varchar2(32767);
  begin
    BeginAttachment(a_MimeType, a_InLine, a_FileName, 'base64');

    -- Split the Base64-encoded attachment into multiple lines
    i   := 1;
    len := dbms_lob.getlength(a_Blob);
    while (i <= len) loop
      if (i + LOB_MAX_PIECE_SIZE <= len+1) then
        amount := LOB_MAX_PIECE_SIZE;
        dbms_lob.read(a_Blob, amount, i, t_Data);
      else
        amount := len - i + 1;
        dbms_lob.read(a_Blob, amount, i, t_Data);
      end if;

      j := 1;
      rawlen := utl_raw.length(t_Data);
      t_Buffer := '';
      while (j <= rawlen) loop
        if (j + MAX_BASE64_LINE_WIDTH <= rawlen+1) then
          t_LineData := utl_raw.substr(t_Data, j, MAX_BASE64_LINE_WIDTH);
        else
          t_LineData := utl_raw.substr(t_Data, j);
        end if;
        t_Buffer := t_Buffer ||
            utl_raw.cast_to_varchar2(utl_encode.base64_encode(t_LineData)) ||
            gc_NewLine;
        j := j + MAX_BASE64_LINE_WIDTH;
      end loop;
      WriteText(t_Buffer);

      i := i + LOB_MAX_PIECE_SIZE;
    end loop;

    EndAttachment(a_Last);
  end;

  /*---------------------------------------------------------------------------
   * Send()
   *   Sends an email message.
   *-------------------------------------------------------------------------*/
  function Send (
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_Subject                           varchar2,
    a_Body                              varchar2,
    a_MimeType                          varchar2 default 'text/plain',
    a_Priority                          pls_integer default null,
    a_CcAddress                         varchar2 default null,
    a_BccAddress                        varchar2 default null,
    a_Message                           varchar2 default null,
    a_RelatedObjectID                   number default null
  ) return udt_Id is
    t_EmailId                           udt_Id;
    t_IsProduction                      varchar2(1);
    t_ToAddresses                       varchar2(4000);
  begin

/*Check to see if the system is production or if it is development*/
  select s.IsProduction, s.DevelopmentEmailAddresses
    into t_IsProduction, t_ToAddresses
    from query.o_SystemSettings s;

--if it is production
  if t_IsProduction = 'Y'
    then --send the email to what was passed in
      t_EmailId := BeginMail(a_FromAddress, a_ToAddress, a_Subject, a_MimeType,
          a_Priority, a_CcAddress, a_BccAddress, a_Message, a_RelatedObjectID);
      WriteText(a_Body);
      EndMail();
    else --otherwise send it to the addresses off of the system settings
      t_EmailId := BeginMail(a_FromAddress, t_ToAddresses, a_Subject, a_MimeType,
          a_Priority, a_CcAddress, a_BccAddress, a_Message, a_RelatedObjectID);
      WriteText(a_Body);
      EndMail();
    end if;
    return t_EmailId;
  end;

  /*---------------------------------------------------------------------------
   * SendWithAttachment()
   *   Sends an email message with a binary attachment.
   *-------------------------------------------------------------------------*/
  function SendWithAttachment (
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_Subject                           varchar2,
    a_Body                              varchar2,
    a_Attachment                        blob,
    a_MimeType                          varchar2 default 'multipart/mixed',
    a_Priority                          pls_integer default null,
    a_CcAddress                         varchar2 default null,
    a_BccAddress                        varchar2 default null,
    a_AttachMimeType                    varchar2 default 'application/octet',
    a_AttachInLine                      boolean default false,
    a_AttachFileName                    varchar2 default null,
    a_Message                           varchar2 default null,
    a_RelatedObjectId                   number default null
  ) return udt_Id is
    t_EmailId                           udt_Id;
  begin
    t_EmailId := BeginMail(a_FromAddress, a_ToAddress, a_Subject, a_MimeType,
        a_Priority, a_CcAddress, a_BccAddress, a_Message, a_RelatedObjectId);
    AttachText(a_Body);
    AttachBase64(a_Attachment, a_AttachMimeType, a_AttachInLine,
        a_AttachFileName, true);
    EndMail();
    return t_EmailId;
  end;

  /*---------------------------------------------------------------------------
   * SendWithAttachment()
   *   Sends an email message with a text attachment.
   *-------------------------------------------------------------------------*/
  function SendWithAttachment (
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_Subject                           varchar2,
    a_Body                              varchar2,
    a_Attachment                        clob,
    a_MimeType                          varchar2 default 'multipart/mixed',
    a_Priority                          pls_integer default null,
    a_CcAddress                         varchar2 default null,
    a_BccAddress                        varchar2 default null,
    a_AttachMimeType                    varchar2 default 'text/plain',
    a_AttachInLine                      boolean default false,
    a_AttachFileName                    varchar2 default null,
    a_Message                           varchar2 default null,
    a_RelatedObjectId                   number default null
  ) return udt_Id is
    t_EmailId                           udt_Id;
  begin
    t_EmailId := BeginMail(a_FromAddress, a_ToAddress, a_Subject, a_MimeType,
        a_Priority, a_CcAddress, a_BccAddress, a_Message, a_RelatedObjectId);
    AttachText(a_Body);
    AttachText(a_Attachment, a_AttachMimeType, a_AttachInLine,
        a_AttachFileName, true);
    EndMail();
    return t_EmailId;
  end;

  /*---------------------------------------------------------------------------
   * UniqueFileName()
   *-------------------------------------------------------------------------*/
  function UniqueFileName (
    a_FileName				varchar2,
    a_Extension				varchar2,
    a_Count				pls_integer,
    a_FileNameList			udt_StringIndexedList
  ) return varchar2 is
    t_FileName				varchar2(260);
  begin
    if length(a_Count) > 2 then
      api.pkg_Errors.RaiseError(-20000,
          'Count exceeds two digits in call to UniqueFileName.');
    end if;

    t_FileName := substr(a_FileName, 1, 246);

    if a_Count > 1 then
      t_FileName := t_FileName || '_' || to_char(a_Count);
    end if;

    if a_Extension is not null then
      if instr(t_FileName, '.'||a_Extension, -1) = 0 then
        t_FileName := t_FileName || '.' || a_Extension;
      end if;
    end if;

    if a_FileNameList.exists(t_FileName) then
      t_FileName := UniqueFileName(a_FileName, a_Extension, a_Count + 1,
          a_FileNameList);
    end if;

    return t_FileName;
  end UniqueFileName;

  /*---------------------------------------------------------------------------
   * GetPosseDocument()
   *-------------------------------------------------------------------------*/
  procedure GetPosseDocument (
    a_DocumentId                        udt_Id,
    a_Blob                              out nocopy blob,
    a_FileName                          out nocopy varchar2,
    a_MimeType                          out nocopy varchar2,
    a_FileNameList                      in out nocopy udt_StringIndexedList
  ) is
    t_Extension                         varchar2(256);
  begin

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

    begin
      select
        dt.value,
        dt.extension,
        nvl(replace(api.pkg_ColumnQuery.Value(
            d.DocumentId, 'EmailFileName'), ' ', '_'), 'Doc' || d.DocumentId)
      into
        a_Blob,
        t_Extension,
        a_FileName
      from
        api.Documents d,
        api.DocumentRevisions dt
      where d.DocumentId = a_DocumentId
        and dt.DocumentId = d.DocumentId
        and dt.RevisionNum = d.TailRevisionNum;
    exception
    when no_data_found then
      api.pkg_Errors.RaiseError(-20000, 'The DocumentId '||a_DocumentId ||
          'was not found');
    end;

    a_FileName := UniqueFileName(a_FileName, t_Extension, 1, a_FileNameList);
    a_FileNameList(a_FileName) := null;

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

    if lower(t_Extension) = 'doc' then
      a_MimeType := 'application/msword';
    elsif lower(t_Extension) = 'docx' then
      a_MimeType := 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    elsif lower(t_Extension) = 'xls' then
      a_MimeType := 'application/excel';
    elsif lower(t_Extension) = 'xlsx' then
      a_MimeType := 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    elsif lower(t_Extension) = 'pdf' then
      a_MimeType := 'application/pdf';
    elsif lower(t_Extension) = 'zip' then
      a_MimeType := 'application/zip';
    elsif lower(t_Extension) = 'bmp' then
      a_MimeType := 'image/bmp';
    elsif lower(t_Extension) in ('jpg','jpeg') then
      a_MimeType := 'image/jpg';
    elsif lower(t_Extension) = 'gif' then
      a_MimeType := 'image/gif';
    elsif lower(t_Extension) in ('htm','html') then
      a_MimeType := 'text/html';
    elsif lower(t_Extension) in ('txt','log','text') then
      a_MimeType := 'text/plain';
    end if;

  end;

  /*---------------------------------------------------------------------------
   * SendPosseEmail()
   *-------------------------------------------------------------------------*/
  procedure SendPosseEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_CcAddressColName                  varchar2,
    a_BccAddressColName                 varchar2,
    a_SubjectColName                    varchar2,
    a_BodyColName                       varchar2,
    a_DocumentIdColName                 varchar2,
    a_DocumentEndPointName              varchar2,
    a_BodyIsHtml                        varchar2 default 'N'
  ) is
    t_EmailId                           udt_Id;
    t_EmailBlob                         blob;
    t_FromAddress                       varchar2(4000);
    t_ToAddress                         varchar2(4000);
    t_CcAddress                         varchar2(4000);
    t_BccAddress                        varchar2(4000);
    t_Subject                           varchar2(4000);
    t_Body                              varchar2(4000);
    t_DocumentId                        udt_Id;
    t_FileName                          varchar2(500);
    t_MimeType                          varchar2(500);
    t_Documents                         udt_IdList;
    t_ObjectDefId                       udt_Id;
    t_EndPointId                        udt_Id;
    t_FileNameList                      udt_StringIndexedList;
    t_NoSourceError                     varchar2(4000);
    t_NoDestinationError                varchar2(4000);
    t_NoContentError                    varchar2(4000);
    t_RelatedObjectID                   number(9);
    t_Message                           varchar2(4000);
    t_AddressList                       udt_StringList;
    t_index                             number;
  begin
    t_FromAddress := api.pkg_ColumnQuery.Value(a_ObjectId, a_FromAddressColName);
    t_ToAddress := api.pkg_ColumnQuery.Value(a_ObjectId, a_ToAddressColName);
    t_CcAddress := api.pkg_ColumnQuery.Value(a_ObjectId, a_CcAddressColName);
    t_BccAddress := api.pkg_ColumnQuery.Value(a_ObjectId, a_BccAddressColName);
    t_Subject := api.pkg_ColumnQuery.Value(a_ObjectId, a_SubjectColName);
    t_Body := api.pkg_ColumnQuery.Value(a_ObjectId, a_BodyColName);
    t_DocumentId := api.pkg_ColumnQuery.NumericValue(a_ObjectId,
        a_DocumentIdColName);
    t_RelatedObjectID := a_ObjectId;
    t_Message := t_Body;

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

    -- Replace spaces and semicolons in an email address list with commas
    t_ToAddress := replace(t_ToAddress, ' ', ',');
    t_ToAddress := replace(t_ToAddress, ';', ',');
    t_CcAddress := replace(t_CcAddress, ' ', ',');
    t_CcAddress := replace(t_CcAddress, ';', ',');
    t_BccAddress := replace(t_BccAddress, ' ', ',');
    t_BccAddress := replace(t_BccAddress, ';', ',');


    -- Trigger Errors on Missing Data
    t_NoSourceError := 'No "From" address provided...';
    t_NoDestinationError := 'No "To" address provided...';
    t_NoContentError := 'No email content provided...';

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

    if t_FromAddress is null then
       api.pkg_errors.RaiseError(-20000, t_NoSourceError);
    end if;
    if t_ToAddress is null and t_CcAddress is null and t_BccAddress is null then
       api.pkg_errors.RaiseError(-20000, t_NoDestinationError);
    end if;
    if t_Body is null or t_Subject is null then
       api.pkg_errors.RaiseError(-20000, t_NoContentError);
    end if;
    -- End "Trigger Errors on Missing Data"

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

    -- Trigger Errors on Malformed Addresses
    -- To
    t_AddressList := extension.pkg_utils.split(t_ToAddress, ',');
    for i in 1..t_AddressList.count loop
       if instr(t_AddressList(i), '@') = 0 or instr(t_AddressList(i), '.', instr(t_AddressList(i), '@')) = 0 then
          api.pkg_errors.RaiseError(-20000, '"To" address "' || t_AddressList(i) || '" does not appear to be a valid email address');
       end if;
    end loop;
    -- CC
    t_AddressList := extension.pkg_utils.split(t_CcAddress, ',');
    for i in 1..t_AddressList.count loop
       if instr(t_AddressList(i), '@') = 0 or instr(t_AddressList(i), '.', instr(t_AddressList(i), '@')) = 0 then
          api.pkg_errors.RaiseError(-20000, '"CC" address "' || t_AddressList(i) || '" does not appear to be a valid email address');
       end if;
    end loop;
    -- BCC
    t_AddressList := extension.pkg_utils.split(t_BccAddress, ',');
    for i in 1..t_AddressList.count loop
       if instr(t_AddressList(i), '@') = 0 or instr(t_AddressList(i), '.', instr(t_AddressList(i), '@')) = 0 then
          api.pkg_errors.RaiseError(-20000, '"BCC" address "' || t_AddressList(i) || '" does not appear to be a valid email address');
       end if;
    end loop;

    if instr(t_FromAddress, '@') = 0 or instr(t_FromAddress, '.', instr(t_FromAddress, '@')) = 0 then
       api.pkg_errors.RaiseError(-20000, '"' || t_FromAddress || '" does not appear to be a valid email address');
    end if;
    -- End "Trigger Errors on Malformed Addresses"

    if t_FromAddress is null or t_Body is null or (t_ToAddress is null and
        t_CcAddress is null and t_BccAddress is null) then
      return;
    end if;

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE || '.  t_DocumentId: ' || t_DocumentId);

    if t_DocumentId is not null then
      GetPosseDocument(t_DocumentId, t_EmailBlob, t_FileName, t_MimeType,
          t_FileNameList);
      t_EmailId := BeginMail(t_FromAddress, t_ToAddress, t_Subject,
          'multipart/mixed', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectId);
      if a_BodyIsHtml = 'Y' then
        AttachText(t_Body, 'text/html');
      else
        AttachText(t_Body, 'text/plain');
      end if;
      AttachBase64(t_EmailBlob, t_MimeType, false, t_FileName, true);
      EndMail();

      --t_EmailId := SendWithAttachment(t_FromAddress, t_ToAddress, t_Subject,
      --    t_Body, t_EmailBlob, a_CcAddress => t_CCAddress,
      --    a_BccAddress => t_BccAddress, a_AttachMimeType => t_MimeType,
      --    a_AttachFileName => t_FileName, a_Message => t_Message, a_RelatedObjectId => t_RelatedObjectId);

      Insert Into EmailDocuments (
        EmailId,
        DocumentId
      ) Values (
        t_EmailId,
        t_DocumentId
      );

    elsif a_DocumentEndPointName is not null then

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

      select ObjectDefId
      into t_ObjectDefId
      from api.Objects
      where ObjectId = a_ObjectId;

--api.pkg_errors.raiseerror(-200000, 'TEST ERROR LINE #' || $$PLSQL_LINE || ' in extension.pkg_SendMail.  a_ObjectId, t_ObjectDefId' || a_ObjectId || ', ' || t_ObjectDefId);

      t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
          a_DocumentEndPointName);

      t_EmailId := BeginMail(t_FromAddress, t_ToAddress, t_Subject,
          'multipart/mixed', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectId);
      if a_BodyIsHtml = 'Y' then
        AttachText(t_Body, 'text/html');
      else
        AttachText(t_Body, 'text/plain');
      end if;

      if t_EndPointId is not null then
        t_Documents := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId,
            t_EndPointId);
        for i in 1..t_Documents.count loop
          GetPosseDocument(t_Documents(i), t_EmailBlob, t_FileName, t_MimeType, t_FileNameList);
          if i = t_Documents.count then
            AttachBase64(t_EmailBlob, t_MimeType, false, t_FileName, true);
          else
            AttachBase64(t_EmailBlob, t_MimeType, false, t_FileName, false);
          end if;
        end loop;
      end if;
      EndMail();
    else

      if a_BodyIsHtml = 'Y' then
        t_EmailId := Send(t_FromAddress, t_ToAddress, t_Subject, t_Body,
            'text/html', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectID);
      else
        t_EmailId := Send(t_FromAddress, t_ToAddress, t_Subject, t_Body,
            'text/plain', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectID);
      end if;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * SendEmail()
   *-------------------------------------------------------------------------*/
  procedure SendEmail (
    a_ObjectId                          udt_Id,
    a_FromAddress                       varchar2,
    a_ToAddress                         varchar2,
    a_CcAddress                         varchar2,
    a_BccAddress                        varchar2,
    a_Subject                           varchar2,
    a_Body                              varchar2,
    a_DocumentId                        varchar2,
    a_DocumentEndPointName              varchar2,
    a_BodyIsHtml                        varchar2 default 'N',
    a_AttachAllDocs                     varchar2 default 'N'
  ) is
    t_EmailId                           udt_Id;
    t_EmailBlob                         blob;
    t_FromAddress                       varchar2(4000);
    t_ToAddress                         varchar2(4000);
    t_CcAddress                         varchar2(4000);
    t_BccAddress                        varchar2(4000);
    t_Subject                           varchar2(4000);
    t_Body                              varchar2(4000);
    t_DocumentId                        udt_Id;
    t_FileName                          varchar2(500);
    t_MimeType                          varchar2(500);
    t_Documents                         udt_IdList;
    t_ObjectDefId                       udt_Id;
    t_EndPointId                        udt_Id;
    t_FileNameList                      udt_StringIndexedList;
    t_NoSourceError                     varchar2(4000);
    t_NoDestinationError                varchar2(4000);
    t_NoContentError                    varchar2(4000);
    t_RelatedObjectID                   number(9);
    t_Message                           varchar2(4000);
    t_AddressList                       udt_StringList;
    t_index                             number;
  begin
    t_FromAddress := a_FromAddress;
    t_ToAddress := a_ToAddress;
    t_CcAddress := a_CcAddress;
    t_BccAddress := a_BccAddress;
    t_Subject := a_Subject;
    t_Body := a_Body;
    t_DocumentId := a_DocumentId;
    t_RelatedObjectID := a_ObjectId;
    t_Message := t_Body;

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

    -- Replace spaces and semicolons in an email address list with commas
    t_ToAddress := replace(t_ToAddress, ' ', ',');
    t_ToAddress := replace(t_ToAddress, ';', ',');
    t_CcAddress := replace(t_CcAddress, ' ', ',');
    t_CcAddress := replace(t_CcAddress, ';', ',');
    t_BccAddress := replace(t_BccAddress, ' ', ',');
    t_BccAddress := replace(t_BccAddress, ';', ',');


    -- Trigger Errors on Missing Data
    t_NoSourceError := 'No "From" address provided...';
    t_NoDestinationError := 'No "To" address provided...';
    t_NoContentError := 'No email content provided...';

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

    if t_FromAddress is null then
       api.pkg_errors.RaiseError(-20000, t_NoSourceError);
    end if;
    if t_ToAddress is null and t_CcAddress is null and t_BccAddress is null then
       api.pkg_errors.RaiseError(-20000, t_NoDestinationError);
    end if;
    if t_Body is null or t_Subject is null then
       api.pkg_errors.RaiseError(-20000, t_NoContentError);
    end if;
    -- End "Trigger Errors on Missing Data"

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

    -- Trigger Errors on Malformed Addresses
    -- To
    t_AddressList := extension.pkg_utils.split(t_ToAddress, ',');
    for i in 1..t_AddressList.count loop
       if instr(t_AddressList(i), '@') = 0 or instr(t_AddressList(i), '.', instr(t_AddressList(i), '@')) = 0 then
          api.pkg_errors.RaiseError(-20000, '"To" address "' || t_AddressList(i) || '" does not appear to be a valid email address');
       end if;
    end loop;
    -- CC
    t_AddressList := extension.pkg_utils.split(t_CcAddress, ',');
    for i in 1..t_AddressList.count loop
       if instr(t_AddressList(i), '@') = 0 or instr(t_AddressList(i), '.', instr(t_AddressList(i), '@')) = 0 then
          api.pkg_errors.RaiseError(-20000, '"CC" address "' || t_AddressList(i) || '" does not appear to be a valid email address');
       end if;
    end loop;
    -- BCC
    t_AddressList := extension.pkg_utils.split(t_BccAddress, ',');
    for i in 1..t_AddressList.count loop
       if instr(t_AddressList(i), '@') = 0 or instr(t_AddressList(i), '.', instr(t_AddressList(i), '@')) = 0 then
          api.pkg_errors.RaiseError(-20000, '"BCC" address "' || t_AddressList(i) || '" does not appear to be a valid email address');
       end if;
    end loop;

    if instr(t_FromAddress, '@') = 0 or instr(t_FromAddress, '.', instr(t_FromAddress, '@')) = 0 then
       api.pkg_errors.RaiseError(-20000, '"' || t_FromAddress || '" does not appear to be a valid email address');
    end if;
    -- End "Trigger Errors on Malformed Addresses"

    if t_FromAddress is null or t_Body is null or (t_ToAddress is null and
        t_CcAddress is null and t_BccAddress is null) then
      return;
    end if;

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE || '.  t_DocumentId: ' || t_DocumentId);

    if a_AttachAllDocs = 'Y' then

      t_EmailId := BeginMail(t_FromAddress, t_ToAddress, t_Subject,
          'multipart/mixed', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectId);
      if a_BodyIsHtml = 'Y' then
        AttachText(t_Body, 'text/html');
      else
        AttachText(t_Body, 'text/plain');
      end if;
      
      -- Get all related document types
      select r.ToObjectId
        bulk collect into t_Documents
        from api.Relationships r
        join api.Documents d on d.DocumentId = r.ToObjectId
       where r.FromObjectId = a_ObjectId;
      
      for i in 1..t_Documents.count loop
        GetPosseDocument(t_Documents(i), t_EmailBlob, t_FileName, t_MimeType, t_FileNameList);

        if i = t_Documents.count then
          AttachBase64(t_EmailBlob, t_MimeType, false, t_FileName, true);
        else
          AttachBase64(t_EmailBlob, t_MimeType, false, t_FileName, false);
        end if;
      end loop;

      EndMail();
    elsif t_DocumentId is not null then
      GetPosseDocument(t_DocumentId, t_EmailBlob, t_FileName, t_MimeType,
          t_FileNameList);
      t_EmailId := BeginMail(t_FromAddress, t_ToAddress, t_Subject,
          'multipart/mixed', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectId);
      if a_BodyIsHtml = 'Y' then
        AttachText(t_Body, 'text/html');
      else
        AttachText(t_Body, 'text/plain');
      end if;
      AttachBase64(t_EmailBlob, t_MimeType, false, t_FileName, true);
      EndMail();

      --t_EmailId := SendWithAttachment(t_FromAddress, t_ToAddress, t_Subject,
      --    t_Body, t_EmailBlob, a_CcAddress => t_CCAddress,
      --    a_BccAddress => t_BccAddress, a_AttachMimeType => t_MimeType,
      --    a_AttachFileName => t_FileName, a_Message => t_Message, a_RelatedObjectId => t_RelatedObjectId);

      Insert Into EmailDocuments (
        EmailId,
        DocumentId
      ) Values (
        t_EmailId,
        t_DocumentId
      );

    elsif a_DocumentEndPointName is not null then

--api.pkg_errors.raiseerror(-20000, 'TEST ERROR at line#' || $$PLSQL_LINE);

      select ObjectDefId
      into t_ObjectDefId
      from api.Objects
      where ObjectId = a_ObjectId;

--api.pkg_errors.raiseerror(-200000, 'TEST ERROR LINE #' || $$PLSQL_LINE || ' in extension.pkg_SendMail.  a_ObjectId, t_ObjectDefId' || a_ObjectId || ', ' || t_ObjectDefId);

      t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_ObjectDefId,
          a_DocumentEndPointName);

      t_EmailId := BeginMail(t_FromAddress, t_ToAddress, t_Subject,
          'multipart/mixed', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectId);
      if a_BodyIsHtml = 'Y' then
        AttachText(t_Body, 'text/html');
      else
        AttachText(t_Body, 'text/plain');
      end if;

      if t_EndPointId is not null then
        t_Documents := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId,
            t_EndPointId);
        for i in 1..t_Documents.count loop
          GetPosseDocument(t_Documents(i), t_EmailBlob, t_FileName, t_MimeType, t_FileNameList);
          if i = t_Documents.count then
            AttachBase64(t_EmailBlob, t_MimeType, false, t_FileName, true);
          else
            AttachBase64(t_EmailBlob, t_MimeType, false, t_FileName, false);
          end if;
        end loop;
      end if;
      EndMail();
    else

      if a_BodyIsHtml = 'Y' then
        t_EmailId := Send(t_FromAddress, t_ToAddress, t_Subject, t_Body,
            'text/html', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectID);
      else
        t_EmailId := Send(t_FromAddress, t_ToAddress, t_Subject, t_Body,
            'text/plain', null, t_CcAddress, t_BccAddress, t_Message, t_RelatedObjectID);
      end if;
    end if;
  end;


  /*---------------------------------------------------------------------------
   * FinalizeEmail()
   *  Sends out the emails from the emails_t table
   *-------------------------------------------------------------------------*/
  procedure FinalizeEmail is
    t_Connection                        utl_smtp.connection;
    t_Buffer                            varchar2(32767);
    t_Length                            pls_integer;
    t_Index                             pls_integer;
    t_Amount                            pls_integer;
    t_Addresses                         udt_StringList;
    t_smptserver                        varchar2(100);
    t_ErrorMessage                      varchar2(4000);

    cursor c_SingleEmail(a_EmailId number) is
      select
        FromAddress,
        ToAddress,
        CcAddress,
        BccAddress,
        Body
      from Emails_old
      where EmailId = a_EmailId
        and SentDate is null
      for update;
    -- Resend any email that has a null SentDate and was created in the past "EMAILDAYSTOLIVE" days
    cursor c_Emails is
      select EmailId
      from Emails_old
      where SentDate is null and CreatedDate > sysdate - api.pkg_constantquery.value('EMAILDAYSTOLIVE')
      order by EmailId desc;
  Begin

    select Value
    into t_smptserver
    from api.constants
    where Name = 'SMTPSERVER';

    t_Connection := utl_smtp.open_connection(t_smptserver, 25);
    utl_smtp.helo(t_Connection, t_smptserver);
    for e in c_Emails loop
      for x in c_SingleEmail(e.EmailId) loop
        begin
          /* Add all the recipients */
          utl_smtp.mail(t_Connection, x.FromAddress);
          t_Addresses := pkg_Utils.Split(x.ToAddress, ',');
          for i in 1..t_Addresses.count loop
            utl_smtp.rcpt(t_Connection, t_Addresses(i));
          end loop;

          t_Addresses := pkg_Utils.Split(x.CcAddress, ',');
          for i in 1..t_Addresses.count loop
            utl_smtp.rcpt(t_Connection, t_Addresses(i));
          end loop;

          t_Addresses := pkg_Utils.Split(x.BccAddress, ',');
          for i in 1..t_Addresses.count loop
            utl_smtp.rcpt(t_Connection, t_Addresses(i));
          end loop;

          /* Write the body of the message */
          utl_smtp.open_data(t_Connection);
          t_Length := dbms_lob.getlength(x.Body);
          t_Index := 1;
          while t_Length >= t_Index loop
            t_Amount := least(t_Length - t_Index + 1, 32767);
            dbms_lob.read(x.Body, t_Amount, t_Index, t_Buffer);
            utl_smtp.write_data(t_Connection, t_Buffer);
            t_Index := t_Index + 32767;
          end loop;
          utl_smtp.close_data(t_Connection);

          /* If there are no SMTP errors, flag the message as sent */
          update emails_old
          set SentDate = sysdate
          where EmailId = e.EmailId;

        exception when others then
          utl_smtp.rset(t_Connection);
          t_ErrorMessage := sqlerrm;
          update emails_old t
          set t.errormessage = t_ErrorMessage
          where t.emailid = e.EmailId;
        end;
      end loop;
      commit;
    end loop;
    utl_smtp.quit(t_Connection);
  end;
end pkg_SendMail_old;

/

