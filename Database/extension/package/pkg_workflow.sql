create or replace package pkg_Workflow as

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_ObjectList is api.pkg_Definition.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * DeleteIncompleteProcesses() -- PUBLIC
   *   Delete any incomplete processes of the specified type(s) on this job
   * type.  The process types must be passed in as a comma separated list of
   * process type view names.
   *-------------------------------------------------------------------------*/
  procedure DeleteIncompleteProcesses (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_ProcessTypes                      varchar2
  );


  /*----------------------------------------------------------------------------
   * DefaultAssignedUsers()
   *   This procedure is configured on the Post verify of the process and the
   *   single parameter UserEndPointName will define a comma separated list of
   *   relationship end point names. The procedure will use the end point names
   *   to determine this corresponding Access Group and the default user.
   *--------------------------------------------------------------------------*/
  procedure DefaultAssignedUsers (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_UserEndPointNames                 varchar2
   );

   /*----------------------------------------------------------------------------
    * AddDefaultChecklistItems()
    *   Michael Schouten, May 23, 2006
    *
    *   Requires:
    *      - Default Checklist Item Object named o_DefaultChecklistItem
    *      - o_DefaultChecklistItem must contain detail "ChecklistItem"
    *      - o_DefaultChecklistItem must contain detail "Sequence"
    *      - A relationship between o_DefaultChecklistItem and a_ObjectDefID with endpoint 'Checklist'
    *
    *   Creates a set of o_ChecklistItem objects on the supplied object a_ObjectID,
    *     copying over the data from o_DefaultChecklistItem.  The particular default
    *     checklist items can be specified with the arguments a_IndexName and a_IndexValue
    *----------------------------------------------------------------------------*/
   procedure AddDefaultChecklistItems(
     a_ObjectId                         udt_Id,
     a_AsOfDate                         date,
     a_IndexName                        varchar2,
     a_IndexValue                       varchar2
   );


   /*----------------------------------------------------------------------------
   * AssignToAccessGroup()
   *   This procedure is designed to assign each member of a specified Access
   * Group to a specified process.  Requires the process id and access group
   * description as input parmameters.
   *--------------------------------------------------------------------------*/
   Procedure AssignToAccessGroup (
     a_ProcessId                         udt_Id,
     a_AsOfDate                          date,
     a_AccessGroup                       varchar2
  );


end pkg_Workflow;
 
/

grant execute
on pkg_workflow
to lms;

create or replace package body pkg_Workflow as

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  /*---------------------------------------------------------------------------
   * DeleteIncompleteProcesses() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure DeleteIncompleteProcesses (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_ProcessTypes                      varchar2
  ) is
    t_ProcessTypeList                   udt_StringList;
    t_ProcessTypeId                     udt_Id;

    cursor c_Processes(a_ProcessTypeId udt_Id) is
      select ProcessId
      from api.Processes p
      where DateCompleted is null
        and ProcessTypeId = a_ProcessTypeId
        and JobId =
          ( select JobId
            from api.Processes
            where ProcessId = a_ProcessId
          );
  begin
    t_ProcessTypeList := pkg_Utils.Split(a_ProcessTypes, ',');
    for i in 1..t_ProcessTypeList.count loop
      t_ProcessTypeId :=
          api.pkg_ConfigQuery.ObjectDefIdForName(t_ProcessTypeList(i));
      if t_ProcessTypeId is null then
        api.pkg_Errors.RaiseError(-20000, 'Process Type "'||
            t_ProcessTypeList(i)||'" does not exist');
      end if;

      for p in c_Processes(t_ProcessTypeId) loop
        api.pkg_ProcessUpdate.Remove(p.ProcessId);
      end loop;
    end loop;
  end;

  /*---------------------------------------------------------------------------
   * DefaultAssignedUsers()
   *-------------------------------------------------------------------------*/
  procedure DefaultAssignedUsers (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_UserEndPointNames                 varchar2
   ) is
    t_UserEndPointNames                 udt_StringList;
    t_AccessGroups                      udt_StringList;
    t_EndPointId                        udt_Id;
    t_DefaultUserObjectId               udt_Id;
    t_ProcessUserRelId                  udt_Id;
    t_AccessGroupsObjectId              udt_Id;
    t_Count                             number;
  begin
    /* Returns a comma separated list of endpointnames */
    t_UserEndPointNames := pkg_Utils.Split(a_UserEndPointNames,',');

    for j in 1..t_UserEndPointNames.count loop
      t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          api.pkg_ColumnQuery.NumericValue(a_ProcessId, 'ObjectDefId'),
          t_UserEndPointNames(j));

      select count(*)
      into t_Count
      from api.Relationships
      where FromObjectId = a_ProcessId
        and EndPointId = t_EndPointId;

      if t_Count = 0 then
        t_AccessGroups := pkg_Utils.Split(api.pkg_ColumnQuery.Value(a_ProcessId,
            t_UserEndPointNames(j)||'AccessGroup'), '<>');

        for i in 1..t_AccessGroups.count loop
          /* Retrieve the specific DefaultUserObjectId (Default User) of the
            access group*/

          t_AccessGroupsObjectId := api.pkg_SimpleSearch.ObjectByIndex(
              'o_PosseAccessGroups','AccessGroupName',t_AccessGroups(i));
          t_DefaultUserObjectId  := api.pkg_ColumnQuery.value(
              t_AccessGroupsObjectId,'DefaultUserObjectId');

          if t_DefaultUserObjectId is not null then
            t_ProcessUserRelId := api.pkg_relationshipupdate.new(t_EndPointId,
                a_Processid, t_DefaultUserObjectId);
          end if;
        end loop;
      end if;
    end loop;
  end;

  procedure AddDefaultChecklistItems(
     a_ObjectId                         udt_Id,
     a_AsOfDate                         date,
     a_IndexName                        varchar2,
     a_IndexValue                       varchar2
   ) is
     t_DefaultItems                     udt_ObjectList;
     t_ChecklistDefID                   udt_ID;
     t_ChecklistID                      udt_ID;
     t_RelID                            udt_ID;
   begin
     t_ChecklistDefID := api.pkg_configquery.ObjectDefIDForName('o_ChecklistItem');
     t_DefaultItems := api.pkg_simplesearch.ObjectsByIndex('o_DefaultChecklistItem', a_IndexName, a_IndexValue);
     for i in 1..t_DefaultItems.count loop
        t_ChecklistID := api.pkg_objectupdate.new(t_ChecklistDefID);
        api.pkg_columnupdate.SetValue(t_ChecklistID, 'ChecklistItem', api.pkg_columnquery.Value(t_DefaultItems(i), 'ChecklistItem'));
        api.pkg_columnupdate.SetValue(t_ChecklistID, 'Sequence', api.pkg_columnquery.Value(t_DefaultItems(i), 'Sequence'));
        t_RelID := extension.pkg_relationshipupdate.new( a_ObjectID, t_ChecklistID, 'Checklist');
     end loop;

   end AddDefaultChecklistItems;

  /*---------------------------------------------------------------------------
   * AssignToAccessGroup()
   *-------------------------------------------------------------------------*/
  procedure AssignToAccessGroup (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_AccessGroup                       varchar2
  ) is
    Cursor c1 Is
     select U.OracleLogonId
     from
       api.Accessgroups AG
       join api.Accessgroupusers AGU
           on AGU.AccessGroupId = AG.AccessGroupId
       join api.Users U
           on U.UserId = AGU.UserId
     where AG.Description = a_AccessGroup;
  begin
    for i in C1 loop
      api.Pkg_Processupdate.Assign(a_ProcessId, i.OracleLogonId);
    end Loop;
  end AssignToAccessGroup;

end pkg_Workflow;
/

