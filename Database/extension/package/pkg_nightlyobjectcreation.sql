create or replace package pkg_NightlyObjectCreation is

  -----------------------------------------------------------------------------
  -- pkg_NightlyObjectCreation
  --   Procedures that handle scheduled creation of objects.
  --   The Process Server was not used because of the varying conditions
  -- for creating the objects.  e.g. A job should be created 30 days after a
  -- given date unless some other process was created sooner; or expire on
  -- a certain date, but the expiry date could be delayed.
  -----------------------------------------------------------------------------

  -----------------------------------------------------------------------------
  -- CreateObjects()
  -----------------------------------------------------------------------------
  procedure CreateObjects (
    a_SourceObjectDefNames    varchar2 default null,
    a_TargetObjectDefName    varchar2 default null
  );

  -----------------------------------------------------------------------------
  -- TestParse()
  -----------------------------------------------------------------------------
  procedure TestParse (
    a_ObjectDefName                     varchar2 default null
  );

  -----------------------------------------------------------------------------
  -- AnyObjectIdForDef()
  -----------------------------------------------------------------------------
  function AnyObjectIdForDef (
    a_ObjectDefId			pls_integer
  ) return pls_integer;


end pkg_NightlyObjectCreation;

 
/
create or replace package body pkg_NightlyObjectCreation is

  -----------------------------------------------------------------------------
  -- Types
  -----------------------------------------------------------------------------
  type udt_DetailParts is record (
    ObjectDefTypeId                     pls_integer,
    DateColumn        varchar2(60),
    CheckBoxColumn      varchar2(60),
    ObjectDefName      varchar2(60),
    UpdateColumn      varchar2(60),
    EndPointName      varchar2(60),
    Outcome           varchar2(60)
  );

  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  c_DateFormat        varchar2(25) :=
      api.pkg_Definition.gc_DateFormat;

  -----------------------------------------------------------------------------
  -- RaiseMissingParmError() -- PRIVATE
  -----------------------------------------------------------------------------
  procedure RaiseMissingParmError (
    a_ObjectDefName      varchar2,
    a_Number        pls_integer
  ) is
  begin
    api.pkg_Errors.Clear;
    api.pkg_Errors.SetArgValue('ObjDef', a_ObjectDefName);
    api.pkg_Errors.SetArgValue('ParmNum', a_Number);
    api.pkg_Errors.RaiseError(-20000,
        '{ObjDef}.NightlyObjectCreationDetails is missing a parameter'
        || ' in position {ParmNum}.');
  end RaiseMissingParmError;

  -----------------------------------------------------------------------------
  -- GetDetailParts() -- PRIVATE
  --   Split the string into four parts and return a record.
  -----------------------------------------------------------------------------
  function GetDetailParts (
    a_ObjectDefName      varchar2,
    a_String        varchar2
  ) return udt_DetailParts is
    t_HasSixParms                       boolean;
    t_PartList        udt_StringList;
    t_Parts        udt_DetailParts;
  begin
    t_HasSixParms := instr(a_String, ',', 1, 5) > 0;
    t_PartList := toolbox.pkg_Parse.ListFromString(a_String, ',');

    if t_PartList.exists(1) then
      t_Parts.DateColumn := t_PartList(1);
    else
      RaiseMissingParmError(a_ObjectDefName, 1);
    end if;

    if t_PartList.exists(2) then
      t_Parts.CheckboxColumn := t_PartList(2);
    else
      RaiseMissingParmError(a_ObjectDefName, 2);
    end if;

    if t_PartList.exists(3) then
      t_Parts.ObjectDefName := t_PartList(3);
    else
      RaiseMissingParmError(a_ObjectDefName, 3);
    end if;

    -- get the type of the object def to be created
    begin
      select ObjectDefTypeId
      into t_Parts.ObjectDefTypeId
      from api.ObjectDefs
      where Name = t_Parts.ObjectDefName;
    exception
    when no_data_found then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('ObjDef', t_Parts.ObjectDefName);
      api.pkg_Errors.RaiseError(-20000, 'Invalid object def name "{ObjDef}".');
    end;

    -- the fourth item can be the DateColumn, CheckBoxColumn or null
    if t_PartList.exists(4) then
      t_Parts.UpdateColumn := t_PartList(4);
    end if;

    if t_Parts.UpdateColumn is not null and t_Parts.UpdateColumn not in
        (t_Parts.DateColumn, t_Parts.CheckBoxColumn) then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('ObjDef', a_ObjectDefName);
      api.pkg_Errors.SetArgValue('Parm', t_Parts.UpdateColumn);
      api.pkg_Errors.RaiseError(-20000,
          '{ObjDef}: Invalid fourth parameter: "{Parm}".'
          || ' It must be the same as one of the first two or null.');
    end if;

    -- if there are six parameters then 5 and 6 are EndPointName and Outcome
    if t_HasSixParms then
      if t_PartList.exists(5) then
        t_Parts.EndPointName := t_PartList(5);
      end if;
      if t_PartList.exists(6) then
        t_Parts.Outcome := t_PartList(6);
      end if;

    -- if five parameters, the fifth item is an Outcome for process types
    -- (to support initial implemenation) or EndPointName for other types
    elsif t_PartList.exists(5) then
      if t_Parts.ObjectDefTypeId = 3 then
        t_Parts.Outcome := t_PartList(5);
      else
        t_Parts.EndPointName := t_PartList(5);
      end if;
    end if;

    if t_Parts.Outcome is not null and t_Parts.ObjectDefTypeId <> 3 then
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('Outcome', t_Parts.Outcome);
      api.pkg_Errors.SetArgValue('ObjDef', t_Parts.ObjectDefName);
      api.pkg_Errors.RaiseError(-20000, 'Outcome of "{Outcome}" specified' ||
          ' for Object Def "{ObjDef}" which is not a Process Type.');
    end if;

    return t_Parts;
  end GetDetailParts;

  -----------------------------------------------------------------------------
  -- LogBatchError() -- PRIVATE
  --   Set the ErrorColumn on the Batch table.
  -----------------------------------------------------------------------------
  procedure LogBatchError (
    a_BatchId        pls_integer,
    a_Error        varchar2 default null
  ) is
    t_Error        varchar2(4000);
  begin
    t_Error := nvl(a_Error, sqlerrm);
    api.pkg_LogicalTransactionUpdate.ResetTransaction;
    rollback;

    update NightlyObjectCreationBatch set
      ErrorMessage = t_Error
    where BatchId = a_BatchId;
    commit;
  end LogBatchError;

  -----------------------------------------------------------------------------
  -- LogRow() -- PRIVATE
  --   Insert a log row in to the batch table.
  -----------------------------------------------------------------------------
  procedure LogRow (
    a_BatchId        pls_integer,
    a_SourceObjectId      pls_integer,
    a_TargetObjectDef      varchar2,
    a_CreatedObjectId      pls_integer,
    a_Error        varchar2 default null
  ) is
  begin
    insert into NightlyObjectCreationLog (
      BatchId,
      SourceObjectId,
      TargetObjectDef,
      CreatedObjectId,
      ErrorMessage
    ) values (
      a_BatchId,
      a_SourceObjectId,
      a_TargetObjectDef,
      a_CreatedObjectId,
      a_Error
    );
    commit;
  end LogRow;

  -----------------------------------------------------------------------------
  -- CreateObject() -- PRIVATE
  --   Create a new object according to the values in the a_Parts parameter.
  -----------------------------------------------------------------------------
  procedure CreateObject (
    a_BatchId        pls_integer,
    a_ObjectId        pls_integer,
    a_Parts        udt_DetailParts
  ) is
    t_JobId                             pls_integer;
    t_NewObjectId      pls_integer;
    t_RelId        pls_integer;
    t_Error        varchar2(4000);
  begin
    -- create a new object of the given type
    dbms_output.put_line('Def Type: ' || a_Parts.ObjectDefTypeId);
    if a_Parts.ObjectDefTypeId = 3 then
      t_JobId := nvl(abc.pkg_ABC_Common.JobIdForProcess(a_ObjectId),
          a_ObjectId);
      t_NewObjectId := api.pkg_ProcessUpdate.New(t_JobId,
          api.pkg_ConfigQuery.ObjectDefIdForName(a_Parts.ObjectDefName),
          'Created by pkg_NightlyObjectCreation', null, null, null);
    elsif a_Parts.ObjectDefTypeId = 2 then
      t_NewObjectId := api.pkg_JobUpdate.New(
          api.pkg_ConfigQuery.ObjectDefIdForName(a_Parts.ObjectDefName),
          'Created by pkg_NightlyObjectCreation', null);
      dbms_output.put_line('New Object: ' ||t_NewObjectId);  
    else
      api.pkg_Errors.RaiseError(-20000, 'Creation of object def type "'
          || a_Parts.ObjectDefTypeId || '" not handled.');
    end if;
    dbms_output.put_line('Update? ' ||a_Parts.UpdateColumn);  
    -- modify the UpdateColumn if so indicated
    if a_Parts.UpdateColumn is not null then
      if a_Parts.UpdateColumn = a_Parts.DateColumn then
        api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, a_Parts.UpdateColumn);
      dbms_output.put_line('Remove Value: ' || api.pkg_ColumnQuery.value(a_ObjectId, a_Parts.UpdateColumn));  
      elsif a_Parts.UpdateColumn = a_Parts.CheckBoxColumn then
        api.pkg_ColumnUpdate.SetValue(a_ObjectId, a_Parts.UpdateColumn, 'N');
      else
        api.pkg_Errors.Clear;
        api.pkg_Errors.SetArgValue('Parm', a_Parts.UpdateColumn);
        api.pkg_Errors.RaiseError(-20000,
            'Invalid NightlyObjectCreationDetails fourth parameter: "{Parm}".');
      end if;
    end if;
    dbms_output.put_line('EP: ' || a_Parts.EndPointName);
    -- relate the creating object to the new object if EndPointName specified
    if a_Parts.EndPointName is not null then
      t_RelId := extension.pkg_RelationshipUpdate.New(a_ObjectId,
          t_NewObjectId, a_Parts.EndPointName);
      dbms_output.put_line('New Rel Id: ' || t_RelId);
    end if;
    dbms_output.put_line('Outcome: ' || a_Parts.Outcome);
    -- complete the process if the Outcome is specified
    if a_Parts.Outcome is not null then
      api.pkg_ProcessUpdate.Complete(t_NewObjectId, a_Parts.Outcome);
    end if;
    dbms_output.put_line('Batch: ' || a_BatchId || ' From Object: ' || a_ObjectId || ' Def Name: ' || a_Parts.ObjectDefName || ' New Object Id: ' || t_NewObjectId);
    api.pkg_LogicalTransactionUpdate.EndTransaction;

    LogRow(a_BatchId, a_ObjectId, a_Parts.ObjectDefName, t_NewObjectId);
  exception
  when others then
    t_Error := sqlerrm;
    dbms_output.put_line('Err: ' || t_error);
    api.pkg_LogicalTransactionUpdate.ResetTransaction;
    rollback;

    LogRow(a_BatchId, a_ObjectId, a_Parts.ObjectDefName, null, t_Error);
  end CreateObject;

  -----------------------------------------------------------------------------
  -- CreateObjects()
  --   Used to create jobs, processes or objects based on data and metadata
  -- columns.  This procedure is intended to be scheduled to run nightly as
  -- a dbms_job, but it may also be run manually if necessary.
  --
  --   One or more BusinessAreaNames must be passed in which refer to an
  -- object of the o_AMS_BusinessArea ObjectDef.  This object will have a list
  -- of the source object def names that will be processed.  Each of the
  -- object defs listed must have the NightlyObjectCreationDetails expression
  -- column that is a semi-colon separated list of the following comma
  -- separated values:
  --     DateColumn - an indexed date column containing the creation date
  --     CheckBoxColumn - boolean indicating if the object should be created
  --     ObjectDefName - hard coded name of the object def to create
  --     UpdateColumn - DateColumn or CheckBoxColumn or null
  --     Optional - when 5 values the fifth is an Outcome for ProcessTypes
  --                or EndPointName for other types
  --     Outcome - when 6 values, 5th is EndPointName and 6th is Outcome
  -----------------------------------------------------------------------------
  procedure CreateObjects (
    a_SourceObjectDefNames    varchar2 default null,
    a_TargetObjectDefName    varchar2 default null
  ) is
    t_SystemSettingsId        pls_integer;
    t_BatchId        pls_integer;
    t_NightlyCreationObjectDefList  varchar2(4000);
    t_MaxLeadDays      pls_integer;
    t_SourceObjectDefNames    udt_StringList;
    t_SourceObjectDefId      pls_integer;
    t_ObjectId        pls_integer;
    t_DetailsString      varchar2(4000);
    t_DetailsList      udt_StringList;
    t_Parts        udt_DetailParts;
    t_ObjectList      udt_IdList;
    t_Count        pls_integer;
    t_Error        varchar2(4000);
    t_CreateObject      varchar2(4000);
  begin
    -- start a new batch
    select NightlyObjectCreationBatchId_s.nextval
    into t_BatchId
    from dual;

    insert into NightlyObjectCreationBatch (
      BatchId,
      BatchDate
    ) values (
      t_BatchId,
      sysdate
    );
    commit;
   
      /*Get the system settings object id to get Lead Days and Objectdef List values.*/
      select max(objectid) 
        into t_SystemSettingsId
        from query.o_SystemSettings;


      if t_SystemSettingsId is null then
        api.pkg_Errors.RaiseError(-20000, 'Invalid System Settings object');
      end if;

      -- get metadata values from the System Settings object
      t_NightlyCreationObjectDefList := api.pkg_ColumnQuery.Value(
          t_SystemSettingsId, 'NightlyCreationObjectDefList');
      t_MaxLeadDays := api.pkg_ColumnQuery.Value(t_SystemSettingsId,
          'NightlyCreationMaxLeadDays');

      -- run for the specific source object defs specified or default to
      -- the metadata values
      t_SourceObjectDefNames := toolbox.pkg_Parse.ListFromString(
          nvl(a_SourceObjectDefNames, t_NightlyCreationObjectDefList), ',');
          
      for s in 1..t_SourceObjectDefNames.count loop
        t_SourceObjectDefId := api.pkg_ConfigQuery.ObjectDefIdForName(
            t_SourceObjectDefNames(s));
            
       if t_SourceObjectDefId is null then
         api.pkg_errors.RaiseError(-20000,'Invalid object def:  '|| t_SourceObjectDefNames(s));
       end if;
        
        -- get any ObjectId for the ObjectDef to evaluate the class level column
        t_ObjectId := AnyObjectIdForDef(t_SourceObjectDefId);
        t_DetailsString := api.pkg_ColumnQuery.Value(t_ObjectId,
            'NightlyObjectCreationDetails'); 
        t_DetailsString := replace(replace(t_DetailsString, chr(13)), chr(10));

        -- go through each of the object defs to be created
        t_DetailsList := toolbox.pkg_Parse.ListFromString(t_DetailsString, ';');

        for d in 1..t_DetailsList.count loop
          t_Parts := GetDetailParts(t_SourceObjectDefNames(s),
              t_DetailsList(d));
       -- api.pkg_errors.RaiseError(-20000, 'Error: ' || t_SourceObjectDefNames(s) || ' ' ||   t_DetailsList(d));   
          -- check if we should be creating this object def
          if t_Parts.ObjectDefName = a_TargetObjectDefName
              or a_TargetObjectDefName is null then

            -- use the indexed date column to get an initial list of objects
            -- sysdate minus MaxLeadDays is used as the low date value for the
            -- search to avoid missing records
            pkg_debug.putline('Search for ' || t_SourceObjectDefNames(s)
                || ' using ' || t_Parts.DateColumn || ' mld: ' || t_MaxLeadDays);
                
            begin
              t_ObjectList := api.pkg_SimpleSearch.ObjectsByIndex(
                  t_SourceObjectDefNames(s), t_Parts.DateColumn,
                  to_char(trunc(sysdate) - t_MaxLeadDays, c_DateFormat),
                  to_char(trunc(sysdate) + 86399/86400,
                      c_DateFormat));
            exception
            when others then
              t_Error := sqlerrm || ' Searching for {ObjDef}.{ColDef}';
              api.pkg_Errors.Clear;
              api.pkg_Errors.SetArgValue('ObjDef', t_SourceObjectDefNames(s));
              api.pkg_Errors.SetArgValue('ColDef', t_Parts.DateColumn);
              api.pkg_Errors.RaiseError(-20000, t_Error);
              dbms_output.put_line('Error Message: ' || t_Error);
            end;
         
            --api.pkg_errors.RaiseError(-20000,          'Search result count: ' || t_ObjectList.count);
            pkg_debug.putline('Search result count: ' || t_ObjectList.count);
            for o in 1..t_ObjectList.count LOOP
              pkg_debug.putline('Evaluate CheckBox on: ' || t_ObjectList(o));
              t_CreateObject := api.pkg_ColumnQuery.Value(t_ObjectList(o),
                  t_Parts.CheckBoxColumn);
            --api.pkg_errors.RaiseError(-20000,'Evaluate CheckBox on: ' || t_CreateObject);
              if t_CreateObject = 'N' then
                null;
              elsif t_CreateObject = 'Y' then
                CreateObject(t_BatchId, t_ObjectList(o), t_Parts);
              else
                api.pkg_Errors.Clear;
                api.pkg_Errors.SetArgValue('ObjDef', t_SourceObjectDefNames(s));
                api.pkg_Errors.SetArgValue('ColDef', t_Parts.CheckBoxColumn);
                api.pkg_Errors.RaiseError(-20000,
                    '{ObjDef}.{ColDef} should return a Y or N value.');
              end if;
            end loop;
          end if;
        end loop;
      end loop;

    select count(*)
      into t_Count
      from NightlyObjectCreationLog
      where BatchId = t_BatchId
        and ErrorMessage is not null;

    if t_Count > 0 then
      LogBatchError(t_BatchId, to_char(t_Count)
          || ' items failed, see Log rows.');
    end if;
  exception
  when others then
    LogBatchError(t_BatchId);
  end CreateObjects;

  -----------------------------------------------------------------------------
  -- TestParse()
  --   Used to test the parsing of all NightlyObjectCreationDetails columns
  -- in the system.
  -----------------------------------------------------------------------------
  procedure TestParse (
    a_ObjectDefName                     varchar2 default null
  ) is
    t_DetailsList      udt_StringList;
    t_DetailsString   varchar2(4000);
    t_Parts            udt_DetailParts;

    cursor c_rows is
      select
        od.Name,
        api.pkg_ColumnQuery.Value(
            anyobjectidfordef(cd.ObjectDefId),
            'NightlyObjectCreationDetails') Value
      from
        api.ColumnDefs cd
        join api.ObjectDefs od
            on od.ObjectDefId = cd.ObjectDefId
      where cd.Name = 'NightlyObjectCreationDetails'
        and (od.Name = a_ObjectDefName or a_ObjectDefName is null);

  begin
    for r in c_Rows loop
      dbms_output.put_line('-- ' || r.Name || ' --');
      t_DetailsString := replace(replace(r.Value, chr(13)), chr(10));
      t_DetailsList := toolbox.pkg_Parse.ListFromString(t_DetailsString, ';');

      for d in 1..t_DetailsList.count loop
        t_Parts := GetDetailParts(r.Name, t_DetailsList(d));
        dbms_output.put_line('  DefTypeId: ' || t_Parts.ObjectDefTypeId);
        dbms_output.put_line('  DateColumn: ' || t_Parts.DateColumn);
        dbms_output.put_line('  CheckBoxColumn: ' || t_Parts.CheckBoxColumn);
        dbms_output.put_line('  ObjectDefName: ' || t_Parts.ObjectDefName);
        dbms_output.put_line('  UpdateColumn: ' || t_Parts.UpdateColumn);
        dbms_output.put_line('  EndPointName: ' || t_Parts.EndPointName);
        dbms_output.put_line('  Outcome: ' || t_Parts.Outcome);
        dbms_output.put_line(' ');
      end loop;
    end loop;
  end;
  
  -----------------------------------------------------------------------------
  -- AnyObjectIdForDef() -- PUBLIC
  --   Get any object id for the given object def.  Used to evaluate "Class"
  -- level column defs which will evaluate the same for any object of the class.
  -----------------------------------------------------------------------------
    function AnyObjectIdForDef (
      a_ObjectDefId			pls_integer
    ) return pls_integer is
      t_ObjectId				pls_integer;
    begin
      select ObjectId
      into t_ObjectId
      from
        api.ObjectDefs od
        join api.Objects o
            on o.ObjectDefTypeId = od.ObjectDefTypeId
            and o.ObjectDefId = od.ObjectDefId
      where od.ObjectDefId = a_ObjectDefId
        and rownum <= 1;

      return t_ObjectId;
    exception
    when no_data_found then
      return null;
    end AnyObjectIdForDef;  
  
end pkg_NightlyObjectCreation;
/
