create or replace package           pkg_proceduralsearch is
  subtype udt_id     is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * Procedure FindUserProcesses()
   *   Author:  Stanley Shum
   *   Created: 3/16/2006
   *   Purpose: This procedure will return a list of incompleted processes
   *            assigned to a current login user for the purpose of filling a
   *            To-Do-List. You have the option of either displaying current
   *            processes(i.e. the scheduledstartdate is null or it is less
   *            than the sysdate) or all processes.
   *-------------------------------------------------------------------------*/
  Procedure FindUserProcesses(
                a_CurrentOnly           varchar2,
                a_User                  varchar2,
                a_JobId                 number,
                a_ProcessType           varchar2,
                a_JobType               varchar2,
                a_RelatedObjectIds	 out nocopy api.udt_ObjectList
                );

  /*---------------------------------------------------------------------------
   * Procedure FindPermitProjects()
   *   Author:  Shane Gilbert
   *   Created: 10/22/2008
   *   Purpose: This procedure will return a list of Permit Projects filtering
   *   the Permit Projects that have been removed.
   *-------------------------------------------------------------------------*/
  Procedure FindPermitProjects(
       a_ObjectId                          udt_Id,
       a_EndPointId                        udt_Id,
       a_RelatedObjectIds       out nocopy api.udt_ObjectList);

  /*---------------------------------------------------------------------------
   * Procedure FindHRMPermit()
   *   Author:  Shane Gilbert
   *   Created: 10/22/2008
   *   Purpose: This procedure will return a list of HRM Permits
   *-------------------------------------------------------------------------*/
  Procedure FindHRMPermit (a_ObjectId udt_Id,
                           a_EndPointId udt_Id,
                           a_RelatedObjectIds	 out nocopy api.udt_ObjectList);

end pkg_ProceduralSearch;

 
/

grant execute
on pkg_proceduralsearch
to posseextensions;

