create or replace package pkg_Pagination as

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *   Support procedures for paginating Outrider searches.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * GetLastSearchPageNumber()
   *    Returns the value of the global variable g_LastSearchPageNumber. This
   * is used by Outrider configuration to display the pagination description and
   * navigation links.
   *-------------------------------------------------------------------------*/
  function GetLastSearchPageNumber return number;

  /*---------------------------------------------------------------------------
   * GetLastSearchTotalRows()
   *    Returns the value of the global variable g_LastSearchTotalRows. This
   * is used by Outrider configuration to display the pagination description and
   * navigation links.
   *-------------------------------------------------------------------------*/
  function GetLastSearchTotalRows return number;

  /*---------------------------------------------------------------------------
   * GetLastSearchPageSize()
   *    Returns the value of the global variable g_LastSearchPageSize. This
   * is used by Outrider configuration to display the pagination description and
   * navigation links.
   *-------------------------------------------------------------------------*/
  function GetLastSearchPageSize return number;

  /*---------------------------------------------------------------------------
   * Paginate()
   *   Called by a procedural search to return the objects for a page number
   * within the result set.
   *-------------------------------------------------------------------------*/
  function Paginate (
    a_Objects                           api.udt_ObjectList,
    a_PageNumber                        number,
    a_PageSize                          number default null
  ) return api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * SetTotalRows()
   *   Called by a procedural search to set the total number of rows into
   * package state.  The GetLastSearchTotalRows function can then be used
   * by the results pane to display the total number of rows.
   *
   * Use this procedure when pagination isn't needed but a result count is
   * still desired.
   *-------------------------------------------------------------------------*/
  procedure SetTotalRows (
    a_TotalRows                         number
  );

end pkg_Pagination;
 
/

grant execute
on pkg_pagination
to bcpdata;

create or replace package body pkg_Pagination as

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Globals
   *-------------------------------------------------------------------------*/
  g_DefaultPageSize constant number :=
      api.pkg_ConstantQuery.Value('WEBPAGINATIONSIZE');
  g_LastSearchPageNumber number;
  g_LastSearchTotalRows  number;
  g_LastSearchPageSize   number;

  /*---------------------------------------------------------------------------
   * GetLastSearchPageNumber() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetLastSearchPageNumber return number is
  begin
    return g_LastSearchPageNumber;
  end;

  /*---------------------------------------------------------------------------
   * GetLastSearchTotalRows() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetLastSearchTotalRows return number is
  begin
    return g_LastSearchTotalRows;
  end;

  /*---------------------------------------------------------------------------
   * GetLastSearchPageSize() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function GetLastSearchPageSize return number is
  begin
    return g_LastSearchPageSize;
  end;

  /*---------------------------------------------------------------------------
   * Paginate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function Paginate (
    a_Objects                           api.udt_ObjectList,
    a_PageNumber                        number,
    a_PageSize                          number default null
  ) return api.udt_ObjectList is
    t_StartRow                          number;
    t_EndRow                            number;
    t_FilteredObjects                   api.udt_ObjectList;
  begin
    g_LastSearchPageNumber := nvl(a_PageNumber, 1);
    g_LastSearchTotalRows := a_Objects.count;
    g_LastSearchPageSize := nvl(a_PageSize, g_DefaultPageSize);
    t_StartRow := (g_LastSearchPageNumber-1) * g_LastSearchPageSize + 1;
    t_EndRow := least(t_StartRow + g_LastSearchPageSize - 1, a_Objects.count);

    t_FilteredObjects := api.udt_ObjectList();
    if t_EndRow >= t_StartRow then
      t_FilteredObjects.extend(t_EndRow-t_StartRow+1);
      for i in t_StartRow..t_EndRow loop
        t_FilteredObjects(i-t_StartRow+1) := a_Objects(i);
      end loop;
    end if;

    return t_FilteredObjects;
  end;

  /*---------------------------------------------------------------------------
   * SetTotalRows() -- PUBLIC
   *   Called by a procedural search to set the total number of rows into
   * package state.  The GetLastSearchTotalRows function can then be used
   * by the results pane to display the total number of rows.
   *
   * Use this procedure when pagination isn't needed but a result count is
   * still desired.
   *-------------------------------------------------------------------------*/
  procedure SetTotalRows (
    a_TotalRows                         number
  ) is
  begin
    g_LastSearchTotalRows := a_TotalRows;
  end;

end pkg_Pagination;
/

