create or replace package pkg_processserver is

  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_Name is api.pkg_definition.udt_Name;

  /*----------------------------------------------------------------------
   * ScheduleRTFReport()
   *   Handles the functionality needed place the specified report on the
   *   process server queue with the specified parameters.
   *
   *   Depends On:
   *     - RTF Reporters being configured on the Process Server.
   *     - A python script named 'RunRTFReport' configured in the Process
   *       server scripts.
   *--------------------------------------------------------------------*/
  function ScheduleRTFReport (
    a_ReportName                        varchar,
    a_ObjectId                          udt_Id default null,
    a_ProcessId                         udt_Id default null
  ) return udt_Id;

end pkg_processserver;

/

grant execute
on pkg_processserver
to abc;

create or replace package body pkg_processserver is

  function ScheduleRTFReport (
    a_ReportName                        varchar,
    a_ObjectId                          udt_Id default null,
    a_ProcessId                         udt_Id default null
  ) return udt_Id as
    t_Arguments                         varchar2(32767);
    t_ScheduleId                        udt_Id;
  begin
    api.pkg_ProcessServer.AddArgument(t_Arguments, a_ReportName);
    api.pkg_ProcessServer.AddArgument(t_Arguments, nvl(a_ObjectId, -1));
    api.pkg_ProcessServer.AddArgument(t_Arguments, nvl(a_ProcessId, -1));

    t_ScheduleId := api.pkg_processserver.SchedulePythonScript(
      a_Name => 'RunRTFReport',
      a_Description => 'RunRTFReport "'||a_ReportName||'"('||a_ObjectId||'/'||a_ProcessId||')',
      -- a_ServerId => 7,  -- Used to test the RTF Reporter on a specific Process Server.
      a_Arguments => t_Arguments);

    return t_ScheduleId;
  end;

end pkg_processserver;

/

