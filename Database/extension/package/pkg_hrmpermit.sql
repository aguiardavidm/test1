create or replace package           pkg_HRMPermit is
  /*---------------------------------------------------------------------------
   * Description
   *   HRM Permit Object Procedures.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  --
  /*---------------------------------------------------------------------------
   * Description
   *   Validates the HRM Permit object.
   *-------------------------------------------------------------------------*/
  procedure ValidateHRMPermit     ( a_ObjectId udt_Id,
                                    a_AsOfDate date );
  --
 /*---------------------------------------------------------------------------
  * Description
  *   HRM Permit Search
  *-------------------------------------------------------------------------*/
  procedure HRMPermitSearch       ( a_PermitNumber varchar2,
                                    a_PermitType   varchar2,
                                    a_FirstName    varchar2,
                                    a_LastName     varchar2,
                                    a_ProjectName  varchar2,
                                    a_Mer          varchar2,
                                    a_Rge          varchar2,
                                    a_Twp          varchar2,
                                    a_Sec          varchar2,
                                    a_FromApprovedDate       date,
                                    a_ToApprovedDate         date,
                                    a_FromExpirationDate     date,
                                    a_ToExpirationDate       date,
                                    a_FromSiteFormDueDate    date,
                                    a_ToSiteFormDueDate      date,
                                    a_FromFinalReportDueDate date,
                                    a_ToFinalReportDueDate   date,
                                    a_FromArtifactDueDate    date,
                                    a_ToArtifactDueDate      date,
                                    a_SiteId             varchar2,
                                    a_ExcludeCancelled   varchar2,
                                    a_ExcludeTransferred varchar2,
                                    a_OutstandingReport  varchar2,
                                    a_Objects            out nocopy api.udt_ObjectList);

  /*-----------------------------------------------------------------------------
  * Procedural Relationship
  * Provides a list of removed projects on an Amendment Application
  *----------------------------------------------------------------------------*/
  Procedure RemovedProjects(a_ObjectId                    udt_Id,
                            a_EndPointId                  udt_Id,
                            a_ProjectList    out nocopy api.udt_ObjectList);

  end pkg_HRMPermit;

 
/

grant execute
on pkg_hrmpermit
to posseextensions;

