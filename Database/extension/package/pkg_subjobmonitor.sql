create or replace package pkg_SubJobMonitor is
  /*----------------------------------------------------------------------------
   * Types - PRIVATE
   *--------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  /*----------------------------------------------------------------------------
   * CreateSubJobs()
   *   This procedure creates SubJobs for each Referral Party on a Parent job
   *
   * Optional Arguments:
   *   a_RelViewNamesToIgnore will ignore these relationships that would
   *     normally be automatically copied from Parent j=Job to SubJob
   *
   * How to Use:
   *   Attach this procedure to a Parent job workflow process outcome as an
   *   activity to run.
   *
   * Arguments (required unless optional is specified):
   *   a_SubJobViewName - View Name of SubJob to create
   *   a_SourceRelEndPointName - End Point Name from Parent Job process to
   *     Referral Party
   *   a_PartyRelViewName - End Point Name from SubJob to Referral Party
   *   a_AlwaysAddSubJob
   *     N - will not add SubJob for a Referral Party if it already exists and
   *         is not complete
   *     Y - will always add a new SubJob for each Referral Party
   *   a_RelViewNamesToIgnore (optional) - Relationship View Names from Parent
   *     Job, to ignore, that would get automatically copied to SubJob - list
   *     is comma delimited
   *--------------------------------------------------------------------------*/
  procedure CreateSubJobs (
    a_processid                         number,
    a_asofdate                          date,
    a_SubJobViewName                    varchar2,
    a_SourceRelEndPointName             varchar2,
    a_PartyRelEndPointName              varchar2,
    a_AlwaysAddSubJob                   char,
    a_RelViewNamesToIgnore              varchar2
  );

  /*----------------------------------------------------------------------------
   * EmailSubJobs()
   *   This procedure sends an email with optional attached document for each
   *   SubJob using email dynamic details on the SubJob.
   *
   *  Attached documents can not be opened unless they are an uncompressed
   *  document type so the setup of the related document on the SubJob should be
   *  with an uncompressed document type.
   *
   * Optional Arguments:
   *   a_SubJobProcViewNameToCreate will create this process on the SubJob.
   *   a_SubJobProcessOutcome will set the new process Outcome to this value.
   *   a_DocumentIdColumnName will attach this SubJob document to the email.
   *
   * How to Use:
   *   Attach this procedure to a Parent job workflow process outcome as an
   *   activity to run.
   *
   *   SubJob must have Dynamic Details for the following information:
   *     EmailFromAddress string
   *     EmailToAddress   string
   *     EmailSubject     string
   *     EmailBody        string
   *     EmailCcAddress   string (optional) to send to CcAddresses
   *     EmailBccAddress  string (optional) to send to BccAdresses
   *     EmailDocumentId  number (optional) to attach a document to email
   *
   * Arguments (required unless optional is specified):
   *   a_SubJobViewName - View Name of SubJob
   *   a_SubJobProcViewNameToCreate (optional) - Process View Name on SubJob to
   *     Create
   *   a_SubJobProcessOutcome (optional) - Process Outcome for SubJob
   *   a_FromAddressColumnName - Dynamic Detail Name on SubJob containing
   *     From Email Address
   *   a_ToAddressColumnName - Dynamic Detail Name on SubJob containing
   *     To Email Addresses delimited by commas
   *   a_CcAddressColumnName (optional) - Dynamic Detail Name on SubJob
   *     containing Cc Email Addresses delimited by commas
   *   a_BccAddressColumnName (optional) - Dynamic Detail Name on SubJob
   *     containing Bcc Email Addresses delimited by commas
   *   a_SubjectColumnName - Dynamic Detail Name on SubJob containing email
   *     Subject Line
   *   a_BodyColumnName - Dynamic Detail Name on SubJob containing email
   *     Message Body
   *   a_DocumentIdColumnName (optional) - Dynamic Detail Name containing
   *     DocumentId of document to attach to email. This Dynamic Detail is a
   *     lookup column of DocumentId from the related document object.
   *--------------------------------------------------------------------------*/
  procedure EmailSubJobs (
    a_processid                         number,
    a_asofdate                          date,
    a_SubJobViewName                    varchar2,
    a_SubJobProcViewNameToCreate        varchar2,
    a_SubJobProcessOutcome              varchar2,
    a_FromAddressColumnName             varchar2,
    a_ToAddressColumnName               varchar2,
    a_CcAddressColumnName               varchar2,
    a_BccAddressColumnName              varchar2,
    a_SubjectColumnName                 varchar2,
    a_BodyColumnName                    varchar2,
    a_DocumentIdColumnName              varchar2
  );

  /*----------------------------------------------------------------------------
   * NotifyWhenSubJobsComplete()
   *   This procedure checks whether all sibling SubJobs are completed
   *   (i.e. DateCompleted on SubJobs are not null).
   *   If all completed, then notify parent job by creating and/or completing a
   *   process.
   *
   * Optional Arguments:
   *   a_Outcome will set the new Parent process Outcome to this value.
   *
   * How to Use:
   *   Attach this procedure to a SubJob as a PostVerify procedure or
   *   attach to a SubJob process as a PostVerify procedure.
   *
   * Arguments (required unless optional is specified):
   *   a_ProcViewNameOnParentJob - View Name of Process to create on Parent Job
   *   a_Outcome (optional) - Process Outcome for Parent Job
   *--------------------------------------------------------------------------*/
  procedure NotifyWhenSubJobsComplete (
    a_ProcessIdOrJobId                  number,
    a_asofdate                          date,
    a_ProcViewNameOnParentJob           varchar2,
    a_ParentOutcome                     varchar2
  );

  /*----------------------------------------------------------------------------
   * CompleteSubJobs() Called from a process of the Parent Job.
   *   This procedure looks for all SubJobs of given type to find incomplete
   *   processes of given processtype to complete them with the given outcome.
   *
   * How to Use:
   *   Attach this procedure to a Parent job workflow process outcome as an
   *   activity to run.
   *
   * Arguments (required unless optional is specified):
   *   a_SubJobViewName - View Name of SubJob
   *   a_SubJobProcessViewName - View Name of SubJob Process to outcome
   *   a_SubJobOutcome - valid outcome value for SubJob process
   *--------------------------------------------------------------------------*/
  procedure CompleteSubJobs (
    a_processid                         number,
    a_asofdate                          date,
    a_SubJobViewName                    varchar2,
    a_SubJobProcessViewName             varchar2,
    a_SubJobOutcome                     varchar2
  );

end pkg_SubJobMonitor;
 
/

create or replace package body pkg_SubJobMonitor is
  /*----------------------------------------------------------------------------
   * Types - PRIVATE
   *--------------------------------------------------------------------------*/
  subtype udt_Idlist is api.pkg_Definition.udt_Idlist;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  /*---------------------------------------------------------------------------
   * CreateSubJobs() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure CreateSubJobs (
    a_Processid                         number,
    a_AsOfdate                          date,
    a_SubJobViewName                    varchar2,
    a_SourceRelEndPointName             varchar2,
    a_PartyRelEndPointName              varchar2,
    a_AlwaysAddSubJob                   char,
    a_RelViewNamesToIgnore              varchar2
  ) is
    t_Index                             number;
    t_ViewNamesindex                    number;
    t_ParentJobId                       udt_Id;
    t_ParentJobTypeId                   udt_Id;
    t_SubJobTypeId                      udt_Id;
    t_SubJobId                          udt_Id;
    t_ToEndPointId                      udt_Id;
    t_RelationshipId                    udt_Id;
    t_SubReferralPartyObj               udt_Id;
    t_RelViewNamesToIgnore              udt_StringList;
    t_FromRelationshipNameId            udt_Id;
    t_SourceObjects                     udt_IdList;
    t_PartyRelEndPointId                udt_Id;
  begin
    -- Get SubJob def type
    t_SubJobTypeId := api.pkg_ObjectDefQuery.IdForName(a_SubJobViewName);
    if nvl(t_SubJobTypeId,0) = 0 then
      raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.CreateSubJobs - SubJobViewName is not a valid Job Type');
    end if;

    t_PartyRelEndPointId := api.pkg_ConfigQuery.EndPointIdForName(t_SubJobTypeId,
        a_PartyRelEndPointName);

    -- Get Parent JobId and Parent JobTypeId
    select a.jobid, b.jobtypeid
      into t_ParentJobId, t_ParentJobTypeId
      from api.jobs b,
               api.Processes a
     where a.processid = a_processid
           and a.jobid = b.jobid;

    /* This produces a SubJob for each source object. */
    t_SourceObjects := pkg_ObjectQuery.RelatedObjects(a_ProcessId,
        a_SourceRelEndPointName);
    for i in 1..t_SourceObjects.count loop
      if nvl(a_AlwaysAddSubjob,'X') not in ('Y','N') then
        raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.CreateSubJobs - AddAlways is not N or Y');
      end if;

      /* First determine if we even need to create a new sub-job.  If one
        already exists that hasn't been completed yet, then use it. */
      t_SubJobId := null;
      if a_AlwaysAddSubjob = 'N' then
        begin
          select j.JobId
            into t_SubJobId
            from api.Relationships r,
                 api.Jobs j
           where j.ParentJobId = t_ParentJobId
             and j.CompletedDate is null
             and j.JobId = r.FromObjectId
             and r.ToObjectId = t_SourceObjects(i)
             and r.EndPointId = t_PartyRelEndPointId;
        exception when no_data_found then
          null;
        end;
      end if;
      if t_SubJobId is null then
        /* Create SubJob. */
        t_SubJobId := api.pkg_jobupdate.new(t_SubJobTypeId, null,
            null, t_ParentJobId);
        /* Create relationship from SubJob to the Referral Party object. */
        t_RelationshipId := pkg_RelationshipUpdate.New(t_SubJobId,
            t_SourceObjects(i), a_PartyRelEndPointName);
      end if;

      /* Relationships automatically copy from Parent Job to Subjob if they have
             the same Relatioship Names. This will remove unwanted ones */
      if a_RelViewNamesToIgnore is not null then
         t_RelViewNamesToIgnore := toolbox.pkg_parse.ListFromString (
            a_RelViewNamesToIgnore,',',false,true);
         FOR ViewNameIndex IN 1..t_RelViewNamesToIgnore.COUNT LOOP
               if nvl(api.pkg_ObjectDefQuery.IdForName(t_RelViewNamesToIgnore(ViewNameIndex)),0) = 0 then
             raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.CreateSubJobs - RelViewNamesToIgnore are not valid Relationship Types');
               end if;

           --Find RelationshipNameId of Relationhship to Ignore
           begin
                     select FromRelationshipNameId
                       into t_FromRelationshipNameId
                       from api.relationshipdefs
                          where name = t_RelViewNamesToIgnore(ViewNameIndex)
                            and fromobjectdefid = t_ParentJobTypeId;
                   exception when no_data_found then
             raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.CreateSubJobs - RelViewNamesToIgnore does not exist on Parent Job');
                   end;
           --Find Relationship EndPointID for SubJob using FromRelationshipNameId
           --If RelationshipName does not exist then bypass
           begin
                     select toendpointid
                       into t_ToEndPointID
                       from api.relationshipdefs
                          where fromrelationshipnameid = t_FromRelationshipNameId
                            and fromobjectdefid = t_SubJobTypeId;

             --Remove
             for j in (
                   select relationshipid
                     from api.relationships
                        where endpointId = t_ToEndPointID
                          and fromobjectid = t_SubJobId
                     ) loop
                       api.pkg_relationshipupdate.remove(j.relationshipid);
                 end loop;
                   exception when no_data_found then
             null;
                   end;
         end loop;
      end if;
    end loop;
  end;
  /*---------------------------------------------------------------------------
   * EmailSubJobs() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure EmailSubJobs (
    a_processid                         number,
    a_asofdate                          date,
    a_SubJobViewName                    varchar2,
        a_SubJobProcViewNameToCreate        varchar2,
    a_SubJobProcessOutcome              varchar2,
    a_FromAddressColumnName             varchar2,
    a_ToAddressColumnName               varchar2,
    a_CcAddressColumnName               varchar2,
    a_BccAddressColumnName              varchar2,
    a_SubjectColumnName                 varchar2,
    a_BodyColumnName                    varchar2,
    a_DocumentIdColumnName              varchar2
  ) is
    t_FromAddress                       varchar(100);
    t_ToAddress                         varchar(100);
        t_CcAddress                         varchar(100);
    t_Subject                           varchar(100);
    t_Message                           varchar(4000);
    t_ProcessId                         udt_Id;
        t_SubJobTypeID                      udt_Id;
        t_ProcessTypeID                     udt_Id;
    t_AttachRelTypeID                   udt_Id;
        t_JobId                             udt_Id;
        t_documentid                        udt_Id;
    t_Attachment                        BLOB;
        t_Count                             number;
  begin
    select jobid into t_JobId
    from api.processes
    where processid = a_ProcessId;

    t_SubJobTypeID := api.pkg_ObjectDefQuery.IdForName(a_SubJobViewName);
    if nvl(t_SubJobTypeID,0) = 0 then
      raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.EmailSubJobs - SubJobViewName is not a valid Job Type');
    end if;
    if a_SubJobProcViewNameToCreate is not null then
      t_ProcessTypeID := api.pkg_ObjectDefQuery.IdForName(a_SubJobProcViewNameToCreate);
      if nvl(t_ProcessTypeID,0) = 0 then
        raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.EmailSubJobs - SubJobProcViewNameToCreate is not a valid Process Type');
      end if;
        end if;
    -- Check for email errors
        for j in (
      select jobid SubJobId
        from api.jobs
       where parentjobid = t_JobId
             and JobTypeId = t_SubJobTypeID
        ) loop
      if api.pkg_ColumnQuery.Value(j.SubJobId,a_FromAddressColumnName) is null then
        raise_application_error(-20000,'Configuration or Data Error on pkg_SubJobMonitor.EmailSubJobs - Subjob SenderColumn missing or no data');
      end if;
          t_ToAddress := api.pkg_ColumnQuery.Value(j.SubJobId,a_ToAddressColumnName);
      if t_ToAddress is null then
        raise_application_error(-20000,'Configuration or Data Error on pkg_SubJobMonitor.EmailSubJobs - Subjob ToColumn missing or no data');
      end if;
      if instr(t_ToAddress,';') <> 0 then
        raise_application_error(-20000,'Configuration or Data Error on pkg_SubJobMonitor.EmailSubJobs - Subjob ToColumn cannot have multiple addresses');
          end if;
      if api.pkg_ColumnQuery.Value(j.SubJobId,a_SubjectColumnName) is null then
        raise_application_error(-20000,'Configuration or Data Error on pkg_SubJobMonitor.EmailSubJobs - Subjob SubjectColumn missing or no data');
      end if;
      if api.pkg_ColumnQuery.Value(j.SubJobId,a_BodyColumnName) is null then
        raise_application_error(-20000,'Configuration or Data Error on pkg_SubJobMonitor.EmailSubJobs - Subjob BodyColumn missing or no data');
      end if;
    end loop;
    -- For each Subjob
        for j in (
      select jobid SubJobId
        from api.jobs
       where parentjobid = t_JobId
             and JobTypeId = t_SubJobTypeID
        ) loop
      -- Send email from each Subjob
      pkg_SendMail.SendPosseEmail(j.SubJobId, sysdate, a_FromAddressColumnName,
          a_ToAddressColumnName, a_CcAddressColumnName, a_BccAddressColumnName,
          a_SubjectColumnName, a_BodyColumnName, a_DocumentIdColumnName, null);
      --Create Subjob Process and give and Outcome
      if a_SubJobProcViewNameToCreate is not null then
        --Check for Valid Outcome
        if a_SubJobProcessOutcome is not null then
          select count(*)
            into t_Count
            from api.processoutcomes b,
                   api.jobs a
             where a.jobid = j.SubJobId
               and a.jobtypeid = b.jobtypeid
                   and b.processtypeid = t_ProcessTypeID
                   and b.outcome = a_SubJobProcessOutcome;
          if t_count = 0 then
            raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.EmailSubJobs - SubJobProcessOutcome does not exist for Subjob Process');
          end if;
        end if;

        --Create Subjob Process if it does not already exist
        begin
          select processid
            into t_Processid
            from api.processes
           where jobid = j.SubJobId
             and processtypeid = t_ProcessTypeID
                         and outcome is null;
        exception when no_data_found then
          t_ProcessId := api.pkg_processupdate.new(j.SubJobId,t_ProcessTypeID,
            null,null,null,null);
        end;

        --Complete Subjob process
        if a_SubJobProcessOutcome is not null then
          api.pkg_ProcessUpdate.complete(t_ProcessId, a_SubJobProcessOutcome);
        end if;
          end if;
      /* Whatever the sub-job status is set to, revert to default security. */
      api.pkg_objectupdate.RevertToDefaultSecurity(j.SubJobId);
    end loop;
  end;
  /*---------------------------------------------------------------------------
   * NotifyWhenSubjobsComplete() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure NotifyWhenSubjobsComplete (
    a_ProcessIdOrJobId                  number,
    a_asofdate                          date,
    a_ProcViewNameOnParentJob           varchar2,
    a_ParentOutcome                     varchar2
  ) is
    t_ProcessId                         udt_Id;
    t_JobId                             udt_Id;
    t_ParentJobId                       udt_Id;
    t_Count                             udt_Id;
        t_ProcessTypeID                     udt_Id;
        t_SubJobTypeID                      udt_Id;
  begin
        t_ProcessTypeID := api.pkg_ObjectDefQuery.IdForName(a_ProcViewNameOnParentJob);
    if nvl(t_ProcessTypeID,0) = 0 then
      raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.NotifyWhenSubjobsComplete - ProcViewNameOnParentJob is not a valid Process Type');
    end if;
    begin
      select JobId
        into t_JobId
        from api.processes
       where ProcessId = a_ProcessIdOrJobId;
        exception when no_data_found then
      t_JobId := a_ProcessIdOrJobId;
        end;
    select ParentJobId, JobTypeId
      into t_ParentJobId, t_SubJobTypeID
      from api.Jobs
     where JobId = t_JobId;

    if a_ParentOutcome is not null then
      --Check for valid Outcome
      select count(*)
        into t_Count
        from api.processoutcomes b,
                 api.jobs a
           where a.jobid = t_ParentJobId
             and a.jobtypeid = b.jobtypeid
         and b.processtypeid = t_ProcessTypeID
                 and b.outcome = a_ParentOutcome;
      if t_count = 0 then
        raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.NotifyWhenSubjobsComplete - Outcome is not valid for Parent Process');
      end if;
    end if;

    --Check to see if all sibling SubJobs are complete
    select count(*)
      into t_Count
      from api.Jobs
     where ParentJobId = t_ParentJobId
       and JobTypeId = t_SubJobTypeID
       and CompletedDate is null;


    --All sibling SubJobs are complete
    if t_Count = 0 then
      --Check if Parent Job Process exists with an outcome
          --if it does exist then do nothing
          --if it does not exist then add process with outcome
      --begin
      select min(ProcessId)
      into t_ProcessId
      from api.Processes
      where JobId = t_ParentJobId
        and ProcessTypeId = t_ProcessTypeID
        and outcome is null;

      if t_ProcessId is null then
        --Create Parent Job process if it does not exist
        t_ProcessId := api.pkg_ProcessUpdate.New(t_ParentJobId, t_ProcessTypeID,
                  null, null, null, null);
      end if;

      --Change outcome of Parent Job process if parameter is given
      if a_ParentOutcome is not null then
        api.pkg_ProcessUpdate.complete(t_ProcessId, a_ParentOutcome);
      end if;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * CompleteSubJobs() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure CompleteSubJobs (
    a_processid                         number,
    a_asofdate                          date,
    a_SubJobViewName                    varchar2,
    a_SubJobProcessViewName             varchar2,
    a_SubJobOutcome                     varchar2
  ) is
    t_ProcessId                         udt_Id;
    t_ParentJobId                       udt_Id;
        t_SubJobTypeID                      udt_Id;
        t_SubJobProcessTypeID               udt_Id;
        t_Count                             number;
  begin
    t_SubJobTypeID := api.pkg_ObjectDefQuery.IdForName(a_SubJobViewName);
    if nvl(t_SubJobTypeID,0) = 0 then
      raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.CompleteSubJobs - SubJobViewName is not a valid Job Type');
    end if;

    t_SubJobProcessTypeID := api.pkg_ObjectDefQuery.IdForName(a_SubJobProcessViewName);
    if nvl(t_SubJobProcessTypeID,0) = 0 then
      raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.CompleteSubJobs - SubJobProcessViewName is not a valid Process Type');
    end if;
    --Check that Outcome is valid
    select count(*)
      into t_Count
      from api.processoutcomes
         where jobtypeid = t_SubJobTypeID
       and processtypeid = t_SubJobProcessTypeID
           and outcome = a_SubJobOutcome;
    if t_count = 0 then
      raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.CompleteSubJobs - Outcome is not valid for Subjob Process');
    end if;

    -- Get the job that this process was called from.
    select jobid
      into t_ParentJobId
      from api.processes
     where processid = a_processid;
    -- Loop through uncompleted Subjobs
    for i in (
       select jobid
       from api.jobs
       where parentjobid = t_ParentJobId
           and JobTypeId = t_SubJobTypeID
       and completeddate is null
    )
    loop
       -- Find the process on the subjob with no outcome.
       begin
         select processid
           into t_Processid
           from api.processes
          where jobid = i.jobid
            and processtypeid = t_SubJobProcessTypeID;
       exception when no_data_found then
         --Create Parent Job process if it does not exist
         t_ProcessId := api.pkg_ProcessUpdate.New(i.JobId, t_SubJobProcessTypeID,
                   null, null, null, null);
           end;
       -- Complete each process for the sub-job by setting Outcome.
       api.pkg_ProcessUpdate.complete(t_ProcessId, a_SubJobOutcome);
           -- Ensure outcome changes Subjob status to Completed
           select count(*)
             into t_count
                 from api.jobs
                where jobid = i.jobid
                  and completeddate is null;
       if t_count <> 0 then
         raise_application_error(-20000,'Configuration Error on pkg_SubJobMonitor.CompleteSubJobs - Outcome not changing Subjob status to completed');
       end if;
    end loop;
  end;
end pkg_SubJobMonitor;
/

