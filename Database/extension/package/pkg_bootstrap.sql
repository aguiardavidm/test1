create or replace package pkg_Bootstrap is

  /*---------------------------------------------------------------------------
   * Description
   *   The package is temporarily intended to provide interim functionality that
   * will be replaced by enhancements in Posse 6.1.7.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * ProcessPaneHTML()
   *   Return a table with a list of the processes for this job.  This will
   * be replaced by the configurable process pane but this provides interim
   * functionality for development purposes.  Feel free to customize the HTML
   * as needed.
   *-------------------------------------------------------------------------*/
  function ProcessPaneHTML (
    a_JobId                             udt_Id
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * ProcessInsertionHTML()
   *   Return a hidden, absolute positioned <div> section that implements a pop
   * up menu with a list of process types to insert.  This list will not be
   * filtered based on security or lock-down.  This HTML will then also render
   * an <input> button that pops up the menu.
   *-------------------------------------------------------------------------*/
  function ProcessInsertionHTML (
    a_JobId                             udt_Id
  ) return varchar2;

end pkg_Bootstrap;
 
/

grant execute
on pkg_bootstrap
to posseextensions;

create or replace package body pkg_Bootstrap is

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * ProcessPaneHTML() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function ProcessPaneHTML (
    a_JobId                             udt_Id
  ) return varchar2 is
    t_HTML                              varchar2(32767);
    t_FooterHTML                        varchar2(32767);
    t_RowHTMl                           varchar2(32767);
    cursor c_Processes is
      select
        p.ProcessId,
	pt.Description ProcessType,
	p.ScheduledStartDate,
	p.DateCompleted,
	p.Outcome,
	p.AssignedStaff,
	p.ProcessStatus
      from
        api.Processes p,
	api.ProcessTypes pt
      where p.JobId = a_JobId
        and pt.ProcessTypeId = p.ProcessTypeId
      order by nvl(p.DateCompleted, sysdate) desc, p.ScheduledStartDate;
  begin
    t_HTML :=
      '<table class="possegrid" border="0" rules="groups" cellspacing="0" cellpadding="4">' ||
      '<thead>' ||
      '<tr>' ||
      '<th></th>' ||
      '<th>Process Type</th>' ||
      '<th>Status</th>' ||
      '<th>Assigned To</th>' ||
      '<th>Outcome</th>' ||
      '<th>Scheduled Start</th>' ||
      '<th>Date Completed</th>' ||
      '</tr>' ||
      '</thead>';
    t_FooterHTML := '</table>';

    for p in c_Processes loop
      t_RowHTML := '<tbody class="posseband_' ||
          (mod(c_Processes%rowcount, 2)+1) || '">';
      t_RowHTML := t_RowHTML || '<tr>';
      t_RowHTML := t_RowHTML || '<td><a href="default.aspx?PossePresentation=Default=' || p.ProcessId || '">Edit</a></td>';
      t_RowHTML := t_RowHTML || '<td>'|| p.ProcessType||'</td>';
      t_RowHTML := t_RowHTML || '<td>'|| p.ProcessStatus||'</td>';
      t_RowHTML := t_RowHTML || '<td>'|| p.AssignedStaff||'</td>';
      t_RowHTML := t_RowHTML || '<td>'|| p.Outcome||'</td>';
      t_RowHTML := t_RowHTML || '<td>'||
          to_char(p.ScheduledStartDate,'Mon dd, yyyy')||'</td>';
      t_RowHTML := t_RowHTML || '<td>'||
          to_char(p.DateCompleted,'Mon dd, yyyy')||'</td>';
      t_RowHTML := t_RowHTML || '</tr>';
      t_RowHTML := t_RowHTML || '</tbody>';

      if length(t_RowHTMl) + length(t_HTML) + length(t_FooterHTMl) <= 4000 then
        t_HTML := t_HTML || t_RowHTML;
      else
        exit;
      end if;
    end loop;

    return t_HTML || t_FooterHTML;
  end;

  /*---------------------------------------------------------------------------
   * ProcessInsertionHTML() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function ProcessInsertionHTML (
    a_JobId                             udt_Id
  ) return varchar2 is
    t_HTML                              varchar2(32767);
    t_FooterHTMl                        varchar2(32767);
    t_RowHTML                           varchar2(32767);
    cursor c_ProcessTypes is
      select
        pt.Name,
	pt.Description
      from
        api.Jobs j,
	api.JobTypeProcessTypes jp,
	api.ProcessTypes pt
      where j.JobId = a_JobId
        and jp.JobTypeId = j.JobTypeId
	and pt.ProcessTypeId = jp.ProcessTypeId
      order by pt.Description;
  begin
    t_HTML :=
        '<div class="popupmenunormal" onMouseOver="stopTimer(timerID);"' ||
	' onMouseOut="timerID=startTimer();" id="InsertProcess"' ||
        ' style="z-index: 100; visibility: hidden; position: absolute;' ||
	' top: -999px; left: -999px;">' ||
        '<table style="padding: 0px;" border="0" cellpadding="0"'||
        ' cellspacing="0">';
    t_FooterHTML := '</table></div>';

    /* Add the code to display a button to pop up the menu */
    t_FooterHTML := t_FooterHTML ||
        '<input type="button" value="Insert Process >>"' ||
        ' onclick="hideAll(); showMenu(''InsertProcess'', this); event.cancelBubble=true;"' ||
        ' onmouseout="timerID=startTimer();"/>';

    /* Build the menu items.  Keep going until we exceed 4000 characters */
    for p in c_ProcessTypes loop
      t_RowHTML := '';
      if c_ProcessTypes%rowcount > 1 then
        t_RowHTML := '<tr><td class="popupmenuseparator"></td></tr>';
      end if;
      t_RowHTML := t_RowHTML || '<tr><td class="popupmenunormal" ' ||
          ' onmouseover="className=''popupmenuselect'';"' ||
          ' onmouseout="className=''popupmenunormal'';">' ||
          '<a class="menutext" href="default.aspx?PossePresentation=Default&'||
	  'PosseObjectDef='||p.Name||'='||a_JobId||'">'|| p.Description ||
          '</a></td></tr>';

      if length(t_HTML) + length(t_RowHTML) + length(t_FooterHTML) <= 4000 then
        t_HTML := t_HTML || t_RowHTML;
      else
	exit;
      end if;
    end loop;

    return t_HTML || t_FooterHTML;
  end;

end pkg_Bootstrap;
/

