create or replace package           pkg_errors is

  -- Author  : CARL.BRUINSMA
  -- Created : 9/26/2008 13:20:30
  -- Purpose : Utilities for raising error messages in a stanadard way.  Most
  --           of the procedures in here check for some condition and if it
  --           isn't verified for the object then it adds a string to a
  --           stringlist.  ConstructMessage then places the strings in the
  --           stringlist into a proper sentance with a your choice of beginning
  --           and end (prepend and append).

  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_StringList is api.pkg_definition.udt_StringList;

  -- Public constant declarations
  gc_Prepend constant varchar2(50) := 'Please ensure that the following is entered: ';
  gc_Append constant varchar2(50) := '.';

 /*--------------------------------------------------------------------------
  * CheckAllMandatoryColumns () - populates a_ErrorItemList with the column
  *     labels of the columns on a_ObjectId that have 'Mandatory'
  *     checked, but are missing a value.
  *------------------------------------------------------------------------*/
  procedure CheckAllMandatoryColumns(
    a_ObjectId                          udt_Id,
    a_ErrorItemList                     in out udt_StringList
  );

 /*--------------------------------------------------------------------------
  * CheckColumn () - check a specific column to see if it is null.  Use this
  *     to check optionally conditional fields.
  *     This version will add the column's label to the a_ErrorItemList.
  *-------------------------------------------------------------------------*/
  procedure CheckColumn(
    a_ObjectId                          udt_Id,
    a_ColumnName                        varchar2,
    a_ErrorItemList                     in out udt_StringList
  );

 /*--------------------------------------------------------------------------
  * CheckColumn () - check a specific column to see if it is null.  Use this
  *     to check optionally conditional fields.
  *     Returns null if the column has a value, returns the columns label if not
  *-------------------------------------------------------------------------*/
  function CheckColumn(
    a_ObjectId                          udt_Id,
    a_ColumnName                        varchar2
  ) return varchar2;

 /*--------------------------------------------------------------------------
  * CheckRelationshipMin () - if the relationship is not more then the min
  *     specified then it will add an item to the ErrorItemList in the format:
  *       'at least {Min} {MessagePiece}'
  *       e.g. Final error message: (a_MessagePiece := 'Type of Research')
  *           "Please ensure at least 1 Type of Research is entered."
  *-------------------------------------------------------------------------*/
  procedure CheckRelationshipMin(
    a_ObjectId                          udt_Id,
    a_EndPointName                      varchar2,
    a_MinCount                          pls_integer,
    a_MessagePiece                      varchar2,
    a_ErrorItemList                     in out udt_StringList
  );

 /*--------------------------------------------------------------------------
  * CheckForRelationshipWithValue () - See if any of the related items at
  *     the endpoint have a specific Value in the RelatedColumnName column.
  *-------------------------------------------------------------------------*/
  procedure CheckForRelationshipWithValue(
    a_ObjectId                         udt_Id,
    a_EndPointName                     varchar2,
    a_RelatedColumnName                varchar2,
    a_Value                            varchar2,
    a_MessagePiece                     varchar2,
    a_ErrorItemList                    in out udt_StringList
  );

 /*--------------------------------------------------------------------------
  * ConstructMessage () - will construct a string to use as an error message.
  *     String will start with a_Prepend (assumes this ends with a space), then
  *     will have a comma seperated list of all the items in a_ErrorItemList,
  *     followed by a_Append (assumes this starts with a space).
  *-------------------------------------------------------------------------*/
  function ConstructMessage(
    a_ErrorItemList                   udt_StringList,
    a_Prepend                         varchar2 default gc_Prepend,
    a_Append                          varchar2 default gc_Append
  ) return varchar2;

 /*--------------------------------------------------------------------------
  * RaiseError () - will raise a -20000 error message using api if
  *     a_ErrorMessage is not null.
  *-------------------------------------------------------------------------*/
  procedure RaiseError(
    a_ErrorMessage                      varchar2
  );


 /*--------------------------------------------------------------------------
  * AddToStringList () - merely adds another item to the end of the string list.
  *-------------------------------------------------------------------------*/
  procedure AddToStringList(
    a_String                           varchar2,
    a_StringList                       in out udt_stringList
  );

 /*--------------------------------------------------------------------------
  * LabelForName () - returns the label of the dynamic detail on the object
  *-------------------------------------------------------------------------*/
  function LabelForName(
    a_ObjectId                        udt_Id,
    a_ColumnName                      varchar2
  ) return varchar2;

 /*--------------------------------------------------------------------------
  * RelatedObjectColumnLabel () - returns the label of the dynamic detail
  *     on across the relationship.
  *-------------------------------------------------------------------------*/
  function RelatedObjectColumnLabel(
    a_ObjectId                        udt_Id,
    a_EndPointId                      udt_Id,
    a_ColumnName                      varchar2
  ) return varchar2;

end pkg_errors;

 
/

grant execute
on pkg_errors
to posseextensions;

