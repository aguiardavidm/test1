create or replace package pkg_CxProceduralSearch as

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_ObjectTable is pkg_CollectionUtils.udt_ObjectTable;
  subtype udt_ExternalFileNum is api.pkg_Definition.udt_ExternalFileNum;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * InitializeSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure InitializeSearch (
    a_ObjectDefName                     varchar2
  );

  /*---------------------------------------------------------------------------
   * SearchByJob() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByJob (
    a_Project                           udt_Id,
    a_SeqNum                            number,
    a_ExternalFileNum                   udt_ExternalFileNum,
    a_Status                            varchar2,
    a_CreatedStartDate                  date,
    a_CreatedEndDate                    date,
    a_IssuedStartDate                   date,
    a_IssuedEndDate                     date,
    a_CompletedStartDate                date,
    a_CompletedEndDate                  date
  );

  /*---------------------------------------------------------------------------
   * SearchByIndex() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByIndex (
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean default false
  );

  /*---------------------------------------------------------------------------
   * SearchByIndex() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByIndex (
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  );

  /*---------------------------------------------------------------------------
   * SearchBySystemColumn() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchBySystemColumn (
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  );

  /*---------------------------------------------------------------------------
   * SearchBySystemColumn() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchBySystemColumn (
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  );

  /*---------------------------------------------------------------------------
   * SearchByKeywords() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByKeywords (
    a_Keywords                          varchar2
  );

  /*---------------------------------------------------------------------------
   * SearchByNullDate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByNullDate (
    a_ColumnName                        varchar2
  );

  /*---------------------------------------------------------------------------
   * FilterObjects() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FilterObjects (
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  );

  /*---------------------------------------------------------------------------
   * FilterObjects() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FilterObjects (
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  );

  /*---------------------------------------------------------------------------
   * SearchByRelatedIndex() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelatedIndex (
    a_EndPointName                      varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  );

  /*---------------------------------------------------------------------------
   * SearchByRelatedIndex() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelatedIndex (
    a_EndPointName                      varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  );

  /*---------------------------------------------------------------------------
   * SearchByRelIndexOtherEndPoint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelIndexOtherEndPoint (
    a_OtherEndPointName                 varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  );

  /*---------------------------------------------------------------------------
   * SearchByRelIndexOtherEndPoint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelIndexOtherEndPoint (
    a_OtherEndPointName                 varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  );

  /*---------------------------------------------------------------------------
   * SearchByRelatedAddress() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelatedAddress (
    a_EndPointName                      varchar2,
    a_FromHouseNumber                   number,
    a_ToHouseNumber                     number,
    a_HouseSuffix                       varchar2,
    a_StreetName                        varchar2,
    a_Suite                             varchar2,
    a_ExactMatch                        varchar2
  );

  /*---------------------------------------------------------------------------
   * SearchByRelAddrOtherEndPoint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelAddrOtherEndPoint (
    a_OtherEndPointName                 varchar2,
    a_FromHouseNumber                   number,
    a_ToHouseNumber                     number,
    a_HouseSuffix                       varchar2,
    a_StreetName                        varchar2,
    a_Suite                             varchar2,
    a_ExactMatch                        varchar2
  );

  /*---------------------------------------------------------------------------
   * SearchByAddress() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByAddress (
    a_FromHouseNumber                   number,
    a_ToHouseNumber                     number,
    a_HouseSuffix                       varchar2,
    a_StreetName                        varchar2,
    a_Suite                             varchar2,
    a_ExactMatch                        varchar2
  );

  /*---------------------------------------------------------------------------
   * PerformSearch() -- PUBLIC
   *   Perform the search and place the results in a_Objects.
   *
   *   The parameter a_SearchType must be one of: 'and' or 'or'. A value of
   * 'and' means that individual searchs should intersected.  A value of 'or'
   * means that individual searches should be unioned.
   *
   *   The a_AppendResultsType parameter must be one of: 'none', 'and', 'or'.
   * A value of 'none' means that any existing objects in a_Objects will be
   * deleted.  A value of 'or' means the search results will be unioned with
   * the current values in a_Objects.  A value of 'and' means the search
   * results will be intersected with the values in a_Objects.
   *-------------------------------------------------------------------------*/
  procedure PerformSearch (
    a_Objects                           in out nocopy api.udt_ObjectList,
    a_SearchType                        varchar2 default 'or',
    a_AppendResultsType                 varchar2 default 'none'
  );

    /*---------------------------------------------------------------------------
   * ReverseResults() -- PUBLIC
   *   Takes the ResultSet and reverses the order
   *-------------------------------------------------------------------------*/
  procedure ReverseResults (
    a_Objects                           in out nocopy api.udt_ObjectList

  );

  /*---------------------------------------------------------------------------
   * SearchPerformed() -- PUBLIC
   *   Returns 'Y' if at least one of the searches actually had a non-null
   * argument.
   *-------------------------------------------------------------------------*/
  function SearchPerformed
  return boolean;

  /*--------------------------------------------------------------------------
   * RelatedObjects()
   * This function is here because, as of POSSE 6.1.11,
   * api.pkg_ObjectQuery.RelatedObjects returns only about 80 rows per second.
   * This function attempts to avoid using api.pkg_ObjectQuery.RelatedObjects
   * wherever reasonable.
   *------------------------------------------------------------------------*/
  function RelatedObjects(
    a_ObjectId                          udt_Id,
    a_EndPointId                        udt_Id,
    a_StartDate                         date default null,
    a_EndDate                           date default null
  ) return udt_IdList;

end pkg_CxProceduralSearch;

 
/

grant execute
on pkg_cxproceduralsearch
to abc;

grant execute
on pkg_cxproceduralsearch
to bcpdata;

grant execute
on pkg_cxproceduralsearch
to conversion;

grant execute
on pkg_cxproceduralsearch
to ereferral;

create or replace package body pkg_CxProceduralSearch as

  /*--------------------------------------------------------------------------
   * Types - PRIVATE
   *------------------------------------------------------------------------*/
  subtype udt_RawQueryValueList is api.pkg_Definition.udt_RawQueryValueList;
  subtype udt_HouseNumber is api.pkg_Definition.udt_HouseNumber;
  subtype udt_HouseSuffix is api.pkg_Definition.udt_HouseSuffix;
  subtype udt_Boolean is api.pkg_Definition.udt_Boolean;
  type udt_Cursor is ref cursor;

  type udt_KeyWordsSearch is record (
    keywords             varchar2(100)
  );
  type udt_KeyWordsSearchList is table of udt_KeyWordsSearch
  index by binary_integer;


  type udt_SystemSearch is record (
    Stat_Sql                  varchar2(4000),
    Stat_Parameters           varchar2(50)
  );
  type udt_SystemSearchList is table of udt_SystemSearch
  index by binary_integer;


  type udt_IndexSearch is record (
    ColumnName                varchar2(30),
    IndexDef                  udt_Id,
    FromIndexValue            varchar2(4000),
    ToIndexValue              varchar2(4000),
    PerformWildCardSearch     boolean
  );
  type udt_IndexSearchList is table of udt_IndexSearch
  index by binary_integer;


  type udt_RelatedSearch is record (
    ColumnName                  varchar2(30),
    IndexDef                    udt_Id,
    EndPointId                  udt_Id,
    ObjectDefName               varchar2(100),
    ObjectDefId                 udt_Id,
    FromIndexValue              varchar2(4000),
    ToIndexValue                varchar2(4000),
    PerformWildCardSearch       boolean,
    MinValueLength              pls_integer
  );
  type udt_RelatedSearchList is table of udt_RelatedSearch
  index by binary_integer;


  type udt_AddressSearch is record (
    AddressFromHouseNumber  number,
    AddressToHouseNumber    number,
    AddressHouseSuffix      varchar2(100),
    AddressStreetName       varchar2(4000),
    AddressHouseSuite       varchar2(100),
    AddressExactMatch       varchar2(1)
  );
  type udt_AddressSearchList is table of udt_AddressSearch
  index by binary_integer;


  type udt_RelatedAddressSearch is record (
    EndPointId                  udt_Id,
    ObjectName                  varchar2(50),
    ObjectDefId                 udt_Id,
    AddressFromHouseNumber      number,
    AddressToHouseNumber        number,
    AddressHouseSuffix          varchar2(100),
    AddressStreetName           varchar2(4000),
    AddressHouseSuite           varchar2(100),
    AddressExactMatch           varchar2(1)
  );
  type udt_RelatedAddressSearchList is table of udt_RelatedAddressSearch
  index by binary_integer;


  type udt_FilterSearch is record (
    ColumnName                  api.ColumnDefs.Name%TYPE,
    DataType                    api.ColumnDefs.DataType%TYPE,
    FromValue                   varchar2(4000),
    ToValue                     varchar2(4000),
    DateFromValue               date,
    DateToValue                 date,
    NumberFromValue             number,
    NumberToValue               number,
    PerformWildCardSearch       boolean
  );
  type udt_FilterList is table of udt_FilterSearch
  index by binary_integer;


  type udt_JobSearch is record (
    Project               udt_Id,
    SeqNum                number,
    ExternalFileNum       udt_ExternalFileNum,
    Status                varchar2(50),
    CreatedStartDate      date,
    CreatedEndDate        date,
    IssuedStartDate       date,
    IssuedEndDate         date,
    CompletedStartDate    date,
    CompletedEndDate      date
  );
  type udt_JobSearchList is table of udt_JobSearch
  index by binary_integer;


  /*--------------------------------------------------------------------------
   * Globals - PRIVATE
   *------------------------------------------------------------------------*/
  g_ResultTable                         udt_ObjectTable;
  g_KeyWordsList                        udt_KeyWordsSearchList;
  g_IndexSearch                         udt_IndexSearchList;
  g_RelatedIndexSearch                  udt_RelatedSearchList;
  g_AddressSearchList                   udt_AddressSearchList;
  g_RelatedAddressSearchList            udt_RelatedAddressSearchList;
  g_JobSearchList                       udt_JobSearchList;
  g_SystemSearchList                    udt_SystemSearchList;
  g_FilterList                          udt_FilterList;
  g_FirstSearch                         char(1) := 'Y';
  g_ObjectDefId                         udt_Id;
  g_ObjectDefName                       varchar2(100);
  g_ObjectDefTypeId                     udt_Id;
  g_ObjectDefTypeName                   varchar2(100);
  g_CRLF                                varchar2(2) := chr(13) || chr(10);

  /*--------------------------------------------------------------------------
   * ApplyFilter() - PRIVATE
   *------------------------------------------------------------------------*/
  procedure ApplyFilter (
    a_ResultsObjects                    in out udt_ObjectTable
  ) is
    t_ColumnName                        api.ColumnDefs.Name%TYPE;
    t_FromValue                         varchar2(4000);
    t_ToValue                           varchar2(4000);
    t_DateFromValue                     date;
    t_DateToValue                       date;
    t_NumberFromValue                   number;
    t_NumberToValue                     number;
    t_WildCard                          boolean;
    t_Value                             varchar2(4000);
    t_DateValue                         date;
    t_NumberValue                       number;
    t_Index                             number;
    t_FilteredObjects                   udt_ObjectTable;
    t_FilteredOut                       boolean;
    t_DataType                          api.ColumnDefs.Name%TYPE;
  begin

    if g_FilterList.count > 0 then

      /* Loop through all of the objects */
      t_Index := a_ResultsObjects.first;
      while t_Index is not null loop

        /* Apply all of the filters to this object */
        t_FilteredOut := false;
        for i in 1..g_FilterList.count loop

          t_ColumnName := g_FilterList(i).ColumnName;
          t_WildCard := g_FilterList(i).PerformWildCardSearch;
          t_DataType := g_FilterList(i).DataType;
          if t_DataType = 'date' then

            /* Date filter */
            t_DateFromValue := g_FilterList(i).DateFromValue;
            t_DateToValue := g_FilterList(i).DateToValue;
            t_DateValue := api.pkg_ColumnQuery.DateValue(t_Index, t_ColumnName);
            if t_DateValue is null or not (t_DateValue between t_DateFromValue and t_DateToValue) then
              t_FilteredOut := true;
              exit;
            end if;

          elsif t_DataType = 'number' then

            /* Numeric Filter */
            t_NumberFromValue := g_FilterList(i).NumberFromValue;
            t_NumberToValue := g_FilterList(i).NumberToValue;
            t_NumberValue := api.pkg_ColumnQuery.NumericValue(t_Index, t_ColumnName);
            if t_NumberValue is null or not (t_NumberValue between t_NumberFromValue and t_NumberToValue) then
              t_FilteredOut := true;
              exit;
            end if;

          else

            t_FromValue := g_FilterList(i).FromValue;
            t_Value := upper(api.pkg_ColumnQuery.Value(t_Index, t_ColumnName));
            if g_FilterList(i).PerformWildCardSearch then

              /* String with Wildcard */
              if t_Value is null or not (t_Value like t_FromValue || '%') then
                t_FilteredOut := true;
                exit;
              end if;

            else

              t_ToValue := nvl(g_FilterList(i).ToValue, t_FromValue);
              /* String range */
              if t_Value is null or not (t_Value between t_FromValue and t_ToValue) then
                t_FilteredOut := true;
                exit;
              end if;

            end if;

          end if;

        end loop;

        if not t_FilteredOut then

          /* Keep this row in the results */
          t_FilteredObjects(t_Index) := 'Y';

        end if;

        t_Index := a_ResultsObjects.next(t_Index);

      end loop;

      a_ResultsObjects := t_FilteredObjects;

    end if;

  end;

  /*--------------------------------------------------------------------------
   * FilterObjects() - PUBLIC
   *------------------------------------------------------------------------*/
  procedure FilterObjects (
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  )is
  begin
    FilterObjects(
      a_ColumnName,
      to_char(a_FromValue, api.pkg_Definition.gc_DateFormat),
      to_char(a_ToValue, api.pkg_Definition.gc_DateFormat), false);
  end;

  /*--------------------------------------------------------------------------
   * FilterObjects() - PUBLIC
   *------------------------------------------------------------------------*/
  procedure FilterObjects (
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  )is
   t_Count                              number;
   t_DataType                           api.ColumnDefs.DataType%TYPE;
  begin
    if a_FromValue is not null then
      select cd.DataType
        into t_DataType
        from api.ColumnDefs cd
        where cd.ObjectDefId = g_ObjectDefId
          and cd.name = a_ColumnName;

      t_Count := g_FilterList.count + 1;
      g_FilterList(t_count).ColumnName := a_ColumnName;
      g_FilterList(t_count).DataType := t_DataType;

      if t_DataType = 'date' then

        g_FilterList(t_count).DateFromValue := to_date(a_FromValue, api.pkg_Definition.gc_DateFormat);
        g_FilterList(t_count).DateToValue := to_date(nvl(a_ToValue, a_FromValue), api.pkg_Definition.gc_DateFormat);
        g_FilterList(t_count).PerformWildCardSearch := false;

      elsif t_DataType = 'number' then

        g_FilterList(t_count).NumberFromValue := a_FromValue;
        g_FilterList(t_count).NumberToValue := nvl(a_ToValue, a_FromValue);
        g_FilterList(t_count).PerformWildCardSearch := false;

      else

        g_FilterList(t_count).FromValue := upper(a_FromValue);
        g_FilterList(t_count).ToValue := upper(a_ToValue);
        g_FilterList(t_count).PerformWildCardSearch := a_PerformWildCardSearch;

      end if;

    end if;
  end;


  /*--------------------------------------------------------------------------
   * SearchByJob() - PUBLIC
   *------------------------------------------------------------------------*/
  procedure SearchByJob (
    a_Project                           udt_Id,
    a_SeqNum                            number,
    a_ExternalFileNum                   udt_ExternalFileNum,
    a_Status                            varchar2,
    a_CreatedStartDate                  date,
    a_CreatedEndDate                    date,
    a_IssuedStartDate                   date,
    a_IssuedEndDate                     date,
    a_CompletedStartDate                date,
    a_CompletedEndDate                  date
  )is
  t_count                               number;
  begin

    if g_ObjectDefTypeName <> 'JobTypes' then
      api.pkg_Errors.RaiseError(-20000, 'SearchByJob: ' || g_ObjectDefName || ' is not a Job');
    end if;

    if a_Project is not null or
       a_SeqNum  is not null or
       a_ExternalFileNum  is not null or
       a_Status  is not null or
       a_CreatedStartDate  is not null or
       a_CreatedEndDate  is not null or
       a_IssuedStartDate    is not null or
       a_IssuedEndDate    is not null or
       a_CompletedStartDate  is not null or
       a_CompletedEndDate  is not null then

      t_count := g_JobSearchList.count + 1;
      g_JobSearchList(t_count).Project := a_Project;
      g_JobSearchList(t_count).SeqNum := a_SeqNum;
      g_JobSearchList(t_count).ExternalFileNum  := upper(a_ExternalFileNum);
      g_JobSearchList(t_count).Status := a_Status;
      g_JobSearchList(t_count).CreatedStartDate := a_CreatedStartDate;
      g_JobSearchList(t_count).CreatedEndDate := a_CreatedEndDate;
      g_JobSearchList(t_count).IssuedStartDate := a_IssuedStartDate;
      g_JobSearchList(t_count).IssuedEndDate := a_IssuedEndDate;
      g_JobSearchList(t_count).CompletedStartDate := a_CompletedStartDate;
      g_JobSearchList(t_count).CompletedEndDate := a_CompletedEndDate;
    end if;
  end;

  /*--------------------------------------------------------------------------
   * ConvertToList() -- PRIVATE
   *------------------------------------------------------------------------*/
  function ConvertToList (
    a_Table                             udt_ObjectTable
  ) return udt_IdList is
    t_TempList                          udt_IdList;
    t_Index                             number;
    t_count                             number default 0;
  begin
    t_Index := a_Table.first;
    while t_Index is not null loop
      t_count := t_count + 1;
      t_TempList(t_count) := t_Index;
      t_Index := a_Table.next(t_Index);
    end loop;
    return t_TempList;
  end;

 /*---------------------------------------------------------------------------
  * GetEndPointId -- PRIVATE
  *-------------------------------------------------------------------------*/
  function GetEndPointId (
    a_EndPointName                      varchar2,
    a_IsOtherEndPoint                   boolean default false
  ) return udt_Id is
    t_EndPointId                        udt_Id;
  begin
    if not a_IsOtherEndPoint then
      t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(g_ObjectDefId,
          a_EndPointName);
    else
      t_EndPointId := api.pkg_ConfigQuery.OtherEndPointIdForName(g_ObjectDefId,
          a_EndPointName);
    end if;

    if t_EndPointId is null then
      api.pkg_Errors.RaiseError(-20000, 'End Point Name "'||a_EndPointName||
          '" not found for object type "' ||g_ObjectDefName||'"');
    end if;

    return t_EndPointId;
  end;

  /*---------------------------------------------------------------------------
   * SearchByIndex() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByIndex (
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  ) is
    t_FromValue                         date;
    t_ToValue                           date;
  begin

    if a_FromValue is not null or a_ToValue is not null then
      if a_FromValue is null then
        t_FromValue := to_date('19000101', 'YYYYMMDD');
      else
        t_FromValue := a_FromValue;
      end if;
      if a_ToValue is null then
        t_ToValue := to_date('39991231', 'YYYYMMDD');
      else
        t_ToValue := a_ToValue;
      end if;
    end if;

     SearchByIndex (a_ColumnName,
       to_char(t_FromValue, api.pkg_Definition.gc_DateFormat),
       to_char(t_ToValue, api.pkg_Definition.gc_DateFormat), false);
  end;

  /*---------------------------------------------------------------------------
   * SearchByIndex() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByIndex (
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean default false
  )is
    t_Count                             number;
    t_ColumnDefId                       udt_Id;
    t_IndexDefId                        udt_Id;
    t_DataType                          varchar2(10);
  begin
    begin

      select a.ColumnDefId
        into t_ColumnDefId
        from api.ColumnDefs a
       where a.ObjectDefId = g_ObjectDefId
         and a.Name = a_ColumnName;

      select a.IndexDefId
        into t_IndexDefId
        from api.IndexDefColumns a
       where a.ColumnDefId = t_ColumnDefId;

    exception
      when no_data_found then
        api.pkg_Errors.Clear();
        api.pkg_Errors.RaiseError(-20000, 'Could not find an index named ' || a_ColumnName || ' on Object ' || g_ObjectDefName || '.');
      when too_many_rows then
        api.pkg_Errors.Clear();
        api.pkg_Errors.RaiseError(-20000, 'More than one index named ' || a_ColumnName || ' on Object ' || g_ObjectDefName || '.');
    end;

    select a.DataType
      into t_DataType
      from api.ColumnDefs a
     where a.ObjectDefId = g_ObjectDefId
       and a.Name = a_ColumnName;

    /* a voir  */
    if t_DataType = 'list' and a_PerformWildCardSearch then
      api.pkg_Errors.RaiseError(-20000, 'You can not Perform a WildCardSearch on a list column');
    end if;
    if a_FromValue is not null then
      t_Count := g_IndexSearch.count + 1;
      g_IndexSearch(t_Count).ColumnName := a_ColumnName;
      g_IndexSearch(t_Count).IndexDef := t_IndexDefId;
      g_IndexSearch(t_Count).FromIndexValue := a_FromValue;
      g_IndexSearch(t_Count).ToIndexValue := a_ToValue;

      -- ignore wildcard flag for anything other than char data type
      if t_DataType = 'char' then
        g_IndexSearch(t_Count).PerformWildCardSearch := a_PerformWildCardSearch;
      else
        g_IndexSearch(t_Count).PerformWildCardSearch := false;
      end if;
    end if;

  end;


  /*---------------------------------------------------------------------------
   * SearchBySystemColumn() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchBySystemColumn (
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  ) is
    t_FromValue                         varchar2(100) := a_FromValue;
    t_ToValue                           varchar2(100) := a_ToValue;
    t_SQL                               varchar2(4000);
    t_Count                             binary_integer;
    t_Parameters                        varchar2(50);
  begin
    if a_FromValue is not null then
      if g_ObjectDefTypeName = 'ObjectTypes' then
        t_SQL :=
            'Select a.ObjectId ' ||
            '  from api.objects a ' ||
            '  where a.ObjectDefId = :ObjectDefId ';
      elsif g_ObjectDefTypeName = 'JobTypes' then
        t_SQL :=
            'Select a.JobId ' ||
            '  from api.Jobs a' ||
            '  where a.JobTypeId = :ObjectDefId ';
      elsif g_ObjectDefTypeName = 'ProcessTypes' then
        t_SQL :=
            'Select a.ProcessId ' ||
            '  from api.Processes a ' ||
            '  where a.ProcessTypeId = :ObjectDefId ';
      else
        api.pkg_Errors.RaiseError(-20000, 'SearchBySystemColumn: ' || g_ObjectDefTypeName || ' are not supported');
      end if;

      if a_PerformWildCardSearch then
        t_FromValue := a_FromValue || '%';
        t_SQL := t_SQL ||
            '    and a.' || a_ColumnName || ' like  :FromValue ';


        t_Parameters := g_ObjectDefId || ',' || t_FromValue;

      else
        t_SQL := t_SQL ||
            '    and a.' || a_ColumnName || ' >= :FromValue ' ||
            '    and a.' || a_ColumnName || ' <= :ToValue ';


        t_Parameters := g_ObjectDefId || ',' || t_FromValue || ',' || t_ToValue;

      end if;

      t_Count := g_SystemSearchList.count + 1;
      g_SystemSearchList(t_Count).Stat_Sql := t_SQL;
      g_SystemSearchList(t_Count).Stat_Parameters  := t_Parameters;
    end if;
  end;


  /*---------------------------------------------------------------------------
   * SearchBySystemColumn() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchBySystemColumn (
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  ) is
    t_FromValue                         date;
    t_ToValue                           date;
    t_SQL                               varchar2(4000);
    t_Count                             binary_integer;
    t_Parameters                        varchar2(50);
    c_DateFormat			varchar2(50) :=
        api.pkg_Definition.gc_DateFormat;
  begin

    if a_FromValue is null then
      t_FromValue := to_date('19000101', 'YYYYMMDD');
    else
      t_FromValue := a_FromValue;
    end if;
    if a_ToValue is null then
      t_ToValue := to_date('39991231', 'YYYYMMDD');
    else
      t_ToValue := a_ToValue;
    end if;

    if a_FromValue is not null then

      if g_ObjectDefTypeName = 'ObjectTypes' then
        t_SQL :=
            'Select a.ObjectId ' ||
            '  from api.objects a ' ||
            '  where a.ObjectDefId = :ObjectDefId ';
      elsif g_ObjectDefTypeName = 'JobTypes' then
        t_SQL :=
            'Select a.JobId ' ||
            '  from api.Jobs a' ||
            '  where a.JobTypeId = :ObjectDefId ';
      elsif g_ObjectDefTypeName = 'ProcessTypes' then
        t_SQL :=
            'Select a.ProcessId ' ||
            '  from api.Processes a ' ||
            '  where a.ProcessTypeId = :ObjectDefId ';
      else
        api.pkg_Errors.RaiseError(-20000, 'SearchBySystemColumn: ' || g_ObjectDefTypeName || ' are not supported');
      end if;

      t_SQL := t_SQL ||
          '    and a.' || a_ColumnName || ' >= to_date(:FromValue, ''' ||
          c_DateFormat || ''') ' ||
          '    and a.' || a_ColumnName || ' <= to_date(:ToValue, ''' ||
          c_DateFormat || ''') ';

      t_Parameters := g_ObjectDefId || ',' || to_char(t_FromValue, c_DateFormat) || ',' || to_char(t_ToValue, c_DateFormat);

      t_Count := g_SystemSearchList.count + 1;

      g_SystemSearchList(t_Count).Stat_Sql := t_SQL;
      g_SystemSearchList(t_Count).Stat_Parameters  := t_Parameters;

    end if;

  end;



    procedure ReverseResults ( a_Objects in out nocopy api.udt_ObjectList) is

    t_templist api.udt_ObjectList;

    begin
    t_templist := a_Objects;
    if a_Objects.count <> 0 then
    for x in 1..a_Objects.count loop

        a_Objects(a_Objects.count-x+1) := t_templist(x);

    end loop;


    end if;
    end;


  /*---------------------------------------------------------------------------
   * SearchByNullDate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByNullDate (
    a_ColumnName                        varchar2
  ) is
    t_SQL                               varchar2(4000);
    t_Count                             binary_integer;
  begin

    if g_ObjectDefTypeName = 'ObjectTypes' then
      t_SQL :=
          'Select a.ObjectId ';
    elsif g_ObjectDefTypeName = 'JobTypes' then
      t_SQL :=
          'Select a.JobId ';
    elsif g_ObjectDefTypeName = 'ProcessTypes' then
      t_SQL :=
          'Select a.ProcessId ';
    else
      api.pkg_Errors.RaiseError(-20000, 'SearchByNullDate: ' || g_ObjectDefTypeName || ' are not supported');
    end if;


    t_SQL := t_SQL ||
        '  from query.' || g_ObjectDefName || ' a ' ||
        ' where a.' || a_ColumnName || ' is null ';


    t_Count := g_SystemSearchList.count + 1;

    g_SystemSearchList(t_Count).Stat_Sql := t_SQL;


  end;

  /*---------------------------------------------------------------------------
   * SearchByRelatedIndex -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure SearchByRelatedIndex (
    a_EndPointId                        udt_Id,
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  ) is
    t_IndexDefId                        udt_Id;
    t_count                             pls_integer;
    t_DataType                          varchar2(50);
    t_ObjectDefName                     varchar2(100);
    t_ObjectDefId                       udt_Id;
    t_ColumnDefId                       udt_Id;
    t_FromEndPointId                    udt_Id;
  begin

    if a_FromValue is not null then
      select
        o.ObjectDefId,
        o.Name,
        r.FromEndPointId
      into
        t_ObjectDefId,
        t_ObjectDefName,
        t_FromEndPointId
      from
        api.RelationshipDefs r,
        api.ObjectDefs o
      where r.FromObjectDefId = g_ObjectDefId
        and r.ToEndPointId = a_EndPointId
        and o.ObjectDefId = r.ToObjectDefId;

      begin
        select a.ColumnDefId,
               a.DataType
          into t_ColumnDefId,
               t_DataType
          from api.ColumnDefs a
         where a.ObjectDefId = t_ObjectDefId
           and a.Name = a_ColumnName;

        select a.IndexDefId
          into t_IndexDefId
          from api.IndexDefColumns a
         where a.ColumnDefId = t_ColumnDefId;
      exception
      when no_data_found then
        api.pkg_Errors.Clear();
        api.pkg_Errors.RaiseError(-20000, 'SearchByRelatedIndex: Index ' ||
            t_ObjectDefName || '.' || a_ColumnName || ' not found');
      when too_many_rows then
        api.pkg_Errors.Clear();
        api.pkg_Errors.RaiseError(-20000, 'SearchByRelatedIndex: More than one Index ' || a_ColumnName ||
            ' on ObjectDef ' || t_ObjectDefName || ' found!');
      end;

      if  t_DataType = 'list' and a_PerformWildCardSearch then
        api.pkg_Errors.RaiseError(-20000, 'SearchByRelatedIndex: You cannot perform a WildCardSearch on ' ||
            t_ObjectDefName || '.' || a_ColumnName || ' because it is a list column');
      end if;

      t_Count := g_RelatedIndexSearch.count + 1;
      g_RelatedIndexSearch(t_Count).ColumnName := a_ColumnName;
      g_RelatedIndexSearch(t_Count).IndexDef := t_IndexDefId;
      g_RelatedIndexSearch(t_Count).EndPointId := t_FromEndPointId;
      g_RelatedIndexSearch(t_Count).ObjectDefName := t_ObjectDefName;
      g_RelatedIndexSearch(t_Count).ObjectDefId := t_ObjectDefId;
      g_RelatedIndexSearch(t_Count).FromIndexValue := a_FromValue;
      g_RelatedIndexSearch(t_Count).ToIndexValue := a_ToValue;

      -- ignore wildcard flag for anything other than char data type
      if t_DataType = 'char' then
        g_RelatedIndexSearch(t_Count).PerformWildCardSearch := a_PerformWildCardSearch;
      else
        g_RelatedIndexSearch(t_Count).PerformWildCardSearch := false;
      end if;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * SearchByRelatedIndex() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelatedIndex (
    a_EndPointName                      varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  ) is
    t_EndPointId                        udt_Id;
  begin
    t_EndPointId := GetEndPointId(a_EndPointName);
    SearchByRelatedIndex(t_EndPointId, a_ColumnName, a_FromValue, a_ToValue,
        a_PerformWildCardSearch);
  end;

  /*---------------------------------------------------------------------------
   * SearchByRelatedIndex() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelatedIndex (
    a_EndPointName                      varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  ) is
    t_FromValue                         date;
    t_ToValue                           date;
  begin

    if a_FromValue is not null or a_ToValue is not null then
      if a_FromValue is null then
        t_FromValue := to_date('19000101', 'YYYYMMDD');
      else
        t_FromValue := a_FromValue;
      end if;
      if a_ToValue is null then
        t_ToValue := to_date('39991231', 'YYYYMMDD');
      else
        t_ToValue := a_ToValue;
      end if;
    end if;

    SearchByRelatedIndex(a_EndPointName, a_ColumnName,
        to_char(t_FromValue, api.pkg_Definition.gc_DateFormat),
        to_char(t_ToValue, api.pkg_Definition.gc_DateFormat), false);
  end;

  /*---------------------------------------------------------------------------
   * SearchByRelIndexOtherEndPoint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelIndexOtherEndPoint (
    a_OtherEndPointName                 varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2,
    a_PerformWildCardSearch             boolean
  ) is
    t_EndPointId                        udt_Id;
  begin
    t_EndPointId := GetEndPointId(a_OtherEndPointName, true);
    SearchByRelatedIndex(t_EndPointId, a_ColumnName, a_FromValue, a_ToValue,
        a_PerformWildCardSearch);
  end;

  /*---------------------------------------------------------------------------
   * SearchByRelIndexOtherEndPoint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelIndexOtherEndPoint (
    a_OtherEndPointName                 varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         date,
    a_ToValue                           date
  ) is
    t_FromValue                         date;
    t_ToValue                           date;
  begin

    if a_FromValue is not null or a_ToValue is not null then
      if a_FromValue is null then
        t_FromValue := to_date('19000101', 'YYYYMMDD');
      else
        t_FromValue := a_FromValue;
      end if;
      if a_ToValue is null then
        t_ToValue := to_date('39991231', 'YYYYMMDD');
      else
        t_ToValue := a_ToValue;
      end if;
    end if;

    SearchByRelIndexOtherEndPoint(a_OtherEndPointName, a_ColumnName,
        to_char(t_FromValue, api.pkg_Definition.gc_DateFormat),
        to_char(t_ToValue, api.pkg_Definition.gc_DateFormat), false);
  end;

  /*---------------------------------------------------------------------------
   * SearchByRelatedAddress() -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure SearchByRelatedAddress (
    a_EndPointId                        udt_Id,
    a_FromHouseNumber                   number,
    a_ToHouseNumber                     number,
    a_HouseSuffix                       varchar2,
    a_StreetName                        varchar2,
    a_Suite                             varchar2,
    a_ExactMatch                        varchar2
  )is
   t_Count                              number;
   t_ObjectDefName                      varchar2(100);
   t_ObjectDefId                        number;
  begin
    if a_FromHouseNumber is not null
        or a_ToHouseNumber is not null
        or a_HouseSuffix is not null
        or a_StreetName is not null  then

      select
        o.ObjectDefId,
        o.Name
      into
        t_ObjectDefId,
        t_ObjectDefName
      from
        api.RelationshipDefs r,
        api.ObjectDefs o
      where r.FromObjectDefId = g_ObjectDefId
        and r.ToEndPointName = a_EndPointId
        and o.ObjectDefId = r.ToObjectDefId;

      t_Count := g_AddressSearchList.count + 1;
      g_RelatedAddressSearchList(t_Count).EndPointId := a_EndPointId;
      g_RelatedAddressSearchList(t_Count).ObjectName := t_ObjectDefName;
      g_RelatedAddressSearchList(t_Count).AddressFromHouseNumber := a_FromHouseNumber;
      g_RelatedAddressSearchList(t_Count).AddressToHouseNumber :=  a_ToHouseNumber;
      g_RelatedAddressSearchList(t_Count).AddressHouseSuffix := a_HouseSuffix;
      g_RelatedAddressSearchList(t_Count).AddressStreetName := a_StreetName;
      g_RelatedAddressSearchList(t_Count).AddressHouseSuite := a_Suite;
      g_RelatedAddressSearchList(t_Count).AddressExactMatch := a_ExactMatch;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * SearchByRelatedAddress() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelatedAddress (
    a_EndPointName                      varchar2,
    a_FromHouseNumber                   number,
    a_ToHouseNumber                     number,
    a_HouseSuffix                       varchar2,
    a_StreetName                        varchar2,
    a_Suite                             varchar2,
    a_ExactMatch                        varchar2
  ) is
    t_EndPointId                        udt_Id;
  begin
    t_EndPointId := GetEndPointId(a_EndPointName);
    SearchByRelatedAddress(t_EndPointId, a_FromHouseNumber, a_ToHouseNumber,
        a_HouseSuffix, a_StreetName, a_Suite, a_ExactMatch);
  end;


  /*---------------------------------------------------------------------------
   * SearchByRelAddrOtherEndPoint() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByRelAddrOtherEndPoint (
    a_OtherEndPointName                 varchar2,
    a_FromHouseNumber                   number,
    a_ToHouseNumber                     number,
    a_HouseSuffix                       varchar2,
    a_StreetName                        varchar2,
    a_Suite                             varchar2,
    a_ExactMatch                        varchar2
  ) is
    t_EndPointId                        udt_Id;
  begin
    t_EndPointId := GetEndPointId(a_OtherEndPointName, true);
    SearchByRelatedAddress(t_EndPointId, a_FromHouseNumber, a_ToHouseNumber,
        a_HouseSuffix, a_StreetName, a_Suite, a_ExactMatch);
  end;


  /*---------------------------------------------------------------------------
   * SearchByAddress() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByAddress (
    a_FromHouseNumber                   number,
    a_ToHouseNumber                     number,
    a_HouseSuffix                       varchar2,
    a_StreetName                        varchar2,
    a_Suite                             varchar2,
    a_ExactMatch                        varchar2
  )is
    t_Count                             number;
  begin
    --Write out the Parameters passed
    pkg_debug.putline('a_FromHouseNumber=' || a_FromHouseNumber);
    pkg_debug.putline('a_ToHouseNumber=' || a_ToHouseNumber);
    pkg_debug.putline('a_HouseSuffix=' || a_HouseSuffix);
    pkg_debug.putline('a_StreetName=' || a_StreetName);
    pkg_debug.putline('a_Suite=' || a_Suite);
    pkg_debug.putline('a_ExactMatch=' || a_ExactMatch);       
      
    if a_FromHouseNumber is not null
        or a_ToHouseNumber is not null
        or a_HouseSuffix is not null
        or a_StreetName is not null  then
     t_Count := g_AddressSearchList.count + 1;
     g_AddressSearchList(t_Count).AddressFromHouseNumber := a_FromHouseNumber;
     g_AddressSearchList(t_Count).AddressToHouseNumber :=  a_ToHouseNumber;
     g_AddressSearchList(t_Count).AddressHouseSuffix := a_HouseSuffix;
     g_AddressSearchList(t_Count).AddressStreetName := a_StreetName;
     g_AddressSearchList(t_Count).AddressHouseSuite := a_Suite;
     g_AddressSearchList(t_Count).AddressExactMatch := a_ExactMatch;
   end if;
  end;


  /*---------------------------------------------------------------------------
   * InitializeSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure InitializeSearch (
   a_ObjectDefName                      varchar2
  ) is
  begin
    begin
      select a.ObjectDefId,
             a.ObjectDefTypeId,
             b.Name
        into g_ObjectDefId,
             g_ObjectDefTypeId,
             g_ObjectDefTypeName
        from api.ObjectDefs a,
             api.ObjectDefTypes b
        where a.Name = a_ObjectDefName
          and b.ObjectDefTypeId = a.ObjectDefTypeId;
    exception
    when no_data_found then
      api.pkg_Errors.RaiseError(-20000, 'Initialize Search: Cannot find ObjectDefId for ' || a_ObjectDefName );
    end;

    g_ObjectDefName := a_ObjectDefName;
    g_IndexSearch.delete;
    g_RelatedIndexSearch.delete;
    g_AddressSearchList.delete;
    g_RelatedAddressSearchList.delete;
    g_JobSearchList.delete;
    g_FilterList.delete;
    g_ResultTable.delete;
    g_SystemSearchList.delete;
    g_KeyWordsList.delete;
    g_FirstSearch := 'Y';
  end;

  /*---------------------------------------------------------------------------
   * SearchByKeywords() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SearchByKeywords (
    a_Keywords                          varchar2
  ) is
    t_Count                             pls_integer;
  begin
    if a_Keywords is not null and trim(a_Keywords) <> '%' then
      t_Count := g_KeyWordsList.count + 1;
      g_KeyWordsList(t_Count).Keywords := a_Keywords;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * PerformSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PerformSearch (
    a_Objects                           in out nocopy api.udt_ObjectList,
    a_SearchType                        varchar2 default 'or',
    a_AppendResultsType                 varchar2 default 'none'
  ) is
    t_FromValue                         udt_RawQueryValueList;
    t_ToValue                           udt_RawQueryValueList;
    t_RawFound                          number;
    t_TempObjectTable                   udt_ObjectTable;
    t_TempObjectList                    udt_IdList;
    t_IndexDef                          udt_Id;
    t_Index                             number;
    t_EndpointId                        number;
    t_TempList                          udt_IdList;
    t_ObjectList                        udt_IdList;
    t_WildCard                          boolean;
    t_ColumnDef                         udt_IdList;
    t_FromHouseNumber                   udt_HouseNumber;
    t_ToHouseNumber                     udt_HouseNumber;
    t_HouseSuffix                       udt_HouseSuffix;
    t_StreetName                        varchar2(100);
    t_Suite                             varchar2(50);
    t_ExactMatch                        udt_Boolean;
    t_ObjectDefList                     udt_IdList;
    t_Count                             pls_integer;
    c_Objects                           udt_Cursor;
    t_SQL                               varchar2(4000);
    t_Parameters                        varchar2(50);
    t_Pos                               integer;
    t_ObjectDefId                       udt_Id;
    t_FromVal                           varchar2(100);
    t_ToVal                             varchar2(100);
    t_Keywords                          varchar2(100);

  begin
    pkg_Debug.PutLine('pkg_CxProceduralSearch.PerformSearch');
    /* Ensure the parameters are valid. */
    if lower(a_AppendResultsType) not in ('none','and','or') then
      api.pkg_Errors.RaiseError(-20000, 'a_AppendResultsType must be one of: '||
          '''none'', ''and'', ''or''');
    end if;

    if lower(a_SearchType) not in ('and','or') then
      api.pkg_Errors.RaiseError(-20000, 'a_SearchType must be one of: '||
          '''and'', ''or''');
    end if;

    /* Perform keyword searches */
    for i in 1..g_KeyWordsList.count loop

      pkg_Debug.PutLine('Searching by keywords ' || t_Keywords);

      t_ObjectDefList(1) := g_ObjectDefId;
      t_Keywords := g_KeyWordsList(i).Keywords;

      t_TempObjectList := api.pkg_SimpleSearch.ObjectsByKeywords(g_ObjectDefName, t_Keywords);
/*      api.pkg_ObjectSearch.InitializeSearch();
      api.pkg_ObjectSearch.FilterObjects(t_ObjectDefList, sysdate, sysdate);
      api.pkg_ObjectSearch.SearchByKeywords(t_Keywords);

      t_RawFound := api.pkg_ObjectSearch.PerformSearch();
      for i in (select ObjectId
                from api.TemporaryObjectLists) loop
        t_TempObjectTable(i.ObjectId) := 'Y';
      end loop;
*/
      pkg_Debug.PutLine(to_char(t_TempObjectList.count) || ' rows found by this search.');
      if lower(a_SearchType) = 'and' and t_TempObjectList.count = 0 then
        g_ResultTable.delete;
        GOTO appendlist;
      end if;

      if g_FirstSearch = 'Y' or lower(a_SearchType) = 'or' then
        pkg_CollectionUtils.Append(g_ResultTable, t_TempObjectList);
      else
        pkg_CollectionUtils.Join(g_ResultTable, t_TempObjectList);
      end if;

      g_FirstSearch := 'N';
      t_TempObjectTable.delete;
      t_FromValue.delete;
      t_ToValue.delete;
      t_ColumnDef.delete;
    end loop;

    /* Perform system column searches */
    for i in 1..g_SystemSearchList.count loop

      t_SQL := g_SystemSearchList(i).Stat_Sql;
      t_Parameters := g_SystemSearchList(i).Stat_Parameters;
      t_Pos := instr(t_Parameters, ',');
      if t_Pos > 0 then
        t_ObjectDefId := substr(t_Parameters, 0, t_Pos - 1);
        t_Parameters := substr(t_Parameters, t_Pos + 1);
        t_Pos := instr(t_Parameters, ',');
        if t_Pos > 0 then
          t_FromVal := substr(t_Parameters, 0, t_Pos - 1);
          t_ToVal := substr(t_Parameters, t_Pos + 1);
          open c_Objects for t_SQL using t_ObjectDefId, t_FromVal, t_ToVal;
        else
          t_FromVal := substr(t_Parameters, t_Pos + 1);
          open c_Objects for t_SQL using t_ObjectDefId, t_FromVal;
        end if;
      else
        open c_Objects for t_SQL;
      end if;

      /* Now do the search */
      t_Count := 0;
      loop
        t_Count := t_Count + 1;
        fetch c_Objects into t_TempObjectList(t_Count);
        exit when c_Objects%notfound;
      end loop;
      close c_Objects;

      if lower(a_SearchType) = 'and' and t_TempObjectList.count = 0 then
        g_ResultTable.delete;
        GOTO appendlist;
      end if;

      if g_FirstSearch = 'Y' or lower(a_SearchType) = 'or' then
        pkg_CollectionUtils.Append(g_ResultTable, t_TempObjectList);
      else
        pkg_CollectionUtils.Join(g_ResultTable, t_TempObjectList);
      end if;

      g_FirstSearch := 'N';
      t_TempObjectTable.delete;
      t_TempObjectList.delete;
    end loop;

    /* Perform index searches */
    for i in 1..g_IndexSearch.count loop
      t_FromValue(1) := g_IndexSearch(i).FromIndexValue;
      t_ToValue(1)   := g_IndexSearch(i).ToIndexValue;
      t_IndexDef     := g_IndexSearch(i).IndexDef;
      t_WildCard     := g_IndexSearch(i).PerformWildCardSearch;
      t_ObjectDefList(1) := g_ObjectDefId;
      pkg_Debug.PutLine('Searching by indexdef ' || to_char(t_IndexDef));
      pkg_Debug.PutLine('From: ' || t_FromValue(1));
      pkg_Debug.PutLine('To: ' || t_ToValue(1));
      if t_WildCard then
        pkg_Debug.PutLine('Wild Card: true');
      else
        pkg_Debug.PutLine('Wild Card: false');
      end if;

      if t_WildCard then
        g_IndexSearch(i).FromIndexValue := g_IndexSearch(i).FromIndexValue || '%';
        g_IndexSearch(i).ToIndexValue := g_IndexSearch(i).ToIndexValue || '%';
      end if;
      t_TempObjectList := api.pkg_SimpleSearch.ObjectsByIndex(g_ObjectDefname,
          g_IndexSearch(i).ColumnName, g_IndexSearch(i).FromIndexValue,
          g_IndexSearch(i).ToIndexValue);
      pkg_Debug.PutLine(to_char(t_TempObjectList.count) || ' rows found by this search.');
      /*api.pkg_objectsearch.InitializeSearch();
      api.pkg_ObjectSearch.FilterObjects(t_ObjectDefList, sysdate, sysdate);

      api.pkg_objectsearch.SearchByIndex(t_IndexDef, t_FromValue, t_ToValue, t_WildCard);

      t_RawFound := api.pkg_objectsearch.PerformSearch();
      pkg_Debug.PutLine(to_char(t_RawFound) || ' rows found by this search.');

      for i in (select ObjectId
                  from api.TemporaryObjectLists) loop
        t_TempObjectTable(i.ObjectId) := 'Y';
      end loop; */

      if lower(a_SearchType) = 'and' and t_TempObjectList.count = 0 then
        pkg_Debug.PutLine('Since no rows were found on "and" search, deleting everything');
        g_ResultTable.delete;
        GOTO appendlist;
      end if;

      if g_FirstSearch = 'Y' or lower(a_SearchType) = 'or' then
        pkg_CollectionUtils.Append(g_ResultTable, t_TempObjectList);
      else
        pkg_CollectionUtils.Join(g_ResultTable, t_TempObjectList);
      end if;

      t_count := g_ResultTable.count;
      pkg_Debug.PutLine('After append/join, results table has ' || to_char(t_Count) || ' rows.');

      g_FirstSearch := 'N';
      t_TempObjectTable.delete;
      t_FromValue.delete;
      t_ToValue.delete;
      t_ColumnDef.delete;
      t_ObjectDefList.delete;

    end loop;

    /* Perform related index searches */
    for i in 1..g_RelatedIndexSearch.count loop

      t_FromValue(1) := g_RelatedIndexSearch(i).FromIndexValue;
      t_ToValue(1)   := g_RelatedIndexSearch(i).ToIndexValue;
      t_IndexDef     := g_RelatedIndexSearch(i).IndexDef;
      t_WildCard     := g_RelatedIndexSearch(i).PerformWildCardSearch;
      t_ObjectDefList(1) := g_RelatedIndexSearch(i).ObjectDefId;
      pkg_Debug.PutLine('Searching (related object) by indexdef ' || to_char(t_IndexDef));
      pkg_Debug.PutLine('From: ' || t_FromValue(1));
      pkg_Debug.PutLine('To: ' || t_ToValue(1));
      if t_WildCard then
        pkg_Debug.PutLine('Wild Card: true');
      else
        pkg_Debug.PutLine('Wild Card: false');
      end if;

      if t_WildCard then
        g_RelatedIndexSearch(i).FromIndexValue := g_RelatedIndexSearch(i).FromIndexValue || '%';
        g_RelatedIndexSearch(i).ToIndexValue := g_RelatedIndexSearch(i).ToIndexValue || '%';
      end if;
      t_TempObjectList := api.pkg_SimpleSearch.ObjectsByIndex(
          g_RelatedIndexSearch(i).ObjectDefName, g_RelatedIndexSearch(i).ColumnName,
          g_RelatedIndexSearch(i).FromIndexValue, g_RelatedIndexSearch(i).ToIndexValue);
      pkg_Debug.PutLine(to_char(t_TempObjectList.count) || ' rows found by this search.');
      /*api.pkg_objectsearch.InitializeSearch();
      api.pkg_ObjectSearch.FilterObjects(t_ObjectDefList, sysdate, sysdate);
      api.pkg_objectsearch.SearchByIndex(t_IndexDef, t_FromValue, t_ToValue, t_WildCard);

      t_RawFound := api.pkg_objectsearch.PerformSearch();
      pkg_Debug.PutLine(to_char(t_RawFound) || ' rows found by this search.');

      for j in (select ObjectId
                  from api.TemporaryObjectLists) loop
        t_TempObjectTable(j.ObjectId) := 'Y';

      end loop; */

      /** get Related Objects **/
      t_EndpointId := g_RelatedIndexSearch(i).EndPointId;
      pkg_Debug.PutLine('Traversing endpoint ' || to_char(t_EndPointId));

      for j in 1..t_TempObjectList.count loop
        pkg_Debug.PutLine('Getting related objects for object ' || to_char(t_TempObjectList(j)));
        t_TempList := RelatedObjects(t_TempObjectList(j), t_EndpointId, sysdate, sysdate);
        pkg_Debug.PutLine('Found ' || to_char(t_TempList.count) || ' related objects');
        pkg_CollectionUtils.Append(t_ObjectList, t_TempList);
        t_TempList.delete;
      end loop;

      /*t_Index := t_TempObjectTable.first;

      while t_Index is not null loop
        pkg_Debug.PutLine('Getting related objects for object ' || to_char(t_Index));
        t_TempList := pkg_ObjectQuery.RelatedObjects(t_Index, t_EndpointId, sysdate, sysdate);
        pkg_Debug.PutLine('Found ' || to_char(t_TempList.count) || ' related objects');

        pkg_CollectionUtils.Append(t_ObjectList, t_TempList);
        t_TempList.delete;
        t_Index := t_TempObjectTable.next(t_Index);
      end loop; */
      t_count := g_ResultTable.count;
      pkg_Debug.PutLine('Results table now has ' || to_Char(t_Count) || ' rows.');

      if lower(a_SearchType) = 'and' and t_ObjectList.count = 0 then
        pkg_Debug.PutLine('Since no rows were found on "and" search, deleting everything');
        g_ResultTable.delete;
        GOTO appendlist;
      end if;

      if g_FirstSearch = 'Y' or lower(a_SearchType) = 'or' then
        pkg_CollectionUtils.Append(g_ResultTable, t_ObjectList);
      else
        pkg_CollectionUtils.Join(g_ResultTable, t_ObjectList);
      end if;
      t_count := g_ResultTable.count;
      pkg_Debug.PutLine('After append/join, results table has ' || to_char(t_Count) || ' rows.');

      g_FirstSearch := 'N';
      t_TempObjectTable.delete;
      t_FromValue.delete;
      t_ToValue.delete;
      t_ObjectList.delete;
      t_ObjectDefList.delete;

    end loop;

    /* Perform address searches */
    for i in 1..g_AddressSearchList.count loop
      t_ObjectDefList(1) := g_ObjectDefId;
      api.pkg_ObjectSearch.InitializeSearch();
      api.pkg_ObjectSearch.FilterObjects(t_ObjectDefList, sysdate, sysdate);
      t_FromHouseNumber := g_AddressSearchList(i).AddressFromHouseNumber;
      t_ToHouseNumber  := g_AddressSearchList(i).AddressToHouseNumber;
      t_HouseSuffix  := g_AddressSearchList(i).AddressHouseSuffix;
      t_StreetName  := g_AddressSearchList(i).AddressStreetName;
      t_Suite    := g_AddressSearchList(i).AddressHouseSuite;
      t_ExactMatch  := g_AddressSearchList(i).AddressExactMatch;

      api.pkg_ObjectSearch.SearchByAddress(t_FromHouseNumber, t_ToHouseNumber, t_HouseSuffix, t_StreetName, t_Suite, t_ExactMatch);

      t_RawFound := api.Pkg_ObjectSearch.PerformSearch();

      for j in (select ObjectId
                  from api.TemporaryObjectLists) loop
        t_TempObjectTable(j.ObjectId) := 'Y';
      end loop;

      if lower(a_SearchType) = 'and' and t_TempObjectTable.count = 0 then
        g_ResultTable.delete;
        GOTO appendlist;
      end if;

      if g_FirstSearch = 'Y' or lower(a_SearchType) = 'or' then
        pkg_CollectionUtils.Append(g_ResultTable, t_TempObjectTable);
      else
        pkg_CollectionUtils.Join(g_ResultTable, t_TempObjectTable);
      end if;
      t_count := g_ResultTable.count;

      g_FirstSearch := 'N';
      t_TempObjectTable.delete;
      t_FromValue.delete;
      t_ToValue.delete;
      t_ColumnDef.delete;

    end loop;

    /* Perform related address searches */
    for i in 1..g_RelatedAddressSearchList.count loop

      t_ObjectDefList(1) := api.pkg_ObjectDefQuery.IdForName(g_RelatedAddressSearchList(i).ObjectName);
      api.pkg_ObjectSearch.InitializeSearch();
      api.pkg_ObjectSearch.FilterObjects(t_ObjectDefList, sysdate, sysdate);

      t_FromHouseNumber := g_RelatedAddressSearchList(i).AddressFromHouseNumber;
      t_ToHouseNumber  := g_RelatedAddressSearchList(i).AddressToHouseNumber;
      t_HouseSuffix  := g_RelatedAddressSearchList(i).AddressHouseSuffix;
      t_StreetName  := g_RelatedAddressSearchList(i).AddressStreetName;
      t_Suite    := g_RelatedAddressSearchList(i).AddressHouseSuite;
      t_ExactMatch  := g_RelatedAddressSearchList(i).AddressExactMatch;

      api.pkg_ObjectSearch.SearchByAddress(t_FromHouseNumber, t_ToHouseNumber, t_HouseSuffix, t_StreetName, t_Suite, t_ExactMatch);
      t_RawFound := api.Pkg_ObjectSearch.PerformSearch();

      for j in (select ObjectId
                  from api.TemporaryObjectLists) loop
        t_TempObjectTable(j.ObjectId) := 'Y';
      end loop;

      /** get Related Objects **/
      t_EndpointId := g_RelatedAddressSearchList(i).EndPointId;
      t_Index := t_TempObjectTable.first;

      while t_Index is not null loop
        t_TempList := RelatedObjects(t_Index, t_EndpointId, sysdate, sysdate);
        pkg_CollectionUtils.Append(t_ObjectList, t_TempList);
        t_TempList.delete;
        t_Index := t_TempObjectTable.next(t_Index);
      end loop;

     if lower(a_SearchType) = 'and' and t_ObjectList.count = 0 then
        g_ResultTable.delete;
        GOTO appendlist;
      end if;

      if g_FirstSearch = 'Y' or lower(a_SearchType) = 'or' then
        pkg_CollectionUtils.Append(g_ResultTable, t_ObjectList);
      else
        pkg_CollectionUtils.Join(g_ResultTable, t_ObjectList);
      end if;

      t_count := g_ResultTable.count;

      g_FirstSearch := 'N';
      t_TempObjectTable.delete;
      t_ObjectList.delete;

    end loop;

    /* Perform job searches */
    for i in 1..g_JobSearchList.count loop
      api.pkg_ObjectSearch.InitializeSearch();
      api.pkg_ObjectSearch.SearchByJob(g_JobSearchList(i).Project,
          g_JobSearchList(i).SeqNum, g_JobSearchList(i).ExternalFileNum,
          g_ObjectDefId, g_JobSearchList(i).Status,
          g_JobSearchList(i).CreatedStartDate,
          g_JobSearchList(i).CreatedEndDate,
          g_JobSearchList(i).IssuedStartDate,
          g_JobSearchList(i).IssuedEndDate,
          g_JobSearchList(i).CompletedStartDate,
          g_JobSearchList(i).CompletedEndDate);

      t_RawFound := api.Pkg_ObjectSearch.PerformSearch();

      for j in (select ObjectId
                from api.TemporaryObjectLists) loop
        t_TempObjectTable(j.ObjectId) := 'Y';
      end loop;

      if lower(a_SearchType) = 'and' and t_TempObjectTable.count = 0 then
        g_ResultTable.delete;
        GOTO appendlist;
      end if;

      if g_FirstSearch = 'Y' or lower(a_SearchType) = 'or' then
        pkg_CollectionUtils.Append(g_ResultTable, t_TempObjectTable);
      else
        pkg_CollectionUtils.Join(g_ResultTable, t_TempObjectTable);
      end if;

      t_count := g_ResultTable.count;
      g_FirstSearch := 'N';
      t_TempObjectTable.delete;

    end loop;

    /* Perform Filters */
    ApplyFilter(g_ResultTable);


    <<appendlist>>


     if lower(a_AppendResultsType) in ('and','or') then
       if g_FirstSearch = 'N' then -- the current search is performed.
         if lower(a_AppendResultsType) = 'or' then -- if it's match any.
           pkg_CollectionUtils.Append(a_Objects, g_ResultTable);
         else -- if it's match all.
           pkg_CollectionUtils.Join(a_Objects, g_ResultTable);
         end if;
       end if;
     else -- if we don't want to Append the current result to the previous results.
       a_Objects.Delete;
       pkg_CollectionUtils.Append(a_Objects, g_ResultTable);
     end if;
  end;


  /*---------------------------------------------------------------------------
   * SearchPerformed() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function SearchPerformed
    return                              boolean
  is
  begin
    if g_FirstSearch = 'Y' then
      return false;
    else
      return true;
    end if;
  end;

  /*--------------------------------------------------------------------------
   * RelatedObjects() - PUBLIC
   *------------------------------------------------------------------------*/
  function RelatedObjects(
    a_ObjectId                          udt_Id,
    a_EndPointId                        udt_Id,
    a_StartDate                         date default null,
    a_EndDate                           date default null
  ) return udt_IdList is

    TYPE udt_RefCursor is ref cursor;
    c_Objects                           udt_RefCursor;
    t_TempList                          udt_IdList;
    t_Stored                            char(1);
    t_FromColumnName                    api.RelationshipDefs.FromColumnName%Type;
    t_ToColumnName                      api.RelationshipDefs.ToColumnName%Type;
    t_Name                              api.RelationshipDefs.Name%Type;
    t_EffectiveDated                    char(1);
    t_Temp                              number;
    t_Count                             pls_integer;
    t_Done                              boolean := false;
    t_UpperName                         api.RelationshipDefs.Name%Type;
    t_SQL                               varchar2(4000);

  begin

    select a.Stored,
           a.EffectiveDated,
           a.FromColumnName,
           a.ToColumnName,
           a.Name
      into t_Stored,
           t_EffectiveDated,
           t_FromColumnName,
           t_ToColumnName,
           t_Name
      from api.RelationshipDefs a
      where a.FromEndPointId = a_EndPointId;

    /* Not going to try and deal with effective dated rels for now */
    if t_EffectiveDated = 'N' then

      /* If this is a stored rel, go against api.Relationships */
      if t_Stored = 'Y' then

        select a.ToObjectId
          bulk collect into t_TempList
          from api.Relationships a
          where a.EndPointId = a_EndPointId
            and a.FromObjectId = a_ObjectId;
        t_Done := true;

      elsif t_Name is not null and t_FromColumnName is not null and t_ToColumnName is not null then

        /* See if there is a query view */
        t_UpperName := upper(t_Name);
        select count(1)
          into t_Temp
          from all_views a
          where a.owner = 'QUERY'
            and a.view_name = t_UpperName;
        if t_Temp > 0 then

          /* Select the rows from the query view */
          t_Count := 0;
          t_SQL := 'select ' || t_FromColumnName || g_CRLF ||
                   '  from query.' || t_Name || g_CRLF ||
                   '  where ' || t_ToColumnName || ' = :a_ObjectId';
          open c_Objects for t_SQL using a_ObjectId;
          loop
            fetch c_Objects
              into t_Temp;
            exit when c_Objects%NOTFOUND;
            t_Count := t_Count + 1;
            t_TempList(t_Count) := t_Temp;
          end loop;
          t_Done := true;

        end if;

      end if;

    end if;

    if not t_Done then

      /* Give up and call api.pkg_ObjectQuery.RelatedObjects*/
      t_TempList := api.pkg_ObjectQuery.RelatedObjects(a_ObjectId, a_EndPointId, a_StartDate, a_EndDate);

    end if;

    return t_TempList;
  end;

end pkg_CxProceduralSearch;

/

