create or replace package pkg_MetaData is

  -- Author  : JIM.DENOTTER
  -- Created : 6/22/2007 4:18:15 PM
  -- Purpose : Handling routines for meta data objects

  subtype udt_id is api.pkg_Definition.udt_Id;

  -- Public function and procedure declarations
  procedure SetXferGuid(a_ObjectId udt_id, a_AsOfDate date);

end pkg_MetaData;
 
/

create or replace package body pkg_MetaData is

  procedure SetXferGuid(a_ObjectId udt_id, a_AsOfDate date) is
    t_GUID  varchar2(34);
  begin
    if api.pkg_ColumnQuery.IsNull(a_ObjectId, 'XferGuid') then
      t_GUID := SYS_GUID();
      api.pkg_ColumnUpdate.SetValue(a_ObjectID, 'XferGuid', t_GUID);
    end if;
  end;

end pkg_MetaData;
/

