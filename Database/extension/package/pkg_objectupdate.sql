create or replace package pkg_ObjectUpdate as

  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * DeleteObject()
   *   Delete the object.  This procedure is meant to be called from either
   * post-verify or a column change procedure in POSSE and it will delete the
   * object that it's called from.
   *-------------------------------------------------------------------------*/
  procedure DeleteObject (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_ConditionColumn                   varchar2
  );

  /*---------------------------------------------------------------------------
   * DeletePriorDocs()
   *  This procedure is intended to be run on post-verify when generating and
   * saving reports. When a new report is generated and saved, this procedure
   * will delete any old documents for that report.  The constraints on the
   * relationshiop have to be set 0 to many (or 1 to many).  This procedure is
   * normally useful on the web to allow the user the regenerate the report
   * and have it automatically save only the most recent version.
   *-------------------------------------------------------------------------*/
  procedure DeletePriorDocs (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_EndPointName                      varchar2
  );

  /*---------------------------------------------------------------------------
   * SearchAndRelateObjects()
   *   Search for the objects and relate them to the current object using the
   * specified end point name.
   *-------------------------------------------------------------------------*/
  procedure SearchAndRelateObjects (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_EndpointName                      varchar2,
    a_ObjectDefName                     varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2 default null
  );

  /*---------------------------------------------------------------------------
  * CreateMissingUserRel()
  *   If the given object has no relationship to a user object along the
  *   relationship defined by the given endpoint name then create one.  The
  *   current user will be used as the endpoint if the current user is a member
  *   member of the access group specified on the object.
  *-------------------------------------------------------------------------*/
  procedure CreateMissingUserRel(a_ObjectId     udt_Id,
    a_AsOfDate     date,
    a_EndpointName varchar2
  );

  /*---------------------------------------------------------------------------
  * CopyObject() -- PUBLIC
  * Created by Andy Patterson on 9/8/2011
  * Pass in an objectid and this procedure will create an object of the same
  * def and then copy all of the stored details over to the new object
  *-------------------------------------------------------------------------*/
  procedure CopyObject(a_ObjectId     udt_Id,
    a_NewObjectId  out udt_Id
  );

  /*---------------------------------------------------------------------------
  * New
  *  Create a new object given this ObjectDefName.
  *-------------------------------------------------------------------------*/
  function New (
    a_ObjectDefName                     varchar2,
    a_AsOfDate                          date default sysdate
  ) return udt_Id;

end;

 
/

grant execute
on pkg_objectupdate
to abc;

grant execute
on pkg_objectupdate
to bcpdata;

create or replace package body pkg_ObjectUpdate as

  /*---------------------------------------------------------------------------
   * Types - PRIVATE
   *-------------------------------------------------------------------------*/
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * IsIndexed() -- Private procedure that checks if a column is indexed
   *-------------------------------------------------------------------------*/
  function IsIndexed(a_ColumnDefId     udt_Id,
                     a_ObjectDefId     udt_Id)
    return boolean
  is
    t_IndexCount number;
    t_Return     boolean := false;
  begin
    
    select count(*)
      into t_IndexCount
      from api.indexdefcolumns cols 
      join api.indexdefs defs on
        cols.IndexDefId = defs.IndexDefId
      where defs.objectdefid = a_ObjectDefId 
      and columndefid = a_ColumnDefId
      -- we are assuming that any indexes that don't start with dup are not unique
      and description like 'dup_%';
      
    if t_IndexCount > 0 then
      t_Return := true;
    end if;
    
    return t_Return;
  
  end;
  
  /*---------------------------------------------------------------------------
   * DeleteObject()
   *-------------------------------------------------------------------------*/
  procedure DeleteObject (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_ConditionColumn                   varchar2
  ) is
  begin
    if api.pkg_ColumnQuery.Value(a_ObjectId, a_ConditionColumn) = 'Y' then
      api.pkg_ObjectUpdate.Remove(a_ObjectId);
    end if;
  end;

  /*---------------------------------------------------------------------------
   * DeletePriorDocs()
   *-------------------------------------------------------------------------*/
  procedure DeletePriorDocs (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_EndPointName                      varchar2
  ) is
    t_ProcessTypeId                     udt_Id;

    cursor c_Docs is
      select d.DocumentId
      from
        api.Relationships r,
        api.Documents d
      where r.FromObjectId = a_ProcessId
        and r.EndPointId = api.pkg_ConfigQuery.EndPointIdForName(
                t_ProcessTypeId, a_EndPointName)
        and d.DocumentId = r.ToObjectId
      order by d.LogicalTransactionId desc;
  begin
    select ObjectDefId
    into t_ProcessTypeId
    from api.Objects
    where ObjectId = a_ProcessId;

    for d in c_Docs loop
      if c_Docs%rowcount > 1 then
        api.pkg_ObjectUpdate.Remove(d.DocumentId);
      end if;
    end loop;
  end;

  /*---------------------------------------------------------------------------
   * SearchAndRelateObjects()
   *-------------------------------------------------------------------------*/
  procedure SearchAndRelateObjects (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_EndpointName                      varchar2,
    a_ObjectDefName                     varchar2,
    a_ColumnName                        varchar2,
    a_FromValue                         varchar2,
    a_ToValue                           varchar2 default null
  ) is
    t_Objects                           udt_IdList;
    t_RelId                             udt_Id;
  begin
    t_Objects := pkg_ObjectQuery.RelatedObjects(a_ObjectId, a_EndPointName);
    if t_Objects.count = 0 then
      t_Objects := api.pkg_SimpleSearch.ObjectsByIndex(a_ObjectDefName,
          a_ColumnName, a_FromValue, a_ToValue);
      for i in 1..t_Objects.count loop
        t_RelId := pkg_RelationshipUpdate.New(a_ObjectId, t_Objects(i),
            a_EndPointName);
      end loop;
    end if;
  end;

  /*---------------------------------------------------------------------------
  * CreateMissingUserRel() -- PUBLIC
  *-------------------------------------------------------------------------*/
  procedure CreateMissingUserRel(a_ObjectId     udt_Id,
    a_AsOfDate     date,
    a_EndpointName varchar2
  ) is
    t_ObjectDefId          udt_Id;
    t_EndpointId           udt_Id;
    t_ObjectDefDescription varchar2(60);
    t_Count                number(9);
    t_ColumnName           varchar2(30);
    t_ColumnName1          varchar2(30);
    t_AccessGroup          varchar2(60);
    t_AccessGroup1         varchar2(60);
    t_UserId               udt_Id;
    t_RelationshipId       udt_Id;
  begin
    -- Determine the appropriate EndpointId.
    t_ObjectDefId := pkg_ObjectQuery.ObjectDefId(a_ObjectId);
    t_EndpointId  := api.pkg_ConfigQuery.EndpointIdForName(t_ObjectDefId,
                                                           a_EndpointName);
    if t_EndpointId is null then
      select Description
        into t_ObjectDefDescription
        from api.Objectdefs
       where ObjectDefId = t_ObjectDefId;
      api.pkg_Errors.Clear;
      api.pkg_Errors.SetArgValue('EndPointName', a_EndPointName);
      api.pkg_Errors.SetArgValue('ObjectDef', t_ObjectDefDescription);
      api.pkg_Errors.RaiseError(-20000,
                                'Configuration error: "{EndPointName}" is not a valid endpoint name' ||
                                ' for "{ObjectDef}".');
    end if;
    -- Find out how many relationships with that EndPointId exist.
    select count(*)
      into t_Count
      from api.Relationships
     where FromObjectId = a_ObjectId
       and EndPointId = t_EndPointId;
    if t_Count = 0 then
      -- No relationship exists, so see if the effective user is in the
      -- access group.
      t_ColumnName  := a_EndpointName || '_AGrp';
      t_ColumnName1 := a_EndpointName || 'AccessGroup';
      t_AccessGroup := api.pkg_ColumnQuery.Value(a_ObjectId, t_ColumnName);
      t_AccessGroup1 := api.pkg_ColumnQuery.Value(a_ObjectId, t_ColumnName1);
      if t_AccessGroup is null and t_AccessGroup1 is null then
        select Description
          into t_ObjectDefDescription
          from api.Objectdefs
         where ObjectDefId = t_ObjectDefId;
        select count(*)
          into t_Count
          from api.ColumnDefs
         where ObjectDefId = t_ObjectDefId
           and Name in (t_ColumnName, t_ColumnName1);
        if t_Count > 0 then
          api.pkg_errors.Clear;
          api.pkg_errors.SetArgValue('Column', t_ColumnName || ' or ' || t_ColumnName1);
          api.pkg_Errors.SetArgValue('ObjectDef', t_ObjectDefDescription);
          api.pkg_errors.RaiseError(-20000,
                                    'Configuration error: column "{Column}" on "{ObjectDef}"' ||
                                    ' returned a null value.');
        else
          api.pkg_errors.Clear;
          api.pkg_errors.SetArgValue('Column', t_ColumnName || ' or ' || t_ColumnName1);
          api.pkg_Errors.SetArgValue('ObjectDef', t_ObjectDefDescription);
          api.pkg_errors.RaiseError(-20000,
                                    'Configuration error: column' ||
                                    ' "{Column}" not found on "{ObjectDef}".');
        end if;
      end if;
      t_UserId := api.pkg_SecurityQuery.EffectiveUserId();
      t_AccessGroup := nvl(t_AccessGroup,t_AccessGroup1);
      if pkg_SecurityUtils.IsMemberOfAccessGroup(t_AccessGroup, t_UserId) then
        -- Create the new relationship.
        t_RelationshipId := api.pkg_RelationshipUpdate.New(t_EndPointId,
                                                           a_ObjectId,
                                                           t_UserId,
                                                           a_AsOfDate);
      end if;
    end if;
  end;

  /*---------------------------------------------------------------------------
  * CopyObject() -- PUBLIC
  * Created by Andy Patterson on 9/8/2011
  * Pass in an objectid and this procedure will create an object of the same
  * def and then copy all of the stored details over to the new object
  *-------------------------------------------------------------------------*/
  procedure CopyObject(a_ObjectId     udt_Id,
    a_NewObjectId  out udt_Id
  ) is
    t_ObjectDefId      udt_Id;
    t_CharValue        varchar2(4000);
    t_NumValue         number;
    t_DateValue        date;
    t_RunDupProc       boolean := false;
    t_ErrorName        varchar2(4000);
    
  begin

  --get the objectdefid of our source object
    select ObjectDefId
      into t_ObjectDefId
      from api.Objects
     where Objectid = a_ObjectId;

  --create a new object with that defid
    a_NewObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);

    --loop through all of the stored columns for that objectdef
      for a in(select Name, DataType, ColumnDefId
                from api.ColumnDefs
               where Stored = 'Y'
                 and ObjectDefId = t_ObjectDefId)loop
        
        if not isIndexed(a.ColumnDefId, t_ObjectDefId) then
          --if the column data type is a date, process it as such
          if a.DataType = 'date' then

            t_DateValue := api.pkg_ColumnQuery.DateValue(a_ObjectId, a.Name);
            api.pkg_ColumnUpdate.SetValue(a_NewObjectId, a.Name, t_DateValue);

          --if the column data type is a number, process it as such
          elsif a.DataType = 'number' then
            t_ErrorName := a.Name;
            t_NumValue := api.pkg_ColumnQuery.NumericValue(a_ObjectId, a.Name);
            api.pkg_ColumnUpdate.SetValue(a_NewObjectId, a.Name, t_NumValue);

          else --otherwise it is a list, boolean or char
            t_CharValue := api.pkg_ColumnQuery.Value(a_ObjectId, a.Name);
            t_ErrorName := a.Name || ' ' || t_CharValue || '|' ||a_NewObjectId;
            api.pkg_ColumnUpdate.SetValue(a_NewObjectId, a.Name, t_CharValue);

          end if; --if datatype
        else
          
          t_RunDupProc := true;
          
        end if;
      end loop; --a
   
      if t_RunDupProc = true then

         toolbox.pkg_Copy.MaintainDuplicateDetails(a_NewObjectId, null);
 
     end if;
     exception 
       when others then
         raise_application_error(-20000, t_errorname);

  end CopyObject;
  
  /*---------------------------------------------------------------------------
  * New
  *  Create a new object given this ObjectDefName.
  *--------------------------------------------------------------------------*/
  function New (
    a_ObjectDefName                     varchar2,
    a_AsOfDate                          date default sysdate
  ) return udt_Id is
  begin
    return api.pkg_ObjectUpdate.New(api.pkg_ConfigQuery.ObjectDefIdForName(a_ObjectDefName), a_AsOfDate);
  end New;

end pkg_ObjectUpdate;

/

