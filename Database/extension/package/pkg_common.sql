create or replace package pkg_Common is

  -- Author  : SHANE.GILBERT
  -- Created : 10/15/2008 9:31:57 AM
  -- Purpose :

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

 /*---------------------------------------------------------------------------
 * Assign()
 * Assigned users based on AccessGroup.
 * A check can be done to ensure the users are in an appropriate secondary
 * group. (i.e. 'Parks Edit')
 * Both parameters are case sensitive.
 *-------------------------------------------------------------------------*/
  procedure Assign ( a_ObjectId    udt_Id,
                     a_AsOfDate    Date,
                     a_AccessGroup varchar2,
                     a_UsersInAg   varchar2 default null);


  -----------------------------------------------------------------------------
  -- JobIdForProcess()
  -----------------------------------------------------------------------------
  function JobIdForProcess(a_ProcessId pls_integer) return pls_integer;



 /**************************************************************************
  *  Cancel Job
  *  Purpose: Setup to run as an Activity to Run on the Job for the
  *  Change Status >> Change Status - Cancel Outcome
  *
  *  Outcomes all incomplete processes with Cancel
  *  Sends an Email to the Applicant of the Job
  *    Requires that the JobType have the p_SendEmail process as apart of the
  *    workflow.
  *
  *    Requires the following details on the Job:
  *      - Submitted Date
  *      - Revision Number
  *      - Email Event - Should be set to the Cancel Event for your JobType
  *         (For Template Email Verbiage to be a part of the message.
  *    Requires the existence of the Applicant:
  *      - Relationship to the User using an endpoint of Applicant
  *
  ***************************************************************************/
  Procedure CancelJob(a_ObjectId              udt_Id,
                      a_AsOfDate              date) ;
  /**************************************************************************
  *  CreateEmail
  *  - Sends Application Cancellation Email.
  ***************************************************************************/
  Procedure CreateEmail (a_ObjectId udt_Id,
                         a_AsOfDate date,
                         a_EndPointName varchar2 default '',
                         a_SendEmailId udt_Id default 0) ;
 /******************************************************************************
  * MyApplicationsSearch - Search for jobs where the current user is the
  *     applicant.
  *****************************************************************************/
  procedure MyApplicationsSearch(
    a_ApplicationNumber                 udt_Id,
    a_ApplReferenceNumber               varchar2,
    a_SubmittedDateFrom                 date,
    a_SubmittedDateTo                   date,
    a_OutstandingOnly                   varchar2,
    a_Objects                           out nocopy api.udt_objectlist
  );
  /******************************************************************************
  * MyWordDocumentTemplates - Creates a relationship to a relationship to Jobs
    for each Template created.
  *****************************************************************************/
  procedure CreateWordDocumentTemplateRel(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_DocumentTemplateName              varchar2
  );
  /******************************************************************************
  * ScheduleDocumentGeneration -
  *****************************************************************************/
  procedure ScheduleDocumentGeneration(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date);
  /*--------------------------------------------------------------------------
   This procedure makes sure Cover Letter,Schedule A requirements and Schedule B requirements
   have HRM Clearance Types.
  *------------------------------------------------------------------------*/
  procedure SetInternalDocTypes(
       a_ObjectId                udt_Id,
       a_AsOfDate                date );

  /*---------------------------------------------------------------------------
   * PPASearch()
   * Parks and Protected Areas Search by its Name with Filter on Removed.
   *
   *-------------------------------------------------------------------------*/
    procedure PPASearch (a_PPAName                         varchar2,
                         a_ParksManagementArea             varchar2,
                         a_RestrictedManagementArea        varchar2,
                         a_ApplicationType                 varchar2,
                         a_RestrictToActive                varchar2 default 'Y',
                         a_Objects              out api.udt_objectList);

  /*---------------------------------------------------------------------------
   * CheckCompliance()
   *  - Checks the Applicant submitting the Application for Compliance holds.
  ---------------------------------------------------------------------------*/
  Procedure CheckCompliance ( a_ObjectId udt_Id,
                              a_AsOfDate Date);

end pkg_Common;

 
/

grant execute
on pkg_common
to bcpdata;

grant execute
on pkg_common
to posseextensions;

