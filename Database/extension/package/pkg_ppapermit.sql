create or replace Package           pkg_PPAPermit Is
-- Author  : SHANE.GILBERT
-- Created : 11/18/2008 19:01
-- Purpose : Main package for the o_PPAPermit Object (Parks Permit)

  /*---------------------------------------------------------------------------
   * Types - PUBLIC
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * CreatePPAPermit
   *   Creates the o_PPAPermit by copying details and relationships from
   *   the Parks Permit Application
   *   Also Creates the New o_PPAPermit from a Renewal
   *-------------------------------------------------------------------------*/
  Procedure CreatePPAPermit ( a_ObjectId udt_Id,
                              a_AsOfDate Date );

  /*---------------------------------------------------------------------------
   * CopyDetails
   *   Copies details from Source to Target Objects based on
   *   Source and Target Def Ids.
   *   Note: Copies all detail values where the names and datatypes match.
   *-------------------------------------------------------------------------*/
  Procedure CopyDetails ( a_SourceObjectId udt_Id,
                          a_TargetObjectId udt_Id,
                          a_SourceDefId    udt_Id,
                          a_TargetDefId    udt_Id );

  /*---------------------------------------------------------------------------
   * CopyRelationships
   *   Creates relationships from the source to the target objectId based
   *   on the EndPointName provided.
   *-------------------------------------------------------------------------*/
  Procedure CopyRelationships ( a_SourceObjectId udt_Id,
                                a_TargetObjectId udt_Id,
                                a_EndPointName   varchar2 );

  /*---------------------------------------------------------------------------
   * CopyRelationships
   *   Creates a new object of the same objectdef and copies the details
   *   to the new object, relates to the newly created object
   *-------------------------------------------------------------------------*/
  Procedure CopyRelationships ( a_SourceObjectId    udt_Id,
                                a_TargetObjectId    udt_Id,
                                a_EndPointObjectDefId udt_Id,
                                a_EndPointName      varchar2 );

  /*---------------------------------------------------------------------------
   * CopyPPAPermit
   *   Copies data between the PPA Permit Object and the
   *   PPA Permit Application (Amendments & Renewals)
   *-------------------------------------------------------------------------*/
  Procedure CopyPPAPermit ( a_FromObjectId   udt_Id,
                            a_ToObjectId     udt_Id,
                            a_FromObjectDef  udt_Id,
                            a_ToObjectDef    udt_Id );

  /*---------------------------------------------------------------------------
   * ValidatePPAPermit - Validates the PPA Permit Object &
   *                     the PPA Permit Application.
   *-------------------------------------------------------------------------*/
  Procedure ValidatePPAPermit ( a_ObjectId udt_Id,
                                a_AsOfDate Date );

  /*---------------------------------------------------------------------------
   * CreateAmendPPAPermit - Sets PPA Permit Data onto the Amendment
   *-------------------------------------------------------------------------*/
  Procedure CreateAmendPPAPermit ( a_ObjectId udt_Id,
                                   a_AsOfDate Date );

  /*---------------------------------------------------------------------------
   * CreateRenewPPAPermit - Sets the PPA Permit Data onto the Renewal
   *-------------------------------------------------------------------------*/
  Procedure CreateRenewPPAPermit ( a_ObjectId udt_Id,
                                   a_AsOfDate Date );

  /*---------------------------------------------------------------------------
   * AmendPPAPermit - Amends the Existing PPA Permit Object from
   *                  Info from the PPA Permit Application.
   *-------------------------------------------------------------------------*/
  Procedure AmendPPAPermit ( a_ObjectId udt_Id,
                             a_AsOfDate Date );

  /*---------------------------------------------------------------------------
   * RemovePPARelationships
   *   Removes PPA Specific Relationships as apart of the Amendment
   *   to the PPA Object.
   *
   *-------------------------------------------------------------------------*/
  Procedure RemovePPARelationships ( a_ObjectId udt_Id,
                                     a_EndPointName varchar2);

  /*---------------------------------------------------------------------------
   * RemoveRelationships - Removes all relationships on the PPA Permit Object
   *-------------------------------------------------------------------------*/
  Procedure RemoveRelationships ( a_ObjectId udt_Id);

  /*---------------------------------------------------------------------------
   * Search - PPA Permit Object Search
   *-------------------------------------------------------------------------*/
  Procedure Search (a_PermitNumber            varchar2,
                    a_LastName                varchar2,
                    a_FirstName               varchar2,
                    a_Affiliation             varchar2,
                    a_PPAName                 varchar2,
                    a_PPAType                 varchar2,
                    a_PPAManagementArea       varchar2,
                    a_Activity                varchar2,
                    a_Research                varchar2,
                    a_FromIssueDate           Date,
                    a_ToIssueDate             Date,
                    a_FromCommencementDate    Date,
                    a_ToCommencementDate      Date,
                    a_FromTerminationDate     Date,
                    a_ToTerminationDate       Date,
                    a_FromProjectEndDate      Date,
                    a_ToProjectEndDate        Date,
                    a_NumberOfDaysOverdue     number,
                    a_ExcludeCancelledPermits varchar2,
                    a_Objects      out nocopy api.udt_ObjectList );
  /*---------------------------------------------------------------------------
   * GenerateReminders
   *  Generates Progress Report Reminder Emails
   *    Determines which permits require progress reports and do not have
   *    a report.
   *
   *-------------------------------------------------------------------------*/
  Procedure GenerateReminders;

End pkg_PPAPermit;

 
/

grant execute
on pkg_ppapermit
to posseextensions;

