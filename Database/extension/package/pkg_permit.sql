create or replace package           PKG_PERMIT is

  -- Author  : CARL.BRUINSMA
  -- Created : 9/11/2008 17:00:53
  -- Purpose : Procdures specific to the Archeaological Research Permit

  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

 /*--------------------------------------------------------------------------
  * ArchaeologistSearch() - Search for external users of the archaeologist type.
  *------------------------------------------------------------------------*/
  procedure ArchaeologistSearch (
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_Objects                           out api.udt_objectList
  );


 /*--------------------------------------------------------------------------
  * CreateAndRelateHRMPermit() - create an o_HRMPermit and relate it to the
  *     object that called this procedure using the end point name HRMPermit
  *------------------------------------------------------------------------*/
  procedure CreateAndRelateHRMPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );


 /*--------------------------------------------------------------------------
  * CopyHRMPermitForAmendment () - copy relevent details and relationships
  *     from the HRMPermit passed in to the amendment job(a_ObjectId).
  *     Run on constructor of the j_ArhaeologicalResearchPermit job if the
  *     application type is 'Amendment'.
  *------------------------------------------------------------------------*/
  procedure CopyHRMPermitForAmendment(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_PermitObjectId                    udt_Id
  );

 /*--------------------------------------------------------------------------
  * AmendHRMPermit () - update the permit that is being amended with the
  *     changes requested through the appliction job
  *------------------------------------------------------------------------*/
  procedure AmendHRMPermit(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_PermitObjectId                    udt_Id
  );

 /*--------------------------------------------------------------------------
  * ValidateArchResPermit () - Validate the mandatory columns and
  *    relationships for new Archaeological Research Permits
  *------------------------------------------------------------------------*/
  procedure ValidateArchResPermit(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

 /*--------------------------------------------------------------------------
  * ValidateArchResPermitAmend () - Validate the mandatory columns and
  *    relationships for new Archaeological Research Permit Amendments
  *------------------------------------------------------------------------*/
  procedure ValidateArchResPermitAmend (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_PermitObjectId                    udt_Id
  );

 /*--------------------------------------------------------------------------
  * ValidateAmendmentApproval () -
  *  Do final checks before amendment before the application is sent to the
  *  process server to be approved.
  *------------------------------------------------------------------------*/
  procedure ValidateAmendmentApproval(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_PermitObjectId                    udt_Id
  );

 /*--------------------------------------------------------------------------
  * ArchResPermitSendEmail () - Wrapper procedure for creating and completing
  *   the SendEmail process to ensure the timing is correct.
  *------------------------------------------------------------------------*/
  procedure ArchResPermitSendEmail (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
    );
 /*--------------------------------------------------------------------------
  * ArchResearchPermitSearch () - Archaeology Research Permit Search
  *------------------------------------------------------------------------*/
  procedure ArchResearchPermitSearch(
    a_ApplicationNumber        udt_Id,
    a_ApplicantRefNumber       varchar2,
    a_FromSubmittedDate        Date,
    a_ToSubmittedDate          Date,
    a_FromAcceptedDate         Date,
    a_ToAcceptedDate           Date,
    a_OutstandingOnly          varchar2,
    a_ApplicantFirstName       varchar2,
    a_ApplicantLastName        varchar2,
    a_ApplicantAffiliation     varchar2,
    a_PermitNumber             varchar2,
    a_Mer                      varchar2,
    a_Rge                      varchar2,
    a_Twp                      varchar2,
    a_Sec                      varchar2,
    a_StatusName               varchar2,
    a_Objects            out nocopy api.udt_ObjectList);

 /*--------------------------------------------------------------------------
  * CancelJob () - Cancel Archaeology Research Permit Application
  *------------------------------------------------------------------------*/
  procedure CancelJob(
    a_ObjectId                            udt_Id,
    a_AsOfDate                            date
  );

 /*--------------------------------------------------------------------------
  * PaymentCompleted () - The payment has been Approved so workflow should
  *     initiated.
  *------------------------------------------------------------------------*/
  procedure PaymentCompleted(
    a_ObjectId                            udt_Id,
    a_AsOfDate                            date
  );

 /*--------------------------------------------------------------------------
  * CopyPublishedDocToPermit () - Copies published docs from the
  *     application to the HRMPermit. HRMPermit endpoint 'PublishedDoc'.
  *------------------------------------------------------------------------*/
  procedure CopyPublishedDocToPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );
end PKG_PERMIT;

 
/

grant execute
on pkg_permit
to posseextensions;

