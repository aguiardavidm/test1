create or replace package pkg_utils as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * AddSection()
   *   Add a section to a string, separating the section from the remainder
   * of the string, if necessary.
   *-------------------------------------------------------------------------*/
  procedure AddSection (
    a_String        in out varchar2,
    a_Section       varchar2,
    a_Separator       varchar2
  );

  /*---------------------------------------------------------------------------
   * Split ()
   *  Return a list of the words in a_String that are separated by a_Separator.
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                    varchar2,
    a_Separator                 varchar2
  ) return udt_StringList;

  function Split2 (
    a_String varchar2,
    a_Separator varchar2
  ) return extension.udt_StringList2;

  /*---------------------------------------------------------------------------
   * IsNumber()
   *   Returns a 'Y' or 'N' indicating if the string is a number.
   *-------------------------------------------------------------------------*/
  function IsNumber (
    a_String        varchar2
  ) return varchar2;

  /*--------------------------------------------------------------------------
   * NumericPrefix()
   *   Return the numeric prefix of a string.  For example, '23abc' would
   * return 23.
   *------------------------------------------------------------------------*/
  function NumericPrefix (
    a_String                 varchar2
  ) return number;

/*---------------------------------------------------------------------------
  * ParseTextValue()
  *  From the System Parameter with the specified System Reference 
  *  treat each values contained in braces ('{' and '}') as a
  *  column name on the specified object and return its value.
  *-------------------------------------------------------------------------*/
  function ParseTextValue (
    a_ObjectId                          udt_id,
    a_TextToParse                       varchar2
  ) return varchar2;
  
  /*--------------------------------------------------------------------------
   * StringSuffix()
   *   Return the section of a string that comes after the numeric prefix.
   * For example, '23abc123' would return 'abc123'.
   *------------------------------------------------------------------------*/
  function StringSuffix (
    a_String                 varchar2
  ) return varchar2;

  /*--------------------------------------------------------------------------
   * SplitLines()
   *  Split the text into an array, with one entry for each line in the string.
   * This is usefull for taking a very long block of text and using the
   * table(cast(... as extension.udt_StingList)) in a SQL statement to get a row
   * for each line of text.
   *------------------------------------------------------------------------*/
  function SplitLines (
    a_String         varchar2
  ) return udst_StringList;

  /*--------------------------------------------------------------------------
   * SplitNote()
   *  SplitNote does the same thing as SplitLines, except it takes a note id as
   * an argument and splits the note text.
   *------------------------------------------------------------------------*/
  function SplitNote (
    a_NoteId         udt_Id
  ) return udst_StringList;

  /*---------------------------------------------------------------------------
  * ConvertToCastableStringList ()
  *  The results are castable as udst_StringList.
  *
  *  Example:
  *     select column_value
  *     from table (cast (
  *          extension.pkg_utils.SplitCastable('a,b,c', ',') as extension.udst_StringList)))
  *--------------------------------------------------------------------------*/
 function ConvertToCastableStringList(a_Source udt_StringList)
    return udst_StringList;

  /*--------------------------------------------------------------------------
   * ConvertToObjectList()
   *   Convert an udt_IdList to an api.udt_ObjectList;
   *------------------------------------------------------------------------*/
  function ConvertToObjectList (
    a_IdList                udt_IdList
  ) return api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * SetErrorArgs()
   *   Clears existing error arguments and allows multiple error arguments to
   * be set with a single call.
   *--------------------------------------------------------------------------*/
  procedure SetErrorArgs (
    a_Name1                             varchar2,
    a_Value1                            varchar2,
    a_Name2                             varchar2 default null,
    a_Value2                            varchar2 default null,
    a_Name3                             varchar2 default null,
    a_Value3                            varchar2 default null,
    a_Name4                             varchar2 default null,
    a_Value4                            varchar2 default null,
    a_Name5                             varchar2 default null,
    a_Value5                            varchar2 default null,
    a_Name6                             varchar2 default null,
    a_Value6                            varchar2 default null,
    a_Name7                             varchar2 default null,
    a_Value7                            varchar2 default null,
    a_Name8                             varchar2 default null,
    a_Value8                            varchar2 default null,
    a_Name9                             varchar2 default null,
    a_Value9                            varchar2 default null,
    a_Name10                            varchar2 default null,
    a_Value10                           varchar2 default null
  );



  /*---------------------------------------------------------------------------
   * WorkingDaysBetween()
   *   Finds the amount of working days between a period of two dates.  Working
   * days are defind as Calendar days - weekends and holidays.
   *--------------------------------------------------------------------------*/
  function WorkingDaysBetween (
    a_StartDate                         date,
    a_EndDate                           date
  ) return number;

  /*---------------------------------------------------------------------------
   * AddWorkingDays()
   *  Takes the start date of a process and returns a due date based on the days
   * to respond set on the permit type
   *--------------------------------------------------------------------------*/
  function AddWorkingDays (
    a_StartDate                         date,
    a_DaysToAdd                         integer
  ) return date;

  /*---------------------------------------------------------------------------
   * GetSystemSettingsStringValue()
   *  Ability to retrieve a string value from the system settings view
   *--------------------------------------------------------------------------*/
  function GetSystemSettingsStringValue(
    a_ColumnName                         varchar2
  ) return varchar2;
end pkg_utils;

 
/

grant execute
on pkg_utils
to abc;

grant execute
on pkg_utils
to bcpdata;

grant execute
on pkg_utils
to conversion;

grant execute
on pkg_utils
to ereferral;

grant execute
on pkg_utils
to lms;

grant execute
on pkg_utils
to posseextensions;

create or replace package body pkg_utils as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;

  /*---------------------------------------------------------------------------
   * AddSection()
   *-------------------------------------------------------------------------*/
  procedure AddSection (
    a_String        in out varchar2,
    a_Section       varchar2,
    a_Separator       varchar2
  ) is
  begin
    if a_Section is not null then
      if a_String is null then
        a_String := a_Section;
      else
        a_String := a_String || a_Separator || a_Section;
      end if;
    end if;
  end;
  /*---------------------------------------------------------------------------
  * ConvertToCastableStringList ()
  *--------------------------------------------------------------------------*/
  function ConvertToCastableStringList(a_Source udt_StringList) return udst_StringList is
    t_ReturnList udst_StringList;
  begin
    t_ReturnList := udst_StringList();
    for i in 1.. a_Source.count loop
      t_ReturnList.Extend();
      t_ReturnList(i) := a_Source(i);
    end loop;

    return t_ReturnList;
  end;
  /*---------------------------------------------------------------------------
   * Split ()
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                    varchar2,
    a_Separator                 varchar2
  ) return udt_StringList is
    t_List                      udt_StringList;
    t_Pos                       number;
    t_Index                     number;
    t_SepLen                    number;
  begin
    t_SepLen := length(a_Separator);
    t_Pos := 0;
    while t_Pos <= length(a_String) loop
      t_Index := instr(a_String, a_Separator, t_Pos+1);
      if t_Index > 0 then
        t_List(t_List.count+1) := substr(a_String, t_Pos+1, t_Index-t_Pos-1);
        t_Pos := t_Index + t_SepLen - 1;
      else
        t_List(t_List.count+1) := substr(a_String, t_Pos+1);
        t_Pos := length(a_String) + 1;
      end if;
    end loop;

    return t_List;
  end;

  function Split2 (
    a_String                    varchar2,
    a_Separator                 varchar2
  ) return extension.udt_StringList2 is
    t_StringList udt_StringList;
    t_TempString extension.udt_StringList2;
    t_Index                pls_integer;
    t_Count                pls_integer;
  begin
    t_StringList := Split(a_String, a_Separator);

    t_TempString := extension.udt_StringList2();
    t_TempString.extend(t_StringList.count);



    t_Count := 1;
    t_Index := t_StringList.first;
    while t_Index is not null loop
      t_TempString(t_Count) := t_StringList(t_Index);
      t_Count := t_Count + 1;
      t_Index := t_StringList.next(t_Index);
    end loop;

    return t_TempString;
  end;

  /*---------------------------------------------------------------------------
   * IsNumber() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function IsNumber (
    a_String        varchar2
  ) return varchar2 is
    t_Temp        number;
  begin
    t_Temp := a_String;
    return 'Y';
  exception
  when value_error then
    return 'N';
  end;

  /*--------------------------------------------------------------------------
   * NumericPrefix()
   *------------------------------------------------------------------------*/
  function NumericPrefix (
    a_String                 varchar2
  ) return number is
    t_Number                 number;
  begin
    if a_String is null then
      return null;
    end if;

    for i in 1..length(a_String) loop
      if substr(a_String, i, 1) between '0' and '9' then
        t_Number := to_number(substr(a_String, 1, i));
      else
        exit;
      end if;
    end loop;

    return t_Number;
  end;

 /*---------------------------------------------------------------------------
  * ColumnExistsOnObject()
  *-------------------------------------------------------------------------*/  
  function ColumnExistsOnObject(
    a_ObjectId                          udt_id,
    a_ColumnName                        varchar2
  ) return boolean is
    t_Count                             pls_integer;
    
  begin
    select count(*)
      into t_Count
      from api.objects o
      where o.ObjectId = a_ObjectId
        and exists (
          select 1
            from api.ColumnDefs cd
            where cd.ObjectDefId = o.ObjectDefId
              and lower(cd.Name) = lower(a_ColumnName));
    
    return t_Count <> 0;
  end ColumnExistsOnObject;
  
 /*---------------------------------------------------------------------------
  * ParseTextValue()
  *-------------------------------------------------------------------------*/  
  function ParseTextValue (
    a_ObjectId                          udt_id,
    a_TextToParse                       varchar2
  ) return varchar2 is
    t_ColumnName                        varchar2(4000);
    t_First                             varchar2(4000);
    t_Index                             pls_integer;
    t_Rest                              varchar2(4000); --:= GetTextValue(a_SystemReference);
    
  begin
    t_Rest := a_TextToParse;
    loop
      -- find beginning of column name
      t_Index := instr(t_Rest, '{');
      exit when t_Index = 0 or t_Index is null;
      t_First := t_First || substr(t_Rest, 0, t_Index - 1);
      t_Rest := substr(t_Rest, t_Index);

      -- find end of column name
      t_Index := instr(t_Rest, '}');
      exit when t_Index = 0 or t_Index is null;
      t_ColumnName := substr(t_Rest, 2, t_Index - 2);
      if ColumnExistsOnObject(a_ObjectId, t_ColumnName) then
        t_First := t_First || api.pkg_ColumnQuery.Value(a_ObjectId, t_ColumnName);
      else
        t_First := t_First || '{' || t_ColumnName || '}';
      end if;
      t_Rest := substr(t_Rest, t_Index + 1);
    end loop;

    return t_First || t_Rest;
  end ParseTextValue;
  
  /*--------------------------------------------------------------------------
   * StringSuffix()
   *------------------------------------------------------------------------*/
  function StringSuffix (
    a_String                 varchar2
  ) return varchar2 is
    t_Suffix                 varchar2(1000);
    t_Index                  pls_integer;
  begin
    if a_String is null then
      return null;
    end if;

    for i in 1..length(a_String) loop
      if substr(a_String, i, 1) not between '0' and '9' then
        t_Index := i;
        exit;
      end if;
    end loop;

    if t_Index is not null then
      t_Suffix := substr(a_String, t_Index);
    end if;

    return t_Suffix;
  end;

  /*--------------------------------------------------------------------------
   * SplitLines() -- PUBLIC
   *------------------------------------------------------------------------*/
  function SplitLines (
    a_String         varchar2
  ) return udst_StringList is
    t_Text varchar2(32767);
    t_TextList udst_StringList;
    t_IndexCR number;
    t_IndexLF number;
    t_Index number;
    t_NextIndex number;
    t_Position number;
    t_Continue boolean;
  begin
    t_TextList := udst_StringList();
    t_Position := 1;
    t_Text := a_String;

    if t_Text is not null then
      t_Continue := true;
      while t_Continue loop
        t_IndexCR := instr(t_Text, chr(13), t_Position);
        t_IndexLF := instr(t_Text, chr(10), t_Position);

        if t_IndexLF > 0 and t_IndexCR = 0 then
          t_Index := t_IndexLF;
          t_NextIndex := t_Index + 1;
        elsif t_IndexCR > 0 and t_IndexLF = 0 then
          t_Index := t_IndexCR;
          t_NextIndex := t_Index + 1;
        elsif t_IndexCR > 0 and t_IndexLF > 0 then
          if t_IndexLF < t_IndexCR then
            t_Index := t_IndexLF;
            if t_IndexCR = t_IndexLF + 1 then
              t_NextIndex := t_Index + 2;
            else
              t_NextIndex := t_Index + 1;
            end if;
          else
            t_Index := t_IndexCR;
            if t_IndexLF = t_IndexCR + 1 then
              t_NextIndex := t_Index + 2;
            else
              t_NextIndex := t_Index + 1;
            end if;
          end if;
        else
          t_Continue := false;
          t_Index := length(t_Text)+1;
        end if;

    /* If the line is longer than 4000 characters, cut if off.  SQL cannot
      deal with varchar's over 4000 characters */
        t_TextList.extend(1);
        t_TextList(t_TextList.count) :=
            substr(substr(t_Text, t_Position, t_Index-t_Position),1,4000);
        t_Position := t_NextIndex;
      end loop;
    end if;

    return t_TextList;
  end;

  /*--------------------------------------------------------------------------
   * SplitNote() -- PUBLIC
   *------------------------------------------------------------------------*/
  function SplitNote (
    a_NoteId         udt_Id
  ) return udst_StringList is
    t_Text           varchar2(32767);
  begin
    select Text
      into t_Text
      from api.Notes
     where NoteId = a_NoteId;

    return SplitLines(t_Text);
  end;

  /*--------------------------------------------------------------------------
   * ConvertToObjectList()
   *   Convert an udt_IdList to an api.pkg_ObjectList;
   *------------------------------------------------------------------------*/
  function ConvertToObjectList (
    a_IdList                udt_IdList
  ) return api.udt_ObjectList is
    t_Objects                api.udt_ObjectList;
    t_Index                pls_integer;
    t_Count                pls_integer;
  begin
    t_Objects := api.udt_ObjectList();
    t_Objects.extend(a_IdList.count);

    t_Count := 1;
    t_Index := a_IdList.first;
    while t_Index is not null loop
      t_Objects(t_Count) := api.udt_Object(a_IdList(t_Index));
      t_Count := t_Count + 1;
      t_Index := a_IdList.next(t_Index);
    end loop;

    return t_Objects;
  end;

  /*--------------------------------------------------------------------------
   * SetErrorArgs() -- PUBLIC
   *------------------------------------------------------------------------*/
  procedure SetErrorArgs (
    a_Name1                             varchar2,
    a_Value1                            varchar2,
    a_Name2                             varchar2 default null,
    a_Value2                            varchar2 default null,
    a_Name3                             varchar2 default null,
    a_Value3                            varchar2 default null,
    a_Name4                             varchar2 default null,
    a_Value4                            varchar2 default null,
    a_Name5                             varchar2 default null,
    a_Value5                            varchar2 default null,
    a_Name6                             varchar2 default null,
    a_Value6                            varchar2 default null,
    a_Name7                             varchar2 default null,
    a_Value7                            varchar2 default null,
    a_Name8                             varchar2 default null,
    a_Value8                            varchar2 default null,
    a_Name9                             varchar2 default null,
    a_Value9                            varchar2 default null,
    a_Name10                            varchar2 default null,
    a_Value10                           varchar2 default null
  ) is
  begin
    api.pkg_Errors.Clear;
    if a_Name1 is not null then
      api.pkg_Errors.SetArgValue(a_Name1, a_Value1);
    end if;
    if a_Name2 is not null then
      api.pkg_Errors.SetArgValue(a_Name2, a_Value2);
    end if;
    if a_Name3 is not null then
      api.pkg_Errors.SetArgValue(a_Name3, a_Value3);
    end if;
    if a_Name4 is not null then
      api.pkg_Errors.SetArgValue(a_Name4, a_Value4);
    end if;
    if a_Name5 is not null then
      api.pkg_Errors.SetArgValue(a_Name5, a_Value5);
    end if;
    if a_Name6 is not null then
      api.pkg_Errors.SetArgValue(a_Name6, a_Value6);
    end if;
    if a_Name7 is not null then
      api.pkg_Errors.SetArgValue(a_Name7, a_Value7);
    end if;
    if a_Name8 is not null then
      api.pkg_Errors.SetArgValue(a_Name8, a_Value8);
    end if;
    if a_Name9 is not null then
      api.pkg_Errors.SetArgValue(a_Name9, a_Value9);
    end if;
    if a_Name10 is not null then
      api.pkg_Errors.SetArgValue(a_Name10, a_Value10);
    end if;
  end SetErrorArgs;

  /*---------------------------------------------------------------------------
   * WorkingDaysBetween()
   *--------------------------------------------------------------------------*/
  function WorkingDaysBetween (
    a_StartDate                         date,
    a_EndDate                           date
  ) return number is
   t_StartDate                          date;
   t_EndDate                            date;
   t_Workdays                           pls_integer;
   t_NumHolidays                        pls_integer;
  begin

    /* Any null input will return a null output */
    if a_StartDate is null or a_EndDate is null then
      return null;
    end if;

    /* Initialization */
    t_StartDate := trunc(a_StartDate);
    t_EndDate := trunc(a_EndDate);

    /* Count Days */
    if t_EndDate >= t_StartDate then
      /* Date Range */
      t_workdays := t_EndDate - t_StartDate
            /* Subtracts the number of weekend days by determining the number
             of days, dividing by 7 to weeks and then multiplying by 2 to
             get the number of weekend days */
         - ((trunc(t_EndDate,'D') - trunc(t_StartDate,'D'))/7)*2 + 1;

      /* Adjust for ending date on a saturday */
      if to_char(t_EndDate,'D') = '7' then
        t_workdays := t_workdays - 1;
      end if;

      /* Adjust for starting date on a sunday */
      if to_char(t_StartDate,'D') = '1' then
        t_workdays := t_workdays - 1;
      end if;
    else
      t_workdays := 0;
    end if;

    /* Count Holidays */
    select Count(Holiday)
    into t_NumHolidays
    from api.holidays
    where to_char(Holiday, 'D') not in (1, 7)
      and holiday between t_StartDate and t_EndDate;


    /* Subtract the WorkDays from the Holidays */
    t_workdays := t_workdays - t_NumHolidays;

    return(t_workdays);
  end;

  /*---------------------------------------------------------------------------
   * AddWorkingDays()
   *   Takes the start date of a process and adds a number of working days 
   * excluding Holidays and Weekends.
   *--------------------------------------------------------------------------*/
  function AddWorkingDays (
    a_StartDate                         date,
    a_DaysToAdd                         integer
  ) return date is
  t_StartDate                           date := a_StartDate;
  t_ReturnDate                          date;
  t_DaysToAdd                           integer := a_DaysToAdd;
  t_WorkWeeks                           integer;
  t_WorkDays                            integer;
  t_WeekendWrap                         char(1);
  t_NumHolidays                         integer;
  t_IsHoliday                           char(1);
  begin
      
    if a_StartDate is null or a_DaysToAdd is null then
      return t_ReturnDate;
    end if;
      
    -- Handles when the StartDate is on a Friday or Saturday  
    if to_char(t_Startdate, 'D') = 6 then
      t_StartDate := t_StartDate + 2;
    elsif to_char(t_StartDate, 'D') = 7 then
      t_StartDate := t_StartDate + 1;
    end if;
      
    -- Ensures that we won't be subtracting days or adding 0  
    if t_DaysToAdd < 1 then
      t_DaysToAdd := 1;
    end if;
      
    -- Find how many work weeks to add and calculate adding weekends
    t_WorkWeeks := floor(t_DaysToAdd/5);
    t_WorkDays := mod(t_DaysToAdd, 5);
    if to_char(t_StartDate, 'D') + t_WorkDays > 6 then 
      t_WorkDays := t_WorkDays + 2;
    end if;
      
    -- Adds the necessary number of work days, weekends, and ensures that
    -- we aren't returning a weekend date.  
    t_ReturnDate := t_StartDate + (t_WorkWeeks * 7) + t_WorkDays;
    if to_char(t_ReturnDate, 'D') = 7 then
      t_ReturnDate := t_ReturnDate + 2;
    elsif to_char(t_ReturnDate, 'D') = 1 then
      t_ReturnDate := t_ReturnDate + 1;
    end if;

    -- Finds how many holidays are in the time span after adding work days.
    -- Ignores holidays that fall on weekends.
    select count(*)
    into t_NumHolidays
    from query.o_abc_holidays h
    where to_char(h.HolidayDate, 'D') not in (1,7)
      and h.HolidayDate > t_startdate 
      and h.HolidayDate <= t_ReturnDate;
    -- There should be relatively few holidays to add, so we can add them one at a
    -- time, making sure that we aren't ending up on a weekend or another holiday.
    while t_NumHolidays > 0 loop
      t_ReturnDate := t_ReturnDate + 1;
      select 
        case when count(*) > 0 then 'Y'
        else 'N'
        end
      into t_IsHoliday
      from query.o_abc_holidays ho
      where trunc(ho.HolidayDate, 'DD') = trunc(t_ReturnDate, 'DD');
      
      if to_char(t_ReturnDate, 'D') not in (1,7) and t_IsHoliday != 'Y' then
        t_NumHolidays := t_NumHolidays - 1;
      end if;  
    end loop;

    return t_ReturnDate;
  end AddWorkingDays; 

  /*---------------------------------------------------------------------------
  * GetSystemSettingsStringValue ()
  *--------------------------------------------------------------------------*/
  function GetSystemSettingsStringValue(
    a_ColumnName varchar2
  ) return varchar2 is
    t_ObjectId    number;                            
    t_ReturnValue varchar2(4000);
  begin
    select objectid into t_ObjectId
      from query.o_systemsettings
      where rownum = 1;
    
    t_ReturnValue := api.pkg_columnquery.value(t_ObjectId, '' || a_ColumnName || '');
    
    return t_ReturnValue;
  end;  

end pkg_utils;
/
