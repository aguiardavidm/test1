create or replace package pkg_DashboardUtils as

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_DateList is api.pkg_Definition.udt_DateList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_Widget is pkg_DashboardDefinition.udt_Widget;
  subtype udt_NumberListByString is
      pkg_DashboardDefinition.udt_NumberListByString;
  subtype udt_DataMatrix is pkg_DashboardDefinition.udt_DataMatrix;
  subtype udt_UrlParameters is pkg_DashboardDefinition.udt_UrlParameters;
  subtype udt_CorralTable is pkg_DashboardDefinition.udt_CorralTable;

  g_NullNumberList                      udt_NumberList;
  g_NullStringList                      udt_StringList;

  /*--------------------------------------------------------------------------
   * Constants
   *------------------------------------------------------------------------*/
  -- constants for Function Name
  gc_Count                              constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Count;
  gc_Sum                                constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Sum;
  gc_Average                            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Average;

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_StringList,
    a_SecondList                        udt_StringList
  );

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_DateList,
    a_SecondList                        udt_DateList
  );

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_NumberList,
    a_SecondList                        udt_NumberList
  );

  /*---------------------------------------------------------------------------
   * ParseQueryString()
   *-------------------------------------------------------------------------*/
  function ParseQueryString (
    a_QueryString                       varchar2
  ) return udt_UrlParameters;

  /*---------------------------------------------------------------------------
   * GetWidgetJSON()
   *  Run the PL/SQL procedure for the widget with the arguments and return
   * the JSON result.
   *-------------------------------------------------------------------------*/
   function GetWidgetJSON (
     a_WidgetId                         udt_Id,
     a_WidgetArgs                       varchar2
   ) return clob;

  /*---------------------------------------------------------------------------
   * GetWidgetInfo()
   *-------------------------------------------------------------------------*/
   function GetWidgetInfo (
     a_WidgetId                         udt_Id
   ) return clob;

  /*---------------------------------------------------------------------------
   * GetDashboardInfo()
   *  For a given Dashboard, retrieve the information about the widgets.
   *-------------------------------------------------------------------------*/
  function GetDashboardInfo (
    a_DashboardName                     varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * SetDashboardInfo()
   *  Update the visible widgets for the current user for a given dashboard.
   *-------------------------------------------------------------------------*/
  procedure SetDashboardInfo (
    a_WidgetXrefObjectIds               varchar2,
    a_VisibleList                       varchar2
  );

  /*---------------------------------------------------------------------------
   * GetGroupConfig()
   *-------------------------------------------------------------------------*/
  procedure GetGroupConfig (
    a_WidgetId                          udt_Id,
    a_GroupExpressions                  out udt_StringList,
    a_ShowValues                        out udt_StringList
  );

  /*---------------------------------------------------------------------------
   * GetAccessGroupUsersJSON()
   *-------------------------------------------------------------------------*/
  function GetAccessGroupUsersJSON (
    a_AccessGroupName                  varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * ApplyWidgetFilters()
   *-------------------------------------------------------------------------*/
   procedure ApplyWidgetFilters (
     a_Objects                          in out nocopy api.udt_ObjectList,
     a_ObjectDefId                      udt_Id,
     a_WidgetId                         udt_Id
   );

  /*---------------------------------------------------------------------------
   * FilterJoinClause() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function FilterJoinClause(
    a_Widget                            udt_Widget,
    a_JoinColumn                        varchar2,
    a_AlwaysJoin                        boolean default false
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * GetColumnValues()
   *  Get the column values for the set of objects.  Perform efficient joins
   * to the physical Posse tables if stored this is for a stored column, or
   * join to Corral tables if available.  Otherwise fall back on
   * api.pkg_ColumnQuery.
   *-------------------------------------------------------------------------*/
  function GetColumnValues (
    a_Objects                          api.udt_ObjectList,
    a_ObjectDefId                      udt_Id,
    a_ColumnName                       varchar2
  ) return udt_StringList;

-- FIX ME - this will soon replace the above procedure
  function GetColumnValues (
    a_CorralTable                       udt_CorralTable,
    a_ColumnName                        varchar2
  ) return udt_StringList;

  /*---------------------------------------------------------------------------
   * GetGroupColumnValues()
   *-------------------------------------------------------------------------*/
  function GetGroupColumnValues (
    a_Widget                            udt_Widget
  ) return udt_StringList;

  /*---------------------------------------------------------------------------
   * GetCorralTable()
   *-------------------------------------------------------------------------*/
  function GetCorralTable (
    a_WidgetId                          pls_integer
  ) return udt_CorralTable;

  /*---------------------------------------------------------------------------
   * GroupByTimeline()
   *   GroupByPeriod should be one of 'month', 'week' or 'day'.
   *-------------------------------------------------------------------------*/
  procedure GroupByTimeline (
    a_Widget                            in out nocopy udt_Widget,
    a_Dates                             udt_DateList,
    a_GroupValues                       udt_StringList default g_NullStringList,
    a_FunctionName                      varchar2 default gc_Count,
    a_FunctionValues                    udt_NumberList default g_NullNumberList
  );

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Widget                            in out nocopy udt_Widget,
    a_xValues                           udt_StringList,
    a_sgValues                          udt_StringList default g_NullStringList,
    a_xDistinctValues                   udt_StringList default g_NullStringList,
    a_sgDistinctValues                  udt_StringList default g_NullStringList
  );

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Widget                            in out nocopy udt_Widget,
    a_xValues                           udt_StringList,
    a_sgValues                          udt_StringList,
    a_xDistinctValues                   udt_StringList,
    a_sgDistinctValues                  udt_StringList,
    a_FunctionName                      varchar2,
    a_FunctionValues                    udt_NumberList
  );

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *   This overload is for Compare Previous Years Bar graphs.
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Widget                            in out nocopy udt_Widget,
    a_xValues                           udt_StringList,
    a_Dates                             udt_DateList,
    a_xDistinctValues                   udt_StringList default g_NullStringList
  );

end pkg_DashboardUtils;

 

/

grant execute
on pkg_dashboardutils
to posseextensions;

create or replace package body pkg_DashboardUtils as

  /*--------------------------------------------------------------------------
   * Types - PRIVATE
   *------------------------------------------------------------------------*/
  type udt_Filter is record (
    ColumnName                  varchar2(30),
    ColumnDefId                 udt_Id,
    Stored                      varchar2(1),
    Indexed                     varchar2(1),
    LookupIndexed               varchar2(1),
    EndPointName                varchar2(30),
    LookupColumnName            varchar2(30),
    FromValue                   varchar2(4000),
    ToValue                     varchar2(4000)
  );
  type udt_FilterList is table of udt_Filter index by binary_integer;

  /*--------------------------------------------------------------------------
   * Constants - PRIVATE
   *------------------------------------------------------------------------*/
  -- constant for udt_Widget.view_
  gc_Compare				            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Compare;

  -- constant EOL characters
  gc_CRLF                               constant varchar2(2) :=
      chr(13) || chr(10);

  /*---------------------------------------------------------------------------
   * CorralColumnDataType()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  function CorralColumnDataType (
    a_CorralTable                       udt_CorralTable,
    a_ColumnName                        varchar2
  ) return varchar2 is
    t_DataType                          varchar2(110);
  begin
    begin
      select data_type
        into t_DataType
        from all_tab_columns
        where owner = upper(a_CorralTable.Schema)
          and table_name = upper(a_CorralTable.Name)
          and column_name = upper(a_ColumnName);
    exception
    when no_data_found then
      null;
    end;

    if t_DataType is null then
      api.pkg_Errors.RaiseError(-20000, 'A "' || a_ColumnName ||
          '" column does not exist on "' || a_CorralTable.Schema || '.' ||
          a_CorralTable.Name || '.');
    end if;

    return t_DataType;
  end CorralColumnDataType;

  /*---------------------------------------------------------------------------
   * GenerateDatePoints()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  function GenerateDatePoints (
    a_FromDate                          date,
    a_ToDate                            date,
    a_TruncBy                           varchar2,
    a_Format                            varchar2,
    a_GroupByPeriod                     varchar2
  ) return udt_StringList is
    t_Date                              date;
    t_DatePoints                        udt_StringList;
  begin
    t_Date := a_FromDate;
    while t_Date <= a_ToDate loop
      t_DatePoints(t_DatePoints.count + 1) :=
          to_char(trunc(t_Date, a_TruncBy), a_Format);

      -- NOTE: if this code is changed then the case statement in
      -- GroupByTimeline() must be modifed to match
      case a_GroupByPeriod
        when 'year' then
          t_Date := add_months(t_Date, 12);
        when 'month' then
          t_Date := add_months(t_Date, 1);
        when 'week' then
          t_Date := t_Date + 7;
        when 'day' then
          t_Date := t_Date + 1;
        else
          api.pkg_Errors.RaiseError(-20000,
              'Invalid GroupByPeriod: "' || a_GroupByPeriod || '".');
      end case;
    end loop;
    return t_DatePoints;
  end GenerateDatePoints;

  /*---------------------------------------------------------------------------
   * GroupParameters()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure GroupParameters (
    a_Widget                            udt_Widget,
    a_GroupByPeriod                     in out varchar2,
    a_TruncBy                           out varchar2,
    a_Format                            out varchar2,
    a_CompareYears                      boolean
  ) is
    t_DaysDiff                          number;
  begin

    if a_GroupByPeriod = 'auto' then
      t_DaysDiff := a_Widget.ToDate - a_Widget.FromDate;
      if t_DaysDiff <= 31 then
        a_GroupByPeriod := 'day';
      elsif t_DaysDiff <= 731 then
        a_GroupByPeriod := 'month';
      else
        a_GroupByPeriod := 'year';
      end if;
    end if;

    -- NOTE: if this code is changed then the case statement in
    -- GenerateDatePoints() must be modifed to match
    case a_GroupByPeriod
      when 'year' then
        a_TruncBy := 'YYYY';
        a_Format := 'YYYY';
      when 'month' then
        a_TruncBy := 'MM';
        a_Format := 'Mon YYYY';
      when 'week' then
        a_TruncBy := 'IW';
        a_Format := 'Mon DD YYYY';
      when 'day' then
        a_TruncBy := 'DD';
        a_Format := 'Mon DD YYYY';
      else
        api.pkg_Errors.RaiseError(-20000,
            'Invalid GroupByPeriod: "' || a_GroupByPeriod || '".');
    end case;

    -- drop the year from the x axis data when comparing years
    if a_CompareYears then
      a_Format := replace(a_Format, ' YYYY');
    end if;
  end GroupParameters;

  /*---------------------------------------------------------------------------
   * FormatString()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  function FormatString (
    a_Value                             varchar2,
    a_DataType                          varchar2
  ) return varchar2 is
  begin
    if a_DataType = 'NUMBER' then
      return a_Value;
    elsif a_DataType = 'VARCHAR2' then
      return '''' || replace(a_Value,  '''', '''''') || '''';
    else
      api.pkg_Errors.RaiseError(-20000,
          'Unsupported filter column data type: ' || a_DataType);
    end if;
  end FormatString;

  /*---------------------------------------------------------------------------
   * SparseDistinctList()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure SparseDistinctList (
    a_Values                            udt_StringList,
    a_DistinctValues                    udt_StringList,
    a_OutList                           in out nocopy udt_NumberListByString
  ) is
    t_ValueList                         udt_NumberListByString;
    t_Value                             varchar2(100);
  begin
    -- get list of distinct values from the data
    for i in 1..a_Values.count loop
      if a_Values(i) is not null then
        t_ValueList(a_Values(i)) := null;
      end if;
    end loop;

    -- build a sparse list of values with the seqnum as the data
    -- start with the a_DistinctValues if populated
    for i in 1..a_DistinctValues.count loop
      a_OutList(a_DistinctValues(i)) := i;
    end loop;

    -- append any additional values from the data to the array
    -- this way the added values will be in alphabetical order
    t_Value := t_ValueList.first();
    while t_Value is not null loop
      if not a_OutList.exists(t_Value) then
        a_OutList(t_Value) := a_OutList.count + 1;
      end if;
      t_Value := t_ValueList.next(t_Value);
    end loop;
  end SparseDistinctList;

  /*---------------------------------------------------------------------------
   * VerifyCorralColumnExists()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure VerifyCorralColumnExists (
    a_CorralTable                       udt_CorralTable,
    a_ColumnName                        varchar2
  ) is
    t_DataType                          varchar2(110);
  begin
    t_DataType := CorralColumnDataType(a_CorralTable, a_ColumnName);
  end VerifyCorralColumnExists;

  /*---------------------------------------------------------------------------
   * YearGroupValues()  -- PRIVATE
   *   Create a parallel array of year values from the date list.
   *-------------------------------------------------------------------------*/
  procedure YearGroupValues (
    a_Widget                            udt_Widget,
    a_Dates                             udt_DateList,
    a_yValues                           in out nocopy udt_StringList
  ) is
    t_yRanges                           udt_StringList;
    t_Date                              date;
  begin
    -- first, the easy way - date range within a single year
    if to_char(a_Widget.FromDate, 'YYYY') = to_char(a_Widget.ToDate, 'YYYY')
        then
      for i in 1..a_Dates.count loop
        a_yValues(i) := to_char(a_Dates(i), 'YYYY');
      end loop;

    -- the hard way - date range includes two years
    else
      t_yRanges(3) := to_char(a_Widget.FromDate, 'YYYY')
          || ' - ' || to_char(a_Widget.ToDate, 'YYYY');
      t_yRanges(2) := to_char(add_months(a_Widget.FromDate, -12), 'YYYY')
          || ' - ' || to_char(add_months(a_Widget.ToDate, -12), 'YYYY');
      t_yRanges(1) := to_char(add_months(a_Widget.FromDate, -24), 'YYYY')
          || ' - ' || to_char(add_months(a_Widget.ToDate, -24), 'YYYY');

      t_Date := add_months(a_Widget.ToDate, -24) + 1;
      for i in 1..a_Dates.count loop
        if a_Dates(i) >= a_Widget.FromDate then
          a_yValues(i) := t_yRanges(3);
        elsif a_Dates(i) < t_Date then
          a_yValues(i) := t_yRanges(1);
        else
          a_yValues(i) := t_yRanges(2);
        end if;
      end loop;
    end if;
  end YearGroupValues;

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_StringList,
    a_SecondList                        udt_StringList
  ) is
  begin
    for i in 1..a_SecondList.count loop
      a_MainList(a_MainList.count + 1) := a_SecondList(i);
    end loop;
  end Append;

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_DateList,
    a_SecondList                        udt_DateList
  ) is
  begin
    for i in 1..a_SecondList.count loop
      a_MainList(a_MainList.count + 1) := a_SecondList(i);
    end loop;
  end Append;

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_NumberList,
    a_SecondList                        udt_NumberList
  ) is
  begin
    for i in 1..a_SecondList.count loop
      a_MainList(a_MainList.count + 1) := a_SecondList(i);
    end loop;
  end Append;
  /*---------------------------------------------------------------------------
   * ParseQueryString()
   *-------------------------------------------------------------------------*/
  function ParseQueryString (
    a_QueryString                       varchar2
  ) return udt_UrlParameters is
    t_UrlArgs                           udt_StringList;
    t_Args                              udt_UrlParameters;
    t_Name                              varchar2(4000);
    t_Value                             varchar2(4000);
    t_Index                             pls_integer;
  begin
    t_UrlArgs := extension.pkg_Utils.Split(a_QueryString, '&');
    for i in 1..t_UrlArgs.count loop
      t_Index := instr(t_UrlArgs(i), '=');
      if t_Index > 1 then
        t_Name := substr(t_UrlArgs(i), 1, instr(t_UrlArgs(i), '=')-1);
        t_Value := utl_url.unescape(substr(t_UrlArgs(i), instr(t_UrlArgs(i), '=')+1));
        t_Args(upper(t_Name)) := t_Value;
      end if;
    end loop;
    return t_Args;
  end;

  /*---------------------------------------------------------------------------
   * GetWidgetInfo()
   *-------------------------------------------------------------------------*/
  function GetWidgetInfo (
    a_WidgetId                         udt_Id
  ) return clob is
    t_Widget                           udt_Widget;
  begin
    /* Temporary until all widgets are switched over */
    --if api.pkg_ColumnQuery.Value(a_WidgetId, 'WidgetTypeName') in ('JobIntakeRate','JobsInWarningState') then
      t_Widget := extension.pkg_DashboardWidgetUtils.NewWidget(a_WidgetId, null);
      return extension.pkg_DashboardWidgetUtils.ToJSON(t_Widget);
    --else
    --  return extension.pkg_DashboardUtils.GetWidgetJSON(a_WidgetId, null);
    --end if;
  end;

  /*---------------------------------------------------------------------------
   * GetWidgetJSON()
   *-------------------------------------------------------------------------*/
  function GetWidgetJSON (
    a_WidgetId                         udt_Id,
    a_WidgetArgs                       varchar2
  ) return clob is
    t_JSON                             clob;
    t_SQL                              varchar2(31767);
    t_WidgetTypeId                     udt_Id;
    t_ProcedureName                    varchar2(100);
  begin
    t_WidgetTypeId :=
        extension.pkg_ObjectQuery.RelatedObject(a_WidgetId, 'WidgetType');
    t_ProcedureName :=
        api.pkg_ColumnQuery.Value(t_WidgetTypeId, 'ProcedureName');

    if t_ProcedureName is not null or t_ProcedureName = 'null' then
      t_SQL := 'begin' || chr(10) || ' :p_RetVal := ' || t_ProcedureName ||
          '(:p_WidgetId, :p_WidgetArgs);' || chr(10) || 'end;';
      execute immediate t_SQL using out t_JSON, a_WidgetId, utl_url.unescape(a_WidgetArgs);
    else
      t_JSON := '{}';
    end if;

    rollback; -- clear global temp table
    return t_JSON;
  end;

  /*---------------------------------------------------------------------------
   * GetDashboardInfo()
   *-------------------------------------------------------------------------*/
  function GetDashboardInfo (
    a_DashboardName                     varchar2
  ) return clob is
    t_JSON                              clob;
    t_WidgetJSON                        clob;
    t_Widget                            pkg_DashboardDefinition.udt_Widget;
    t_IncludeExclude                    varchar2(30);

    cursor c_Widgets is
      select
        r2.ToObjectId WidgetId,
        c.Name Category,
        x.ObjectId,
        x.SeqNum,
        x.VisibleByDefault
      from
        api.Relationships r
        join query.o_dashboardchartxref x
            on x.ObjectId = r.ToObjectId
        join api.Relationships r2
            on r2.FromObjectId = r.ToObjectId
            and r2.EndPointId = api.pkg_configquery.EndPointIdForName('o_DashboardWidgetXref','Widget')
        join api.Relationships r3
            on r3.FromObjectId = r.ToObjectId
            and r3.EndPointId = api.pkg_configquery.EndPointIdForName('o_DashboardWidgetXref','Category')
        join query.o_dashboardcategory c
            on c.ObjectId = r3.ToObjectId
      where r.FromObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_Dashboard', 'DashboardName', a_DashboardName)
        and r.EndPointId = api.pkg_configquery.EndPointIdForName('o_Dashboard','WidgetXref')
      order by c.SeqNum, c.Name, x.SeqNum; -- c.Name is need when multiple categories have the same SeqNum
  begin
    t_JSON := '[';
    for w in c_Widgets loop
      t_Widget := extension.pkg_DashboardWidgetUtils.NewWidget(w.widgetid, null);
      t_Widget.category := w.Category;
      t_Widget.dashboardwidgetxrefid := w.ObjectId;

      begin
        select api.pkg_ColumnQuery.Value(r.RelationshipId, 'IncludeExclude')
        into t_IncludeExclude
        from api.Relationships r
        where r.FromObjectId = w.ObjectId
          and r.EndPointId = api.pkg_ConfigQuery.EndPointIdForName('o_DashboardWidgetXref','User')
          and r.ToObjectId = api.pkg_SecurityQuery.EffectiveUserId();

        if t_IncludeExclude = 'Include' then
          t_Widget.visible := 'Y';
        else
          t_Widget.visible := 'N';
        end if;
      exception
      when no_data_found then
        t_Widget.visible := w.VisibleByDefault;
      end;

      t_WidgetJSON := extension.pkg_DashboardWidgetUtils.ToJSON(t_Widget);
      if t_JSON <> '[' then
        t_JSON := t_JSON || ',';
      end if;
      t_JSON := t_JSON || t_WidgetJSON;
    end loop;

    return t_JSON || ']';
  end;

  /*---------------------------------------------------------------------------
   * SetDashboardInfo()
   *-------------------------------------------------------------------------*/
  procedure SetDashboardInfo (
    a_WidgetXrefObjectIds               varchar2,
    a_VisibleList                       varchar2
  ) is
    t_WidgetList                        udt_StringList;
    t_VisibleList                       udt_StringList;
    t_VisibleByDefault                  varchar2(1);
    t_RelationshipId                    udt_Id;
  begin
    t_WidgetList := extension.pkg_Utils.split(a_WidgetXrefObjectIds, ',');
    t_VisibleList := extension.pkg_Utils.split(a_VisibleList, ',');

    for i in 1..t_WidgetList.count loop
      begin
        select r.RelationshipId
        into t_RelationshipId
        from api.Relationships r
        where r.FromObjectId = t_WidgetList(i)
          and r.EndPointId = api.pkg_ConfigQuery.EndPointIdForName('o_DashboardWidgetXref','User')
          and r.ToObjectId = api.pkg_SecurityQuery.EffectiveUserId();
      exception
      when no_data_found then
        t_RelationshipId := null;
      end;

      t_VisibleByDefault := api.pkg_ColumnQuery.Value(t_WidgetList(i), 'VisibleByDefault');
      if t_VisibleList(i) = t_VisibleByDefault and t_RelationshipId is not null then
        api.pkg_RelationshipUpdate.Remove(t_RelationshipId);
      elsif t_VisibleList(i) <> t_VisibleByDefault then
        if t_RelationshipId is null then
          t_RelationshipId := extension.pkg_RelationshipUpdate.New(t_WidgetList(i),
              api.pkg_SecurityQuery.EffectiveUserId(), 'User');
        end if;

        if t_VisibleList(i) = 'Y' then
          api.pkg_ColumnUpdate.SetValue(t_RelationshipId, 'IncludeExclude', 'Include');
        else
          api.pkg_ColumnUpdate.SetValue(t_RelationshipId, 'IncludeExclude', 'Exclude');
        end if;
      end if;
    end loop;
  end;

  /*---------------------------------------------------------------------------
   * GetGroupConfig()
   *-------------------------------------------------------------------------*/
  procedure GetGroupConfig (
    a_WidgetId                          udt_Id,
    a_GroupExpressions                  out udt_StringList,
    a_ShowValues                        out udt_StringList
  ) is
    t_Exp                               varchar2(30);
    t_ShowValues                        varchar2(30);
  begin
    t_Exp := 'Group0Expression';
    t_ShowValues := 'AlwaysShowGroup0Values';
    for i in 1..4 loop
      t_Exp := regexp_replace(t_Exp, '\d', i);
      t_ShowValues := regexp_replace(t_ShowValues, '\d', i);
      a_GroupExpressions(i) := api.pkg_ColumnQuery.Value(a_WidgetId, t_Exp);
      a_ShowValues(i) := api.pkg_ColumnQuery.Value(a_WidgetId, t_ShowValues);
    end loop;
  end GetGroupConfig;

  /*---------------------------------------------------------------------------
   * GetAccessGroupUsersJSON()
   *-------------------------------------------------------------------------*/
  function GetAccessGroupUsersJSON (
    a_AccessGroupName                  varchar2
  ) return clob is
    t_JSON                             clob;
    t_UserIds                          udt_IdList;
  begin
    select u.UserId
    bulk collect into t_UserIds
    from
      api.users u
      join api.accessgroupusers agu
          on agu.UserId = u.UserId
          and u.Active = 'Y'
      join api.accessgroups ag
          on ag.AccessGroupId = agu.AccessGroupId
    where upper(ag.Description) = upper(a_AccessGroupName)
    order by u.Name;

     t_JSON :=
       '{users: [';
       for i in 1..t_UserIds.count loop
          t_JSON := concat(t_JSON, '{id: '|| t_UserIds(i)||', name: "'||api.pkg_columnquery.Value(t_UserIds(i),'Name')||'"},');

       end loop;

       if t_UserIds.count > 0 then
          t_JSON := concat(substr(t_JSON, 1, length(t_JSON)-1), ']}');
       else
          t_JSON := concat(t_JSON,']}');
       end if;

    return t_JSON;
  end;

  /*---------------------------------------------------------------------------
   * ApplyBasicFilter() -- PRIVATE
   *  Apply a basic filter using api.pkg_ColumnQuery.  This is the fall back
   * option if other options aren't available for this column.
   *-------------------------------------------------------------------------*/
  procedure ApplyBasicFilter (
    a_Objects                          in out nocopy api.udt_ObjectList,
    a_ColumnName                       varchar2,
    a_FromValue                        varchar2,
    a_ToValue                          varchar2
  ) is
    t_FilteredObjects                  api.udt_ObjectList;
    t_Value                            varchar2(4000);
  begin
    t_FilteredObjects := api.udt_ObjectList();
    for i in 1..a_Objects.count loop
      t_Value := api.pkg_ColumnQuery.Value(a_Objects(i).ObjectId, a_ColumnName);
      if t_Value between a_FromValue and nvl(a_ToValue, a_FromValue) then
        t_FilteredObjects.extend(1);
        t_FilteredObjects(t_FilteredObjects.count) := api.udt_Object(a_Objects(i).ObjectId);
      end if;
    end loop;

    a_Objects := t_FilteredObjects;
  end;

  /*---------------------------------------------------------------------------
   * ApplyWidgetFilters() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ApplyWidgetFilters (
    a_Objects                          in out nocopy api.udt_ObjectList,
    a_ObjectDefId                      udt_Id,
    a_WidgetId                         udt_Id
  ) is
    t_AllFilters                       udt_IdList;
    t_Filters                          udt_FilterList;
    t_ObjectDefName                    varchar2(30);
    t_ColumnName                       varchar2(30);
    t_FromValue                        varchar2(4000);
    t_ToValue                          varchar2(4000);
    t_IndexFound                       boolean := false;
    t_Count                            pls_integer;
    t_FilteredObjects                  api.udt_ObjectList;
  begin
    select Name
    into t_ObjectDefName
    from api.ObjectDefs
    where ObjectDefId = a_ObjectDefId;

    /* Retrieve information about the filters that apply to this object def */
    t_AllFilters := extension.pkg_ObjectQuery.RelatedObjects(a_WidgetId, 'Filter');
    for i in 1..t_AllFilters.count loop
      begin
        t_Count := t_Filters.count + 1;
        select
          cd.ColumnDefId,
          cd.Name,
          cd.Stored,
          (select decode(count(*), 0, 'N', 'Y')
           from api.IndexDefColumns idc
           where idc.ColumnDefId = cd.ColumnDefId
             and idc.SeqNum = 1) Indexed,
          ep.Name EndPointName,
          cd2.Name LookupColumnName,
          (select decode(count(*), 0, 'N', 'Y')
           from api.IndexDefColumns idc
           where idc.ColumnDefId = cd2.ColumnDefId
             and idc.SeqNum = 1) LookupIndexed
        into
          t_Filters(t_Count).ColumnDefId,
          t_Filters(t_Count).ColumnName,
          t_Filters(t_Count).Stored,
          t_Filters(t_Count).Indexed,
          t_Filters(t_Count).EndPointName,
          t_Filters(t_Count).LookupColumnName,
          t_Filters(t_Count).LookupIndexed
        from
          api.ColumnDefs cd
          left outer join objmodelphys.LookupColumnDefs lcd
              on lcd.ColumnDefId = cd.ColumnDefId
              and lcd.ValueHandlerId = 1
          left outer join api.EndPoints ep
              on ep.EndPointId = lcd.EndPointId
          left outer join api.ColumnDefs cd2
              on cd2.ColumnDefId = lcd.ValueColumnDefId
        where cd.ObjectDefId = a_ObjectDefId
          and lower(cd.Name) like lower(api.pkg_ColumnQuery.Value(t_AllFilters(i), 'ColumnName'));

        t_Filters(i).FromValue := api.pkg_ColumnQuery.Value(t_AllFilters(i), 'FromValue');
        t_Filters(i).ToValue := api.pkg_ColumnQuery.Value(t_AllFilters(i), 'ToValue');
      exception when no_data_found then
        null;
      end;
    end loop;

    /* Try to filter using Cx Procedural Search */
    extension.pkg_CxProceduralSearch.InitializeSearch(t_ObjectDefName);
    for i in 1..t_Filters.count loop
      /* If this is an indexed column, perform a SearchByIndex().  If it's a lookup to an indexed column,
        then perform a SearchByRelatedIndex.  Othherwise, use FilterObjects().*/
      if t_Filters(i).Indexed = 'Y' and t_Filters(i).Stored = 'Y' then
        extension.pkg_CxProceduralSearch.SearchByIndex(t_Filters(i).ColumnName,
            t_Filters(i).FromValue, t_Filters(i).ToValue);
        t_IndexFound := true;
      elsif t_Filters(i).LookupIndexed = 'Y' and t_Filters(i).EndPointName is not null then
        extension.pkg_CxProceduralSearch.SearchByRelatedIndex(t_Filters(i).EndPointName,
            t_Filters(i).LookupColumnName, t_Filters(i).FromValue, t_Filters(i).ToValue, false);
        t_IndexFound := true;
      else
        extension.pkg_CxProceduralSearch.FilterObjects(t_Filters(i).ColumnName,
            t_Filters(i).FromValue, t_Filters(i).ToValue, false);
      end if;

    end loop;

    /* If we were able to find an index, then perform a Cx Procedural Search.
      Otherwise do a basic filter */
    if t_IndexFound then
      -- NOTE: should be able to make the following call, but due to a bug in
      -- cxProceduralSearch the complete list of a_Objects will be returned if
      -- the search found nothing - it should be returning an empty list
      -- extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and', 'and');
      t_FilteredObjects := api.udt_ObjectList();
      extension.pkg_CxProceduralSearch.PerformSearch(t_FilteredObjects, 'and');
      extension.pkg_CollectionUtils.Join(a_Objects, t_FilteredObjects);
    else
      for i in 1..t_Filters.count loop
        ApplyBasicFilter(a_Objects, t_Filters(i).ColumnName, t_Filters(i).FromValue, t_Filters(i).ToValue);
      end loop;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * FilterJoinClause() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function FilterJoinClause(
    a_Widget                            udt_Widget,
    a_JoinColumn                        varchar2,
    a_AlwaysJoin                        boolean default false
  ) return varchar2 is
    t_FilterIds                         udt_IdList;
    t_CorralTable                       udt_CorralTable;
    t_ColumnName                        varchar2(30);
    t_FromValue                         varchar2(4000);
    t_ToValue                           varchar2(4000);
    t_DataType                          varchar2(110);
    t_Sql                               varchar2(4000);
  begin
    t_FilterIds := extension.pkg_ObjectQuery.RelatedObjects(a_Widget.WidgetId,
        'Filter');

    if t_FilterIds.count > 0 or a_AlwaysJoin then
      t_CorralTable := GetCorralTable(a_Widget.WidgetId);

      t_Sql :=
        '  join ' || t_CorralTable.Schema || '.' || t_CorralTable.Name ||
            ' ct' || gc_CRLF ||
        '    on ct.' || t_CorralTable.IdName || ' = ' || a_JoinColumn;
    end if;

    for i in 1..t_FilterIds.count loop
      t_ColumnName := api.pkg_ColumnQuery.Value(t_FilterIds(i), 'ColumnName');
      t_FromValue := api.pkg_ColumnQuery.Value(t_FilterIds(i), 'FromValue');
      t_ToValue := api.pkg_ColumnQuery.Value(t_FilterIds(i), 'ToValue');

      t_DataType := CorralColumnDataType(t_CorralTable, t_ColumnName);

      if t_FromValue = t_ToValue or t_ToValue is null then
        t_Sql := t_Sql || gc_CRLF || ' and ct.' || t_ColumnName || ' = ' ||
            FormatString(t_FromValue, t_DataType);
      else
        t_Sql := t_Sql || gc_CRLF || ' and ct.' || t_ColumnName ||
            ' between ' ||
            FormatString(t_FromValue, t_DataType) || ' and ' ||
            FormatString(t_ToValue, t_DataType);
      end if;
    end loop;

    if t_Sql is not null then
      t_Sql := t_Sql || gc_CRLF;
    end if;

    return t_Sql;
  end FilterJoinClause;

  /*---------------------------------------------------------------------------
   * GetColumnValues()
   *-------------------------------------------------------------------------*/
  function GetColumnValues (
    a_Objects                           api.udt_ObjectList,
    a_ObjectDefId                       udt_Id,
    a_ColumnName                        varchar2
  ) return udt_StringList is
    t_ColumnDefId                       udt_Id;
    t_DataType                          varchar2(30);
    t_Values                            udt_StringList;
    c_NullReplacement                   constant varchar2(4) := 'NULL';
  begin
    if a_ColumnName is null then
      return g_NullStringList;
    end if;

    -- use the corral tables for job and process statuses
    if lower(a_ColumnName) = 'statusname' then
      select nvl(j.StatusName, c_NullReplacement)
        bulk collect into t_Values
        from
          table(a_Objects) t
          join extensioncorral.Jobs j
            on j.JobId = t.ObjectId
        order by t.ObjectId;

      return t_Values;
    end if;

    -- get ColumnDefId and DomainId if it is a stored column
    begin
      select
        ColumnDefId,
        DataType
      into
        t_ColumnDefId,
        t_DataType
      from api.ColumnDefs c
      where ObjectDefId = a_ObjectDefId
        and Stored = 'Y'
        and lower(Name) = lower(a_ColumnName)
        -- eliminate expression with overide column defs
        and not exists (
          select *
          from objmodelphys.ExpressionColumnDefs e
          where e.ColumnDefId = c.ColumnDefId);
    exception
    when no_data_found then
      t_ColumnDefId := null;
      t_DataType := null;
    end;

    if t_DataType = 'list' then

      -- query for Domain List columns
      select nvl(substr(nvl(v.Value, d.AttributeValue), 1, 100),
          c_NullReplacement)
        bulk collect into t_Values
        from
          table(a_Objects) t
          left join possedata.ObjectColumnData d
            on d.ObjectId = t.ObjectId
            and d.ColumnDefId = t_ColumnDefId
          left outer join objmodelphys.AllDomainListOfValues v
            on v.ListValueId = d.AttributeValueId
        order by t.ObjectId;
    elsif t_ColumnDefId is not null then

      -- query for stored columns
      select nvl(AttributeValue, c_NullReplacement)
        bulk collect into t_Values
        from
          table(a_Objects) t
          left join possedata.ObjectColumnData d
            on d.ObjectId = t.ObjectId
            and d.ColumnDefId = t_ColumnDefId
        order by t.ObjectId;
    else

      -- fall back to package call for other types
      select nvl(api.pkg_ColumnQuery.Value(t.ObjectId, a_ColumnName),
          c_NullReplacement)
        bulk collect into t_Values
        from table(a_Objects) t
        order by t.ObjectId;
    end if;

    return t_Values;
  end;

-- FIX ME - this procedure will soon replace the above one
  /*---------------------------------------------------------------------------
   * GetColumnValues()
   *-------------------------------------------------------------------------*/
  function GetColumnValues (
    a_CorralTable                       udt_CorralTable,
    a_ColumnName                        varchar2
  ) return udt_StringList is
    t_Count                             pls_integer;
    t_Values                            udt_StringList;
    t_Query                             varchar2(1000);
    c_NullReplacement                   constant varchar2(4) := 'NULL';
  begin
    if a_ColumnName is null then
      return g_NullStringList;
    end if;

    VerifyCorralColumnExists(a_CorralTable, a_ColumnName);

    t_Query :=
      'select nvl(to_char(ct.' || a_ColumnName || '), :1)' ||
      '  from extension.DashboardObjects_gt do' ||
      '  join ' || a_CorralTable.Schema || '.' || a_CorralTable.Name || ' ct' ||
      '    on ct.' || a_CorralTable.IdName || ' = do.ObjectId' ||
      '  order by do.ObjectId';

    execute immediate t_Query
    bulk collect into t_Values
    using c_NullReplacement;

    return t_Values;
  end GetColumnValues;

  /*---------------------------------------------------------------------------
   * GetGroupColumnValues()
   *-------------------------------------------------------------------------*/
  function GetGroupColumnValues (
    a_Widget                            udt_Widget
  ) return udt_StringList is
  begin
    return GetColumnValues(GetCorralTable(a_Widget.WidgetId),
        a_Widget.subgroup);
  end GetGroupColumnValues;

  /*---------------------------------------------------------------------------
   * GetCorralTable()
   *-------------------------------------------------------------------------*/
  function GetCorralTable (
    a_WidgetId                          pls_integer
  ) return udt_CorralTable is
    t_CorralTable                       udt_CorralTable;
  begin
    t_CorralTable.Schema := api.pkg_ColumnQuery.Value(a_WidgetId,
        'CorralSchemaName');
    t_CorralTable.Name := api.pkg_ColumnQuery.Value(a_WidgetId,
        'CorralTableName');
    t_CorralTable.IdName := api.pkg_ColumnQuery.Value(a_WidgetId,
        'CorralObjectIdColumnName');

    if t_CorralTable.Schema is null then
      api.pkg_Errors.RaiseError(-20000,
          'Corral Table Schema not configured for "' ||
          api.pkg_ColumnQuery.Value(a_WidgetId, 'Title') || '" widget.');
    elsif t_CorralTable.Name is null then
      api.pkg_Errors.RaiseError(-20000,
          'Corral Table Name not configured for "' ||
          api.pkg_ColumnQuery.Value(a_WidgetId, 'Title') || '" widget.');
    elsif t_CorralTable.IdName is null then
      api.pkg_Errors.RaiseError(-20000,
          'Corral Object Id Column Name not configured for "' ||
          api.pkg_ColumnQuery.Value(a_WidgetId, 'Title') || '" widget.');
    end if;

    return t_CorralTable;
  end GetCorralTable;

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Widget                            in out nocopy udt_Widget,
    a_xValues                           udt_StringList,
    a_sgValues                          udt_StringList default g_NullStringList,
    a_xDistinctValues                   udt_StringList default g_NullStringList,
    a_sgDistinctValues                  udt_StringList default g_NullStringList
  ) is
    t_NullNumberList                    udt_NumberList;
  begin
    GroupByValues(a_Widget, a_xValues, a_sgValues, a_xDistinctValues,
        a_sgDistinctValues, gc_Count, t_NullNumberList);
  end GroupByValues;

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Widget                            in out nocopy udt_Widget,
    a_xValues                           udt_StringList,
    a_sgValues                          udt_StringList,
    a_xDistinctValues                   udt_StringList,
    a_sgDistinctValues                  udt_StringList,
    a_FunctionName                      varchar2,
    a_FunctionValues                    udt_NumberList
  ) is
    t_xList                             udt_NumberListByString;
    t_sgList                            udt_NumberListByString;
    t_x                                 varchar2(10000);
    t_sg                                varchar2(10000);
    t_HasSubGroup                       boolean;
    t_Matrix                            udt_DataMatrix;
    t_CountMatrix                       udt_DataMatrix;
    c_Zero                              constant char(1) := '0';
t_key  varchar2(100);
t_AllDistinct udt_StringList;
  begin
    if a_FunctionName not in (gc_Count, gc_Sum, gc_Average) then
      api.pkg_Errors.RaiseError(-20000, 'Invalid function name "' ||
          a_FunctionName || '" passed in to GroupByValues.');
    end if;

    if a_sgValues.count > 0 and a_xValues.count <> a_sgValues.count then
      api.pkg_Errors.RaiseError(-20000, 'GroupByValues x and sub-group '
          || 'arrays are not the same length: ' || a_xValues.count || ' and '
          || a_sgValues.count);
    end if;

    if a_FunctionValues.count > 0
        and a_xValues.count <> a_FunctionValues.count then
      api.pkg_Errors.RaiseError(-20000, 'GroupByValues x and FunctionValue '
          || 'arrays are not the same length: ' || a_xValues.count || ' and '
          || a_FunctionValues.count);
    end if;

    -- build sparse lists of x and sub-group values with the seqnum as the data
    -- start with the a_xDistinctValues if populated
    SparseDistinctList(a_xValues, a_xDistinctValues, t_xList);
    if a_sgValues.count > 0 then
      SparseDistinctList(a_sgValues, a_sgDistinctValues, t_sgList);
      t_HasSubGroup := true;
    else
      t_sgList(c_Zero) := 1;
      t_HasSubGroup := false;
    end if;

    -- create a matrix of all points with the value set to 0
    t_x := t_xList.first();
    while t_x is not null loop
      t_sg := t_sgList.first();
      while t_sg is not null loop
        t_Matrix(t_x)(t_sg) := 0;
        t_CountMatrix(t_x)(t_sg) := 0;
        t_sg := t_sgList.next(t_sg);
      end loop;
      t_x := t_xList.next(t_x);
    end loop;

    -- load the matrix with data
    t_sg := c_Zero;
    for i in 1..a_xValues.count loop
      t_x := a_xValues(i);
      if t_HasSubGroup then
        t_sg := a_sgValues(i);
      end if;
      if t_sg is not null then
        if a_FunctionName = gc_Count then
          t_Matrix(t_x)(t_sg) := t_Matrix(t_x)(t_sg) + 1;
        elsif a_FunctionValues(i) is not null then
          t_Matrix(t_x)(t_sg) := t_Matrix(t_x)(t_sg) + a_FunctionValues(i);
          t_CountMatrix(t_x)(t_sg) := t_CountMatrix(t_x)(t_sg) + 1;
        end if;
      end if;
    end loop;

    if a_FunctionName = gc_Average then
      -- calculate the average for each value in the matrix
      t_x := t_xList.first();
      while t_x is not null loop
        t_sg := t_sgList.first();
        while t_sg is not null loop
          if t_CountMatrix(t_x)(t_sg) <> 0 then
            t_Matrix(t_x)(t_sg) := round(t_Matrix(t_x)(t_sg)
                / t_CountMatrix(t_x)(t_sg));
          end if;
          t_sg := t_sgList.next(t_sg);
        end loop;
        t_x := t_xList.next(t_x);
      end loop;
    end if;
    t_CountMatrix.delete;

    extension.pkg_dashboardwidgetutils.ApplyDataMatrixToWidget(t_Matrix,
        a_Widget, t_xList, t_sgList);
  end GroupByValues;

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *   This overload is for Compare previous years Bar graphs.
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Widget                            in out nocopy udt_Widget,
    a_xValues                           udt_StringList,
    a_Dates                             udt_DateList,
    a_xDistinctValues                   udt_StringList default g_NullStringList
  ) is
    t_sgValues				udt_StringList;
  begin
    YearGroupValues(a_Widget, a_Dates, t_sgValues);
    GroupByValues(a_Widget, a_xValues, t_sgValues, a_xDistinctValues);
  end GroupByValues;

  /*---------------------------------------------------------------------------
   * GroupByTimeline()
   *-------------------------------------------------------------------------*/
  procedure GroupByTimeline (
    a_Widget                           in out nocopy udt_Widget,
    a_Dates                             udt_DateList,
    a_GroupValues                       udt_StringList default g_NullStringList,
    a_FunctionName                      varchar2 default gc_Count,
    a_FunctionValues                    udt_NumberList default g_NullNumberList
  ) is
    t_GroupByPeriod                     varchar2(10);
    t_TruncBy                           varchar2(4);
    t_Format                            varchar2(12);
    t_DatePoints                        udt_StringList;
    t_xValues                           udt_StringList;
    t_sgValues                          udt_StringList;
    t_CompareYears                      boolean := false;
    t_yRanges                           udt_StringList;
  begin
    if a_GroupValues.count > 0 and a_Dates.count <> a_GroupValues.count then
      api.pkg_Errors.RaiseError(-20000, 'GroupByTimeline Dates and Group '
          || 'arrays are not the same length: ' || a_Dates.count || ' and '
          || a_GroupValues.count);
    end if;

    if a_FunctionValues.count > 0 and a_Dates.count <> a_FunctionValues.count
        then
      api.pkg_Errors.RaiseError(-20000, 'GroupByTimeline Dates and Function '
          || 'arrays are not the same length: ' || a_Dates.count || ' and '
          || a_FunctionValues.count);
    end if;

    t_CompareYears := a_Widget.view_ = gc_Compare;
    t_GroupByPeriod := 'auto';
    GroupParameters(a_Widget, t_GroupByPeriod, t_TruncBy, t_Format,
        t_CompareYears);

    -- get a list of all the required date points
    t_DatePoints := GenerateDatePoints(a_Widget.FromDate, a_Widget.ToDate,
        t_TruncBy, t_Format, t_GroupByPeriod);

    -- convert the dates to the appropriate string value
    for i in 1..a_Dates.count loop
      t_xValues(i) := to_char(trunc(a_Dates(i), t_TruncBy), t_Format);
    end loop;

    if t_CompareYears then
      YearGroupValues(a_Widget, a_Dates, t_sgValues);
      GroupByValues(a_Widget, t_xValues, t_sgValues, t_DatePoints,
          g_NullStringList, a_FunctionName, a_FunctionValues);
    else
      GroupByValues(a_Widget, t_xValues, a_GroupValues, t_DatePoints,
          g_NullStringList, a_FunctionName, a_FunctionValues);
    end if;
  end GroupByTimeline;

end pkg_DashboardUtils;

/

