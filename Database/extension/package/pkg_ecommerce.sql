create or replace package           pkg_eCommerce is

  -- Author  : CARL.BRUINSMA
  -- Created : 11/17/2008 14:01:09
  -- Purpose : Procedures needed to setup and process an eCommerce transaction

  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

 /*---------------------------------------------------------------------------
  * ManuallyApprove - PUBLIC
  * When a reconciliation is required the eCom Administrator may manually
  * enter the data and approve the transaction.  Creates and completes the
  * Record eCommerce Transaction Outcome with Approved. Sets the Transaction
  * Status.
  *-------------------------------------------------------------------------*/
  procedure ManuallyApprove (
    a_ObjectId              udt_Id,
    a_AsOfDate              date
  );

  /*---------------------------------------------------------------------------
   * MiraPaySetup - PUBLIC
   * Retrieves information needed to perform a MiraPay eCommerce transaction
   * and stores them on the transaction job.
   *-------------------------------------------------------------------------*/
  procedure MiraPaySetup(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * Approval - PUBLIC
   * Create a payment for all fees on all the jobs in the ObjectList the
   * transaction was created to pay for.  The jobs will be notified
   * this is complete by setting the column specified in a_MarkJobsAsDoneUsing
   * to 'Y' on each of them.
   *-------------------------------------------------------------------------*/
  procedure Approval(
    a_ProcessId                             udt_Id,
    a_AsOfDate                              date,
    a_MarkJobsAsDoneUsing                   char
  );


  /*---------------------------------------------------------------------------
   * TransactionSearch - PUBLIC
   * Search for transaction jobs based on transaction ID, applicationNum
   * (in the list of objects the transaction job was created to pay for, ie
   * objectList), createdDate and  processedDate (the date a response was
   * recieved from the ePayment provider) ObjectList is a comma seperated list
   * of jobids so a keyword search is used on it.
   *-------------------------------------------------------------------------*/
  procedure TransactionSearch (
    a_TransactionID             varchar2,
    a_ApplicationNum            number,
    a_CreatedDateFrom           date,
    a_CreatedDateTo             date,
    a_ProcessedDateFrom         date,
    a_ProcessedDateTo           date,
    a_Results                out api.udt_Objectlist
  );
end pkg_eCommerce;

 
/

grant execute
on pkg_ecommerce
to posseextensions;

