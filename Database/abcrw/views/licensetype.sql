create or replace view licensetype as
select lt.LICENSETYPEID,
       lt.Code,
       lt.Name,
       lt.ObjectDefName,
       lt.Active,
       lt.IsSecondaryOnly,
       lt.IsSpecialEventLicense,
       lt.ExpirationMethod,
       lt.ExpirationMonth,
       lt.ExpirationNumber,
       lt.ExpirationReminderDays,
       lt.GracePeriodDays,
       lt.CPLSubmission,
       lt.EnableWarehouseAddressEntry,
       lt.issuingAuthority,
       lt.PremiseAddressRequired,
       lt.DefaultLicensingClerkId,
       lt.DefaultLicensingSupervisorId,
       lt.GLAccountId,
       lt.IncludeOnAddPrivilegeReport,
       lt.IsValidAsregistrantDistributor,
       lt.LicenseCertificateText,
       lt.AvailableOnline,
       lt.AvailableToPrintOnline,
       lt.CanRenewDaysPrior,
       lt.CanRenewExpired,
       lt.OnlineDescription,
       lt.RenewalAvailableOnline,
       lt.ExpirationEndDay,
       lt.ExpirationEndMonth,
       lt.ExpirationStartDay,
       lt.ExpirationStartMonth,
       lt.AutomaticCancellationPeriod,
       lt.DaysToNotify,
       lt.EnableAutomaticCancellation,
       lt.SendCancellationNotification,
       lt.NotifyMunicipality,
       lt.NotifyPolice
  from BCPCORRAL.Licensetype lt;

grant select
on licensetype
to
  dashboard,
  posseextensions,
  posseuser;

