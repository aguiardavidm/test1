create or replace view licensejob as
select lj.JOBID
     , lj.LicenseObjectId
     , lj.JobNameDescription
     , lj.ObjectDefDescription
     , lj.ObjectDefName
     , lj.Region
     , lj.CompletedDate
     , lj.CreatedDate
     , lj.StatusDescription
     , lj.StatusId
     , lj.ExternalFileNum
     , lj.ObjectDefId
     , lj.StatusName
     , lj.LicenseType
     , lj.RegionObjectId
  from BCPCORRAL.LicenseJob lj;

grant select
on licensejob
to
  abcnj,
  dashboard,
  posseextensions,
  posseuser;

