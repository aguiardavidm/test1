create or replace view abcrw.emails as
select e.*, CAST(substr(ee.body,1,4000) AS VARCHAR2(4000)) body
from abcrw.email_data e
join extension.Emails ee
on   ee.ObjectId = e.EmailId;
