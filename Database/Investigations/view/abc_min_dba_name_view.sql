create or replace view investigations.abc_min_dba_name_view as
select abc_num,
       (select e.DoingBusinessAsTradeNames
          from abcrw.establishment e
         where e.establishmentid = t.EstablishmentId) name
from (select distinct cast(LIC.LicenseNumber as varchar2(15)) abc_num,
            min(est.ESTABLISHMENTID) EstablishmentId
       FROM bcpcorral.License LIC
       join abcrw.establishment est on est.ESTABLISHMENTID = lic.EstablishmentId
      where lic.state = 'Active'
        group by lic.LicenseNumber) t
        order by 1