create or replace view investigations.abc_permit_min_date_view as
select cast(p.PermitObjectId as number) perm_object_id
     , cast(p.PermitNumber as varchar2(18)) perm_num
     , coalesce(p.EarliestEventDate, p.EffectiveDate, p.IssueDate) min_date
  from abcrw.abc_permit p
  where p.PermitNumber is not null
    and p.PermitTypeCode != 'SOL';
