create or replace view investigations.abc_solicitor_emp_all_view as
select replace(substr(rsl.LicenseNumber,1,11), '-', '') employer_lic_num
     , rsl.Licensee employer_name
  from bcpcorral.license rsl
  where rsl.LicenseNumber is not null
    and rsl.IsLatestVersion = 'Y'
    and rsl.state != 'Pending';
