create or replace view investigations.abc_10a_interest_view as
select distinct cast(lic.LicenseNumber as varchar2(15))lic_num
     ,(case
          when length(corp.dup_FormattedName) > 60
            then cast(corp.dup_FormattedName as varchar2(57)) || '...'
          else cast(corp.dup_FormattedName as varchar2(60))
        end) name
     ,  addr.FormattedStreetAddressShort addr
     , addr.Suite addr2
     , cast(addr.CityTown as varchar2(25)) city
     , addr.ProvinceCode state
     , cast(addr.PostalCode as varchar2(5)) zip
     , cast(addr.ZipCodeExtension as varchar2(4)) zipext
     , substr(corp.BusinessPhone, 1, 3) business_phone_area_code
     , substr(corp.BusinessPhone, 4, 3) business_phone_exchange
     , substr(corp.BusinessPhone, 7, 4) business_phone_number
     , substr(corp.MobilePhone, 1, 3) home_phone_area_code
     , substr(corp.MobilePhone, 4, 3) home_phone_exchange
     , substr(corp.MobilePhone, 7, 4) home_phone_number
     , cast(corp.SSNTINEncrypted as varchar2(12))ssn
     , corp.PercentageInterest pct_business_control
     , corp.CommonVotingShares number_shares_owned
  from bcpcorral.license lic
  --Join to the corporate structure materialized view
  join investigations.licenseecorpstructure_mv corp
      on corp.RootLE = lic.LicenseeId
  left outer join abcrw.address addr on nvl(corp.MailingAddressId, corp.PhysicalAddressId) = addr.ADDRESSID
 where corp.active = 'Y'