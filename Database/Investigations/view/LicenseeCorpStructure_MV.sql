create materialized view investigations.LicenseeCorpStructure_MV
refresh on demand
start with trunc(sysdate) + 13/12 next sysdate+1
as
select connect_by_root ParticipatesInLegalEntityId RootLE, r.PARTICIPATESINLEGALENTITYID ParentLE, le.LEGALENTITYID ChildLE, level NestLevel, le.*, r.*
                     from abcrw.legalentitycorpstructure r
                     join abcrw.LegalEntity le on le.LEGALENTITYID = r.IncludesLegalEntityId
                    start with r.ParticipatesInLegalEntityId in (select l.LicenseeId 
                                                                   from bcpcorral.license l)
                   connect by nocycle r.ParticipatesInLegalEntityId = prior r.IncludesLegalEntityId
                          and r.RelationshipId != prior r.RelationshipId;
                          
create index investigations.CorpStrucRootLEIX1 on investigations.LicenseeCorpStructure_MV(RootLE);