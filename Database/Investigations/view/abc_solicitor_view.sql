create or replace view investigations.abc_solicitor_view as
select cast(p.PermitNumber as number(6))perm_num
     , (case
          when length(sol.dup_FormattedName) > 60
            then cast(sol.dup_FormattedName as varchar2(57)) || '...'
          else cast(sol.dup_FormattedName as varchar2(60))
        end) perm_name
     , cast(rsl.Licensee as varchar2(60)) emp_by
     , cast(substr(rsl.LicenseNumber,1,4) ||
       substr(rsl.LicenseNumber,6,2) ||
       substr(rsl.LicenseNumber,9,3)as varchar2(9)) emp_by_lic_num

     , sol_addr.FormattedStreetAddressShort mailing_addr
     , cast(sol_addr.Suite as varchar2(6)) mailing_addr2
     , cast(sol_addr.CityTown as varchar2(25)) mailing_city
     , sol_addr.ProvinceCode mailing_state
     , cast(sol_addr.PostalCode as varchar2(5)) mailing_zip
     , cast(sol_addr.ZipCodeExtension as varchar2(4)) mailing_zipext

     , substr(sol.HomePhone, 1,3) mailing_phone_area_code
     , substr(sol.HomePhone, 4,3) mailing_phone_exchange
     , substr(sol.HomePhone, 7,4) mailing_phone_number
     , substr(sol.BusinessPhone, 1,3) business_phone_area_code
     , substr(sol.BusinessPhone, 4,3) business_phone_exchange
     , substr(sol.BusinessPhone, 7,4) business_phone_number
     , substr(sol.MobilePhone, 1,3) home_phone_area_code
     , substr(sol.MobilePhone, 4,3) home_phone_exchange
     , substr(sol.MobilePhone, 7,4) home_phone_number

  from abcrw.abc_permit p
  join abcrw.legalentity sol on p.SolicitorId = sol.LEGALENTITYID
  left outer join abcrw.address sol_addr on sol.MailingAddressObjectId = sol_addr.ADDRESSID
  join bcpcorral.license rsl on p.licenseId = rsl.ObjectId
  where p.State = 'Active'
    and p.PermitTypeCode = 'SOL';
