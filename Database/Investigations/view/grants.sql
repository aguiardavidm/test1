grant select on abcrw.establishment to investigations;
grant select on abcrw.establishmenthistory to investigations;
grant select on abcrw.licenseestablishmenthist to investigations;
grant select on abcrw.address to investigations;
grant select on abcrw.office to investigations;
grant select on abcrw.legalentitycorpstructure to investigations; 
grant select on abcrw.legalentitytype to investigations;
grant select on abcrw.newapplicationjob to investigations;
grant select on abcrw.permit to investigations;
grant select on abcrw.permittype to investigations;
grant select on abcrw.license to investigations;
grant select on abcrw.county to investigations; 
grant select on abcrw.municipality to investigations;  
grant select on abcrw.LicenseWarehouseAddress to investigations;

grant create table to investigations;
grant create materialized view to investigations;
grant unlimited tablespace to investigations; --This seems like kinda a bad idea...