create or replace view investigations.abc_position_view as
select l.licensenumber
     , cast(corp.INCLUDESLEGALENTITYID as number(9)) name_mstr_id_num
     , cast(corp.RelationshipType as varchar2(4000)) position_cd
     , (select lt.StartDate
          from api.objects o
          join api.logicaltransactions lt on lt.LogicalTransactionId = o.CreatedLogicalTransactionId
         where o.ObjectId = corp.relationshipid) dt_row_updt
     , (select lt.StartDate
          from api.objects o
          join api.logicaltransactions lt on lt.LogicalTransactionId = o.LogicalTransactionId
         where o.ObjectId = corp.relationshipid) updt_oper_id
  from investigations.licenseecorpstructure_mv corp
  join abcrw.license l on l.licenseeid = corp.rootle
 where corp.relationshiptype is not null