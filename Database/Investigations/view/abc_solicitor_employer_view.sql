create or replace view investigations.abc_solicitor_employer_view as
select cast(substr(rsl.LicenseNumber,1,4) ||
       substr(rsl.LicenseNumber,6,2) ||
       substr(rsl.LicenseNumber,9,3) as varchar2(9)) employer_lic_num,
       cast(le.FormattedName as varchar(60))employer_name
  from  bcpcorral.license rsl
  join abcrw.legalentity le on le.LEGALENTITYID = rsl.LicenseeId
  where rsl.State = 'Active'