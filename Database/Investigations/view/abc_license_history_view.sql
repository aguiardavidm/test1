create or replace view investigations.abc_license_history_view as
select distinct cast(Lic.LicenseNumber as varchar2(15)) lic_num
     , (case
          when length(le.dup_FormattedName) > 60
            then cast(le.dup_FormattedName as varchar2(57)) || '...'
          else cast(le.dup_FormattedName as varchar2(60))
        end) lic_name
     , cast(nvl(est.DoingBusinessAsTradeNames, esth.DoingBusinessAs) as varchar2(60)) trade_as
     -- Establishment (Premises)
     , le_paddr.FormattedStreetAddressShort premise_addr
     , cast(le_paddr.Suite as varchar2(6))premise_addr2
     , cast(le_paddr.CityTown as varchar2(25))premise_city
     , le_paddr.ProvinceCode premise_state
     , cast(le_paddr.PostalCode as varchar2(5)) premise_zip
     , cast(le_paddr.ZipCodeExtension as varchar2(4)) premise_zipext
     -- Mailing
     , le_maddr.FormattedStreetAddressShort mailing_addr
     , cast(le_maddr.Suite as varchar2(6)) mailing_addr2
     , cast(le_maddr.CityTown as varchar2(25)) mailing_city
     , le_maddr.ProvinceCode mailing_state
     , cast(le_maddr.PostalCode as varchar2(5)) mailing_zip
     , cast(le_maddr.ZipCodeExtension as varchar2(4))  mailing_zipext
     -- Phone Number
     , substr(le.HomePhone, 1, 3) mailing_phone_area_code
     , substr(le.HomePhone, 4, 3) mailing_phone_exchange
     , substr(le.HomePhone, 7, 4) mailing_phone_number
     , substr(le.BusinessPhone, 1, 3) business_phone_area_code
     , substr(le.BusinessPhone, 4, 3) business_phone_exchange
     , substr(le.BusinessPhone, 7, 4) business_phone_number
     , substr(le.MobilePhone, 1, 3) home_phone_area_code
     , substr(le.MobilePhone, 4, 3) home_phone_exchange
     , substr(le.MobilePhone, 7, 4) home_phone_number

  FROM bcpcorral.License LIC
  JOIN abcrw.legalentity le
    ON le.LEGALENTITYID = LIC.LicenseeId
  left outer join abcrw.address le_maddr on le.MailingAddressId = le_maddr.ADDRESSID
  left outer join abcrw.address le_paddr on le_paddr.ADDRESSID = le.PhysicalAddressId
  -- Establishment
  left outer join abcrw.establishment est on est.ESTABLISHMENTID = lic.EstablishmentId
  left outer join abcrw.licenseestablishmenthist leh on leh.LicenseObjectId = lic.LICENSEID
  left outer join abcrw.establishmenthistory esth on esth.ESTABLISHMENTID = leh.EstablishHistObjectId
 where lic.LICENSEID = coalesce((select mrl.licenseid licenseid
                                   from abcrw.license mrl
                                  where mrl.licensenumber = lic.LicenseNumber
                                    and mrl.state = 'Active'),
                                  (select max(mrl.licenseid) licenseid
                                    from abcrw.license mrl
                                   where mrl.licensenumber = lic.LicenseNumber
                                     and mrl.state IN ('Expired', 'Closed')
                                   group by mrl.licensenumber))
;
