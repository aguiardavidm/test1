create or replace view investigations.abc_min_dba_name_history_view as
select abc_num,
       coalesce((select e.DoingBusinessAsTradeNames
                   from abcrw.establishment e
                  where e.establishmentid = t.EstablishmentId),
                (select eh.DoingBusinessAs
                   from abcrw.establishmenthistory eh
                  where eh.establishmentid = t.EstablishmentHistId),
                'Not Available') name
from (select distinct cast(LIC.LicenseNumber as varchar2(15)) abc_num,
            min(est.ESTABLISHMENTID) EstablishmentId,
            min(esth.ESTABLISHMENTID) EstablishmentHistId
       FROM bcpcorral.License LIC
       left outer join abcrw.establishment est on est.ESTABLISHMENTID = lic.EstablishmentId
       left outer join abcrw.licenseestablishmenthist leh on leh.LicenseObjectId = lic.LICENSEID
       left outer join abcrw.establishmenthistory esth on esth.ESTABLISHMENTID = leh.EstablishHistObjectId
      where lic.state <> 'Pending'
        and (est.ESTABLISHMENTID is not null or esth.ESTABLISHMENTID is not null)
        group by lic.LicenseNumber) t