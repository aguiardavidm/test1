create or replace view investigations.abc_municipality_view as
select cast(o.MunicipalityCountyCode as varchar2(12)) municipal_code
     , cast(o.Name as varchar2(64)) municipal_name
  from abcrw.office o;
