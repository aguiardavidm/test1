create or replace view investigations.abc_permit_view as
select cast(p.PermitObjectId as number) perm_object_id
     , cast(p.PermitNumber as varchar2(18))perm_num
     , cast(replace(p.LicenseNumber, '-', '') as varchar2(12)) lic_num
     , cast(p.PermitTypeCode as varchar2(5)) perm_type_code
     , cast(p.PermitType as varchar2(64)) perm_type_desc
     
     -- Permittee
     , (case 
          when length(le.dup_FormattedName) > 60 
            then cast(le.dup_FormattedName as varchar2(57)) || '...'
          else cast(le.dup_FormattedName as varchar2(60))
        end) name
     , le_maddr.FormattedStreetAddressShort addr
     , le_maddr.Suite addr2
     , cast(le_maddr.CityTown as varchar2(25)) city
     , le_maddr.ProvinceCode state
     , cast(le_maddr.PostalCode as varchar2(5)) zip
     , cast(le_maddr.ZipCodeExtension as varchar2(4)) zipext

     , p.EffectiveDate date_effective
     , p.ExpirationDate date_expire
     , p.IssueDate date_issued
     , cast(null as varchar2(60)) location
     , cast(null as varchar2(60)) event_type
     , cast(null as varchar2(60)) event_place

     -- Event Address Details
     , ev_addr.FormattedStreetAddressShort event_addr
     , cast(ev_addr.CityTown as varchar2(25)) event_city
     , ev_addr.ProvinceCode event_state
     , cast(ev_addr.PostalCode as varchar2(5)) event_zip
     , cast(ev_addr.ZipCodeExtension as varchar2(4)) event_zipext
     
     -- Other Details
     , p.State status
     
  from abcrw.abc_permit p
  join abcrw.legalentity le on p.PermitteeId = le.LEGALENTITYID
  -- address (where available)
  left outer join abcrw.address le_maddr on le.PhysicalAddressId = le_maddr.ADDRESSID
  -- Event Location Address (where available)
  left outer join abcrw.address ev_addr on p.EventLocationAddressId = ev_addr.ADDRESSID
 where p.PermitTypeCode != 'SOL'
