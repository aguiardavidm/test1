create or replace view investigations.abc_license_view as
select cast(Lic.LicenseNumber as varchar2(15)) lic_num
     , cast(LE.FormattedName as varchar2(60)) lic_name
     , cast(Est.DoingBusinessAsTradeNames as varchar2(60)) trade_as
     -- Physical Address
     , est_paddr.FormattedStreetAddressShort premise_addr
     , cast(est_paddr.Suite as varchar2(6)) premise_addr2
     , cast(est_paddr.CityTown as varchar2(25)) premise_city
     , cast(est_paddr.ProvinceCode as varchar2(2)) premise_state
     , cast(est_paddr.PostalCode as varchar2(5)) premise_zip
     , cast(est_paddr.ZipCodeExtension as varchar2(4)) premise_zipext
     -- Mailing
     , cast(le_maddr.FormattedStreetAddressShort as varchar2(33)) mailing_addr
     , cast(le_maddr.Suite as varchar2(6)) mailing_addr2
     , cast(le_maddr.CityTown as varchar2(25)) mailing_city
     , cast(le_maddr.ProvinceCode as varchar2(2)) mailing_state
     , cast(le_maddr.PostalCode as varchar2(5)) mailing_zip
     , cast(le_maddr.ZipCodeExtension as varchar2(4)) mailing_zipext
     -- Phone Number
     , substr(le.HomePhone, 1, 3) mailing_phone_area_code
     , substr(le.HomePhone, 4, 3) mailing_phone_exchange
     , substr(le.HomePhone, 7, 4) mailing_phone_number
     , substr(le.BusinessPhone, 1, 3) business_phone_area_code
     , substr(le.BusinessPhone, 4, 3) business_phone_exchange
     , substr(le.BusinessPhone, 7, 4) business_phone_number
     , substr(le.MobilePhone, 1, 3) home_phone_area_code
     , substr(le.MobilePhone, 4, 3) home_phone_exchange
     , substr(le.MobilePhone, 7, 4) home_phone_number

  FROM bcpcorral.License LIC
  JOIN abcrw.legalentity le
    ON le.LEGALENTITYID = LIC.LicenseeObjectId
  left outer join abcrw.address le_maddr on le.MailingAddressObjectId = le_maddr.ADDRESSID
  -- Establishment
  left outer join abcrw.establishment est on EST.EstablishmentId = LIC.EstablishmentObjectId
  left outer join abcrw.address est_paddr on est.PhysicalAddressId = est_paddr.ADDRESSID
  WHERE LIC.State = 'Active'
order by 1
;
