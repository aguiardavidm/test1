drop materialized view investigations.abc_all_license_view;
create materialized view investigations.abc_all_license_view
refresh on demand
start with trunc(sysdate) + 14/12 next sysdate+1
as
select cast(Lic.LicenseNumber as varchar2(15)) lic_num
     , cast(null as varchar2(4)) VEH_VES_LIC_TYPE
     , cast(decode(Lic.state, 'Closed', 'H',
                              'Expired', 'H',
                              'Active', 'C',
                              'Pending', 'P',
                              'Cancelled', 'W',
                              'Suspended', 'W',
                              'Revoked', 'W',
                              'Not Approved', 'D',
                              'Current', 'C',
                              'U') as varchar2(1)) Lic_Stat
     , cast(null as varchar2(1)) ACT_ID_CD
     , cast(null as varchar2(12)) REASON_TRANS_CD
     , cast(le.LegalEntityType as varchar2(4)) APPL_FILED_CD
     , cast(null as varchar2(1)) SPEC_COND_IND
     -- warehouse address details
     , wha.streetnumber WAR_STR_NUM
     , cast(wha.street as varchar2(25)) WAR_STR_NAME
     , cast(wha.Suite as varchar2(6)) WAR_PO_BOX
     , cast(wha.citytown as varchar2(25)) WAR_MUNI
     , wha.provincecode WAR_ST
     , cast(wha.postalcode as varchar2(5)) WAR_ZIP_1_5
     , cast(wha.ZipCodeExtension as varchar2(4)) WAR_ZIP_6_9
     -- warehouse phone
     , substr(LIC.WarehousePhoneNumber, 1, 3) WAR_AREA_CD
     , substr(LIC.WarehousePhoneNumber, 4, 3) WAR_PREFIX
     , substr(LIC.WarehousePhoneNumber, 7, 4) WAR_SUFFIX
     -- License Details Continued
     , cast(null as varchar2(1)) LIC_ACTIVE_IND
     , cast(null as date)BUS_INACT_DT
     , cast(null as number(2)) BLDG_TOT
     , cast(null as varchar2(1)) ENTRANCE_IND
     , cast(null as varchar2(1)) OTH_BUS_PREM_IND
     , cast(null as varchar2(1)) LAW_ENF_IND
     , cast(LIC.conflictofinterest as varchar2(1)) ELEC_OFFICE_IND
     , cast(null as varchar2(1)) OTH_RET_INT_IND
     , cast(null as varchar2(1)) OTH_WHOL_INT_IND
     , cast(null as varchar2(1)) APPL_NJ_DEN_IND
     , cast(null as varchar2(1)) NON_APPL_NJ_DEN_IND
     , cast(null as varchar2(1)) NEG_ACTION_IND
     , cast(LIC.criminalConviction as varchar2(1)) CRIME_CONV_IND
     , cast(null as varchar2(1)) OTH_NJ_LIC_INT_IND
     , cast(null as varchar2(1)) FAIL_QUAL_IND
     , cast(null as varchar2(1)) FEE_OWED_IND
     , cast(null as varchar2(1)) POP_RESTR_IND
     , cast(null as varchar2(4)) EXCPT_RSN_CD
     , cast(null as varchar2(1)) LIC_LMT_EXCPT_IND
     , cast(null as varchar2(1)) EXCPT_RSN_HTL
     , cast(null as varchar2(1)) EXCPT_RSN_RSTRNT
     , cast(null as varchar2(1)) EXCPT_RSN_BWLNG
     , cast(null as varchar2(1)) EXCPT_RSN_AIRPRT
     , cast(null as varchar2(1)) EXCPT_RSN_OTH
     , cast(null as varchar2(1)) CLUB_NJ_ACTIVE_IND
     , cast(null as varchar2(1)) CLUB_CHART_IND
     , cast(null as varchar2(1)) CLUB_QRTR_POS_IND
     , cast(null as varchar2(1)) CLUB_VOTE_MEM_IND
     , cast(null as varchar2(1)) N_APP_INT_LIC_IND
     , cast(null as varchar2(1)) SEC_INT_IND
     , cast(null as varchar2(1)) BENEF_INT_IND
     , cast(null as varchar2(1)) VEH_VES_LSD_IND
     , cast(null as varchar2(1)) VEH_VES_FNCD_IND
     , cast(null as varchar2(4)) VEH_YR
     , cast(null as varchar2(10)) VEH_MAKE
     , cast(null as varchar2(20)) VEH_MODEL
     , cast(null as varchar2(25)) VEH_ID_NUM
     , cast(null as varchar2(2)) VEH_REG_ST
     , cast(null as varchar2(25)) VES_USCG_NUM
     , cast(null as date) DT_RECOM
     , cast(null as varchar2(3)) RECOM_INITS
     , n.ApplicationReceivedDate DT_APPL_FILED
     , cast(null as varchar2(40)) APPL_PURPOSE
     , cast(null as number(8,2)) MUNI_FEE
     , cast(null as varchar2(1)) INACT_ACT_XFER_IND
     , cast(null as varchar2(1)) NEW_LIC_ACT_IND
     , cast(null as date) DT_ANTIC_ACTIVE
     , cast(null as varchar2(1)) SIGNED_WAIVER_IND
     , cast(null as varchar2(1)) VEH_TRAN_IND
     , cast(null as varchar2(1)) TRAN_INSIG_IND
     , cast(null as varchar2(1)) BATF_REG_IND
     , cast(null as date) DT_BATF_FILED
     , cast(lic.FederalTaxRegistrationNumber as varchar2(9)) FED_TAX_REG_NUM
     , cast(null as varchar2(12)) PREV_LIC_NUM_XFER
     , cast(null as varchar2(1)) PLC_PLC_PKT_XFER
     , cast(null as varchar2(1)) FED_BAS_PERM_IND
     , cast(null as date) DT_PERM_FILED
     , cast(null as varchar2(10)) FED_BAS_PERM_NUM
     , cast(null as varchar2(4)) PLACE_CNTY_MUNI_CD
     , cast(null as varchar2(12)) ACT_RESTR
     -- Legal Entity (Licensee) Details
     , cast(LE.LEGALENTITYID as number(9)) NAME_MSTR_ID_NUM
     , (case 
          when length(le.dup_FormattedName) > 60 
            then cast(le.dup_FormattedName as varchar2(57)) || '...'
          else cast(le.dup_FormattedName as varchar2(60))
        end) Name
     , cast(null as varchar2(12)) SRCE_GRP_CD
     , cast(le.LegalEntityType as varchar2(12)) NAME_TYPE_CD
     , cast(decode(le.Active, 'Y', 'C', 'H') as varchar2(1)) NAME_STAT
     , cast('Licensee' as varchar2(25)) relationship --"Licensee"?
     -- Establishment Details
     -- establishment address details
     , cast(paddr.STREETNUMBER as varchar2(7)) STR_NUM
     , cast(paddr.street as varchar2(25)) STR_NAME
     , cast(paddr.PO_PostOfficeBox as varchar2(6)) PO_BOX
     , cast(paddr.citytown as varchar2(25)) MUNI_name
     , cast(null as varchar2(25)) cnty_name
     , cast(paddr.provincecode as varchar2(2)) ST_prov_cd
     , cast(paddr.postalcode as varchar2(5)) ZIP_1_5
     , cast(paddr.zipcodeextension as varchar2(4)) ZIP_6_9
     , cast(paddr.zipcodeextension as varchar2(10)) FOR_Post_CD
     , cast(decode(maddr.Country, 'USA', 'US', '') as varchar2(2)) cntry_cd
     -- Licensee Address Details
       -- Mailing
       , maddr.StreetNumber mail_str_num
       , cast(maddr.street as varchar2(25)) mail_str_name
       , cast(maddr.PO_PostOfficeBox as varchar2(6)) mail_po_box
       , cast(maddr.CityTown as varchar2(25)) mail_muni_name
       , cast(null as varchar2(25)) MAIL_CNTY_NAME
       , maddr.ProvinceCode mail_st_prov_cd
       , cast(maddr.PostalCode as varchar2(5)) mail_zip_1_5
       , cast(maddr.ZipCodeExtension as varchar2(4)) mail_zip_6_9
       , cast(maddr.ZipCodeExtension as varchar2(10)) MAIL_FOR_POST_CD
       , cast(decode(maddr.Country, 'USA', 'US', '') as varchar2(2)) MAIL_CNTRY_CD
       , substr(le.HomePhone, 1, 3) mail_area_cd
       , substr(le.HomePhone, 4, 3) mail_prefix
       , substr(le.HomePhone, 7, 4) mail_suffix
       , cast(null as varchar2(7)) NJ_STR_NUM
       , cast(null as varchar2(25)) NJ_STR_NAME
       , cast(null as varchar2(6)) NJ_PO_BOX
       , cast(null as varchar2(25)) NJ_MUNI_NAME
       , cast(null as varchar2(25)) NJ_CNTY_NAME
       , cast(null as varchar2(5)) NJ_ZIP_1_5
       , cast(null as varchar2(4)) NJ_ZIP_6_9
       , cast(null as varchar2(2)) NJ_ST
     -- Legal Entity Details
     , cast(le.SSNTINEncrypted as varchar2(12)) SSN_TIN
     , cast(LE.NJTaxAuthNumber as varchar2(12)) NJ_TAX_AUTH_NUM
     , cast(LE.CorporationNumber as varchar2(10)) CERT_INC_NUM
     , CAST(NULL AS DATE) DT_BIRTH
     , cast(null as varchar2(1)) HT_FT
     , cast(null as varchar2(2)) HT_INCH
     , cast(null as varchar2(3)) WT
     , cast(null as varchar2(5)) EYE_COLOR  
     , cast(null as varchar2(8)) HAIR_COLOR
     , cast(null as varchar2(1)) SEX
     , substr(le.BusinessPhone, 1, 3) bus_area_cd
     , substr(le.BusinessPhone, 4, 3) bus_prefix
     , substr(le.BusinessPhone, 7, 4) bus_suffix
     , substr(le.MobilePhone, 1, 3) home_area_cd
     , substr(le.MobilePhone, 4, 3) home_prefix
     , substr(le.MobilePhone, 7, 4) home_suffix
     , cast(null as number(5,2)) PCT_BUS_CNTRL
     , cast(null as number(7)) NUM_SHARES_OWN
     , cast(null as varchar2(12)) POS_TITLE
     , cast(null as varchar2(1)) VALID_CORP_IND
     , cast(LE.IncorporationDate as date) DT_CORP_CHART
     , cast(null as varchar2(2)) CORP_CHART_ST
     , cast(null as varchar2(1)) CORP_NJ_AUTH
     , cast(null as varchar2(1)) CORP_NJ_REVOC_IND
     , cast(null as varchar2(25)) EMP_AGENCY_MUNI
     , cast(null as varchar2(3)) DIST_ID_NUM
     -- Owner/Stakeholder
     , cast(null as number(9)) PARENT_ID_NUMBER
     , cast(null as varchar2(4)) NAME_CNTY_MUNI_CD
  from bcpcorral.license lic
  join abcrw.legalentity LE on lic.LicenseeId = LE.LEGALENTITYID
  -- physical address (where available)
  left outer join abcrw.address paddr on le.PhysicalAddressId = paddr.ADDRESSID
  -- mailing address (where available)
  left outer join abcrw.address maddr on le.MailingAddressId = maddr.ADDRESSID
  -- Warehouse
  LEFT OUTER JOIN (select rwha.LicenseId, wa.StreetNumber, wa.CityTown, wa.PostalCode, wa.ProvinceState, wa.provincecode, wa.ZipCodeExtension, null Suite, Street
                     from abcrw.address wa
                     join abcrw.LicenseWarehouseAddress rwha
                       on wa.ADDRESSID = rwha.WarehouseAddressId
                  ) WHA on wha.LicenseId = lic.LICENSEID
  left outer join abcrw.newapplicationjob n on n.LicenseId = LIC.LICENSEID
  where lic.LICENSEID = coalesce((select max(mrl.licenseid) licenseid
                                   from abcrw.license mrl
                                  where mrl.licensenumber = lic.LicenseNumber
                                    and mrl.state = 'Active'
                                  group by mrl.licensenumber),
                                  (select max(mrl.licenseid) licenseid
                                    from abcrw.license mrl
                                   where mrl.licensenumber = lic.LicenseNumber
                                     and mrl.state IN ('Expired', 'Closed', 'Cancelled')
                                   group by mrl.licensenumber))
union all
select cast(Lic.LicenseNumber as varchar2(15)) lic_num--, paddr.ADDRESSID, maddr.ADDRESSID
     , cast(null as varchar2(4)) VEH_VES_LIC_TYPE
     , cast(decode(Lic.state, 'Closed', 'H',
                              'Expired', 'H',
                              'Active', 'C',
                              'Pending', 'P',
                              'Cancelled', 'W',
                              'Suspended', 'W',
                              'Revoked', 'W',
                              'Not Approved', 'D',
                              'Current', 'C',
                              'U') as varchar2(1)) Lic_Stat
     , cast(null as varchar2(1)) ACT_ID_CD
     , cast(null as varchar2(12)) REASON_TRANS_CD
     , cast(corp.LegalEntityType as varchar2(4)) APPL_FILED_CD
     , cast(null as varchar2(1)) SPEC_COND_IND
     -- warehouse address details
     , wha.streetnumber WAR_STR_NUM
     , cast(wha.street as varchar2(25)) WAR_STR_NAME
     , cast(wha.Suite as varchar2(6)) WAR_PO_BOX
     , cast(wha.citytown as varchar2(25)) WAR_MUNI
     , wha.provincecode WAR_ST
     , cast(wha.postalcode as varchar2(5)) WAR_ZIP_1_5
     , cast(wha.ZipCodeExtension as varchar2(4)) WAR_ZIP_6_9
     -- warehouse phone
     , substr(LIC.WarehousePhoneNumber, 1, 3) WAR_AREA_CD
     , substr(LIC.WarehousePhoneNumber, 4, 3) WAR_PREFIX
     , substr(LIC.WarehousePhoneNumber, 7, 4) WAR_SUFFIX
     -- License Details Continued
     , cast(null as varchar2(1)) LIC_ACTIVE_IND
     , cast(null as date)BUS_INACT_DT
     , cast(null as number(2)) BLDG_TOT
     , cast(null as varchar2(1)) ENTRANCE_IND
     , cast(null as varchar2(1)) OTH_BUS_PREM_IND
     , cast(null as varchar2(1)) LAW_ENF_IND
     , cast(LIC.conflictofinterest as varchar2(1)) ELEC_OFFICE_IND
     , cast(null as varchar2(1)) OTH_RET_INT_IND
     , cast(null as varchar2(1)) OTH_WHOL_INT_IND
     , cast(null as varchar2(1)) APPL_NJ_DEN_IND
     , cast(null as varchar2(1)) NON_APPL_NJ_DEN_IND
     , cast(null as varchar2(1)) NEG_ACTION_IND
     , cast(LIC.criminalConviction as varchar2(1)) CRIME_CONV_IND
     , cast(null as varchar2(1)) OTH_NJ_LIC_INT_IND
     , cast(null as varchar2(1)) FAIL_QUAL_IND
     , cast(null as varchar2(1)) FEE_OWED_IND
     , cast(null as varchar2(1)) POP_RESTR_IND
     , cast(null as varchar2(4)) EXCPT_RSN_CD
     , cast(null as varchar2(1)) LIC_LMT_EXCPT_IND
     , cast(null as varchar2(1)) EXCPT_RSN_HTL
     , cast(null as varchar2(1)) EXCPT_RSN_RSTRNT
     , cast(null as varchar2(1)) EXCPT_RSN_BWLNG
     , cast(null as varchar2(1)) EXCPT_RSN_AIRPRT
     , cast(null as varchar2(1)) EXCPT_RSN_OTH
     , cast(null as varchar2(1)) CLUB_NJ_ACTIVE_IND
     , cast(null as varchar2(1)) CLUB_CHART_IND
     , cast(null as varchar2(1)) CLUB_QRTR_POS_IND
     , cast(null as varchar2(1)) CLUB_VOTE_MEM_IND
     , cast(null as varchar2(1)) N_APP_INT_LIC_IND
     , cast(null as varchar2(1)) SEC_INT_IND
     , cast(null as varchar2(1)) BENEF_INT_IND
     , cast(null as varchar2(1)) VEH_VES_LSD_IND
     , cast(null as varchar2(1)) VEH_VES_FNCD_IND
     , cast(null as varchar2(4)) VEH_YR
     , cast(null as varchar2(10)) VEH_MAKE
     , cast(null as varchar2(20)) VEH_MODEL
     , cast(null as varchar2(25)) VEH_ID_NUM
     , cast(null as varchar2(2)) VEH_REG_ST
     , cast(null as varchar2(25)) VES_USCG_NUM
     , cast(null as date) DT_RECOM
     , cast(null as varchar2(3)) RECOM_INITS
     , n.ApplicationReceivedDate DT_APPL_FILED
     , cast(null as varchar2(40)) APPL_PURPOSE
     , cast(null as number(8,2)) MUNI_FEE
     , cast(null as varchar2(1)) INACT_ACT_XFER_IND
     , cast(null as varchar2(1)) NEW_LIC_ACT_IND
     , cast(null as date) DT_ANTIC_ACTIVE
     , cast(null as varchar2(1)) SIGNED_WAIVER_IND
     , cast(null as varchar2(1)) VEH_TRAN_IND
     , cast(null as varchar2(1)) TRAN_INSIG_IND
     , cast(null as varchar2(1)) BATF_REG_IND
     , cast(null as date) DT_BATF_FILED
     , cast(lic.FederalTaxRegistrationNumber as varchar2(9)) FED_TAX_REG_NUM
     , cast(null as varchar2(12)) PREV_LIC_NUM_XFER
     , cast(null as varchar2(1)) PLC_PLC_PKT_XFER
     , cast(null as varchar2(1)) FED_BAS_PERM_IND
     , cast(null as date) DT_PERM_FILED
     , cast(null as varchar2(10)) FED_BAS_PERM_NUM
     , cast(null as varchar2(4)) PLACE_CNTY_MUNI_CD
     , cast(null as varchar2(12)) ACT_RESTR
     -- Legal Entity (Licensee) Details
     , cast(corp.LEGALENTITYID as number(9)) NAME_MSTR_ID_NUM
     , (case 
          when length(corp.dup_FormattedName) > 60 
            then cast(corp.dup_FormattedName as varchar2(57)) || '...'
          else cast(corp.dup_FormattedName as varchar2(60))
        end) Name
     , cast(null as varchar2(12)) SRCE_GRP_CD
     , cast(corp.LegalEntityType as varchar2(12)) NAME_TYPE_CD
     , cast(decode(corp.Active, 'Y', 'C', 'H') as varchar(1)) NAME_STAT
     , cast(corp.relationshiptype as varchar2(25)) relationship --"Licensee"?
     -- Establishment Details
     -- establishment address details
     , cast(paddr.STREETNUMBER as varchar2(7)) STR_NUM
     , cast(paddr.street as varchar2(25)) STR_NAME
     , cast(paddr.PO_PostOfficeBox as varchar2(6)) PO_BOX
     , cast(paddr.citytown as varchar2(25)) MUNI_name
     , cast(null as varchar2(25)) cnty_name
     , cast(paddr.provincecode as varchar2(2)) ST_prov_cd
     , cast(paddr.postalcode as varchar2(5)) ZIP_1_5
     , cast(paddr.zipcodeextension as varchar2(4)) ZIP_6_9
     , cast(paddr.zipcodeextension as varchar2(10)) FOR_Post_CD
     , cast(decode(paddr.Country, 'USA', 'US', '') as varchar2(2)) cntry_cd
     -- Licensee Address Details
       -- Mailing
       , maddr.StreetNumber mail_str_num
       , cast(maddr.street as varchar2(25)) mail_str_name
       , cast(maddr.PO_PostOfficeBox as varchar2(6)) mail_po_box
       , cast(maddr.CityTown as varchar2(25)) mail_muni_name
       , cast(null as varchar2(25)) MAIL_CNTY_NAME
       , maddr.ProvinceCode mail_st_prov_cd
       , cast(maddr.PostalCode as varchar2(5)) mail_zip_1_5
       , cast(maddr.ZipCodeExtension as varchar2(4)) mail_zip_6_9
       , cast(maddr.ZipCodeExtension as varchar2(10)) MAIL_FOR_POST_CD
       , cast(decode(maddr.Country, 'USA', 'US', '') as varchar2(2)) MAIL_CNTRY_CD
       , substr(corp.HomePhone, 1, 3) mail_area_cd
       , substr(corp.HomePhone, 4, 3) mail_prefix
       , substr(corp.HomePhone, 7, 4) mail_suffix
       , cast(null as varchar2(7)) NJ_STR_NUM
       , cast(null as varchar2(25)) NJ_STR_NAME
       , cast(null as varchar2(6)) NJ_PO_BOX
       , cast(null as varchar2(25)) NJ_MUNI_NAME
       , cast(null as varchar2(25)) NJ_CNTY_NAME
       , cast(null as varchar2(5)) NJ_ZIP_1_5
       , cast(null as varchar2(4)) NJ_ZIP_6_9
       , cast(null as varchar2(2)) NJ_ST
     -- Legal Entity Details
     , cast(corp.SSNTINEncrypted as varchar2(12)) SSN_TIN
     , cast(corp.NJTaxAuthNumber as varchar2(12)) NJ_TAX_AUTH_NUM
     , cast(corp.CorporationNumber as varchar2(10)) CERT_INC_NUM
     , cast(null as date) DT_BIRTH
     , cast(null as varchar2(1)) HT_FT
     , cast(null as varchar2(2)) HT_INCH
     , cast(null as varchar2(3)) WT
     , cast(null as varchar2(5)) EYE_COLOR  
     , cast(null as varchar2(8)) HAIR_COLOR
     , cast(null as varchar2(1)) SEX
     , substr(corp.BusinessPhone, 1, 3) bus_area_cd
     , substr(corp.BusinessPhone, 4, 3) bus_prefix
     , substr(corp.BusinessPhone, 7, 4) bus_suffix
     , substr(corp.MobilePhone, 1, 3) home_area_cd
     , substr(corp.MobilePhone, 4, 3) home_prefix
     , substr(corp.MobilePhone, 7, 4) home_suffix
     , cast(corp.PercentageInterest as number(5,2)) PCT_BUS_CNTRL
     , cast(corp.CommonVotingShares as number(7)) NUM_SHARES_OWN
     , cast(null as varchar2(12)) POS_TITLE
     , cast(null as varchar2(1)) VALID_CORP_IND
     , cast(corp.IncorporationDate as date) DT_CORP_CHART
     , cast(null as varchar2(2)) CORP_CHART_ST
     , cast(null as varchar2(1)) CORP_NJ_AUTH
     , cast(null as varchar2(1)) CORP_NJ_REVOC_IND
     , cast(null as varchar2(25)) EMP_AGENCY_MUNI
     , cast(null as varchar2(3)) DIST_ID_NUM
     -- Owner/Stakeholder
     , cast(corp.ParentLE as number(9)) PARENT_ID_NUMBER
     , cast(null as varchar2(4)) NAME_CNTY_MUNI_CD
  from bcpcorral.license lic
  join abcrw.legalentity LE on lic.LicenseeId = le.LEGALENTITYID
  -- Licensee is stakeholder/owner
  join investigations.licenseecorpstructure_mv corp on corp.RootLE = le.LEGALENTITYID
  -- physical address (where available)
  left outer join abcrw.address paddr on corp.PhysicalAddressId = paddr.ADDRESSID
  -- mailing address (where available)
  left outer join abcrw.address maddr on corp.MailingAddressId = maddr.ADDRESSID
  -- Warehouse
  LEFT OUTER JOIN (select rwha.LicenseId, wa.StreetNumber, wa.CityTown, wa.PostalCode, wa.ProvinceState, wa.provincecode, wa.ZipCodeExtension, null Suite, Street
                     from abcrw.address wa
                     join abcrw.LicenseWarehouseAddress rwha
                       on wa.ADDRESSID = rwha.WarehouseAddressId
                  ) WHA on wha.LicenseId = lic.LICENSEID
  left outer join abcrw.newapplicationjob n on n.LicenseId = LIC.LICENSEID
  where lic.LICENSEID = coalesce((select max(mrl.licenseid) licenseid
                                   from abcrw.license mrl
                                  where mrl.licensenumber = lic.LicenseNumber
                                    and mrl.state = 'Active'
                                  group by mrl.licensenumber),
                                  (select max(mrl.licenseid) licenseid
                                    from abcrw.license mrl
                                   where mrl.licensenumber = lic.LicenseNumber
                                     and mrl.state IN ('Expired', 'Closed', 'Cancelled')
                                   group by mrl.licensenumber))
union all
select cast(Lic.LicenseNumber as varchar2(15)) lic_num--, paddr.ADDRESSID, maddr.ADDRESSID, est.ESTABLISHMENTID
     , cast(null as varchar2(4)) VEH_VES_LIC_TYPE
     , cast(decode(Lic.state, 'Closed', 'H',
                              'Expired', 'H',
                              'Active', 'C',
                              'Pending', 'P',
                              'Cancelled', 'W',
                              'Suspended', 'W',
                              'Revoked', 'W',
                              'Not Approved', 'D',
                              'Current', 'C',
                              'U') as varchar2(1)) Lic_Stat
     , cast(null as varchar2(1)) ACT_ID_CD
     , cast(null as varchar2(12)) REASON_TRANS_CD
     , cast('Establishment' as varchar2(4)) APPL_FILED_CD
     , cast(null as varchar2(1)) SPEC_COND_IND
     -- warehouse address details
     , wha.streetnumber WAR_STR_NUM
     , cast(wha.street as varchar2(25)) WAR_STR_NAME
     , cast(wha.Suite as varchar2(6)) WAR_PO_BOX
     , cast(wha.citytown as varchar2(25)) WAR_MUNI
     , wha.provincecode WAR_ST
     , cast(wha.postalcode as varchar2(5)) WAR_ZIP_1_5
     , cast(wha.ZipCodeExtension as varchar2(4)) WAR_ZIP_6_9
     -- warehouse phone
     , substr(LIC.WarehousePhoneNumber, 1, 3) WAR_AREA_CD
     , substr(LIC.WarehousePhoneNumber, 4, 3) WAR_PREFIX
     , substr(LIC.WarehousePhoneNumber, 7, 4) WAR_SUFFIX
     -- License Details Continued
     , cast(null as varchar2(1)) LIC_ACTIVE_IND
     , cast(null as date)BUS_INACT_DT
     , cast(null as number(2)) BLDG_TOT
     , cast(null as varchar2(1)) ENTRANCE_IND
     , cast(null as varchar2(1)) OTH_BUS_PREM_IND
     , cast(null as varchar2(1)) LAW_ENF_IND
     , cast(LIC.conflictofinterest as varchar2(1)) ELEC_OFFICE_IND
     , cast(null as varchar2(1)) OTH_RET_INT_IND
     , cast(null as varchar2(1)) OTH_WHOL_INT_IND
     , cast(null as varchar2(1)) APPL_NJ_DEN_IND
     , cast(null as varchar2(1)) NON_APPL_NJ_DEN_IND
     , cast(null as varchar2(1)) NEG_ACTION_IND
     , cast(LIC.criminalConviction as varchar2(1)) CRIME_CONV_IND
     , cast(null as varchar2(1)) OTH_NJ_LIC_INT_IND
     , cast(null as varchar2(1)) FAIL_QUAL_IND
     , cast(null as varchar2(1)) FEE_OWED_IND
     , cast(null as varchar2(1)) POP_RESTR_IND
     , cast(null as varchar2(4)) EXCPT_RSN_CD
     , cast(null as varchar2(1)) LIC_LMT_EXCPT_IND
     , cast(null as varchar2(1)) EXCPT_RSN_HTL
     , cast(null as varchar2(1)) EXCPT_RSN_RSTRNT
     , cast(null as varchar2(1)) EXCPT_RSN_BWLNG
     , cast(null as varchar2(1)) EXCPT_RSN_AIRPRT
     , cast(null as varchar2(1)) EXCPT_RSN_OTH
     , cast(null as varchar2(1)) CLUB_NJ_ACTIVE_IND
     , cast(null as varchar2(1)) CLUB_CHART_IND
     , cast(null as varchar2(1)) CLUB_QRTR_POS_IND
     , cast(null as varchar2(1)) CLUB_VOTE_MEM_IND
     , cast(null as varchar2(1)) N_APP_INT_LIC_IND
     , cast(null as varchar2(1)) SEC_INT_IND
     , cast(null as varchar2(1)) BENEF_INT_IND
     , cast(null as varchar2(1)) VEH_VES_LSD_IND
     , cast(null as varchar2(1)) VEH_VES_FNCD_IND
     , cast(null as varchar2(4)) VEH_YR
     , cast(null as varchar2(10)) VEH_MAKE
     , cast(null as varchar2(20)) VEH_MODEL
     , cast(null as varchar2(25)) VEH_ID_NUM
     , cast(null as varchar2(2)) VEH_REG_ST
     , cast(null as varchar2(25)) VES_USCG_NUM
     , cast(null as date) DT_RECOM
     , cast(null as varchar2(3)) RECOM_INITS
     , n.ApplicationReceivedDate DT_APPL_FILED
     , cast(null as varchar2(40)) APPL_PURPOSE
     , cast(null as number(8,2)) MUNI_FEE
     , cast(null as varchar2(1)) INACT_ACT_XFER_IND
     , cast(null as varchar2(1)) NEW_LIC_ACT_IND
     , cast(null as date) DT_ANTIC_ACTIVE
     , cast(null as varchar2(1)) SIGNED_WAIVER_IND
     , cast(null as varchar2(1)) VEH_TRAN_IND
     , cast(null as varchar2(1)) TRAN_INSIG_IND
     , cast(null as varchar2(1)) BATF_REG_IND
     , cast(null as date) DT_BATF_FILED
     , cast(lic.FederalTaxRegistrationNumber as varchar2(9)) FED_TAX_REG_NUM
     , cast(null as varchar2(12)) PREV_LIC_NUM_XFER
     , cast(null as varchar2(1)) PLC_PLC_PKT_XFER
     , cast(null as varchar2(1)) FED_BAS_PERM_IND
     , cast(null as date) DT_PERM_FILED
     , cast(null as varchar2(10)) FED_BAS_PERM_NUM
     , cast(null as varchar2(4)) PLACE_CNTY_MUNI_CD
     , cast(null as varchar2(12)) ACT_RESTR
     -- Legal Entity (Licensee) Details
     , cast(est.ESTABLISHMENTID as number(9)) NAME_MSTR_ID_NUM
     , (case 
          when length(est.DoingBusinessAsTradeNames) > 60 
            then cast(est.DoingBusinessAsTradeNames as varchar2(57)) || '...'
          else cast(est.DoingBusinessAsTradeNames as varchar2(60))
        end) Name
     , cast(null as varchar2(12)) SRCE_GRP_CD
     , cast('Establishment' as varchar2(12)) NAME_TYPE_CD
     , cast(decode(est.Active, 'Y', 'C', 'H') as varchar(1)) NAME_STAT
     , cast('Establishment' as varchar2(25)) relationship --"Licensee"?
     -- Establishment Details
     -- establishment address details
     , cast(paddr.STREETNUMBER as varchar2(7)) STR_NUM
     , cast(paddr.street as varchar2(25)) STR_NAME
     , cast(paddr.PO_PostOfficeBox as varchar2(6)) PO_BOX
     , cast(paddr.citytown as varchar2(25)) MUNI_name
     , cast(null as varchar2(25)) cnty_name
     , cast(paddr.provincecode as varchar2(2)) ST_prov_cd
     , cast(paddr.postalcode as varchar2(5)) ZIP_1_5
     , cast(paddr.zipcodeextension as varchar2(4)) ZIP_6_9
     , cast(paddr.zipcodeextension as varchar2(10)) FOR_Post_CD
     , cast(decode(paddr.Country, 'USA', 'US', '') as varchar2(2)) cntry_cd
     -- Licensee Address Details
       -- Mailing
       , maddr.StreetNumber mail_str_num
       , cast(maddr.street as varchar2(25)) mail_str_name
       , cast(maddr.PO_PostOfficeBox as varchar2(6)) mail_po_box
       , cast(maddr.CityTown as varchar2(25)) mail_muni_name
       , cast(null as varchar2(25)) MAIL_CNTY_NAME
       , maddr.ProvinceCode mail_st_prov_cd
       , cast(maddr.PostalCode as varchar2(5)) mail_zip_1_5
       , cast(maddr.ZipCodeExtension as varchar2(4)) mail_zip_6_9
       , cast(maddr.ZipCodeExtension as varchar2(10)) MAIL_FOR_POST_CD
       , cast(decode(maddr.Country, 'USA', 'US', '') as varchar2(2)) MAIL_CNTRY_CD
       , substr(est.HomePhone, 1, 3) mail_area_cd
       , substr(est.HomePhone, 4, 3) mail_prefix
       , substr(est.HomePhone, 7, 4) mail_suffix
       , cast(null as varchar2(7)) NJ_STR_NUM
       , cast(null as varchar2(25)) NJ_STR_NAME
       , cast(null as varchar2(6)) NJ_PO_BOX
       , cast(null as varchar2(25)) NJ_MUNI_NAME
       , cast(null as varchar2(25)) NJ_CNTY_NAME
       , cast(null as varchar2(5)) NJ_ZIP_1_5
       , cast(null as varchar2(4)) NJ_ZIP_6_9
       , cast(null as varchar2(2)) NJ_ST
     -- Legal Entity Details
     , cast(null as varchar2(12)) SSN_TIN
     , cast(null as varchar2(12)) NJ_TAX_AUTH_NUM
     , cast(null as varchar2(10)) CERT_INC_NUM
     , cast(null as date) DT_BIRTH
     , cast(null as varchar2(1)) HT_FT
     , cast(null as varchar2(2)) HT_INCH
     , cast(null as varchar2(3)) WT
     , cast(null as varchar2(5)) EYE_COLOR  
     , cast(null as varchar2(8)) HAIR_COLOR
     , cast(null as varchar2(1)) SEX
     , substr(est.BusinessPhone, 1, 3) bus_area_cd
     , substr(est.BusinessPhone, 4, 3) bus_prefix
     , substr(est.BusinessPhone, 7, 4) bus_suffix
     , substr(est.MobilePhone, 1, 3) home_area_cd
     , substr(est.MobilePhone, 4, 3) home_prefix
     , substr(est.MobilePhone, 7, 4) home_suffix
     , cast(null as number(5,2)) PCT_BUS_CNTRL
     , cast(null as number(7)) NUM_SHARES_OWN
     , cast(null as varchar2(12)) POS_TITLE
     , cast(null as varchar2(1)) VALID_CORP_IND
     , cast(null as date) DT_CORP_CHART
     , cast(null as varchar2(2)) CORP_CHART_ST
     , cast(null as varchar2(1)) CORP_NJ_AUTH
     , cast(null as varchar2(1)) CORP_NJ_REVOC_IND
     , cast(null as varchar2(25)) EMP_AGENCY_MUNI
     , cast(null as varchar2(3)) DIST_ID_NUM
     -- Owner/Stakeholder
     , cast(null as number(9)) PARENT_ID_NUMBER
     , cast(null as varchar2(4)) NAME_CNTY_MUNI_CD
  from bcpcorral.license lic
  join abcrw.legalentity LE on lic.LicenseeId = le.LEGALENTITYID
  -- Licensee is stakeholder/owner
  join (select est1.BusinessPhone,
               est1.MobilePhone,
               est1.HomePhone,
               est1.Active,
               est1.DoingBusinessAsTradeNames,
               est1.ESTABLISHMENTID ESTABLISHMENTID,
               null LICENSEOBJECTID,
               est1.PhysicalAddressId,
               est1.MailingAddressId
          from abcrw.establishment est1
        union
        select est2.BusinessPhone,
               est2.MobilePhone,
               est2.HomePhone,
               'N',
               est2.DoingBusinessAs,
               esth.ESTABLISHHISTOBJECTID ESTABLISHMENTID,
               esth.LICENSEOBJECTID, 
               est2.PhysicalAddressObjectId,
               est2.MailingAddressObjectId
          from abcrw.licenseestablishmenthist esth
          join abcrw.establishmenthistory est2 on est2.ESTABLISHMENTID = esth.ESTABLISHHISTOBJECTID) est on (est.ESTABLISHMENTID = lic.EstablishmentId or est.licenseobjectid = lic.LICENSEID)
  -- physical address (where available)
  left outer join abcrw.address paddr on est.PhysicalAddressId = paddr.ADDRESSID
  -- mailing address (where available)
  left outer join abcrw.address maddr on est.MailingAddressId = maddr.ADDRESSID
  -- Warehouse
  LEFT OUTER JOIN (select rwha.LicenseId, wa.StreetNumber, wa.CityTown, wa.PostalCode, wa.ProvinceState, wa.provincecode, wa.ZipCodeExtension, null Suite, Street
                     from abcrw.address wa
                     join abcrw.LicenseWarehouseAddress rwha
                       on wa.ADDRESSID = rwha.WarehouseAddressId
                  ) WHA on wha.LicenseId = lic.LICENSEID
  left outer join abcrw.newapplicationjob n on n.LicenseId = LIC.LICENSEID
  where lic.LICENSEID = coalesce((select max(mrl.licenseid) licenseid
                                   from abcrw.license mrl
                                  where mrl.licensenumber = lic.LicenseNumber
                                    and mrl.state = 'Active'
                                  group by mrl.licensenumber),
                                  (select max(mrl.licenseid) licenseid
                                    from abcrw.license mrl
                                   where mrl.licensenumber = lic.LicenseNumber
                                     and mrl.state IN ('Expired', 'Closed', 'Cancelled')
                                   group by mrl.licensenumber))
