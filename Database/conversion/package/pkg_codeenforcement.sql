create or replace package            pkg_CodeEnforcement is

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * CompleteComplaints() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CompleteComplaints(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * ResolveViolation() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  ResolveViolation(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );
  
  /*---------------------------------------------------------------------------
   * GenerateFines() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  GenerateFines(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );

end pkg_CodeEnforcement;

 

/

create or replace package body            pkg_CodeEnforcement is

  /*---------------------------------------------------------------------------
   * CompleteComplaints() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  CompleteComplaints(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  )is

  t_CompleteInvDefId    udt_Id := api.pkg_configquery.objectdefidforname('p_CompleteInvestigation');
  t_JobId               udt_Id;

  begin

    select JobId
      into t_JobId
      from api.processes p
     where p.processid = a_ObjectId;

    for c in (select p.processid
                from api.jobs j
                join api.processes p on p.jobid = j.jobid
                 and p.processtypeid = t_CompleteInvDefId
               where j.parentjobid = t_JobId)loop
      api.pkg_processupdate.Complete(c.ProcessId, 'Complete');
    end loop;
  end CompleteComplaints;

  /*---------------------------------------------------------------------------
   * ResolveViolation() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  ResolveViolation(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  )is
  
  begin
/*What if they are changing the Resolved Date??? Do we want to reset these things?  AWP 2/25/2011*/
    if api.pkg_columnquery.datevalue(a_ObjectId, 'ResolvedDate') is not null
      then
        api.pkg_columnupdate.setvalue(a_ObjectId, 'DailyFine', 0);
    end if;
  end ResolveViolation;

  /*---------------------------------------------------------------------------
   * GenerateFines() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  GenerateFines(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  )is
    t_ViolationEndPointId udt_Id := api.pkg_configquery.endpointidforname('j_Violation', 'Violation');
  begin
    --loop through the Violation objects
    for c in (select r.toobjectid, 
                     api.pkg_columnquery.datevalue(r.toobjectid, 'ResolutionDate') ResolutionDate,
                     api.pkg_columnquery.value(r.toobjectid, 'FeesAssessed') FeesAssessed
                from api.relationships r
               where r.fromobjectid = a_ObjectId
                 and r.endpointid = t_ViolationEndpointId)loop
--      --if the violation has been resolved
      if c.FeesAssessed = 'N'
        then
--dbms_output.put_line('Violation ObjectId: ' || c.ToObjectId);
          feescheduleplus.pkg_posseregisteredprocedures.GenerateFees(a_ObjectId, Sysdate, 'N', c.ToObjectId);
          api.pkg_columnupdate.SetValue(c.ToObjectId, 'FeesAssessed', 'Y');
      end if;
    end loop;
  end GenerateFines;

  /*---------------------------------------------------------------------------
   * IssueNOV() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure  IssueNOV(
    a_ObjectId                udt_Id,
    a_AsOfDate                date
  )is
    t_InvestigationJobId      udt_Id;
    t_ViolationJobId          udt_Id;
    t_ViolationJobTypeId      udt_Id         := api.pkg_ConfigQuery.ObjectDefIdForName('j_Violation');
    t_NewProcessId            udt_Id;
    t_InvestgationJobId       udt_Id;
    t_Outcome                 varchar2(4000) := api.pkg_columnquery.value(a_ObjectId, 'Outcome');
    t_IssueNOVProcessTypeId   udt_Id         := api.pkg_ConfigQuery.ObjectDefIdForName('p_IssueNOV');
    t_PerfViolInspTypeId      udt_Id         := api.pkg_ConfigQuery.ObjectDefIdForName('p_PerformViolationInspection');
  begin

    select jobid
      into t_InvestigationJobId
      from api.processes p
     where p.processid = a_ObjectId;

    select j.jobid
      into t_ViolationJobId
      from api.jobs j
     where j.parentjobid = t_InvestigationJobId
       and j.jobtypeid = t_ViolationJobTypeId;

    if t_Outcome = 'Issue Violation'
      then
        t_NewProcessId := api.pkg_processupdate.New(t_ViolationJobId,
                                                    t_IssueNOVProcessTypeId,
                                                    null,
                                                    null, --may want a scheduled start date...
                                                    null,
                                                    null);
       --copy Violation relationships to process
       --copy Address relationship to address
       --anything else to copy?
      else
        t_NewProcessId := api.pkg_processupdate.New(t_ViolationJobId,
                                                    t_PerfViolInspTypeId,
                                                    null,
                                                    null, --insert scheduled start date
                                                    null,
                                                    null);
        --anything to copy?
    end if;

  end IssueNOV;
end pkg_CodeEnforcement;

/

