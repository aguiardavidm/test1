create or replace package pkg_ER_MonitorReferral is

  -- Author  : MICHAEL.FROESE
  -- Created : 7/10/2009 9:26:17 AM
  -- Purpose : To monitor the referral requests and trigger workflow as necessary
  --           should the scheduled processes not get there first

  subtype udt_Id     is api.pkg_Definition.udt_Id;
  subtype udt_IDList is api.pkg_definition.udt_IdList;

  -- Run as an activity to run on ReferralRequest process completion
  -- check for mandatory requests.
  -- if no mandatory requests (path two):
  --  if no open optional requests then close referral
  --  else if open optional responses then check for minimum open time
  --   if past minimum open time close referral
  -- elseif mandatory requests (path one)
  --  if open mandatories then do nothing (scheduled process Manage Overdue Mandatory Requests handles this)
  --  if no open mandatories then
  --   if no Manage Overdue Mandatory Requests process goto path two
  --   else complete Manage Overdue Mandatory Requests
  --
  procedure MonitorReferral (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure RespondToDayChanges (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

end pkg_ER_MonitorReferral;

 
/

create or replace package body pkg_ER_MonitorReferral is

  g_ReferralRequestDefId udt_Id;
  g_CloseReferralResponseDefId udt_Id;
  g_ManageOverdueResponseDefId udt_id;
  g_SendReminderDefId udt_Id;

  procedure dbg( txt varchar2 ) is
  begin
    pkg_debug.putline( 'pkg_ER_MonitorReferral: ' || txt );
  end;

  --returns TRUE if process is complete
  function Complete (
    a_ProcessId udt_Id
  ) return boolean is
    t_Id udt_Id;
  begin
    select processid
      into t_Id
      from api.processes
      where processid = a_ProcessId
        and datecompleted is not null;
    return true;
    exception
      when no_data_found
      then return false;
  end;

  --returns processid given jobid and process type. returns null if not found.
  function GetProcessId (
    a_JobId udt_Id,
    a_ProcessType udt_Id
  ) return udt_Id is
    t_Id udt_Id := null;
  begin
    select processid
      into t_Id
      from api.processes
      where jobid = a_JobId
        and processtypeid = a_ProcessType;
    return t_Id;
    exception
      when no_data_found
      then return t_Id;
  end;

  procedure CloseReferral (
    a_JobId udt_Id
  ) is
    t_Id udt_Id;
  begin
    dbg('  CloseReferral');
    toolbox.pkg_workflow.DeleteScheduledProcesses( a_JobId, sysdate, 'p_ER_CloseReferralResponse');
    select processid
      into t_Id
      from api.processes
      where jobid = a_JobId
        and processtypeid = g_CloseReferralResponseDefId;

    if Complete( t_Id )
    then
      dbg('  Already closed');
    else
      dbg('  Bump Close Referral so that it closes itself');
      api.pkg_ColumnUpdate.SetValue( t_Id, 'CloseMe', 'Y' );
    end if;

    exception
      when no_data_found
      then
        t_Id := api.pkg_processupdate.New( a_JobId, g_CloseReferralResponseDefId, '', null, null, null);
        dbg('  Bump Close Referral so that it closes itself');
        api.pkg_ColumnUpdate.SetValue( t_Id, 'CloseMe', 'Y' );
  end;

  /* Path Two is used when there are no mandatories, or all mandatories
     are closed prior to any of them being overdue, ie, #70 doesn't exist
   */
  procedure Path_Two (
    a_ObjectId udt_Id,
    a_JobId    udt_Id
  ) is
    t_CountOpen pls_integer;
    t_Id udt_Id;
  begin
    dbg('  Path Two');
    select count(*)
      into t_CountOpen
      from api.processes p
      where jobid = a_JobId
        and p.ProcessTypeId = g_ReferralRequestDefId
        and api.pkg_ColumnQuery.Value( processid, 'ReferralLevel') = 'Optional'
        and p.Outcome is null;
    dbg( 'Open Optional Count: ' ||t_CountOpen );
    if t_CountOpen = 0
    then
      dbg('  Closing Referral');
      CloseReferral( a_JobId );
    else
      dbg('  Checking to see if we are past minimum optional open days');
      if api.pkg_ColumnQuery.DateValue( a_JobId, 'WaitForOptionalReferralsUntil') < trunc(sysdate)+1
      then --we will hit this option if the Close Referral process
           -- is created and no mandatory requests being outstanding
        dbg('  Closing Referral');
        CloseReferral( a_JobId );
      else dbg('  Do nothing');
      end if;
    end if;
  end;

  /* Path One is used when there are mandatories
   */
  procedure Path_One (
    a_ObjectId udt_Id,
    a_JobId    udt_Id,
    a_MandatoryCount pls_integer
  ) is
    t_CountClosed pls_integer;
    t_CountOpen pls_integer;
    t_Id udt_Id;
  begin
    dbg('  Path One');
    select count(*)
      into t_CountClosed
      from api.processes p
      where jobid = a_JobId
        and p.ProcessTypeId = g_ReferralRequestDefId
        and api.pkg_ColumnQuery.Value( processid, 'ReferralLevel') = 'Mandatory'
        and p.Outcome = 'Completed';
    t_CountOpen := a_MandatoryCount - t_CountClosed;
    if t_CountOpen > 0
    then
      /*  Open Mandatories ... let the scheduled "Manage overdue mandatories" process
        deal with them
       */
      null;
      dbg('  Open Mandatories still exist... exiting');
    else
      dbg('  No Open Mandatories');
      t_Id := GetProcessId( a_JobId, g_ManageOverdueResponseDefId);
      if t_Id is null -- ie, Mandatory Responses are not overdue
      then -- unschedule it and go to path two
        --dbg('  Unschedule "Manage Overdue Mandatories"');
        --toolbox.pkg_workflow.DeleteScheduledProcesses( a_JobId, sysdate, g_ManageOverdueResponseDefId);
        -- instead make a constructor on the Manage Overdue Mandatories so that it dies on the process server if it isn't needed
        Path_Two( a_ObjectId, a_JobId );
      else -- complete it and close referral if it exists
        if Complete( t_Id )
        then
          dbg('  Manage Overdue Mandatories already complete.  Id: '||t_Id);
        else
          dbg('  Completing Manage Overdue Mandatories id: '||t_Id);
          api.pkg_processupdate.Complete( t_Id, 'Managed');
        end if;

        t_Id := GetProcessId( a_JobId, g_CloseReferralResponseDefId) ;
        if t_Id is not null --Close Referral is the process exists (it will exist if we are past the min response days)
          then
            if Complete ( t_Id )
            then
              dbg('  Referral already closed');
            else
              dbg('  Bump Close Referral so that it closes itself');
              api.pkg_ColumnUpdate.SetValue( t_Id, 'CloseMe', 'Y' );
--              dbg('  Closing Referral');
--              api.pkg_ProcessUpdate.Complete( t_Id, 'Closed' );
            end if;
        end if;

      end if;
    end if;
  end;

  procedure MonitorReferral (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_MandatoryRequests api.udt_ObjectList;
    t_JobId udt_Id;
    t_MandatoryCount pls_integer;
  begin
    dbg('pkg_ER_MonitorReferral -- for processId: ' ||a_ObjectId);
    select jobid
      into t_JobId
      from api.processes
      where processid = a_ObjectId;

    t_MandatoryRequests := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_ReferralRequest');
    extension.pkg_CxProceduralSearch.SearchBySystemColumn( 'JobId', t_JobId, t_JobId, false);
    extension.pkg_CxProceduralSearch.SearchByIndex( 'ReferralLevel', 'Mandatory', 'Mandatory', false);
    extension.pkg_CxProceduralSearch.PerformSearch(t_MandatoryRequests, 'and');

    t_MandatoryCount := t_MandatoryRequests.count;

    dbg('  MandatoryCount: ' ||t_MandatoryCount);

    if t_MandatoryCount = 0
    then
      Path_two( a_ObjectId, t_JobId );
    else
      Path_one( a_ObjectId, t_JobId, t_MandatoryCount ) ;
    end if;
  end;

  procedure AddItem (
    a_List in out nocopy api.pkg_definition.udt_StringList,
    a_Text varchar2
  ) is
  begin
    a_List(a_List.count+1) :=  a_Text;
  end;

  procedure AnalyzeDayData(
    a_JobId udt_Id,
    d1 pls_integer, --response
    d2 pls_integer, --reminder
    d3 pls_integer  --optional
  ) is
    t_Errors varchar2(4000);
    t_ErrorList api.pkg_definition.udt_StringList;
    i pls_integer;
    t_MandatoryRequests api.udt_ObjectList;
  begin

    t_MandatoryRequests := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_ReferralRequest');
    extension.pkg_CxProceduralSearch.SearchBySystemColumn( 'JobId', a_JobId, a_JobId, false);
    extension.pkg_CxProceduralSearch.SearchByIndex( 'ReferralLevel', 'Mandatory', 'Mandatory', false);
    extension.pkg_CxProceduralSearch.PerformSearch(t_MandatoryRequests, 'and');

    t_ErrorList.delete;
    if d1 <0
    then
      AddItem( t_ErrorList, 'Mandatory Reponse Days can not be negative');
    end if;
    if d2 <0
    then
      AddItem( t_ErrorList, 'Mandatory Reminder Days can not be negative');
    end if;
    if d3 <0
    then
      AddItem( t_ErrorList, 'Minimum Days Open for Optional Response can not be negative');
    end if;

    if d1 is null
    then
      AddItem( t_ErrorList, 'Mandatory Reponse Days can not be null');
    end if;
    if d2 is null
    then
      AddItem( t_ErrorList, 'Mandatory Reminder Days can not be null');
    end if;
    if d3 is null
    then
      AddItem( t_ErrorList, 'Minimum Days Open for Optional Response can not be null');
    end if;

    if t_MandatoryRequests.count > 0 and d1 = 0
    then
     AddItem( t_ErrorList, 'Mandatory Reponse Days can not be 0 since a Referral Request exists with a referral level of Mandatory');
    end if;

    if not d1 >= d2
    then
      AddItem( t_ErrorList, 'Mandatory Reponse Days must be greater than or equal to the Mandatory Reminder Days');
    end if;

    if not d1 >= d3
    then
      AddItem( t_ErrorList, 'Mandatory Reponse Days must be greater than or equal to the Minimum Days Open for Optional Response');
    end if;

    if d1 + api.pkg_ColumnQuery.DateValue( a_JobId, 'PreparedDate') < api.pkg_ColumnQuery.DateValue( a_JobId, 'LatestDueDate')
    then
      AddItem( t_ErrorList, 'Mandatory Reponse Days can not be changed to ');
    end if;

    if t_ErrorList.count > 0
    then
      for i in 1..t_ErrorList.count
      loop
        if i < t_ErrorList.count
        then
          t_Errors := t_Errors || t_ErrorList(i) ||'; ' || chr(13) || chr(10);
        else
          t_Errors := t_Errors || t_ErrorList(i);
        end if;
      end loop;
      api.pkg_errors.RaiseError( -20000, t_Errors );
    end if;
  end;

  function ProcessCount (
    a_ObjectId udt_Id,
    a_DefId udt_Id
  ) return pls_integer is
    t_Cnt pls_integer;
  begin
    select count(*)
      into t_Cnt
      from api.processes
      where jobid = a_ObjectId
        and processtypeid = a_DefId;
    return t_Cnt;
  end;

  procedure RespondToDayChanges (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_PrevDays1 pls_integer;
    t_CurrDays1 pls_integer;
    t_PrevDays2 pls_integer;
    t_CurrDays2 pls_integer;
    t_PrevDays3 pls_integer;
    t_CurrDays3 pls_integer;
    t_Schedule boolean := false;
    t_Cancel boolean := false;
  begin
    dbg('Check for changes in Response, Reminder, and Minimum Optional days');
    select prev_MandatoryResponseDays, prev_MandatoryReminderDays, prev_MinimumOptionalDays,
           MandatoryResponseDays, MandatoryReminderDays, MinimumOptionalDays
      into t_PrevDays1, t_PrevDays2, t_PrevDays3,
           t_CurrDays1, t_CurrDays2, t_CurrDays3
      from query.j_ER_Referral
      where jobid = a_ObjectId;

    AnalyzeDayData( a_ObjectId, t_CurrDays1, t_CurrDays2, t_CurrDays3);
    dbg('  Set new values as old values');
    api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'prev_MandatoryResponseDays', t_CurrDays1 );
    api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'prev_MandatoryReminderDays', t_CurrDays2 );
    api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'prev_MinimumOptionalDays', t_CurrDays3 );
    --only do this if the job is actively waiting for responses
    if api.pkg_ColumnQuery.value( a_ObjectId, 'StatusName') = 'WAIT'
    then
      dbg('  Job in "WAIT" ... do some checking');
      if ProcessCount( a_ObjectId, g_ManageOverdueResponseDefId ) = 0
      then
        dbg('  No existing Manage Overdue Response process ...');
        t_Cancel := t_PrevDays1 is not null and (t_CurrDays1 != t_PrevDays1 or t_CurrDays1 = 0);
        t_Schedule := (t_PrevDays1 is null and t_CurrDays1 > 0) or t_CurrDays1 != t_PrevDays1;
        if t_Cancel
        then
          dbg('  Cancel scheduled Manage Overdue process ...');
          toolbox.pkg_workflow.DeleteScheduledProcesses( a_ObjectId, a_AsOfDate, 'p_ER_ManageOverdueMandRequests');
        end if;
        if t_Schedule
        then
          dbg('  Schedule new Manage Overdue process ...');
          toolbox.pkg_workflow.CreateProcessesFromCheckBoxes( a_ObjectId, a_AsOfDate, 'VALUE:Y<, , , , MandatoryResponseDueDate>p_ER_ManageOverdueMandRequests');
        end if;
      end if;

      if ProcessCount( a_ObjectId, g_SendReminderDefId ) = 0
      then
        dbg('  No existing Send Reminder process ...');
        t_Cancel := t_PrevDays2 is not null and (t_CurrDays2 != t_PrevDays2 or t_CurrDays2 = 0);
        t_Schedule := (t_PrevDays2 is null and t_CurrDays2 > 0) or t_CurrDays2 != t_PrevDays2;
        if t_Cancel
        then
          dbg('  Cancel scheduled Send Reminder process ...');
          toolbox.pkg_workflow.DeleteScheduledProcesses( a_ObjectId, a_AsOfDate, 'p_ER_SendReminder');
        end if;
        if t_Schedule
        then
          dbg('  Schedule new Send Reminder process ...');
          toolbox.pkg_workflow.CreateProcessesFromCheckBoxes( a_ObjectId, a_AsOfDate, 'VALUE:Y<Sent, , , , SendReminderDate>p_ER_SendReminder');
        end if;
      end if;

      if ProcessCount( a_ObjectId, g_CloseReferralResponseDefId ) = 0
      then
        dbg('  No existing Close Referral Response process ...');
        t_Cancel := t_PrevDays3 is not null and (t_CurrDays3 != t_PrevDays3 or t_CurrDays3 = 0);
        t_Schedule := (t_PrevDays3 is null and t_CurrDays3 > 0) or t_CurrDays3 != t_PrevDays3;
        if t_Cancel
        then
          dbg('  Cancel scheduled Close Referral Response process ...');
          toolbox.pkg_workflow.DeleteScheduledProcesses( a_ObjectId, a_AsOfDate, 'p_ER_CloseReferralResponse');
        end if;
        if t_Schedule
        then
          dbg('  Schedule new Close Referral Response process ...');
          toolbox.pkg_workflow.CreateProcessesFromCheckBoxes( a_ObjectId, a_AsOfDate, 'VALUE:Y<, , , , WaitForOptionalReferralsUntil>p_ER_CloseReferralResponse');
        end if;
      end if;

      dbg('Done checking changes');
    end if;

  end;

begin
  g_ReferralRequestDefId := api.pkg_configquery.ObjectDefIdForName( 'p_ER_ReferralRequest');
  g_CloseReferralResponseDefId := api.pkg_configquery.ObjectDefIdForName( 'p_ER_CloseReferralResponse');
  g_ManageOverdueResponseDefId := api.pkg_configquery.ObjectDefIdForName( 'p_ER_ManageOverdueMandRequests');
  g_SendReminderDefId := api.pkg_configquery.ObjectDefIdForName( 'p_ER_SendReminder');
end pkg_ER_MonitorReferral;

/

