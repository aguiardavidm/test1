create or replace package pkg_ER_ReferralRequest is

  -- Author  : MICHAEL.FROESE
  -- Created : 6/10/2009 12:31:26 PM
  -- Purpose : Referral Request utilities

  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  g_ERRecipientAccessGroupId udt_Id;


  /***
   *  ProcessReferees
   *  -- The Black box which takes Distribution Lists, Users, Referral Agencies
   *     related to the Referral Job and creates Referral Request Processes
   *     if one doesn't already exist for that User/Referral Agency
   *  -- First must break down a Distribution List into its components if req.
   */
  procedure ProcessReferees (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /* SendReferralRequests
   *  do necessary bits to send the Referral Request
   --per process found by search on JobId, RequestNotSent=Y
      -- error check
      -- copy referee detail lookups to stored
      -- set email flag ( .. anticipating an email being sent as a column change procedure ?)
        -- the column set is a trigger only and needs to be reset by the email procedure so we can fire it again
      -- unset RequestNotSent
      -- set RequestSent
      -- set RequestSentDate
      --do the flag setting for resends too
      --
  */
  procedure Send (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure Resend (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure ReAssignToOtherStaff (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) ;

  procedure CopyDetailsToSubJob (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) ;

    procedure CopySummaryDocToOrigRequest (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  function IsInAccessGroup (
    a_UserId udt_Id,
    a_AccessGroupId udt_Id
  ) return boolean;

  procedure RelateSummaryRecommendations (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure AssignSummaryNotification (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  procedure TriggerSummaryEmails (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /****************************************************************************************************
  /* Attached on the post-verify of p_ER_ReferralRequest.
  /* validates that the mandatory information is provided, and then completes the process
  /****************************************************************************************************/
  procedure ValidateResponse (
    a_ObjectId       udt_Id,
    a_AsOfDate       date,
    a_Outcome        varchar2,
    a_ResponseClosed varchar2,
    a_NoResponse     varchar2
  );

  /****************************************************************************************************
  /* Attached on the Delete Me detail of the r_ReferralRequestRecipientAtt relationship
  /* Used to delete the rel and document on an external presentation.
  /****************************************************************************************************/
  procedure DeleteRecipientDoc (
    a_ObjectId       udt_Id,
    a_AsOfDate       date,
    a_DeleteMe       varchar2
  );

  /****************************************************************************************************
  /* CopyInfoToRequest()
  /* Copies the accurate info to the request in the case where Request Method changes.
  /****************************************************************************************************/
  procedure CopyInfoToRequest (
    a_ObjectId udt_Id,
    a_AsOfDate date

  );

end pkg_ER_ReferralRequest;

 

/

create or replace package body pkg_ER_ReferralRequest is

  subtype udt_ObjectList is api.udt_objectlist;

  g_ReferralRequestDefId udt_Id := api.pkg_configquery.ObjectDefIdForName('p_ER_ReferralRequest');
  g_RefReqUserEPId       udt_Id := api.Pkg_Configquery.EndPointIdForName( g_ReferralRequestDefId,
                                                                     'UserRecipient');
  g_RefReqAgencyEPId     udt_Id := api.Pkg_Configquery.EndPointIdForName( g_ReferralRequestDefId,
                                                                     'ReferralAgencyRecipient');

  type udt_Referee is record (
    ReferralType           varchar2(15),
    Id                     udt_Id,
    ToEndPointId           udt_Id,
    RequestMethodEmail     varchar2(1),
    RequestMethodOnline    varchar2(1),
    RequestMethodHardcopy  varchar2(1),
    ResponseMethod         varchar2(50),
    ReferralLevel          varchar2(50),
    SendSummary            varchar2(1),
    Legislated             varchar2(1),
    RecipientType          varchar2(10),
    PublishingLevel        varchar2(50)
  );

  type udt_Referees is table of udt_Referee index by binary_integer;

  function AlreadyExists(
    a_Job udt_Id,
    a_ObjectId udt_Id
  ) return boolean is
    t_Ids udt_IdList;
    t_IdsLevel2 udt_IdList;
    i pls_integer;
    j pls_integer;
    ret_val boolean := false;
  begin
    t_Ids := extension.pkg_objectquery.RelatedObjects( a_Job, 'Recipients');
    for i in 1..t_Ids.count
    loop
      t_IdsLevel2 := extension.pkg_objectquery.RelatedObjects( t_Ids(i), 'RecipientGRP');
      for j in 1..t_IdsLevel2.count
      loop
        if a_ObjectId = t_IdsLevel2(j)
        then
          ret_val := True;
          exit; --kill the loop as we don't need to check anymore
        end if;
      end loop;
    end loop;
    return ret_val;
  end;


  procedure DoIt(
    a_ObjectId udt_Id,
    a_Referees in out nocopy udt_Referees
  ) is
    t_ProcessId udt_Id;
    t_RelId     udt_Id;
    i           pls_integer;
  begin
    for i in 1..a_Referees.count
    loop
      if not AlreadyExists (a_ObjectId, a_Referees(i).Id )
      then
        t_ProcessId := api.Pkg_Processupdate.New( a_ObjectId,
                                                  g_ReferralRequestDefId,
                                                  '', --Description
                                                  null, null, null
                                                );
        t_RelId := api.pkg_relationshipupdate.New( a_Referees(i).ToEndPointId,
                                                   t_ProcessId,
                                                   a_Referees(i).Id
                                                 );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'RequestMethodEmail', a_Referees(i).RequestMethodEmail );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'RequestMethodOnline', a_Referees(i).RequestMethodOnline );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'RequestMethodHardcopy', a_Referees(i).RequestMethodHardcopy );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'ResponseMethod', a_Referees(i).ResponseMethod );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'ReferralLevel', a_Referees(i).ReferralLevel );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'SendSummary', a_Referees(i).SendSummary );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'Legislated', a_Referees(i).Legislated );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'RecipientType', a_Referees(i).RecipientType );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'PublishingLevel', a_Referees(i).PublishingLevel );
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'RequestNotSent', 'Y' );

      end if;
    end loop;
  end;

  procedure AddUser (
    a_Id       in out nocopy udt_Id,
    a_Referees in out nocopy udt_Referees
  ) is
    t_Referee udt_Referee;
  begin
    t_Referee.ReferralType          := 'User';
    t_Referee.Id                    := a_Id;
    t_Referee.ToEndPointId          := g_RefReqUserEPId;
    t_Referee.RequestMethodEmail    := 'N';
    t_Referee.RequestMethodOnline   := 'Y';
    t_Referee.RequestMethodHardcopy := 'N';
    t_Referee.ResponseMethod        := 'Authenticated Web';
    t_Referee.ReferralLevel         := 'Optional';
    t_Referee.SendSummary           := 'N';
    t_Referee.Legislated            := 'N';
    t_Referee.RecipientType         := 'User';
    t_Referee.PublishingLevel       := 'Internal Only';

    a_Referees(a_Referees.count + 1 ) := t_Referee;
  end;

  procedure AddUsers (
    a_IdList    in out nocopy udt_IdList,
    a_Referees  in out nocopy udt_Referees
  ) is
    i pls_integer;
  begin
    for i in 1..a_IdList.count
    loop
      AddUser( a_IdList(i), a_Referees);
    end loop;
  end;

  procedure AddAgency (
    a_Id       in out nocopy udt_Id,
    a_Referees in out nocopy udt_Referees
  ) is
    t_Referee udt_Referee;

  begin
    t_Referee.ReferralType          := 'Agency';
    t_Referee.Id                    := a_Id;
    t_Referee.ToEndPointId          := g_RefReqAgencyEPId;
    t_Referee.RequestMethodEmail    := api.pkg_ColumnQuery.Value( a_Id, 'RequestMethodEmail');
    t_Referee.RequestMethodOnline   := api.pkg_ColumnQuery.Value( a_Id, 'RequestMethodOnline');
    t_Referee.RequestMethodHardcopy := api.pkg_ColumnQuery.Value( a_Id, 'RequestMethodHardcopy');
    t_Referee.ResponseMethod        := api.pkg_ColumnQuery.Value( a_Id, 'ResponseMethod');
    t_Referee.ReferralLevel         := 'Optional';
    t_Referee.SendSummary           := 'N';
    t_Referee.Legislated            := 'N';
    t_Referee.RecipientType         := 'Agency';
    if api.pkg_ColumnQuery.Value( a_Id, 'Type') in ('Referral Center','Internal Agency') then
       t_Referee.PublishingLevel       := 'Internal Only';
    else
       t_Referee.PublishingLevel       := 'External Agency';
    end if;


    -- Set Agency's flag "AgencyTypeIsLocked" so this doesn't get changed.  We set it once we use it.. which we are now doing
    api.pkg_ColumnUpdate.SetValue( a_Id, 'AgencyTypeIsLocked', 'Y');

    a_Referees(a_Referees.count + 1 ) := t_Referee;
  end;

  procedure AddAgencies (
    a_IdList    in out nocopy udt_IdList,
    a_Referees  in out nocopy udt_Referees
  ) is
    i pls_integer;

  begin
    for i in 1..a_IdList.count
    loop
      AddAgency( a_IdList(i), a_Referees);
    end loop;

  end;

  procedure ProcessDistributionList (
    a_DistId    in out nocopy udt_Id,
    a_Referees  in out nocopy udt_Referees
  ) is
    t_Referees udt_Referees;
    i pls_integer;

  begin
    select 'User',
           UserId,
           g_RefReqUserEPId,
           'N',
           'Y',
           'N',
           'Authenticated Web',
           ReferralLevel,
           SendSummary,
           Legislated,
           'User',
           'Internal Only'
      bulk collect into t_Referees
      from query.r_ER_UserDistributionList r
      where r.DistributionListObjectId = a_DistId
        and api.pkg_ColumnQuery.Value( UserId, 'Active') = 'Y';

    for i in 1..t_Referees.count
    loop
      a_Referees(a_Referees.count + 1) := t_Referees(i);
    end loop;

    t_Referees.delete;


    select 'Agency',
           ReferralAgencyObjectId,
           g_RefReqAgencyEPId,
           api.pkg_ColumnQuery.Value( r.ReferralAgencyObjectId, 'RequestMethodEmail'),
           api.pkg_ColumnQuery.Value( r.ReferralAgencyObjectId, 'RequestMethodOnline'),
           api.pkg_ColumnQuery.Value( r.ReferralAgencyObjectId, 'RequestMethodHardcopy'),
           api.pkg_ColumnQuery.Value( r.ReferralAgencyObjectId, 'ResponseMethod'),
           ReferralLevel,
           SendSummary,
           Legislated,
           'Agency',
           'External Agency'
      bulk collect into t_Referees
      from query.r_ER_ReferAgentDistribList r
      where r.DistributionListObjectId = a_DistId
        and api.pkg_ColumnQuery.Value( ReferralAgencyObjectId, 'Active') = 'Y';

    for i in 1..t_Referees.count
    loop
      a_Referees(a_Referees.count + 1) := t_Referees(i);
    end loop;

    t_Referees.delete;

  end;

  procedure ProcessDistributionLists (
    a_DistIds   in out nocopy udt_IdList,
    a_Referees  in out nocopy udt_Referees
  ) is
    i pls_integer;
  begin
    for i in 1..a_DistIds.count
    loop
      ProcessDistributionList( a_DistIds(i), a_Referees );
    end loop;
  end;


  procedure ProcessReferees (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_Referees                 udt_Referees;
    t_DistributionListRelIds   udt_IdList;
    t_UserRelIds               udt_IdList;
    t_ReferralAgencyRelIds     udt_IdList;

    t_DistributionListIds      udt_IdList;
    t_UserIds                  udt_IdList;
    t_ReferralAgencyIds        udt_IdList;

    t_ReferralObjectDefId      udt_Id;
    t_DistributionListEPId     udt_Id;
    t_UserEPId                 udt_Id;
    t_ReferralAgencyEPId       udt_Id;

  begin
    t_ReferralObjectDefId  := api.pkg_configquery.ObjectDefIdForName( 'j_ER_Referral');
    t_DistributionListEPId := api.pkg_configquery.EndPointIdForName(t_ReferralObjectDefId,  'RRDistListTMP' );
    t_UserEPId             := api.pkg_configquery.EndPointIdForName(t_ReferralObjectDefId,  'RRUserTMP' );
    t_ReferralAgencyEPId   := api.pkg_configquery.EndPointIdForName(t_ReferralObjectDefId, 'RRAgencyTMP' );

    --Acquire Relationships that we need to process
    --Distribution Lists
    select RelationshipId, ToObjectId
      bulk collect into t_DistributionListRelIds, t_DistributionListIds
      from api.relationships r
      where r.FromObjectId = a_ObjectId
        and r.EndPointId =  t_DistributionListEPId;

    --Users
    select RelationshipId, ToObjectId
      bulk collect into t_UserRelIds, t_UserIds
      from api.relationships r
      where r.FromObjectId = a_ObjectId
        and r.EndPointId =  t_UserEPId;

    --Referral Agencies
    select RelationshipId, ToObjectId
      bulk collect into t_ReferralAgencyRelIds, t_ReferralAgencyIds
      from api.relationships r
      where r.FromObjectId = a_ObjectId
        and r.EndPointId =  t_ReferralAgencyEPId;


    if t_DistributionListRelIds.count
         + t_UserRelIds.count
         + t_ReferralAgencyRelIds.count > 0
    then

      --Process Distribution List
      --  this means, break out the Users and Agencies and stuff them into the users or
      --  referral agencies collections
      if t_DistributionListRelIds.count > 0
      then
        ProcessDistributionLists( t_DistributionListIds, t_Referees  );
      end if;

      --Process Users
      if t_UserIds.count > 0
      then
        AddUsers( t_UserIds, t_Referees );
      end if;

      --Process Referral Agencies
      if t_ReferralAgencyIds.count > 0
      then
        AddAgencies( t_ReferralAgencyIds, t_Referees );
      end if;

      DoIt( a_ObjectId, t_Referees );

      t_Referees.delete;

      -- Cleanup Referral Job Rels
      for i in 1 .. t_DistributionListRelIds.count
      loop
        api.Pkg_RelationshipUpdate.Remove( t_DistributionListRelIds(i) );
      end loop;
      for i in 1 .. t_ReferralAgencyRelIds.count
      loop
        api.Pkg_RelationshipUpdate.Remove( t_ReferralAgencyRelIds(i) );
      end loop;
      for i in 1 .. t_UserRelIds.count
      loop
        api.Pkg_RelationshipUpdate.Remove( t_UserRelIds(i) );
      end loop;

    else
      null; --nothing to do...
    end if;
  end;



  /**********************************
    Send Referral Request Logic
   **********************************/


  /* SendReferralRequests
   *  do necessary bits to send the Referral Request
   --per process found by search on JobId, RequestNotSent=Y
      -- error check
      -- copy referee detail lookups to stored
      -- set email flag ( .. anticipating an email being sent as a column change procedure ?)
      -- unset RequestNotSent
      -- set RequestSent
      -- set RequestSentDate

      --
  */

/*  procedure DocCopy (
    a_JobId udt_Id,
    a_ProcessId udt_Id,
    a_EPName varchar2,
    a_WipeOutExisting boolean
  ) is
    t_Rels udt_IdList;
    i pls_integer;
    t_Docs udt_IdList;
    t_RelId udt_Id;
  begin
    if a_WipeOutExisting
    then
      select relationshipid
        bulk collect into t_Rels
        from api.relationships r
             join api.relationshipdefs rd
               on rd.RelationshipDefId = r.RelationshipDefId
               and rd.ToEndPointName = a_EPName;
      for i in 1..t_Rels.count
      loop
        api.pkg_relationshipupdate.Remove( t_Rels(i), sysdate );
      end loop;
    end if;

    t_Docs := extension.pkg_objectquery.RelatedObjects( a_JobId, a_EPName );
    for i in 1..t_Docs.count
    loop
      t_RelId := extension.pkg_relationshipupdate.New( a_ProcessId, t_Docs(i), 'Document');
    end loop;
  end;*/

  procedure QuickCopy(
    a_ObjectId udt_Id,
    a_FromCol  varchar2,
    a_ToCol    varchar2
  ) is
  begin
    api.pkg_ColumnUpdate.SetValue( a_ObjectId, a_ToCol, api.pkg_ColumnQuery.Value( a_ObjectId, a_FromCol) );
  end;

  procedure MarkDocsAsSent(
    a_ObjectId udt_Id
  ) is
    t_Objects api.Udt_ObjectList;
    i pls_integer;
  begin
    pkg_ER_Utils.AllDocsForDistribution( a_ObjectId, null, t_Objects);
    --blindly go through all the documents and set their ERSent flag.
    -- ... probably just as expensive to set a 'Y' to a 'Y' as to check the
    -- value and then change it if required.
    for i in 1..t_Objects.count
    loop
      api.pkg_ColumnUpdate.SetValue(t_Objects(i).ObjectId, 'ERSent', 'Y');
    end loop;
  end;

  function IsInAccessGroup (
    a_UserId udt_Id,
    a_AccessGroupId udt_Id
  ) return boolean
  is
    i pls_integer;
  begin
    select count(*)
      into i
      from api.accessgroupusers agu
      where agu.AccessGroupId = a_AccessGroupId
        and agu.UserId = a_UserId;
    if i > 0
    then
      return true;
    else
      return false;
    end if;
  end;

  procedure AssignRequest (
    a_ObjectId udt_Id
  ) is
    t_Assignees udt_IdList;
    t_SummaryNotifId udt_Id default null;
    t_RequestObjectId udt_Id;
  begin

    /* See first where the call is coming from, Request process or Summary Notification process */
    if api.pkg_columnquery.Value(a_ObjectId,'ObjectDefName') = 'p_ER_SummaryNotification'
      then
      /*If the call is coming from Summary Notification process then */
      t_SummaryNotifId := a_ObjectId;
      begin
        select RequestProcessId
          into t_RequestObjectId
          from query.r_ReferralRequestSummaryNotif
         where NotificationProcessId = t_SummaryNotifId;
        exception
           when NO_DATA_FOUND then
             return;
        end;
    else
      t_RequestObjectId := a_ObjectId;
    end if;

    --Assign Primary Active Agency Reps who are online
    select unique ru.UserId
      bulk collect into t_Assignees
    from query.r_ER_ReferAgencyRefAgenRepres ra
         join query.r_er_ReferralReqRefAgency re
           on ra.ReferralAgencyObjectId = re.ReferralAgencyObjectId
           and api.pkg_columnquery.Value(ra.ReferralAgencyRepObjectId ,'Primary') = 'Y'
         join query.r_ER_ReferAgencyRepresentUser ru
           on ru.ReferralAgencyRepObjectId = ra.ReferralAgencyRepObjectId
         join api.users u
           on u.UserId = ru.UserId
           and u.Active = 'Y'
     where re.ReferralRequestId = t_RequestObjectId;

    for i in 1..t_Assignees.count
    loop
      --ensure user has correct access group
      if not IsInAccessGroup( t_Assignees(i), g_ERRecipientAccessGroupId )
        then
          api.pkg_userupdate.AddToAccessGroup( t_Assignees(i), g_ERRecipientAccessGroupId);
      end if;
      pkg_Debug.Putline( '##ER - ASSIGNMENT##: ' || t_Assignees(i) );
      --If t_SummaryNotifId is null that means the call is coming from request process
      if t_SummaryNotifId is null then
         api.Pkg_Processupdate.Assign( a_ObjectId, api.pkg_ColumnQuery.Value( t_Assignees(i), 'OracleLogonId') );
      else
         api.Pkg_Processupdate.Assign( t_SummaryNotifId, api.pkg_ColumnQuery.Value( t_Assignees(i), 'OracleLogonId') );
      end if;
    end loop;

    --This only apply if coming from Request Process
    if t_Assignees.count > 0 and t_SummaryNotifId is null
    then
      -- Update Recipient List with actual recipients
      api.pkg_columnupdate.SetValue( a_ObjectID, 'RecipientList', api.pkg_processquery.StaffNameList( a_ObjectId ) );
    else
      null; -- just leave things alone
    end if;

    --Assign Directly related users (i.e. Internal Staff)
    t_Assignees := extension.pkg_objectquery.RelatedObjects( t_RequestObjectId, 'UserRecipient');
    for i in 1..t_Assignees.count
    loop
      --ensure user has correct access group
      if not IsInAccessGroup( t_Assignees(i), g_ERRecipientAccessGroupId )
        then
          api.pkg_userupdate.AddToAccessGroup( t_Assignees(i), g_ERRecipientAccessGroupId);
      end if;
      pkg_Debug.Putline( '##ER - Internal ASSIGNMENT##: ' || t_Assignees(i) );
      --If t_SummaryNotifId is null that means the call is coming from request process
      if t_SummaryNotifId is null
      then
        api.Pkg_Processupdate.Assign( a_ObjectId, api.pkg_ColumnQuery.Value( t_Assignees(i), 'OracleLogonId') );
      else
        api.Pkg_Processupdate.Assign( t_SummaryNotifId, api.pkg_ColumnQuery.Value( t_Assignees(i), 'OracleLogonId') );
      end if;
    end loop;
  end;

  procedure RelateSummaryRecommendations (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_JobId udt_Id;
    t_RelationshipId udt_Id;
    i pls_integer;

    cursor c_Recommendations is
    select SummaryRecommendationObjectId,
           Checked
      from query.r_er_ReferralSumRecommendation
     where ReferralJobId = api.pkg_columnquery.Value(a_ObjectId,'JobId');
  begin

    for i in c_Recommendations loop
      if i.Checked = 'Y' then
         t_RelationshipId := extension.pkg_relationshipupdate.New(a_ObjectId, i.SummaryRecommendationObjectId, 'SummRecommendation');
      end if;
    end loop;

  end;

  procedure AssignSummaryNotification (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
  begin

    AssignRequest( a_ObjectId );

  end;

  procedure SendRequest (
    a_ObjectId udt_Id,
    a_FromResend boolean default false
  ) is
    t_ReferralLevel varchar2(50) := api.pkg_ColumnQuery.Value( a_ObjectId, 'ReferralLevel');
    t_PublishingLevel varchar2(50) := api.pkg_ColumnQuery.Value( a_ObjectId, 'PublishingLevel');
    t_ExtensionDate date := api.pkg_ColumnQuery.DateValue( a_ObjectId, 'ExtensionDate');
    t_JobId udt_Id := api.pkg_ColumnQuery.NumericValue( a_ObjectId, 'JobId');
    t_Assignees udt_IdList;
  begin

 --   api.pkg_errors.RaiseError( -20000, 'ret' || api.pkg_ColumnQuery.DateValue( a_ObjectId, 'RequestSentDate'));
    --no error checking in this... I'm going to assume it all works... the only thing to ensure that
    --I can see from my vantage point is that every user must be an ER Recipient.

    if api.pkg_ColumnQuery.Value( a_ObjectId, 'RequestMethodOnline' ) = 'Y'
    then
      AssignRequest( a_ObjectId );
    end if;

    MarkDocsAsSent( a_ObjectId);

    api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'RequestNotSent', 'N');
    api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'RequestSent', 'Y');
    api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'RequestSentDate', sysdate);
--    api.pkg_errors.RaiseError( -20000, t_ReferralLevel);
    if t_ReferralLevel in ('Notification', 'Summary Only')
    then
      api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'ResponseClosed', 'Y');
    else
      if t_ExtensionDate is not null
      then
        null;
        /* I am now doing this as a column change procedure on ExtensionDate
        QuickCopy( a_ObjectId, 'ExtensionDate', 'ResponseDueDate');
        */
      else if not a_FromResend
           then
             QuickCopy( a_ObjectId, 'ResponseDueDate', 'ResponseDueDate');
           else
             QuickCopy( a_ObjectId, 'JobBasedResponseDueDate', 'ResponseDueDate');
           end if;
      end if;
    end if;
    --trigger email if necessary
    if api.pkg_ColumnQuery.Value( a_ObjectId, 'RequestMethodEmail' ) = 'Y'
    and api.pkg_ColumnQuery.Value( a_ObjectId, 'ReferralLevel' ) != 'Summary Only'
    then
      api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'TriggerEmail', 'Y');
    end if;
/* -- using this approach.... documents on the job will be available via an indirect or procedural rel
    if t_PublishingLevel = 'Internal Only'
    then
      DocCopy( t_JobId, a_ObjectId, 'InternalAttachment', a_FromResend);
    elsif t_PublishingLevel = 'External Agency'
    then
      DocCopy( t_JobId, a_ObjectId, 'ExternalAttachment', a_FromResend);
    elsif t_PublishingLevel = 'Public'
    then
      DocCopy( t_JobId, a_ObjectId, 'PublicAttachment', a_FromResend);
    end if;
*/
  end;

  procedure Resend (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_Resend varchar2(1) := api.pkg_ColumnQuery.Value( a_ObjectId, 'SendAgain');
  begin
    if t_Resend = 'Y'
    then
      --raise_application_error(-20000, 'hjere');
      SendRequest( a_ObjectId, true );
      api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'SendAgain', 'N');
    end if;
  end;

  procedure SendRequestWithCopy (
    a_ProcessId udt_Id
  ) is
    t_RecipientType varchar2(100) := api.pkg_ColumnQuery.Value( a_ProcessId, 'RecipientType');
  begin
    if t_RecipientType = 'User'
    then
      QuickCopy( a_ProcessId, 'UserOrganizationName', 'OrganizationName');
      QuickCopy( a_ProcessId, 'UserName',             'RecipientList');
      QuickCopy( a_ProcessId, 'UserName',             'RecipientName');
      if api.pkg_ColumnQuery.Value( a_ProcessId, 'EmailToList') is null
      then
         QuickCopy( a_ProcessId, 'UserEmail',           'EmailToList');
      end if;
      QuickCopy( a_ProcessId, 'UserBusinessTitle',    'BusinessTitle');
    end if;

    if t_RecipientType = 'Agency'
    then
      QuickCopy( a_ProcessId, 'AgencyOrganizationName', 'OrganizationName');
      QuickCopy( a_ProcessId, 'AgencyAddressLine1',     'AddressLine1');
      QuickCopy( a_ProcessId, 'AgencyAddressLine2',     'AddressLine2');
      QuickCopy( a_ProcessId, 'AgencyCity',             'City');
      QuickCopy( a_ProcessId, 'AgencyEmailCCList',      'EmailCCList');
      QuickCopy( a_ProcessId, 'AgencyEmailToList',      'EmailToList');
      QuickCopy( a_ProcessId, 'AgencyName',             'RecipientName');
      QuickCopy( a_ProcessId, 'AgencyPostalCode',       'PostalCode');
      QuickCopy( a_ProcessId, 'AgencyProvince',         'Province');
      QuickCopy( a_ProcessId, 'AgencyRecipientList',    'RecipientList');
    end if;

    SendRequest( a_ProcessId );

  end;

  function NextRequestNumber (
    a_ObjectId udt_Id
  ) return varchar2 is
    t_RetVal varchar2(10);
  begin
    t_RetVal := api.pkg_ColumnQuery.Value( a_ObjectId, 'NextRequestNumber');
    api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'RequestNumberSequence', to_number(t_RetVal));
    return t_RetVal;
  end;

 procedure RelateSupportingObjects (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_JobId udt_Id;
    t_AnswerDefId udt_Id := api.pkg_configquery.ObjectDefIdForName( 'o_ER_ReferralAnswer');
    t_ReqRecDefId udt_Id := api.pkg_configquery.ObjectDefIdForName( 'o_ER_RequestRecommendation');
    t_Ids udt_IdList;
    t_RelationshipId udt_Id;
    i pls_integer;
  begin
    pkg_debug.putline( '##Relate Supporting  Objects##' );
    select jobid into t_JobID
      from api.processes where processid = a_ObjectID;

    select count(r.RelationshipId) into i
    from query.r_ER_ReferralRequestAnswer r
    where r.ReferralRequestId = a_ObjectID;
    if i = 0 then
      t_Ids.delete;
      t_Ids := extension.pkg_objectquery.RelatedObjects( t_JobID, 'ReferralAnswer' );
      for i in 1..t_Ids.count
      loop
        t_RelationshipId := extension.pkg_relationshipupdate.New(a_ObjectId, t_Ids(i), 'ReferralAnswer');
      end loop;
    end if;

    select count(r.RelationshipId) into i
    from query.r_er_RefReqRequestRecommend r
    where r.ReferralRequestProcessId = a_ObjectID;
    if i = 0 then
      t_Ids.delete;
      t_Ids := extension.pkg_objectquery.RelatedObjects( t_JobID, 'RequestRecommendation' );
      for i in 1..t_Ids.count
      loop
        t_RelationshipId := extension.pkg_relationshipupdate.New(a_ObjectId, t_Ids(i), 'RequestRecommendation');
      end loop;
    end if;
  end;

  procedure Send (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_Objects udt_ObjectList;
    i pls_integer;
    t_ErrorsExist boolean := false;
    t_PrepReferralProcessId udt_Id;
  begin

    begin
      select ProcessId
        into t_PrepReferralProcessId
        from api.Processes
       where JobId = a_ObjectId
         and api.pkg_ColumnQuery.Value(ProcessId, 'ObjectDefName') = 'p_ER_PrepareReferral'
         and outcome is null;
    exception
    when no_data_found then
      null ;
    end;

    --pkg_debug.putline( '##SEND##' );
    if api.pkg_ColumnQuery.Value( a_ObjectId, 'TriggerSendReferral') = 'Y'
    then
      --pkg_debug.putline( '##SEND processing##' );
      t_Objects := api.udt_ObjectList();
      extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_ReferralRequest');
      extension.pkg_CxProceduralSearch.SearchBySystemColumn( 'JobId', a_ObjectId, a_ObjectId, false);
      extension.pkg_CxProceduralSearch.SearchByIndex( 'RequestNotSent', 'Y', 'Y', false);
      extension.pkg_CxProceduralSearch.PerformSearch( t_Objects, 'and');

      if t_Objects.count = 0
      then
        null;
        --skip the error as we are no longer showing the button to send unless there is something to send
        --additionally, if the user refreshes the screen (F5) after sending , the browser raises this error..
        --so if we swallow it, it should be ok.

      else
        for i in 1..t_Objects.count
        loop
          if api.pkg_ColumnQuery.Value( t_Objects(i).objectid, 'SQLErrorsExist') = 'Y'
          then
            t_ErrorsExist := true;
          end if;
          if api.pkg_ColumnQuery.Value( a_ObjectId, 'MandatoryResponseReferralText') is null
          then
             if api.pkg_ColumnQuery.Value( t_Objects(i).objectid, 'ReferralLevel') = 'Mandatory'
             then
               api.pkg_errors.RaiseError( -2000, 'Before you go on, you must enter a value for Mandatory Referral Text.');
             end if;
          end if;
          if api.pkg_ColumnQuery.Value( a_ObjectId, 'OptionalResponseReferralText') is null
          then
             if api.pkg_ColumnQuery.Value( t_Objects(i).objectid, 'ReferralLevel') = 'Optional'
             then
               api.pkg_errors.RaiseError( -2000, 'Before you go on, you must enter a value for Optional Referral Text.');
             end if;
          end if;
          if api.pkg_ColumnQuery.Value( a_ObjectId, 'NotificationReferralText') is null
          then
             if api.pkg_ColumnQuery.Value( t_Objects(i).objectid, 'ReferralLevel') = 'Notification'
             then
               api.pkg_errors.RaiseError( -2000, 'Before you go on, you must enter a value for Notification Referral Text.');
             end if;
          end if;
          if api.pkg_ColumnQuery.Value( a_ObjectId, 'SummaryReferralText') is null
          then
             if api.pkg_ColumnQuery.Value( t_Objects(i).objectid, 'ReferralLevel') = 'Summary Only'
             then
               api.pkg_errors.RaiseError( -2000, 'Before you go on, you must enter a value for Summary Referral Text.');
             end if;
          end if;
        end loop;

        if t_ErrorsExist
        then
          api.pkg_errors.RaiseError( -2000, 'Errors exist with Referral Request recipients.  Please review the grid to locate and correct the errors.');
        end if;


        for i in 1..t_Objects.count
        loop
        -- set Request Number
        api.Pkg_Columnupdate.SetValue( t_Objects(i).objectid, 'RequestNumber', NextRequestNumber( a_ObjectId ) );
        SendRequestWithCopy( t_Objects(i).objectid );
        RelateSupportingObjects( t_Objects(i).objectid, sysdate);
        end loop;

      end if;

      api.pkg_ColumnUpdate.SetValue( a_ObjectId, 'TriggerSendReferral', 'N');

      if t_PrepReferralProcessId is null then
         null;
      else
         api.pkg_processupdate.Complete(t_PrepReferralProcessId, 'Referral Prepared');
      end if;

    end if;
  end;

  procedure ReAssignToOtherStaff (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
  t_count number;

  begin
      if api.pkg_ColumnQuery.Value(a_ObjectId,'CurrentUserName') is not null and api.pkg_ColumnQuery.Value(a_ObjectId,'ReassignedOracleLogonId') is not null then
         api.Pkg_Processupdate.Unassign(a_ObjectId, api.pkg_ColumnQuery.Value(a_ObjectId,'CurrentUserName'));
         api.Pkg_Processupdate.Assign( a_ObjectId, api.pkg_ColumnQuery.Value(a_ObjectId,'ReassignedOracleLogonId'));
      end if;
  end;

  procedure CopyDetailsToSubJob (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
  t_ParentJobId   number;
  t_RelationshipId number;
  t_OriginalRequestId number;
  t_Ids udt_IdList;

  begin

      t_OriginalRequestId := api.pkg_columnquery.NumericValue(a_ObjectId,'OriginalReferralRequestId');
      t_ParentJobId := api.pkg_columnquery.NumericValue(t_OriginalRequestId,'JobId');
       api.pkg_columnupdate.SetValue(a_ObjectId,'ReferenceNumber',api.pkg_columnquery.Value(t_ParentJobId, 'ReferenceNumber'));
      api.pkg_columnupdate.SetValue(a_ObjectId,'ParentJobId',t_ParentJobId);
      api.pkg_columnupdate.SetValue(a_ObjectId,'ReferralSummary',api.pkg_columnquery.Value(t_OriginalRequestId, 'ResponseText'));

      t_Ids := extension.pkg_objectquery.RelatedObjects( t_ParentJobId, 'Document' );
      for i in 1..t_Ids.count
      loop
        t_RelationshipId := extension.pkg_relationshipupdate.New(a_ObjectId, t_Ids(i), 'Document');
      end loop;

  end;

  procedure CopySummaryDocToOrigRequest (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
  t_RelationshipId number;
  t_OriginalRequestId number;
  t_JobId number;
  t_Ids udt_IdList;

  begin



      t_JobId := api.pkg_columnquery.NumericValue(a_ObjectId,'JobId');
      t_OriginalRequestId := api.pkg_columnquery.NumericValue(t_JobId,'OriginalReferralRequestId');
      t_Ids := extension.pkg_objectquery.RelatedObjects(t_JobId, 'SummaryAttachmentFIL' );
      If t_OriginalRequestId is not null then
        for i in 1..t_Ids.count
        loop
          t_RelationshipId := extension.pkg_relationshipupdate.New(t_OriginalRequestId, t_Ids(i), 'Document');
        end loop;
      end if;
  end;

  procedure TriggerSummaryEmails (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_Objects udt_ObjectList;
    i pls_integer;
    t_ErrorsExist boolean := false;
    t_ProcessId pls_Integer;
    t_EndPointId pls_Integer;
    t_Dummy pls_Integer;
  begin

    if api.pkg_ColumnQuery.Value( a_ObjectId, 'InitiateSummary') = 'Y'
    then

     /* ALBERT - this logic doesn't fit in the originally thought scope of this procedure
      however, I need to get this functionality in and this is the place to do it!
      When completing a summarization, if this is a sub-job, then the summary results need to
      get to the parent job.
      The System must default original request [Response Text] to the [Referral Summary].
      The System must create original request (Response Attachments) based on
      ((Referral Attachments)) marked as [Include in Summary].
      */
      begin
        t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName( 'p_ER_ReferralRequest', 'RecipientAttachment' );
        select r.ProcessId
          into t_ProcessId
          from query.r_ER_ReferralRequestSubRefer r
         where r.JobId = a_ObjectId;
        api.pkg_ColumnUpdate.SetValue( t_ProcessId, 'ResponseText',
          api.pkg_ColumnQuery.Value( a_ObjectId, 'ReferralSummary' ));
        for c in ( select r.WebDocumentId
                     from query.r_er_ReferralSumAttachment r
                    where r.ReferralJobId = a_ObjectId ) loop
          t_Dummy := api.pkg_RelationshipUpdate.New( t_EndPointId , t_ProcessId, c.WebDocumentId );
        end loop;

      exception when no_data_found then
        null;
      end;



      t_Objects := api.udt_ObjectList();
      extension.pkg_CxProceduralSearch.InitializeSearch('p_ER_SummaryNotification');
      extension.pkg_CxProceduralSearch.SearchBySystemColumn( 'JobId', a_ObjectId, a_ObjectId, false);
      extension.pkg_CxProceduralSearch.PerformSearch( t_Objects, 'and');
    end if;
    if t_Objects.count = 0
    then
      null;
    else
      for i in 1..t_Objects.count
      loop
        api.pkg_columnupdate.SetValue(t_Objects(i).objectid,'TriggerEmail','Y');
      end loop;
    end if;
  end;

  procedure CopyInfoToRequest (
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
    t_RecipientType varchar2(100) := api.pkg_ColumnQuery.Value( a_ObjectId, 'RecipientType');
  begin
    if t_RecipientType = 'User'
    then
      QuickCopy( a_ObjectId, 'UserOrganizationName', 'OrganizationName');
      QuickCopy( a_ObjectId, 'UserName',             'RecipientList');
      QuickCopy( a_ObjectId, 'UserName',             'RecipientName');
      if api.pkg_ColumnQuery.Value( a_ObjectId, 'EmailToList') is null
      then
         QuickCopy( a_ObjectId, 'UserEmail',           'EmailToList');
      end if;
      QuickCopy( a_ObjectId, 'UserBusinessTitle',    'BusinessTitle');
    end if;

    if t_RecipientType = 'Agency'
    then

      QuickCopy( a_ObjectId, 'AgencyOrganizationName', 'OrganizationName');
      QuickCopy( a_ObjectId, 'AgencyAddressLine1',     'AddressLine1');
      QuickCopy( a_ObjectId, 'AgencyAddressLine2',     'AddressLine2');
      QuickCopy( a_ObjectId, 'AgencyCity',             'City');
      QuickCopy( a_ObjectId, 'AgencyEmailCCList',      'EmailCCList');
      QuickCopy( a_ObjectId, 'AgencyEmailToList',      'EmailToList');
      QuickCopy( a_ObjectId, 'AgencyName',             'RecipientName');
      QuickCopy( a_ObjectId, 'AgencyPostalCode',       'PostalCode');
      QuickCopy( a_ObjectId, 'AgencyProvince',         'Province');
      QuickCopy( a_ObjectId, 'AgencyRecipientList',    'RecipientList');
    end if;

  end;


  procedure ValidateResponse (
    a_ObjectId       udt_Id,
    a_AsOfDate       date,
    a_Outcome        varchar2,
    a_ResponseClosed varchar2,
    a_NoResponse     varchar2
  )
  is
    t_Count        pls_Integer;
    t_CheckedCount pls_Integer default 0;
    t_TextRequired boolean default false;
    t_ReferralLevel varchar2(30);
  begin

    if a_ResponseClosed = 'Y' and a_Outcome is null then
      if a_NoResponse = 'N' then
        t_ReferralLevel := api.pkg_ColumnQuery.Value( a_ObjectId, 'ReferralLevel' );
        if t_ReferralLevel != 'Summary Only' and t_ReferralLevel != 'Notification' then
          --if there are questions, check that all have been answered.
          select count(r.RelationshipId)
            into t_Count
            from query.r_ER_ReferralRequestAnswer r
           where r.ReferralRequestId = a_ObjectId
             and r.Answer is null;
          if t_Count > 0 then
            api.pkg_Errors.RaiseError( -20000, 'Please provide an answer to all the questions.' );
          end if;
          --if there are recommendations, check that one and only one has been selected.
          --if a recommendation is chosen that has 'Text Required' set, then
          --check that a text respones has also been provided.
          --if there are no recommendations, ensure that a text response has been given
          t_Count := 0;
          for c in ( select r.RequestRecommendationObjectId,
                            r.Checked,
                            rr.ResponseTextRequired
                       from query.r_er_RefReqRequestRecommend r
                       join query.o_ER_RequestRecommendation rr on r.RequestRecommendationObjectId = rr.ObjectId
                      where r.ReferralRequestProcessId = a_ObjectId ) loop
            t_Count := t_Count + 1;
            if c.Checked = 'Y' then
              t_CheckedCount := t_CheckedCount + 1;
              if c.ResponseTextRequired = 'Y' then
                t_TextRequired := true;
              end if;
            end if;
          end loop;
          if t_CheckedCount > 1 then
            api.pkg_Errors.RaiseError( -20000, 'Please choose only one recommendation.');
          end if;
          if t_Count > 0 and t_CheckedCount = 0 then
            api.pkg_Errors.RaiseError( -20000, 'Please choose a recommendation.');
          end if;
          if t_Count = 0 then
            t_TextRequired := true;
          end if;
          if t_TextRequired then
            if api.pkg_ColumnQuery.IsNull( a_ObjectId, 'ResponseText' ) then
              api.pkg_Errors.RaiseError( -20000, 'Please provide Response Text.');
            end if;
          end if;
        end if;
      end if;
      api.pkg_ProcessUpdate.Complete( a_ObjectId, 'Completed' );
    end if;
  end ValidateResponse;

  procedure DeleteRecipientDoc (
    a_ObjectId       udt_Id,
    a_AsOfDate       date,
    a_DeleteMe       varchar2
  )
  is
    t_DocId udt_Id;
  begin
    if a_DeleteMe = 'Y' then
      select r.WebDocumentId
        into t_DocID
        from query.r_ReferralRequestRecipientAtt r
       where r.RelationshipId = a_ObjectId;
      api.pkg_ObjectUpdate.Remove( t_DocId );
    end if;
  exception when no_data_found then
    null;
  end DeleteRecipientDoc;


  begin
    select ag.AccessGroupId
      into g_ERRecipientAccessGroupId
      from api.accessgroups ag
      where ag.Description = 'ER Recipient';
end pkg_ER_ReferralRequest;

/

