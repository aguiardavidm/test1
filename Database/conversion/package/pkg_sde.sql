create or replace package pkg_SDE as

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *   Routines used to query an SDE listener.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_ObjectList is api.udt_ObjectList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  /*---------------------------------------------------------------------------
   * Merge()
   *   Perform a merge of the items which match the input criteria, creating
   * a new item on the specified layer with the data attributes being set.
   *-------------------------------------------------------------------------*/
  Procedure Merge (
    a_SourceLayerName           varchar2,
    a_SourceLayerColumnName     varchar2,
    a_SourceValues              udt_StringList,
    a_ResultLayerName           varchar2,
    a_ResultColumnNames         udt_StringList,
    a_ResultValues              udt_StringList
  );

  /*---------------------------------------------------------------------------
   * PerformQuery()
   *   Perform a spatial query using the SDE listener and the parameters stored
   * in the table SDEQueries indexed by EndPointId. The objects found are
   * placed in the provided array.
   *-------------------------------------------------------------------------*/
  procedure PerformQuery (
    a_ObjectId				udt_Id,
    a_EndPointId			udt_Id,
    a_RelatedObjects			out nocopy udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PerformQuery()
   *   Perform a spatial query using the SDE listener the specified parameters.
   * The objects found are returned in a udt_StringArray.
   *-------------------------------------------------------------------------*/
  function PerformQuery (
    a_SearchLayerName           varchar2,
    a_SearchLayerColumnName     varchar2,
    a_SearchValue               varchar2,
    a_ReturnLayerName           varchar2,
    a_ReturnLayerColumnName     varchar2
  ) return udt_StringList;

  /*---------------------------------------------------------------------------
   * ReturnQueryResults()
   *   Perform a spatial query using the SDE listener the specified parameters.
   * The objects found are returned in a udt_StringArray.
   *-------------------------------------------------------------------------*/
  Procedure ReturnQueryResultCount (
    a_SearchLayerName           varchar2,
    a_SearchLayerColumnName     varchar2,
    a_SearchValue               varchar2,
    a_ReturnLayerName           varchar2,
    a_ReturnLayerColumnName     varchar2,
    a_Count                     out number
  );

  /*---------------------------------------------------------------------------
   * SetPipeName()
   *   Set the pipe name to which SDE Listener requests will be sent.
   *-------------------------------------------------------------------------*/
  procedure SetPipeName (
    a_PipeName				varchar2
  );

  /*---------------------------------------------------------------------------
   * SetTimeOut()
   *   Set the time in seconds to wait for a response from the SDE Listener.
   *-------------------------------------------------------------------------*/
  procedure SetTimeOut (
    a_TimeOut				number
  );

  /*---------------------------------------------------------------------------
   * Shutdown()
   *   Shutdowns down the SDE Listener.
   *-------------------------------------------------------------------------*/
  procedure Shutdown ;

end;

 
/

