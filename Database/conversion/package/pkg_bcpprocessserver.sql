create or replace package            pkg_BCPProcessServer is

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * ExpirePowerGas() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ExpirePowerGas (
    a_LetterProcessType varchar2,
    a_AsOfDate          date
  );

  /*---------------------------------------------------------------------------
   * ExpirePermit() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ExpirePermit (
    a_ProcessTypeViewName   varchar2,
    --a_ExpiredJobStatus      varchar2,
    a_ExpiredProcessOutcome varchar2,
    a_AsOfDate              date
  );
end pkg_BCPProcessServer;



 

/

create or replace package body            pkg_BCPProcessServer is

  /*---------------------------------------------------------------------------
   * ExpirePowerGas() -- PUBLIC
   *-------------------------------------------------------------------------*/

  procedure ExpirePowerGas (
    a_LetterProcessType varchar2,
    a_AsOfDate          date
  ) is
  t_ProcessTypeId       udt_Id;
  t_ProcessId           udt_Id;
  t_Count               number;
  t_RelId               udt_Id;
  begin
    api.pkg_LogicalTransactionUpdate.StartTransaction;

    t_ProcessTypeId := api.pkg_ObjectDefQuery.IdForName(a_LetterProcessType);
    if t_ProcessTypeId is null then
      raise_application_error(-20000, 'Invalid process type specified: ' || a_LetterProcessType);
    end if;

    -- Check the expiration date AND check if we already sent a letter
    for c in (select pg.JobId,
                     pg.ProcessId
                from query.p_IssueConditionalPowerGas pg
               where pg.ExpirationDate < a_AsOfDate
                 and pg.Outcome is not null
                 and not exists (select 1
                                   from query.r_ICPGNotifyOfExpiration REL
                                  where REL.IssueConditionalProcessId = pg.ProcessId)) loop

      t_ProcessId := api.pkg_ProcessUpdate.New(c.JobId, t_ProcessTypeId, null, null, null, null);
      t_RelId := bcpdata.pkg_Utils.RelationshipNew(c.ProcessId, t_ProcessId);
    end loop;

    api.pkg_LogicalTransactionUpdate.EndTransaction;
    commit;
  end;

  /*---------------------------------------------------------------------------
   * ExpirePermit() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ExpirePermit (
    a_ProcessTypeViewName   varchar2,
    a_ExpiredProcessOutcome varchar2,
    a_AsOfDate              date
  ) is
    t_ProcessId               udt_Id;
    t_ProcessTypeId           udt_Id := toolbox.pkg_Util.GetIdForName(a_ProcessTypeViewName);
    t_TempPermitNumber        varchar2(4000);
    t_Count                   number;
  begin
    api.pkg_LogicalTransactionUpdate.ResetTransaction;

    if t_ProcessTypeId is null then
      raise_application_error(-20000, 'Invalid process type specified: ' || a_ProcessTypeViewName);
    end if;
    
    for c in (select JobId,
                     ExternalFileNum
                from api.Jobs jo
               where trunc(api.pkg_ColumnQuery.DateValue(JobId, 'ExpirationDate')) < trunc(sysdate)
                 and JobStatus in('ISSUED', 'STOP WORK')) loop
      
      -- Check for open processes
      select count(1)
        into t_Count
                from api.Jobs jo,
                     api.Processes pr,
                     api.ProcessTypes pt
               where jo.JobID = c.JobID
                 and pr.JobID = jo.JobID
                 and pr.DateCompleted is null
                 and pt.ProcessTypeID = pr.ProcessTypeID
                 and not exists (select 1
                                   from api.ProcessOutcomes po,
                                        api.StatusChanges sc
                                  where po.ProcessTypeId = pr.ProcessTypeID
                                    and po.JobTypeID = jo.JobTypeID
                                    and sc.ProcessOutcomeID = po.ProcessOutcomeID
                                    and (sc.FromStatusID = jo.StatusID or sc.FromStatusID is null))
                 and exists (select 1  -- Ensure that there is at least one outcome.  If there are no outcomes, process is valid in any status.
                               from api.ProcessOutcomes po
                              where po.ProcessTypeID = pr.ProcessTypeID
                                and po.JobTypeID = jo.JobTypeID);
      
      if (t_Count > 0) then
        api.pkg_Errors.RaiseError(-20000, ' Permit Number: ' || t_TempPermitNumber);
      end if;
      
      t_TempPermitNumber := c.ExternalFileNum;
      t_ProcessId := api.Pkg_ProcessUpdate.New(c.JobId, t_ProcessTypeId, 'Expire Permit', null, null, null);
      api.pkg_ProcessUpdate.Complete(t_ProcessId, a_ExpiredProcessOutcome);
      dbms_output.put_line(c.JobId);
      api.pkg_LogicalTransactionUpdate.EndTransaction;
    end loop;
    
    
    --commit;
    
  end;
end pkg_BCPProcessServer;



/

