create table pbcatcol (
  pbc_tnam                        varchar2(30) not null,
  pbc_tid                         integer,
  pbc_ownr                        varchar2(30) not null,
  pbc_cnam                        varchar2(30) not null,
  pbc_cid                         integer,
  pbc_labl                        varchar2(254),
  pbc_lpos                        integer,
  pbc_hdr                         varchar2(254),
  pbc_hpos                        integer,
  pbc_jtfy                        integer,
  pbc_mask                        varchar2(31),
  pbc_case                        integer,
  pbc_hght                        integer,
  pbc_wdth                        integer,
  pbc_ptrn                        varchar2(31),
  pbc_bmap                        char(1),
  pbc_init                        varchar2(254),
  pbc_cmnt                        varchar2(254),
  pbc_edit                        varchar2(31),
  pbc_tag                         varchar2(254)
) tablespace system;

grant delete
on pbcatcol
to public;

grant insert
on pbcatcol
to public;

grant select
on pbcatcol
to public;

grant update
on pbcatcol
to public;

create unique index pbsyscatcoldict_idx
on pbcatcol (
  pbc_tnam,
  pbc_ownr,
  pbc_cnam
) tablespace system;

