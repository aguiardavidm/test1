create table pbcatfmt (
  pbf_name                        varchar2(30),
  pbf_frmt                        varchar2(254),
  pbf_type                        integer not null,
  pbf_cntr                        integer
) tablespace system;

grant delete
on pbcatfmt
to public;

grant insert
on pbcatfmt
to public;

grant select
on pbcatfmt
to public;

grant update
on pbcatfmt
to public;

create unique index pbsyscatfrmts_idx
on pbcatfmt (
  pbf_name
) tablespace system;

