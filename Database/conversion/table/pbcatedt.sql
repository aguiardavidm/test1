create table pbcatedt (
  pbe_name                        varchar2(30),
  pbe_edit                        varchar2(254),
  pbe_type                        integer,
  pbe_cntr                        integer,
  pbe_seqn                        integer,
  pbe_flag                        integer,
  pbe_work                        varchar2(32)
) tablespace system;

grant delete
on pbcatedt
to public;

grant insert
on pbcatedt
to public;

grant select
on pbcatedt
to public;

grant update
on pbcatedt
to public;

create unique index pbsyspbe_idx
on pbcatedt (
  pbe_name,
  pbe_seqn
) tablespace system;

