create table pbcatvld (
  pbv_name                        varchar2(30),
  pbv_vald                        varchar2(254),
  pbv_type                        integer,
  pbv_cntr                        integer,
  pbv_msg                         varchar2(254)
) tablespace system;

grant delete
on pbcatvld
to public;

grant insert
on pbcatvld
to public;

grant select
on pbcatvld
to public;

grant update
on pbcatvld
to public;

create unique index pbsyscatvlds_idx
on pbcatvld (
  pbv_name
) tablespace system;

