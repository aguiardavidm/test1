create table pbcattbl (
  pbt_tnam                        varchar2(30) not null,
  pbt_tid                         integer,
  pbt_ownr                        varchar2(30) not null,
  pbd_fhgt                        integer,
  pbd_fwgt                        integer,
  pbd_fitl                        char(1),
  pbd_funl                        char(1),
  pbd_fchr                        integer,
  pbd_fptc                        integer,
  pbd_ffce                        varchar2(18),
  pbh_fhgt                        integer,
  pbh_fwgt                        integer,
  pbh_fitl                        char(1),
  pbh_funl                        char(1),
  pbh_fchr                        integer,
  pbh_fptc                        integer,
  pbh_ffce                        varchar2(18),
  pbl_fhgt                        integer,
  pbl_fwgt                        integer,
  pbl_fitl                        char(1),
  pbl_funl                        char(1),
  pbl_fchr                        integer,
  pbl_fptc                        integer,
  pbl_ffce                        varchar2(18),
  pbt_cmnt                        varchar2(254)
) tablespace system;

grant delete
on pbcattbl
to public;

grant insert
on pbcattbl
to public;

grant select
on pbcattbl
to public;

grant update
on pbcattbl
to public;

create unique index pbsyscatpbt_idx
on pbcattbl (
  pbt_tnam,
  pbt_ownr
) tablespace system;

