create table streets_cooked (
  fullstreetname                  varchar2(100),
  streettype                      varchar2(100),
  streetname                      varchar2(100),
  length                          number
) tablespace system;

