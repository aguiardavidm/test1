create or replace package abc.pkg_ABC_Users as

  /*---------------------------------------------------------------------------
  * DESCRIPTION
  *   Routines used to manage internet users who do not have Oracle Logons.
  * Two user constants are defined. The first "NumberOfTries" defines how
  * many unsuccessful logon attempts are permitted before the user is locked
  * out. The second "TimeoutLength" defines the number of minutes the user
  * will be locked out for. If this constant is missing, a day is assumed.
  *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
  * Types
  *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*--------------------------------------------------------------------------
  * ActivateExt()
  * Activate a new external user
  * UMS - External
  *------------------------------------------------------------------------*/
  procedure ActivateExt(
    a_User        udt_Id,
    a_OracleLogon varchar2,
    a_Password    varchar2
  );

  /*--------------------------------------------------------------------------
  * Activate()
  * Activate a new internal user
  * UMS - Internal
  *------------------------------------------------------------------------*/
  procedure Activate(
    a_User        udt_Id,
    a_OracleLogon varchar2,
    a_Password    varchar2
  );

  /*---------------------------------------------------------------------------
  * Inactivate()
  *   Inactivate the internal user so that they can no longer log on.
  * UMS - Internal
  *-------------------------------------------------------------------------*/
  procedure Inactivate(
    a_User udt_Id,
    a_OracleLogon varchar2
  );

  /*---------------------------------------------------------------------------
  * InactivateExt()
  *   Inactivate the external user so that they can no longer log on.
  * UMS - External
  *-------------------------------------------------------------------------*/
  procedure InactivateExt(
    a_User udt_Id,
    a_OracleLogon varchar2
  );

  /*--------------------------------------------------------------------------
  * AuthenticateExt()
  * AuthenticateExt a new external user
  * UMS - External
  *------------------------------------------------------------------------*/
  procedure AuthenticateExt(
    a_User          udt_Id,
    a_OracleLogon   varchar2,
    a_Password      varchar2,
    a_ConnectString varchar2
  );

  /*---------------------------------------------------------------------------
  * IntAuthenticate()
  *   Authenticate internal users.
  * UMS - Internal
  *-------------------------------------------------------------------------*/
  procedure IntAuthenticate(
    a_User          udt_Id,
    a_OracleLogon   varchar2,
    a_Password      varchar2,
    a_ConnectString varchar2
  );

  /*---------------------------------------------------------------------------
  * ChangePassword()
  *   Change the user's password.
  * UMS - External
  *-------------------------------------------------------------------------*/
  procedure ChangePassword(
    a_User        udt_Id,
    a_OracleLogon varchar2,
    a_Password    varchar2
  );

  /*--------------------------------------------------------------------------
  * AddAcess() - Add an access group to a user
  * Run on Constructor of the relationship between User and PosseAccessGroups
  *------------------------------------------------------------------------*/
  procedure AddAccess(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * RemoveAccess() - Add an access group to a user
  * Run on Destructor of the relationship between User and PosseAccessGroups
  *------------------------------------------------------------------------*/
  procedure RemoveAccess(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * ArchAccess() - Add/Remove Approved Archaeologist Access Group via checkbox
  * Run on Column Change of boolean Archaeologist column on Users
  *------------------------------------------------------------------------*/
  procedure ArchAccess(
    a_StaffId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * SendRegistrationEmail()
  * Send a registration email to a new external user from registration page
  * Run on Column Change of boolean SendRegistrationEmail column on
  * j_Registration.
  * Column is set via action on button on external New presentation
  *------------------------------------------------------------------------*/
  procedure SendRegistrationEmail(
    a_RegistrationId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * SendPasswordResetEmail()
  * Run on Column Change of boolean SendPasswordResetEmail column on
  * j_Registration.
  *------------------------------------------------------------------------*/
  procedure SendPasswordResetEmail(
    a_RegistrationId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * CreateExtUser()
  * Create a new external user
  * Called from POST VERIFY of o_REGISTRATION object.
  *------------------------------------------------------------------------*/
  procedure CreateExtUser(
    a_RegistrationId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * NewIntUser()
  * Fill in some details on a new internal user
  * Run on Column Change of boolean CreateIntUser column on o_Registration
  * Column is set via action on CreateUser button on internal New presentation
  * on o_Registration.
  *------------------------------------------------------------------------*/
  procedure NewIntUser(
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * NewReferralUser() -- PUBLIC
  * Fill in some details on a new Referral user
  *------------------------------------------------------------------------*/
  /*  procedure NewReferralUser (
      a_UserId          udt_Id,
      a_AsOfDate          date
    );
  */ /*--------------------------------------------------------------------------
   * SetPwdEncryption()
   * Set a random value to use for authorization
   * Run from column change on PasswordWeb column on o_Registration object
   *------------------------------------------------------------------------*/
  procedure SetPwdEncryption(
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*---------------------------------------------------------------------------
   * AdminChangePassword() -- PUBLIC
   * ABC Admin Site - User Maintainance
   * Used by ABC Admin Users to manually reset a Public or Internal User Password.
   * Called from Column Change Procedure on boolean ResetPassword column on
   * u_users.  ResetPassword is set via action on Reset Password button on
   * User presentation.
   *-------------------------------------------------------------------------*/
  procedure AdminChangePassword (
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * ChangeExtPwd()
  * Change the external user's password
  * Called from Column Change procedure on boolean ChangePwd column on user
  * object.  Column is set via action on Change Password button on external
  * user Change Password presentation.
  *
  * The password can be any combination of letters, numbers and special
  * characters up to 30 characters long, and is case sensitive.
  * Passwords must meet the following restrictions:
  * - must be at least eight (8) characters long and may not include any part
  *   of your User ID
  * - must include at least one Upper case letter (A, B, C, ...)
  * - must include at least one lower case letter (a, b, c, ...)
  * - must include at least one number (1, 2, 3, ...)
  *------------------------------------------------------------------------*/
  procedure ChangeExtPwd(
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * FindUserId()
  * Find a User record from a string and create a relationship to it
  * Used for external reset password screen
  * Called from Column Change procedure on column SetUserRelationship
  * on o_Registration.
  * SetUserRelationship is set via an action on the Next button on the
  * external Reset presentation.
  * The user types in their userid and we try to find a user to match it.
  *------------------------------------------------------------------------*/
  procedure FindUserId(
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * OSUserSearch()
  * Used for internal user login
  *------------------------------------------------------------------------*/
  procedure OSUserSearch(
    a_OSUserId varchar2,
    a_Objects  out api.udt_objectList
  );

  /*--------------------------------------------------------------------------
  * ResetPassword()
  * Change the external user's password
  * Used for external reset password screen
  * Called from Column Change Procedure on boolean ResetPassword column on
  * o_Registration.  Column is set via action on Change Password button on
  * ChangePassword presentation.
  *------------------------------------------------------------------------*/
  procedure ResetPassword(
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * DocUploadLimit()
  * Limits the total size of documents that can be submitted by a user per day.
  * Run from Post Verify on Application Documents
  *------------------------------------------------------------------------*/
  procedure DocUploadLimit(
    a_DocId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * AppUploadLimit()
  * Limits the number of applications that can be submitted by a user per day.
  * Run from Constructor on all the main job types
  *------------------------------------------------------------------------*/
  procedure AppUploadLimit(
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * TransferAssignments()
  * Transfers all job roles and/or process assignments to another user
  * Run from PostVerify on Users
  * Drives logic on the internal Reassign presentation
  *------------------------------------------------------------------------*/
  procedure TransferAssignments(
    a_UserId udt_Id,
    a_AsOfDate date
  );

  function Encrypt(
    a_Password varchar2
  ) return varchar2;

  /*--------------------------------------------------------------------------
  * AccessGroupBasedOnCheckbox() -- PUBLIC
  * This procedure will put in and take out a single user from access groups
  * based on checkboxes on the user object that have the same name as the
  * corresponding access group.  This allows for users to check and un-check
  * checkboxes and then the user will be put in or taken out of access groups
  *------------------------------------------------------------------------*/
  procedure AccessGroupBasedOnCheckboxes(
    a_UserId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
   * CreateInternalUser() -- PUBLIC
   * This procedure will create an Internal user.  It is triggered from the
   * o_ABC_PosseUserCreation object.
   *
   * NOTE: in order for this to work, the extension user needs to have ALTER USER privs
   *------------------------------------------------------------------------*/
  procedure CreateInternalUser (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  );

  /*--------------------------------------------------------------------------
   * CreateMunicipalUser() -- PUBLIC
   * This procedure will create a Municipal user.  It is triggered from the
   * o_ABC_PosseUserCreation object.
   *
   * NOTE: extension user needs ALTER USER privs
   *------------------------------------------------------------------------*/
  procedure CreateMunicipalUser (
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  );

  procedure ValidateEmailForNotif (
    a_ObjectID                 udt_Id,
    a_AsAtDate                 date,
    a_UserId                   udt_Id default null
  );


  /*---------------------------------------------------------------------------
   * TrackInvalidAttempts()
   *   Wrapper for tracking invalid login attempts
   *-------------------------------------------------------------------------*/
  procedure TrackInvalidAttempts (
    a_User            udt_Id,
    a_OracleLogon     varchar2,
    a_Password        varchar2,
    a_ConnectString   varchar2
  );

   /*---------------------------------------------------------------------------
   * UnlockAccount()
   *   Used to unlock a user's account that has been locked due to invalid
   *   access attempts
   *-------------------------------------------------------------------------*/
  procedure UnlockAccount (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) ;

   /*---------------------------------------------------------------------------
   * TrackLastSuccessfulLogin()
   *   Update the user's record in abc.internetUsers with successful login date
   *-------------------------------------------------------------------------*/
  procedure TrackLastSuccessfulLogin (
    a_User            udt_Id,
    a_AsOfDate          date
  );

   /*---------------------------------------------------------------------------
   * UpdateAuditDate()
   *   Update the date when a new object is constructed
   *-------------------------------------------------------------------------*/
  procedure UpdateAuditDate (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  );

  /*---------------------------------------------------------------------------
   * CreateLockAuditLogs()
   *   Used to create audit logs for any accounts that were locked.
   *-------------------------------------------------------------------------*/
  procedure CreateLockAuditLogs;

   /*---------------------------------------------------------------------------
   * RemOutstandingFeeLegalEntity()
   *   Removes Outstanding Fee LegalEntity - User relationship if it exists
   *-------------------------------------------------------------------------*/
  procedure RemOutstandingFeeLegalEntity (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  );

   /*---------------------------------------------------------------------------
   * RemoveLegalEntity()
   *   Removes LegalEntity - Public User relationship if selected
   *-------------------------------------------------------------------------*/
  procedure RemoveLegalEntity (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  );

  /*--------------------------------------------------------------------------
  * CreatePoliceUser()
  * Create a new Police user
  * It is triggered from the o_ABC_PosseUserCreation object.
  *
  * NOTE: extension user needs ALTER USER privs
  *------------------------------------------------------------------------*/
  procedure CreatePoliceUser(
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  );

   /*---------------------------------------------------------------------------
   * RemoveMunicipality()
   *   Removes Municipality - Police User relationship if selected
   *-------------------------------------------------------------------------*/
  procedure RemoveMunicipality (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  );

   /*---------------------------------------------------------------------------
   * ValidateUserEmail()
   *   Deactivates Municipal User if Police User with same email is created.
   *-------------------------------------------------------------------------*/
  procedure ValidateUserEmail (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  );

end pkg_ABC_Users;
/

grant execute on abc.pkg_abc_users to posseextensions;

create or replace package body abc.pkg_ABC_Users as

  /*---------------------------------------------------------------------------
   * Globals
   *-------------------------------------------------------------------------*/
  g_EncryptionKey      constant varchar2(8) := 'X4%s-<E3';
  gc_Newline                            constant char(2) := chr(13)||chr(10);

  /*---------------------------------------------------------------------------
   * ValidatePwd() -- PRIVATE
   *   ValidatePwd the encrypted password.
   *-------------------------------------------------------------------------*/
  procedure ValidatePwd (
    a_Password        varchar2
  ) is
    t_asc                               pls_integer;
    t_hasCap                            boolean;
    t_hasLowerCase                      boolean;
    t_hasNumber                         boolean;
--    t_hasLetter                         boolean;
    t_hasSpecialCharacter               boolean;
  begin
    -- Check null
    if a_Password is null then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
      api.pkg_Errors.RaiseError(-20000, 'User must be given a password.');
    end if;
    -- Check min length
    if length(a_Password) < 8 then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
      api.pkg_errors.raiseError(-20000,
                       'Password must be at least 8 characters long.');
    end if;

    -- Check for mandatory upper, lower, number
    t_hasCap := False;
    t_hasLowerCase := False;
    t_hasNumber := False;
--    t_hasLetter := False;
    t_hasSpecialCharacter := False;
    for x in 1..length(a_Password) loop
      t_asc := ascii(substr(a_Password, x, 1));
      if t_hasCap = False and t_asc >= 65 and t_asc <= 90 then
        t_hasCap := True;
      end if;
      if t_hasLowerCase = False and t_asc >= 97 and t_asc <= 122 then
        t_hasLowerCase := True;
      end if;
      if t_hasNumber = False and t_asc >= 48 and t_asc <= 57 then
        t_hasNumber := True;
      end if;
/*      if t_hasLetter = False and ((t_asc >= 65 and t_asc <= 90) or (t_asc >= 97 and t_asc <= 122))  then
        t_hasLetter := True;
      end if;*/
      -- Check for Special Character. Allowed ! # @ $ ^ + \ [ ] ~ - /
      if t_hasSpecialCharacter = False and (t_asc in (33, 35, 64, 36, 94, 43, 92, 91, 93, 126, 45, 47)) then
        t_hasSpecialCharacter := True;
      end if;
    end loop;

/*    if t_hasLetter = False then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
      api.pkg_errors.raiseError(-20000,
                       'Password must have at least one letter.');*/
    if t_hasCap = False then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
      api.pkg_errors.raiseError(-20000,
                       'Password must have at least one capital letter.');
    elsif t_hasLowerCase = False then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
      api.pkg_errors.raiseError(-20000,
                       'Password must have at least one lower case letter.');
    elsif t_hasNumber = False then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
      api.pkg_errors.raiseError(-20000,
                       'Password must have at least one number.');
    elsif t_hasSpecialCharacter = False then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
      api.pkg_errors.raiseError(-20000,
                       'Password must have at least one special character.');
    end if;
  end;

  /*---------------------------------------------------------------------------
   * Encrypt() -- PRIVATE
   *   Return the encrypted password.
   *-------------------------------------------------------------------------*/
  function Encrypt (
    a_Password        varchar2
  ) return varchar2 is
    t_ClearPassword      varchar2(4000);
    t_EncryptedPassword      varchar2(4000);
    t_PadLength        number;
    e_doubleEncryption       exception;
    pragma exception_init(e_doubleEncryption, -28233);
  begin
    /* encrypt the password (which must exist), after padding to a multiple of
       8 bytes */
    t_PadLength := 8 - mod(length(a_Password), 8);
    if t_PadLength < 8 then
      t_ClearPassword := rpad(a_Password, length(a_Password) + t_PadLength);
    else
      t_ClearPassword := a_Password;
    end if;

    begin
      dbms_obfuscation_toolkit.DESEncrypt(input_string => t_ClearPassword,
          key_string => g_EncryptionKey,
          encrypted_string => t_EncryptedPassword);
    exception when e_doubleEncryption then
      t_EncryptedPassword := null;
    end;

    return t_EncryptedPassword;
  end;

  /*---------------------------------------------------------------------------
   * ActivateExt() -- PUBLIC
   * UMS - External
   *-------------------------------------------------------------------------*/
  procedure ActivateExt (
    a_User        udt_Id,
    a_OracleLogon      varchar2,
    a_Password        varchar2
  ) is
  begin
    null;
  end;

  /*---------------------------------------------------------------------------
   * Activate() -- PUBLIC
   * UMS - Internal
   *-------------------------------------------------------------------------*/
  procedure Activate (
    a_User        udt_Id,
    a_OracleLogon      varchar2,
    a_Password        varchar2
  ) is
  begin
    null;
  end;

  /*---------------------------------------------------------------------------
   * Inactivate() -- PUBLIC
   *   Inactivate the internal user so that they can no longer log on.
   * UMS - Internal
   *-------------------------------------------------------------------------*/
  procedure Inactivate (
    a_User        udt_Id,
    a_OracleLogon      varchar2
  ) is
    t_roleCount         pls_integer;
    t_assignedCount     pls_integer;
  begin
    -- Fail if no other users are assigned to a system role
    select count(*) into t_roleCount
      from api.relationships r
      join api.relationshipdefs rd on r.endpointid = rd.toendpointid
      join api.relationshipnames rn
        on rd.fromrelationshipnameid = rn.relationshipnameid
     where r.fromobjectid = a_user
       and rn.relationshipname in ( 'PC Owner:', 'Planner:', 'Archaeologist:',
           'Palaeontologist:', 'Permit Coordinator:', 'Head Archaeologist:',
           'Verifier:', 'Reviewer:');
    if t_roleCount > 0 then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'Inactivate');
      api.pkg_errors.raiseError(-20000,
          'User cannot be inactivated while assigned a role on a job.');
    end if;

    -- Fail if user has been assigned incomplete processes
    select count(*) into t_assignedCount from api.incompleteprocessassignments
     where assignedto = a_user;
    if t_assignedCount > 0 then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'Inactivate');
      api.pkg_errors.raiseError(-20000,
          'User cannot be inactivated with active assigned tasks.');
    end if;

    -- Fail if user has not had access groups removed
    select count(*) into t_roleCount
      from api.accessgroups ag
      join api.accessgroupusers agu on ag.accessgroupid = agu.accessgroupid
     where agu.userid = a_user and ag.description <> 'Public';
    if t_roleCount > 0 then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'Inactivate');
      api.pkg_errors.raiseError(-20000,
          'User cannot be inactivated with access groups assigned.');
    end if;

    /*
    -- Removed because ...
    -- The delete of the relationship fails saying the relid cannot be found
    -- So, this will need to be done as a manual step before deactivating
    -- delete access group object rels and remove access groups (except public)
    t_UserDef := api.pkg_configquery.ObjectDefIdForName('Users');
    t_accessGrEndPt := api.pkg_configquery.EndPointIdForName(t_UserDef,
                           'AccessGroups');
    for x in (select relationshipid from api.relationships
               where fromobjectid = a_user
                 and endpointid = t_accessGrEndPt) loop
      api.pkg_relationshipupdate.remove(x.relationshipid);
    end loop;
    for x in (select agu.accessgroupid
                from api.accessgroupusers agu
                join api.accessgroups ag on agu.accessgroupid = ag.accessgroupid
               where agu.userid = a_user and ag.description <> 'Public') loop
      api.pkg_userupdate.removeFromAccessGroup(a_user, x.accessgroupid);
    end loop;
    */
  end;

  /*---------------------------------------------------------------------------
   * InactivateExt() -- PUBLIC
   *   Inactivate the external user so that they can no longer log on.
   * UMS - External
   *-------------------------------------------------------------------------*/
  procedure InactivateExt (
    a_User        udt_Id,
    a_OracleLogon      varchar2
  ) is
  begin
    /* Commented code. Since the external users don't have an Oracle userid,
     * the only way that the sytem can be accessed is through Outrider.
     * Outrider will not allow a connection with an inactive user account.

    -- Remove from list of approved archaeologists
    for x in (select agu.accessgroupid
                from api.accessgroupusers agu
                join api.accessgroups ag on agu.accessgroupid = ag.accessgroupid
               where agu.userid = a_user
                 and ag.description = 'Archaeology External') loop
      api.pkg_userupdate.removeFromAccessGroup(a_user, x.accessgroupid);
    end loop;
    */
    null;
  end;

  /*---------------------------------------------------------------------------
   * PwdFail() -- PRIVATE Autonomous
   * Updates user record in a seperate transaction while throwing an error
   * in the primary transaction.
   *-------------------------------------------------------------------------*/
  procedure PwdFail (
    a_User        udt_Id
  ) is pragma autonomous_transaction;
    t_appObj                            udt_Id;
    t_tries        number(2);
    t_maxRetries                        number(4);
    t_LockoutHours                      number(4);
    t_LockoutTime                       date;
    t_LoginAuditDefId                   udt_Id;
    t_LoginAuditId                      udt_Id;
    t_User                              varchar2(400);
    t_UserType                          varchar2(100);
  begin
    pkg_debug.putline('#####: PwdFail at: ' || to_char(systimestamp, 'HH24:MI:SS:FF6'));
    -- Get password policy info
/*    t_appObj := api.pkg_SimpleSearch.ObjectByIndex(
                     'o_ApplicationInformation',
                     'WebSiteName',
                     'DemoExt' );*/
    t_maxRetries := 5; --api.pkg_columnquery.numericValue(t_appObj, 'MaxPwdRetries');
    select ss.HoursToUnlockAccount into t_LockoutHours from query.o_systemsettings ss;


    if t_LockoutHours is null then
      t_LockoutHours := 5; --api.pkg_columnquery.numericValue(t_appObj,
    end if;                --    'PwdLockoutTime');

    -- Get user failed logon info
    begin
      select Retries, LockoutDate into t_tries, t_LockoutTime
        from internetUsers
       where userId = a_user;
       --if user does not exist, insert into table
       exception
         when no_data_found then
             insert into internetUsers (userId, retries, lockoutDate) values (a_user, 0, null);
    end;

    select i.Retries, i.LockoutDate, (case when u.UserType = 'Public' then
                                          u.EmailAddress
                                      else u.OracleLogonId
                                      end), u.UserType
    into t_tries, t_LockoutTime, t_User, t_UserType
    from   internetUsers i
    join   query.u_users u on u.UserId = i.userid
    where  i.userId = a_user;

    -- Reset timeout if expired
    if t_LockoutTime is not null and t_LockoutTime < sysdate then
      t_tries := 0;
      update internetUsers
         set Retries = 0, LockoutDate = null
       where userid = a_user;
      if sql%rowcount = 0 then
        insert into internetUsers (userId, retries, lockoutDate) values
          (a_user, 0, null);
      end if;

    else -- set lockout if max retries exceeded
      if t_tries >= t_maxRetries and t_LockoutTime is null then

    INSERT INTO abc.loginaudit
        (Event, EventStatus, AuditDate, Account, Site)
        VALUES
        ('Invalid Login', 'Account is locked.', sysdate, t_User, t_UserType);

        update internetUsers
           set lockoutDate = sysdate + 1/24*t_LockoutHours
         where userid = a_user;
        if sql%rowcount = 0 then
          insert into internetUsers (userId, retries, lockoutDate) values
            (a_user, 0, sysdate + 1/24*t_LockoutHours);
        end if;
      end if;
    end if;

    -- Increment fail counter
    update internetUsers set retries = t_tries + 1 where userid = a_user;
    if sql%rowcount = 0 then
      insert into internetUsers (userId, retries) values (a_user, 0);
    end if;

    commit;
  end PwdFail;

  /*---------------------------------------------------------------------------
  * AuthenticateCheck() -- PRIVATE
  * Ensures that a session request comes from a valid COM server and that the
  * COM is running with the expected user credentials and that the COM process
  * has the expected name.
  *-------------------------------------------------------------------------*/
  procedure AuthenticateCheck (
    a_OracleLogon      varchar2,
    a_System          varchar2
    ) is
    t_count                             pls_integer;
    t_User                              v$session.Username%type;
    t_OSUser                            v$session.OSUser%type;
    t_Program                           v$session.Program%type;
    t_Host                              v$session.Machine%type;
    t_Sid                               v$session.sid%type;
  begin
    t_Sid := sys_context('userenv', 'SESSIONID');

    select UserName, OSUser, Program, Machine
      into t_User, t_OSUser, t_Program, t_Host
      from v$Session
     where audsid = t_Sid;

    begin
      select count(*) into t_count
        from systemConstants
       where lower(system) = a_System
             --machine name of physical machine running the com is as expected
         and lower(host) = lower(t_Host)
             --user is outrider's user (usu. OUTRIDERSYS)
         and upper(UserName) = upper(t_User)
             --os user is com + definitions 'running as' os user
         and lower(OSUser) = lower(t_OSUser)
             --process making call is dllhost.exe
         and lower(t_Program) = 'dllhost.exe';
    exception when no_data_found then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'Check Host');
      api.pkg_Errors.SetArgValue('OracleLogonId', a_OracleLogon);
      api.pkg_Errors.RaiseError(-20000, 'Invalid host');
    end;
  end;

  /*---------------------------------------------------------------------------
  * AuthenticateExt() -- PUBLIC
  * UMS - External
  *-------------------------------------------------------------------------*/
  procedure AuthenticateExt (
    a_User        udt_Id,
    a_OracleLogon      varchar2,
    a_Password        varchar2,
    a_ConnectString      varchar2
    ) is
    t_EncryptedPassword      varchar2(4000);
    t_Error        varchar2(4000);
    t_Message        varchar2(4000);
    t_Active        varchar2(1);
    t_LockoutTime                       date;
  begin
pkg_debug.putline('#####: AuthenticateExt at: ' || to_char(systimestamp, 'HH24:MI:SS:FF6'));
    AuthenticateCheck(a_OracleLogon, 'external');
--raise_Application_error(-20000, a_OracleLogon || ' ' || a_User);
    select min(LockoutDate) into t_LockoutTime from internetUsers
     where userId = a_user;
    if t_LockoutTime > sysdate then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'Check Timeout');
      api.pkg_Errors.SetArgValue('OracleLogonId', a_OracleLogon);
      api.pkg_Errors.SetArgValue('Timeout',to_char(t_LockoutTime,'hh24:mi:ss'));
      api.pkg_Errors.RaiseError(-20000,
          'Too many failures:  You may not attempt to log on again until '||
           to_char(t_LockoutTime, 'hh:mi:ss am')||'.');
    end if;

    -- Get the stored encrypted password
    select active, api.pkg_columnquery.value(userid, 'PasswordWeb')
      into t_active, t_EncryptedPassword
      from api.users where userid = a_User;

    -- Test for active
    if t_active = 'N' then
      raise no_data_found;
    end if;

    if a_OracleLogon <> 'WEBGUEST' then
      -- Test for nulls:  this should never happen
      if t_EncryptedPassword is null or a_Password is null then
--        raise no_data_found;
api.pkg_errors.raiseerror(-20000, 'TEST ERROR  Encrypted Pwd and Pwd:' || t_EncryptedPassword || ' and ' || a_Password || '  Active: ' || t_Active);
      end if;

      -- Test password
      if t_EncryptedPassword <> Encrypt(a_Password) then
        -- increment failed password count
        PwdFail(a_user);
        raise no_data_found;
      end if;

      -- Login success
      update internetUsers
         set Retries = 0, LockoutDate = null
       where userid = a_user;
      if sql%rowcount = 0 then
        insert into internetUsers (userId, retries, lockoutDate) values
          (a_user, 0, null);
      end if;

    --Update last Successful Login
      TrackLastSuccessfulLogin(a_User, sysdate);

      commit;
    end if;
    pkg_debug.putline('External Login succeeded');
  exception when no_data_found then
    pkg_debug.putline('External Login failed');
    t_Error := sqlerrm;
    t_Message := 'Authentication failed';
    api.pkg_Errors.Clear();
    api.pkg_Errors.SetArgValue('OriginalError', t_Error);
    api.pkg_Errors.SetArgValue('Operation', 'Authenticate');
    api.pkg_Errors.SetArgValue('OracleLogonId', a_OracleLogon);
    api.pkg_Errors.RaiseError(-20000, t_Message);
  end;

  /*---------------------------------------------------------------------------
   * IntAuthenticate() -- PUBLIC
   * UMS - Internal
   *-------------------------------------------------------------------------*/
  procedure IntAuthenticate (
    a_User        udt_Id,
    a_OracleLogon      varchar2,
    a_Password        varchar2,
    a_ConnectString      varchar2
  ) is
  begin
    -- Integrated Login
    -- If the OsUserId matches, let them through
    -- The password here needs to match the one in NewSession call in Login.aspx
    -- This a just a secondary security check
    if a_Password <> 'opac' then
    api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
      api.pkg_errors.RaiseError(-20000, 'Invalid login');
    end if;

    AuthenticateCheck(a_OracleLogon, 'internal');
  end;

  /*---------------------------------------------------------------------------
   * ChangePassword() -- PUBLIC
   * UMS - External
   *-------------------------------------------------------------------------*/
  procedure ChangePassword (
    a_User        udt_Id,
    a_OracleLogon      varchar2,
    a_Password        varchar2
  ) is
    t_EncryptedPassword      varchar2(4000);
  begin
    ValidatePwd(a_Password);
    t_EncryptedPassword := Encrypt(a_Password);
    api.pkg_columnUpdate.setValue(a_user, 'PasswordWeb', t_EncryptedPassword);

  end;

 /*--------------------------------------------------------------------------
  * AddAccess() -- PUBLIC
  * Run on Constructor of the relationship between User and PosseAccessGroups
  *------------------------------------------------------------------------*/
  procedure AddAccess (
    a_RelationshipId    udt_Id,
    a_AsOfDate          date
  ) is
    t_user              udt_Id;
    t_group             udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    select FromObjectId, ToObjectId into t_user, t_group
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;

    t_GroupId := api.pkg_columnquery.value(t_group, 'AccessGroupId');
    select count(*) into t_exists from api.accessGroupUsers
     where userId = t_user and accessGroupId = t_GroupId;
    if t_exists < 1 then
      api.pkg_UserUpdate.AddToAccessGroup(t_user, t_groupid);
    end if;
  end AddAccess;

 /*--------------------------------------------------------------------------
  * FindRelUserGrp() -- PRIVATE Autonomous
  * Used on destructor of a relationship to find what it was relating
  *------------------------------------------------------------------------*/
  function FindRelUserGrp(
    a_RelationshipId    udt_Id
  ) return varchar2 is
    t_ids               varchar2(20);
  pragma autonomous_transaction;
  begin
    select to_char(fromObjectId) || ',' ||
           toObjectId || ',' ||
           to_char(api.pkg_columnquery.value(toObjectId, 'AccessGroupId'))
      into t_ids
      from api.relationships r
      join api.users u on r.fromObjectId = u.userId
     where relationshipId = a_RelationshipId;
    return t_ids;
  exception when no_data_found then
    return null;
  end FindRelUserGrp;

 /*--------------------------------------------------------------------------
  * RemoveAccess() -- PUBLIC
  * Run on Destructor of the relationship between User and PosseAccessGroups
  *------------------------------------------------------------------------*/
  procedure RemoveAccess (
    a_RelationshipId    udt_Id,
    a_AsOfDate          date
  ) is
    t_user              udt_Id;
    t_groupId           udt_Id;
    t_groupObjId        udt_Id;
    t_count             pls_integer;
    t_ids               varchar2(20);
    t_group             varchar2(60);
    t_isRole            varchar2(1);
  begin
    t_ids := FindRelUserGrp(a_RelationshipId);
    if t_ids is not null and instr(t_ids, ',') > 0 then
      t_user       := to_number(substr(t_ids, 1, instr(t_ids, ',')-1 ));
      t_groupObjId := to_number(
                         substr(t_ids,
                                instr(t_ids, ',', 1, 1)+1,
                                instr(t_ids, ',', 1, 2) -
                                  instr(t_ids, ',', 1, 1) - 1
                               ));
      t_groupId    := to_number(substr(t_ids, instr(t_ids, ',', 1, 2)+1 ));
      t_isRole := api.pkg_columnquery.value(t_groupObjId, 'IsRole');
    end if;
    if t_user is not null and t_groupId is not null then
      select count(*) into t_count
        from api.accessGroupUsers agu
        join api.users u on agu.userid = u.userid
       where agu.accessGroupId = t_groupId
         and u.active = 'Y';
      select min(description) into t_group from api.accessGroups
       where accessGroupId = t_groupId;
      -- Do not allow a role to have noone assigned to it
      if t_group is not null then
        if t_count <= 1 and t_isRole = 'Y' then
          -- Raise error
          api.pkg_Errors.Clear();
          api.pkg_Errors.SetArgValue('Operation', 'Remove Access Group');
          api.pkg_errors.raiseError(-20000,
              'Please assign someone else to the role of '|| t_group||
              ' before removing it from this user.');
        else
          api.pkg_UserUpdate.RemoveFromAccessGroup(t_user, t_groupId);
        end if;
      end if;
    end if;
  end RemoveAccess;

 /*--------------------------------------------------------------------------
  * ArchAccess() -- PUBLIC
  * Add / Remove Security for External Archaeologists
  * Run on Column Change of boolean Archaeologist column on Users
  *------------------------------------------------------------------------*/
  procedure ArchAccess (
    a_StaffId           udt_Id,
    a_AsOfDate          date
  ) is
    t_isStaff           char(1);
    t_isArch            char(1);
    t_groupId           udt_Id;
    t_hasAccess         pls_integer;
  begin
    t_isStaff := api.pkg_columnquery.value(a_staffId, 'IsStaff');
    t_isArch := api.pkg_columnquery.value(a_staffId, 'Archaeologist');
    select accessgroupId into t_groupId
      from api.accessGroups
     where Description = 'Archaeology External';
    select count(*) into t_hasAccess
      from api.accessGroupUsers
     where userid = a_staffId and accessGroupId = t_groupId;

    if t_isStaff = 'N' and t_isArch = 'Y' and t_hasAccess < 1 then
      api.pkg_UserUpdate.AddToAccessGroup(a_staffId, t_groupId);
    elsif t_isStaff = 'N' and t_isArch = 'N' and t_hasAccess > 0 then
      api.pkg_UserUpdate.RemoveFromAccessGroup(a_staffId, t_groupId);
    elsif t_isStaff = 'Y' and t_isArch = 'Y' then
      api.pkg_errors.RaiseError(-20000,
          'Staff cannot be approved archaeologists.');
    end if;
  end ArchAccess;

  /*--------------------------------------------------------------------------
   * SendRegistrationEmail() -- PUBLIC
   * Send a registration email to a new external user from registration page
   * Run on Column Change of boolean SendRegistrationEmail column on
   * j_Registration and j_ER_ReferralUserRegistration
   * Column is set via action on button on external New presentation
   *------------------------------------------------------------------------*/
  procedure SendRegistrationEmail (
    a_RegistrationId    udt_Id,
    a_AsOfDate          date
  ) is
  begin
    extension.pkg_SendMail_old.SendPosseEmail(a_RegistrationId, sysdate, 'FromEmailAddress', 'EmailAddress',
      'CCEmailsTo', null, 'NewRegistrationEmailSubject', 'NewRegistrationEmailBody',
      null, null, 'N');
  end SendRegistrationEmail;

  /*--------------------------------------------------------------------------
   * SendPasswordResetEmail() -- PUBLIC
   * Run on Column Change of boolean SendPasswordResetEmail column on
   * j_Registration
   *------------------------------------------------------------------------*/
  procedure SendPasswordResetEmail (
    a_RegistrationId    udt_Id,
    a_AsOfDate          date
  ) is
  begin
    extension.pkg_SendMail_old.SendPosseEmail(a_RegistrationId, sysdate, 'FromEmailAddress', 'EmailAddress',
      'CCEmailsTo', null, 'PasswordResetEmailSubject', 'PasswordResetEmailBody',
      null, null, 'N');
  end SendPasswordResetEmail;

  /*--------------------------------------------------------------------------
   * CreateExtUser() -- PUBLIC
   * Create a new external user
   * Called from POST VERIFY of j_REGISTRATION
   * and j_ER_regerraluserregistration job.
   *------------------------------------------------------------------------*/
  procedure CreateExtUser (
    a_RegistrationId      udt_Id,
    a_AsOfDate            date
  ) is
    t_RegDef              udt_Id;
    t_StaffDef            udt_Id := api.pkg_ObjectDefQuery.IdForName('u_Users');
    t_UserId              udt_Id;
    t_ABCPublicAccessGroup udt_Id;
    t_ClearanceExt        udt_Id;
    t_ParksExt            udt_Id;
--    t_appObj              udt_Id;
    t_ActPwd              varchar2(32);
    t_UpdateUser          varchar2(1);
    t_LogonId             varchar2(30);
    t_Name                varchar2(500);
    t_Question            varchar2(250);
--    t_timeout             number(3);
--    t_timeExpired         number(9);
    t_nameExists          pls_integer;
    t_AccessGroupId       udt_Id;
    t_NewContractor       udt_Id;
    t_NewRelId            udt_Id;
    t_ContractorUserEPId  udt_Id;
    t_ContractorDefId     udt_Id;
    t_Count               number;
    t_Email               varchar2(200);
--    t_AccessGroupExists   pls_integer;

  begin

      --OVERALL SETUP
      -- This is set from an action on the Activate Presentation
      t_UpdateUser := api.pkg_columnQuery.value(a_RegistrationId, 'CreateUser');
      -- The user must re-enter their password on the "Activate" presentation
      t_ActPwd := api.pkg_columnQuery.value(a_RegistrationId, 'ActivatePwd');

      if api.pkg_columnQuery.value(a_RegistrationId, 'AuthKey') is null then
        api.pkg_columnUpdate.SetValue(a_RegistrationId, 'AuthKey',
            dbms_random.string('x', 9) );
      end if;

    if t_UpdateUser = 'Y' then  -- Activate has been triggerred

    select j.JobTypeId
      into t_RegDef
      from api.jobs j
     where j.jobid = a_Registrationid;

    -- check to see if we already have an attached user
    t_UserId  := api.pkg_columnQuery.value(a_RegistrationId, 'UserObjectId');
    t_Name    := nvl(api.pkg_columnquery.value(a_RegistrationId, 'BusinessName'),
                     api.pkg_columnquery.value(a_RegistrationId, 'FirstName') || ' ' || api.pkg_columnquery.value(a_RegistrationId, 'LastName'));
    --t_LogonId := api.pkg_columnQuery.value(a_RegistrationId, 'UserId');
    t_Email := api.pkg_columnQuery.value(a_RegistrationId, 'EmailAddress');
    if t_UserId is not null then -- we are resetting the password, otherwise a new LogonId will be created below.
      begin
        select /*+cardinality(x 1)*/u.OracleLogonId into t_LogonId
        from table(api.pkg_simplesearch.castableobjectsbyindex('u_Users', 'EmailAddress', t_Email)) x
        join query.u_users u on u.ObjectId = x.objectid
        where u.usertype = 'Public';
      exception
        when too_many_rows then
          api.pkg_errors.raiseerror(-20000, 'Found more than one record with this email.');
        when no_data_found then
          api.pkg_errors.raiseerror(-20000, 'Email not found.');
      end;
    end if;

    if t_UserId is null and t_LogonId in
       ('ACCESS', 'ELSE', 'MODIFY', 'START', 'ADD', 'EXCLUSIVE', 'NOAUDIT',
        'SELECT', 'ALL', 'EXISTS', 'NOCOMPRESS', 'SESSION', 'ALTER', 'FILE',
        'NOT', 'SET', 'AND', 'FLOAT', 'NOTFOUND', 'SHARE', 'ANY', 'FOR',
        'NOWAIT', 'SIZE', 'ARRAYLEN', 'FROM', 'NULL', 'SMALLINT', 'AS',
        'GRANT', 'NUMBER', 'SQLBUF', 'ASC', 'GROUP', 'OF', 'SUCCESSFUL',
        'AUDIT', 'HAVING', 'OFFLINE', 'SYNONYM', 'BETWEEN', 'IDENTIFIED', 'ON',
        'SYSDATE', 'BY', 'IMMEDIATE', 'ONLINE', 'TABLE', 'CHAR', 'IN',
        'OPTION', 'THEN', 'CHECK', 'INCREMENT', 'OR', 'TO', 'CLUSTER', 'INDEX',
        'ORDER', 'TRIGGER', 'COLUMN', 'INITIAL', 'PCTFREE', 'UID', 'COMMENT',
        'INSERT', 'PRIOR', 'UNION', 'COMPRESS', 'INTEGER', 'PRIVILEGES',
        'UNIQUE', 'CONNECT', 'INTERSECT', 'PUBLIC', 'UPDATE', 'CREATE', 'INTO',
        'RAW', 'USER', 'CURRENT', 'IS', 'RENAME', 'VALIDATE', 'DATE', 'LEVEL',
        'RESOURCE', 'VALUES', 'DECIMAL', 'LIKE', 'REVOKE', 'VARCHAR',
        'DEFAULT', 'LOCK', 'ROW', 'VARCHAR2', 'DELETE', 'LONG', 'ROWID',
        'VIEW', 'DESC', 'MAXEXTENTS', 'ROWLABEL', 'WHENEVER', 'DISTINCT',
        'MINUS', 'ROWNUM', 'WHERE', 'DROP', 'MODE', 'ROWS', 'WITH' ) then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'Create External User');
      api.pkg_errors.raiseError(-20000, 'Invalid User Name.');
    end if;


    if t_RegDef = api.pkg_configquery.objectdefidforname('j_ER_ReferralUserRegistration')
      then

        -- Make sure the "activate" password matches the stored password on the
        --  registration object
        t_ActPwd := Encrypt(t_ActPwd);
        if t_ActPwd != api.pkg_columnQuery.value(a_RegistrationId,
                                                 'PasswordWeb') then
          api.pkg_errors.RaiseError(-20000, 'Invalid Password - 2 ' || t_ActPwd ||' - '|| api.pkg_columnQuery.value(a_RegistrationId,
                                                 'PasswordWeb'));
        else

          if t_UserId is not null then -- we are resetting the password
            -- set password on user object
            api.pkg_columnUpdate.SetValue(t_UserId, 'PasswordWeb', t_ActPwd);

          else  -- Create a new external user
            t_LogonId := api.pkg_columnQuery.value(a_RegistrationId, 'UserId');
            select count(*) into t_nameExists from api.users
             where oracleLogonId = t_LogonId;
            if t_nameExists > 0 then
              api.pkg_Errors.Clear();
              api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
              api.pkg_errors.raiseError(-20000, 'User already exists.');
            end if;
            t_UserId := api.pkg_UserUpdate.new('eReferralUsers', t_LogonId,
                                   t_Name, t_ActPwd);


            -- Set OSUserId to OracleLogonId for external users so that user
            --   searches can use a single index to search by
            --api.pkg_columnUpdate.setValue(a_RegistrationId, 'OSUserId', t_LogonId);
            -- copy matching details
            for x in (
                select cd1.name,
                       api.pkg_columnquery.value(a_RegistrationId, cd1.name) val
                  from api.columndefs cd1
                  join api.columndefs cd2 on cd1.name = cd2.name
                 where cd1.objectdefid = t_RegDef
                   and cd2.objectdefid = t_StaffDef
                   and cd2.stored = 'Y') loop
              api.pkg_columnUpdate.setValue(t_UserId, x.name, x.val);
            end loop;
            api.pkg_columnUpdate.setValue(a_RegistrationId, 'UserObjectId',    t_UserId);
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'FirstName',       api.pkg_columnquery.value(a_RegistrationId, 'FirstName'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'LastName',        api.pkg_columnquery.value(a_RegistrationId, 'LastName'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'ERPositionTitle', api.pkg_columnquery.value(a_RegistrationId, 'ERPositionTitle'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'AddressLine1',    api.pkg_columnquery.value(a_RegistrationId, 'AddressLine1'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'AddressLine2',    api.pkg_columnquery.value(a_RegistrationId, 'AddressLine2'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'CityTown',        api.pkg_columnquery.value(a_RegistrationId, 'CityTown'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'State',           api.pkg_columnquery.value(a_RegistrationId, 'State'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'ZipCode',         api.pkg_columnquery.value(a_RegistrationId, 'ZipCode'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'ZipExtension',    api.pkg_columnquery.value(a_RegistrationId, 'ZipExtension'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'PhoneNumber',     api.pkg_columnquery.value(a_RegistrationId, 'PhoneNumber'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'EmailAddress',    api.pkg_columnquery.value(a_RegistrationId, 'EmailAddress'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'BusinessName',    api.pkg_columnquery.value(a_RegistrationId, 'BusinessName'));
            api.pkg_ColumnUpdate.SetValue(t_UserId,         'UserType',        'Referral');
          end if;

        --add user to ER Recipient Access Group
          select AccessGroupId
            into t_AccessGroupId
            from api.accessgroups ag
           where ag.Description = 'ER Recipient';

          select count(1)
            into t_Count
            from api.accessgroupusers a
           where a.userid = t_UserId
             and a.AccessGroupId = t_AccessGroupId;

          if t_Count = 0
            then
              api.pkg_userupdate.AddToAccessGroup(t_UserId, t_AccessGroupId);
          end if;

--pkg_debug.putline('####: Resetting flags');
          -- Reset flags and remove password values
          api.pkg_columnUpdate.setValue(a_RegistrationId,   'IsProcessed', 'Y');
          api.pkg_columnUpdate.setValue(a_RegistrationId,   'CreateUser', 'N');
          api.pkg_columnUpdate.removeValue(a_RegistrationId,'PasswordWeb');
          api.pkg_columnUpdate.removeValue(a_RegistrationId,'ConfirmPassword');
        end if;





--REG DEF
    elsif t_RegDef = api.pkg_configquery.objectdefidforname('j_Registration')
    then

    if t_ActPwd is null then
      api.pkg_errors.raiseError(-20000, 'Invalid Password.');
    end if;

        -- Make sure the "activate" password matches the stored password on the registration job
        t_ActPwd := Encrypt(t_ActPwd);
        if t_ActPwd != api.pkg_columnQuery.value(a_RegistrationId,
                                                 'PasswordWeb') then
          api.pkg_errors.RaiseError(-20000, 'The password entered does not match the password you registered with, please try again.');
        else

          if t_UserId is not null then -- we are resetting the password
            -- set password on user object
            api.pkg_columnUpdate.SetValue(t_UserId, 'PasswordWeb', t_ActPwd);

          else  -- Create a new external user
            t_LogonId := upper(substr(api.pkg_columnquery.Value(a_RegistrationId, 'FirstName'), 0, 1)) ||
                         upper(substr(api.pkg_columnquery.Value(a_RegistrationId, 'LastName'), 0, 1)) ||
                         a_RegistrationId;
            --check if Email Address has already been used for a Public user
            select /*+cardinality(x 1)*/ count(1)--u.AuthenticationName
              into t_Count
              from table(cast(api.pkg_simplesearch.castableobjectsbyindex('u_Users', 'EmailAddress', api.pkg_columnquery.Value(a_RegistrationId, 'EmailAddress')) as api.udt_ObjectList)) x
              join query.u_users u on u.ObjectId = x.objectid
             where u.usertype = 'Public';

            if t_Count > 0 then
              api.pkg_errors.raiseError(-20000, 'The email address entered has already been used.');
            end if;

            --create the user
            t_UserId := api.pkg_UserUpdate.New('PublicInternetUsers', substr(t_Name, 0, 50) || a_RegistrationId, t_LogonId, t_LogonId, t_ActPwd);

            select accessgroupid
              into t_ABCPublicAccessGroup
              from api.accessgroups
             where description = 'ABC Public';

            api.pkg_UserUpdate.AddToAccessGroup(t_UserId, t_ABCPublicAccessGroup);

            -- Set OSUserId to OracleLogonId for external users so that user
            --   searches can use a single index to search by
            --api.pkg_columnUpdate.setValue(a_RegistrationId, 'OSUserId', t_LogonId);
            -- copy matching details
            for x in (
                select cd1.name,
                       api.pkg_columnquery.value(a_RegistrationId, cd1.name) val
                  from api.columndefs cd1
                  join api.columndefs cd2 on cd1.name = cd2.name
                 where cd1.objectdefid = t_RegDef
                   and cd2.objectdefid = t_StaffDef
                   and cd2.stored = 'Y') loop
              api.pkg_columnUpdate.setValue(t_UserId, x.name, x.val);

            end loop;
            api.pkg_columnUpdate.setValue(a_RegistrationId, 'UserObjectId', t_UserId);
            api.pkg_ColumnUpdate.SetValue(t_UserId, 'SecurityQuestion',     api.pkg_columnquery.value(a_RegistrationId, 'SecurityQuestion'));
            api.pkg_ColumnUpdate.SetValue(t_UserId, 'FirstName',            api.pkg_columnquery.value(a_RegistrationId, 'FirstName'));
            api.pkg_ColumnUpdate.SetValue(t_UserId, 'LastName',             api.pkg_columnquery.value(a_RegistrationId, 'LastName'));
            api.pkg_ColumnUpdate.SetValue(t_UserId, 'PhoneNumber',          api.pkg_columnquery.value(a_RegistrationId, 'PhoneNumber'));
            api.pkg_ColumnUpdate.SetValue(t_UserId, 'EmailAddress',         api.pkg_columnquery.value(a_RegistrationId, 'EmailAddress'));
            api.pkg_ColumnUpdate.SetValue(t_UserId, 'UserType',             'Public');
          end if; --Change Password or Create User
          api.pkg_columnUpdate.setValue(a_RegistrationId, 'UserAuthenticationName', t_LogonId);

          -- Reset flags and remove password values
          api.pkg_columnUpdate.setValue(a_RegistrationId, 'IsProcessed', 'Y');
          api.pkg_columnUpdate.setValue(a_RegistrationId, 'CreateUser', 'N');
          api.pkg_columnUpdate.removeValue(a_RegistrationId, 'PasswordWeb');
          api.pkg_columnUpdate.removeValue(a_RegistrationId, 'ConfirmPassword');
        end if; --Valid Password
      end if; --RegDef is for Public or for Referral user
    end if; --CreateUser
  end CreateExtUser;

  /*--------------------------------------------------------------------------
   * NewIntUser() -- PUBLIC
   * Fill in some details on a new internal user
   * Run on Column Change of boolean CreateIntUser column on j_Registration
   * Column is set via action on CreateUser button on internal New presentation
   * on j_Registration.
   *------------------------------------------------------------------------*/
  procedure NewIntUser (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    t_Name              varchar2(50);
    t_Logon             varchar2(50);
    t_FirstName         varchar2(50);
    t_LastName          varchar2(50);
    t_Id                varchar2(200);
    t_Email             varchar2(60);
    t_NewUserId         udt_Id;
    t_rel               udt_Id;
    t_EndPt             udt_Id;
    t_Def               udt_id;
    t_nameExists        pls_integer;
    t_ext               pls_integer;
    t_UserId            pls_Integer;
  begin
    if api.pkg_columnquery.value(a_ObjectId, 'CreateIntUser') = 'Y' then
      select objectDefId into t_Def from api.objects
       where objectId = a_ObjectId;
      t_EndPt := api.pkg_configQuery.EndPointIdForName(t_def, 'Users');
      t_Id := api.pkg_columnquery.value(a_objectId, 'OSUserId');
      --check that t_Id is unique
      t_UserId := api.pkg_SimpleSearch.ObjectByIndex( 'u_Users', 'OSUserId', t_Id );
      if t_UserId > 0 then
        api.pkg_errors.raiseError(-20000, 'User Id already exists. Please provide a different User Id.');
      end if;
      t_FirstName := api.pkg_columnquery.value(a_objectId, 'FirstName');
      t_LastName := api.pkg_columnquery.value(a_ObjectId, 'LastName');
      t_Email := api.pkg_columnquery.value(a_ObjectId, 'EmailAddress');
      t_name := upper(substr(t_FirstName, 1, 1) ||
                      substr(replace(t_LastName, ' ',''), 1, 27));
      t_ext := 0;
      if api.pkg_columnquery.numericValue(a_ObjectId, 'UserCount') > 0 then
        api.pkg_errors.raiseError(-20000, 'User already exists!');
      end if;

      select count(*) into t_nameExists from api.users
       where oracleLogonId = t_name;

      -- We are auto-generating the OracleLogonId from first letter of
      -- first name + Last Name (truncated) + optional sequence
      -- in order to ensure that OracleLogonId is unique
      t_Logon := replace(t_name, ' ', '');  -- Strip spaces
      while t_nameExists > 0 loop
        select count(*) into t_nameExists from api.users
         where oracleLogonId = t_Logon;
        if t_nameExists > 0 then
          t_ext := t_ext + 1;
          t_Logon := t_name || t_ext;
        end if;
      end loop;

      -- The user name is displayed on assignment lists.
      -- Posse insists that it be unique.
      t_name := api.pkg_columnquery.value(a_ObjectId, 'Name');
      select count(*) into t_nameExists from api.users
       where upper(name) = upper(t_name);

      if t_nameExists > 0 then
        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('Operation', 'Create Staff');
        api.pkg_Errors.RaiseError(-20000, 'Display name is not unique. '||
            'Please change the display name.  Eg: Add middle initials');
      end if;

      t_newUserId := api.pkg_UserUpdate.new('Internal', trim(t_Logon),
                                             trim(t_Name), 'opac');
      api.pkg_columnUpdate.setValue(t_newUserId, 'IsStaff', 'Y');
      api.pkg_columnUpdate.setValue(t_newUserId, 'FirstName', t_FirstName);
      api.pkg_columnUpdate.setValue(t_newUserId, 'LastName', t_LastName);
      api.pkg_columnUpdate.setValue(t_newUserId, 'OSUserId', t_Id);
      api.pkg_columnUpdate.setValue(t_newUserId, 'IsNew', 'Y');
      api.pkg_columnUpdate.setValue(t_newUserId, 'Province', 'AB');
      api.pkg_columnUpdate.setValue(t_newUserId, 'Country', 'Canada');
      if t_Email is not null then
        api.pkg_columnUpdate.setValue(t_newUserId, 'EmailAddress', t_Email);
      end if;
      t_rel := api.pkg_relationshipUpdate.new(t_endpt, a_ObjectId, t_newUserId);

      api.pkg_columnUpdate.setValue(a_objectId, 'CreateIntUser', 'N');
    end if;
  end NewIntUser;


  /*--------------------------------------------------------------------------
   * NewReferralUser() -- PUBLIC
   * Create a new external Referral user
   *------------------------------------------------------------------------*/
/*  procedure NewReferralUser (
    a_UserId              udt_Id,
    a_AsOfDate            date
  ) is
    t_RegDef              udt_Id;
    t_StaffDef            udt_Id;
    t_UserId              udt_Id;
    t_ClearanceExt        udt_Id;
    t_ParksExt            udt_Id;
    t_appObj              udt_Id;
    t_ActPwd              varchar2(32);
    t_UpdateUser          varchar2(1);
    t_LogonId             varchar2(30);
    t_Name                varchar2(500);
    t_Question            varchar2(250);
    t_timeout             number(3);
    t_timeExpired         number(9);
    t_nameExists          pls_integer;
--    t_AccessGroupId       udt_Id;
--    t_NewContractor       udt_Id;
--    t_NewRelId            udt_Id;
--    t_ContractorUserEPId  udt_Id;
--    t_ContractorDefId     udt_Id;
--    t_AccessGroupExists   pls_integer;
  begin

--    t_ContractorUserEPId  := api.pkg_configquery.endpointidforname('o_Contractor', 'Users');
--    t_ContractorDefId     := api.pkg_configquery.objectdefidforname('o_Contractor');
--    t_RegDef              := api.pkg_ObjectDefQuery.IdForName('j_Registration');
    t_StaffDef            := api.pkg_ObjectDefQuery.IdForName('u_Users');

    -- This is set from a round trip on the maintenance pane
    t_UpdateUser := api.pkg_columnQuery.value(a_UserId, 'CreateReferralUser');

    -- The user must re-enter their password on the "Activate" presentation
--    t_ActPwd := api.pkg_columnQuery.value(a_RegistrationId, 'ActivatePwd');
--    t_Question := api.pkg_columnQuery.value(a_RegistrationId, 'SecurityQuestion');

--    select accessgroupid into t_ParksExt from api.accessgroups
--     where description = 'Super Users';
--    select accessgroupid into t_ClearanceExt from api.accessgroups
--     where description = 'Web Contractors';

--    if api.pkg_columnQuery.value(a_RegistrationId, 'AuthKey') is null then
--      api.pkg_columnUpdate.SetValue(a_RegistrationId, 'AuthKey',
--          dbms_random.string('x', 9) );
--    end if;

    -- check to see if we already have an attached user
--    t_UserId := api.pkg_columnQuery.value(a_RegistrationId, 'UserObjectId');

    t_Name    := nvl(api.pkg_columnquery.value(a_UserId, 'OSUserId'),
                     api.pkg_columnquery.value(a_UserId, 'FirstName') || ' ' || api.pkg_columnquery.value(a_UserId, 'LastName'));
    t_LogonId := api.pkg_columnQuery.value(a_UserId, 'OSUserId');
    if
--t_UserId is null and
      t_LogonId in
       ('ACCESS', 'ELSE', 'MODIFY', 'START', 'ADD', 'EXCLUSIVE', 'NOAUDIT',
        'SELECT', 'ALL', 'EXISTS', 'NOCOMPRESS', 'SESSION', 'ALTER', 'FILE',
        'NOT', 'SET', 'AND', 'FLOAT', 'NOTFOUND', 'SHARE', 'ANY', 'FOR',
        'NOWAIT', 'SIZE', 'ARRAYLEN', 'FROM', 'NULL', 'SMALLINT', 'AS',
        'GRANT', 'NUMBER', 'SQLBUF', 'ASC', 'GROUP', 'OF', 'SUCCESSFUL',
        'AUDIT', 'HAVING', 'OFFLINE', 'SYNONYM', 'BETWEEN', 'IDENTIFIED', 'ON',
        'SYSDATE', 'BY', 'IMMEDIATE', 'ONLINE', 'TABLE', 'CHAR', 'IN',
        'OPTION', 'THEN', 'CHECK', 'INCREMENT', 'OR', 'TO', 'CLUSTER', 'INDEX',
        'ORDER', 'TRIGGER', 'COLUMN', 'INITIAL', 'PCTFREE', 'UID', 'COMMENT',
        'INSERT', 'PRIOR', 'UNION', 'COMPRESS', 'INTEGER', 'PRIVILEGES',
        'UNIQUE', 'CONNECT', 'INTERSECT', 'PUBLIC', 'UPDATE', 'CREATE', 'INTO',
        'RAW', 'USER', 'CURRENT', 'IS', 'RENAME', 'VALIDATE', 'DATE', 'LEVEL',
        'RESOURCE', 'VALUES', 'DECIMAL', 'LIKE', 'REVOKE', 'VARCHAR',
        'DEFAULT', 'LOCK', 'ROW', 'VARCHAR2', 'DELETE', 'LONG', 'ROWID',
        'VIEW', 'DESC', 'MAXEXTENTS', 'ROWLABEL', 'WHENEVER', 'DISTINCT',
        'MINUS', 'ROWNUM', 'WHERE', 'DROP', 'MODE', 'ROWS', 'WITH' ) then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'Create External User');
      api.pkg_errors.raiseError(-20000, 'Invalid User Name.');
    end if;

    if t_UpdateUser = 'Y' then  -- Create has been triggerred
--pkg_debug.putline('#####: CreateExtUser at: ' || to_char(systimestamp, 'HH24:MI:SS:FF6') || '  t_ActivatePwd: ' || t_ActPwd);
--      if t_ActPwd is null then
--        api.pkg_errors.raiseError(-20000, 'Invalid Password.');
--      end if;

      -- Make sure the "activate" password matches the stored password on the
      --  registration object
--      t_ActPwd := Encrypt(t_ActPwd);
--pkg_debug.putline('####: just before check on Active Password. RegistrationID: ' || a_Registrationid);
--      if t_ActPwd != api.pkg_columnQuery.value(a_RegistrationId,
--                                               'PasswordWeb') then
--        api.pkg_errors.RaiseError(-20000, 'Invalid Password - 2 ' || t_ActPwd ||' - '|| api.pkg_columnQuery.value(a_RegistrationId,
--                                               'PasswordWeb'));
--      else
--        if t_UserId is not null then -- we are resetting the password
--pkg_debug.putline('####: t_UserId is not null we are resetting the password');
          -- set password on user object
--          api.pkg_columnUpdate.SetValue(t_UserId, 'PasswordWeb', t_ActPwd);

--        else  -- Create a new external user
pkg_debug.putline('####: t_UserId is null and we are creating a new external user.');
          t_LogonId := api.pkg_columnQuery.value(a_UserId, 'OSUserId');
          select count(*) into t_nameExists from api.users
           where oracleLogonId = t_LogonId;
          if t_nameExists > 0 then
            api.pkg_Errors.Clear();
            api.pkg_Errors.SetArgValue('Operation', 'ValidatePwd');
            api.pkg_errors.raiseError(-20000, 'User already exists.');
          end if;
pkg_debug.putline('### ' || t_name);
          t_UserId := api.pkg_UserUpdate.new('eReferralUsers', t_LogonId,
                                 t_Name, t_ActPwd);
pkg_debug.putline('### ' || t_name);
--          api.pkg_UserUpdate.AddToAccessGroup(t_UserId, t_ClearanceExt);
--          api.pkg_UserUpdate.AddToAccessGroup(t_UserId, t_ParksExt);

          -- Set OSUserId to OracleLogonId for external users so that user
          --   searches can use a single index to search by
          --api.pkg_columnUpdate.setValue(a_RegistrationId, 'OSUserId', t_LogonId);
          -- copy matching details
--          for x in (
--              select cd1.name,
--                     api.pkg_columnquery.value(a_RegistrationId, cd1.name) val
--                from api.columndefs cd1
--                join api.columndefs cd2 on cd1.name = cd2.name
--               where cd1.objectdefid = t_RegDef
--                 and cd2.objectdefid = t_StaffDef
--                 and cd2.stored = 'Y') loop
--            api.pkg_columnUpdate.setValue(t_UserId, x.name, x.val);
--pkg_debug.putline('####: User created and this column was set: ' || x.name || ' to this value: ' || x.val || ' on this user: ' || t_UserId);
--          end loop;


--pkg_debug.putline('####: values from loop that is not entered. t_RegDef: ' || t_RegDef || '. t_StaffDef: ' || t_StaffDef);
--          api.pkg_columnUpdate.setValue(a_RegistrationId, 'UserObjectId', t_UserId);
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'SecurityQuestion',     t_Question);
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'FirstName',            api.pkg_columnquery.value(a_RegistrationId, 'FirstName'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'LastName',             api.pkg_columnquery.value(a_RegistrationId, 'LastName'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'AddressLine1',         api.pkg_columnquery.value(a_RegistrationId, 'AddressLine1'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'AddressLine2',         api.pkg_columnquery.value(a_RegistrationId, 'AddressLine2'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'CityTown',             api.pkg_columnquery.value(a_RegistrationId, 'CityTown'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'State',                api.pkg_columnquery.value(a_RegistrationId, 'State'));
--          --api.pkg_ColumnUpdate.SetValue(t_UserId, 'County',              api.pkg_columnquery.value(a_RegistrationId, 'County'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'ZipCode',              api.pkg_columnquery.value(a_RegistrationId, 'ZipCode'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'ZipExtension',         api.pkg_columnquery.value(a_RegistrationId, 'ZipExtension'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'BusinessPhone',        api.pkg_columnquery.value(a_RegistrationId, 'BusinessPhone'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'EmailAddress',         api.pkg_columnquery.value(a_RegistrationId, 'EmailAddress'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'BusinessName',         api.pkg_columnquery.value(a_RegistrationId, 'BusinessName'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'ExternalSelectDeselectAll',   api.pkg_columnquery.value(a_RegistrationId, 'EmailNotifications'));
--          api.pkg_ColumnUpdate.SetValue(t_UserId, 'UserType',             'Public');

--          if api.pkg_columnquery.value(a_RegistrationId, 'Contractor') = 'Y'
--            then --create and relate a contractor object to the user that was just related
--              t_NewContractor := api.pkg_objectupdate.new(t_ContractorDefId);
--              t_NewRelId      := api.pkg_relationshipupdate.new(t_ContractorUserEPId, t_NewContractor, t_UserId);

--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'BusinessName', api.pkg_columnquery.value(a_RegistrationId, 'BusinessName'));
--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'AddressLine1', api.pkg_columnquery.value(a_RegistrationId, 'AddressLine1'));
--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'AddressLine2', api.pkg_columnquery.value(a_RegistrationId, 'AddressLine2'));
--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'City', api.pkg_columnquery.value(a_RegistrationId, 'CityTown'));
--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'State', api.pkg_columnquery.value(a_RegistrationId, 'State'));
--              --api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'County', api.pkg_columnquery.value(a_RegistrationId, 'County'));
--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'ZipCode', api.pkg_columnquery.value(a_RegistrationId, 'ZipCode'));
--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'ZipExtension', api.pkg_columnquery.value(a_RegistrationId, 'ZipExtension'));
--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'PrimaryPhoneNumber', api.pkg_columnquery.value(a_RegistrationId, 'BusinessPhone'));
--              api.pkg_ColumnUpdate.SetValue(t_NewContractor, 'PrimaryContactName', api.pkg_columnquery.value(a_RegistrationId, 'PrimaryContactName'));
--              --api.pkg_ColumnUpdate.SetValue(t_NewContractor, '', api.pkg_columnquery.value(a_RegistrationId, ''));

--           end if;
        end if;
--pkg_debug.putline('####: Resetting flags');
        -- Reset flags and remove password values
--        api.pkg_columnUpdate.setValue(a_RegistrationId, 'IsProcessed', 'Y');
--        api.pkg_columnUpdate.setValue(a_RegistrationId, 'CreateUser', 'N');
--        api.pkg_columnUpdate.removeValue(a_RegistrationId, 'PasswordWeb');
--        api.pkg_columnUpdate.removeValue(a_RegistrationId, 'ConfirmPassword');
--      end if;
--    end if;
  end NewReferralUser;
*/
  /*--------------------------------------------------------------------------
   * SetPwdEncryption() -- PUBLIC
   * Encrypt the password so that it is not stored in clear text
   * Run from column change on PasswordWeb column on j_Registration object
   *------------------------------------------------------------------------*/
  procedure SetPwdEncryption (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    t_pwd               varchar2(32);
    t_EncPwd            varchar2(32);
    t_LogonId           varchar2(30);
  begin
    t_pwd := api.pkg_columnQuery.value(a_ObjectId, 'PasswordWeb');
    if t_pwd is not null then
      t_EncPwd := Encrypt(t_pwd);
      if t_EncPwd is not null then
        t_LogonId := api.pkg_columnQuery.value(a_ObjectId, 'UserId');
        ValidatePwd(t_pwd);
/*        if instr(upper(t_pwd), t_LogonId) > 0 then  --John P Aug 23, 2011. With email lagin, users do not necessarily know their userid
          api.pkg_errors.raiseError(-20000,
              'Password must not include any part of your User Id');
        end if;*/

        api.pkg_columnUpdate.SetValue(a_ObjectId, 'PasswordWeb', t_EncPwd);
        t_pwd := Encrypt(api.pkg_columnQuery.value(a_ObjectId,
                         'ConfirmPassword'));
        api.pkg_columnUpdate.SetValue(a_ObjectId, 'ConfirmPassword', t_pwd);
      end if;
    end if;
  end SetPwdEncryption;

  /*---------------------------------------------------------------------------
   * AdminChangePassword() -- PUBLIC
   * This can change passwords for both Public and Internal users
   *
   * NOTE: in order for this to work, the extension user needs to have ALTER USER privs
   *-------------------------------------------------------------------------*/
  procedure AdminChangePassword (
    a_ObjectId    udt_Id,
    a_AsOfDate    date
  ) is
    t_NewPassword            varchar2(32) := api.pkg_columnquery.Value(a_ObjectId,'PasswordNew');
    t_EncryptedPassword      varchar2(4000);
    t_UserType               varchar2(100) := api.pkg_columnquery.Value(a_ObjectId, 'UserType');
    t_OracleLogonId          varchar2(30) := api.pkg_columnquery.Value(a_ObjectId, 'OracleLogonId');
    jobno                    binary_integer;
    t_SQL                    varchar2(4000);

  begin
    case
      when t_UserType = 'Public' then
        ValidatePwd(t_NewPassword);
        t_EncryptedPassword := Encrypt(t_NewPassword);
        api.pkg_columnUpdate.setValue(a_ObjectId, 'PasswordWeb', t_EncryptedPassword);
        api.pkg_columnUpdate.RemoveValue(a_ObjectId, 'PasswordNew');

      when t_UserType = 'Internal' then
        if t_NewPassword is null then
           api.pkg_Errors.RaiseError(-20000, 'Please enter a new password.');
        else
           sys.dbms_job.submit(jobno, 'extension.ExecuteDDL(''alter user ' || t_OracleLogonId || ' identified by ' || t_NewPassword || ''');', sysdate, null);
           api.pkg_columnUpdate.RemoveValue(a_ObjectId, 'PasswordNew');
        end if;
      else
        null;
    end case;


  end;

  /*--------------------------------------------------------------------------
   * ChangeExtPwd() -- PUBLIC
   * Change the external user's password
   * Called from Column Change procedure on boolean ChangePwd column on user
   * object.  Column is set via action on Change Password button on external
   * user Change Password presentation.
   *
   * The password can be any combination of letters, numbers and special
   * characters up to 30 characters long, and is case sensitive.
   * Passwords must meet the following restrictions:
   * - must be at least eight (8) characters long and may not include any part
   *   of your User ID
   * - must include at least one Upper case letter (A, B, C, ...)
   * - must include at least one lower case letter (a, b, c, ...)
   * - must include at least one number (1, 2, 3, ...)
   *------------------------------------------------------------------------*/
  procedure ChangeExtPwd (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    t_CurrentPwd        varchar2(32);
    t_OldPwd            varchar2(32);
    t_NewPwd            varchar2(32);
    t_ConfirmPwd        varchar2(32);
    t_UserId            varchar2(30);
    t_LockoutTime       date;
  begin
    t_UserId := api.pkg_columnQuery.value(a_ObjectId, 'OracleLogonId');
    if api.pkg_columnQuery.value(a_ObjectId, 'ChangePwd') = 'Y' then
      /* POSSE ABC reused this LMS procedure however lockout was not an
         initial system requirement therefore the lockout code has
         been commented out for the time being                      */

      -- select min(LockoutDate) into t_LockoutTime from internetUsers
       -- where userId = a_ObjectId;
      -- if t_LockoutTime > sysdate then
      --  api.pkg_Errors.Clear();
      --  api.pkg_Errors.SetArgValue('Operation', 'Change Password');
      --  api.pkg_Errors.SetArgValue('User Id', a_ObjectId);
      --  api.pkg_Errors.SetArgValue('Lockout',
      --                             to_char(t_LockoutTime, 'hh24:mi:ss'));
      --  api.pkg_Errors.RaiseError(-20000,
      --    'Too many failures:  ' ||
      --    'You may not attempt to change your password again until '||
      --    to_char(t_LockoutTime, 'hh:mi:ss am')||'.');
      -- end if;

      -- Existing password on User record
      t_OldPwd := api.pkg_columnQuery.value(a_ObjectId, 'PasswordWeb');
      -- Old Password as entered by Online User
      t_CurrentPwd := api.pkg_columnQuery.value(a_ObjectId, 'PasswordSupplied');
      -- New Password as entered by Online User
      t_NewPwd := api.pkg_columnQuery.value(a_ObjectId, 'PasswordNew');
      -- Confirmed New Password as entered by Online User
      t_ConfirmPwd := api.pkg_columnQuery.value(a_ObjectId, 'PasswordConfirm');

      -- Raise error if online user has not supplied any password information
      if t_CurrentPwd is null and t_NewPwd is null and t_ConfirmPwd is null then
        api.pkg_errors.raiseError(-20000, 'Please supply password information.');
      end if;
      -- Raise error if old password has not been entered
      if t_CurrentPwd is null then
        api.pkg_errors.RaiseError(-20000, 'Please supply Old Password');
      -- Raise error if old password entered does not match existing password in db
      elsif (Encrypt(t_CurrentPwd) <> t_OldPwd) then
        -- PwdFail(a_ObjectId);  setting lockout not ABC requirement
        api.pkg_errors.raiseError(-20000, 'Old Password supplied does not match existing password.');
      end if;

      -- Raise error if New Password is not entered
      if t_NewPwd is null then
        api.pkg_errors.raiseError(-20000, 'Please supply New Password.');
      -- Raise error if Confirm Password is not entered
      elsif t_ConfirmPwd is null then
        api.pkg_errors.raiseError(-20000, 'Please Confirm New Password.');
      -- Raise error if NewPassword and Confirm Password entered do not match
      elsif t_NewPwd <> t_ConfirmPwd then
          api.pkg_errors.raiseError(-20000, 'New Password fields do not match.');
      -- Ensure the new password entered meets validation rules
      else
        ValidatePwd(t_NewPwd);
      end if;
/*      if instr(upper(t_NewPwd), t_UserId) > 0 then  --John P Aug 23, 2011. With email lagin, users do not necessarily know their userid
        api.pkg_errors.raiseError(-20000,
            'Password must not include any part of your User Id.');
      end if;*/

      -- Change Password
      t_NewPwd := Encrypt(t_NewPwd);
      api.pkg_columnUpdate.SetValue(a_ObjectId, 'PasswordWeb', t_NewPwd);

      -- Remove temporary values and reset ChangePwd flag
      api.pkg_columnUpdate.RemoveValue(a_ObjectId, 'PasswordNew');
      api.pkg_columnUpdate.RemoveValue(a_ObjectId, 'PasswordConfirm');
      api.pkg_columnUpdate.RemoveValue(a_ObjectId, 'PasswordSupplied');
      api.pkg_columnUpdate.SetValue(a_ObjectId, 'ChangePwd', 'N');
    end if;
  end ChangeExtPwd;

  /*--------------------------------------------------------------------------
   * FindUserId() -- PUBLIC
   * Find a User record from a string and create a relationship to it
   * Used for external reset password screen
   * Called from Column Change procedure on column SetUserRelationship
   * on j_Registration and the j_ER_ReferralUserRegistration.
   * SetUserRelationship is set via an action on the Next button on the
   * external Reset presentation.
   * The user types in their email and we try to find a user to match it.
   *------------------------------------------------------------------------*/
  procedure FindUserId (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    t_def               udt_Id;
    t_rel               udt_Id;
    t_endPt             udt_Id;
    t_user              varchar2(30);
    t_find              varchar2(1);
    t_userId            udt_Id;
    t_Email             varchar2(200);
    t_countRels         pls_Integer;
    t_countUsers        pls_Integer;
    t_UserManSchemeId   udt_id;
  begin
    t_find := api.pkg_columnQuery.value(a_ObjectId, 'SetUserRelationship');
    if t_find = 'Y' then

      select objectDefId into t_def from api.objects
       where objectId = a_ObjectId;
      t_endPt := api.pkg_ConfigQuery.EndpointIdForName(t_Def, 'Users');
      --t_user := api.pkg_columnQuery.value(a_ObjectId, 'UserId');
      t_Email := api.pkg_columnQuery.value(a_ObjectId, 'EmailAddress');
      begin
        select /*+cardinality(x 1)*/u.OracleLogonId into t_user
      from table(api.pkg_simplesearch.castableobjectsbyindex('u_Users', 'EmailAddress', t_Email)) x
      join query.u_users u on u.ObjectId = x.objectid
     where u.usertype in ('Public', 'Police');
      exception
        when too_many_rows then
          api.pkg_errors.raiseerror(-20000, 'Found more than one record with this email.');
        when no_data_found then
          api.pkg_errors.raiseerror(-20000, 'Email not found.');
      end;

      select count(*) into t_countUsers from api.users
       where oraclelogonid = t_user;
      select count(*) into t_countRels from api.relationships
       where fromObjectId = a_ObjectId and endPointId = t_endPt;

      if t_countUsers = 1 then
        if t_countRels > 0 then
          for x in ( select relationshipId from api.relationships
                      where fromObjectId = a_ObjectId
                        and endPointId = t_endPt) loop
            api.pkg_relationshipUpdate.remove(x.relationshipId);
          end loop;
        end if;
        select userid into t_userId from api.users
         where oraclelogonid = t_user;

        -- only create the rel if user is external
--        t_UserManSchemeId := bcpdata.pkg_apibreaks.GetUserManSchemeId('PublicInternetUsers');
        if api.pkg_columnquery.Value(t_userId, 'UserType') <> 'Internal'
          then
            t_Rel := api.pkg_relationshipUpdate.new(t_endPt, a_ObjectId,t_userId);
        end if;
      end if;
      api.pkg_columnUpdate.setValue(a_ObjectId, 'SetUserRelationship', 'N');
      if t_Def = api.pkg_configquery.objectdefidforname('j_Registration')
        then
          api.pkg_columnupdate.setValue(a_ObjectId, 'SecurityQuestion', api.pkg_columnquery.value(t_UserId, 'SecurityQuestion'));
      end if;
      if t_Def = api.pkg_configquery.ObjectDefIdForName('j_ER_ReferralUserRegistration')
        then
        --set the AuthKey on the Referral User Registration job
          if api.pkg_columnQuery.value(a_ObjectId, 'AuthKey') is null then
            api.pkg_columnUpdate.SetValue(a_ObjectId, 'AuthKey', dbms_random.string('x', 9) );
          end if;
        --Set the Email Address
          api.pkg_columnupdate.setvalue(a_ObjectId, 'EmailAddress', api.pkg_columnquery.value(t_UserId, 'EmailAddress'));
      end if;
    end if;
  end FindUserId;

 /*--------------------------------------------------------------------------
  * OSUserSearch()
  * Used for internal user login
  *------------------------------------------------------------------------*/
  procedure OSUserSearch (
      a_OSUserId                        varchar2,
      a_Objects                         out api.udt_objectList
  ) is
  begin
    if a_OSUserId is not null then
      extension.pkg_CxProceduralSearch.InitializeSearch( 'u_Users');
      extension.pkg_CxProceduralSearch.SearchByIndex('dup_OSLogon', a_OSUserId ,
                null, False);
      extension.pkg_CxProceduralSearch.SearchByIndex('IsStaff','Y',null,False);
      extension.pkg_CxProceduralSearch.SearchByIndex('Active', 'Y',null,False);
      a_Objects := api.udt_ObjectList();
      extension.pkg_CxProceduralSearch.PerformSearch( a_Objects, 'and' );
    end if;
  end OSUserSearch;

  /*--------------------------------------------------------------------------
   * ResetPassword() -- PUBLIC
   * Change the external user's password
   * Used for external reset password screen
   * Called from Column Change Procedure on boolean ResetPassword column on
   * j_Registration and j_ER_ReferralUserRegistration.  Column is set via
   * action on Change Password button on ChangePassword presentation.
   *------------------------------------------------------------------------*/
  procedure ResetPassword (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    t_def               udt_Id;
    t_endPt             udt_Id;
    t_pwd               varchar2(32);
    t_UserAnsr          varchar2(200);
    t_ResetAnsr         varchar2(200);
    t_reset             varchar2(1);
    t_LockoutTime       date;
    t_userId            udt_Id;
    t_CurrentPasswordCheck varchar2(32);
    t_CurrentPasswordReq varchar2(1) := api.pkg_columnquery.value(a_ObjectId, 'CurrentPasswordReq');

  begin
     t_reset := api.pkg_columnquery.value(a_ObjectId, 'ResetPassword');
    if t_reset = 'Y' then
      select objectDefId into t_def from api.objects
       where objectId = a_ObjectId;
      t_endPt := api.pkg_ConfigQuery.EndpointIdForName(t_Def, 'Users');

      select toObjectId into t_userid
        from api.relationships
       where fromObjectId = a_ObjectId
         and endPointId = t_endPt;
--if we are resetting the password from the Change Password presentation
  if t_def = api.pkg_configquery.objectdefidforname('j_ER_ReferralUserRegistration') and t_CurrentPasswordReq = 'Y'
    then
    --get the entered current password
      t_CurrentPasswordCheck := api.pkg_columnquery.value(a_ObjectId, 'CurrentPassword');
    --if the encrypted value of that password does not equal the value of the password on the user...
      if pkg_abc_users.Encrypt(t_CurrentPasswordCheck) <> api.pkg_columnquery.value(t_UserId, 'PasswordWeb')
        then
        --raise an error
          api.pkg_errors.raiseerror(-20000, 'The current Password that was entered is invalid, please try again.');
      end if;
  --we also need to
  end if; --if def is referral reg job

--api.pkg_errors.raiseerror(-20000, '###ERROR###');
      select min(LockoutDate) into t_LockoutTime from internetUsers
       where userId = t_userId;
      if t_LockoutTime > sysdate then
        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('Operation', 'Reset Password');
        api.pkg_Errors.SetArgValue('User Id', t_UserId);
        api.pkg_Errors.SetArgValue('Lockout',
                                   to_char(t_LockoutTime, 'hh24:mi:ss'));
        api.pkg_Errors.RaiseError(-20000,
          'Too many failures:  ' ||
          'You may not attempt to change your password again until '||
          to_char(t_LockoutTime, 'hh:mi:ss am')||'.');
      end if;
      api.pkg_columnUpdate.setValue(a_ObjectId, 'UserObjectId', t_userId);

      t_UserAnsr := api.pkg_columnQuery.value(t_userId, 'SecurityAnswer');
      t_ResetAnsr := api.pkg_columnQuery.value(a_objectId, 'SecurityAnswer');

      if lower(t_UserAnsr) = lower(t_ResetAnsr) and t_Def = api.pkg_configquery.objectdefidforname('j_Registration')
        then

        t_Pwd := api.pkg_columnQuery.value(a_ObjectId, 'PasswordWeb');
        api.pkg_columnUpdate.setValue(a_objectId, 'ResetPassword', 'N');
        api.pkg_columnUpdate.setValue(a_objectId, 'EmailType', 'PWREML');
        api.pkg_columnUpdate.setValue(a_objectId, 'SendPasswordResetEmail', 'Y');

      elsif t_Def = api.pkg_configquery.objectdefidforname('j_ER_ReferralUserRegistration')
        then
          t_Pwd := api.pkg_columnQuery.value(a_ObjectId, 'PasswordWeb');
          api.pkg_columnUpdate.setValue(a_objectId, 'ResetPassword', 'N');
--          api.pkg_columnUpdate.setValue(a_objectId, 'SendRegistrationEmail', 'Y');
      else

        PwdFail(t_userid);

        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('Operation', 'ResetPwd');
        api.pkg_errors.raiseError(-20000,
                         'Security Question/Answer is not correct.');
      end if;

      api.pkg_columnupdate.setvalue(a_ObjectId, 'ResetPassword', 'N');

    end if;

  end ResetPassword;

  /*--------------------------------------------------------------------------
   * DocUploadLimit() -- PUBLIC
   * Limits the total size of documents that can be submitted by a user per day.
   * Run from Post Verify on Application Documents
   *------------------------------------------------------------------------*/
  procedure DocUploadLimit (
    a_DocId             udt_Id,
    a_AsOfDate          date
  ) is
    t_appObj            udt_Id;
    t_userId            udt_Id;
    t_isNew             varchar2(1);
    t_docUpl            number(8,4) := 0;
    t_docLimit          number(4);
    t_docSize           number(10);
    t_docDate           date;
  begin
    --  api.pkg_errors.raiseError(-20000,
    --      'This upload surpasses the daily maximum upload size limit.');
    t_isNew := api.pkg_columnquery.value(a_DocId, 'IsNew');
    if t_isNew = 'Y' then
      t_appObj := api.pkg_SimpleSearch.ObjectByIndex(
                       'o_ApplicationInformation',
                       'dup_MenuCodeWebSiteName',
                       'mm-capsexternal' );
      t_docLimit := api.pkg_columnquery.numericValue(t_appObj, 'DailyDocLimit');
      t_userId := api.pkg_securityquery.EffectiveUserId;
      t_docDate := api.pkg_columnquery.dateValue(t_userId, 'LastDocAddedDate');
      if t_docDate >= trunc(sysdate) then
        t_docUpl := api.pkg_columnquery.numericValue(t_userId,
                        'DailyMBUploaded');
      else
        api.pkg_columnUpdate.setValue(t_userId, 'LastDocAddedDate', sysdate);
      end if;
      select uncompressedLength into t_docSize
        from api.documentRevisions
       where documentId = a_DocId;

      if t_docSize + t_docUpl > t_docLimit*1024*1024 then
        api.pkg_errors.raiseError(-20000,
            'This upload surpasses the daily maximum upload size limit.');
      else
        api.pkg_columnUpdate.setValue(t_userId, 'DailyMBUploaded',
                             round(t_docSize/1024/1024, 4) + t_docUpl);
      end if;
    end if;
    api.pkg_columnUpdate.setValue(a_DocId, 'IsNew', 'N');
  end DocUploadLimit;

  /*--------------------------------------------------------------------------
   * AppUploadLimit() -- PUBLIC
   * Limits the number of applications that can be submitted by a user per day.
   * Run from Constructor on all the main job types
   *------------------------------------------------------------------------*/
  procedure AppUploadLimit (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    t_appObj            udt_Id;
    t_userId            udt_Id;
    t_appLimit          number(4);
    t_appCount          number(4) := 0;
    t_appDate           date;
  begin
    t_appObj := api.pkg_SimpleSearch.ObjectByIndex(
                     'o_ApplicationInformation',
                     'dup_MenuCodeWebSiteName',
                     'mm-capsexternal' );
    t_appLimit := api.pkg_columnquery.numericValue(t_appObj, 'DailyAppLimit');
    t_userId := api.pkg_securityquery.EffectiveUserId;
    t_appDate := api.pkg_columnquery.dateValue(t_userId, 'LastNewAppDate');
    if t_appDate >= trunc(sysdate) then
      t_appCount := api.pkg_columnquery.numericValue(t_userId, 'DailyAppCount');
    else
      api.pkg_columnUpdate.setValue(t_userId, 'LastNewAppDate', sysdate);
    end if;

    if t_appCount + 1 > t_appLimit then
      api.pkg_errors.raiseError(-20000,
          'You have surpassed the daily maximum new application limit.' ||
          gc_NewLine ||
          'You may resume submissions tomorrow.');
    else
      api.pkg_columnUpdate.setValue(t_userId, 'DailyAppCount', t_appCount + 1);
    end if;
  end AppUploadLimit;

  /*--------------------------------------------------------------------------
   * TransferAssignments() -- PUBLIC
   * Transfers all job roles and/or process assignments to another user
   * Run from PostVerify on Users
   * Drives logic on the internal Reassign presentation
   *------------------------------------------------------------------------*/
  procedure TransferAssignments (
    a_UserId            udt_Id,
    a_AsOfDate          date
  ) is
    t_newUser           udt_Id;
    t_rel               udt_Id;
    t_count             pls_integer;
    t_oldUserLogon      varchar2(30);
    t_newUserLogon      varchar2(30);
    t_TransferRoles     varchar2(1);
    t_TransferTasks     varchar2(1);
    e_noprivs           exception;
    pragma exception_init(e_noprivs, -20918);
    cursor c_userJobs is
      -- find job role rels for a given user
      select r.relationshipid, r.endpointid, r.toobjectid
        from api.relationships r
        join api.relationshipdefs rd on r.endpointid = rd.toendpointid
        join api.relationshipnames rn
          on rd.fromrelationshipnameid = rn.relationshipnameid
        join api.jobs j on r.toobjectid = j.jobid
       where r.fromobjectid = a_userId
         and rn.relationshipname in ('PC Owner:', 'Planner:', 'Archaeologist:',
             'Palaeontologist:', 'Permit Coordinator:', 'Head Archaeologist:',
             'Verifier:', 'Reviewer:');
  begin
    t_newUser := api.pkg_columnquery.value(a_userId, 'TransferToUser');
    t_transferRoles := api.pkg_columnquery.value(a_userId, 'TransferRoles');
    t_transferTasks := api.pkg_columnquery.value(a_userId, 'TransferTasks');
    -- Transfer job roles
    if t_newUser is not null and t_transferRoles = 'Y' then
      for x in c_userJobs loop
        -- remove rel to current user
        api.pkg_relationshipupdate.remove(x.relationshipid);
        -- create rel to new user if this rel does not already exist
        select count(*) into t_count
          from api.relationships
         where fromobjectid = t_newUser
           and toobjectid = x.toobjectid
           and endpointid = x.endpointid;
        if t_count < 1 then
          t_rel := api.pkg_relationshipupdate.new(x.endpointid,
                       t_newUser, x.toobjectid);
        end if;
      end loop;
      api.pkg_columnUpdate.SetValue(a_UserId, 'TransferRoles', 'N');
    end if;

    -- Transfer process assignments
    if t_newUser is not null and t_transferTasks = 'Y' then
      select oraclelogonid into t_newUserLogon from api.users
       where userid = t_newUser;
      select oraclelogonid into t_oldUserLogon from api.users
       where userid = a_UserId;
      if t_NewUserLogon = t_oldUserLogon then
        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('Operation', 'Transfer Assignments');
        api.pkg_errors.raiseError(-20000,
                         'From User and To User are the same person!');
      end if;
      for x in (select processid,
                       (select count(*)
                          from api.incompleteprocessassignments
                         where assignedto = t_newUser
                           and processid = i.processid) done
                  from api.incompleteprocessassignments i
                 where assignedto = a_userId ) loop
        -- if the user being assigned a task does not have permission
        -- to be assignee, skip it and don't unassign it
        begin
          if x.done = 0 then
            api.pkg_processUpdate.assign(x.processId, t_newUserLogon);
          end if;
          api.pkg_processUpdate.unAssign(x.processId, t_oldUserLogon);
        exception when e_noprivs then
          null;
        end;
      end loop;
      api.pkg_columnUpdate.SetValue(a_UserId, 'TransferTasks', 'N');
    end if;

    if t_newUser is not null then
      api.pkg_columnUpdate.removeValue(a_UserId, 'TransferToUser');
    end if;
  end;


  /*--------------------------------------------------------------------------
   * AccessGroupBasedOnCheckbox() -- PUBLIC
   * This procedure will put in and take out a single user from access groups
   * based on checkboxes on the user object that have the same name as the
   * corresponding access group.  This allows for users to check and un-check
   * checkboxes and then the user will be put in or taken out of access groups
   *------------------------------------------------------------------------*/
  procedure AccessGroupBasedOnCheckboxes (
    a_UserId          udt_Id,
    a_AsOfDate          date
  ) is

  t_UserDefId    udt_Id;
  t_Count          number := 0;

  begin

    t_UserDefId := api.pkg_configquery.objectdefidforname('u_Users');

  --loop through the columns and access groups that have the same name, and get the value of the column
    for c in(select cd.name,
                    ag.description,
                    api.pkg_columnquery.value(a_UserId, cd.name) AddToAccessGroup,
                    ag.AccessGroupId
               from api.accessgroups ag
               join api.columndefs cd on cd.Name = replace(ag.Description, ' ' , '')
                and cd.ObjectDefId = t_UserDefId)loop

    --reset the count
      t_Count := 0;

    --select the count from the x-ref table to see if the user is already in the access group
      select count(1)
        into t_Count
        from api.accessgroupusers agu
       where agu.UserId = a_UserId
         and agu.AccessGroupId = c.AccessGroupId;

    --if the checkbox says to to add them to the access group and they are not already in that access group...
      if c.AddToAccessGroup = 'Y' and t_Count = 0
        then
        --add to the access group
          api.pkg_userupdate.AddToAccessGroup(a_UserId, c.accessgroupid);
    --elsif the checkbox says to remove them and they are in that access group...
      elsif (c.AddToAccessGroup = 'N' and t_Count > 0) or (c.AddToAccessGroup is null and t_Count > 0)
        then
        --remove from the access group
          api.pkg_userupdate.RemoveFromAccessGroup(a_UserId, c.accessgroupid);
      end if;
    end loop;

  end AccessGroupBasedOnCheckboxes;

  /*--------------------------------------------------------------------------
   * CreateInternalUser() -- PUBLIC
   * This procedure will create an Internal user.  It is triggered from the
   * o_ABC_PosseUserCreation object.
   *------------------------------------------------------------------------*/
  procedure CreateInternalUser (
    a_ObjectId          udt_Id,
    a_AsOfDate          date

  ) is
    t_Name              varchar2(50);
    t_Logon             varchar2(50);
    t_FirstName         varchar2(50);
    t_LastName          varchar2(50);
    t_Id                varchar2(200);
    t_Email             varchar2(4000);
    t_Phone             varchar2(10);
    t_NewUserId         udt_Id;
    t_RelId             udt_Id;
    t_nameExists        pls_integer;
    t_ext               pls_integer;
    t_UserId            pls_Integer;
    t_Password          varchar2(30) := trim(api.pkg_columnquery.value(a_objectId, 'Password'));
    t_ConfirmPassword   varchar2(30) := trim(api.pkg_columnquery.value(a_objectId, 'ConfirmPassword'));

  begin

    if api.pkg_columnquery.value(a_ObjectId, 'CreatedUserId') is null then
      if t_Password <> t_ConfirmPassword then
        api.pkg_errors.raiseError(-20000, 'Passwords do not match.');
      end if;

      if instr(t_Password, ' ') > 0 then
        api.pkg_errors.raiseError(-20000, 'Password cannot contain spaces.');
      end if;

      t_Id := api.pkg_columnquery.value(a_objectId, 'AuthenticationName');

      if instr(t_Id, ' ') > 0 then
        api.pkg_errors.raiseError(-20000, 'User Id cannot contain spaces.');
      end if;

      --check that t_Id is unique
      t_UserId := api.pkg_SimpleSearch.ObjectByIndex( 'u_Users', 'OSUserId', t_Id );
      if t_UserId > 0 then
        api.pkg_errors.raiseError(-20000, 'User Id already exists. Please provide a different User Id.');
      end if;
      t_FirstName := api.pkg_columnquery.value(a_objectId, 'FirstName');
      t_LastName := api.pkg_columnquery.value(a_ObjectId, 'LastName');
      t_Email := api.pkg_columnquery.value(a_ObjectId, 'EmailAddress');
      t_Phone := api.pkg_columnquery.value(a_ObjectId, 'PhoneNumber');

      -- The user name is displayed on assignment lists.
      -- Posse insists that it be unique.
      t_name := api.pkg_columnquery.value(a_ObjectId, 'FirstName') || ' ' || api.pkg_columnquery.value(a_ObjectId, 'LastName');
      select count(*) into t_nameExists from api.users
       where upper(name) = upper(t_name);

      if t_nameExists > 0 then
        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('Operation', 'Create Internal User');
        api.pkg_Errors.RaiseError(-20000, 'The First Name and Last Name of the user already exist in the system. '||
            'Please change the name.  Eg: Add middle initials');
      end if;
      t_newUserId := api.pkg_UserUpdate.new('Default', trim(t_Name), trim(t_Id), trim(t_Id), t_Password);

      api.pkg_columnUpdate.setValue(t_newUserId, 'FirstName', t_FirstName);
      api.pkg_columnUpdate.setValue(t_newUserId, 'LastName', t_LastName);
      api.pkg_columnUpdate.setValue(t_newUserId, 'OSUserId', t_Id);
      if t_Email is not null then
        api.pkg_columnUpdate.setValue(t_newUserId, 'EmailAddress', t_Email);
      end if;
      if t_Phone is not null then
        api.pkg_columnUpdate.setValue(t_newUserId, 'PhoneNumber', t_Phone);
      end if;

      --remove the passwords on the temp object
      api.pkg_columnupdate.RemoveValue(a_objectId, 'Password');
      api.pkg_columnupdate.RemoveValue(a_objectId, 'ConfirmPassword');
      api.pkg_columnupdate.SetValue(a_objectId, 'AuthenticationName', '(removed)');

      api.pkg_columnupdate.SetValue(a_objectId, 'CreatedUserId', t_newUserId);
    end if;
  end CreateInternalUser;

  /*--------------------------------------------------------------------------
   * CreateMunicipalUser() -- PUBLIC
   * This procedure will create a Municipal user.  It is triggered from the
   * o_ABC_PosseUserCreation object.
   *------------------------------------------------------------------------*/
  procedure CreateMunicipalUser (
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  ) is
    t_Logon                            varchar2(50);
    t_FirstName                        varchar2(50);
    t_LastName                         varchar2(50);
    t_Name                             varchar2(50);
    t_Phone                            varchar2(10);
    t_CellPhone                        varchar2(10);
    t_Email                            varchar2(4000);
    t_OptOutTextNotifications          varchar2(1);
    t_NewUserId                        udt_Id;
    t_RelId                            udt_Id;
    t_NameExists                       pls_integer;
    t_Id                               varchar2(200);
    t_ext                              pls_integer;
    t_UserId                           pls_Integer;
    t_OfficeObjectId                   pls_Integer;
    t_Password                         varchar2(30);
    t_EncryptPW                        varchar2(32);
    t_ABCMuniAccessGroup               udt_Id;

  begin

    if api.pkg_ColumnQuery.Value(a_ObjectId, 'CreatedUserId') is null then

      -- Check if AuthenticationName has spaces
      t_Id := api.pkg_ColumnQuery.Value(a_objectId, 'AuthenticationName');
      if instr(t_Id, ' ') > 0 then
        api.pkg_errors.raiseError(-20000, 'User Id cannot contain spaces.');
      end if;

      -- Check that t_Id is unique
      t_UserId := api.pkg_SimpleSearch.ObjectByIndex('u_Users', 'OSUserId', t_Id);
      if t_UserId > 0 then
        api.pkg_errors.raiseError(-20000, 'User Id already exists. Please provide a different User Id.');
      end if;

      t_FirstName := trim(api.pkg_ColumnQuery.Value(a_objectId, 'FirstName'));
      t_LastName := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'LastName'));
      t_Phone := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'PhoneNumber'));
      t_CellPhone := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'CellPhone'));
      t_OptOutTextNotifications := api.pkg_ColumnQuery.Value(a_ObjectId, 'OptOutTextNotifications');
      t_Email := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'EmailAddress'));
      t_OfficeObjectId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'OfficeObjectId');

      -- The user name is displayed on assignment lists.
      -- Posse insists that it be unique.
      t_Name := api.pkg_ColumnQuery.Value(a_ObjectId, 'FirstName') || ' ' ||
          api.pkg_ColumnQuery.Value(a_ObjectId, 'LastName');

      select count(*)
      into t_NameExists
      from api.Users
      where upper(name) = upper(t_Name);

      if t_NameExists > 0 then
        api.pkg_Errors.Clear();
        api.pkg_Errors.SetArgValue('Operation', 'Create Internal User');
        api.pkg_Errors.RaiseError(-20000, 'The First Name and Last Name of the user already ' ||
          'exist in the system. Please change the name.  Eg: Add middle initials');
      end if;

      -- Check Email: no email for Police Users
      ValidateUserEmail(a_ObjectId, sysdate);

      -- Generate temporary pw for system
      t_Password := (dbms_random.string('x', round(dbms_random.value(5, 10))) ||
          chr(round(dbms_random.value(35,47))) ||
          dbms_random.string('a', round(dbms_random.value(5, 10))) ||
          chr(round(dbms_random.value(64,94))));
      t_EncryptPW := encrypt(t_Password);

      -- Create a new Municipality User
      t_NewUserId := api.pkg_UserUpdate.New('MunicipalityInternetUsers', trim(t_Name), trim(t_Id),
          trim(t_Id), t_EncryptPW);
      api.pkg_ColumnUpdate.SetValue(t_NewUserId, 'PasswordWeb', t_EncryptPW);
      api.pkg_ColumnUpdate.SetValue(t_NewUserId, 'UserType', 'Municipal');
      api.pkg_ColumnUpdate.SetValue(t_NewUserId, 'FirstName', t_FirstName);
      api.pkg_ColumnUpdate.SetValue(t_NewUserId, 'LastName', t_LastName);
      api.pkg_ColumnUpdate.SetValue(t_NewUserId, 'OSUserId', t_Id);
      api.pkg_ColumnUpdate.SetValue(t_NewUserId, 'OptOutTextNotifications',
          t_OptOutTextNotifications);
      if t_Email is not null then
        api.pkg_ColumnUpdate.SetValue(t_newUserId, 'EmailAddress', t_Email);
      end if;
      if t_Phone is not null then
        api.pkg_ColumnUpdate.SetValue(t_newUserId, 'PhoneNumber', t_Phone);
      end if;
      if t_CellPhone is not null then
        api.pkg_ColumnUpdate.SetValue(t_newUserId, 'BusinessPhone', t_CellPhone);
      end if;
      -- Relate Municipality
      if t_OfficeObjectId is not null then
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndPointIdForName('u_Users',
            'Municipality'), t_NewUserId, t_OfficeObjectId);
      end if;

      -- Add Municipal User to correct Access Group
      select AccessGroupId
      into t_ABCMuniAccessGroup
      from api.AccessGroups
      where Description = 'Municipal Users';
      api.pkg_UserUpdate.AddToAccessGroup(t_NewUserId, t_ABCMuniAccessGroup);

      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'AuthenticationName', '(removed)');
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'CreatedUserId', t_NewUserId);
    end if;
  end CreateMunicipalUser;

  /*---------------------------------------------------------------------------
  * AuthenticateMuni() -- PUBLIC
  * UMS - External
  *-------------------------------------------------------------------------*/
  procedure AuthenticateMuni (
    a_User        udt_Id,
    a_OracleLogon      varchar2,
    a_Password        varchar2,
    a_ConnectString      varchar2
    ) is
    t_EncryptedPassword      varchar2(4000);
    t_Error        varchar2(4000);
    t_Message        varchar2(4000);
    t_Active        varchar2(1);
    t_LockoutTime                       date;
  begin
pkg_debug.putline('#####: AuthenticateExt at: ' || to_char(systimestamp, 'HH24:MI:SS:FF6'));
    AuthenticateCheck(a_OracleLogon, 'external');
--raise_Application_error(-20000, a_OracleLogon || ' ' || a_User);
    select min(LockoutDate) into t_LockoutTime from internetUsers
     where userId = a_user;
    if t_LockoutTime > sysdate then
      api.pkg_Errors.Clear();
      api.pkg_Errors.SetArgValue('Operation', 'Check Timeout');
      api.pkg_Errors.SetArgValue('OracleLogonId', a_OracleLogon);
      api.pkg_Errors.SetArgValue('Timeout',to_char(t_LockoutTime,'hh24:mi:ss'));
      api.pkg_Errors.RaiseError(-20000,
          'Too many failures:  You may not attempt to log on again until '||
           to_char(t_LockoutTime, 'hh:mi:ss am')||'.');
    end if;

    -- Get the stored encrypted password
    select active, api.pkg_columnquery.value(userid, 'PasswordWeb')
      into t_active, t_EncryptedPassword
      from api.users where userid = a_User;

    -- Test for active
    if t_active = 'N' then
      raise no_data_found;
    end if;

    if a_OracleLogon <> 'WEBGUEST' then
      -- Test for nulls:  this should never happen
      if t_EncryptedPassword is null or a_Password is null then
--        raise no_data_found;
api.pkg_errors.raiseerror(-20000, 'TEST ERROR  Encrypted Pwd and Pwd:' || t_EncryptedPassword || ' and ' || a_Password || '  Active: ' || t_Active);
      end if;

      -- Test password
      if t_EncryptedPassword <> Encrypt(a_Password) then
        -- increment failed password count
        PwdFail(a_user);
        raise no_data_found;
      end if;

      -- Login success
      update internetUsers
         set Retries = 0, LockoutDate = null
       where userid = a_user;
      if sql%rowcount = 0 then
        insert into internetUsers (userId, retries, lockoutDate) values
          (a_user, 0, null);
      end if;

      commit;
    end if;
    pkg_debug.putline('External Login succeeded');
  exception when no_data_found then
    pkg_debug.putline('External Login failed');
    t_Error := sqlerrm;
    t_Message := 'Authentication failed';
    api.pkg_Errors.Clear();
    api.pkg_Errors.SetArgValue('OriginalError', t_Error);
    api.pkg_Errors.SetArgValue('Operation', 'Authenticate');
    api.pkg_Errors.SetArgValue('OracleLogonId', a_OracleLogon);
    api.pkg_Errors.RaiseError(-20000, t_Message);
  end AuthenticateMuni;

  /*---------------------------------------------------------------------------
  * ValidateEmailForNotif() -- PUBLIC
  * On Admin site, Internal User presentation
  * If any Email Notifications are checked, but the user does not have an
  * email address, raise an error.
  *
  * Called on post verify of the relationship or on change of the Email Address
  *-------------------------------------------------------------------------*/
  procedure ValidateEmailForNotif (
    a_ObjectID            udt_Id,
    a_AsAtDate            date,
    a_UserId              udt_Id default null
  ) is
    t_EmailAddress        varchar2(4000);
    t_EmailNotifCount     number := 0;
  begin
    --api.pkg_Errors.RaiseError(-20000, 'ObjectId: ' || a_ObjectId || ' UserId: ' || a_UserID);

    if a_UserId is not null then
      begin
        -- From change of Email Address on User object
        select sum(case when p.EmailAssignments = 'Y' then 1 else 0 end)
             , api.pkg_columnQuery.Value(p.UserId, 'EmailAddress')
          into t_EmailNotifCount, t_EmailAddress
          from query.r_abcprocessemailuser p
         where p.UserId = a_UserId
         group by p.UserId;
      exception when no_data_found then
        -- If user has no relationships to ABC Process Email, then the user is being newly
        -- created or it is External/Municipal/Referral. Do not throw no data found error.
        null;
      end;
    else
      -- Assume we have come from the PosseEmail relationship to User
      select sum(case when p.EmailAssignments = 'Y' then 1 else 0 end)
           , api.pkg_columnQuery.Value(p.UserId, 'EmailAddress')
        into t_EmailNotifCount, t_EmailAddress
        from query.r_abcprocessemailuser p
       where p.RelationshipId = a_ObjectId
       group by p.UserId;
    end if;

    if t_EmailAddress is null and t_EmailNotifCount>0 then
      api.pkg_errors.RaiseError(-20000, 'Please ensure this user has an email address specified to receive assignment notifications.');
    end if;

  end ValidateEmailForNotif;

/*---------------------------------------------------------------------------
  * TrackInvalidAttempts() - Public
  *-------------------------------------------------------------------------*/
  procedure TrackInvalidAttempts (
    a_User            udt_Id,
    a_OracleLogon     varchar2,
    a_Password        varchar2,
    a_ConnectString   varchar2
  )is
    t_EncryptedPassword      varchar2(4000);
    t_LockoutTime            date;
  begin
        --call password failure
        if a_OracleLogon <> 'ABCWEBGUEST' then
        PwdFail (a_User);
        end if;

         --call internal/admin authentication
         toolboxnew.pkg_userupdate.Authenticate(a_User, a_OracleLogon, a_Password, a_ConnectString);

          select min(LockoutDate) into t_LockoutTime from internetUsers
           where userId = a_user;

          if t_LockoutTime > sysdate then
            api.pkg_Errors.Clear();
            api.pkg_Errors.SetArgValue('Operation', 'Check Timeout');
            api.pkg_Errors.SetArgValue('OracleLogonId', a_OracleLogon);
            api.pkg_Errors.SetArgValue('Timeout',to_char(t_LockoutTime,'hh24:mi:ss'));
            api.pkg_Errors.RaiseError(-20000,
                'Too many failures:  You may not attempt to log on again until '||
                 to_char(t_LockoutTime, 'hh:mi:ss am')||'.');
          else
           -- Login success
              update internetUsers
                 set Retries = 0, LockoutDate = null
               where userid = a_user;
              if sql%rowcount = 0 then
                insert into internetUsers (userId, retries, lockoutDate) values
                  (a_user, 0, null);
              end if;

              --Update last Successful Login
              TrackLastSuccessfulLogin(a_User, sysdate);
           end if;
  end;


  /*---------------------------------------------------------------------------
  * Unlock_Oracle_User() - Used to unlock an oracle account
  *-------------------------------------------------------------------------*/
  procedure Unlock_Oracle_User(
    a_userName varchar2)
  is pragma autonomous_transaction;
  begin
    execute immediate 'alter user ' || a_userName || ' account unlock';
  end;


   /*---------------------------------------------------------------------------
   * UnlockAccount()
   *-------------------------------------------------------------------------*/
  procedure UnlockAccount (
    a_ObjectId          udt_Id,
    a_AsOfDate          date
  ) is
    t_userName          varchar2(4000);
    t_userType          varchar2(4000);
  begin
      update   abc.internetUsers iu
      set      iu.retries = 0,
               iu.lockoutdate = null
      where    iu.userid = a_ObjectId;
      if sql%rowcount = 0 then
        raise_application_error(-20000, 'Error - Cannot find user specified');
      end if;

      select u.OracleLogonId, u.UserType into t_userName, t_userType from query.u_users u where u.UserId = a_ObjectId;
      if t_userType = 'Internal' then
        Unlock_Oracle_User(t_userName);
      end if;
  end;

   /*---------------------------------------------------------------------------
   * TrackLastSuccessfulLogin()
   *-------------------------------------------------------------------------*/
  procedure TrackLastSuccessfulLogin (
    a_User            udt_Id,
    a_AsOfDate          date
  ) is
  begin

      update   abc.internetUsers iu
      set      iu.lastsuccessfullogin = a_AsOfDate
      where    iu.userid = a_User;
  end;

  /*---------------------------------------------------------------------------
   * UpdateAuditDate()
   *-------------------------------------------------------------------------*/
  procedure UpdateAuditDate (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  ) is
  begin
       update   abc.Loginaudit la
       set      la.auditdate = sysdate
       where    la.objectid = a_ObjectId;
  end;

   /*---------------------------------------------------------------------------
   * CreateLockAuditLogs()
   *-------------------------------------------------------------------------*/
  procedure CreateLockAuditLogs is
     t_NewAuditID  udt_id;
     begin
         api.pkg_logicaltransactionupdate.ResetTransaction;
         for rec in (select * from abc.loginaudit where objectid is null) loop
             t_NewAuditID := api.pkg_objectupdate.new(api.pkg_objectdefquery.IdForName('o_LoginAudit'));
             update abc.loginaudit set objectid = t_NewAuditID where auditdate = rec.auditdate and account = rec.account;
          end loop;
          api.pkg_logicaltransactionupdate.EndTransaction;
     end;

   /*---------------------------------------------------------------------------
   * RemOutstandingFeeLegalEntity()
   *   Removes Outstanding Fee LegalEntity - User relationship if it exists
   *-------------------------------------------------------------------------*/
  procedure RemOutstandingFeeLegalEntity (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  ) is
    t_RelationshipDefId integer;
    t_Label             varchar2(4000) := 'Outstanding Fee Legal Entity:';

  begin
     begin
        select rd.RelationshipDefId
          into t_RelationshipDefId
          from api.relationshipdefs rd
         where rd.ToLabel = t_Label;
     exception
        when no_data_found then
           api.pkg_errors.RaiseError(-20000,'No RelationshipDef found for ' || t_Label);
     end;

     for r1 in (select distinct(r.RelationshipId)
                  from api.relationships r
                 where r.RelationshipDefId = t_RelationshipDefId
                   and r.FromObjectId      = a_ObjectId) loop
        api.pkg_relationshipupdate.Remove(r1.relationshipid);
     end loop;

  end RemOutstandingFeeLegalEntity;

   /*---------------------------------------------------------------------------
   * RemoveLegalEntity()
   *   Removes LegalEntity - Public User relationship if selected
   *-------------------------------------------------------------------------*/
  procedure RemoveLegalEntity (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  ) is

  begin
     for r1 in (select distinct(r.RelationshipId)
                from   query.r_abc_userlegalentity r
                where  r.UserId = a_ObjectId
                and    r.RemoveFromPublicUser = 'Y'
               )loop
        api.pkg_relationshipupdate.Remove(r1.relationshipid);
     end loop;

  end RemoveLegalEntity;

  /*---------------------------------------------------------------------------
  * CreatePoliceUser()
  * Create a new Police user
  * It is triggered from the o_ABC_PosseUserCreation object.
  *--------------------------------------------------------------------------*/
  procedure CreatePoliceUser(
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  ) is
    t_RankTitle                        varchar2(50);
    t_FirstName                        varchar2(50);
    t_LastName                         varchar2(50);
    t_Name                             varchar2(110);
    t_Email                            varchar2(4000);
    t_Phone                            varchar2(10);
    t_CellPhone                        varchar2(10);
    t_OptOutTextNotifications          varchar2(1);
    t_Extension                        varchar2(6);
    t_RelId                            udt_Id;
    t_NameExists                       pls_integer;
    t_UserId                           pls_Integer;
    t_Password                         varchar2(30);
    t_EncryptPW                        varchar2(32);
    t_Id                               varchar2(200);
    t_EndPointId                       integer;
    t_ABCPoliceAccessGroup             udt_Id;

  begin

    if api.pkg_ColumnQuery.Value(a_ObjectId, 'CreatedUserId') is null then
      t_RankTitle := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'RankTitle'));
      t_FirstName := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'FirstName'));
      t_LastName := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'LastName'));
      t_Name := t_FirstName || ' ' || t_LastName;
      t_Email := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'EmailAddress'));
      t_Phone := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'PhoneNumber'));
      t_CellPhone := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'CellPhone'));
      t_OptOutTextNotifications := api.pkg_ColumnQuery.Value(a_ObjectId, 'OptOutTextNotifications');
      t_Extension := trim(api.pkg_ColumnQuery.Value(a_ObjectId, 'Extension'));
      t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName('u_Users', 'MunicipalityPolice');

      t_Id := upper(substr(t_FirstName, 0, 1)) || upper(substr(t_LastName, 0, 1))
          || a_ObjectId;
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'AuthenticationName', t_Id);
      if instr(t_Id, ' ') > 0 then
        api.pkg_errors.RaiseError(-20000, 'User Id cannot contain spaces.');
      end if;
      --check that t_Id is unique
      t_UserId := api.pkg_SimpleSearch.ObjectByIndex( 'u_Users', 'OSUserId', t_Id);
      if t_UserId > 0 then
        api.pkg_errors.RaiseError(-20000, 'User Id already exists. Please provide'
            || ' a different User Id.');
      end if;

      -- Check Email: no email for Police or Public Users, if Muni User with same
      -- email then de-activate the Municipal User
      select count(*)
      into t_NameExists
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('u_Users',
          'EmailAddress', t_Email) as api.udt_ObjectList)) x
        join query.u_Users u
            on u.ObjectId = x.ObjectId
      where  u.UserType in ('Public', 'Police');

      if t_NameExists > 0 then
         api.pkg_Errors.RaiseError(-20000, 'Email address has already been used '
             || 'for another user.');
      end if;
      -- Check Email: Deactivate Municipal User if present with same email
      ValidateUserEmail(a_ObjectId, sysdate);

      -- Generate temporary pw for system
      t_Password := (dbms_random.string('x', round(dbms_random.value(5, 10))) ||
          chr(round(dbms_random.value(35,47))) ||
          dbms_random.string('a', round(dbms_random.value(5, 10))) ||
          chr(round(dbms_random.value(64,94)))); --ADDED
      t_EncryptPW := encrypt(t_Password);

      -- Create a new Police User
      t_UserId := api.pkg_UserUpdate.New(
          a_UserManagementScheme => 'PublicInternetUsers',
          a_Name                 => substr(trim(t_Name), 0, 50) || t_Id || '_P',
          a_ShortName            => t_Id,
          a_AuthenticationName   => t_id,
          a_Password             => t_EncryptPW);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'PasswordWeb' ,t_EncryptPW);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'UserType', 'Police');
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'Active', 'N');
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'AwaitingMuniActivation', 'Y');
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'RankTitle', t_Ranktitle);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'FirstName', t_Firstname);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'LastName', t_Lastname);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'PhoneNumber', t_Phone);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'ExtensionNumber', t_Extension);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'BusinessPhone', t_Cellphone);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'OptOutTextNotifications', t_OptOutTextNotifications);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'EmailAddress', t_Email);
      api.pkg_ColumnUpdate.SetValue(t_UserId, 'OSUserId', t_Id);

      -- Relate Municipalities
      for i in (select *
                from   query.r_posseusercreationofficepolic r
                where  r.PosseUserCreationObjectId = a_ObjectId
               ) loop
          t_RelId := api.pkg_RelationshipUpdate.New(a_EndPoint   => t_EndPointId,
                                                   a_FromObject => t_UserId,
                                                   a_ToObject   => i.officeobjectid,
                                                   a_AsOfDate   => sysdate);
      end loop;

      -- Add Police User to correct Access Group
      select AccessGroupId
      into t_ABCPoliceAccessGroup
      from   api.AccessGroups
      where  Description = 'Police Users';
      api.pkg_UserUpdate.AddToAccessGroup(t_UserId, t_ABCPoliceAccessGroup);

      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'AuthenticationName', '(removed)');
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'CreatedUserId', t_UserId);
    end if;

  end CreatePoliceUser;

   /*---------------------------------------------------------------------------
   * RemoveMunicipality()
   *   Removes Municipality - Police User relationship if selected
   *-------------------------------------------------------------------------*/
  procedure RemoveMunicipality (
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  ) is

  begin
     for r1 in (select distinct(r.RelationshipId)
                from   query.r_PoliceUserMunicipality r
                where  r.PoliceUserObjectId = a_ObjectId
                and    r.RemoveFromPoliceUser = 'Y'
               )loop
        api.pkg_relationshipupdate.Remove(r1.relationshipid);
     end loop;
     for r1 in (select distinct(r.RelationshipId)
                from   query.r_PosseUserCreationOfficePolic r
                where  r.PosseUserCreationObjectId = a_ObjectId
                and    r.RemoveFromPoliceUser = 'Y'
               )loop
        api.pkg_relationshipupdate.Remove(r1.relationshipid);
     end loop;
  end RemoveMunicipality;

  /*---------------------------------------------------------------------------
  * ValidateUserEmail()
  * Deactivates Municipal User if Police User with same email is created.
  *--------------------------------------------------------------------------*/
  procedure ValidateUserEmail(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_Email                             varchar2(4000) :=
        api.pkg_columnquery.Value(a_ObjectId, 'EmailAddress');
    t_PoliceUser                        varchar2(1) :=
        api.pkg_columnquery.Value(a_ObjectId, 'IsPoliceUser');
    t_MunicipalUser                     varchar2(1)
        := api.pkg_columnquery.Value(a_ObjectId, 'IsMunicipalUser');
    t_NameExists                        number;
  begin

    if t_PoliceUser = 'Y' then
      -- No existing email for Police or Public Users allowed
      select count(*)
      into t_nameExists
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('u_Users',
          'EmailAddress', t_Email) as api.udt_ObjectList)) x
      join query.u_users u
          on u.ObjectId = x.objectid
      where u.usertype in ('Public', 'Police')
        and u.ObjectId != a_ObjectId;
      if t_nameExists > 0 then
         api.pkg_Errors.RaiseError(-20000, 'Email address has already been used'
             || ' for another user.');
      end if;
      -- Deactivate any Municipal User(s) with same email
      for u1 in (
          select u.ObjectId
          from  table(cast(api.pkg_simplesearch.castableobjectsbyindex('u_Users',
              'EmailAddress', t_Email) as api.udt_ObjectList)) x
          join query.u_users u
              on u.ObjectId = x.objectid
          where u.usertype = 'Municipal'
          ) loop
        api.pkg_columnupdate.SetValue(u1.objectid, 'Active', 'N');
      end loop;
    end if;

    if t_MunicipalUser = 'Y' then
      -- Check Email: can not use email if active Police User exists with same
      select count(*)
      into t_nameExists
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('u_Users',
          'EmailAddress', t_Email) as api.udt_ObjectList)) x
      join query.u_users u
          on u.ObjectId = x.objectid
      where u.usertype = 'Police'
        and u.Active = 'Y';
      if t_nameExists > 0 then
        api.pkg_Errors.RaiseError(-20000, 'Email address has already been used'
            ||' for a Police user and can not be used for a Municipal user.');
      end if;
    end if;

  end ValidateUserEmail;

end pkg_ABC_Users;
/
