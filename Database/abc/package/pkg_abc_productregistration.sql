create or replace package abc.PKG_ABC_ProductRegistration is

  /*---------------------------------------------------------------------------
   * Public type declarations
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_definition.udt_StringList;

  /*---------------------------------------------------------------------------
   * PostVerify()
   *   Wrapper for the Post Verify event on the Product Registration job
   *-------------------------------------------------------------------------*/
  procedure PostVerify (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * PostVerifyErrors()
   *   Wrapper for the Post Verify event on the Product Registration job
   *-------------------------------------------------------------------------*/
  procedure PostVerifyErrors (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * PostVerifyAmendment()
   *   Wrapper for the Post Verify event on the Product Registration Amendment
   * job
   *-------------------------------------------------------------------------*/
  procedure PostVerifyAmendment (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * PostVerifyAmendmentErrors()
   *   Wrapper for the Post Verify event on the Product Registration Amendment
   * job
   *-------------------------------------------------------------------------*/
  procedure PostVerifyAmendmentErrors (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /****************************************************************************
   * SetRegistrantRelationship()
   *   Sets the Registrant for a Product Registration job based on the selected
   * Legal Entity on the public application. Run on UseLegalEntityObjectId
   ***************************************************************************/
  procedure SetRegistrantRelationship(
  a_ObjectId           udt_Id,
  a_AsOfDate           date
  );

  /****************************************************************************
   * RelRemoveLicense()
   *   Removes any license rels from the Product Registration job.
   * Runs when the relationship is changed or removed.
   ***************************************************************************/
  procedure RelRemoveLicense(
    a_ObjectId             udt_Id,
    a_AsOfDate             date
  );

  /*---------------------------------------------------------------------------
   * ResetLicense()
   *-------------------------------------------------------------------------*/
  procedure ResetLicense(
    a_ObjectId     udt_Id,
    a_AsOfDate     date
  );

  /*---------------------------------------------------------------------------
   * JobRemoveLicense()
   *   Removes any license rels from the Product Registration job.
   * Runs on the job itself.
   *-------------------------------------------------------------------------*/
  procedure JobRemoveLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * JobRemovePermit()
   *   Removes any Permit rels from the Product Registration job.
   * Runs on the job itself.
   *-------------------------------------------------------------------------*/
  procedure JobRemovePermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /****************************************************************************
   * JobRemoveProducts()
   *   Removes any product rels from the Product Registration Amendment job.
   * Runs on the job itself.
   ***************************************************************************/
  procedure JobRemoveProducts(
    a_ObjectId             udt_Id,
    a_AsOfDate             date
  );

  /*---------------------------------------------------------------------------
   * OnlineJobRemoveLicense() -- PUBLIC
   *   Removes any license rels from the Online Product Registration job.
   * Runs on the job itself.
   *-------------------------------------------------------------------------*/
  procedure OnlineJobRemoveLicense(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  );

  /*---------------------------------------------------------------------------
   * OnlineJobRemovePermit() -- PUBLIC
   *   Removes any permit rels from the Online Product Registration job.
   * Runs on the job itself.
   *-------------------------------------------------------------------------*/
  procedure OnlineJobRemovePermit(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  );

  /*---------------------------------------------------------------------------
   * RemoveRelsOnLicensePermitRadioButtons() -- PUBLIC
   *   Removes any permit or Licence rels from the Online Product Registration 
   * job based on the License/Pemrit Information radio buttons.
   * Run on change of OnlineRegistrantHasNJLicense and 
   * OnlineRegistrantHasNJTAPPermit
   *-------------------------------------------------------------------------*/
   procedure RemoveRelsOnLicensePermitRadioButtons(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  );

  /****************************************************************************
   * AutomationOnApproval()
   *   Activates products on approval of Product Registration job,
   * removes vintages on non-distilled spirits products
   ***************************************************************************/
  procedure AutomationOnApproval(
    a_ObjectId             udt_Id,
    a_AsOfDate             date
  );

  /****************************************************************************
   * CreateExpirationDate()
   *   Calculates the expiration date for products, etc. from the system settings
   ***************************************************************************/
  function CreateExpirationDate return date;

  /****************************************************************************
   * CreateOnlineProducts()
   *   Creates Online Products as a record of what the applicant entered in the
   * online application.
   ***************************************************************************/
  procedure CreateOnlineProducts(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
    );

  /****************************************************************************
   * CreateNonRenewalNoticeDate()
   *   Calculates the Non-Renewal Date based on the Expiration Date
   ***************************************************************************/
  function CreateNonRenewalNoticeDate(
    t_ExpDate               date
  ) return date;

  /****************************************************************************
   * AmendOnApproval()
   *   Amends products on approval of Product Registration Amendment job
   * to the new Distributors
   ***************************************************************************/
  procedure AmendOnApproval(
    a_ObjectId             udt_Id,
    a_AsOfDate             date
  );

  /*---------------------------------------------------------------------------
   * AmendCheckRels()
   *   Checks the relationships of Products to Distributors on the Product
   * Registration Amendment job. If a relationship exists between the Products
   * and Distributors, an error is raised.
   *-------------------------------------------------------------------------*/
  procedure AmendCheckRels (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CreateOnlineAmendProductRels()
   *   Create a new relationship between the Product and Online PR Amendment
   * Job upon submission.
   *-------------------------------------------------------------------------*/
  procedure CreateOnlineAmendProductRels (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end PKG_ABC_ProductRegistration;
/

create or replace package body abc.PKG_ABC_ProductRegistration is

  /*---------------------------------------------------------------------------
   * PostVerify() -- PUBLIC
   *   Wrapper for the Post Verify event on the Product Registration job
   *-------------------------------------------------------------------------*/
  procedure PostVerify (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_Clerk                             varchar2(4000);
    t_ClerkId                           number(9);
    t_HasLicenseeType                   varchar2(1);
    t_HasNJLicense                      varchar2(3);
    t_HasNJTAP                          varchar2(3);
    t_HasNJPresence                     varchar2(3);
    t_IsSolicitingInNJ                  varchar2(3);
    t_JobId                             udt_Id;
    t_LEObjectId                        udt_Id;
    t_PRAppToClerkEndpointId            udt_Id
        := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication', 'ProductRegClerk');
    t_PRAppToLETypeEndpointId           udt_Id
        := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication', 'LegalEntityType');
    t_RelId                             udt_Id;
  begin

    t_JobId := a_ObjectId;
    t_Clerk := api.pkg_columnquery.Value(t_JobId, 'DefaultClerk');
    t_HasLicenseeType := extension.pkg_RelUtils.StoredRelExistsViaEPNames(t_JobId,
        'LegalEntityType');
    t_HasNJLicense := api.pkg_ColumnQUery.Value(t_JobId, 'RegistrantHasNJLicense');
    t_HasNJTAP := api.pkg_ColumnQUery.Value(t_JobId, 'RegistrantHasNJTAPPermit');
    t_HasNJPresence := api.pkg_ColumnQUery.Value(t_JobId, 'RegistrantHasNJPresence');
    t_IsSolicitingInNJ := api.pkg_ColumnQUery.Value(t_JobId, 'RegistrantSolicitingInNJ');
    t_LEObjectId := api.pkg_columnquery.Value(t_JobId, 'OnlineUseLegalEntityObjectId');

    -- Set the Default Clerk
    if t_Clerk is null then
      -- Get the clerk user id
      select r.UserId
      into t_ClerkId
      from
        query.o_SystemSettings o
        join query.r_DefaultPRClerkSysSetting r
            on o.ObjectId = r.SystemSettingsObjectId
      where o.ObjectDefTypeId = 1
        and rownum = 1;
      -- Create relationship
      t_RelId := api.pkg_relationshipupdate.New(t_PRAppToClerkEndpointId, t_JobId, t_ClerkId);
    end if;

    -- Clean up Registrant Information
    if t_HasNJLicense = 'Yes' then
      abc.pkg_ABC_ProductRegistration.JobRemovePermit(t_JobId, a_AsOfDate);
      if t_HasNJTAP is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_JobId, 'RegistrantHasNJTAPPermit');
      end if;
      if t_HasNJPresence is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_JobId, 'RegistrantHasNJPresence');
      end if;
      if t_IsSolicitingInNJ is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_JobId, 'RegistrantSolicitingInNJ');
      end if;
    elsif t_HasNJLicense = 'No' then
      abc.pkg_ABC_ProductRegistration.JobRemoveLicense(t_JobId, a_AsOfDate);
    end if;

    if t_HasNJTAP = 'Yes' then
      abc.pkg_ABC_ProductRegistration.JobRemoveLicense(t_JobId, a_AsOfDate);
      if t_HasNJPresence is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_JobId, 'RegistrantHasNJPresence');
      end if;
      if t_IsSolicitingInNJ is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_JobId, 'RegistrantSolicitingInNJ');
      end if;
    elsif t_HasNJTAP = 'No' then
      abc.pkg_ABC_ProductRegistration.JobRemovePermit(t_JobId, a_AsOfDate);
    end if;

    -- If LicenseeType is related to job and OnlineUseLegalEntityObjectId is not null,
    -- remove the LicenseeType relationship
    if t_HasLicenseeType = 'Y' and t_LEObjectId is not null then
      for x in (
          select r.RelationshipId
          from api.relationships r
          where r.FromObjectId = a_ObjectId
            and r.EndPointId = t_PRAppToLETypeEndpointId
          ) loop
        api.pkg_relationshipupdate.Remove(x.relationshipid);
      end loop;
    end if;

  end PostVerify;

  /*---------------------------------------------------------------------------
   * PostVerifyErrors() -- PUBLIC
   *   Wrapper for the Post Verify event on the Product Registration job
   *-------------------------------------------------------------------------*/
  procedure PostVerifyErrors (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_CancelJob                         varchar2(1);
    t_DistributorCount                  number;
    t_ObjectIsLockedDown                varchar2(1);
    t_ProductCount                      number;
  begin

    t_CancelJob := api.pkg_columnquery.value(a_ObjectId, 'CancelJob');
    t_ObjectIsLockedDown := api.pkg_columnquery.value(a_ObjectId, 'ObjectIsLockedDown');

    select count(1)
    into t_ProductCount
    from query.r_ABC_ProdRegToProduct r
    where r.ProdRegJobId = a_ObjectId;

    select sum(count)
    into t_DistributorCount
    from (
        select count(1) count
        from query.r_ABC_ProdRegToDistributor r
        where r.ProdRegJobId = a_ObjectId
        union
        select count(1) count
        from query.r_ABC_ProductRegTAPDist r
        where r.ProductRegistrationId = a_ObjectId
        );

    --Raise Errors for PR Application Job
    if api.pkg_columnquery.value(a_ObjectId, 'SubmitInternal') = 'Y' then
      if api.pkg_columnquery.value(a_ObjectId, 'EnteredOnline') = 'Y' then
        null;
      else
        -- Select Registrant
        if api.pkg_columnquery.value(a_ObjectId, 'Registrant') is not null or t_CancelJob = 'Y'
            or t_ObjectIsLockedDown = 'Y' then
          null;
        else
          api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must select a Registrant.');
        end if;

        -- Registrant has License
        if api.pkg_Columnquery.value(a_ObjectId, 'RegistrantHasNJLicense') is not null
            or t_CancelJob = 'Y' or t_ObjectIsLockedDown = 'Y' then
          null;
        else
          api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must specify whether the ' ||
              'Registrant has a New Jersey License.');
        end if;
        if api.pkg_Columnquery.value(a_ObjectId, 'RegistrantHasNJLicense') != 'Yes'
            or api.pkg_Columnquery.value(a_ObjectId, 'License') is not null
            or api.pkg_Columnquery.value(a_ObjectId, 'InternalLicense') is not null then
          null;
        else
          api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must select a License.');
        end if;

        -- Registrant has TAP
        if api.pkg_Columnquery.value(a_ObjectId, 'RegistrantHasNJTAPPermit') is not null
            or t_CancelJob = 'Y' or t_ObjectIsLockedDown = 'Y' then
          null;
        else
          api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must specify whether the ' ||
              'Registrant has a New Jersey TAP Permit.');
        end if;
        if api.pkg_Columnquery.value(a_ObjectId, 'RegistrantHasNJTAPPermit') != 'Yes'
            or api.pkg_Columnquery.value(a_ObjectId, 'TAPPermitNumber') is not null then
          null;
        else
          api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must select a TAP Permit.');
        end if;

        -- Registrant does not have License or TAP
        if api.pkg_ColumnQuery.Value(a_ObjectId, 'RegistrantHasNJLicense') = 'No'
            and api.pkg_ColumnQuery.Value(a_ObjectId, 'RegistrantHasNJTAPPermit') = 'No' then
          -- Registrant has commercial premise/stores product in NJ
          if api.pkg_ColumnQuery.Value(a_ObjectId, 'RegistrantHasNJPresence') is not null
              or t_CancelJob = 'Y' or t_ObjectIsLockedDown = 'Y' then
            null;
          else
            api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must specify whether the ' ||
                'Registrant has a commercial premise or stores product in New Jersey.');
          end if;
          if api.pkg_ColumnQuery.Value(a_ObjectId, 'RegistrantHasNJPresence') != 'Yes' then
            null;
          else
            api.pkg_Errors.RaiseError(-20000, 'The Registrant cannot have a commercial premise ' ||
                'or store product in NJ without a License or a TAP Permit.');
          end if;

          -- Registrant solicits sales in NJ
          if api.pkg_ColumnQuery.Value(a_ObjectId, 'RegistrantSolicitingInNJ') is not null
              or t_CancelJob = 'Y' or t_ObjectIsLockedDown = 'Y' then
            null;
          else
            api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must specify whether the ' ||
                'Registrant is soliciting sales in New Jersey.');
          end if;
          if api.pkg_ColumnQuery.Value(a_ObjectId, 'RegistrantSolicitingInNJ') != 'Yes' then
            null;
          else
            api.pkg_Errors.RaiseError(-20000, 'The Registrant cannot solicit sales in NJ ' ||
                'without a License or a TAP Permit.');
          end if;
        end if;

        -- Products
        if t_ProductCount > 0 or t_ObjectIsLockedDown = 'Y' then
          null;
        else
          api.pkg_errors.raiseerror(-20000, 'Before you go on, you must enter at least one ' ||
              'Product.');
        end if;

        if t_ProductCount > 0 then
          for i in (
              select
                p.Name,
                p.Type,
                p.TTBCOLA,
                p.TTBExplanation
              from
                query.o_ABC_Product p
                join api.relationships r
                    on r.ToObjectId = p.ObjectId
                join api.jobs j
                    on j.jobid = r.FromObjectId
              where j.jobid = a_ObjectId
              ) loop

            if i.Name is not null then
              null;
            else
              api.pkg_errors.raiseerror(-20000, 'Product Name is required.');
            end if;

            if i.type is not null then
              null;
            else
              api.pkg_errors.raiseerror(-20000, 'Product Type is required.');
            end if;

            if i.TTBCOLA is not null then
              null;
            else
              api.pkg_errors.raiseerror(-20000, 'TTB / COLA is required.');
            end if;

            if (i.TTBCOLA = 'No' and i.TTBExplanation is not null) or i.TTBCOLA = 'Yes' then
              null;
            else
              api.pkg_errors.raiseerror(-20000, 'Please provide an explanation for no TTB/COLA.');
            end if;
          end loop;
        end if;

        -- Distributor(s)
        if t_DistributorCount > 0 or t_ObjectIsLockedDown = 'Y' then
          null;
        else
          api.pkg_errors.raiseerror(-20000, 'Before you go on, you must select at least one ' ||
              'Distributor.');
        end if;

        if api.pkg_columnquery.value(a_ObjectId, 'DefaultClerk') is not null
            or t_CancelJob = 'Y' or t_ObjectIsLockedDown = 'Y' then
          null;
        else
          api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must specify a Default Clerk.');
        end if;
      end if;
    end if;

    api.pkg_columnupdate.setvalue(a_ObjectId, 'SubmitInternal', 'N');

  end PostVerifyErrors;

  /*---------------------------------------------------------------------------
   * PostVerifyAmendment() -- PUBLIC
   *   Wrapper for the Post Verify event on the Product Registration Amendment
   * job
   *-------------------------------------------------------------------------*/
  procedure PostVerifyAmendment (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_AmendRegistrantEndPoint           udt_id
        := api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment', 'RegistrantEndPoint');
    t_Clerk                             varchar2(4000);
    t_ClerkId                           number(9);
    t_EndPoint                          udt_Id;
    t_HasLicenseeType                   varchar2(1);
    t_IsAmendment                       varchar2(1);
    t_JobId                             udt_Id;
    t_PubAmendRegId                     udt_id;
    t_PubAmendRegRelId                  udt_id;
    t_RegBasedNJ                        varchar2(3);
    t_RelId                             udt_Id;
    t_SelectedObject                    udt_Id;
  begin

    t_JobId := a_ObjectId;
    t_Clerk := api.pkg_columnquery.Value(t_JobId, 'DefaultClerk');
    t_RegBasedNJ := api.pkg_columnquery.Value(t_JobId, 'IsRegistrantBasedNewJersey');
    t_SelectedObject := api.pkg_columnquery.Value(t_JobId, 'OnlineUseLegalEntityObjectId');
    t_HasLicenseeType := extension.pkg_RelUtils.StoredRelExistsViaEPNames(t_JobId,
        'LegalEntityType');
    t_IsAmendment := api.pkg_columnquery.Value(t_JobId, 'IsAmendment');

    -- Set the Default Clerk
    if t_Clerk is null then
      -- Get the end point
      if t_IsAmendment is null or t_IsAmendment = 'N' then
        t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication',
            'ProductRegClerk');
      else
        t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment',
            'ProductAmendClerk');
      end if;

      -- Get the clerk user id
      select r.UserId
      into t_ClerkId
      from
        query.o_SystemSettings o
        join query.r_DefaultPRClerkSysSetting r
            on o.ObjectId = r.SystemSettingsObjectId
      where o.ObjectDefTypeId = 1
        and rownum = 1;

      -- Create relationship
      t_RelId := api.pkg_relationshipupdate.New(t_EndPoint,t_JobId,t_ClerkId);
    end if;

    -- Remove License/TAP if Registrant not based in New Jersey
    if t_RegBasedNJ = 'No' then
      abc.pkg_ABC_ProductRegistration.JobRemoveLicense(t_JobId,a_AsOfDate);
      abc.pkg_ABC_ProductRegistration.JobRemovePermit(t_JobId,a_AsOfDate);
    end if;

    if t_IsAmendment = 'Y' then
      begin

        select r.RelationshipId, r.ToObjectId
        into t_PubAmendRegRelId, t_PubAmendRegId
        from api.relationships r
        where r.FromObjectId = t_JobId
          and r.EndPointId = t_AmendRegistrantEndPoint;

        if t_PubAmendRegId != t_SelectedObject then
          api.pkg_relationshipupdate.Remove(t_PubAmendRegRelId);
          t_PubAmendRegRelId := api.pkg_relationshipupdate.New(t_AmendRegistrantEndPoint, t_JobId,
              t_SelectedObject);
        end if;

      exception
        when no_data_found then --Create the relationship
          if t_SelectedObject is not null then
            t_PubAmendRegRelId := api.pkg_relationshipupdate.New(t_AmendRegistrantEndPoint, t_JobId,
                t_SelectedObject);
          end if;

      end;
     end if;

    -- If LicenseeType is related to job and OnlineUseLegalEntityObjectId is not null,
    -- remove the LicenseeType relationship
    if t_HasLicenseeType = 'Y' and t_SelectedObject is not null then
      if t_IsAmendment is null or t_IsAmendment = 'N' then
        t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication',
            'LegalEntityType');
      else
        t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment', 'LegalEntityType');
      end if;

      for x in (
          select r.RelationshipId
          from api.relationships r
          where r.FromObjectId = a_ObjectId
            and r.EndPointId = t_EndPoint
          ) loop
        api.pkg_relationshipupdate.Remove(x.relationshipid);
      end loop;
    end if;

  end PostVerifyAmendment;

  /*---------------------------------------------------------------------------
   * PostVerifyAmendmentErrors() -- PUBLIC
   *   Wrapper for the Post Verify event on the Product Registration Amendment
   * job
   *-------------------------------------------------------------------------*/
  procedure PostVerifyAmendmentErrors (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_AmendDistCount                    number;
    t_AmendProdCount                    number;
    t_IsAmendment                       varchar2(1);
    t_RegDistCount                      number;
    t_RegProdCount                      number;
  begin

    t_IsAmendment := api.pkg_columnquery.Value(a_ObjectId, 'IsAmendment');

    --Find count of Reg products
    select count(*)
    into t_RegProdCount
    from query.r_ABC_ProdRegToProduct r
    where r.ProdRegJobId = a_ObjectId;

    --Find count of amend products
    select count(*)
    into t_AmendProdCount
    from query.r_Abc_Pramendtoproduct r
    where r.PRAmendObjectId = a_ObjectId;

    --Find count of distributors
    select sum(count)
    into t_RegDistCount
    from (
        select count(*) count
        from query.r_ABC_ProdRegToDistributor r
        where r.ProdRegJobId = a_ObjectId
        union
        select count(*) count
        from query.r_ABC_ProductRegTAPDist r
        where r.ProductRegistrationId = a_ObjectId
        );

    --Find count of Amend distributors
    select count(*)
    into t_AmendDistCount
    from query.r_ABC_ProdAmendToDist r
    where r.JobId = a_ObjectId;
    select (t_AmendDistCount + count(*))
    into t_AmendDistCount
    from query.r_ABC_PRAmendTAPDistributor r
    where r.PRAmendJobId = a_ObjectId;

    if api.pkg_columnquery.value(a_ObjectId, 'SubmitInternal') <> 'Y'
      or api.pkg_columnquery.value(a_ObjectId, 'EnteredOnline') = 'Y' then
      null;
    else
      -- Check Amendment Type
      if api.pkg_columnquery.value(a_ObjectId, 'ProductAmendmentTypeObjectId') is not null
          or api.pkg_columnquery.value(a_ObjectId, 'CancelJob') = 'Y'
          or api.pkg_columnquery.value(a_ObjectId, 'ObjectIsLockedDown') = 'Y' then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must select an Amendment Type.');
      end if;
        
      -- Select Registrant
      if api.pkg_columnquery.value(a_ObjectId, 'Registrant') is not null
          or api.pkg_columnquery.value(a_ObjectId, 'CancelJob') = 'Y'
          or api.pkg_columnquery.value(a_ObjectId, 'ObjectIsLockedDown') = 'Y' then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must select a Registrant.');
      end if;

      if api.pkg_Columnquery.value(a_ObjectId, 'IsRegistrantBasedNewJersey') = 'No'
          or api.pkg_Columnquery.value(a_ObjectId, 'IsRegistrantBasedNewJersey') is null
          or api.pkg_Columnquery.value(a_ObjectId, 'IsRegistrantBasedNewJersey') = 'Yes'
          and (
              api.pkg_Columnquery.value(a_ObjectId, 'License') is not null
              or api.pkg_Columnquery.value(a_ObjectId, 'InternalLicense') is not null
          ) then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must select a License.');
      end if;

      if t_AmendProdCount > 0 or api.pkg_columnquery.value(a_ObjectId, 'ObjectIsLockedDown') = 'Y'
          or t_IsAmendment = 'N' then
        null;
      else
        api.pkg_errors.raiseerror(-20000, 'Before you go on, you must enter at least one ' ||
            'Product.');
      end if;

      if api.pkg_columnquery.value(a_ObjectId, 'ProductAmendmentType') = 'Adjust Vintages'
          or t_AmendDistCount > 0 
          or api.pkg_columnquery.value(a_ObjectId, 'ObjectIsLockedDown') = 'Y'
          or t_IsAmendment = 'N' then
        null;
      else
        api.pkg_errors.raiseerror(-20000, 'Before you go on, you must select at least one ' ||
            'Distributor.');
      end if;

      if api.pkg_columnquery.value(a_ObjectId, 'DefaultClerk') is not null
          or api.pkg_columnquery.value(a_ObjectId, 'CancelJob') = 'Y'
          or api.pkg_columnquery.value(a_ObjectId, 'ObjectIsLockedDown') = 'Y' then
        null;
      else
        api.pkg_Errors.RaiseError(-20000, 'Before you go on, you must specify a Default Clerk.');
      end if;
    end if;

    api.pkg_columnupdate.setvalue(a_ObjectId, 'SubmitInternal', 'N');

  end PostVerifyAmendmentErrors;

  /****************************************************************************
   * SetRegistrantRelationship()
   *   Sets the Registrant for a Product Registration job based on the selected
   * Legal Entity on the public application. Runs on UseLegalEntityObjectId
   ***************************************************************************/
  procedure SetRegistrantRelationship(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_JobId               udt_Id := a_objectId;
    t_RegistrantEndpoint  udt_Id;
    t_NewRegistrant       udt_Id;
    t_SelectedLE          udt_Id;
    t_HasRegistrant       varchar2(1);
    t_EndPoint            udt_Id;

  begin
    t_HasRegistrant       := extension.pkg_RelUtils.StoredRelExistsViaEPNames(t_JobId,'Registrant');
    t_RegistrantEndPoint  := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication','Registrant');
    t_SelectedLE          := api.pkg_columnquery.Value(t_JobId,'OnlineUseLegalEntityObjectId');

    -- Make sure that any existing Registrant rels are removed
    if t_HasRegistrant = 'Y' then
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication','Registrant');
      for x in (select r.RelationshipId
                  from api.relationships r
                 where r.FromObjectId = t_JobId
                   and r.EndPointId = t_EndPoint) loop
        api.pkg_relationshipupdate.Remove(x.relationshipid);
      end loop;
    end if;

    -- Create the new Registrant relationship using the selected Legal Entity
    if t_SelectedLE is not null then
      t_NewRegistrant := api.pkg_relationshipupdate.New(t_RegistrantEndpoint,t_JobId,t_SelectedLE);
    end if;

  end SetRegistrantRelationship;

  /****************************************************************************
   * RelRemoveLicense()
   *   Removes any license rels from the Product Registration job.
   * Runs when the relationship is changed or removed.
   ***************************************************************************/
  procedure RelRemoveLicense(
    a_ObjectId             udt_Id,
    a_AsOfDate             date
  ) is
    t_JobId                number;
    t_HasLicense           varchar2(3);
    t_EndPoint             udt_Id;

  begin
    -- This procedure runs on a rel. Get the job id
    t_JobId := api.pkg_columnquery.value(a_ObjectId,'ProdRegJobId');

    -- Is a license rel present?
    t_HasLicense := extension.pkg_RelUtils.StoredRelExistsViaEPNames(t_JobId,'License');

    -- If licence rel is present, remove rel to reset license selection
    if t_HasLicense = 'Y' then
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication','License');
      for i in (select r.RelationshipId
                  from api.relationships r
                 where r.FromObjectId = a_ObjectId
                   and r.EndPointId = t_EndPoint) loop
        api.pkg_relationshipupdate.Remove(i.relationshipid);
      end loop;
    end if;

  end RelRemoveLicense;

  /*---------------------------------------------------------------------------
   * ResetLicense()
   *-------------------------------------------------------------------------*/
  procedure ResetLicense(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  ) is
    t_ProdRegJobId     udt_Id;
    t_JobId            udt_Id;

  begin
    -- Get the job objectid as set on button click
    t_ProdRegJobId := api.pkg_columnquery.Value(a_ObjectId, 'TempProductRegId');
    
    -- Remove any Licenses
    OnlineJobRemoveLicense(t_ProdRegJobId, a_AsOfDate);

    -- Remove any Tap Permit
    OnlineJobRemovePermit(t_ProdRegJobId, a_AsOfDate);

    -- Null the contact details on PR Application if it is new and entered online
    if api.pkg_ColumnQuery.Value(t_ProdRegJobId, 'EnteredOnline') = 'Y' 
        and api.pkg_ColumnQuery.Value(t_ProdRegJobId, 'StatusName') = 'NEW' then
      if api.pkg_ColumnQuery.Value(t_ProdRegJobId, 'OnlineRegistrantHasNJLicense') is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_ProdRegJobId, 'OnlineRegistrantHasNJLicense');
      end if;
      if api.pkg_ColumnQuery.Value(t_ProdRegJobId, 'OnlineRegistrantHasNJTAPPermit') is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_ProdRegJobId, 'OnlineRegistrantHasNJTAPPermit');
      end if;
      if api.pkg_ColumnQuery.Value(t_ProdRegJobId, 'OnlineRegistrantHasNJPresence') is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_ProdRegJobId, 'OnlineRegistrantHasNJPresence');
      end if;
      if api.pkg_ColumnQuery.Value(t_ProdRegJobId, 'OnlineRegistrantSolicitingInNJ') is not null then
        api.pkg_ColumnUpdate.RemoveValue(t_ProdRegJobId, 'OnlineRegistrantSolicitingInNJ');
      end if;
    end if;    
  end;

  /*---------------------------------------------------------------------------
   * OnlineJobRemoveLicense() -- PUBLIC
   *   Removes any license rels from the Online Product Registration job.
   * Runs on the job itself.
   *-------------------------------------------------------------------------*/
  procedure OnlineJobRemoveLicense(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  ) is
    t_EndPoint         udt_Id;
  begin
    t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication','License');
    for i in (
        select r.RelationshipId
        from api.relationships r
        where r.FromObjectId = a_ObjectId 
          and r.EndPointId = t_EndPoint
        ) loop
      api.pkg_relationshipupdate.Remove(i.relationshipid);
    end loop;
  end OnlineJobRemoveLicense;

  /*---------------------------------------------------------------------------
   * OnlineJobRemovePermit() -- PUBLIC
   *   Removes any permit rels from the Online Product Registration job.
   * Runs on the job itself.
   *-------------------------------------------------------------------------*/
  procedure OnlineJobRemovePermit(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  ) is
    t_EndPoint         udt_Id;
  begin
    t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication','TAPPermit');
    for i in (
        select r.RelationshipId
        from api.relationships r
        where r.FromObjectId = a_ObjectId 
          and r.EndPointId = t_EndPoint
        ) loop
      api.pkg_relationshipupdate.Remove(i.relationshipid);
    end loop;
  end OnlineJobRemovePermit;

  /*---------------------------------------------------------------------------
   * RemoveRelsOnLicensePermitRadioButtons() -- PUBLIC
   *   Removes any permit or Licence rels from the Online Product Registration 
   * job based on the License/Pemrit Information radio buttons.
   * Run on change of OnlineRegistrantHasNJLicense and 
   * OnlineRegistrantHasNJTAPPermit
   *-------------------------------------------------------------------------*/
   procedure RemoveRelsOnLicensePermitRadioButtons(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  ) is
  begin
    -- Delete the License 
    if api.pkg_columnquery.value(a_ObjectId,'License') is not null and 
        (api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantHasNJLicense') = 'No' or
        api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantHasNJTAPPermit') = 'Yes') then 
      OnlineJobRemoveLicense(a_ObjectId, a_AsOfDate);
    end if;

    if api.pkg_columnquery.value(a_ObjectId,'TAPPermitNumberOnline') is not null and 
        (api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantHasNJTAPPermit') = 'No' or
        api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantHasNJLicense') = 'Yes') then 
      OnlineJobRemovePermit(a_ObjectId, a_AsOfDate);
    end if;

    -- When License is changed to yes make sure that the tap is set to null
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantHasNJLicense') = 'Yes' then
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'OnlineRegistrantHasNJTAPPermit', ''); 
    end if;
    
    -- When License is changed to yes make sure that the tap is set to no
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantHasNJLicense') = 'Yes'
        or api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantHasNJTAPPermit') = 'Yes' then
      if api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantHasNJPresence') is not null then
        api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'OnlineRegistrantHasNJPresence');
      end if;
      if api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineRegistrantSolicitingInNJ') is not null then
        api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'OnlineRegistrantSolicitingInNJ');
      end if;
    end if;

  end RemoveRelsOnLicensePermitRadioButtons;

  /*---------------------------------------------------------------------------
   * JobRemoveLicense() -- PUBLIC
   *   Removes any license rels from the Product Registration job.
   * Runs on the job itself.
   *-------------------------------------------------------------------------*/
  procedure JobRemoveLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_EndPoint                          udt_Id;
    t_HasLicense                        varchar2(1);
    t_HasNJLicense                      varchar2(3);
    t_JobId                             udt_Id;
    t_JobLicenseCount                   number;
  begin

    t_JobId := a_ObjectId;
    t_HasNJLicense := api.pkg_columnquery.value(t_JobId, 'RegistrantHasNJLicense');
    t_HasLicense := extension.pkg_RelUtils.StoredRelExistsViaEPNames(t_JobId, 'License');

    if t_HasNJLicense != 'Yes' and t_HasLicense = 'Y' then
      -- If licence rel is present, remove rel to reset license selection
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication', 'License');
      for i in (
          select r.RelationshipId
          from api.relationships r
          where r.FromObjectId = a_ObjectId
            and r.EndPointId = t_EndPoint
          ) loop
        api.pkg_relationshipupdate.Remove(i.relationshipid);
      end loop;
    end if;

  end JobRemoveLicense;

  /*---------------------------------------------------------------------------
   * JobRemovePermit() -- PUBLIC
   *   Removes any Permit rels from the Product Registration job.
   * Runs on the job itself.
   *-------------------------------------------------------------------------*/
  procedure JobRemovePermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_EndPoint                          udt_Id;
    t_HasNJTAP                          varchar2(3);
    t_HasPermit                         varchar2(1);
    t_JobId                             udt_Id;
    t_JobPermitCount                    number;
  begin

    t_JobId := a_ObjectId;
    t_HasNJTAP := api.pkg_columnquery.value(t_JobId, 'RegistrantHasNJTAPPermit');
    t_HasPermit := extension.pkg_RelUtils.StoredRelExistsViaEPNames(t_JobId, 'TAPPermit');

    if t_HasNJTAP != 'Yes' and t_HasPermit = 'Y' then
      -- If Permit rel is present, remove rel to reset license selection
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication', 'TAPPermit');
      for i in (
          select r.RelationshipId
          from api.relationships r
          where r.FromObjectId = a_ObjectId
            and r.EndPointId = t_EndPoint
          ) loop
        api.pkg_relationshipupdate.Remove(i.relationshipid);
      end loop;
    end if;

  end JobRemovePermit;

  /****************************************************************************
   * JobRemoveProducts()
   *   Removes any product rels from the Product Registration Amendment job.
   * Runs on the job itself.
   ***************************************************************************/
  procedure JobRemoveProducts(
    a_ObjectId             udt_Id,
    a_AsOfDate             date
  ) is
    t_EndPoint             udt_Id;

  begin

      -- If product rel is present, remove all product rels
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment','ProductEndPoint');
      for i in (select r.RelationshipId
                  from api.relationships r
                 where r.FromObjectId = a_ObjectId
                   and r.EndPointId = t_EndPoint) loop
        api.pkg_relationshipupdate.Remove(i.relationshipid);
      end loop;

  end JobRemoveProducts;

  /****************************************************************************
   * AutomationOnApproval()
   *   Activates products on approval of Product Registration job,
   * removes vintages on product types that are not allowed to have them
   ***************************************************************************/
  procedure AutomationOnApproval(
    a_ObjectId              udt_Id,
    a_AsOfDate              date
  ) is
    t_JobId                 udt_Id;
    t_RegistrantEndPoint    number;
    t_DistributorEndPoint   number;
    t_LicenseEndPoint       number;
    t_VintageEndPoint       number;
    t_RegistrantOut         number;
    t_DistributorOut        number;
    t_LicenseOut            number;
    t_ExpirationDate        date;
    t_NonRenewalDate        date;
    t_DaysBeforeExp         number;
    t_ProdTypeHasVintage    varchar2(1);
    t_ProductIds            udt_IdList;
    t_RegistrantIds         udt_IdList;
    t_DistributorIds        udt_IdList;
    t_LicenseIds            udt_IdList;
    t_VintageIds            udt_IdList;
    t_PermitIds             udt_IdList;
    t_TAPRegEndPoint        number;
    t_TAPDistributorIds     udt_IdList;
    t_TAPDistributorEP      number;
    t_Registrant            number;
    t_OnlineUser            number;
    t_ExistingRelCount      number;

  begin
    t_JobId                 := api.pkg_columnquery.Value(a_ObjectId,'JobId');
    t_ExpirationDate        := CreateExpirationDate();
    t_NonRenewalDate        := CreateNonRenewalNoticeDate(t_ExpirationDate);

    t_RegistrantEndPoint    := api.pkg_configquery.EndPointIdForName('o_ABC_Product','LERegistrant');
    t_DistributorEndPoint   := api.pkg_configquery.EndPointIdForName('o_ABC_Product','DistributorLic');
    t_TAPDistributorEP      := api.pkg_configquery.EndPointIdForName('o_ABC_Product','TAPDistributor');
    t_LicenseEndPoint       := api.pkg_configquery.EndPointIdForName('o_ABC_Product','RegistrantLic');
    t_VintageEndPoint       := api.pkg_configquery.EndPointIdForName('o_ABC_Product','Vintage');
    t_TAPRegEndPoint        := api.pkg_configquery.EndPointIdForName('o_ABC_Product','TAPRegistrant');

    t_ProductIds            := extension.pkg_objectquery.RelatedObjects(t_JobId,'Product');
    t_RegistrantIds         := extension.pkg_objectquery.RelatedObjects(t_JobId,'Registrant');
    t_DistributorIds        := extension.pkg_objectquery.RelatedObjects(t_JobId,'Distributor');
    t_LicenseIds            := extension.pkg_objectquery.RelatedObjects(t_JobId,'License');
    t_PermitIds             := extension.pkg_objectquery.RelatedObjects(t_JobId,'TAPPermit');
    t_TAPDistributorIds     := extension.pkg_objectquery.RelatedObjects(t_JobId,'TAPDistributor');

    /* CHECK MAILING ADDRESS OF REGISTRANT */
      for r in 1..t_RegistrantIds.count loop
        if api.pkg_columnquery.Value(t_RegistrantIds(r), 'MailingAddress') is null then
           api.pkg_errors.RaiseError(-20000, 'Please ensure the Registrant has a Mailing Address.');
        end if;
      end loop;

    -- Get the days before expiration date from system settings
    select PRRenewalDaysBeforeExpDate
      into t_DaysBeforeExp
      from query.o_Systemsettings
     where rownum = 1;

    /* Add relationships and details to Products */
    for x in 1..t_ProductIds.count loop

      toolbox.pkg_counter.GenerateCounter(t_ProductIds(x), sysdate, '[9999999]', 'ABC Product Registration Number', 'RegistrationNumber');
      api.pkg_columnupdate.SetValue(t_ProductIds(x),'State','Current');
      api.pkg_columnupdate.SetValue(t_ProductIds(x),'RegistrationDate',trunc(sysdate));

      -- If the current day is past the expiration date, then set the new expiration date to the following year
      if trunc(sysdate) > (t_ExpirationDate - t_DaysBeforeExp) then
        t_ExpirationDate := add_months(t_ExpirationDate,12);
        t_NonRenewalDate := add_months(t_NonRenewalDate,12);
      end if;

      api.pkg_columnupdate.SetValue(t_ProductIds(x),'ExpirationDate',t_ExpirationDate);
      api.pkg_columnupdate.SetValue(t_ProductIds(x),'NonRenewalNoticeDate',t_NonRenewalDate);
      t_VintageIds := extension.pkg_objectquery.RelatedObjects(t_ProductIds(x),'Vintage');

      /* SET REGISTRANT */
      for r in 1..t_RegistrantIds.count loop
        t_RegistrantOut := api.pkg_relationshipupdate.New(t_RegistrantEndPoint,t_ProductIds(x),t_RegistrantIds(r));
      end loop;

      /* SET DISTRIBUTORS */
      for d in 1..t_DistributorIds.count loop
        t_DistributorOut := api.pkg_relationshipupdate.New(t_DistributorEndPoint,t_ProductIds(x),t_DistributorIds(d));
      end loop;
      for d in 1..t_TAPDistributorIds.count loop
        t_DistributorOut := api.pkg_relationshipupdate.New(t_TAPDistributorEP,t_ProductIds(x),t_TAPDistributorIds(d));
      end loop;

      /* SET LICENSE */
      for l in 1..t_LicenseIds.count loop
        t_LicenseOut := api.pkg_relationshipupdate.New(t_LicenseEndPoint,t_ProductIds(x),t_LicenseIds(l));
      end loop;

      /* SET PERMIT */
      for l in 1..t_PermitIds.count loop
        t_LicenseOut := api.pkg_relationshipupdate.New(t_TAPRegEndPoint,t_ProductIds(x),t_PermitIds(l));
      end loop;

      /* REMOVE VINTAGES */
      -- remove Vintages on products if they erroneously exist
      t_ProdTypeHasVintage := api.pkg_columnquery.Value(t_ProductIds(x),'ProductTypeHasVintages');
      if t_ProdTypeHasVintage = 'N' then
        for v in (select r.RelationshipId
                  from api.relationships r
                 where r.FromObjectId = t_ProductIds(x)
                   and r.EndPointId = t_VintageEndPoint) loop
          api.pkg_relationshipupdate.Remove(v.relationshipid);
        end loop;
      end if;

    end loop;

  -- Copy Registrant rel to online user for use in future applications. Includes all checks to see if a copy is necessary.
    if api.pkg_ColumnQuery.Value(t_JobId,'EnteredOnline') = 'Y' then
       select j.RegistrantObjectId, j.OnlineUserObjectId
         into t_Registrant, t_OnlineUser
         from query.j_Abc_Prapplication j
        where j.JobId = t_JobId;
    end if;

    if t_OnlineUser is not null then -- no copy for guest users and users that already have this registrant
       select count(1)
         into t_ExistingRelCount
         from query.r_ABC_UserLegalEntity rul
        where rul.UserId = t_OnlineUser
          and rul.LegalEntityObjectId = t_Registrant;
       if t_ExistingRelCount = 0 then
          extension.pkg_RelationshipUpdate.New(t_OnlineUser, t_Registrant, 'LegalEntity');
       end if;
    end if;

  end AutomationOnApproval;

  /****************************************************************************
   * CreateExpirationDate()
   *   Calculates the expiration date for products, etc. from the system settings
   ***************************************************************************/
  function CreateExpirationDate
    return                  date is
    t_SysDate               date;
    t_ExpirationMethod      varchar2(50);
    t_ExpirationNumber      number;
    t_ExpirationMonth       varchar2(20);
    t_LastMonthValue        date;
    t_ExpirationDate        date;
    t_DaysBeforeExp         number;

  begin
    t_SysDate               := trunc(sysdate);

    --Get values from System Settings
    select PRExpirationMethod,PRExpirationNumber,PRExpirationDateMonth,PRRenewalDaysBeforeExpDate
      into t_ExpirationMethod,t_ExpirationNumber,t_ExpirationMonth,t_DaysBeforeExp
      from query.o_Systemsettings
     where rownum = 1;

    --Calculate Expiration Date if the Expiration Month column on system settings has a value
    if t_ExpirationMonth is not null then
      if to_date(to_char(t_SysDate,'ddyyyy') || t_ExpirationMonth, 'ddyyyyMonth') < t_SysDate then
         t_LastMonthValue := last_day(add_months(to_date(to_char(t_SysDate,'ddyyyy') || t_ExpirationMonth, 'ddyyyyMonth'),12));
      else
         t_LastMonthValue := last_day(to_date(to_char(t_SysDate,'ddyyyy') || t_ExpirationMonth, 'ddyyyyMonth'));
      end if;
    end if;

    --Decide what to return based on Expiration Method
    select decode (t_ExpirationMethod,'Number of Years',       to_date(to_char(t_SysDate, 'MM-DD') || ' ' ||
                                                               to_char(to_number(to_char(t_SysDate, 'YYYY')+
                                                               t_ExpirationNumber)), 'MM-DD YYYY')-1,
                                      'Number of Months',      add_months(t_SysDate,t_ExpirationNumber)-1,
                                      'Number of Days',        (t_SysDate + t_ExpirationNumber)-1,
                                      'Manual',                t_SysDate,
                                      'End of Specific Month', t_LastMonthValue,
                                                          null) into t_ExpirationDate from dual;


    return t_ExpirationDate;

  end CreateExpirationDate;

  /****************************************************************************
   * CreateOnlineProducts()
   *   Creates Online Products as a record of what the applicant entered in the
   * online application.
   ***************************************************************************/
  procedure CreateOnlineProducts(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_ProductIdList                     udt_IdList;
    t_Name                              udt_StringList;
    t_Type                              udt_StringList;
    t_TTBCOLA                           udt_StringList;
    t_ProductTypeHasVintages            udt_StringList;
    t_CurrentVintageIds                 udt_StringList;
    t_ApplicationProductEPId            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRApplication', 'Product');
    t_NewOnlineProductDefId             udt_Id := api.pkg_configquery.ObjectDefIdForName(
        'o_ABC_OnlineProduct');
    t_NewOnlineProductId                udt_Id;
    t_ApplicationOnlineProdEPId         udt_id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRApplication', 'OnlineProduct');
    t_OnlineProdList                    udt_IdList;
    t_ApplicationOnlineProductRel       udt_Id;
    t_ProductVintagesId                 udt_IdList;
    t_ProductVintagesEPId               udt_Id := api.pkg_configquery.EndPointIdForName(
        'o_ABC_Product', 'Vintage');
    t_OnlineProductVintagesEPId         udt_Id := api.pkg_configquery.EndPointIdForName(
        'o_ABC_OnlineProduct', 'Vintage');
    t_OnlineProductVintagesRelId        udt_Id;
    t_EnteredOnline                     varchar2(1);
    t_SetOnlineProducts                 varchar2(1);

  begin
    t_SetOnlineProducts := api.pkg_columnquery.value(a_ObjectId, 'SetOnlineProducts');
    t_EnteredOnline := api.pkg_columnquery.value(a_ObjectId, 'EnteredOnline');

    -- Only run this proccedure if the application was entered online
    -- Ensure that this doesn't get into an infinite loop with the Set Online Products
    if t_SetOnlineProducts = 'Y' and t_EnteredOnline = 'Y' then
      -- Check if there are online products already created and related to this application
      -- This is for an edge case where the submit button is clicked and the applicant hits
      -- the back button.
      Select rel.toobjectid
      bulk collect into
      t_OnlineProdList
      from api.relationships rel
      where rel.FromObjectId = a_ObjectId
          and rel.EndPointId = t_ApplicationOnlineProdEPId;
      if t_OnlineProdList.count > 0 then
        for k in 1..t_OnlineProdList.count loop
          api.pkg_objectupdate.Remove(t_OnlineProdList(k));
        end loop;
      end if;

      -- Find all products related to the application as well as the product details
      select
          r.ToObjectId,
          api.pkg_columnquery.Value(r.ToobjectId, 'Name'),
          api.pkg_columnquery.Value(r.ToObjectId, 'Type'),
          api.pkg_columnquery.value(r.ToObjectId, 'TTBCOLA'),
          api.pkg_columnquery.value(r.ToObjectId, 'ProductTypeHasVintages')
      bulk collect into
          t_ProductIdList,
          t_Name,
          t_Type,
          t_TTBCola,
          t_ProductTypeHasVintages
      from api.relationships r
      where r.fromobjectid = a_ObjectId
         and r.endpointid = t_ApplicationProductEPId;

      -- For each product related to the application, create a duplicate of the product
      -- which will be a read only record of what was submitted.
      for i in 1..t_ProductIdList.count loop
        t_NewOnlineProductId := api.pkg_objectupdate.New(t_NewOnlineProductDefId);
        -- Relate the new online product to the application
        t_ApplicationOnlineProductRel := api.pkg_relationshipupdate.New(
            t_ApplicationOnlineProdEPId, a_ObjectId, t_NewOnlineProductId);
        -- Set the details for the online product
        api.pkg_columnupdate.SetValue(t_NewOnlineProductId, 'Name', t_Name(i));
        api.pkg_columnupdate.SetValue(t_NewOnlineProductId, 'Type', t_Type(i));
        api.pkg_columnupdate.SetValue(t_NewOnlineProductId, 'TTBCOLA', t_TTBCOLA(i));
        api.pkg_columnupdate.SetValue(t_NewOnlineProductId, 'ProductTypeHasVintages',
            t_ProductTypeHasVintages(i));
        -- Find all vintages for the product, relate the same vintages to the online product
        select re.ToObjectId
        bulk collect into t_ProductVintagesId
        from api.relationships re
        where re.fromobjectid = t_ProductIdList(i)
        and re.EndPointId = t_ProductVintagesEPId;
        for i in 1..t_ProductVintagesId.count loop
          t_OnlineProductVintagesRelId := api.pkg_relationshipupdate.New(
              t_OnlineProductVintagesEPId, t_NewOnlineProductId, t_ProductVintagesId(i));
        end loop;
      end loop;
      -- Set the Set Online Products detail on the application to 'N' so we don't process twice
      api.pkg_columnupdate.SetValue(a_ObjectId, 'SetOnlineProducts', 'N');
    end if;
  end;

  /****************************************************************************
   * CreateNonRenewalNoticeDate()
   *   Calculates the Non-Renewal Date based on the Expiration Date
   ***************************************************************************/
  function CreateNonRenewalNoticeDate(
    t_ExpDate         date
  ) return            date is
    t_ExpMonth        varchar2(10);
    t_ExpDay          varchar2(10);
    t_ExpDayOfYear    number;
    t_ExpYear         varchar2(4);

    t_SysMonth        varchar2(10);
    t_SysDay          varchar2(10);
    t_SysDayOfYear    number;

    t_NoticeDate      date;
  begin
    -- Get Expiration Month, Day, and Year
    t_ExpMonth        := to_char(t_ExpDate,'fmMonth');
    t_ExpDay          := to_char(t_ExpDate,'dd');
    t_ExpYear         := to_number(to_char(t_ExpDate,'yyyy'));

    -- Get System Settings month and day
    select PRNonRenewalNoticeMonth,PRNonRenewalNoticeDay
      into t_SysMonth,t_SysDay
      from query.o_SystemSettings
     where rownum = 1;

    -- Get the days of the year
    t_SysDayOfYear    := to_number(to_char(to_date(t_SysMonth||t_SysDay,'Monthdd'),'ddd'));
    t_ExpDayOfYear    := to_number(to_char(to_date(t_ExpMonth||t_ExpDay,'Monthdd'),'ddd'));

    -- Compare the dates
    if t_SysDayOfYear > t_ExpDayOfYear then
      t_NoticeDate := to_date(t_SysMonth||' '||t_SysDay||' '||t_ExpYear,'Month dd yyyy');
    elsif t_SysDayOfYear < t_ExpDayOfYear then
      t_NoticeDate := to_date(t_SysMonth||' '||t_SysDay||' '||(t_ExpYear+1),'Month dd yyyy');
    end if;

    return t_NoticeDate;

  end CreateNonRenewalNoticeDate;

  /****************************************************************************
   * AmendOnApproval()
   *   Amends products on approval of Product Registration Amendment job
   * to the new Distributors
   ***************************************************************************/
  procedure AmendOnApproval(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_JobId                  udt_Id;
    t_DistributorEndPoint    number;
    t_DistributorOut         number;
    t_ProductIds             udt_IdList;
    t_DistributorIds         udt_IdList;
    t_TAPDistributorEndPoint number;
    t_TAPDistributorIds      udt_IdList;

  begin
    t_JobId                  := api.pkg_columnquery.Value(a_ObjectId,'JobId');
    t_DistributorEndPoint    := api.pkg_configquery.EndPointIdForName('o_ABC_Product','DistributorLic');
    t_ProductIds             := extension.pkg_objectquery.RelatedObjects(t_JobId,'ProductEndPoint');
    t_DistributorIds         := extension.pkg_objectquery.RelatedObjects(t_JobId,'DistributorEndPoint');
    t_TAPDistributorEndPoint := api.pkg_configquery.EndPointIdForName('o_ABC_Product','TAPDistributor');
    t_TAPDistributorIds      := extension.pkg_objectquery.RelatedObjects(t_JobId,'TAPDistributor');

    /* Add relationships to Products */
    for x in 1..t_ProductIds.count loop
      for d in 1..t_DistributorIds.count loop

          t_DistributorOut := api.pkg_relationshipupdate.New(t_DistributorEndPoint,t_ProductIds(x),t_DistributorIds(d));

      end loop;
    end loop;
    /* Add relationships to Products */
    for x in 1..t_ProductIds.count loop
      for d in 1..t_TAPDistributorIds.count loop

          t_DistributorOut := api.pkg_relationshipupdate.New(t_TAPDistributorEndPoint,t_ProductIds(x),t_TAPDistributorIds(d));

      end loop;
    end loop;

  end AmendOnApproval;

  /*---------------------------------------------------------------------------
   * AmendCheckRels() -- PUBLIC
   *   Checks the relationships of Products to Distributors on the Product
   * Registration Amendment job. If a relationship exists between the Products
   * and Distributors, an error is raised.
   *-------------------------------------------------------------------------*/
  procedure AmendCheckRels (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_DistributorEndPoint               number;
    t_DistributorIds                    udt_IdList;
    t_Licensee                          varchar2(400);
    t_ObjectDefDescription              varchar2(4000);
    t_ObjectId                          udt_Id;
    t_Product                           varchar2(4000);
    t_ProductIds                        udt_IdList;
    t_RelId                             number;
    t_TAPDistributorEndPoint            number;
    t_TAPDistributorIds                 udt_IdList;
    t_Transaction                       udt_id;
  begin

    t_ObjectDefDescription  := api.pkg_columnquery.Value(a_ObjectId,'ObjectDefDescription');

    -- Distinguish the Job from the Process
    if t_ObjectDefDescription = 'Product Registration Amendment' then
      t_ObjectId := a_ObjectId;
    else
      t_ObjectId := api.pkg_columnquery.Value(a_ObjectId, 'JobId');
    end if;

    t_DistributorEndPoint := api.pkg_configquery.EndPointIdForName(
        'o_ABC_Product', 'DistributorLic');
    t_DistributorIds := extension.pkg_objectquery.RelatedObjects(t_ObjectId, 'DistributorEndPoint');
    t_ProductIds := extension.pkg_objectquery.RelatedObjects(t_ObjectId, 'ProductEndPoint');
    t_TAPDistributorEndPoint := api.pkg_configquery.EndPointIdForName(
        'o_ABC_Product', 'TAPDistributor');
    t_TAPDistributorIds := extension.pkg_objectquery.RelatedObjects(t_ObjectId, 'TAPDistributor');

    -- On the Process, verify that Products and Distributors have been entered
    if t_ObjectDefDescription != 'Product Registration Amendment'
        and api.pkg_columnquery.value(t_ObjectId, 'ProductAmendmentType') != 'Adjust Vintages'
        and t_DistributorIds.count() = 0 and t_TAPDistributorIds.count() = 0 then
      api.pkg_errors.RaiseError(-20000, 'You must choose at least one Distributor before' ||
          ' continuing.');
    elsif t_ObjectDefDescription != 'Product Registration Amendment'
        and t_ProductIds.count() = 0 then
      api.pkg_errors.RaiseError(-20000, 'You must choose at least one Product before continuing.');
    end if;

    -- Add relationships to Products
    for x in 1..t_ProductIds.count() loop
      -- Check Distributor Licenses
      for d in 1..t_DistributorIds.count() loop
        -- Check if the relationship already exists
        t_RelId := NULL;
        for c in (
            select r.RelationshipId
            from api.relationships r
            where r.EndPointId = t_DistributorEndPoint
              and r.FromObjectId = t_ProductIds(x)
              and r.ToObjectId = t_DistributorIds(d)
            ) loop
          t_RelId := c.relationshipid;
        end loop;

        -- If the relationship exists, raise an error
        if t_RelId is not null then
          select
            (
                select l.Licensee
                from query.o_abc_license l
                where l.ObjectId = r.ToObjectId
            ),
            (
                select p.Name
                from query.o_abc_product p
                where p.ObjectId = r.FromObjectId
            ),
            r.LogicalTransactionId
          into
            t_Licensee,
            t_Product,
            t_Transaction
          from api.relationships r
          where r.RelationshipId = t_RelId
          and r.ToEndPoint = 1;

          if t_Transaction != api.pkg_logicaltransactionupdate.CurrentTransaction() then
            api.pkg_errors.RaiseError(-20000, t_Licensee || ' is already a distributor of '
                || t_Product || '.');
          end if;
        end if;
      end loop;

      -- Check Distributor TAPs
      for d in 1..t_TAPDistributorIds.count() loop
        -- Check if the relationship already exists
        t_RelId := NULL;
        for c in (
            select r.RelationshipId
            from api.relationships r
            where r.EndPointId = t_TAPDistributorEndPoint
              and r.FromObjectId = t_ProductIds(x)
              and r.ToObjectId = t_TAPDistributorIds(d)
            ) loop
          t_RelId := c.relationshipid;
        end loop;

        -- If the relationship exists, raise an error
        if t_RelId is not null then
          select
            (
                select l.dup_PermitteeDisplay_SQL
                from query.o_abc_permit l
                where l.ObjectId = r.ToObjectId
            ),
            (
                select p.Name
                from query.o_abc_product p
                where p.ObjectId = r.FromObjectId
            ),
            r.LogicalTransactionId
          into
            t_Licensee,
            t_Product,
            t_Transaction
          from api.relationships r
          where r.RelationshipId = t_RelId
          and r.ToEndPoint = 1;
          if t_Transaction != api.pkg_logicaltransactionupdate.CurrentTransaction() then
            api.pkg_errors.RaiseError(-20000, t_Licensee || ' is already a distributor of '
                || t_Product || '.');
          end if;
        end if;
      end loop;

    end loop;

  end AmendCheckRels;

  /*---------------------------------------------------------------------------
   * CreateOnlineAmendProductRels() -- PUBLIC
   *   Create a new relationship between the Product and Online PR Amendment
   * Job upon submission.
   *-------------------------------------------------------------------------*/
  procedure CreateOnlineAmendProductRels (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_EnteredOnline                     varchar2(1);
    t_OnlineAmendProductEPId            udt_Id;
    t_OnlineAmendProductRelIds          udt_IdList;
    t_ProductAmendTypeEndpointId        udt_Id;
    t_ProductAmendmentType              varchar2(4000);
    t_ProductEndpointId                 udt_Id;
    t_ProductIdList                     udt_IdList;
    t_SetOnlineAmendToProdRel           varchar2(1);
  begin

    t_EnteredOnline := api.pkg_ColumnQuery.Value(a_ObjectId, 'EnteredOnline');
    t_ProductEndpointId := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PRAmendment', 'ProductEndpoint');
    t_OnlineAmendProductEPId := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PRAmendment', 'Product');
    t_SetOnlineAmendToProdRel := api.pkg_ColumnQuery.Value(a_ObjectId,
        'SetOnlinePRAmendToProductRel');

    t_ProductAmendTypeEndpointId := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PRAmendment', 'ProductAmendmentType');
    select api.pkg_ColumnQuery.Value(r.ToObjectId, 'Name')
    into t_ProductAmendmentType
    from api.relationships r
    where r.FromObjectId = a_ObjectId
      and r.EndpointId = t_ProductAmendTypeEndpointId;

    -- Ensure the amendment was entered online and prevent an infinite loop with the Set Online
    -- PR Amend. To Product Rel detail
    if t_EnteredOnline = 'Y' and t_SetOnlineAmendToProdRel = 'Y'
        and t_ProductAmendmentType != 'Adjust Vintages' then
      -- Check for existing relationships between online amendment and products, then remove them.
      -- This addresses cases when the applicant clicks 'Submit' and then hits the back button.
      select r.RelationshipId
      bulk collect into t_OnlineAmendProductRelIds
      from api.relationships r
      where r.FromObjectId = a_ObjectId
        and r.EndpointId = t_OnlineAmendProductEPId;

      if t_OnlineAmendProductRelIds.count() > 0 then
        for i in 1..t_OnlineAmendProductRelIds.count() loop
          api.pkg_RelationshipUpdate.Remove(t_OnlineAmendProductRelIds(i));
        end loop;
      end if;

      -- Find all products related to the online amendment
      select r.ToObjectId
      bulk collect into t_ProductIdList
      from api.relationships r
      where r.FromObjectId = a_ObjectId
        and r.EndpointId = t_ProductEndpointId;

      -- For each related product, create a new rel from online amendment to product
      for i in 1..t_ProductIdList.count() loop
        t_OnlineAmendProductRelIds(1) := api.pkg_RelationshipUpdate.New(t_OnlineAmendProductEPId,
            a_ObjectId, t_ProductIdList(i));
      end loop;
      -- Set the Set Online PR Amend. To Product Rel detail on the amendment to 'N' so we don't
      -- process twice
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'SetOnlinePRAmendToProductRel', 'N');
    end if;

  end CreateOnlineAmendProductRels;

end PKG_ABC_ProductRegistration;
/

