create or replace package pkg_ABC_municipality is

  /*---------------------------------------------------------------------------
  * DESCRIPTION
  *   Routines used to manage municipalities.  Creation and assignment of a
  * municipality-specific instance security accessgroup is managed here.
  *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
  * Types
  *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * AssignAccessGroup() -- PUBLIC
   * Intended to run on constructor
   *-------------------------------------------------------------------------*/
  procedure AssignAccessGroup(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*--------------------------------------------------------------------------
  * AddStaff() - Add municipality access group to a user
  * Run on Constructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure AddStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * AddPoliceStaff() - Add police municipality access group to a user
  * Run on Constructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure AddPoliceStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * RemoveStaff() - Add municipality access group to a user
  * Run on Deleting event of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure RemoveStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * RemovePoliceStaff() - Remove police  municipality access group from a user
  * Run on Destructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure RemovePoliceStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*---------------------------------------------------------------------------
   * CreateOrderIssuanceSeq() -- PUBLIC
   * Run on constructor of the Municipality (o_ABC_Office)
   *-------------------------------------------------------------------------*/
  procedure CreateOrderIssuanceSeq(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_MunicipalityCountyCode            number
  );

  /*---------------------------------------------------------------------------
   * DeleteOrderIssuanceSeq() -- PUBLIC
   * Run on destructor of the Municipality (o_ABC_Office)
   *-------------------------------------------------------------------------*/
  procedure DeleteOrderIssuanceSeq(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * ValidateWholesaleDefaultFlag()
   *   Run on post verify of the Municipality (o_ABC_Office)
   *-------------------------------------------------------------------------*/
  procedure ValidateWholesaleDefaultFlag (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * MuniEmailForPermit()
   *   Send notification Email to Municipality / Applicant on Permit
   * Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure MuniEmailForPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * PoliceEmailForPermit()
   *   Send notification Email to Police Users / Applicant on Permit
   * Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure PoliceEmailForPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * ReviewEmailReminder() -- PUBLIC
   *   Send reminder Email to Municipal Users / Applicant on Permit Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure ReviewEmailReminder(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_municipality;
/

create or replace package body pkg_ABC_municipality is

  /*---------------------------------------------------------------------------
   * AssignAccessGroup() -- PUBLIC
   * Intended to run on constructor
   *-------------------------------------------------------------------------*/
  procedure AssignAccessGroup(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_AccessGroupId                     udt_Id;
    t_AccessGroupObjectId               udt_Id;
    t_MunicipalityToAccessGroup                 udt_Id :=
        api.pkg_ConfigQuery.EndPointIdForName('o_ABC_Office', 'WWWAccessGroup');
    t_PoliceAccessGroup                 udt_id :=
        api.pkg_ConfigQuery.EndpointIdForName('o_ABC_Office', 'PoliceWWWAccessGroup');
    t_RelId                             udt_Id;
  begin
    IF api.pkg_ColumnQuery.Value(a_ObjectId, 'WWWAccessGroupName') is null THEN
      -- Create a new Access Group.
      t_AccessGroupId := abc.pkg_InstanceSecurity.NewAccessGroup;

      -- Access Group is already registered... locate its ObjectId
      t_AccessGroupObjectId := api.pkg_simplesearch.ObjectByIndex('o_AccessGroup', 'AccessgroupId', t_AccessGroupId);

      -- Relate Access Group and Municipality
      t_RelId := api.pkg_RelationshipUpdate.New(t_MunicipalityToAccessGroup,
          a_ObjectId, t_AccessGroupObjectId);

  /*    -- Adjust security appropriately.
      abc.pkg_InstanceSecurity.AdjustByColumn(a_User, sysdate,
          api.pkg_ColumnQuery.Value(a_User, 'ReadAccessGroups'),
          api.pkg_ColumnQuery.Value(a_User, 'ModifyAccessGroups'));
  */
   END IF;
   IF api.pkg_ColumnQuery.Value(a_ObjectId, 'PoliceWWWAccessGroupName') is null THEN
     -- Create a new Police Access Group.
     t_AccessGroupId := abc.pkg_InstanceSecurity.NewAccessGroup;

     --Police Access Group is already registered... locate its ObjectId
     t_AccessGroupObjectId := api.pkg_simplesearch.ObjectByIndex('o_AccessGroup', 'AccessgroupId', t_AccessGroupId);

     -- Relate Police Access Group and Municipality
      t_RelId := api.pkg_RelationshipUpdate.New(t_PoliceAccessGroup,
          a_ObjectId, t_AccessGroupObjectId);
    END IF;
  end AssignAccessGroup;

  /*--------------------------------------------------------------------------
  * AddStaff() - Add municipality access group to a user
  * Run on Constructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure AddStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  ) is
    t_user              udt_Id;
    t_municipality      udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    select FromObjectId, ToObjectId into t_user, t_municipality
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;
    t_GroupId := api.pkg_columnquery.value(t_municipality, 'AccessGroupId');
    select count(*) into t_exists
      from api.accessGroupUsers
      where userId = t_user
        and accessGroupId = t_GroupId;
    if t_exists < 1 then
      api.pkg_UserUpdate.AddToAccessGroup(t_user, t_groupid);
    end if;
  end AddStaff;

  /*--------------------------------------------------------------------------
  * AddPoliceStaff() - Add police municipality access group to a user
  * Run on Constructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure AddPoliceStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  ) is
    t_user              udt_Id;
    t_municipality      udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    select FromObjectId, ToObjectId into t_user, t_municipality
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;
    t_GroupId := api.pkg_columnquery.value(t_municipality, 'PoliceAccessGroupId');
    select count(*) into t_exists
      from api.accessGroupUsers
      where userId = t_user
        and accessGroupId = t_GroupId;
    if t_exists < 1 then
      api.pkg_UserUpdate.AddToAccessGroup(t_user, t_groupid);
    end if;
  end AddPoliceStaff;


  /*--------------------------------------------------------------------------
  * RemoveStaff() - Add municipality access group to a user
  * Run on Destructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure RemoveStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  ) is
    t_user              udt_Id;
    t_municipality      udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    --Obtain UserId for outgoing staff relationship
    select FromObjectId, ToObjectId into t_user, t_municipality
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;
    t_GroupId := api.pkg_columnquery.value(t_municipality, 'AccessGroupId');
    select count(*) into t_exists
      from api.accessGroupUsers
      where userId = t_user
        and accessGroupId = t_GroupId;
    if t_exists > 0 then
      api.pkg_UserUpdate.RemoveFromAccessGroup(t_user, t_groupid);
    end if;
  end RemoveStaff;

  /*--------------------------------------------------------------------------
  * RemovePoliceStaff() - Remove police  municipality access group from a user
  * Run on Destructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure RemovePoliceStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  ) is
    t_user              udt_Id;
    t_municipality      udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    --Obtain UserId for outgoing staff relationship
    select FromObjectId, ToObjectId into t_user, t_municipality
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;
    t_GroupId := api.pkg_columnquery.value(t_municipality, 'PoliceAccessGroupId');
    select count(*) into t_exists
      from api.accessGroupUsers
      where userId = t_user
        and accessGroupId = t_GroupId;
    if t_exists > 0 then
      api.pkg_UserUpdate.RemoveFromAccessGroup(t_user, t_groupid);
    end if;
  end RemovePoliceStaff;

  /*--------------------------------------------------------------------------
  * CreateOrderIssuanceSeq() - Create a sequence to be used for the
  *   Municipality's Order of Issuance.
  * Run on Constructor of the Municipality
  *------------------------------------------------------------------------*/
  procedure CreateOrderIssuanceSeq(
    a_ObjectId udt_Id,
    a_AsOfDate date,
    a_MunicipalityCountyCode number
  ) is
--  t_MuniCode number(4);
  t_SeqName  varchar2(40);
  t_sql      varchar2(4000);
  PRAGMA AUTONOMOUS_TRANSACTION;
  begin
    t_Seqname := 'abc.muniorderissue' || lpad(a_MunicipalityCountyCode, 4, 0) || '_seq';
    t_sql := 'create sequence ' ||
             t_SeqName ||
             ' increment by 1 start with ' ||
             (case
                when api.pkg_columnquery.value(a_ObjectId, 'OrderofIssuance') = 0
                  or api.pkg_columnquery.value(a_ObjectId, 'OrderofIssuance') is null
                  then 1
                else api.pkg_columnquery.value(a_ObjectId, 'OrderofIssuance')
              end) ||
             ' maxvalue 999 nocycle nocache';
  begin
    execute immediate t_sql;
  exception when others then
    api.pkg_errors.RaiseError(-20001, 'Cannot create sequence: ' ||  t_SeqName || '. ' || sqlerrm);
  end;
  end CreateOrderIssuanceSeq;

  /*--------------------------------------------------------------------------
  * DeleteOrderIssuanceSeq() - Delete the sequence used for the
  *   Municipality's Order of Issuance.
  * Run on Destructor of the Municipality
  *------------------------------------------------------------------------*/
  procedure DeleteOrderIssuanceSeq(
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
  t_MuniCode number(4);
  t_SeqName  varchar2(40);
  t_sql      varchar2(4000);
  PRAGMA AUTONOMOUS_TRANSACTION;
  begin
    t_MuniCode := api.pkg_columnquery.value(a_objectid, 'MunicipalityCountyCode');
    t_Seqname := 'abc.muniorderissue' || lpad(t_MuniCode, 4, 0) || '_seq';
    t_sql := 'drop sequence ' || t_SeqName;
    execute immediate t_sql;
  end DeleteOrderIssuanceSeq;

  /*---------------------------------------------------------------------------
   * ValidateWholesaleDefaultFlag() -- PUBLIC
   *   Run on post verify of the Municipality (o_ABC_Office)
   *-------------------------------------------------------------------------*/
  procedure ValidateWholesaleDefaultFlag (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_CountyIsActive                    varchar2(1);
    t_CountyWholesaleDefaultFlag        varchar2(1);
    t_MunicipalityIsActive              varchar2(1);
    t_UserCount                         number(4);
    t_WholesaleDefaultFlag              varchar2(1);
    t_WholesaleDefaultMuniCount         number(4);
  begin

    select muni.WholesaleDefault
    into t_WholesaleDefaultFlag
    from query.o_abc_office muni
    where muni.ObjectId = a_ObjectId;

    --check if any other municipalities are marked as a wholesale default
    if t_WholesaleDefaultFlag = 'Y' then
      select count(*)
      into t_WholesaleDefaultMuniCount
      from query.o_abc_office o
      where o.WholesaleDefault = 'Y';

      if t_WholesaleDefaultMuniCount > 1 then
        api.pkg_errors.RaiseError(-20000, 'Only one municipality is allowed to be marked as'
            || ' Wholesale Default.');
      end if;

      --check if the associated county is also marked as wholesale default also
      select r.WholesaleDefault
      into t_CountyWholesaleDefaultFlag
      from
        query.r_abc_regionoffice ro
        join query.o_abc_region r
            on r.ObjectId = ro.RegionObjectId
      where ro.OfficeObjectId = a_ObjectId;

      if t_CountyWholesaleDefaultFlag = 'N' then
        api.pkg_errors.RaiseError(-20000, 'The county must also be marked as Wholesale Default.');
      end if;
    end if;

    select api.pkg_columnquery.Value(o.CountyObjectId, 'Active')
    into t_CountyIsActive
    from query.o_abc_office o
    where o.ObjectId = a_ObjectId;

    select count(distinct u.ObjectId)
    into t_UserCount
    from
      api.objects o
      join api.relationships r
          on r.FromObjectId = o.ObjectId
      join query.u_users u
          on r.ToObjectId = u.ObjectId
    where o.ObjectId = a_ObjectId
      and u.UserType in ('Municipal', 'Police')
      and u.Active = 'Y';

    if t_CountyIsActive = 'N' then
      api.pkg_errors.RaiseError(-20009, 'Before you proceed to activate the Municipality, you must'
          || ' choose an active County.');
    end if;

    t_MunicipalityIsActive := api.pkg_columnquery.Value(a_ObjectId, 'Active');
    if t_UserCount > 0 and t_MunicipalityIsActive = 'N' then
      api.pkg_errors.RaiseError(-20009, 'You cannot deactivate the Municipality when there are'
          || ' Municipal or Police Users associated.');
    end if;

  end ValidateWholesaleDefaultFlag;

  /*---------------------------------------------------------------------------
   * MuniEmailForPermit() -- PUBLIC
   *   Send notification Email to Municipality / Applicant on Permit
   * Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure MuniEmailForPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_MunicipalityObjectId              udt_id;
    t_JobId                             integer;
    t_PermitObjectId                    integer;
    t_PermitteeObjectId                 integer;
    t_NotifyMunicipality                varchar2(1);
    t_MunicipalityRequired              varchar2(1);
    t_From                              varchar2(4000);
    t_Subject                           varchar2(4000);
    t_IsProduction                      varchar2(1);
    t_To                                varchar2(4000);
    t_HeaderImageName                   varchar2(4000);
    t_HeaderText                        varchar2(4000);
    t_FooterText                        varchar2(4000);
    t_baseURL                           varchar2(4000);
    t_JobDefName                        varchar2(200);
    t_PermitType                        varchar2(200);
    t_PermitTypeCode                    varchar2(20);
    t_JobNumber                         varchar2(200);
    t_PermitteeName                     varchar2(200);
    t_LicenseNumber                     varchar2(50);
    t_MailingAddress                    varchar2(4000);
    t_PhysicalAddress                   varchar2(4000);
    t_ContactName                       varchar2(250);
    t_ContactPhoneNumber                varchar2(50);
    t_County                            varchar2(200);
    t_Municipality                      varchar2(200);
    t_AdditionalPermitInfo              varchar2(4000);
    t_SAQuestion                        varchar2(500);
    t_EventLocationAddress              varchar2(4000);
    t_EventLocAddressDescription        varchar2(4000);
    t_EventDetails                      varchar2(4000);
    t_AnswerOnJob                       varchar2(2000);
    t_AcceptableAnswer                  varchar2(2000);
    t_RegistrantEmail                   varchar2(4000);
    t_OnlineUserId                      integer;
    t_EventDatesCLOB                    clob;
    t_RainDatesCLOB                     clob;
    t_QuestionsCLOB                     clob;
    t_DocumentListCLOB                  clob;
    t_Body                              clob;
    t_Body1                             clob;
    t_HeaderTextCLOB                    clob;
    t_FooterTextCLOB                    clob;
    t_Body2                             clob;
    t_PermitTypeCLOB                    clob;
    t_Body3                             clob;
    t_JobNumberCLOB                     clob;
    t_Body4                             clob;
    t_PermitteeNameCLOB                 clob;
    t_Body12                            clob;
    t_LicenseNumberCLOB                 clob;
    t_Body5                             clob;
    t_MailingAddressCLOB                clob;
    t_Body6                             clob;
    t_PhysicalAddressCLOB               clob;
    t_Body7                             clob;
    t_ContactNameCLOB                   clob;
    t_Body8                             clob;
    t_ContactPhoneNumberCLOB            clob;
    t_Body8a                            clob;
    t_RegistrantEmailCLOB               clob;
    t_Body9                             clob;
    t_AdditionalPermitInfoCLOB          clob;
    t_Body10                            clob;
    t_BodyEND                           clob;
    t_EventLocationCLOB                 clob;
    t_EventDetailsCLOB                  clob;
    t_Body11                            clob;
    t_MunicipalEmailsCLOB               clob;
  begin

    -- Collect all data needed for the email.  The email will be generated after all data is
    -- collected. This will be run on completion of a process which means we will need to get
    -- the JobId
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    -- Need the Job def name to determine what rels to use to get the required information
    t_JobDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_PermitType := api.pkg_ColumnQuery.Value(t_JobId, 'PermitType');
    -- Check to see if the permit type requires the muni or police to be notified.
    t_NotifyMunicipality := api.pkg_ColumnQuery.Value(
        api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeObjectId'), 'NotifyMunicipality');
    -- Check to see if the permit type requires muni or police review
    t_MunicipalityRequired := api.pkg_ColumnQuery.Value(
        api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeObjectId'), 'MunicipalityRequired');

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      -- Get the information needed that can be found through api.pkg_ColumnQuery
      t_OnlineUserId := api.pkg_ColumnQuery.NumericValue(t_JobId, 'OnlineUserObjectId');
      t_RegistrantEmail := api.pkg_columnquery.value(t_OnlineUserId, 'EmailAddress');
      t_JobNumber := api.pkg_ColumnQuery.Value(t_JobId, 'ExternalFileNum');
      t_PermitObjectId := api.pkg_ColumnQuery.Value(t_JobId, 'PermitObjectId');
      t_PermitTypeCode := api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeCode');
      t_LicenseNumber := coalesce(api.pkg_ColumnQuery.Value(t_PermitObjectid, 'LicenseNumber'),
            'N/A');
      t_PermitteeObjectId := api.pkg_ColumnQuery.Value(t_PermitObjectId, 'PermitteeObjectId');
      t_PermitteeName := nvl(api.pkg_ColumnQuery.Value(t_PermitObjectId, 'PermitteeName'),
          api.Pkg_Columnquery.Value(t_PermitObjectId, 'OnlineLEFormattedName'));
      t_MailingAddress := replace(nvl(
          api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'MailingAddress'),
          api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineLEMailingAddress')),
          chr(13) || chr(10), '<br>');
      if api.pkg_columnquery.Value(t_JobId,'OnlineLESameAsMailing') = 'Y' then
         t_PhysicalAddress := t_MailingAddress;
      else
        t_PhysicalAddress := replace(nvl(
            api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'PhysicalAddress'),
            api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePhysicalAddress')),
            chr(13)||chr(10), '<br>');
      end if;
      t_ContactName := coalesce(api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'ContactName'),
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactName'), t_PermitteeName);
      t_ContactPhoneNumber := REGEXP_REPLACE(nvl(
          api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'ContactPhoneNumber'),
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactPhone')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');
      t_County := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'County');
      t_Municipality := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'Municipality');
      t_MunicipalityObjectId := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'MunicipalityObjectId');
      t_AdditionalPermitInfo := api.pkg_ColumnQuery.Value(t_PermitObjectId,
          'AdditionalPermitInformation');
      t_EventLocationAddress := replace(nvl(
          nvl(api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineEventLocationAddress'),
          api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineEventLocationAddress')),
          api.Pkg_Columnquery.Value(t_PermitObjectId, 'EventLocationAddress')),
          chr(13)||chr(10), '<br>');
      t_EventLocAddressDescription := api.pkg_ColumnQuery.Value(t_PermitObjectId,
          'EventLocAddressDescription');
      t_EventDetails := api.pkg_ColumnQuery.Value(t_PermitObjectId, 'EventDetails');

      -- If permit is a Social Affair permit get the answer to the SA Question.
      if lower(t_PermitTypeCode) = 'sa' then
        t_SAQuestion := 'Was the Non-Profit Group/Organization formed as a ' ||
            'Religious, Civic or Educational Entity?<br>' ||
            nvl(api.pkg_ColumnQuery.Value(t_JobId,'OnlineCivicReligOrEduc'),
            api.pkg_ColumnQuery.Value(t_JobId, 'CivicReligOrEduc'));
      end if;

      -- Get the event dates and convert to clob with HTML formatting for the body of the email.
      for e in (
          select ed.EventDateObjectId
          from query.r_Abc_Permiteventdate ed
          where ed.PermitObjectId = t_PermitObjectId
          ) loop
        t_EventDatesCLOB := t_EventDatesCLOB || to_clob(to_char(
            api.pkg_ColumnQuery.DateValue(e.eventdateobjectid, 'StartDate'), 'Mon DD, YYYY'));
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'StartTimeHours') is not null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || ' from ' ||
              api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'FormattedStartTime'));
        end if;
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'EndTimeHours') is not null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || ' to ' ||
              api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'FormattedEndTime') || '<br>');
        end if;
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'EndTimeHours') is null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || '<br>');
        end if;
      end loop;
      -- Add Event Dates header to t_EventDatesCLOB if there were any event dates.
      if t_EventDatesCLOB is not null then
        t_EventDatesCLOB := '<br><div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"> <u>Event Dates</u><br>' || t_EventDatesCLOB ||
            '</div>';
      end if;

      -- Get the Rain Dates and convert to clob with HTML formatting for the body of the email.
      for r in (
          select rd.RainDateObjectId
          from query.r_ABC_PermitRainDate rd
          where rd.PermitObjectId = t_PermitObjectId
          ) loop
        t_RainDatesCLOB := t_RainDatesCLOB || to_clob(to_char(
            api.pkg_ColumnQuery.DateValue(r.RainDateObjectId, 'StartDate'), 'Mon DD, YYYY'));
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'StartTimeHours') is not null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || ' from ' ||
              api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'FormattedStartTime'));
        end if;
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'EndTimeHours') is not null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || ' to ' ||
              api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'FormattedEndTime') || '<br>');
        end if;
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'EndTimeHours') is null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || '<br>');
        end if;
      end loop;
      -- Add Rain Dates header to t_RainDatesCLOB if there were any rain dates.
      if t_RainDatesCLOB is not null then
        t_RainDatesCLOB := '<br><u>Rain Dates</u><br>' || t_RainDatesCLOB;
      end if;

      -- Get the information from the Permit Application.
      if t_JobDefName = 'j_ABC_PermitApplication' then
        -- If there is a rel to questions start creation of the HTML table
        if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
          -- Table header
          t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br><u>Application Questions</u><br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;">');
           -- Loop through all QAResponses
          for q in (
               select qa.ResponseId
               from query.r_ABC_PermitAppQAResponse qa
               where qa.PermitApplicationId = t_JobId
               order by api.pkg_columnquery.NumericValue(qa.ResponseId, 'SortOrder')
               ) loop
            t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Question ' ||
                api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') || ':</td>' ||
                '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'Question') || '</td></tr>');
            if nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A' then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                  "vertical-align:top; text-align:left">Yes / No Response:</td>' ||
                  '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
            end if;
            t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.responseid, 'AnswerOnJob');
            t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.responseid, 'AcceptableAnswer');
            if nvl(t_AnswerOnJob, 'N') != nvl(t_AcceptableAnswer, 'Y')
                and t_AcceptableAnswer is not null then
              if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                  then
                if nvl(t_AcceptableAnswer, 'N') != 'N/A' then
                  t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                      "width: 250px; vertical-align:top; text-align:left"></td>'
                      || '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText')
                      || '</td></tr>');
                end if;
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "vertical-align:top; text-align:left">Response:</td><td>'
                    || nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                    to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId,
                    'ResponseDate'), 'Mon DD, YYYY')) || '</td></tr>');
               end if;
            end if;
            t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
          end loop;
          -- Close the table
          t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
        end if;

        -- If there is a rel to documents create table for the documents
        if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
          -- Table header for documents
          t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
              Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Documents</u><br>
              See below for a list of documents attached to this email.<br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><tr><th align="left">Document Type</th>
              <th align="left">File Name</th><th align="left">Upload Date</th>
              </tr>');

          -- Loop through Online Uploads
          for d2 in (
              select
                api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
                dr.FileName,
                lt.CreatedDate
              from query.r_OLNewPermitAppElectronicDoc AppDoc
              join api.documentrevisions dr
                  on AppDoc.OLDocumentId = dr.DocumentId
              join query.r_ElectronicDocDocType dt
                  on dr.DocumentId = dt.DocumentId
              join api.logicaltransactions lt
                  on dr.LogicalTransactionId = lt.LogicalTransactionId
              where AppDoc.OLNewPermitAppJobId = t_JobId
              ) loop
            t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td>' || d2.doctype ||
                '</td><td>' || d2.filename || '</td><td>' || to_char(d2.Createddate, 'Mon DD, YYYY')
                || '</td></tr>');
          end loop;
          -- Close the table
          t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div>';
        end if;
      end if;

      -- Get the information from the Permit Renewal.
      if t_JobDefName = 'j_ABC_PermitRenewal' then
        -- If there is a rel to questions start creation of the HTML table
        if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
          -- Table header
          t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Application Questions
              </u><br><table style="width:100%; font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;">');
          -- Loop through all QAResponses
          for q in (
              select qa.ResponseId
              from query.r_ABC_PermitRnwQAResponse qa
              where qa.PermitRenewalId = t_JobId
              order by api.pkg_columnquery.NumericValue(qa.ResponseId, 'SortOrder')
              ) loop
            t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Question ' ||
                api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') ||
                ':</td><td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'Question')
                || '</td></tr>');
            if nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A' then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                  "vertical-align:top; text-align:left">Yes / No Response:</td><td>'
                  || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
            end if;
            t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.responseid, 'AnswerOnJob');
            t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.responseid, 'AcceptableAnswer');
            if nvl(t_AnswerOnJob, 'N') != nvl(t_AcceptableAnswer, 'Y')
                and t_AcceptableAnswer is not null then
              if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                  then
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "vertical-align:top; text-align:left">Response:</td><td>'
                    || nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                    to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId, 'ResponseDate'),
                    'Mon DD, YYYY')) || '</td></tr>');
                if nvl(t_AcceptableAnswer, 'N') != 'N/A' then
                  t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td
                      style="width: 250px; vertical-align:top; text-align:left">
                      Non-Acceptable Response Text:</td><td>' ||
                      api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText') || '</td></tr>');
                end if;
              end if;
            end if;
            t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
          end loop;
          -- Close the table
          t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
        end if;

        -- If there is a rel to documents creat table for the documents
        if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
          -- Table header for documents
          t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
              Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Documents</u><br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><tr><th align="left">Document Type</th>
              <th align="left">File Name</th><th align="left">Upload Date</th>
              </tr>');
          for d2 in (
              select
                api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
                dr.FileName,
                lt.CreatedDate
              from
                query.r_OLPermitRenewElectronicDoc AppDoc
                join api.documentrevisions dr
                    on AppDoc.OLDocumentId = dr.DocumentId
                join query.r_ElectronicDocDocType dt
                    on dr.DocumentId = dt.DocumentId
                join api.logicaltransactions lt
                    on dr.LogicalTransactionId = lt.LogicalTransactionId
              where AppDoc.OLPermitRenewJobId = t_JobId
              ) loop
            t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td>' || d2.doctype ||
                '</td><td>' || d2.filename || '</td><td>' || to_char(d2.Createddate, 'Mon DD, YYYY')
                || '</td></tr>');
          end loop;
          --Close the table
          t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div>';
        end if;
      end if;

      -- Retrieve active Municipal and Police Users for Municipality. Emails are sorted
      -- alphabetically
      for i in (
          select EmailAddress
          from (
              select api.pkg_ColumnQuery.Value(mu.MunicipalUserObjectId, 'EmailAddress')
                  EmailAddress
              from
                query.o_ABC_Office m
                join query.r_MunicipalUserMunicipality mu
                    on m.objectid = mu.MunicipalityObjectId
              where m.ObjectId = t_MunicipalityObjectId
                and api.pkg_columnquery.Value(mu.MunicipalUserObjectId, 'Active') = 'Y'
                and t_NotifyMunicipality = 'Y'
              union
              select t_RegistrantEmail
              from dual
              )
          order by lower(EmailAddress)
          ) loop
            t_MunicipalEmailsCLOB := t_MunicipalEmailsCLOB || to_clob('- ' || i.EmailAddress ||
                '<br>');
      end loop;

      -- Get the details needed from the system settings
      select
        ss.FromEmailAddress,
        ss.IsProduction,
        ss.ToEmailAddressNonProd,
        ss.MunicipalWebsiteBaseUrl
      into
        t_From,
        t_IsProduction,
        t_To,
        t_BaseURL
      from query.o_systemsettings ss
      where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
          'SystemSettingsNumber', 1);

      -- Conditionally get Endorsement/Standard details from system settings. When review is
      -- required, populate the email w/ Endorsement details
      if t_MunicipalityRequired = 'Y' then
        select
          ss.MuniEndEmailSubject,
          ss.MuniEndEmailHeaderPhotoURL,
          ss.MuniEndEmailBodyHeader,
          ss.MuniEndEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      -- Otherwise, send the standard email.
      else
        select
          ss.MuniStdEmailSubject,
          ss.MuniStdEmailHeaderPhotoURL,
          ss.MuniStdEmailBodyHeader,
          ss.MuniStdEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      end if;

      -- Create the body of the email in a clob. Breaking this into several clobs to concat
      -- together in hopes of making this easier to read.
      t_Body1 := '<table cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="100%"><tbody><tr><td style="padding: 20px;"><table
          border="0" cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="600"style="margin: 0px auto; border-radius: 10px;
          background-color: rgb(255, 255, 255);"><tbody><tr><td colspan="2">
          </td></tr><tr><td width="600"></td></tr><tr><td colspan="2" height="24">
          <img alt="New Jersey ABC" src="' || t_HeaderImageName || '" border="0"
          style="display: block; margin: 0px; padding-bottom: 3px;"></td></tr>
          <tr><td colspan="2"><table border="0" cellpadding="0" cellspacing="0"
          width="600"><tbody><tr><td align="left"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><br><br>';
      t_HeaderTextCLOB := to_clob(t_HeaderText);

      t_Body11 := to_clob('<br>This notification is being sent to the
          following email addresses:<br>');

      t_Body2 := to_clob('</div><div style="font-family: Calibri, Arial,
          Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
          line-height: 1.4em;"><br><table style="width:100%; font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
          rgb(0, 0, 0); line-height: 1.4em;"><tr><td>Permit Type:</td><td>');
      t_PermitTypeCLOB := to_clob(t_PermitType || '</td>');

      t_Body3 := '</tr><tr><td style="width: 250px;">File Number:</td><td>';
      t_JobNumberCLOB := to_clob(t_JobNumber || '</td>');

      t_Body4 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Permittee:</td>
          <td>');
      t_PermitteeNameCLOB := to_clob(t_PermitteeName || '</td>');

      t_Body12 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>License Number:</td>
          <td>');
      t_LicenseNumberCLOB := to_clob(t_LicenseNumber || '</td>');

      t_Body5 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Mailing Address:</td><td>');
      t_MailingAddressCLOB := to_clob(t_MailingAddress || '</td>');

      t_Body6 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Physical Address:</td><td>');
      t_PhysicalAddressCLOB := to_clob(t_PhysicalAddress || '</td>');

      t_Body7 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Contact:</td><td>');
      t_ContactNameCLOB := to_clob(t_ContactName || '</td>');

      t_Body8 := to_clob('</tr><tr><td></td><td>');
      t_ContactPhoneNumberCLOB := to_clob(t_ContactPhoneNumber || '</td><br>');

      t_Body8a := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Applicant Email:</td><td>');
      t_RegistrantEmailCLOB := to_clob(t_RegistrantEmail || '</td>');

      t_Body9 := to_clob('</tr>');
      t_FooterTextCLOB := to_clob(t_FooterText);

      -- If any type of additional information is needed create the header for the Additional info
      -- section.
      if t_County is not null or t_Municipality is not null or t_AdditionalPermitInfo is not null
          or lower(t_PermitTypeCode) = 'sa' then
        t_AdditionalPermitInfoCLOB := to_clob('<tr><td><u>Additional Permit Information</u></td></tr>');
        if t_County is not null then
          t_AdditionalPermitInfoCLOB :=  t_AdditionalPermitInfoCLOB ||
              to_clob('<tr><td>County:</td><td>' || t_County || '</td></tr>');
        end if;
        if t_Municipality is not null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_Clob('<tr><td>Municipality:</td><td>' || t_Municipality ||
              '</td></tr>');
        end if;
        if t_AdditionalPermitInfo is not null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('</table></div><div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br>' || t_AdditionalPermitInfo || '</div>');
        end if;
        if t_AdditionalPermitInfo is null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('</table></div>');
        end if;
        if lower(t_PermitTypeCode) = 'sa' then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('<div style="font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br>' || t_SAQuestion || '<br></div>');
        end if;
      end if;

      if t_AdditionalPermitInfoCLOB is null then
        t_Body10 := to_clob('</table></div>');
      end if;

      if t_EventLocationAddress is not null or t_EventLocAddressDescription is not null then
        t_EventLocationCLOB := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Location</u><br>');
        if t_EventLocAddressDescription is not null then
          t_EventLocationCLOB := t_EventLocationCLOB ||
              to_clob('Location Description:<br>' || t_EventLocAddressDescription || '<br>');
        end if;
        if t_EventLocationAddress is not null then
          t_EventLocationCLOB := t_EventLocationCLOB || to_clob('<br>Address:<br>'
              || t_EventLocationAddress || '<br>');
        end if;
        t_EventLocationCLOB := t_EventLocationCLOB || to_clob('</div>');
      end if;

      if t_EventDetails is not null then
        t_EventDetailsCLOB := to_clob('<div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><u>Event Details</u><br>What is the specific
            event being held?<br>' || t_EventDetails || '</div>');
      end if;

      t_BodyEND := to_clob('</td></tr></tbody></table></tbody></table><br></td>
          </tr></tbody></table>');
      t_Body := t_body1 || t_HeaderTextCLOB || t_Body11 || t_MunicipalEmailsCLOB ||
          t_Body2 || t_PermitTypeCLOB || t_Body3 || t_JobNumberCLOB ||
          t_body4 || t_PermitteeNameCLOB || t_Body12 || t_LicenseNumberCLOB ||
          t_Body5 || t_MailingAddressCLOB || t_Body6 || t_PhysicalAddressCLOB ||
          t_Body7 || t_ContactNameCLOB || t_Body8 || t_ContactPhoneNumberCLOB ||
          t_Body8a || t_RegistrantEmailCLOB || t_Body9 || t_AdditionalPermitInfoCLOB ||
          t_Body10 || t_EventLocationCLOB || t_EventDetailsCLOB || t_EventDatesCLOB ||
          t_RainDatesCLOB || t_QuestionsCLOB || t_DocumentListCLOB || t_FooterTextClob ||
          t_BodyEnd;

      -- Get the users that the email should be sent to
      if t_IsProduction = 'Y' then
        -- If the system is a production system send it to the user who applied
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_RegistrantEmail, null,
            null, t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
        -- If the system is a production system send it to the clerks related to the municipality.
        if t_NotifyMunicipality = 'Y' then
          for i in (
              select
                mu.MunicipalUserObjectId,
                api.pkg_ColumnQuery.Value(mu.MunicipalUserObjectId, 'EmailAddress') ToAddress
              from
                query.o_ABC_Office m
                join query.r_MunicipalUserMunicipality mu
                    on m.objectid = mu.MunicipalityObjectId
              where m.ObjectId = t_MunicipalityObjectId
              ) loop
            if api.pkg_columnquery.Value(i.MunicipalUserObjectId, 'Active') = 'Y' then
              extension.pkg_sendmail.SendEmail(t_JobId, t_From, i.toaddress, null,
                  null, t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
            end if;
          end loop;
        end if;
      else
        -- If the system is a test system send it to the test email on the config site
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_To, null, null,
            t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
      end if;
    end if;

  end MuniEmailForPermit;

  /*---------------------------------------------------------------------------
   * PoliceEmailForPermit() -- PUBLIC
   *   Send notification Email to Police Users / Applicant on Permit
   * Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure PoliceEmailForPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_MunicipalityObjectId              udt_id;
    t_JobId                             integer;
    t_PermitObjectId                    integer;
    t_PermitteeObjectId                 integer;
    t_NotifyPolice                      varchar2(1);
    t_PoliceRequired                    varchar2(1);
    t_From                              varchar2(4000);
    t_Subject                           varchar2(4000);
    t_IsProduction                      varchar2(1);
    t_To                                varchar2(4000);
    t_HeaderImageName                   varchar2(4000);
    t_HeaderText                        varchar2(4000);
    t_FooterText                        varchar2(4000);
    t_ConfigText                        varchar2(4000);
    t_baseURL                           varchar2(4000);
    t_JobDefName                        varchar2(200);
    t_PermitType                        varchar2(200);
    t_PermitTypeCode                    varchar2(20);
    t_JobNumber                         varchar2(200);
    t_PermitteeName                     varchar2(200);
    t_LicenseNumber                     varchar2(50);
    t_MailingAddress                    varchar2(4000);
    t_PhysicalAddress                   varchar2(4000);
    t_ContactName                       varchar2(250);
    t_ContactPhoneNumber                varchar2(50);
    t_County                            varchar2(200);
    t_Municipality                      varchar2(200);
    t_AdditionalPermitInfo              varchar2(4000);
    t_SAQuestion                        varchar2(500);
    t_EventLocationAddress              varchar2(4000);
    t_EventLocAddressDescription        varchar2(4000);
    t_EventDetails                      varchar2(4000);
    t_AnswerOnJob                       varchar2(2000);
    t_AcceptableAnswer                  varchar2(2000);
    t_RegistrantEmail                   varchar2(4000);
    t_OnlineUserId                      integer;
    t_EventDatesCLOB                    clob;
    t_RainDatesCLOB                     clob;
    t_QuestionsCLOB                     clob;
    t_DocumentListCLOB                  clob;
    t_Body                              clob;
    t_Body1                             clob;
    t_HeaderTextCLOB                    clob;
    t_FooterTextCLOB                    clob;
    t_Body2                             clob;
    t_PermitTypeCLOB                    clob;
    t_Body3                             clob;
    t_JobNumberCLOB                     clob;
    t_Body4                             clob;
    t_PermitteeNameCLOB                 clob;
    t_Body12                            clob;
    t_LicenseNumberCLOB                 clob;
    t_Body5                             clob;
    t_MailingAddressCLOB                clob;
    t_Body6                             clob;
    t_PhysicalAddressCLOB               clob;
    t_Body7                             clob;
    t_ContactNameCLOB                   clob;
    t_Body8                             clob;
    t_ContactPhoneNumberCLOB            clob;
    t_Body8a                            clob;
    t_RegistrantEmailCLOB               clob;
    t_Body9                             clob;
    t_AdditionalPermitInfoCLOB          clob;
    t_Body10                            clob;
    t_BodyEND                           clob;
    t_EventLocationCLOB                 clob;
    t_EventDetailsCLOB                  clob;
    t_Body11                            clob;
    t_PoliceEmailsCLOB                  clob;
  begin

    -- Collect all data needed for the email.  The email will be generated after all data is
    -- collected. This will be run on completion of a process which means we will need to get
    -- the JobId
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    -- Need the Job def name to determine what rels to use to get the required information
    t_JobDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_PermitType := api.pkg_ColumnQuery.Value(t_JobId, 'PermitType');
    -- Check to see if the permit type requires the muni or police to be notified.
    t_NotifyPolice := api.pkg_ColumnQuery.Value(
        api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeObjectId'), 'NotifyPolice');
    -- Check to see if the permit type requires muni or police review
    t_PoliceRequired := api.pkg_ColumnQuery.Value(
        api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeObjectId'), 'PoliceRequired');

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      -- Get the information needed that can be found through api.pkg_ColumnQuery
      t_OnlineUserId := api.pkg_ColumnQuery.NumericValue(t_JobId, 'OnlineUserObjectId');
      t_RegistrantEmail := api.pkg_columnquery.value(t_OnlineUserId, 'EmailAddress');
      t_JobNumber := api.pkg_ColumnQuery.Value(t_JobId, 'ExternalFileNum');
      t_PermitObjectId := api.pkg_ColumnQuery.Value(t_JobId, 'PermitObjectId');
      t_PermitTypeCode := api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeCode');
      t_LicenseNumber := coalesce(api.pkg_ColumnQuery.Value(t_PermitObjectid, 'LicenseNumber'),
            'N/A');
      t_PermitteeObjectId := api.pkg_ColumnQuery.Value(t_PermitObjectId, 'PermitteeObjectId');
      t_PermitteeName := nvl(api.pkg_ColumnQuery.Value(t_PermitObjectId, 'PermitteeName'),
          api.Pkg_Columnquery.Value(t_PermitObjectId, 'OnlineLEFormattedName'));
      t_MailingAddress := replace(nvl(
          api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'MailingAddress'),
          api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineLEMailingAddress')),
          chr(13) || chr(10), '<br>');
      if api.pkg_columnquery.Value(t_JobId,'OnlineLESameAsMailing') = 'Y' then
         t_PhysicalAddress := t_MailingAddress;
      else
        t_PhysicalAddress := replace(nvl(
            api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'PhysicalAddress'),
            api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePhysicalAddress')),
            chr(13)||chr(10), '<br>');
      end if;
      t_ContactName := coalesce(api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'ContactName'),
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactName'), t_PermitteeName);
      t_ContactPhoneNumber := REGEXP_REPLACE(nvl(
          api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'ContactPhoneNumber'),
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactPhone')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');
      t_County := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'County');
      t_Municipality := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'Municipality');
      t_MunicipalityObjectId := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'MunicipalityObjectId');
      t_AdditionalPermitInfo := api.pkg_ColumnQuery.Value(t_PermitObjectId,
          'AdditionalPermitInformation');
      t_EventLocationAddress := replace(nvl(
          nvl(api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineEventLocationAddress'),
          api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineEventLocationAddress')),
          api.Pkg_Columnquery.Value(t_PermitObjectId, 'EventLocationAddress')),
          chr(13)||chr(10), '<br>');
      t_EventLocAddressDescription := api.pkg_ColumnQuery.Value(t_PermitObjectId,
          'EventLocAddressDescription');
      t_EventDetails := api.pkg_ColumnQuery.Value(t_PermitObjectId, 'EventDetails');

      -- If permit is a Social Affair permit get the answer to the SA Question.
      if lower(t_PermitTypeCode) = 'sa' then
        t_SAQuestion := 'Was the Non-Profit Group/Organization formed as a ' ||
            'Religious, Civic or Educational Entity?<br>' ||
            nvl(api.pkg_ColumnQuery.Value(t_JobId,'OnlineCivicReligOrEduc'),
            api.pkg_ColumnQuery.Value(t_JobId, 'CivicReligOrEduc'));
      end if;

      -- Get the event dates and convert to clob with HTML formatting for the body of the email.
      for e in (
          select ed.EventDateObjectId
          from query.r_Abc_Permiteventdate ed
          where ed.PermitObjectId = t_PermitObjectId
          ) loop
        t_EventDatesCLOB := t_EventDatesCLOB || to_clob(to_char(
            api.pkg_ColumnQuery.DateValue(e.eventdateobjectid, 'StartDate'), 'Mon DD, YYYY'));
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'StartTimeHours') is not null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || ' from ' ||
              api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'FormattedStartTime'));
        end if;
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'EndTimeHours') is not null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || ' to ' ||
              api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'FormattedEndTime') || '<br>');
        end if;
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'EndTimeHours') is null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || '<br>');
        end if;
      end loop;
      -- Add Event Dates header to t_EventDatesCLOB if there were any event dates.
      if t_EventDatesCLOB is not null then
        t_EventDatesCLOB := '<br><div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"> <u>Event Dates</u><br>' || t_EventDatesCLOB ||
            '</div>';
      end if;

      -- Get the Rain Dates and convert to clob with HTML formatting for the body of the email.
      for r in (
          select rd.RainDateObjectId
          from query.r_ABC_PermitRainDate rd
          where rd.PermitObjectId = t_PermitObjectId
          ) loop
        t_RainDatesCLOB := t_RainDatesCLOB || to_clob(to_char(
            api.pkg_ColumnQuery.DateValue(r.RainDateObjectId, 'StartDate'), 'Mon DD, YYYY'));
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'StartTimeHours') is not null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || ' from ' ||
              api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'FormattedStartTime'));
        end if;
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'EndTimeHours') is not null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || ' to ' ||
              api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'FormattedEndTime') || '<br>');
        end if;
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'EndTimeHours') is null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || '<br>');
        end if;
      end loop;
      -- Add Rain Dates header to t_RainDatesCLOB if there were any rain dates.
      if t_RainDatesCLOB is not null then
        t_RainDatesCLOB := '<br><u>Rain Dates</u><br>' || t_RainDatesCLOB;
      end if;

      -- Get the information from the Permit Application.
      if t_JobDefName = 'j_ABC_PermitApplication' then
        -- If there is a rel to questions start creation of the HTML table
        if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
          -- Table header
          t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br><u>Application Questions</u><br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;">');
           -- Loop through all QAResponses
          for q in (
               select qa.ResponseId
               from query.r_ABC_PermitAppQAResponse qa
               where qa.PermitApplicationId = t_JobId
               order by api.pkg_columnquery.NumericValue(qa.ResponseId, 'SortOrder')
               ) loop
            t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Question ' ||
                api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') || ':</td>' ||
                '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'Question') || '</td></tr>');
            if nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A' then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                  "vertical-align:top; text-align:left">Yes / No Response:</td>' ||
                  '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
            end if;
            t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.responseid, 'AnswerOnJob');
            t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.responseid, 'AcceptableAnswer');
            if nvl(t_AnswerOnJob, 'N') != nvl(t_AcceptableAnswer, 'Y')
                and t_AcceptableAnswer is not null then
              if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                  then
                if nvl(t_AcceptableAnswer, 'N') != 'N/A' then
                  t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                      "width: 250px; vertical-align:top; text-align:left"></td>'
                      || '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText')
                      || '</td></tr>');
                end if;
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "vertical-align:top; text-align:left">Response:</td><td>'
                    || nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                    to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId,
                    'ResponseDate'), 'Mon DD, YYYY')) || '</td></tr>');
               end if;
            end if;
            t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
          end loop;
          -- Close the table
          t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
        end if;

        -- If there is a rel to documents create table for the documents
        if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
          -- Table header for documents
          t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
              Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Documents</u><br>
              See below for a list of documents attached to this email.<br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><tr><th align="left">Document Type</th>
              <th align="left">File Name</th><th align="left">Upload Date</th>
              </tr>');

          -- Loop through Online Uploads
          for d2 in (
              select
                api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
                dr.FileName,
                lt.CreatedDate
              from query.r_OLNewPermitAppElectronicDoc AppDoc
              join api.documentrevisions dr
                  on AppDoc.OLDocumentId = dr.DocumentId
              join query.r_ElectronicDocDocType dt
                  on dr.DocumentId = dt.DocumentId
              join api.logicaltransactions lt
                  on dr.LogicalTransactionId = lt.LogicalTransactionId
              where AppDoc.OLNewPermitAppJobId = t_JobId
              ) loop
            t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td>' || d2.doctype ||
                '</td><td>' || d2.filename || '</td><td>' || to_char(d2.Createddate, 'Mon DD, YYYY')
                || '</td></tr>');
          end loop;
          -- Close the table
          t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div>';
        end if;
      end if;

      -- Get the information from the Permit Renewal.
      if t_JobDefName = 'j_ABC_PermitRenewal' then
        -- If there is a rel to questions start creation of the HTML table
        if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
          -- Table header
          t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Application Questions
              </u><br><table style="width:100%; font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;">');
          -- Loop through all QAResponses
          for q in (
              select qa.ResponseId
              from query.r_ABC_PermitRnwQAResponse qa
              where qa.PermitRenewalId = t_JobId
              order by api.pkg_columnquery.NumericValue(qa.ResponseId, 'SortOrder')
              ) loop
            t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Question ' ||
                api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') ||
                ':</td><td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'Question')
                || '</td></tr>');
            if nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A' then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                  "vertical-align:top; text-align:left">Yes / No Response:</td><td>'
                  || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
            end if;
            t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.responseid, 'AnswerOnJob');
            t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.responseid, 'AcceptableAnswer');
            if nvl(t_AnswerOnJob, 'N') != nvl(t_AcceptableAnswer, 'Y')
                and t_AcceptableAnswer is not null then
              if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                  then
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "vertical-align:top; text-align:left">Response:</td><td>'
                    || nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                    to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId, 'ResponseDate'),
                    'Mon DD, YYYY')) || '</td></tr>');
                if nvl(t_AcceptableAnswer, 'N') != 'N/A' then
                  t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td
                      style="width: 250px; vertical-align:top; text-align:left">
                      Non-Acceptable Response Text:</td><td>' ||
                      api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText') || '</td></tr>');
                end if;
              end if;
            end if;
            t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
          end loop;
          -- Close the table
          t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
        end if;

        -- If there is a rel to documents creat table for the documents
        if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
          -- Table header for documents
          t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
              Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Documents</u><br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><tr><th align="left">Document Type</th>
              <th align="left">File Name</th><th align="left">Upload Date</th>
              </tr>');
          for d2 in (
              select
                api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
                dr.FileName,
                lt.CreatedDate
              from
                query.r_OLPermitRenewElectronicDoc AppDoc
                join api.documentrevisions dr
                    on AppDoc.OLDocumentId = dr.DocumentId
                join query.r_ElectronicDocDocType dt
                    on dr.DocumentId = dt.DocumentId
                join api.logicaltransactions lt
                    on dr.LogicalTransactionId = lt.LogicalTransactionId
              where AppDoc.OLPermitRenewJobId = t_JobId
              ) loop
            t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td>' || d2.doctype ||
                '</td><td>' || d2.filename || '</td><td>' || to_char(d2.Createddate, 'Mon DD, YYYY')
                || '</td></tr>');
          end loop;
          --Close the table
          t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div>';
        end if;
      end if;

      -- Retrieve active Municipal and Police Users for Municipality. Emails are sorted
      -- alphabetically
      for i in (
          select EmailAddress
          from (
              select u.EmailAddress
              from query.u_users u
              join query.r_policeusermunicipality pu
                  on u.ObjectId = pu.PoliceUserObjectId
              where pu.MunicipalityObjectId = t_MunicipalityObjectId
                and u.Active = 'Y'
                and t_NotifyPolice = 'Y'
              union
              select
                t_RegistrantEmail
              from dual
              )
          order by lower(EmailAddress)
          ) loop
            t_PoliceEmailsCLOB := t_PoliceEmailsCLOB || to_clob('- ' || i.EmailAddress || '<br>');
      end loop;

      -- Get the details needed from the system settings
      select
        ss.FromEmailAddress,
        ss.IsProduction,
        ss.ToEmailAddressNonProd,
        ss.PoliceWebsiteBaseUrl
      into
        t_From,
        t_IsProduction,
        t_To,
        t_BaseURL
      from query.o_systemsettings ss
      where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
          'SystemSettingsNumber', 1);

      -- Conditionally get Endorsement/Standard details from system settings. When review is
      -- required, populate the email w/ Endorsement details
      if t_PoliceRequired = 'Y' then
        select
          ss.PoliceEndEmailSubject,
          ss.PoliceEndEmailHeaderPhotoURL,
          ss.PoliceEndEmailBodyHeader,
          ss.PoliceEndEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      -- Otherwise, send the standard email.
      else
        select
          ss.PoliceStdEmailSubject,
          ss.PoliceStdEmailHeaderPhotoURL,
          ss.PoliceStdEmailBodyHeader,
          ss.PoliceStdEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      end if;

      -- Create the body of the email in a clob. Breaking this into several clobs to concat
      -- together in hopes of making this easier to read.
      t_Body1 := '<table cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="100%"><tbody><tr><td style="padding: 20px;"><table
          border="0" cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="600"style="margin: 0px auto; border-radius: 10px;
          background-color: rgb(255, 255, 255);"><tbody><tr><td colspan="2">
          </td></tr><tr><td width="600"></td></tr><tr><td colspan="2" height="24">
          <img alt="New Jersey ABC" src="' || t_HeaderImageName || '" border="0"
          style="display: block; margin: 0px; padding-bottom: 3px;"></td></tr>
          <tr><td colspan="2"><table border="0" cellpadding="0" cellspacing="0"
          width="600"><tbody><tr><td align="left"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><br><br>';
      t_HeaderTextCLOB := to_clob(t_HeaderText);

      t_Body11 := to_clob('<br>This notification is being sent to the
          following email addresses:<br>');

      t_Body2 := to_clob('</div><div style="font-family: Calibri, Arial,
          Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
          line-height: 1.4em;"><br><table style="width:100%; font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
          rgb(0, 0, 0); line-height: 1.4em;"><tr><td>Permit Type:</td><td>');
      t_PermitTypeCLOB := to_clob(t_PermitType || '</td>');

      t_Body3 := '</tr><tr><td style="width: 250px;">File Number:</td><td>';
      t_JobNumberCLOB := to_clob(t_JobNumber || '</td>');

      t_Body4 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Permittee:</td>
          <td>');
      t_PermitteeNameCLOB := to_clob(t_PermitteeName || '</td>');

      t_Body12 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>License Number:</td>
          <td>');
      t_LicenseNumberCLOB := to_clob(t_LicenseNumber || '</td>');

      t_Body5 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Mailing Address:</td><td>');
      t_MailingAddressCLOB := to_clob(t_MailingAddress || '</td>');

      t_Body6 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Physical Address:</td><td>');
      t_PhysicalAddressCLOB := to_clob(t_PhysicalAddress || '</td>');

      t_Body7 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Contact:</td><td>');
      t_ContactNameCLOB := to_clob(t_ContactName || '</td>');

      t_Body8 := to_clob('</tr><tr><td></td><td>');
      t_ContactPhoneNumberCLOB := to_clob(t_ContactPhoneNumber || '</td><br>');

      t_Body8a := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Applicant Email:</td><td>');
      t_RegistrantEmailCLOB := to_clob(t_RegistrantEmail || '</td>');

      t_Body9 := to_clob('</tr>');
      t_FooterTextCLOB := to_clob(t_FooterText);

      -- If any type of additional information is needed create the header for the Additional info
      -- section.
      if t_County is not null or t_Municipality is not null or t_AdditionalPermitInfo is not null
          or lower(t_PermitTypeCode) = 'sa' then
        t_AdditionalPermitInfoCLOB := to_clob('<tr><td><u>Additional Permit Information</u></td></tr>');
        if t_County is not null then
          t_AdditionalPermitInfoCLOB :=  t_AdditionalPermitInfoCLOB ||
              to_clob('<tr><td>County:</td><td>' || t_County || '</td></tr>');
        end if;
        if t_Municipality is not null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_Clob('<tr><td>Municipality:</td><td>' || t_Municipality ||
              '</td></tr>');
        end if;
        if t_AdditionalPermitInfo is not null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('</table></div><div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br>' || t_AdditionalPermitInfo || '</div>');
        end if;
        if t_AdditionalPermitInfo is null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('</table></div>');
        end if;
        if lower(t_PermitTypeCode) = 'sa' then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('<div style="font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br>' || t_SAQuestion || '<br></div>');
        end if;
      end if;

      if t_AdditionalPermitInfoCLOB is null then
        t_Body10 := to_clob('</table></div>');
      end if;

      if t_EventLocationAddress is not null or t_EventLocAddressDescription is not null then
        t_EventLocationCLOB := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Location</u><br>');
        if t_EventLocAddressDescription is not null then
          t_EventLocationCLOB := t_EventLocationCLOB ||
              to_clob('Location Description:<br>' || t_EventLocAddressDescription || '<br>');
        end if;
        if t_EventLocationAddress is not null then
          t_EventLocationCLOB := t_EventLocationCLOB || to_clob('<br>Address:<br>'
              || t_EventLocationAddress || '<br>');
        end if;
        t_EventLocationCLOB := t_EventLocationCLOB || to_clob('</div>');
      end if;

      if t_EventDetails is not null then
        t_EventDetailsCLOB := to_clob('<div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><u>Event Details</u><br>What is the specific
            event being held?<br>' || t_EventDetails || '</div>');
      end if;

      t_BodyEND := to_clob('</td></tr></tbody></table></tbody></table><br></td>
          </tr></tbody></table>');
      t_Body := t_body1 || t_HeaderTextCLOB || t_Body11 || t_PoliceEmailsCLOB ||
          t_Body2 || t_PermitTypeCLOB || t_Body3 || t_JobNumberCLOB ||
          t_body4 || t_PermitteeNameCLOB || t_Body12 || t_LicenseNumberCLOB ||
          t_Body5 || t_MailingAddressCLOB || t_Body6 || t_PhysicalAddressCLOB ||
          t_Body7 || t_ContactNameCLOB || t_Body8 || t_ContactPhoneNumberCLOB ||
          t_Body8a || t_RegistrantEmailCLOB || t_Body9 || t_AdditionalPermitInfoCLOB ||
          t_Body10 || t_EventLocationCLOB || t_EventDetailsCLOB || t_EventDatesCLOB ||
          t_RainDatesCLOB || t_QuestionsCLOB || t_DocumentListCLOB || t_FooterTextClob ||
          t_BodyEnd;

      -- Get the users that the email should be sent to
      if t_IsProduction = 'Y' then
        -- If the system is a production system send it to the user who applied
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_RegistrantEmail, null,
            null, t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
        -- If the system is a production system send it to the Police users related to the
        -- municipality.
        if t_NotifyPolice = 'Y' then
          for i in (
              select
                u.UserId,
                u.EmailAddress,
                u.Active
              from
                query.u_users u
                join query.r_policeusermunicipality pu
                    on u.ObjectId = pu.PoliceUserObjectId
                where pu.MunicipalityObjectId = t_MunicipalityObjectId
                ) loop
            if i.Active = 'Y' then
              extension.pkg_sendmail.SendEmail(t_JobId, t_From, i.EmailAddress, null,
                  null, t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
            end if;
          end loop;
        end if;
      else
        -- If the system is a test system send it to the test email on the config site
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_To, null, null,
            t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
      end if;
    end if;

  end PoliceEmailForPermit;

  /*---------------------------------------------------------------------------
  * ReviewEmailReminder() -- PUBLIC
  *   Send reminder Email to Municipal Users / Applicant on Permit Application/
  * Renewal
  *-------------------------------------------------------------------------*/
  procedure ReviewEmailReminder (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_MunicipalityObjectId              udt_id;
    t_Body                              clob;
    t_FromEmailAddress                  varchar2(4000);
    t_IsProd                            varchar2(1);
    t_ProcessType                       varchar2(4000);
    t_Subject                           varchar2(4000);
    t_ToAddress                         varchar2(4000);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_MuniSubject                       varchar2(4000);
    t_PoliceSubject                     varchar2(4000);
    t_NoteDefDescription                varchar2(60);
    t_SystemSettingsObjId               udt_Id;
    t_MunicipalReviewType               udt_Id;
    t_PoliceReviewType                  udt_Id;
    t_NoteTag                           varchar2(10);
    t_IsProduction                      varchar2(1);
    t_UserIdList                        api.pkg_definition.udt_IdList;
  begin

    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    t_ProcessType := api.pkg_columnquery.value(a_ObjectId, 'ObjectDefId');
    t_MunicipalReviewType := api.pkg_configquery.ObjectDefIdForName('p_ABC_MunicipalityReview');
    t_PoliceReviewType := api.pkg_configquery.ObjectDefIdForName('p_ABC_PoliceReview');
    t_MunicipalityObjectId := api.pkg_columnquery.value(a_ObjectId, 'MunicipalityObjectId');

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      -- Get the details needed from the system settings
      select ss.FromEmailAddress,
             ss.IsProduction,
             ss.ToEmailAddressNonProd,
             ss.MuniRevReminderEmailSubject,
             ss.PoliceRevReminderEmailSubject,
             ss.objectid
      into t_FromEmailAddress,
           t_IsProduction,
           t_ToEmailAddressNonProd,
           t_MuniSubject,
           t_PoliceSubject,
           t_SystemSettingsObjId
      from query.o_systemsettings ss;

      if t_ProcessType = t_MunicipalReviewType then
        t_NoteTag := 'MRREML';
        t_Subject := t_MuniSubject;
        select mu.MunicipalUserObjectId
        bulk collect
        into t_UserIdList
        from query.r_MunicipalUserMunicipality mu
        where mu.MunicipalityObjectId = t_MunicipalityObjectId;

      elsif t_ProcessType = t_PoliceReviewType then
        t_NoteTag := 'PRREML';
        t_Subject := t_PoliceSubject;
        select pu.PoliceUserObjectId
        bulk collect
        into t_UserIdList
        from query.r_policeusermunicipality pu
        where pu.MunicipalityObjectId = t_MunicipalityObjectId;
      else
        return;
      end if;

      select n.NoteText
      into t_Body
      from api.notes n
      join api.notedefs nd
        on nd.NoteDefId = n.NoteDefId
      where nd.tag = t_NoteTag
        and n.ObjectId = t_SystemSettingsObjId;

      -- Make substitutions
      t_Body := replace(t_Body, '{FirstName}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'FirstName'));
      t_Body := replace(t_Body, '{LastName}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'LastName'));
      t_Body := replace(t_Body, '{EmailAddress}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'EmailAddress'));
      t_Body := replace(t_Body, '{PhoneNumber}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'PhoneNumber'));
      t_Body := replace(t_Body, '{FormattedPhoneNumber}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'FormattedPhoneNumber'));
      t_Body := replace(t_Body, '{JobNumber}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'FileNumber'));

      -- Get the users that the email should be sent to
      if t_IsProduction = 'Y' then
        -- If the system is a production system send it to the clerks related
        -- to the municipality.
          for i in 1..t_UserIdList.count loop
            if api.pkg_columnquery.Value(t_UserIdlist(i), 'Active') = 'Y' then
              t_ToAddress := api.pkg_columnquery.Value(t_UserIdList(i),'EmailAddress');
              -- Verify that To Address is not null
              continue when t_ToAddress is not null;
              extension.pkg_sendmail.SendEmail(t_JobId, t_FromEmailAddress, t_ToAddress, null,
                  null, t_Subject,t_Body, null, null, 'Y', 'N');
            end if;
          end loop;
      else
        -- Verify that To Email Address Non Prod is not null
        if t_ToEmailAddressNonProd is null then
          return;
        end if;
        -- If the system is a test system send it to the test email on the config
        -- site
        extension.pkg_sendmail.SendEmail(t_JobId, t_FromEmailAddress, t_ToEmailAddressNonProd,
            null, null, t_Subject, t_Body, null, null, 'Y', 'N');
      end if;
    end if;
  end ReviewEmailReminder;

end pkg_ABC_municipality;
/

