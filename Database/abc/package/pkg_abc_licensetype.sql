create or replace package pkg_ABC_LicenseType is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * PostVerifyWrapper
   *  Runs on Post-Verify of the ABC License Type.
   *-------------------------------------------------------------------------*/
  procedure PostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

end pkg_ABC_LicenseType;
/
create or replace package body pkg_ABC_LicenseType is

  /*---------------------------------------------------------------------------
   * PostVerifyWrapper
   *  Runs on Post-Verify of the ABC License Type.
   *-------------------------------------------------------------------------*/
  procedure PostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_IssuingAuthority    varchar2(100);
    t_CheckDate           varchar2(100);
  begin
    -- Get Issuing Authority
    t_IssuingAuthority := api.pkg_ColumnQuery.Value(a_ObjectId, 'IssuingAuthority');
    -- Check for Municipality in Issuing Authority
    if t_IssuingAuthority = 'Municipality' then
      -- Check for relationship to Default Licensing Clerk
      for c in (select r.RelationshipId
                from query.r_ABC_LicenseTypeDefaultClerk r
                where r.LicenseTypeId = a_ObjectId) loop
        -- Relationship exists! Need to delete.
        api.pkg_RelationshipUpdate.Remove(c.RelationshipId, a_AsOfDate);
      end loop;

      if api.pkg_columnquery.Value(a_ObjectId, 'ExpirationMethod') = 'Manual' then
        api.pkg_errors.RaiseError(-20000,
          'Expiration Method may not be set to Manual if the Issuing Authority is Municipality.');
      elsif api.pkg_columnquery.Value(a_ObjectId, 'ExpirationMethod') = 'Never' then
        api.pkg_errors.RaiseError(-20000,
          'Expiration Method may not be set to Never if the Issuing Authority is Municipality.');
      end if;
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'ExpirationMethod') = 'Seasonal' then
       if (api.pkg_columnquery.Value(a_ObjectId, 'ExpirationStartMonth') is null or
           api.pkg_columnquery.Value(a_ObjectId, 'ExpirationStartDay') is null)   then
           api.pkg_errors.RaiseError(-20000, 'Expiration Method of Seasonal requires a Starting Date.');
       end if;
       if (api.pkg_columnquery.Value(a_ObjectId, 'ExpirationEndMonth') is null or
           api.pkg_columnquery.Value(a_ObjectId, 'ExpirationEndDay') is null)   then
           api.pkg_errors.RaiseError(-20000, 'Expiration Method of Seasonal requires an Ending Date.');
       end if;
       begin
          t_CheckDate := to_Date(api.pkg_columnquery.Value(a_ObjectId, 'ExpirationStartMonth')
              || '/' || api.pkg_columnquery.Value(a_ObjectId, 'ExpirationStartDay'), 'MM/DD');
       exception
          when others then
             api.pkg_errors.RaiseError(-20000, 'Starting Date is invalid.');
       end;
       begin
          t_CheckDate := to_Date(api.pkg_columnquery.Value(a_ObjectId, 'ExpirationEndMonth')
              || '/' || api.pkg_columnquery.Value(a_ObjectId, 'ExpirationEndDay'), 'MM/DD');
       exception
          when others then
             api.pkg_errors.RaiseError(-20000, 'Ending Date is invalid.');
       end;
    end if;

  end PostVerifyWrapper;

end pkg_ABC_LicenseType;
/
