create or replace package pkg_ABC_Metadata is

  -- Author  : BRUCE.JAKEWAY
  -- Created : 2011 Apr 19 10:35:45
  -- Purpose : Procedures for Metadata objects

  -- Public type declarations
  subtype udt_Id is api.pkg_Definition.udt_Id;


  -- Function and procedure implementations

  ------------------------------------------------------------------------------
  -- WordTemplateToSystemSettings
  --  Relate the Word Interface Template to the System Settings object if it is
  -- not already.
  ------------------------------------------------------------------------------
  procedure WordTemplateToSystemSettings(
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );
  
  -----------------------------------------------------------------------------
  -- Renewal Questions Errors
  -- Validates requirements for the questions grid on license types
  -----------------------------------------------------------------------------
  procedure RenewalQuestionsErrors (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );
  
  /*---------------------------------------------------------------------------
   * CanDeleteWordInterfaceTemplate()
   *   Raises an error when deleting the Word Interface Template if there are
   * unsigned letters for it.
   *-------------------------------------------------------------------------*/
  procedure CanDeleteWordInterfaceTemplate (
    a_ObjectId                          udt_Id, 
    a_AsOfDate                          date
  );

end pkg_ABC_Metadata;
/
create or replace package body pkg_ABC_Metadata is

  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  -- Function and procedure implementations\

  ------------------------------------------------------------------------------
  -- WordTemplateToSystemSettings
  --  Relate the Word Interface Template to the System Settings object if it is
  -- not already.
  ------------------------------------------------------------------------------
  procedure WordTemplateToSystemSettings(
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_Count                             number;
    t_SystemSettingsObjectId            udt_Id;
    t_WordTemplateDocumentId            udt_Id;

  begin
    select o.ObjectId
      into t_SystemSettingsObjectId
      from query.o_SystemSettings o
      where rownum <= 1;

   select rptwt.WordTemplateDocumentId
     into t_WordTemplateDocumentId
     from query.r_ProcessTypeWordInterfaceTemp rptwt
     where rptwt.RelationshipId = a_RelationshipId;

    select count(*)
      into t_Count
      from query.r_SystemSettingsLetterTemplate rsslt
      where rsslt.SystemSettingsObjectId = t_SystemSettingsObjectId
        and rsslt.WordTemplateDocumentId = t_WordTemplateDocumentId;

    if t_Count = 0 then
      extension.pkg_RelationshipUpdate.New(t_SystemSettingsObjectId, t_WordTemplateDocumentId, 'LetterTemplate');
    end if;
  end WordTemplateToSystemSettings;
  
  -----------------------------------------------------------------------------
  -- Renewal Questions Errors
  -- Validates requirements for the questions grid on license types
  -----------------------------------------------------------------------------
  procedure RenewalQuestionsErrors (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
  
    t_CriminalConvictionCount      number;
    t_ConflictOfInterestCount      number;

  begin
    
/*    select count(rq.CriminalConviction), count(rq.ConflictOfInterest)
      into t_CriminalConvictionCount, t_ConflictOfInterestCount
      from query.r_ABC_LicTypeRenewQuestion ltrq 
      join query.o_abc_renewalquestion rq
        on ltrq.QuestionId = rq.ObjectId
     where licensetypeid = (select ltr.LicenseTypeId
                              from query.r_ABC_LicTypeRenewQuestion ltr
                             where ltr.QuestionId = a_ObjectId);
       
     if t_CriminalConvictionCount > 1 then
       api.pkg_errors.QueueBusinessErrorForColumn('Criminal Conviction may only be selected for one question.'
                                                  , a_ObjectId
                                                  , api.pkg_configquery.ColumnDefIdForName('o_ABC_RenewalQuestion', 'CriminalConviction'));
     end if;
       
     if t_ConflictOfInterestCount > 1 then
       api.pkg_errors.QueueBusinessErrorForColumn('Conflict Of Interest may only be selected for one question.'
                                                  , a_ObjectId
                                                  , api.pkg_configquery.ColumnDefIdForName('o_ABC_RenewalQuestion', 'ConflictOfInterest'));
     end if;*/
     
     api.pkg_errors.RaiseQueuedErrors();
     
  end RenewalQuestionsErrors;

  /*---------------------------------------------------------------------------
   * CanDeleteWordInterfaceTemplate() -- PUBLIC
   *   Raises an error when deleting the Word Interface Template if there are
   * unsigned letters for it.
   *-------------------------------------------------------------------------*/
  procedure CanDeleteWordInterfaceTemplate (
    a_ObjectId                          udt_Id, 
    a_AsOfDate                          date
  ) is
    t_WordLetters                       udt_IdList;
    t_Extension                         varchar2(20);
  begin
    t_WordLetters := api.pkg_SimpleSearch.ObjectsByIndex('d_WordMergeLetter',
        'd_WordMergeTemplate_ObjectId', a_ObjectId);
        
    for w in 1..t_WordLetters.count() loop
      select extension
      into t_Extension
      from api.documentrevisions
      where documentId = t_WordLetters(w);
      
      if t_Extension = 'docm' then
        api.pkg_Errors.RaiseError(-20100, 'The template cannot be deleted, as there are '
            || 'unsigned letters using it.');
      end if;
    end loop;
    
  end CanDeleteWordInterfaceTemplate;

end pkg_ABC_Metadata;
/
