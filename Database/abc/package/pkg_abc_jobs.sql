create or replace package abc.pkg_ABC_Jobs is

  /*---------------------------------------------------------------------------
   * Public type declarations
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  -----------------------------------------------------------------------------
  -- ExpirationJobPostVerify
  -- Create p_ABC_TakeExpirationActions process and complete it based on condition
  -----------------------------------------------------------------------------
  procedure ExpirationJobPostVerify (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- InspectionJobPostVerify
  -- Create p_ABC_InitiateInspection process and complete it based on condition
  -----------------------------------------------------------------------------
  procedure InspectionJobPostVerify (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- SetupPaymentAdjustment
  --  Set up the Payment Adjustment job with the appropriate pay distribution
  --  objects.
  --  Developer: Andy Patterson (CXUSA)
  --  Created On: 7/31/2014
  -----------------------------------------------------------------------------
  procedure SetupPaymentAdjustment (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- AdjustPayments
  --  Complete the Payment Adjustment job with the appropriate pay distributions
  --  and adjustments to the fee/payment.
  --  Developer: Andy Patterson (CXUSA)
  --  Created On: 7/31/2014
  -----------------------------------------------------------------------------
  procedure AdjustPayments (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  ----------------------------------------------------------------------------
  --  RemovePaymentsFromDailyDeposit
  --  Run on Daily Deposit job to remove selected payment records.
  ----------------------------------------------------------------------------
  procedure RemovePaymentsFromDailyDeposit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * AutoCancelJobs()
   *   Run as a processes server nightly procedure to cancel all jobs with
   * Automatic Cancellation enabled that have exceeded the cancellation period.
   *-------------------------------------------------------------------------*/
  procedure AutoCancelJobs;

  /*---------------------------------------------------------------------------
   * AutoCancelJobsNotification()
   *   Run as a processes server nightly procedure to notify applicants of
   * the jobs that will be automatically cancelled based on the Send
   * Cancellation Notification flag and Days To Notify.
   *-------------------------------------------------------------------------*/
  procedure AutoCancelJobsNotification;

end pkg_ABC_Jobs;
/

grant execute
on abc.pkg_abc_Jobs
to posseextensions;

create or replace package body abc.pkg_ABC_Jobs is

  -----------------------------------------------------------------------------
  -- ExpirationJobPostVerify
  --  Create p_ABC_TakeExpirationActions process and complete it based on condition
  --
  -- Change History:
  -- 08Aug2011, Stan H: Status on License is changed to Expired only for Active licenses.
  --09Sep2015, Josh L: Modified to include permit functionality as well.
  -----------------------------------------------------------------------------
  procedure ExpirationJobPostVerify (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is
    t_ExpirationLicenseEPId             udt_Id :=
       api.pkg_ConfigQuery.EndPointIdForName('j_ABC_Expiration', 'License');
    --t_LicensesExist                     boolean := false;
    t_ProcessId                         udt_Id;
    t_ProcessTypeId                     udt_Id :=
       api.pkg_ConfigQuery.ObjectDefIdForName('p_ABC_TakeExpirationActions');
    t_IsSpecialEvent                    varchar(1);
    t_RelId                             udt_Id;
    t_UserId                            udt_Id;
    t_CountProcessId                    number;
    t_LicenseId                         udt_Id;
    t_PermitId                          udt_Id;
    t_ObjectDef                         varchar2(20);

  begin
    if api.pkg_columnquery.value(a_JobId, 'LicenseObjectId') is not null then
      t_ObjectDef := api.pkg_columnquery.value(api.pkg_columnquery.value(a_JobId, 'LicenseObjectId'), 'ObjectDefName');
    elsif api.pkg_columnquery.value(a_JobId, 'PermitObjectId') is not null then
      t_ObjectDef := api.pkg_columnquery.value(api.pkg_columnquery.value(a_JobId, 'PermitObjectId'), 'ObjectDefName');
    else
      null;
    end if;

    if t_ObjectDef = 'o_ABC_License' then
      -- Get the license objectid
      begin
        select LicenseObjectId
          into t_LicenseId
          from query.r_ABC_ExpirationLicense r
          where r.ExpirationJobId = a_JobId;
      exception
        when too_many_rows then
          raise_application_error(-20000, 'Expiration job related to more than one License.');
        when no_data_found then
          null;
      end;
    else
      null;
    end if;

    if t_ObjectDef = 'o_ABC_Permit' then
      --Get the permit objectid
      begin
        select PermitObjectId
          into t_PermitId
          from query.r_ABC_ExpirationPermit r
          where r.ExpirationJobId = a_JobId;
      exception
        when too_many_rows then
          raise_application_error(-20000, 'Expiration job related to more than one Permit.');
      end;
    end if;

    if t_LicenseId is not null then
      -- Find if p_ABC_TakeExpirationActions already exist
      begin
        select count(p.Objectid)
          into t_CountProcessId
          from query.p_ABC_TakeExpirationActions p
          where p.JobId = a_JobId;
      end;

      t_IsSpecialEvent := api.pkg_columnquery.value(t_LicenseId ,'IsSpecialEvent');
      -- Create p_ABC_TakeExpirationActions process and assigning it to the clerk
      if t_CountProcessId = 0 then

        /*Set the state to Expired to the License Object if it is currently Active*/
        if api.pkg_columnquery.value(t_LicenseId ,'State') = 'Active' then
          begin
            api.pkg_ColumnUpdate.SetValue(t_LicenseId, 'State', 'Expired');
          /*Delete all relationships on the license to Products*/

            abc.pkg_abc_license.RelRemoveUponExpiration(t_LicenseId, sysdate);
          end;
        end if;
        t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, null, sysdate, null, null);
        api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Complete');
        /*If it is a special event then complete the process.*/
        if t_IsSpecialEvent = 'Y' then
           api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'IsSystemProcess', 'Y');
        end if;
        --api.pkg_ProcessUpdate.Assign(t_ProcessId, t_UserId);
      end if;
    elsif t_PermitId is not null then
      -- Find if p_ABC_TakeExpirationActions already exist
      begin
        select count(p.Objectid)
          into t_CountProcessId
          from query.p_ABC_TakeExpirationActions p
         where p.JobId = a_JobId;
      end;

      -- Create p_ABC_TakeExpirationActions process and assigning it to the clerk
      if t_CountProcessId = 0 then

        /*Set the state to Expired to the License Object if it is currently Active*/
        if api.pkg_columnquery.value(t_PermitId ,'State') = 'Active' then
          begin
            if api.pkg_columnquery.value(t_PermitId, 'AutoExpire') = 'Y' then
              api.pkg_ColumnUpdate.SetValue(t_PermitId, 'State', 'Expired');
            end if;
          end;
        end if;
        t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, null, sysdate, null, null);
        api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Complete');

      end if;
    end if;

  end ExpirationJobPostVerify;


  -----------------------------------------------------------------------------
  -- InspectionJobPostVerify
  --  Create p_ABC_TakeExpirationActions process and complete it based on condition
  -----------------------------------------------------------------------------
  procedure InspectionJobPostVerify (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is
    t_ProcessId                         udt_Id;
    t_RegionDefaultInspectorUserId      udt_Id;
    t_NotFromNightlyObjectCreation      varchar(1);
    t_InitiateInspection                varchar(1);
    t_Outcome                           varchar(40);
    t_ProcessTypeId                     udt_Id :=
       api.pkg_ConfigQuery.ObjectDefIdForName('p_ABC_InitiateInspection');

  begin
    -- Find if p_ABC_InitiateInspection already exist
    begin
      select p.Objectid,
             p.Outcome
        into t_ProcessId,
             t_Outcome
        from query.p_ABC_InitiateInspection p
        where p.JobId = a_JobId;
     exception
      when too_many_rows then
        raise_application_error(-20000, 'Inspection job related to more than one Initiate Inspection process.');
      when no_data_found then
        /*For the previous jobs that are in Status new and don't have an initiate inspection created yet.*/
        t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, null, sysdate, null, null);
    end;

    t_NotFromNightlyObjectCreation := api.pkg_columnquery.value(a_JobId ,'NotFromNightlyObjectCreation');
    t_InitiateInspection := api.pkg_columnquery.value(a_JobId ,'InitiateInspection');
    t_RegionDefaultInspectorUserId := api.pkg_columnquery.value(a_JobId ,'RegionDefaultInspectorUserId');

    /*See if the job created via nightly object creation or default presentation. */
    if t_NotFromNightlyObjectCreation = 'Y' and t_Outcome is null then
       if t_InitiateInspection = 'Y' then
          api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Complete');
       end if;
    else
       if t_Outcome is null then
           if t_RegionDefaultInspectorUserId is not null then
              extension.pkg_RelationshipUpdate.New(a_JobId, t_RegionDefaultInspectorUserId, 'DefaultInspector');
           end if;
           api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Complete');
       end if;
    end if ;
  end InspectionJobPostVerify;

  -----------------------------------------------------------------------------
  -- SetupPaymentAdjustment
  --  Set up the Payment Adjustment job with the appropriate pay distribution
  --  objects.
  --  Developer: Andy Patterson (CXUSA)
  --  Created On: 7/31/2014
  -----------------------------------------------------------------------------
  procedure SetupPaymentAdjustment (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is

    t_PaymentObjectId                   udt_Id := api.pkg_ColumnQuery.NumericValue(a_JobId, 'PaymentObjectId');
    t_PaymentId                         udt_Id := api.pkg_ColumnQuery.NumericValue(t_PaymentObjectId, 'PaymentId');
    t_PayAdjPaymentEPID                 udt_Id := api.pkg_ConfigQuery.EndPointIdForName('j_ABC_PaymentAdjustment', 'Payment');
    t_PayAdjDistributionEPID            udt_Id := api.pkg_ConfigQuery.EndPointIdForName('j_ABC_PaymentAdjustment', 'PaymentDistribution');
    t_FeeTransactionDefId               udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('o_FeeTransactions');
    t_PayDistDefId                      udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_PaymentDistribution');
    t_RelId                             udt_Id;
    t_DistObjectId                      udt_Id;
    t_LicenseNumber                     varchar2(500);
    t_JobNumber                         varchar2(500);
    t_FeeDescription                    varchar2(500);

  begin

    -- Get the payment object and relate it to the job
    t_RelId := api.pkg_RelationshipUpdate.New(t_PayAdjPaymentEPID, a_JobId, t_PaymentObjectId);

    -- Set the Total Amount field
    api.pkg_ColumnUpdate.SetValue(a_JobId, 'TotalAmount', api.pkg_ColumnQuery.NumericValue(t_PaymentObjectId, 'PaymentAmount'));

    -- Get the fee transactions for the payment
    for a in (select ft.TransactionDate,
                     ft.DescriptionForDisplay Description,
                     (ft.Amount * -1) Amount,
                     ft.ObjectId,
                     ft.FeeId,
                     ft.TransactionId
                from api.PaymentTransactions pt
                join api.RegisteredExternalObjects reo on reo.LinkValue = to_char(pt.TransactionId)
                 and reo.ObjectDefId = t_FeeTransactionDefId
                join query.o_Feetransactions ft on ft.ObjectId = reo.ObjectId
               where pt.PaymentId = t_PaymentId) loop

      -- Get the License Number and External File Num. Assumes it is the job on which the fees for the transaction were made
      select api.pkg_ColumnQuery.Value(fe.JobId, 'LicenseNumber'),
             api.pkg_ColumnQuery.Value(fe.JobId, 'ExternalFileNum'),
             fe.Description
        into t_LicenseNumber,
             t_JobNumber,
             t_FeeDescription
        from api.fees fe
       where fe.FeeId = a.FeeId;

      -- Copy the fee transactions to payment distribution objects
      t_DistObjectId := api.pkg_ObjectUpdate.New(t_PayDistDefId);

      -- Set the details on the new Dist object
      api.pkg_ColumnUpdate.SetValue(t_DistObjectId, 'TransactionDate', a.TransactionDate);
      api.pkg_ColumnUpdate.SetValue(t_DistObjectId, 'TransactionDescription', a.Description);
      api.pkg_ColumnUpdate.SetValue(t_DistObjectId, 'FeeDescription', t_FeeDescription);
      api.pkg_ColumnUpdate.SetValue(t_DistObjectId, 'LicenseNumber', t_LicenseNumber);
      api.pkg_ColumnUpdate.SetValue(t_DistObjectId, 'JobNumber', t_JobNumber);
      api.pkg_ColumnUpdate.SetValue(t_DistObjectId, 'Balance', a.Amount);
      api.pkg_ColumnUpdate.SetValue(t_DistObjectId, 'OriginalTransactionId', a.TransactionId);
      api.pkg_ColumnUpdate.SetValue(t_DistObjectId, 'FeeId', a.FeeId);

      -- Relate the pay dist object to the payment adjustment job
      t_RelId := api.pkg_RelationshipUpdate.New(t_PayAdjDistributionEPID, a_JobId, t_DistObjectId);

    end loop; --a loop

  end SetupPaymentAdjustment;


  -----------------------------------------------------------------------------
  -- AdjustPayments
  --  Complete the Payment Adjustment job with the appropriate pay distributions
  --  and adjustments to the fee/payment.
  --  Developer: Andy Patterson (CXUSA)
  --  Created On: 7/31/2014
  -----------------------------------------------------------------------------
  procedure AdjustPayments (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is

    t_PayAdjDistributionEPID            udt_Id := api.pkg_ConfigQuery.EndPointIdForName('j_ABC_PaymentAdjustment', 'PaymentDistribution');
    t_PayAdjOrigEPID                    udt_Id := api.pkg_ConfigQuery.EndPointIdForName('j_ABC_PaymentAdjustment', 'Payment');
    t_PayAdjReverseEPID                 udt_Id := api.pkg_ConfigQuery.EndPointIdForName('j_ABC_PaymentAdjustment', 'PaymentReversal');
    t_PayAdjNewEPID                     udt_Id := api.pkg_ConfigQuery.EndPointIdForName('j_ABC_PaymentAdjustment', 'PaymentNew');
    t_OrigPayRevPayEPId                 udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_Payment', 'PaymentReversal');
    t_OrigPayNewPayEPId                 udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_Payment', 'PaymentNewAdjustment');
    t_PaymentMethodEPId                 udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_Payment', 'PaymentMethod');
    t_PaymentOnlinePaidUserEPId         udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_Payment', 'OnlineUser');
    t_PendAdjProcDefId                  udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('p_ABC_PendingAdjustment');
    t_OriginalPaymentId                 udt_Id;
    t_TransactionId                     udt_Id;
    t_PaymentId                         udt_Id;
    t_PayObjId                          udt_Id;
    t_ProcessId                         udt_Id;
    t_RelId                             udt_Id;
    t_PayMethodId                       udt_Id;
    t_PaidByOnlineUserId                udt_Id;
    t_OrigPayRef                        varchar2(4000);
    t_OrigRecNum                        varchar2(4000);
    t_FeeList                           api.pkg_Definition.udt_IdList;
    t_FeeAmounts                        api.pkg_Definition.udt_NumberList;
    t_FromObject                        varchar2(4000);
    t_IsOnlinePayment                   varchar2(1);
    t_CameFromObjectId                  udt_Id;
    t_Payor                             varchar2(100);
    t_DepositDate                       date;
    t_CreatedBy                         varchar2(4000);
    t_TotalAmount                       number;
    t_DistributionAmount                number;

  begin

    -- First ensure that the Over / Under calculation is equal to the sum of all amounts on the Pay Distributions
    t_TotalAmount := api.pkg_ColumnQuery.NumericValue(a_JobId, 'TotalAmount');
    select sum(nvl(pd.Pay, 0))
      into t_DistributionAmount
      from api.Relationships r
      join query.o_ABC_PaymentDistribution pd on pd.ObjectId = r.ToObjectId
       and pd.ObjectDefTypeId = 1
     where r.FromObjectId = a_JobId
       and r.EndPointId = t_PayAdjDistributionEPID;

    if t_TotalAmount != t_DistributionAmount then
      api.pkg_Errors.RaiseError(-20000, 'Please ensure that the Over / Under amount calculates to zero.');
    end if;

    -- Get the amounts for each distribution
    for a in (select pd.OriginalTransactionId TransactionId,
                     pd.Pay AmountToAdjust,
                     pd.Balance OriginalPaidAmt,
                     pd.LicenseNumber,
                     pd.JobNumber,
                     pd.FeeId,
                     pd.TransactionDescription Description
                from api.Relationships r
                join query.o_ABC_PaymentDistribution pd on pd.ObjectId = r.ToObjectId
                 and pd.ObjectDefTypeId = 1
               where r.FromObjectId = a_JobId
                 and r.EndPointId = t_PayAdjDistributionEPID) loop

      -- Get the original payment id
      begin

        select r.ToObjectId,
               api.pkg_ColumnQuery.Value(r.ToObjectId, 'PayeeReference'),
               api.pkg_ColumnQuery.Value(r.ToObjectId, 'ReceiptNumber')
          into t_OriginalPaymentId,
               t_OrigPayRef,
               t_OrigRecNum
          from api.Relationships r
         where r.FromObjectId = a_JobId
           and r.EndPointId = t_PayAdjOrigEPID;

      exception when no_data_found then
        api.pkg_Errors.RaiseError(-20000, 'No payment is related for adjustments. Please start a new Adjustment.');
      when too_many_rows then
        api.pkg_Errors.RaiseError(-20000, 'There are too many payments related for adjustments. Please start a new Adjustment.');
      end;

      -- Get the original Payment Method
      select max(r.ToObjectId)
        into t_PayMethodId
        from api.Relationships r
       where r.FromObjectId = t_OriginalPaymentId
         and r.EndPointId = t_PaymentMethodEPId;

      -- Get the original Paid By Online User
      select max(r.ToObjectId)
        into t_PaidByOnlineUserId
        from api.Relationships r
       where r.FromObjectId = t_OriginalPaymentId
         and r.EndPointId = t_PaymentOnlinePaidUserEPId;

      -- Get some original stored columns to put on the reversal and new payments
      select FromObject,
             IsOnlinePayment,
             CameFromObjectId,
             Payor,
             DepositDate,
             CreatedBy
        into t_FromObject,
             t_IsOnlinePayment,
             t_CameFromObjectId,
             t_Payor,
             t_DepositDate,
             t_CreatedBy
        from query.o_Payment p
       where p.ObjectId = t_OriginalPaymentId;


      -- Flag Original Payment to prevent VOIDS, REFUNDS and other ADJUSTMENTS
      api.pkg_ColumnUpdate.SetValue(t_OriginalPaymentId, 'IsAdjustedOrIsReversal', 'Y');

      -- Adjust the payment for each distribution as needed
      t_TransactionId := a.TransactionId;

      t_FeeList(1) := a.FeeId;
      t_FeeAmounts(1) := a.OriginalPaidAmt * -1;

      -- First, adjust away the original paid amount (i.e. a "Reversal")
      t_PaymentId := api.pkg_PaymentUpdate.New(sysdate, t_FeeList, t_FeeAmounts);
      api.pkg_PaymentUpdate.Modify(t_PaymentId, t_OrigPayRef || ' / Reversal Adjustment', t_OrigRecNum);
      t_PayObjId := api.pkg_ObjectUpdate.RegisterExternalObject('o_Payment', t_PaymentId);

      -- Flag Reversal Payment to prevent VOIDS, REFUNDS and other ADJUSTMENTS
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'IsAdjustedOrIsReversal', 'Y');

      -- Relate the reversal adjustment (payment) to the job and to the original payment
      t_RelId := api.pkg_RelationshipUpdate.New(t_PayAdjReverseEPID, a_JobId, t_PayObjId);

      -- Relate the original payment to the new payment for an audit trail
      t_RelId := api.pkg_RelationshipUpdate.New(t_OrigPayRevPayEPId, t_OriginalPaymentId, t_PayObjId);

      -- Set the License number and Job Number from original Fee Transaction to this reversal Payment.
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'LicenseNumber', a.LicenseNumber);
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'JobNumber', a.JobNumber);

      -- Relate the Original Payment Method object to the reversal payment
      if t_PayMethodId is not null then
        t_RelId := api.pkg_RelationshipUpdate.New(t_PaymentMethodEPId, t_PayObjId, t_PayMethodId);
      end if;

      -- Relate the Original Paid by User to the reversal payment
      if t_PaidByOnlineUserId is not null then
        t_RelId := api.pkg_RelationshipUpdate.New(t_PaymentOnlinePaidUserEPId, t_PayObjId, t_PaidByOnlineUserId);
      end if;

      -- Set the stored details on the reversal payment equal to the values from the original payment
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'FromObject', t_FromObject);
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'IsOnlinePayment', t_IsOnlinePayment);
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'CameFromObjectId', t_CameFromObjectId);
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'Payor', t_Payor);
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'DepositDate', t_DepositDate);
      api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'CreatedBy', t_CreatedBy);

      -----------------------------------------------------------
      -- Second, adjust in the new paid amount (i.e. new payment)
      t_FeeAmounts(1) := a.AmountToAdjust;

      if t_FeeAmounts(1) <> 0 then
        t_PaymentId := api.pkg_PaymentUpdate.New(sysdate, t_FeeList, t_FeeAmounts);
        api.pkg_PaymentUpdate.Modify(t_PaymentId, t_OrigPayRef || ' / New Adjustment', t_OrigRecNum);
        t_PayObjId := api.pkg_ObjectUpdate.RegisterExternalObject('o_Payment', t_PaymentId);

        -- Relate the new adjustment (Payment) to the job
        t_RelId := api.pkg_RelationshipUpdate.New(t_PayAdjNewEPID, a_JobId, t_PayObjId);

        -- Relate the original payment to the new payment for an audit trail
        t_RelId := api.pkg_RelationshipUpdate.New(t_OrigPayNewPayEPId, t_OriginalPaymentId, t_PayObjId);

        -- Relate the Original Payment Method object to the new payment
        if t_PayMethodId is not null then
          t_RelId := api.pkg_RelationshipUpdate.New(t_PaymentMethodEPId, t_PayObjId, t_PayMethodId);
        end if;

        -- Relate the Original Paid by User to the new payment
        if t_PaidByOnlineUserId is not null then
          t_RelId := api.pkg_RelationshipUpdate.New(t_PaymentOnlinePaidUserEPId, t_PayObjId, t_PaidByOnlineUserId);
        end if;

        -- Set the stored details on the new payment equal to the values from the original payment
        api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'FromObject', t_FromObject);
        api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'IsOnlinePayment', t_IsOnlinePayment);
        api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'CameFromObjectId', t_CameFromObjectId);
        api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'Payor', t_Payor);
        api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'DepositDate', t_DepositDate);
        api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'CreatedBy', t_CreatedBy);

        -- Set the License number and Job Number from original Fee Transaction to this new Payment.
        api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'LicenseNumber', a.LicenseNumber);
        api.pkg_ColumnUpdate.SetValue(t_PayObjId, 'JobNumber', a.JobNumber);
      end if;

      -- Reset the Fee Lists
      t_FeeList(1) := null;
      t_FeeAmounts(1) := null;

    end loop; --a loop

    -- Complete the workflow on the job
    select max(ProcessId)
      into t_ProcessId
      from query.p_ABC_PendingAdjustment
     where jobid = a_JobId;

    if t_ProcessId is null then
      t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_PendAdjProcDefId, null, null, null, null);
    end if;

    api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Adjusted');


  end AdjustPayments;

  ----------------------------------------------------------------------------
  --  RemovePaymentsFromDailyDeposit
  --  Written By: Joshua Lenon
  --  Date: 7/15/2015
  --  Run on Daily Deposit job to remove selected payment records.
  ----------------------------------------------------------------------------
  procedure RemovePaymentsFromDailyDeposit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is

  begin

    -- Loop through all payments on the Daily Deposit that have been marked for removal and delete the relationship
    for d in (select nd.relationshipid
                from query.r_Abc_Dailydepositnodeposit nd
               where nd.DailyDepositId = a_ObjectId
                 and api.pkg_columnquery.value(nd.PaymentId, 'DeleteFromDD') = 'Y') loop

       api.pkg_relationshipUpdate.remove(d.relationshipid);

    end loop;

  end RemovePaymentsFromDailyDeposit;

  /*---------------------------------------------------------------------------
   * PerformAutoCancel() -- PRIVATE
   *   This procedure takes in a list of JobIds and performs actions to cancel
   * the job, including the creation of a Case Note on the Job.
   *-------------------------------------------------------------------------*/
  procedure PerformAutoCancel (
    a_JobIds                            udt_IdList
  ) is
    t_CaseNoteDef                       udt_Id;
    t_CaseNoteId                        udt_Id;
    t_CaseNoteText                      varchar(4000) := 'Auto Cancelled.';
    t_ChangeStatusDefId                 udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'p_ChangeStatus');
    t_ProcessId                         udt_Id;
  begin

    api.pkg_LogicalTransactionupdate.ResetTransaction();

    select nd.NoteDefId
    into t_CaseNoteDef
    from api.notedefs nd
    where nd.Tag = 'GEN';

    for i in 1..a_JobIds.count() loop
      api.pkg_ColumnUpdate.SetValue(a_JobIds(i), 'Autocancelled', 'Y');
      -- Add a Case Note to the Job, indicating that the job will be auto-cancelled
      t_CaseNoteId := api.pkg_NoteUpdate.New(a_JobIds(i), t_CaseNoteDef, 'N', t_CaseNoteText);
      -- add and complete the Change Status process to Cancel the Job
      t_ProcessId := api.pkg_ProcessUpdate.New(a_JobIds(i), t_ChangeStatusDefId,
          'Automatically Cancelled', null, sysdate, null);
      api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ReasonForChange', t_CaseNoteText);
      api.pkg_ProcessUpdate.Complete(t_ProcessId, 'Cancelled');
    end loop;

    api.pkg_LogicalTransactionupdate.EndTransaction();
    commit;

  end PerformAutoCancel;

  /*---------------------------------------------------------------------------
   * AutoCancelJobs() -- PUBLIC
   *   Run as a processes server nightly procedure to cancel all jobs with
   * Automatic Cancellation enabled that have exceeded the cancellation period.
   *-------------------------------------------------------------------------*/
  procedure AutoCancelJobs is
    t_AmendmentAppJobDefId              udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_AmendmentApplication');
    t_AutoCancelPeriod                  udt_NumberList;
    t_AutoCancelPeriodLTPT              udt_NumberList;
    t_EnableAutoCancel                  udt_StringList;
    t_IsLicenseJob                      udt_StringList;
    t_IsPermitJob                       udt_StringList;
    t_JobIds                            udt_IdList;
    t_JobTypeIds                        udt_IdList;
    t_LicenseAmendEndpointId            udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_AmendmentApplication', 'LicenseToAmend');
    t_LicenseRenewEndpointId            udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_RenewalApplication', 'LicenseToRenew');
    t_LicenseTypeIds                    udt_IdList;
    t_LicTypLicenseEndpointId           udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'o_ABC_License', 'LicenseType');
    t_LicTypNewAppEndpointId            udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_NewApplication', 'OLLicenseType');
    t_NewAppJobDefId                    udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_NewApplication');
    t_NoLicTypeCancelPeriod             number;
    t_NoPermTypeCancelPeriod            number;
    t_PermitApplicationJobDefId         udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_PermitApplication');
    t_PermitRenewalJobDefId             udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_PermitRenewal');
    t_PermitRenewEndpointId             udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PermitRenewal', 'PermitToRenew');
    t_PermitTypeIds                     udt_IdList;
    t_PermTypPermAppEndpointId          udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PermitApplication', 'PermitType');
    t_PermTypPermitEndpointId           udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'o_ABC_Permit', 'PermitType');
    t_RenewalAppJobDefId                udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_RenewalApplication');
  begin

    select
      api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'AutomaticCancellationPeriod'),
      api.pkg_ColumnQuery.Value(x.ObjectId, 'EnableAutomaticCancellation'),
      api.pkg_ColumnQuery.Value(x.ObjectId, 'IsLicenseJob'),
      api.pkg_ColumnQuery.Value(x.ObjectId, 'IsPermitJob'),
      api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'JobTypeId')
    bulk collect into
      t_AutoCancelPeriod,
      t_EnableAutoCancel,
      t_IsLicenseJob,
      t_IsPermitJob,
      t_JobTypeIds
    from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_JobTypes', 'IsABCJob', 'Y')) x;

    for i in 1..t_JobTypeIds.count() loop
      t_NoLicTypeCancelPeriod := t_AutoCancelPeriod(i);
      t_NoPermTypeCancelPeriod := t_AutoCancelPeriod(i);
      -- When Autocancel is enabled at the job-type-level, IsLicenseJob and IsPermitJob must be 'N'
      -- Using the settings on the job type, autocancel qualifying jobs
      if (t_EnableAutoCancel(i) = 'Y') and (t_IsLicenseJob(i) = 'N') and (t_IsPermitJob(i) = 'N')
          then
        select j.JobId
        bulk collect into t_JobIds
        from
          api.jobs j
          join api.Statuses s
              on j.StatusId = s.StatusId
        where j.JobTypeId = t_JobTypeIds(i)
          and s.Tag = 'NEW'
          and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
          and trunc(j.CreatedDate) + t_AutoCancelPeriod(i) <= trunc(sysdate);
        PerformAutoCancel(t_JobIds);

      -- Autocancel Licensing Jobs
      elsif (t_IsLicenseJob(i) = 'Y') then
        -- Cancel jobs with no license type set and Auto Cancel enabled at the job level.
        begin

          select j.JobId
          bulk collect into t_JobIds
          from
            api.jobs j
            join api.statuses s
                on j.StatusId = s.StatusId
          where j.JobTypeId = t_JobTypeIds(i)
            and s.Tag = 'NEW'
            and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
            and trunc(j.CreatedDate) + t_NoLicTypeCancelPeriod <= trunc(sysdate)
            and api.pkg_ColumnQuery.Value(j.JobId, 'OnlineLicenseTypeObjectId') is null
            and api.pkg_ColumnQuery.Value(j.JobId, 'LicenseTypeObjectId') is null
            and t_EnableAutoCancel(i) = 'Y';
          PerformAutoCancel(t_JobIds);

        exception
          when no_data_found then
            null;
        end;

        -- Get all License Types and their associated auto-cancellation periods
        select
          api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'AutomaticCancellationPeriod'),
          x.ObjectId
        bulk collect into
          t_AutoCancelPeriodLTPT,
          t_LicenseTypeIds
        from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_LicenseType',
            'EnableAutomaticCancellation', 'Y')) x;

        -- Cancel jobs for each license type
        for j in 1..t_LicenseTypeIds.count() loop
          if t_JobTypeIds(i) = t_NewAppJobDefId then
            select j.JobId
            bulk collect into t_JobIds
            from
              api.jobs j
              join api.statuses s
                  on j.StatusId = s.StatusId
              join api.relationships r
                  on j.JobId = r.FromObjectId
              join query.o_ABC_LicenseType lt
                  on r.ToObjectId = lt.ObjectId
            where j.JobTypeId = t_NewAppJobDefId
              and s.Tag = 'NEW'
              and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
              and trunc(j.CreatedDate) + t_AutoCancelPeriodLTPT(j) <= trunc(sysdate)
              and r.EndpointId = t_LicTypNewAppEndpointId
              and lt.ObjectId = t_LicenseTypeIds(j);
            PerformAutoCancel(t_JobIds);

          elsif t_JobTypeIds(i) in (t_AmendmentAppJobDefId, t_RenewalAppJobDefId) then
            select j.JobId
            bulk collect into t_JobIds
            from
              api.jobs j
              join api.Statuses s
                  on j.StatusId = s.StatusId
              join api.relationships r
                  on j.JobId = r.FromObjectId
              join api.relationships rr
                  on r.ToObjectId = rr.FromObjectId
              join query.o_ABC_LicenseType lt
                  on rr.ToObjectId = lt.ObjectId
            where j.JobTypeId = t_JobTypeIds(i)
              and s.Tag = 'NEW'
              and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
              and trunc(j.CreatedDate) + t_AutoCancelPeriodLTPT(j) <= trunc(sysdate)
              and r.EndpointId in (t_LicenseAmendEndpointId, t_LicenseRenewEndpointId)
              and rr.EndpointId = t_LicTypLicenseEndpointId
              and lt.ObjectId = t_LicenseTypeIds(j);
            PerformAutoCancel(t_JobIds);
          end if;
        end loop;

      -- Autocancel Permitting Jobs
      elsif (t_IsPermitJob(i) = 'Y') then
        -- Cancel jobs with no permit type set and Auto Cancel enabled at the job level.
        begin

          select j.JobId
          bulk collect into t_JobIds
          from
            api.jobs j
            join api.statuses s
                on j.StatusId = s.StatusId
          where j.JobTypeId = t_JobTypeIds(i)
            and s.Tag = 'NEW'
            and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
            and trunc(j.CreatedDate) + t_NoPermTypeCancelPeriod <= trunc(sysdate)
            and api.pkg_ColumnQuery.Value(j.JobId, 'OnlinePermitTypeObjectId') is null
            and api.pkg_ColumnQuery.Value(j.JobId, 'PermitTypeObjectId') is null
            and t_EnableAutoCancel(i) = 'Y';
          PerformAutoCancel(t_JobIds);

        exception
          when no_data_found then
            null;
        end;

        -- Get all Permit Types and their associated auto-cancellation periods
        select
          api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'AutomaticCancellationPeriod'),
          x.ObjectId
        bulk collect into
          t_AutoCancelPeriodLTPT,
          t_PermitTypeIds
        from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_PermitType',
            'EnableAutomaticCancellation', 'Y')) x;

        -- Cancel jobs for each permit type
        for j in 1..t_PermitTypeIds.count() loop
          --Permit Application
          if t_JobTypeIds(i) = t_PermitApplicationJobDefId then
            select j.JobId
            bulk collect into t_JobIds
            from
              api.jobs j
              join api.statuses s
                  on j.StatusId = s.StatusId
              join api.relationships r --rel to Permit Type
                  on j.JobId = r.FromObjectId
            where j.JobTypeId = t_PermitApplicationJobDefId
              and s.Tag = 'NEW'
              and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
              and trunc(j.CreatedDate) + t_AutoCancelPeriodLTPT(j) <= trunc(sysdate)
              and r.EndpointId = t_PermTypPermAppEndpointId
              and r.ToObjectId = t_PermitTypeIds(j);
            PerformAutoCancel(t_JobIds);
          --Permit Renewal
          elsif t_JobTypeIds(i) = t_PermitRenewalJobDefId then
            select j.JobId
            bulk collect into t_JobIds
            from
              api.jobs j
              join api.Statuses s
                  on j.StatusId = s.StatusId
              join api.relationships r --rel to Permit
                  on j.JobId = r.FromObjectId
              join api.relationships rr --rel to Permit Type
                  on r.ToObjectId = rr.FromObjectId
            where j.JobTypeId = t_JobTypeIds(i)
              and s.Tag = 'NEW'
              and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
              and trunc(j.CreatedDate) + t_AutoCancelPeriodLTPT(j) <= trunc(sysdate)
              and r.EndpointId in (t_PermitRenewEndpointId)
              and rr.EndpointId = t_PermTypPermitEndpointId
              and rr.ToObjectId = t_PermitTypeIds(j);
            PerformAutoCancel(t_JobIds);
          end if;
        end loop;
      end if;
    end loop;

  end AutoCancelJobs;

  /*---------------------------------------------------------------------------
   * AutoCancelJobsNotification() -- PUBLIC
   *   Run as a processes server nightly procedure to notify applicants of
   * the jobs that will be automatically cancelled based on the Send
   * Cancellation Notification flag and Days To Notify.
   *-------------------------------------------------------------------------*/
  procedure AutoCancelJobsNotification is
    t_AmendmentAppJobDefId              udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_AmendmentApplication');
    t_AutoCancelPeriod                  udt_NumberList;
    t_AutoCancelPeriodLTPT              udt_NumberList;
    t_DaysToNotify                      udt_NumberList;
    t_DaysToNotifyLTPT                  udt_NumberList;
    t_EnableAutoCancel                  udt_StringList;
    t_IsLicenseJob                      udt_StringList;
    t_IsPermitJob                       udt_StringList;
    t_JobIds                            udt_IdList;
    t_JobTypeIds                        udt_IdList;
    t_LicenseAmendEndpointId            udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_AmendmentApplication', 'LicenseToAmend');
    t_LicenseRenewEndpointId            udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_RenewalApplication', 'LicenseToRenew');
    t_LicenseTypeIds                    udt_IdList;
    t_LicTypLicenseEndpointId           udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'o_ABC_License', 'LicenseType');
    t_LicTypNewAppEndpointId            udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_NewApplication', 'OLLicenseType');
    t_NewAppJobDefId                    udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_NewApplication');
    t_NoLicTypeCancelPeriod             number;
    t_NoLicenseTypeDaysToNotify         number;
    t_NoPermTypeCancelPeriod            number;
    t_NoPermitTypeDaysToNotify          number;
    t_PermitApplicationJobDefId         udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_PermitApplication');
    t_PermitRenewalJobDefId             udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_PermitRenewal');
    t_PermitRenewEndpointId             udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PermitRenewal', 'PermitToRenew');
    t_PermitTypeIds                     udt_IdList;
    t_PermTypPermAppEndpointId          udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PermitApplication', 'PermitType');
    t_PermTypPermitEndpointId           udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'o_ABC_Permit', 'PermitType');
    t_RenewalAppJobDefId                udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'j_ABC_RenewalApplication');
    t_SendCancelNotification            udt_StringList;
    t_SendCancelNotificationLTPT        udt_StringList;
  begin

    select
      api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'AutomaticCancellationPeriod'),
      api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'DaysToNotify'),
      api.pkg_ColumnQuery.Value(x.ObjectId, 'EnableAutomaticCancellation'),
      api.pkg_ColumnQuery.Value(x.ObjectId, 'IsLicenseJob'),
      api.pkg_ColumnQuery.Value(x.ObjectId, 'IsPermitJob'),
      api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'JobTypeId'),
      api.pkg_ColumnQuery.Value(x.ObjectId, 'SendCancellationNotification')
    bulk collect into
      t_AutoCancelPeriod,
      t_DaysToNotify,
      t_EnableAutoCancel,
      t_IsLicenseJob,
      t_IsPermitJob,
      t_JobTypeIds,
      t_SendCancelNotification
    from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_JobTypes', 'IsABCJob', 'Y')) x;

    for i in 1..t_JobTypeIds.count() loop
      t_NoLicTypeCancelPeriod := t_AutoCancelPeriod(i);
      t_NoLicenseTypeDaysToNotify := t_DaysToNotify(i);
      t_NoPermTypeCancelPeriod := t_AutoCancelPeriod(i);
      t_NoPermitTypeDaysToNotify := t_DaysToNotify(i);
      -- When Autocancel is enabled at the job-type-level, IsLicenseJob and IsPermitJob must be 'N'
      -- Using the settings on the job type, send notification for qualifying jobs
      if (t_EnableAutoCancel(i) = 'Y') and (t_IsLicenseJob(i) = 'N') and (t_IsPermitJob(i) = 'N')
          and (t_SendCancelNotification(i) = 'Y') then
        begin

          select j.JobId
          bulk collect into t_JobIds
          from
            api.jobs j
            join api.Statuses s
                on j.StatusId = s.StatusId
          where j.JobTypeId = t_JobTypeIds(i)
            and s.Tag = 'NEW'
            and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
            and trunc(j.CreatedDate) + (t_AutoCancelPeriod(i) - t_DaysToNotify(i))
                = trunc(sysdate);
          for k in 1..t_JobIds.count() loop
            abc.pkg_abc_email.sendautomaticcancellationemail(t_JobIds(k), t_AutoCancelPeriod(i),
                sysdate);
          end loop;

        exception
          when no_data_found then
            null;
        end;

      -- Notify of auto cancel for Licensing Jobs
     elsif (t_IsLicenseJob(i) = 'Y') then
        -- Send notification for jobs with no license type set
        begin

          select j.JobId
          bulk collect into t_JobIds
          from
            api.jobs j
            join api.statuses s
                on j.StatusId = s.StatusId
          where j.JobTypeId = t_JobTypeIds(i)
            and s.Tag = 'NEW'
            and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
            and trunc(j.CreatedDate) + (t_NoLicTypeCancelPeriod - t_NoLicenseTypeDaysToNotify)
                = trunc(sysdate)
            and api.pkg_ColumnQuery.Value(j.JobId, 'OnlineLicenseTypeObjectId') is null
            and api.pkg_ColumnQuery.Value(j.JobId, 'LicenseTypeObjectId') is null
            and t_SendCancelNotification(i) = 'Y';
          for k in 1..t_JobIds.count() loop
            abc.pkg_abc_email.sendautomaticcancellationemail(t_JobIds(k), t_NoLicTypeCancelPeriod,
                sysdate);
          end loop;

        exception
          when no_data_found then
            null;
        end;

        select
          api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'AutomaticCancellationPeriod'),
          api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'DaysToNotify'),
          x.ObjectId,
          api.pkg_ColumnQuery.Value(x.ObjectId, 'SendCancellationNotification')
        bulk collect into
          t_AutoCancelPeriodLTPT,
          t_DaysToNotifyLTPT,
          t_LicenseTypeIds,
          t_SendCancelNotificationLTPT
        from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_LicenseType',
            'EnableAutomaticCancellation', 'Y')) x;

        -- Notify users for each license type
        for j in 1..t_LicenseTypeIds.count() loop
          if t_JobTypeIds(i) = t_NewAppJobDefId and (t_SendCancelNotificationLTPT(j) = 'Y') then
            begin

              select j.JobId
              bulk collect into t_JobIds
              from
                api.jobs j
                join api.statuses s
                    on j.StatusId = s.StatusId
                join api.relationships r
                    on j.JobId = r.FromObjectId
                join query.o_ABC_LicenseType lt
                    on r.ToObjectId = lt.ObjectId
              where j.JobTypeId = t_NewAppJobDefId
                and s.Tag = 'NEW'
                and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
                and trunc(j.CreatedDate) + (t_AutoCancelPeriodLTPT(j) - t_DaysToNotifyLTPT(j))
                    = trunc(sysdate)
                and r.EndpointId = t_LicTypNewAppEndpointId
                and lt.ObjectId = t_LicenseTypeIds(j);
              for k in 1..t_JobIds.count() loop
                abc.pkg_abc_email.sendautomaticcancellationemail(t_JobIds(k),
                    t_AutoCancelPeriodLTPT(j), sysdate);
              end loop;

            exception
              when no_data_found then
                null;
            end;

          elsif t_JobTypeIds(i) in (t_AmendmentAppJobDefId, t_RenewalAppJobDefId)
              and (t_SendCancelNotificationLTPT(j) = 'Y') then
            begin

              select j.JobId
              bulk collect into t_JobIds
              from
                api.jobs j
                join api.Statuses s
                    on j.StatusId = s.StatusId
                join api.relationships r
                    on j.JobId = r.FromObjectId
                join api.relationships rr
                    on r.ToObjectId = rr.FromObjectId
                join query.o_ABC_LicenseType lt
                    on rr.ToObjectId = lt.ObjectId
              where j.JobTypeId = t_JobTypeIds(i)
                and s.Tag = 'NEW'
                and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
                and trunc(j.CreatedDate) + (t_AutoCancelPeriodLTPT(j) - t_DaysToNotifyLTPT(j)) =
                    trunc(sysdate)
                and r.EndpointId in (t_LicenseAmendEndpointId, t_LicenseRenewEndpointId)
                and rr.EndpointId = t_LicTypLicenseEndpointId
                and lt.ObjectId = t_LicenseTypeIds(j);
              for k in 1..t_JobIds.count() loop
                abc.pkg_abc_email.sendautomaticcancellationemail(t_JobIds(k),
                    t_AutoCancelPeriodLTPT(j), sysdate);
              end loop;

            exception
              when no_data_found then
                null;
            end;
          end if;
        end loop;

      -- Notify of auto cancel for Permitting Jobs
      elsif (t_IsPermitJob(i) = 'Y') then
        -- Send notification for jobs with no permit type set
        begin

          select j.JobId
          bulk collect into t_JobIds
          from
            api.jobs j
            join api.statuses s
                on j.StatusId = s.StatusId
          where j.JobTypeId = t_JobTypeIds(i)
            and s.Tag = 'NEW'
            and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
            and trunc(j.CreatedDate) + (t_NoPermTypeCancelPeriod - t_NoPermitTypeDaysToNotify)
                = trunc(sysdate)
            and api.pkg_ColumnQuery.Value(j.JobId, 'OnlinePermitTypeObjectId') is null
            and api.pkg_ColumnQuery.Value(j.JobId, 'PermitTypeObjectId') is null
            and t_SendCancelNotification(i) = 'Y';
          for k in 1..t_JobIds.count() loop
            abc.pkg_abc_email.sendautomaticcancellationemail(t_JobIds(k), t_NoPermTypeCancelPeriod,
                sysdate);
          end loop;

        exception
          when no_data_found then
            null;
        end;

        select
          api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'AutomaticCancellationPeriod'),
          api.pkg_ColumnQuery.NumericValue(x.ObjectId, 'DaysToNotify'),
          x.ObjectId,
          api.pkg_ColumnQuery.Value(x.ObjectId, 'SendCancellationNotification')
        bulk collect into
          t_AutoCancelPeriodLTPT,
          t_DaysToNotifyLTPT,
          t_PermitTypeIds,
          t_SendCancelNotificationLTPT
        from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_PermitType',
            'EnableAutomaticCancellation', 'Y')) x;

        -- Notify users for each permit type
        for j in 1..t_PermitTypeIds.count() loop
          --Permit Application
          if t_JobTypeIds(i) = t_PermitApplicationJobDefId
              and (t_SendCancelNotificationLTPT(j) = 'Y') then
            begin

              select j.JobId
              bulk collect into t_JobIds
              from
                api.jobs j
                join api.statuses s
                    on j.StatusId = s.StatusId
                join api.relationships r --rel to Permit Type
                    on j.JobId = r.FromObjectId
              where j.JobTypeId = t_PermitApplicationJobDefId
                and s.Tag = 'NEW'
                and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
                and trunc(j.CreatedDate) + (t_AutoCancelPeriodLTPT(j) - t_DaysToNotifyLTPT(j))
                    = trunc(sysdate)
                and r.EndpointId = t_PermTypPermAppEndpointId
                and r.ToObjectId = t_PermitTypeIds(j);
              for k in 1..t_JobIds.count() loop
                abc.pkg_abc_email.sendautomaticcancellationemail(t_JobIds(k),
                    t_AutoCancelPeriodLTPT(j), sysdate);
              end loop;

            exception
              when no_data_found then
                null;
            end;

          --Permit Renewal
          elsif t_JobTypeIds(i) = t_PermitRenewalJobDefId
              and (t_SendCancelNotificationLTPT(j) = 'Y') then
            begin

              select j.JobId
              bulk collect into t_JobIds
              from
                api.jobs j
                join api.Statuses s
                    on j.StatusId = s.StatusId
                join api.relationships r --rel to Permit
                    on j.JobId = r.FromObjectId
                join api.relationships rr --rel to Permit Type
                    on r.ToObjectId = rr.FromObjectId
              where j.JobTypeId = t_JobTypeIds(i)
                and s.Tag = 'NEW'
                and api.pkg_ColumnQuery.Value(j.JobId, 'EnteredOnline') = 'Y'
                and trunc(j.CreatedDate) + (t_AutoCancelPeriodLTPT(j) - t_DaysToNotifyLTPT(j))
                    = trunc(sysdate)
                and r.EndpointId in (t_PermitRenewEndpointId)
                and rr.EndpointId = t_PermTypPermitEndpointId
                and rr.ToObjectId = t_PermitTypeIds(j);
              for k in 1..t_JobIds.count() loop
                abc.pkg_abc_email.sendautomaticcancellationemail(t_JobIds(k),
                    t_AutoCancelPeriodLTPT(j), sysdate);
              end loop;

            exception
              when no_data_found then
                null;
            end;
          end if;
        end loop;
      end if;
    end loop;

  end AutoCancelJobsNotification;

end pkg_ABC_Jobs;
/
