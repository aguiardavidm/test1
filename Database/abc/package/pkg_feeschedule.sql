create or replace package abc.pkg_FeeSchedule as

  /*----------------------------------------------------------------------
   * Types
   *--------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  type udt_FeeDefinitionList is table of query.o_FeeDefinition%rowtype index by binary_integer;--FeeSchedulePlus.pkg_FeeSchedulePlus.udt_FeeDefinitionList;
  subtype udt_ObjectList is api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * CalculateFeeForLicense()
   *  Returns Fee amount for License.
   *-------------------------------------------------------------------------*/
  function CalculateFeeForLicense(
    a_ObjectId                   udt_Id
  ) return number;
  
  /*---------------------------------------------------------------------------
   * DetermineRenewalFees() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DetermineRenewalFees (
    a_PosseObjectId                   number,
    a_DataSourceObjectId              number,
    a_ExpirationDate                  date default null
  ) return udt_FeeDefinitionList;
  
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_FeeDefinitionId           udt_Id,
    a_Data                   out query.o_FeeDefinition%rowtype
  );

end pkg_FeeSchedule;
/
create or replace package body abc.pkg_FeeSchedule as

  /*---------------------------------------------------------------------------
   * CalculateFeeForLicense()
   *  Returns Fee amount for License.
   *-------------------------------------------------------------------------*/
  function CalculateFeeForLicense(
    a_ObjectId                          udt_Id
  ) return number is
    t_FeeDefinitionList                 udt_FeeDefinitionList;
    t_Amount                            number(14,2) := 0;
    t_FeeScheduleId                     udt_Id;
    t_DataSourceId                      udt_Id;
    t_FeeDefinitionId                   udt_Id;
    t_LicenseDataSource                 udt_Id;
    t_FeeDefs                           number := 0 ;
    t_ExpirationDate                    date;
    t_ObjectDefName                     varchar2(60);
    t_LicenseTypeCode                   varchar2(60);
    t_OnlineLicenseTypeCode             varchar2(60);
    t_ObjectId                          udt_id;
    t_FeeDefDefId                       udt_id := api.pkg_configquery.objectdefidforname('o_FeeDefinition');
  begin
  
  select od.Name 
    into t_ObjectDefName
   from api.objects o
   join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
  where o.ObjectId = a_ObjectId;
  
  t_LicenseTypeCode := api.pkg_columnquery.value(a_ObjectId, 'LicenseTypeCode');
  t_OnlineLicenseTypeCode := api.pkg_columnquery.value(a_ObjectId, 'OnlineLicenseTypeCode');
  
 -- dbms_output.put_line(t_LicenseTypeCode);                              
  
    
  if t_ObjectDefName = 'o_ABC_License' then
    t_ObjectId := a_ObjectId; --We are on the right object
  elsif t_ObjectDefName = 'j_ABC_NewApplication' then
    select rl.RelatedLicenseObjectId
      into t_ObjectId
      from query.r_ABC_NewApplicationRelLicense rl
     where rl.NewAppObjectId = a_ObjectId;
  end if;
  
  if (t_LicenseTypeCode = '24' and t_ObjectDefName = 'j_ABC_NewApplication') or t_ObjectDefName = 'o_ABC_License' then
  t_ExpirationDate := api.pkg_ColumnQuery.DateValue(t_ObjectId, 'ExpirationDate');
  select max(l.ObjectId)
    into t_LicenseDataSource
    from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_DataSource',
          'PosseViewName', 'o_ABC_License')) l;
   -- join api.registeredexternalobjects reo on reo.ObjectId = l.o      ;
 /*   where t_ExpirationDate between
        Today's Date
        api.pkg_columnquery.DateValue(
          api.pkg_columnquery.NumericValue(l.ObjectId, 'FeeScheduleId'),
          'ScheduleEffectiveStartDate')
        and api.pkg_columnquery.DateValue(
          api.pkg_columnquery.NumericValue(l.ObjectId, 'FeeScheduleId'),
          'ScheduleEffectiveEndDate');*/

    t_FeeDefinitionList := DetermineRenewalFees(t_ObjectId, t_LicenseDataSource);

  for i in 1..t_FeeDefinitionList.Count loop
    select api.pkg_columnquery.Value(r.FeeScheduleId, 'FeeScheduleId')--448--FeeScheduleId
      into t_FeeScheduleId
      from query.r_Feedef_Feeschedule r
     where r.FeeDefinitionObjectId = t_FeeDefinitionList(i).ObjectId;

    select api.pkg_columnquery.Value(r.DataSourceObjectId , 'DataSourceId')--449--r.DataSourceObjectId
      into t_DataSourceId
      from query.r_FeeDef_DataSource r
     where r.FeeDefinitionObjectId = t_FeeDefinitionList(i).ObjectId;
     
    select to_number(reo.LinkValue)
      into t_FeeDefinitionId
      from api.registeredexternalobjects reo 
     where reo.ObjectId = t_FeeDefinitionList(i).ObjectId
       and reo.ObjectDefId = t_FeeDefDefId ;
 /* dbms_output.put_line(FeeSchedulePlus.pkg_ExpressionColumn.gc_ExpElementSyntaxNumber
                                                || ' SchedId: ' || t_FeeScheduleId
                                                || ' Amount: ' || t_FeeDefinitionList(i).Amount
                                                || ' ObjId: ' || t_ObjectId
                                                || ' DataId: ' || t_DataSourceId);*/
/*      select r.FeeTypeGroupId
        into t_FeetypeGroupId
        from query.r_FeeTypeGroup_FeeDefinition r
       where r.FeeDefinitionId = t_FeeDefinitionList(i).ObjectId;*/
--if t_FeeDefinitionList(i).Objectid = 12352551 then
    t_Amount := t_Amount + nvl(FeeSchedulePlus.pkg_ExpressionColumn.NumberValue(FeeSchedulePlus.pkg_ExpressionColumn.gc_ExpElementSyntaxNumber,
                                                 t_FeeScheduleId,
                                                 t_FeeDefinitionList(i).Amount,
                                                 t_ObjectId,
                                                 t_DataSourceId),0);
                                                 t_FeeDefs := t_FeeDefs +1;
--end if;                                                 
   end loop;
    return nvl(t_Amount, 0);
  
  else
    return(0);  
  end if;  
  
  end CalculateFeeForLicense;
  
      /*---------------------------------------------------------------------------
   * EvaluateCondition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function EvaluateCondition (
    a_FeeScheduleId                     udt_Id,
    a_FeeDefinitionId                   udt_Id,
    a_PosseObjectId                     udt_Id,
    a_LicenseDataSource                 udt_Id
  ) return boolean is
    t_FeeDefinitionCondition            query.o_feedefinition.Condition%type;
    t_CategoryCondition                 boolean;
    t_FeeCategoryId                     udt_Id;
    t_DataSourceId                      udt_Id;
    t_FeeDefinitionObjId                udt_id;
    t_FeeDefObjDefId                    udt_Id := api.pkg_configquery.ObjectDefIdForName('o_FeeDefinition');
    
  begin
    
  
    select reo.ObjectId
      into t_FeeDefinitionObjId
      from api.registeredexternalobjects reo  
     where reo.LinkValue = to_char(a_FeeDefinitionId)
       and reo.ObjectDefId = t_FeeDefObjDefId;
    
    t_FeeDefinitionCondition := api.pkg_columnquery.Value(t_FeeDefinitionObjId, 'Condition');
    --dbms_output.put_line('Line93 '||t_FeeDefinitionCondition);
    begin
      select r.FeeCategoryId
        into t_FeeCategoryId
        from query.r_feedef_FeeCategory r
       where r.FeeDefinitionId = t_FeeDefinitionObjId;
    exception when NO_DATA_FOUND then
      null;
    end;
    
    select to_number(reo.LinkValue)
      into t_DataSourceId 
     from api.registeredexternalobjects reo 
    where reo.ObjectId = a_LicenseDataSource; 
    
    --t_DataSourceId := a_LicenseDataSource;

    if t_FeeCategoryId is not null then
      t_CategoryCondition := feescheduleplus.pkg_FeeCategory.EvaluateCondition(a_FeeScheduleId,
          t_FeeCategoryId, a_PosseObjectId, t_DataSourceId);
    else
      t_CategoryCondition := true;
    end if;

    if t_FeeDefinitionCondition is null then
      return t_CategoryCondition;
    else
     -- dbms_output.put_line(t_FeeDefinitionCondition);
      return t_CategoryCondition and nvl(feescheduleplus.pkg_ExpressionColumn.BooleanValue(feescheduleplus.pkg_ExpressionColumn.gc_ExpElementSyntaxBoolean,
          a_FeeScheduleId, t_FeeDefinitionCondition, a_PosseObjectId, t_DataSourceId), true);
    end if;
  end;

  
 /*---------------------------------------------------------------------------
   * DetermineRenewalFees() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function DetermineRenewalFees (
    a_PosseObjectId                   number,
    a_DataSourceObjectId              number,
    a_ExpirationDate                  date default null
  ) return udt_FeeDefinitionList is
    t_FeeDefinitionList               udt_FeeDefinitionList;
    t_TestNum                         number;
    t_TestNum2                        number;
    t_TestString                      varchar2(4000);
    t_TestString2                     varchar2(4000);
    t_ExpirationDate                  date;
    t_RevenueCode                     number;
  begin
    /*
      - Logic:
        - find data sources that use given object type
        - get feedefinition objects for the data sources
        - get date column used to determine fee schedule from linked feedefinitions
        - get date value from object
        - filter feedefinitions down to those that are valid for the date
        - for each fee definition, resolve the conditions
        - limit list of fee definitions to those for which the condition holds true
        - build and return list
    */
    t_ExpirationDate := nvl(a_ExpirationDate, api.pkg_ColumnQuery.DateValue(a_PosseObjectId, 'ExpirationDate'));
    t_RevenueCode := api.pkg_ColumnQuery.NumericValue(a_PosseObjectId, 'RevenueCode');
    for c in (select (select to_number(reo.LinkValue) from 
                          api.registeredexternalobjects reo 
                         where reo.ObjectId =  fd.objectid) FeeDefinitionId,
                      fs.objectid FeeScheduleObjIdId,
                      fs.FeeScheduleId
                 from api.ObjectDefs od 
                 join query.o_DataSource ds on ds.PosseViewName = od.Name
                 join query.r_feedef_datasource r on r.DataSourceObjectId = ds.ObjectId
                 join query.o_FeeDefinition fd on fd.objectid = r.FeeDefinitionObjectId
                 join query.r_feedef_feeschedule r2 on r2.FeeDefinitionObjectId = fd.objectid
                 join query.o_FeeSchedule fs on fs.objectid = r2.FeeScheduleId
                where od.Name = 'j_ABC_NewApplication'
                
                /*  and t_ExpirationDate
                      between fs.ScheduleEffectiveStartDate 
                          and fs.ScheduleEffectiveEndDate*/
                  --and fd.RevenueCode = t_RevenueCode        
                ) loop
      
    
     -- if c.feedefinitionid = 1538 then
        
       -- dbms_output.put_line(case when EvaluateCondition(c.FeeScheduleId, c.FeeDefinitionId, a_PosseObjectId, a_DataSourceObjectId)= True then 'True' else 'False' end);
      --end if;
      if EvaluateCondition(c.FeeScheduleId, c.FeeDefinitionId, a_PosseObjectId, a_DataSourceObjectId) then
      --dbms_output.put_line(c.FeeScheduleId || ' ' || c.FeeDefinitionId|| ' ' ||  a_PosseObjectId|| ' ' ||  a_DataSourceObjectId);  
        GetData(c.FeeScheduleId, c.FeeDefinitionId, t_FeeDefinitionList(t_FeeDefinitionList.count + 1));
      end if;

    end loop;

    return t_FeeDefinitionList;
  end;   
  
  procedure GetData (
    a_FeeScheduleId             udt_Id,
    a_FeeDefinitionId           udt_Id,
    a_Data                  out query.o_FeeDefinition%rowtype
  ) is
  
  t_Data  feescheduleplus.FeeDefinition%rowtype;
  begin
  
    feescheduleplus.pkg_FeeDefinition.GetData(a_FeeScheduleId, a_FeeDefinitionId, t_Data);    
    
    select fd.*
      into a_Data
      from api.registeredexternalobjects reo 
      join query.o_feedefinition fd on fd.ObjectId = reo.ObjectId---feescheduleplus.feedefinition fd on fd.feedefinitionid = to_number(reo.LinkValue )
     where reo.LinkValue =  t_Data.feedefinitionid;
    
  end;

end pkg_FeeSchedule;
/
