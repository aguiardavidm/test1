create or replace package pkg_ABC_ProductRenewal is

  /*---------------------------------------------------------------------------
   * Public type declarations
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * PostVerify()
   *   Wrapper for the Post Verify event on the Product Registration job
   *-------------------------------------------------------------------------*/
  procedure PostVerify(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
    );

  /*---------------------------------------------------------------------------
   * SetRenewalRegistrant()
   *   Sets the Registrant for a Product Registration Renewal job based on the
   * selected Legal Entity on the public application. Runs on 
   * UseLegalEntityObjectId
   *-------------------------------------------------------------------------*/
  procedure SetRenewalRegistrant(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
    );

  /*---------------------------------------------------------------------------
   * RunRegistrantChecks()
   *   Checks Registrant selection criteria
   *-------------------------------------------------------------------------*/
  procedure RunRegistrantChecks(
    a_ObjectId        udt_Id,
    a_AsOfDate        date
    );

  /*---------------------------------------------------------------------------
   * SetRegistrantProductCount()
   *   Sets the Registrant Product Count on constructor of Registrant rel.
   * This allows an internal application to correctly assign the product count
   * without the need to evaluate the Applicant/Licensee list on the public site
   *-------------------------------------------------------------------------*/
  procedure SetRegistrantProductCount(
    a_ObjectId        udt_Id,
    a_AsOfDate        date
  );

  /*---------------------------------------------------------------------------
   * PopulateProductXrefs()
   *   Copies active products for high-volume Registrant to Renewal job
   *-------------------------------------------------------------------------*/
  procedure PopulateProductXrefs(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  /*---------------------------------------------------------------------------
   * SetOnlineDetails() -- PUBLIC
   *   Sets Online Submitted details on XRef Products
   *-------------------------------------------------------------------------*/
  procedure SetOnlineDetails(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
    );
    
  /*---------------------------------------------------------------------------
   *  RunRetirementCheck()
   *    On submittal of the job, checks to see if any products have been 
   * selected for retirement
   *-------------------------------------------------------------------------*/
  procedure RunRetirementCheck(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
    );

  /*---------------------------------------------------------------------------
   * RenewProducts() -- PUBLIC
   *   Renews selected Products
   *-------------------------------------------------------------------------*/
  procedure RenewProducts(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * RetireProducts()
   *   Retires selected Products
   *-------------------------------------------------------------------------*/
  procedure RetireProducts(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
    );

  /*---------------------------------------------------------------------------
   * CheckFees()
   *   Check outstanding Fees
   *-------------------------------------------------------------------------*/
  procedure CheckFees(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
    );

  /*---------------------------------------------------------------------------
   * CheckProductTypes()
   *   On submittal of the job, checks to see if any products that have not been
   * select for retirement do not have an Active Product Type
   *-------------------------------------------------------------------------*/
  procedure CheckProductTypes(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
    );

  /*---------------------------------------------------------------------------
   * OnlineErrorListPRRenew() -- PUBLIC
   *   Used to call a list of errors for products to be renewed that are expired
   * Called from ErrorsInHTML to get the list of errors.
   *-------------------------------------------------------------------------*/
  procedure OnlineErrorListPRRenew(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );
end pkg_ABC_ProductRenewal;
/
create or replace package body pkg_ABC_ProductRenewal is

  /*---------------------------------------------------------------------------
   * PostVerify()
   *   Wrapper for the Post Verify event on the Product Registration job
   *-------------------------------------------------------------------------*/
  procedure PostVerify(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
    ) is
    t_JobId               udt_Id;
    t_Clerk               varchar2(4000);
    --t_RevClerk            varchar2(4000);
    t_ClerkId             number(9);
    t_EndPoint            udt_Id;
    t_RelId               udt_Id;
    t_EnteredOnline       varchar2(1);

  begin
    t_JobId               := a_ObjectId;
    t_Clerk               := api.pkg_columnquery.Value(t_JobId,'DefaultClerk');
    --t_RevClerk            := api.pkg_columnquery.Value(t_JobId,'DefaultRevenueClerk');
    t_EnteredOnline       := api.pkg_columnquery.Value(t_JobId,'EnteredOnline');

    if api.pkg_columnquery.Value(t_JobId,'SubmitRenewal') = 'Y' then
       if api.pkg_columnquery.NumericValue(t_JobId,'RegistrantObjectId') is null then
          api.pkg_errors.RaiseError(-20000,'Please enter a Registrant');
       end if;
    end if;

    /* Set the Default Clerk */
    if t_Clerk is null then
      -- Get the end point
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal','ProductRenewalClerk');

      -- Get the clerk user id
      select r.UserId
        into t_ClerkId
        from query.o_SystemSettings o
        join query.r_DefaultPRClerkSysSetting r
          on o.ObjectId = r.SystemSettingsObjectId
       where o.ObjectDefTypeId = 1
         and rownum = 1;
      -- Create relationship
      t_RelId := api.pkg_relationshipupdate.New(t_EndPoint,t_JobId,t_ClerkId);
    end if;

/*    \* Set the Default Revenue Clerk *\
    if t_RevClerk is null then
      -- Get the end point
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal','ProductRenewalRevClerk');

      -- Get the revenue clerk user id
      select r.UserId
        into t_ClerkId
        from query.o_SystemSettings o
        join query.r_DefaultRevClerkSysSetting r
          on o.ObjectId = r.SystemSettingsObjectId
       where o.ObjectDefTypeId = 1
         and rownum = 1;
      -- Create relationship
      t_RelId := api.pkg_relationshipupdate.New(t_EndPoint,t_JobId,t_ClerkId);
    end if;*/

    /* Generate Fees */
    if api.pkg_columnquery.value(t_JobId,'XrefGenerationComplete') = 'Y' then
      if api.pkg_columnquery.value(t_JobId,'ReGenerateFees') = 'Y' then
        extension.pkg_fees.ReGenerateFees(t_JobId,a_asofdate);
        api.pkg_columnupdate.SetValue(t_JobId,'ReGenerateFees','N');
      end if;
    end if;

  end PostVerify;

  /*---------------------------------------------------------------------------
   * SetRenewalRegistrant()
   *   Sets the Registrant for a Product Registration Renewal job based on the
   * selected Legal Entity on the public application. Runs on 
   * UseLegalEntityObjectId
   *-------------------------------------------------------------------------*/
  procedure SetRenewalRegistrant(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
    ) is
    t_JobId               udt_Id := a_objectId;
    t_RegistrantEndpoint  udt_Id;
    t_NewRegistrant       udt_Id;
    t_SelectedLE          udt_Id;
    t_HasRegistrant       varchar2(1);
    t_EndPoint            udt_Id;
    t_ProductIds          udt_IdList;

  begin
    t_HasRegistrant       := extension.pkg_RelUtils.StoredRelExistsViaEPNames(t_JobId,'Registrant');
    t_RegistrantEndPoint  := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal','Registrant');
    t_SelectedLE          := api.pkg_columnquery.Value(t_JobId,'OnlineUseLegalEntityObjectId');

    -- Make sure that any existing Registrant rels are removed
    if t_HasRegistrant = 'Y' then
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal','Registrant');
      for x in (select r.RelationshipId
                  from api.relationships r
                 where r.FromObjectId = t_JobId
                   and r.EndPointId = t_EndPoint) loop
        api.pkg_relationshipupdate.Remove(x.relationshipid);
      end loop;
    end if;

    -- Create the new Registrant relationship using the selected Legal Entity
    if t_SelectedLE is not null then
      t_NewRegistrant := api.pkg_relationshipupdate.New(t_RegistrantEndpoint,t_JobId,t_SelectedLE);
    end if;

  end SetRenewalRegistrant;

  /*---------------------------------------------------------------------------
   * RunRegistrantChecks()
   *   Checks Registrant selection criteria
   *-------------------------------------------------------------------------*/
  procedure RunRegistrantChecks(
    a_ObjectId        udt_Id,
    a_AsOfDate        date
    ) is
    t_JobId           udt_Id;
    t_RegId           udt_Id;
    t_ProductIds      udt_IdList;
    t_ProdCount       number;
    t_DaysBeforeExp   number;
    t_RegHasRenewal   number;

  begin
  null;
    -- Get the JobId and RegistrantId
    select r.ProdRegJobId,r.RegistrantObjectId
      into t_JobId,t_RegId
      from query.r_ABC_ProdRenewalToRegistrant r
      where r.RelationshipId = a_ObjectId;

    if api.pkg_columnquery.Value(t_JobId,'CancelJob') = 'Y' then
       return;
    end if;

    if api.pkg_columnquery.Value(t_JobId,'SubmitRenewal')  = 'Y' or
       api.pkg_columnquery.Value(t_JobId, 'EnteredOnline') = 'Y' then
       if t_RegId is null then
          api.pkg_errors.raiseerror(-20000, 'Please enter a Registrant.');
       end if;

    -- Make sure the Registrant has renewable products
    if api.pkg_columnquery.value(t_JobId,'RegistrantProductCount') = 0 then
      api.pkg_errors.raiseerror(-20000, 'The Registrant you have selected does not have any products that may be renewed at this time. Please click the ''Cancel Product Renewal'' button below to continue.');
    end if;

    -- Count any existing unapproved or undenied product renewals
    select count(*)
      into t_RegHasRenewal
      from query.r_Abc_Prodrenewaltoregistrant r
     where r.RegistrantObjectId = t_RegId
       and api.pkg_columnquery.value(r.ProdRegJobId, 'StatusName') in ('NEW','REVIEW','PENDRT');
    -- Make sure outstanding Product Renewal jobs do not exist
    -- (>1 condition to account for current job)
    if t_RegHasRenewal > 1 then
       api.pkg_errors.raiseerror(-20000,'The Registrant you have selected has an existing Product Renewal in progress. Please click the ''Cancel Product Renewal'' button below to continue.');
    end if;

    end if;

  end RunRegistrantChecks;

  /*---------------------------------------------------------------------------
   * SetRegistrantProductCount()
   *   Sets the Registrant Product Count on constructor of Registrant rel.
   * This allows an internal application to correctly assign the product count
   * without the need to evaluate the Applicant/Licensee list on the public site
   *-------------------------------------------------------------------------*/
  procedure SetRegistrantProductCount(
    a_ObjectId        udt_Id,
    a_AsOfDate        date
    ) is
    t_JobId           udt_Id;
    t_RegId           udt_Id;
    t_ProductIds      udt_IdList;
    t_ProdCount       number;
    t_DaysBeforeExp   number;
    t_HighVolThresh   number;

  begin

    -- Get the Job Id
    select r.ProdRegJobId
      into t_JobId
      from query.r_ABC_ProdRenewalToRegistrant r
      where r.RelationshipId = a_ObjectId;

    -- Get days before exp date from system settings
    select PRRenewalDaysBeforeExpDate
      into t_DaysBeforeExp
      from query.o_Systemsettings
     where rownum = 1;

    -- Get the Registrant Id
    select r.RegistrantObjectId
      into t_RegId
      from query.r_ABC_ProdRenewalToRegistrant r
      where r.RelationshipId = a_ObjectId;

 --   api.pkg_errors.RaiseError(-20000,(api.pkg_columnquery.datevalue(r.ProductObjectId, 'ExpirationDate') - t_DaysBeforeExp));
    -- Get the Count for active Products that will expire in the year
    select count(r.ProductObjectId)
      into t_ProdCount
      from query.r_ABC_ProductRegistrant r
     where r.RegistrantObjectId = t_RegId
       and api.pkg_columnquery.value(r.ProductObjectId, 'State') = 'Current'
       and trunc(sysdate) >= (api.pkg_columnquery.datevalue(r.ProductObjectId, 'ExpirationDate') - t_DaysBeforeExp);

    -- Set the count on the job
    api.pkg_columnupdate.SetValue(t_JobId,'RegistrantProductCount',t_ProdCount);

    -- Get the HighVolumeThreshold from system settings
    select PRHighVolumeThreshold
      into t_HighVolThresh
      from query.o_Systemsettings
     where rownum = 1;

    -- If appropriate, set IsHighVolume
    if t_ProdCount > t_HighVolThresh then
      api.pkg_columnupdate.setvalue(t_JobId,'IsHighVolume','Y');
    end if;

  end SetRegistrantProductCount;

 /*---------------------------------------------------------------------------
  *  HighVolumeProcessing() -- PUBLIC
  *    Relates the xref objects on the proper high-volume tabs
  *-------------------------------------------------------------------------*/
  procedure HighVolumeProcessing(
    a_JobId                             number,
    a_XRefObjectid                      number,
    a_Ordinal                           number
    ) is
    t_Tab1EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup1');
    t_Tab2EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup2');
    t_Tab3EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup3');
    t_Tab4EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup4');
    t_Tab5EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup5');
    t_Tab6EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup6');
    t_Tab7EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup7');
    t_Tab8EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup8');
    t_Tab9EP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup9');
    t_Tab10EP                           udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup10');
    t_Tab11EP                           udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup11');
    t_Tab12EP                           udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup12');
    t_Tab13EP                           udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup13');
    t_Tab14EP                           udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup14');
    t_Tab15EP                           udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup15');
    t_Tab16EP                           udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal','XrefGroup16');
    t_TabSpecificEP                     udt_Id;
    t_XrefRel                           number;
  begin

    -- Figure out which rel to create.  Each rel is displayed on a separate tab.
    t_TabSpecificEP := (case when a_Ordinal between 1 and 500  then t_Tab1EP
        when a_Ordinal between 501  and 1000 then t_Tab2EP
        when a_Ordinal between 1001 and 1500 then t_Tab3EP
        when a_Ordinal between 1501 and 2000 then t_Tab4EP
        when a_Ordinal between 2001 and 2500 then t_Tab5EP
        when a_Ordinal between 2501 and 3000 then t_Tab6EP
        when a_Ordinal between 3001 and 3500 then t_Tab7EP
        when a_Ordinal between 3501 and 4000 then t_Tab8EP
        when a_Ordinal between 4001 and 4500 then t_Tab9EP
        when a_Ordinal between 4501 and 5000 then t_Tab10EP
        when a_Ordinal between 5001 and 5500 then t_Tab11EP
        when a_Ordinal between 5501 and 6000 then t_Tab12EP
        when a_Ordinal between 6001 and 6500 then t_Tab13EP
        when a_Ordinal between 6501 and 7000 then t_Tab14EP
        when a_Ordinal between 7001 and 7500 then t_Tab15EP
        when a_Ordinal between 7501 and 8000 then t_Tab16EP
        end);

    -- Now create the rel
    if t_TabSpecificEP is not null then
      t_XrefRel := api.pkg_relationshipupdate.New(t_TabSpecificEP,a_JobId,
          a_XRefObjectid);
    end if;

  end HighVolumeProcessing;

  /*---------------------------------------------------------------------------
   * PopulateProductXrefs()
   *   Copies active products for high-volume Registrant to Renewal job
   *-------------------------------------------------------------------------*/
  procedure PopulateProductXrefs(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
    ) is
    t_JobId                             udt_Id;
    t_RegistrantId                      number;
    t_ProductIds                        udt_IdList;
    t_VintageIds                        udt_IdList;
    t_ProductCount                      number;
    t_XrefToProductEP                   udt_Id;
    t_XrefToRenewalEP                   udt_Id;
    t_XrefToVintageEP                   udt_Id;
    t_XrefToTypeEP                      udt_Id;
    t_XrefRel                           number;
    t_XrefTypeRel                       udt_Id;
    t_Ordinal                           number;
    t_ProductXrefId                     number;
    t_NewXrefObjId                      udt_Id;
    t_RegEndPoint                       udt_Id;
    t_HighVolume                        char(1);
    t_DaysBeforeExp                     number;
    t_RegHasRenewal                     number;
    t_ProductTypeID                     udt_Id;
  begin

    -- This procedure is run on a process, so get the jobid
    t_JobId := api.pkg_columnquery.Value(a_ObjectId,'JobId');
    t_HighVolume := api.pkg_columnquery.value(t_JobId,'IsHighVolume');
    -- Get the Registrant Id
    t_RegEndPoint := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal', 'Registrant');

    select r.ToObjectId
    into t_RegistrantId
    from api.relationships r
    where r.FromObjectId = t_JobId
      and r.EndPointId = t_RegEndPoint;

    select PRRenewalDaysBeforeExpDate
    into t_DaysBeforeExp
    from query.o_Systemsettings
    where rownum = 1;

    -- Get the active Products for the appropriate time frame
    select r.ProductObjectId
    bulk collect into t_ProductIds
    from query.r_ABC_ProductRegistrant r
    where r.RegistrantObjectId = t_RegistrantId
      and api.pkg_columnquery.value(r.ProductObjectId, 'State') = 'Current'
      and trunc(sysdate) >= (api.pkg_columnquery.datevalue(
          r.ProductObjectId, 'ExpirationDate') - t_DaysBeforeExp)
    order by api.pkg_columnquery.value(r.ProductObjectId, 'Name');

    t_XrefToProductEP := api.pkg_configquery.EndPointIdForName('o_ABC_ProductXref',
        'Product');
    t_XrefToTypeEP := api.pkg_configquery.EndPointIdForName('o_ABC_ProductXref',
        'ProductType');
    t_XrefToRenewalEP := api.pkg_configquery.EndPointIdForName('o_ABC_ProductXref',
        'ProductRenewal');
    t_XrefToVintageEP := api.pkg_configquery.EndPointIdForName('o_ABC_ProductXref',
        'Vintage');
    t_ProductCount := api.pkg_columnquery.value(t_JobId,'RegistrantProductCount');
    t_Ordinal := 1;

    -- Loop through all of the Registrant's Products
    for id in 1..t_ProductIds.count loop
      -- Exit when number of products exceed 8000 (2018 requirements)
      if id > 8000 then
        exit;
      end if;
      t_ProductXrefId := ProductXref_seq.nextval;

      -- Insert values into the external table row
      insert into ProductXref_t
      values (t_ProductXrefId,null,t_JobId,t_ProductIds(id));

      -- Register the product xref with posse
      t_NewXrefObjId := api.Pkg_Objectupdate.RegisterExternalObject(
          'o_ABC_ProductXref',t_ProductXrefId);

      -- Set the previously null value for PosseXrefObjId
      update ProductXref_t
      set PosseXrefObjId = t_NewXrefObjId
      where ProductXrefId = t_ProductXrefId;

      -- Create rel from Product to Xref
      t_XrefRel := api.pkg_relationshipupdate.New(t_XrefToProductEP,t_NewXrefObjId,
          t_ProductIds(id));

      -- Create rel from Xref to Product Type if the Type is active
      t_ProductTypeID := api.pkg_columnquery.value(t_ProductIds(id), 'TypeId');
      if api.pkg_columnquery.value(t_ProductTypeId, 'Active') = 'Y' then
        t_XrefTypeRel := api.pkg_relationshipupdate.New(t_XrefToTypeEP,
            t_NewXrefObjId,t_ProductTypeID);
      end if;

      -- Create rel from Renewal to Xref
      t_XrefRel := api.pkg_relationshipupdate.New(t_XrefToRenewalEP,t_NewXrefObjId,
          t_JobId);

      -- Create rels from Vintage object to Xref
      t_VintageIds := extension.pkg_objectquery.RelatedObjects(t_ProductIds(id),
          'Vintage');

      for v in 1..t_VintageIds.count loop
        t_XrefRel := api.pkg_relationshipupdate.New(t_XrefToVintageEP,
            t_NewXrefObjId,t_VintageIds(v));
      end loop;

      -- Do high volume processing if necessary
      if t_HighVolume = 'Y' then
        HighVolumeProcessing(t_JobId,t_NewXrefObjId,t_Ordinal);
        t_ordinal := t_ordinal + 1;
      end if;

    end loop;

    -- Mark the generation as complete
    api.pkg_columnupdate.SetValue(t_JobId,'XrefGenerationComplete','Y');
    api.pkg_columnupdate.SetValue(t_JobId,'ReGenerateFees','Y');

    if t_HighVolume = 'Y' then
      abc.pkg_ABC_Email.SendPRRenewalEmail(t_JobId,sysdate);
    end if;

  end PopulateProductXrefs;
  
  /*---------------------------------------------------------------------------
   * SetOnlineDetails() -- PUBLIC
   *   Sets Online Submitted details on XRef Products
   *-------------------------------------------------------------------------*/
  procedure SetOnlineDetails(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
    ) is
    t_XrefIds                           udt_IdList;
    t_CreatedOnline                     varchar2(1);
    t_SetOnlineDetails                  varchar2(1);
  begin
    t_SetOnlineDetails := api.pkg_columnquery.Value(a_ObjectId, 'SetOnlineDetails');
    t_CreatedOnline := api.pkg_columnquery.Value(a_ObjectId, 'EnteredOnline');
    if t_CreatedOnline = 'Y' and t_SetOnlineDetails = 'Y' then
      t_XrefIds := extension.pkg_objectquery.RelatedObjects(a_ObjectId, 'ProductXref');
      
      -- Loop through Xrefs and set each Online Detail as of submittal
      for i in 1..t_xrefIds.count loop
        api.pkg_columnupdate.setvalue(t_xrefIds(i), 'OnlineRenewal', api.pkg_columnquery.value(
            t_xrefIds(i), 'Renew'));
        api.pkg_columnupdate.SetValue(t_xrefIds(i), 'OnlineVintages', api.pkg_columnquery.value(
            t_xrefIds(i), 'CurrentVintageYears'));
        api.pkg_columnupdate.SetValue(t_xrefIds(i), 'OnlineType', api.pkg_columnquery.value(
            t_xrefIds(i), 'NewType'));            
        api.pkg_columnupdate.SetValue(t_xrefIds(i), 'OnlineSubmitted', 'Y');

      end loop;
      api.pkg_columnupdate.SetValue(a_ObjectId, 'OnlineSubmitted', 'Y');
      api.pkg_columnupdate.SetValue(a_ObjectId, 'SetOnlineDetails', 'N');
    end if;
  end SetOnlineDetails;
    

  /*---------------------------------------------------------------------------
   *  RunRetirementCheck()
   *    On submittal of the job, checks to see if any products have been 
   * selected for retirement
   *-------------------------------------------------------------------------*/
  procedure RunRetirementCheck(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
    ) is
    t_JobId             udt_Id;
    t_HasProdToRetire   varchar2(1);
    t_XrefIds           udt_IdList;
    t_Renew             varchar2(3);
  begin
    t_JobId             := api.pkg_columnquery.value(a_ObjectId,'JobId');
    t_XrefIds           := extension.pkg_objectquery.RelatedObjects(t_JobId,'ProductXref');

    -- Loop through Xrefs and flag if any are selected for retirement
    for i in 1..t_XrefIds.count loop
      t_Renew := api.pkg_columnquery.value(t_XrefIds(i),'Renew');
      if t_Renew = 'No' then
        -- set the products to retire flag
        api.pkg_columnupdate.setvalue(t_JobId,'HasProductsToRetire','Y');
        exit;
      end if;
    end loop;

  end RunRetirementCheck;

  /*---------------------------------------------------------------------------
   * RenewProducts() -- PUBLIC
   *   Renews selected Products
   *-------------------------------------------------------------------------*/
  procedure RenewProducts(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
    ) is
    t_JobId                             udt_Id;
    t_XrefIds                           udt_IdList;
    t_XrefVintageIds                    udt_IdList;
    t_ProdVintageIds                    udt_IdList;
    t_ProdDoubleIds                     udt_IdList;
    t_ProductId                         udt_Id;
    t_ExpDate                           date;
    t_BaseExpDate                       date;
    t_NewExpDate                        date;
    t_NonRenewNotice                    date;
    t_Renew                             varchar2(3);
    t_ProdVintageEP                     udt_Id;
    t_NewRel                            udt_Id;
    t_RelId                             udt_Id;
    t_CurrExpYear                       number;
    t_BaseExpMMDD                       number;
    t_OldProductTypeId                  udt_Id;
    t_NewProductTypeId                  udt_Id;
    t_ProductTypeRelId                  udt_Id;
    t_ProductToTypeEP                   udt_Id;
  begin

    t_JobId := api.pkg_columnquery.Value(a_ObjectId,'JobId');
    t_XrefIds := extension.pkg_objectquery.RelatedObjects
        (t_JobId,'ProductXref');
    t_ProdVintageEP := api.pkg_configquery.EndPointIdForName
        ('o_ABC_Product','Vintage');
    t_ProductToTypeEP := api.pkg_configquery.EndPointIdForName
        ('o_ABC_Product','ProductType');
    t_BaseExpDate := abc.pkg_ABC_ProductRegistration.CreateExpirationDate();
    t_BaseExpMMDD := to_char(t_BaseExpDate,'mmdd');
    t_ExpDate := t_BaseExpDate + 1 + 1/1440;

    api.pkg_columnupdate.setvalue(t_JobId,'ExpirationDate',t_ExpDate);
    -- Loop through the Product Xrefs
    for i in 1..t_XrefIds.count loop
      t_Renew := api.pkg_columnquery.value(t_XrefIds(i),'Renew');
      -- PRODUCTS TO RENEW
      if t_Renew = 'Yes' then
        -- Get the Product Id from the Xref
        t_ProductId := extension.pkg_objectquery.RelatedObject
            (t_XrefIds(i),'Product');
        -- Update Product Type if it has changed
        t_OldProductTypeId := api.pkg_columnquery.Value(t_ProductId, 'TypeId');
        t_NewProductTypeId := api.pkg_columnquery.Value(t_XrefIds(i), 'NewTypeId');
        if t_OldProductTypeId != t_NewProductTypeId then
          -- Remove the relationship to the old type
          select t.RelationshipId
          into t_ProductTypeRelId
          from query.r_abc_producttypeproduct t
          where t.ProductId = t_ProductId
            and t.ProductTypeId = t_OldProductTypeId;
          api.pkg_relationshipupdate.Remove(t_ProductTypeRelId);
          -- Create the relationship to the new type
          t_ProductTypeRelId := api.pkg_relationshipupdate.New
              (t_ProductToTypeEP, t_ProductID, t_NewProductTypeId);
        end if;

        -- Get the Vintages
        if api.pkg_columnquery.value(t_XrefIds(i), 'ProductTypeHasVintages') = 'Y' then
          -- Only grab vintages from the xref if the new product type can have vintages
          t_XrefVintageIds := extension.pkg_objectquery.RelatedObjects
              (t_XrefIds(i),'Vintage');
        else
          -- Clear all Vintages from the Xref object as well
          for v in (select relationshipid
                    from query.r_abc_Vintages r
                    where r.ProductXrefId = t_XrefIds(i)) loop
            api.pkg_relationshipupdate.remove(v.relationshipid);
          end loop;
          -- This just clears out the variable
          t_XrefVintageIds := extension.pkg_objectquery.RelatedObjects
              (t_XrefIds(i),'Vintage');
        end if;
        t_ProdVintageIds := extension.pkg_objectquery.RelatedObjects
            (t_ProductId,'Vintage');
        t_ProdDoubleIds  := t_ProdVintageIds;

        -- Update Vintages
        extension.pkg_CollectionUtils.Subtract(t_ProdVintageIds,t_XrefVintageIds);
        if t_ProdVintageIds.count != 0 then
          for pv in 1..t_ProdVintageIds.count loop
            select r.RelationshipId
            into t_RelId
            from api.relationships r
            where r.ToObjectId = t_ProdVintageIds(pv)
              and r.FromObjectId = t_ProductId;
            api.pkg_relationshipupdate.Remove(t_RelId);
          end loop;
        end if;
        extension.pkg_CollectionUtils.Subtract(t_XrefVintageIds,t_ProdDoubleIds);
        if t_XrefVintageIds.count != 0 then
          for xv in 1..t_XrefVintageIds.count loop
            t_NewRel := api.pkg_relationshipupdate.New
                (t_ProdVintageEP,t_ProductId,t_XrefVintageIds(xv));
          end loop;
        end if;

        -- use system settings to determine appropriate expiration date
        t_CurrExpYear := to_char(api.pkg_columnquery.DateValue
            (t_ProductId,'ExpirationDate'),'yyyy');
        t_NewExpDate  := 
            to_date(lpad(to_char(t_BaseExpMMDD || t_CurrExpYear + 1),8,'0'),'mmddyyyy');
        t_NonRenewNotice :=
            abc.pkg_ABC_ProductRegistration.CreateNonRenewalNoticeDate(t_NewExpDate);
        api.pkg_columnupdate.SetValue(t_ProductId,'ExpirationDate',t_NewExpDate);
        api.pkg_columnupdate.SetValue(t_ProductId,'NonRenewalNoticeDate',t_NonRenewNotice);
        api.pkg_columnupdate.SetValue(t_ProductId,'State','Current');

      /* PRODUCTS TO RETIRE */
      /*elsif t_Renew = 'No' then
        -- set the products to retire flag
        api.pkg_columnupdate.setvalue(t_JobId,'HasProductsToRetire','Y');
      end if;*/
      end if;
    end loop;

    -- If products to retire is true, schedule the creation of a retire products
    -- process on the appropriate date
    if api.pkg_columnquery.value(t_JobId,'HasProductsToRetire') = 'Y' then
      -- Ssetting this detail will trigger the process creation for Retire
      -- Products
      api.pkg_columnupdate.setvalue(t_JobId,'RetireProducts','Y');
    end if;

  end RenewProducts;

  /*---------------------------------------------------------------------------
   * RetireProducts()
   *   Retires selected Products
   *-------------------------------------------------------------------------*/
  procedure RetireProducts(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
    ) is
    t_JobId             udt_Id;
    t_XrefIds           udt_IdList;
    t_ProdDistIds       udt_IdList;
    t_Renew             char(10);
    t_ProductId         udt_Id;
    t_ProdToDistEP      udt_Id;
    t_DistributorRel    udt_Id;

  begin
    t_JobId             := api.pkg_columnquery.value(a_ObjectId,'JobId');
    t_XrefIds           := extension.pkg_objectquery.RelatedObjects(t_JobId,'ProductXref');
    t_ProdToDistEP      := api.pkg_configquery.EndPointIdForName('o_ABC_Product','DistributorLic');

    -- Loop through the Product Xrefs
    for i in 1..t_XrefIds.count loop
      t_Renew := api.pkg_columnquery.value(t_XrefIds(i),'Renew');

      /* PRODUCTS TO RETIRE */
      if t_Renew = 'No' then

        -- Get Product Id from Xref
        t_ProductId   := extension.pkg_objectquery.RelatedObject(t_XrefIds(i),'Product');

        -- Set Product to Historical
        api.pkg_columnupdate.SetValue(t_ProductId,'State','Historical');

        -- Set Product DirectExpire to Y so a Non-Renewal job is not created for it
        api.pkg_columnupdate.SetValue(t_ProductId,'DirectExpire','Y');

      end if;
    end loop;
    api.pkg_columnupdate.setvalue(t_JobId,'ProductRetirementComplete','Y');

  end RetireProducts;

  /*---------------------------------------------------------------------------
   * CheckFees()
   *   Check outstanding Fees
   *-------------------------------------------------------------------------*/
  procedure CheckFees(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
    ) is
    t_JobId               udt_Id;

  begin
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');

    -- Check if all fees are paid
    if api.pkg_columnquery.Value(t_JobId, 'HasNonZeroBalances') = 'Y' then
       api.pkg_errors.RaiseError(-20000, 'Before you go on, ensure all fees are paid.');
    end if;

  end CheckFees;

  /*---------------------------------------------------------------------------
   * CheckProductTypes()
   *   On submittal of the job, checks to see if any products that have not 
   * been select for retirement do not have an Active Product Type
   *-------------------------------------------------------------------------*/
  procedure CheckProductTypes(
    a_ObjectId          udt_Id,
    a_AsOfDate          date
    ) is
    t_XrefIds           udt_IdList;
    t_Type              varchar2(100);
    t_XrefEP            udt_Id := api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'ProductXref');
  begin
    t_XrefIds           := api.pkg_objectquery.RelatedObjects(a_ObjectId, t_XrefEP);

    if api.pkg_columnquery.value(a_ObjectID,'CheckProductTypes') = 'Y' then
      -- Loop through Xrefs and check if any are missing a Type
      for i in 1..t_XrefIds.count loop
        if api.pkg_columnquery.value(t_XrefIds(i),'Renew') = 'Yes' then
          t_Type := api.pkg_columnquery.value(t_XrefIds(i),'NewType');
          if t_Type is null then
            api.pkg_errors.RaiseError(-20000, 'Product Type is Required.');
          end if;
        end if;
      end loop;
      api.pkg_columnupdate.setvalue(a_ObjectId, 'CheckProductTypes', 'N');
      OnlineErrorListPRRenew(a_ObjectId, sysdate);
    end if;

  end CheckProductTypes;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPRRenew() -- PUBLIC
   *   Used to call a list of errors for products to be renewed that are expired
   * Called from ErrorsInHTML to get the list of errors.
   *-------------------------------------------------------------------------*/
  procedure OnlineErrorListPRRenew(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_XrefEP                            udt_Id := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PRRenewal', 'ProductXref');
    t_XrefIds                           udt_IdList;
    t_ExpiredProducts                   udt_IdList;
    t_ProductNames                      varchar2(4000);
  begin
    t_XrefIds := api.pkg_objectquery.RelatedObjects(a_ObjectId, t_XrefEP);
    
    -- Check all products on PRRenewal job and their status
    -- If product is to be renewed but is expired, add product name to error msg
    for i in 1..t_XrefIds.count loop
      if api.pkg_ColumnQuery.Value(t_XrefIds(i), 'Renew') = 'Yes'
          and api.pkg_ColumnQuery.Value(t_XrefIds(i), 'ProductIsCurrent') = 'N' then
        t_ExpiredProducts(i) := t_XrefIds(i);
        t_ProductNames := t_ProductNames || api.pkg_ColumnQuery.Value(t_XrefIds(i), 'Name')
            || chr(10) || chr(13);
      end if;
    end loop;
    if t_ExpiredProducts.count > 0 then
      api.pkg_Errors.RaiseError(-20000, 'The following products are expired and cannot be renewed: '
          || chr(10) || chr(13) || t_ProductNames);
    end if;
  end OnlineErrorListPRRenew;

end pkg_ABC_ProductRenewal;
/
