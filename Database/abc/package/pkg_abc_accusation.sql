create or replace package pkg_ABC_Accusation is

  -----------------------------------------------------------------------------
  -- Author  : BRUCE.JAKEWAY
  -- Created : 2011 May 12 09:39:09
  -- Purpose : Functions and procedures for the Accusation job
  -----------------------------------------------------------------------------
  -- Change History:
  -- 16Aug2011, StanH: InspectionCreatesAccusation not needed; Violations on
  --                   Accusation are now indirect instead of stored.
  -----------------------------------------------------------------------------

  -- Public type declarations
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  -- Public function and procedure declarations

  -----------------------------------------------------------------------------
  -- CopyPleadingText
  --  Copy the pleading text from the Pleading Type object to the Pleading
  -- object if it doesn't already exist.
  -----------------------------------------------------------------------------
  procedure CopyPleadingText (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- DecisionActions
  --  Items to do on completion of the Penalize Licence, Revoke License, and
  -- Suspend License processes.  Items include:
  --  - Generating Fees
  --  - Setting the License status
  --  - Sending Notifications
  --  - Checking whether to complete the job
  -----------------------------------------------------------------------------
  procedure DecisionActions (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );


  -----------------------------------------------------------------------------
  -- ManualReactivationActions
  --  When a suspension ends manually, determine if we need to reactive the
  -- license.  All accusation jobs against this license will be completed and
  -- the license will be set to 'Active'.
  --
  -- If so, create the Take Reactivation Actions process
  -- Otherwise determine if we should complete the job.
  -----------------------------------------------------------------------------
  procedure ManualReactivationActions (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- PaymentProcessing
  --  Items to do when a payment has been made against the job.
  -----------------------------------------------------------------------------
  procedure PaymentProcessing (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- SystemSuspensionActions
  --  Added to Suspend Outcome of SystemProcess in addition to the Copy Details that is already there.
  -- Marks the current line in the date grid as suspended if it exists.
  -----------------------------------------------------------------------------
  procedure SystemSuspensionActions (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- SystemReactivationActions
  --  When a suspension ends, determine if we need to reactive the license.
  -- The system should be calling this and a license will not be reactivated if
  -- there are other accusation jobs against this license which have also
  -- suspended the license.
  --
  -- If so, create the Take Reactivation Actions process
  -- Otherwise determine if we should complete the job.
  -----------------------------------------------------------------------------
  procedure SystemReactivationActions (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- CopySuspensionDate
  --  Copy the rels to the SuspensionDate objects from Record and Execute Decision
  -- to Suspend License on constructor of the latter.
  -----------------------------------------------------------------------------
  procedure CopySuspensionDate (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_Accusation;

 
/

create or replace package body pkg_ABC_Accusation is

  -----------------------------------------------------------------------------
  -- Function and procedure implementations
  --
  -- Change History:
  -- 08Aug2011, Stan H: Allow Expired licenses to be Suspended/Revoked.  (This is possible
  --            either now or at a scheduled time. Suspended status supersedes
  --            Expired, but it must go back to Expired after suspension ends.)
  -----------------------------------------------------------------------------

  -----------------------------------------------------------------------------
  -- JobCompletionCheck
  --  Whether to complete the job.
  -----------------------------------------------------------------------------
  procedure JobCompletionCheck (
    a_JobId                             udt_Id
  ) is
  begin
    if api.pkg_ColumnQuery.Value(a_JobId, 'ShouldCompleteJob') = 'Y' then
      extension.pkg_ProcessUpdate.NewComplete(a_JobId, 'p_ABC_SystemProcess', 'Complete');
    end if;
  end JobCompletionCheck;


  -----------------------------------------------------------------------------
  -- RevokeLicense
  --
  -- Private procedure to execute the mechanics of revoking license LicenseId after
  -- confirming that they are in a valid state.
  --
  -- Created: 26Jul2011, Stan H
  -- Notes: Added check on state.  Parameterized cursor was already implemented, but not checked in.
  -- Changed: 08Aug2011, Stan H: allow revocation of Expired license
  -----------------------------------------------------------------------------
  procedure RevokeLicense (
    a_LicenseObjectId                   udt_Id,  -- license to revoke
    a_JobId                             udt_Id  -- current job, which is not to be autoclosed
  ) is

    t_LicenseState                      varchar2(20) := api.pkg_ColumnQuery.Value(a_LicenseObjectId, 'State');

    cursor t_OtherSuspendedLicenses (
      c_JobId                           udt_Id
    ) is
      select j.JobId, j.DoSuspendLicense, j.DoReactivateLicense
        from query.r_ABC_AccusationLicense r
        join query.j_ABC_Accusation j
          on r.AccusationJobId = j.ObjectId
       where r.LicenseObjectId = a_LicenseObjectId
         and r.AccusationJobId != c_JobId
         and (j.DoSuspendLicense = 'Y' or j.DoReactivateLicense = 'Y');

  begin

        if t_LicenseState in ('Active', 'Suspended', 'Expired') then
          api.pkg_ColumnUpdate.SetValue(a_LicenseObjectId, 'State', 'Revoked');

          -- remove other suspensions against this license
          for j in t_OtherSuspendedLicenses(a_JobId) loop
            if j.DoSuspendLicense = 'Y' then
              api.pkg_ColumnUpdate.SetValue(j.JobId, 'DoSuspendLicense', 'N');
            elsif j.DoReactivateLicense = 'Y' then
              api.pkg_ColumnUpdate.SetValue(j.JobId, 'DoReactivateLicense', 'N');
            end if;

            -- Determine whether to complete the job
            JobCompletionCheck(j.JobId);
            if api.pkg_ColumnQuery.Value(j.JobId, 'SuspensionActualEndDate') is null then
              api.pkg_ColumnUpdate.SetValue(j.JobId, 'SuspensionActualEndDate', trunc(sysdate));
            end if;
          end loop;
        end if; -- License in revocable state

  end RevokeLicense;


  -----------------------------------------------------------------------------
  -- DecisionActions
  --  Items to do on completion of the Penalize Licence, Revoke License, and
  -- Suspend License processes.  Items include:
  --  - Generating Fees
  --  - Setting the License State
  --  - Sending Notifications
  --
  -- Change History:
  -- 26Jul2011,  Stan H
  --    Cause revoke and suspend to apply to secondary licenses.  This means breaking
  --    out repeatedly called code into a private procedure (RevokeLicense) and adding loops
  --    through secondary licenses.  Depends on config for system process to set license state for future suspensions.
  -----------------------------------------------------------------------------
  procedure DecisionActions (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id := api.pkg_ColumnQuery.NumericValue(a_ProcessId, 'JobId');
    t_LicenseObjectId                   udt_Id := api.pkg_ColumnQuery.NumericValue(t_JobId, 'LicenseObjectId');
    t_LicenseState                      varchar2(20) := api.pkg_ColumnQuery.Value(t_LicenseObjectId, 'State');
    t_PenaltyAmount                     number := api.pkg_ColumnQuery.NumericValue(a_ProcessId, 'PenaltyAmount');
    t_ProcessType                       varchar2(30) := api.pkg_ColumnQuery.Value(a_ProcessId, 'ObjectDefName');
    t_SuspensionDurationType            varchar2(100);
    t_SuspensionEndDate                 date;
    t_SuspensionStartDate               date;
    t_Today                             date;
    t_SuspendLicenseId                 udt_Id := api.pkg_ColumnQuery.NumericValue(t_JobId, 'SuspendLicenseId');
    t_CurrentSuspensionDateId                 udt_Id := api.pkg_ColumnQuery.NumericValue(t_SuspendLicenseId, 'CurrentSuspensionDateId');

  begin
    -- Generate Fees
    api.pkg_ColumnUpdate.SetValue(t_JobId, 'PenaltyAmount', t_PenaltyAmount);
    if nvl(t_PenaltyAmount, 0) > 0 then
      api.pkg_ColumnUpdate.SetValue(t_JobId, 'GenerateFees', 'Y');
      api.pkg_ColumnUpdate.SetValue(t_JobId, 'PreventJobCompletion', 'Y');
    end if;

    -- Set License State
    case t_ProcessType
      when 'p_ABC_RevokeLicense' then

        if t_LicenseState not in ('Active', 'Suspended', 'Expired') then
          api.pkg_Errors.RaiseError(-20000, 'As the License is ' || t_LicenseState || ', you may not Revoke the License.');
        end if;

        RevokeLicense (t_LicenseObjectId, t_JobId);   -- revoke license

        -- revoke all secondary licenses
        for i in (select ps.SecondaryLicenseObjectId
                  from query.r_ABC_PrimarySecondaryLicense ps
                  where ps.PrimaryLicenseObjectId = t_LicenseObjectId) loop
            RevokeLicense (i.secondarylicenseobjectid, t_JobId);
        end loop;

      when 'p_ABC_SuspendLicense' then
        if t_LicenseState not in ('Active', 'Suspended', 'Expired') then
          api.pkg_Errors.RaiseError(-20000, 'As the License is ' || t_LicenseState || ', you may not Suspend the License.');
        end if;

        t_SuspensionDurationType := api.pkg_ColumnQuery.Value(a_ProcessId, 'SuspensionDurationType');
        if t_SuspensionDurationType = 'Intermittent' then
          t_SuspensionStartDate := trunc(api.pkg_ColumnQuery.DateValue(a_ProcessId, 'NextSuspensionStartDate'));
          t_SuspensionEndDate := trunc(api.pkg_ColumnQuery.DateValue(a_ProcessId, 'NextSuspensionEndDate'));
        else
          t_SuspensionStartDate := trunc(api.pkg_ColumnQuery.DateValue(a_ProcessId, 'SuspensionStartDate'));
          t_SuspensionEndDate := trunc(api.pkg_ColumnQuery.DateValue(a_ProcessId, 'SuspensionEndDate'));
        end if;

        t_Today := trunc(sysdate);

        api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionStartDate', t_SuspensionStartDate);
        api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionDurationType', t_SuspensionDurationType);
        if t_SuspensionDurationType = 'Indefinite' then
          api.pkg_ColumnUpdate.RemoveValue(t_JobId, 'SuspensionDays');
          api.pkg_ColumnUpdate.RemoveValue(t_JobId, 'SuspensionStayedDays');
        else
          api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionDays', api.pkg_ColumnQuery.Value(a_ProcessId, 'SuspensionDays'));
          api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionStayedDays', api.pkg_ColumnQuery.Value(a_ProcessId, 'SuspensionStayedDays'));
        end if;
        api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionEndDate', t_SuspensionEndDate);
        api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionConditions', api.pkg_ColumnQuery.Value(a_ProcessId, 'SuspensionConditions'));

        case when t_SuspensionEndDate <= t_Today or
            -- when the suspension end date has passed
              t_SuspensionStartDate >= t_SuspensionEndDate then
            -- when the suspension starts and ends the same day
            api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionActualEndDate', t_SuspensionEndDate);
          when t_SuspensionStartDate <= t_Today then
            -- when the suspension start date is today or some time in the past
            api.pkg_ColumnUpdate.SetValue(t_LicenseObjectId, 'State', 'Suspended');
            if t_CurrentSuspensionDateId is not null then
              api.pkg_Columnupdate.SetValue(t_CurrentSuspensionDateId, 'Suspended', 'Y');
            end if;
            for i in (select ps.SecondaryLicenseObjectId
                      from query.r_ABC_PrimarySecondaryLicense ps
                      join query.o_ABC_License sl on sl.ObjectId = ps.SecondaryLicenseObjectId
                      where ps.PrimaryLicenseObjectId = t_LicenseObjectId
                      and (sl.State = 'Active' or sl.State = 'Expired') ) loop
              -- if secondary license is active or expired, set its state to Suspended
              api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Suspended');
            end loop;
            api.pkg_ColumnUpdate.SetValue(t_JobId, 'DoReactivateLicense', 'Y');
          else
            -- when the suspension is in the future
            api.pkg_ColumnUpdate.SetValue(t_JobId, 'DoSuspendLicense', 'Y'); -- license and any secondary licenses will be suspended on system process outcome
        end case;

      else null;
    end case;

    -- Send Notifications
    pkg_ABC_Workflow.CreateEnforcementNotifications(a_ProcessId, a_AsOfDate);

    -- Determine whether to complete the job
    JobCompletionCheck(t_JobId);
  end DecisionActions;


  -----------------------------------------------------------------------------
  -- InspectionCreatesAccusation
  --  Items to do on constructor of the relationship that gets created when the
  -- Inspection job creates the Accusation job.  Items include:
  --  - cloning the violations
  -----------------------------------------------------------------------------
  procedure InspectionCreatesAccusation (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_AccusationJobId                   udt_Id;
    t_InspectionJobId                   udt_Id;
    t_ViolationObjectId                 udt_Id;

    cursor c_Violations is
      select o.ViolationTypeObjectId, o.ViolationDate, o.ViolationTime, o.Sections, o.Counts
        from query.r_ABC_InspectionViolation r
          join query.o_ABC_Violation o
            on r.ViolationObjectId = o.ObjectId
        where r.InspectionJobId = t_InspectionJobId;

  begin
    select ria.AccusationObjectId, ria.InspectionJobId
      into t_AccusationJobId, t_InspectionJobId
      from query.r_ABC_InspectCreatesAccusation ria
      where ria.RelationshipId = a_RelationshipId;

    -- Clone the Violations from the Inspection job.  Violations need to be editable on the
    -- Accusation job, but the edits shouldn't change the Violations on the Inspection job.
    for v in c_Violations loop
      t_ViolationObjectId := extension.pkg_ObjectUpdate.New('o_ABC_Violation');
      extension.pkg_RelationshipUpdate.New(t_ViolationObjectId, v.ViolationTypeObjectId, 'ViolationType');
      api.pkg_ColumnUpdate.SetValue(t_ViolationObjectId, 'ViolationDate', v.ViolationDate);
      api.pkg_ColumnUpdate.SetValue(t_ViolationObjectId, 'ViolationTime', v.ViolationTime);
      api.pkg_ColumnUpdate.SetValue(t_ViolationObjectId, 'Sections', v.Sections);
      api.pkg_ColumnUpdate.SetValue(t_ViolationObjectId, 'Counts', v.Counts);
      extension.pkg_RelationshipUpdate.New(t_AccusationJobId, t_ViolationObjectId, 'Violation');
    end loop;
  end InspectionCreatesAccusation;


  -----------------------------------------------------------------------------
  -- PaymentProcessing
  --  Items to do when a payment has been made against the job.
  -----------------------------------------------------------------------------
  procedure PaymentProcessing (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is
  begin
    if api.pkg_ColumnQuery.Value(a_JobId, 'PaymentMade') = 'Y' then
      api.pkg_ColumnUpdate.SetValue(a_JobId, 'PreventJobCompletion', 'N');
      api.pkg_ColumnUpdate.SetValue(a_JobId, 'PaymentMade', 'N');

      -- Determine whether to complete the job
      JobCompletionCheck(a_JobId);
    end if;
  end PaymentProcessing;


  -----------------------------------------------------------------------------
  -- NoOutstandingSuspensions
  --  Utility function, returns true when the license passed in has no outstanding suspensions other than the current job.
  -----------------------------------------------------------------------------
    function NoOutstandingSuspensions (
      a_LicenseId                        udt_Id,
      a_CurrentJobId                     udt_Id
    ) return boolean is
      t_SuspendedAccusationCount          number;
    begin
      select count(*)
        into t_SuspendedAccusationCount
        from query.r_ABC_AccusationLicense ral
          join query.j_ABC_Accusation j
            on ral.AccusationJobId = j.JobId
        where ral.LicenseObjectId = a_LicenseId
          and j.DoReactivateLicense = 'Y'
          and j.JobId != a_CurrentJobId
          and rownum <= 1;
      return (t_SuspendedAccusationCount = 0);
    end; -- NoOutstandingSuspensions

  -----------------------------------------------------------------------------
  -- SystemSuspensionActions
  --  Added to Suspend Outcome of SystemProcess in addition to the Copy Details that is already there.
  -- Marks the current line in the date grid as suspended if it exists. Issue 5824 -JP
  -----------------------------------------------------------------------------
  procedure SystemSuspensionActions (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             number := api.pkg_ColumnQuery.NumericValue(a_ProcessId, 'JobId');
    t_SuspendLicenseId                 udt_Id := api.pkg_ColumnQuery.NumericValue(t_JobId, 'SuspendLicenseId');
    t_CurrentSuspensionDateId                 udt_Id := api.pkg_ColumnQuery.NumericValue(t_SuspendLicenseId, 'CurrentSuspensionDateId');
  begin
    if t_CurrentSuspensionDateId is not null then
      api.pkg_Columnupdate.SetValue(t_CurrentSuspensionDateId, 'Suspended', 'Y');
    end if;
  end SystemSuspensionActions;

  -----------------------------------------------------------------------------
  -- SystemReactivationActions
  --  When a suspension ends, determine if we need to reactive the license.
  -- The system should be calling this and a license will not be reactivated if
  -- there are other accusation jobs against this license which have also
  -- suspended the license.
  --
  -- If so, create the Take Reactivation Actions process
  -- Otherwise determine if we should complete the job.
  -----------------------------------------------------------------------------
  procedure SystemReactivationActions (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             number := api.pkg_ColumnQuery.NumericValue(a_ProcessId, 'JobId');
    t_LicenseObjectId                   number := api.pkg_ColumnQuery.Value(t_JobId, 'LicenseObjectId');
    t_SuspendLicenseId                 udt_Id := api.pkg_ColumnQuery.NumericValue(t_JobId, 'SuspendLicenseId');
    t_CurrentSuspensionDateId                 udt_Id := api.pkg_ColumnQuery.NumericValue(t_SuspendLicenseId, 'CurrentSuspensionDateId');
    t_IsLastSuspension                        char(1) := api.pkg_ColumnQuery.Value(t_CurrentSuspensionDateId, 'IsLastSLSuspension');

  begin
    if t_CurrentSuspensionDateId is not null then
      api.pkg_Columnupdate.SetValue(t_CurrentSuspensionDateId, 'Reactivated', 'Y');
    end if;

    if NoOutstandingSuspensions(t_LicenseObjectId, t_JobId) and
        api.pkg_ColumnQuery.Value(t_LicenseObjectId, 'State') = 'Suspended' then
      if api.pkg_ColumnQuery.DateValue(t_LicenseObjectId, 'ExpirationDate') < sysdate then
        api.pkg_ColumnUpdate.SetValue(t_LicenseObjectId, 'State', 'Expired');
      else
        api.pkg_ColumnUpdate.SetValue(t_LicenseObjectId, 'State', 'Active');
      end if;
      if nvl(t_IsLastSuspension, 'Y') = 'Y' then
        api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionActualEndDate', trunc(sysdate));
        extension.pkg_ProcessUpdate.New(t_JobId, 'p_ABC_TakeReactivationActions');
      elsif t_IsLastSuspension = 'N' then
        api.pkg_ColumnUpdate.SetValue(t_JobId, 'DoSuspendLicense', 'Y'); -- license and any secondary licenses will be suspended on system process outcome
        api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionStartDate', api.pkg_Columnquery.DateValue(t_SuspendLicenseId, 'NextSuspensionStartDate'));
        api.pkg_ColumnUpdate.SetValue(t_JobId, 'SuspensionEndDate', api.pkg_Columnquery.DateValue(t_SuspendLicenseId, 'NextSuspensionEndDate'));
      end if;
    else
      JobCompletionCheck(t_JobId); -- done only for directly related license
    end if;

    -- reactivate secondary licenses for which this job is their only suspension
    for i in (select ps.SecondaryLicenseObjectId
              from query.r_ABC_PrimarySecondaryLicense ps
              join query.o_ABC_License sl on sl.ObjectId = ps.SecondaryLicenseObjectId
              where ps.PrimaryLicenseObjectId = t_LicenseObjectId
              and sl.State = 'Suspended') loop
      if NoOutstandingSuspensions(i.SecondaryLicenseObjectId, t_JobId) then
      -- license is no longer suspended, so change state
        if api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'ExpirationDate') < sysdate then
          api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Expired');
        else
          api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Active');
        end if;
      end if;
    end loop;

  end SystemReactivationActions;


  -----------------------------------------------------------------------------
  -- ManualReactivationActions
  --  When a suspension ends manually, determine if we need to reactive the
  -- license.  All accusation jobs against this license will be completed and
  -- the license will be set to 'Active'.
  --
  -- If so, create the Take Reactivation Actions process
  -- Otherwise determine if we should complete the job.
  -----------------------------------------------------------------------------
  procedure ManualReactivationActions (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is
    t_LicenseObjectId                   number := api.pkg_ColumnQuery.Value(a_JobId, 'LicenseObjectId');
    t_ReactivationReason                varchar2(4000);
    t_SuspendedAccusationJobId          udt_Id;

  begin
    t_ReactivationReason := api.pkg_ColumnQuery.Value(a_JobId, 'ReactivationReason');
    if t_ReactivationReason is null then
      api.pkg_Errors.RaiseError(-20000,
        'Before you can reactivate the suspended license, you must enter a value for Reactivation Reason.');
    end if;

    api.pkg_ColumnUpdate.SetValue(a_JobId, 'DoReactivateLicense', 'N');  -- for Nightly Object Creation
    api.pkg_ColumnUpdate.SetValue(a_JobId, 'SuspensionActualEndDate', trunc(sysdate));

    if api.pkg_ColumnQuery.Value(t_LicenseObjectId, 'State') != 'Active' then
        if api.pkg_ColumnQuery.DateValue(t_LicenseObjectId, 'ExpirationDate') <= sysdate then
          api.pkg_ColumnUpdate.SetValue(t_LicenseObjectId, 'State', 'Expired');
        else
          api.pkg_ColumnUpdate.SetValue(t_LicenseObjectId, 'State', 'Active');
        end if;
      extension.pkg_ProcessUpdate.New(a_JobId, 'p_ABC_TakeReactivationActions');
    elsif api.pkg_ColumnQuery.Value(a_JobId, 'ShouldCompleteJob') = 'Y' then
      extension.pkg_ProcessUpdate.NewComplete(a_JobId, 'p_ABC_SystemProcess', 'Complete');
    end if;

    begin
      select j.ObjectId
        into t_SuspendedAccusationJobId
        from query.r_ABC_AccusationLicense ral
          join query.j_ABC_Accusation j
            on ral.AccusationJobId = j.JobId
        where ral.LicenseObjectId = t_LicenseObjectId
          and j.DoReactivateLicense = 'Y'
          and j.JobId != a_JobId
          and rownum <= 1;
    exception
      when no_data_found then
        t_SuspendedAccusationJobId := null;
    end;

    if t_SuspendedAccusationJobId is not null then
      -- reactivate another license
      if api.pkg_ColumnQuery.Value(t_SuspendedAccusationJobId, 'ReactivationReason') is null then
        api.pkg_ColumnUpdate.SetValue(t_SuspendedAccusationJobId, 'ReactivationReason', t_ReactivationReason);
      end if;

      -- this will cause a recursive call of ManualReactivationActions until
      -- there are no more accusation jobs which have suspended this license
      api.pkg_ColumnUpdate.SetValue(t_SuspendedAccusationJobId, 'ReactivateLicense', 'Y');
    end if;

    -- reactivate secondary licenses for which this job is their only suspension (Brad approved this)
    for i in (select ps.SecondaryLicenseObjectId
              from query.r_ABC_PrimarySecondaryLicense ps
              join query.o_ABC_License sl on sl.ObjectId = ps.SecondaryLicenseObjectId
              where ps.PrimaryLicenseObjectId = t_LicenseObjectId
              and sl.State = 'Suspended') loop
      if NoOutstandingSuspensions(i.SecondaryLicenseObjectId, a_JobId) then
        if api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'ExpirationDate') <= sysdate then
          api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Expired');
        else
          api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Active');
        end if;
      end if;
    end loop;

  end ManualReactivationActions;


  -----------------------------------------------------------------------------
  -- CopyPleadingText
  --  Copy the pleading text from the Pleading Type object to the Pleading
  -- object if it doesn't already exist.
  -----------------------------------------------------------------------------
  procedure CopyPleadingText (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_PleadingObjectId                  udt_Id;
    t_PleadingText                      varchar2(4000);
    t_PleadingTypeText                  varchar2(4000);
  begin
    select r.PleadingObjectId,
          api.pkg_ColumnQuery.Value(r.PleadingObjectId, 'PleadingText') PleadingText,
          api.pkg_ColumnQuery.Value(r.PleadingTypeObjectId, 'PleadingText') PleadingTypeText
      into t_PleadingObjectId, t_PleadingText, t_PleadingTypeText
      from query.r_ABC_PleadingPleadingType r
      where r.RelationshipId = a_RelationshipId;

    if t_PleadingText is null then
      api.pkg_ColumnUpdate.SetValue(t_PleadingObjectId, 'PleadingText', t_PleadingTypeText);
    end if;
  end CopyPleadingText;

  -----------------------------------------------------------------------------
  -- CopySuspensionDate
  --  Copy the rels to the SuspensionDate objects from Record and Execute Decision
  -- to Suspend License on constructor of the latter.
  -----------------------------------------------------------------------------
  procedure CopySuspensionDate (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id := api.pkg_ColumnQuery.NumericValue(a_ProcessId, 'JobId');
    t_RecExecDecisionId                 udt_Id := api.pkg_ColumnQuery.NumericValue(t_JobId, 'RecExecDecisionId');
    t_SuspensionDateIds                 udt_IdList := extension.pkg_ObjectQuery.RelatedObjects(t_RecExecDecisionId, 'SuspensionDate');
    t_SuspensionDateEP                  udt_Id := api.pkg_configquery.EndPointIdForName('p_ABC_SuspendLicense', 'SuspensionDate');
    t_RelId                             udt_Id;
  begin
    for i in 1..t_SuspensionDateIds.count loop
      t_RelId := api.pkg_RelationshipUpdate.New(t_SuspensionDateEP, a_ProcessId, t_SuspensionDateIds(i));
    end loop;

  end CopySuspensionDate;

end pkg_ABC_Accusation;

/

