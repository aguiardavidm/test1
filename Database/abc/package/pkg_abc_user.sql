create or replace package pkg_ABC_User is

  -- Author  : BRUCE.JAKEWAY
  -- Created : 2011 Apr 28 09:02:36
  -- Purpose : User functions

  -- Public type declarations
  subtype udt_Id is api.pkg_Definition.udt_Id;

  -----------------------------------------------------------------------------
  -- RoleAccessGroupAdded
  --  If an access group object is added to a role, ensure that the access
  -- group itself is added to the users who have that role.
  -----------------------------------------------------------------------------
  procedure RoleAccessGroupAdded (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- RoleAccessGroupDeleted
  --  If an access group object is removed from a role, ensure that the access
  -- group itself is removed to the users who have that role, if it doesn't
  -- exist in another role or in the user's additional access groups.
  -----------------------------------------------------------------------------
  procedure RoleAccessGroupDeleted (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- UserAccessGroupAdded
  --  Once an access group has been added to the Additional Access Groups, it
  -- must also be added to the table AccessGroupUsers, if not already there.
  -----------------------------------------------------------------------------
  procedure UserAccessGroupAdded (
    a_RelationshipId           udt_Id,
    a_AsOfDate                 date
  );

  -----------------------------------------------------------------------------
  -- UserAccessGroupDeleted
  --  Once an access group has been deleted from the Additional Access Groups,
  -- it must also be deleted from the table AccessGroupUsers, if not used in
  -- roles attached to the user.
  -----------------------------------------------------------------------------
  procedure UserAccessGroupDeleted (
    a_RelationshipId           udt_Id,
    a_AsOfDate                 date
  );

  -----------------------------------------------------------------------------
  -- UserRoleAdded
  --  Once a role has been added, its access groups need to be added to the
  -- table AccessGroupUsers, if not already there.
  -----------------------------------------------------------------------------
  procedure UserRoleAdded (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- UserRoleDeleted
  --  Once a role has been deleted, its access groups must also be deleted from
  -- the table AccessGroupUsers, if not used in other roles or Additional
  -- Access Groups attached to the user.
  -----------------------------------------------------------------------------
  procedure UserRoleDeleted (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_User;

 
/

create or replace package body pkg_ABC_User is


  -----------------------------------------------------------------------------
  -- RoleAccessGroupAdded
  --  If an access group object is added to a role, ensure that the access
  -- group itself is added to the users who have that role.
  -----------------------------------------------------------------------------
  procedure RoleAccessGroupAdded (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_AccessGroupId                     udt_Id;
    t_RoleObjectId                      udt_Id;

    cursor c_Users is
      select rur.UserId
        from query.r_UserRole rur
        where rur.RoleObjectId = t_RoleObjectId
          and not exists (
            select 1
              from api.AccessGroupUsers agu
              where agu.AccessGroupId = t_AccessGroupId
                and agu.UserId = rur.UserId);

  begin
    begin
      select r.RoleObjectId, api.pkg_ColumnQuery.Value(r.AccessGroupObjectId, 'AccessGroupId') AccessGroupId
        into t_RoleObjectId, t_AccessGroupId
        from query.r_RoleAccessGroup r
        where r.RelationshipId = a_RelationshipId;
    exception
      when no_data_found then
        t_RoleObjectId := null;
    end;

    if t_RoleObjectId is not null and t_AccessGroupId is not null then
      for u in c_Users loop
        api.pkg_UserUpdate.AddToAccessGroup(u.UserId, t_AccessGroupId);
      end loop;
    end if;
  end RoleAccessGroupAdded;


  -----------------------------------------------------------------------------
  -- RoleAccessGroupDeleted
  --  If an access group object is removed from a role, ensure that the access
  -- group itself is removed to the users who have that role, if it doesn't
  -- exist in another role or in the user's additional access groups.
  -----------------------------------------------------------------------------
  procedure RoleAccessGroupDeleted (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_AccessGroupId                     udt_Id;
    t_AccessGroupObjectId               udt_Id;
    t_RoleObjectId                      udt_Id;

    cursor c_Users is
      select rur.UserId
        from query.r_UserRole rur
          join api.AccessGroupUsers agu
            on rur.UserId = agu.UserId
        where rur.RoleObjectId = t_RoleObjectId
          and agu.AccessGroupId = t_AccessGroupId
          and not exists (
            select 1
              from query.r_UserAccessGroup ruag
                where ruag.UserId = rur.UserId
                  and ruag.AccessGroupObjectId = t_AccessGroupObjectId)
          and not exists (
            select 1
              from query.r_UserRole rur1
                join query.r_RoleAccessGroup rrag
                  on rur1.RoleObjectId = rrag.RoleObjectId
              where rur1.UserId = rur.UserId
                and rur1.RoleObjectId <> rur.RoleObjectId
                and rrag.AccessGroupObjectId = t_AccessGroupObjectId);

  begin
    begin
      select rrag.RoleObjectId, rrag.AccessGroupObjectId, api.pkg_ColumnQuery.Value(rrag.AccessGroupObjectId, 'AccessGroupId') AccessGroupId
        into t_RoleObjectId, t_AccessGroupObjectId, t_AccessGroupId
        from query.r_RoleAccessGroup rrag
        where rrag.RelationshipId = a_RelationshipId;
    exception
      when no_data_found then
        t_RoleObjectId := null;
    end;

    if t_RoleObjectId is not null and t_AccessGroupId is not null then
      for u in c_Users loop
        api.pkg_UserUpdate.RemoveFromAccessGroup(u.UserId, t_AccessGroupId);
      end loop;
    end if;
  end RoleAccessGroupDeleted;


  -----------------------------------------------------------------------------
  -- UserAccessGroupAdded
  --  Once an access group has been added to the Additional Access Groups, it
  -- must also be added to the table AccessGroupUsers, if not already there.
  -----------------------------------------------------------------------------
  procedure UserAccessGroupAdded (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_UserAccessGroupEP                 udt_Id :=
        api.pkg_ConfigQuery.EndPointIdForName('u_Users', 'AccessGroup');

    cursor c_UserAccessGroups is
      select r.FromObjectId UserId,
             api.pkg_ColumnQuery.NumericValue(r.ToObjectId, 'AccessGroupId') AccessGroupId
        from api.relationships r
        where r.RelationshipId = a_RelationshipId
          and r.EndPointId = t_UserAccessGroupEP
          and not exists (
            select 1
              from api.AccessGroupUsers agu
              where agu.UserId = r.FromObjectId
                and agu.AccessGroupId = api.pkg_ColumnQuery.NumericValue(r.ToObjectId, 'AccessGroupId'));

  begin
    -- there should be at most one access group to add
    for c in c_UserAccessGroups loop
      api.pkg_UserUpdate.AddToAccessGroup(c.UserId, c.AccessGroupId);
    end loop;
  end UserAccessGroupAdded;


  -----------------------------------------------------------------------------
  -- UserAccessGroupDeleted
  --  Once an access group has been deleted from the Additional Access Groups,
  -- it must also be deleted from the table AccessGroupUsers, if not used in
  -- roles attached to the user.
  -----------------------------------------------------------------------------
  procedure UserAccessGroupDeleted (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_UserAccessGroupEP                 udt_Id :=
        api.pkg_ConfigQuery.EndPointIdForName('u_Users', 'AccessGroup');

    cursor c_UserAccessGroups is
      select r.FromObjectId UserId,
             api.pkg_ColumnQuery.NumericValue(r.ToObjectId, 'AccessGroupId') AccessGroupId
        from api.relationships r
        where r.RelationshipId = a_RelationshipId
          and r.EndPointId = t_UserAccessGroupEP
          and not exists (
            select 1
              from query.r_UserRole rur
                join query.r_RoleAccessGroup rrag
                  on rur.RoleObjectId = rrag.RoleObjectId
              where rur.UserId = r.FromObjectId
                and rrag.AccessGroupObjectId = r.ToObjectId);

  begin
    -- there should be at most one access group to remove
    for c in c_UserAccessGroups loop
      api.pkg_UserUpdate.RemoveFromAccessGroup(c.UserId, c.AccessGroupId);
    end loop;
  end UserAccessGroupDeleted;


  -----------------------------------------------------------------------------
  -- UserRoleAdded
  --  Once a role has been added, its access groups need to be added to the
  -- table AccessGroupUsers, if not already there.
  -----------------------------------------------------------------------------
  procedure UserRoleAdded (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_RoleEP                            udt_Id :=
        api.pkg_ConfigQuery.EndPointIdForName('u_Users', 'Role');

    cursor c_UserAccessGroups is
      select r.FromObjectId UserId,
             api.pkg_ColumnQuery.NumericValue(rrag.AccessGroupObjectId, 'AccessGroupId') AccessGroupId
        from api.relationships r
          join query.r_RoleAccessGroup rrag
            on rrag.RoleObjectId = r.ToObjectId
        where r.RelationshipId = a_RelationshipId
          and r.EndPointId = t_RoleEP
          and not exists (
            select 1
              from api.AccessGroupUsers agu
              where agu.UserId = r.FromObjectId
                and agu.AccessGroupId = api.pkg_ColumnQuery.NumericValue(rrag.AccessGroupObjectId, 'AccessGroupId'));

  begin
    for c in c_UserAccessGroups loop
      api.pkg_UserUpdate.AddToAccessGroup(c.UserId, c.AccessGroupId);
    end loop;
  end UserRoleAdded;


  -----------------------------------------------------------------------------
  -- UserRoleDeleted
  --  Once a role has been deleted, its access groups must also be deleted from
  -- the table AccessGroupUsers, if not used in other roles or Additional
  -- Access Groups attached to the user.
  -----------------------------------------------------------------------------
  procedure UserRoleDeleted (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_RoleEP                            udt_Id :=
        api.pkg_ConfigQuery.EndPointIdForName('u_Users', 'Role');

    cursor c_UserAccessGroups is
      select r.FromObjectId UserId,
             api.pkg_ColumnQuery.NumericValue(rrag.AccessGroupObjectId, 'AccessGroupId') AccessGroupId
        from api.relationships r
          join query.r_RoleAccessGroup rrag
            on rrag.RoleObjectId = r.ToObjectId
        where r.RelationshipId = a_RelationshipId
          and r.EndPointId = t_RoleEP
          and not exists (
            select 1
              from query.r_UserRole rur
                join query.r_RoleAccessGroup rrag2
                  on rrag2.RoleObjectId = rur.RoleObjectId
              where rur.UserId = r.FromObjectId
                and rur.RoleObjectId <> r.ToObjectId
                and rrag2.AccessGroupObjectId = rrag.AccessGroupObjectId
            union all
            select 1
              from query.r_UserAccessGroup ruag
              where ruag.UserId = r.FromObjectId
                and ruag.AccessGroupObjectId = rrag.AccessGroupObjectId);

  begin
    for c in c_UserAccessGroups loop
      api.pkg_UserUpdate.RemoveFromAccessGroup(c.UserId, c.AccessGroupId);
    end loop;
  end UserRoleDeleted;

end pkg_ABC_User;

/

