create or replace package pkg_ABC_Product is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  -----------------------------------------------------------------------------
  -- PostVerifyWrapper
  -- Runs on Post-Verify of the ABC Amendment Object.
  -----------------------------------------------------------------------------
  procedure MakeHistorical (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  --  ProductRevocation
  --  Runs as procedure on constructor of rel r_ABC_ProductRevocation
  --  handles product revocation
  -----------------------------------------------------------------------------
  procedure ProductRevocation(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

 /*--------------------------------------------------------------------------
  * SetFlagForChangedNonrenewal
  *   Sets the flag when PRNonrenewalNotice date is changed
  ------------------------------------------------------------------------*/
  procedure SetFlagForChangedNoticeDate(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

 /*--------------------------------------------------------------------------
  * ScheduleNoticeDateUpdate
  *   Schedules procedure that will update nonrenewal dates to the
  *   Process Server.
  ------------------------------------------------------------------------*/
  procedure ScheduleNoticeDateUpdate(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

 /*--------------------------------------------------------------------------
  * UpdateNoticeDate -PRIVATE-
  *   Updates NonRenewal date for all current products, called only by
  *   ScheduleNoticeDateUpdate procedure
  ------------------------------------------------------------------------*/
  procedure UpdateNoticeDate(
    a_AsOfDate               date,
    a_ObjectId               udt_Id
  );

end pkg_ABC_Product;
/

grant execute
on abc.pkg_ABC_Product
to posseextensions;

create or replace package body pkg_ABC_Product is

  -----------------------------------------------------------------------------
  -- MakeHistorical
  --  Makes Distributor of Product historical (License and TAP Permit)
  -----------------------------------------------------------------------------
  procedure MakeHistorical (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_ProductId           udt_id;
    t_RelationshipId      udt_id;
    t_EndpointId          udt_id;
    t_ObjectDefName       varchar2(500) := api.pkg_columnquery.Value(a_ObjectId, 'ObjectDefName');

  begin
     t_ProductId := api.pkg_columnquery.NumericValue(a_ObjectId,'ProductObjectId');
     if t_ProductId is not null then
        if t_ObjectDefName = 'o_ABC_License' then
           t_EndPointId := api.pkg_configquery.EndPointIdForName ('o_ABC_Product', 'HistoricProducts');
        else
           t_EndPointId := api.pkg_configquery.EndPointIdForName ('o_ABC_Product', 'HistoricTAPDistributor');
        end if;
        begin
           select pd.RelationshipId
             into t_RelationshipId
             from query.r_ABCProduct_Distributor pd
            where pd.ProductId = t_ProductId
              and pd.LicenseId = a_ObjectId
           union
           select ptd.RelationshipId
             from query.r_ABC_ProductTAPDistributor ptd
            where ptd.ProductId = t_ProductId
              and ptd.PermitId = a_ObjectId;
        exception
           when no_data_found then
              t_RelationshipId := 0;
        end;

-- Remove current relationship
        if t_RelationshipId > 0 then
           api.pkg_relationshipupdate.Remove(t_RelationshipId);
        end if;
-- Create new historical relationship
        t_RelationshipId := api.pkg_relationshipupdate.New(t_EndPointId, t_ProductId, a_ObjectId, sysdate);
        api.pkg_columnupdate.RemoveValue(a_ObjectId,'ProductObjectId');
     end if;

  end MakeHistorical;

  -----------------------------------------------------------------------------
  --  ProductRevocation
  --  Runs as procedure on constructor of rel r_ABC_ProductRevocation
  --  handles product revocation
  -----------------------------------------------------------------------------
  procedure ProductRevocation(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is

    t_ProductObjectId        udt_Id;
    t_RevocationObjectId     udt_Id;
    t_RevocationType         varchar2(100);

  begin
     select r.ProductObjectId
          , r.RevocationObjectId
       into t_ProductObjectId
          , t_RevocationObjectId
       from query.r_ABC_ProductRevocation r
      where r.RelationshipId = a_ObjectId;

     t_RevocationType := api.pkg_columnquery.value(t_RevocationObjectId, 'RevocationType');

    -- set State of the Product to Revoked or Active
     if t_RevocationType = 'Reactivation' then
        api.pkg_columnupdate.setvalue(t_ProductObjectId,'State','Current');
     elsif t_RevocationType = 'Revocation' then
        api.pkg_columnupdate.setvalue(t_ProductObjectId,'State','Revoked');
     end if;

    -- set details on revocation object
     api.pkg_columnupdate.SetValue(t_RevocationObjectId,'RevokedByUserName',
         api.pkg_columnquery.value(t_RevocationObjectId, 'UserFormattedName2'));

     api.pkg_columnupdate.SetValue(t_RevocationObjectId,'RevokedDate',
         api.pkg_columnquery.value(t_RevocationObjectId, 'CurrentDate'));

  end ProductRevocation;

 /*--------------------------------------------------------------------------
  * SetFlagForChangedNoticeDate
  *   Sets the flag when PRNonrenewalNotice date is changed
  ------------------------------------------------------------------------*/
  procedure SetFlagForChangedNoticeDate(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is

  begin
    api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'ScheduleNoticeDateUpdate', 'Y');
  end SetFlagForChangedNoticeDate;

 /*--------------------------------------------------------------------------
  * ScheduleNoticeDateUpdate
  *   Schedules procedure that will update nonrenewal dates to the
  *   Process Server.
  ------------------------------------------------------------------------*/
  procedure ScheduleNoticeDateUpdate(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_ProductExpDate         date;
    t_ScheduledTime          date := to_date(to_char(trunc(sysdate), 'DD/MM/YYYY')
        || ' 23:00', 'DD/MM/YYYY HH24:MI');
    t_Arguments              varchar2(4000);
    t_ScheduleId             number(9);
  begin
    -- Checks if the procedure needs to be scheduled and
    -- if the procedure has already been scheduled
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'ScheduleNoticeDateUpdate') = 'Y'
        and api.pkg_ColumnQuery.Value(a_Objectid, 'AlreadyScheduledUpdate') = 'N' then
      api.Pkg_Processserver.AddArgument(t_Arguments, sysdate);
      api.pkg_ProcessServer.AddArgument(t_Arguments, a_ObjectId);

      t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure(
          a_Name => 'ABC.pkg_abc_Product.UpdateNoticeDate',
          a_Description => 'Update changed Nonrenewal Notice Date for all current products',
          a_Arguments => t_Arguments,
          a_Date => t_ScheduledTime);
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'AlreadyScheduledUpdate', 'Y');
    end if;

  end ScheduleNoticeDateUpdate;

 /*--------------------------------------------------------------------------
  * UpdateNoticeDate -PRIVATE-
  *   Updates NonRenewal date for all current products, called only by
  *   ScheduleNoticeDateUpdate procedure
  ------------------------------------------------------------------------*/
  procedure UpdateNoticeDate(
    a_AsOfDate               date,
    a_ObjectId               udt_Id
  ) is
    t_CurrentProductList     udt_IdList;
    t_ProductExpDate         date;
    t_NoticeDate             date;
  begin
    select prod.ObjectId
    bulk collect into t_CurrentProductList
    from query.o_abc_product prod
    where prod.State = 'Current';

    for i in 1..t_CurrentProductList.count loop
      t_ProductExpDate := api.pkg_ColumnQuery.DateValue(t_CurrentProductList(i), 'ExpirationDate');
      t_NoticeDate := abc.pkg_ABC_ProductRegistration.CreateNonRenewalNoticeDate(t_ProductExpDate);
      api.pkg_ColumnUpdate.SetValue(t_CurrentProductList(i), 'NonRenewalNoticeDate', t_NoticeDate);
    end loop;

    -- Resets flags so that the update procedure can be scheduled again when needed
    api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'ScheduleNoticeDateUpdate', 'N');
    api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'AlreadyScheduledUpdate', 'N');
  end UpdateNoticeDate;

end pkg_ABC_Product;
/

