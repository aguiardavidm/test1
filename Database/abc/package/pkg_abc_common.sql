create or replace package pkg_ABC_Common is

  -- Author  : Ali.Akadjal
  -- Created : 2011 May 13 14:47:55
  -- Purpose : Procedures for Common ABC Functions

  -- Public type declarations
  subtype udt_Id is api.pkg_Definition.udt_Id;


  -- Function and procedure implementations

  -----------------------------------------------------------------------------
  -- JobIdForProcess()
  -----------------------------------------------------------------------------
  function JobIdForProcess(a_ProcessId pls_integer) return pls_integer;

  -----------------------------------------------------------------------------
  -- RemoveColumnValue()
  -----------------------------------------------------------------------------
  procedure RemoveColumnValue(a_ObjectId   pls_integer,
                              a_AsOfDate   date,
                              a_ColumnName varchar2);
  
  
  /*---------------------------------------------------------------------------
   * RemoveRelationship() -- PUBLIC
   *   Removes a relationship
   *-------------------------------------------------------------------------*/
  procedure RemoveRelationship (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  /*---------------------------------------------------------------------------
   * UpdateColumnValue() -- PUBLIC
   *   Updates a job column for a processid
   *-------------------------------------------------------------------------*/
  procedure UpdateColumnValue(
    a_ObjectId   pls_integer,
    a_AsOfDate   date,
    a_ColumnName varchar2
  );
  
  -----------------------------------------------------------------------------
  -- EncryptSSNField
  -- Is called from the SSN/TIN/EIN field
  -----------------------------------------------------------------------------
  procedure EncryptSSNField (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date,
    a_FieldName                             varchar2
  );

  -----------------------------------------------------------------------------
  -- EncryptDateField
  -- Is called from the (Birth)Date field
  -----------------------------------------------------------------------------
  procedure EncryptDateField (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date,
    a_FieldName                             varchar2
  );

  -----------------------------------------------------------------------------
  -- ReGenerateFee
  -- Is called from the Miscellaneous Transaction Job
  -----------------------------------------------------------------------------
  procedure ReGenerateFee (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- SetReGenerateFee
  -- Is called from the Miscellaneous Transaction Job
  -----------------------------------------------------------------------------
  procedure SetReGenerateFees (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- CheckDocumentType
  -- Is called from post-verify of d_ElectronicDocument
  -----------------------------------------------------------------------------
  procedure CheckDocumentType (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- Verify Questions
  -- Is called from post-verify of o_ABC_RenewalQuestion
  -----------------------------------------------------------------------------
  procedure VerifyQuestions (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------------
  -- UniqueColumn
  -- Is called to error with the given error message if the given column is not unique
  -----------------------------------------------------------------------------------
  procedure UniqueColumn(
    a_ObjectId                             udt_Id,
    a_AsOfDate                             date,
     a_ColumnName                           varchar2,
     a_ErrorMessage                         varchar2,
     a_CheckBox                             varchar2

  );

  -----------------------------------------------------------------------------
  -- Update dup_IsWithinThreshold
  -- Is called from process server on anual basis
  -----------------------------------------------------------------------------
  procedure dup_IsWithinThreshold;

  -----------------------------------------------------------------------------
  -- CheckPaymentReceipt
  -- Is called from Payment-Receipt relationship
  -----------------------------------------------------------------------------
  procedure CheckPaymentReceipt (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- SetCommentEdited
  -- Is called from d_ElectronicDocument when CommentText is changed
  -----------------------------------------------------------------------------
  procedure SetCommentEdited (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- EncryptDLField
  -- Is called from the Driver's License field
  -----------------------------------------------------------------------------
  procedure EncryptDLField (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date,
    a_FieldName                             varchar2
  );

  -----------------------------------------------------------------------------
  -- CheckEmail
  -- Is called from various email details. Check for valid format.
  -----------------------------------------------------------------------------
  procedure CheckEmail (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date,
    a_FieldName                             varchar2
  );

  -----------------------------------------------------------------------------
  -- RemoveDupReceipts
  -- Is called daily from Process Server to remove duplicate receipts
  -----------------------------------------------------------------------------
  procedure RemoveDupReceipts;

  /*---------------------------------------------------------------------------
   * CheckPublicAccessGroups()
   *   Is called monthly from Process Server to check available Access Groups
   *-------------------------------------------------------------------------*/
  procedure CheckPublicAccessGroups;

  /*---------------------------------------------------------------------------
   * UnsetAvailableOnline
   *   Referenced on Deletion of rel from ElectronicDocument to SampleForm.
   *   Resets the AvailableOnline flag when the Sample Form is deleted.
   *-------------------------------------------------------------------------*/
  procedure UnsetAvailableOnline (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  /*---------------------------------------------------------------------------
   * CheckMandatoryDocType
   *   Check if job type is set when marking document type mandatory.
   *-------------------------------------------------------------------------*/
  procedure CheckMandatoryDocType (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CheckDataWarehouseFailure()
   *   Check if Data Warehouse had a sync failure
   *-------------------------------------------------------------------------*/
  procedure CheckDataWarehouseFailure;

end pkg_ABC_Common;
/
create or replace package body pkg_ABC_Common is
  g_ShouldEncryptSSN varchar2(1) := 'Y';
  g_ShouldEncryptDL  varchar2(1) := 'Y';
  -- Function and procedure implementations

  -----------------------------------------------------------------------------
  -- JobIdForProcess()
  -----------------------------------------------------------------------------
  function JobIdForProcess(a_ProcessId pls_integer) return pls_integer is
    t_JobId pls_integer;
  begin
    select JobId
      into t_JobId
      from api.Processes
     where ProcessId = a_ProcessId;

    return t_JobId;
  exception
    when no_data_found then
      return null;
  end JobIdForProcess;

  -----------------------------------------------------------------------------
  -- RemoveColumnValue()
  -----------------------------------------------------------------------------
  procedure RemoveColumnValue(a_ObjectId   pls_integer,
                              a_AsOfDate   date,
                              a_ColumnName varchar2) is
    t_ObjectId pls_integer;
  begin
    t_ObjectId := nvl(JobIdForProcess(a_ObjectId), a_ObjectId);
    api.pkg_ColumnUpdate.RemoveValue(t_ObjectId, a_ColumnName);
  end RemoveColumnValue;
  
  /*---------------------------------------------------------------------------
   * RemoveRelationship() -- PUBLIC
   *   Removes a relationship
   *-------------------------------------------------------------------------*/
  procedure RemoveRelationship (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
  begin
    api.pkg_relationshipupdate.Remove(a_ObjectId);
  end RemoveRelationship;
  
  /*---------------------------------------------------------------------------
   * UpdateColumnValue() -- PUBLIC
   *   Removes a relationship
   *-------------------------------------------------------------------------*/
  procedure UpdateColumnValue(a_ObjectId   pls_integer,
                              a_AsOfDate   date,
                              a_ColumnName varchar2) is
    t_ObjectId pls_integer;
  begin
    t_ObjectId := nvl(JobIdForProcess(a_ObjectId), a_ObjectId);
    api.pkg_ColumnUpdate.SetValue(t_ObjectId, a_ColumnName);
  end UpdateColumnValue;

  -----------------------------------------------------------------------------
  -- EncryptSSNField
  -- Is called from the SSN/TIN/EIN field
  -----------------------------------------------------------------------------
  procedure EncryptSSNField(
    a_ObjectId           udt_Id,
    a_AsOfDate           date,
    a_FieldName          varchar2
    ) is
    t_OriginalValue      varchar2(4000);
    t_EncryptedValue     varchar2(4000);
    t_Counter            number := 0;
    t_Asterisks          varchar2(4000);
    t_SavedPart          varchar2(4000);

  begin
    if g_ShouldEncryptSSN = 'Y' then
      if a_FieldName is not null then

        t_OriginalValue := api.pkg_columnquery.Value(a_ObjectId, a_FieldName);
        --figure out what the "asterisks" part should be
        --save the last 2 characters
        t_SavedPart := substr(t_OriginalValue, length(t_OriginalValue) - 1);

        while t_Counter < (length(t_OriginalValue) - 2) loop
          t_Asterisks := t_Asterisks || '*';
          t_Counter := t_Counter + 1;
        end loop;
        --if the entered SSN is invalid, raise an error
        if (regexp_replace(t_OriginalValue, '[0-9]*')) is not null
          or length (regexp_replace(t_OriginalValue, '[^0-9]*')) != 9 then
          api.pkg_errors.RaiseError(-20000, 'You must enter a 9 digit number for SSN/TIN/EIN.' );
        end if;

        t_EncryptedValue := abc.pkg_abc_encryption.Encrypt(t_OriginalValue);
        api.pkg_columnupdate.SetValue(a_ObjectId, a_FieldName || 'Encrypted', t_EncryptedValue);

        --replace existing field with asterisks, except for the last 2 characters
        g_ShouldEncryptSSN := 'N';
        api.pkg_columnupdate.SetValue(a_ObjectId, a_FieldName, t_Asterisks || t_SavedPart);

      end if;
    end if;

  end EncryptSSNField;

  -----------------------------------------------------------------------------
  -- EncryptDateField
  -- Is called from the (Birth)Date field
  -----------------------------------------------------------------------------
  procedure EncryptDateField(
    a_ObjectId           udt_Id,
    a_AsOfDate           date,
    a_FieldName          varchar2
    ) is
    t_OriginalValue      varchar2(4000);
    t_EncryptedValue     varchar2(4000);

  begin
    if a_FieldName is not null then

      t_OriginalValue := api.pkg_columnquery.Value(a_ObjectId, a_FieldName);
      --if the asterisks have already been fully set, then just return
      if t_OriginalValue is null then
        return;
      end if;

      --if the user only partially replaced the asterisks with some numbers, raise an error
      if instr(substr(t_OriginalValue, 0, length(t_OriginalValue)), '*') > 0 then
        api.pkg_errors.RaiseError(-20000, 'You must replace all the asterisks in the field.' );
      end if;

      --encrypt the date
      t_EncryptedValue := abc.pkg_abc_encryption.Encrypt(t_OriginalValue);
      api.pkg_columnupdate.SetValue(a_ObjectId, a_FieldName || 'Encrypted', t_EncryptedValue);

      --replace existing field with asterisks
      api.pkg_columnupdate.RemoveValue(a_ObjectId, a_FieldName);

    end if;

  end EncryptDateField;

  -----------------------------------------------------------------------------
  -- ReGenerateFee
  -- Is called from the Miscellaneous Transaction Job
  -----------------------------------------------------------------------------
  procedure ReGenerateFee (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
    t_PaneName                              varchar2(4000) := api.pkg_columnquery.Value(a_ObjectId,'DraftPaneName');
  begin
    if t_PaneName is null then
       null;
    else
      extension.pkg_Fees.ReGenerateFees(a_ObjectId, sysdate);
    end if;
  end ReGenerateFee;

  -----------------------------------------------------------------------------
  -- SetReGenerateFee
  -- Is called from the Miscellaneous Transaction Job for LE Relationship
  -----------------------------------------------------------------------------
  procedure SetReGenerateFees (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
    t_GLAccountCode                         varchar2(4000) := api.pkg_columnquery.Value(a_ObjectId,'GLAccountCode');
  begin
    if t_GLAccountCode is not null then
      extension.pkg_Fees.ReGenerateFees(a_ObjectId, sysdate);
    end if;
  end SetReGenerateFees;

  -----------------------------------------------------------------------------
  -- CheckDocumentType
  -- Is called from post-verify of d_ElectronicDocument
  -----------------------------------------------------------------------------
  procedure CheckDocumentType (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
    t_RelObjectId                           udt_Id;
  begin

    for c in (select o.objectid
                from query.d_Electronicdocument e
                join api.relationships r
                  on r.ToObjectId = e.ObjectId
                join api.objects o
                  on o.objectid = r.FromObjectId
               where e.objectid = a_ObjectId
               and   not exists (select r1.ElectronicDocId
                                 from   query.r_NewAppAdditionalInfoDocs r1
                                 where  r1.ElectronicDocId = a_ObjectId
                                 union
                                 select r2.ElectronicDocId
                                 from   query.r_Amendmentadditionalinfodocs r2
                                 where  r2.ElectronicDocId = a_ObjectId
                                )
              )loop
      if api.pkg_columnQuery.VALUE (a_ObjectId,'DocumentType') is null
         and api.pkg_columnquery.value(c.ObjectId, 'ObjectDefName') not in ('p_ABC_SubmitResolution', 'j_ABC_CPLSubmissions', 'j_ABC_BatchRenewalNotification', 'o_ABC_DocumentType', 'p_ABC_ReceiptNewInformation', 'p_ABC_PoliceReview', 'p_ABC_MunicipalityReview') then
         api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Document Type.');
      end if;

    end loop;
  end CheckDocumentType;

  /*---------------------------------------------------------------------------
   * VerifyQuestions() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure VerifyQuestions (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_AllOrSomeLicTypes                 varchar2(100);
    t_AmendmentTypeId                   udt_Id;
    t_AmendTypeRelDefId                 udt_id;
    t_DocTypeObjectId                   udt_id;
    t_JobTypeRelDefId                   udt_id;
    t_LicenseTypeAmendId                udt_Id;    
    t_LicenseTypeNewId                  udt_Id;
    t_LicenseTypeRenewId                udt_Id;
    t_LicTypeMuniRelDefId               udt_Id;
    t_LicTypePoliceRelDefId             udt_Id;
    t_LicenseTypes                      varchar2(100);
    t_LicTypeAppRelDefId                udt_id;
    t_LicTypeRenewRelDefId              udt_id;
    t_NonAccResponseType                varchar2(100);
    t_PermitTypeAppRelDefId             udt_id;    
    t_PermitTypeId                      udt_Id;
    t_PermitTypeRenewId                 udt_Id;
    t_PermitTypeRenewRelDefId           udt_id;
    t_PermitTypeMuniRelDefId            udt_id;
    t_PermitTypePoliceRelDefId          udt_id;
    t_PetitionAmendTypeRelDefId         udt_Id;
    t_PetitionTypeRelDefId              udt_Id;
    t_PrevValue                         varchar2(10) := '-1';
    t_ProdAmendTypeRelDefId             udt_id;
    t_RelObjectId                       udt_Id;
    t_RelTypeId                         udt_id;
    t_ToObjectId                        udt_id;
  begin

    -- set defaults
    t_AmendTypeRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_AmendTypeQuestion');
    t_AllOrSomeLicTypes := api.pkg_columnquery.Value (a_ObjectId, 'AllOrSomeLicTypes');
    t_DocTypeObjectId := api.pkg_columnquery.Value (a_ObjectId, 'DocumentTypeObjectId');
    t_JobTypeRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_JobTypeQuestion');
    t_LicenseTypes := api.pkg_columnquery.Value (a_ObjectId, 'LicenseTypes');
    t_LicTypeAppRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_LicTypeAppQuestion');
    t_LicTypeRenewRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_LicTypeRenewQuestion');
    t_LicTypeMuniRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_LicTypeMuniQuestion');
    t_LicTypePoliceRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_LicTypePoliceQuestion');
    t_NonAccResponseType := api.pkg_columnquery.Value (a_ObjectId, 'NonacceptableResponseType');
    t_PermitTypeAppRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitTypeAppQuestion');
    t_PermitTypeRenewRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitTypeRenewQuestion');
    t_PermitTypeMuniRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitTypeMuniQuestion');
    t_PermitTypePoliceRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_PermitTypePoliceQuestion');
    t_PetitionAmendTypeRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_PetitionAmndTypeQuestion');
    t_PetitionTypeRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_PetitionTypeQuestion');
    t_ProdAmendTypeRelDefId := api.pkg_configquery.ObjectDefIdForName('r_ABC_ProductAmendTypeQuestion');

    -- Check for documents required and entered
    if t_NonAccResponseType = 'Document' then
      if t_DocTypeObjectId is null then
        api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Document Type.');
      end if;
    end if;

    -- If "Some License Types" is selected, ensure at least 1 License Type is selected.
    if t_AllOrSomeLicTypes = 'Some' then
      if t_LicenseTypes is null then
        api.pkg_errors.RaiseError(-20000, 'Before you go on, you must select at lease one License Type, or click ''All''.');
      end if;
    end if;

    -- make sure that a group of questions for a specific license type, permit type, permit renewal,
    -- amendment type or product amendment type do not have duplicate sort orders
    select
      r.ToObjectId,
      r.RelationshipDefId
    into 
      t_ToObjectId,
      t_RelTypeId
    from api.relationships r
    where r.FromObjectId = a_ObjectId
    and r.RelationshipDefId in (
        t_LicTypeAppRelDefId,
        t_LicTypeRenewRelDefId,
        t_LicTypeMuniRelDefId,
        t_LicTypePoliceRelDefId,
        t_PermitTypeAppRelDefId,
        t_PermitTypeRenewRelDefId,
        t_PermitTypeMuniRelDefId,
        t_PermitTypePoliceRelDefId,
        t_AmendTypeRelDefId,
        t_ProdAmendTypeRelDefId,
        t_JobTypeRelDefId,
        t_PetitionAmendTypeRelDefId,
        t_PetitionTypeRelDefId);

    if t_RelTypeId = t_AmendTypeRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.questionid, 'SORTORDER') SORTORDER
          from query.r_abc_amendtypequestion a
          where a.AmendmentTypeId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

    if t_RelTypeId = t_PermitTypeAppRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.questionid, 'SORTORDER') SORTORDER
          from query.r_abc_permittypeappquestion a
          where a.PermitTypeId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

    if t_RelTypeId = t_PermitTypeRenewRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.questionid, 'SORTORDER') SORTORDER
          from query.r_abc_permittyperenewquestion a
          where a.PermitTypeId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

    if t_RelTypeId = t_LicTypeAppRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.questionid, 'SORTORDER') SORTORDER
          from query.r_abc_lictypeappquestion a
          where a.LicenseTypeId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

    if t_RelTypeId = t_LicTypeRenewRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.questionid, 'SORTORDER') SORTORDER
          from query.r_abc_lictyperenewquestion a
          where a.LicenseTypeId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

    if t_RelTypeId = t_ProdAmendTypeRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.questionid, 'SORTORDER') SORTORDER
          from query.r_abc_productamendtypequestion a
          where a.ProductAmendmentTypeId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

    if t_RelTypeId = t_JobTypeRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.questionid, 'SORTORDER') SORTORDER
          from query.r_abc_jobtypequestion a
          where a.JobTypeId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

    if t_RelTypeId = t_PetitionAmendTypeRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.QuestionId, 'SORTORDER') SORTORDER
          from query.r_ABC_PetitionAmndTypeQuestion a
          where a.PetitionAmendTypeObjectId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

    if t_RelTypeId = t_PetitionTypeRelDefId then
      for i in (
          select api.pkg_columnquery.Value(a.QuestionId, 'SORTORDER') SORTORDER
          from query.r_ABC_PetitionTypeQuestion a
          where a.PetitionTypeObjectId = t_ToObjectId 
          order by 1
          ) loop
        if t_PrevValue = i.sortorder then
          api.pkg_errors.RaiseError(-20000, 'Questions cannot have duplicate sort order values.');
          exit;
        end if;
        t_PrevValue := i.sortorder;
      end loop;
    end if;

  end VerifyQuestions;

    -----------------------------------------------------------------------------------
    --UniqueColumn
    --Is called to error with the given error message if the given column is not unique
    procedure UniqueColumn(
     a_ObjectId                             udt_Id,
     a_AsOfDate                             date,
     a_ColumnName                           varchar2,
     a_ErrorMessage                         varchar2,
     a_CheckBox                             varchar2

    )
    is
      t_ObjectDefName varchar2(4000) := api.pkg_columnquery.value(a_ObjectId,'ObjectDefName');
      t_ObjectId      api.pkg_definition.udt_id;
      t_Unique        varchar2(4000) := api.pkg_columnquery.value(a_ObjectId,a_ColumnName);
      t_CheckBox      varchar2(4000) := api.pkg_columnquery.value(a_ObjectId,a_CheckBox);


    begin

      if api.pkg_columnquery.value(a_ObjectId,a_CheckBox) = 'Y' then
        begin
          select objectid
          into t_ObjectId
          from table(api.pkg_simplesearch.CastableObjectsByIndex(t_ObjectDefName, a_ColumnName, api.pkg_columnquery.value(a_ObjectId,a_ColumnName)));
        exception when too_many_rows
          then api.pkg_errors.RaiseError(-20001,a_ErrorMessage);
        end
        ;
      end if;
    end UniqueColumn;

  -----------------------------------------------------------------------------
  -- Update dup_IsWithinThreshold
  -- Is called from process server on anual basis
  -----------------------------------------------------------------------------
  procedure dup_IsWithinThreshold
  is
  t_VintageIds               api.pkg_definition.udt_IdList;

  begin
     select v.ObjectId
       bulk collect into t_VintageIds
       from query.o_abc_Vintage v;
     for i in 1 .. t_VintageIds.count loop
        api.pkg_columnupdate.SetValue (t_VintageIds(i), 'dup_IsWithinThreshold', api.pkg_columnquery.Value(t_VintageIds(i), 'IsWithinThreshold'));
     end loop;

  end dup_IsWithinThreshold;

  -----------------------------------------------------------------------------
  -- CheckPaymentReceipt
  -- Is called from Payment-Receipt relationship
  -----------------------------------------------------------------------------
  procedure CheckPaymentReceipt (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  )
  is
    t_PaymentObjectId                       udt_Id;
    t_ReceiptDocumentId                     udt_Id;
    t_Number                                integer;

  begin
     select prr.PaymentObjectId, prr.ReceiptDocumentId
       into t_PaymentObjectId, t_ReceiptDocumentId
       from query.r_Paymentreceiptreport prr
      where prr.RelationshipId = a_ObjectId;
    select count (*)
      into t_Number
      from query.r_PaymentReceiptReport pr
     where pr.PaymentObjectId   = t_PaymentObjectId
     and   pr.ReceiptDocumentId != t_ReceiptDocumentId;
     if t_Number > 0 then
        api.pkg_errors.RaiseError(-20000,'A receipt has already been created for this payment.');
     end if;
  end CheckPaymentReceipt;

  -----------------------------------------------------------------------------
  -- SetCommentEdited
  -- Is called from d_ElectronicDocument when CommentText is changed
  -----------------------------------------------------------------------------
  procedure SetCommentEdited (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
    t_EditedBy                              varchar2(60) := toolbox.pkg_Formula.CurrentUserStaffValue('Name');
  begin
     api.pkg_columnupdate.SetValue(a_ObjectId, 'CommentEditedBy', t_EditedBy, sysdate);
     api.pkg_columnupdate.SetValue(a_ObjectId, 'CommentEditedWhen', sysdate, sysdate);
  end SetCommentEdited;

  -----------------------------------------------------------------------------
  -- EncryptDLField
  -- Is called from the Driver's License field
  -----------------------------------------------------------------------------
  procedure EncryptDLField(
    a_ObjectId           udt_Id,
    a_AsOfDate           date,
    a_FieldName          varchar2
    ) is
    t_OriginalValue      varchar2(4000);
    t_EncryptedValue     varchar2(4000);
    t_Counter            number := 0;
    t_Asterisks          varchar2(4000);
    t_SavedPart          varchar2(4000);

  begin
     if g_ShouldEncryptDL = 'Y' then
        if a_FieldName is not null then
           t_OriginalValue := api.pkg_columnquery.Value(a_ObjectId, a_FieldName);

           --figure out what the "asterisks" part should be
           --save the last 2 characters
           t_SavedPart := substr(t_OriginalValue, length(t_OriginalValue) - 1);
           while t_Counter < (length(t_OriginalValue) - 2) loop
             t_Asterisks := t_Asterisks || '*';
             t_Counter := t_Counter + 1;
           end loop;

           t_EncryptedValue := abc.pkg_abc_encryption.Encrypt(t_OriginalValue);
           api.pkg_columnupdate.SetValue(a_ObjectId, a_FieldName || 'Encrypted', t_EncryptedValue);
           --replace existing field with asterisks, except for the last 2 characters
           g_ShouldEncryptDL := 'N';
           api.pkg_columnupdate.SetValue(a_ObjectId, a_FieldName, t_Asterisks || t_SavedPart);
        end if;
     end if;
  end EncryptDLField;

  -----------------------------------------------------------------------------
  -- CheckEmail
  -- Is called from various email details. Check for valid format.
  -----------------------------------------------------------------------------
  procedure CheckEmail(
    a_ObjectId              udt_Id,
    a_AsOfDate              date,
    a_FieldName             varchar2
    ) is
    My_String               varchar2(4000);
    t_AddressList           api.pkg_Definition.udt_StringList;

  begin
     My_String := api.pkg_columnquery.Value(a_ObjectId, a_FieldName);
     My_String := replace (My_String, ' ', ',');
     My_String := replace (My_String, ';', ',');
     t_AddressList := extension.pkg_Utils.Split(My_String, ',');
     for i in 1..t_AddressList.count loop
        if not REGEXP_LIKE (t_AddressList(i),'^(\S+)\@(\S+)\.(\S+)$') then
           api.pkg_errors.RaiseError(-20000, 'Email address "' || t_AddressList(i) || '" does not appear to be in a valid format. Please verify and correct.');
        end if;
     end loop;

     api.pkg_columnupdate.SetValue(a_ObjectId, a_FieldName, ltrim(rtrim(api.pkg_columnquery.Value(a_ObjectId, a_FieldName))));
  end CheckEmail;

  -----------------------------------------------------------------------------
  -- RemoveDupReceipts
  -- Is called daily from Process Server to remove duplicate receipts
  -----------------------------------------------------------------------------
  procedure RemoveDupReceipts
  is
  t_RelationshipId      number (9);
  t_EndPointId          number (9) := api.pkg_configquery.EndPointIdForName('o_Payment' ,'Receipt');
  begin
  -- Find Payments with more than 1 Receipt Document
  for r in (select pr.PaymentObjectId
              from query.r_PaymentReceiptReport pr
             group by pr.PaymentObjectId
            having count(*) > 1
           ) loop
  -- Find correct Document Relationship Id for the Payment
     select max (r.RelationshipId)
     into t_RelationshipId
     from api.relationships r
     join api.objects o on o.ObjectId = r.ToObjectId
     join api.objectdefs od on od.ObjectDefId = o.ObjectDefId
    where r.FromObjectId = r.PaymentObjectId
      and od.Name = 'd_ReceiptDocument';
  -- Use the found RelationshipId as parameter:
     for dp in (select r1.RelationshipId, r1.ToObjectId
                from   api.relationships r1
                where  r1.FromObjectId = r.PaymentObjectId
                and    r1.RelationshipId != t_RelationshipId
                and    r1.EndPointId = t_EndPointId
               ) loop
        api.pkg_relationshipupdate.Remove(dp.relationshipid);
        insert into possedba.remove_dup_receipts
               (Payment_Id, Receipt_Id, Deleted_Date)
        values
               (r.PaymentObjectId, dp.ToObjectId, sysdate);
     end loop;
  end loop;

  end RemoveDupReceipts;

  /*---------------------------------------------------------------------------
   * CheckPublicAccessGroups() -- PUBLIC
   *   Is called monthly from Process Server to check available Access Groups
   *-------------------------------------------------------------------------*/
  procedure CheckPublicAccessGroups is
    t_Body                              varchar2(4000);
    t_Subject                           varchar2(4000)
        := 'NJ ABC Public Access Groups as of ' || to_char(sysdate,'dd-MON-yyyy')
        || '.';
    t_count                             number;
    t_Description                       varchar2(4000);
    t_SystemSetting                     number;
    t_FromEmailAddress                  varchar2(1000);
    t_ToEmailAddress                    varchar2(1000);
    t_ToEmailAddressNonProd             varchar2(1000);
    t_Today                             date := trunc (sysdate);
  begin

    -- Get System Settings data
    select
      s.ObjectId,
      s.ToEmailAddressNonProd,
      s.FromEmailAddress
    into
      t_SystemSetting,
      t_ToEmailAddressNonProd,
      t_FromEmailAddress
    from
      query.o_systemsettings s
    where s.ObjectId = api.pkg_Search.ObjectByColumnValue('o_SystemSettings',
        'SystemSettingsNumber', 1);

    begin
      select e.ag_emailaddress
      into t_ToEmailAddress
      from possedba.ag_email e;
    exception
      when no_data_found then
        t_ToEmailAddress := t_ToEmailAddressNonProd;
    end;

    -- Get latest Access Group information
    select
      count(0),
      max(ag.Description)
    into
      t_count,
      t_Description
    from api.accessgroups ag
    where not exists (
        select 1
        from api.accessgroupusers agu
        where  agu.accessgroupid = ag.accessgroupid
        )
    and ag.description like 'www0%'
    -- to get a more accurate count
    and ag.Description > 'www000010000';

    -- Set up email body
    t_Body := 'There are still ' || ltrim(to_char(t_Count, '9,999,999')) ||
        ' Access Groups available. The last available Access Group is ' ||
        t_Description || '.';
    if (t_Count < 500) or (t_Count < 1000 and extract(month from t_Today) = 5) then
      t_Body := t_Body || chr(13) || chr(13) || 'Please prepare to request more' ||
          ' Access Groups (see NJ Wiki for instructions).';
      t_Subject := 'MINIMUM NUMBER OF ACCESS GROUPS REACHED !! ' || t_Subject;
    end if;

    -- Send email
    extension.pkg_sendmail.SendEmail(a_ObjectId => t_SystemSetting,
        a_FromAddress => t_FromEmailAddress, a_ToAddress => t_ToEmailAddress,
        a_CcAddress => '', a_BccAddress => '', a_Subject => t_Subject,
        a_Body => t_Body, a_DocumentId=> '', a_DocumentEndPointName => '',
        a_BodyIsHtml => '', a_AttachAllDocs => '');

    -- Update table
    insert into possedba.AG_Availableaccessgroups
    values (
      t_ToEmailAddress,
      t_Today,
      t_Count,
      t_Description
    );

  end CheckPublicAccessGroups;

  /*---------------------------------------------------------------------------
   * UnsetAvailableOnline() -- PUBLIC
   *   Referenced on Deletion of rel from ElectronicDocument to SampleForm.
   *   Resets the AvailableOnline flag when the Sample Form is deleted.
   *-------------------------------------------------------------------------*/
  procedure UnsetAvailableOnline (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_DocTypeObjectId                   udt_Id;
  begin

    -- Get the DocType Object Id
    select r.FromObjectId
    into t_DocTypeObjectId
    from
      api.relationships r
      join api.endpoints e
          on e.endpointid = r.endpointid
    where r.RelationshipId = a_ObjectId
      and e.name = 'SampleForm';

    -- Unflag the Document Type's Available Online checkbox.
    api.pkg_columnupdate.SetValue(t_DocTypeObjectId, 'AvailableOnline' ,'N');

  end UnsetAvailableOnline;

  /*---------------------------------------------------------------------------
   * CheckMandatoryDocType -- PUBLIC
   *   Check if job type is set when marking document type mandatory.
   *-------------------------------------------------------------------------*/
  procedure CheckMandatoryDocType (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_Mandatory                         varchar2(01);
    t_Application                       varchar2(01);
  begin

    t_Mandatory := api.pkg_columnquery.Value(a_ObjectId, 'Mandatory');
    t_Application := api.pkg_columnquery.Value(a_ObjectId, 'NewApplication');
    if t_Mandatory = 'Y' and
       t_Application = 'N' then
      api.pkg_errors.RaiseError(-20000,'You can''t mark a document as Mandatory ' ||
          'if the New Application is not set.');
    end if;

    t_Mandatory := api.pkg_columnquery.Value(a_ObjectId, 'RenewalMandatory');
    t_Application := api.pkg_columnquery.Value(a_ObjectId, 'RenewalApplication');
    if t_Mandatory = 'Y' and
       t_Application = 'N' then
      api.pkg_errors.RaiseError(-20000,'You can''t mark a document as Mandatory ' ||
          'if the Renewal Application is not set.');
    end if;

    t_Mandatory := api.pkg_columnquery.Value(a_ObjectId, 'MunicipalAmendmentMandatory');
    t_Application := api.pkg_columnquery.Value(a_ObjectId, 'MunicipalAmendment');
    if t_Mandatory = 'Y' and
       t_Application = 'N' then
      api.pkg_errors.RaiseError(-20000,'You can''t mark a document as Mandatory ' ||
          'if the Municipal Amendment is not set.');
    end if;

    t_Mandatory := api.pkg_columnquery.Value(a_ObjectId, 'MunicipalAppMandatory');
    t_Application := api.pkg_columnquery.Value(a_ObjectId, 'MunicipalApp');
    if t_Mandatory = 'Y' and
       t_Application = 'N' then
      api.pkg_errors.RaiseError(-20000,'You can''t mark a document as Mandatory ' ||
          'if the Municipal App is not set.');
    end if;

    t_Mandatory := api.pkg_columnquery.Value(a_ObjectId, 'MunicipalRenewMandatory');
    t_Application := api.pkg_columnquery.Value(a_ObjectId, 'MunicipalRenew');
    if t_Mandatory = 'Y' and
       t_Application = 'N' then
      api.pkg_errors.RaiseError(-20000,'You can''t mark a document as Mandatory ' ||
          'if the Municpal Renew is not set.');
    end if;

    t_Mandatory := api.pkg_columnquery.Value(a_ObjectId, 'PoliceAmendmentMandatory');
    t_Application := api.pkg_columnquery.Value(a_ObjectId, 'PoliceAmendment');
    if t_Mandatory = 'Y' and
       t_Application = 'N' then
      api.pkg_errors.RaiseError(-20000,'You can''t mark a document as Mandatory ' ||
          'if the Police Amendment is not set.');
    end if;

    t_Mandatory := api.pkg_columnquery.Value(a_ObjectId, 'PoliceAppMandatory');
    t_Application := api.pkg_columnquery.Value(a_ObjectId, 'PoliceApp');
    if t_Mandatory = 'Y' and
       t_Application = 'N' then
      api.pkg_errors.RaiseError(-20000,'You can''t mark a document as Mandatory ' ||
          'if the Police App is not set.');
    end if;

    t_Mandatory := api.pkg_columnquery.Value(a_ObjectId, 'PoliceRenewMandatory');
    t_Application := api.pkg_columnquery.Value(a_ObjectId, 'PoliceRenew');
    if t_Mandatory = 'Y' and
       t_Application = 'N' then
      api.pkg_errors.RaiseError(-20000,'You can''t mark a document as Mandatory ' ||
          'if the Police Renew is not set.');
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'SpecialConditionsMandatory') = 'Y'
        and not (api.pkg_columnquery.Value(a_ObjectId, 'MunicipalApp') = 'Y'
        and api.pkg_columnquery.Value(a_ObjectId, 'MunicipalRenew') = 'Y'
        and api.pkg_columnquery.Value(a_ObjectId, 'PoliceApp') = 'Y'
        and api.pkg_columnquery.Value(a_ObjectId, 'PoliceRenew') = 'Y') then
      api.pkg_errors.RaiseError(-20000, 'Please enable the special condition document type on all municipal and police processes.');
    end if;

  end CheckMandatoryDocType;

  /*---------------------------------------------------------------------------
   * CheckDataWarehouseFailure() -- PUBLIC
   *   Check if Data Warehouse had a sync failure
   *-------------------------------------------------------------------------*/
  procedure CheckDataWarehouseFailure
  is
    t_Body                              varchar2(4000);
    t_Subject                           varchar2(4000)
        := 'NJ-ABC Data Warehouse Synchronization Failure Alert.';
    t_SystemSetting                     udt_id;
    t_FromEmailAddress                  varchar2(1000);
    t_ToEmailAddress                    varchar2(1000);
    t_Error                             varchar2(4000);
    t_SyncDate                          date;
  begin

    -- Get System Settings data
    select
      s.ObjectId,
      nvl(s.ToEmailAddressDataWarehFailure, s.ToEmailAddressNonProd),
      s.FromEmailAddress
    into
      t_SystemSetting,
      t_ToEmailAddress,
      t_FromEmailAddress
    from query.o_systemsettings s;

    select
      s.errormessage,
      s.LastSynchronized
    into
      t_Error,
      t_SyncDate
    from sheriff.instances s;

    if t_Error is not null and
       t_ToEmailAddress is not null then
      t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
          'font-weight: bold; color: red;">This is an automated notification.' ||
          '</span><br><br>' || 'The NJ-ABC Data Warehouse encountered an error ' ||
          'during the synchronization attempt on ' || t_SyncDate || ', with ' ||
          'the error message: <br>"' || t_Error || '"<br><br>';
      extension.pkg_sendmail.SendEmail(t_SystemSetting, t_FromEmailAddress,
          t_ToEmailAddress, null, null, t_Subject, t_Body, null, null, 'Y', null);
    end if;

  end CheckDataWarehouseFailure;

end pkg_ABC_Common;
/
