create or replace package     pkg_ABC_AmendmentType is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  -----------------------------------------------------------------------------
  -- PostVerifyWrapper
  -- Runs on Post-Verify of the ABC Amendment Object.
  -----------------------------------------------------------------------------
  procedure PostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

end pkg_ABC_AmendmentType;

/

create or replace package body     pkg_ABC_AmendmentType is

  -----------------------------------------------------------------------------
  -- PostVerifyWrapper
  --  Runs on Post-Verify of the ABC Amendment Object.
  -----------------------------------------------------------------------------
  procedure PostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is

  begin

   if api.pkg_columnquery.Value(a_Objectid, 'RequiresMunicipalityInvolvemen') = 'N' and
      api.pkg_columnquery.Value(a_Objectid, 'RequiresResolution') = 'Y' then
      api.pkg_errors.RaiseError(-20000, 'You cannot have Requires Municipality Resolution selected if Requires Municiaplity Involvement is not selected.');
   end if;
   
   if api.pkg_columnquery.Value(a_Objectid, 'StartInactivity') = 'Y' and
      api.pkg_columnquery.Value(a_ObjectId, 'EndInactivity')   = 'Y' then
      api.pkg_errors.RaiseError(-20000, 'You cannot have both Start and End Inactivity Date selected. Please choose one or none.');
   end if;

  end PostVerifyWrapper;

end pkg_ABC_AmendmentType;

/

