create or replace package abc.PKG_ABC_BATCHRENEWALNOTIF is

  -- Author  : Ron Hagens
  -- Created : September 2015
  -- Purpose : Handle the Batch Renewal Notifications

  -- Public type declarations
  subtype udt_Id is api.pkg_Definition.udt_Id;

procedure StartBatchRenewalRequest(a_ProcessId udt_Id, a_AsOfDate date);
procedure SendCustomerEmailNotifications(a_ProcessId udt_Id, a_AsOfDate date);

  /*---------------------------------------------------------------------------
   * SendMuniEmailNotification()
   *   Send emails to Municipal users for "License - Municipality Issued" batch
   * renewals. This procedure is called from a system process and sends emails
   * to the municipality. This can also be called from the send / re-send button.
   *-------------------------------------------------------------------------*/
  procedure SendMuniEmailNotification(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );
  
procedure ResendCustomerLicenseNotif(a_LicenseObjectId udt_Id, a_AsOfDate date);
procedure ResendCustomerPermitNotif(a_PermitObjectId udt_Id, a_AsOfDate date);
procedure ResendCustomerProductNotif(a_RegistrantObjectId udt_Id, a_AsOfDate date);
  
/*---------------------------------------------------------------------------
 * CancelProductNotif
 *   Is called on cancellation of a Batch Renewal Notification job
 * Removes product registrants from the renewal cycle object so the batch can be re-run
 *-------------------------------------------------------------------------*/
procedure CancelProductNotif(a_ObjectId udt_Id,
                             a_AsOfDate date);

  /*---------------------------------------------------------------------------
   * RetrieveOnlineUserEmail. This is called to retrieve the emails of
   * online users associated with a Legal Entity
   *-------------------------------------------------------------------------*/
  function RetrieveOnlineUserEmail(
    a_LegalEntityId                     udt_Id
  ) return                              varchar2;

end PKG_ABC_BATCHRENEWALNOTIF;
/
create or replace package body abc.PKG_ABC_BATCHRENEWALNOTIF is

/***********************************
* Ron H. Nov 2015
* When this package was first created my expectaion for determining if a license was issued by the state was a check on the
* Municipality. If the Municipality was 'State' then it would be considered issued by the state. Late in development and testing
* the approach was changed to check the issuer on the license type. In cross-testing it was determined that it was not a 100%
* correlation between the Issuer specified on the license type and the "State" value for the municipality. It was also requested
* that we not include the "State" as part of the batch description.
* To minimize the code change, the approach I took was to select specific values for Muncilipality and MunicpalityObjectId
* if the Issuer was State so I could still group by them accurately and then enhance the logic of the batch description to not
* include the Municipality if it was issued by the State.
************************************/



/**** StartBatch ********************
* Creates a batch notification record and sets the initial relationships
*************************************/
function StartBatch(a_BatchRequestId udt_Id, a_StartRange varchar2, a_NotificationType varchar2, a_MunicpalityObjectId udt_Id ) return udt_Id is
    t_NewBatchId      udt_Id;
    t_BatchJobTypeId  udt_Id := api.pkg_configquery.ObjectDefIdForName('j_ABC_BatchRenewalNotification');
    t_NewRelId        udt_Id;
    t_BatchRenewalMuniEPId           udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'Municipality');
    t_BatchRenewalCountyEPId         udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'County');
    t_BatchRenewalNotifEPId          udt_Id :=  api.pkg_configquery.EndPointIdForName('j_ABC_BatchRenewalNotifRequest' , 'BatchRenewalNotif');
begin

  t_NewBatchId := api.pkg_jobupdate.New(a_JobType => t_BatchJobTypeId
                                     , a_Description => null
                                     , a_Location => null
                                     , a_ParentJob => null)
                                     ;

  t_NewRelId := api.pkg_relationshipupdate.New(a_EndPoint => t_BatchRenewalNotifEPId
                                            , a_FromObject => a_BatchRequestId
                                            , a_ToObject => t_NewBatchId)
                                            ;

  if a_MunicpalityObjectId is not null and a_MunicpalityObjectId <> 0 then

    t_NewRelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalCountyEPId,
                                                 t_NewBatchId,
                                                 a_MunicpalityObjectId);

  end if;

  api.pkg_columnupdate.setvalue( t_NewBatchId, 'BatchStartRange', a_StartRange );
  api.pkg_columnupdate.setvalue( t_NewBatchId, 'NotificationType', a_NotificationType );

  return t_NewBatchId;

end;


/**** StartBatch ********************
* Overloaded version - this one does not require the municpality and passes null in its place
*************************************/
function StartBatch(a_BatchRequestId number, a_StartRange varchar2, a_NotificationType varchar2 ) return udt_Id is
begin

  return StartBatch(a_BatchRequestId, a_StartRange, a_NotificationType, null );

end;

/**** FinishBatch *******************
* Finishes the batch writing and assigns a processes to the user
*************************************/
procedure FinishBatch( a_BatchJobId udt_Id, a_EndRange varchar2, a_UserId udt_Id, a_AsOfDate date) is
  t_ProcessId              udt_Id;
  t_ProcessTypeId          udt_Id := api.pkg_configquery.ObjectDefIdForName('p_ABC_ReleasePrintBatch');
begin
  --Contruct Strng for EndRange Here
/*  t_Test := a_EndRange;
  if  LENGTH(TRIM(TRANSLATE(a_EndRange, '0123456789',''))) = 0 then
  begin
  select api.pkg_columnquery.Value(o.ObjectId, 'dup_MunicipalityCountyCode')
    into t_TempEndRange
    from api.objects o
   where o.ObjectId = t_Test;
    exception when no_data_found then
      t_TempEndRange := a_EndRange;
  end;
  else
    t_TempEndRange := a_EndRange;
  end if;*/
  --We Now Get a ObjectId no longer a string
  api.pkg_columnupdate.setvalue( a_BatchJobId, 'BatchEndRange', a_EndRange);
  t_ProcessId := api.pkg_ProcessUpdate.New(a_BatchJobId, t_ProcessTypeId, null, a_AsOfDate, null, null);
  api.pkg_ProcessUpdate.Assign(t_ProcessId, a_UserId);

end;

/**** AddLicensesToBatchExp *********
* Loops through a cursor of licenses and creates a rel to the Expired notification
*************************************/
procedure AddLicensesToBatchExp( a_BatchJobId udt_Id,
                                 a_CountyObjectId udt_Id,
                                 a_County varchar2,
                                 a_LicenseType number,
                                 a_AsOfDate date,
                                 a_Issuer varchar2 ) is
    t_BatchRenewalLicenseEPId           udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'License');
    t_RelId                             udt_Id;
begin

    if a_LicenseType is null then

        --add all muni licenses
        for ml in (
                  select gt.licenseobjectid, gt.licensetypeobjectid
                    from abc.licensebatchjob_gt gt
                    where not exists (
                                       select 1
                                         from abc.licensebatch_xref_t xt
                                         join query.j_ABC_BatchRenewalNotification nj
                                           on nj.ObjectId = xt.renewalnotifbatchjobid
                                        where gt.licenseobjectid = xt.LicenseObjectId
                                          and nj.StatusName in ('INPROG', 'COMP')
                                      )
                      and ( ( a_Issuer = 'State' and gt.IssuingAuthority = ('State') ) or
                            ( a_Issuer = 'Municipality' and gt.IssuingAuthority = ('Municipality') ) )
                      and decode( gt.IssuingAuthority, 'State', 0, gt.CountyObjectId ) = a_CountyObjectId
                 ) loop

           insert into abc.licensebatch_xref_t
                       values(
                       a_BatchJobId,
                       ml.licenseobjectid,
                       ml.licensetypeobjectid);
           -- t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalLicenseEPId, a_BatchJobId, ml.ObjectId);

        end loop;

    else

        --add specific licence type for the muni
        for mlt in (
                  select gt.licenseobjectid,
                         gt.licensetypeobjectid
                    from abc.licensebatchjob_gt gt
                    where not exists (
                                      select 1
                                       from abc.licensebatch_xref_t xt
                                       join query.j_ABC_BatchRenewalNotification nj
                                         on nj.ObjectId = xt.renewalnotifbatchjobid
                                    where gt.licenseobjectid = xt.licenseobjectid
                                      and nj.StatusName in ('INPROG', 'COMP')
                                      )
                      and ( ( a_Issuer = 'State' and gt.IssuingAuthority = ('State') ) or
                            ( a_Issuer = 'Municipality' and gt.IssuingAuthority = ('Municipality') ) )
                      and decode( gt.IssuingAuthority, 'State', 0, gt.CountyObjectId ) = a_CountyObjectId
                      and gt.licensetypeobjectid = a_LicenseType
                 ) loop


            insert into abc.licensebatch_xref_t
                       values(
                       a_BatchJobId,
                       mlt.licenseobjectid,
                       mlt.licensetypeobjectid);
            --t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalLicenseEPId, a_BatchJobId, mlt.ObjectId);

        end loop;

    end if;

end;

/**** AddLicensesToBatchGrcPd *********
* Loops through a cursor of licenses and creates a rel to the Grace Period or second notification
***************************************/

procedure AddLicensesToBatchGrcPd( a_BatchJobId udt_Id, a_CountyObjectId udt_Id, a_County varchar2, a_LicenseType varchar2, a_AsOfDate date, a_Issuer varchar2 ) is
    t_BatchRenewalLicenseEPId           udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'LicenseGrcPd');
    t_RelId                             udt_Id;
begin

    if a_LicenseType is null then

             /*abc.licensebatchjobgrace_gt gt*/
        --add all muni licenses
        for ml in (
                  select gt.licenseobjectid,
                         gt.Licensetypeobjectid
                    from abc.licensebatchjobgrace_gt gt
                    where( ( a_Issuer = 'State' and gt.IssuingAuthority = ('State') ) or
                            ( a_Issuer = 'Municipality' and gt.IssuingAuthority = ('Municipality') ) )
                      and decode( gt.IssuingAuthority, 'State', 0, gt.CountyObjectId ) = a_CountyObjectId
                      and not exists (
                          select 1
                         from abc.licensebatchgrace_xref_t xt
                           join query.j_ABC_BatchRenewalNotification nj
                             on nj.ObjectId = xt.renewalnotifbatchjobid
                          where gt.licenseobjectid = xt.LicenseObjectId
                            and nj.StatusName in ('INPROG')
                         )
                 ) loop


           insert into abc.licensebatchgrace_xref_t
              values (a_BatchJobId,
                      ml.Licenseobjectid,
                      ml.Licensetypeobjectid);

            --t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalLicenseEPId, a_BatchJobId, ml.ObjectId);

        end loop;

    else

        --add specific licence type for the muni
        for mlt in (select gt.licenseobjectid,
                           gt.Licensetypeobjectid
                    from abc.licensebatchjobgrace_gt gt
                    where( ( a_Issuer = 'State' and gt.IssuingAuthority = ('State') ) or
                            ( a_Issuer = 'Municipality' and gt.IssuingAuthority = ('Municipality') ) )
                      and decode( gt.IssuingAuthority, 'State', 0, gt.CountyObjectId ) = a_CountyObjectId
                      and gt.LicenseType = a_LicenseType
                      and not exists (
                          select 1
                         from abc.licensebatchgrace_xref_t xt
                           join query.j_ABC_BatchRenewalNotification nj
                             on nj.ObjectId = xt.renewalnotifbatchjobid
                          where gt.licenseobjectid = xt.LicenseObjectId
                            and nj.StatusName in ('INPROG')
                         )
                 ) loop

            insert into abc.licensebatchgrace_xref_t
              values (a_BatchJobId,
                      mlt.Licenseobjectid,
                      mlt.Licensetypeobjectid);


            --t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalLicenseEPId, a_BatchJobId, mlt.ObjectId);

        end loop;

    end if;

end;


/**** AddMuniLicTypeBySegmentExp *********
* Add licenses to the batch - this covers the scenario where there are more than <threshold> licenses for a specific license type
* They will be bundled in <threshold> size batches
* This procedure works with the Expiration notifications
******************************************/
procedure AddMuniLicTypeBySegmentExp(a_BatchRequestId number,
                                     a_CountyObjectId udt_Id,
                                     a_County varchar2,
                                     a_LicenseTypeId varchar2,
                                     a_BatchSizeThreshold number,
                                     a_UserId udt_Id,
                                     a_AsOfDate date,
                                     a_Issuer varchar2 ) is
    t_CurrentBatchSize                 integer := 0;
    t_CurrentBatchJobId                udt_Id := 0;
    t_RecordIndex                      integer := 0;
    t_RelId                            udt_Id := 0;
    t_BatchRenewalLicenseEPId          udt_Id  :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'License');
    t_NotificationType                 varchar2(25) := 'License Expiration';
    t_MuniBatchDescription             varchar2(50);

begin

    if a_County = 'State' then
      t_MuniBatchDescription := '';
    else
      t_MuniBatchDescription := a_County || ': ';
    end if;

--New Code
    for mlts in ( select gt.licenseobjectid,
                         gt.licensetype,
                         gt.Licensetypeobjectid
                      from abc.licensebatchjob_gt gt
                      where not exists (
                                         select 1
                                           from abc.licensebatch_xref_t xt
                                           join query.j_ABC_BatchRenewalNotification nj
                                             on nj.ObjectId = xt.renewalnotifbatchjobid
                                          where gt.licenseobjectid = xt.licenseobjectid
                                            and nj.StatusName in ('INPROG', 'COMP')
                                        )
                        and decode( gt.IssuingAuthority, 'State', 0, gt.CountyObjectId ) = a_CountyObjectId
                        and gt.licensetypeobjectid = a_LicenseTypeId
                ) loop

        t_RecordIndex := t_RecordIndex + 1;

        if t_CurrentBatchJobId = 0 then
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                              t_MuniBatchDescription || mlts.licensetype
                                              || ': Records ' || t_RecordIndex,
                                              t_NotificationType,
                                              a_CountyObjectId );
            t_CurrentBatchSize := 0;
        end if;
--End New Code
        if t_CurrentBatchSize >= a_BatchSizeThreshold then
            FinishBatch( t_CurrentBatchJobId, t_RecordIndex - 1, a_UserId, a_AsOfDate );
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                              t_MuniBatchDescription || mlts.licensetype
                                              || ': Records ' || t_RecordIndex,
                                              t_NotificationType,
                                              a_CountyObjectId );
            t_CurrentBatchSize := 0;
        end if;
-----New Code
       insert into abc.licensebatch_xref_t
       values(
       t_CurrentBatchJobId,
       mlts.licenseobjectid,
       mlts.licensetypeobjectid);
-----End New Code
       -- t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalLicenseEPId, t_CurrentBatchJobId, mlts.ObjectId);
        t_CurrentBatchSize := t_CurrentBatchSize + 1;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_CurrentBatchSize > 0 then
         FinishBatch( t_CurrentBatchJobId, t_RecordIndex, a_UserId, a_AsOfDate );
     end if;
end;

/**** AddMuniLicTypeBySegmentGrcPd *********
* Add licenses to the batch - this covers the scenario where there are more than <threshold> licenses for a specific license type
* They will be bundled in <threshold> size batches
* This procedure works with the Grace Period notifications
******************************************/
procedure AddMuniLicTypeBySegmentGrcPd(a_BatchRequestId number, a_CountyObjectId udt_Id, a_County varchar2, a_LicenseType varchar2, a_BatchSizeThreshold number, a_UserId udt_Id, a_AsOfDate date, a_Issuer varchar2 ) is
    t_CurrentBatchSize                 integer := 0;
    t_CurrentBatchJobId                udt_Id := 0;
    t_RecordIndex                      integer := 0;
    t_RelId                            udt_Id := 0;
    t_BatchRenewalLicenseEPId          udt_Id  :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'LicenseGrcPd');
    t_NotificationType                 varchar2(25) := 'License Grace Period';
    t_MuniBatchDescription             varchar2(50);

begin

    if a_County = 'State' then
      t_MuniBatchDescription := '';
    else
      t_MuniBatchDescription := a_County || ': ';
    end if;

    for mlts in ( select gt.licenseobjectid,
                         gt.licensetypeobjectid
                    from abc.licensebatchjobgrace_gt gt
                    where ( ( a_Issuer = 'State' and gt.IssuingAuthority = ('State') ) or
                            ( a_Issuer = 'Municipality' and gt.IssuingAuthority = ('Municipality') ) )
                      and decode( gt.IssuingAuthority, 'State', 0, gt.CountyObjectId ) = a_CountyObjectId
                      and gt.LicenseType = a_LicenseType
                      and not exists (
                          select 1
                         from abc.licensebatchgrace_xref_t xt
                           join query.j_ABC_BatchRenewalNotification nj
                             on nj.ObjectId = xt.renewalnotifbatchjobid
                          where gt.licenseobjectid = xt.LicenseObjectId
                            and nj.StatusName in ('INPROG')
                         )
              ) loop

        t_RecordIndex := t_RecordIndex + 1;

        if t_CurrentBatchJobId = 0 then
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId, t_MuniBatchDescription || a_LicenseType || ': Records ' || t_RecordIndex, t_NotificationType, a_CountyObjectId );
            t_CurrentBatchSize := 0;
        end if;

        if t_CurrentBatchSize >= a_BatchSizeThreshold then
            FinishBatch( t_CurrentBatchJobId, t_RecordIndex - 1, a_UserId, a_AsOfDate );
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId, t_MuniBatchDescription || a_LicenseType || ': Records ' || t_RecordIndex, t_NotificationType, a_CountyObjectId );
            t_CurrentBatchSize := 0;
        end if;

       insert into abc.licensebatchgrace_xref_t
         values(t_CurrentBatchJobId,
                mlts.licenseobjectid,
                mlts.licensetypeobjectid);
       -- t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalLicenseEPId, t_CurrentBatchJobId, mlts.ObjectId);

        t_CurrentBatchSize := t_CurrentBatchSize + 1;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_CurrentBatchSize > 0 then
         FinishBatch( t_CurrentBatchJobId, t_RecordIndex, a_UserId, a_AsOfDate );
     end if;
end;


/**** AddMuniLicensesByLicTypeExp *********
* Add all the licenses for the specific license type for the specific municipality
* This handles the Expiration Notices
*******************************************/
procedure AddMuniLicensesByLicTypeExp(a_BatchRequestId udt_Id,
                                      a_CountyObjectId udt_Id,
                                      a_County varchar2,
                                      a_BatchSizeThreshold number,
                                      a_UserId udt_Id,
                                      a_AsOfDate date,
                                      a_Issuer varchar2) is

    t_CurrentBatchSize       integer := 0;
    t_CurrentBatchJobId      udt_Id := 0;
    t_PreviousLicType        varchar2(50);
    t_NotificationType       varchar2(25) := 'License Expiration';

    t_CombineLicTypes        varchar2(1) := 'Y';  --'Y/N'
    t_MuniBatchDescription   varchar2(50);

begin

    if a_Issuer = 'State' then
      t_CombineLicTypes := 'N';
      t_MuniBatchDescription := '';
    else
      t_MuniBatchDescription := a_County || ': ';
    end if;

    for l in (
                  select a.LicenseTypeObjectId,
                         a.licensetype,
                         count(*) count_for_LicType
                  from (
                        select gt.licenseobjectid, gt.LicenseTypeObjectId, gt.LicenseType
                        from abc.licensebatchjob_gt gt
                       where  not exists (
                                          select 1
                                           from abc.licensebatch_xref_t xt
                                           join query.j_ABC_BatchRenewalNotification nj
                                             on nj.ObjectId = xt.renewalnotifbatchjobid
                                          where gt.licenseobjectid = xt.licenseobjectid
                                            and nj.StatusName in ('INPROG', 'COMP')
                                        )
                        and ( ( a_Issuer = 'State' and gt.IssuingAuthority = ('State') ) or
                              ( a_Issuer = 'Municipality' and gt.IssuingAuthority = ('Municipality') ) )
                        --and decode( gt.IssuingAuthority, 'State', 'State', gt.Municipality ) = a_municipality
                        and decode( gt.IssuingAuthority, 'State', 0, gt.CountyObjectId ) = a_CountyObjectId
                       ) a
                  group by a.LicenseTypeObjectId, a.licensetype
                  order by 1
             ) loop

         if  l.count_for_LicType > 0 then

             if l.count_for_LicType > a_BatchSizeThreshold then
                 -- add it in threshold size segments

                 if t_CurrentBatchJobId <> 0 and t_PreviousLicType is not null then
                   FinishBatch( t_CurrentBatchJobId, t_PreviousLicType, a_UserId, a_AsOfDate );
                 end if;

                 --set things up so when we come back it will be as if we are starting new...
                 t_CurrentBatchJobId := 0;
                 t_PreviousLicType := null;
                 t_CurrentBatchSize := 0;

                 AddMuniLicTypeBySegmentExp(a_BatchRequestId,
                                            a_CountyObjectId,
                                            a_County,
                                            l.LicenseTypeObjectId,
                                            a_BatchSizeThreshold,
                                            a_UserId,
                                            a_AsOfDate,
                                            a_Issuer);

             else

                 if t_CombineLicTypes = 'Y' then

                     --check if adding new lic type will exceed threshold
                     if t_CurrentBatchSize + l.count_for_LicType > a_BatchSizeThreshold then

                         if t_CurrentBatchJobId <> 0 and t_PreviousLicType is not null then
                             FinishBatch( t_CurrentBatchJobId, t_PreviousLicType, a_UserId, a_AsOfDate );
                         end if;

                         t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                                           t_MuniBatchDescription || l.LicenseType,
                                                           t_NotificationType, a_CountyObjectId );
                         t_CurrentBatchSize := 0;

                     end if;

                 else

                     if t_CurrentBatchJobId <> 0 and t_PreviousLicType is not null then
                         FinishBatch( t_CurrentBatchJobId, t_PreviousLicType, a_UserId, a_AsOfDate );
                     end if;

                     t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                                       t_MuniBatchDescription || l.LicenseType,
                                                       t_NotificationType,
                                                       a_CountyObjectId );
                     t_CurrentBatchSize := 0;

                 end if;

                 --the first pass through the or after processing by License Type the CurrentBatchJobId will be 0
                 if l.count_for_LicType > 0 and t_CurrentBatchJobId = 0 then
                     t_CurrentBatchJobId := StartBatch(a_BatchRequestId, t_MuniBatchDescription || l.LicenseType, t_NotificationType, a_CountyObjectId );
                     t_CurrentBatchSize := 0;
                 end if;

                 AddLicensesToBatchExp( t_CurrentBatchJobId,
                                        a_CountyObjectId,
                                        a_County,
                                        l.LicenseTypeObjectId,
                                        a_AsOfDate,
                                        a_Issuer );
                 t_CurrentBatchSize := t_CurrentBatchSize + l.count_for_LicType;
                 t_PreviousLicType := l.LicenseType;

             end if;

         end if;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_PreviousLicType is not null then
         FinishBatch( t_CurrentBatchJobId, t_PreviousLicType, a_UserId, a_AsOfDate );
     end if;

end;


/**** AddMuniLicensesByLicTypeGrcPd *********
* Add all the licenses for the specific license type for the specific municipality
* This handles the Grace Period Notices
*********************************************/
procedure AddMuniLicensesByLicTypeGrcPd(a_BatchRequestId udt_Id, a_CountyObjectId udt_Id, a_County varchar2, a_BatchSizeThreshold number, a_UserId udt_Id, a_AsOfDate date, a_Issuer varchar2) is

    t_CurrentBatchSize       integer := 0;
    t_CurrentBatchJobId      udt_Id := 0;
    t_PreviousLicType        varchar2(50);
    t_NotificationType       varchar2(25) := 'License Grace Period';

    t_CombineLicTypes        varchar2(1) := 'Y';  --'Y/N'
    t_MuniBatchDescription   varchar2(50);

begin

    if a_Issuer = 'State' then
      t_CombineLicTypes := 'N';
      t_MuniBatchDescription := '';
    else
      t_MuniBatchDescription := a_County || ': ';    end if;

    for l in (
                  select a.LicenseType, count(*) count_for_LicType
                  from (
                        select gt.licenseobjectid, gt.licensetype
                        from abc.licensebatchjobgrace_gt gt
                        where ( ( a_Issuer = 'State' and gt.IssuingAuthority = ('State') ) or
                              ( a_Issuer = 'Municipality' and gt.IssuingAuthority = ('Municipality') ) )
                        and decode( gt.IssuingAuthority, 'State', 0, gt.CountyObjectId ) = a_CountyObjectId
                        and not exists (
                          select 1
                         from abc.licensebatchgrace_xref_t xt
                           join query.j_ABC_BatchRenewalNotification nj
                             on nj.ObjectId = xt.renewalnotifbatchjobid
                          where gt.licenseobjectid = xt.LicenseObjectId
                            and nj.StatusName in ('INPROG')
                         )
                       ) a
                  group by a.LicenseType
                  order by 1
             ) loop

         if  l.count_for_LicType > 0 then

             if l.count_for_LicType > a_BatchSizeThreshold then
                 -- add it in threshold size segments

                 if t_CurrentBatchJobId <> 0 and t_PreviousLicType is not null then
                   FinishBatch( t_CurrentBatchJobId, t_PreviousLicType, a_UserId, a_AsOfDate );
                 end if;

                 --set things up so when we come back it will be as if we are starting new...
                 t_CurrentBatchJobId := 0;
                 t_PreviousLicType := null;
                 t_CurrentBatchSize := 0;

                 AddMuniLicTypeBySegmentGrcPd(a_BatchRequestId,
                                              a_CountyObjectId,
                                              a_County,
                                              l.LicenseType,
                                              a_BatchSizeThreshold,
                                              a_UserId,
                                              a_AsOfDate,
                                              a_Issuer);

             else

                 if t_CombineLicTypes = 'Y' then

                     --check if adding new lic type will exceed threshold
                     if t_CurrentBatchSize + l.count_for_LicType > a_BatchSizeThreshold then

                         if t_CurrentBatchJobId <> 0 and t_PreviousLicType is not null then
                             FinishBatch( t_CurrentBatchJobId,
                                          t_PreviousLicType,
                                          a_UserId,
                                          a_AsOfDate );
                         end if;

                         t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                                           t_MuniBatchDescription ||
                                                           l.LicenseType,
                                                           t_NotificationType,
                                                           a_CountyObjectId );
                         t_CurrentBatchSize := 0;

                     end if;

                 else

                     if t_CurrentBatchJobId <> 0 and t_PreviousLicType is not null then
                         FinishBatch( t_CurrentBatchJobId,
                                      t_PreviousLicType,
                                      a_UserId,
                                      a_AsOfDate );
                     end if;

                     t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                                       t_MuniBatchDescription || l.LicenseType,
                                                       t_NotificationType,
                                                       a_CountyObjectId );
                     t_CurrentBatchSize := 0;

                 end if;

                 --the first pass through the or after processing by License Type the CurrentBatchJobId will be 0
                 if l.count_for_LicType > 0 and t_CurrentBatchJobId = 0 then
                     t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                                       t_MuniBatchDescription || l.LicenseType,
                                                       t_NotificationType,
                                                       a_CountyObjectId );
                     t_CurrentBatchSize := 0;
                 end if;

                 AddLicensesToBatchGrcPd( t_CurrentBatchJobId,
                                          a_CountyObjectId,
                                          a_County,
                                          l.LicenseType,
                                          a_AsOfDate,
                                          a_Issuer );
                 t_CurrentBatchSize := t_CurrentBatchSize + l.count_for_LicType;
                 t_PreviousLicType := l.LicenseType;

             end if;

         end if;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_PreviousLicType is not null then
         FinishBatch( t_CurrentBatchJobId, t_PreviousLicType, a_UserId, a_AsOfDate );
     end if;

end;

/**** AddPermitsToBatchExp *********
* Adds the permits to the batch
* this one handles the expiration notices
*************************************/
procedure AddPermitsToBatchExp( a_BatchJobId udt_Id, a_PermitType varchar2, a_AsOfDate date ) is
    t_BatchRenewalPermitEPId           udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'PermitExp');
    t_RelId                            udt_Id;
begin

    for pt in (
              select gt.Permitobjectid,
                     gt.PermitType,
                     gt.Permittypeobjectid
                from abc.permitbatchjob_gt  gt
                where gt.PermitType = a_PermitType
                  and not exists (
                                   select 1
                                     from abc.permitbatch_xref_t r
                                     join query.j_ABC_BatchRenewalNotification nj
                                       on nj.ObjectId = r.renewalnotifbatchjobid
                                    where gt.Permitobjectid = r.PermitObjectId
                                      and nj.StatusName in ('INPROG', 'COMP')
                                   )
             ) loop

        insert into abc.permitbatch_xref_t
        values(
        a_BatchJobId,
        pt.Permitobjectid,
        pt.Permittypeobjectid
        );
    end loop;

end;

/**** AddPermitsToBatchGrcPd *********
* Adds the permits to the batch
* this one handles the grace period notices
*************************************/
procedure AddPermitsToBatchGrcPd( a_BatchJobId udt_Id, a_PermitType varchar2, a_AsOfDate date ) is
    t_BatchRenewalPermitEPId           udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'PermitGrcPd');
    t_RelId                            udt_Id;
begin

    for pt in (
              select gt.PermitObjectId,
                     gt.permittypeobjectid
                 from abc.permitbatchjobgrace_gt  gt
                where gt.PermitType = a_PermitType
                  and not exists (
                                   select 1
                                     from abc.permitbatchgrace_xref_t r
                                     join query.j_ABC_BatchRenewalNotification nj
                                       on nj.ObjectId = r.renewalnotifbatchjobid
                                    where gt.Permitobjectid = r.PermitObjectId
                                      and nj.StatusName in ('INPROG')
                                   )
             ) loop

        insert into abc.permitbatchgrace_xref_t
        values(
        a_BatchJobId,
        pt.Permitobjectid,
        pt.permittypeobjectid
        );

        --t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalPermitEPId, a_BatchJobId, pt.ObjectId);

    end loop;

end;

/**** AddPermitTypeBySegmentExp *********
* When there are more permits of the specific permit type, add them in <threshold> size segments
* This procedure handles the expiration notifications
******************************************/
procedure AddPermitTypeBySegmentExp(a_BatchRequestId number, a_PermitType varchar2, a_BatchSizeThreshold number, a_UserId udt_Id, a_AsOfDate date ) is
    t_CurrentBatchSize                 integer := 0;
    t_CurrentBatchJobId                udt_Id := 0;
    t_RecordIndex                      integer := 0;
    t_RelId                            udt_Id := 0;
    t_BatchRenewalPermitEPId           udt_Id  :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'PermitExp');
    t_NotificationType                 varchar2(25) := 'Permit Expiration';

begin
    for pts in (
              select gt.permitobjectid,
                     gt.PermitType,
                     gt.Permittypeobjectid
                from abc.permitbatchjob_gt gt
               where gt.PermitType = a_PermitType
               and not exists (
                                   select 1
                                     from abc.permitbatch_xref_t r
                                     join query.j_ABC_BatchRenewalNotification nj
                                       on nj.ObjectId = r.renewalnotifbatchjobid
                                    where gt.Permitobjectid = r.PermitObjectId
                                      and nj.StatusName in ('INPROG', 'COMP')
                                   )
                ) loop

        t_RecordIndex := t_RecordIndex + 1;

        if t_CurrentBatchJobId = 0 then
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                              a_PermitType || ': Records ' ||
                                              t_RecordIndex,
                                              t_NotificationType );
            t_CurrentBatchSize := 0;
        end if;

        if t_CurrentBatchSize >= a_BatchSizeThreshold then
            FinishBatch( t_CurrentBatchJobId,
                         t_RecordIndex - 1,
                         a_UserId,
                         a_AsOfDate );
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId,
                                              a_PermitType || ': Records ' ||
                                              t_RecordIndex,
                                              t_NotificationType );
            t_CurrentBatchSize := 0;
        end if;
        insert into abc.permitbatch_xref_t
        values(
        t_CurrentBatchJobId,
        pts.Permitobjectid,
        pts.Permittypeobjectid
        );
        --t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalPermitEPId, t_CurrentBatchJobId, pts.ObjectId);
        t_CurrentBatchSize := t_CurrentBatchSize + 1;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_CurrentBatchSize > 0 then
         FinishBatch( t_CurrentBatchJobId,
                      t_RecordIndex,
                      a_UserId,
                      a_AsOfDate );
     end if;
end;

/**** AddPermitTypeBySegmentGrcPd *********
* When there are more permits of the specific permit type, add them in <threshold> size segments
* This procedure handles the grace period notifications
******************************************/
procedure AddPermitTypeBySegmentGrcPd(a_BatchRequestId number, a_PermitType varchar2, a_BatchSizeThreshold number, a_UserId udt_Id, a_AsOfDate date ) is
    t_CurrentBatchSize                 integer := 0;
    t_CurrentBatchJobId                udt_Id := 0;
    t_RecordIndex                      integer := 0;
    t_RelId                            udt_Id := 0;
    t_BatchRenewalPermitEPId           udt_Id  :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'PermitGrcPd');
    t_NotificationType                 varchar2(25) := 'Permit Grace Period';

begin
    for pts in (
              select gt.Permitobjectid,
                     gt.PermitType,
                     gt.Permittypeobjectid
                from abc.permitbatchjobgrace_gt gt
               where gt.PermitType = a_PermitType
               and not exists (
                                   select 1
                                     from abc.permitbatchgrace_xref_t r
                                     join query.j_ABC_BatchRenewalNotification nj
                                       on nj.ObjectId = r.renewalnotifbatchjobid
                                    where gt.Permitobjectid = r.PermitObjectId
                                      and nj.StatusName in ('INPROG')
                                   )
                ) loop

        t_RecordIndex := t_RecordIndex + 1;

        if t_CurrentBatchJobId = 0 then
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId, a_PermitType || ': Records ' || t_RecordIndex, t_NotificationType );
            t_CurrentBatchSize := 0;
        end if;

        if t_CurrentBatchSize >= a_BatchSizeThreshold then
            FinishBatch( t_CurrentBatchJobId, t_RecordIndex - 1, a_UserId, a_AsOfDate );
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId, a_PermitType || ': Records ' || t_RecordIndex, t_NotificationType );
            t_CurrentBatchSize := 0;
        end if;

        insert into abc.permitbatchgrace_xref_t
        values(
        t_CurrentBatchJobId,
        pts.permitobjectid,
        pts.permittypeobjectid
        );
        --t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalPermitEPId, t_CurrentBatchJobId, pts.ObjectId);
        t_CurrentBatchSize := t_CurrentBatchSize + 1;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_CurrentBatchSize > 0 then
         FinishBatch( t_CurrentBatchJobId, t_RecordIndex, a_UserId, a_AsOfDate );
     end if;

end;

/**** AddProdDetailsToRel *********
* Updates a couple of details on the rel between the registrant and the Product Notification Year.
*************************************/
procedure AddProdDetailsToRel( a_RelId udt_Id, a_RegistrantObjectId udt_Id, a_ProductNotificationYear number ) is
  t_ProductCount                 number;
  t_EarliestProdExpirationDate   date;
begin

  select count(*), min(op.ExpirationDate)
    into t_ProductCount, t_EarliestProdExpirationDate
    from query.r_ABC_ProductRegistrant r
    join query.o_abc_product op on op.ObjectId = r.ProductObjectId
    where r.RegistrantObjectId = a_RegistrantObjectId
      and to_char( op.ExpirationDate, 'YYYY' ) = to_char( a_ProductNotificationYear );

  api.pkg_columnupdate.SetValue( a_RelId, 'ProductCount', t_ProductCount );
  api.pkg_columnupdate.SetValue( a_RelId, 'EarliestProductExpirationDate', t_EarliestProdExpirationDate );

end;


/**** AddProdRegistrantsToBatchExp *********
* Add the registrants for expiring products to the batch.
* This procedure handles the expiration notices (there are no grace period notices for product registrants)
*************************************/
procedure AddProdRegistrantsToBatchExp( a_BatchJobId udt_Id, --Batch Notifcation Job Id
                                        a_RegistrantInitial varchar2,
                                        a_AsOfDate date,
                                        a_ProductNotificationYear number,
                                        a_ProductRenewalCycleObjectId udt_Id ) is
    t_LegalEntityPRCEPId               udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('o_ABC_LegalEntity', 'ProdRenCyc');
    t_RelId                            udt_Id;
    t_Count                            integer;
--    t_SearchResults2 := api.pkg_Search.NewSearchResults();
    t_InitialSearchDone                boolean;
    t_BeginingofYear                   date := to_date('01/01' || a_ProductNotificationYear, 'MM/DD/YYYY');
    t_EndOfYear                        date := to_date('12/31/' || a_ProductNotificationYear, 'MM/DD/YYYY');
    t_LimitedList                      api.pkg_definition.udt_IdList;
    t_ProductRenewalCycleObjectId      udt_id;
    t_TempList                         api.pkg_definition.udt_IdList;
    type NumberList                       is table of number;
    t_RegistrantRelatedList            NumberList;
    t_SearchResults                    api.pkg_search.udt_SearchResults;
    t_Exists                           boolean :=True;



begin

    t_SearchResults := api.pkg_Search.NewSearchResults();

     t_ProductRenewalCycleObjectId := a_ProductRenewalCycleObjectId;

     --Return a List of Regisrtant Legal Entites That have Active Products Expiring this year.
     t_InitialSearchDone := api.pkg_search.IntersectByRelColumnValue('o_abc_legalentity',
                                              'Product',
                                              'State',
                                              'Current',
                                              t_SearchResults);

     t_InitialSearchDone := api.pkg_Search.IntersectByRelColumnValue('o_abc_legalentity',
                                              'Product',
                                              'ExpirationDate' ,
                                              t_BeginingofYear,
                                              t_EndOfYear,
                                              t_SearchResults);


     if t_InitialSearchDone = True then
     --If we found registrants that have products expiring this year then look for already related registratns

       t_LimitedList := api.pkg_search.SearchResultsToList(t_SearchResults);

       select rt.registrantobjectid
        bulk collect into t_RegistrantRelatedList
        from abc.registrantnotifications_t rt
        where rt.renewalcycleobjectid = t_ProductRenewalCycleObjectId;
       /*select r2.RegistrantObjectId
        bulk collect into t_RegistrantRelatedList
        from query.r_ABC_BatchRenewalProdRenewCyc r
        join query.r_ABC_RegistrantProdRenewCycle r2 on r2.ProductRenewalCycleObjectId = r.ProductRenewalCycleObjectId
        where r.ProductRenewalCycleObjectId = t_ProductRenewalCycleObjectId;*/

     end if;

     for x in 1..t_LimitedList.count loop
      t_Exists := to_number(t_LimitedList(x))  Member of t_RegistrantRelatedList;
      if not t_Exists then
        t_TempList(t_TempList.count + 1) := t_LimitedList(x);
      end if;
    end loop;

    for y in 1..t_TempList.count loop
      if api.pkg_columnquery.value(t_TempList(Y), 'LegalEntityInitial') =  a_RegistrantInitial then
        t_RelId := api.pkg_RelationshipUpdate.New(t_LegalEntityPRCEPId, t_TempList(y), t_ProductRenewalCycleObjectId);

        --Create the External Relationship between the Notification Job and Legal Entity
        insert into abc.registrantnotifications_t rt
        values(a_BatchJobId,
               t_TempList(y),
               a_RegistrantInitial,
               t_ProductRenewalCycleObjectId);

        AddProdDetailsToRel( t_RelId, t_TempList(y), a_ProductNotificationYear );
      end if;
    end loop;

end;

/**** AddProdRegistrantsBySegmentExp *********
* If there are more Registrants than <threshold> for a specific first initial add them in <threshold> size segments.
******************************************/
procedure AddProdRegistrantsBySegmentExp(a_BatchRequestId number,
									     a_RegistrantInitial varchar2,
										 a_BatchSizeThreshold number,
										 a_UserId udt_Id, a_AsOfDate date,
										 a_ProductNotificationYear number,
										 a_ProductRenewalCycleObjectId udt_Id ) is
    t_CurrentBatchSize                 integer := 0;
    t_CurrentBatchJobId                udt_Id := 0;
    t_RecordIndex                      integer := 0;
    t_RelId                            udt_Id := 0;
    t_BatchRenewalProdPRCEPId          udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'ProdRenCyc');
    t_LegalEntityPRCEPId               udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('o_ABC_LegalEntity', 'ProdRenCyc');
    t_NotificationType                 varchar2(25) := 'Product Expiration';
    t_Count                            integer;
    t_InitialSearchDone                boolean;
    t_BeginingofYear                   date := to_date('01/01' || a_ProductNotificationYear, 'MM/DD/YYYY');
    t_EndOfYear                        date := to_date('12/31/' || a_ProductNotificationYear, 'MM/DD/YYYY');
    t_LimitedList                      api.pkg_definition.udt_IdList;
    t_ProductRenewalCycleObjectId      udt_id;
    t_TempList                         api.pkg_definition.udt_IdList;
    type NumberList                       is table of number;
    t_RegistrantRelatedList            NumberList;
    t_SearchResults                    api.pkg_search.udt_SearchResults;
    t_Exists                           boolean :=True;

begin


t_SearchResults := api.pkg_Search.NewSearchResults();

     t_ProductRenewalCycleObjectId := a_ProductRenewalCycleObjectId;

     --Return a List of Regisrtant Legal Entites That have Active Products Expiring this year.
     t_InitialSearchDone := api.pkg_search.IntersectByRelColumnValue('o_abc_legalentity',
                                              'Product',
                                              'State',
                                              'Current',
                                              t_SearchResults);

     t_InitialSearchDone := api.pkg_Search.IntersectByRelColumnValue('o_abc_legalentity',
                                              'Product',
                                              'ExpirationDate' ,
                                              t_BeginingofYear,
                                              t_EndOfYear,
                                              t_SearchResults);


     --api.pkg_errors.RaiseError(-20000, 'Count of Results: ');

     if t_InitialSearchDone = True then
     --If we found registrants that have products expiring this year then look for already related registratns

       t_LimitedList := api.pkg_search.SearchResultsToList(t_SearchResults);

        select rt.registrantobjectid
        bulk collect into t_RegistrantRelatedList
        from abc.registrantnotifications_t rt
        where rt.renewalcycleobjectid = t_ProductRenewalCycleObjectId;

      -- extension.pkg_collectionutils.Subtract();

     end if;

     for x in 1..t_LimitedList.count loop
	  --If the registrant is not already related to a job then checn the initial of the Legal Entity.
      --If the registrant is not already related to a job then checn the initial of the Legal Entity.
      t_Exists := to_number(t_LimitedList(x))  Member of t_RegistrantRelatedList;
      if not t_Exists then
	    --If the Initial is matching add it to the list to work with.
	    if api.pkg_columnquery.value(t_LimitedList(x), 'LegalEntityInitial') =  a_RegistrantInitial then
          t_TempList(t_TempList.count + 1) := t_LimitedList(x);
		end if;
      end if;
    end loop;

        for y in 1..t_TempList.count loop
        t_RecordIndex := t_RecordIndex + 1;

        if t_CurrentBatchJobId = 0 then
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId, a_RegistrantInitial || ': Records ' || t_RecordIndex, t_NotificationType );
            t_CurrentBatchSize := 0;
            t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalProdPRCEPId, t_CurrentBatchJobId, a_ProductRenewalCycleObjectId);
        end if;

        if t_CurrentBatchSize >= a_BatchSizeThreshold then
            FinishBatch( t_CurrentBatchJobId, t_RecordIndex - 1, a_UserId, a_AsOfDate );
            t_CurrentBatchJobId := StartBatch(a_BatchRequestId, a_RegistrantInitial || ': Records ' || t_RecordIndex, t_NotificationType );
            t_CurrentBatchSize := 0;
            t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalProdPRCEPId, t_CurrentBatchJobId, a_ProductRenewalCycleObjectId);
        end if;

        t_RelId := api.pkg_RelationshipUpdate.New(t_LegalEntityPRCEPId, t_TempList(y), a_ProductRenewalCycleObjectId);

        --Create the External Relationship between the Notification Job and Legal Entity
        insert into abc.registrantnotifications_t rt
        values(t_CurrentBatchJobId,
               t_TempList(y),
               a_RegistrantInitial,
               t_ProductRenewalCycleObjectId);

        AddProdDetailsToRel( t_RelId, t_TempList(y), a_ProductNotificationYear );
        t_CurrentBatchSize := t_CurrentBatchSize + 1;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_CurrentBatchSize > 0 then
         FinishBatch( t_CurrentBatchJobId, t_RecordIndex, a_UserId, a_AsOfDate );
     end if;

end;

/**** BatchRenewalLicensesExp *********
* This gets the outer cursor grouped by municpality
* This procedure handles the expiration notices
***************************************/
procedure BatchRenewalLicensesExp(a_BatchRequestId udt_Id, a_AsOfDate date, a_Issuer varchar2, a_BatchSizeThreshold integer, a_UserId udt_Id,
                                  a_LicenseTypes varchar2 )
   is
  t_CurrentBatchSize       integer := 0;
  t_BatchJobTypeId         udt_Id := api.pkg_configquery.ObjectDefIdForName('j_ABC_BatchRenewalNotification');
  t_CurrentBatchJobId      udt_Id := 0;
  t_PreviousCounty   number(9);--varchar2(400);
  t_PreviousCountyName   varchar2(400);
  t_NotificationType       varchar2(25) := 'License Expiration';

  t_CombineMunis           varchar2(1) := 'N';  --'Y/N'
  t_BatchRenewalMuniEPId   udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'Municipality');
  t_BatchRenewalCountyEPId udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'County');
  t_RelId                  udt_Id;
  t_LicenseTypeSplit       extension.udt_StringList2;

begin
---------New Code

  if a_LicenseTypes is null then
    insert into abc.licensebatchjob_gt
        select ol.ObjectId,
               ol.LicenseTypeObjectId,
               ol.LicenseType,
               decode( ol.IssuingAuthority, 'State', 0, ol.RegionObjectId) CountyObjectId,
               decode( ol.IssuingAuthority, 'State', '', ol.Region) CountyName,
               ol.ExpirationDate,
               ol.IssuingAuthority
           from query.o_ABC_License ol
         where ol.ObjectDefTypeId = 1
           and (ol.ExpirationDate - nvl( ol.ExpirationReminderDays, 0 )) <= a_AsOfDate --minus or plus
           and not exists (
                           select 1
                             from abc.licensebatch_xref_t xt
                             join query.j_ABC_BatchRenewalNotification nj
                               on nj.ObjectId = xt.renewalnotifbatchjobid
                            where ol.ObjectId = xt.licenseobjectid
                              and nj.StatusName in ('INPROG', 'COMP')
                           )
          and ol.State in ('Active')
          and ol.IsSecondaryLicenseSQL = 'N'
          and ol.IsSpecialEvent = 'N'
          and ( ( a_Issuer = 'State' and ol.IssuingAuthority = ('State') ) or
                ( a_Issuer = 'Municipality' and ol.IssuingAuthority = ('Municipality') ) )
          and ol.ExpirationDate >= sysdate;
  else
    t_LicenseTypeSplit := extension.pkg_Utils.Split2(a_LicenseTypes, ', ');
    for i in 1..t_LicenseTypeSplit.Count loop
      insert into abc.licensebatchjob_gt
          select ol.ObjectId,
                 ol.LicenseTypeObjectId,
                 ol.LicenseType,
                 decode( ol.IssuingAuthority, 'State', 0, ol.RegionObjectId) CountyObjectId,
                 decode( ol.IssuingAuthority, 'State', '', ol.Region) CountyName,
                 ol.ExpirationDate,
                 ol.IssuingAuthority
             from query.o_ABC_License ol
           where ol.ObjectDefTypeId = 1
             and (ol.ExpirationDate - nvl( ol.ExpirationReminderDays, 0 )) <= a_AsOfDate --minus or plus
             and not exists (
                             select 1
                               from abc.licensebatch_xref_t xt
                               join query.j_ABC_BatchRenewalNotification nj
                                 on nj.ObjectId = xt.renewalnotifbatchjobid
                              where ol.ObjectId = xt.licenseobjectid
                                and nj.StatusName in ('INPROG', 'COMP')
                             )
            and ol.State in ('Active')
            and ol.IsSecondaryLicenseSQL = 'N'
            and ol.IsSpecialEvent = 'N'
            and ( ( a_Issuer = 'State' and ol.IssuingAuthority = ('State') ) or
                  ( a_Issuer = 'Municipality' and ol.IssuingAuthority = ('Municipality') ) )
            and ol.ExpirationDate >= sysdate
            and ol.LicenseType = t_LicenseTypeSplit(i);
      end loop;      
    end if;     
    
    for m in (
                select a.County, a.CountyObjectId, count(*) county_for_county
                from ( select gt.licenseobjectid,
                              gt.County,
                              gt.CountyObjectId
                         from abc.licensebatchjob_gt gt
                         where not exists (
                       select 1
                         from abc.licensebatch_xref_t xt
                         join query.j_ABC_BatchRenewalNotification nj
                           on nj.ObjectId = xt.renewalnotifbatchjobid
                        where gt.Licenseobjectid = xt.licenseobjectid
                          and nj.StatusName in ('INPROG', 'COMP')
                       )
                     ) a
                group by a.County, a.CountyObjectId
                order by 1
             )
             loop
--------------End New Code

         if m.county_for_county > 0 then

            if m.county_for_county > a_BatchSizeThreshold or a_Issuer = 'State' then

              --need to split out by licence type as well
              --if batch is currently being built we need to finish processing
              if t_CurrentBatchJobId <> 0 and t_PreviousCounty is not null then
                FinishBatch( t_CurrentBatchJobId,
                             t_PreviousCountyName,
                             a_UserId,
                             a_AsOfDate );
              end if;

              t_CurrentBatchJobId := 0;
              t_PreviousCounty := null;
              t_CurrentBatchSize := 0;

              AddMuniLicensesByLicTypeExp( a_BatchRequestId,
                                           m.CountyObjectId,
                                           m.County,
                                           a_BatchSizeThreshold,
                                           a_UserId,
                                           a_AsOfDate,
                                           a_Issuer );

            else

              if t_CombineMunis = 'Y' then

                 if t_CurrentBatchSize + m.county_for_county > a_BatchSizeThreshold then

                   if t_CurrentBatchJobId <> 0 and t_PreviousCounty is not null then
                     FinishBatch( t_CurrentBatchJobId,
                                  t_PreviousCountyName,
                                  a_UserId,
                                  a_AsOfDate );
                   end if;

                   t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                      m.County,
                                                      t_NotificationType,
                                                      m.CountyObjectId );
                   t_CurrentBatchSize := 0;

                 else
                   t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalCountyEPId,
                                                             t_CurrentBatchJobId,
                                                             m.CountyObjectId);
                 end if;

              else

                 if t_CurrentBatchJobId <> 0 and t_PreviousCounty is not null then
                   FinishBatch( t_CurrentBatchJobId,
                                t_PreviousCountyName,
                                a_UserId,
                                a_AsOfDate );
                 end if;

                 t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                     m.County,
                                                     t_NotificationType,
                                                     m.CountyObjectId );
                 t_CurrentBatchSize := 0;

              end if;

              --the first pass through the or after processing by License Type the CurrentBatchJobId will be 0
              if m.county_for_county > 0 and t_CurrentBatchJobId = 0 then
                  t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                     m.County,
                                                     t_NotificationType,
                                                     m.CountyObjectId );
                  t_CurrentBatchSize := 0;
              end if;

              AddLicensesToBatchExp( t_CurrentBatchJobId,
                                     m.CountyObjectId,
                                     m.County,
                                     null,
                                     a_AsOfDate,
                                     a_Issuer );
              t_CurrentBatchSize := t_CurrentBatchSize + m.county_for_county;
              t_PreviousCounty := m.CountyObjectId;
              t_PreviousCountyName := m.County;

            end if;

         end if;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_PreviousCounty is not null then
         FinishBatch( t_CurrentBatchJobId,
                      t_PreviousCountyName,
                      a_UserId,
                      a_AsOfDate );
     end if;

end;

/**** BatchRenewalLicensesGrcPd *********
* This gets the outer cursor grouped by municpality
* This procedure handles the grace period notices
*****************************************/
procedure BatchRenewalLicensesGrcPd(a_BatchRequestId udt_Id, a_AsOfDate date, a_Issuer varchar2, a_BatchSizeThreshold integer, a_UserId udt_Id,
                                    a_LicenseTypes varchar2 )
   is
  t_CurrentBatchSize       integer := 0;
  t_BatchJobTypeId         udt_Id := api.pkg_configquery.ObjectDefIdForName('j_ABC_BatchRenewalNotification');
  t_CurrentBatchJobId      udt_Id := 0;
  t_PreviousCounty   number(9);--varchar2(400);
  t_PreviousCountyName   varchar2(400);
  t_NotificationType       varchar2(25) := 'License Grace Period';

  t_CombineMunis           varchar2(1) := 'N';  --'Y/N'
  t_BatchRenewalMuniEPId   udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'Municipality');
  t_BatchRenewalCountyEPId udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'County');
  t_RelId                  udt_Id;
  t_LicenseTypeSplit       extension.udt_StringList2;

begin
-------New Code
/*
insert into abc.licensebatchjob_gt
    select ol.ObjectId,
           ol.LicenseTypeObjectId,
           ol.LicenseType,
           decode( ol.IssuingAuthority, 'State', 0, ol.OfficeObjectId ) MunicipalityObjectId,
           decode( ol.IssuingAuthority, 'State', 'State', ol.dup_Office ) Municipality,
           ol.ExpirationDate,
           ol.IssuingAuthority
       from query.o_ABC_License ol
     where ol.ObjectDefTypeId = 1
       and (ol.ExpirationDate - nvl( ol.ExpirationReminderDays, 0 )) <= a_AsOfDate --minus or plus
       and not exists (
                       select 1
                         from query.r_ABC_BatchRenewalJobLicense r
                         join query.j_ABC_BatchRenewalNotification nj
                           on nj.ObjectId = r.BatchRenewalNotificationJobId
                        where ol.ObjectId = r.LicenseObjectId
                          and nj.StatusName in ('INPROG', 'COMP')
                       )
      and ol.State in ('Active')
      and ol.IsSecondaryLicenseSQL = 'N'
      and ol.IsSpecialEvent = 'N'
      and ( ( a_Issuer = 'State' and ol.IssuingAuthority = ('State') ) or
            ( a_Issuer = 'Municipality' and ol.IssuingAuthority = ('Municipality') ) )
      and ol.ExpirationDate > sysdate;*/

  if a_LicenseTypes is null then
    insert into abc.licensebatchjobgrace_gt
          select ol.ObjectId,
          ol.LicenseTypeObjectId,
          ol.LicenseType,
          decode( ol.IssuingAuthority, 'State', 0, ol.RegionObjectId) CountyObjectId,
          decode( ol.IssuingAuthority, 'State', '', ol.Region) CountyName,
          ol.ExpirationDate,
          ol.IssuingAuthority
            from query.o_ABC_License ol
           where ol.ObjectDefTypeId = 1
             and (ol.ExpirationDate - nvl( ol.ExpirationReminderDays, 0 ) ) <= a_AsOfDate --minus or plus
            and not exists (
                            select 1
                           from abc.licensebatchgrace_xref_t xt
                             join query.j_ABC_BatchRenewalNotification nj
                               on nj.ObjectId = xt.renewalnotifbatchjobid
                            where ol.ObjectId = xt.LicenseObjectId
                              and nj.StatusName in ('INPROG')
                           )
            and ol.State in ('Active')
            and ol.IsSecondaryLicenseSQL = 'N'
            and ol.IsSpecialEvent = 'N'
            and ( ( a_Issuer = 'State' and ol.IssuingAuthority = ('State') ) or
                  ( a_Issuer = 'Municipality' and ol.IssuingAuthority = ('Municipality') ) )
            and ol.ExpirationDate < sysdate;
  else
    t_LicenseTypeSplit := extension.pkg_Utils.Split2(a_LicenseTypes, ', ');
    for i in 1..t_LicenseTypeSplit.Count loop
      insert into abc.licensebatchjobgrace_gt
            select ol.ObjectId,
            ol.LicenseTypeObjectId,
            ol.LicenseType,
            decode( ol.IssuingAuthority, 'State', 0, ol.RegionObjectId) CountyObjectId,
            decode( ol.IssuingAuthority, 'State', '', ol.Region) CountyName,
            ol.ExpirationDate,
            ol.IssuingAuthority
              from query.o_ABC_License ol
             where ol.ObjectDefTypeId = 1
               and (ol.ExpirationDate - nvl( ol.ExpirationReminderDays, 0 ) ) <= a_AsOfDate --minus or plus
              and not exists (
                              select 1
                             from abc.licensebatchgrace_xref_t xt
                               join query.j_ABC_BatchRenewalNotification nj
                                 on nj.ObjectId = xt.renewalnotifbatchjobid
                              where ol.ObjectId = xt.LicenseObjectId
                                and nj.StatusName in ('INPROG')
                             )
              and ol.State in ('Active')
              and ol.IsSecondaryLicenseSQL = 'N'
              and ol.IsSpecialEvent = 'N'
              and ( ( a_Issuer = 'State' and ol.IssuingAuthority = ('State') ) or
                    ( a_Issuer = 'Municipality' and ol.IssuingAuthority = ('Municipality') ) )
              and ol.ExpirationDate < sysdate
              and ol.LicenseType = t_LicenseTypeSplit(i);              
    end loop; 
  end if;

----End New Code
    for m in (
                select a.County,
                       a.CountyObjectId,
                       count(*) county_for_county
                from ( select gt.County,
                              gt.CountyObjectId
                        from abc.licensebatchjobgrace_gt gt
                        where not exists (
                          select 1
                         from abc.licensebatchgrace_xref_t xt
                           join query.j_ABC_BatchRenewalNotification nj
                             on nj.ObjectId = xt.renewalnotifbatchjobid
                          where gt.licenseobjectid = xt.LicenseObjectId
                            and nj.StatusName in ('INPROG')
                         )
                     ) a
                group by a.County, a.CountyObjectId
                order by 1
             )
             loop

         if m.county_for_county > 0 then

            if m.county_for_county > a_BatchSizeThreshold or a_Issuer = 'State' then

              --need to split out by licence type as well
              --if batch is currently being built we need to finish processing
              if t_CurrentBatchJobId <> 0 and t_PreviousCounty is not null then
                FinishBatch( t_CurrentBatchJobId,
                             t_PreviousCountyName,
                             a_UserId,
                             a_AsOfDate );
              end if;

              t_CurrentBatchJobId := 0;
              t_PreviousCounty := null;
              t_PreviousCountyName := null;
              t_CurrentBatchSize := 0;

              AddMuniLicensesByLicTypeGrcPd( a_BatchRequestId,
                                             m.CountyObjectId,
                                             m.County,
                                             a_BatchSizeThreshold,
                                             a_UserId,
                                             a_AsOfDate,
                                             a_Issuer );

            else

              if t_CombineMunis = 'Y' then

                 if t_CurrentBatchSize + m.county_for_county > a_BatchSizeThreshold then

                   if t_CurrentBatchJobId <> 0 and t_PreviousCounty is not null then
                     FinishBatch( t_CurrentBatchJobId,
                                  t_PreviousCountyName,
                                  a_UserId,
                                  a_AsOfDate );
                   end if;

                   t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                      m.County,
                                                      t_NotificationType,
                                                      m.CountyObjectId );
                   t_CurrentBatchSize := 0;

                 else
                   t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalCountyEPId,
                                                             t_CurrentBatchJobId,
                                                             m.CountyObjectId);
                 end if;

              else

                 if t_CurrentBatchJobId <> 0 and t_PreviousCounty is not null then
                   FinishBatch( t_CurrentBatchJobId,
                                t_PreviousCountyName,
                                a_UserId,
                                a_AsOfDate );
                 end if;

                 t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                    m.County,
                                                    t_NotificationType,
                                                    m.CountyObjectId );
                 t_CurrentBatchSize := 0;

              end if;

              --the first pass through the or after processing by License Type the CurrentBatchJobId will be 0
              if m.county_for_county > 0 and t_CurrentBatchJobId = 0 then
                  t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                     m.County,
                                                     t_NotificationType,
                                                     m.CountyObjectId );
                  t_CurrentBatchSize := 0;
              end if;

              AddLicensesToBatchGrcPd( t_CurrentBatchJobId,
                                       m.CountyObjectId,
                                       m.County,
                                       null,
                                       a_AsOfDate,
                                       a_Issuer );
              t_CurrentBatchSize := t_CurrentBatchSize + m.county_for_county;
              t_PreviousCounty := m.CountyObjectId;
              t_PreviousCountyName := m.County;

            end if;

         end if;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_PreviousCounty is not null then
         FinishBatch( t_CurrentBatchJobId,
                      t_PreviousCountyName,
                      a_UserId,
                      a_AsOfDate );
     end if;

end;

/**** BatchRenewalPermitsExp *********
* This gets the outer cursor grouped by permit type
* This procedure handles the expiration notices
***************************************/
procedure BatchRenewalPermitsExp(a_BatchRequestId udt_Id, a_AsOfDate date, a_BatchSizeThreshold integer, a_UserId udt_Id,
                                 a_PermitTypes varchar2 )
   is
  t_CurrentBatchSize       integer := 0;
  t_BatchJobTypeId         udt_Id := api.pkg_configquery.ObjectDefIdForName('j_ABC_BatchRenewalNotification');
  t_CurrentBatchJobId      udt_Id := 0;
  t_PreviousPermitType     varchar2(400);
  t_NotificationType       varchar2(25) := 'Permit Expiration';

  t_CombinePermitTypes     varchar2(1) := 'N';  --'Y/N'
  t_PermitTypeSplit        extension.udt_StringList2;

begin


  if a_PermitTypes is null then
    insert into abc.permitbatchjob_gt
    select op.ObjectId,
           op.PermitTypeObjectId,
           op.PermitType,
           op.ExpirationDate,
           op.LicenseObjectId
      from query.o_abc_permit op
      where op.ObjectDefTypeId = 1
        and op.ExpirationDate - nvl( op.ExpirationReminderDays, 0 ) <= a_AsOfDate
         and not exists (
                         select 1
                           from abc.permitbatch_xref_t r
                           join query.j_ABC_BatchRenewalNotification nj
                             on nj.ObjectId = r.renewalnotifbatchjobid
                          where op.ObjectId = r.PermitObjectId
                            and nj.StatusName in ('INPROG', 'COMP')
                     )
    and op.ExpirationDate > sysdate
    and op.State in ('Active')
    and op.PermitTypeRenewable = 'Y';
  else
    t_PermitTypeSplit := extension.pkg_Utils.Split2(a_PermitTypes, ', ');
    for i in 1..t_PermitTypeSplit.Count loop
      insert into abc.permitbatchjob_gt
      select op.ObjectId,
             op.PermitTypeObjectId,
             op.PermitType,
             op.ExpirationDate,
             op.LicenseObjectId
        from query.o_abc_permit op
        where op.ObjectDefTypeId = 1
          and op.ExpirationDate - nvl( op.ExpirationReminderDays, 0 ) <= a_AsOfDate
           and not exists (
                           select 1
                             from abc.permitbatch_xref_t r
                             join query.j_ABC_BatchRenewalNotification nj
                               on nj.ObjectId = r.renewalnotifbatchjobid
                            where op.ObjectId = r.PermitObjectId
                              and nj.StatusName in ('INPROG', 'COMP')
                       )
      and op.ExpirationDate > sysdate
      and op.State in ('Active')
      and op.PermitTypeRenewable = 'Y'
      and op.PermitType = t_PermitTypeSplit(i);
    end loop;
  end if;    


     for p in ( select gt.PermitType, count(*) count_for_PermitType
                  from abc.permitbatchjob_gt gt
                   group by gt.PermitType
                   order by 1
             ) loop

         if p.count_for_PermitType > 0 then

            if p.count_for_PermitType > a_BatchSizeThreshold then

              --add them in threshold size segments
              --if batch is currently being built we need to finish processing
              if t_CurrentBatchJobId <> 0 and t_PreviousPermitType is not null then
                FinishBatch( t_CurrentBatchJobId,
                             t_PreviousPermitType,
                             a_UserId,
                             a_AsOfDate );
              end if;

              t_CurrentBatchJobId := 0;
              t_PreviousPermitType := null;
              t_CurrentBatchSize := 0;

              AddPermitTypeBySegmentExp(a_BatchRequestId,
                                        p.PermitType,
                                        a_BatchSizeThreshold,
                                        a_UserId,
                                        a_AsOfDate );

            else

              if t_CombinePermitTypes = 'Y' then

                 if t_CurrentBatchSize + p.count_for_PermitType > a_BatchSizeThreshold then

                   if t_CurrentBatchJobId <> 0 and t_PreviousPermitType is not null then
                     FinishBatch( t_CurrentBatchJobId,
                                  t_PreviousPermitType,
                                  a_UserId,
                                  a_AsOfDate );
                   end if;

                   t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                      p.PermitType,
                                                      t_NotificationType );
                   t_CurrentBatchSize := 0;

                 end if;

              else

                 if t_CurrentBatchJobId <> 0 and t_PreviousPermitType is not null then
                   FinishBatch( t_CurrentBatchJobId,
                                t_PreviousPermitType,
                                a_UserId,
                                a_AsOfDate );
                 end if;

                 t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                    p.PermitType,
                                                    t_NotificationType );
                 t_CurrentBatchSize := 0;

              end if;

              --the first pass through the or after processing by License Type the CurrentBatchJobId will be 0
              if p.count_for_PermitType > 0 and t_CurrentBatchJobId = 0 then
                  t_CurrentBatchJobId := StartBatch( a_BatchRequestId, p.PermitType, t_NotificationType );
                  t_CurrentBatchSize := 0;
              end if;

              AddPermitsToBatchExp( t_CurrentBatchJobId,
                                    p.PermitType,
                                    a_AsOfDate );
              t_CurrentBatchSize := t_CurrentBatchSize + p.count_for_PermitType;
              t_PreviousPermitType := p.PermitType;

            end if;

         end if;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_PreviousPermitType is not null then
         FinishBatch( t_CurrentBatchJobId, t_PreviousPermitType, a_UserId, a_AsOfDate );
     end if;

end;

/**** BatchRenewalPermitsGrcPd *********
* This gets the outer cursor grouped by permit type
* This procedure handles the grace period notices
***************************************/
procedure BatchRenewalPermitsGrcPd(a_BatchRequestId udt_Id, a_AsOfDate date, a_BatchSizeThreshold integer, a_UserId udt_Id,
                                   a_PermitTypes varchar2 )
   is
  t_CurrentBatchSize       integer := 0;
  t_BatchJobTypeId         udt_Id := api.pkg_configquery.ObjectDefIdForName('j_ABC_BatchRenewalNotification');
  t_CurrentBatchJobId      udt_Id := 0;
  t_PreviousPermitType     varchar2(400);
  t_NotificationType       varchar2(25) := 'Permit Grace Period';

  t_CombinePermitTypes     varchar2(1) := 'N';  --'Y/N'
  t_PermitTypeSplit        extension.udt_StringList2;

begin

  if a_PermitTypes is null then
     insert into abc.permitbatchjobgrace_gt
     select op.ObjectId,
            op.PermitTypeObjectId,
            op.PermitType,
            op.ExpirationDate,
            op.LicenseObjectId
            from query.o_abc_permit op
            where op.ObjectDefTypeId = 1
              and op.ExpirationDate + nvl( op.GracePeriodDays, 0 ) - nvl( op.ExpirationReminderDays, 0 ) <= a_AsOfDate
               and not exists (
                               select 1
                                 from abc.permitbatchgrace_xref_t r
                                 join query.j_ABC_BatchRenewalNotification nj
                                   on nj.ObjectId = r.renewalnotifbatchjobid
                                where op.ObjectId = r.PermitObjectId
                                  and nj.StatusName in ('INPROG')
                               )
              and op.State in ('Active')
              and op.PermitTypeRenewable = 'Y'
              and op.ExpirationDate < sysdate;
  else
    t_PermitTypeSplit := extension.pkg_Utils.Split2(a_PermitTypes, ', ');
    for i in 1..t_PermitTypeSplit.Count loop
      insert into abc.permitbatchjobgrace_gt
      select op.ObjectId,
              op.PermitTypeObjectId,
              op.PermitType,
              op.ExpirationDate,
              op.LicenseObjectId
              from query.o_abc_permit op
              where op.ObjectDefTypeId = 1
                and op.ExpirationDate + nvl( op.GracePeriodDays, 0 ) - nvl( op.ExpirationReminderDays, 0 ) <= a_AsOfDate
                 and not exists (
                                 select 1
                                   from abc.permitbatchgrace_xref_t r
                                   join query.j_ABC_BatchRenewalNotification nj
                                     on nj.ObjectId = r.renewalnotifbatchjobid
                                  where op.ObjectId = r.PermitObjectId
                                    and nj.StatusName in ('INPROG')
                                 )
                and op.State in ('Active')
                and op.PermitTypeRenewable = 'Y'
                and op.ExpirationDate < sysdate
                and op.PermitType = t_PermitTypeSplit(i);
    end loop;              
  end if;

     for p in (
                select gt.PermitType,
                       count(*) count_for_PermitType
                  from abc.permitbatchjobgrace_gt gt
                   group by gt.PermitType
                   order by 1
             ) loop

         if p.count_for_PermitType > 0 then

            if p.count_for_PermitType > a_BatchSizeThreshold then

              --add them in threshold size segments
              --if batch is currently being built we need to finish processing
              if t_CurrentBatchJobId <> 0 and t_PreviousPermitType is not null then
                FinishBatch( t_CurrentBatchJobId,
                             t_PreviousPermitType,
                             a_UserId,
                             a_AsOfDate );
              end if;

              t_CurrentBatchJobId := 0;
              t_PreviousPermitType := null;
              t_CurrentBatchSize := 0;

              AddPermitTypeBySegmentGrcPd(a_BatchRequestId,
                                          p.PermitType,
                                          a_BatchSizeThreshold,
                                          a_UserId,
                                          a_AsOfDate );

            else

              if t_CombinePermitTypes = 'Y' then

                 if t_CurrentBatchSize + p.count_for_PermitType > a_BatchSizeThreshold then

                   if t_CurrentBatchJobId <> 0 and t_PreviousPermitType is not null then
                     FinishBatch( t_CurrentBatchJobId,
                                  t_PreviousPermitType,
                                  a_UserId,
                                  a_AsOfDate );
                   end if;

                   t_CurrentBatchJobId := StartBatch( a_BatchRequestId,
                                                      p.PermitType,
                                                      t_NotificationType );
                   t_CurrentBatchSize := 0;

                 end if;

              else

                 if t_CurrentBatchJobId <> 0 and t_PreviousPermitType is not null then
                   FinishBatch( t_CurrentBatchJobId,
                                t_PreviousPermitType,
                                a_UserId,
                                a_AsOfDate );
                 end if;

                 t_CurrentBatchJobId := StartBatch( a_BatchRequestId, p.PermitType, t_NotificationType );
                 t_CurrentBatchSize := 0;

              end if;

              --the first pass through the or after processing by License Type the CurrentBatchJobId will be 0
              if p.count_for_PermitType > 0 and t_CurrentBatchJobId = 0 then
                  t_CurrentBatchJobId := StartBatch( a_BatchRequestId, p.PermitType, t_NotificationType );
                  t_CurrentBatchSize := 0;
              end if;

              AddPermitsToBatchGrcpd( t_CurrentBatchJobId, p.PermitType, a_AsOfDate );
              t_CurrentBatchSize := t_CurrentBatchSize + p.count_for_PermitType;
              t_PreviousPermitType := p.PermitType;

            end if;

         end if;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_PreviousPermitType is not null then
         FinishBatch( t_CurrentBatchJobId, t_PreviousPermitType, a_UserId, a_AsOfDate );
     end if;

end;

/**** BatchRenewalProductsExp *********
* This gets the outer cursor grouped by the registrants first initial
***************************************/
procedure BatchRenewalProductsExp(a_BatchRequestId udt_Id, a_AsOfDate date, a_ProductNotificationYear number, a_BatchSizeThreshold integer, a_UserId udt_Id )
   is
  t_CurrentBatchSize       integer := 0;
  t_BatchJobTypeId         udt_Id := api.pkg_configquery.ObjectDefIdForName('j_ABC_BatchRenewalNotification');
  t_CurrentBatchJobId      udt_Id := 0;
  t_PreviousInitial        varchar2(1);
  t_NotificationType       varchar2(25) := 'Product Expiration';
  t_ProductRenewalCycleObjectId udt_Id;
  t_BatchRenewalProdPRCEPId     udt_Id :=   api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'ProdRenCyc');
  t_RelId                  udt_Id;

  t_CombineRegistrants     varchar2(1) := 'Y';  --'Y/N'

begin

    t_ProductRenewalCycleObjectId := api.pkg_simplesearch.ObjectByIndex( 'o_ABC_ProductRenewalCycle', 'RenewalCycle', a_ProductNotificationYear );

    if t_ProductRenewalCycleObjectId is null then
      t_ProductRenewalCycleObjectId := api.pkg_objectupdate.New( api.pkg_configquery.ObjectDefIdForName('o_ABC_ProductRenewalCycle') , a_AsOfDate );
      api.pkg_columnupdate.SetValue( t_ProductRenewalCycleObjectId, 'RenewalCycle', a_ProductNotificationYear );
    end if;

     for p in (
                select l.LegalEntityInitial RegistrantInitial,
                       count(*) count_by_RegistrantInitial
                  from query.o_abc_legalentity l
                  where exists ( select r.RegistrantObjectId
                                   from query.r_ABC_ProductRegistrant r
                                   join query.o_abc_product pr on pr.ObjectId = r.ProductObjectId
                                   where r.RegistrantObjectId = l.ObjectId
                                     and pr.State in ('Current')
                                     and to_char( pr.ExpirationDate, 'YYYY' ) = to_char( a_ProductNotificationYear )
                               )
                    --and l.LegalEntityInitial ='A'
                  group by l.LegalEntityInitial
                  order by 1
             ) loop

         if p.count_by_RegistrantInitial > 0 then

            if p.count_by_RegistrantInitial > a_BatchSizeThreshold then

              --add them in threshold size segments
              --if batch is currently being built we need to finish processing
              if t_CurrentBatchJobId <> 0 and t_PreviousInitial is not null then
                FinishBatch( t_CurrentBatchJobId, t_PreviousInitial, a_UserId, a_AsOfDate );
              end if;

              t_CurrentBatchJobId := 0;
              t_PreviousInitial := null;
              t_CurrentBatchSize := 0;

              AddProdRegistrantsBySegmentExp(a_BatchRequestId, p.RegistrantInitial, a_BatchSizeThreshold, a_UserId, a_AsOfDate, a_ProductNotificationYear, t_ProductRenewalCycleObjectId );

            else

              if t_CombineRegistrants = 'Y' then

                 if t_CurrentBatchSize + p.count_by_RegistrantInitial > a_BatchSizeThreshold then

                   if t_CurrentBatchJobId <> 0 and t_PreviousInitial is not null then
                     FinishBatch( t_CurrentBatchJobId, t_PreviousInitial, a_UserId, a_AsOfDate );
                   end if;

                   t_CurrentBatchJobId := StartBatch( a_BatchRequestId, p.RegistrantInitial, t_NotificationType );
                   t_CurrentBatchSize := 0;
                   t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalProdPRCEPId, t_CurrentBatchJobId, t_ProductRenewalCycleObjectId);

                 end if;

              else

                 if t_CurrentBatchJobId <> 0 and t_PreviousInitial is not null then
                   FinishBatch( t_CurrentBatchJobId, t_PreviousInitial, a_UserId, a_AsOfDate );
                 end if;

                 t_CurrentBatchJobId := StartBatch( a_BatchRequestId, p.RegistrantInitial, t_NotificationType );
                 t_CurrentBatchSize := 0;
                 t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalProdPRCEPId, t_CurrentBatchJobId, t_ProductRenewalCycleObjectId);

              end if;

              --the first pass through the or after processing by License Type the CurrentBatchJobId will be 0
              if p.count_by_RegistrantInitial > 0 and t_CurrentBatchJobId = 0 then
                  t_CurrentBatchJobId := StartBatch( a_BatchRequestId, p.RegistrantInitial, t_NotificationType );
                  t_CurrentBatchSize := 0;
                  t_RelId := api.pkg_RelationshipUpdate.New(t_BatchRenewalProdPRCEPId, t_CurrentBatchJobId, t_ProductRenewalCycleObjectId);
              end if;

              AddProdRegistrantsToBatchExp( t_CurrentBatchJobId, p.RegistrantInitial, a_AsOfDate, a_ProductNotificationYear, t_ProductRenewalCycleObjectId );
              t_CurrentBatchSize := t_CurrentBatchSize + p.count_by_RegistrantInitial;
              t_PreviousInitial := p.RegistrantInitial;

            end if;

         end if;

     end loop;

     --finish the last batch
     if t_CurrentBatchJobId <> 0 and t_PreviousInitial is not null then
         FinishBatch( t_CurrentBatchJobId, t_PreviousInitial, a_UserId, a_AsOfDate );
     end if;

end;

/**** StartBatchRenewalRequest *********
* This procedure is called from the process to kick-off the processing
* It determines the batch type and calls the appropriate procedures
****************************************/
procedure StartBatchRenewalRequest(a_ProcessId udt_Id, a_AsOfDate date) is
  t_BatchType                varchar2(100);
  t_JobId                    udt_Id;
  t_ProductNotificationYear  number(4);
  t_BatchSizeThreshold       integer;
  t_UserIdRetail             udt_Id := 0;
  t_UserIdWholesale          udt_Id := 0;
  t_UserIdPermit             udt_Id := 0;
  t_UserIdProduct            udt_Id := 0;
  t_ProductRenewalReminderDays  integer;
  t_LicenseTypes             varchar2(4000);
  t_PermitTypes              varchar2(4000);
begin

    t_JobId := api.pkg_columnquery.NumericValue( a_ProcessId, 'JobId' );
    t_BatchType := api.pkg_columnquery.Value( t_JobId, 'BatchType' );
    t_ProductNotificationYear := api.pkg_columnquery.NumericValue( t_JobId, 'ProductNotificationYear' );

    -- Get some information from System Settings
    begin
      select o.BatchNotificationBatchSize, o.BatchNotifDefaultClerkIdRetail, o.BatchNotifDefaultClerkIdWhlsal, o.BatchNotifDefaultClerkIdPermit, o.BatchNotifDefaultClerkIdProd, o.ProductRenewalReminderDays
        into t_BatchSizeThreshold, t_UserIdRetail, t_UserIdWholesale, t_UserIdPermit, t_UserIdProduct, t_ProductRenewalReminderDays
        from query.o_SystemSettings o
        where o.ObjectDefTypeId = 1
          and rownum = 1;
    exception
      when no_data_found then
        raise_application_error(-20000, 'System Settings Object is not configured.');
    end;

    --set batch size to 250 if it's not configured
    t_BatchSizeThreshold := nvl( t_BatchSizeThreshold, 250 );
    if t_BatchType = 'License - Municipality Issued' then
      t_LicenseTypes := api.pkg_columnquery.Value(t_JobId, 'PermitLicenseType');
      BatchRenewalLicensesExp(t_JobId, a_AsOfDate, 'Municipality', t_BatchSizeThreshold, t_UserIdRetail, t_LicenseTypes);
      BatchRenewalLicensesGrcPd(t_JobId, a_AsOfDate, 'Municipality', t_BatchSizeThreshold, t_UserIdRetail, t_LicenseTypes);

    elsif t_BatchType = 'License - State Issued' then
      t_LicenseTypes := api.pkg_columnquery.Value(t_JobId, 'PermitLicenseType');
      BatchRenewalLicensesExp(t_JobId, a_AsOfDate, 'State', t_BatchSizeThreshold, t_UserIdWholesale, t_LicenseTypes);
      BatchRenewalLicensesGrcPd(t_JobId, a_AsOfDate, 'State', t_BatchSizeThreshold, t_UserIdWholesale, t_LicenseTypes);

    elsif t_BatchType = 'Permit' then
      t_PermitTypes := api.pkg_columnquery.Value(t_JobId, 'PermitLicenseType');
      BatchRenewalPermitsExp(t_JobId, a_AsOfDate, t_BatchSizeThreshold, t_UserIdPermit, t_PermitTypes);
      BatchRenewalPermitsGrcPd(t_JobId, a_AsOfDate, t_BatchSizeThreshold, t_UserIdPermit, t_PermitTypes);

    elsif t_BatchType = 'Product' then

      if ( to_date( t_ProductNotificationYear || '-DEC-31', 'YYYY-MON-DD' ) - nvl( t_ProductRenewalReminderDays, 0 )) <= a_AsOfDate then
        BatchRenewalProductsExp(t_JobId, a_AsOfDate, t_ProductNotificationYear, t_BatchSizeThreshold, t_UserIdProduct);
      end if;

    end if;

    api.pkg_processupdate.Complete( a_ProcessId, 'Completed');

end StartBatchRenewalRequest;

  /*---------------------------------------------------------------------------
   * SendCustomerEmailNotifications. This is called from a system process and 
   * sends email notifications
   *-------------------------------------------------------------------------*/
  procedure SendCustomerEmailNotifications(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_FromAddress                       varchar2(100);
    t_ToAddress                         varchar2(4000);
    t_CCAddress                         varchar2(4000);
    t_BccAddress                        varchar2(4000);
    t_Subject                           varchar2(4000);
    t_Body                              varchar2(32767);
    t_DocumentId                        varchar2(20);
    t_DocumentEndPointName              varchar2(100);
    t_BodyIsHTML                        varchar2(1) := 'Y';
    t_AttachAllDocs                     varchar2(1);
    t_LicenseSubject                    varchar2(4000);
    t_PermitSubject                     varchar2(4000);
    t_ProductSubject                    varchar2(4000);
    t_LicenseBody                       varchar2(32767);
    t_PermitBody                        varchar2(32767);
    t_ProductBody                       varchar2(32767);
    t_SystemSettingsObjectId            udt_Id;
    t_ExternalWebsiteBaseURL            varchar2(4000);
    t_IsProd                            varchar2(1);
    t_JobId                             udt_Id;
    t_ToEmailAddressNonProd             varchar2(100);
    t_ReliefRequiredText1218            varchar2(4000);
    t_ReliefRequiredText1239            varchar2(4000);
    t_NotifyText                        varchar2(4000);
    t_PublicUsers                       varchar2(32767);
  begin
    
    select p.JobId
    into t_JobId
    from api.processes p
    where p.ProcessId = a_ProcessId;

    -- Get some information from System Settings
    begin
      select
        o.ObjectId,
        o.IsProduction,
        o.FromEmailAddress,
        o.LicenseEmailSubject,
        o.PermitEmailSubject,
        o.ProductEmailSubject,
        o.ExternalWebsiteBaseURL,
        o.ReliefRequiredText1218,
        o.ReliefRequiredText1239,
        o.ToEmailAddressNonProd,
        o.PermitLetterText
      into 
        t_SystemSettingsObjectId,
        t_IsProd,
        t_FromAddress,
        t_LicenseSubject,
        t_PermitSubject,
        t_ProductSubject,
        t_ExternalWebsiteBaseURL,
        t_ReliefRequiredText1218,
        t_ReliefRequiredText1239,
        t_ToEmailAddressNonProd,
        t_NotifyText
      from query.o_SystemSettings o
      where o.ObjectDefTypeId = 1
        and rownum = 1;
    exception
      when no_data_found then
        raise_application_error(-20000, 'System Settings Object is not ' ||
            'configured.');
    end;

    -- Get Note Text (Email Body)
    select n.Text
    into t_LicenseBody
    from api.notes n
    join api.notedefs nd
        on nd.NoteDefId = n.NoteDefId
    join api.objectdefnotedefs odnd
        on odnd.NoteDefId = nd.NoteDefId
        and odnd.ObjectDefId = 
            api.pkg_configquery.ObjectDefIdForName('o_SystemSettings')
    where nd.Tag = 'BRLIC'
      and n.ObjectId = t_SystemSettingsObjectId;

    select n.Text
    into t_PermitBody
    from api.notes n
    join api.notedefs nd
        on nd.NoteDefId = n.NoteDefId
    join api.objectdefnotedefs odnd
        on odnd.NoteDefId = nd.NoteDefId 
        and odnd.ObjectDefId = 
            api.pkg_configquery.ObjectDefIdForName('o_SystemSettings')
    where nd.Tag = 'BRPERM'
      and n.ObjectId = t_SystemSettingsObjectId;

    select n.Text
    into t_ProductBody
    from api.notes n
    join api.notedefs nd
        on nd.NoteDefId = n.NoteDefId
    join api.objectdefnotedefs odnd
        on odnd.NoteDefId = nd.NoteDefId
        and odnd.ObjectDefId = 
            api.pkg_configquery.ObjectDefIdForName('o_SystemSettings')
    where nd.Tag = 'BRPROD'
      and n.ObjectId = t_SystemSettingsObjectId;

    -- License Expiration Notifications
    for ln in (
        select
          l.ObjectId ObjectId,
          l.EstablishmentLocationAddress EstablishmentAddress,
          l.LicenseNumber LicenseNumber,
          l.LicenseType LicenseType,
          l.Licensee Licensee,
          le.ObjectId LicenseeObjectId,
          l.Establishment Establishment,
          to_char(l.ExpirationDate,'fmMonth DD, YYYY') ExpirationDate,
          l.ReliefRequired1218 ReliefRequired1218,
          l.ReliefRequired1239 ReliefRequired1239,
          le.MailingAddress MailingAddress,
          le.PreferredContactMethod PreferredContactMethod,
          le.ContactEmailAddress ContactEmailAddress
        from query.j_abc_batchrenewalnotification j
        join abc.licensebatchgrace_xref_t rg
            on rg.renewalnotifbatchjobid = j.JobId
        join query.o_abc_license l
            on l.ObjectId = rg.LicenseObjectId
        join query.r_ABC_LicenseLicenseeLE rle
            on rle.LicenseObjectId = l.objectid
        join query.o_abc_legalentity le
            on le.objectid = rle.LegalEntityObjectId
        where j.objectid = t_JobId
          and (le.PreferredContactMethod is not null
              and le.PreferredContactMethod = 'Email')
          and le.PreferredContactMethod is not null
          and le.Active = 'Y'
        UNION ALL
        select
          l2.ObjectId ObjectId,
          l2.EstablishmentLocationAddress EstablishmentAddress,
          l2.LicenseNumber LicenseNumber,
          l2.LicenseType LicenseType,
          l2.Licensee Licensee,
          le.ObjectId LicenseeObjectId,
          l2.Establishment Establishment,
          to_char(l2.ExpirationDate,'fmMonth DD, YYYY') ExpirationDate,
          l2.ReliefRequired1218 ReliefRequired1218,
          l2.ReliefRequired1239 ReliefRequired1239,
          le.MailingAddress MailingAddress,
          le.PreferredContactMethod PreferredContactMethod,
          le.ContactEmailAddress ContactEmailAddress
        from query.j_abc_batchrenewalnotification j
        join abc.licensebatch_xref_t rl
            on rl.renewalnotifbatchjobid = j.jobid
        join query.o_abc_license l2
            on l2.objectid = rl.LicenseObjectId
        join query.r_ABC_LicenseLicenseeLE rle
            on rle.LicenseObjectId = l2.objectid
        join query.o_abc_legalentity le
            on le.objectid = rle.LegalEntityObjectId
        where j.objectid = t_JobId
          and (le.PreferredContactMethod is not null
              and le.PreferredContactMethod = 'Email')
          and le.ContactEmailAddress is not null
          and le.Active = 'Y'
        ) loop

      t_PublicUsers := RetrieveOnlineUserEmail (ln.licenseeobjectid);
      t_Subject := t_LicenseSubject;
      t_Body := t_LicenseBody;
      
      t_Body := replace(t_Body, '{Licensee}', ln.Licensee);
      t_Body := replace(t_Body, '{LicenseNumber}', ln.LicenseNumber);
      t_Body := replace(t_Body, '{Establishment}', ln.Establishment);
      t_Body := replace(t_Body, '{ExpirationDate}', ln.ExpirationDate);
      t_Body := replace(t_Body, '{Link}', t_ExternalWebsiteBaseURL);
      t_Body := replace(t_Body, '{LicenseType}', ln.Licensetype);
      t_Body := replace(t_Body, '{PreferredContact}', 'Preferred Contact: ' ||
          ln.ContactEmailAddress);
      t_Body := replace(t_Body, '{OnlineUsers}', t_PublicUsers);
      if ln.reliefrequired1218 = 'Y' then
        t_Body := replace(t_Body, '{12.18Relief}', t_ReliefRequiredText1218);
      else
        t_Body := replace(t_Body, '{12.18Relief}', '');
      end if;
      if ln.reliefrequired1239 = 'Y' then
        t_Body := replace(t_Body, '{12.39Relief}', t_ReliefRequiredText1239);
      else
        t_Body := replace(t_Body, '{12.39Relief}', '');
      end if;
      t_Body := pkg_abc_email.InsertLink(t_Body, t_ExternalWebsiteBaseURL);

      -- Append a warning to the beginning of the body if this is a non-production
      -- environment.
      if t_IsProd = 'N' then
        -- Leave 200 for this html, and 200 for the email headers added later.
        if Length(t_Body) < 32300 then
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a non-' ||
              'production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a non-' ||
              'production system.<br><br>Message too long... truncated.</span><br>';
        end if;
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromAddress);
      else
        t_ToAddress := ln.ContactEmailAddress;
      end if;

      extension.pkg_sendmail.SendEmail(ln.ObjectId, t_FromAddress, t_ToAddress,
          t_CCAddress, t_BccAddress, t_Subject, t_Body, t_DocumentId,
          t_DocumentEndPointName, t_BodyIsHTML, t_AttachAllDocs);

      -- Find associated Public Users and send email (only for Production)
      if t_IsProd = 'Y' then
        for e in (
            select
              ule.UserId,
              api.pkg_columnquery.Value(ule.UserId, 'EmailAddress') EmailAddress
            from  query.r_ABC_UserLegalEntity ule
            where ule.LegalEntityObjectId = ln.licenseeobjectid
            ) loop
          if api.pkg_columnquery.Value(e.userid, 'Active') = 'Y' then
            if ltrim(rtrim(lower(e.emailaddress))) != 
                ltrim(rtrim(lower(t_ToAddress))) then
              extension.pkg_sendmail.SendEmail(ln.ObjectId, t_FromAddress,
                  e.emailaddress, t_CCAddress, t_BccAddress, t_Subject, t_Body,
                  t_DocumentId, t_DocumentEndPointName, t_BodyIsHTML,
                  t_AttachAllDocs);
            end if;
          end if;
        end loop;
      end if;
    end loop;

    -- Permit Expiration Notifications
    for pn in (
        select
          p.ObjectId ObjectId,
          le.dup_FormattedName Permittee,
          p.PermitNumber PermitNumber,
          p.PermitType PermitType,
          to_char(p.ExpirationDate,'fmMonth DD, YYYY') ExpirationDate,
          p.LicenseEstablishment Establishment,
          p.LicenseEstablishmentLocation EstablishmentAddress,
          api.pkg_ColumnQuery.NumericValue(p.ObjectId, 'FormattedPermitteeObjectId')
              PermitteeObjectID,
          le.dup_FormattedName LegalName,
          le.MailingAddress MailingAddress,
          le.PreferredContactMethod PreferredContactMethod,
          le.ContactEmailAddress ContactEmailAddress,
          (case
            when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
              p.EventLocAddressDescription
            when p.PermitTypeCode = 'SOL' then
              p.SolicitorName
          end) AddDetails1,
          (case
            when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
              'Location:'
            when p.PermitTypeCode = 'SOL' then
              'Solicitor:'
          end) AddLabels1,
          (case
            when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
              p.EventLocationAddress
          end) AddDetails2
        from query.j_abc_batchrenewalnotification j
        join abc.permitbatchgrace_xref_t rg
            on rg.renewalnotifbatchjobid = j.JobId
        join query.o_abc_permit p
            on p.ObjectId = rg.PermitObjectId
        join query.o_abc_legalentity le
            on le.objectid = p.dup_FormattedPermitteeObjectId
        where j.objectid = t_JobId
          and (le.PreferredContactMethod is not null
              and le.PreferredContactMethod = 'Email')
          and le.PreferredContactMethod is not null
          and le.Active = 'Y'
        UNION ALL
        select
          p2.ObjectId ObjectId,
          le.dup_FormattedName Permittee,
          p2.PermitNumber PermitNumber,
          p2.PermitType PermitType,
          to_char(p2.ExpirationDate,'fmMonth DD, YYYY') ExpirationDate,
          p2.LicenseEstablishment Establishment,
          p2.LicenseEstablishmentLocation EstablishmentAddress,
          api.pkg_ColumnQuery.NumericValue(p2.ObjectId, 'FormattedPermitteeObjectId')
              PermitteeObjectID,
          le.dup_FormattedName LegalName,
          le.MailingAddress MailingAddress,
          le.PreferredContactMethod PreferredContactMethod,
          le.ContactEmailAddress ContactEmailAddress,
          (case
            when p2.RequiresLocation = 'Y' and p2.RequiresEvents = 'N' then
              p2.EventLocAddressDescription
            when p2.PermitTypeCode = 'SOL' then
              p2.SolicitorName
          end) AddDetails1,
          (case
            when p2.RequiresLocation = 'Y' and p2.RequiresEvents = 'N' then
              'Location:'
            when p2.PermitTypeCode = 'SOL' then
              'Solicitor:'
          end) AddLabels1,
          (case
            when p2.RequiresLocation = 'Y' and p2.RequiresEvents = 'N' then
              p2.EventLocationAddress
          end) AddDetails2
        from query.j_abc_batchrenewalnotification j
        join abc.permitbatch_xref_t rl
            on rl.renewalnotifbatchjobid = j.jobid
        join query.o_abc_permit p2
            on p2.objectid = rl.PermitObjectId
        join query.o_abc_legalentity le
            on le.objectid = p2.dup_FormattedPermitteeObjectId
        where j.objectid = t_JobId
          and (le.PreferredContactMethod is not null
              and le.PreferredContactMethod = 'Email')
          and le.PreferredContactMethod is not null
          and le.Active = 'Y'
        ) loop
        
      t_PublicUsers := RetrieveOnlineUserEmail (pn.Permitteeobjectid);
      t_Subject := t_PermitSubject;
      t_Body := t_PermitBody;
      
      t_Body := replace(t_Body, '{Permitee}', pn.Permittee);
      t_Body := replace(t_Body, '{PermitType}', pn.PermitType);
      t_Body := replace(t_Body, '{PermitNumber}', pn.PermitNumber);
      t_Body := replace(t_Body, '{Establishment}', pn.Establishment);
      t_Body := replace(t_Body, '{EstablishmentAddress}', pn.EstablishmentAddress);
      t_Body := replace(t_Body, '{ExpirationDate}', pn.ExpirationDate);
      t_Body := replace(t_Body, '{Link}', t_ExternalWebsiteBaseURL);
      t_Body := replace(t_Body, '{NotifyText}', t_NotifyText);
      t_Body := replace(t_Body, '{AddDetails1}', pn.Adddetails1);
      t_Body := replace(t_Body, '{AddDetails2}', pn.Adddetails2);
      t_Body := replace(t_Body, '{AddLabels1}', pn.AddLabels1);
      t_Body := replace(t_Body, '{PreferredContact}', 'Preferred Contact: ' ||
          pn.ContactEmailAddress);
      t_Body := replace(t_Body, '{OnlineUsers}', t_PublicUsers);
      t_Body := pkg_abc_email.InsertLink(t_Body, t_ExternalWebsiteBaseURL);

      -- Append a warning to the beginning of the body if this is a non-production 
      -- environment.
      if t_IsProd = 'N' then
        -- Leave 200 for this html, and 200 for the email headers added later.
        if Length(t_Body) < 32300 then 
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a non-' ||
              'production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a non-' ||
              'production system.<br><br>Message too long... truncated.</span><br>';
        end if;
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromAddress);
      else
        t_ToAddress := pn.ContactEmailAddress;
      end if;

      extension.pkg_sendmail.SendEmail(pn.ObjectId, t_FromAddress, t_ToAddress,
          t_CCAddress, t_BccAddress, t_Subject, t_Body, t_DocumentId,
          t_DocumentEndPointName, t_BodyIsHTML, t_AttachAllDocs);
                                        
      -- Find associated Public Users and send email (only for Production)
      if t_IsProd = 'Y' then
        for e in (
            select ule.UserId, api.pkg_columnquery.Value(ule.UserId, 'EmailAddress')
                EmailAddress
            from query.r_ABC_UserLegalEntity ule
            where ule.LegalEntityObjectId = pn.permitteeobjectid
            ) loop
          if api.pkg_columnquery.Value(e.userid, 'Active') = 'Y' then
            if ltrim(rtrim(lower(e.emailaddress))) != 
                ltrim(rtrim(lower(t_ToAddress))) then
              extension.pkg_sendmail.SendEmail( pn.ObjectId, t_FromAddress,
                e.EmailAddress, t_CCAddress, t_BccAddress, t_Subject, t_Body,
                t_DocumentId, t_DocumentEndPointName, t_BodyIsHTML,
                t_AttachAllDocs);
            end if;
          end if;
        end loop;
      end if;                                                          
    end loop;

    --Product Expiration Notifications
    for pe in (
        select
          ol.ObjectId,
          ol.dup_FormattedName Registrant,
          ol.ContactEmailAddress,
          oprc.RenewalCycle,
          r.ProductCount,
          to_char(r.EarliestProductExpirationDate, 'fmMonth DD, YYYY')  ExpirationDate
        from abc.RegistrantNotifications_t rt
        join query.r_ABC_RegistrantProdRenewCycle r
            on r.ProductRenewalCycleObjectId = rt.renewalcycleobjectid
            and r.RegistrantObjectId = rt.registrantobjectid
        join query.o_abc_productrenewalcycle oprc
            on oprc.ObjectId = rt.Renewalcycleobjectid
        -- Only need this if we are going to use RenewCycle
        join query.o_abc_legalentity ol
            on ol.ObjectId = rt.RegistrantObjectid  
        where rt.notificationjobid = t_JobId
          and (ol.PreferredContactMethod is not null
              and ol.PreferredContactMethod = 'Email')
          and ol.ContactEmailAddress is not null
          and ol.Active = 'Y'
        ) loop
        
      t_PublicUsers := RetrieveOnlineUserEmail(pe.Objectid);
      t_Subject := t_ProductSubject;
      t_Body := t_ProductBody;

      t_Body := replace(t_Body, '{Registrant}', pe.Registrant);
      t_Body := replace(t_Body, '{ProductCount}', pe.ProductCount);
      t_Body := replace(t_Body, '{ExpirationDate}', pe.ExpirationDate);
      t_Body := replace(t_Body, '{PreferredContact}', 'Preferred Contact: ' ||
          pe.ContactEmailAddress);
      t_Body := replace(t_Body, '{OnlineUsers}', t_PublicUsers);
      t_Body := replace(t_Body, '{Link}', t_ExternalWebsiteBaseURL);
      t_Body := pkg_abc_email.InsertLink(t_Body, t_ExternalWebsiteBaseURL);

      -- Append a warning to the beginning of the body if this is a non-production 
      -- environment.
      if t_IsProd = 'N' then
        -- Leave 200 for this html, and 200 for the email headers added later.
        if Length(t_Body) < 32300 then 
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a non-' ||
              'production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a non-' ||
              'production system.<br><br>Message too long... truncated.</span><br>';
        end if;
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromAddress);
      else
        t_ToAddress := pe.ContactEmailAddress;
      end if;
        
      t_body := abc.pkg_abc_email.EmailFormat(t_body);

      extension.pkg_sendmail.SendEmail( pe.ObjectId, t_FromAddress, t_ToAddress,
          t_CCAddress, t_BccAddress, t_Subject, t_Body, t_DocumentId,
          t_DocumentEndPointName, t_BodyIsHTML, t_AttachAllDocs);

      -- Find associated Public Users and send email (only for Production)
      if t_IsProd = 'Y' then
        for e in (
            select
              ule.UserId,
              api.pkg_columnquery.Value(ule.UserId, 'EmailAddress') EmailAddress
            from   query.r_ABC_UserLegalEntity ule
            where  ule.LegalEntityObjectId = pe.Objectid
            ) loop
          if api.pkg_columnquery.Value(e.userid, 'Active') = 'Y' then
            if ltrim(rtrim(lower(e.emailaddress))) != 
                  ltrim(rtrim(lower(t_ToAddress))) then
              extension.pkg_sendmail.SendEmail( pe.ObjectId, t_FromAddress,
                  e.EmailAddress, t_CCAddress, t_BccAddress, t_Subject, t_Body, 
                  t_DocumentId, t_DocumentEndPointName, t_BodyIsHTML,
                  t_AttachAllDocs);
            end if;
          end if;
        end loop;
      end if;                                        
    end loop;

  end;

  /*---------------------------------------------------------------------------
   * SendMuniEmailNotification() -- PUBLIC
   *   Send emails to Municipal users for "License - Municipality Issued" batch
   * renewals. This procedure is called from a system process and sends emails
   * to the municipality. This can also be called from the send / re-send button.
   *-------------------------------------------------------------------------*/
  procedure SendMuniEmailNotification(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_FromAddress                       varchar2(100);
    t_ToAddress                         varchar2(4000);
    t_CCAddress                         varchar2(4000);
    t_BccAddress                        varchar2(4000);
    t_Subject                           varchar2(4000);
    t_Body                              varchar2(32767);
    t_DocumentId                        varchar2(20);
    t_DocumentEndPointName              varchar2(100);
    t_BodyIsHTML                        varchar2(1) := 'Y';
    t_AttachAllDocs                     varchar2(1);
    t_SystemSettingsObjectId            udt_Id;
    t_ExternalWebsiteBaseURL            varchar2(4000);
    t_IsProd                            varchar2(1);
    t_JobId                             udt_Id;
    t_SendNotifEmailToMunicipality      varchar2(1) := 'N';
    t_ToEmailAddressNonProd             varchar2(100);
    t_MuniBody                          varchar2(32767);
    t_ObjectDefTypeId                   udt_Id;
    t_MunicipalityName                  varchar2(4000);
    t_CurrentMunicipalUserEmails        varchar2(4000);
    type NumberList                     is table of number;
    t_MunicipalList                     NumberList;
  begin

    select o.ObjectDefTypeId
    into t_ObjectDefTypeId
    from api.objects o
    where o.ObjectId = a_ObjectId;
    if t_ObjectDefTypeId = 3 then
      select p.JobId
      into t_JobId
      from api.processes p
      where p.ProcessId = a_ObjectId;
    else
      t_JobId := a_ObjectId;
    end if;

    if api.pkg_columnquery.Value(t_JobId, 'SendNotifEmailToMunicipality') = 'Y' or
       api.pkg_columnquery.Value(t_JobId, 'ReSendNotifEmailToMunicipality') = 'Y' then
      t_SendNotifEmailToMunicipality := 'Y';
    end if;
    if t_SendNotifEmailToMunicipality = 'Y' then
      -- Get some information from System Settings
      begin
        select
          o.ObjectId,
          o.IsProduction,
          o.FromEmailAddress,
          o.LicenseEmailSubject,
          o.ExternalWebsiteBaseURL,
          o.ToEmailAddressNonProd
        into
          t_SystemSettingsObjectId,
          t_IsProd,
          t_FromAddress,
          t_Subject,
          t_ExternalWebsiteBaseURL,
          t_ToEmailAddressNonProd
        from query.o_SystemSettings o
        where o.ObjectDefTypeId = 1
          and rownum = 1;
      exception
        when no_data_found then
          raise_application_error(-20000, 'System Settings Object is not ' ||
              'configured.');
      end;

      -- Get Note Text (Email Body)
      select n.Text
      into t_MuniBody
      from api.notes n
      join api.notedefs nd
          on nd.NoteDefId = n.NoteDefId
      join api.objectdefnotedefs odnd
          on odnd.NoteDefId = nd.NoteDefId and
             odnd.ObjectDefId = api.pkg_configquery.ObjectDefIdForName
                 ('o_SystemSettings')
      where nd.Tag = 'BRMUNI'
        and n.ObjectId = t_SystemSettingsObjectId;

      -- rsh next step: need to figure out who to send the emails to.
      select distinct(api.pkg_columnquery.Value(r.licenseobjectid,
          'MunicipalityObjectId')) MunicipalityObjectId
      bulk collect into t_MunicipalList
      from abc.licensebatch_xref_t r
      where r.Renewalnotifbatchjobid = t_JobId
      union
      select distinct(api.pkg_columnquery.Value(r1.licenseobjectid,
          'MunicipalityObjectId')) MunicipalityObjectId
      from abc.licensebatchgrace_xref_t r1
      where r1.renewalnotifbatchjobid = t_JobId;

      for m in 1..t_MunicipalList.Count loop
        t_MunicipalityName := api.pkg_columnquery.Value(t_MunicipalList(m), 'Name');
        t_CurrentMunicipalUserEmails := api.pkg_columnquery.Value
            (t_MunicipalList(m),'CurrentMunicipalUserEmails');
        if t_CurrentMunicipalUserEmails is null then
          api.pkg_Errors.RaiseError(-20001, 'No Active Clerk Email addresses found for Municipality: ' 
              || t_MunicipalityName);
        end if;
        t_Body := t_MuniBody;
        t_Body := replace(t_Body, '{Municipality}', t_MunicipalityName);
        -- Append a warning to the beginning of the body if this is a non-production
        -- environment.
        if t_IsProd = 'N' then
          -- Leave 200 for this html, and 200 for the email headers added later.
          if Length(t_Body) < 32300 then 
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a ' || 
                'non-production system.</span><br><br>' || t_Body;
          else
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a ' ||
                'non-production system.<br><br>Message too long... truncated.</span><br>';
          end if;
          t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromAddress);
        else
          t_ToAddress := t_CurrentMunicipalUserEmails;
        end if;
        extension.pkg_sendmail.SendEmail(t_MunicipalList(m), t_FromAddress,
            t_ToAddress, t_CCAddress, t_BccAddress, t_Subject, t_Body, t_DocumentId,
            t_DocumentEndPointName, t_BodyIsHTML, t_AttachAllDocs);
      end loop;
    end if;

    if api.pkg_columnquery.Value (t_JobId, 'SendNotifEmailToMunicipality') = 'Y' then
      api.pkg_columnupdate.SetValue(t_JobId, 'SendNotifEmailToMunicipality', 'N');
    end if;
 
  end SendMuniEmailNotification;

  /*---------------------------------------------------------------------------
   * ResendCustomerLicenseNotif. This procedure sends an email to a specific 
   * licensee on a batch renewal its used to re-send an email
   *-------------------------------------------------------------------------*/
  procedure ResendCustomerLicenseNotif(
    a_LicenseObjectId                   udt_Id,
    a_AsOfDate                          date
  ) is
    t_FromAddress                       varchar2(100);
    t_ToAddress                         varchar2(4000);
    t_CCAddress                         varchar2(4000);
    t_BccAddress                        varchar2(4000);
    t_Subject                           varchar2(4000);
    t_Body                              varchar2(32767);
    t_DocumentId                        varchar2(20);
    t_DocumentEndPointName              varchar2(100);
    t_BodyIsHTML                        varchar2(1) := 'Y';
    t_AttachAllDocs                     varchar2(1);
    t_LicenseSubject                    varchar2(4000);
    t_LicenseBody                       varchar2(32767);
    t_SystemSettingsObjectId            udt_Id;
    t_ExternalWebsiteBaseURL            varchar2(4000);
    t_IsProd                            varchar2(1);
    t_JobId                             udt_Id;
    t_ToEmailAddressNonProd             varchar2(100);
    t_ReliefRequiredText1218            varchar2(4000);
    t_ReliefRequiredText1239            varchar2(4000);
    t_ShouldProcess                     varchar2(1);
    t_PublicUsers                       varchar2(4000);
  begin

    t_ShouldProcess := api.pkg_columnquery.Value(a_LicenseObjectId,
        'SendLicenseEmailNotification');
    if t_ShouldProcess = 'Y' then
      -- Get some information from System Settings
      begin
        select
          o.ObjectId,
          o.IsProduction,
          o.FromEmailAddress,
          o.LicenseEmailSubject,
          o.ExternalWebsiteBaseURL,
          o.ReliefRequiredText1218,
          o.ReliefRequiredText1239,
          o.ToEmailAddressNonProd
        into
          t_SystemSettingsObjectId,
          t_IsProd,
          t_FromAddress,
          t_LicenseSubject,
          t_ExternalWebsiteBaseURL,
          t_ReliefRequiredText1218,
          t_ReliefRequiredText1239,
          t_ToEmailAddressNonProd
        from query.o_SystemSettings o
        where o.ObjectDefTypeId = 1
          and rownum = 1;
      exception
        when no_data_found then
          raise_application_error(-20000, 'System Settings Object is not ' ||
              'configured.');
      end;

      -- Get Note Text (Email Body)
      select n.Text
      into t_LicenseBody
      from api.notes n
      join api.notedefs nd
          on nd.NoteDefId = n.NoteDefId
      join api.objectdefnotedefs odnd
          on odnd.NoteDefId = nd.NoteDefId
          and odnd.ObjectDefId = api.pkg_configquery.ObjectDefIdForName
              ('o_SystemSettings')
      where nd.Tag = 'BRLIC'
        and n.ObjectId = t_SystemSettingsObjectId;

      --License Expiration Notifications
      for ln in (
          select
            l.ObjectId ObjectId,
            l.EstablishmentLocationAddress EstablishmentAddress,
            l.LicenseNumber LicenseNumber,
            l.LicenseType LicenseType,
            l.Licensee Licensee,
            l.licenseeobjectid licenseeobjectid,
            l.Establishment Establishment,
            to_char(l.ExpirationDate,'fmMonth DD, YYYY') ExpirationDate,
            l.ReliefRequired1218 ReliefRequired1218,
            l.ReliefRequired1239 ReliefRequired1239,
            le.MailingAddress MailingAddress,
            le.PreferredContactMethod PreferredContactMethod,
            le.ContactEmailAddress ContactEmailAddress
          from query.o_abc_license l
          join query.r_ABC_LicenseLicenseeLE rle
              on rle.LicenseObjectId = l.objectid
          join query.o_abc_legalentity le
              on le.objectid = rle.LegalEntityObjectId
          where l.objectid = a_LicenseObjectId
            and (le.PreferredContactMethod is not null
                and le.PreferredContactMethod = 'Email')
            and le.PreferredContactMethod is not null
            and l.SendLicenseEmailNotification = 'Y'
            and le.Active = 'Y'
          ) loop

        t_PublicUsers := RetrieveOnlineUserEmail (ln.licenseeobjectid);
        t_Subject := t_LicenseSubject;
        t_Body := t_LicenseBody;

        t_Body := replace(t_Body, '{Licensee}', ln.Licensee);
        t_Body := replace(t_Body, '{LicenseNumber}', ln.LicenseNumber);
        t_Body := replace(t_Body, '{LicenseType}', ln.LicenseType);
        t_Body := replace(t_Body, '{Establishment}', ln.Establishment);
        t_Body := replace(t_Body, '{ExpirationDate}', ln.ExpirationDate);
        t_Body := replace(t_Body, '{PreferredContact}', 'Preferred Contact: ' ||
            ln.ContactEmailAddress);
        t_Body := replace(t_Body, '{OnlineUsers}', t_PublicUsers);
        t_Body := replace(t_Body, '{Link}', t_ExternalWebsiteBaseURL);
        if ln.reliefrequired1218 = 'Y' then
          t_Body := replace(t_Body, '{12.18Relief}', t_ReliefRequiredText1218);
        else
          t_Body := replace(t_Body, '{12.18Relief}', '');
        end if;
        if ln.reliefrequired1239 = 'Y' then
          t_Body := replace(t_Body, '{12.39Relief}', t_ReliefRequiredText1239);
        else
          t_Body := replace(t_Body, '{12.39Relief}', '');
        end if;
        t_Body := pkg_abc_email.InsertLink(t_Body, t_ExternalWebsiteBaseURL);

        -- Append a warning to the beginning of the body if this is a non-production
        -- environment.
        if t_IsProd = 'N' then
          -- Leave 200 for this html, and 200 for the email headers added later.
          if Length(t_Body) < 32300 then 
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a non-' ||
                'production system.</span><br><br>' || t_Body;
          else
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a non-' ||
                'production system.<br><br>Message too long... truncated.</span><br>';
          end if;
          t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromAddress);
        else
          t_ToAddress := ln.ContactEmailAddress;
        end if;

        extension.pkg_sendmail.SendEmail(ln.ObjectId, t_FromAddress, t_ToAddress,
            t_CCAddress, t_BccAddress, t_Subject, t_Body, t_DocumentId,
            t_DocumentEndPointName, t_BodyIsHTML, t_AttachAllDocs);
      end loop;
    end if;

  end ResendCustomerLicenseNotif;

  /*---------------------------------------------------------------------------
   * ResendCustomerPermitNotif. This procedure sends an email to a specific 
   * permitee on a batch renewal its used to re-send an email
   *-------------------------------------------------------------------------*/
  procedure ResendCustomerPermitNotif(
    a_PermitObjectId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_FromAddress                       varchar2(100);
    t_ToAddress                         varchar2(4000);
    t_CCAddress                         varchar2(4000);
    t_BccAddress                        varchar2(4000);
    t_Subject                           varchar2(4000);
    t_Body                              varchar2(32767);
    t_DocumentId                        varchar2(20);
    t_DocumentEndPointName              varchar2(100);
    t_BodyIsHTML                        varchar2(1) := 'Y';
    t_AttachAllDocs                     varchar2(1);
    t_PermitSubject                     varchar2(4000);
    t_PermitBody                        varchar2(32767);
    t_SystemSettingsObjectId            udt_Id;
    t_ExternalWebsiteBaseURL            varchar2(4000);
    t_IsProd                            varchar2(1);
    t_JobId                             udt_Id;
    t_ToEmailAddressNonProd             varchar2(100);
    t_ShouldProcess                     varchar2(1);
    t_NotifyText                        varchar2(4000);
    t_PublicUsers                       varchar2(4000);    
begin

    t_ShouldProcess := api.pkg_columnquery.Value(a_PermitObjectId,
        'SendPermitEmailNotification');
    if t_ShouldProcess = 'Y' then

      -- Get some information from System Settings
      begin
        select
          o.ObjectId,
          o.IsProduction,
          o.FromEmailAddress,
          o.PermitEmailSubject,
          o.ExternalWebsiteBaseURL,
          o.ToEmailAddressNonProd,
          o.PermitLetterText
        into
          t_SystemSettingsObjectId,
          t_IsProd,
          t_FromAddress,
          t_PermitSubject,
          t_ExternalWebsiteBaseURL,
          t_ToEmailAddressNonProd,
          t_NotifyText
        from query.o_SystemSettings o
        where o.ObjectDefTypeId = 1
          and rownum = 1;
      exception
        when no_data_found then
          raise_application_error(-20000, 'System Settings Object is not ' ||
              'configured.');
      end;

      -- Get Note Text (Email Body)
      select n.Text
      into t_PermitBody
      from api.notes n
      join api.notedefs nd
          on nd.NoteDefId = n.NoteDefId
      join api.objectdefnotedefs odnd
          on odnd.NoteDefId = nd.NoteDefId
          and odnd.ObjectDefId = api.pkg_configquery.ObjectDefIdForName
              ('o_SystemSettings')
      where nd.Tag = 'BRPERM'
        and n.ObjectId = t_SystemSettingsObjectId;

      --Permit Expiration Notifications
      for pn in (
          select
            p.ObjectId ObjectId,
            le.dup_FormattedName Permittee,
            p.PermitNumber PermitNumber,
            p.PermitType PermitType,
            to_char(p.ExpirationDate,'fmMonth DD, YYYY') ExpirationDate,
            p.LicenseEstablishment Establishment,
            p.LicenseEstablishmentLocation EstablishmentAddress,
            api.pkg_ColumnQuery.NumericValue(p.ObjectId, 'FormattedPermitteeObjectId')
                PermitteeObjectID,
            le.dup_FormattedName LegalName,
            le.MailingAddress MailingAddress,
            le.PreferredContactMethod PreferredContactMethod,
            le.ContactEmailAddress ContactEmailAddress,
            (case
              when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
                p.EventLocAddressDescription
              when p.PermitTypeCode = 'SOL' then
                p.SolicitorName
            end) AddDetails1,
            (case
              when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
                'Location:'
              when p.PermitTypeCode = 'SOL' then
                'Solicitor:'
            end) AddLabels1,
            (case
              when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
                p.EventLocationAddress
            end) AddDetails2                           
          from  query.o_abc_permit p
          join  query.o_abc_legalentity le
              on le.objectid = p.dup_FormattedPermitteeObjectId
          where p.objectid = a_PermitObjectId
            and (le.PreferredContactMethod is not null
                and le.PreferredContactMethod = 'Email')
            and le.PreferredContactMethod is not null
            and p.SendPermitEmailNotification = 'Y'
            and le.Active = 'Y'
          ) loop
        t_PublicUsers := RetrieveOnlineUserEmail (pn.Permitteeobjectid);
        t_Subject := t_PermitSubject;
        t_Body := t_PermitBody;

        t_Body := replace(t_Body, '{Permitee}', pn.Permittee);
        t_Body := replace(t_Body, '{PermitNumber}', pn.PermitNumber);
        t_Body := replace(t_Body, '{PermitType}', pn.PermitType);
        t_Body := replace(t_Body, '{Establishment}', pn.Establishment);
        t_Body := replace(t_Body, '{EstablishmentAddress}', pn.EstablishmentAddress);
        t_Body := replace(t_Body, '{ExpirationDate}', pn.ExpirationDate);
        t_Body := replace(t_Body, '{Link}', t_ExternalWebsiteBaseURL);
        t_Body := replace(t_Body, '{NotifyText}', t_NotifyText);
        t_Body := replace(t_Body, '{AddDetails1}', pn.Adddetails1);
        t_Body := replace(t_Body, '{AddDetails2}', pn.Adddetails2);
        t_Body := replace(t_Body, '{AddLabels1}', pn.AddLabels1);
        t_Body := replace(t_Body, '{PreferredContact}', 'Preferred Contact: ' ||
            pn.ContactEmailAddress);
        t_Body := replace(t_Body, '{OnlineUsers}', t_PublicUsers);
        t_Body := pkg_abc_email.InsertLink(t_Body, t_ExternalWebsiteBaseURL);

        -- Append a warning to the beginning of the body if this is a non-production
        -- environment.
        if t_IsProd = 'N' then
          -- Leave 200 for this html, and 200 for the email headers added later.
          if Length(t_Body) < 32300 then 
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a non-' ||
                'production system.</span><br><br>' || t_Body;
          else
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a non-' ||
                'production system.<br><br>Message too long... truncated.</span><br>';
          end if;
          t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromAddress);
        else
          t_ToAddress := pn.ContactEmailAddress;
        end if;

        extension.pkg_sendmail.SendEmail(pn.ObjectId, t_FromAddress, t_ToAddress,
            t_CCAddress, t_BccAddress, t_Subject, t_Body, t_DocumentId,
            t_DocumentEndPointName, t_BodyIsHTML, t_AttachAllDocs);
      end loop;
    end if;

  end ResendCustomerPermitNotif;

  /*---------------------------------------------------------------------------
   * ResendCustomerProductNotif. This procedure sends an email to a specific 
   * registrant on a batch renewal its used to re-send an email
   *-------------------------------------------------------------------------*/
  procedure ResendCustomerProductNotif(
    a_RegistrantObjectId                udt_Id,
    a_AsOfDate                          date
  ) is
    t_FromAddress                       varchar2(100);
    t_ToAddress                         varchar2(4000);
    t_CCAddress                         varchar2(4000);
    t_BccAddress                        varchar2(4000);
    t_Subject                           varchar2(4000);
    t_Body                              varchar2(32767);
    t_DocumentId                        varchar2(20);
    t_DocumentEndPointName              varchar2(100);
    t_BodyIsHTML                        varchar2(1) := 'Y';
    t_AttachAllDocs                     varchar2(1);
    t_ProductSubject                    varchar2(4000);
    t_ProductBody                       varchar2(32767);
    t_SystemSettingsObjectId            udt_Id;
    t_ExternalWebsiteBaseURL            varchar2(4000);
    t_IsProd                            varchar2(1);
    t_JobId                             udt_Id;
    t_ToEmailAddressNonProd             varchar2(100);
    t_ShouldProcess                     varchar2(1);
    t_BatchRenewalNotifJobId            udt_Id;
    t_PublicUsers                       varchar2(4000);     
  begin

    t_BatchRenewalNotifJobId := api.pkg_columnquery.NumericValue(a_RegistrantObjectId,
        'ResendRegistrantNotifBatchId');
    if t_BatchRenewalNotifJobId is null then
      t_ShouldProcess := 'N';
    else
      t_ShouldProcess := 'Y';
    end if;

    if t_ShouldProcess = 'Y' then
      -- Get some information from System Settings
      begin
        select
          o.ObjectId,
          o.IsProduction,
          o.FromEmailAddress,
          o.ProductEmailSubject,
          o.ExternalWebsiteBaseURL,
          o.ToEmailAddressNonProd
        into
          t_SystemSettingsObjectId,
          t_IsProd,
          t_FromAddress,
          t_ProductSubject,
          t_ExternalWebsiteBaseURL,
          t_ToEmailAddressNonProd
        from query.o_SystemSettings o
        where o.ObjectDefTypeId = 1
          and rownum = 1;
      exception
        when no_data_found then
          raise_application_error(-20000, 'System Settings Object is not ' ||
              'configured.');
      end;

      -- Get Note Text (Email Body)
      select n.Text
      into t_ProductBody
      from api.notes n
      join api.notedefs nd
          on nd.NoteDefId = n.NoteDefId
      join api.objectdefnotedefs odnd
          on odnd.NoteDefId = nd.NoteDefId
          and odnd.ObjectDefId = api.pkg_configquery.ObjectDefIdForName
              ('o_SystemSettings')
      where nd.Tag = 'BRPROD'
        and n.ObjectId = t_SystemSettingsObjectId;

      --Product Expiration Notifications
      for pe in (
          select
            ol.ObjectId,
            ol.dup_FormattedName Registrant,
            ol.ContactEmailAddress,
            oprc.RenewalCycle,
            r1.ProductCount,
            to_char( r1.EarliestProductExpirationDate, 'fmMonth DD, YYYY')
                ExpirationDate
          from query.r_abc_batchrenewalprodrenewcyc r2
          join query.o_abc_productrenewalcycle oprc
              on oprc.ObjectId = r2.ProductRenewalCycleObjectId
          join query.r_abc_registrantprodrenewcycle r1
              on r1.ProductRenewalCycleObjectId = oprc.ObjectId
          join query.o_abc_legalentity ol
              on ol.ObjectId = r1.RegistrantObjectId
          where r2.BatchRenewalNotificationJobId = t_BatchRenewalNotifJobId
            and r1.RegistrantObjectId = a_RegistrantObjectId
            and (ol.PreferredContactMethod is not null
                and ol.PreferredContactMethod = 'Email')
            and ol.ContactEmailAddress is not null
            and ol.Active = 'Y'
          ) loop

        t_PublicUsers := RetrieveOnlineUserEmail (pe.objectid);
        t_Subject := t_ProductSubject;
        t_Body := t_ProductBody;

        t_Body := replace(t_Body, '{Registrant}', pe.Registrant);
        t_Body := replace(t_Body, '{ProductCount}', pe.ProductCount);
        t_Body := replace(t_Body, '{ExpirationDate}', pe.ExpirationDate);
        t_Body := replace(t_Body, '{PreferredContact}', 'Preferred Contact: ' ||
            pe.ContactEmailAddress);
        t_Body := replace(t_Body, '{OnlineUsers}', t_PublicUsers);
        t_Body := replace(t_Body, '{Link}', t_ExternalWebsiteBaseURL);
        t_Body := pkg_abc_email.InsertLink(t_Body, t_ExternalWebsiteBaseURL);

        -- Append a warning to the beginning of the body if this is a non-production
        -- environment.
        if t_IsProd = 'N' then
          -- Leave 200 for this html, and 200 for the email headers added later.
          if Length(t_Body) < 32300 then 
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a non-' ||
                'production system.</span><br><br>' || t_Body;
          else
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a non-' ||
                'production system.<br><br>Message too long... truncated.</span><br>';
          end if;
          t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromAddress);
        else
          t_ToAddress := pe.ContactEmailAddress;
        end if;
        
        t_body := abc.pkg_abc_email.EmailFormat(t_body);

        extension.pkg_sendmail.SendEmail(pe.ObjectId, t_FromAddress, t_ToAddress,
            t_CCAddress, t_BccAddress, t_Subject, t_Body, t_DocumentId,
            t_DocumentEndPointName, t_BodyIsHTML, t_AttachAllDocs);
      end loop;
    end if;

  end ResendCustomerProductNotif;

/*---------------------------------------------------------------------------
 * CancelProductNotif
 *   Is called on cancellation of a Batch Renewal Notification job
 * Removes product registrants from the renewal cycle object so the batch can be re-run
 *-------------------------------------------------------------------------*/
procedure CancelProductNotif(a_ObjectId udt_Id,
                             a_AsOfDate date) is
  t_JobId                udt_Id;
  t_BatchCycleEPId       udt_Id;
  t_CycleRegistrantEPId  udt_Id;
begin
  t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
  t_BatchCycleEPId := api.pkg_configquery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'ProdRenCyc');
  t_CycleRegistrantEPId := api.pkg_configquery.EndPointIdForName('o_ABC_ProductRenewalCycle', 'Registrant');
  
  delete 
    from abc.registrantnotifications_t t
   where t.notificationjobid = t_JobId;
   
  for i in (select cr.RelationshipId 
              from api.relationships jc
              join api.relationships cr
                on cr.FromObjectId = jc.ToObjectId
             where jc.fromobjectid = t_JobId
               and jc.EndPointId = t_BatchCycleEPId
               and cr.EndPointId = t_CycleRegistrantEPId) loop
    api.pkg_relationshipupdate.Remove(i.relationshipid);
  end loop;

end CancelProductNotif;

  /*---------------------------------------------------------------------------
   * RetrieveOnlineUserEmail. This is called to retrieve the emails of
   * online users associated with a Legal Entity
   *-------------------------------------------------------------------------*/
  function RetrieveOnlineUserEmail(
    a_LegalEntityId                     udt_Id
  ) return                              varchar2
    is
    t_PublicUsers                       varchar2(10000) := '';
  begin

    for oo in (
        select api.pkg_columnquery.Value(r.UserId, 'EmailAddress') EmailAddress
        from query.r_ABC_UserLegalEntity r
        where r.LegalEntityObjectId = a_LegalEntityId
        order by 1
        ) loop
      t_PublicUsers := t_PublicUsers || 'Online User: ' || oo.Emailaddress ||
          '<br>';
      if length(t_PublicUsers) > 4000 then
        t_PublicUsers := substr(t_PublicUsers, 1, 3996) || ' ...';
        exit;
      end if;
    end loop;
    return t_PublicUsers;

  end RetrieveOnlineUserEmail;

end PKG_ABC_BATCHRENEWALNOTIF;
/
