create or replace package pkg_ABC_PermitType is

  /*---------------------------------------------------------------------------
   * Public type declarations
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * PostVerifyWrapper()
   *   Runs on Post-Verify of the ABC Permit Type.
   *-------------------------------------------------------------------------*/
  procedure PostVerifyWrapper (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );
  
  /*---------------------------------------------------------------------------
   * ExpirePreviousPermitsOfPermitType()
   *   Expires all permits of the permit type that are active and past their 
   *   expiration date
   *-------------------------------------------------------------------------*/
  procedure ExpirePreviousPermitsOfPermitType(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );
  
  /*---------------------------------------------------------------------------
   * ExpirePermitsOnProcessServer()
   *   Runs ExpirePreviousPermitsOfPermitType on process server
   *-------------------------------------------------------------------------*/
  procedure ExpirePermitsOnProcessServer(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_PermitType;
/
grant execute 
on pkg_Abc_PermitType 
to PosseExtensions;

create or replace package body pkg_ABC_PermitType is

  /*---------------------------------------------------------------------------
   * PostVerifyWrapper() -- PUBLIC
   *   Runs on Post-Verify of the ABC Permit Type.
   *-------------------------------------------------------------------------*/
  procedure PostVerifyWrapper (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
  begin

    if api.pkg_columnquery.value(a_objectId, 'Code') is null then
      api.pkg_errors.RaiseError(-20000, 'Please enter a Permit Type Code.');
    end if;
    if api.pkg_columnquery.value(a_objectId, 'Name') is null then
      api.pkg_errors.RaiseError(-20000, 'Please enter a Name for the Permit Type.');
    end if;

    if api.pkg_columnquery.value(a_objectId, 'ExpirationMethod') is null then
      api.pkg_errors.RaiseError(-20000, 'Please choose an expiration method.');
    end if;
    if api.pkg_columnquery.value(a_objectId, 'ExpirationMethod') = 'End of Specific Month' and
        api.pkg_columnquery.value(a_objectId, 'ExpirationEndOfMonth') is null then
      api.pkg_errors.RaiseError(-20000, 'Please choose a month.');
    end if;
    if api.pkg_columnquery.value(a_objectId, 'ExpirationMethod') like 'Number of %' and
        api.pkg_columnquery.value(a_objectId, 'ExpirationNumberOf') is null then
      api.pkg_errors.RaiseError(-20000, 'Please enter the number of ' ||
          substr(api.pkg_columnquery.value(a_objectId, 'ExpirationMethod'), 10) ||'.');
    end if;
    if api.pkg_columnquery.value(a_objectId, 'ExpirationReminderDays') is null then
      api.pkg_errors.RaiseError(-20000, 'Please enter the number of expiration reminder days.');
    end if;

    if api.pkg_ColumnQuery.Value(a_ObjectId, 'RequiresEvents') = 'N' and
        api.pkg_ColumnQuery.Value(a_ObjectId, 'MaximumEventPermitDays') is not null then
      api.pkg_errors.RaiseError(-20000, 'A value for Maximum Event Permit Days can only be' ||
          ' entered when Requires Events is checked.');
    end if;

    if api.pkg_columnquery.value(a_objectId, 'MunicipalityRequired') = 'Y' and
        api.pkg_columnquery.value(a_objectId, 'ReviewRequired') = 'N' then
      api.pkg_errors.RaiseError(-20000, 'Municipal Review Required can only be checked when' ||
          ' Administrative Review Required is checked.');
    end if;

    if api.pkg_columnquery.value(a_objectId, 'PoliceRequired') = 'Y' and
        api.pkg_columnquery.value(a_objectId, 'ReviewRequired') = 'N' then
      api.pkg_errors.RaiseError(-20000, 'Police Review Required can only be checked when' ||
          ' Administrative Review Required is checked.');
    end if;

    if api.pkg_ColumnQuery.Value(a_ObjectId, 'Renewable') = 'Y' and
        api.pkg_ColumnQuery.Value(a_ObjectId, 'MaximumPermitsPerLegalEntity') is not null then
      api.pkg_errors.RaiseError(-20000, 'Maximum Permits per Legal Entity cannot be set when ' ||
          'Renewable is checked.');
    end if;

    if api.pkg_columnquery.value(a_objectId, 'PermitRequired') = 'Y' and
        api.pkg_columnquery.value(a_objectId, 'PermitTypeCount') = 0 then
      api.pkg_errors.RaiseError(-20000, 'Please supply at least one Permit Type.');
    end if;

    if api.pkg_columnquery.value(a_objectId, 'LicenseRequired') != 'N/A' and
        api.pkg_columnquery.value(a_objectId, 'LicenseTypeCount') = 0 then
      api.pkg_errors.RaiseError(-20000, 'Please supply at least one License Type.');
    end if;

    if api.pkg_columnquery.value(a_objectId, 'RequiresCoOp') = 'Y' and
        api.pkg_columnquery.value(a_objectId, 'CoOpLicenseTypeCount') = 0 then
      api.pkg_errors.RaiseError(-20000, 'Please supply at lease one Co-Op License Type.');
    end if;

    if api.pkg_columnquery.value(a_ObjectId, 'IncludeOnAddPrivilegeReport') = 'Y' then
      if api.pkg_columnquery.value(a_ObjectId, 'RequiresLocation') = 'N' then
        api.pkg_errors.RaiseError(-20000, 'Permit Type cannot be included on the Winery Outlets/' ||
            'Salesrooms Report if it doesn''t require a Location or License Type.');
      end if;
      if api.pkg_columnquery.value(a_objectId, 'LicenseRequired') != 'Required' then
        api.pkg_errors.RaiseError(-20000, 'Permit Type cannot be included on the Winery Outlets/' ||
            'Salesrooms Report if it doesn''t require a Location or License Type.');
      end if;
    end if;

    if api.pkg_columnquery.value(a_ObjectId, 'AllowTapOverride') = 'Y' then
      if api.pkg_columnquery.value(a_objectId, 'LicenseRequired') != 'Required' then
        api.pkg_errors.RaiseError(-20000, 'Allow TAP Override can only be set if License Required'
            ||' is set with a value of Required.');
      end if;
      if api.pkg_columnquery.value(a_objectId, 'PermitRequired') = 'Y' then
        api.pkg_errors.RaiseError(-20000, 'Permit Required can not be set if Allow TAP Override is'
            || ' set.');
      end if;
    end if;

    if api.pkg_columnquery.value(a_objectId, 'LicenseRequired') = 'N/A' then
      if api.pkg_columnquery.value(a_objectId, 'DisableLicenseLookup') = 'Y' then
        api.pkg_errors.RaiseError(-20000, 'Disable License Lookup can not be set if a License' ||
            ' can not be entered.');
      end if;
      if api.pkg_columnquery.value(a_objectId, 'PermitteeIsLicensee') = 'Y' then
        api.pkg_errors.RaiseError(-20000, 'Permittee Is Licensee can not be set if a License' ||
            ' can not be entered.');
      end if;
    end if;
    if api.pkg_columnquery.value(a_objectId, 'DisableLicenseLookup') = 'Y' and
        api.pkg_columnquery.value(a_objectId, 'PermitteeIsLicensee') = 'Y' then
      api.pkg_errors.RaiseError(-20000, 'Permittee Is Licensee can not be set if License Lookup' ||
          ' is disabled.');
    end if;

    if api.pkg_columnquery.value(a_objectId, 'DefaultPermittingClerk') is null then
      api.pkg_errors.RaiseError(-20000, 'Please supply a default Clerk.');
    end if;
    if api.pkg_columnquery.value(a_objectId, 'DefaultPermittingSupervisor') is null then
      api.pkg_errors.RaiseError(-20000, 'Please supply a default Supervisor.');
    end if;
    
    if api.pkg_columnquery.value(a_objectId, 'AllowSellerTapOverride') = 'Y' and
        api.pkg_columnquery.value(a_objectId, 'RequireSellersLicenseInfo') = 'N' then
      api.pkg_errors.RaiseError(-20000, 'The seller''s license information must be required to' ||
          ' allow TAP Override');
    end if;

  end PostVerifyWrapper;
  
  /*---------------------------------------------------------------------------
   * ExpirePreviousPermitsOfPermitType()
   *   Expires all permits of the permit type that are active and past their 
   *   expiration date
   *-------------------------------------------------------------------------*/
  procedure ExpirePreviousPermitsOfPermitType(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_PermitIdList                      udt_IdList;
  begin
    select t.objectid
    bulk collect into t_PermitIdList
	  from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_Permit', 'State', 'Active')) t
	  join query.o_abc_permittype pt
	      on api.pkg_ColumnQuery.value(t.objectid, 'PermitTypeObjectId') = pt.objectid
	 where pt.Objectid = a_ObjectId
	   and api.pkg_columnquery.datevalue(t.objectid, 'ExpDatePlusGracePeriod') < sysdate;
      
    for i in 1..t_PermitIdList.count loop
      api.pkg_ColumnUpdate.SetValue(t_PermitIdList(i),'State', 'Expired');
    end loop;
  end ExpirePreviousPermitsOfPermitType;
  
  /*---------------------------------------------------------------------------
   * ExpirePermitsOnProcessServer()
   *   Runs ExpirePreviousPermitsOfPermitType on process server
   *-------------------------------------------------------------------------*/
  procedure ExpirePermitsOnProcessServer(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  )is
    t_Arguments                         varchar2(32767);
    t_ScheduleId                        udt_Id;
  begin
    api.pkg_ProcessServer.AddArgument(t_Arguments, a_ObjectId);
    api.pkg_ProcessServer.AddArgument(t_Arguments, a_AsOfDate);
    t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure('abc.pkg_ABC_PermitType.ExpirePreviousPermitsOfPermitType', 'Expire Permits', t_Arguments);
  end ExpirePermitsOnProcessServer;

end pkg_ABC_PermitType;
/
