create or replace package abc.pkg_ABC_License is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * PostVerifyWrapper()
   *   Runs on Post-Verify of the ABC License.
   *-------------------------------------------------------------------------*/
  procedure PostVerifyWrapper (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- SyncSecondaryLicenseInfo
  --  Runs on Post-Verify of relationships on the ABC License
  -----------------------------------------------------------------------------
  procedure SyncSecondaryLicenseInfo (
    a_RelationshipId      udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  -- CreateDefaultConditions
  --  Runs on Constructor of the ABC License (see Issue 7358)
  -----------------------------------------------------------------------------
  procedure CreateDefaultConditions(
    a_ObjectId            udt_Id,
    a_AsOfDate            date,
    a_DefaultCondCreated  char
  );

  -----------------------------------------------------------------------------
  -- SetNextInspectionDate
  --  Runs on "Approve" outcome of Approve/Reject Application process and
  -- "Close" outcome of Close Inspection process.
  -- Also runs on Post-Verify of o_ABC_RiskFactor if frequency or units are changed.
  -----------------------------------------------------------------------------
  procedure SetNextInspectionDate(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2015 Mar 27
  -- Purpose    : Determines if a given job is a Major Application or not
  -------------------------------------
  function IsMajorJob(
    a_JobId                             udt_Id
  ) return boolean;

  --------------------------------------------------------------------------------------------------
  -- Programmer : Yars T
  -- Date       : 2015 Sept 2
  -- Purpose    : Determines if the license has open renewals when the expiration date is in the past
  ---------------------------------------------------------------------------------------------------
  function HasRenewals(
    a_LicenseId                          udt_Id
  ) return varchar2;

  -----------------------------------------------------------------------------
  --  LicenseCancellation
  --  Runs as procedure on constructor of rel r_ABC_LicenseCancellation
  --  handles license cancellation
  -----------------------------------------------------------------------------
  procedure LicenseCancellation(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

    -----------------------------------------------------------------------------
  --  UndoCancellation
  --  Runs as a procedure on constructor of o_ABC_LicenseCancellation
  --  handles Undo Cancellation processing
  -----------------------------------------------------------------------------
  procedure UndoCancellation(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -----------------------------------------------------------------------------
  --  RelRemoveUponExpiration
  --  On expiration of the license, delete the product relationship(s) to the distributors.
  -----------------------------------------------------------------------------
  procedure RelRemoveUponExpiration(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -----------------------------------------------------------------------------
  --  OnlineAppDocumentVerify
  --  On change of License Municipality, verify online documents
  -----------------------------------------------------------------------------
  procedure OnlineAppDocumentVerify(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -----------------------------------------------------------------------------
  --  VerifyEventDate
  --  Verify Event Date entered
  -----------------------------------------------------------------------------
  procedure VerifyEventDate(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -----------------------------------------------------------------------------
  -- ValidateLicenseLetterType
  --  Runs on Post-Verify of LicenseLetter object.
  --  Purpose:
  --    Verify a LetterType is given for a Letter.
  -----------------------------------------------------------------------------
  procedure ValidateLicenseLetterType(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -----------------------------------------------------------------------------
  -- ValidateCondition
  --  Runs on Post-Verify of Condition object.
  --  Purpose:
  --    Verify certain fields are entered
  -----------------------------------------------------------------------------
  procedure ValidateCondition(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -----------------------------------------------------------------------------
  -- SellerRelPostVerifyWrapper
  --  Runs on Post-Verify of the Seller License relationship.
  -----------------------------------------------------------------------------
  procedure SellerRelPostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  -- RelatedLicenseConstructor
  --  Runs on Constructor of the Related License relationship.
  -----------------------------------------------------------------------------
  procedure RelatedLicenseConstructor (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  -- RelatedLicenseDestructor
  --  Runs on Destructor of the Related License relationship.
  -----------------------------------------------------------------------------
  procedure RelatedLicenseDestructor (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  -- StateChangeActivities
  --  Runs on change of the State detail
  --  Run any activities that may need to happen when the License State changes
  -----------------------------------------------------------------------------
  procedure StateChangeActivities (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  /*---------------------------------------------------------------------------
   * UpgradeDowngradeLicense()
   *   Upgrade/Downgrade License. Move Products from the Source License to the
   * Target License.
   *-------------------------------------------------------------------------*/
  procedure UpgradeDowngradeLicense(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopyDefaultAttachmentToLicenseLetter()
   *   Copies system settings detail to license letter
   *-------------------------------------------------------------------------*/
  procedure CopyDefaultAttachmentToLicenseLetter(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_License;
/
create or replace package body abc.pkg_ABC_License is

  /*---------------------------------------------------------------------------
   * PostVerifyWrapper() -- PUBLIC
   *   Runs on Post-Verify of the ABC License.
   *-------------------------------------------------------------------------*/
  procedure PostVerifyWrapper (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_RegionOfficeCount                 number;
    t_SecondaryLicenseObjectId          number;
    t_LicenseObjectDefId                number
        := api.pkg_configquery.ObjectDefIdForName('o_ABC_License');
    t_RelId                             number;
    t_SecondaryTypeCount                number;
    t_MasterLicenseObjectId             number;
    t_IsSecondary                       varchar2(1)
        := api.pkg_columnquery.Value(a_ObjectId, 'IsSecondaryLicenseSQL');
    t_LicenseTypeObjectId               number
        := api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeObjectId');
    t_Count                             number;
    t_VehicleId                         udt_IdList;
  begin

    if api.pkg_columnquery.value(a_ObjectId, 'VerifyLicense') = 'Y' then
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseType') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select a License Type.');
      end if;

      -- Check for a Related License if the License Type Code = 24
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '24' then
        if api.pkg_columnquery.Value(a_ObjectId, 'RelatedLicenseObjectId') is null
            and api.pkg_columnquery.Value(a_ObjectId, 'RelatedLicenseHistoryObjectId') is null then
          null;
        end if;
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'Licensee') is null then
        api.pkg_errors.RaiseError(-20000, 'Please enter a Licensee.');
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'Region') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select a County.');
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'Office') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select a Municipality.');
      end if;
      if api.pkg_columnQuery.Value (a_ObjectId, 'Region') is not null then
        if api.pkg_columnQuery.Value (a_ObjectId, 'RegionObjectId') in
            (api.pkg_columnQuery.Value (a_ObjectId, 'OfficeRegionObjectIds')) then
          null;
        else
          api.pkg_errors.RaiseError(-20000, 'The selected Municipality does not belong to the' ||
              ' selected County. Please select a new Municipality.');
        end if;
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'EstablishmentRequired') is null then
        api.pkg_errors.RaiseError(-20000, 'Please enter an Establishment.');
      end if;

      -- Check Special Event License Details
      if api.pkg_columnquery.Value(a_ObjectId, 'IsSpecialEvent') = 'Y' then
        if api.pkg_columnquery.Value(a_ObjectId, 'EventLocationAddress') is null and
            api.pkg_columnquery.Value(a_ObjectId, 'Establishment') is null then
          api.pkg_errors.RaiseError(-20000, 'Please enter an Establishment or Address for the' ||
              ' event location.');
        end if;
      end if;

      -- Check License Type specific Details
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '06' and
          api.pkg_columnquery.Value(a_ObjectId, 'SportingFacilityCapacity') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select a sporting event Capacity.');
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '08' and
          api.pkg_columnquery.Value(a_ObjectId, 'RestrictedBreweryProduction') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select a Restricted Brewery Production value.');
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '11' and
          api.pkg_columnquery.Value(a_ObjectId, 'LimitedBreweryProduction') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select a Limited Brewery Production value.');
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '18' and
          api.pkg_columnquery.Value(a_ObjectId, 'SupplementaryLimitedDistillery') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select a Supplementary Limited Distillery' ||
            ' Production value.');
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '22' and
          api.pkg_columnquery.Value(a_ObjectId, 'FarmWineryProduction') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select a Farm Winery Production value.');
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '21' and
          api.pkg_columnquery.Value(a_Objectid, 'PlenaryWineryWholesalePrivileg') = 'Y' and
          api.pkg_columnquery.Value(a_ObjectId, 'PlenaryWineryProduction') is null then
        api.pkg_errors.RaiseError(-20000, 'A Winery Production is required when Wholesale' ||
            ' Privilege is selected.');
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '41' and
          api.pkg_columnquery.Value(a_ObjectId, 'OutOfStateWineryProduction') is null then
        api.pkg_errors.RaiseError(-20000, 'Please select an Out of State Winery Production' ||
            ' value.');
      end if;

      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '13' then
        if api.pkg_columnquery.Value(a_ObjectId, 'RetailTransitType') is null then
          api.pkg_errors.RaiseError(-20000, 'Please select a Transit Type.');
        end if;
        if api.pkg_columnquery.Value(a_ObjectId, 'RetailTransitType') = 'Limousine' then
          if api.pkg_columnquery.value(a_ObjectId, 'VehiclesEntered') is null then
            api.pkg_errors.RaiseError(-20000, 'At least one vehicle is required for a ' ||
                api.pkg_columnquery.value(a_ObjectId, 'RetailTransitType') || ' License.');
          end if;

          select lv.VehicleId
          bulk collect into t_VehicleId
          from query.r_ABC_LicenseVehicle lv
          where lv.LicenseId = a_ObjectId
            and api.pkg_columnquery.Value(lv.VehicleId, 'VehicleType') = 'Limousine';

          for p in 1..t_VehicleId.count loop
            if api.pkg_columnquery.Value(t_VehicleId(p), 'VIN') is null then
              api.pkg_errors.RaiseError(-20000,'Please enter the Limousine''s VIN');
            end if;
            if api.pkg_columnquery.Value(t_VehicleId(p), 'InsigniaNumber') is null then
              api.pkg_errors.RaiseError(-20000,'Please enter the Limousine''s Insignia Number');
            end if;
            if api.pkg_columnquery.Value(t_VehicleId(p), 'MakeModelYear') is null  then
              api.pkg_errors.RaiseError(-20000,'Please enter the Limousine''s Make/Model/Year');
            end if;
            if api.pkg_columnquery.Value(t_VehicleId(p), 'StateRegistration') is null then
              api.pkg_errors.RaiseError(-20000,'Please enter the Limousine''s License number');
            end if;
            if api.pkg_columnquery.Value(t_VehicleId(p), 'StateOfRegistration') is null then
              api.pkg_errors.RaiseError(-20000,'Please enter the Limousine''s State of' ||
                  ' Registration');
            end if;
            if api.pkg_columnquery.Value(t_VehicleId(p), 'OwnedOrLeasedLimousine') is null then
              api.pkg_errors.RaiseError(-20000,'Please select if the Limousine is Owned or' ||
                  ' Leased');
            end if;
            if nvl (api.pkg_columnquery.Value(t_VehicleId(p), 'InternalStorageAddressObjectId'),
                api.pkg_columnquery.Value(t_VehicleId(p), 'OnlineStorageAddress')) is null then
              api.pkg_errors.RaiseError(-20000,'Please enter the Limousine''s Storage Address');
            end if;
          end loop;
        end if;

        if api.pkg_columnquery.Value(a_ObjectId, 'RetailTransitType') = 'Boat' then
          if api.pkg_columnquery.value(a_ObjectId, 'VesselsEntered') is null then
            api.pkg_errors.RaiseError(-20000, 'At least one vessel is required for a ' ||
                api.pkg_columnquery.value(a_ObjectId, 'RetailTransitType') || ' License.');
          end if;

          select lv.VehicleId
          bulk collect into t_VehicleId
          from query.r_ABC_LicenseVehicle lv
          where lv.LicenseId = a_ObjectId
            and api.pkg_columnquery.Value(lv.VehicleId, 'VehicleType') = 'Boat';

          for p in 1..t_VehicleId.count loop
            if api.pkg_columnquery.Value(t_VehicleId(p), 'USCGRegistrationNumber') is null then
              api.pkg_errors.RaiseError(-20000,'Please enter the Vessel''s Coast Guard' ||
                  ' Registration');
            end if;
            if api.pkg_columnquery.Value(t_VehicleId(p), 'VesselName') is null then
              api.pkg_errors.RaiseError(-20000,'Please enter the Vessel''s Name');
            end if;
            if api.pkg_columnquery.Value(t_VehicleId(p), 'VesselLength') is null then
              api.pkg_errors.RaiseError(-20000,'Please enter the Vessel''s Length');
            end if;
            if api.pkg_columnquery.Value(t_VehicleId(p), 'OwnedOrLeasedVessel') is null then
              api.pkg_errors.RaiseError(-20000,'Please select if the Vessel is Owned or Leased');
            end if;
            if nvl (api.pkg_columnquery.Value(t_VehicleId(p), 'InternalStorageAddressObjectId'),
                api.pkg_columnquery.Value(t_VehicleId(p), 'OnlineStorageAddress')) is null
                then
              api.pkg_errors.RaiseError(-20000,'Please enter the Vessel''s Storage Address');
            end if;
          end loop;
        end if;
      end if;

      -- Regenerate Fees
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') in
          ('06', '08', '11', '13', '18', '21', '22', '24', '41', 'OSWW') then
        abc.Pkg_Abc_Workflow.SetRegenerateFees (a_ObjectId, a_AsOfDate);
      end if;
      if api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeCode') = '13' and
          api.pkg_columnquery.Value(a_ObjectId, 'RetailTransitType') in ('Boat', 'Limousine') then
        abc.Pkg_Abc_Workflow.SetRegenerateFees (a_ObjectId, a_AsOfDate);
      end if;

      if api.pkg_columnquery.Value(a_ObjectId, 'DoNotRunPostVerifyProcedures') = 'N'
          and api.pkg_columnquery.Value(a_ObjectId, 'HasBeenIssued') = 'N' then
        -- Ensure Secondary License Types attached to the license exist on the License Type.
        -- Excluding licenses being Amended, Renewed or Reinstated
        if api.pkg_columnquery.Value(a_ObjectId, 'LicenseNumber') is null then
          select count(LicenseTypeObjectId)
          into t_Count
          from (
              select a.LicenseTypeObjectId
              from query.r_ABC_LicenseSecondaryLicType a
              where a.LicenseObjectId = a_ObjectId
              minus
              select b.SecondaryLicenseId
              from query.r_ABC_PrimarySecondaryLicType b
              where b.PrimaryLicenseId = t_LicenseTypeObjectId
              );
          if t_Count > 0 then
            api.pkg_errors.RaiseError(-20000, 'Before you change the License Type, please remove'
                || ' the Secondary Licenses first.');
          end if;
        end if;

        -- Create Master License if there is none
        if api.pkg_columnquery.NumericValue(a_ObjectId, 'MasterLicenseObjectId') is null then
          t_MasterLicenseObjectId := api.pkg_objectupdate.New(
              api.pkg_configquery.ObjectDefIdForName('o_ABC_MasterLicense'));
          t_RelId := api.pkg_relationshipupdate.New(
              api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Master'), a_ObjectId,
              t_MasterLicenseObjectId);
        end if;

         -- Sync up Secondary Licenses if the primary license has mandatory relationships
        if api.pkg_columnquery.NumericValue(a_ObjectId, 'OfficeObjectId') is not null then
          for i in (
              select r.LicenseTypeObjectId
              from query.r_ABC_LicenseSecondaryLicType r
              where r.LicenseObjectId = a_ObjectId
              ) loop
            select max(p.SecondaryLicenseObjectId)
            into t_SecondaryLicenseObjectId
            from
              query.r_Abc_PrimarySecondaryLicense p
              join query.r_ABC_LicenseLicenseType l
                  on l.LicenseObjectId = p.SecondaryLicenseObjectId
            where p.PrimaryLicenseObjectId = a_ObjectId
              and l.LicenseTypeObjectId = i.LicenseTypeObjectId;

            -- Create the Secondary License if not found
            if t_SecondaryLicenseObjectId is null then
              t_SecondaryLicenseObjectId := api.pkg_ObjectUpdate.New(t_LicenseObjectDefId);
              api.pkg_ColumnUpdate.SetValue(t_SecondaryLicenseObjectId, 'SourceObjectId',
                  t_SecondaryLicenseObjectId);
              t_RelId := api.pkg_RelationshipUpdate.New(
                  api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'LicenseType'),
                  t_SecondaryLicenseObjectId, i.LicenseTypeObjectId);
              t_RelId := api.pkg_RelationshipUpdate.New(
                  api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'Establishment'),
                  t_SecondaryLicenseObjectId, api.pkg_ColumnQuery.NumericValue(a_ObjectId,
                  'EstablishmentObjectId'));
              t_RelId := api.pkg_RelationshipUpdate.New(
                  api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'Licensee'),
                  t_SecondaryLicenseObjectId, api.pkg_ColumnQuery.NumericValue(a_ObjectId,
                  'LicenseeObjectId'));
              t_RelId := api.pkg_RelationshipUpdate.New(
                  api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'Region'),
                  t_SecondaryLicenseObjectId, api.pkg_ColumnQuery.NumericValue(a_ObjectId,
                  'RegionObjectId'));
              t_RelId := api.pkg_RelationshipUpdate.New(
                  api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'Office'),
                  t_SecondaryLicenseObjectId, api.pkg_ColumnQuery.NumericValue(a_ObjectId,
                  'OfficeObjectId'));
              t_RelId := api.pkg_RelationshipUpdate.New(
                  api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'RiskFactor'),
                  t_SecondaryLicenseObjectId, api.pkg_ColumnQuery.NumericValue(a_ObjectId,
                  'RiskFactorObjectId'));
              t_RelId := api.pkg_RelationshipUpdate.New(
                  api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'Secondary'), a_ObjectId,
                  t_SecondaryLicenseObjectId);
            end if;
          end loop;

          -- Remove any Licenses that are not in the selected Secondary License Types
          for j in (
              select
                p.relationshipid,
                l.LicenseTypeObjectId
              from
                query.r_Abc_Primarysecondarylicense p
                join query.r_ABC_LicenseLicenseType l
                    on l.LicenseObjectId = p.SecondaryLicenseObjectId
              where p.PrimaryLicenseObjectId = a_ObjectId
              ) loop
            select count(1)
            into t_SecondaryTypeCount
            from query.r_ABC_LicenseSecondaryLicType r
            where r.LicenseObjectId = a_ObjectId
              and r.LicenseTypeObjectId = j.licensetypeobjectid;

            if t_SecondaryTypeCount = 0 then
              api.pkg_relationshipupdate.Remove(j.relationshipid);
            end if;
          end loop;
        end if;
      end if;
    end if;

  end PostVerifyWrapper;

  -----------------------------------------------------------------------------
  -- SyncSecondaryLicenseInfo
  --  Runs on Post-Verify of relationships on the ABC License
  -----------------------------------------------------------------------------
  procedure SyncSecondaryLicenseInfo (
    a_RelationshipId      udt_Id,
    a_AsOfDate            date )
  is
    t_PrimaryLicenseObjectId      number;
    t_ToObjectDefId               number;
    t_ToObjectId                  number;
    t_ToEndPointName              varchar2(30);
    t_ToEndPointId                number;
    t_RelId                       number;


  begin
    select r.FromObjectId, rd.ToObjectDefId, r.ToObjectId, rd.ToEndPointName
      into t_PrimaryLicenseObjectId, t_ToObjectDefId, t_ToObjectId, t_ToEndPointName
      from api.relationships r
      join api.relationshipdefs rd on rd.RelationshipDefId = r.RelationshipDefId
     where r.RelationshipId = a_RelationshipId
       and rd.FromObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_ABC_License')
       and r.ToEndPoint = '1';

    if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'DoNotRunPostVerifyProcedures') = 'N' then

      t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_License', t_ToEndPointName);

      for i in (select r.ToObjectId, r.RelationshipId, p.SecondaryLicenseObjectId
                  from query.r_Abc_Primarysecondarylicense p
                  join api.relationships r on r.FromObjectId = p.SecondaryLicenseObjectId
                 where p.PrimaryLicenseObjectId = t_PrimaryLicenseObjectId
                   and r.EndPointId = t_ToEndPointId) loop

        if i.toobjectid <> t_ToObjectId then

          api.pkg_relationshipupdate.Remove(i.relationshipid);
          t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, i.secondarylicenseobjectid, t_ToObjectId);

        end if;

      end loop;

    end if;

  end SyncSecondaryLicenseInfo;

  -----------------------------------------------------------------------------
  -- CreateDefaultConditions
  --  Runs on Constructor of the ABC License (see Issue 7358)
  -----------------------------------------------------------------------------
  procedure CreateDefaultConditions(
    a_ObjectId               udt_Id,
    a_AsOfDate               date,
    a_DefaultCondCreated     char
  )
  is
    t_ABCNewCondId           number;
    t_LicCondEndPointId      number;
    t_CondTypeCondEndPointId number;
    t_LicCondRelId           number;
    t_CondTypeCondRelId      number;
    t_CondObjDefId           number;
    t_SortCount              number;
  begin
    --since this is on post verify of a license, we don't want it to run every time
    if (a_DefaultCondCreated = 'N') then
      t_SortCount              := 1;
      t_LicCondEndPointId      := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License',
                                                                        'Condition');
      t_CondTypeCondEndPointId := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_Condition',
                                                                        'ConditionType');
      t_CondObjDefId := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_Condition');
      --raise_application_error(-20000, 'Got Here! '||  t_LtObjectId|| ' ');
      for i in (select ct.ObjectId ctObjId, ct.Code, ct.ConditionText
                  from query.o_abc_license l
                  join query.r_abc_conditiontypelicensetype r
                    on r.LicenseTypeObjectId = l.LicenseTypeObjectId
                  join query.o_abc_conditiontype ct
                    on ct.ObjectId = r.ConditionTypeObjectId
                 where l.ObjectId = a_ObjectId
                   and r.DefaultConditionType = 'Y'
                   order by ct.SortOrder) loop

        t_ABCNewCondId := api.pkg_objectupdate.New(t_CondObjDefId);
        api.pkg_columnupdate.SetValue(t_ABCNewCondId, 'SortOrder', t_SortCount);
        api.pkg_columnupdate.SetValue(t_ABCNewCondId, 'ConditionText', i.conditiontext);
        t_LicCondRelId := api.pkg_relationshipupdate.New(t_LicCondEndPointId, a_ObjectId, t_ABCNewCondId);
        t_CondTypeCondRelId := api.pkg_relationshipupdate.New(t_CondTypeCondEndPointId, t_ABCNewCondId,i.ctobjid);

        t_SortCount := t_SortCount + 1;
      end loop;
    end if;
    api.pkg_columnupdate.SetValue(a_ObjectId, 'DefaultCondCreated', 'Y');
  end CreateDefaultConditions;

  -----------------------------------------------------------------------------
  -- SetNextInspectionDate
  --  Runs on "Approve" outcome of Approve/Reject Application process and
  --  "Close" outcome of Close Inspection process.
  --  Also runs on Post-Verify of o_ABC_RiskFactor if frequency or units are changed.
  -----------------------------------------------------------------------------
  procedure SetNextInspectionDate(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  )
  is
    t_ObjectDefName          varchar2(30) := api.pkg_columnquery.Value(a_ObjectId, 'ObjectDefName');
    t_LicensesToUpdate       udt_IdList;
    t_LicenseNumber          varchar2(50);
    t_LicenseId              udt_Id;
    t_RiskFactorId           udt_Id;
    t_FrequencyUnits         varchar2(50);
    t_InspectionFrequency    number;
    t_Today                  date := trunc(sysdate);
    t_LastInspection         date;
    t_NextInspection         date;
    t_IssueDate              date;
    t_InProgressCount        number;
  begin

    null;

/* Commented out this procedure since New Jersey is not using ABC for their inspections anyway. Since RiskFactor has been eliminated
   from the License, this code needs to be changed to not use RiskFactor for determining the next inspection date. */

/*    --if run from Risk Factor, get the Licenses that use it.
    if t_ObjectDefName = 'o_ABC_RiskFactor' then
      select r.LicenseObjectId bulk collect into t_LicensesToUpdate
        from query.r_ABC_LicenseRiskFactor r
        join query.o_Abc_License o on o.ObjectId = r.LicenseObjectId
       where r.RiskFactorObjectId = a_ObjectId
         and o.State in ('Active','Suspended');
      t_RiskFactorId := a_ObjectId;
    else --this is run from a process, and we only have 1 License
      t_LicenseId := api.pkg_columnquery.NumericValue(api.pkg_columnquery.NumericValue(a_ObjectId, 'JobId'), 'LicenseObjectId');
      if t_LicenseId is not null then
        t_LicensesToUpdate(1) := t_LicenseId;
        t_RiskFactorId := api.pkg_columnquery.NumericValue(t_LicenseId, 'RiskFactorObjectId');
      end if;
    end if;

    t_FrequencyUnits := api.pkg_columnquery.Value(t_RiskFactorId, 'FrequencyUnits');
    t_InspectionFrequency := api.pkg_columnquery.NumericValue(t_RiskFactorId, 'InspectionFrequency');

    for i in 1..t_LicensesToUpdate.count loop --only 1 if run from a process
      t_LicenseNumber := nvl(api.pkg_columnquery.Value(t_LicensesToUpdate(i), 'LicenseNumber'), 'none');
      t_IssueDate := nvl(api.pkg_columnquery.DateValue(t_LicensesToUpdate(i), 'IssueDate'), t_Today);

      --find the last Inspection with this License Number
      select nvl(max(IssueDate), t_IssueDate) into t_LastInspection from (
        (select \*+cardinality(x 1)*\max(j.InspectionDate) IssueDate
          from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ABC_License', 'LicenseNumber', t_LicenseNumber) as api.udt_ObjectList)) x
          join query.r_Abc_Inspectionjoblicense r on r.LicenseObjectId = x.objectid
          join query.j_abc_inspection j on j.ObjectId = r.InspectionJobId
         where j.StatusName = 'COMP')
       union all  --include any "pre-approval" Inspection with no License Number yet
        (select max(j2.InspectionDate) IssueDate
          from query.r_Abc_Inspectionjoblicense r
          join query.j_abc_inspection j2 on j2.ObjectId = r.InspectionJobId
         where r.LicenseObjectId = t_LicensesToUpdate(i)
           and j2.StatusName = 'COMP'));

      --calculate the next inspection date
      t_NextInspection :=
        case t_FrequencyUnits
          when 'Days' then t_LastInspection + t_InspectionFrequency
          when 'Months' then ADD_MONTHS(t_LastInspection,t_InspectionFrequency)
          when 'Years' then ADD_MONTHS(t_LastInspection,t_InspectionFrequency*12)
        end;

      --if it's in the past, make it Today
      if t_NextInspection < t_Today then
        t_NextInspection := t_Today;
      end if;

      --find any In Progress inspection with no License Number yet
      if t_LicenseNumber = 'none' then
        select count(j.ObjectId) into t_InProgressCount
          from query.r_Abc_Inspectionjoblicense r
          join query.j_abc_inspection j on j.ObjectId = r.InspectionJobId
         where r.LicenseObjectId = t_LicensesToUpdate(i)
           and j.CompletedDate is null;
      else --find In Progress inspections with this License Number
        select \*+cardinality(x 1)*\count(j.ObjectId) into t_InProgressCount
          from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ABC_License', 'LicenseNumber', t_LicenseNumber) as api.udt_ObjectList)) x
          join query.r_Abc_Inspectionjoblicense r on r.LicenseObjectId = x.objectid
          join query.j_abc_inspection j on j.ObjectId = r.InspectionJobId
         where j.CompletedDate is null;
      end if;

      --if no In Progress Inspections, set DateOfNextInspection
      if t_InProgressCount = 0 then
        api.pkg_columnupdate.SetValue(t_LicensesToUpdate(i), 'DateOfNextInspection', t_NextInspection);
      end if;
    end loop;*/

  end SetNextInspectionDate;

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2015 Mar 27
  -- Purpose    : Determines if a given job is a Major Application or not
  -------------------------------------
  function IsMajorJob(
    a_JobId                             udt_Id
  ) return boolean is
  begin
    case api.pkg_ColumnQuery.Value(a_JobId, 'ObjectDefName')
      when 'j_ABC_NewApplication' then
        return true;
      when 'j_ABC_RenewalApplication' then
        return true;
      when 'j_ABC_AmendmentApplication' then
        if api.pkg_ColumnQuery.Value(a_JobId, 'MajorAmendment') = 'Y' then
          return true;
        else
          return false;
        end if;
    end case;

    return false;
  end IsMajorJob;

    --------------------------------------------------------------------------------------------------
  -- Programmer : Yars T
  -- Date       : 2015 Sept 2
  -- Purpose    : Determines if the license has open renewals when the expiration date is in the past
  ---------------------------------------------------------------------------------------------------
  function HasRenewals(
    a_LicenseId                          udt_Id
  ) return varchar2 is
  begin

      for i in (
        SELECT api.pkg_columnquery.Value(r.RenewJobId, 'StatusName') status
        FROM query.r_ABC_LicenseToRenewJobLicense r
        where r.LicenseObjectId = a_LicenseId) loop

        if i.status in ('NEW', 'AWAIT', 'REVIEW', 'HOLD', 'DIST') then
          return 'Yes';
        end if;
      end loop;

      --if it reaches here that means no open renewal jobs found for this license
      return 'No';

  end HasRenewals;

  -----------------------------------------------------------------------------
  --  LicenseCancellation
  --  Runs as procedure on constructor of rel r_ABC_LicenseCancellation
  --  handles license cancellation
  -----------------------------------------------------------------------------
  procedure LicenseCancellation(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is

    t_LicenseObjectId        udt_Id;
    t_CancellationObjectId   udt_Id;
    t_LicenseState           varchar2(20);
    t_relid                  number;

  begin

    select r.LicenseObjectId
         , r.CancellationObjectId
      into t_LicenseObjectId
         , t_CancellationObjectId
      from query.r_ABC_LicenseCancellation r
     where r.RelationshipId = a_ObjectId;

    -- get the State of the license
    t_LicenseState := api.pkg_columnquery.value(t_LicenseObjectId,'State');

    -- update the License StatePriorToCancellation with the State Value
    api.pkg_columnupdate.SetValue(t_LicenseObjectId, 'StatePriorToCancellation', t_LicenseState);

    -- set State on License to Cancelled
    api.pkg_columnupdate.setvalue(t_LicenseObjectId,'State','Cancelled');

    -- set details on cancellation object
    api.pkg_columnupdate.SetValue(t_CancellationObjectId,'CancelledByUserName',
      api.pkg_columnquery.value(t_CancellationObjectId, 'UserFormattedName2'));

    api.pkg_columnupdate.SetValue(t_CancellationObjectId,'CancelledDate',
      api.pkg_columnquery.value(t_CancellationObjectId, 'CurrentDate'));

  end LicenseCancellation;

  -----------------------------------------------------------------------------
  --  UndoCancellation
  --  Runs as a procedure on change of UndoReason detail of o_ABC_LicenseCancellation
  --  handles Undo Cancellation processing
  -----------------------------------------------------------------------------
  procedure UndoCancellation(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is

    t_LicenseObjectId        udt_Id;
    t_LicenseState           varchar2(20);

  begin

    -- an Undo Reason must be provided
    if api.pkg_columnquery.value(a_objectid,'UndoReason') is not null then

      -- get the LicenseObjectId
      select r.LicenseObjectId
        into t_LicenseObjectId
        from query.r_ABC_LicenseCancellation r
       where r.CancellationObjectId = a_ObjectId;

      -- get the State of the license -- should be cancelled
      t_LicenseState := api.pkg_columnquery.value(t_LicenseObjectId,'State');

      -- this in an undo Cancellation, set State to StatePriortoCancellation
      api.pkg_columnupdate.SetValue(t_LicenseObjectId,'State',
        api.pkg_columnquery.value(t_LicenseObjectId, 'StatePriorToCancellation'));

      -- update the License StatePriorToCancellation with the original State Value
      api.pkg_columnupdate.SetValue(t_LicenseObjectId, 'StatePriorToCancellation', t_LicenseState);

      -- set details on cancellation object
      api.pkg_columnupdate.SetValue(a_ObjectId,'UndoneByUserName',
        api.pkg_columnquery.value(a_ObjectId, 'UserFormattedName2'));

      api.pkg_columnupdate.SetValue(a_ObjectId,'UndoDate',
      api.pkg_columnquery.value(a_ObjectId, 'CurrentDate'));

    else
      api.pkg_errors.RaiseError(-20000, 'To undo a license cancellation, a reason must be provided.');
    end if;

  end UndoCancellation;


  -----------------------------------------------------------------------------
  --  RelRemoveUponExpiration
  --  On expiration of the license, delete the license distributor relationships.
  -----------------------------------------------------------------------------
  procedure RelRemoveUponExpiration(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is

  t_LicenseObjectId          udt_Id;
  t_ToObjectId               udt_Idlist;
  t_CurrentEP                udt_Id;
  t_HistoricEP               udt_Id;
  t_RelationshipId           number;
  t_ProdIds                  udt_IdList;
  t_NewRel                   udt_Id;
  t_ObjectDef                 varchar2(20) := api.pkg_columnquery.Value(a_ObjectId, 'ObjectDefName');

begin
  if t_ObjectDef = 'o_ABC_License' then
     t_LicenseObjectId := a_ObjectId;
  else
     t_LicenseObjectId := api.pkg_columnquery.numericvalue(api.pkg_columnquery.numericvalue(a_ObjectId,'JobId'),'LicenseObjectId');
  end if;
  t_HistoricEP              := api.pkg_configquery.EndPointIdForName('o_ABC_License','HistoricDist');


   t_ProdIds := extension.pkg_objectquery.RelatedObjects(t_LicenseObjectId,'DistributorProduct');

   if t_ProdIds.count > 0 then
     for i in 1..t_ProdIds.count loop

       -- get current rel
       select r.RelationshipId
         into t_RelationshipId
         from query.r_ABCProduct_Distributor r
        where r.LicenseId = t_LicenseObjectId
          and r.ProductId = t_ProdIds(i);

       -- remove current rel
       api.pkg_relationshipupdate.Remove(t_RelationshipId);

       -- add historical rel
       t_NewRel := api.pkg_relationshipupdate.new(t_HistoricEP, t_LicenseObjectId, t_ProdIds(i));

     end loop;
   end if;
  end RelRemoveUponExpiration;

  -----------------------------------------------------------------------------
  --  OnlineAppDocumentVerify
  --  On change of License Municipality, verify online documents
  -----------------------------------------------------------------------------
  procedure OnlineAppDocumentVerify(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_JobId                  udt_Id;

  begin

    if api.pkg_columnquery.value(a_ObjectId, 'Office') is not null then

      if api.pkg_columnquery.value(a_ObjectId, 'NewApplicationJobId') is not null then
        t_JobId := api.pkg_columnquery.value(a_ObjectId, 'NewApplicationJobId');
      elsif api.pkg_columnquery.value(a_ObjectId, 'RenewalApplicationJobId') is not null then
        t_JobId := api.pkg_columnquery.value(a_ObjectId, 'RenewalApplicationJobId') ;
      else t_JobId := api.pkg_columnquery.value(a_ObjectId, 'AmendmentApplicationJobId');
      end if;

      for c in (select r.fromobjectid DocumentId
                  from api.relationships r
                  join api.relationshipdefs rd on rd.RelationshipDefId = r.RelationshipDefId
                 where r.ToObjectId = t_JobId
                   and rd.tolabel = 'Online Document:') loop

          api.pkg_objectupdate.Verify(c.DocumentId);

      end loop;

    end if;

  end OnlineAppDocumentVerify;

  -----------------------------------------------------------------------------
  -- ValidateLicenseLetterType
  --  Runs on Post-Verify of LicenseLetter object.
  --  Purpose:
  --    Verify a LetterType is given for a Letter.
  -----------------------------------------------------------------------------
  procedure ValidateLicenseLetterType(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_ValidateData         varchar2(1) := 'N';
  begin
    if api.pkg_columnQuery.VALUE (a_ObjectId,'LetterType') is null then
       api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Letter Type.');
    end if;

  end ValidateLicenseLetterType;

  -----------------------------------------------------------------------------
  --  VerifyEventDate
  --  Verify Event Date entered
  -----------------------------------------------------------------------------
  procedure VerifyEventDate(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_JobId                  udt_Id;

  begin
    if api.pkg_columnquery.value(a_ObjectId, 'StartDate') is null then
       api.pkg_errors.RaiseError(-20000,'Please enter a Date.');
    end if;
    if api.pkg_columnquery.value(a_ObjectId, 'IsMultiDayEvent') = 'Y' and
       api.pkg_columnquery.value(a_ObjectId, 'EndDate') is null then
       api.pkg_errors.RaiseError(-20000,'Before you go on, you must enter a value for End Date.');
    end if;
  end VerifyEventDate;

  -----------------------------------------------------------------------------
  -- ValidateCondition
  --  Runs on Post-Verify of Condition object.
  --  Purpose:
  --    Verify certain fields are entered
  -----------------------------------------------------------------------------
  procedure ValidateCondition(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_JobId                  udt_Id;

  begin
    if api.pkg_columnquery.Value (a_ObjectId, 'SortOrder') is null then
       api.pkg_errors.RaiseError(-20000,'Please enter a Sort Order.');
    end if;
    if api.pkg_columnquery.Value (a_ObjectId, 'ConditionTypeObjectId') is null then
       api.pkg_errors.RaiseError(-20000,'Please select a Code.');
    end if;
  end ValidateCondition;

  -----------------------------------------------------------------------------
  -- SellerRelPostVerifyWrapper
  --  Runs on Post-Verify of the Seller License relationship.
  -----------------------------------------------------------------------------
  procedure SellerRelPostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_PermitType          varchar2(20);
    t_LicenseState        varchar2(20);
    t_LabelType           varchar2(4000);
  begin
    select api.pkg_columnquery.Value(sl.PermitObjectId, 'permittypecode'),
        api.pkg_columnquery.Value(sl.LicenseObjectId, 'state'),
        api.pkg_columnquery.Value(sl.PermitObjectId, 'LicenseTapInformationLabel')
    into t_PermitType, t_LicenseState, t_LabelType
    from query.r_sellerslicense sl
    where sl.RelationshipId = a_ObjectId;

    if t_PermitType = 'RR' and t_LicenseState <> 'Active' then
      api.pkg_errors.RaiseError(-20000,'The selected '||t_LabelType||' license must be active.');
    end if;

  end SellerRelPostVerifyWrapper;

  -----------------------------------------------------------------------------
  -- RelatedLicenseConstructor
  --  Runs on Constructor of the Related License relationship.
  -----------------------------------------------------------------------------
  procedure RelatedLicenseConstructor (
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_RelatedLicenseObjectId udt_id;
    t_LicenseObjectId        udt_id;
    t_LicenseeObjectId       udt_id;
    t_ExistingLicenseeId     udt_Id;
    t_LicenseLicenseeEP      udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_License','Licensee');
    t_RelId                  udt_id;
    t_LicenseeRelId          udt_id;

  begin
    select al.AddtlWhseLicenseObjectId, al.LicenseObjectId
      into t_LicenseObjectId, t_RelatedLicenseObjectId
      from query.r_abc_addtlwarehouselicense al
     where al.RelationshipId = a_ObjectId;

    select sl.LegalEntityObjectId
      into t_LicenseeObjectId
      from query.r_ABC_LicenseLicenseeLE sl
     where sl.LicenseObjectId = t_RelatedLicenseObjectId;

    begin

    select sl.LegalEntityObjectId, sl.RelationshipId
      into t_ExistingLicenseeId, t_LicenseeRelId
      from query.r_ABC_LicenseLicenseeLE sl
     where sl.LicenseObjectId = t_LicenseObjectId;

    exception when no_data_found then

      t_ExistingLicenseeId := null;

    end;

    if t_ExistingLicenseeId is null then

      t_RelId := api.pkg_relationshipupdate.New(t_LicenseLicenseeEP, t_LicenseObjectId, t_LicenseeObjectId);

    elsif t_ExistingLicenseeId  is not null
      and t_LicenseeObjectId != t_ExistingLicenseeId then

      api.pkg_relationshipupdate.Remove(t_LicenseeRelId);
      t_RelId := api.pkg_relationshipupdate.New(t_LicenseLicenseeEP, t_LicenseObjectId, t_LicenseeObjectId);

    end if;



  end RelatedLicenseConstructor;

  -----------------------------------------------------------------------------
  -- RelatedLicenseDestructor
  --  Runs on Destructor of the Related License relationship.
  -----------------------------------------------------------------------------
  procedure RelatedLicenseDestructor (
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_LicenseObjectId        udt_id;
    t_RelationshipId         udt_id;
  begin
    select al.LicenseObjectId
      into t_LicenseObjectId
      from query.r_abc_addtlwarehouselicense al
     where al.RelationshipId = a_ObjectId;

    select lle.RelationshipId
      into t_RelationshipId
      from query.r_ABC_LicenseLicenseeLE lle
     where lle.LicenseObjectId = t_LicenseObjectId;

    api.pkg_relationshipupdate.Remove (t_RelationshipId);
  end RelatedLicenseDestructor;

  -----------------------------------------------------------------------------
  -- StateChangeActivities
  --  Runs on change of the State detail
  --  Run any activities that may need to happen when the License State changes
  -----------------------------------------------------------------------------
  procedure StateChangeActivities(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
  begin
    --Run verify on all related establishments to make sure the dup_LicenseList detail stays correct.
    for i in (select *
                from query.r_abc_licenseestablishment e
               where e.LicenseObjectId = a_ObjectId) loop
      api.pkg_objectupdate.Verify(i.establishmentobjectid);
    end loop;
  end StateChangeActivities;

  /*---------------------------------------------------------------------------
   * UpgradeDowngradeLicense() - PUBLIC
   *   Upgrade/Downgrade License. Move Products from the Source License to the
   * Target License.
   *-------------------------------------------------------------------------*/
  procedure UpgradeDowngradeLicense(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_LicTypeIsValid                    varchar2(01);
    t_TargetLicenseId                   udt_id;
    t_SourceLicenseId                   udt_id;
    t_Products                          api.udt_ObjectList;
    t_RelId                             udt_id;
    t_ProductExists                     udt_id;
    t_CurrentEndPointId                 udt_id :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'DistributorLic');
    t_HistoricalEndPointId              udt_id :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'HistoricProducts');
    t_RelatedEndPointId                 udt_id :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'RegistrantLic');
    t_UpgradeEndPointId                 udt_id :=
        api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseSource');
    t_Objects                           api.udt_ObjectList;
    t_LicenseNumberNoGeneration         varchar2(11);
  begin

    t_TargetLicenseId := api.pkg_columnquery.Value(a_ObjectId,
        'TargetLicenseObjectId');
    t_SourceLicenseId := api.pkg_columnquery.Value(a_ObjectId,
        'SourceLicenseIdStored');
    t_LicenseNumberNoGeneration := api.pkg_columnquery.Value(t_SourceLicenseId,
        'LicenseNumberNoGeneration');

    if t_TargetLicenseId is null then
      api.pkg_errors.RaiseError(-20000, 'You must specify a License to transfer' ||
          ' your Products to.');
    end if;
    if t_TargetLicenseId = t_SourceLicenseId then
      api.pkg_errors.RaiseError(-20000, 'You must specify a different License' ||
          ' to transfer your Products to.');
    end if;
    t_LicTypeIsValid := api.pkg_columnquery.value(t_TargetLicenseId,
        'IsValidAsRegistrantDistributor');
    if t_LicTypeIsValid = 'N' then
      api.pkg_errors.RaiseError(-20000, 'The License you selected is not a' ||
          ' valid Registrant / Distributor License. Please select another' ||
          ' License.');
    end if;

    -- Find current Products on the Source License
    for p in (
        select r.ProductId, r.RelationshipId
        from query.r_ABCProduct_Distributor r
        where r.LicenseId = t_SourceLicenseId
        ) loop
      -- Copy Product from Source License to Target License (if not exists)
      begin
        select r1.RelationshipId
        into t_ProductExists
        from query.r_ABCProduct_Distributor r1
        where r1.ProductId = p.productid
          and r1.LicenseId = t_TargetLicenseId;
      exception
        when no_data_found then
          t_RelId := api.pkg_relationshipupdate.New(t_CurrentEndPointId,
              p.productid, t_TargetLicenseId, sysdate);
      end;

      -- Move Product from Source License Current to Historical
      t_RelId := api.pkg_relationshipupdate.New(t_HistoricalEndPointId,
          p.productid, t_SourceLicenseId, sysdate);
      api.pkg_relationshipupdate.Remove(p.relationshipid);

      -- If Product is on Source License as Related License, move to Target
      -- License as Related License
      for l in (select
                  r2.RelationshipId,
                  r2.LicenseId
                from query.r_ABCProduct_RegistrantLicense r2
                where r2.ProductId = p.productid
               ) loop
        if t_LicenseNumberNoGeneration = api.pkg_columnquery.Value(l.LicenseId,
            'LicenseNumberNoGeneration') then
          t_RelId := api.pkg_relationshipupdate.New(t_RelatedEndPointId,
              p.productid, t_TargetLicenseId, sysdate);
          api.pkg_relationshipupdate.Remove(l.RelationshipId);
        end if;
      end loop;
    end loop;

    -- Create Source to Target License Relationship
    begin
      select r1.RelationshipId
      into t_RelId
      from query.r_ABC_SourceTargetLicense r1
      where r1.SourceLicenseId = t_SourceLicenseId
        and r1.TargetLicenseId = t_TargetLicenseId;
    exception
      when no_data_found then
        t_RelId := api.pkg_relationshipupdate.New(t_UpgradeEndPointId,
            t_SourceLicenseId, t_TargetLicenseId, sysdate);
    end;

  end UpgradeDowngradeLicense;

  /*---------------------------------------------------------------------------
   * CopyDefaultAttachmentToLicenseLetter()
   *   Copies system settings detail to license letter
   *-------------------------------------------------------------------------*/
  procedure CopyDefaultAttachmentToLicenseLetter(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_DefaultAttach                     varchar2(1);
    t_LicenseLetterId                   udt_Id;
  begin

    select d.DefaultAttachment, r.LicenseLetterId
    into t_DefaultAttach, t_LicenseLetterId
    from query.r_ABC_LicLetterWordTemplate r
    join query.d_wordinterfacetemplate d
      on r.WordMergeTemplateId = d.ObjectId
    where r.RelationshipId = a_ObjectId;

    api.pkg_ColumnUpdate.SetValue(t_LicenseLetterId, 'AttachToEmail', t_DefaultAttach);

  end CopyDefaultAttachmentToLicenseLetter;

end pkg_ABC_License;
/
