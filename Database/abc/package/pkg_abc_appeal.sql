create or replace package pkg_ABC_Appeal is

  subtype udt_id is api.pkg_definition.udt_id;
  subtype udt_idlist is api.pkg_definition.udt_idlist;   
    
 /*---------------------------------------------------------------------------
  * PostVerify()
  * Set the AppealNumber and default legal entities from the System object.
  *-------------------------------------------------------------------------*/ 
  procedure PostVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * DueDateAlignment()
  * When either the Job or the Process is updated, update the date on the
  * other.
  *-------------------------------------------------------------------------*/   
  procedure DueDateAlignment(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * UpdateOALDate()
  * Update the OAL Date on the job from either Initial Process or Manage Hearing.
  *-------------------------------------------------------------------------*/   
  procedure UpdateOALDate(
    a_ProcessId                         udt_id,
    a_AsOfDate                          date
  );

end pkg_ABC_Appeal;
/
create or replace package body pkg_ABC_Appeal is

  -- Author  : Cameron Nelson
  -- Created : 9/18/2014

 /*---------------------------------------------------------------------------
  * PostVerify()
  * Set the AppealNumber and default legal entities from the System object.
  *-------------------------------------------------------------------------*/   
  procedure PostVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is 
    t_AppealProcessorId                 udt_Id;
    --t_RevenueClerkId                    udt_Id;
    t_AttorneyGeneralId                 udt_Id;
    t_AppealFineClerk                   udt_Id;
    t_AppealObject                      udt_Id;
    t_AppealObjectRevClerk              udt_Id;
    t_AppealObjectDAG                   udt_Id;
    t_AppealNumber                      number :=
        api.pkg_columnquery.Value(a_ObjectId, 'AppealNumber');
    t_AppealProcessorEndPoint           udt_Id := 
        api.pkg_configquery.EndPointIdForName('j_abc_Appeal', 'DefaultProcessor');
    t_DefaultClerkEndPoint              udt_Id := 
       api.pkg_configquery.EndPointIdForName('j_abc_Appeal', 'AppealRevClerk'); 
    t_DAGEndPoint                       udt_Id := 
       api.pkg_configquery.EndPointIdForName('j_abc_Appeal', 'AttorneyGeneral');
    t_AppealFineClerkEndPoint           udt_Id :=             
       api.pkg_configquery.EndPointIdForName('j_abc_Appeal', 'AppealFineClerk');
 
 
 begin
  -- Set the Appeal Number
    if t_AppealNumber is NULL then
      toolbox.pkg_counter.GenerateCounter(a_ObjectId, sysdate, '[9999999]', 'ABC Appeal Number', 'AppealNumber');
    end if; 
    
  -- Default Processor
    begin
      select aap.UserId
        into t_AppealProcessorId
        from query.r_ABC_AppealAppealProcessor aap
       where aap.AppealId = a_ObjectId;
    exception
      when no_data_found then
      select sap.AppealProcessorId
          into t_AppealProcessorId
          from query.r_ABC_SystemAppealProcessor sap;
         
        t_AppealObject := api.pkg_relationshipupdate.New(t_AppealProcessorEndPoint, a_objectId, t_AppealProcessorId);
    end;      

--Default Revenue Clerk
/*     begin
      select rc.UserId
        into t_RevenueClerkId
        from query.r_ABC_AppealRevenueClerk rc
       where rc.AppealId = a_ObjectId;
     exception
      when no_data_found then
        select rcs.UserId
          into t_RevenueClerkId
          from query.r_DefaultRevClerkSysSetting rcs;
         
        t_AppealObject := api.pkg_relationshipupdate.New(t_DefaultClerkEndPoint, a_objectId, t_RevenueClerkId);
     end;  */   
     
 --Default Attorney General
     begin
      select at.UserId
        into t_AttorneyGeneralId
        from query.r_ABC_AppealDeputyAttorneyGen at
       where at.AppealId = a_ObjectId;
     exception
      when no_data_found then
        select dag.AttorneyGeneralId
          into t_AttorneyGeneralId
          from query.r_ABC_AppealDAG dag;
          
        t_AppealObject := api.pkg_relationshipupdate.New(t_DAGEndPoint, a_objectId, t_AttorneyGeneralId);
     end;     
     
 --Default Fine Clerk
     begin
      select afc.UserId
        into t_AppealFineClerk
        from query.r_ABC_AppealAppealFineClerk afc
       where afc.AppealId = a_ObjectId;
     exception
      when no_data_found then
        select afc.AppealFineClerkId
          into t_AppealFineClerk
          from query.r_ABC_AppealFineClerk afc;
         
        t_AppealObject := api.pkg_relationshipupdate.New(t_AppealFineClerkEndPoint, a_objectId, t_AppealFineClerk);
     end;          
  end;
  
  
  
 /*---------------------------------------------------------------------------
  * DueDateAlignment()
  * When either the Job or the Process is updated, update the date on the
  * other.
  *-------------------------------------------------------------------------*/     
  
procedure DueDateAlignment(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is 

 t_ObjectDefName         varchar2(40) := api.pkg_columnquery.Value(a_ObjectId, 'ObjectDefName');
 t_OrderDueDate          date         := api.pkg_columnquery.DateValue(a_ObjectId, 'OrderDueDate');
 t_ScheduledCompleteDate date         := api.pkg_columnquery.DateValue(a_ObjectId, 'ScheduledCompleteDate');
 t_ProcessId             udt_id;
 t_JobId                 udt_id;
 t_RelExists             number;
 
 
 begin

   -- On the Appeal job, copy the Order Due Date to the Scheduled Complete Date on the Draft Final Order process
   if t_ObjectDefName = 'j_ABC_Appeal' then
    if t_OrderDueDate is not NULL then
           
   -- Find the Scheduled Complete Date of the Process
      begin
      select dfo.ScheduledCompleteDate, dfo.ProcessId
        into t_ScheduledCompleteDate, t_ProcessId
        from query.p_Abc_Draftfinalorder dfo
       where dfo.JobId = a_ObjectId;
      exception
        when no_data_found then
          return;
      end;
       
       -- If the two dates are different, sync the dates. Otherwise, do nothing.
       if t_OrderDueDate != t_ScheduledCompleteDate then
         api.pkg_logicaltransactionupdate.StartTransaction();
           api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'ScheduledCompleteDate', t_OrderDueDate);
         api.pkg_logicaltransactionupdate.EndTransaction();
       else
         return; 
       end if;
       
     end if;
   end if;
   
  -- On the Draft Final Order process, copy the Scheduled Complete Date to the Order Due Date on the Appeal job
  if t_ObjectDefName = 'p_ABC_DraftFinalOrder' then
    if t_ScheduledCompleteDate is not NULL then
   
   -- Find the Order Due Date on the Job
      select a.OrderDueDate, a.JobId
        into t_OrderDueDate, t_JobId
        from query.p_Abc_Draftfinalorder dfo
        join query.j_abc_appeal a on dfo.JobId = a.JobId
       where dfo.ObjectId = a_ObjectId;

       -- If the two dates are different, sync the dates. Otherwise, do nothing.
       if t_OrderDueDate != t_ScheduledCompleteDate then
         api.pkg_logicaltransactionupdate.StartTransaction();
           api.pkg_ColumnUpdate.SetValue(t_JobId, 'OrderDueDate', t_ScheduledCompleteDate);
         api.pkg_logicaltransactionupdate.EndTransaction();
       else
         return; 
       end if;

     end if;
   end if;
        
  end;
  
 /*---------------------------------------------------------------------------
  * UpdateOALDate()
  * Update the OAL Date on the job from either Initial Process or Manage Hearing.
  *-------------------------------------------------------------------------*/     
  
procedure UpdateOALDate(
    a_ProcessId                         udt_id,
    a_AsOfDate                          date
  ) is 
 t_JobId                                udt_id := api.pkg_columnquery.NumericValue(a_ProcessId, 'JobId');
 
 begin
   -- On the Appeal job, set the Date Sent To OAL to be the value of the date of the process
   api.pkg_columnupdate.SetValue(t_JobId, 'DateSentToOAL', api.pkg_columnquery.DateValue(a_ProcessId, 'DateSentToOAL'), sysdate);
end UpdateOALDate;
  
end pkg_ABC_Appeal;
/
