create or replace package pkg_ABC_Processes is

  -- Author  : BRUCE.JAKEWAY
  -- Created : 2011 Apr 19 11:51:26
  -- Purpose : Process procedures

  -- Public type declarations
  subtype udt_Id is api.pkg_Definition.udt_Id;

  -- Function and procedure implementations

  /*---------------------------------------------------------------------------
   * CopyTemplateToChecklist()
   *   Copies a checklist template from the process type to a new checklist on
   * the process.
   *-------------------------------------------------------------------------*/
  procedure CopyTemplateToChecklist(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_ProcessTypeId                     udt_Id
  );

  ------------------------------------------------------------------------------
  -- CreateReminder
  --  Create a reminder process on the job for the specified date.  This code
  -- assumes the Reminder process is added to the job.
  ------------------------------------------------------------------------------
  procedure CreateReminder(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_JobId                             udt_Id,
    a_ReminderDate                      date,
    a_Note                              varchar2
  );

  /*-------------------------------------------------------------------------*
   * VerifyChecklist
   * Ensures that all checklist items that are mandatory for this outcome are
   *   checked.
   *-------------------------------------------------------------------------*/
  procedure VerifyChecklist(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_Outcome                           varchar
  );

  ------------------------------------------------------------------------------
  -- CopySubmitResolutionItems
  --  Copiescomments and checklist status to repeat Submit Resolution processes
  ------------------------------------------------------------------------------
  procedure CopySubmitResolutionItems(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_Processes;
/
create or replace package body pkg_ABC_Processes is

  -- Function and procedure implementations

  /*---------------------------------------------------------------------------
   * CopyTemplateToChecklist() -- PUBLIC
   *   Copies a checklist template from the process type to a new checklist on
   * the process.
   *-------------------------------------------------------------------------*/
  procedure CopyTemplateToChecklist(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_ProcessTypeId                     udt_Id
  ) is
    t_AmendmentTypeObjectId             udt_Id;
    t_EndpointId                        udt_Id;
    t_IncludeAmendment                  boolean;
    t_IncludeInsp                       boolean;
    t_IncludeJob                        boolean;
    t_IncludeLic                        boolean;
    t_IncludePermit                     boolean;
    t_InspectionTypeObjectId            udt_Id;
    t_JobId                             udt_id;
    t_JobType                           varchar2(30);
    t_JobTypeObjectId                   udt_Id;
    t_LicenseTypeObjectId               udt_Id;
    t_ObjectDefId                       udt_Id;
    t_ObjectId                          udt_Id;
    t_PermitTypeObjectId                udt_Id;
    t_PreviousProcessId                 udt_id;
    t_Process                           varchar2(200) :=
        api.pkg_columnquery.Value(a_ProcessId, 'ObjectDefName');
    t_ProcessTypeObjectId               udt_Id;
    t_RelId                             udt_Id;

    cursor c_ChecklistTemplate is
      select
        o.ObjectId,
        o.SortOrder,
        o.Name,
        o.Description,
        o.Mandatory,
        o.AllOrSomeLicTypes,
        o.AllOrSomeInspectionTypes,
        o.AllOrSomeAmendmentTypes,
        o.AllOrSomeJobTypes,
        o.AllOrSomePermitTypes,
        o.InternalOnly
      from
        query.r_ProcessTypeChecklistTemplate r
      join query.o_ABC_ChecklistTemplate o
          on r.ChecklistTemplateObjectId = o.ObjectId
      where r.ProcessTypeObjectId = t_ProcessTypeObjectId
        and o.Active = 'Y'
      order by o.SortOrder;

    cursor c_ChecklistFromProcess is
      select
        o.ObjectId,
        o.SortOrder,
        o.Name,
        o.Description,
        o.Completed,
        o.CompletedDate,
        o.Mandatory,
        o.InternalOnly
      from
        api.relationships r
      join query.o_abc_checklist o
        on o.objectid = r.ToObjectId
      where r.FromObjectId = t_PreviousProcessId
      order by o.SortOrder;

  begin

    t_JobId := api.pkg_columnquery.NumericValue(a_ProcessId, 'JobId');
    -- No need for checklist items for processes on Product Non-Renewal job.
    if api.pkg_columnquery.Value(t_JobId, 'ObjectDefName') =
        'j_ABC_PRNonRenewal' then
      return;
    end if;

    if nvl(api.pkg_ColumnQuery.Value(a_ProcessId, 'IsSystemProcess'), 'N') = 'N' then
      t_EndpointId := api.pkg_ConfigQuery.EndPointIdForName
          (a_ProcessTypeId, 'Checklist');
      t_ObjectDefId := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_Checklist');
      t_ProcessTypeObjectId := api.pkg_SimpleSearch.ObjectByIndex('o_ProcessType',
          'ProcessTypeId', a_ProcessTypeId);
      t_LicenseTypeObjectId := api.pkg_columnquery.NumericValue
          (api.pkg_columnquery.NumericValue(api.pkg_columnquery.NumericValue
          (a_ProcessId, 'JobId'), 'LicenseObjectId'), 'LicenseTypeObjectId');
      t_AmendmentTypeObjectId := api.pkg_columnquery.NumericValue
          (t_JobId, 'AmendmentTypeObjectId');
      t_JobType := api.pkg_columnquery.Value(t_JobId, 'ObjectDefName');
      t_PermitTypeObjectId := api.pkg_columnquery.NumericValue
          (api.pkg_columnquery.NumericValue(api.pkg_columnquery.NumericValue
          (a_ProcessId, 'JobId'), 'PermitObjectId'), 'PermitTypeObjectId');

      begin
        select /*cardinality (x 1) */ j.JobTypeId
        into t_JobTypeObjectId
        from api.jobtypes j
        where j.Name = t_JobType;
      exception
        when no_data_found then
          return;
      end;

      t_InspectionTypeObjectId := api.pkg_columnquery.NumericValue
          (api.pkg_columnquery.NumericValue(a_ProcessId, 'JobId'), 'InspectionTypeObjectId');
      if api.pkg_columnquery.NumericValue(t_JobId, 'PreviousProcessId') is not null and
          api.pkg_columnquery.Value(a_ProcessId, 'ObjectDefName') != 'p_ABC_ReceiptNewInformation'
          then
        t_PreviousProcessId := api.pkg_columnquery.Value(t_JobId, 'PreviousProcessId');
        for l in c_ChecklistFromProcess loop
          t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'SortOrder', l.SortOrder);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Name', l.Name);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Description', l.Description);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Mandatory', l.Mandatory);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Completed', l.Completed);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'CompletedDate', l.CompletedDate);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InternalOnly', l.InternalOnly);
          t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, a_ProcessId, t_ObjectId);
        end loop;
        api.pkg_columnupdate.RemoveValue(t_JobId, 'PreviousProcessId');
        return;
      end if;

      for c in c_ChecklistTemplate loop
        t_IncludeLic := false;
        t_IncludeInsp := false;
        t_IncludeAmendment := false;
        t_IncludeJob := false;
        t_IncludePermit := false;
        -- Find License Types when 'some' is selected
        for x in (select rr.LicenseTypeObjectId
                  from query.r_LicenseTypeChecklistTemplate rr
                  where rr.ChecklistTemplateObjectId = c.Objectid
                 ) loop
          if x.LicenseTypeObjectId = t_LicenseTypeObjectId and not t_IncludeInsp then
            t_IncludeLic := true;
          end if;
        end loop;
        -- For Submit Resolution Process only
        if t_Process = 'p_ABC_SubmitResolution' then
          -- Find Amendment Types when 'some' is selected
          for x in (select at.AmendmentTypeId
                    from query.r_AmendmentTypeChecklistTemp at
                    where at.ChecklistTemplateId = c.Objectid
                   ) loop
            if x.Amendmenttypeid = t_AmendmentTypeObjectId and not t_IncludeInsp then
              t_IncludeAmendment := true;
            end if;
          end loop;
          -- Find Job Types when 'some' is selected
          for x in (select jt.JobTypeId
                    from query.r_JobTypeChecklistTemplate jt
                    where jt.ChecklistTemplateId = c.Objectid
                   ) loop
            if x.JobTypeId = t_JobTypeObjectId and not t_IncludeInsp then
              t_IncludeJob := true;
            end if;
          end loop;
        end if;
        -- Find Inspection Types when 'some' is selected
        for i in (select rr.InspectionTypeObjectId
                  from query.r_InspTypeChecklistTemplate rr
                  where rr.ChecklistTemplateObjectId = c.Objectid
                 ) loop
          if i.InspectionTypeObjectId = t_InspectionTypeObjectId then
            t_IncludeInsp := true;
          end if;
        end loop;
        if t_Process = 'p_ABC_PoliceReview' or t_Process = 'p_ABC_MunicipalityReview' then
          -- Find Permit Types when 'some' is selected
          for x in (select pt.ABCPermitTypeObjectId
                    from query.r_ABCPermitTypeChecklistTemp pt
                    where pt.ChecklistTemplateObjectId = c.ObjectId
                   ) loop
            if x.ABCPermitTypeObjectId = t_PermitTypeObjectId then
              t_IncludePermit := true;
            end if;
          end loop;
        end if;

        -- Create checklists based on Inspection Type and License Type ONLY if the
        -- process type is an inspection process
        if api.pkg_columnquery.Value(api.pkg_columnquery.NumericValue
            (a_ProcessId, 'JobId'), 'ObjectDefName') =  'j_ABC_Inspection' then
          if (c.AllOrSomeLicTypes = 'All' and c.AllOrSomeInspectionTypes = 'All') or
              (t_IncludeLic and t_IncludeInsp) or (c.Allorsomelictypes = 'All' and t_IncludeInsp)
              or (c.Allorsomeinspectiontypes = 'All' and t_IncludeLic) then
            t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
            api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'SortOrder', c.SortOrder);
            api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Name', c.Name);
            api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Description', c.Description);
            api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Mandatory', c.Mandatory);
            api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InternalOnly', c.Internalonly);
            t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, a_ProcessId, t_ObjectId);
          end if;
        else
          if t_Process = 'p_ABC_SubmitResolution' then
            -- If process is Submit Resolution, distinguish between Amendment and
            -- other jobs for which types to select
            if t_JobType = 'j_ABC_AmendmentApplication' then
              if (c.Allorsomelictypes = 'All' or t_IncludeLic) and
                  (c.Allorsomejobtypes = 'All' or t_IncludeJob) and
                  (c.Allorsomeamendmenttypes = 'All' or t_IncludeAmendment) then
                t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'SortOrder', c.SortOrder);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Name', c.Name);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Description', c.Description);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Mandatory', c.Mandatory);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InternalOnly', c.Internalonly);
                t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, a_ProcessId, t_ObjectId);
              end if;
            else
              if (c.Allorsomelictypes = 'All' or t_IncludeLic) and
                  (c.Allorsomejobtypes = 'All' or t_IncludeJob) then
                t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'SortOrder', c.SortOrder);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Name', c.Name);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Description', c.Description);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Mandatory', c.Mandatory);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InternalOnly', c.Internalonly);
                t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, a_ProcessId, t_ObjectId);
              end if;
            end if;
          else
            -- Process is Police Review or Municipality Review
            if t_Process = 'p_ABC_PoliceReview' or t_Process = 'p_ABC_MunicipalityReview' then
              if (c.allorsomepermittypes = 'All' or t_IncludePermit) then
                t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'SortOrder', c.SortOrder);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Name', c.Name);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Description', c.Description);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Mandatory', c.Mandatory);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InternalOnly', c.InternalOnly);
                t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, a_ProcessId, t_ObjectId);
              end if;
              else
               -- Process is not Submit Resolution, therefor only License Types apply
              if c.Allorsomelictypes = 'All' or t_IncludeLic then
                t_ObjectId := api.pkg_ObjectUpdate.New(t_ObjectDefId);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'SortOrder', c.SortOrder);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Name', c.Name);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Description', c.Description);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'Mandatory', c.Mandatory);
                api.pkg_ColumnUpdate.SetValue(t_ObjectId, 'InternalOnly', c.Internalonly);
                t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, a_ProcessId, t_ObjectId);
              end if;
            end if;
          end if;
        end if;
      end loop;
    end if;

  end CopyTemplateToChecklist;

  /*-------------------------------------------------------------------------*
   * VerifyChecklist
   * Ensures that all checklist items that are mandatory for this outcome are
   *   checked.
   *-------------------------------------------------------------------------*/
  procedure VerifyChecklist(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_Outcome                           varchar
  ) is
    t_Count                             number;
    t_EndpointId                        udt_Id;
    t_ProcessOutcomeObjectId            udt_Id;
    t_ProcessTypeId                     udt_Id;
    t_IgnoreChecklist                   varchar2(1);

  begin
    t_IgnoreChecklist := api.pkg_ColumnQuery.Value(a_ProcessId, 'IgnoreMandatoryChecklistItems');

    if a_Outcome is not null and (t_IgnoreChecklist = 'N' or t_IgnoreChecklist is null) then
      select p.ProcessTypeId, api.pkg_SimpleSearch.ObjectByIndex('o_ProcessOutcome', 'ProcessOutcomeId', po.ProcessOutcomeId)
        into t_ProcessTypeId, t_ProcessOutcomeObjectId
        from api.processes p
          join api.ProcessOutcomes po
            on p.JobTypeProcessTypeId = po.JobTypeProcessTypeId
              and po.Outcome = a_Outcome
        where p.ProcessId = a_ProcessId;

      t_EndpointId := api.pkg_ConfigQuery.EndPointIdForName(t_ProcessTypeId, 'Checklist');

      for x in (select r.ToObjectId from api.relationships r
          where r.FromObjectId = a_ProcessId
            and r.EndPointId = t_EndpointId) loop
        -- Set Completed if the Process was submitted Online.
        if api.pkg_columnquery.Value(a_ProcessId, 'OnlineInfoHasBeenSubmitted') = 'Y' then
           api.pkg_columnupdate.SetValue(x.ToObjectId, 'Completed', 'Y');
        end if;
        if api.pkg_columnquery.Value(x.ToObjectId, 'Completed') = 'Y' then
          api.pkg_columnupdate.SetValue(x.ToObjectId, 'CompletedDate', sysdate);
        else
          api.pkg_columnupdate.RemoveValue(x.ToObjectId, 'CompletedDate');
        end if;
      end loop;

      if api.pkg_ColumnQuery.Value(t_ProcessOutcomeObjectId, 'MandatoryChecklistsApplicable') = 'Y' then
        select count(*)
          into t_Count
          from api.relationships r
          where r.FromObjectId = a_ProcessId
            and r.EndPointId = t_EndpointId
            and exists (
              select 1
                from query.o_ABC_Checklist o
                where r.ToObjectId = o.ObjectId
                  and o.Mandatory = 'Y'
                  and o.CompletedDate is null);

        if t_Count > 0 then
          api.pkg_Errors.RaiseError(-20000, 'Before you go on, please ensure that all mandatory checklist items are completed.');
        end if;
      end if;


      select count(*)
        into t_Count
        from api.relationships r
        where r.FromObjectId = a_ProcessId
          and r.EndPointId = t_EndpointId
          and exists (
            select 1
              from query.o_ABC_Checklist o
              where r.ToObjectId = o.ObjectId
                and trunc(o.CompletedDate) > sysdate);

      if t_Count > 0 then
        api.pkg_Errors.RaiseError(-20000, 'Before you go on, please ensure that checklist items do not have a Completed Date in the future.');
      end if;
    end if;
  end VerifyChecklist;


  ------------------------------------------------------------------------------
  -- CreateReminder
  --  Create a reminder process on the job for the specified date.  This code
  -- assumes the Reminder process is added to the job.
  ------------------------------------------------------------------------------
  procedure CreateReminder(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_JobId                             udt_Id,
    a_ReminderDate                      date,
    a_Note                              varchar2
  ) is
    t_ProcessId                         udt_Id;
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('p_ABC_Reminder');

  begin
    t_ProcessId := api.pkg_ProcessUpdate.New(a_JobId, t_ProcessTypeId, null, a_ReminderDate, to_date(null), to_date(null));
    api.pkg_ColumnUpdate.SetValue(t_ProcessId, 'Note', a_Note);
    api.pkg_ProcessUpdate.Assign(t_ProcessId, api.pkg_SecurityQuery.EffectiveUserId());
  end CreateReminder;

  ------------------------------------------------------------------------------
  -- CopySubmitResolutionItems
  --  Copies comments and checklist status to repeat Submit Resolution processes
  ------------------------------------------------------------------------------
  procedure CopySubmitResolutionItems(
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_LastProcessId udt_Id;
    t_Name          varchar2(400);
    t_Completed     varchar2(1);
  begin
    --Only run if we are a Submit Resolution process
    if api.pkg_columnquery.Value(a_ProcessId, 'ObjectDefName') = 'p_ABC_SubmitResolution' then

      --Only run if this is not the first Submit Resolution on this job
      select max(p2.ProcessId)
        into t_LastProcessId
        from api.processes p
        join api.processes p2 on p2.JobId = p.JobId
                               and p2.ProcessTypeId = p.ProcessTypeId
       where p.processid = a_ProcessId
         and p2.ProcessId != p.ProcessId;
      if t_LastProcessId is not null then
        --Copy comments from the previous process
        api.pkg_columnupdate.SetValue(a_ProcessId,
                                      'CommentsMunicipal',
                                      api.pkg_columnquery.Value(t_LastProcessId, 'CommentsMunicipal'));

        --loop through checklist items to see if they need to be set to completed
        for i in (select r.ChecklistObjectId,
                         api.pkg_columnquery.value(r.ChecklistObjectId, 'Name') Name
                    from query.r_ABC_SubmitResoChecklist r
                   where r.ProcessId = a_ProcessId) loop

          --Get the completed state of the checklist item for the previous submit resolution process
          begin
            select c.Completed
              into t_Completed
             from query.r_ABC_SubmitResoChecklist r
             join query.o_abc_checklist c on c.ObjectId = r.ChecklistObjectId
            where r.ProcessId = t_LastProcessId
              and c.Name = i.Name;

            --Set checklist item to complete if previous version was completed
            if t_Completed = 'Y' then
              api.pkg_columnupdate.SetValue(i.checklistobjectid, 'Completed', 'Y');
            end if;
          exception when NO_DATA_FOUND then
            --Checklist item was added after the last process was completed
            null;
          end;
        end loop;
      end if; --Previous process exists
    end if; --Is Submit Resolution
  end CopySubmitResolutionItems;

end pkg_ABC_Processes;
/
