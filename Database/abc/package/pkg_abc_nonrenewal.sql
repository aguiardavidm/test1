create or replace package PKG_ABC_NonRenewal is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
    -----------------------------------------------------------------------------
  -- SetRegistrantRel
  --   Sets the Registrant for a Product Registation job based on the selected
  --   Legal Entity on the public application. Run on UseLegalEntityObjectId
  -----------------------------------------------------------------------------


  procedure PostVerify(
  a_ObjectId            udt_Id,
  a_AsOfDate            date
  );

 /********************************************************************************
  *  PostVerify()
  *    Wrapper for the Post Verify event on the Product Registration Non Renewal Job
  ********************************************************************************/


 /*---------------------------------------------------------------------------
  *  NonRenewalConstructor
  *    Runs on constructor of Nonrenewal Job. Relates the registrant and the
  * products that are not renewing.
  *-------------------------------------------------------------------------*/
  procedure NonRenewalConstructor(
  a_ObjectId                            udt_Id,
  a_AsOfDate                            date
  );

 /*---------------------------------------------------------------------------
  *  ExpireProducts
  *    Expires Products that have not been renewed per the Product Registration
  * Non Renewal Job
  *-------------------------------------------------------------------------*/
  procedure ExpireProducts(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  *  SendEmail
  *    Sends emails to NJ ABC about non-renewed Products and cancelled Product
  * Renewal Jobs that were in New status.
  *-------------------------------------------------------------------------*/
  procedure SendEmail;

end PKG_ABC_NonRenewal;
/

create or replace package body PKG_ABC_NonRenewal is

 /*********************************************************************************
  *  PostVerify()
  *    Wrapper for the Post Verify event on the Product Registration Non Renewal Job
  *********************************************************************************/

  procedure PostVerify(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
    ) is
    t_JobId               udt_Id;
    t_Clerk               varchar2(4000);
    t_ClerkId             number(9);
    t_EndPoint            udt_Id;
    t_RelId               udt_Id;

  begin
    t_JobId               := a_ObjectId;
    t_Clerk               := api.pkg_columnquery.Value(t_JobId,'DefaultClerk');

    /* Set the Default Clerk */
    if t_Clerk is null then
      -- Get the end point
      t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PRNonRenewal','DefaultClerk');
      -- Get the clerk user id
      select r.UserId
        into t_ClerkId
        from query.o_SystemSettings o
        join query.r_DefaultPRClerkSysSetting r
          on o.ObjectId = r.SystemSettingsObjectId
       where o.ObjectDefTypeId = 1
         and rownum = 1;
      -- Create relationship
      t_RelId := api.pkg_relationshipupdate.New(t_EndPoint,t_JobId,t_ClerkId);

 end if;

end PostVerify;

 /*---------------------------------------------------------------------------
  *  NonRenewalConstructor -- PUBLIC()
  *    Runs on constructor of Nonrenewal Job. Relates the registrant and the
  * products that are not renewing.
  *-------------------------------------------------------------------------*/
  procedure NonRenewalConstructor(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
    ) is
    t_RegistrantObjId                   udt_id;
    t_NonRenewalJobId                   udt_id;
    t_RegistrantEndpointId              udt_id
        := api.pkg_configquery.EndPointIdForName('j_ABC_PRNonRenewal','Registrant');
    t_RegistrantRelId                   udt_id;
    t_ProdcutEndPointId                 udt_id
        := api.pkg_configquery.EndPointIdForName('j_ABC_PRNonRenewal','Product');
    t_ProductRelId                      udt_id;
    t_ProcessId                         udt_id;
    t_ProcessDefId                      number :=
        api.pkg_configquery.ObjectDefIdForName('p_ABC_ReviewStatusOfNonRenewal');
    t_Outcome                           varchar2(100):= 'Retire Products';
  begin

    -- Process checks to see if there is a relationship from the Nonrenewal job to
    -- the Registrant (Legal Entnty).
    t_NonRenewalJobId := a_ObjectId;
    if api.pkg_columnquery.Value(t_NonRenewalJobId, 'DoesRegistrantExist') = 'N' then
      select pr.RegistrantObjectId
      into t_RegistrantObjId
      from query.j_abc_prnonrenewal nr
      join query.r_abc_ProductToNonRenewalJob pnr
          on pnr.NonRenewalJobID = nr.JobId
      join query.r_ABC_ProductRegistrant pr
          on pr.ProductObjectId = pnr.ProductObjectID
      where nr.JobId = t_NonRenewalJobId;

      -- If there is no relationship between the Regiatrant and the Nonrenewal job,
      -- then one is created.
      t_RegistrantRelId := api.pkg_relationshipupdate.New(t_RegistrantEndpointId,
          t_NonRenewalJobId, t_RegistrantObjId);
      api.pkg_columnupdate.SetValue(t_NonRenewalJobId, 'DoesRegistrantExist', 'Y');
      -- The process then loops through the products associated with the Registrant
      -- that are expired, current, then creates relationships between the products
      -- and the NonRenewal job.
      for c in (select p.ObjectId
                from query.r_ABC_ProductRegistrant pr2
                join query.o_abc_product p
                    on p.ObjectId = pr2.ProductObjectId
                where pr2.RegistrantObjectId = t_RegistrantObjId
                  and p.CreateNonRenewalJob = 'Y'
                  and p.NonRenewalNoticeDate < sysdate
               ) loop
        t_ProductRelId := api.pkg_relationshipupdate.New(t_ProdcutEndPointId,
            t_NonRenewalJobId, c.objectid );
      end loop;

    -- Creating process to retire products
      t_ProcessId := api.pkg_processupdate.New(t_NonRenewalJobId, t_ProcessDefId,
          'Retire Products', sysdate, sysdate, sysdate);
      api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);
    end if;

  end NonRenewalConstructor;

 /*----------------------------------------------------------------------------
  *  ExpireProducts -- PUBLIC()
  *    Expires Products that have not been renewed per the Product Registration
  * Non Renewal Job
  *--------------------------------------------------------------------------*/
  procedure ExpireProducts (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_RegCount                          number := 0;
    t_RegistrantId                      udt_id;
    t_ScheduleId                        udt_id;
    t_Frequency                         api.pkg_Definition.udt_Frequency;
  begin

    t_JobId := api.pkg_columnquery.Value(a_ObjectId, 'JobId');
    select distinct (l.ObjectId)
    into t_RegistrantId
    from
      query.r_abc_ProductToNonRenewalJob r1
      join query.r_ABC_ProductRegistrant r2
          on r2.ProductObjectId = r1.ProductObjectID
      join query.o_abc_legalentity l
          on l.ObjectId = r2.RegistrantObjectId
    where r1.NonRenewalJobID = t_JobId;

    -- Loop through Products and change state to 'Historical' if the Expiration Date is passed
    for i in (
        select r.ProductObjectID
        from query.r_abc_ProductToNonRenewalJob r
        where r.NonRenewalJobID = t_JobId
        ) loop
      if api.pkg_columnquery.Value(i.ProductObjectId, 'ExpirationDateInThePast') = 'Y' then
        api.pkg_columnupdate.SetValue(i.ProductObjectId,'State','Historical');
        t_RegCount := t_RegCount + 1;
      end if;
    -- End loop through Products
    end loop;

    -- Set Registrant information into table possedba.Registrants_Nonrenewed if
    -- Registrant has non-renewed products.
    -- Each non-renewal job has only 1 Registrant, so this will work.
    if t_RegCount > 0 then
      insert into possedba.Registrants_NonRenewed
        (RegistrantName,
         RegistrantEmail,
         NumberOfProducts
        )
      values
        (api.pkg_columnquery.Value(t_RegistrantId, 'FormattedName'),
         api.pkg_columnquery.Value(t_RegistrantId, 'ContactEmailAddress'),
         t_RegCount
        );
      abc.pkg_ABC_Email.SendPRNonRenewalEmail(t_RegistrantId, t_RegCount, sysdate);
    end if;

    -- Create a Process Server entry if one doesn't exist. This will run at
    -- 4:00am and will send an email to NJ ABC.
    begin
      select s.ScheduleKey
      into t_ScheduleId
      from procsrvr.sysschedule s
      where s.Description = 'Send Product Non-Renewal emails'
        and s.Status = 1;
    exception
      when no_data_found then
        -- Create new Process Server entry
        t_Frequency := api.pkg_Definition.gc_Once;
        t_ScheduleId := api.pkg_ProcessServer.ScheduleTrustedProcedure(
            'abc.pkg_ABC_NonRenewal.SendEmail', -- Name
            'Send Product Non-Renewal emails', -- Description
            t_Frequency, -- Frequency
            null, -- IncrementBy
            null, -- Unit
            null, -- Arguments
            'N', -- SkipBacklog
            'Y', -- DisableOnError
            'N', -- Sunday
            'N', -- Monday
            'N', -- Tuesday
            'N', -- Wednesday
            'N', -- Thursday
            'N', -- Friday
            'N', -- Saturday
            'N', -- MonthEnd
            sysdate + 4/24, --Start 4 hours from now
            null, --EndDate
            null, --Email
            null, --ServerId
            api.pkg_Definition.gc_normal --Priority
            );
    end;

  end ExpireProducts;

 /*---------------------------------------------------------------------------
  *  SendEmail
  *    Sends emails to NJ ABC about non-renewed Products and cancelled Product
  * Renewal Jobs that were in New status.
  *-------------------------------------------------------------------------*/
  procedure SendEmail
  is
    t_Body                              varchar2(4000);
    t_Subject                           varchar2(4000)
        := 'Non-Renewed Products by Registrant.';
    t_Subject1                          varchar2(4000)
        := 'Cancelled product Renewal jobs.';
    t_SystemSetting                     udt_id;
    t_FromEmailAddress                  varchar2(1000);
    t_ToEmailAddress                    varchar2(1000);
    t_StatusId                          udt_id;
    t_ProcessId                         number;
    t_ProcessDefId                      number
        := api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus');
    t_Outcome                           varchar2(100):= 'Auto Cancelled';
    t_PreviousJob                       number := 0;
    t_Amount                            number;
  begin

    -- Get System Settings data
    select
      s.ObjectId,
      nvl(s.EmailNotificationAddress, s.ToEmailAddressNonProd),
      s.FromEmailAddress
    into
      t_SystemSetting,
      t_ToEmailAddress,
      t_FromEmailAddress
    from query.o_systemsettings s;
    select s.StatusId
    into t_StatusId
    from api.statuses s
    where s.Description = 'New';

    -- Send email for Registrants
    t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
        'font-weight: bold; color: red;">This is an automated notification.' ||
        '</span><br><br>' || 'The following listing reflects all Registrants ' ||
        'and the amount of products that has been retired automatically as a ' ||
        'result of not filing a Product Registration Renewal.<br><br>';
    for r in (select p.registrantname, p.registrantemail, numberofproducts
              from possedba.registrants_nonrenewed p
              where p.emailsent is null
              order by p.registrantname, p.registrantemail
             ) loop
      -- Create email body.
      t_Body := t_Body || 'Registrant ' || r.registrantname || ' (' ||
          r.registrantemail || ') ' || r.numberofproducts || ' Products.<br>';
      if length(t_Body) > 3800 then
        t_Body := abc.pkg_ABC_Email.EmailFormat(t_Body);
        extension.pkg_sendmail.SendEmail(t_SystemSetting, t_FromEmailAddress,
            t_ToEmailAddress, null, null, t_Subject, t_Body, null, null, 'Y',
            null);
        t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
            'font-weight: bold; color: red;">This is an automated notification.' ||
            '</span><br><br>' || 'The following listing reflects all ' ||
            'Registrants and the amount of products that has been retired ' ||
            ' automatically as a result of not filing a Product Registration ' ||
            'Renewal.<br><br>';
      end if;
      update possedba.registrants_nonrenewed p
      set p.emailsent = trunc(sysdate)
      where p.registrantname = r.registrantname
        and p.registrantemail = r.registrantemail;
    end loop;
    t_Body := t_Body || '<br><br>';
    t_Body := abc.pkg_ABC_Email.EmailFormat(t_Body);
    extension.pkg_sendmail.SendEmail(t_SystemSetting, t_FromEmailAddress,
        t_ToEmailAddress, null, null, t_Subject, t_Body, null, null, 'Y', null);

    -- Send email for cancelled Renewal jobs
    t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
        'font-weight: bold; color: red;">This is an automated notification.' ||
        '</span><br><br>' || 'The following listing reflects all Product ' ||
        'Renewal jobs in a status of NEW that were cancelled automatically ' ||
        'for not filing their Brand/Product Registration Renewal Job.<br><br>';
    for r in (select
                p.ObjectId,
                p.ExternalFileNum,
                p.CreatedDate,
                p.OnlineUserNameAndEmail,
                f.Amount,
                f.PaidAmount,
                pr.ProcessId,
                pr.Outcome
              from query.j_abc_prrenewal p
              left join api.fees f on f.JobId = p.ObjectId
              left join api.processes pr on pr.JobId = p.ObjectId
              where p.StatusId = t_StatusId
              order by p.CreatedDate
             ) loop
      if r.objectid = t_PreviousJob then
        continue;
      end if;
      t_PreviousJob := r.Objectid;
      -- Create email body.
      t_Body := t_Body || 'Job ' || r.externalfilenum || ' - Created by ' ||
          r.onlineusernameandemail || ' on ' || r.createddate || '.';
      if r.paidamount is not null and
        r.paidamount != 0 then
        t_Body := t_Body || ' Fee Paid ' || ltrim(to_char(r.paidamount * -1,
            '$99,999,999.99')) || '.';
      end if;
      t_Body := t_Body  || '<br>';
      if length(t_Body) > 3800 then
        t_Body := t_Body || '<br><br>';
        t_Body := abc.pkg_ABC_Email.EmailFormat(t_Body);
        extension.pkg_sendmail.SendEmail(t_SystemSetting, t_FromEmailAddress,
            t_ToEmailAddress, null, null, t_Subject1, t_Body, null, null, 'Y',
            null);
        t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
            'font-weight: bold; color: red;">This is an automated notification.' ||
            '</span><br><br>' || 'The following listing reflects all Product ' ||
            'Renewal jobs in a status of NEW that were cancelled automatically ' ||
            'for not filing their Brand/Product Registration Renewal Job.<br><br>';
      end if;

      -- Unassign processes and cancel job.
      if r.processid is not null
        and r.outcome is null then
        for pa in (select p.UserId
                   from api.processassignments p
                   where p.ProcessId = r.processid
                  ) loop
          api.pkg_processupdate.Unassign(r.processid, pa.userid);
        end loop;
        api.pkg_processupdate.Remove(r.ProcessId);
      end if;
      t_ProcessId := api.pkg_processupdate.New (r.objectid, t_ProcessDefId,
          'Change Status',sysdate,sysdate,sysdate);
      api.pkg_columnupdate.SetValue(t_ProcessId,'ReasonForChange',
          'Non-renewal Cancelled.');
      api.pkg_processupdate.Complete(t_ProcessId, t_Outcome);
    end loop;

    t_Body := t_Body || '<br><br>';
    t_Body := abc.pkg_ABC_Email.EmailFormat(t_Body);
    extension.pkg_sendmail.SendEmail(t_SystemSetting, t_FromEmailAddress,
        t_ToEmailAddress, null, null, t_Subject1, t_Body, null, null, 'Y', null);

  end SendEmail;

end PKG_ABC_NonRenewal;
/

