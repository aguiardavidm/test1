begin
  DBMS_DDL.CREATE_WRAPPED('create or replace package pkg_ABC_Encryption is

  -- Author  : BRAD.VEENSTRA
  -- Created : 03/25/2015 4:40:10 PM
  -- Purpose : Encrypt and decrypt data as per ABC requirements
  
  function Encrypt(a_StringToEncrypt in varchar2) return raw;
  
  function Decrypt(a_StringToDecrypt in raw) return varchar2;

end pkg_ABC_Encryption;');
end;
/
begin
  DBMS_DDL.CREATE_WRAPPED('create or replace package body pkg_ABC_Encryption is

  g_key raw(32) := ''008F19E5182681F0965134D5135F943BB3D9BD9810A898CDEC8BF18D0B8F5EA5'';  
  --use this to generate a new key:  SELECT DBMS_CRYPTO.RANDOMBYTES(32) FROM DUAL
  
  function Encrypt(a_StringToEncrypt in varchar2)
     return raw
  is
     t_Encrypted     raw(500);
     t_PreSalt       varchar2(4) := 5514;
     t_PostSalt      varchar2(4) := 4357;
     t_Mod           number := dbms_crypto.ENCRYPT_AES256 + dbms_crypto.CHAIN_CBC + dbms_crypto.PAD_PKCS5;
  begin
     t_Encrypted := dbms_crypto.Encrypt(utl_raw.cast_to_raw(t_PreSalt || a_StringToEncrypt || t_PostSalt), t_Mod, g_key);
     return t_Encrypted;
  end;


  function Decrypt(a_StringToDecrypt in raw)
     return varchar2
  is
     t_Decrypted    varchar2(500);
     t_Decrypt_Raw  raw(500);
     t_Mod          number := dbms_crypto.ENCRYPT_AES256 + dbms_crypto.CHAIN_CBC + dbms_crypto.PAD_PKCS5;
     t_LoggedOnUser varchar2(100);
  begin
     
     --check for an authorized user
     select SYS_CONTEXT (''USERENV'', ''SESSION_USER'') 
     into t_LoggedOnUser
     from dual;
     
     if t_LoggedOnUser in (''POSSEAPPSERVERSYS'', ''OUTRIDERSYS'', ''ABC'', ''INVESTIGATIONS'', ''POSSEDBA'') then
       t_Decrypt_Raw := dbms_crypto.decrypt (a_StringToDecrypt, t_Mod, g_key);
       t_Decrypted := utl_raw.cast_to_varchar2(t_Decrypt_Raw);
       --remove the salt
       t_Decrypted := substr(t_Decrypted, 5);
       t_Decrypted := substr(t_Decrypted, 0, length(t_Decrypted) - 4);
       return t_Decrypted;
     else
       raise_application_error(-20000, ''Unauthorized access to this decrypt function.'');
     end if;
  end;
  
end pkg_ABC_Encryption;');
end;
/

grant execute on pkg_abc_encryption to posseextensions;
