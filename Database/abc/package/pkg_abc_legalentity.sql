create or replace package pkg_ABC_LegalEntity is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  -----------------------------------------------------------------------------
  -- StartNewEditMode
  --   Sets the edit-related details based on current edit mode and creates a snapshot with
  --   Legal Entity History objects.
  -----------------------------------------------------------------------------
  procedure StartNewEditMode (
    a_LegalEntityId                         udt_Id,
    a_AsOfDate                              date default sysdate
  );

  -----------------------------------------------------------------------------
  -- CopyObjectIdToJob
  --   Copies the current objectId onto the New Application Job.
  -----------------------------------------------------------------------------
  procedure CopyObjectIdToJob (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- CopyObjectIdToPermitJob
  --   Copies the current objectId onto the New Application Job.
  -----------------------------------------------------------------------------
  procedure CopyObjectIdToPermitJob (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- MaintainLEDetails
  --   Ensures hidden data is set to Null when the Legal Entity Type changes
  --   from Individual to non Individual
  -----------------------------------------------------------------------------
  procedure MaintainLEDetails (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- GenerateOnlineAccessCode
  -----------------------------------------------------------------------------
  procedure GenerateOnlineAccessCode (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- FinishEditing
  -----------------------------------------------------------------------------
  procedure FinishEditing (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  /*---------------------------------------------------------------------------
   * MaintainDuplicatesOnRelatedObjects()
   *   Trigger toolbox procedure to maintain dups on related objects. To be run
   * on constructor of relationships.
   *-------------------------------------------------------------------------*/
  procedure MaintainDuplicatesOnRelatedObjects (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_LegalEntity;
/
grant execute
on pkg_abc_legalentity
to posseextensions;

create or replace package body pkg_ABC_LegalEntity is

  /*---------------------------------------------------------------------------
   * CreateSnapshot() -- PRIVATE
   * Creates a Legal Entity History object that duplicates the details on a_LegalEntityObjectId,
   *   relates the two, and creates a tree of related LEH objects below the new one that
   *   duplicates the tree of Legal Entities below a_LegalEntityObjectId.
   *-------------------------------------------------------------------------*/
  procedure CreateSnapshot (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date default sysdate
    ) is

    -- globally accessed sparse array of Legal Entities already part of snapshot
    -- with mappings to corresponding Legal Entity History
    t_CopiedAlreadyIdArray                  udt_IdList;
    -- globally accessed sparse array of a_LegalEntityId's ancestors in corporate tree
    -- with mappings to the ObjectId of the LE that participates in them
    t_AncestorIdArray                       udt_IdList;
    t_NextLegalEntityHistory                udt_Id;
    t_AncestorIndex                         udt_Id;
    t_EndpointId                            udt_Id;
    t_RelId                                 udt_Id;

    procedure AddAncestors(
      a_ObjectId                               udt_Id
      ) is
    begin -- AddAncestors
      for parentle in (select ParticipatesInLEObjectId ObjectId
                       from query.r_ABC_ParticipatesInIncludesLE
                      where IncludesLEObjectId = a_ObjectId
                     ) loop
        -- prevent infinite loops
        if not t_AncestorIdArray.exists(parentle.ObjectId) then
          -- flag parent as an ancestor, record that its child is a_ObjectId
          t_AncestorIdArray(parentle.ObjectId) := a_ObjectId;
          AddAncestors(parentle.ObjectId); -- recursive call
        end if;
      end loop;
    end AddAncestors;

    procedure CreateCorporateStructure(
      a_CurrentLegalEntityId           udt_Id,
      a_NewLegalEntityHistoryId    out udt_Id
      ) is

      type NameArrayType is varray(30) of varchar2(40);
      ColumnsToCopy NameArrayType
        := NameArrayType('LegalName','FirstName','LastName','MiddleName','CorporationNumber',
            'IncorporationDate','BirthDate','ContactName','ContactPhoneNumber',
            'ContactAltPhoneNumber','ContactFaxNumber','ContactEmailAddress','PreferredContactMethod',
            'Active', 'SSNTIN', 'NJTaxAuthNumber','CriminalRecordCheckResults','CriminalRecordCheckDate',
            'CriminalRecordCheckComments','BirthDateEncrypted','SSNTINEncrypted','DriversLicense',
            'DriversLicenseEncrypted');

      RelColumnsToCopy NameArrayType
        := NameArrayType('RelationshipType','PercentageInterest','CommonVotingShares',
                         'CommonNonVotingShares','PreferredVotingShares','PreferredNonVotingShares');
      t_RelatedLegalEntityHistoryId                udt_Id := null;
      t_LatestHistoryId                            udt_Id := null;
      t_NewIncludesRelId                           udt_Id := null;

    begin -- CreateCorporateStructure
      a_NewLegalEntityHistoryId
        := api.pkg_objectupdate.new( api.pkg_configquery.ObjectDefIdForName('o_ABC_LegalEntityHistory') );

      -- FromDate comes from the last LEH related to this Legal Entity
        -- (not necessarily the last LEH representing it, though)
      select max(LegalEntityHistoryObjectId)
        into t_LatestHistoryId
        from query.r_abc_LegalEntityLEHistory
       where LegalEntityObjectId = a_CurrentLegalEntityId;
      api.pkg_columnupdate.SetValue (a_NewLegalEntityHistoryId, 'FromDate',
                                    nvl(api.pkg_columnquery.value(t_LatestHistoryId, 'ToDate'),
                                    api.pkg_columnquery.value(a_CurrentLegalEntityId, 'CreatedDate')));
      api.pkg_columnupdate.SetValue (a_NewLegalEntityHistoryId, 'ToDate', sysdate);

      -- keep a record of this new LEH's ID to use if the same LE comes up again in the snapshot
      t_CopiedAlreadyIdArray(a_CurrentLegalEntityId) := a_NewLegalEntityHistoryId;

      -- copy details to new LEH
      for i in 1..ColumnsToCopy.count loop
        api.pkg_ColumnUpdate.SetValue(a_NewLegalEntityHistoryId, ColumnsToCopy(i),
                                      api.pkg_columnquery.value(a_CurrentLegalEntityId,ColumnsToCopy(i)));
      end loop;

      -- copy non-LE rels to new LEH
      for reltocopy in (select r.ToObjectId, rd.ToEndPointName
                         from api.relationships r
                         join api.relationshipdefs rd
                           on r.EndPointId = rd.ToEndPointId
                        where r.FromObjectId = a_CurrentLegalEntityId
                          and rd.ToEndpointName in ('MailingAddress','PhysicalAddress','LegalEntityType')
                        ) loop
        extension.pkg_relationshipupdate.new(a_NewLegalEntityHistoryId, reltocopy.toobjectid, reltocopy.ToEndpointName);
      end loop; -- related non-LEs

      -- copy LE relationships to new LEH
      for reltocopy in (select r.ToObjectId, rd.ToEndPointName, r.relationshipid
                         from api.relationships r
                         join api.relationshipdefs rd
                           on r.EndPointId = rd.ToEndPointId
                        where r.FromObjectId = a_CurrentLegalEntityId
                          and rd.ToEndpointName in ('Includes','SpouseTo','SpouseFrom')
                        ) loop
        if t_CopiedAlreadyIdArray.exists(reltocopy.ToObjectId) then
          t_RelatedLegalEntityHistoryId := t_CopiedAlreadyIdArray(reltocopy.ToObjectId);
        else
          CreateCorporateStructure(reltocopy.ToObjectId, t_RelatedLegalEntityHistoryId);
        end if;
        if reltocopy.toendpointname not in ('SpouseTo','SpouseFrom')
            or (extension.pkg_relutils.relexists(a_NewLegalEntityHistoryId, 'SpouseFrom') = 'N'
            and extension.pkg_relutils.relexists(a_NewLegalEntityHistoryId, 'SpouseTo') = 'N') then
          t_NewIncludesRelId := extension.pkg_relationshipupdate.new(a_NewLegalEntityHistoryId,
              t_RelatedLegalEntityHistoryId, reltocopy.toendpointname);
          if reltocopy.toendpointname = 'Includes' then -- copy Includes relationship details
            for i in 1..RelColumnsToCopy.count loop
              api.pkg_ColumnUpdate.SetValue(t_NewIncludesRelId, RelColumnsToCopy(i),
                                            api.pkg_columnquery.value(reltocopy.relationshipid,RelColumnsToCopy(i)));
            end loop;
          end if;
        end if; -- OK to create rel (i.e., this is not a duplicate spouse situation)
      end loop; -- related LEs
    end CreateCorporateStructure;

  begin  -- CreateSnapshot
    AddAncestors(a_ObjectId);
    CreateCorporateStructure(a_ObjectId, t_NextLegalEntityHistory);

    if api.Pkg_Columnquery.Value(a_ObjectId, 'CountryObjectId') is not null then
      t_EndpointId := api.pkg_ConfigQuery.EndPointIdForName(
          api.Pkg_Columnquery.value(t_NextLegalEntityHistory,'ObjectDefName'), 'Country');
      t_RelId := api.Pkg_Relationshipupdate.New(t_EndpointId, t_NextLegalEntityHistory,
          api.Pkg_Columnquery.Value(a_ObjectId, 'CountryObjectId'));
    end if;

    if api.Pkg_Columnquery.Value(a_ObjectId, 'StateObjectId') is not null then
      t_EndpointId := api.pkg_ConfigQuery.EndPointIdForName(
          api.Pkg_Columnquery.value(t_NextLegalEntityHistory,'ObjectDefName'), 'State');
      t_RelId := api.Pkg_Relationshipupdate.New(t_EndpointId, t_NextLegalEntityHistory,
          api.Pkg_Columnquery.Value(a_ObjectId, 'StateObjectId'));
    end if;

    -- relate LEH (with all its rels and corporate structure) to passed-in LE
    extension.pkg_relationshipupdate.new(a_ObjectId, t_NextLegalEntityHistory, 'LegalEntityHistory');

    -- create a full corporate structure under each ancestor
    t_AncestorIndex := t_AncestorIdArray.first;
    while t_AncestorIndex is not null loop
        if t_CopiedAlreadyIdArray.exists(t_AncestorIndex) then
          t_NextLegalEntityHistory := t_CopiedAlreadyIdArray(t_AncestorIndex);
        else
          CreateCorporateStructure(t_AncestorIndex, t_NextLegalEntityHistory);
        end if;
        -- the comprising LE's snapped corporate structure needs to be connected to the LE so
          -- changes to its included LEs are not hidden
        -- if current ancestor is a direct parent of a_ObjectId...
        if t_AncestorIdArray(t_AncestorIndex) = a_ObjectId
            -- ...and not yet related to its LEH
            and extension.pkg_relutils.RelExists(t_NextLegalEntityHistory,'LegalEntity')='N' then
          extension.pkg_relationshipupdate.new(t_AncestorIndex, t_NextLegalEntityHistory, 'LegalEntityHistory');
        end if;

        -- Note: no need to explicitly connect to ancestor, since it already found its child's
          -- existing LEH and created an Includes rel to it
        t_AncestorIndex := t_AncestorIdArray.next(t_AncestorIndex);

      -- copy LE relationships to new LEH
    end loop; -- ancestors

  end CreateSnapshot;

  /*---------------------------------------------------------------------------
   * StartNewEditMode() -- PUBLIC
   * Sets the edit-related details based on current edit mode and creates a snapshot with
   *   Legal Entity History objects.
   *-------------------------------------------------------------------------*/
  procedure StartNewEditMode (
    a_LegalEntityId                              udt_Id,
    a_AsOfDate                              date default sysdate
    ) is

  begin -- StartNewEditMode
    if api.pkg_columnquery.value(a_LegalEntityId, 'Edit') = 'Y' then
      CreateSnapshot(a_LegalEntityId);
    end if;
  end;  -- StartNewEditMode


  -----------------------------------------------------------------------------
  -- CopyObjectIdToJob
  --   Copies the current objectId onto the New Application Job.
  -----------------------------------------------------------------------------
  procedure CopyObjectIdToJob (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
    t_TargetObjectId                        udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'TemporaryNewApplicationJobId');
  begin
    if t_TargetObjectId is not null then
      api.pkg_ColumnUpdate.SetValue(t_TargetObjectId, 'OnlineUseLegalEntityObjectId', a_ObjectId);
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'TemporaryNewApplicationJobId');
    end if;
  end CopyObjectIdToJob;

  -----------------------------------------------------------------------------
  -- CopyObjectIdToPermitJob
  --   Copies the current objectId onto the New Application Job.
  -----------------------------------------------------------------------------
  procedure CopyObjectIdToPermitJob (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
    t_TargetObjectId                        udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'TemporaryNewPermitAppJobId');
  begin
    if t_TargetObjectId is not null then
      api.pkg_ColumnUpdate.SetValue(t_TargetObjectId, 'OnlineUseLegalEntityObjectId', a_ObjectId);
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'TemporaryNewPermitAppJobId');
    end if;
  end CopyObjectIdToPermitJob;

  -----------------------------------------------------------------------------
  -- MaintainLEDetails
  --   Ensures hidden data is set to Null when the Legal Entity Type changes
  --   from Individual to non Individual
  -----------------------------------------------------------------------------
  procedure MaintainLEDetails(
    a_ObjectId           udt_Id,
    a_AsOfDate           date
    ) is

  begin
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'IsIndividualType') = 'Y' then
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId,'LegalName');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId,'CorporationNumber');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId,'IncorporationDate');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId,'COntactName');
    elsif api.pkg_ColumnQuery.Value(a_ObjectId, 'IsIndividualType') = 'N' then
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId,'FirstName');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId,'LastName');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId,'MiddleName');
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId,'BirthDate');
    end if;
  end MaintainLEDetails;

  -----------------------------------------------------------------------------
  -- GenerateOnlineAccessCode
  --   runs on constructor of the Legal Entity
  -----------------------------------------------------------------------------
  procedure GenerateOnlineAccessCode(
    a_ObjectId           udt_Id,
    a_AsOfDate           date
    ) is
    t_OnlineAccessCode   number(9);
    t_startingpoint      number(9);

  begin
    if api.pkg_columnquery.Value(a_ObjectId, 'OnlineAccessCode') is not null then
      return;
    end if;

    --generate an 8 digit random code
    loop
      t_OnlineAccessCode := abs(MOD(DBMS_CRYPTO.RANDOMINTEGER, 99999999));

      if length(t_OnlineAccessCode) = 8 then
        exit;
      end if;
    end loop;

    t_startingpoint := t_OnlineAccessCode;

    loop
      begin
        --make sure it hasn't already been used
        insert into OnlineAccessCode (
          OnlineAccessCode
        ) values (
          t_OnlineAccessCode
        );
        api.pkg_columnupdate.SetValue(a_ObjectId, 'OnlineAccessCode', t_OnlineAccessCode);
        exit;
      exception
      when dup_val_on_index then
        null;
      end;
      t_OnlineAccessCode := t_OnlineAccessCode + 4764453;  --add a random number

      --we have come full circle there are no Online Access Codes left (can't imagine this would ever happen)
      if t_OnlineAccessCode = t_startingpoint
      then
        api.pkg_errors.RaiseError(-20000, 'Unable to generate an' ||
            ' Online Access Code.  Please contact the help desk.');
      end if;

      if t_OnlineAccessCode >= 99999999
      then
        t_OnlineAccessCode := 11415432;  -- reset to a random number
      end if;

    end loop;

  end GenerateOnlineAccessCode;

  -----------------------------------------------------------------------------
  -- FinishEditing
  --   runs on change of value of Edit
  -----------------------------------------------------------------------------
  procedure FinishEditing(
    a_ObjectId           udt_Id,
    a_AsOfDate           date
    ) is
    t_OnlineAccessCode   number(9);
    t_startingpoint      number(9);

  begin
    if api.pkg_ColumnQuery.VALUE (a_ObjectId,'Edit') = 'Y' then
       return;
    end if;

    if api.pkg_ColumnQuery.VALUE (a_ObjectId,'Edit') = 'N' then
       if (api.pkg_ColumnQuery.VALUE (a_ObjectId,'CorporationCharterBeenRevoked') = 'Yes'
           and api.pkg_ColumnQuery.VALUE (a_ObjectId,'RevocationDate') is null
           and (api.pkg_ColumnQuery.VALUE (a_ObjectId,'SuspensionBeginningDate') is null
                or api.pkg_ColumnQuery.VALUE (a_ObjectId,'SuspensionEndingDate') is null)) then
          api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for either Date of Revocation or both Beginning and Ending Date.');
       end if;
       if api.pkg_columnquery.Value(a_ObjectId, 'ContactPhoneNumber') is null then
          api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Business Phone Number.');
       end if;
       if api.pkg_columnquery.Value(a_ObjectId, 'PreferredContactMethod') = 'Email' and
          api.pkg_columnquery.Value(a_ObjectId, 'ContactEmailAddress') is null then
          api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Email Address.');
       end if;
       if api.pkg_columnquery.Value(a_ObjectId, 'PreferredContactMethod') = 'Mail' and
          api.pkg_columnquery.Value(a_ObjectId, 'MailingAddress') is null then
          api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Mailing Address.');
       end if;
       if api.pkg_columnquery.Value(a_ObjectId, 'PreferredContactMethod') = 'Home Phone' and
          api.pkg_columnquery.Value(a_ObjectId, 'ContactAltPhoneNumber') is null then
          api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Home Phone Number.');
       end if;
       if api.pkg_columnquery.Value(a_ObjectId, 'PreferredContactMethod') = 'Mobile Phone' and
          api.pkg_columnquery.Value(a_ObjectId, 'ContactFaxNumber') is null then
          api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Mobile Phone Number.');
       end if;
    end if;

  end FinishEditing;

  /*---------------------------------------------------------------------------
   * MaintainDuplicatesOnRelatedObjects() -- PUBLIC
   *   Trigger toolbox procedure to maintain dups on related objects. To be run
   * on constructor of relationships.
   *-------------------------------------------------------------------------*/
  procedure MaintainDuplicatesOnRelatedObjects (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_IsLicenseOrTAP                    varchar(1) := 'N';
    t_LicenseToPermitEndpointId         udt_Id;
    t_PermitToTypeEndpointId            udt_Id;
    t_RelatedObjectIds                  udt_IdList;
    t_TAPToPermitEndpointId             udt_Id;
    t_ToObjectId                        udt_Id;
    t_ToObjectDefName                   varchar2(30);
  begin

    t_LicenseToPermitEndpointId := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'Permit');
    t_PermitToTypeEndpointId := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_Permit', 'PermitType');
    t_TAPToPermitEndpointId := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_Permit', 'Permit');

    select
      r.ToObjectId,
      api.pkg_ColumnQuery.Value(r.ToObjectId, 'ObjectDefName')
    into
      t_ToObjectId,
      t_ToObjectDefName
    from api.Relationships r
    where r.ToEndPoint = 0
      and r.RelationshipId = a_ObjectId;

    if t_ToObjectDefName = 'o_ABC_License' then
      t_IsLicenseOrTAP := 'Y';
    elsif t_ToObjectDefName = 'o_ABC_Permit' then
      select
        case
           when api.pkg_ColumnQuery.Value(r.ToObjectId, 'Code') = 'TAP' then 'Y'
        end
      into t_IsLicenseOrTAP
      from api.relationships r
      where r.EndpointId = t_PermitToTypeEndpointId
        and r.FromObjectId = t_ToObjectId;
    end if;

    if t_IsLicenseOrTAP = 'Y' then
      select r.ToObjectId
      bulk collect into t_RelatedObjectIds
      from api.relationships r
      where r.EndpointId in (t_TAPToPermitEndpointId, t_LicenseToPermitEndpointId)
        and r.FromObjectId = t_ToObjectId;

      for i in 1..t_RelatedObjectIds.count() loop
        toolbox.pkg_Copy.MaintainDuplicateDetails(t_RelatedObjectIds(i), sysdate);
      end loop;
    end if;

  end MaintainDuplicatesOnRelatedObjects;

end pkg_ABC_LegalEntity;
/
