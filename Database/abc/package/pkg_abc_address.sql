create or replace package abc.pkg_ABC_Address is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  -----------------------------------------------------------------------------
  -- PostVerifyWrapper
  -- Runs validation on the Address object from the internal site
  -----------------------------------------------------------------------------
  procedure PostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );
  
  -----------------------------------------------------------------------------
  -- PostVerifyWrapperOnline
  -- Runs validation on the Address Online Entry object from the public site
  -----------------------------------------------------------------------------
  procedure PostVerifyWrapperOnline (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );
  
  -----------------------------------------------------------------------------
  -- CopyInternalAddresstoOnline
  -- Copies an Internal Address (o_ABC_Address) to an Online Address (o_ABC_AddressOnlineEntry)
  -- Created: Nov 2015
  -- Updated: 10 Nov, 2015 by Elijah Rust
  -- Notes: Removed most of the copy logic, instead copies Internal Display Format into a
  --        an address of type "Other / International" this prevents errors of bad address data
  -----------------------------------------------------------------------------
  function CopyInternalAddresstoOnline (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) return udt_Id;
  
end pkg_ABC_Address;

 
/
create or replace package body abc.pkg_ABC_Address is
  -----------------------------------------------------------------------------
  -- PostVerifyWrapper
  -- Runs validation on the Address object from the internal site
  -----------------------------------------------------------------------------
  procedure PostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_AddressType         varchar2(100);
    t_ErrorMsg            varchar2(4000);
    t_RaiseError          varchar2(1) := 'N';
  begin
    t_AddressType := api.pkg_columnquery.Value(a_ObjectId, 'AddressType');
    if t_AddressType is null then
      api.pkg_errors.RaiseError(-20100, 'Please select an Address Type.');
    end if;
    select
       case when CivicAddressType = 'N'
        and OtherAddressType = 'N'
        and POBoxAddressType = 'N'
        and RuralRouteAddressType = 'N'
        and IsConverted = 'N'
        and GeneralDeliveryAddressType = 'N' then 'Y'
       else 'N'
       end
      into t_RaiseError
      from query.o_abc_address a
     where a.objectid = a_ObjectId;
    if t_RaiseError = 'Y' then
      api.pkg_errors.RaiseError(-20100, 'Please enter address information.');
    end if;

    t_RaiseError := 'N';
    case t_AddressType
      when 'Civic' then
        select case when Street is null
                      or Streetnumber is null
                      or CityTown is null
                      or ProvinceState is null
                      or Country is null
                      or PostalCode is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_Address a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for Civic Address -> Street Number, Street, Street Type, City/Town, State, Country and Postal/Zip Code.';
      when 'PO Box' then
        select case when PO_PostOfficeBox is null
                      or CityTown is null
                      or ProvinceState is null
                      or Country is null
                      or PostalCode is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_Address a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for PO Box Address -> PO Box Number, City/Town, State, Country and Postal/Zip Code.';
      when 'Rural Route' then
        select case when RR_RuralRouteIdentifier is null
                      or CityTown is null
                      or ProvinceState is null
                      or Country is null
                      or PostalCode is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_Address a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for Rural Route Address -> Rural Route Identifier, City/Town, State, Country and Postal Code.';
      when 'General Delivery' then
        select case when GD_GeneralDeliveryIndicator is null
                      or CityTown is null
                      or ProvinceState is null
                      or Country is null
                      or PostalCode is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_Address a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for General Delivery Address -> GD Indicator, City/Town, State, Country and Postal/Zip Code.';
      when 'Other / International' then
        select case when OtherAddress is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_Address a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for Other Address.';
      else
        t_RaiseError := 'N';
    end case;
    if t_RaiseError = 'Y' then
      api.pkg_errors.RaiseError(-20100, t_ErrorMsg);
    end if;

  end PostVerifyWrapper;


  -----------------------------------------------------------------------------
  -- PostVerifyWrapperOnline
  -- Runs validation on the Address Online Entry object from the public site
  -----------------------------------------------------------------------------
  procedure PostVerifyWrapperOnline (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_AddressType         varchar2(100);
    t_ErrorMsg            varchar2(4000);
    t_RaiseError          varchar2(1) := 'N';
  begin
    t_AddressType := api.pkg_columnquery.Value(a_ObjectId, 'AddressType');
    if t_AddressType is null then
      api.pkg_errors.RaiseError(-20100, 'Please select an Address Type.');
    end if;
    select
       case when CivicAddressType = 'N'
        and OtherAddressType = 'N'
        and POBoxAddressType = 'N'
        and RuralRouteAddressType = 'N'
        and IsConverted = 'N'
        and GeneralDeliveryAddressType = 'N' then 'Y'
       else 'N'
       end
      into t_RaiseError
      from query.o_ABC_AddressOnlineEntry a
     where a.objectid = a_ObjectId;
    if t_RaiseError = 'Y' then
      api.pkg_errors.RaiseError(-20100, 'Please enter address information.');
    end if;

    t_RaiseError := 'N';

    case t_AddressType
      when 'Civic' then
        select case when Street is null
                      or CityTown is null
                      or ProvinceState is null
                      or Country is null
                      or PostalCode is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_AddressOnlineEntry a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for Civic Address -> Street, City/Town, State, Country and Postal/Zip Code.';
      when 'PO Box' then
        select case when PO_PostOfficeBox is null
                      or CityTown is null
                      or ProvinceState is null
                      or Country is null
                      or PostalCode is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_AddressOnlineEntry a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for PO Box Address -> PO Box Number, City/Town, State, Country and Postal/Zip Code.';
      when 'Rural Route' then
        select case when RR_RuralRouteIdentifier is null
                      or CityTown is null
                      or ProvinceState is null
                      or Country is null
                      or PostalCode is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_AddressOnlineEntry a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for Rural Route Address -> Rural Route Identifier, City/Town, State, Country and Postal Code.';
      when 'General Delivery' then
        select case when GD_GeneralDeliveryIndicator is null
                      or CityTown is null
                      or ProvinceState is null
                      or Country is null
                      or PostalCode is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_AddressOnlineEntry a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for General Delivery Address -> GD Indicator, City/Town, State, Country and Postal/Zip Code.';
      when 'Other / International' then
        select case when OtherAddress is null
                    then 'Y' else 'N' end
          into t_RaiseError
          from query.o_ABC_AddressOnlineEntry a
         where objectid = a_ObjectId;
        t_ErrorMsg := 'Please enter all information for Other Address.';
      else
        t_RaiseError := 'N';
    end case;
    if t_RaiseError = 'Y' then
      api.pkg_errors.RaiseError(-20100, t_ErrorMsg);
    end if;

  end PostVerifyWrapperOnline;


  -----------------------------------------------------------------------------
  -- CopyInternalAddresstoOnline
  -- Copies an Internal Address (o_ABC_Address) to an Online Address (o_ABC_AddressOnlineEntry)
  -- Created: Nov 2015
  -- Updated: 10 Nov, 2015 by Elijah Rust
  -- Notes: Removed most of the copy logic, instead copies Internal Display Format into a
  --        an address of type "Other / International" this prevents errors of bad address data
  -----------------------------------------------------------------------------
  function CopyInternalAddresstoOnline (
    a_ObjectId                 udt_Id,
    a_AsOfDate                 date
  ) return udt_Id is
  
  t_OnlineAddressObjectDefId   udt_Id;
  t_InternalAddressObjectDefId udt_Id;
  t_PassedInObjectDefId        udt_Id;
  
  t_NewObjectId                udt_id;
  
  t_Return                     udt_id;

  t_CharValue                  varchar2(4000);
  
  begin

  t_OnlineAddressObjectDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_AddressOnlineEntry');
  t_InternalAddressObjectDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_Address');
  
  --get the objectdefid of our source object
    select ObjectDefId
      into t_PassedInObjectDefId
      from api.Objects
     where Objectid = a_ObjectId;

    if t_PassedInObjectDefId = t_InternalAddressObjectDefId then

      --create a new online address object 
      t_NewObjectId := api.pkg_ObjectUpdate.New(t_OnlineAddressObjectDefId);
      t_Return := t_NewObjectId;
      
      -- Overrides, forces address to be stored as an "Other Address Type"
      -- this prevents errors that occure when there is no Street Type associated with the address
      t_CharValue := api.pkg_ColumnQuery.Value(a_ObjectId, 'DisplayFormat');
      api.pkg_ColumnUpdate.SetValue(t_NewObjectId, 'OtherAddress', t_CharValue);
      -- Address Type
      t_CharValue := 'Other / International';
      api.pkg_ColumnUpdate.SetValue(t_NewObjectId, 'AddressType', t_CharValue);
      
    else
      t_Return := null;
    end if;

    return t_Return;

    exception 
      when others then
        raise_application_error(-20000, 'There was an error copying the address data from address: ' || a_ObjectId || ' to address: ' || t_NewObjectId);

  end CopyInternalAddresstoOnline;

end pkg_ABC_Address;
/
