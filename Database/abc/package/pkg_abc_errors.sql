create or replace package pkg_ABC_Errors is

  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_ObjectList is api.pkg_Definition.udt_ObjectList;
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_DateList is api.pkg_Definition.udt_DateList;
  type udt_ErrorList is record (
    Pane                  udt_StringList,
    Msg                   udt_StringList,
    Presentation          udt_StringList
  );

  /*---------------------------------------------------------------------------
   * ErrorsInHTML()
   *   Used to create table for errors on public jobs
   *-------------------------------------------------------------------------*/
  function ErrorsInHTML(
    a_JobId                            number
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * ErrorsInHTMLPR()
   * Used to create table for errors on public Product Registration job
   *-------------------------------------------------------------------------*/
  /*function ErrorsInHTMLPR(
    a_JobId number
  ) return varchar2;*/

  /*---------------------------------------------------------------------------
   * ErrorsInHTMLPRAmend()
   * Used to create table for errors on public Product Registration Amendment
   * job
   *-------------------------------------------------------------------------*/
  /*function ErrorsInHTMLPRAmend(
    a_JobId number
  ) return varchar2;*/

  /*---------------------------------------------------------------------------
   * OnlineErrorList()
   * Used to determine which errors to raise on errors pane on public Product
   * Registration job
   *-------------------------------------------------------------------------*/
  function OnlineErrorList (
    a_JobID                               udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListAmend()
   *   Currently used to call a list of document errors
   * Called from ErrorsInHTML to get the list of errors.
   *-------------------------------------------------------------------------*/
  function OnlineErrorListAmend (
    a_JobId                             udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPR()
   *   Used to determine which errors to raise on errors pane on public Product
   * Registration job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPR (
    a_JobId                             udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPRRenewal() -- PUBLIC
   *   Used to determine which errors to raise on errors pane on public Product
   * Renewal job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPRRenewal(
    a_JobId                             udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPRAmend()
   * Used to determine which errors to raise on errors pane on public Product
   * Registration Amendment job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPRAmend (
    a_JobId udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPermit()
   *   Used to determine which errors to raise on the errors pane of the public
   * Permit Application job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPermit (
    a_JobId                             udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPermitRenewal()
   *   Used to determine which errors to raise on the errors pane of the public
   * Permit Renewal job.
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPermitRenewal (
    a_JobId                             udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListCPL()
   *   Used to determine which errors to raise on the errors pane of the public
   * CPL Submission job.
   *-------------------------------------------------------------------------*/
  function OnlineErrorListCPL (
    a_JobId                             udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPetition()
   * Used to determine which errors to raise on the errors pane of the public
   * Petition job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPetition (
    a_JobId                        udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPetitionAmend()
   *   Used to determine which errors to raise on the errors pane of the public
   * Petition Amendment job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPetitionAmend (
    a_JobId                        udt_Id
  ) return udt_ErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListRenewal()
   *   Currently used to call a list of document errors
   * Called from ErrorsInHTML to get the list of errors.
   *-------------------------------------------------------------------------*/
  function OnlineErrorListRenewal (
    a_JobId                             udt_Id
  ) return udt_ErrorList;

end pkg_ABC_Errors;
/
create or replace package body pkg_ABC_Errors is

  /*---------------------------------------------------------------------------
   * CheckResponsesForJob() -- PUBLIC
   *   Return application errors for responses on a job.
   *   Any changes made to code here likely will also need to be made in either
   *    the Pane Mandatory python details on the job or the following details
   *    on o_ABC_QAResponse: IsUnanswered, ResponseTextRequired, ResponseDateRequired
   *-------------------------------------------------------------------------*/
  function CheckResponsesForJob (
    a_JobId                             udt_Id,
    a_ResponseEndPointName              varchar2,
    a_ErrorList                         in out udt_ErrorList
  ) return udt_ErrorList is
    t_UnansweredCount                   number;
    t_NoTextResponseCount               number;
    t_NoDateResponseCount               number;
    t_ResponseEPId                      udt_Id;
  begin
    t_ResponseEPId := api.pkg_ConfigQuery.EndPointIdForName(
        api.pkg_ColumnQuery.value(a_JobId, 'ObjectDefName'), a_ResponseEndPointName);
    select
      count(case when ((q.AcceptableAnswer != 'N/A' or q.AcceptableAnswer is null)
          and q.AnswerOnJob is null) then 1 end) UnansweredCount,
      count(case when ((q.AcceptableAnswer in ('Yes', 'No') and q.AcceptableAnswer != q.AnswerOnJob) or q.AcceptableAnswer = 'N/A')
          and q.NonacceptableResponseType = 'Textbox'
          and q.ResponseText is null then 1 end) NoTextResponseCount,
      count(case when ((q.AcceptableAnswer in ('Yes', 'No') and q.AcceptableAnswer != q.AnswerOnJob) or q.AcceptableAnswer = 'N/A')
          and q.NonacceptableResponseType = 'Date'
          and q.ResponseDate is null then 1 end) NoDateResponseCount
      into
        t_UnansweredCount,
        t_NoTextResponseCount,
        t_NoDateResponseCount
      from api.relationships r
      join query.o_abc_qaresponse q
          on q.objectid = r.ToObjectId
     where r.EndPointId = t_ResponseEPId
       and r.FromObjectId = a_JobId
       and q.ShowQuestionSQL = 'Y';
    if t_UnansweredCount > 0 then
      a_ErrorList.Msg(a_ErrorList.Msg.count + 1) := 'Before you go on, you must answer all questions.';
      a_ErrorList.Pane(a_ErrorList.Pane.count + 1) := 'Questions';
    end if;
    if t_NoTextResponseCount > 0 then
      a_ErrorList.Msg(a_ErrorList.Msg.count + 1) := 'Before you go on, you must provide a Response.';
      a_ErrorList.Pane(a_ErrorList.Pane.count + 1) := 'Questions';
    end if;
    if t_NoDateResponseCount > 0 then
      a_ErrorList.Msg(a_ErrorList.Msg.count + 1) := 'Before you go on, you must provide a Date Response.';
      a_ErrorList.Pane(a_ErrorList.Pane.count + 1) := 'Questions';
    end if;

    return a_ErrorList;

  end CheckResponsesForJob;

  /*---------------------------------------------------------------------------
   * ErrorsInHTML() -- PUBLIC
   *   Used to create table for errors on public jobs
   *-------------------------------------------------------------------------*/
  function ErrorsInHTML(
    a_JobId                             number
  ) return varchar2 is
    t_baseurl                           varchar2(200);
    t_FourSpaces                        varchar2(200);
    t_JobDefName                        varchar2(100) :=
        api.pkg_ColumnQuery.Value(a_JobId, 'ObjectDefName');
    t_IsOnlineSpecialEvent              varchar2(1);
    t_List                              udt_ErrorList;
    t_output                            varchar2(4000) :=
        '<table>';
    tempstring                          varchar2(4000);
    t_url                               varchar2(400);
  begin
    if t_JobDefName = 'j_ABC_NewApplication' then
      t_List := pkg_ABC_Errors.OnlineErrorList(a_JobId);
    elsif t_JobDefName = 'j_ABC_AmendmentApplication' then
      t_List := pkg_ABC_Errors.OnlineErrorListAmend(a_JobId);
    elsif t_JobDefName = 'j_ABC_RenewalApplication' then
      t_List := pkg_ABC_Errors.OnlineErrorListRenewal(a_JobId);
    elsif t_JobDefName = 'j_ABC_PRApplication' then
      t_List := pkg_ABC_Errors.OnlineErrorListPR(a_JobId);
    elsif t_JobDefName = 'j_ABC_PRRenewal' then
      t_List := pkg_ABC_Errors.OnlineErrorListPRRenewal(a_JobId);
    elsif t_JobDefName = 'j_ABC_PRAmendment' then
      t_List := pkg_ABC_Errors.OnlineErrorListPRAmend(a_JobId);
    elsif t_JobDefName = 'j_ABC_PermitApplication' then
      t_List := pkg_ABC_Errors.OnlineErrorListPermit(a_JobId);
    elsif t_JobDefName = 'j_ABC_PermitRenewal' then
      t_List := pkg_ABC_Errors.OnlineErrorListPermitRenewal(a_JobId);
    elsif t_JobDefName = 'j_ABC_CPLSubmissions' then
      t_List := pkg_ABC_Errors.OnlineErrorListCPL(a_JobId);
    elsif t_JobDefName = 'j_ABC_Petition' then
      t_List := pkg_ABC_Errors.OnlineErrorListPetition(a_JobId);
    elsif t_JobDefName = 'j_ABC_PetitionAmendment' then
      t_List := pkg_ABC_Errors.OnlineErrorListPetitionAmend(a_JobId);
    end if;

    t_IsOnlineSpecialEvent := nvl(api.pkg_columnquery.Value(a_JobId, 'OnlineSpecialEvent'),'N');
    t_baseurl := 'Default.aspx?PossePresentation=';
    t_FourSpaces := chr(38) || 'nbsp;' || chr(38) || 'nbsp;' || chr(38) || 'nbsp;' || chr(38) ||
        'nbsp;';

    for i in 1..t_List.Pane.count loop
      if t_IsOnlineSpecialEvent = 'Y' then
        t_url := t_baseurl || 'SpecialEventWizard' || chr(38) || 'PossePane=' || t_List.Pane(i) ||
            chr(38) || 'PosseObjectId=' || a_JobId;
      elsif t_List.Presentation.Exists(i) then
        t_url := t_baseurl || t_List.Presentation(i) || chr(38) || 'PossePane=' || t_List.Pane(i) ||
            chr(38) || 'PosseObjectId=' || a_JobId;
      else
        t_url := t_baseurl || 'Wizard' || chr(38) || 'PossePane=' || t_List.Pane(i) ||
            chr(38) || 'PosseObjectId=' || a_JobId;
      end if;
      tempstring := '<tr><td>' || '<img src="images/icons/Complaint.png">' || '</td><td>' ||
          t_FourSpaces ||'<a href="' || t_url || '">Fix</a>' || t_FourSpaces ||'</td><td>' ||
          t_List.Msg(i) || '</td></tr>';
      if length(tempstring) + length(t_output) < 3960 then
        t_output := t_output || tempstring;
      else
        t_output := t_output || '<tr><td>....</td></tr>';
        exit;
      end if;
    end loop;

    t_output := t_output || '</table>';
    if t_output = '<table></table>' then
      t_output := '';
    end if;

    return t_output;

  end ErrorsInHTML;

/*
  /*---------------------------------------------------------------------------
   * ErrorsInHTMLPR()
   * Used to create table for errors on public Product Registration job
   *-------------------------------------------------------------------------*-/

  function ErrorsInHTMLPR(
    a_JobId number
  ) return varchar2 is
    t_url           varchar2(400);
    t_baseurl       varchar2(200);
    t_output        varchar2(4000) := '<table>';
    tempstring      varchar2(4000);
    t_FourSpaces    varchar2(200);
    t_List udt_ErrorList := pkg_ABC_Errors.OnlineErrorListPR(a_JobId);
  begin

    t_baseurl := 'Default.aspx?PossePresentation=Wizard&PossePane=';

    t_FourSpaces := chr(38) || 'nbsp;' || chr(38) || 'nbsp;' || chr(38) || 'nbsp;' || chr(38) || 'nbsp;';

    for i in 1..t_List.Pane.count loop
      t_url := t_baseurl || t_List.Pane(i) || '&PosseObjectId=' || a_JobId;
       tempstring := '<tr><td>' || '<img src="images/icons/Complaint.png">' || '</td><td>'||t_FourSpaces||'<a href="' ||
         t_url || '">Fix</a>'||t_FourSpaces||'</td><td>' || t_List.Msg(i) || '</td></tr>';
       if length(tempstring) + length(t_output) < 3960 then
          t_output := t_output || tempstring;
       else
          t_output := t_output || '<tr><td>....</td></tr>';
          exit;
       end if;
    end loop;
    t_output := t_output || '</table>';

    if t_output = '<table></table>' then t_output := ''; end if;

    return t_output;
  end ErrorsInHTMLPR;

  /*---------------------------------------------------------------------------
   * ErrorsInHTMLPRAmend()
   * Used to create table for errors on public Product Registration Amendment
   * job
   *-------------------------------------------------------------------------*-/

  function ErrorsInHTMLPRAmend(
    a_JobId number
  ) return varchar2 is
    t_url           varchar2(400);
    t_baseurl       varchar2(200);
    t_output        varchar2(4000) := '<table>';
    tempstring      varchar2(4000);
    t_FourSpaces    varchar2(200);
    t_List udt_ErrorList := pkg_ABC_Errors.OnlineErrorListPRAmend(a_JobId);
  begin

    t_baseurl := 'Default.aspx?PossePresentation=Wizard&PossePane=';

    t_FourSpaces := chr(38) || 'nbsp;' || chr(38) || 'nbsp;' || chr(38) || 'nbsp;' || chr(38) || 'nbsp;';

    for i in 1..t_List.Pane.count loop
      t_url := t_baseurl || t_List.Pane(i) || '&PosseObjectId=' || a_JobId;
       tempstring := '<tr><td>' || '<img src="images/icons/Complaint.png">' || '</td><td>'||t_FourSpaces||'<a href="' ||
         t_url || '">Fix</a>'||t_FourSpaces||'</td><td>' || t_List.Msg(i) || '</td></tr>';
       if length(tempstring) + length(t_output) < 3960 then
          t_output := t_output || tempstring;
       else
          t_output := t_output || '<tr><td>....</td></tr>';
          exit;
       end if;
    end loop;
    t_output := t_output || '</table>';

    if t_output = '<table></table>' then t_output := ''; end if;

    return t_output;
  end ErrorsInHTMLPRAmend;
*/

  /*---------------------------------------------------------------------------
   * CheckUserDetails() -- PRIVATE
   * Common code for checking the user's details on the applications
   *-------------------------------------------------------------------------*/
  function CheckUserDetails (
    a_List     udt_ErrorList,
    a_JobId    udt_Id
  ) return udt_ErrorList is
    t_List     udt_ErrorList;
  begin
    t_List := a_List;

    if api.pkg_columnquery.value(a_JobId, 'OnlineGTFirstName') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Your First Name is required.';
       t_List.Pane(t_List.Pane.count + 1) := 'Contact';
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineGTLastName') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Your Last Name is required.';
       t_List.Pane(t_List.Pane.count + 1) := 'Contact';
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineGTEmail') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Your Email is required.';
       t_List.Pane(t_List.Pane.count + 1) := 'Contact';
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineGTPhone') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Your Phone Number is required.';
       t_List.Pane(t_List.Pane.count + 1) := 'Contact';
    end if;

    return t_List;
  end CheckUserDetails;

  /*---------------------------------------------------------------------------
   * CheckLEDetails() --PRIVATE
   * Common code for checking the legal entity details on the applications
   *   - can be existing legal entity or new one
   *-------------------------------------------------------------------------*/
  function CheckLEDetails (
    a_List               udt_ErrorList,
    a_JobId              udt_ID
  ) return udt_ErrorList is
    t_List               udt_ErrorList;
    t_JobDefName         varchar2(100) := api.pkg_columnQuery.Value(a_JobId, 'ObjectDefName');
    t_PaneName           varchar2(20);
  begin
    t_List := a_List;

    t_PaneName := 'Contact';

    --Use Previous Legal Entity
    if api.pkg_columnquery.value(a_JobId, 'OnlineUseLegalEntityObjectId') is null then
      if api.pkg_columnquery.value(a_JobId, 'LicenseeType') is null then
        if t_JobDefName = 'j_ABC_NewApplication' then
          t_List.Msg(t_List.Msg.count + 1) := 'Licensee Type is required.';
        elsif t_JobDefName = 'j_ABC_PermitApplication' then
          t_List.Msg(t_List.Msg.count + 1) := 'Permittee Type is required.';
        elsif t_JobDefName = 'j_ABC_PRApplication' then
          t_List.Msg(t_List.Msg.count + 1) := 'Specify a registrant or select a registrant type.';
        end if;

        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      else -- Licensee Type is not null
        -- Ensure that new applicants have entered a Mailing Address
        if t_JobDefName = 'j_ABC_PRApplication'
           and api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddress') is null or api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddress') = '(None)' then
           t_List.Msg(t_List.Msg.count + 1) := 'A mailing address must be supplied for a new applicant.';
           t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        end if;
      end if;

      if t_JobDefName in ('j_ABC_PRApplication', 'j_ABC_NewApplication', 'j_ABC_PermitApplication') then
        if api.pkg_columnquery.value(a_JobId, 'SSNTIN') is not null then
          --Make sure the value entered for SSN is exactly 9 digits with no special characters
          if regexp_replace(abc.pkg_abc_encryption.decrypt(api.pkg_columnquery.value(a_JobId, 'SSNTINEncrypted')), '[0-9]*') is not null
             or length(abc.pkg_abc_encryption.decrypt(api.pkg_columnquery.value(a_JobId, 'SSNTINEncrypted'))) != 9 then
          t_List.Msg(t_List.Msg.count + 1) := 'Applicant SSN/TIN/EIN must be a 9 digit number without any dashes, slashes or spaces, e.g. 123456789.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
          end if;
        end if;
      end if;

      -- non-Individual Licensee Type
      if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseeTypeIsIndividual') <> 'Y' then

        if t_JobDefName in ('j_ABC_PRApplication', 'j_ABC_NewApplication', 'j_ABC_PermitApplication') then
          if api.pkg_columnquery.value(a_JobId, 'OnlineLELegalName') is null then
            t_List.Msg(t_List.Msg.count + 1) := 'Applicant Legal Name is required.';
            t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
          end if;
        end if;

        if api.pkg_columnquery.value(a_JobID, 'OnlineCountry') is null then 
          t_List.Msg(t_List.Msg.count + 1) := 'A Country of Incorporation is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        elsif api.pkg_columnquery.value(a_JobID, 'OnlineCountryOther') = 'Y' then 
          if api.pkg_columnquery.value(a_JobID, 'OnlineOtherCountry') is null then 
            t_List.Msg(t_List.Msg.count + 1) := 'A Country is required.';
            t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
          end if;
          if api.pkg_columnquery.value(a_JobID, 'OnlineOtherState') is null then 
            t_List.Msg(t_List.Msg.count + 1) := 'A State is required.';
            t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
          end if;
        elsif api.pkg_columnquery.value(a_JobID, 'OnlineState') is null then 
          t_List.Msg(t_List.Msg.count + 1) := 'A State is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        end if;

        if api.pkg_columnquery.value(a_JobId, 'OnlineState') = 'New Jersey'
          and api.pkg_columnquery.value(a_JobId, 'OnlineLECharterRevoked') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'Secretary of State question is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        elsif (api.pkg_columnquery.value(a_JobId, 'OnlineState') != 'New Jersey'
          -- Need to check null because onlinestate will be null when otherstate is used.
          or api.pkg_columnquery.value(a_JobId, 'OnlineOtherState') is not null)
          -- the NVL is necessary as the column name is inconsistent on j_ABC_PermitApplication
          and nvl(api.pkg_columnquery.value(a_JobId, 'OnlineLEAuthBusiness'),
                  api.pkg_columnquery.value(a_JobId, 'OnlineLEBusinessAuth')) is null then
          t_List.Msg(t_List.Msg.count + 1) := 'Secretary of State question is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        end if;

        if api.pkg_columnquery.value(a_JobId, 'OnlineLECharterRevoked') = 'Yes'
          and api.pkg_columnquery.value(a_JobId, 'OnlineLERevocationDate') is null and
          (api.pkg_columnquery.value(a_JobId, 'OnlineLESuspensionBegin') is null or
           api.pkg_columnquery.value(a_JobId, 'OnlineLESuspensionEnd') is null) then
           t_List.Msg(t_List.Msg.count + 1) := 'Date of Revocation or Beginning and Ending Date required.';
           t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        end if;

        -- Contact Details
        if api.pkg_columnquery.value(a_JobId, 'OnlineLEContactName') is null then
           t_List.Msg(t_List.Msg.count + 1) := 'Applicant Contact Name is required.';
           t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        end if;

      end if; -- end non-Individual Licensee Type

     -- Individual Licensee Type
     if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseeTypeIsIndividual') = 'Y' then
       if api.pkg_columnquery.value(a_JobId, 'OnlineLEFirstName') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Applicant First Name is required.';
         t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
       end if;

       if api.pkg_columnquery.value(a_JobId, 'OnlineLELastName') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Applicant Last Name is required.';
         t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
       end if;

       if t_JobDefName = 'j_ABC_NewApplication' then
          if api.pkg_columnquery.value(a_JobId, 'OnlLEDriversLicense') is null then
             t_List.Msg(t_List.Msg.count + 1) := 'Applicant Drivers License is required.';
             t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
          end if;
       end if;
     end if;
     --end Indivudial Licensee Type

      -- Contact Details
      if api.pkg_columnquery.value(a_JobId, 'OnlineLEContactPhone') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Applicant Business Phone is required.';
       t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId, 'OnlineLEPreferredContactMethod') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Applicant Preferred Contact Method is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId, 'OnlineLEPreferredContactMethod') = 'Mail'
        and api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddress') is null or api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddress') = '(None)' then
          t_List.Msg(t_List.Msg.count + 1) := 'Applicant Mailing Address is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId, 'OnlineLEPreferredContactMethod') = 'Mobile Phone'
        and api.pkg_columnquery.value(a_JobId, 'OnlineLEFax') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'Applicant Mobile Phone is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId, 'OnlineLEPreferredContactMethod') = 'Home Phone'
        and api.pkg_columnquery.value(a_JobId, 'OnlineLEAltPhone') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'Applicant Home Phone is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId, 'OnlineLEPreferredContactMethod') = 'Email'
        and api.pkg_columnquery.value(a_JobId, 'OnlineLEEmail') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'Applicant Email is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId, 'OnlineLESameAsMailing') = 'Y'
        and api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddress') is null or api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddress') = '(None)' then
           t_List.Msg(t_List.Msg.count + 1) := 'Applicant Mailing Address is required when the "Same As" checkbox is selected.';
           t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

    end if;  --Use Previous Legal Entity

    return t_List;
  end CheckLEDetails;

  /*---------------------------------------------------------------------------
   * OnlineErrorList()
   * Used to determine which errors to raise on errors pane on public Product
   * Registration job
   *-------------------------------------------------------------------------*/
  function OnlineErrorList (
    a_JobId                               udt_Id
  ) return udt_ErrorList is
  t_List                                  udt_ErrorList;
  t_DocRequired                           varchar2(50);
  t_LicenseTypeObjectId                   udt_Id := api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeObjectId');
  t_WarehouseAddressRelationship          udt_Id;
  t_RegionObjectId                        udt_id;
  t_OfficeObjectId                        udt_id;
  t_InvalidQuestionCount                  number;

  begin
    if api.pkg_columnquery.value(a_JobId, 'OnlineSpecialEvent') = 'Y'
      and api.pkg_columnquery.value(a_JobId, 'OnlineEVTEventType') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Event Type is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'Contact';
    end if;

    -- User Details
    t_List := CheckUserDetails(t_List, a_JobId);

    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') = '24' and
       api.pkg_columnquery.value(a_JobId, 'OnlineRelatedLicenseObjectId') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Please select a related License.';
         t_List.Pane(t_List.Pane.count + 1) := 'Contact';
    else
    -- Legal Entity Details
       t_List := CheckLEDetails(t_List, a_JobId);
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSpecialEvent') = 'Y' then
      if api.pkg_columnquery.value(a_JobId, 'OnlineEVTLocationName') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Event Location Name is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(a_JobId, 'OnlineEVTAddress') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Event Address is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(a_JobId, 'OnlineEVTDates') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Event Date is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(a_JobId, 'OnlineEVTNumOfDays') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Number of Event Days is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
    else --Non-Special Event
      -- Require premises mailing address if any ES information is filled
      if api.pkg_columnquery.value(t_LicenseTypeObjectId, 'PremiseAddressRequired') = 'N' and
        (api.pkg_columnquery.value(a_JobId, 'OnlineESEstablishmentType') is not null or
        api.pkg_columnquery.value(a_JobId, 'OnlineESPreferredContactMethod') is not null or
        api.pkg_columnquery.value(a_JobId, 'OnlinePremiseAddressES') is not null or
        api.pkg_columnquery.value(a_JobId, 'OnlineESAlternatePhone') is not null or
        api.pkg_columnquery.value(a_JobId, 'OnlineESContactName') is not null or
        api.pkg_columnquery.value(a_JobId, 'OnlineESDoingBusinessAs') is not null or
        api.pkg_columnquery.value(a_JobId, 'OnlineESEmail') is not null or
        api.pkg_columnquery.value(a_JobId, 'OnlineESFax') is not null or
        api.pkg_columnquery.value(a_JobId, 'OnlineESOperator') is not null) and
        (api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddressES') is null or api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddressES') = '(None)') then
          t_List.Msg(t_List.Msg.count + 1) := 'Premises Mailing Address is required when establishment information is entered.';
          t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(t_LicenseTypeObjectId, 'PremiseAddressRequired') = 'Y'
        and api.pkg_columnquery.value(a_JobId, 'OnlineESEstablishmentType') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Establishment Type is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(t_LicenseTypeObjectId, 'PremiseAddressRequired') = 'Y'
        and api.pkg_columnquery.value(a_JobId, 'OnlineESDoingBusinessAs') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Doing Business As / Trade Names is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(t_LicenseTypeObjectId, 'PremiseAddressRequired') = 'Y'
        and api.pkg_columnquery.value(a_JobId, 'OnlineESPreferredContactMethod') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Establishment Preferred Contact Method is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(a_JobId, 'OnlineESSameAsMailing') = 'Y'
        and api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddressES') is null then
           t_List.Msg(t_List.Msg.count + 1) := 'Premises Mailing Address is required when the "Same As" checkbox is selected.';
           t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if --api.pkg_columnquery.value(a_JobId, 'OnlineESPreferredContactMethod') = 'Mail'
        --and
         api.pkg_columnquery.value(t_LicenseTypeObjectId, 'PremiseAddressRequired') = 'Y'
        and api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddressES') is null or api.pkg_columnquery.value(a_JobId, 'OnlineMailingAddressES') = '(None)' then
           t_List.Msg(t_List.Msg.count + 1) := 'Premises Mailing Address is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(t_LicenseTypeObjectId, 'PremiseAddressRequired') = 'Y'
        and (api.pkg_columnquery.value(a_JobId, 'OnlinePremiseAddressES') is null or api.pkg_columnquery.value(a_JobId, 'OnlinePremiseAddressES') = '(None)')
        and api.pkg_columnquery.value(a_JobId, 'OnlineESSameAsMailing') != 'Y' then
          t_List.Msg(t_List.Msg.count + 1) := 'Premises Physical Address is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(a_JobId, 'OnlineESContactPhone') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Establishment Business Phone is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(a_JobId, 'OnlineESPreferredContactMethod') = 'Mobile Phone'
        and api.pkg_columnquery.value(a_JobId, 'OnlineESFax') is null then
           t_List.Msg(t_List.Msg.count + 1) := 'Establishment Mobile Phone is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(a_JobId, 'OnlineESPreferredContactMethod') = 'Email'
        and api.pkg_columnquery.value(a_JobId, 'OnlineESEmail') is null then
           t_List.Msg(t_List.Msg.count + 1) := 'Establishment Email is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
      if api.pkg_columnquery.value(a_JobId, 'OnlineESPreferredContactMethod') = 'Home Phone'
        and api.pkg_columnquery.value(a_JobId, 'OnlineESAlternatePhone') is null then
           t_List.Msg(t_List.Msg.count + 1) := 'Establishment Home Phone is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
    end if;

     -- License Type Specific Details
    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') = '06' and
       api.pkg_columnquery.value(a_JobId, 'OnlineSportingFacilityCapacity') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'A value for Sporting Facility Capacity is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') = '08' and
       api.pkg_columnquery.value(a_JobId, 'OnlineRstrctdBreweryProduction') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'A value for Restricted Brewery Production is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') = '11' and
       api.pkg_columnquery.value(a_JobId, 'OnlineLimitedBreweryProduction') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'A value for Limited Brewery Production is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') = '13' then
       if api.pkg_columnquery.value(a_JobId, 'OnlineRetailTransitType') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A value for Transit License is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
       else
          if api.pkg_columnquery.value(a_JobId, 'OnlineRetailTransitType') in ('Limousine','Boat') then
             if api.pkg_columnquery.value(a_JobId, 'OnlineRetailTransitType') = 'Limousine' then
                if api.pkg_columnquery.value(a_JobId, 'VehiclesEntered') is null then
                   t_List.Msg(t_List.Msg.count + 1) := 'At least one vehicle is required for a ' || api.pkg_columnquery.value(a_JobId, 'OnlineRetailTransitType') || ' License.';
                   t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
                end if;
                for p in (select v.objectid
                            from query.o_abc_vehicle v
                            join query.r_abc_applicationvehicle av on av.VehicleId = v.ObjectId
                            join query.j_abc_newapplication na     on na.ObjectId  = av.NewApplicationId
                           where na.ObjectId = a_JobId
                             and v.VehicleType = 'Limousine'
                         )
                         loop
                            if api.pkg_columnquery.Value(p.objectid, 'VIN') is null or
                               api.pkg_columnquery.Value(p.objectid, 'MakeModelYear') is null or
                               api.pkg_columnquery.Value(p.objectid, 'StateRegistration') is null or
                               api.pkg_columnquery.Value(p.objectid, 'StateOfRegistration') is null or
                               api.pkg_columnquery.Value(p.objectid, 'OwnedOrLeasedLimousine') is null or
                               nvl (api.pkg_columnquery.Value(p.objectid, 'StorageAddress') , api.pkg_columnquery.Value(p.objectid, 'OnlineStorageAddress') ) is null then
                               t_List.Msg(t_List.Msg.count + 1) := 'At least one mandatory detail for Limousine is missing';
                               t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
                               exit;
                            end if;
                         end loop;
             else
                if api.pkg_columnquery.value(a_JobId, 'VesselsEntered') is null then
                   t_List.Msg(t_List.Msg.count + 1) := 'At least one vessel is required for a ' || api.pkg_columnquery.value(a_JobId, 'OnlineRetailTransitType') || ' License.';
                   t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
                end if;
                for p in (select v.objectid
                            from query.o_abc_vehicle v
                            join query.r_abc_applicationvehicle av on av.VehicleId = v.ObjectId
                            join query.j_abc_newapplication na     on na.ObjectId  = av.NewApplicationId
                           where na.ObjectId = a_JobId
                             and v.VehicleType = 'Boat'
                         )
                         loop
                            if api.pkg_columnquery.Value(p.objectid, 'OwnedOrLeasedVessel') is null or
                               api.pkg_columnquery.Value(p.objectid, 'USCGRegistrationNumber') is null or
                               api.pkg_columnquery.Value(p.objectid, 'VesselName') is null or
                               api.pkg_columnquery.Value(p.objectid, 'VesselLength') is null or
                               nvl (api.pkg_columnquery.Value(p.objectid, 'StorageAddress') , api.pkg_columnquery.Value(p.objectid, 'OnlineStorageAddress') ) is null then
                               t_List.Msg(t_List.Msg.count + 1) := 'At least one mandatory detail for Boat is missing';
                               t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
                               exit;
                            end if;
                         end loop;
             end if;
          end if;
       end if;
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') = '18' and
       api.pkg_columnquery.value(a_JobId, 'OnlineSupplementLmtdDistillery') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'A value for Supplementary Limited Distillery Production is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') = '21' then
       if api.pkg_columnquery.Value(a_JobId, 'OnlinePlenaryWineryWholesalePr') = 'Y' and
          api.pkg_columnquery.Value(a_JobId, 'OnlinePlenaryWineryProduction') is null then
           t_List.Msg(t_List.Msg.count + 1) := 'A value for Plenary Winery Production is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
       end if;
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') = '22' and
       api.pkg_columnquery.value(a_JobId, 'OnlineFarmWineryProduction') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'A value for Farm Winery Production is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
    end if;
    if api.pkg_columnquery.value(a_JobId, 'OnlineLicenseTypeCode') in ('41', 'OSWW') and
       api.pkg_columnquery.value(a_JobId, 'OnlineOutStateWineryProduction') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'A value for Out of State Winery Production is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
    end if;
    -- Warehouse Address and Phone Number check
    if api.pkg_ColumnQuery.Value(a_JobId, 'EnableWarehouseAddressEntry') = 'Y' then
      -- Check for Warehouse detail values
      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineWarehouseName') is not null then
        -- Ensure Address and Phone Number are set
        for wRel in (select 1
                       from dual
                      where not exists (select r.AddressObjectId
                                          from query.r_ABC_NewAppWarehouseAddress r
                                         where r.ApplicationJobId = a_JobId)) loop
          t_List.Msg(t_List.Msg.count + 1) := 'An address for the Warehouse is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
        end loop;

        if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineWarehousePhoneNumber') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A value for Warehouse Phone Number is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'AdditionalInformation';
        end if;
      end if;
    end if;

    -- Questions are done by Pane Mandatory due to being Python. There was a loophole to circumvent the pane mandatory, so we need it here as well
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Handle Document Responses
    for i in (select r.DocumentTypeObjectId DocOnLicType
                from query.r_OLLicenseTypeDocumentType r
               where r.LicenseTypeObjectId = api.pkg_columnquery.NumericValue(a_JobId, 'OnlineLicenseTypeObjectId')
                 and r.Mandatory = 'Y'
                 and r.DocumentTypeObjectId not in
                (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                  from query.r_Olnewappelectronicdocument rr
                 where rr.OLNewAppJobId = a_JobId)
              union
              select na.OnlineDocTypeId
              from query.r_ABC_NewAppDocumentType na
              where na.NewAppId = a_JobId
                and na.QuestionMandatory = 'Y'
                and na.OnlineDocTypeId not in
                (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                  from query.r_Olnewappelectronicdocument rr
                 where rr.OLNewAppJobId = a_JobId))
    loop
      t_DocRequired := api.pkg_columnquery.Value(i.doconlictype, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    for z in (select r.DocTypeId
                from query.r_ABC_AppResponseDocType r
               where r.NewAppId = a_JobId
                 and r.Mandatory = 'Y'
                 and r.DocTypeId not in
                (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                  from query.r_Olnewappelectronicdocument rr
                 where rr.OLNewAppJobId = a_JobId)
              union
              select ar.DocTypeId
              from query.r_ABC_AppResponseDocType ar
              where ar.NewAppId = a_JobId
                and ar.QuestionMandatory = 'Y'
                and ar.DocTypeId not in
                (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                  from query.r_Olnewappelectronicdocument rr
                 where rr.OLNewAppJobId = a_JobId))
    loop
      t_DocRequired := api.pkg_columnquery.Value(z.DocTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    -- County and municipality check
    --if the license type issuring authority is municipality, check that the user selected county and municipality
    if api.pkg_columnquery.Value(a_JobId, 'OnlineLicenseIssuingAuthority') = 'Municipality' then
      if api.pkg_columnquery.Value(a_JobId, 'CountyObjectId') is null then
        --this is an error, the user must select a county
        t_List.Msg(t_List.Msg.count + 1) := 'A value for County is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;

      if api.pkg_columnquery.Value(a_JobId, 'MunicipalityObjectId') is null then
        --this is an error, the user must select a municipality
        t_List.Msg(t_List.Msg.count + 1) := 'A value for Municipality is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
    else
      select count(*)
             into t_RegionObjectId
             from query.o_abc_region r
             where r.WholesaleDefault = 'Y';

      if t_RegionObjectId = 0 then
         --this is an error because someone on admin forgot to select a county marked as Wholesale Default
         t_List.Msg(t_List.Msg.count + 1) := 'No default County is set.  Please contact the system administrator.';
         t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;

      select count(*)
             into t_OfficeObjectId
             from query.o_abc_office o
             where o.WholesaleDefault = 'Y';

      if t_OfficeObjectId = 0 then
         --this is an error because someone on admin forgot to select a municipality marked as Wholesale Default
         t_List.Msg(t_List.Msg.count + 1) := 'No default Municipality is set.  Please contact the system administrator.';
         t_List.Pane(t_List.Pane.count + 1) := 'Region';
      end if;
    end if;

    return t_List;
  end OnlineErrorList;

  /*---------------------------------------------------------------------------
   * OnlineErrorListAmend() -- PUBLIC
   *   Currently used to call a list of document errors
   * Called from ErrorsInHTML to get the list of errors.
   *-------------------------------------------------------------------------*/
  function OnlineErrorListAmend (
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_List                              udt_ErrorList;
    t_DocRequired                       varchar2(50);
    t_InvalidQuestionCount              number;
  begin

    -- Questions are done by Pane Mandatory due to being Python. There was a
    -- loophole to circumvent the pane mandatory, so we need it here as well
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Handle Document Responses
    for a in (select r.DocTypeId
              from query.r_ABC_AmendResponseDocType r
              where r.AmendAppId = a_JobId
                and (r.Mandatory = 'Y' or r.QuestionMandatory = 'Y')
                and r.DocTypeId not in
                  (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId,
                       'DocumentTypeObjectId')
                   from query.r_Olamendappelectronicdocument rr
                   where rr.JobId = a_JobId)
             ) loop
      t_DocRequired := api.pkg_columnquery.Value(a.DocTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired ||
          '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;
    for a in (select r.OnlineDocTypeId
              from query.r_ABC_AmendAppDocumentType r
              where r.AmendAppId = a_JobId
                and (r.Mandatory = 'Y' or r.QuestionMandatory = 'Y')
                and r.OnlineDocTypeId not in
                  (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId,
                       'DocumentTypeObjectId')
                   from query.r_Olamendappelectronicdocument rr
                   where rr.JobId = a_JobId)
             ) loop
      t_DocRequired := api.pkg_columnquery.Value(a.Onlinedoctypeid, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired ||
          '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;

  end OnlineErrorListAmend;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPR() -- PUBLIC
   *   Used to determine which errors to raise on errors pane on public Product
   * Registration job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPR (
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_DocRequired                       varchar2(50);
    t_ExplainTTB                        varchar2(4000);
    t_InvalidQuestionCount              number;
    t_IsProdNoTTB                       varchar2(3);
    t_LicenseeIsNull                    number;
    t_LicenseNumberIsNull               number;
    t_LicRelCount                       number;
    t_List                              udt_ErrorList;
    t_PaneName                          varchar2(50) := 'Contact';
    t_ProductIds                        udt_IdList;
    t_ProductTypeIsNull                 number;
    t_ProductIsNull                     number;
    t_TTBIsNull                         number;
  begin

    t_ExplainTTB := api.pkg_columnquery.Value(a_JobId,'WhyTTBLabelApprovalNotRequired');

    -- User details checks
    t_List := CheckUserDetails(t_List, a_JobId);
    t_List := CheckLEDetails(t_List, a_JobId);

    -- Initial error checks
    if api.pkg_columnquery.value(a_JobId,'OnlineRegistrantHasNJLicense') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Please specify whether the Registrant has a NJ License.';
      t_List.Pane(t_List.Pane.count() + 1) := t_PaneName;
    elsif api.pkg_columnquery.value(a_JobId,'OnlineRegistrantHasNJLicense') = 'Yes'
        and api.pkg_columnquery.value(a_JobId,'License') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Please select a License.';
      t_List.Pane(t_List.Pane.count() + 1) := t_PaneName;
    elsif api.pkg_columnquery.value(a_JobId,'OnlineRegistrantHasNJLicense') = 'No'
        and api.pkg_columnquery.value(a_JobId,'OnlineRegistrantHasNJTAPPermit') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Please specify whether the Registrant has a NJ TAP Permit.';
      t_List.Pane(t_List.Pane.count() + 1) := t_PaneName;
    elsif api.pkg_columnquery.value(a_JobId,'OnlineRegistrantHasNJTAPPermit') = 'Yes'
        and api.pkg_columnquery.value(a_JobId,'TAPPermitNumberOnline') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Please select a TAP Permit.';
      t_List.Pane(t_List.Pane.count() + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId,'OnlineRegistrantHasNJLicense') = 'No'
        and api.pkg_columnquery.value(a_JobId,'OnlineRegistrantHasNJTAPPermit') = 'No' then
      if api.pkg_columnquery.value(a_JobId, 'OnlineRegistrantHasNJPresence') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Please specify whether the Registrant has a NJ '
        || 'Commercial Premise or Stores Products in NJ.';
        t_List.Pane(t_List.Pane.count() + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId,'OnlineRegistrantSolicitingInNJ') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Please specify whether the Registrant is Soliciting '
        || 'Sales in NJ.';
        t_List.Pane(t_List.Pane.count() + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId,'OnlineRegistrantHasNJPresence') = 'Yes' then
        t_List.Msg(t_List.Msg.count() + 1) := 'A License or TAP must be provided when the Registrant'
        || ' has a NJ Commercial Premise or Stores Products in NJ.';
        t_List.Pane(t_List.Pane.count() + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId,'OnlineRegistrantSolicitingInNJ') = 'Yes' then
        t_List.Msg(t_List.Msg.count() + 1) := 'A License or TAP must be provided when the Registrant'
        || ' is Soliciting Sales in NJ.';
        t_List.Pane(t_List.Pane.count() + 1) := t_PaneName;
      end if;
    end if;

    --Check if there are any products associated to the job.
    if extension.pkg_RelUtils.StoredRelExistsViaEPNames(a_JobId,'Product') = 'N' then
      t_List.Msg(t_List.Msg.count() + 1) := 'Products are required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Products';
    end if;

    --Check if there are any required product details that are blank.
    for i in (
        select p.name ProductName
        from
          query.j_abc_prapplication pr
          join api.relationships r
              on r.FromObjectId = pr.ObjectId
          join query.o_abc_product p
              on p.ObjectId = r.ToObjectId
        where p.Name is null
          and pr.JobId = a_JobId
        ) loop
      if i.ProductName is null and t_ProductIsNull is null then
        t_List.Msg(t_List.Msg.count() + 1) := 'Product name is required.';
        t_List.Pane(t_List.Pane.count() + 1) := 'Products';
        t_ProductIsNull := 1;
      else
        exit;
      end if;
    end loop;

    for i in (
        select p.Type ProductType
        from
          query.j_abc_prapplication pr
          join api.relationships r
              on r.FromObjectId = pr.ObjectId
          join query.o_abc_product p
              on p.ObjectId = r.ToObjectId
        where p.Type is null
          and pr.JobId = a_JobId
        ) loop
      if i.productType is null and t_ProductTypeIsNull is null then
        t_List.Msg(t_List.Msg.count() + 1) := 'Product type is required.';
        t_List.Pane(t_List.Pane.count() + 1) := 'Products';
        t_ProductTypeIsNull := 1;
      else
        exit;
      end if;
    end loop;

    for i in (
        select
          p.TTBExplanation,
          p.TTBCOLA
        from
          query.j_abc_prapplication pr
          join api.relationships r
              on r.FromObjectId = pr.ObjectId
          join query.o_abc_product p
              on p.ObjectId = r.ToObjectId
        where ((p.TTBCOLA = 'No' and p.TTBExplanation is null) or p.TTBCOLA is null)
          and pr.JobId = a_JobId and ROWNUM < 2
        ) loop
      if i.TTBCOLA is null then
        t_List.Msg(t_List.Msg.count() + 1) := 'TTB/COLA is required.';
        t_List.Pane(t_List.Pane.count() + 1) := 'Products';
      elsif i.TTBExplanation is null then
        t_List.Msg(t_List.Msg.count() + 1) := 'Please provide an explanation for no TTB/COLA.';
        t_List.Pane(t_List.Pane.count() + 1) := 'Products';
      else
        exit;
      end if;
    end loop;

    --Check if there are any distributors associated to the job.
    if extension.pkg_RelUtils.StoredRelExistsViaEPNames(a_JobId,'TempDistributor') = 'N' and
        extension.Pkg_Relutils.StoredRelExistsViaEPNames(a_JobId,'TempDistributorPermit') = 'N' then
      t_List.Msg(t_List.Msg.count() + 1) := 'At least 1 Distributor is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Distributors';
    end if;

    --Check if there are any required distributor details that are blank.
    for i in (
        select d.LicenseNumber LicenseNumber
          from query.j_abc_prapplication pr
          join api.relationships r
            on r.FromObjectId = pr.ObjectId
          join query.o_abc_distributor d
            on d.ObjectId = r.ToObjectId
        where d.LicenseNumber is null
          and pr.JobId = a_JobId
        ) loop
      if i.LicenseNumber is null and t_LicenseNumberIsNull is null then
        t_List.Msg(t_List.Msg.count() + 1) := 'Distributor license number is required.';
        t_List.Pane(t_List.Pane.count() + 1) := 'Distributors';
        t_LicenseNumberIsNull := 1;
      else
        exit;
      end if;
    end loop;

    for i in (
        select d.Licensee Licensee
          from query.j_abc_prapplication pr
          join api.relationships r
            on r.FromObjectId = pr.ObjectId
          join query.o_abc_distributor d
            on d.ObjectId = r.ToObjectId
        where d.Licensee is null
          and pr.JobId = a_JobId
        ) loop
      if i.Licensee is null and t_LicenseeIsNull is null then
        t_List.Msg(t_List.Msg.count() + 1) := 'Distributor licensee is required.';
        t_List.Pane(t_List.Pane.count() + 1) := 'Distributors';
        t_LicenseeIsNull := 1;
      else
        exit;
      end if;
    end loop;

    select count (*)
    into t_LicRelCount
      from query.j_Abc_Prapplication pr
      join api.Relationships r
        on r.FromObjectId = pr.ObjectId
      join query.o_Abc_Distributorpermit d
        on d.ObjectId = r.ToObjectId
    where d.PermitNumber is null
      and pr.JobId = a_JobId;

    if t_LicRelCount > 0 then
      t_List.Msg(t_List.Msg.count() + 1) := 'Distributor permit number is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Distributors';
    end if;

    select count (*)
    into t_LicRelCount
      from query.j_Abc_Prapplication pr
      join api.Relationships r
        on r.FromObjectId = pr.ObjectId
      join query.o_Abc_Distributorpermit d
        on d.ObjectId = r.ToObjectId
    where d.Permittee is null
      and pr.JobId = a_JobId;

    if t_LicRelCount > 0 then
      t_List.Msg(t_List.Msg.count() + 1) := 'Distributor permittee name is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Distributors';
    end if;

    t_ProductIds := extension.pkg_objectquery.RelatedObjects(a_JobId,'Product');

    -- Questions & Documents
    -- Questions are done by Pane Mandatory and may be circumventable
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Documents from Document Grid
    for i in (
        select pa.DocumentTypeId DocumentTypeObjectId
        from query.r_ABC_OLPRAppDocumentType pa
        where pa.PRApplicationId = a_JobId
          and (pa.Mandatory = 'Y' or pa.QuestionMandatory = 'Y')
          and pa.DocumentTypeId not in (
              select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
              from query.r_OLPRApplicationElectronicDoc rr
              where rr.OLPRApplicationId = a_JobId)
    ) loop
      t_DocRequired := api.pkg_columnquery.Value(i.documenttypeobjectid, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    -- Documents
    for z in (
        select r.DocTypeId
        from query.r_ABC_PRAppResponseDocType r
        where r.PRApplicationId = a_JobId
          and r.Mandatory = 'Y'
          and r.DocTypeId not in (
              select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
              from query.r_OLPRApplicationElectronicDoc rr
              where rr.OLPRApplicationId = a_JobId)
        union
        select pr.DocTypeId
        from query.r_ABC_PRAppResponseDocType pr
        where pr.PRApplicationId = a_JobId
        and (pr.Mandatory = 'Y' or pr.QuestionMandatory = 'Y')
        and pr.DocTypeId not in (
            select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
            from query.r_OLPRApplicationElectronicDoc rr
            where rr.OLPRApplicationId = a_JobId)
    ) loop
      t_DocRequired := api.pkg_columnquery.Value(z.DocTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired
        || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;

  end OnlineErrorListPR;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPRRenewal() -- PUBLIC
   *   Used to determine which errors to raise on errors pane on public Product
   * Renewal job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPRRenewal(
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_DocRequired                       varchar2(50);
    t_InvalidQuestionCount              number;
    t_List                              udt_ErrorList;
    t_ProductXrefIds                    udt_ObjectList;
    t_ProductXrefGroupName              varchar2(100);
  begin
    t_ProductXrefIds := api.pkg_search.ObjectsByRelColumnValue('o_ABC_ProductXref', 'ProductRenewal', 'ObjectId', a_JobID);

    -- Check that Product New Type is not none
    for a in 1..t_ProductXrefIds.count() loop
      if api.pkg_columnquery.Value(t_ProductXrefIds(a), 'NewType') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Before you go on, you must supply a product type.';
        if api.pkg_columnquery.Value(a_JobId, 'IsHighVolume') = 'Y' then
          t_List.Presentation(t_List.Pane.count + 1) := 'TabbedPresentation';
          t_ProductXrefGroupName := api.pkg_columnquery.Value(t_ProductXrefIds(a), 'XrefGroupNumber');
          case t_ProductXrefGroupName
            when 'XrefGroup1' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume500';
            when 'XrefGroup2' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume1000';
            when 'XrefGroup3' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume1500';
            when 'XrefGroup4' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume2000';
            when 'XrefGroup5' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume2500';
            when 'XrefGroup6' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume3000';
            when 'XrefGroup7' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume3500';
            when 'XrefGroup8' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume4000';
            when 'XrefGroup9' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume4500';
            when 'XrefGroup10' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume5000';
            when 'XrefGroup11' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume5500';
            when 'XrefGroup12' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume6000';
            when 'XrefGroup13' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume6500';
            when 'XrefGroup14' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume7000';
            when 'XrefGroup15' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume7500';
            when 'XrefGroup16' then t_List.Pane(t_List.Pane.count + 1) := 'HighVolume8000';
            else t_List.Pane(t_List.Pane.count + 1) := 'ProductSearch';
          end case;
        else
          t_List.Pane(t_List.Pane.count + 1) := 'LowVolume';
        end if;
        exit;
      end if;
    end loop;

    -- Questions & Documents
    -- Questions are done by Pane Mandatory and may be circumventable
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Documents from Document Grid
    for i in (
        select pa.DocumentTypeId DocumentTypeObjectId
        from query.r_ABC_PRRenewalDocumentType pa
        where pa.PRRenewalId = a_JobId
          and (pa.Mandatory = 'Y' or pa.QuestionMandatory = 'Y')
          and pa.DocumentTypeId not in (
              select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
              from query.r_OLPRRenewalElectronicDoc rr
              where rr.OLPRRenewalJobId = a_JobId)
    ) loop
      t_DocRequired := api.pkg_columnquery.Value(i.documenttypeobjectid, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    -- Documents
    for z in (
        select r.DocTypeId
        from query.r_ABC_PRRenewalResponseDocType r
        where r.PRRenewalId = a_JobId
          and r.Mandatory = 'Y'
          and r.DocTypeId not in (
              select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
              from query.r_OLPRRenewalElectronicDoc rr
              where rr.OLPRRenewalJobId = a_JobId)
        union
        select pr.DocTypeId
        from query.r_ABC_PRRenewalResponseDocType pr
        where pr.PRRenewalId = a_JobId
        and (pr.Mandatory = 'Y' or pr.QuestionMandatory = 'Y')
        and pr.DocTypeId not in (
            select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
            from query.r_OLPRRenewalElectronicDoc rr
            where rr.OLPRRenewalJobId = a_JobId)
    ) loop
      t_DocRequired := api.pkg_columnquery.Value(z.DocTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired
        || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;

  end OnlineErrorListPRRenewal;

 /*---------------------------------------------------------------------------
   * OnlineErrorListPRAmend()
   * Used to determine which errors to raise on errors pane on public Product
   * Registration Amendment job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPRAmend (
    a_JobId                        udt_Id
  ) return udt_ErrorList is
    t_List                         udt_ErrorList;
    t_LicenseNumberIsNull          number;
    t_LicenseeIsNull               number;
    t_LicRelCount                  number;
    t_AmendType                    varchar2(30);
    t_DocRequired                  varchar2(50);
    t_InvalidQuestionCount         number;
  begin
   t_AmendType := api.pkg_columnquery.value(a_JobId, 'ProductAmendmentType');
   --Check if there are any products associated to the job.

   if extension.pkg_RelUtils.StoredRelExistsViaEPNames(a_JobId,'ProductEndPoint') = 'N' then
     t_List.Msg(t_List.Msg.count + 1) := 'Products are required.';
     t_List.Pane(t_List.Pane.count + 1) := 'Products';
   end if;

   if t_AmendType = 'Adjust Vintages'
       and api.pkg_columnquery.value(a_JobId, 'VintagesChanged') != 'Y' then
     t_List.Msg(t_List.Msg.count + 1) := 'No Vintages have been updated or requested.';
     t_List.Pane(t_List.Pane.count + 1) := 'Contact';
   end if;

    -- validate questions page
    -- Questions are done by Pane Mandatory due to being Python. There was a
    -- loophole to circumvent the pane mandatory, so we need it here as well
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- validate distributor page
   if t_AmendType = 'Add Distributor' then
     --Check if there are any distributors associated to the job.

     if extension.pkg_RelUtils.StoredRelExistsViaEPNames(a_JobId,'Distributor') = 'N' and
        extension.Pkg_Relutils.StoredRelExistsViaEPNames(a_JobId,'DistributorPermit') = 'N' then
        t_List.Msg(t_List.Msg.count + 1) := 'At least 1 Distributor is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Distributors';
     end if;

      --Check if there are any required distributor details that are blank.

      for i in (select d.LicenseNumber LicenseNumber
          from query.j_abc_pramendment pr
          join api.relationships r on r.FromObjectId = pr.ObjectId
          join query.o_abc_distributor d on d.ObjectId = r.ToObjectId
         where d.LicenseNumber is null
           and pr.JobId = a_JobId) loop

          if i.LicenseNumber is null and t_LicenseNumberIsNull is null then
            t_List.Msg(t_List.Msg.count + 1) := 'Distributor license number is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'Distributors';
            t_LicenseNumberIsNull := 1;
           else
            exit;
          end if;
         end loop;

      for i in (select d.Licensee Licensee
          from query.j_abc_pramendment pr
          join api.relationships r on r.FromObjectId = pr.ObjectId
          join query.o_abc_distributor d on d.ObjectId = r.ToObjectId
         where d.Licensee is null
           and pr.JobId = a_JobId) loop

          if i.Licensee is null and t_LicenseeIsNull is null then
            t_List.Msg(t_List.Msg.count + 1) := 'Distributor licensee is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'Distributors';
            t_LicenseeIsNull := 1;
           else
            exit;
          end if;
         end loop;

        select count (*)
          into t_LicRelCount
          from query.j_abc_pramendment pr
          join api.relationships r on r.FromObjectId = pr.ObjectId
          join query.o_Abc_Distributorpermit d on d.ObjectId = r.ToObjectId
         where d.PermitNumber is null
           and pr.JobId = a_JobId;
        if t_LicRelCount > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'Distributor permit number is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Distributors';
        end if;
        select count (*)
          into t_LicRelCount
          from query.j_abc_pramendment pr
          join api.relationships r on r.FromObjectId = pr.ObjectId
          join query.o_Abc_Distributorpermit d on d.ObjectId = r.ToObjectId
         where d.Permittee is null
           and pr.JobId = a_JobId;
        if t_LicRelCount > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'Distributor permittee is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Distributors';
        end if;
    end if;

    -- validate documents page
    for a in (select r.DocTypeId
              from query.r_ABC_PRAmendResponseDocType r
              where r.PRAmendAppId = a_JobId
                and (r.Mandatory = 'Y' or r.QuestionMandatory = 'Y')
                and r.DocTypeId not in
                  (select api.pkg_ColumnQuery.NumericValue(rr.OnlineDocumentId,
                       'DocumentTypeObjectId')
                   from query.r_PRAmendmentOnlineDocument rr
                   where rr.PRAmendmentJobId = a_JobId)
             ) loop
      t_DocRequired := api.pkg_columnquery.Value(a.DocTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired ||
          '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    for a in (select r.OnlineDocTypeId
              from query.r_ABC_PRAmendAppDocumentType r
              where r.PRAmendAppId = a_JobId
                and (r.Mandatory = 'Y' or r.QuestionMandatory = 'Y')
                and r.OnlineDocTypeId not in
                  (select api.pkg_ColumnQuery.NumericValue(rr.OnlineDocumentId,
                       'DocumentTypeObjectId')
                   from query.r_PRAmendmentOnlineDocument rr
                   where rr.PRAmendmentJobId = a_JobId)
             ) loop
      t_DocRequired := api.pkg_columnquery.Value(a.Onlinedoctypeid, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired ||
          '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;
  end OnlineErrorListPRAmend;

  /*---------------------------------------------------------------------------
   * CheckSolicitorLEDetails() -- PRIVATE
   * Common code for checking the Solicitor legal entity details on the
   * Permit Application (New Legal Entity only)
   *-------------------------------------------------------------------------*/
  function CheckSolicitorLEDetails (
    a_List               udt_ErrorList,
    a_JobId              udt_ID
  ) return udt_ErrorList is
    t_List               udt_ErrorList;
    t_JobDefName         varchar2(100) := api.pkg_columnQuery.Value(a_JobId, 'ObjectDefName');
    t_PaneName           varchar2(20);
  begin
    t_List := a_List;

    t_PaneName := 'Solicitor';

    -- Contact Details
    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorFirstName') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Solicitor First Name is required.';
       t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorLastName') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Last Name is required.';
       t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolBirthDateEncrypted') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Birth Date is required.';
       t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolSSNTIN') is null then
      t_List.Msg(t_List.Msg.count + 1) := 'Solicitor SSN/TIN is required.';
      t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    else
      --Make sure the value entered for SSN is exactly 9 digits with no special characters
      if regexp_replace(abc.pkg_abc_encryption.decrypt(api.pkg_columnquery.value(a_JobId, 'OnlineSolSSNTINEncrypted')), '[0-9]*') is not null
         or length(abc.pkg_abc_encryption.decrypt(api.pkg_columnquery.value(a_JobId, 'OnlineSolSSNTINEncrypted'))) != 9 then
        t_List.Msg(t_List.Msg.count + 1) := 'Solicitor SSN/TIN/EIN must be a 9 digit number without any dashes, slashes or spaces, e.g. 123456789.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlSolDriversLicense') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Drivers License is required.';
       t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorContactPhone') is null then
     t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Business Phone is required.';
     t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorContactMethod') is null then
      t_List.Msg(t_List.Msg.count + 1) := 'Preferred Contact Method is required for the Solicitor.';
      t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if (api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorPhysicalAddress') is null or api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorPhysicalAddress') = '(None)')
      and (api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorMailingAddress') is null or api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorMailingAddress') = '(None)') then
        t_List.Msg(t_List.Msg.count + 1) := 'Either a Solicitor Mailing Address or Solicitor Physical Address is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorContactMethod') = 'Mobile Phone'
      and api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorFax') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Mobile Phone is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorContactMethod') = 'Home Phone'
      and api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorAltPhoneNumber') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Home Phone is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorContactMethod') = 'Email'
      and api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorEmail') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Email is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorSameAsMailing') = 'Y'
      and (api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorMailingAddress') is null or api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorMailingAddress') = '(None)') then
         t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Mailing Address is required when the "Same As" checkbox is selected.';
         t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    return t_List;
  end CheckSolicitorLEDetails;
/*
  /*---------------------------------------------------------------------------
   * CheckOfficerLEDetails() -- PRIVATE
   * Check Co-Op Officer LE Details
   * Common code for checking the Co-Op Officer legal entity details
   * on the Permit Application (New or existing Legal Entity only)
   *-------------------------------------------------------------------------*-/
  function CheckOfficerLEDetails (
    a_List               udt_ErrorList,
    a_JobId              udt_ID
  ) return udt_ErrorList is
    t_List               udt_ErrorList;
    t_JobDefName         varchar2(100) := api.pkg_columnQuery.Value(a_JobId, 'ObjectDefName');
    t_PaneName           varchar2(20);
  begin
    t_List := a_List;

    t_PaneName := 'CoOp';

    -- non-Individual Licensee Type
    if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerTypeIsIndiv') <> 'Y' then
      if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerContactPhone') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Co-Op Officer Contact Phone Number is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      -- Incorporation details
      if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerLEStateOfInc') is null
        and api.pkg_columnquery.value(a_JobId, 'StateOfIncorporationMandatory') = 'Y' then
         t_List.Msg(t_List.Msg.count + 1) := 'State of Incorporation is required for the Co-Op Officer.';
         t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      elsif api.pkg_columnquery.value(a_JobId, 'OnlineOfficerLEStateOfInc') = 'NJ'
        and api.pkg_columnquery.value(a_JobId, 'OnlineOfficerLECharterRevoked') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Secretary of State question is required  for the Co-Op Officer.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      elsif api.pkg_columnquery.value(a_JobId, 'OnlineOfficerLEStateOfInc') != 'NJ'
        and api.pkg_columnquery.value(a_JobId, 'OnlineOfficerLEAuthBusiness') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Secretary of State question is required for the Co-Op Officer.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerCharterRevoked') = 'Yes'
        and api.pkg_columnquery.value(a_JobId, 'OnlineOfficerRevocationDate') is null and
        (api.pkg_columnquery.value(a_JobId, 'OnlineOfficerSuspensionBegin') is null or
         api.pkg_columnquery.value(a_JobId, 'OnlineOfficerSuspensionEnd') is null) then
         t_List.Msg(t_List.Msg.count + 1) := 'Date of Revocation or Beginning and Ending Date required for the Co-Op Officer.';
         t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

      -- Contact Details
      if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerContactName') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Officer Contact Name is required.';
         t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
      end if;

    end if; -- end non-Individual Licensee Type

   -- Individual Licensee Type
   if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerTypeIsIndiv') = 'Y' then
     if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerFirstName') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Officer First Name is required.';
       t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
     end if;

     if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerLastName') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'Officer Last Name is required.';
       t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
     end if;
   end if;  --end Indivudial Licensee Type

    -- Contact Details
    if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerContactPhone') is null then
     t_List.Msg(t_List.Msg.count + 1) := 'Officer Phone Number is required.';
     t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerContactMethod') is null then
      t_List.Msg(t_List.Msg.count + 1) := 'Preferred Contact Method is required for the Co-Op Officer.';
      t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerContactMethod') = 'Mail'
      and api.pkg_columnquery.value(a_JobId, 'OnlineOfficerMailingAddress') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Officer Mailing Address is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerContactMethod') = 'Fax'
      and api.pkg_columnquery.value(a_JobId, 'OnlineOfficerFax') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Officer Fax is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineSolicitorContactMethod') = 'Email'
      and api.pkg_columnquery.value(a_JobId, 'OnlineOfficerEmail') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Officer Email is required.';
        t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    if api.pkg_columnquery.value(a_JobId, 'OnlineOfficerSameAsMailing') = 'Y'
      and api.pkg_columnquery.value(a_JobId, 'OnlineOfficerMailingAddress') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Officer Mailing Address is required when the "Same As" checkbox is selected.';
         t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
    end if;

    return t_List;
  end CheckOfficerLEDetails;
  */

  /*---------------------------------------------------------------------------
   * OnlineErrorListPermit() -- PUBLIC
   *   Used to determine which errors to raise on the errors pane of the public
   * Permit Application job
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPermit (
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_CheckInsigniaPermits              number;
    t_DateList                          udt_DateList;
    t_DocRequired                       varchar2(50);
    t_EndTimeAMPM                       varchar2(2);
    t_EndTimeError                      varchar2(1) := 'N';
    t_EndTimeHours                      number;
    t_EndTimeMin                        number;
    t_EventLeadDays                     number
        := api.pkg_columnquery.Value(api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeObjectId'),
        'EventPermitLeadDays');
    t_InvalidQuestionCount              number;
    t_LicenseeIsNull                    number;
    t_LicenseNumberIsNull               number;
    t_List                              udt_ErrorList;
    t_MaximumEventPermitDays            number;
    t_MaximumPermitsPerLE               varchar2(4000);
    t_Members                           number;
    t_MinDate                           date;
    t_OnlineLEObjectId                  udt_Id;
    t_OnlinePermitTypeObjectId          udt_id
        := api.pkg_columnquery.NumericValue(a_JobId, 'OnlinePermitTypeObjectId');
    t_Permit                            varchar2(50);
    t_PermitsPerPermitteeCount          number;
    t_PermitTypeName                    varchar2(50);
    t_Products                          number;
    t_RainEndTimeError                  varchar2(1) := 'N';
    t_RainStartTimeError                varchar2(1) := 'N';
    t_Solicitors                        number;
    t_StartTimeAMPM                     varchar2(2);
    t_StartTimeError                    varchar2(1) := 'N';
    t_StartTimeHours                    number;
    t_StartTimeMin                      number;
    t_Vehicles                          number;
  begin

    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'A Permit Type is required.';
       t_List.Pane(t_List.Pane.count + 1) := 'Contact';
    end if;
    t_PermitTypeName := api.pkg_columnquery.Value(t_OnlinePermitTypeObjectId, 'Name');

    -- User Details
    t_List := CheckUserDetails(t_List, a_JobId);

    -- LE/Permittee Details
    t_List := CheckLEDetails(t_List, a_JobId);

    t_OnlineLEObjectId := api.pkg_ColumnQuery.NumericValue(a_JobId,
        'OnlineUseLegalEntityObjectId');
    t_MaximumPermitsPerLE := api.pkg_columnquery.NumericValue(t_OnlinePermitTypeObjectId,
        'MaximumPermitsPerLegalEntity');
    t_PermitsPerPermitteeCount := api.pkg_ColumnQuery.NumericValue(a_JobId,
        'PermitsByTypePerPermitteeCount');
    if t_PermitsPerPermitteeCount >= t_MaximumPermitsPerLE then
      t_List.Msg(t_List.Msg.count() + 1) := api.pkg_ColumnQuery.Value(t_OnlineLEObjectId,
          'dup_FormattedName') || ' has already been issued ' || t_PermitsPerPermitteeCount || ' '
          || t_PermitTypeName || ' permits for this calendar year.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
    end if;

    if api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresVehicle') = 'Y' and
       api.pkg_columnquery.Value(a_JobId, 'OnlineInsigniaPermitObjectId') is not null then
       null;
    else
-- Check TAP Permit if applicable
       if api.pkg_columnquery.Value(a_JobId, 'OnlineAllowTAPOverride') = 'Y' then
          if api.pkg_columnquery.Value(a_JobId, 'OnlineTAPPermitNumber') is not null and
             api.pkg_columnquery.Value(a_JobId, 'OnlineLicenseNumber')   is not null then
             t_List.Msg(t_List.Msg.count + 1) := 'Please enter either a License or a TAP Number, not both.';
             t_List.Pane(t_List.Pane.count + 1) := 'Contact';
          end if;
          if api.pkg_columnquery.Value(a_JobId, 'OnlineTAPPermitNumber') is null and
             api.pkg_columnquery.Value(a_JobId, 'OnlineLicenseNumber')   is null then
             t_List.Msg(t_List.Msg.count + 1) := 'Please enter either a License or a TAP Number.';
             t_List.Pane(t_List.Pane.count + 1) := 'Contact';
          end if;
       else
    -- License and/or permit
          if api.pkg_columnQuery.Value(a_JobId, 'OnlineLicenseRequired') = 'Required' then
             if api.pkg_columnquery.Value(a_JobId, 'OnlineDisableLicenseLookup') = 'N' and
                api.pkg_columnQuery.Value(a_JobId, 'OnlineLicenseObjectId') is null then
                t_List.Msg(t_List.Msg.count + 1) := 'License is required.';
                t_List.Pane(t_List.Pane.count + 1) := 'Contact';
             end if;
             if api.pkg_columnquery.Value(a_JobId, 'OnlineDisableLicenseLookup') = 'Y' and
                api.pkg_columnQuery.Value(a_JobId, 'OnlineLicenseNumberStored') is null then
                t_List.Msg(t_List.Msg.count + 1) := 'License is required.';
                t_List.Pane(t_List.Pane.count + 1) := 'Contact';
             end if;
          end if;
          if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitRequired') = 'Y' and
             api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitObjectId') is null then
             t_List.Msg(t_List.Msg.count + 1) := 'Permit is required.';
             t_List.Pane(t_List.Pane.count + 1) := 'Contact';
          end if;
       end if;
    end if;

    --Location
    if api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresLocation') = 'Y' then
      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLocationName') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Location Name is required.';
        t_List.Pane(t_List.Pane.count + 1) := (case when api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresEvents') = 'Y' then 'EventDetails' else 'Location' end);
      end if;

      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineEventLocationAddress') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Location Address is required.';
        t_List.Pane(t_List.Pane.count + 1) := (case when api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresEvents') = 'Y' then 'EventDetails' else 'Location' end);
      end if;
    end if;

    -- Event
    t_MaximumEventPermitDays := api.pkg_columnquery.NumericValue(t_OnlinePermitTypeObjectId, 'MaximumEventPermitDays');
    if api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresEvents') = 'Y' then
      if api.pkg_columnQuery.VALUE (a_JobId, 'OnlineEventDetails') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Please enter the Event Information.';
         t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineEventDateCount')<1 then
        t_List.Msg(t_List.Msg.count + 1) := 'At least one Event Date is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      else
        -- Check if Event Dates are greater than Maximum Event Permit Days
        if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineEventDateCount') >
            t_MaximumEventPermitDays then
          t_List.Msg(t_List.Msg.count + 1) := 'The number of Event Days exceeds the ' ||
              t_MaximumEventPermitDays || ' day maximum allowed for a ' || t_PermitTypeName|| '.';
          t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
        end if;
        for wRel in (select 1
                    from dual
                   where not exists (select api.pkg_columnQuery.Value(r.EventDateObjectID,'StartDate')
                                       from query.r_ABC_PermitAppEventDate r
                                      where r.PermitAppJobId = a_JobId)) loop
         t_List.Msg(t_List.Msg.count + 1) := 'A date for the Event Date is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
        end loop;
-- Check if Event Dates are consecutive
        select trunc(ed.StartDate)
          bulk collect into t_DateList
          from query.r_abc_permitappeventdate er
          join query.o_Abc_Eventdate ed on ed.ObjectId = er.EventDateObjectID
         where er.PermitAppJobId = a_JobId
         order by 1;
   --Start with the second earliest date
        for i in 2..t_DateList.count loop
     --if this iteration's date doesn't equal the previous iteration + 1, error.
           if t_DateList(i) != t_DateList(i-1)+1 then
              t_List.Msg(t_List.Msg.count + 1) := 'Event Dates must be consecutive.';
              t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
              exit;
           end if;
        end loop;
      end if;
      if api.pkg_columnQuery.VALUE (a_JobId, 'OnlineHasRainDate') is not null then
         if api.pkg_columnQuery.VALUE (a_JobId, 'OnlineEarliestRainDate') is null then
            t_List.Msg(t_List.Msg.count + 1) := 'Please enter a Date for the Rain Date.';
            t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
         end if;
      end if;
      if api.pkg_columnQuery.VALUE (a_JobId, 'OnlineRainDateAfterEventDate') is null then
         t_List.Msg(t_List.Msg.count + 1) := 'Rain date must be after the event date.';
         t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      select min(api.pkg_columnQuery.DateValue(r.EventDateObjectID,'StartDate'))
        into t_MinDate
        from query.r_ABC_PermitAppEventDate r
       where r.PermitAppJobId = a_JobId;
      if t_MinDate < (sysdate + t_EventLeadDays) then
         t_List.Msg(t_List.Msg.count + 1) := 'Please enter a Start Date that is at least ' || t_EventLeadDays || ' days after today.';
         t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      -- Check if Rain Dates are greater than Maximum Event Permit Days.
      if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineRainDateCount') >
          t_MaximumEventPermitDays then
        t_List.Msg(t_List.Msg.count + 1) := 'The number of Rain Days exceeds the ' ||
            t_MaximumEventPermitDays || ' day maximum allowed for a ' || t_PermitTypeName || '.';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
-- Check if Rain Dates are consecutive
      select trunc(ed.StartDate)
        bulk collect into t_DateList
        from query.r_ABC_PermitAppRainDate er
        join query.o_Abc_Eventdate ed on ed.ObjectId = er.RainDateObjectId
       where er.PermitAppJobId = a_JobId
       order by 1;
   --Start with the second earliest date
      for i in 2..t_DateList.count loop
     --if this iteration's date doesn't equal the previous iteration + 1, error.
         if t_DateList(i) != t_DateList(i-1)+1 then
            t_List.Msg(t_List.Msg.count + 1) := 'Rain Dates must be consecutive.';
            t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
            exit;
         end if;
      end loop;

      --All fields of the time are required
      --Loop through the event dates related to the job and
      --check to see if any of the time components have a
      --null value if they do raise an error
      for i in (select r.EventDateObjectID
                  from query.r_ABC_PermitAppEventDate r
                 where r.PermitAppJobId = a_JobId) loop
        select ed.StartTimeHours
              ,ed.StartTimeMinutes
              ,ed.StartTimeAMPM
              ,ed.EndTimeHours
              ,ed.EndTimeMinutes
              ,ed.EndTimeAMPM
          into t_StartTimeHours
              ,t_StartTimeMin
              ,t_StartTimeAMPM
              ,t_EndTimeHours
              ,t_EndTimeMin
              ,t_EndTimeAMPM
          from query.o_abc_eventdate ed
         where ed.ObjectId = i.eventdateobjectid;

         if t_StartTimeHours is null or
            t_StartTimeMin is null or
            t_StartTimeAMPM is null then

            t_StartTimeError := 'Y';

         end if;

         if t_EndTimeHours is null or
            t_EndTimeMin is null or
            t_EndTimeAMPM is null then

            t_EndTimeError := 'Y';

         end if;

      end loop;

      for i in (select r.RainDateObjectId
                  from query.r_ABC_PermitAppRainDate r
                 where r.PermitAppJobId = a_JobId) loop
        select ed.StartTimeHours
              ,ed.StartTimeMinutes
              ,ed.StartTimeAMPM
              ,ed.EndTimeHours
              ,ed.EndTimeMinutes
              ,ed.EndTimeAMPM
          into t_StartTimeHours
              ,t_StartTimeMin
              ,t_StartTimeAMPM
              ,t_EndTimeHours
              ,t_EndTimeMin
              ,t_EndTimeAMPM
          from query.o_abc_eventdate ed
         where ed.ObjectId = i.RainDateObjectId;

         if t_StartTimeHours is null or
            t_StartTimeMin is null or
            t_StartTimeAMPM is null then

            t_RainStartTimeError := 'Y';

         end if;

         if t_EndTimeHours is null or
            t_EndTimeMin is null or
            t_EndTimeAMPM is null then

            t_RainEndTimeError := 'Y';

         end if;

      end loop;

      if t_StartTimeError = 'Y' then
         t_List.Msg(t_List.Msg.count + 1) := 'Please enter hours, minutes and AM/PM for the event date start time';
         t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;

      if t_EndTimeError = 'Y' then
         t_List.Msg(t_List.Msg.count + 1) := 'Please enter hours, minutes and AM/PM for the event date end time';
         t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;

      if t_RainStartTimeError = 'Y' then
         t_List.Msg(t_List.Msg.count + 1) := 'Please enter hours, minutes and AM/PM for the rain date start time';
         t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;

      if t_RainEndTimeError = 'Y' then
         t_List.Msg(t_List.Msg.count + 1) := 'Please enter hours, minutes and AM/PM for the rain date end time';
         t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
    end if;

    --Vehicles
    if api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresVehicle') = 'Y' then
       if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineTotalNumberofVehicles') < 1 then
          t_List.Msg(t_List.Msg.count + 1) := 'At least one Vehicle is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
       else
          select count (*)
            into t_Vehicles
            from query.r_ABC_PermitAppVehicleOnline pavo
           where pavo.PermitApplicationObjectId = a_JobId
             and pavo.MakeModelYear is null;
          if t_Vehicles > 0 then
             t_List.Msg(t_List.Msg.count + 1) := 'Make / Model / Year is required.';
             t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
          end if;
          select count (*)
            into t_Vehicles
            from query.r_ABC_PermitAppVehicleOnline pavo
           where pavo.PermitApplicationObjectId = a_JobId
             and pavo.StateRegistration is null;
          if t_Vehicles > 0 then
             t_List.Msg(t_List.Msg.count + 1) := 'License Plate is required.';
             t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
          end if;
          select count (*)
            into t_Vehicles
            from query.r_ABC_PermitAppVehicleOnline pavo
           where pavo.PermitApplicationObjectId = a_JobId
             and pavo.StateOfRegistration is null;
          if t_Vehicles > 0 then
             t_List.Msg(t_List.Msg.count + 1) := 'State of Registration is required.';
             t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
          end if;
          select count (*)
            into t_Vehicles
            from query.r_ABC_PermitAppVehicleOnline pavo
           where pavo.PermitApplicationObjectId = a_JobId
             and (pavo.VIN is null or
                 length (pavo.VIN) < 5);
          if t_Vehicles > 0 then
             t_List.Msg(t_List.Msg.count + 1) := 'VIN Number is required.';
             t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
          end if;
          select count (*)
            into t_Vehicles
            from query.r_ABC_PermitAppVehicleOnline pavo
           where pavo.PermitApplicationObjectId = a_JobId
             and pavo.OwnedOrLeasedLimousine is null;
          if t_Vehicles > 0 then
             t_List.Msg(t_List.Msg.count + 1) := 'Please select if vehicle is Owned or Leased.';
             t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
          end if;
       end if;
       if api.pkg_columnQuery.Value(a_JobId, 'OnlineLicenseObjectId') is not null and
          api.pkg_columnQuery.Value(a_JobId, 'OnlineInsigniaPermitObjectId') is not null then
          begin
             select r.PermitObjectId
             into   t_Permit
             from   query.r_PermitLicense r
             where  r.LicenseObjectId = api.pkg_columnQuery.Value(a_JobId, 'OnlineLicenseObjectId')
             and    r.PermitObjectId = api.pkg_columnQuery.Value(a_JobId, 'OnlineInsigniaPermitObjectId');
          exception
             when no_data_found then
                t_List.Msg(t_List.Msg.count + 1) := 'The selected Permit does not belong to the selected License. Please select a new License or Permit.';
                t_List.Pane(t_List.Pane.count + 1) := 'Contact';
          end;
       end if;

    end if;

    --Co-Op
    if api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresCoOp') = 'Y' then
        -- Check Members
      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineTotalCoOpMembers') = 0 then
         t_List.Msg(t_List.Msg.count + 1) := 'At least one Co-Op Group member is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'CoOp';
      elsif
         api.pkg_ColumnQuery.Value(a_JobId, 'OnlineTotalCoOpMembers') > 33 then
         t_List.Msg(t_List.Msg.count + 1) := 'Your Application has more than the allowable 33 Members.';
         t_List.Pane(t_List.Pane.count + 1) := 'CoOp';
      else
         select count (*)
           into t_Members
           from query.r_ABC_PermitApplCoOpMember pacm
          where pacm.PermitApplicationObjectId = a_JobId
            and pacm.LicenseeName is null;
         if t_Members > 0 then
            t_List.Msg(t_List.Msg.count + 1) := 'Member Name is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'CoOp';
         end if;
         select count (*)
           into t_Members
           from query.r_ABC_PermitApplCoOpMember pacm
          where pacm.PermitApplicationObjectId = a_JobId
            and pacm.LicenseNumber is null;
         if t_Members > 0 then
            t_List.Msg(t_List.Msg.count + 1) := 'Member License is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'CoOp';
         end if;
      end if;
    end if;

    --Solicitor
    if api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresSolicitor') = 'Y' then
      t_List := CheckSolicitorLEDetails(t_List, a_JobId);
    end if;

    --Products
    if api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresProducts') = 'Y' then
      select count (*)
        into t_Products
        from query.r_ABC_PermitApplProductOnline papo
       where papo.PermitApplicationObjectId = a_JobId;
      if t_Products < 1 then
         t_List.Msg(t_List.Msg.count + 1) := 'At least one Product is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'Products';
      else
         select count (*)
           into t_Products
           from query.r_ABC_PermitApplProductOnline papo
          where papo.PermitApplicationObjectId = a_JobId
            and papo.ProductDescription is null;
         if t_Products > 0 then
            t_List.Msg(t_List.Msg.count + 1) := 'Product Description is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'Products';
         end if;
         select count (*)
           into t_Products
           from query.r_ABC_PermitApplProductOnline papo
          where papo.PermitApplicationObjectId = a_JobId
            and papo.ProductNumber is null;
         if t_Products > 0 then
            t_List.Msg(t_List.Msg.count + 1) := 'Product Registration Number is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'Products';
         end if;
         select count (*)
           into t_Products
           from query.r_ABC_PermitApplProductOnline papo
          where papo.PermitApplicationObjectId = a_JobId
            and (papo.NumberOfCasesAffected is null or
                 papo.NumberOfCasesAffected = 0);
         if t_Products > 0 then
            t_List.Msg(t_List.Msg.count + 1) := 'Number of Cases is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'Products';
         end if;
         select count (*)
           into t_Products
           from query.r_ABC_PermitApplProductOnline papo
          where papo.PermitApplicationObjectId = a_JobId
            and (papo.PricePerCase is null or
                 papo.PricePerCase = 0);
         if t_Products > 0 then
            t_List.Msg(t_List.Msg.count + 1) := 'Price per Case is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'Products';
         end if;
      end if;
    end if;

    -- Permit Type Details
    -- Seller's License
    if api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresSellerLicense') = 'Y' and
          api.pkg_columnQuery.Value (a_JobId, 'OnlineAllowSellerTapOverride') = 'Y' then
      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineSellersLicenseNumber') is null and
          api.pkg_columnQuery.Value (a_JobId, 'OnlineSellersPermitNumber') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'A '||api.pkg_columnQuery.Value (a_JobId, 'LicenseTapInformationLabel')||'''s License Number or '||
            api.pkg_columnQuery.Value (a_JobId, 'LicenseTapInformationLabel')||'''s Permit Number is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
      end if;
    elsif api.pkg_columnQuery.Value(a_JobId, 'OnlineRequiresSellerLicense') = 'Y' and
        api.pkg_columnQuery.Value (a_JobId, 'OnlineAllowSellerTapOverride') = 'N' then
      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineSellersLicenseNumber') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A '||api.pkg_columnQuery.Value (a_JobId, 'LicenseTapInformationLabel')||'''s License Number is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;

    -- License Transfer Date
    if api.pkg_columnQuery.Value (a_JobId, 'OnlineRequireLicenseTrnsfrDate') = 'Y' and
       api.pkg_ColumnQuery.Value(a_JobId, 'OnlineEffecDateLicenseTransfer') is null then
       t_List.Msg(t_List.Msg.count + 1) := 'A date for the License Transfer is required.';
       t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
    end if;
    -- Blanket Employment
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'BE' then
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineNumberOfIndividuals') is null or
          api.pkg_ColumnQuery.Value(a_JobId, 'OnlineNumberOfIndividuals') = 0 then
          t_List.Msg(t_List.Msg.count + 1) := 'At least one Individual is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Bulk Sale
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'BSP' then
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePurchasingAlcInventory') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'Please select if you are purchasing Alcoholic Inventory.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Food & Pharmaceutical
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'FP' then
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineNumberOfGallons') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A gallonage is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineFPProductDescription') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A product description is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Social Affair
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'SA' then
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineCivicReligOrEduc') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'Please answer the question if the Organization is Religious, Civic or Educational.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Permit Term
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') in ('AI','BRW','WN','TAP','TE') then
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePermitTermFrom') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A From Date is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePermitTermTo') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A To Date is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePermitTermTo') < api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePermitTermFrom') then
          t_List.Msg(t_List.Msg.count + 1) := 'The To Date cannot be prior to the From Date.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Personal Consumption
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'PC' then
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePersConsProdDescription') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A product description is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePersConsProdDestination') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A product destination is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePersConsProdOrigin') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A product origin is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Sale by Judgement
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'PS' then
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineSaleByJudgementAt') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A Sale At location is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineSaleByJudgementBy') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A Sale By person is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Consumer Tasting - Supplier
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'CTS' then
       if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineTotalNumberOfRepresentat') = 0 then
          t_List.Msg(t_List.Msg.count + 1) := 'At least one Representative is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       select count (*)
         into t_Solicitors
         from query.r_ABC_PermitAppCTSRepOnline paco
        where paco.PermitApplicationObjectId = a_JobId
          and paco.Name is null;
       if t_Solicitors > 0 then
          t_List.Msg(t_List.Msg.count + 1) := 'Representative Name is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Merchandising Show
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'MSO' then
       if api.pkg_columnQuery.VALUE (a_JobId, 'OnlineTTBPermitNumber') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'A Federal Basic Permit Number is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Marketing Agent
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'MA' then
       if api.pkg_columnQuery.VALUE (a_JobId, 'OnlineMAPromoterCompanyName') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'The Company Name for the Promoter is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       if api.pkg_columnQuery.VALUE (a_JobId, 'OnlineMAPromoterAddress') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'The Address for the Promoter is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- Consumer Tasting - Wholesale
    if api.pkg_columnQuery.Value(a_JobId, 'OnlinePermitTypeCode') = 'CTW' then
       if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineNumberOfSolicitors') = 0 and
          api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineNumberOfOwners') = 0 then
             t_List.Msg(t_List.Msg.count + 1) := 'At least one Solicitor or one Owner is required.';
             t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       select count (*)
         into t_Solicitors
         from query.r_ABC_PermitAppSolicitorOnline paso
        where paso.PermitApplicationObjectId = a_JobId
          and paso.SolicitorNumber is null;
       if t_Solicitors > 0 then
          t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Number is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       select count (*)
         into t_Solicitors
         from query.r_ABC_PermitAppSolicitorOnline paso
        where paso.PermitApplicationObjectId = a_JobId
          and paso.SolicitorName is null;
       if t_Solicitors > 0 then
          t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Name is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
       select count (*)
         into t_Solicitors
         from query.r_ABC_PermitAppOwnerOnline paoo
        where paoo.PermitApplicationObjectId = a_JobId
          and paoo.OwnerName is null;
       if t_Solicitors > 0 then
          t_List.Msg(t_List.Msg.count + 1) := 'Owner Name is required.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
       end if;
    end if;
    -- County / Municipality
    if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineMuniReviewRequired') = 'Y'
        or api.pkg_columnQuery.Value(a_JobId, 'OnlineNotifyMunicipality') = 'Y'
        or api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePoliceReviewRequired') = 'Y'
        or api.pkg_ColumnQuery.Value(a_JobId, 'OnlineNotifyPolice') = 'Y' then
      if api.pkg_columnQuery.Value(a_JobId, 'OnlineCounty') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Please select a County.';
        t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
      else
        if api.pkg_columnQuery.Value(a_JobId, 'OnlineMunicipality') is null then
          t_List.Msg(t_List.Msg.count + 1) := 'Please select a Municipality.';
          t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
        end if;
      end if;
      if api.pkg_columnQuery.Value(a_JobId, 'OnlineCheckCountyAndMunicipali') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'The selected Municipality does not belong to the' ||
            ' selected County. Please select a new Municipality.';
        t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
      end if;
    end if;
    -- Sampling or Display
    if api.pkg_columnquery.Value(a_JobId, 'OnlinePermitTypeCode') = 'SP' then
      select count (*)
        into t_Products
        from query.r_ABC_PermitApplProductOnline papo
       where papo.PermitApplicationObjectId = a_JobId;
      if t_Products < 1 then
         t_List.Msg(t_List.Msg.count + 1) := 'At least one Product is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
      else
         select count (*)
           into t_Products
           from query.r_ABC_PermitApplProductOnline papo
          where papo.PermitApplicationObjectId = a_JobId
            and papo.ProductDescription is null;
         if t_Products > 0 then
            t_List.Msg(t_List.Msg.count + 1) := 'Product Name is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
         end if;
         select count (*)
           into t_Products
           from query.r_ABC_PermitApplProductOnline papo
          where papo.PermitApplicationObjectId = a_JobId
            and papo.ProductNumber is null;
         if t_Products > 0 then
            t_List.Msg(t_List.Msg.count + 1) := 'Product Registration Number is required.';
            t_List.Pane(t_List.Pane.count + 1) := 'PermitTypeDetails';
         end if;
      end if;
    end if;

    --Questions & Documents
    -- Questions are done via Pane Mandatories on the job, but there was a loophole to circumvent the pane mandatories so we need it here as well.
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Documents from Document Grid on Permit Type
    for i in (select pa.OnlineDocTypeId DocumentTypeObjectId
              from query.r_ABC_PermitAppDocumentType pa
              where pa.PermitAppId = a_JobId
                and (pa.Mandatory = 'Y' or pa.QuestionMandatory = 'Y')
                and pa.OnlineDocTypeId not in
               (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                from query.r_OLNewPermitAppElectronicDoc rr
                where rr.OLNewPermitAppJobId = a_JobId)
             )
    loop
      t_DocRequired := api.pkg_columnquery.Value(i.documenttypeobjectid, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    -- Documents
    for z in (select r.DocTypeId
                from query.r_ABC_PermitAppResponseDocType r
               where r.PermitAppId = a_JobId
                 and r.Mandatory = 'Y'
                 and r.DocTypeId not in
                (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                  from query.r_OLNewPermitAppElectronicDoc rr
                 where rr.OLNewPermitAppJobId = a_JobId)
              union
              select pr.DocTypeId
              from query.r_ABC_PermitAppResponseDocType pr
              where pr.PermitAppId = a_jobid
                and (pr.Mandatory = 'Y' or pr.QuestionMandatory = 'Y')
                and pr.DocTypeId not in
               (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                from query.r_OLNewPermitAppElectronicDoc rr
                where rr.OLNewPermitAppJobId = a_JobId)
             )
    loop
      t_DocRequired := api.pkg_columnquery.Value(z.DocTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;
  end OnlineErrorListPermit;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPermitRenewal() -- PUBLIC
   *   Used to determine which errors to raise on the errors pane of the public
   * Permit Renewal job.
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPermitRenewal (
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_List                              udt_ErrorList;
    t_MaximumEventPermitDays            number;
    t_Members                           number;
    t_Vehicles                          number;
    t_DocRequired                       varchar2(50);
    t_RaiseErrorProductDescription      varchar2(1) := 'N';
    t_RaiseErrorProductNumber           varchar2(1) := 'N';
    t_PaneName                          varchar2(50);
    t_PermitTypeName                    varchar2(50);
    t_PermitTypeObjectId          udt_id :=
        api.pkg_columnquery.NumericValue(a_JobId, 'PermitTypeObjectId');
    t_StartTimeHours                    number;
    t_StartTimeMin                      number;
    t_StartTimeAMPM                     varchar2(2);
    t_EndTimeHours                      number;
    t_EndTimeMin                        number;
    t_EndTimeAMPM                       varchar2(2);
    t_StartTimeError                    varchar2(1) := 'N';
    t_EndTimeError                      varchar2(1) := 'N';
    t_RainStartTimeError                varchar2(1) := 'N';
    t_RainEndTimeError                  varchar2(1) := 'N';
    t_InvalidQuestionCount              number;
    t_DateList                          udt_DateList;
    t_Solicitors                        number;
  begin

    -- Permit Type Details
    -- Co-Op
    if api.pkg_columnQuery.Value(a_JobId, 'RequiresCoOp') = 'Y' then
      if api.pkg_ColumnQuery.NumericValue(a_JobId, 'TotalCoOpMembers') = 0 then
         t_List.Msg(t_List.Msg.count + 1) := 'At least one Co-Op Group member ' ||
             'is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'CoOp';
      elsif
         api.pkg_ColumnQuery.Value(a_JobId, 'TotalCoOpMembers') > 33 then
         t_List.Msg(t_List.Msg.count + 1) := 'Your Renewal has more than the ' ||
             'allowable 33 Members.';
         t_List.Pane(t_List.Pane.count + 1) := 'CoOp';
      else
         select count (*)
         into t_Members
         from query.r_ABC_PermitRenewalCoOpMember prcm
         join query.o_abc_coopmember cm
             on cm.ObjectId = prcm.CoOpMemberObjectId
         where prcm.PermitRenewalObjectId = a_JobId
           and cm.Licensee is null;
         if t_Members > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'Member Name is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'CoOp';
         end if;
         select count (*)
         into t_Members
         from query.r_ABC_PermitRenewalCoOpMember prcm
         join query.o_abc_coopmember cm
             on cm.ObjectId = prcm.CoOpMemberObjectId
         where prcm.PermitRenewalObjectId = a_JobId
           and cm.LicenseNumber is null;
         if t_Members > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'Member License is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'CoOp';
         end if;
      end if;
    end if;

    --Requires Events
    t_MaximumEventPermitDays := api.pkg_columnquery.NumericValue(t_PermitTypeObjectId, 'MaximumEventPermitDays');
    t_PermitTypeName := api.pkg_columnquery.Value(a_JobId, 'PermitType');
    if api.pkg_ColumnQuery.Value(a_JobId, 'PermitTypeRequiresEvents') = 'Y' then
      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineEventDetails') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Event Details is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLocationName') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Location name for the event is ' ||
            'required.';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      if extension.pkg_relutils.RelExists(a_JobId, 'OnlineEventLocationAddress') = 'N' then
        t_List.Msg(t_List.Msg.count + 1) := 'Location address for the event is ' ||
            'required.';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      if extension.pkg_relutils.RelExists(a_JobId, 'OnlineEventDate') = 'N' then
        t_List.Msg(t_List.Msg.count + 1) := 'Event dates are required.';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      else
        for ed in (select ed.EventDateObjectID
                   from query.r_ABC_PermitRenewalEventDate ed
                   where ed.PermitRenewalJobId = a_JobId
                  ) loop
          if api.pkg_ColumnQuery.DateValue(
                ed.eventdateobjectid, 'StartDate') < sysdate + 5 or
             api.pkg_ColumnQuery.DateValue(
                 ed.eventdateobjectid, 'StartDate') is null then
            t_List.Msg(t_List.Msg.count + 1) := 'Event start dates are required ' ||
                'and must be at least 5 days in the future.';
            t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
          end if;
        end loop;
         -- Check if Event Dates are greater than Maximum Event Permit Days
        if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineEventDateCount') > t_MaximumEventPermitDays then
          t_List.Msg(t_List.Msg.count + 1) := 'The number of Event Days exceeds the ' ||
              t_MaximumEventPermitDays || ' day maximum allowed for a ' || t_PermitTypeName|| '.';
          t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
        end if;
        -- Check if Event Dates are consecutive
        select trunc(ed.StartDate)
        bulk collect into t_DateList
        from query.r_ABC_PermitRenewalEventDate er
        join query.o_Abc_Eventdate ed
            on ed.ObjectId = er.EventDateObjectID
        where er.PermitRenewalJobId = a_JobId
        order by 1;
        -- Start with the second earliest date
        for i in 2..t_DateList.count loop
        -- If this iteration's date doesn't equal the previous iteration + 1, error.
          if t_DateList(i) != t_DateList(i-1)+1 then
            t_List.Msg(t_List.Msg.count + 1) := 'Event Dates must be consecutive.';
            t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
            exit;
          end if;
        end loop;
      end if;

      if extension.pkg_relutils.RelExists(a_JobId, 'OnlineRainDate') = 'Y' then
        for rd in (select ed.RainDateObjectId
                   from query.r_ABC_PermitRenewalRainDate ed
                   where ed.PermitAppJobId = a_JobId
                  ) loop
          if api.pkg_ColumnQuery.DateValue(rd.Raindateobjectid, 'StartDate')
                < sysdate + 5 or
             api.pkg_ColumnQuery.DateValue(rd.Raindateobjectid, 'StartDate')
                 is null then
              t_List.Msg(t_List.Msg.count + 1) := 'Rain start dates are ' ||
                  'required and must be at least 5 days in the future.';
              t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
          end if;
        end loop;
      end if;
      -- Check if Rain Dates are greater than Maximum Event Permit Days.
      if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineRainDateCount') >
           t_MaximumEventPermitDays then
        t_List.Msg(t_List.Msg.count + 1) := 'The number of Rain Days exceeds the ' ||
            t_MaximumEventPermitDays || ' day maximum allowed for a ' || t_PermitTypeName || '.';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      -- Check if Rain Dates are consecutive
      select trunc(ed.StartDate)
      bulk collect into t_DateList
      from query.r_ABC_PermitRenewalRainDate er
      join query.o_Abc_Eventdate ed
          on ed.ObjectId = er.RainDateObjectId
      where er.PermitAppJobId = a_JobId
      order by 1;
     -- Start with the second earliest date
      for i in 2..t_DateList.count loop
      -- If this iteration's date doesn't equal the previous iteration + 1, error.
        if t_DateList(i) != t_DateList(i-1)+1 then
          t_List.Msg(t_List.Msg.count + 1) := 'Rain Dates must be consecutive.';
          t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
          exit;
        end if;
      end loop;

      -- All fields of the time are required
      -- Loop through the event dates related to the job and check to see if
      -- any of the time components have a null value if they do raise an error
      for i in (select r.EventDateObjectID
                from query.r_ABC_PermitRenewalEventDate r
                where r.PermitRenewalJobId = a_JobId
              ) loop
        select
          ed.StartTimeHours,
          ed.StartTimeMinutes,
          ed.StartTimeAMPM,
          ed.EndTimeHours,
          ed.EndTimeMinutes,
          ed.EndTimeAMPM
        into
          t_StartTimeHours,
          t_StartTimeMin,
          t_StartTimeAMPM,
          t_EndTimeHours,
          t_EndTimeMin,
          t_EndTimeAMPM
        from query.o_abc_eventdate ed
        where ed.ObjectId = i.eventdateobjectid;

        if t_StartTimeHours is null or
           t_StartTimeMin is null or
           t_StartTimeAMPM is null then
          t_StartTimeError := 'Y';
        end if;

        if t_EndTimeHours is null or
           t_EndTimeMin is null or
           t_EndTimeAMPM is null then
          t_EndTimeError := 'Y';
        end if;
      end loop;

      for i in (select r.RainDateObjectId
                from query.r_ABC_PermitRenewalRainDate r
                where r.PermitAppJobId = a_JobId
               ) loop
        select
          ed.StartTimeHours,
          ed.StartTimeMinutes,
          ed.StartTimeAMPM,
          ed.EndTimeHours,
          ed.EndTimeMinutes,
          ed.EndTimeAMPM
        into
          t_StartTimeHours,
          t_StartTimeMin,
          t_StartTimeAMPM,
          t_EndTimeHours,
          t_EndTimeMin,
          t_EndTimeAMPM
        from query.o_abc_eventdate ed
        where ed.ObjectId = i.RainDateObjectId;
        if t_StartTimeHours is null or
           t_StartTimeMin is null or
           t_StartTimeAMPM is null then
          t_RainStartTimeError := 'Y';
        end if;
        if t_EndTimeHours is null or
           t_EndTimeMin is null or
           t_EndTimeAMPM is null then
          t_RainEndTimeError := 'Y';
        end if;
      end loop;

      if t_StartTimeError = 'Y' then
        t_List.Msg(t_List.Msg.count + 1) := 'Please enter hours, minutes and ' ||
            'AM/PM for the event date start time';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      if t_EndTimeError = 'Y' then
        t_List.Msg(t_List.Msg.count + 1) := 'Please enter hours, minutes and ' ||
            'AM/PM for the event date end time';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      if t_RainStartTimeError = 'Y' then
        t_List.Msg(t_List.Msg.count + 1) := 'Please enter hours, minutes and ' ||
            'AM/PM for the rain date start time';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
      if t_RainEndTimeError = 'Y' then
        t_List.Msg(t_List.Msg.count + 1) := 'Please enter hours, minutes and ' ||
            'AM/PM for the rain date end time';
        t_List.Pane(t_List.Pane.count + 1) := 'EventDetails';
      end if;
    end if;

    -- Requires Locations
    if api.pkg_ColumnQuery.Value(a_JobId, 'PermitTypeRequiresLocation') = 'Y' then
      if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLocationName') is null then
        t_List.Msg(t_List.Msg.count + 1) := 'Location name for the event is ' ||
            'required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Location';
      end if;
      if extension.pkg_relutils.RelExists(a_JobId, 'OnlineEventLocationAddress')
            = 'N' then
        t_List.Msg(t_List.Msg.count + 1) := 'Location address for the event is ' ||
            'required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Location';
      end if;
    end if;

    -- Requires Products
    if api.pkg_columnquery.Value(a_JobId, 'PermitTypeRequiresProducts') = 'Y' then
      if extension.pkg_relutils.RelExists(a_JobId, 'ProductOnline') = 'N' then
        t_List.Msg(t_List.Msg.count + 1) := 'At Least one product is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Products';
      else
        for i in (select
                    prod.ProductDescription,
                    prod.ProductNumber
                  from query.r_ABC_ProductPermitRenewal prod
                  where prod.PermitApplicationObjectId = a_JobId
                 ) loop
          if i.ProductDescription is null then
            t_RaiseErrorProductDescription := 'Y';
          end if;
          if i.ProductNumber is null then
            t_RaiseErrorProductNumber := 'Y';
          end if;
        end loop;
        if api.pkg_ColumnQuery.Value(a_JobId, 'PermitTypeCode') = 'SP' then
          t_PaneName := 'Renewal';
        else
          t_PaneName := 'Products';
        end if;
        if t_RaiseErrorProductDescription = 'Y' then
          t_List.Msg(t_List.Msg.count + 1) := 'Product description is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        end if;
        if t_RaiseErrorProductNumber = 'Y' then
          t_List.Msg(t_List.Msg.count + 1) := 'Product number is required.';
          t_List.Pane(t_List.Pane.count + 1) := t_PaneName;
        end if;
      end if;
    end if;

    --Vehicles
    if api.pkg_columnQuery.Value(a_JobId, 'RequiresVehicle') = 'Y' then
       if api.pkg_columnQuery.NumericValue(
             a_JobId, 'OnlineTotalNumberofVehicles') < 1 then
         t_List.Msg(t_List.Msg.count + 1) := 'At least one Vehicle is required.';
         t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
       else
         select count (*)
         into t_Vehicles
         from query.r_ABC_PermitRenewalVehicle pavo
         join query.o_abc_vehicle v
             on v.ObjectId = pavo.VehicleObjectId
         where pavo.PermitRenewalObjectId = a_JobId
           and v.MakeModelYear is null;
         if t_Vehicles > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'Make / Model / Year is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
         end if;
         select count (*)
         into t_Vehicles
         from query.r_ABC_PermitRenewalVehicle pavo
         join query.o_abc_vehicle v
             on v.ObjectId = pavo.VehicleObjectId
         where pavo.PermitRenewalObjectId = a_JobId
           and v.StateRegistration is null;
         if t_Vehicles > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'License Plate is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
         end if;
         select count (*)
         into t_Vehicles
         from query.r_ABC_PermitRenewalVehicle pavo
         join query.o_abc_vehicle v
             on v.ObjectId = pavo.VehicleObjectId
         where pavo.PermitRenewalObjectId = a_JobId
           and v.StateOfRegistration is null;
         if t_Vehicles > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'State of Registration is ' ||
               'required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
         end if;
         select count (*)
         into t_Vehicles
         from query.r_ABC_PermitRenewalVehicle pavo
         join query.o_abc_vehicle v
             on v.ObjectId = pavo.VehicleObjectId
         where pavo.PermitRenewalObjectId = a_JobId
           and (v.VIN is null or
                length (v.VIN) < 5);
         if t_Vehicles > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'VIN Number is required.';
           t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
         end if;
         select count (*)
         into t_Vehicles
         from query.r_ABC_PermitRenewalVehicle pavo
         join query.o_abc_vehicle v
             on v.ObjectId = pavo.VehicleObjectId
         where pavo.PermitRenewalObjectId = a_JobId
           and v.OwnedOrLeasedLimousine is null;
         if t_Vehicles > 0 then
           t_List.Msg(t_List.Msg.count + 1) := 'Please select if vehicle is ' ||
               'Owned or Leased.';
           t_List.Pane(t_List.Pane.count + 1) := 'Vehicles';
         end if;
       end if;
    end if;

    -- Consumer Tasting - Wholesale
    if api.pkg_columnQuery.Value(a_JobId, 'PermitTypeCode') = 'CTW' then
      if api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineNumberOfSolicitors')
             = 0 and
         api.pkg_columnQuery.NumericValue(a_JobId, 'OnlineNumberOfOwners')
             = 0 then
        t_List.Msg(t_List.Msg.count + 1) := 'At least one Solicitor or one Owner ' ||
            'is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Renewal';
      end if;
      select count (*)
      into t_Solicitors
      from query.r_ABC_PermitRenSolicitorOnline paso
      join query.o_ABC_SolicitorOnline so
          on so.ObjectId = paso.SolicitorObjectId
      where paso.PermitRenewalObjectId = a_JobId
        and so.SolicitorNumber is null;
      if t_Solicitors > 0 then
        t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Number is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Renewal';
      end if;
      select count (*)
      into t_Solicitors
      from query.r_ABC_PermitRenSolicitorOnline paso
      join query.o_ABC_SolicitorOnline so
          on so.ObjectId = paso.SolicitorObjectId
      where paso.PermitRenewalObjectId = a_JobId
        and so.SolicitorName is null;
      if t_Solicitors > 0 then
        t_List.Msg(t_List.Msg.count + 1) := 'Solicitor Name is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Renewal';
      end if;
      select count (*)
      into t_Solicitors
      from query.r_ABC_PermitRenewOwnerOnline paoo
      join query.o_Abc_OwnerOnline oo
          on oo.ObjectId = paoo.OwnerObjectId
      where paoo.PermitRenewalObjectId = a_JobId
        and oo.OwnerName is null;
      if t_Solicitors > 0 then
        t_List.Msg(t_List.Msg.count + 1) := 'Owner Name is required.';
        t_List.Pane(t_List.Pane.count + 1) := 'Renewal';
      end if;
    end if;

    -- Questions & Documents
    -- Questions are done by Pane Mandatory due to being Python. There was a
    -- loophole to circumvent the pane mandatory, so we need it here as well
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Documents from Questions
    for z in (select r.DocTypeId
              from query.r_ABC_PermitRnwResponseDocType r
              where r.PermitRnwId = a_JobId
                and (r.Mandatory = 'Y' or r.QuestionMandatory = 'Y')
                and r.DocTypeId not in
                   (select api.pkg_ColumnQuery.NumericValue(
                       rr.OLDocumentId, 'DocumentTypeObjectId')
                    from query.r_OLPermitRenewElectronicDoc rr
                    where rr.OLPermitRenewJobId = a_JobId)
             ) loop
      t_DocRequired := api.pkg_columnquery.Value(z.DocTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired ||
          '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    -- Documents from Document Grid on Permit Type
    for i in (select pa.OnlineDocTypeId DocumentTypeObjectId
              from query.r_ABC_PermitRenewDocumentType pa
              where pa.PermitRenewId = a_JobId
                and (pa.Mandatory = 'Y' or pa.QuestionMandatory = 'Y')
                and pa.OnlineDocTypeId not in
               (select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                from query.r_OLPermitRenewElectronicDoc rr
                where rr.OLPermitRenewJobId = a_JobId)
             )
    loop
      t_DocRequired := api.pkg_columnquery.Value(i.documenttypeobjectid, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;

  end OnlineErrorListPermitRenewal;

  /*---------------------------------------------------------------------------
   * OnlineErrorListCPL() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function OnlineErrorListCPL (
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_BeerCount                         number;
    t_ComboCount                        number;
    t_DocRequired                       varchar2(50);
    t_Initial                           boolean;
    t_LicenseCount                      number;
    t_List                              udt_ErrorList;
    t_PermitCount                       number;
    t_RIPCount                          number;
    t_SpreadCount                       number;
    t_WineSpiritCount                   number;

  begin

    -- Ensure at least 1 license or permit is added
    if api.pkg_ColumnQuery.Value(a_JobId, 'LicenseOrPermitSelected') = 'N' then
      t_List.Msg(t_List.Msg.count() + 1) := 'At least one license or permit is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
    else
      t_Initial := False;
      for x in (
          select t.ObjectId
          from
            table(
                api.pkg_SimpleSearch.CastableObjectsByIndex('j_ABC_CPLSubmissions',
                'dup_LicenseSubmissionPeriod', api.pkg_ColumnQuery.Value(a_JobId,
                'dup_LicenseSubmissionPeriod'))) t
            join api.Jobs s
                on s.JobId = t.ObjectId
          where s.JobStatus = 'COMP'
          ) loop
        if api.pkg_ColumnQuery.Value(x.ObjectId,'SubmissionType') = 'Initial' then
          t_Initial := true;
        end if;
      end loop;
      if api.pkg_ColumnQuery.Value(a_JobId, 'IsResubmissionOrAmendment') = 'Y' then
        if t_Initial = false then
          t_List.Msg(t_List.Msg.count() + 1) := 'There must be an Initial submission ' ||
              'for this License/TAP before a Resubmission or Amendment can be made.';
          t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
        end if;
      elsif t_Initial = true then
        t_List.Msg(t_List.Msg.count() + 1) := 'There is already an Initial Submission ' ||
            'for this License/TAP. A Resubmission or Amendment must be submitted instead.';
        t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
      end if;
    end if;

    -- Applicant Details
    if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLEContactName') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Applicant contact name is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
    end if;

    if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLEPreferredContactMethod') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Applicant Preferred Contact Method is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
    end if;

    if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLEPreferredContactMethod') = 'Mobile Phone'
        and api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLEFax') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Applicant Mobile Phone is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
    end if;

    if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLEPreferredContactMethod') = 'Home Phone'
        and api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLEAlternatePhone') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Applicant Home Phone is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
    end if;

    if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLEEmail') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Applicant Email is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
    end if;

    if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLEContactPhone') is null then
      t_List.Msg(t_List.Msg.count() + 1) := 'Applicant Business Phone is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Contact';
    end if;

    -- Ensure a CPL Spreadsheet has been attached
    select count(r.DocumentId)
    into t_SpreadCount
    from (
        select DocumentId, JobId
        from query.r_ABC_CPLOnlineSpreadsheet
        union
        select DocumentId, JobId
        from query.r_ABC_CPLSpreadsheet) r
    where r.JobId = a_JobId;

    if t_SpreadCount = 0 then
      t_List.Msg(t_List.Msg.count() + 1) := 'A price list spreadsheet is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Documents';
    end if;
    if t_SpreadCount > 1 then
       t_List.Msg(t_List.Msg.count() + 1) := 'Only one price list spreadsheet is allowed.';
       t_List.Pane(t_List.Pane.count() + 1) := 'Documents';
    end if;

    select count(*)
    into t_WineSpiritCount
    from (
        select DocumentId, JobId
        from query.r_ABC_CPLOnlineSpreadsheet
        union
        select DocumentId, JobId
        from query.r_ABC_CPLSpreadsheet) r
    where r.JobId = a_JobId
      and api.pkg_ColumnQuery.Value(r.DocumentId, 'CPLType') = 'Wine / Spirits';
    select count(*)
    into t_BeerCount
    from (
        select DocumentId, JobId
        from query.r_ABC_CPLOnlineSpreadsheet
        union
        select DocumentId, JobId
        from query.r_ABC_CPLSpreadsheet) r
    where r.JobId = a_JobId
      and api.pkg_ColumnQuery.Value(r.DocumentId, 'CPLType') = 'Beer';
    select count(*)
    into t_ComboCount
    from (select DocumentId, JobId
        from query.r_ABC_CPLOnlineSpreadsheet
        union
        select DocumentId, JobId
        from query.r_ABC_CPLSpreadsheet) r
    where r.JobId = a_JobId
      and api.pkg_ColumnQuery.Value(r.DocumentId, 'CPLType') = 'Combination Packs';
    select count(*)
    into t_RIPCount
    from (
        select DocumentId, JobId
        from query.r_ABC_CPLOnlineSpreadsheet
        union
        select DocumentId, JobId
        from query.r_ABC_CPLSpreadsheet) r
    where r.JobId = a_JobId
      and api.pkg_ColumnQuery.Value(r.DocumentId, 'CPLType') = 'Retailer Incentive Program';

    if t_WineSpiritCount > 1 then
      t_List.Msg(t_List.Msg.count() + 1) := 'Only one Wine / Spirits Spreadsheet may be uploaded.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Documents';
    end if;
    if t_BeerCount > 1 then
      t_List.Msg(t_List.Msg.count() + 1) := 'Only one Beer Spreadsheet may be uploaded.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Documents';
    end if;
    if t_ComboCount > 1 then
      t_List.Msg(t_List.Msg.count() + 1) := 'Only one Combo Spreadsheet may be uploaded.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Documents';
    end if;
    if t_RIPCount > 1 then
      t_List.Msg(t_List.Msg.count() + 1) := 'Only one RIP Spreadsheet may be uploaded.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Documents';
    end if;

    -- Ensure Mandatory Documents are uploaded
    for d in (
        select r.DocumentTypeObjectId DocOnLicType
        from query.r_ABC_CPLOnlineDocumentType r
        where r.SysSettingsObjectId = (
              select ObjectId
              from query.o_SystemSettings)
          and r.Mandatory = 'Y'
          and r.DocumentTypeObjectId not in (
              select api.pkg_ColumnQuery.NumericValue(rr.DocumentId, 'DocumentTypeObjectId')
              from query.r_ABC_OnlineCPLeDocument rr
              where rr.JobId = a_JobId)
        ) loop
      t_DocRequired := api.pkg_ColumnQuery.Value(d.DocOnLicType, 'Name');
      t_List.Msg(t_List.Msg.count() + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count() + 1) := 'Documents';
    end loop;

    return t_List;

  end OnlineErrorListCPL;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPetition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPetition (
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_List                              udt_ErrorList;
    t_DocRequired                       varchar2(50);
    t_MaxTerms                          number := 0;
    t_PetitionTypeId                    udt_Id;
  begin
    
    t_PetitionTypeId := api.pkg_columnquery.NumericValue(a_JobId, 'PetitionTypeId');

    if t_PetitionTypeId is null then
      t_List.Msg(t_List.Msg.count + 1) := 'A petition type must be selected.';
      t_List.Pane(t_List.Pane.count + 1) := 'Petition';
      
      -- All required items for petition are based on petition type so there is no need to do further
      -- checks until a Petition Type is supplied
      return t_List;
    end if;

    if nvl(api.pkg_columnquery.NumericValue(a_JobId, 'OnlineNumRequestedTerms'), 0) = 0 then
      t_List.Msg(t_List.Msg.count + 1) := 'At least one term must be added.';
      t_List.Pane(t_List.Pane.count + 1) := 'PetitionTerms';
    end if;

    t_MaxTerms := api.pkg_columnquery.NumericValue(t_PetitionTypeId, 'MaxTermsPerApplication');
    if api.pkg_columnquery.NumericValue(a_JobId, 'OnlineNumRequestedTerms') > t_MaxTerms then
      t_List.Msg(t_List.Msg.count + 1) := 'The number of Terms may not exeed ' || t_MaxTerms
          || ' for a ' || api.pkg_columnquery.Value(t_PetitionTypeId, 'Name') || ' petition.'
          || ' Please remove terms to submit your application.';
      t_List.Pane(t_List.Pane.count + 1) := 'PetitionTerms';
    end if;

    -- Check questions
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Ensure Mandatory Documents are uploaded
    for d in (
        select r.DocumentTypeId
        from query.r_ABC_PetitionAppDocumentType r
        where (r.Mandatory = 'Y' or r.QuestionMandatory = 'Y')
          and r.PetitionId = a_JobId
          and r.DocumentTypeId not in (
                  select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                  from query.r_OLPetitionElectronicDocument rr
                  where rr.PetitionId = a_JobId))
    loop
      t_DocRequired := api.pkg_columnquery.Value(d.DocumentTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;

  end OnlineErrorListPetition;

  /*---------------------------------------------------------------------------
   * OnlineErrorListPetitionAmend() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function OnlineErrorListPetitionAmend (
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_List                              udt_ErrorList;
    t_DocRequired                       varchar2(50);
    t_DraftJobCount                     number := 0;
    t_LicenseId                         udt_Id;
    t_PetitionAmendTypeId               udt_Id;
  begin
    
    t_PetitionAmendTypeId := api.pkg_columnquery.NumericValue(a_JobId, 'PetitionAmendmentTypeId');
    if t_PetitionAmendTypeId is null then
      t_List.Msg(t_List.Msg.count + 1) := 'An Amendment type must be selected.';
      t_List.Pane(t_List.Pane.count + 1) := 'Petition';
      
      -- All required items for petition are based on amendment type so there is no need to do further
      -- checks until an amendment Type is supplied
      return t_List;
    end if;

    t_LicenseId := api.pkg_columnquery.NumericValue(a_JobId, 'LicenseObjectId');
    select count(p.ObjectId)
    into t_DraftJobCount
    from 
      query.r_abc_LicensePetition r
      join query.j_ABC_Petition p
          on p.ObjectId = r.PetitionId
    where r.LicenseId = t_LicenseId
      and p.StatusName = 'NEW';

    if t_DraftJobCount > 0 then
      t_List.Msg(t_List.Msg.count + 1) := 'The selected license is not eligible for amendment ' 
          || 'because there are in progress draft petition applications.';
      t_List.Pane(t_List.Pane.count + 1) := 'License';
    end if;

    select count(p.ObjectId)
    into t_DraftJobCount
    from 
      query.r_ABC_LicensePetitionAmendment r
      join query.j_ABC_PetitionAmendment p
          on p.ObjectId = r.PetitionAmendmentId
    where r.LicenseId = t_LicenseId
      and p.StatusName = 'NEW'
      and p.ObjectId != a_JobId;

    if t_DraftJobCount > 0 then
      t_List.Msg(t_List.Msg.count + 1) := 'The selected license is not eligible for amendment ' 
          || 'because there is already a draft amendment for the license.';
      t_List.Pane(t_List.Pane.count + 1) := 'License';
    end if;

    -- Check questions
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Ensure Mandatory Documents are uploaded
    for d in (
        select r.DocumentTypeId
        from query.r_ABC_PetAmendOnlineDocType r
        where (r.Mandatory = 'Y' or r.QuestionMandatory = 'Y')
          and r.PetitionAmendmentId = a_JobId
          and r.DocumentTypeId not in (
                  select api.pkg_ColumnQuery.NumericValue(rr.OLDocumentId, 'DocumentTypeObjectId')
                  from query.r_ABC_PetitionAmendOnlineDoc rr
                  where rr.PetitionAmendmentId = a_JobId))
    loop
      t_DocRequired := api.pkg_columnquery.Value(d.DocumentTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired || '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;

  end OnlineErrorListPetitionAmend;

  /*---------------------------------------------------------------------------
   * OnlineErrorListRenewal() -- PUBLIC
   *   Currently used to call a list of document errors
   * Called from ErrorsInHTML to get the list of errors.
   *-------------------------------------------------------------------------*/
  function OnlineErrorListRenewal (
    a_JobId                             udt_Id
  ) return udt_ErrorList is
    t_List                              udt_ErrorList;
    t_DocRequired                       varchar2(50);
    t_InvalidQuestionCount              number;
  begin

    -- Ensure Mandatory Documents are uploaded
    for d in (select r.OnlineDocTypeId
              from query.r_ABC_RenewAppDocumentType r
              where r.Mandatory = 'Y'
                and r.RenewAppId = a_JobId
                and r.OnlineDocTypeId not in
                  (select api.pkg_ColumnQuery.NumericValue(rr.DocumentId,
                       'DocumentTypeObjectId')
                   from query.r_Abc_Onlinerenewdocument rr
                   where rr.RenewalId = a_JobId)
              union
              select ra.OnlineDocTypeId
              from query.r_ABC_RenewAppDocumentType ra
              where ra.RenewAppId = a_JobId
                and (ra.Mandatory = 'Y' or ra.QuestionMandatory = 'Y')
                and ra.OnlineDocTypeId not in
               (select api.pkg_ColumnQuery.NumericValue(rr.DocumentId,
                    'DocumentTypeObjectId')
                from query.r_Abc_Onlinerenewdocument rr
                where rr.RenewalId = a_JobId)
             ) loop
      t_DocRequired := api.pkg_columnquery.Value(d.Onlinedoctypeid, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired ||
          '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    -- Questions & Documents
    -- Questions are done by Pane Mandatory and may be circumventable
    t_List := CheckResponsesForJob(a_JobId, 'Response', t_List);

    -- Documents from Questions
    for z in (select r.DocTypeId
              from query.r_ABC_RenewResponseDocType r
              where r.RenewalAppId = a_JobId
                and r.Mandatory = 'Y'
                and r.DocTypeId not in
                   (select api.pkg_ColumnQuery.NumericValue(
                       rr.DocumentId, 'DocumentTypeObjectId')
                    from query.r_ABC_OnlineRenewDocument rr
                    where rr.RenewalId = a_JobId)
              union
                select re.DocTypeId
              from query.r_ABC_RenewResponseDocType re
              where re.RelationshipId = a_JobId
                and (re.Mandatory = 'Y' or re.QuestionMandatory = 'Y')
                and re.DocTypeId not in
                (select api.pkg_ColumnQuery.NumericValue(
                       rr.DocumentId, 'DocumentTypeObjectId')
                    from query.r_ABC_OnlineRenewDocument rr
                    where rr.RenewalId = a_JobId)
             ) loop
      t_DocRequired := api.pkg_columnquery.Value(z.DocTypeId, 'Name');
      t_List.Msg(t_List.Msg.count + 1) := 'A document of type "' || t_DocRequired ||
          '" is required.';
      t_List.Pane(t_List.Pane.count + 1) := 'Documents';
    end loop;

    return t_List;

  end OnlineErrorListRenewal;

end pkg_ABC_Errors;
/
