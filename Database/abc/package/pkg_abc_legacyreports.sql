create or replace package     pkg_ABC_LegacyReports is
 /**********************************************************************************
  *  LegacyABCsa104() -- Called by the rendering on the Excel tab of the  
  *  Legacy Financial Search in the ABC Internal system
  *  This procdure with the expetion of the call to LegacyABCsa104CX 
  *  and delete statements was provided by NJ-ABC and has been left as such.
  **********************************************************************************/
  procedure LegacyABCsa104(HOLD_DT_DEPOSIT_START DATE,
                           HOLD_DT_DEPOSIT_END   DATE,
                           HOLD_DT_REFUND_START  DATE,
                           HOLD_DT_REFUND_END    DATE,
                           a_SESSIONKEY                in varchar2);
 /**********************************************************************************
  *  LegacyABCsa104_fun() -- Called by the rendering on the Excel tab of the  
  *  Legacy Financial Search in the ABC Internal system
  *  This function was writtin to search for and provide a list of UDT_Objects to
  *  the Legacy Finacial Search so that an excel document can be generated.
  *  a_SessionKey is a Time.Time() call from python that is used to make each report unique
  **********************************************************************************/                           
  function LegacyABCsa104_fun(a_SessionKey in         varchar2)
    return api.udt_ObjectList;                         

end;
/
grant debug
on pkg_abc_legacyreports
to posseextensions;

grant execute
on pkg_abc_legacyreports
to posseextensions;

create or replace package body     pkg_ABC_LegacyReports is

  /**********************************************************************************
  * LegacyABCsa104CX() -- Called by LegacyABCsa104 
  *  This procedcure inserts a row into the abcta104_report table which is
  *  then registered as an external object to be returned to an excel report
  *  
  **********************************************************************************/
  
    
  procedure LegacyABCsa104CX(a_SessionKey varchar2)
    is
  
  t_SequenceHolder number;
    
  begin
      
  --Accounting Transaction Type insert    
  insert into abc11p.abcta104_report 
   select a.detl_amt, 
          case when a.grp_cd = 'PER' then a.detl_amt else 0 end, -- Per Amount
          case when a.grp_cd = 'LIC' then a.detl_amt else 0 end, -- Licnese Amount
          case when a.grp_cd = 'MIS' then a.detl_amt else 0 end, --Misc Amount
          (select cd.code || ' - ' || cd.cd_desc
            from abc11p.code_desc cd 
           where cd.code = a.trans_type_cd
             and cd.id = 'TR'), -- Accounting Transaction Type
          null,
          a_SessionKey--  --External Object Key  
   from abc11p.abcta104_sum a 
   where a.refund_ind = 'N'
   and a.adjust_ind is null;
   -- Refund Insert, This is the Refund Line Item
  insert into abc11p.abcta104_report
   select sum(a.detl_amt), 
          sum(case when a.grp_cd = 'PER' then a.detl_amt else 0 end), -- Per Amount
          sum(case when a.grp_cd = 'LIC' then a.detl_amt else 0 end), -- Licnese Amount
          sum(case when a.grp_cd = 'MIS' then a.detl_amt else 0 end), --Misc Amount
          'Refunds', -- Accounting Transaction Type
          null,
          a_SessionKey--  --External Object Key  
   from abc11p.abcta104_sum a 
   where a.refund_ind = 'Y'
   and a.adjust_ind is null
   group by'Refunds', -- Accounting Transaction Type
          null,
          a_SessionKey;
   
   
   for x in (select rowid 
               from abc11p.abcta104_report r
              where r.SessionKey = a_SessionKey) loop
     t_SequenceHolder :=    abc11p.abcta104_s.Nextval();      
     update abc11p.abcta104_report ar 
       set ar.legacykey = t_SequenceHolder
      where ar.rowid = x.rowid;
   end loop;

   for c in (select ar.LegacyKey from abc11p.abcta104_report ar
              where not exists (select 1 from api.registeredexternalobjects reo 
                                  where reo.linkvalue = to_char(ar.LegacyKey)
                                    and reo.objectdefid = 1616082)
                and ar.sessionkey = a_SessionKey) loop
    api.pkg_objectupdate.RegisterExternalObject('o_ABC_LegacyReport',to_char(c.legacykey), sysdate);
   end loop; 
    
    api.pkg_logicaltransactionupdate.EndTransaction();
    commit;
    api.pkg_logicaltransactionupdate.ResetTransaction();
   
     
  end  LegacyABCsa104CX;
 /**********************************************************************************
  *  LegacyABCsa104() -- Called by the rendering on the Excel tab of the  
  *  Legacy Financial Search in the ABC Internal system
  *  This procdure with the expetion of the call to LegacyABCsa104CX 
  *  and delete statements was provided by NJ-ABC and has been left as such.
  **********************************************************************************/
  procedure LegacyABCsa104(HOLD_DT_DEPOSIT_START     DATE,
                           HOLD_DT_DEPOSIT_END       DATE,
                           HOLD_DT_REFUND_START      DATE,
                           HOLD_DT_REFUND_END        DATE,
                           a_SESSIONKEY                in varchar2) is
                          
  
  BEGIN
  
    /*SELECT START_DT1, END_DT1, START_DT2, END_DT2
    INTO HOLD_DT_DEPOSIT_START, HOLD_DT_DEPOSIT_END, HOLD_DT_REFUND_START,
    HOLD_DT_REFUND_END
    FROM ABCTA_PARM WHERE FORM_NAME = 'ABCFA104';*/
    --
    
    delete from abc11p.abcta104;
    delete from abc11p.abcta104_sum; 

    DECLARE
      CURSOR C_NON_REF IS
        SELECT DETL_AMT, GRP_CD, TRANS_TYPE_CD
          FROM ABC11P.TRANS_MSTR M, ABC11P.TRANS_DETL D
         WHERE (DT_PROPOSED_DEPOSIT BETWEEN HOLD_DT_DEPOSIT_START AND
               HOLD_DT_DEPOSIT_END)
           AND (ADJUST_REASON || '' NOT IN ('NSF', 'RE') OR
               ADJUST_REASON || '' IS NULL)
           AND D.TRANS_NUM = M.TRANS_NUM
           AND D.MSTR_SEQ_NUM = M.MSTR_SEQ_NUM;
      --
      HOLD_DETL_AMT      NUMBER(8, 2);
      HOLD_GRP_CD        VARCHAR2(3);
      HOLD_TRANS_TYPE_CD VARCHAR2(5);
      --
    BEGIN
      OPEN C_NON_REF;
      LOOP
        FETCH C_NON_REF
          INTO HOLD_DETL_AMT, HOLD_GRP_CD, HOLD_TRANS_TYPE_CD;
        EXIT WHEN C_NON_REF%NOTFOUND;
        --
        INSERT INTO ABC11P.ABCTA104
        VALUES
          (HOLD_DETL_AMT,
           HOLD_GRP_CD,
           HOLD_TRANS_TYPE_CD,
           'N',
           NULL,
           SYSDATE,
           USER);
        --
      END LOOP;
      CLOSE C_NON_REF;
      COMMIT;
      --
    END;
    --
    DECLARE
      CURSOR C_REF IS
        SELECT DETL_AMT, GRP_CD, TRANS_TYPE_CD
          FROM ABC11P.TRANS_MSTR M, ABC11P.TRANS_DETL D
         WHERE (DT_REFUND BETWEEN HOLD_DT_REFUND_START AND
               HOLD_DT_REFUND_END)
           AND D.TRANS_NUM = M.TRANS_NUM
           AND D.MSTR_SEQ_NUM = M.MSTR_SEQ_NUM;
      --
      HOLD_DETL_AMT      NUMBER(8, 2);
      HOLD_GRP_CD        VARCHAR2(3);
      HOLD_TRANS_TYPE_CD VARCHAR2(5);
      --
    BEGIN
      OPEN C_REF;
      LOOP
        FETCH C_REF
          INTO HOLD_DETL_AMT, HOLD_GRP_CD, HOLD_TRANS_TYPE_CD;
        EXIT WHEN C_REF%NOTFOUND;
        --
        INSERT INTO ABC11P.ABCTA104
        VALUES
          (HOLD_DETL_AMT,
           HOLD_GRP_CD,
           HOLD_TRANS_TYPE_CD,
           'Y',
           NULL,
           SYSDATE,
           USER);
        --
      END LOOP;
      CLOSE C_REF;
      COMMIT;
      --
    END;
    --
    DECLARE
      CURSOR C_ADJUST IS
        SELECT DETL_AMT, GRP_CD
          FROM ABC11P.TRANS_MSTR M, ABC11P.TRANS_DETL D
         WHERE (DT_ADJUST BETWEEN HOLD_DT_DEPOSIT_START AND
               HOLD_DT_DEPOSIT_END)
           AND ADJUST_REASON = 'NSF'
           AND D.TRANS_NUM = M.TRANS_NUM
           AND D.MSTR_SEQ_NUM = M.MSTR_SEQ_NUM;
      --
      HOLD_DETL_AMT NUMBER(8, 2);
      HOLD_GRP_CD   VARCHAR2(3);
      --
    BEGIN
      OPEN C_ADJUST;
      LOOP
        FETCH C_ADJUST
          INTO HOLD_DETL_AMT, HOLD_GRP_CD;
        EXIT WHEN C_ADJUST%NOTFOUND;
        --
        INSERT INTO ABC11P.ABCTA104
        VALUES
          (HOLD_DETL_AMT, HOLD_GRP_CD, NULL, NULL, 'Y', SYSDATE, USER);
        --
      END LOOP;
      CLOSE C_ADJUST;
      COMMIT;
      --
    END;
    --
    DECLARE
      CURSOR C_SUM IS
        SELECT SUM(DETL_AMT), GRP_CD, TRANS_TYPE_CD, REFUND_IND
          FROM ABC11P.ABCTA104
         GROUP BY TRANS_TYPE_CD, GRP_CD, REFUND_IND
        HAVING REFUND_IND IN('Y', 'N') AND SUM(DETL_AMT) != 0
         ORDER BY TRANS_TYPE_CD, GRP_CD;
      --
      HOLD_DETL_AMT      NUMBER(10, 2);
      HOLD_GRP_CD        VARCHAR2(3);
      HOLD_TRANS_TYPE_CD VARCHAR2(5);
      HOLD_REFUND_IND    VARCHAR2(1);
      --
    BEGIN
      OPEN C_SUM;
      LOOP
        FETCH C_SUM
          INTO HOLD_DETL_AMT,
               HOLD_GRP_CD,
               HOLD_TRANS_TYPE_CD,
               HOLD_REFUND_IND;
        EXIT WHEN C_SUM%NOTFOUND;
        --
        INSERT INTO ABC11P.ABCTA104_SUM
        VALUES
          (HOLD_DETL_AMT,
           HOLD_GRP_CD,
           HOLD_TRANS_TYPE_CD,
           HOLD_REFUND_IND,
           NULL,
           SYSDATE,
           USER);
        --
      END LOOP;
      CLOSE C_SUM;
      COMMIT;
      --
    END;
    --
    DECLARE
      CURSOR C_ADJUST_SUM IS
        SELECT SUM(DETL_AMT), GRP_CD, ADJUST_IND
          FROM ABC11P.ABCTA104
         GROUP BY GRP_CD, ADJUST_IND
        HAVING ADJUST_IND = 'Y' AND SUM(DETL_AMT) != 0
         ORDER BY GRP_CD;
      --
      HOLD_DETL_AMT   NUMBER(10, 2);
      HOLD_GRP_CD     VARCHAR2(3);
      HOLD_ADJUST_IND VARCHAR2(1);
      --
    BEGIN
      OPEN C_ADJUST_SUM;
      LOOP
        FETCH C_ADJUST_SUM
          INTO HOLD_DETL_AMT, HOLD_GRP_CD, HOLD_ADJUST_IND;
        EXIT WHEN C_ADJUST_SUM%NOTFOUND;
        --
        INSERT INTO ABC11P.ABCTA104_SUM
        VALUES
          (HOLD_DETL_AMT,
           HOLD_GRP_CD,
           NULL,
           NULL,
           HOLD_ADJUST_IND,
           SYSDATE,
           USER);
        --
      END LOOP;
      CLOSE C_ADJUST_SUM;
      COMMIT;
      /*SELECT GRP_CD
        INTO HOLD_GRP_CD
        FROM ABC11P.ABCTA104_SUM
       WHERE ROWNUM = 1;*/
      --
    end;

    LegacyABCsa104CX(a_SESSIONKEY);
    
  end LegacyABCsa104;

 /**********************************************************************************
  *  LegacyABCsa104_fun() -- Called by the rendering on the Excel tab of the  
  *  Legacy Financial Search in the ABC Internal system
  *  This function was writtin to search for and provide a list of UDT_Objects to
  *  the Legacy Finacial Search so that an excel document can be generated.
  *  a_SessionKey is a Time.Time() call from python that is used to make each report unique
  **********************************************************************************/

   function LegacyABCsa104_fun(a_SessionKey in     varchar2)
     return api.udt_ObjectList is
   t_MasterList     api.udt_ObjectList; 
   t_ReportDefId number := api.pkg_configquery.ObjectDefIdForName('o_ABC_LegacyReport');
    
   begin 
     
  
    t_MasterList := api.udt_objectlist();  
   
  for w in (select objectid
  /* bulk collect into t_MasterList*/
               from api.registeredexternalobjects reo 
               join abc11p.abcta104_report ra on to_char(ra.legacykey) = reo.linkvalue
                                              and  reo.ObjectDefId = t_ReportDefId
              where ra.sessionkey = a_SessionKey
               order by ra.TRANS_TYPE) loop    
    t_MasterList.Extend();
     
    t_Masterlist(t_MasterList.Last) := api.udt_object(w.objectid);
    end loop;
    --api.pkg_errors.RaiseError(-20000, 'Master List Count: ' || t_MasterList.Count);   
    return t_MasterList;
  
  end;

end;
/
