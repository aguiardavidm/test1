create or replace package     pkg_ABC_Reports is

  -- Author  : Stan H.
  -- Created : 06May2011 (Reused from LCLB version in lclb.pkg_reports by AlbertD)
  -- Purpose : Provide a package in which report code can be written.

  -- Public type declarations
  subtype udt_ID is api.pkg_Definition.udt_ID;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_ObjectList is api.udt_ObjectList;
  
  /********************************************************************************
  * Procedure LegalEntitySummary()
  * fill temp table with legal entity ids that all belong within a structure.
  ********************************************************************************/
  procedure LegalEntitySummary (
    a_LegalEntityId udt_id,
    a_Level number default 0 );

  /********************************************************************************
  * Procedure LegalEntitySummary()
  * fill temp table with legal entity ids that all belong within a structure.
  ********************************************************************************/
  procedure LegalEntityHistorySummary ( a_ObjectId udt_id, a_Level number default 0 );

  /********************************************************************************
  * Function Work_Days_In_Month()
  * Built for RPTM-011 Average Inspections
  ********************************************************************************/
  function WorkDaysInMonth(
    a_Month in varchar2,
    a_Year in varchar2,
    a_AddMonths number default 0)
    return number;

  /********************************************************************************
  * Function FiscalStartFromMonthAndDay()
  * Built for RPTM-012 Total Inspections Report
  ********************************************************************************/
  function FiscalStartFromMonthAndDay(
    a_Date in date)
    return date;
  
  /**********************************************************************************
  * SumCategoryRevenue()
  *   Calculates revenue total for two separate years and the difference for
  *   the given GLAccount Category
  **********************************************************************************/
  function SumCategoryRevenue(
    a_PriorDate                       date,
    a_BaseDate                        date,
    a_GLAccountCat                    udt_Id
  ) return number;

  /**********************************************************************************
  * SumAccountRevenue()
  *   Calculates revenue total for the given GLAccount
  **********************************************************************************/
  function SumAccountRevenue(
    a_PriorDate                       date,
    a_BaseDate                        date,
    a_GLAccountId                     udt_Id
  ) return number;
  
  /**********************************************************************************
  * AnnualRevenueReport2()
  *   Used for Annual Revenue Statement Comparison Report. Allows for three
  *   cases of criteria selection
  **********************************************************************************/
  procedure AnnualRevenueReport(
    a_GLAccountCategory               varchar2,
    a_GLAccount                       varchar2,
    a_PriorYear                       varchar2,
    a_BaseYear                        varchar2
  );
  
  /**********************************************************************************
  * GetYear()
  *   Calculates the year for report criteria
  **********************************************************************************/  
  function GetYear(
    a_Year                            varchar2,
    a_IsEnd                           boolean
  ) return date;
  
  /**********************************************************************************
  * SumRefund()
  *   Sums all refund values for the given GL Account Category
  **********************************************************************************/  
  function SumRefund(
    a_PriorYear                       date,
    a_BaseYear                        date,
    a_GLAccountCatObjId               udt_Id
  ) return number;

  /********************************************************************************
  * Procedure AnnualRefundComparison()
  * Built for Annual Refund Comparison Report NJ ABC 113
  ********************************************************************************/
  procedure AnnualRefundComparison(
    a_PriorYear                       varchar2,
    a_BaseYear                        varchar2
  );
  
  /**********************************************************************************
  * RevenueReport()
  *   Creates temp table used by InfoMaker to display revenue per GL Account Category
  *   and GL Account within a specified date range
  **********************************************************************************/
  procedure RevenueReport(
    a_ObjectId                          udt_Id,
    a_PriorDate                         date,
    a_BaseDate                          date,
    a_GLAccountCategory                 varchar2,
    a_GLAccount                         varchar2
  );

end pkg_ABC_Reports;

 
/
grant execute
on pkg_abc_reports
to posseextensions;

create or replace package body     pkg_ABC_Reports is

  type udt_AccountRevenue is record
  ( ObjectId                      udt_Id
  , RevenueSum                    number(15,2) );
  type udt_AccountRevenueList is table of udt_AccountRevenue;

  -- Author  :        Stan H.
  -- Created :        06May2011 (Reused from LCLB version in lclb.pkg_reports by AlbertD)
  -- Purpose :        Provide a package in which report code can be written.  Initial version modified to populate
  --                  table of rows for Legal Entity History report.
  -- Change History : 13May2011, sjh, added Tag column to table to allow repeated rows to be tagged as such in report.
  --                  17May2011, sjh, added initial if statement to make call to LegalEntityHistorySummary
  --                  18May2011, sjh, order history rows by todate not fromdate

  /**********************************************************************************
  * LegalEntitySummary()
  *
  **********************************************************************************/
  procedure LegalEntitySummary ( a_LegalEntityId udt_id, a_Level number default 0 ) is
    t_Level                    number;
    t_PreviousOccurrences      number;

  begin
    if api.pkg_columnquery.value ( a_LegalEntityId, 'ObjectDefName') = 'o_ABC_LegalEntityHistory' then -- this is an LEH
      LegalEntityHistorySummary (a_LegalEntityId , a_Level );
    else -- this is an LE
      if a_level = 0 then
        delete from abc.legalentitysummary_t ;
      end if;

      t_Level := a_Level + 1 ;
      for c in ( select les.IncludesLEObjectId childleid,
                        les.relationshipid,
                        le.Active
                   from query.r_ABC_ParticipatesInIncludesLE les
                   join query.o_ABC_LegalEntity le
                     on les.IncludesLEObjectId = le.objectid
                  where les.ParticipatesInLEObjectId = a_LegalEntityId
                 order by nvl(les.PercentageInterest, 0) desc, le.dup_formattedname
                ) loop
        select count(legalentityid)
          into t_PreviousOccurrences
          from abc.legalentitysummary_t
         where c.childleid = legalentityid;
        if t_PreviousOccurrences = 0 then
          insert into abc.legalentitysummary_t
            values( a_LegalEntityId, c.childleid, c.relationshipid, c.active, t_Level, '' ) ;
          -- this is the first appearance of this LE, so show its substructure after its row
          LegalEntitySummary( c.childleid, t_Level );
        else
          -- this is a repeated appearance of this LE, so tag it as such and show no substructure
          insert into abc.legalentitysummary_t
            values( a_LegalEntityId, c.childleid, c.relationshipid, c.active, t_Level, '(rep.)' ) ;
        end if;
      end loop;
      if a_Level = 0 then
        for c in (select lehs.IncludesLEHObjectId childlehid,
                         lehs.relationshipid
                    from query.r_abc_LegalEntityLEHistory leh
                    join query.r_abc_LEHistoryIncludesLEH lehs
                      on leh.LegalEntityHistoryObjectId = lehs.ParticipatesInLEHObjectId
                    join query.o_abc_legalentityhistory his
                      on lehs.IncludesLEHObjectId = his.objectid
                   where leh.LegalEntityObjectId = a_Legalentityid
                   order by his.todate desc, his.dup_formattedname asc
                  ) loop
          insert into abc.legalentitysummary_t
             values( c.childlehid, c.childlehid, c.relationshipid, '', t_Level, '(hist.)') ;
             -- null in Active lets consumer decide whether to fill in a 'N' or ignore the row
        end loop;
      end if;
    end if; -- LEH rather than LE
  end LegalEntitySummary;

  /**********************************************************************************
  * LegalEntityHistorySummary()
  *
  **********************************************************************************/
  procedure LegalEntityHistorySummary ( a_ObjectId udt_id, a_Level number default 0 ) is
    t_Level                    number;
    t_PreviousOccurrences      number;

  begin
    if a_level = 0 then
      delete from abc.legalentitysummary_t ;
    end if;

    t_Level := a_Level + 1 ;
    for c in ( select les.IncludesLEHObjectId childleid,
                      les.relationshipid,
                      le.Active
                 from query.r_abc_LEHistoryIncludesLEH les
                 join query.o_ABC_LegalEntityHistory le
                   on les.IncludesLEHObjectId = le.objectid
                where les.ParticipatesInLEHObjectId = a_ObjectId
               order by nvl(les.PercentageInterest, 0) desc, le.dup_formattedname
              ) loop
      select count(legalentityid)
        into t_PreviousOccurrences
        from abc.legalentitysummary_t
       where c.childleid = legalentityid;
      if t_PreviousOccurrences = 0 then
        insert into abc.legalentitysummary_t
          values( a_ObjectId, c.childleid, c.relationshipid, c.active, t_Level, '(hist.)' ) ;
        -- this is the first appearance of this LEH so show its substructure after its row
        LegalEntityHistorySummary( c.childleid, t_Level );
      else
        -- this is a repeated appearance of this LEH, so tag it as such and show no substructure
        insert into abc.legalentitysummary_t
          values( a_ObjectId, c.childleid, c.relationshipid, c.active, t_Level, '(rep.)' ) ;
      end if;
    end loop; -- rows
  end LegalEntityHistorySummary;

  /**********************************************************************************
  * WorkDaysInMonth()
  *
  **********************************************************************************/
  function WorkDaysInMonth(a_Month in varchar2, a_Year in varchar2, a_AddMonths number default 0) return number is
    t_DaysInMonth              number := to_number(to_char(add_months(last_day(to_date(a_Month||a_Year, 'MONTHYYYY')),a_AddMonths),'DD'))-1;
    t_WorkDays                 number := 0;
  begin
    for i in 0..t_DaysInMonth loop
      if to_char(add_months(to_date(a_Month||a_Year, 'MONTHYYYY'),a_AddMonths)+i,'DY') not in ('SAT','SUN') then
        t_WorkDays := t_WorkDays + 1;
      end if;
    end loop;
    return(t_WorkDays);
  end WorkDaysInMonth;

  /**********************************************************************************
  * FiscalStartFromMonthAndDay()
  *
  **********************************************************************************/
  function FiscalStartFromMonthAndDay(a_Date in date) return date is
    t_FiscalMonth              number;
    t_FiscalDay                number;
    t_FMonthChar               char(2);
    t_FDayChar                 char(2);
    t_Month                    number := to_number(to_char(a_Date,'MM'));
    t_Day                      number := to_number(to_char(a_Date,'DD'));
    t_Year                     char(4);
  begin
    select nvl(o.FiscalStartMonth,1) into t_FiscalMonth from query.o_systemsettings o where o.ObjectDefTypeId = 1 and rownum = 1;
    select nvl(o.FiscalStartDay,1) into t_FiscalDay from query.o_systemsettings o where o.ObjectDefTypeId = 1 and rownum = 1;
    t_FMonthChar := lpad(to_char(t_FiscalMonth),2,'0');
    t_FDayChar := lpad(to_char(t_FiscalDay),2,'0');

    if t_Month > t_FiscalMonth or (t_Month = t_FiscalMonth and t_Day >= t_FiscalDay) then
      t_Year := to_char(a_Date,'YYYY');
    else
      t_Year := to_char(to_number(to_char(a_Date,'YYYY'))-1);
    end if;

    return to_date(t_FMonthChar||t_FDayChar||t_Year,'MMDDYYYY');

  end FiscalStartFromMonthAndDay;

 /**********************************************************************************
  * SumCategoryRevenue()
  *   Calculates revenue total for two separate years and the difference for
  *   the given GLAccount Category
  **********************************************************************************/
  function SumCategoryRevenue(
    a_PriorDate                       date,
    a_BaseDate                        date,
    a_GLAccountCat                    udt_Id
  ) return number is
    t_Sum                             number(15,2);
    t_GLAccountObjDefId               udt_Id := api.pkg_configquery.ObjectDefIdForName('o_GLAccount');
    t_PaymentObjectDefId              udt_id := api.pkg_configquery.ObjectDefIdForName('o_Payment');
    t_eComObjectDefId                 udt_id := api.pkg_configquery.ObjectDefIdForName('j_eCom');
    t_eComtoPaymentDefId              udt_id;
    
  begin
           
    select rd.RelationshipDefId
     into t_eComtoPaymentDefId
     from api.relationshipdefs rd
    where rd.ToObjectDefId = t_PaymentObjectDefId
      and rd.FromObjectDefId = t_eComObjectDefId;
             
select nvl(sum(x.SumAMT)*-1,0)--x.GLAccountId 
       into t_Sum
       from
        (select nvl(sum(c.amount), 0)SumAMT, c.GLAccountId
 from (         
      select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'
          and 
          trunc(ft.TransactionDate) between a_PriorDate and a_BaseDate
          and not  exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and trunc(ft2.TransactionDate) between a_PriorDate and a_BaseDate
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and not exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
       union all 
       select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'        
          and  exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)) c
  group by c.GLAccountId    ) x
            join api.registeredexternalobjects reo on reo.LinkValue = to_char(x.GLAccountId)
            join query.r_ABC_GLAccountToCategory r on r.GLAccountObjId = reo.ObjectId
          where reo.ObjectDefId = t_GLAccountObjDefId 
            and r.GLAccountCategoryObjId = a_GLAccountCat;           
           
    return t_Sum;
  end SumCategoryRevenue;

  /**********************************************************************************
  * SumAccountRevenue()
  *   Calculates revenue total for the given GLAccount
  **********************************************************************************/
  function SumAccountRevenue(
    a_PriorDate                       date,
    a_BaseDate                        date,
    a_GLAccountId                     udt_Id
  ) return number is
    t_Sum                             number(15,2);
    t_GLAccountObjDefId               udt_Id := api.pkg_configquery.ObjectDefIdForName('o_GLAccount');
    t_PaymentObjectDefId              udt_id := api.pkg_configquery.ObjectDefIdForName('o_Payment');
    t_eComObjectDefId                 udt_id := api.pkg_configquery.ObjectDefIdForName('j_eCom');
    t_eComtoPaymentDefId              udt_id;
    
    
  begin
    select rd.RelationshipDefId
     into t_eComtoPaymentDefId
     from api.relationshipdefs rd
    where rd.ToObjectDefId = t_PaymentObjectDefId
      and rd.FromObjectDefId = t_eComObjectDefId;
     select nvl(sum(x.SumAMT)*-1,0)--x.GLAccountId 
       into t_Sum
       from
        (select nvl(sum(c.amount), 0)SumAMT, c.GLAccountId
 from (         
      select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'
          and trunc(ft.TransactionDate) between a_PriorDate and a_BaseDate
          and f.GLAccountId = a_GLAccountId
          and not  exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and f2.GLAccountId = a_GLAccountId
          and trunc(ft2.TransactionDate) between a_PriorDate and a_BaseDate
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and not exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
       union all 
       select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'
          and f.GLAccountId = a_GLAccountId        
          and  exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and f2.GLAccountId = a_GLAccountId
          and exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)) c
  group by c.GLAccountId    )x;        
             
    return t_Sum;
  end SumAccountRevenue;

  /**********************************************************************************
  * AnnualRevenueReport()
  *   Used for Annual Revenue Statement Comparison Report. Allows for three
  *   cases of criteria selection
  **********************************************************************************/
  procedure AnnualRevenueReport(
    a_GLAccountCategory               varchar2,
    a_GLAccount                       varchar2,
    a_PriorYear                       varchar2,
    a_BaseYear                        varchar2
  ) is
    -- local types
    type GLAccountCategory is record
      ( ObjectId                      udt_Id
      , Category                      varchar2(60) );
    type GLAccount is record
      ( GLAccountId                   udt_Id
      , Description                   varchar2(60) );
    type GLAccountCatList is table of GLAccountCategory;
    type GLAccountList is table of GLAccount;

    t_FromPriorYearDate               date;
    t_toPriorYearDate                 date;
    t_FromBaseYearDate                date;
    t_ToBaseYearDate                  date;

    t_GLAccountCatList                GLAccountCatList;
    t_GLAccountCategoryList           api.pkg_Definition.udt_StringList;
    t_FullAccountList                 udt_IdList;
    a_Objects                         udt_ObjectList;
    t_GLAccounts                      udt_IdList;
    t_GLAccountList                   GLAccountList;

  -- Begin Procedure
  begin
    -- clear the table for new values
    execute immediate 'truncate table abc.AnnualRevenueComparison_t';

    -- Setup dates for date range
    select trunc(to_date(a_PriorYear, 'YYYY'), 'YEAR'),
           trunc(to_date(a_BaseYear, 'YYYY'), 'YEAR')
      into t_FromPriorYearDate,
           t_FromBaseYearDate
      from dual;

    select add_months(t_FromPriorYearDate, 12) - 1,
           add_months(t_FromBaseYearDate, 12) - 1
      into t_ToPriorYearDate,
           t_ToBaseYearDate
      from dual;

    /************************************************************************
    * Case #1: GLAccountCategory not specified (GLAccount cannot be specified)
    ************************************************************************/
    if a_GLAccountCategory is null and a_GLAccount is null then

      -- Get the GL Account Categories
      select ObjectId
           , Category
        bulk collect into t_GLAccountCatList
        from query.o_ABC_GLAccountCategory
       order by Category;

      -- calculate revenue totals for each GL Account Category
      for i in 1..t_GLAccountCatList.count loop
        declare
          t_PriorSum                    number(15,2);
          t_BaseSum                     number(15,2);
        begin
          t_PriorSum := SumCategoryRevenue(t_FromPriorYearDate, t_ToPriorYearDate,
              t_GLAccountCatList(i).ObjectId);
          t_BaseSum := SumCategoryRevenue(t_FromBaseYearDate, t_ToBaseYearDate,
              t_GLAccountCatList(i).ObjectId);

          -- enter the values into the table
          insert into abc.AnnualRevenueComparison_t
            ( Description
            , PriorFiscalYear
            , BaseFiscalYear
            , Difference ) values ( t_GLAccountCatList(i).Category
                                  , t_PriorSum
                                  , t_BaseSum
                                  , t_BaseSum - t_PriorSum );
        end;
      end loop;

    /************************************************************************
    * Case #2: GLAccountCategory specified, GLAccount not specified
    ************************************************************************/
    elsif a_GLAccountCategory is not null and a_GLAccount is null then

      -- Get list of viable GLAccounts
      t_GLAccountCategoryList := extension.pkg_Utils.Split(a_GLAccountCategory, ', ');

      -- Initialize the object list
      a_Objects := api.udt_ObjectList();

      -- collect GL Account objectids into table
      for i in 1..t_GLAccountCategoryList.count loop
        select gl.objectid
          bulk collect into t_GLAccounts
          from query.o_glaccount gl
          join api.relationships r
            on r.FromObjectId = gl.objectid
          join query.o_ABC_GLAccountCategory glc
            on r.ToObjectId = glc.objectid
         where glc.Category = t_GLAccountCategoryList(i);

         -- append the per category list to a final output list
         extension.pkg_collectionutils.Append(t_FullAccountList,t_GLAccounts);
      end loop;

      -- convert the array to an object list
      a_Objects := extension.pkg_utils.ConvertToObjectList(t_FullAccountList);

      -- Get the GLAccount Ids for the specified categories
      select o.GLAccountId
           , o.Description
        bulk collect into t_GLAccountList
        from query.o_GLAccount o
       where o.ObjectId in (select * from table(a_Objects))
       order by Description;

      -- For each category, get the revenue totals
      for acc in 1..t_GLAccountList.count loop
        declare
          t_PriorSum                    number(15,2);
          t_BaseSum                     number(15,2);
        begin
          t_PriorSum := SumAccountRevenue(t_FromPriorYearDate, t_ToPriorYearDate,
              t_GLAccountList(acc).GLAccountId);
          t_BaseSum := SumAccountRevenue(t_FromBaseYearDate, t_ToBaseYearDate,
              t_GLAccountList(acc).GLAccountId);

          -- enter the values into the table
          insert into abc.AnnualRevenueComparison_t
            ( Description
            , PriorFiscalYear
            , BaseFiscalYear
            , Difference ) values ( t_GLAccountList(acc).Description
                                  , t_PriorSum
                                  , t_BaseSum
                                  , t_BaseSum - t_PriorSum );
        end;
      end loop;

    /************************************************************************
    * Case #3: GLAccountCategory specified, GLAccount specified
    ************************************************************************/
    else --if a_GLAccountCategory is not null and a_GLAccount is not null then

      -- Get the GL Accounts (should get only one value)
      select GLAccountId
           , Description
        bulk collect into t_GLAccountList
        from query.o_GLAccount
       where Description = a_GLAccount
       order by Category;

      -- calculate revenue totals for each GL Account
      for i in 1..t_GLAccountList.count loop
        declare
          t_PriorSum                    number(15,2);
          t_BaseSum                     number(15,2);
        begin
          t_PriorSum := SumAccountRevenue(t_FromPriorYearDate, t_ToPriorYearDate,
              t_GLAccountList(i).GLAccountId);
          t_BaseSum := SumAccountRevenue(t_FromBaseYearDate, t_ToBaseYearDate,
              t_GLAccountList(i).GLAccountId);

          -- enter the values into the table
          insert into abc.AnnualRevenueComparison_t
            ( Description
            , PriorFiscalYear
            , BaseFiscalYear
            , Difference ) values ( t_GLAccountList(i).Description
                                  , t_PriorSum
                                  , t_BaseSum
                                  , t_BaseSum - t_PriorSum );
        end;
     end loop;


    end if;

    commit;
  end AnnualRevenueReport;

  /**********************************************************************************
  * GetYear()
  *   Calculates the year for report criteria
  **********************************************************************************/
  function GetYear(
    a_Year                            varchar2,
    a_IsEnd                           boolean   -- Used to determine if end of year or not
  ) return date is
    t_Year                            date;
  begin
    -- beginning of year
    if not a_IsEnd then
      select trunc(to_date(a_Year, 'YYYY'), 'YEAR')
        into t_Year
        from dual;
    -- end of year
    else
      select add_months(trunc(to_date(a_Year, 'YYYY'), 'YEAR'), 12) - 1
        into t_Year
        from dual;
    end if;

    return t_Year;
  end GetYear;

  /**********************************************************************************
  * SumRefund()
  *   Sums all refund values for the given GL Account Category
  **********************************************************************************/
  function SumRefund(
    a_PriorYear                       date,
    a_BaseYear                        date,
    a_GLAccountCatObjId               udt_Id
  ) return number is
    t_Refund                          number(15,2);
  begin

    select nvl(sum(ft.Amount), 0)
      into t_Refund
      from api.Fees f
      join query.o_glaccount gl
        on gl.GLAccountID = f.GLAccountId
      join api.relationships r
        on r.FromObjectId = gl.ObjectId
      join query.o_Abc_Glaccountcategory glc
        on r.ToObjectId = glc.ObjectId
      join api.FeeTransactions ft
        on f.FeeId = ft.FeeId
      join api.PaymentTransactions pt
        on ft.TransactionId = pt.TransactionId
      join table(api.pkg_simplesearch.CastableObjectsByIndex('o_Payment', 'IsRefund', 'Y')) SS
        on api.pkg_columnquery.Value(ss.objectId, 'paymentid') = pt.PaymentId
     where glc.objectid = a_GLAccountCatObjId
           and ft.TransactionDate BETWEEN a_PriorYear and a_BaseYear
           and pt.VoidedByTransactionId is null;

/*select nvl(sum(ft.Amount), 0)
      into t_Refund
      from api.Fees f
      join query.o_glaccount gl
        on gl.GLAccountID = f.GLAccountId
      join api.relationships r
        on r.FromObjectId = gl.ObjectId
      join query.o_Abc_Glaccountcategory glc
        on r.ToObjectId = glc.ObjectId
      join api.FeeTransactions ft
        on f.FeeId = ft.FeeId
      join api.PaymentTransactions pt
        on ft.TransactionId = pt.TransactionId
      join api.Payments p
        on p.PaymentId = pt.PaymentId
      \*join query.o_Payment op
        on op.PaymentId = pt.PaymentId*\
      join table(api.pkg_simplesearch.CastableObjectsByIndex('o_Payment', 'IsRefund', 'Y')) SS
        on ss.objectid = pt.PaymentId
     where glc.objectid = a_GLAccountCatObjId
           and ft.TransactionDate between a_PriorYear and a_BaseYear
           and pt.VoidedByTransactionId is null
     order by glc.Category;*/


    return t_Refund;
  end SumRefund;

  /* This table is used to create the Refund table in the abc schema

  create global temporary table abc.AnnualRefundComparison_t
    ( GLDescription                     varchar2(60)
    , PriorYear                         number(15,2)
    , BaseYear                          number(15,2)
    , Difference                        number(15,2) ) on commit preserve rows;

  grant select on abc.AnnualRefundComparison_t to public;
  */

  /**********************************************************************************
  * AnnualRefundComparison()
  *
  **********************************************************************************/
  procedure AnnualRefundComparison(
    a_PriorYear                         varchar2,
    a_BaseYear                          varchar2
  ) is
    type GLAccountCat is record
      ( ObjectId                        udt_Id,
        Category                        varchar2(60));
    type l_GLAccountCat is table of GLAccountCat;

    t_FromPriorYear                     date;
    t_ToPriorYear                       date;
    t_FromBaseYear                      date;
    t_ToBaseYear                        date;
    t_GLAccountCatList                  l_GLAccountCat;

  begin
    -- clear out temporary table
    execute immediate 'truncate table abc.AnnualRefundComparison_t';

    -- set up year ranges
    t_FromPriorYear := GetYear(a_PriorYear,false);
    t_ToPriorYear   := GetYear(a_PriorYear,true);
    t_FromBaseYear  := GetYear(a_BaseYear, false);
    t_ToBaseYear    := GetYear(a_BaseYear, true);

    -- set up list of G/L Accounts
    select ObjectId,
           Category
      bulk collect into t_GLAccountCatList
      from query.o_ABC_GLAccountCategory;

    -- loop through G/L Account Categories
    for i in 1..t_GLAccountCatList.count loop
      declare
        t_PriorRefund                   number(15,2);
        t_BaseRefund                    number(15,2);
      begin
        t_PriorRefund := SumRefund(t_FromPriorYear, t_ToPriorYear, t_GLAccountCatList(i).ObjectId);
        t_BaseRefund :=  SumRefund(t_FromBaseYear, t_ToBaseYear, t_GLAccountCatList(i).ObjectId);

        insert into abc.AnnualRefundComparison_t
          ( GLDescription
          , PriorYear
          , BaseYear
          , Difference ) values ( t_GLAccountCatList(i).Category
                                , t_PriorRefund
                                , t_BaseRefund
                                , t_BaseRefund - t_PriorRefund );
      end;
    end loop;
  end AnnualRefundComparison;

---------------------------------------------------------------------------------
  /*
  create global temporary table abc.RevenueReport_t
    ( GLAccountDescriptions             varchar2(4000)
    , GLAccountId                       number(9)
    , LicenseTypeId                     number(9)
    , RevenueSum                        number(15,2) ) on commit preserve rows;

  grant select on abc.RevenueReport_t to public;
  */


 /**********************************************************************************
  * CalculateLicenseRevenue()
  *   Used to calculate revenue for the Revenue Report
  **********************************************************************************/
  function CalculateAccountRevenue(
    a_PriorDate                       date,
    a_BaseDate                        date,
    a_GLAccountCatId                  udt_Id,
    a_GLAccountObjId                  udt_Id
  ) return udt_AccountRevenueList is
    t_AccountRevenueList              udt_AccountRevenueList;
    t_GLAccountObjDefId               udt_Id := api.pkg_configquery.ObjectDefIdForName('o_GLAccount');
    t_PaymentObjectDefId              udt_id := api.pkg_configquery.ObjectDefIdForName('o_Payment');
    t_eComObjectDefId                 udt_id := api.pkg_configquery.ObjectDefIdForName('j_eCom');
    t_eComtoPaymentDefId              udt_id;
    t_GLAccountId                     udt_id;

  begin
    
    select rd.RelationshipDefId
     into t_eComtoPaymentDefId
     from api.relationshipdefs rd
    where rd.ToObjectDefId = t_PaymentObjectDefId
      and rd.FromObjectDefId = t_eComObjectDefId;
    
    if  a_GLAccountObjId is not null then 
      select gl.GLAccountId
        into t_GLAccountId
        from query.o_glaccount gl
       where gl.ObjectId = a_GLAccountObjId;
    end if;
       
    
    -- Case 1: GL Account Specified --> Get GL Account revenue
    if a_GLAccountObjId is not null then
      select reo.ObjectId, x.SumAMT*-1--x.GLAccountId 
       bulk collect into t_AccountRevenueList
       from
        (select nvl(sum(c.amount), 0)SumAMT, c.GLAccountId
 from (         
      select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'
          and trunc(ft.TransactionDate) between a_PriorDate and a_BaseDate
          and f.GLAccountId = t_GLAccountId
          and not  exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and f2.GLAccountId = t_GLAccountId
          and trunc(ft2.TransactionDate) between a_PriorDate and a_BaseDate
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and not exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
       union all 
       select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'
          and f.GLAccountId = t_GLAccountId        
          and  exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and f2.GLAccountId = t_GLAccountId
          and exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)) c
  group by c.GLAccountId    ) x
            join api.registeredexternalobjects reo on reo.LinkValue = to_char(x.GLAccountId)
            join query.r_ABC_GLAccountToCategory r on r.GLAccountObjId = reo.ObjectId
          where reo.ObjectDefId = t_GLAccountObjDefId
            and r.GLAccountCategoryObjId = a_GLAccountCatId;
            
      
    -- Case 2: GL Account Not Specified --> Get the GL Account Category revenue
    else       
       select reo.ObjectId, x.SumAMT*-1--x.GLAccountId 
       bulk collect into t_AccountRevenueList
       from
        (select nvl(sum(c.amount), 0)SumAMT, c.GLAccountId
 from (         
      select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'
          and 
          trunc(ft.TransactionDate) between a_PriorDate and a_BaseDate
          and not  exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and trunc(ft2.TransactionDate) between a_PriorDate and a_BaseDate
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and not exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
       union all 
       select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'        
          and  exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)) c
  group by c.GLAccountId    ) x
            join api.registeredexternalobjects reo on reo.LinkValue = to_char(x.GLAccountId)
            join query.r_ABC_GLAccountToCategory r on r.GLAccountObjId = reo.ObjectId
          where reo.ObjectDefId = t_GLAccountObjDefId 
            and r.GLAccountCategoryObjId = a_GLAccountCatId;

      -- we couldn't find gl account, just calculate revenue for
      -- the G/L Account category
      if t_AccountRevenueList.count = 0 then
       select reo.ObjectId, x.SumAMT*-1--x.GLAccountId 
       bulk collect into t_AccountRevenueList
       from
        (select nvl(sum(c.amount), 0)SumAMT, c.GLAccountId
 from (         
      select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'
          and 
          trunc(ft.TransactionDate) between a_PriorDate and a_BaseDate
          and not  exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and trunc(ft2.TransactionDate) between a_PriorDate and a_BaseDate
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and not exists (select 1 
                            from api.relationships r
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId))
       union all 
       select ft.amount, f.GLAccountId
        from api.fees f
        join api.feetransactions ft on ft.FeeId = f.FeeId
        join api.paymenttransactions pt on pt.TransactionId = ft.TransactionId
        where ft.TransactionType = 'Pay'        
          and  exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)
      union all
      select ft2.amount, f2.GLAccountId
        from api.fees f2
        join api.feetransactions ft2 on ft2.FeeId = f2.FeeId
        join api.paymenttransactions pt
          on pt.VoidedByTransactionId = ft2.TransactionId
        join api.feetransactions ft3 on ft3.transactionid = pt.VoidedByTransactionId  
        where ft2.TransactionType = 'Adj'
          and trunc(ft3.TransactionDate) between a_PriorDate and a_BaseDate
          and exists (select 1 
                            from query.j_ecom e
                            join api.relationships r on r.FromObjectId = e.ObjectId
                            join api.registeredexternalobjects reo on reo.ObjectId = r.ToObjectId
                           where r.RelationshipDefId = t_eComtoPaymentDefId
                             and reo.ObjectDefId = t_PaymentObjectDefId
                             and reo.LinkValue = to_char(pt.PaymentId)
                             and trunc(e.CreatedDate) between a_PriorDate and a_BaseDate)) c
  group by c.GLAccountId    ) x
            join api.registeredexternalobjects reo on reo.LinkValue = to_char(x.GLAccountId)
            join query.r_ABC_GLAccountToCategory r on r.GLAccountObjId = reo.ObjectId
          where reo.ObjectDefId = t_GLAccountObjDefId 
            and r.GLAccountCategoryObjId = a_GLAccountCatId;
      end if;

    end if;
    return t_AccountRevenueList;
  end CalculateAccountRevenue;

  /**********************************************************************************
  * RevenueReport()
  *   Creates temp table used by InfoMaker to display revenue per GL Account Category
  *   and GL Account within a specified date range
  **********************************************************************************/
  procedure RevenueReport(
    a_ObjectId                          udt_Id,
    a_PriorDate                         date,
    a_BaseDate                          date,
    a_GLAccountCategory                 varchar2,
    a_GLAccount                         varchar2
  ) is
    -- structure to hold G/L Account information
    type udt_GLAccountCat is record
      ( ObjectId                        udt_Id
      , Category                        varchar2(60) );
    type udt_GLAccountCatList is table of udt_GLAccountCat;

    t_GLAccountCatSplitList             api.pkg_Definition.udt_StringList;
    t_GLAccountCatList                  udt_GLAccountCatList;
    t_AccountRevenueList                udt_AccountRevenueList;
    t_GLAccountObjId                    udt_Id;

  begin

    -- clear out temporary table
    execute immediate 'truncate table abc.RevenueReport_t';

    /********************************************************
    * Get the GL Account Categories
    ********************************************************/
    -- If a GL Account Category has been specified, it will be in the form of a comma-separated
    -- string. We need to split this and put the individual objectids into a new List
    if a_GLAccountCategory is not null then
      t_GLAccountCatSplitList := extension.pkg_Utils.Split(a_GLAccountCategory, ', ');
      t_GLAccountCatList      := udt_GLAccountCatList();

      -- Get the ObjectId and Category for each GL Account Category
      for i in 1..t_GLAccountCatSplitList.count loop
        t_GLAccountCatList.extend;

        select glc.ObjectId
             , glc.Category
          into t_GLAccountCatList(i)
          from query.o_ABC_GLAccountCategory glc
         where glc.Category = t_GLAccountCatSplitList(i);
      end loop;

    -- if no GL Account Categories were selected, get all Categories
    else
      select glc.objectid
           , glc.category
        bulk collect into t_GLAccountCatList
        from query.o_ABC_GLAccountCategory glc;
    end if;

    /********************************************************
    * Calculate the revenue for each GL Account Category
    ********************************************************/
    for i in 1..t_GLAccountCatList.count loop
        t_GLAccountObjId := api.pkg_columnquery.value(a_ObjectId,'GLAccountId');
        -- Calculate the revenue
        t_AccountRevenueList := CalculateAccountRevenue(a_PriorDate, a_BaseDate, t_GLAccountCatList(i).ObjectId, t_GLAccountObjId);

        -- Set values for the temp table
        for j in 1..t_AccountRevenueList.count loop
          insert into abc.RevenueReport_t
            ( GLAccountCategory
            , GLAccountCatObjId
            , GLAccountObjId
            , RevenueSum ) values ( t_GLAccountCatList(i).Category
                                  , t_GLAccountCatList(i).ObjectId
                                  , t_AccountRevenueList(j).ObjectId
                                  , nvl((t_AccountRevenueList(j).RevenueSum),0));
        end loop;

    end loop;

  end RevenueReport;

                                                   
end pkg_ABC_Reports;
/
