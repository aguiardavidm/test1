create or replace package abc.pkg_InstanceSecurity as

  /*---------------------------------------------------------------------------
   * DESCRIPTION
   *   Routines used to manipulate instance security.
   *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * AdjustByColumns()
   *   This takes two parameters both of which are a list of access group
   * descriptions.  The first list will be given read access to the current
   * object.  While the second lilst will be given modify access to the current
   * object.
   *-------------------------------------------------------------------------*/
  procedure AdjustByColumn (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_ReadAccessGroups                  varchar2,
    a_ModifyAccessGroups                varchar2
  );

    /*---------------------------------------------------------------------------
   * NewAccessGroup()
   *  This allocates a new access group from the existing pool and returns
   * its AccessGroupId.
   *-------------------------------------------------------------------------*/
  function NewAccessGroup
  return udt_id;

  /*---------------------------------------------------------------------------
   * GrantWWWAccessGroupToUser() -- PUBLIC
   *   Create a new Access group to assign to the calling user.
   *   This will be used to grant instance security
   *-------------------------------------------------------------------------*/
  procedure GrantWWWAccessGroupToUser(
    a_UserId                          udt_id
  );

  /*---------------------------------------------------------------------------
   * SyncAccessGroupMembers()
   *   This updates the membership of the access group to the provided list.
   *-------------------------------------------------------------------------*/
  procedure SyncAccessGroupMembers (
    a_AccessGroupId                     udt_Id,
    a_Members                           in out nocopy udt_IdList
  );

  /*---------------------------------------------------------------------------
   * UserHasPrivilege() -- PUBLIC
   *   This checks if the user passed in has the requested privilege on the
   *   object requested
   *-------------------------------------------------------------------------*/
  function UserHasPrivilege (
    a_ObjectId                          udt_Id,
    a_UserId                            udt_Id,
    a_Privilege                         varchar2
  ) return boolean;

  /*---------------------------------------------------------------------------
   * ApplyInstanceSecurity()
   *   This is called from the post-verify from various jobs and objects that
   *   would like to have instance security applied to them.
   *-------------------------------------------------------------------------*/
  procedure ApplyInstanceSecurity (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- ReApplyInstanceSecurityForUser
  --  Re-apply instance security on a number of things to take into account a
  --  new user related to an LE
  -- This is called from the rel between Legal Entity and User
  -----------------------------------------------------------------------------
  procedure ReApplyInstanceSecurityForUser (
    a_RelationshipId      udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  -- RevokeInstanceSecurityForUser
  --  Revoke instance security from a specific user for a number of things.
  -- This is called on delete of the rel between Legal Entity and User
  -----------------------------------------------------------------------------
  procedure RevokeInstanceSecurityForUser (
    a_RelationshipId      udt_Id,
    a_AsOfDate            date
  );

end pkg_InstanceSecurity;
/
grant execute
on pkg_instancesecurity
to extension;

grant execute
on pkg_instancesecurity
to posseextensions;

create or replace package body abc.pkg_InstanceSecurity as

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/

  subtype udt_Boolean is api.pkg_Definition.udt_Boolean;
  type udt_SecurityPriv is record (
    AccessGroupId                       udt_Id,
    Privilege                           varchar2(30)
  );
  type udt_SecurityPrivList is table of udt_SecurityPriv index by binary_integer;
  type udt_StringIndexedList is table of boolean index by varchar2(50);

  /*---------------------------------------------------------------------------
   * Global variables
   *-------------------------------------------------------------------------*/
  gc_AccessGroupPrefix                  constant varchar2(10) := 'www';


  /*---------------------------------------------------------------------------
   * Private procedure predeclarations to get around PLS-0313 compile errors
   *-------------------------------------------------------------------------*/
  function SparseIdListDifference (
    a_List1               in out nocopy udt_IdList,
    a_List2               in out nocopy udt_IdList
  ) return udt_IdList;


  /****************************************************************************
   * Internal Utilities
   *   These utilities are generic and can be used elsewhere.
   ***************************************************************************/

  /*---------------------------------------------------------------------------
   * ConvertToSparseArray() -- PRIVATE
   *   Converts a udt_IdList into a sparse array (where the index value is the
   * same as the element value).
   *-------------------------------------------------------------------------*/
  function ConvertIdListToSparse (
    a_List                in out nocopy udt_IdList
  ) return udt_IdList is
    t_NewList                           udt_IdList;
  begin
    for i in 1..a_List.count loop
      t_NewList(a_list(i)) := null;
    end loop;
    return t_NewList;
  end ConvertIdListToSparse;


  /*---------------------------------------------------------------------------
   * SecurityPrivListDifference() -- PRIVATE
   *  Return a_PrivList1 - a_PrivList2.
   * Note: This has the great side effect of removing all duplicates.
   *-------------------------------------------------------------------------*/
  function SecurityPrivListDifference (
    a_PrivList1           in out nocopy udt_SecurityPrivList,
    a_PrivList2           in out nocopy udt_SecurityPrivList
  ) return udt_SecurityPrivList is
    type udt_PrivGroupList is table of udt_IdList index by varchar2(30);
    t_Privilege                         varchar2(30);
    t_PrivGroupList1                    udt_PrivGroupList;
    t_PrivGroupList2                    udt_PrivGroupList;
    t_AccessGroupId                     udt_Id;
    t_AccessGroups                      udt_IdList;
    t_SecurityPrivList                  udt_SecurityPrivList;
  begin
    /* Put the privileges into two dimensional sparse arrays.  The first
      level is indexed by the privilege and the second is indexed by the
      access group id */

    pkg_debug.putline('*** Security Priv Difference Start');

    for i in 1..a_PrivList1.count loop
      t_PrivGroupList1(a_PrivList1(i).Privilege)(a_PrivList1(i).AccessGroupId) := null;
    end loop;

    for i in 1..a_PrivList2.count loop
      t_PrivGroupList2(a_PrivList2(i).Privilege)(a_PrivList2(i).AccessGroupId) := null;
    end loop;

    t_Privilege := t_PrivGroupList1.first;

    while t_Privilege is not null loop
      if not t_PrivGroupList2.exists(t_Privilege) then
        t_AccessGroups := t_PrivGroupList1(t_Privilege);
      else
        t_AccessGroups := SparseIdListDifference(t_PrivGroupList1(t_Privilege),
            t_PrivGroupList2(t_Privilege));
      end if;

      t_AccessGroupId := t_AccessGroups.first;
      while t_AccessGroupId is not null loop
        t_SecurityPrivList(t_SecurityPrivList.count+1).AccessGroupId := t_AccessGroupId;
        t_SecurityPrivList(t_SecurityPrivList.count).Privilege := t_Privilege;
        t_AccessGroupId := t_AccessGroups.next(t_AccessGroupId);
      end loop;

      t_Privilege := t_PrivGroupList1.next(t_Privilege);
    end loop;

    return t_SecurityPrivList;
  end SecurityPrivListDifference;


  /*---------------------------------------------------------------------------
   * SparseIdListDifference() -- PRIVATE
   *   Return a_List1 - a_List2.  In otherwords, return all the elements in
   * a_List1 but not in a_List2.
   *
   * This assumes that both lists are sparse arrays where the index value is
   * the element being tested for.
   *-------------------------------------------------------------------------*/
  function SparseIdListDifference (
    a_List1               in out nocopy udt_IdList,
    a_List2               in out nocopy udt_IdList
  ) return udt_IdList is
    t_DiffList                          udt_IdList;
    t_Id                                udt_Id;
  begin
    t_Id := a_List1.first;
    while t_Id is not null loop
      if not a_List2.exists(t_Id) then
        t_DiffList(t_Id) := null;
      end if;
      t_Id := a_List1.next(t_Id);
    end loop;

    return t_DiffList;
  end SparseIdListDifference;


  /*---------------------------------------------------------------------------
   * SplitList() -- PRIVATE
   *   This takes a semicolon separated list of Access Group descriptions and a
   * security privilege and appends the normalized values to a
   * Security Privilege List.
   *-------------------------------------------------------------------------*/
  procedure SplitList (
    a_AccessGroups                      varchar2,
    a_Privilege                         varchar2,
    a_SecurityPrivList    in out nocopy udt_SecurityPrivList
  ) is
    t_AccessGroup                       api.AccessGroups.Description%type;
    t_AccessGroups                      varchar2(4000);
    t_Pos                               number;
    t_AccessGroupId                     udt_Id;
    t_Count                             pls_integer;
  begin
    t_Count := a_SecurityPrivList.count;

    -- Check to see if there is already a semi-colon at the end
    -- If there is, don't append one.  This will prevent cases where
    -- a misconfigured (or deliberate) expression has one already appended,
    -- and the search for a blank access group causes the procedure to fail
    app.pkg_Debug.putsingleline('SplitList Access groups:' || a_AccessGroups);
    if substr(a_AccessGroups, length(a_AccessGroups)) != ';' then
      t_AccessGroups := a_AccessGroups || ';';
    else
      t_AccessGroups := a_AccessGroups;
    end if;

    --remove any duplicate semi-colons
    t_AccessGroups := regexp_replace(t_AccessGroups, '(;)\1+', '\1');

    t_Pos := instr(t_AccessGroups, ';');
    app.pkg_Debug.putsingleline('Position:' || t_Pos);
    while t_Pos > 0 loop
      t_AccessGroup := substr(t_AccessGroups, 1, t_Pos - 1);
      t_AccessGroups := substr(t_AccessGroups, t_Pos + 1);
      app.pkg_Debug.putsingleline('Access Groups in List' || a_AccessGroups);
      begin
        select AccessGroupId
        into t_AccessGroupId
        from api.AccessGroups
        where Description = t_AccessGroup;
      exception
        when no_data_found then
          app.pkg_debug.putsingleline('NO DATA FOUND');
          api.pkg_Errors.Clear();
          api.pkg_Errors.SetArgValue('AccessGroup',t_AccessGroup);
          api.pkg_Errors.SetArgValue('AccessGroups', a_AccessGroups);
          api.pkg_Errors.RaiseError(-20100, 'Unable to find Access Group "{AccessGroup}".');

      end;
      t_Count := t_Count + 1;
      a_SecurityPrivList(t_Count).AccessGroupId := t_AccessGroupId;
      a_SecurityPrivList(t_Count).Privilege := a_Privilege;
      t_Pos := instr(t_AccessGroups, ';');
    end loop;


    for i in 1..a_SecurityPrivList.count() loop
       app.pkg_debug.putsingleline('Updated Privs ' || a_SecurityPrivList(i).AccessGroupId);
    end loop;
  end;


  /*---------------------------------------------------------------------------
   * SplitToIndexedList() -- PRIVATE
   *   Split a comma-separated string, returning a string-indexed list.
   *-------------------------------------------------------------------------*/
  function SplitToIndexedList (
    a_String                            varchar2
  ) return udt_StringIndexedList is
    t_Pos                               pls_integer;
    t_Index                             pls_integer;
    t_List                              udt_StringIndexedList;
  begin
    t_Pos := 1;
    while t_Pos <= length(a_String) loop
      t_Index := instr(a_String, ',', t_Pos);
      if t_Index > 0 then
        t_List(substr(a_String, t_Pos, t_Index-t_Pos)) := True;
        t_Pos := t_Index + 1;
      else
        t_List(substr(a_String, t_Pos)) := True;
        t_Pos := length(a_String) + 1;
      end if;
    end loop;

    return t_List;
  end SplitToIndexedList;


  /*---------------------------------------------------------------------------
   * SyncInstanceSecurity() -- PRIVATE
   *  Given a list of AccessGroupId/Privilege pairs, synchronize the instance
   * security for the given object and return True if any changes made, False
   * if not changes made.
   *-------------------------------------------------------------------------*/
  function SyncInstanceSecurity (
    a_ObjectId                          udt_Id,
    a_SecurityPrivList    in out nocopy udt_SecurityPrivList
  ) return boolean is
    t_Changes                           boolean;
    t_SecurityOverridden                udt_Boolean;
    t_CurrentPrivList                   udt_SecurityPrivList;
    t_GrantPrivList                     udt_SecurityPrivList;
    t_RevokePrivList                    udt_SecurityPrivList;

    cursor c_ObjectInstanceSecurity is
      select
        o.Name,
        o.AccessGroupId
      from api.ObjectSecurity o
      where o.ObjectId = a_ObjectId
        and o.AccessGroupId <> 2; /* Ignore Super Users */

  begin
    pkg_Debug.Putline('Sync Instance Security start: ' || a_ObjectId);
    select DefaultSecurityOverridden
    into t_SecurityOverridden
    from api.Objects
    where ObjectId = a_ObjectId;

    t_Changes := False;

    -- For debugging purposes
    for j in 1..a_SecurityPrivList.count loop
      app.pkg_debug.putsingleline('Passed Privilege ' || a_SecurityPrivList(j).AccessGroupId);
    end loop;

    pkg_debug.putline('Security Overridden? ' || t_SecurityOverridden);
    if t_SecurityOverridden = 'N' then
      /* Currently not Instance Secured, so grant the specified instance security
         after removing duplicates via SecurityPrivListDifference */
      t_GrantPrivList := SecurityPrivListDifference(a_SecurityPrivList, t_CurrentPrivList);
      --app.pkg_debug.putsingleline('Privileges to be Applied ' || t_GrantPrivList);
      for i in 1..t_GrantPrivList.count loop
        t_Changes := True;
        app.pkg_debug.putsingleline('About to grant access group id: ' || t_GrantPrivList(i).AccessGroupId || ' - Priv: ' || t_GrantPrivList(i).Privilege);
        api.pkg_ObjectUpdate.GrantPrivilege(a_ObjectId,
            t_GrantPrivList(i).AccessGroupId,
            t_GrantPrivList(i).Privilege);
            app.pkg_debug.putsingleline('Granted ' || t_GrantPrivList(i).AccessGroupId);
      end loop;
    else
      /* Modify the current instance security */
      for s in c_ObjectInstanceSecurity loop
        t_CurrentPrivList(t_CurrentPrivList.count+1).AccessGroupId := s.AccessGroupId;
        t_CurrentPrivList(t_CurrentPrivList.count).Privilege := s.Name;
      end loop;

      /* Grant new privileges */
      t_GrantPrivList := SecurityPrivListDifference(a_SecurityPrivList, t_CurrentPrivList);
      for i in 1..t_GrantPrivList.count loop
        t_Changes := True;
        api.pkg_ObjectUpdate.GrantPrivilege(a_ObjectId,
            t_GrantPrivList(i).AccessGroupId, t_GrantPrivList(i).Privilege);
        app.pkg_debug.putsingleline('Granted ' || t_GrantPrivList(i).AccessGroupId);
      end loop;

      /* Revoke old privileges */
      t_RevokePrivList := SecurityPrivListDifference(t_CurrentPrivList, a_SecurityPrivList);
      for i in 1..t_RevokePrivList.count loop
        t_Changes := True;
        api.pkg_ObjectUpdate.RevokePrivilege(a_ObjectId,
            t_RevokePrivList(i).AccessGroupId, t_RevokePrivList(i).Privilege);
        app.pkg_debug.putsingleline('Revoked ' || t_RevokePrivList(i).AccessGroupId);
      end loop;
    end if;
    pkg_Debug.Putline('Sync Instance Security end: ' || a_ObjectId);
    return t_Changes;
  end SyncInstanceSecurity;


  /*---------------------------------------------------------------------------
   * SyncInstanceSecurity() -- PRIVATE
   *  Overload of the SyncInstanceSecurity function that is a procedure as it is
   * very common that you do not want to know if any changes were made.
   *-------------------------------------------------------------------------*/
  procedure SyncInstanceSecurity (
    a_ObjectId                          udt_Id,
    a_SecurityPrivList    in out nocopy udt_SecurityPrivList
  ) is
    t_Changes                           boolean;
  begin
    t_Changes := SyncInstanceSecurity(a_Objectid, a_SecurityPrivList);
  end SyncInstanceSecurity;



  /****************************************************************************
   * ABC Specific Utilities
   *   These utilities are generic and can be used elsewhere.
   ***************************************************************************/

  /*---------------------------------------------------------------------------
   * AdjustByColumn() -- PUBLIC
   *   Takes an ObjectId and two lists of comma separated Access Group
   * Descriptions-first Read, second Modify and adjusts the Instance Security
   * to match the lists.
   *-------------------------------------------------------------------------*/
  procedure AdjustByColumn (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_ReadAccessGroups                  varchar2,
    a_ModifyAccessGroups                varchar2
  ) is
    t_NewSecurity                       udt_SecurityPrivList;
    t_ObjectDefId                       udt_Id;
    t_isDocumentType                    pls_integer;
    t_IsProductType                     char;
  begin

    select ObjectDefId
      into t_ObjectDefId
      from api.objects
      where objectid = a_ObjectId;

    select count(ObjectDefId)
      into t_isDocumentType
      from api.ObjectDefs
      where objectdefTypeId = 5
      and objectdefid = t_ObjectDefId;


    SplitList(a_ReadAccessGroups, 'Read', t_NewSecurity);
    SplitList(a_ModifyAccessGroupS, 'Read', t_NewSecurity);
    SplitList(a_ModifyAccessGroupS, 'Modify', t_NewSecurity);

    /*
      if this is a document type, add the ability to delete the document
      This will prevent instance security from only allowing Super Users delete
      privileges
    */
    if t_isDocumentType > 0 then
      SplitList(a_ModifyAccessGroups, 'Delete', t_NewSecurity);
      SplitList(a_ModifyAccessGroups, 'Lock', t_NewSecurity);
      SplitList(a_ModifyAccessGroups, 'Unlock', t_NewSecurity);
    end if;

    -- We also want to be able to delete Products
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectDefName') = 'o_ABC_Product' then
      SplitList(a_ModifyAccessGroups, 'Delete', t_NewSecurity);
      --SplitList(a_ModifyAccessGroups, 'Lock', t_NewSecurity);
      --SplitList(a_ModifyAccessGroups, 'Unlock', t_NewSecurity);
    end if;

    SyncInstanceSecurity(a_ObjectId, t_NewSecurity);

  end AdjustByColumn;



  /****************************************************************************
   * Public Utilities
   *   These utilities are generic and can be used elsewhere.
   ***************************************************************************/

  /*---------------------------------------------------------------------------
   * NewAccessGroup() -- PUBLIC
   *  This now creates a NewAccessGroup on the fly instead of from a pool.
   *-------------------------------------------------------------------------*/
  function NewAccessGroup
  return udt_id is
    t_AccessGroupId                     udt_Id;
    t_AccessGroupName                   api.AccessGroups.Description%type;
    t_AccessGroupObjectId               udt_Id;
    t_SequenceId                        udt_Id;
  begin
    select s.SequenceId
    into t_SequenceId
    from api.sequences s
    where s.Description = 'NextTokenAccessGroup';

    /* Get the next access group and make sure it exists */
    t_AccessGroupName := gc_AccessGroupPrefix ||
        trim(api.pkg_Utils.GenerateExternalFileNum('[000000000]', t_SequenceId));
    t_AccessGroupId := api.pkg_AccessGroupUpdate.New(t_AccessGroupName);

    t_AccessGroupObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
        'o_AccessGroup', to_char(t_AccessGroupId));

    --Ensure the Access Group does not show up when searching for Roles.
    api.pkg_columnupdate.SetValue(t_AccessGroupObjectId, 'IsAdministratable' ,'N');

    return t_AccessGroupId;
  end NewAccessGroup;

  /*---------------------------------------------------------------------------
   * RelateNewAccessGroup() -- PUBLIC
   *   Create a new Access group to assign to the calling User.
   *   This will be used to grant instance security
   *-------------------------------------------------------------------------*/
  procedure GrantWWWAccessGroupToUser(
    a_UserId                            udt_id
  ) is
    t_AccessGroupId                     udt_Id;
    t_AccessGroupName                   api.AccessGroups.Description%type;
    t_AccessGroupObjectDefId            udt_Id;
    t_AccessGroupObjectId               udt_Id;
    t_SequenceId                        udt_Id;
  begin
    select s.SequenceId
    into t_SequenceId
    from api.sequences s
    where s.Description = 'NextTokenAccessGroup';

    /* Get the next access group and make sure it exists */
    t_AccessGroupName := gc_AccessGroupPrefix ||
        trim(api.pkg_Utils.GenerateExternalFileNum('[000000000]', t_SequenceId));
    t_AccessGroupId := api.pkg_AccessGroupUpdate.New(t_AccessGroupName);
    api.pkg_UserUpdate.AddToAccessGroup(a_UserId, t_AccessGroupId);


    -- Create Relationship between Public User and new Access Group object
    t_AccessGroupObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
        'o_AccessGroup', to_char(t_AccessGroupId));
    extension.pkg_RelationshipUpdate.New(a_UserId, t_AccessGroupObjectId,
        'WWWAccessGroup');

    --Ensure the Access Group does not show up when searching for Roles.
    api.pkg_columnupdate.SetValue(t_AccessGroupObjectId, 'IsAdministratable' ,'N');

  end GrantWWWAccessGroupToUser;

  /*---------------------------------------------------------------------------
   * SyncAccessGroupMembers() -- PUBLIC
   *   This updates the membership of the access group to the provided list.
   *   NOTE: The AccessGroupId required in this instance is the AccessGroupId
   *   from the external object and not the objectid of the AccessGroup in question
   *
   *-------------------------------------------------------------------------*/
  procedure SyncAccessGroupMembers (
    a_AccessGroupId                     udt_Id,
    a_Members             in out nocopy udt_IdList
  ) is
    t_NewMembers                        udt_IdList;
    t_OldMembers                        udt_IdList;
    t_DiffList                          udt_IdList;
    t_UserId                            udt_Id;

  begin
    pkg_Debug.Putline('SyncAccessGroupMembers start: ' || a_AccessGroupId);

    select UserId
    bulk collect into t_OldMembers
    from api.AccessGroupUsers
    where AccessGroupId = a_AccessGroupId;

    t_NewMembers := ConvertIdListToSparse(a_Members);
    t_OldMembers := ConvertIdListToSparse(t_OldMembers);

    /* Delete all the existing members that are not to be kept */
    t_DiffList := SparseIdListDifference(t_OldMembers, t_NewMembers);
    t_UserId := t_DiffList.first;
    while t_UserId is not null loop
      api.pkg_UserUpdate.RemoveFromAccessGroup(t_UserId, a_AccessGroupId);
      t_UserId := t_DiffList.next(t_UserId);
    end loop;

    /* Add the new members */
    t_DiffList := SparseIdListDifference(t_NewMembers, t_OldMembers);
    t_UserId := t_DiffList.first;
    --api.pkg_errors.RaiseError(-20000,t_UserId);
    while t_UserId is not null loop
      app.pkg_Debug.putsingleline(t_userId);
      api.pkg_UserUpdate.AddToAccessGroup(t_UserId, a_AccessGroupId);
      t_UserId := t_DiffList.next(t_UserId);
    end loop;
    pkg_Debug.Putline('SyncAccessGroupMembers end: ' || a_AccessGroupId);
  end SyncAccessGroupMembers;

  /*---------------------------------------------------------------------------
   * UserHasPrivilege() -- PUBLIC
   *   This checks if the user passed in has the requested privilege on the
   *   object requested
   *-------------------------------------------------------------------------*/
  function UserHasPrivilege (
    a_ObjectId                          udt_Id,
    a_UserId                            udt_Id,
    a_Privilege                         varchar2
  ) return boolean is
    t_PrivCount                         number;
  begin

    select count(1)
      into t_PrivCount
      from api.ObjectSecurity os
      join api.AccessGroups ag
        on ag.AccessGroupId = os.AccessGroupId
      join api.AccessGroupUsers agu
        on agu.AccessGroupId = ag.AccessGroupId
     where os.ObjectId = a_ObjectId
       and os.Name = a_Privilege
       and agu.UserId = a_UserId;

    return t_PrivCount > 0;
  end;

  /*---------------------------------------------------------------------------
   * ApplyInstanceSecurity () - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ApplyInstanceSecurity(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_ReadAccessGroups                  varchar2(4000);
    t_ModifyAccessGroups                varchar2(4000);
  begin

    t_ReadAccessGroups := api.pkg_ColumnQuery.Value(a_objectId, 'InstanceSecureReadGroups');
    t_ModifyAccessGroups := api.pkg_ColumnQuery.Value(a_objectId, 'InstanceSecureModifyGroups');

    -- Apply instance Security to the calling object/job/document
    abc.pkg_InstanceSecurity.AdjustByColumn(a_ObjectId, a_AsOfDate, t_ReadAccessGroups, t_ModifyAccessGroups);

  end ApplyInstanceSecurity;

  /*---------------------------------------------------------------------------
   * ReApplySecurityForJob () - PRIVATE
   * Applies security for a job and its assocaited payments/documents.
   *-------------------------------------------------------------------------*/
  procedure ReApplySecurityForJob(
    a_JobId                             udt_Id,
    a_DocId                             udt_Id,
    a_AppSummaryId                      udt_Id
  ) is
  begin
    if a_Jobid is not null then
      abc.pkg_instancesecurity.ApplyInstanceSecurity(a_JobId, sysdate);
      -- Run instance security on all payments for the job
      for p in (
          select distinct p.ObjectId paymentobjectid
            from api.fees f
            join api.feetransactions ft on f.FeeId = ft.FeeId
            join api.paymenttransactions pt on ft.TransactionId = pt.TransactionId
            join api.payments ap on ap.PaymentId = pt.PaymentId
            join api.pkg_simplesearch.CastableObjectsByIndex('o_payment', 'paymentid', ap.paymentid) x on 1=1
            join query.o_payment p on p.objectid = x.objectid
           where f.jobid = a_jobid
          ) loop
        abc.pkg_instancesecurity.ApplyInstanceSecurity(p.paymentobjectid, sysdate);
      end loop;
    end if;
    if a_DocId is not null then
      abc.pkg_instancesecurity.ApplyInstanceSecurity(a_DocId, sysdate);
    end if;
    if a_AppSummaryId is not null then
      abc.pkg_instancesecurity.ApplyInstanceSecurity(a_AppSummaryId, sysdate);
    end if;

  end ReApplySecurityForJob;

  /*---------------------------------------------------------------------------
   * RevokeSecurityForJob () - PRIVATE
   * Revokes security for a job and its assocaited payments/documents.
   *-------------------------------------------------------------------------*/
  procedure RevokeSecurityForJob(
    a_JobId                             udt_Id,
    a_DocId                             udt_Id,
    a_AppSummaryId                      udt_Id,
    a_AccessGroupId                     udt_Id
  ) is
  begin
    if a_Jobid is not null then
      api.pkg_objectupdate.RevokePrivilege(a_JobId, a_AccessGroupId, 'Read');
      -- Revoke instance security from all payments for the job
      for p in (
          select distinct p.ObjectId paymentobjectid
            from api.fees f
            join api.feetransactions ft on f.FeeId = ft.FeeId
            join api.paymenttransactions pt on ft.TransactionId = pt.TransactionId
            join api.payments ap on ap.PaymentId = pt.PaymentId
            join api.pkg_simplesearch.CastableObjectsByIndex('o_payment', 'paymentid', ap.paymentid) x on 1=1
            join query.o_payment p on p.objectid = x.objectid
           where f.jobid = a_jobid
          ) loop
      api.pkg_objectupdate.RevokePrivilege(p.paymentobjectid, a_AccessGroupId, 'Read');
      end loop;
    end if;
    if a_DocId is not null then
      api.pkg_objectupdate.RevokePrivilege(a_DocId, a_AccessGroupId, 'Read');
    end if;
    if a_AppSummaryId is not null then
      api.pkg_objectupdate.RevokePrivilege(a_AppSummaryId, a_AccessGroupId,
          'Read');
    end if;

  end RevokeSecurityForJob;

  /*---------------------------------------------------------------------------
   * ReApplyInstanceSecurityForUser () - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ReApplyInstanceSecurityForUser (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_LEObjectId                        number;
    t_PermitEndPointId                  udt_Id :=
        api.pkg_configquery.EndPointIdForName('o_Abc_LegalEntity', 'Permit');
    t_ObjList                           udt_IdList;
  begin

    -- Find the Legal Entity that had a new user added
    select max(r.FromObjectId)
    into t_LEObjectId
    from
      api.relationships r
      join api.relationshipdefs rd
          on rd.RelationshipDefId = r.RelationshipDefId
    where r.RelationshipId = a_RelationshipId
      and rd.FromObjectDefId =
          api.pkg_configquery.ObjectDefIdForName('o_ABC_LegalEntity')
      and r.ToEndPoint = '0';

    if t_LEObjectId is not null then
      -- Jobs and their license certificates (there is no instance security on
      -- Reinstate job type)
      for i in (
          select
            p.LicenseCertificateDocumentId DocId,
            r2.NewApplicationJobId JobId,
            api.pkg_columnquery.Value(r2.NewApplicationJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from
            query.r_ABC_LicenseLicenseeLE r
            join query.r_abc_newapplicationlicense r2
                on r2.LicenseObjectId = r.LicenseObjectId
            left join query.p_abc_sendlicense p
                on p.jobid = r2.NewApplicationJobId
          where r.LegalEntityObjectId = t_LEObjectId
          union all
          -- Renewal Application
          select
            p.LicenseCertificateDocumentId DocId,
            r5.RenewJobId JobId,
            api.pkg_columnquery.Value(r5.RenewJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from
            query.r_ABC_LicenseLicenseeLE r
            join query.r_ABC_LicenseToRenewJobLicense r5
                on r5.LicenseObjectId = r.LicenseObjectId
            left join query.p_abc_sendlicense p
                on p.jobid = r5.RenewJobId
          where r.LegalEntityObjectId = t_LEObjectId
          union all
          -- Amendment Application
          select
            p.LicenseCertificateDocumentId DocId,
            r6.AmendJobId JobId,
            api.pkg_columnquery.Value(r6.AmendJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from
            query.r_ABC_LicenseLicenseeLE r
            join query.r_ABC_LicenseToAmendJobLicense r6
                on r6.LicenseObjectId = r.LicenseObjectId
            left join query.p_abc_sendlicense p
                on p.jobid = r6.AmendJobId
          where r.LegalEntityObjectId = t_LEObjectId
          union all
          -- Product Registration Application
          select
            null DocId,
            prtr.ProdRegJobId JobId,
            api.pkg_columnquery.Value(prtr.ProdRegJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from query.r_ABC_ProdRegToRegistrant prtr
          where prtr.RegistrantObjectId = t_LEObjectId
          union all
          -- Product Registration Amendment
          select
            null DocId,
            atr.ProdRegJobId JobId,
            api.pkg_columnquery.Value(atr.ProdRegJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from query.r_ABC_PRAmendToRegistrant atr
          where atr.RegistrantObjectId = t_LEObjectId
          union all
          -- Product Registration Renewal
          select
            null DocId,
            prtr1.ProdRegJobId JobId,
            api.pkg_columnquery.Value(prtr1.ProdRegJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from query.r_ABC_ProdRenewalToRegistrant prtr1
          where prtr1.RegistrantObjectId = t_LEObjectId
          union all
          -- Permit Application
          select
            null DocId,
            pap.PermitApplicationId JobId,
            null AppSummaryId
          from query.r_ABC_PermitAppPermittee pap
          where pap.PermitteeId = t_LEObjectId
          union all
          -- Permit Renewal
          Select distinct
            null DocId,
            PermitRenewJobId JobId,
            null AppSummaryId
          from (
            --LE to Permit
            Select
              rtp.PermitRenewJobId
            from query.r_ABC_PermitPermittee rp
            join query.r_ABC_PermitToRenewJobPermit rtp
                on rp.PermitObjectId = rtp.PermitToRenewObjectId --Permit to PermitRenewal
            where rp.PermitteeObjectId = t_LEObjectId
            union all
            --LE to License to Permit
            Select
              rtp.PermitRenewJobId
            from query.r_ABC_LicenseLicenseeLE ll
            join query.r_PermitLicense pl
                on pl.LicenseObjectId = ll.LicenseObjectId --License to Permit
            join query.r_ABC_PermitToRenewJobPermit rtp
                on pl.PermitObjectId = rtp.PermitToRenewObjectId --Permit to PermitRenewal
            where ll.LegalEntityObjectId = t_LEObjectId
            union all
            --LE to TAP to Permit
            Select
              rtp.PermitRenewJobId
            from query.r_ABC_TAPPermitPermittee tpp
            join query.r_ABC_TAPPermit tap
                on tap.TAPPermitObjectId = tpp.PermitObjectId --TAP to Permit
            join query.r_ABC_PermitToRenewJobPermit rtp
                on tap.PermitObjectId = rtp.PermitToRenewObjectId --Permit to PermitRenewal
            where tpp.PermitteeObjectId_TAP = t_LEObjectId)
          union all
          -- Petition
          select
            null DocId,
            lp.PetitionId JobId,
            null AppSummaryId
          from
            query.r_abc_licenselicenseele ll
            join query.r_abc_LicensePetition lp
                on lp.LicenseId = ll.LicenseObjectId
          where ll.LegalEntityObjectId = t_LEObjectId
          ) loop
        ReApplySecurityForJob(i.JobId, i.DocId, i.AppSummaryId);
      end loop;
      
      -- Get the associated Appeals to reapply security on
      t_ObjList := api.pkg_Search.ObjectsByRelColumnValue('j_ABC_Appeal',
          'LegalEntityProc', 'ObjectId', t_LEObjectId);
      for c in 1..t_ObjList.count loop
        ReApplySecurityForJob(t_ObjList(c), null, null);
      end loop;

      -- Additional lookup on Permit Application since no relationship view exists.
      -- This is done for Permit Application jobs in 'New' status.
      t_ObjList := api.pkg_simplesearch.ObjectsByIndex('j_ABC_PermitApplication',
          'OnlineUseLegalEntityObjectId', t_LEObjectId);
      for n in 1..t_ObjList.count loop
        abc.pkg_instancesecurity.ApplyInstanceSecurity(t_ObjList(n), sysdate);
      end loop;

      -- Product Registrants
      for x in (
          select pr.ProductObjectId ProductId
          from query.r_ABC_ProductRegistrant pr
          where pr.RegistrantObjectId = t_LEObjectId
          ) loop
        abc.pkg_instancesecurity.ApplyInstanceSecurity(x.ProductId, sysdate);
      end loop;
      -- Product Distributors
      for x in (
          select r.ProdictId ProductId
          from query.r_Legalentitydist r
          where r.LegalEntityId = t_LEObjectId
          ) loop
        abc.pkg_instancesecurity.ApplyInstanceSecurity(x.ProductId, sysdate);
      end loop;

      -- Permit Certificate
      for x in (
          select api.pkg_columnquery.Value (p.ObjectId,'PermitDocumentId')
              CertificateId
          from
            query.r_ABC_UserLegalEntity ule
            join table(abc.pkg_abc_procedurallookup.PermitsForPermitee(t_LEObjectId,
                t_PermitEndPointId)) p
                on 1 = 1
          where ule.LegalEntityObjectId = t_LEObjectId
          ) loop
        if x.CertificateId is not null then
           abc.pkg_instancesecurity.ApplyInstanceSecurity(x.CertificateId, sysdate);
        end if;
      end loop;

      -- Petition Amendment
      t_ObjList := api.pkg_Search.ObjectsByRelColumnValue('j_ABC_PetitionAmendment',
          'LegalEntity', 'ObjectId', t_LEObjectId);
      for i in 1..t_ObjList.count loop
        ReApplySecurityForJob(t_ObjList(i), null, null);
      end loop;
    end if;

  end ReApplyInstanceSecurityForUser;

  /*---------------------------------------------------------------------------
   * RevokeInstanceSecurityForUser() - PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RevokeInstanceSecurityForUser (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
 ) is
    t_LEObjectId                        number;
    t_UserId                            number;
    t_AccessGroupId                     number;
    t_PermitEndPointId                  udt_Id :=
        api.pkg_configquery.EndPointIdForName('o_Abc_LegalEntity', 'Permit');
    t_ObjList                           udt_IdList;
  begin

    -- Find the Legal Entity and User of the relationship being removed
    select
      r.FromObjectId,
      r.ToObjectId
    into
      t_LEObjectId,
      t_UserId
    from
      api.relationships r
      join api.relationshipdefs rd
          on rd.RelationshipDefId = r.RelationshipDefId
    where r.RelationshipId = a_RelationshipId
      and rd.FromObjectDefId =
          api.pkg_configquery.ObjectDefIdForName('o_ABC_LegalEntity')
      and r.ToEndPoint = '0';

    -- Find the WWW Access group of the user being removed
    select a.AccessGroupId
    into t_AccessGroupId
    from api.accessgroups a
    where a.Description = api.pkg_columnquery.Value(t_UserId, 'WWWAccessGroupName');

    -- Find the Jobs and License Certificates related to this Legal Entity and
    -- revoke the Read priv for the user
    if t_LEObjectId is not null then
      -- Jobs and their license certificates (there is no instance security on
      -- Reinstate job type)
      for i in (
          select
            p.LicenseCertificateDocumentId DocId,
            r2.NewApplicationJobId JobId,
            api.pkg_columnquery.Value(r2.NewApplicationJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from
            query.r_ABC_LicenseLicenseeLE r
            join query.r_abc_newapplicationlicense r2
                on r2.LicenseObjectId = r.LicenseObjectId
            left join query.p_abc_sendlicense p
                on p.jobid = r2.NewApplicationJobId
          where r.LegalEntityObjectId = t_LEObjectId
          union all
          -- Renewal Application
          select
            p.LicenseCertificateDocumentId DocId,
            r5.RenewJobId JobId,
            api.pkg_columnquery.Value(r5.RenewJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from
            query.r_ABC_LicenseLicenseeLE r
          join query.r_ABC_LicenseToRenewJobLicense r5
              on r5.LicenseObjectId = r.LicenseObjectId
          left join query.p_abc_sendlicense p
              on p.jobid = r5.RenewJobId
          where r.LegalEntityObjectId = t_LEObjectId
          union all
          -- Amendment Application
          select
            p.LicenseCertificateDocumentId DocId,
            r6.AmendJobId JobId,
            api.pkg_columnquery.Value(r6.AmendJobId,
                'ApplicationSummaryObjectId') AppSummaryId
          from
            query.r_ABC_LicenseLicenseeLE r
            join query.r_ABC_LicenseToAmendJobLicense r6
                on r6.LicenseObjectId = r.LicenseObjectId
            left join query.p_abc_sendlicense p on p.jobid = r6.AmendJobId
          where r.LegalEntityObjectId = t_LEObjectId
          union all
          -- Product Registration Application
          select
            null DocId,
            prtr.ProdRegJobId JobId,
            null AppSummaryId
          from query.r_ABC_ProdRegToRegistrant prtr
          where prtr.RegistrantObjectId = t_LEObjectId
          union all
          -- Product Registration Amendment
          select
            null DocId,
            atr.ProdRegJobId JobId,
            null AppSummaryId
          from query.r_ABC_PRAmendToRegistrant atr
          where atr.RegistrantObjectId = t_LEObjectId
          union all
          -- Product Registration Renewal
          select
            null DocId,
            prtr1.ProdRegJobId JobId,
            null AppSummaryId
          from query.r_ABC_ProdRenewalToRegistrant prtr1
          where prtr1.RegistrantObjectId = t_LEObjectId
          union all
          -- Permit Application
          select
            null DocId,
            pap.PermitApplicationId JobId,
            null AppSummaryId
          from query.r_ABC_PermitAppPermittee pap
          where pap.PermitteeId = t_LEObjectId
          union all
          -- Permit Renewal
          Select distinct
            null DocId,
            PermitRenewJobId JobId,
            null AppSummaryId
          from (
            --LE to Permit
            Select
              rtp.PermitRenewJobId
            from query.r_ABC_PermitPermittee rp
            join query.r_ABC_PermitToRenewJobPermit rtp
                on rp.PermitObjectId = rtp.PermitToRenewObjectId --Permit to PermitRenewal
            where rp.PermitteeObjectId = t_LEObjectId
            union all
            --LE to License to Permit
            Select
              rtp.PermitRenewJobId
            from query.r_ABC_LicenseLicenseeLE ll
            join query.r_PermitLicense pl
                on pl.LicenseObjectId = ll.LicenseObjectId --License to Permit
            join query.r_ABC_PermitToRenewJobPermit rtp
                on pl.PermitObjectId = rtp.PermitToRenewObjectId --Permit to PermitRenewal
            where ll.LegalEntityObjectId = t_LEObjectId
            union all
            --LE to TAP to Permit
            Select
              rtp.PermitRenewJobId
            from query.r_ABC_TAPPermitPermittee tpp
            join query.r_ABC_TAPPermit tap
                on tap.TAPPermitObjectId = tpp.PermitObjectId --TAP to Permit
            join query.r_ABC_PermitToRenewJobPermit rtp
                on tap.PermitObjectId = rtp.PermitToRenewObjectId --Permit to PermitRenewal
            where tpp.PermitteeObjectId_TAP = t_LEObjectId)
          union all
          -- Petition
          select
            null DocId,
            lp.PetitionId JobId,
            null AppSummaryId
          from
            query.r_abc_licenselicenseele ll
            join query.r_abc_LicensePetition lp
                on lp.LicenseId = ll.LicenseObjectId
          where ll.LegalEntityObjectId = t_LEObjectId
          ) loop
        RevokeSecurityForJob(i.JobId, i.DocId, i.AppSummaryId, t_AccessGroupId);
      end loop;
      
      -- Get the associated Appeals to reapply security on
      t_ObjList := api.pkg_Search.ObjectsByRelColumnValue('j_ABC_Appeal',
          'LegalEntityProc', 'ObjectId', t_LEObjectId);
      for c in 1..t_ObjList.count loop
        RevokeSecurityForJob(t_ObjList(c), null, null, t_AccessGroupId);
      end loop;

      -- Additional lookup on Permit Application since no relationship view exists.
      -- This is done for Permit Application jobs in 'New' status.
      t_ObjList := api.pkg_simplesearch.ObjectsByIndex('j_ABC_PermitApplication',
          'OnlineUseLegalEntityObjectId', t_LEObjectId);
      for n in 1..t_ObjList.count loop
        api.pkg_objectupdate.RevokePrivilege(t_ObjList(n), t_AccessGroupId, 'Read');
      end loop;

      -- Product Registrants
      for x in (
          select pr.ProductObjectId ProductId
          from query.r_ABC_ProductRegistrant pr
          where pr.RegistrantObjectId = t_LEObjectId
          ) loop
        api.pkg_objectupdate.RevokePrivilege(x.ProductId, t_AccessGroupId, 'Read');
      end loop;
      -- Product Distributors
      for x in (
          select r.ProdictId ProductId
          from query.r_Legalentitydist r
          where r.LegalEntityId = t_LEObjectId
          ) loop
        api.pkg_objectupdate.RevokePrivilege(x.ProductId, t_AccessGroupId, 'Read');
      end loop;

      --Permit Certificate
      for x in (
          select api.pkg_columnquery.Value (p.ObjectId,'PermitDocumentId')
              CertificateId
          from
            query.r_ABC_UserLegalEntity ule
            join table(abc.pkg_abc_procedurallookup.PermitsForPermitee(t_LEObjectId,
                t_PermitEndPointId)) p on 1 = 1
          where ule.LegalEntityObjectId = t_LEObjectId
          ) loop
        if x.CertificateId is not null then
           api.pkg_objectupdate.RevokePrivilege(x.CertificateId, t_AccessGroupId,
               'Read');
        end if;
      end loop;

      -- Petition Amendment
      t_ObjList := api.pkg_Search.ObjectsByRelColumnValue('j_ABC_PetitionAmendment',
          'LegalEntity', 'ObjectId', t_LEObjectId);
      for i in 1..t_ObjList.count loop
        RevokeSecurityForJob(t_ObjList(i), null, null, t_AccessGroupId);
      end loop;
    end if;

   end RevokeInstanceSecurityForUser;

end pkg_InstanceSecurity;
/
