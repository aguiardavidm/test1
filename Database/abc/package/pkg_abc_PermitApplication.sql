create or replace package pkg_ABC_PermitApplication is

  subtype udt_id is api.pkg_definition.udt_id;
  subtype udt_idlist is api.pkg_definition.udt_idlist;

 /*---------------------------------------------------------------------------
  * PostVerify()
  *-------------------------------------------------------------------------*/
  procedure PostVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * CopyPermitRels()
  *-------------------------------------------------------------------------*/
  procedure CopyPermitRels(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * ApprovePermit()
   *   Approves the permit and sets it to Active. Run from workflow on the
   * Permit App job.
   *-------------------------------------------------------------------------*/
  procedure ApprovePermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * OnlineSubmission()
  *-------------------------------------------------------------------------*/
  procedure OnlineSubmission(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * MaintainLEPermitAppXref()
  *-------------------------------------------------------------------------*/
  procedure MaintainLEPermitAppXref(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * PrintPermit()
  *-------------------------------------------------------------------------*/
  procedure PrintPermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * WithdrawApplication()
  *-------------------------------------------------------------------------*/
  procedure WithdrawApplication(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * DenyApplication()
   *  Runs Denial actions. Run from workflow on the Permit Renewal job
   *-------------------------------------------------------------------------*/
  procedure DenyApplication(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * CopyCertificateRelToPermit()
  *-------------------------------------------------------------------------*/
  procedure CopyCertificateRelToPermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * ReassignReviewProcesses()
  *-------------------------------------------------------------------------*/
  procedure ReassignReviewProcesses;

end pkg_ABC_PermitApplication;
/
grant execute
on abc.pkg_abc_permitapplication
to posseextensions;

create or replace package body pkg_ABC_PermitApplication is

  -- Author  : Paul Orstad
  -- Created : 8/5/2015

 /*---------------------------------------------------------------------------
  * PostVerify()
  *-------------------------------------------------------------------------*/
  procedure PostVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_DefaultClerkId                    udt_Id;
    t_DefaultSupervisorId               udt_Id;
    t_PermitEPId                        udt_Id :=
        api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'AppToPermit');
    t_DefaultClerkEPId                  udt_Id :=
        api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'DefaultClerk');
    t_DefaultSupervisorEPId             udt_Id :=
        api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'DefaultSupervisor');
    t_RelId                             udt_Id;
  begin
    -- Set Default users if not already selected
    -- If the permit exists, it may not exist yet for a public app
    if api.pkg_objectquery.RelatedObjects(a_ObjectId, t_PermitEPId).count > 0 then
      if api.pkg_objectquery.RelatedObjects(a_ObjectId, t_DefaultClerkEPId).count = 0 then
        select r2.DefaultClerkId
          into t_DefaultClerkId
          from query.r_ABC_PermitAppPermitTypeIND r
          join query.r_abc_permittypedefaultclerk r2 on r2.PermitTypeId = r.PermitTypeId
         where r.PermitApplicationId = a_ObjectId;
        t_RelId := api.pkg_relationshipupdate.New(t_DefaultClerkEPId, a_ObjectId, t_DefaultclerkId);
      end if;
      if api.pkg_objectquery.RelatedObjects(a_ObjectId, t_DefaultSupervisorEPId).count = 0 then
        select r2.PermittingSupervisorId
          into t_DefaultSupervisorId
          from query.r_ABC_PermitAppPermitTypeIND r
          join query.r_SupervisorPermitType r2 on r2.PermitTypeId = r.PermitTypeId
         where r.PermitApplicationId = a_ObjectId;
        t_RelId := api.pkg_relationshipupdate.New(t_DefaultSupervisorEPId, a_ObjectId,
            t_DefaultSupervisorId);
      end if;
    end if;
    -- Generate Fees if created online,
    -- Do not create fees if approved and requires a vehicle
    if api.pkg_columnquery.value(a_ObjectId, 'EnteredOnline') = 'Y' then
        if api.pkg_ColumnQuery.value(a_ObjectID, 'OnlineRequiresVehicle') = 'Y'
            and api.pkg_ColumnQuery.value(a_ObjectID, 'StatusName') In ('APP','NOTIF')  then
          null;
        else
          feescheduleplus.pkg_PosseRegisteredProcedures.GenerateFees(a_ObjectId, null, 'N');
        end if;
    end if;
  end PostVerify;

 /*---------------------------------------------------------------------------
  * CopyPermitRels()
  * Copies relationships from the created permit to the job,
  *   runs on completion of the Enter Permit Application process.
  *   Last update by Paul Orstad 08/13/2015
  *-------------------------------------------------------------------------*/
  procedure CopyPermitRels(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PermitId                          udt_id;
    t_RelatedObjId                      udt_Id;
    t_ToEndPointId                      udt_Id;
    t_RelId                             udt_Id;
    t_PermitteeId                       udt_id;
    t_PermitTypeId                      udt_id;
    t_InsigniaPermit                    varchar2(50);
  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_PermitId := api.pkg_columnquery.value(t_JobId, 'PermitObjectId');

    if api.pkg_columnquery.Value(t_JobId, 'ApplicationReceivedDate') is null or
       (api.pkg_columnquery.DateValue(t_JobId, 'ApplicationReceivedDate') > sysdate) then
       api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Application Received Date that is not in the future.');
    end if;

    --Make sure permit data is valid before we continue
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'N');
    abc.pkg_abc_permit.ValidatePermitData(t_PermitId, sysdate);
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'Y');

    if api.pkg_columnquery.Value(t_PermitId, 'RequiresVehicle') = 'N' or
       api.pkg_columnquery.Value(t_PermitId, 'InsigniaPermitObjectId') is null then
    --Permitee
       t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'Permittee');
       if api.pkg_columnquery.value(t_PermitId, 'TAPPermitNumber') is null then
         if api.pkg_columnquery.value(t_PermitId, 'PermitteeIsLicensee') = 'Y' and api.pkg_columnquery.value(t_PermitId, 'LicenseNumber') is not null then
           t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, api.pkg_columnquery.value(t_PermitId, 'PermitteeLicenseeObjectId'));
         else
           t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, api.pkg_columnquery.value(t_PermitId, 'PermitteeObjectId'));
         end if;
       else
           t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, api.pkg_columnquery.value(t_PermitId, 'PermitteeObjectId_TAP'));
       end if;

    --Set Permitee on Permit for the Permitee rel on the job if it does not already exist on the permit
       if extension.pkg_relutils.RelExists(t_PermitId, 'Permittee') = 'N' then
         t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Permittee');
         for i in (select p.PermitteeId
                   from   query.r_ABC_PermitAppPermittee p
                   where  p.PermitApplicationId = t_JobId) loop
           t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.permitteeid);
         end loop;
       end if;
    end if;

    --County
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'County');
    for i in (select r.CountyObjectId
                from query.r_ABC_PermitCounty r
               where r.PermitObjectId = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.CountyObjectId);
    end loop;

    --Municipality
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'Municipality');
    for i in (select r.MunicipalityObjectId
                from query.r_ABC_PermitMunicipality r
               where r.PermitObjectId = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.MunicipalityObjectId);
    end loop;

    --License
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'License');
    for i in (select r.LicenseObjectId
                from query.r_PermitLicense r
               where r.PermitObjectId = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.licenseobjectid);
    end loop;

    --Seller's License
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'SellersLicense');
    for i in (select r.LicenseObjectId
                from query.r_SellersLicense r
               where r.PermitObjectId = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.licenseobjectid);
    end loop;
    
    --Buyer's Permit
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'BuyersPermit');
    for i in (select r.PermitToBuyers
                from query.r_BuyersPermit r
               where r.BuyersPermit = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.PermitToBuyers);
    end loop;

    --Co-Op License
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'CoOpLicense');
    for i in (select r.CoOpMemberObjectId
                from query.r_ABC_PermitCoOpLicenses r
               where r.PermitObjectId = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.CoOpMemberObjectId);
    end loop;

    --Additional Permit
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'Permit');
    for i in (select r.AsscPermitObjectId
                from query.r_ABC_AssociatedPermit r
               where r.PrimaryPermitObjectId = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.AsscPermitObjectId);
    end loop;

    --TAP Permit
    /*t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'OnlineTAPPermit');
    for i in (select r.TAPPermitObjectId
                from query.r_ABC_TAPPermit r
               where r.PermitObjectId = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.TAPPermitObjectId);
    end loop;*/
    --PermitType
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication', 'PermitType');
    for i in (select r.PermitTypeId
                from query.r_ABC_PermitPermitType r
               where r.PermitId = t_PermitId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_JobId, i.PermitTypeId);
    end loop;
  end CopyPermitRels;

  /*---------------------------------------------------------------------------
   * ApprovePermit() -- PUBLIC
   *   Approves the permit and sets it to Active. Run from workflow on the
   * Permit App job.
   *-------------------------------------------------------------------------*/
  procedure ApprovePermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PermitId                          udt_id;
    t_LicenseId                         udt_id;
    t_LicenseIdNew                      udt_id;
    t_LicenseNumber                     varchar2(50);
    t_RelationshipId                    udt_id;
    t_EffectiveDate                     date;
    t_ExpirationDate                    date;
    t_ReportId                          udt_Id;
    t_EndPointId                        udt_id :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'License');
    t_InsigniaPermitId                  udt_id;
    t_ToEndPointId                      udt_id;
    t_RelId                             udt_id;
    t_VehicleId                         udt_id;
    t_PermitteeId                       udt_id;
    t_PermitTypeId                      udt_id;
    t_VehicleDefId                      udt_id :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_Vehicle');
    t_CancelEndpointId                  integer :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
            'PermitCancellation');
    t_ApplicationEndpointId             integer :=
        api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication',
            'AppToPermit');
    t_ObjectDefId                       integer :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_PermitCancellation');
    t_RenewEndpointId                   integer :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitRenewal');
    t_Reason                            varchar2(400);
    t_ObjectId                          udt_id;
    t_ProcessId                         udt_id;
    t_MonitorReviewProcessId            udt_id;
    t_OpenProcessIds                    udt_IdList;
    t_ProcessOutcome                    varchar2(4000);
    -- DefId for Process to create
    t_ProcessDefId                      number :=
        api.pkg_configquery.ObjectDefIdForName('p_ChangeStatus');
    t_InsigniaNumberList                udt_IdList;
    t_Vehicles                          number;
    t_AssociatedPermitId                udt_id;
    t_TAPObjectId                       udt_id;
    t_RenewalCount                      integer;
  begin

    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_PermitId := api.pkg_columnquery.value(t_JobId, 'PermitObjectId');
    t_LicenseId := api.pkg_columnquery.value(t_PermitId, 'LicenseObjectId');
    t_LicenseNumber := api.pkg_columnquery.value(t_LicenseId, 'LicenseNumber');
    t_PermitteeId := api.pkg_columnquery.value(t_PermitId, 'PermitteeObjectId');
    t_PermitTypeId := api.pkg_columnquery.value(t_PermitId, 'PermitTypeObjectId');

    if api.pkg_columnquery.Value(t_PermitId, 'State') = 'Active' then
      return;
    end if;
    -- Check if all fees are paid
    if api.pkg_columnquery.Value(t_JobId, 'HasNonZeroBalances') = 'Y' then
      api.pkg_errors.RaiseError(-20000, 'Before you go on, ensure all fees are'
          || ' paid.');
    end if;

    if api.pkg_columnQuery.VALUE (t_PermitId, 'RequiresVehicle') = 'Y' then
      -- Vehicles tab
      if api.pkg_columnQuery.NumericValue(t_PermitId, 'TotalNumberofVehicles') < 1
      then
        api.pkg_errors.RaiseError(-20000,  'At least one Vehicle is required.');
      else
        -- All detail fields are mandatory
        -- Find Vehicles without Make/Model
        select count(*)
        into t_Vehicles
        from
          query.O_ABC_Vehicle v
          join query.r_Abc_PermitVehicle pv
              on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_PermitId
             and v.MakeModelYear is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'Make/Model/Year information is '
          || 'required.');
        end if;
        -- Find Vehicles without License Plate
        select count(*)
        into t_Vehicles
        from
          query.O_ABC_Vehicle v
          join query.r_Abc_PermitVehicle pv
              on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_PermitId
            and v.StateRegistration is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'License Plate information is '
          || 'required.');
        end if;
        -- Find Vehicles without State
        select count(*)
        into t_Vehicles
        from
          query.o_abc_vehicle v
          join query.r_Abc_PermitVehicle pv
              on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_PermitId
             and v.StateOfRegistration is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'State of Registration is required.');
        end if;
        -- Find Vehicles without VIN
        select count(*)
        into t_Vehicles
        from
          query.O_ABC_Vehicle v
          join query.r_Abc_PermitVehicle pv
              on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_PermitId
             and v.vin is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'VIN is required.');
        end if;
        -- Find Vehicles without Owned/Leased information
        select count(*)
        into t_Vehicles
        from
          query.O_ABC_Vehicle v
          join query.r_Abc_PermitVehicle pv
              on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_PermitId
             and v.OwnedOrLeasedLimousine is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'Owned/Leased information is required.');
        end if;
      end if;
      -- See if a Transit Permit is used. If so, we will use that Permit instead of
      -- the newly created and copy the Vehicle(s) from the new to the existing
      -- Permit.

      -- Set Effective Date to see if we need to consolidate a permit or not.
      api.pkg_columnupdate.SetValue(t_PermitId, 'EffectiveDate', sysdate);
      t_ExpirationDate := api.pkg_columnquery.datevalue(t_PermitId, 'CalculatedExpirationDate');

      if api.pkg_columnquery.Value(t_PermitId, 'InsigniaPermitObjectId') is null then
        if t_LicenseId is not null then
          for p in (
                    select r.PermitObjectId
                    from query.r_Permitlicense r
                    where r.LicenseObjectId = t_LicenseId
          ) loop
            if api.pkg_columnquery.Value(p.permitobjectid, 'State') = 'Active'
                and api.pkg_columnquery.Value(p.permitobjectid, 'PermitTypeObjectId')
                    = t_PermitTypeId and
                        api.pkg_columnquery.DateValue(p.PermitObjectId, 'ExpirationDate')
                            = t_expirationDate then
              t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                  'InsigniaPermit');
              t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId,
                  p.permitobjectid);
              exit;
            end if;
          end loop;
        end if;
        t_TAPObjectId := api.pkg_columnquery.Value(t_PermitId, 'PermitObjectId_TAP');
        if t_TAPObjectId is not null
            and api.pkg_columnquery.Value(t_TAPObjectId, 'State') = 'Active' then
          for p in (
                    select r.PermitObjectId
                    from query.r_Abc_Tappermit r
                    where r.TAPPermitObjectId = t_TAPObjectId
          ) loop
            if api.pkg_columnquery.Value(p.permitobjectid, 'State') = 'Active'
                and api.pkg_columnquery.Value(p.permitobjectid, 'PermitTypeObjectId')
                    = t_PermitTypeId and
                        api.pkg_columnquery.DateValue(p.PermitObjectId, 'ExpirationDate')
                            = t_expirationDate then
              t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                                                                      'InsigniaPermit');
              t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId,
                                                        t_PermitId,
                                                        p.permitobjectid);
              exit;
            end if;
          end loop;
        elsif t_TAPObjectId is not null then
          api.pkg_errors.RaiseError(-20000, 'NO Active Temporary Authorization to '
              || 'Operate permit to associate.');
        end if;

        t_AssociatedPermitId := api.pkg_columnquery.Value(t_PermitId,
            'AssociatedPermitObjectId');
        if t_AssociatedPermitId is not null
            and api.pkg_columnquery.Value(t_AssociatedPermitId, 'State') = 'Active' then
          for p in (
                    select r.PrimaryPermitObjectId PermitObjectId
                    from query.r_ABC_AssociatedPermit r
                    where  r.AsscPermitObjectId = t_AssociatedPermitId
          ) loop
            if api.pkg_columnquery.Value(p.permitobjectid, 'State') = 'Active'
                and api.pkg_columnquery.Value(p.permitobjectid, 'PermitTypeObjectId')
                    = t_PermitTypeId and
                        api.pkg_columnquery.DateValue(p.PermitObjectId, 'ExpirationDate')
                            = t_expirationDate then
              t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
                  'InsigniaPermit');
              t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId,
                  p.permitobjectid);
              exit;
            end if;
          end loop;
        elsif t_AssociatedPermitId is not null then
          api.pkg_errors.RaiseError(-20000, 'NO Active permit to associate');
        end if;
      end if;

      t_InsigniaPermitId := api.pkg_columnquery.Value(t_PermitId,
          'InsigniaPermitObjectId');
      -- Ensure that there are no active renewal jobs against this insignia permit
      select count(*)
      into t_RenewalCount
      from
        api.relationships r
          join api.jobs j
              on j.JobId = r.ToObjectId
      where r.FromObjectId = t_InsigniaPermitId
          and r.EndPointId = t_RenewEndpointId
          and j.JobStatus in ('NEW','REVIEW');
      if t_InsigniaPermitId is not null and t_RenewalCount = 0  and
          t_ExpirationDate = api.pkg_columnquery.DateValue(t_InsigniaPermitId,
          'CalculatedExpirationDate') then
        if api.pkg_columnquery.Value(t_InsigniaPermitId, 'IsActive') = 'Y' then
          -- Copy Vehicles to existing Permit and remove from old.
          t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
              'Vehicle');
          for i in (select *
                    from query.r_abc_permitvehicle r
                    where r.PermitObjectId = t_PermitId) loop
            t_VehicleId := api.pkg_objectupdate.New(t_VehicleDefId);
            t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId,
                t_InsigniaPermitId, t_VehicleId);
            api.pkg_columnupdate.setvalue(t_VehicleId, 'InsigniaNumber',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'InsigniaNumber'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'MakeModelYear',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'makemodelyear'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'OwnedOrLeasedLimousine',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'ownedorleasedlimousine'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'VIN',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'vin'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateregistration',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateregistration'));
            api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateofregistration',
                api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateofregistration'));
          end loop;
          -- Find Vehicle Insignia Numbers already on this Permit
          select v.InsigniaNumber
          bulk collect into t_InsigniaNumberList
          from query.o_abc_vehicle v
          join
            query.r_Abc_Permitvehicle pv
            on v.ObjectId = pv.VehicleObjectId
          where pv.PermitObjectId = t_InsigniaPermitId
          order by v.InsigniaNumber;
          for i in 2..t_InsigniaNumberList.Count loop
            if t_InsigniaNumberList(i - 1) = t_InsigniaNumberList(i) then
              api.pkg_errors.RaiseError(-20000,  'No duplicate Insignia Numbers are'
                  || ' allowed on the Permit.');
            end if;
          end loop;
          -- Cancel new permit
          api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Cancelled');
          api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'N');
          api.pkg_columnupdate.SetValue(t_PermitId, 'SystemCancelled', 'Y');
          api.pkg_columnupdate.SetValue(t_PermitId, 'ExpirationDate', trunc(sysdate));
          t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
          t_Reason := 'Consolidated with Permit ' ||
              api.pkg_columnquery.Value(t_InsigniaPermitId, 'PermitNumber');
          api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation',
              sysdate);
          api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
          t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, t_PermitId,
              t_ObjectId, sysdate);
          -- Remove new Permit from and associate existing Permit to Application job
          select r.RelationshipId
          into t_RelId
          from query.r_ABC_PermitAppPermit r
          where r.PermitApplicationObjectId = t_JobId
              and r.PermitObjectId = t_PermitId;
          api.pkg_relationshipupdate.Remove (t_RelId);
          t_RelId := api.pkg_relationshipupdate.New (t_ApplicationEndpointId, t_JobId,
              t_InsigniaPermitId, sysdate);
          return;
        end if;
      end if;
    end if;

    -- Check if Permittee is entered. Sometimes when a new Permittee is created on the
    -- Public site the error handling on Permit doesn't trigger the check.
    if api.pkg_columnQuery.VALUE (t_PermitId, 'PermitteeDisplay_SQL') is null then
      api.pkg_errors.RaiseError(-20000, 'Please enter a Permittee.');
    end if;

    -- Set Effective Date
    if api.pkg_columnquery.value(t_PermitId, 'PermitTypeCode') in ('AI','BRW','WN','TAP',
        'TE') then
      t_EffectiveDate := api.pkg_columnquery.datevalue(t_PermitId, 'PermitTermFrom');
    else
      if api.pkg_columnquery.value(t_PermitId, 'EffectiveDate') is null then
        t_EffectiveDate := a_AsOfDate;
      else
        t_EffectiveDate := api.pkg_columnquery.datevalue(t_PermitId, 'EffectiveDate');
      end if;
    end if;
    api.pkg_columnupdate.SetValue(t_PermitId, 'EffectiveDate', t_EffectiveDate);

    -- Set Expiration Date
    if api.pkg_columnquery.value(t_PermitId, 'ExpirationMethod') != 'Manual' then
      t_ExpirationDate := api.pkg_columnquery.DateValue(t_PermitId,
          'CalculatedExpirationDate');
    else
      if api.pkg_columnquery.value(t_PermitId, 'ExpirationDate') is null then
        if api.pkg_columnquery.value(t_PermitId, 'PermitTypeCode') in ('AI', 'BRW', 'WN',
              'TAP', 'TE') then
          t_ExpirationDate := api.pkg_columnquery.datevalue(t_PermitId, 'PermitTermTo');
        else
          select coalesce(
              (select max(api.pkg_columnquery.datevalue(r.RainDateObjectId, 'StartDate'))
               from query.r_ABC_PermitRainDate r
               where r.PermitObjectId = t_PermitId), --Rain Date
              (select max(api.pkg_columnquery.datevalue(r.EventDateObjectId, 'StartDate'))
               from query.r_ABC_PermitEventDate r
               where r.PermitObjectId = t_PermitId)
              ) --Event Date
          into t_ExpirationDate
          from dual;
        end if;
      else
        t_ExpirationDate := api.pkg_columnquery.datevalue(t_PermitId, 'ExpirationDate');
      end if;
    end if;

    api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Active');
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'N');
    api.pkg_columnupdate.SetValue(t_PermitId, 'IssueDate', a_AsOfDate);
    api.pkg_columnupdate.SetValue(t_PermitId, 'ExpirationDate', t_ExpirationDate);
    api.pkg_columnupdate.SetValue(t_PermitId, 'ForceStateReadOnly', 'N');
    -- Set Original Issue Date
    api.pkg_columnupdate.SetValue(t_PermitId, 'OriginalIssueDate', trunc(a_AsOfDate));

    -- Get the latest version of the License
    if t_LicenseNumber is not null then
      t_LicenseIdNew := pkg_ABC_Workflow.GetLatestLicense (t_LicenseId, t_PermitId);
      if t_LicenseIdNew != t_LicenseId then
        select pl.RelationshipId
        into t_RelationshipId
        from query.r_PermitLicense pl
        where pl.PermitObjectId = t_PermitId
          and pl.LicenseObjectId = t_LicenseId;
        api.pkg_relationshipupdate.Remove(t_relationshipId);
        t_RelationshipId := api.pkg_relationshipupdate.New (t_EndPointId, t_PermitId,
            t_LicenseIdNew);
      end if;
      -- Set Original License Number if null
      if api.pkg_columnquery.Value(t_PermitId, 'OriginalLicenseNumber') is null then
        api.pkg_columnupdate.SetValue(t_PermitId, 'OriginalLicenseNumber',
            api.pkg_columnquery.Value(t_LicenseIdNew, 'LicenseNumber'));
      end if;
    end if;

    -- See if Public User needs to be associated with new Permittee
    abc.Pkg_Abc_Workflow.CopyPermitteeToOnlineUserExt(a_ObjectId,sysdate);

    --For Process Override from Muni or Police Review.
    --If outcome is set to Approve and Complete or Deny and Complete, clean up/complete open processes.
    t_ProcessOutcome := api.pkg_columnquery.Value(a_ObjectId, 'Outcome');

    if t_ProcessOutcome = 'Approve and Complete' or t_ProcessOutcome = 'Deny and Complete' then
      --Clean up Monitor Review process if not completed.
      select max(m.ProcessId)
      into t_MonitorReviewProcessId
      from query.p_abc_monitorreview m
      where m.JobId = t_JobId;

      if t_MonitorReviewProcessId is not null then
        api.pkg_processupdate.Remove(t_MonitorReviewProcessId);
      end if;

      --Complete open Municipal and Police process with outcome of the completed process
      select pr.ProcessId
      bulk collect into t_OpenProcessIds
      from
        api.processes pr
        join api.processtypes pt
            on pt.ProcessTypeId = pr.ProcessTypeId
      where pr.JobId = t_JobId
        and pr.Outcome is null
        and pt.name in ('p_ABC_MunicipalityReview', 'p_ABC_PoliceReview');

      if t_OpenProcessIds is not null then
        for i in 1..t_OpenProcessIds.count loop
          api.pkg_processupdate.Complete(t_OpenProcessIds(i), t_ProcessOutcome);
        end loop;
      end if;
    end if;
  end ApprovePermit;

 /*---------------------------------------------------------------------------
  * OnlineSubmission()
  *  Run on online submission of the Permit App.  Create Permit object and rels
  *  Run from workflow on the Permit App job
  *   Last update by Paul Orstad 09/23/2015
  *-------------------------------------------------------------------------*/
  procedure OnlineSubmission(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PermitId                          udt_id;
    t_RelatedObjId                      udt_Id;
    t_ToEndPointId                      udt_Id;
    t_RelId                             udt_Id;
    t_PermitteeId                       udt_Id;
    t_MinDate                           date;
    t_EventLeadDays                     number;
    t_InsigniaPermitObjectId            udt_id;
    t_PermitTypeId                      udt_id;
    t_LicenseId                         udt_id;

  begin

    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    if api.pkg_columnQuery.Value(t_JobId, 'OnlineRequiresEvents') = 'Y' then
      t_EventLeadDays := api.pkg_columnquery.Value(api.pkg_columnQuery.Value(t_JobId, 'OnlinePermitTypeObjectId'),'EventPermitLeadDays');
      select min(api.pkg_columnQuery.DateValue(r.EventDateObjectID,'StartDate'))
        into t_MinDate
        from query.r_ABC_PermitAppEventDate r
       where r.PermitAppJobId = t_JobId;
      if t_MinDate < (sysdate + t_EventLeadDays) then
         api.pkg_errors.RaiseError(-20000,'Please enter an Event Start Date that is at least ' || t_EventLeadDays || ' days after today.');
      end if;
    end if;

    --Create the permit object
    t_PermitId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ABC_Permit'));
    api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Pending');
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'Y');
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitToApp');
    t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, t_JobId);
    --Create all of the permit relationships. Used CopyPermitRels as example
    --Permitee
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Permittee');
    if api.pkg_columnquery.value(t_JobId, 'OnlineTAPPermitNumber') is null then
      if api.pkg_columnquery.value(t_JobId, 'OnlinePermitteeIsLicensee') = 'Y' and api.pkg_columnquery.value(t_JobId, 'OnlineLicenseNumber') is not null then
        t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, api.pkg_columnquery.value(t_JobId, 'PermiteeFromLicense'));
        --null;
      else
        t_PermitteeId := api.pkg_columnquery.value(t_JobId, 'OnlineChosenLEObjectId');
        if t_PermitteeId is not null then
          t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, api.pkg_columnquery.value(t_JobId, 'OnlineChosenLEObjectId'));
        else
          --The clerk will need to create a new LE from the information entered
          null;
        end if;
      end if;
    else
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, api.pkg_ColumnQuery.Value(t_JobId, 'OnlineTAPPermitteeObjectId'));
    end if;
    --County
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'County');
    for i in (select r.CountyObjectId
                from query.r_ABC_PermitAppCounty r
               where r.PermitApplicationId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.CountyObjectId);
    end loop;

    --Municipality
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Municipality');
    for i in (select r.MunicipalityObjectId
                from query.r_ABC_PermitAppMunicipality r
               where r.PermitApplicationId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.MunicipalityObjectId);
    end loop;

    --PermitType
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitType');
    for i in (select r.PermitTypeId
                from query.r_ABC_PermitAppPermitType r
               where r.PermitApplicationId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.PermitTypeId);
    end loop;

    --License
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'License');
    for i in (select r.LicenseObjectId
                from query.r_ABC_PermitApplicationLicense r
               where r.PermitAppObjectId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.licenseobjectid);
    end loop;

    --Seller's License
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'SellersLicense');
    for i in (select r.LicenseId
                from query.r_ABC_PermitAppSellersLicense r
               where r.PermitApplicationId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.licenseid);
    end loop;
    
    --Buyer's Permit
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'BuyersPermit');
    for i in (select r.PermitId
                from query.r_ABC_PermitAppBuyersPermit r
               where r.PermitApplicationId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.Permitid);
    end loop;

    --Additional Permit
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'AssociatedPermit');
    for i in (select r.PermitId
                from query.r_ABC_PermitAppOnlinePermit r
               where r.PermitApplicationId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.PermitId);
    end loop;

    --TAP Permit
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'TAPPermit');
    for i in (select r.OLTAPPermitId
                from query.r_ABC_OnlineTAPPermit r
               where r.OLPermitApplicationId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.OLTAPPermitId);
    end loop;

    --Event/Rain Date
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'EventDate');
    for i in (select r.EventDateObjectID
                from query.r_ABC_PermitAppEventDate r
               where r.PermitAppJobId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.eventdateobjectid);
    end loop;
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'RainDate');
    for i in (select r.RainDateObjectId
                from query.r_ABC_PermitAppRainDate r
               where r.PermitAppJobId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.RainDateObjectId);
    end loop;

    --CTS Representative
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'CTSRepresentative');
    for i in (select r.CTSRepresentativeObjectId, r.Name
                from query.r_ABC_PermitAppCTSRepOnline r
               where r.PermitApplicationObjectId = t_JobId) loop
      t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.ctsrepresentativeobjectid);
      api.pkg_columnupdate.SetValue(i.CTSRepresentativeObjectId, 'Name', i.Name);
    end loop;

    --Insignia Permit
    if api.pkg_columnquery.Value(t_JobId, 'OnlineRequiresVehicle') = 'Y' then
       if api.pkg_columnquery.Value(t_JobId, 'OnlineLicenseObjectId') is not null then
          t_PermitTypeId := api.pkg_columnQuery.Value(t_JobId, 'OnlinePermitTypeObjectId');
          t_LicenseId := api.pkg_columnquery.Value(t_JobId, 'OnlineLicenseObjectId');
          for p in (
                    select r.PermitObjectId
                    from   query.r_Permitlicense r
                    where  r.LicenseObjectId = t_LicenseId
                   ) loop
             if api.pkg_columnquery.Value(p.permitobjectid, 'State') = 'Active' and
                api.pkg_columnquery.Value(p.permitobjectid, 'PermitTypeObjectId') = t_PermitTypeId then
                t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'InsigniaPermit');
                t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, p.permitobjectid);
                api.pkg_columnupdate.SetValue(t_PermitId, 'ExpirationDate', api.pkg_columnquery.DateValue(p.permitobjectid, 'ExpirationDate'));
                exit;
             end if;
          end loop;
       end if;
       if api.pkg_columnquery.Value(t_JobId, 'OnlineTAPPermitObjectId') is not null then
          t_PermitTypeId := api.pkg_columnQuery.Value(t_JobId, 'OnlinePermitTypeObjectId');
          t_LicenseId := api.pkg_columnquery.Value(t_JobId, 'OnlineTAPPermitObjectId');
          for p in (
                    select r.PermitObjectId
                    from   query.r_Abc_Tappermit r
                    where  r.TAPPermitObjectId = t_LicenseId
                   ) loop
             if api.pkg_columnquery.Value(p.permitobjectid, 'State') = 'Active' and
                api.pkg_columnquery.Value(p.permitobjectid, 'PermitTypeObjectId') = t_PermitTypeId then
                t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'InsigniaPermit');
                t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, p.permitobjectid);
                api.pkg_columnupdate.SetValue(t_PermitId, 'ExpirationDate', api.pkg_columnquery.DateValue(p.permitobjectid, 'ExpirationDate'));
                exit;
             end if;
          end loop;
       end if;
       if api.pkg_columnquery.Value(t_JobId, 'OnlinePermitObjectId') is not null then
          t_PermitTypeId := api.pkg_columnQuery.Value(t_JobId, 'OnlinePermitTypeObjectId');
          t_LicenseId := api.pkg_columnquery.Value(t_JobId, 'OnlinePermitObjectId');
          for p in (
                    select r.PrimaryPermitObjectId PermitObjectId
                    from   query.r_ABC_AssociatedPermit r
                    where  r.AsscPermitObjectId = t_LicenseId
                   ) loop
             if api.pkg_columnquery.Value(p.permitobjectid, 'State') = 'Active' and
                api.pkg_columnquery.Value(p.permitobjectid, 'PermitTypeObjectId') = t_PermitTypeId then
                t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'InsigniaPermit');
                t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, p.permitobjectid);
                api.pkg_columnupdate.SetValue(t_PermitId, 'ExpirationDate', api.pkg_columnquery.DateValue(p.permitobjectid, 'ExpirationDate'));
                exit;
             end if;
          end loop;
       end if;
    --Vehicle
       t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Vehicle');
       for i in (select *
                   from query.r_ABC_PermitAppVehicleOnline r
                  where r.PermitApplicationObjectId = t_JobId) loop
         t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_PermitId, i.vehicleobjectid);
         api.pkg_columnupdate.setvalue(i.vehicleobjectid, 'InsigniaNumber', i.insiginanumber);
         api.pkg_columnupdate.setvalue(i.vehicleobjectid, 'MakeModelYear', i.makemodelyear);
         api.pkg_columnupdate.setvalue(i.vehicleobjectid, 'OwnedOrLeasedLimousine', i.ownedorleasedlimousine);
         api.pkg_columnupdate.setvalue(i.vehicleobjectid, 'VIN', i.vin);
         api.pkg_columnupdate.setvalue(i.vehicleobjectid, 'Stateregistration', i.Stateregistration);
         api.pkg_columnupdate.setvalue(i.vehicleobjectid, 'Stateofregistration', i.Stateofregistration);
       end loop;
    end if;

    --PT Details
    api.pkg_columnupdate.setvalue(t_PermitId, 'EventDetails', api.pkg_columnquery.value(t_JobId, 'OnlineEventDetails'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'FPProductDescription', api.pkg_columnquery.value(t_JobId, 'OnlineFPProductDescription'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'NumberOfIndividuals', api.pkg_columnquery.value(t_JobId, 'OnlineNumberOfIndividuals'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PermitTermFrom', api.pkg_columnquery.value(t_JobId, 'OnlinePermitTermFrom'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PermitTermTo', api.pkg_columnquery.value(t_JobId, 'OnlinePermitTermTo'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'SaleByJudgementAt', api.pkg_columnquery.value(t_JobId, 'OnlineSaleByJudgementAt'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'SaleByJudgementBy', api.pkg_columnquery.value(t_JobId, 'OnlineSaleByJudgementBy'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'SolicitorHasOwnership', api.pkg_columnquery.value(t_JobId, 'OnlineSolicitorHasOwnership'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'TTBPermitNumber', api.pkg_columnquery.value(t_JobId, 'OnlineTTBPermitNumber'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'LicenseNumberStored', api.pkg_columnquery.value(t_JobId, 'OnlineLicenseNumberStored'));

    api.pkg_columnupdate.setvalue(t_PermitId, 'EffectiveDateOfLicenseTransfer', api.pkg_columnquery.value(t_JobId, 'OnlineEffecDateLicenseTransfer'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PurchasingAlcoholicInventory', api.pkg_columnquery.value(t_JobId, 'OnlinePurchasingAlcInventory'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'FPNumberOfGallons', api.pkg_columnquery.value(t_JobId, 'OnlineNumberOfGallons'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'IsCivicReligousOrEducational', api.pkg_columnquery.value(t_JobId, 'OnlineCivicReligOrEduc'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PersonalConsumptionProductDesc', api.pkg_columnquery.value(t_JobId, 'OnlinePersConsProdDescription'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PersonalConsumptionProductDest', api.pkg_columnquery.value(t_JobId, 'OnlinePersConsProdDestination'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PersonalConsumptionProductOrig', api.pkg_columnquery.value(t_JobId, 'OnlinePersConsProdOrigin'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PromoterContactPhoneNumber', api.pkg_columnquery.value(t_JobId, 'OnlineMAPhoneNumber'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PromoterCompanyName', api.pkg_columnquery.value(t_JobId, 'OnlineMAPromoterCompanyName'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PromoterContactEmailAddress', api.pkg_columnquery.value(t_JobId, 'OnlineMAPromoterEmail'));
    api.pkg_columnupdate.setvalue(t_PermitId, 'PromoterContactName', api.pkg_columnquery.value(t_JobId, 'OnlineMAPromoterName'));

    api.pkg_columnupdate.setvalue(t_PermitId, 'EventLocAddressDescription', api.pkg_columnquery.value(t_JobId, 'OnlineLocationName'));

    --Make sure PostVerify is run on the job before anything else happens
    Postverify(t_JobId, a_AsOfdate);

  end OnlineSubmission;

 /*---------------------------------------------------------------------------
  * MaintainLEPermitAppXref()
  *  Run on post-verify of a number of relationships to maintain an accurate
  *  cross reference between Legal Entities and Permit Applications.
  *   Last update by Matthew Johnson 09/15/2015
  *-------------------------------------------------------------------------*/
  procedure MaintainLEPermitAppXref(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_PermitApplicationId               udt_id;
    t_LegalEntityId                     udt_id;
    t_PermitObjectId                    udt_id;
    t_LicenseObjectId                   udt_id;
    t_PermitTypeObjectId                udt_id;
    t_XrefId                            udt_id;
    t_AllowTapOverride                  char(1);
    t_LicenseRequired                   varchar2(100);
    t_PermiteeIsLicensee                char(1);
    t_UpdateXrefs          char(1) := 'N';
    t_IsTapRel             char(1) := 'N';
    t_IsLicenseeRel        char(1) := 'N';
    t_IsPermiteeRel        char(1) := 'N';

  begin
    -- Start by checking what relationship we're using
  for rel in (select r.FromObjectId, r.ToObjectId, rd.Name
                from api.Relationships r
                join api.RelationshipDefs rd
                  on rd.RelationshipDefId = r.RelationshipDefId and rd.ToEndPoint = 0
               where r.ToEndPoint = 0
                 and r.RelationshipId = a_ObjectId) loop
    -- Verify which Relationship we are on:
    if rel.Name = 'r_ABC_PermitAppPermittee' then
      -- Relationship from Legal Entity to Permit Application job
      t_PermitApplicationId := rel.ToObjectId;

      for pa in (select p.PermitObjectId
                   from query.r_ABC_PermitAppPermit p
                  where p.PermitApplicationObjectId = t_PermitApplicationId) loop
        t_PermitObjectId := pa.PermitObjectId;
      end loop;

      t_LegalEntityId := rel.FromObjectId;
      t_IsPermiteeRel := 'Y';
    elsif rel.Name = 'r_ABC_PermitPermittee' then
      -- Relationship from Legal Entity to Permit
      -- Start by looking for the Permit Application related to the permit
      for pa in (select p.PermitApplicationObjectId
                   from query.r_ABC_PermitAppPermit p
                  where p.PermitObjectId = rel.ToObjectId) loop
        t_LegalEntityId := rel.FromObjectId;
        t_PermitObjectId := rel.ToObjectId;
        t_PermitApplicationId := pa.PermitApplicationObjectId;
        t_IsPermiteeRel := 'Y';
      end loop;
      -- Otherwise, check to see if this is really a TAP
      for ta in (select p.PermitApplicationObjectId, p.PermitObjectId
                   from query.r_ABC_TAPPermit r
                   join query.r_ABC_PermitAppPermit p on p.PermitObjectId = r.PermitObjectId
                  where r.TAPPermitObjectId = rel.ToObjectId) loop
        t_LegalEntityId := rel.FromObjectId;
        t_PermitObjectId := ta.permitobjectid;
        t_PermitApplicationId := ta.PermitApplicationObjectId;
        t_IsTapRel := 'Y';
      end loop;

    elsif rel.Name = 'r_PermitLicense' then
      -- Relationship from License to Permit
      select r.LegalEntityObjectId
        into t_LegalEntityId
        from query.r_ABC_LicenseLicenseeLE r
       where r.LicenseObjectId = rel.FromObjectId;

      for pa in (select p.PermitApplicationObjectId
                   from query.r_ABC_PermitAppPermit p
                  where p.PermitObjectId = rel.ToObjectId) loop
        t_PermitApplicationId := pa.PermitApplicationObjectId;
      end loop;

      t_PermitObjectId := rel.ToObjectId;

      t_IsLicenseeRel := 'Y';
    elsif rel.Name = 'r_ABC_OnlineTAPPermit' then
      -- Relationship from TAP to Application
      t_PermitApplicationId := rel.ToObjectId;
      for tap in (select r.PermitteeObjectId
                   from query.r_ABC_PermitPermittee r
                  where r.PermitObjectId = rel.FromObjectId) loop
        t_LegalEntityId := tap.PermitteeObjectId;
        t_PermitObjectId := rel.ToObjectId;
        t_PermitApplicationId := rel.ToObjectId;
        t_IsTapRel := 'Y';
      end loop;
    elsif rel.Name = 'r_ABC_PermitApplicationLicense' then
      -- Relationship from License to Application
      t_PermitApplicationId := rel.ToObjectId;
      for le in (select r.LegalEntityObjectId
                   from query.r_ABC_LicenseLicenseeLE r
                  where r.LicenseObjectId = rel.FromObjectId) loop
        t_LegalEntityId := le.LegalEntityObjectId;
        t_PermitApplicationId := rel.ToObjectId;
        t_IsLicenseeRel := 'Y';
      end loop;
    end if;
  end loop;

  -- Check permit for Permit Type
  if t_PermitObjectId is not null then
    -- Permit was found, get Permit Type from Permit
    for pt in (select r.PermitTypeId
                 from query.r_ABC_PermitPermitType r
                where r.PermitId = t_PermitObjectId) loop
      t_PermitTypeObjectId := pt.PermitTypeId;
    end loop;
  else
    -- Permit was not found, get Permit Type from Application Job
    for pt in (select r.PermitTypeId
                 from query.r_ABC_PermitAppPermitType r
                where r.PermitApplicationId = a_ObjectId) loop
      t_PermitTypeObjectId := pt.PermitTypeId;
    end loop;
  end if;

  -- Check if we actually found a Permit Type (and also that we have a Permit Application Id)
  if t_PermitTypeObjectId is not null and t_PermitApplicationId is not null then
    -- Good, we have a permit type. Let's look up some permit type values:
    select api.pkg_ColumnQuery.Value(t_PermitTypeObjectId, 'AllowTAPOverride'),
           api.pkg_ColumnQuery.Value(t_PermitTypeObjectId, 'LicenseRequired'),
           api.pkg_ColumnQuery.Value(t_PermitTypeObjectId, 'PermitteeIsLicensee')
      into t_AllowTapOverride, t_LicenseRequired, t_PermiteeIsLicensee
      from dual;
    -- Now, let's evaluate the criteria above:
    if t_AllowTapOverride = 'Y' and t_LicenseRequired = 'Required' and t_PermiteeIsLicensee = 'Y' then
      -- Check for TAP rel first, otherwise Licensee rel
      if t_IsTapRel = 'Y' then
        t_UpdateXrefs := 'Y';
      elsif t_IsLicenseeRel = 'Y' then
        t_UpdateXrefs := 'Y';
      end if;
    elsif t_AllowTapOverride = 'Y' and t_LicenseRequired = 'Optional' and t_PermiteeIsLicensee = 'Y' then
      -- Check for TAP rel first, then Licensee, then Permitee
      if t_IsTapRel = 'Y' then
        t_UpdateXrefs := 'Y';
      elsif t_IsLicenseeRel = 'Y' then
        t_UpdateXrefs := 'Y';
      elsif t_IsPermiteeRel = 'Y' then
        t_UpdateXrefs := 'Y';
      end if;
    elsif t_AllowTapOverride = 'N' and t_LicenseRequired = 'Optional' and t_PermiteeIsLicensee = 'Y' then
      -- Check for Licensee rel, then Permitee
      if t_IsLicenseeRel = 'Y' then
        t_UpdateXrefs := 'Y';
      elsif t_IsPermiteeRel = 'Y' then
        t_UpdateXrefs := 'Y';
      end if;
    elsif t_AllowTapOverride = 'N' and t_LicenseRequired = 'Required' and t_PermiteeIsLicensee = 'Y' then
      -- Check for Licensee rel
      if t_IsLicenseeRel = 'Y' then
        t_UpdateXrefs := 'Y';
      end if;
    elsif (t_AllowTapOverride = 'Y' and t_LicenseRequired = 'Required' and t_PermiteeIsLicensee = 'N')
       or (t_AllowTapOverride = 'N' and t_LicenseRequired = 'Required' and t_PermiteeIsLicensee = 'N')
       or (t_AllowTapOverride = 'Y' and t_LicenseRequired = 'Optional' and t_PermiteeIsLicensee = 'N')
       or (t_AllowTapOverride = 'N' and t_LicenseRequired = 'Optional' and t_PermiteeIsLicensee = 'N')
       or (t_AllowTapOverride = 'N' and t_LicenseRequired = 'N/A'      and t_PermiteeIsLicensee = 'N') then
      -- Check for Permitee rel
      if t_IsPermiteeRel = 'Y' then
        t_UpdateXrefs := 'Y';
      end if;
    end if;

    -- Let's update the cross references table
    if t_UpdateXrefs = 'Y' then
      for xr in (select x.LEPermitAppXrefId, x.LegalEntityId
                   from abc.LEPermitAppXref_t x
                  where x.PermitApplicationId = t_PermitApplicationId) loop
        t_XrefId := xr.LEPermitAppXrefId;
        -- Check to see if Legal Entity ID is different. If so, update row.
        if xr.LegalEntityId <> t_LegalEntityId then
          update abc.LEPermitAppXref_t
             set LegalEntityId = t_LegalEntityId
           where LEPermitAppXrefId = xr.LEPermitAppXrefId;
        end if;
      end loop;

      -- If no row found in query above, insert new row
      if t_XrefId is null then
        insert into abc.LEPermitAppXref_t
          (LEPermitAppXrefId, LegalEntityId, PermitApplicationId)
        values
          (abc.LEPermitAppXref_Seq.NextVal, t_LegalEntityId, t_PermitApplicationId);
      end if;

    end if; -- End update xrefs check

  end if; -- End permit type and permit application id check
  end MaintainLEPermitAppXref;

 /*---------------------------------------------------------------------------
  * PrintPermit()
  *  Prints the permit Certificate or ID Card
  *  Run from workflow on the Permit App job
  *-------------------------------------------------------------------------*/
  procedure PrintPermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PermitId                          udt_id;
    t_ReportId                          udt_Id;

  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_PermitId := api.pkg_columnquery.value(t_JobId, 'PermitObjectId');
    t_ReportId := extension.pkg_processserver.ScheduleRTFReport(api.pkg_columnquery.Value(t_PermitId,'CertificateType'), t_PermitId, t_PermitId);
  end PrintPermit;

 /*---------------------------------------------------------------------------
  * WithdrawApplication()
  *  Runs Withdrawal actions
  *  Run from workflow on the Permit App job
  *-------------------------------------------------------------------------*/
  procedure WithdrawApplication(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PermitId                          udt_id;

  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_PermitId := api.pkg_columnquery.value(t_JobId, 'PermitObjectId');
    api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Withdrawn');
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'N');
  end WithdrawApplication;

  /*---------------------------------------------------------------------------
   * DenyApplication() -- PUBLIC
   *  Runs Denial actions. Run from workflow on the Permit Renewal job
   *-------------------------------------------------------------------------*/
  procedure DenyApplication(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_MonitorReviewProcessId            udt_id;
    t_OpenProcessIds                    udt_IdList;
    t_PermitId                          udt_id;
    t_ProcessOutcome                    varchar(4000);
  begin

    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_PermitId := api.pkg_columnquery.value(t_JobId, 'PermitObjectId');
    api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Not Approved');
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'N');

    --For Process Override from Muni or Police Review.
    --If outcome is set to Approve and Complete or Deny and Complete, clean up/complete open processes.
    t_ProcessOutcome := api.pkg_columnquery.Value(a_ObjectId, 'Outcome');

    if t_ProcessOutcome = 'Approve and Complete' or t_ProcessOutcome = 'Deny and Complete' then
      --Clean up Monitor Review process if not completed.
      select max(m.ProcessId)
      into t_MonitorReviewProcessId
      from query.p_abc_monitorreview m
      where m.JobId = t_JobId;

      if t_MonitorReviewProcessId is not null then
        api.pkg_processupdate.Remove(t_MonitorReviewProcessId);
      end if;

      --Complete open Municipal and Police process with outcome of the completed process
      select pr.ProcessId
      bulk collect into t_OpenProcessIds
      from
        api.processes pr
        join api.processtypes pt
            on pt.ProcessTypeId = pr.ProcessTypeId
      where pr.JobId = t_JobId
        and pr.Outcome is null
        and pt.name in ('p_ABC_MunicipalityReview', 'p_ABC_PoliceReview');

      if t_OpenProcessIds is not null then
        for i in 1..t_OpenProcessIds.count loop
          api.pkg_processupdate.Complete(t_OpenProcessIds(i), t_ProcessOutcome);
        end loop;
      end if;
    end if;

  end DenyApplication;

 /*---------------------------------------------------------------------------
  * CopyCertificateRelToPermit()
  *  Run on Creation of the relationship from Send License to the Certificate
  *    on the Permit Application job.
  *-------------------------------------------------------------------------*/
  procedure CopyCertificateRelToPermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_ProcessId                         udt_Id;
    t_PermitId                          udt_id;
    t_PermitCertificateEP               udt_Id;
    t_RelId                             udt_Id;

  begin
    for i in (select r.PermitCertificateId, r.SendPermitId
                from query.r_ABC_PrintPermitPermitCert r
               where r.RelationshipId = a_ObjectId) loop
      t_ProcessId := i.sendpermitid;
      t_PermitId := api.pkg_columnquery.value(t_ProcessId, 'PermitId');
      t_PermitCertificateEP := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitCertificate');
      --Relate the Certificate to the Permit
      t_RelId := api.pkg_relationshipupdate.New(t_PermitCertificateEP, t_PermitId, i.PermitCertificateId);
    end loop;
    for i in (select r.IDCardId, r.SendPermitId
                from query.r_ABC_SendPermitIdCard r
               where r.RelationshipId = a_ObjectId) loop
      t_ProcessId := i.sendpermitid;
      t_PermitId := api.pkg_columnquery.value(t_ProcessId, 'PermitId');
      t_PermitCertificateEP := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitIDCard');
      --Relate the Certificate to the Permit
      t_RelId := api.pkg_relationshipupdate.New(t_PermitCertificateEP, t_PermitId, i.idcardid);
    end loop;
    for j in (select r.PermitCertificateId, r.PermitPermitOnlineId
                from query.r_ABC_PrintPermOnlinePermCert r 
               where r.RelationshipId = a_ObjectId) loop 
      t_ProcessId := j.PermitPermitOnlineId;
      t_PermitId := api.pkg_columnquery.value(t_ProcessId, 'PermitId');
      t_PermitCertificateEP := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitCertificate');
      --Relate the Certificate to the Permit
      t_RelId := api.pkg_relationshipupdate.New(t_PermitCertificateEP, t_PermitId, j.PermitCertificateId);
    end loop;
    for k in (select r.PermitCertificateId, r.PrintPermitId
                from query.r_ABC_PrintPermitPermitCertif r 
               where r.RelationshipId = a_ObjectId) loop 
      t_ProcessId := k.PrintPermitId;
      t_PermitId := api.pkg_columnquery.value(t_ProcessId, 'PermitId');
      t_PermitCertificateEP := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitCertificate');
      --Relate the Certificate to the Permit
      t_RelId := api.pkg_relationshipupdate.New(t_PermitCertificateEP, t_PermitId, k.PermitCertificateId);
    end loop;


  end CopyCertificateRelToPermit;

  /*---------------------------------------------------------------------------
  * ReassignReviewProcesses()
  *  Run as a processes server nightly procedure to assign a CSR to Municipal
  *    or Police Review Processes which have not been completed by the due date.
  *-------------------------------------------------------------------------*/
  procedure ReassignReviewProcesses
    is
    t_AssignProcessIdList                udt_idlist;
    t_ClerkId                            udt_id;
    t_ReminderProcessIdList              udt_idlist;
  begin
    select p.processid
    bulk collect into t_AssignProcessIdList
    from api.processes p
    join api.processtypes pt on p.processtypeid = pt.processtypeid
    where p.outcome is null
      and pt.Name in ('p_ABC_PoliceReview', 'p_ABC_MunicipalityReview')
      and trunc(sysdate) >= p.ScheduledCompleteDate
      and api.pkg_columnquery.Value(p.processid, 'IsCSRAssigned') = 'N';

    for i in 1..t_AssignProcessIdList.count loop
      t_ClerkId := to_number(api.pkg_processquery.JobValue(t_AssignProcessIdList(i), 'DefaultClerkId'));
      api.pkg_processupdate.Assign(t_AssignProcessIdList(i), t_ClerkId);
      abc.pkg_abc_email.SendReassignmentEmail(t_AssignProcessIdList(i), t_ClerkId);
      api.pkg_columnupdate.SetValue(t_AssignProcessIdList(i), 'IsCSRAssigned', 'Y');
    end loop;

    select p.processid
    bulk collect into t_ReminderProcessIdList
    from api.processes p
    join api.processtypes pt on p.processtypeid = pt.processtypeid
    where p.outcome is null
      and pt.Name in ('p_ABC_PoliceReview', 'p_ABC_MunicipalityReview')
      and trunc(sysdate) >= api.pkg_columnquery.DateValue(p.processid, 'ReminderDate')
      and api.pkg_columnquery.Value(p.processid, 'isEmailReminderSent') = 'N';

    for i in 1..t_ReminderProcessIdList.count loop
      abc.Pkg_Abc_Municipality.ReviewEmailReminder(t_ReminderProcessIdList(i), trunc(sysdate));
      api.pkg_columnupdate.SetValue(t_ReminderProcessIdList(i), 'isEmailReminderSent', 'Y');
    end loop;

  end ReassignReviewProcesses;
end pkg_ABC_PermitApplication;
/
