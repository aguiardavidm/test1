create or replace package abc.pkg_ABC_SearchProcedures is

  -- Author  : JOHN.PETKAU
  -- Created : 4/13/2011 3:02:22 PM
  -- Purpose : Search Procedures for POSSE ABC

  -- 13Sep2013, Michel S: Removed Legal Name from Establishment. Using DoingBusinessAs instead.
  --      Task 003496 - POSSE ABC

  -- Public type declarations
  subtype udt_ID is api.pkg_Definition.udt_ID;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_ObjectList is api.udt_ObjectList;
  subtype udt_StringList is api.Pkg_Definition.udt_StringList;
  subtype udt_SearchResults is api.pkg_Definition.udt_SearchResults;

  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --<VariableName> <Datatype>;

  -- Public function and procedure declarations

  -----------------------------------------------------------------------------
  -- OutriderSearchLimit
  --  Retrieve the number of rows to limit search to
  -----------------------------------------------------------------------------
  function OutriderSearchLimit
    return number;

  -----------------------------------------------------------------------------
  -- OutriderSearchLimitError
  --  Format the error string to present to the user when too many rows are returned
  -----------------------------------------------------------------------------
  function OutriderSearchLimitError
    (a_SearchLimit number)
    return string;

  -----------------------------------------------------------------------------
  -- AvailableSecLicTypeSearch
  --  Search for valid Secondary License Types based on the Primary License Type selected.
  -----------------------------------------------------------------------------
  procedure AvailableSecLicTypeSearch(
    a_PrimaryLicenseTypeObjectId        number,
    a_Website                           varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  );

   -----------------------------------------------------------------------------
  -- EstablishmentSearch
  --  Search for Establishments.
  -----------------------------------------------------------------------------
  procedure  EstablishmentSearch(
    a_EstablishmentType                 varchar2,
    a_DoingBusinessAs                   varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_Operator                          varchar2,
    a_IncludeHistory                    char,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

 -----------------------------------------------------------------------------
  -- JobSearch
  --  Search for Jobs.
  -----------------------------------------------------------------------------
  procedure JobSearch(
    a_JobNumber                         varchar2,
    a_JobType                           varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_LicenseNumber                     varchar2,
    a_PermitNumber                      varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_CreatedByUserFormattedName        varchar2,
    a_AutoCancelled                     varchar2 default 'N',
    a_ObjectList                        out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );


  -----------------------------------------------------------------------------
  -- AccusationSearch
  --  Search for Accusation jobs.
  -- Programmer: StanH
  -- Date:       20Sep2012
  -- Notes:      Based on JobSearch.
  -----------------------------------------------------------------------------
  procedure AccusationSearch(
    a_JobNumber                         varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_LicenseNumber                     varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_CreatedByUserFormattedName        varchar2,
    a_Violations                        varchar2,
    a_LicenseType                       varchar2,
    a_Decision                          varchar2,
    a_ObjectList                        out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );


  -----------------------------------------------------------------------------
  -- InspectionSearch
  --  Search for Inspection jobs.
  -- Programmer: StanH
  -- Date:       20Sep2012
  -----------------------------------------------------------------------------
  procedure InspectionSearch(
    a_JobNumber                         varchar2,
    a_LicenseNumber                     varchar2,
    a_JobStatus                         varchar2,
    a_InspectionType                    varchar2,
    a_InspectionDateFrom                date,
    a_InspectionDateTo                  date,
    a_InspectedBy                       varchar2,
    a_InspectionResult                  varchar2,
    a_ViolationType                     varchar2,
    a_LicenseType                       varchar2,
    a_Region                            varchar2,
    a_ObjectList                        out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );


  -----------------------------------------------------------------------------
  -- Legal Entity Search
  --  Search for Legal Entities
  -----------------------------------------------------------------------------
  procedure LegalEntitySearch(
    a_LegalName                         varchar2,
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_LegalEntityTypeObjectId           udt_id,
    a_CorporationNumber                 varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_IncludeHistory                    char,
    a_IsBrandRegistrant                 char,
    a_IsPermittee                       char,
    a_SSNTIN                            varchar2,
    a_DriversLicense                    varchar2,
    a_ContactEmail                      varchar2,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  -----------------------------------------------------------------------------
  -- LetterTemplateSearch
  --  Return Letter Templates.
  -----------------------------------------------------------------------------
  procedure LetterTemplateSearch (
    a_WordTemplateName                  varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- License Lookup
  --  Look up Licenses based on License Number, filtered by State
  -----------------------------------------------------------------------------
  procedure LicenseLookup(
    a_LicenseNumber                    varchar2,
    a_IsAmendable                      char,
    a_IsRenewable                      char,
    a_IsReinstatable                   char,
    a_IsInspectable                    char,
    a_IsAccusable                      char,
    a_ObjectList                       out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- LicenseSearch
  --  Search for Licenses.
  -----------------------------------------------------------------------------
  procedure LicenseSearch(
    a_LicenseNumber                     varchar2,
    a_LicenseType                       varchar2,
    a_State                             varchar2,
    a_OriginalIssueDateFrom             date,
    a_OriginalIssueDateTo               date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_Establishment                     varchar2,
    a_PhysicalAddress                   varchar2,
    a_Licensee                          varchar2,
    --a_Operator                          varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_Restriction                       varchar2,
    a_CriminalConviction                varchar2,
    a_ConflictOfInterest                varchar2,
    --a_RiskFactor                        varchar2,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N',
    a_IsAmendable                       varchar2 default null,
    a_IsRenewable                       varchar2 default null,
    a_IsReinstatable                    varchar2 default null,
    a_IsInspectable                     varchar2 default null,
    a_IsAccusable                       varchar2 default null
  );

  -----------------------------------------------------------------------------
  -- DistributorSearch
  --  Search for Distributors.
  -----------------------------------------------------------------------------
  procedure DistributorSearch(
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_IsActive                          char,
    a_IsDistributorLicense              char,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- FilteredDistributorSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
   procedure FilteredDistributorSearch(
    a_DistributorName                   varchar2,
    a_LicenseNumber                     varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- FilteredProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure FilteredActiveLicenseLookup(
    a_LicenseNumber                varchar2,
    a_Objects                      out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- FilteredProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure FilteredActivePermitLookup(
    a_PermitNumber                varchar2,
    a_Objects                     out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- FilteredProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure FilteredProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  );
    -----------------------------------------------------------------------------
  -- FilteredLicenseProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure FilteredLicenseProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- FilteredLicenseProductCurrent
  --   Search for Products on a License, different that FilteredLicenseproductSearch
  --   In that it returns the product list based on the master license
  --   Programmer: Elijah Rust
  --   Date:       11/04/2015
  -----------------------------------------------------------------------------
  procedure FilteredLicenseProductCurrent(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- FilteredLicenseProductHistoric
  --   Search for Products on a License, different that FilteredLicenseproductSearch
  --   In that it returns the product list based on the master license
  --   Programmer: Elijah Rust
  --   Date:       11/04/2015
  -----------------------------------------------------------------------------
  procedure FilteredLicenseProductHistoric(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  ---------------------------------------------------------------------------------
  -- FilteredLicenseLookup_Permit
  --  Search for Licenses, filter by License Type selected on the given Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredLicenseLookup_Permit(
    a_PermitTypeObjectId              number,
    a_LicenseNumber                   varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  );

  ---------------------------------------------------------------------------------
  -- FilteredPermitLookup_Permit
  --  Search for Permits, filter by Permit Type selected on the given Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredPermitLookup_Permit(
    a_PermitTypeObjectId              number,
    a_PermitNumber                    varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  );
  -----------------------------------------------------------------------------
  -- FilteredRenewalProductSearch
  --  Search for Products registered to a legal entity in a Renewal Job.
  -----------------------------------------------------------------------------
  procedure FilteredRenewalProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- ProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure ProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  -----------------------------------------------------------------------------
  -- PublicProductSearch
  --  Search for Products on the Public website.
  -----------------------------------------------------------------------------
  procedure PublicProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_UserId                            number,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  -----------------------------------------------------------------------------
  -- ProcessTypeSearch
  --  Search for Process Types.
  -----------------------------------------------------------------------------
  procedure ProcessTypeSearch(
    a_Description                       varchar2,
    a_Name                              varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- UsersSearch
  --  Search for Internal Users.
  -----------------------------------------------------------------------------
  procedure UserSearch(
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_Active                            char,
    a_UserType                          varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  );


  /*---------------------------------------------------------------------------
   * PublicUserSearch() -- PUBLIC
   *   Search for Public Users
   *-------------------------------------------------------------------------*/
  procedure PublicUserSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_EmailAddress                      varchar2,
    a_PhoneNumber                       varchar2,
    a_BusinessPhone                     varchar2,
    a_UserType                          varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- PortalApplicationSearch
  --  Search for Applications by Municipal Users's Municipality
  -----------------------------------------------------------------------------
  procedure PortalApplicationSearch(
    a_FileNumber                        varchar2,
    a_LicenseNumber                     varchar2,
    a_ApplicationType                   varchar2,
    a_Licensee                          varchar2,
    a_TradeName                         varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * MunicipalUserSearch() -- PUBLIC
   *   Search for Municipal Users
   *-------------------------------------------------------------------------*/
  procedure MunicipalUserSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_BusinessPhone                     varchar2,
    a_UserType                          varchar2,
    a_Municipality                      varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * MunicipalitySearch()
   *   Search for Municipalities.
   *-------------------------------------------------------------------------*/
  procedure MunicipalitySearch (
    a_Name                              varchar2,
    a_Code                              varchar2,
    a_County                            varchar2,
    a_HideInactive                      varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- InstructorSearch
  --  Search for Internal Users.
  -----------------------------------------------------------------------------
  procedure InstructorSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_Active                            char,
    a_Objects                           out nocopy api.udt_ObjectList
  );

   /*---------------------------------------------------------------------------
   * FindProcessEmail() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindProcessEmail (
    a_Description           varchar2,
    a_Results               out api.udt_ObjectList
  );

   /*-------------------------------------------------------------------------------
   * Procedure FindUserProcesses()
   *   Purpose: This procedure will return a list of incompleted processes assigned to
   *            a current login user for the purpose of filling a To-Do-List. You have
   *            the option of either displaying current processes(i.e. the scheduledstartdate
   *            is null or it is less than the sysdate) or all processes.
   *-----------------------------------------------------------------------------*/
  Procedure FindUserProcesses(
                a_User                   varchar2,
                a_ProcessType            varchar2,
                a_JobExternalFileNum     varchar2,
                a_DateCompletedFrom      date,
                a_DateCompletedTo        date,
                a_RelatedObjectIds   out nocopy api.udt_ObjectList
                );
  /*---------------------------------------------------------------------------
   * FindUserProcessesExtract() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function FindUserProcessesExtract(
    a_User                               varchar2,
    a_ProcessType                        varchar2,
    a_JobExternalFileNum                 varchar2,
    a_DateCompletedFrom                  date,
    a_DateCompletedTo                    date
  ) return api.udt_ObjectList;

  -----------------------------------------------------------------------------
  -- Legal Entity Lookup
  --  Look up LE's based on Name - only includes Active LE's
  -----------------------------------------------------------------------------
  procedure LegalEntityLookup(
    a_LegalEntityName                  varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- Establishment Lookup
  --  Look up Establishments based on Name - only includes Active Establishments
  -----------------------------------------------------------------------------
  procedure EstablishmentLookup(
    a_EstablishmentName                varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- DashInspectionSearch
  --  For the Inspections from the dashboard
  -----------------------------------------------------------------------------
  procedure DashInspectionSearch(
    a_InspectionType                   varchar2,
    a_InspectionResult                 varchar2,
    a_Region                           varchar2,
    a_FromInspectionDate               date,
    a_ToInspectionDate                 date,
    a_ObjectList                       out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- DocumentTypeJSON
  -- procedure for the registered statement GetDocumentTypeJSON
  -- for the dropdown on multi-file upload.
  -----------------------------------------------------------------------------
  function DocumentTypeJSON(
    Argument     varchar2
  ) return clob;

  -----------------------------------------------------------------------------
  -- LicenseSearchExtract
  --  Search for Licenses to be exported to Excel.
  -----------------------------------------------------------------------------
  function LicenseSearchExtract(
    a_LicenseNumber                     varchar2,
    a_LicenseType                       varchar2,
    a_State                             varchar2,
    a_OriginalIssueDateFrom             date,
    a_OriginalIssueDateTo               date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_Establishment                     varchar2,
    a_PhysicalAddress                   varchar2,
    a_Licensee                          varchar2,
    --a_Operator                          varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_Restriction                       varchar2,
    a_CriminalConviction                varchar2,
    a_ConflictOfInterest                varchar2
    --a_RiskFactor                        varchar2
  ) return api.udt_ObjectList;

 -----------------------------------------------------------------------------
  -- JobSearch
  --  Search for Jobs for Save as Excel
  -----------------------------------------------------------------------------
  function JobSearchExtract(
    a_JobNumber                         varchar2,
    a_JobType                           varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_LicenseNumber                     varchar2,
    a_PermitNumber                      varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_CreatedByUserFormattedName        varchar2,
    a_AutoCancelled                     varchar2
  ) return api.udt_ObjectList;


 -----------------------------------------------------------------------------
  -- ProductSearch
  --  Search for Jobs for Save as Excel
  -----------------------------------------------------------------------------
  function ProductSearchExtract(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2
  ) return api.udt_ObjectList;

  -----------------------------------------------------------------------------
  -- ProductSearchAmendment
  -----------------------------------------------------------------------------
  procedure ProductSearchAmendment(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_RegistrantObjectId                number,
    a_Objects                           out nocopy api.udt_ObjectList
  );



-----------------------------------------------------------------------------
  -- AccusationSearchExtract
  --   Search for Accusations for Save as Excel
  -- Programmer: StanH
  -- Date:       24Sep2012
  -----------------------------------------------------------------------------
  function AccusationSearchExtract(
    a_JobNumber                         varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_LicenseNumber                     varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_CreatedByUserFormattedName        varchar2,
    a_Violations                        varchar2,
    a_LicenseType                       varchar2,
    a_Decision                          varchar2
  ) return api.udt_ObjectList;

  -----------------------------------------------------------------------------
  -- InspectionSearchExtract
  --   Search for Inspections for Save as Excel
  -- Programmer: StanH
  -- Date:       20Sep2012
  -----------------------------------------------------------------------------
  function InspectionSearchExtract(
    a_JobNumber                         varchar2,
    a_LicenseNumber                     varchar2,
    a_JobStatus                         varchar2,
    a_InspectionType                    varchar2,
    a_InspectionDateFrom                date,
    a_InspectionDateTo                  date,
    a_InspectedBy                       varchar2,
    a_InspectionResult                  varchar2,
    a_ViolationType                     varchar2,
    a_LicenseType                       varchar2,
    a_Region                            varchar2
  ) return api.udt_ObjectList;


  -----------------------------------------------------------------------------
  -- EstablishmentSearch
  --  Search for Establishments for Save as Excel.
  -----------------------------------------------------------------------------
  function EstablishmentSearchExtract(
    a_EstablishmentType                 varchar2,
    a_DoingBusinessAs                   varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_Operator                          varchar2,
    a_IncludeHistory                    char
  ) return api.udt_ObjectList;

  -----------------------------------------------------------------------------
  -- Legal Entity Search
  --  Search for Legal Entities for Save as Excel
  -----------------------------------------------------------------------------
  function LegalEntitySearchExtract(
    a_LegalName                         varchar2,
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_LegalEntityTypeObjectId           udt_id,
    a_CorporationNumber                 varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_IncludeHistory                    char,
    a_IsBrandRegistrant                 char,
    a_IsPermittee                       char,
    a_SSNTIN                            varchar2,
    a_DriversLicense                    varchar2,
    a_ContactEmail                      varchar2
  ) return api.udt_objectlist;

  -----------------------------------------------------------------------------
  -- EducationEventSearch
  --  Search for Education Events
  -----------------------------------------------------------------------------
  procedure EducationEventSearch(
    a_EducationEventType                varchar2,
    a_EventName                         varchar2,
    a_StartDateFrom                     date,
    a_StartDateTo                       date,
    a_Objects                        out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- ClassesSearch
  --  Search for Education Classes
  -----------------------------------------------------------------------------
  procedure ClassesSearch(
    a_Course                varchar2,
    a_ClassDateFrom         date,
    a_ClassDateTo           date,
    a_Objects                        out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- CertificationRequestLicSearch
  --  On the request certification job, search for licenses related to the
  --  selected legal entity
  -----------------------------------------------------------------------------
  procedure CertificationRequestLicSearch(
    a_LicenseeObjectId                  number,
    a_LicenseNumber                     varchar2,
    a_EstablishmentName                 varchar2,
    a_PhysicalAddress                   varchar2,
    a_Objects                        out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- VintageLookup
  --  Look up Vintages from within system settings threshold
  -----------------------------------------------------------------------------
  procedure VintageLookup(
    a_VintageYear               varchar2,
    a_Objects                   out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- VintageLookupWithCurrent
  --  Look up Vintages from within sys settings threshold including any from
  --  outside this range that have been related to the object
  -----------------------------------------------------------------------------
  procedure VintageLookupWithCurrent(
    a_VintageYear     varchar2,
    a_SourceObjectId  number,
    a_Objects         out nocopy udt_ObjectList
    );

  -----------------------------------------------------------------------------
  -- VintageLookupAll
  --  Look up all Vintages
  -----------------------------------------------------------------------------
  procedure VintageLookupAll(
    a_VintageYear               varchar2,
    a_Objects                   out nocopy udt_ObjectList
    );

  -----------------------------------------------------------------------------
  -- DailyDepositSearch
  --  Search for Daily Deposit Jobs
  -----------------------------------------------------------------------------
  procedure DailyDepositSearch(
    a_JobNumber                         varchar2,
    a_FromDepositDate                   date,
    a_ToDepositDate                     date,
    a_FromCreatedDate                   date,
    a_ToCreatedDate                     date,
    a_Status                            varchar2,
    a_CreatedBy                         varchar2,
    a_Objects                       out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  -----------------------------------------------------------------------------
  -- DailyDepositExtract
  --  Search for Daily Deposit Jobs to be exported to Excel
  -----------------------------------------------------------------------------
  function DailyDepositExtract(
    a_JobNumber                         varchar2,
    a_FromDepositDate                   date,
    a_ToDepositDate                     date,
    a_FromCreatedDate                   date,
    a_ToCreatedDate                     date,
    a_Status                            varchar2,
    a_CreatedBy                         varchar2
  ) return api.udt_ObjectList;

  -----------------------------------------------------------------------------
  --  PaymentSearch
  --  Search for Payments.
  -----------------------------------------------------------------------------
  Procedure PaymentSearch(
    a_ReceiptNumber                      varchar2,
    a_PaymentMethod                      varchar2,
    a_PaymentDateFrom                    date,
    a_PaymentDateTo                      date,
    a_DepositDateFrom                    date,
    a_DepositDateTo                      date,
    a_GLAccount                          varchar2,
    a_Payor                              varchar2,
    a_PayerInternal                      varchar2,
    a_PayeeReference                     varchar2,
    a_IsRefund                           char,
    a_ReasonForRefund                    varchar2,
    a_ECommerceReferenceId               number default null,
    a_Results                            out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  -----------------------------------------------------------------------------
  -- PaymentSearchExtract
  -----------------------------------------------------------------------------
  function PaymentSearchExtract(
    a_ReceiptNumber                     varchar2,
    a_PaymentMethod                     varchar2,
    a_PaymentDateFrom                   date,
    a_PaymentDateTo                     date,
    a_DepositDateFrom                   date,
    a_DepositDateTo                     date,
    a_GLAccount                         varchar2,
    a_Payor                             varchar2,
    a_PayerInternal                     varchar2,
    a_PayeeReference                    varchar2,
    a_IsRefund                          char,
    a_ReasonForRefund                   varchar2,
    a_ECommerceReferenceId              number default null
  ) return api.udt_ObjectList;


  -----------------------------------------------------------------------------
  -- ProductRegistrantSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure ProductRegistrantSearch(
    a_DistributorObjectId               number,
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Objects                           out nocopy api.udt_ObjectList
  );


  -----------------------------------------------------------------------------
  --  PaymentAdjustmentSearch
  --  Search for Payment Adjustment jobs.
  --  Created By: Andy Patterson (CXUSA)
  --  Created On: 7/31/2014
  -----------------------------------------------------------------------------
  procedure PaymentAdjustmentSearch(
    a_AdjustmentDateFrom                date,
    a_AdjustmentDateTo                  date,
    a_DepositDateFrom                   date,
    a_DepositDateTo                     date,
    a_Results                           out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

 -----------------------------------------------------------------------------
  -- PaymentAdjustmentSearchExtract
  -----------------------------------------------------------------------------
  function PaymentAdjustmentSearchExtract(
    a_AdjustmentDateFrom                 date,
    a_AdjustmentDateTo                   date,
    a_DepositDateFrom                    date,
    a_DepositDateTo                      date
  )  return api.udt_ObjectList;

 ----------------------------------------------------------------------------
  -- PetitionLicenseSearch
  --  Search for Licenses for a Petition Job.
  -----------------------------------------------------------------------------
  procedure PetitionLicenseSearch(
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- LegalJobSearch
  --  Search for Legal Jobs on Internal Site
  -----------------------------------------------------------------------------
  procedure LegalJobSearch(
    a_JobNumber                         varchar2,
    a_DocketAppealNumber                varchar2,
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_JobType                           varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_DateSendToOALFrom                 date,
    a_DateSendToOALTo                   date,
    a_DAG                               varchar2,
    a_AppealWentToOAL                   char,
    a_AppealResultFine                  char,
    a_AppealType                        varchar2,
    a_Objects                       out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  -----------------------------------------------------------------------------
  -- LegalJobSearchExtract
  --  Extract Legal Jobs for Excel
  -----------------------------------------------------------------------------
  function LegalJobSearchExtract(
    a_JobNumber                         varchar2,
    a_DocketAppealNumber                varchar2,
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_JobType                           varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_DateSendToOALFrom                 date,
    a_DateSendToOALTo                   date,
    a_DAG                               varchar2,
    a_AppealWentToOAL                   char,
    a_AppealResultFine                  char,
    a_AppealType                        varchar2
  ) return api.udt_ObjectList;


  /*---------------------------------------------------------------------------
   * PermitSearch()
   *   Search for Permits
   *-------------------------------------------------------------------------*/
  procedure PermitSearch (
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_State                             varchar2,
    a_OriginalIssueDateFrom             date,
    a_OriginalIssueDateTo               date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Permittee                         varchar2,
    a_Location                          varchar2,
    a_LocationDesc                      varchar2,
    a_County                            number,
    a_Municipality                      number,
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_LicenseType                       number,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_SolicitorName                     varchar2,
    a_Objects                out nocopy api.udt_Objectlist,
    a_ExcelExtract                      varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * PermitSearchExtract()
   *   Search for Permits to be exported to Excel.
   *-------------------------------------------------------------------------*/
  function PermitSearchExtract (
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_State                             varchar2,
    a_OriginalIssueDateFrom             date,
    a_OriginalIssueDateTo               date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Permittee                         varchar2,
    a_Location                          varchar2,
    a_LocationDesc                      varchar2,
    a_County                            number,
    a_Municipality                      number,
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_LicenseType                       number,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_SolicitorName                     varchar2
  ) return api.udt_ObjectList;

  -----------------------------------------------------------------------------
  -- CPLInternalLicenseSelect
  --  Look up Licenses based on their type state and whether they require a CPL Submission,
  --  this procedure is used for the internal CPL Submission job
  -----------------------------------------------------------------------------
  procedure CPLInternalLicenseSelect(
    a_LicenseNumber                    varchar2,
    a_Licensee                         varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- CPLInternalPermitSelect
  --  Look up Permits based on their type state and whether they require a CPL Submission,
  --  this procedure is used for the internal CPL Submission job
  -- Programmer : Elijah R.
  -- Date       : 2015 Dec 30
  -----------------------------------------------------------------------------
  procedure CPLInternalPermitSelect(
    a_PermitNumber                     varchar2,
    a_Permittee                        varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- CPLPublicLicenseSelect
  --  Look up Licenses based on their type state and whether they require a CPL Submission,
  --  this procedure is used for the internal CPL Submission job
  -----------------------------------------------------------------------------
  procedure CPLPublicLicenseSelect(
    a_LicenseNumber                    varchar2,
    a_Licensee                         varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- CPLPublicPermitSelect
  --  Look up Permits based on their type state and whether they require a CPL Submission,
  -- Programmer: Elijah R.
  -- Date:       2015 Dec 30
  -----------------------------------------------------------------------------
  procedure CPLPublicPermitSelect(
    a_PermitNumber                     varchar2,
    a_Permittee                        varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- License Lookup
  --  Look up Licenses based on entered criteria
  -----------------------------------------------------------------------------
  procedure LicenseLookup1(
    a_IsAmendable                      char,
    a_IsRenewable                      char,
    a_IsReinstatable                   char,
    a_IsInspectable                    char,
    a_IsAccusable                      char,
    a_LicenseNumber                    varchar2,
    a_LicenseType                      varchar2,
    a_State                            varchar2,
    a_IssueDateFrom                    date,
    a_IssueDateTo                      date,
    a_EffectiveDateFrom                date,
    a_EffectiveDateTo                  date,
    a_ExpirationDateFrom               date,
    a_ExpirationDateTo                 date,
    a_InactivityStartDateFrom          date,
    a_InactivityStartDateTo            date,
    a_Establishment                    varchar2,
    a_PhysicalAddress                  varchar2,
    a_Licensee                         varchar2,
    a_Region                           varchar2,
    a_Office                           varchar2,
    a_Restriction                      varchar2,
    a_CriminalConviction               varchar2,
    a_ConflictOfInterest               varchar2,
    a_ObjectList                       out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- InactiveUserSearch
  --  Search for Inactive Users.
  -----------------------------------------------------------------------------
  procedure InactiveUserSearch(
    a_ToDate                            date,
    a_FromDate                          date,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  ---------------------------------------------------------------------------------
  -- FilteredPermitLookup_TAP
  --  Search for Permits, filter by TAP Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredPermitLookup_TAP(
    a_PermitNumber                    varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  );

  ---------------------------------------------------------------------------------
  -- FilteredPermitLookup_Solicitor
  --  Search for Permits, filter by Solicitor Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredPermitLookup_Solicitor(
    a_PermitNumber                    varchar2,
    a_SolicitorName                   varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  );



  -------------------------------------
  -- Programmer : Michael F.
  -- Date       : 2015 Aug 07
  -- Purpose    : Search for Active CPL Data for Save as Excel.
  -- Updated    : Elijah R. 2015 Dec 30
  -- Notes      : Added search by Permit capability
  -------------------------------------
  function CPLDataSearchExtract(
    a_ProductName                      varchar2,
    a_ProductRegistrationNumber        varchar2,
    a_SKU                              varchar2,
    a_EffectiveFrom                    date,
    a_EffectiveTo                      date,
    a_LicenseNumber                    varchar2,
    a_Licensee                         varchar2,
    a_PermitNumber                     varchar2,
    a_Permittee                        varchar2,
    a_UnitVolume                       number,
    a_CloseOut                         varchar2,
    a_Combo                            varchar2,
    a_RIP                              varchar2
  ) return api.udt_ObjectList;

  -------------------------------------
  -- Programmer : Michael F.
  -- Date       : 2015 Aug 07
  -- Purpose    : Search for Active CPL Data
  -- Updated    : Elijah R. 2015 Dec 30
  -- Notes      : Added search by Permit capability
  -------------------------------------
  procedure CPLDataSearch(
    a_ProductName                      varchar2,
    a_ProductRegistrationNumber        varchar2,
    a_SKU                              varchar2,
    a_EffectiveFrom                    date,
    a_EffectiveTo                      date,
    a_LicenseNumber                    varchar2,
    a_Licensee                         varchar2,
    a_PermitNumber                     varchar2,
    a_Permittee                        varchar2,
    a_UnitVolume                       number,
    a_CloseOut                         varchar2,
    a_Combo                            varchar2,
    a_RIP                              varchar2,
    a_Objects                      out nocopy udt_ObjectList,
    a_ExcelExtract                     varchar2 default 'N'
  );

  -------------------------------------------
  -- Programmer : Iwasam Agube
  -- Date       : September-01-2015
  -- Purpose    : Search for Missed CPL Data
  -------------------------------------------
  procedure MissedCPLSearch(
    a_SubmissionPeriod             varchar2,
    a_Objects                      out nocopy udt_ObjectList,
    a_ExcelExtract                 varchar2 default 'N'
  )
  ;

      ---------------------------------------------------------------------
  -- Programmer : Iwasam Agube
  -- Date       : September-02-2015
  -- Purpose    : Search for Licenses with Missed CPL for save as excel
  ---------------------------------------------------------------------
  function MissedCPLSearchExtract(
    a_SubmissionPeriod             varchar2
  ) return api.udt_ObjectList;

 -----------------------------------------------------------------------------
  -- PermitRenewalLookup
  --  Lookup for Permits
  --  Updated by: Joshua Camps (CXUSA)
  --  Updated on: 09/15/15
  -----------------------------------------------------------------------------
  procedure PermitRenewalLookup(
    a_PermitNumber                varchar2,
    a_PermitType                  varchar2,
    a_State                       varchar2,
    a_IssueDateFrom               date,
    a_IssueDateTo                 date,
    a_EffectiveDateFrom           date,
    a_EffectiveDateTo             date,
    a_ExpirationDateFrom          date,
    a_ExpirationDateTo            date,
    a_Permittee                   varchar2,
    a_Objects                     out nocopy api.udt_Objectlist,
    a_ExcelExtract                varchar2 default 'N'
  );
  
  /*---------------------------------------------------------------------------
   * PermitLookup() -- PUBLIC
   *   General Lookup script for permits with no conditions
   *-------------------------------------------------------------------------*/
  procedure PermitLookup(
    a_PermitNumber                varchar2,
    a_PermitType                  varchar2,
    a_State                       varchar2,
    a_IssueDateFrom               date,
    a_IssueDateTo                 date,
    a_EffectiveDateFrom           date,
    a_EffectiveDateTo             date,
    a_ExpirationDateFrom          date,
    a_ExpirationDateTo            date,
    a_Permittee                   varchar2,
    a_Objects                     out nocopy api.udt_Objectlist,
    a_TapOnly                     varchar2 default 'N',
    a_ExcelExtract                varchar2 default 'N'
  );  
  
  /*---------------------------------------------------------------------------
   * PermitLookupTap() -- PUBLIC
   *   General Lookup script for Tap Permits
   *-------------------------------------------------------------------------*/
  procedure PermitLookupTap(
    a_PermitNumber                varchar2,
    a_PermitType                  varchar2,
    a_State                       varchar2,
    a_IssueDateFrom               date,
    a_IssueDateTo                 date,
    a_EffectiveDateFrom           date,
    a_EffectiveDateTo             date,
    a_ExpirationDateFrom          date,
    a_ExpirationDateTo            date,
    a_Permittee                   varchar2,
    a_Objects                     out nocopy api.udt_Objectlist
  );

  ---------------------------------------------------------------------------------
  -- FilteredRelatedLicenseLookup
  --  Search for Licenses, filter by License Type that allow Additional Warehouse
  ---------------------------------------------------------------------------------
  procedure FilteredRelatedLicenseLookup(
    a_LicenseNumber                   varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  );

  -----------------------------------------------------------------------------
  -- PublicDistributorProductSearch
  --  Search for Distributor Products on the Public website.
  -----------------------------------------------------------------------------
  procedure PublicDistributorProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_UserId                            number,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  ---------------------------------------------------------------------------------
  -- FilteredPermitLookup_Insignia
  --  Search for Insignia Permits, filter by Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredPermitLookup_Insignia(
    a_PermitTypeObjectId              number,
    a_PermitNumber                    varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  );

 -----------------------------------------------------------------------------
  -- ProductSearch
  --  Search for Products for Save as Excel
  -----------------------------------------------------------------------------
  function PublicProductSearchExtract(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_UserId                            number
  ) return api.udt_ObjectList;

 -----------------------------------------------------------------------------
  -- ProductSearch
  --  Search for Distributed Products for Save as Excel
  -----------------------------------------------------------------------------
  function PublicDistributorSearchExtract(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_UserId                            number
  ) return api.udt_ObjectList;

  -----------------------------------------------------------------------------
  -- Legal Entity Search for Public User
  --  Search for Legal Entities to associate with a Public User
  -----------------------------------------------------------------------------
  procedure LegalEntitySearchPubUser(
    a_LegalName                         varchar2,
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_LegalEntityTypeObjectId           udt_id,
    a_CorporationNumber                 varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_IncludeHistory                    char,
    a_IsBrandRegistrant                 char,
    a_IsPermittee                       char,
    a_SSNTIN                            varchar2,
    a_DriversLicense                    varchar2,
    a_ContactEmail                      varchar2,
    a_PublicUserId                      udt_id,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * PoliceUserSearch() -- PUBLIC
   *   Search for Police Users
   *-------------------------------------------------------------------------*/
  procedure PoliceUserSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_EmailAddress                      varchar2,
    a_PhoneNumber                       varchar2,
    a_BusinessPhone                     varchar2,
    a_Municipality                      varchar2,
    a_UserType                          varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FilteredVehicleSearch()
   *   Used on the Vehicles tab on Legal Entity.
   * Search for vehicles based on the criteria entered on the Vehicles tab
   *-------------------------------------------------------------------------*/
  procedure FilteredVehicleSearch(
    a_MakeModelYear                     varchar2,
    a_StateRegistration                 varchar2,
    a_StateOfRegistration               varchar2,
    a_VIN                               varchar2,
    a_OwnedOrLeasedLimousine            varchar2,
    a_InsigniaNumber                    varchar2,
    a_PermitNumber                      varchar2,
    a_PermitTypeObjectId                udt_Id,
    a_PermitState                       varchar2,
    a_SourceObjectId                    udt_Id,
    a_ObjectList                        out nocopy udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * ActiveDistributorLicenses
   * Search for active Distributor Licenses, filtered by Licensee
   *-------------------------------------------------------------------------*/
  procedure ActiveDistributorLicenses(
    a_LicenseNumber                     varchar2,
    a_Licensee                          udt_id,
    a_Objects                           out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * DocumentTypesForPublicMenu
   *   Used on the Entry Point Document Templates, on the public site.
   *-------------------------------------------------------------------------*/
  procedure DocumentTypesForPublicMenu (
    a_ObjectList                        out nocopy udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * LicenseJobSearch
   *   Search for License Jobs only
   *-------------------------------------------------------------------------*/
  procedure LicenseJobSearch(
    a_ObjectList                        out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PolicePermitAppSearch() -- PUBLIC
   *   Search for Permit Applications from the Police portal
   *-------------------------------------------------------------------------*/
  procedure PolicePermitAppSearch(
    a_FileNumber                        varchar2,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_ApplicationType                   varchar2,
    a_Permittee                         varchar2,
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_TradeName                         varchar2,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_Objects                out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * MuniPermitAppSearch() -- PUBLIC
   *   Search for Permit Applications from the Municipal portal
   *-------------------------------------------------------------------------*/
  procedure MuniPermitAppSearch(
    a_FileNumber                        varchar2,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_ApplicationType                   varchar2,
    a_Permittee                         varchar2,
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_TradeName                         varchar2,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_Objects                out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * CPLSubmissionSearch()
   *   Search CPL Submissions from the Public portal
   *-------------------------------------------------------------------------*/
  procedure CPLSubmissionSearch (
    a_CPLSubmissionType                 varchar2,
    a_Wholesaler                        varchar2,
    a_SubmissionPeriodId                number,
    a_Objects                out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * CPLSubmissionSearchExtract()
   *   Search CPL Submissions from the Public portal
   *-------------------------------------------------------------------------*/
  function CPLSubmissionSearchExtract (
    a_CPLSubmissionType                 varchar2,
    a_Wholesaler                        varchar2,
    a_SubmissionPeriodObjectId          number
  ) return api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * InternalCPLSubmissionSearch()
   *   Search CPL Submissions from the Public portal
   *-------------------------------------------------------------------------*/
  procedure InternalCPLSubmissionSearch (
    a_CPLSubmissionType                 varchar2,
    a_Wholesaler                        varchar2,
    a_PostingDateFrom                   varchar2,
    a_PostingDateTo                     varchar2,
    a_Objects                out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * CPLSubmissionSearchExtractInternal() -- PUBLIC
   *   Search CPL Submissions from the Public portal
   *-------------------------------------------------------------------------*/
  function CPLSubmissionSearchExtractInternal (
    a_CPLSubmissionType                varchar2,
    a_Wholesaler                       varchar2,
    a_PostingDateFrom                  varchar2,
    a_PostingDateTo                    varchar2
  ) return api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * UserPaymentHistorySearch()
   *   Given a User Id, return all payment transactions from the last 90 days.
   *-------------------------------------------------------------------------*/
  procedure UserPaymentHistorySearch (
    a_UserId                            udt_Id,
    a_PaymentDateFrom                   date,
    a_PaymentDateTo                     date,
    a_Objects                out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * JobTypeSearch()
   *   Given a Job Type name return all ABC Job Types.
   *-------------------------------------------------------------------------*/
  procedure JobTypeSearch (
    a_Description                       varchar2,
    a_Name                              varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * UserExternalSearch() -- PUBLIC
   *   Search for Municipal, Police, and Public Users on Internal Site
   *-------------------------------------------------------------------------*/
  procedure UserExternalSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_EmailAddress                      varchar2,
    a_PhoneNumber                       varchar2,
    a_BusinessPhone                     varchar2,
    a_UserType                          varchar2,
    a_Municipality                      varchar2,
    a_LegalEntity                       varchar2,
    a_LicenseNumber                     varchar2,
    a_PermitNumber                      varchar2,
    a_Objects                       out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * UserExternalSearchExtract() -- PUBLIC
   *   Search Extract for Municipal, Police, and Public Users on Internal Site
   *-------------------------------------------------------------------------*/
  function UserExternalSearchExtract(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_EmailAddress                      varchar2,
    a_PhoneNumber                       varchar2,
    a_BusinessPhone                     varchar2,
    a_UserType                          varchar2,
    a_Municipality                      varchar2,
    a_LegalEntity                       varchar2,
    a_LicenseNumber                     varchar2,
    a_PermitNumber                      varchar2
  ) return api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * EmailUsageReport() -- PUBLIC
   *   Search for emails. Used in search procedure s_EmailUsageReport
   *-------------------------------------------------------------------------*/
  Procedure EmailUsageReport(
    a_FromCreatedDate       date,
    a_ToCreatedDate         date,
    a_ObjectList            out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * SMSUsageReport() -- PUBLIC
   *   Search for sms. Used in search procedure s_SMSUsageReport
   *-------------------------------------------------------------------------*/
  Procedure SMSUsageReport(
    a_FromCreatedDate       date,
    a_ToCreatedDate         date,
    a_ObjectList            out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PublicPermitSearch() -- PUBLIC
   *   Search for permits on the public legal entity
   *-------------------------------------------------------------------------*/
  procedure PublicPermitSearch(
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_Permittee                         varchar2,
    a_State                             varchar2,
    a_UserID                            number,
    a_ObjectList                        out nocopy udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * PublicPermitSearchExtract() -- PUBLIC
   *   Provides Excel Extract for public pemrit search.
   *-------------------------------------------------------------------------*/
  function PublicPermitSearchExtract(
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_Permittee                         varchar2,
    a_State                             varchar2,
    a_UserID                            number
  ) return api.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * SetDateSearchCriteria()
   *   Set From/To Date Search Criteria if no date supplied and add timestamps
   * to take the full day into account.
   *-------------------------------------------------------------------------*/
  function SetDateSearchCriteria (
    a_Date                              date,
    a_IsFromDate                        boolean default true
  ) return date;

  /*---------------------------------------------------------------------------
   * FilteredPermitSearch()
   *   Used on the Permits Tab of the Legal Entity to search for Permits
   * related to the Legal Entity based on the criteria entered on the Permits
   * tab.
   *-------------------------------------------------------------------------*/
  procedure FilteredPermitSearch (
    a_PermitNumber                      varchar2,
    a_PermitTypeObjectId                udt_Id,
    a_Permittee                         varchar2,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    udt_Id,
    a_ObjectList             out nocopy api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * ApprovedTermsSearch()
   *   Search for licenses with granted petition terms. Search returns LICENSES
   *-------------------------------------------------------------------------*/
  procedure ApprovedTermsSearch (
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_LicenseType                       varchar2,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_PetitionType                      varchar2,
    a_UserId                            udt_Id,
    a_ObjectList             out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  );

  /*---------------------------------------------------------------------------
   * ApprovedTermsSearchExtract()
   *   Extract all results from ApprovedTermsSearch for excel
   *-------------------------------------------------------------------------*/
  function ApprovedTermsSearchExtract (
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_LicenseType                       varchar2,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_PetitionType                      varchar2,
    a_UserId                            udt_Id
  ) return api.udt_ObjectList;

end pkg_ABC_SearchProcedures;
/
grant execute
on pkg_abc_searchprocedures
to posseextensions;

create or replace package body pkg_ABC_SearchProcedures is

  gc_CPLActiveDataObjectDef constant udt_Id := api.pkg_configquery.ObjectDefIdForName('o_ABC_CPLActiveData');

  -----------------------------------------------------------------------------
  -- Function and procedure implementations
  --
  -- Change History:
  -- 08Aug2011, Stan H:  Do not use cxproceduralsearch in ProcessTypeSearch since
  --      it is case-sensitive for external tables.  A simple query is fine in
  --      terms of performance because size is on order of 100 and search is seldom run.
  -- 13Sep2013, Michel S: Changed EstablishmentLookup, EstablishmentSearch and EstablishmentSearchExtract
  --      to search on DBA (Doing Business As), not on Legal Name.
  --      Task 003496 - POSSE ABC
  -----------------------------------------------------------------------------
  -----------------------------------------------------------------------------
  -- OutriderSearchLimit
  --  Retrieve the number of rows to limit search to
  -----------------------------------------------------------------------------
  function OutriderSearchLimit return number as
      t_SearchLimit  number;

  begin
    select nvl(a.OutriderSearchLimit, 500)
    into t_SearchLimit
    from query.o_systemsettings a;

    return t_SearchLimit;
  end;

  -----------------------------------------------------------------------------
  -- OutriderSearchLimitError
  --  Format the error string to present to the user when too many rows are returned
  -----------------------------------------------------------------------------
  function OutriderSearchLimitError (a_SearchLimit number) return string as
  begin

    Return 'Your search returned more than '|| to_char(a_SearchLimit) ||' records. Please refine your search criteria.';

  end;

-----------------------------------------------------------------------------
  -- SearchResultsToObjList
  --   Convert Search Results to an api.udt_ObjectList
  -- Programmer: Andy Patterson
  -- Date:       07/31/2014
  -----------------------------------------------------------------------------
  function SearchResultsToObjList(
    a_SearchResults     api.pkg_Definition.udt_SearchResults
  ) return api.udt_ObjectList is
    t_Results           api.udt_ObjectList;
    t_idlist            api.pkg_Definition.udt_idlist;

  begin

    t_Results := api.udt_objectlist();
    t_idlist := api.pkg_search.SearchResultsToList(a_SearchResults);

    for x in 1..t_idlist.count loop
      t_Results.extend;
      t_Results(x) := api.udt_Object(t_idlist(x));
    end loop;
    return t_Results;

  end SearchResultsToObjList;

  -----------------------------------------------------------------------------
  -- UsersSearch
  --  Search for Users.
  -----------------------------------------------------------------------------
  procedure UserSearch(
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_Active                            char,
    a_UserType                          varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
  begin
    if a_FirstName is null and a_LastName is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
   --api.pkg_Errors.RaiseError(-20000, a_Office);
    a_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('u_Users');
    extension.pkg_CxProceduralSearch.SearchByIndex('FirstName', a_FirstName, null, true);
    extension.pkg_CxProceduralSearch.SearchByIndex('LastName', a_LastName, null, true);
    if a_Active = 'Y' then
      extension.pkg_CxProceduralSearch.FilterObjects('Active', a_Active, null, False);
    end if;
    extension.pkg_CxProceduralSearch.SearchByIndex('UserType', a_UserType, a_UserType, false);
    extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and');
  end UserSearch;

  /*---------------------------------------------------------------------------
   * PublicUserSearch() -- PUBLIC
   *   Search for Public Users
   *-------------------------------------------------------------------------*/
  procedure PublicUserSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_EmailAddress                      varchar2,
    a_PhoneNumber                       varchar2,
    a_BusinessPhone                     varchar2,
    a_UserType                          varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
  begin
    -- Check to make sure at least one criteria has been entered.
    if a_FirstName is null
        and a_LastName is null
        and a_EmailAddress is null
        and a_PhoneNumber is null
        and a_BusinessPhone is null
    then
        api.pkg_Errors.RaiseError(-20000, 'You must enter some search criteria.');
    end if;

    UserExternalSearch(a_FirstName, a_LastName, a_EmailAddress, a_PhoneNumber,
        a_BusinessPhone, a_UserType, null, null, null,
        null, a_Objects);

  end PublicUserSearch;

  -----------------------------------------------------------------------------
  -- PortalApplicationSearch
  --  Search for Applications by Municipal Users's Municipality
  -----------------------------------------------------------------------------
  procedure PortalApplicationSearch(
    a_FileNumber                        varchar2,
    a_LicenseNumber                     varchar2,
    a_ApplicationType                   varchar2,
    a_Licensee                          varchar2,
    a_TradeName                         varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_UserId                            api.pkg_Definition.udt_Id;
    t_MunicipalityName                  varchar2(50);
    t_AmendmentType                     varchar2(50);
    t_NewApplication                    api.udt_ObjectList;
    t_RenewalApplication                api.udt_ObjectList;
    t_PersonToPersonApplication         api.udt_ObjectList;
    t_PlaceToPlaceApplication           api.udt_ObjectList;
  begin

    if a_FileNumber is null
        and a_LicenseNumber is null
        and a_ApplicationType is null
        and a_Licensee is null
        and a_TradeName is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    a_Objects := api.udt_ObjectList();
    t_NewApplication := api.udt_ObjectList();
    t_RenewalApplication := api.udt_ObjectList();
    t_PersonToPersonApplication := api.Udt_Objectlist();
    t_PlaceToPlaceApplication := api.udt_ObjectList();

    -- Find out what Municipality the user is in
    t_UserId := api.pkg_SecurityQuery.EffectiveUserId;
    t_MunicipalityName := api.pkg_ColumnQuery.Value(t_UserId, 'MunicipalityName');
    if a_ApplicationType in ('Person-To-Person Transfer', 'Place-To-Place Transfer') then
      t_AmendmentType := a_ApplicationType;
    end if;

    if a_ApplicationType = 'New Application' or a_ApplicationType is null then
      extension.pkg_CxProceduralSearch.InitializeSearch('p_ABC_SubmitResolution');
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('NewApplication', 'ExternalFileNum', a_FileNumber, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('NewApplicationLicense', 'LicenseNumber', a_LicenseNumber, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('NewApplicationLegalEntity', 'dup_FormattedName', a_Licensee, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('NewApplicationEstablishment', 'DoingBusinessAs', a_TradeName, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('NewApplicationMunicipality', 'Name', t_MunicipalityName, null, false);
      extension.pkg_CxProceduralSearch.PerformSearch(t_NewApplication, 'and');
    end if;

    if a_ApplicationType = 'Renewal' or a_ApplicationType is null then
      extension.pkg_CxProceduralSearch.InitializeSearch('p_ABC_SubmitResolution');
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('RenewalApplication', 'ExternalFileNum' , a_FileNumber, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('RenewalApplicationLicense', 'LicenseNumber', a_LicenseNumber, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('RenewalApplicationLegalEntity', 'dup_FormattedName', a_Licensee, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('RenewalAppEstablishment', 'DoingBusinessAs', a_TradeName, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('RenewalApplicationMunicipality', 'Name', t_MunicipalityName, null, false);
      extension.pkg_CxProceduralSearch.PerformSearch(t_RenewalApplication, 'and');
    end if;

    if a_ApplicationType = 'Person-To-Person Transfer' or a_ApplicationType is null then
      extension.pkg_CxProceduralSearch.InitializeSearch('p_ABC_SubmitResolution');
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendmentApplication', 'ExternalFileNum' , a_FileNumber, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendmentApplicationLicense', 'LicenseNumber', a_LicenseNumber, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendApplicationLegalEntity', 'dup_FormattedName', a_Licensee, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendmentAppEstablishment', 'DoingBusinessAs', a_TradeName, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendApplicationMunicipality', 'Name', t_MunicipalityName, null, false);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendmentAppAmendmentType', 'Name', t_AmendmentType, null, false);
      extension.pkg_CxProceduralSearch.PerformSearch(t_PersonToPersonApplication, 'and');
    end if;

    if a_ApplicationType = 'Place-To-Place Transfer' or a_ApplicationType is null then
      extension.pkg_CxProceduralSearch.InitializeSearch('p_ABC_SubmitResolution');
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendmentApplication', 'ExternalFileNum' , a_FileNumber, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendmentApplicationLicense', 'LicenseNumber', a_LicenseNumber, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendApplicationLegalEntity', 'dup_FormattedName', a_Licensee, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendmentAppEstablishment', 'DoingBusinessAs', a_TradeName, null, true);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendApplicationMunicipality', 'Name', t_MunicipalityName, null, false);
      extension.pkg_CxProceduralSearch.SearchByRelatedIndex('AmendmentAppAmendmentType', 'Name', t_AmendmentType, null, false);
      extension.pkg_CxProceduralSearch.PerformSearch(t_PlaceToPlaceApplication, 'and');
    end if;

    -- Append results
    extension.pkg_CollectionUtils.Append(a_Objects, t_NewApplication);
    extension.pkg_CollectionUtils.Append(a_Objects, t_RenewalApplication);
    extension.pkg_CollectionUtils.Append(a_Objects, t_PersonToPersonApplication);
    extension.pkg_CollectionUtils.Append(a_Objects, t_PlaceToPlaceApplication);

  exception
    -- We shouldn't get here, this is just in case.
    when no_data_found then
      raise_application_error(-20000, 'User does not have a configured Municipality');

  end PortalApplicationSearch;

  /*---------------------------------------------------------------------------
   * MunicipalUserSearch() -- PUBLIC
   *   Search for Municipal Users
   *-------------------------------------------------------------------------*/
  procedure MunicipalUserSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_BusinessPhone                     varchar2,
    a_UserType                          varchar2,
    a_Municipality                      varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
  begin
    -- Check to make sure at least one criteria has been entered.
    if a_FirstName is null
        and a_LastName is null
        and a_BusinessPhone is null
        and a_Municipality is null
    then
        api.pkg_Errors.RaiseError(-20000, 'You must enter some search criteria.');
    end if;

    UserExternalSearch(a_FirstName, a_LastName, null, null,
        a_BusinessPhone, a_UserType, a_Municipality, null, null,
        null, a_Objects);

  end MunicipalUserSearch;

  /*---------------------------------------------------------------------------
   * MunicipalitySearch() -- PUBLIC
   *   Search for Municipalities.
   *-------------------------------------------------------------------------*/
  procedure MunicipalitySearch (
    a_Name                              varchar2,
    a_Code                              varchar2,
    a_County                            varchar2,
    a_HideInactive                      varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_CountyName                        varchar2(4000);
    t_Name                              varchar2(4000) := a_Name;
    t_Objects                           api.udt_ObjectList;
  begin

    if a_Name is null and a_Code is null and a_County is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    elsif a_Name is null and a_Code is null and a_County is not null then
      t_Name := '%';
    end if;

    a_Objects := api.udt_ObjectList();
    t_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('o_ABC_Office');
    extension.pkg_CxProceduralSearch.SearchByIndex('Name', t_Name, null, true);
    extension.pkg_CxProceduralSearch.SearchByIndex('dup_MunicipalityCountyCode', a_Code, null,
        true);
    if a_HideInactive is null then
      extension.pkg_CxProceduralSearch.SearchByIndex('Active', 'Y', null);
    end if;
    extension.pkg_CxProceduralSearch.PerformSearch(t_Objects, 'and');
    -- The dropdown list that we are using passes in an ObjectId rather than the actual Name
    if a_County is not null then
      select api.udt_Object(x.objectid)
      bulk collect into a_Objects
      from table(cast(t_Objects as api.udt_objectlist)) x
           join query.r_ABC_RegionOffice o
               on o.OfficeObjectId = x.objectid
           join query.o_Abc_Region r
               on r.ObjectId = o.RegionObjectId
      where r.objectid = a_County;
    else
      a_Objects := t_Objects;
    end if;

  exception
    -- This should never be reached. It can only happen if the select gets called with
    -- an incorrect ObjectId. The expression we are using should prevent this.
    when No_Data_Found then
      raise_application_error(-20000, 'You must enter a valid County.');

  end MunicipalitySearch;

  -----------------------------------------------------------------------------
  -- InstructorSearch
  --  Search for Instructor Users.
  -----------------------------------------------------------------------------
  procedure InstructorSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_Active                            char,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
  begin
    if a_FirstName is null and a_LastName is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    a_Objects := api.udt_ObjectList();
    extension.pkg_CxProceduralSearch.InitializeSearch('u_Users');
    extension.pkg_CxProceduralSearch.SearchByIndex('FirstName', a_FirstName, null, true);
    extension.pkg_CxProceduralSearch.SearchByIndex('LastName', a_LastName, null, true);
    extension.pkg_CxProceduralSearch.FilterObjects('IsInstructor', 'Y', null, False);
    if a_Active = 'Y' then
      extension.pkg_CxProceduralSearch.FilterObjects('Active', a_Active, null, False);
    end if;
    extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and');
  end InstructorSearch;


  -----------------------------------------------------------------------------
  -- ProcessTypeSearch
  --  Search for Process Types.
  --
  -- Change History:
  -- 08Aug2011, Stan H:  Do not use cxproceduralsearch in ProcessTypeSearch since
  --      it is case-sensitive for external tables.  A simple query is fine in
  --      terms of performance because size is on order of 100 and search is seldom run.
  -----------------------------------------------------------------------------
  procedure ProcessTypeSearch(
    a_Description                       varchar2,
    a_Name                              varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is

  t_IdList                              udt_IdList;

  begin

    if a_Description is null and a_Name is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    select pt.objectid
      bulk collect into t_IdList
      from query.o_processtype pt
     where (lower(pt.Description) like lower(a_description)||'%' or a_description is null)
       and (lower(pt.Name) like lower(a_name)||'%' or a_name is null)
       and lower(pt.Name) like 'p_abc_%';
    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);

  end ProcessTypeSearch;


  -----------------------------------------------------------------------------
  -- AvailableSecLicTypeSearch
  --  Search for valid Secondary License Types based on the Primary License Type selected.
  -----------------------------------------------------------------------------
  procedure AvailableSecLicTypeSearch(
    a_PrimaryLicenseTypeObjectId        number,
    a_Website                           varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is

  begin
    if a_PrimaryLicenseTypeObjectId is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    a_ObjectList := api.udt_ObjectList();
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_LicenseType');
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex('Primary', 'ObjectId', a_PrimaryLicenseTypeObjectId, null, true);
    extension.pkg_CxProceduralSearch.FilterObjects('Active', 'Y', null, False);

    if a_Website = 'Online' then
      extension.pkg_CxProceduralSearch.FilterObjects('AvailableOnline', 'Y', null, False);
    end if;

    extension.pkg_cxProceduralSearch.PerformSearch(a_ObjectList, 'and');
  end AvailableSecLicTypeSearch;

  -----------------------------------------------------------------------------
  -- ApplyLicenseCriteria
  --
  -----------------------------------------------------------------------------
  function ApplyLicenseCriteria(
    a_LicenseNumber                     varchar2,
    a_LicenseType                       varchar2,
    a_State                             varchar2,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_Establishment                     varchar2,
    a_PhysicalAddress                   varchar2,
    a_Licensee                          varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_Restriction                       varchar2,
    a_CriminalConviction                varchar2,
    a_ConflictOfInterest                varchar2
    ) return api.udt_ObjectList is

    --return api.udt_ObjectList is
    t_Objects                           api.udt_ObjectList;
    t_IssueDateFrom                     date;
    t_IssueDateTo                       date;
    t_EffectiveDateFrom                 date;
    t_EffectiveDateTo                   date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_InactivityStartDateFrom           date;
    t_InactivityStartDateTo             date;
    t_IsDashboardSearch                 boolean := true;
    t_Count                             number;

  begin

   --Dashboard - we need to make License Type and Region more flexible for the Dashboard drill down (criteria NOT coming in as an ObjectId)
    if a_LicenseType is not null then
       select count(1)
         into t_Count
         from query.o_abc_licensetype lt
        where to_char(lt.ObjectId) = a_LicenseType;

       if t_Count > 0 then
         t_IsDashboardSearch := false;

       end if;
     end if;

    if a_Region is not null then
       select count(1)
         into t_Count
         from query.o_abc_region r
        where to_char(r.ObjectId) = a_Region;

       if t_Count > 0 then
         t_IsDashboardSearch := false;
       end if;
     end if;


    /* Dates in Outrider come in as 0000h of the day
       so manipulate dates to give full search of a given day */
    -- IssueDate
    if a_IssueDateFrom is not null or a_IssueDateTo is not null then
      if a_IssueDateFrom is null then
        t_IssueDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_IssueDateFrom := to_date(to_char(a_IssueDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_IssueDateTo is null then
        t_IssueDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_IssueDateTo := to_date(to_char(a_IssueDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- EffectiveDate
    if a_EffectiveDateFrom is not null or a_EffectiveDateTo is not null then
      if a_EffectiveDateFrom is null then
        t_EffectiveDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_EffectiveDateFrom := to_date(to_char(a_EffectiveDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EffectiveDateTo is null then
        t_EffectiveDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_EffectiveDateTo := to_date(to_char(a_EffectiveDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- ExpirationDate
    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- Inactivity Start Date
    if a_InactivityStartDateFrom is not null or a_InactivityStartDateTo is not null then
      if a_InactivityStartDateFrom is null then
        t_InactivityStartDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_InactivityStartDateFrom := to_date(to_char(a_InactivityStartDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_InactivityStartDateTo is null then
        t_InactivityStartDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_InactivityStartDateTo := to_date(to_char(a_InactivityStartDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    t_Objects := api.udt_ObjectList();

    extension.pkg_cxproceduralsearch.SearchByIndex('State',a_State,a_State,false);
    extension.pkg_cxproceduralsearch.SearchByIndex('IssueDate', t_IssueDateFrom,t_IssueDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('EffectiveDate', t_EffectiveDateFrom,t_EffectiveDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('ExpirationDate', t_ExpirationDateFrom,t_ExpirationDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('InactivityStartDate', t_InactivityStartDateFrom,t_InactivityStartDateTo);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Establishment', 'DoingBusinessAs', a_Establishment, a_Establishment, true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('PhysicalAddress', 'dup_FormattedDisplay', a_PhysicalAddress, a_PhysicalAddress, true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Licensee', 'dup_FormattedName', a_Licensee, a_Licensee, true);
    --extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Operator', 'dup_FormattedName', a_Operator, a_Operator, true);

    if a_LicenseType is not null then
      if t_IsDashboardSearch then
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Region', 'Name', a_Region, a_Region, false);
      else
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Region', 'ObjectId', a_Region, a_Region, false);
      end if;
    else
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Region', 'ObjectId', a_Region, a_Region, false);
    end if;

    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Office', 'ObjectId', a_Office, a_Office, false);
    --extension.pkg_cxproceduralsearch.SearchByRelatedIndex('RiskFactor', 'ObjectId', a_RiskFactor, a_RiskFactor, false);
    if a_Restriction is not null then
      case a_Restriction
        when 'Yes' then
          extension.pkg_CxProceduralSearch.SearchByRelatedIndex('LicenseWarning', 'dup_WarningActive', 'Y', 'Y', false);
        when 'No' then
          extension.pkg_CxProceduralSearch.SearchByRelatedIndex('LicenseWarning', 'dup_WarningActive', 'N', 'N', false);
      end case;
    end if;
    extension.pkg_cxproceduralsearch.SearchByIndex('CriminalConviction', a_CriminalConviction, a_CriminalConviction, false);
    extension.pkg_cxproceduralsearch.SearchByIndex('ConflictOfInterest', a_ConflictOfInterest, a_ConflictOfInterest, false);
    extension.pkg_CxProceduralSearch.FilterObjects('dup_HasBeenIssued','Y','Y',false);


  end ApplyLicenseCriteria;
/*  -----------------------------------------------------------------------------
  -- LicenseSearch
  --  Search for Licenses.
  -----------------------------------------------------------------------------
  procedure LicenseSearch(
    a_LicenseNumber                     varchar2,
    a_LicenseType                       varchar2,
    a_State                             varchar2,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_Establishment                     varchar2,
    a_PhysicalAddress                   varchar2,
    a_Licensee                          varchar2,
    --a_Operator                          varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_Restriction                       varchar2,
    a_CriminalConviction                varchar2,
    a_ConflictOfInterest                varchar2,
    --a_RiskFactor                        varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is

    -- Scotts test
    type udt_LicenseType is record
    ( ObjectId                          udt_Id
    , Name                              varchar2(60) );
    type udt_LicenseTypeTable is table of udt_LicenseType;
    t_LicenseTypeList                   udt_LicenseTypeTable;

    t_AllObjects                        udt_ObjectList;
    t_LicenseType                       udt_LicenseTypeTable;
    t_LicenseTypeSplit                  api.pkg_Definition.udt_StringList;
    t_errorText                         varchar2(50);

  begin
    -- Raise error if no search criteria is entered
    if a_LicenseNumber is null
        and a_Licensetype is null
        and a_State is null
        and a_IssueDateFrom is null
        and a_IssueDateTo is null
        and a_EffectiveDateFrom is null
        and a_EffectiveDateTo is null
        and a_ExpirationDateFrom is null
        and a_ExpirationDateTo is null
        and a_InactivityStartDateFrom is null
        and a_InactivityStartDateTo is null
        and a_Establishment is null
        and a_PhysicalAddress is null
        and a_Licensee is null
        --and a_Operator is null
        and a_Region is null
        and a_Office is null
        and a_Restriction is null
        and a_CriminalConviction is null
        and a_ConflictOfInterest is null then
        --and a_RiskFactor is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;





    a_Objects := api.udt_ObjectList();
    t_AllObjects := api.udt_ObjectList();



    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');
    extension.pkg_cxproceduralsearch.SearchByIndex('LicenseNumber',a_LicenseNumber,a_LicenseNumber,true);

    if a_LicenseType is null then
      a_Objects := ApplyLicenseCriteria(a_LicenseNumber,
                                        a_LicenseType,
                                        a_State,
                                        a_IssueDateFrom,
                                        a_IssueDateTo,
                                        a_EffectiveDateFrom,
                                        a_EffectiveDateTo,
                                        a_ExpirationDateFrom,
                                        a_ExpirationDateTo,
                                        a_InactivityStartDateFrom,
                                        a_InactivityStartDateTo,
                                        a_Establishment,
                                        a_PhysicalAddress,
                                        a_Licensee,
                                        a_Region,
                                        a_Office,
                                        a_Restriction,
                                        a_CriminalConviction,
                                        a_ConflictOfInterest);
      -- append criteria to t_AllObjects
    else
      null;
      --GetSearchCriteria();
      -- add SearchCriteria
      -- append criteria to t_AllObjects
    end if;


    -- Scott's special test
    if a_LicenseType is not null then
      t_LicenseTypeSplit := extension.pkg_Utils.Split(a_LicenseType, ', ');
      t_LicenseTypeList  := udt_LicenseTypeTable();
    -- Get the ObjectId and Name for each License Type
      for i in 1..t_LicenseTypeSplit.count loop
        t_LicenseTypeList.extend;
        select lt.ObjectId
             , lt.Name
          into t_LicenseTypeList(i)
          from query.o_ABC_LicenseType lt
         where lt.Name = t_LicenseTypeSplit(i);
      end loop;
    end if;

    \*for lt in 1..t_LicenseTypeList.count loop
      if t_IsDashboardSearch then
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LicenseType','Name',t_LicenseTypeList(lt).name, t_LicenseTypeList(lt).name, false);
      else
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LicenseType','ObjectId',t_LicenseTypeList(lt).objectid, t_LicenseTypeList(lt).objectid, false);
      end if;
    end loop;*\

    \*if a_LicenseType is not null then
      if t_IsDashboardSearch then
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LicenseType','Name',a_LicenseType, a_LicenseType, false);
      else
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LicenseType','ObjectId',a_LicenseType, a_LicenseType, false);
      end if;
    end if;*\


    extension.pkg_cxproceduralsearch.PerformSearch(a_objects, 'and', 'none');

  end LicenseSearch;*/

  -----------------------------------------------------------------------------
  -- LicenseSearch
  --  Search for Licenses.
  -- Dashboard is not currently working and will need to be ajsted to allow for multiple License Types
  -----------------------------------------------------------------------------
  procedure LicenseSearch(
    a_LicenseNumber                     varchar2,
    a_LicenseType                       varchar2,
    a_State                             varchar2,
    a_OriginalIssueDateFrom             date,
    a_OriginalIssueDateTo               date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_Establishment                     varchar2,
    a_PhysicalAddress                   varchar2,
    a_Licensee                          varchar2,
    --a_Operator                          varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_Restriction                       varchar2,
    a_CriminalConviction                varchar2,
    a_ConflictOfInterest                varchar2,
    --a_RiskFactor                        varchar2,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N',
    a_IsAmendable                       varchar2 default null,
    a_IsRenewable                       varchar2 default null,
    a_IsReinstatable                    varchar2 default null,
    a_IsInspectable                     varchar2 default null,
    a_IsAccusable                       varchar2 default null
  ) is

    -- Scotts test
    type udt_LicenseType is record
    ( ObjectId                          udt_Id
    , Name                              varchar2(60) );
    type udt_LicenseTypeTable is table of udt_LicenseType;
    t_LicenseTypeList                   udt_LicenseTypeTable;

    t_AllObjects                        udt_ObjectList;
    t_LicenseType                       udt_LicenseTypeTable;
    t_OriginalIssueDateFrom             date;
    t_OriginalIssueDateTo               date;
    t_IssueDateFrom                     date;
    t_IssueDateTo                       date;
    t_EffectiveDateFrom                 date;
    t_EffectiveDateTo                   date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_InactivityStartDateFrom           date;
    t_InactivityStartDateTo             date;
    t_Count                             number;
    t_IsDashboardSearch                 boolean := true;
    t_LicenseTypeSplit                  extension.udt_StringList2;
    t_errorText                         varchar2(50);
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
    t_DBAResults                        udt_ObjectList;
    t_LicenseIds                        udt_idlist;
    t_LicenseIds1                       udt_idlist;
    t_FirstIssueDate                    date;
    t_Index                             number := 0;
    t_LicenseFound                      varchar2(1);

  begin
    -- Raise error if no search criteria is entered
--    raise_application_error(-20000, 'License Types' ||a_LicenseType);
    if a_LicenseNumber is null
        and a_State is null
        and a_OriginalIssueDateFrom is null
        and a_OriginalIssueDateTo is null
        and a_IssueDateFrom is null
        and a_IssueDateTo is null
        and a_EffectiveDateFrom is null
        and a_EffectiveDateTo is null
        and a_ExpirationDateFrom is null
        and a_ExpirationDateTo is null
        and a_InactivityStartDateFrom is null
        and a_InactivityStartDateTo is null
        and a_Establishment is null
        and a_PhysicalAddress is null
        and a_Licensee is null
        --and a_Operator is null
        and a_Region is null
        and a_Office is null
        and a_Restriction is null then
        --and a_RiskFactor is null then
      if a_LicenseType is null and
         a_CriminalConviction is null
         and a_ConflictOfInterest is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
      else
        raise_application_error(-20000, 'Please enter more search criteria.');
      end if;
    end if;

    /* Dates in Outrider come in as 0000h of the day
       so manipulate dates to give full search of a given day */
    -- OriginalIssueDate
    if a_OriginalIssueDateFrom is not null or a_OriginalIssueDateTo is not null then
      if a_OriginalIssueDateFrom is null then
        t_OriginalIssueDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_OriginalIssueDateFrom := to_date(to_char(a_OriginalIssueDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_OriginalIssueDateTo is null then
        t_OriginalIssueDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_OriginalIssueDateTo := to_date(to_char(a_OriginalIssueDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- IssueDate
    if a_IssueDateFrom is not null or a_IssueDateTo is not null then
      if a_IssueDateFrom is null then
        t_IssueDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_IssueDateFrom := to_date(to_char(a_IssueDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_IssueDateTo is null then
        t_IssueDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_IssueDateTo := to_date(to_char(a_IssueDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- EffectiveDate
    if a_EffectiveDateFrom is not null or a_EffectiveDateTo is not null then
      if a_EffectiveDateFrom is null then
        t_EffectiveDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_EffectiveDateFrom := to_date(to_char(a_EffectiveDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EffectiveDateTo is null then
        t_EffectiveDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_EffectiveDateTo := to_date(to_char(a_EffectiveDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- ExpirationDate
    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- Inactivity Start Date
    if a_InactivityStartDateFrom is not null or a_InactivityStartDateTo is not null then
      if a_InactivityStartDateFrom is null then
        t_InactivityStartDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_InactivityStartDateFrom := to_date(to_char(a_InactivityStartDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_InactivityStartDateTo is null then
        t_InactivityStartDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_InactivityStartDateTo := to_date(to_char(a_InactivityStartDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    --Dashboard - we need to make License Type and Region more flexible for the Dashboard drill down (criteria NOT coming in as an ObjectId)
    if a_LicenseType is not null then
       select count(1)
         into t_Count
         from query.o_abc_licensetype lt
        where to_char(lt.ObjectId) = a_LicenseType;

       if t_Count > 0 then
         t_IsDashboardSearch := false;
       end if;
     end if;

    if a_Region is not null then
       select count(1)
         into t_Count
         from query.o_abc_region r
        where to_char(r.ObjectId) = a_Region;

       if t_Count > 0 then
         t_IsDashboardSearch := false;
       end if;
     end if;

    a_Objects := api.udt_ObjectList();



    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');
    extension.pkg_cxproceduralsearch.SearchByIndex('LicenseNumber',a_LicenseNumber,a_LicenseNumber,true);

/*    if a_LicenseType is null then
      a_Objects := ApplyLicenseCriteria(a_LicenseNumber,
                                        a_LicenseType,
                                        a_State,
                                        a_IssueDateFrom,
                                        a_IssueDateTo,
                                        a_EffectiveDateFrom,
                                        a_EffectiveDateTo,
                                        a_ExpirationDateFrom,
                                        a_ExpirationDateTo,
                                        a_InactivityStartDateFrom,
                                        a_InactivityStartDateTo,
                                        a_Establishment,
                                        a_PhysicalAddress,
                                        a_Licensee,
                                        a_Region,
                                        a_Office,
                                        a_Restriction,
                                        a_CriminalConviction,
                                        a_ConflictOfInterest);
      -- append criteria to t_AllObjects
    else
      null;
      --GetSearchCriteria();
      -- add SearchCriteria
      -- append criteria to t_AllObjects
    end if;*/
/*    t_LicenseTypeSplit := extension.pkg_Utils.Split2(a_LicenseType, ', ');
    for lt in 1..t_LicenseTypeSplit.count loop
      if t_IsDashboardSearch then
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LicenseType','Name',t_LicenseTypeSplit(lt), t_LicenseTypeSplit(lt), false);
      else
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LicenseType','ObjectId',t_LicenseTypeSplit(lt).objectid, t_LicenseTypeSplit(lt).objectid, false);
      end if;
    end loop;

    if a_LicenseType is not null then
      if t_IsDashboardSearch then
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LicenseType','Name',a_LicenseType, a_LicenseType, false);
      else
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LicenseType','ObjectId',a_LicenseType, a_LicenseType, false);
      end if;
    end if;*/
    extension.pkg_cxproceduralsearch.SearchByIndex('State',a_State,a_State,false);
    extension.pkg_cxproceduralsearch.SearchByIndex('OriginalIssueDate', t_OriginalIssueDateFrom,t_OriginalIssueDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('IssueDate', t_IssueDateFrom,t_IssueDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('EffectiveDate', t_EffectiveDateFrom,t_EffectiveDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('ExpirationDate', t_ExpirationDateFrom,t_ExpirationDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('InactivityStartDate', t_InactivityStartDateFrom,t_InactivityStartDateTo);
--    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Establishment', 'DoingBusinessAs', a_Establishment, a_Establishment, true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('PhysicalAddress', 'dup_FormattedDisplay', a_PhysicalAddress, a_PhysicalAddress, true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Licensee', 'dup_FormattedName', a_Licensee, a_Licensee, true);
    --extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Operator', 'dup_FormattedName', a_Operator, a_Operator, true);

    if a_LicenseType is not null then
      if t_IsDashboardSearch then
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Region', 'Name', a_Region, a_Region, false);
      else
        extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Region', 'ObjectId', a_Region, a_Region, false);
      end if;
    else
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Region', 'ObjectId', a_Region, a_Region, false);
    end if;

    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Office', 'ObjectId', a_Office, a_Office, false);
    --extension.pkg_cxproceduralsearch.SearchByRelatedIndex('RiskFactor', 'ObjectId', a_RiskFactor, a_RiskFactor, false);

    extension.pkg_cxproceduralsearch.FilterObjects('CriminalConviction', a_CriminalConviction, a_CriminalConviction, false);
    extension.pkg_cxproceduralsearch.FilterObjects('ConflictOfInterest', a_ConflictOfInterest, a_ConflictOfInterest, false);
--    extension.pkg_CxProceduralSearch.FilterObjects('dup_HasBeenIssued','Y','Y',false);

    if a_IsAmendable = 'Y' then
      -- Filter out Licenses that can be amended
      extension.pkg_cxProceduralSearch.FilterObjects('dup_IsAmendable', a_IsAmendable, a_IsAmendable, false);
    end if;
    if a_IsRenewable = 'Y' then
      -- Filter out Licenses that can be renewed
      extension.pkg_cxProceduralSearch.FilterObjects('dup_IsRenewable', a_IsRenewable, a_IsRenewable, false);
    end if;
    if a_IsReinstatable = 'Y' then
      -- Filter out Licenses that can be reinstated
      extension.pkg_cxProceduralSearch.FilterObjects('dup_IsReinstatable', a_IsReinstatable, a_IsReinstatable, false);
    end if;
    if a_IsInspectable = 'Y' then
      -- Filter out Licenses that can be inspected
      extension.pkg_cxProceduralSearch.FilterObjects('dup_IsInspectable', a_IsInspectable, a_IsInspectable, false);
    end if;
    if a_IsAccusable = 'Y' then
      -- Filter out Licenses that are accusable
      extension.pkg_cxProceduralSearch.FilterObjects('dup_IsAccusable', a_IsAccusable, a_IsAccusable, false);
    end if;



    extension.pkg_cxproceduralsearch.PerformSearch(a_objects, 'and', 'none');
    --Search by the DBA fields on the establishment
    t_DBAResults := api.udt_ObjectList();
    if a_Establishment is not null then
      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Establishment', 'DoingBusinessAs', a_Establishment, a_Establishment, true);
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Establishment', 'AdditionalDBANames', a_Establishment, a_Establishment, true);
      extension.pkg_cxProceduralSearch.PerformSearch(t_DBAResults, 'or');
      if a_Objects.Count > 0 then
        extension.pkg_CollectionUtils.Join(a_Objects, t_DBAResults);
      else
        extension.pkg_CollectionUtils.Append(a_Objects, t_DBAResults);
      end if;
    end if;

   t_AllObjects := api.udt_ObjectList();

    if a_LicenseType is not null then
      t_LicenseTypeSplit := extension.pkg_Utils.Split2(a_LicenseType, ', ');

        select api.udt_object(ob.ObjectId)
          bulk collect into t_allobjects
          from table(cast(a_Objects as api.udt_objectlist)) ob
          join query.r_ABC_LicenseLicenseType ralt
            on ralt.LicenseObjectId =  ob.ObjectId
          /* where ralt.LicenseTypeObjectId in (list from loop)*/
          join query.o_ABC_LicenseType lt
            on lt.ObjectId = ralt.LicenseTypeObjectId
          join table(cast(t_LicenseTypeSplit as extension.udt_stringlist2)) lts
            on upper(lts.Column_value) = upper(lt.Name);
     a_objects := t_AllObjects;
    end if;


   t_AllObjects := api.udt_ObjectList();

    if a_Restriction is not null then
      case a_Restriction
        when 'Yes' then
            select api.udt_object(wl.LicenseId)
              bulk collect into t_Allobjects
              from  query.r_WarningLicense wl
              join table(cast(a_Objects as api.udt_objectlist)) ob on ob.ObjectId = wl.LicenseId
             where api.pkg_columnquery.Value(wl.LicenseWarningId, 'WarningActive') = 'Y' ;
        when 'No' then
            select api.udt_object(w.ObjectId)
                bulk collect into t_Allobjects
                from ( select ob.objectid
                        from table(cast(a_Objects as api.udt_objectlist)) ob
                       minus
                     Select wl.LicenseId
                      from  query.r_WarningLicense wl
                      join table(cast(a_Objects as api.udt_objectlist)) ob on ob.ObjectId = wl.LicenseId
                     where api.pkg_columnquery.Value(wl.LicenseWarningId, 'WarningActive') = 'Y' ) w ;
      end case;

     a_objects := t_AllObjects;
    end if;

    -- Eliminate licenses that are in Pending State
    t_AllObjects := api.udt_ObjectList();
    select api.udt_object(w.ObjectId)
      bulk collect into t_Allobjects
      from (select ob.objectid, api.pkg_columnquery.Value(ob.ObjectId, 'State') State
              from table(cast(a_Objects as api.udt_objectlist)) ob) w
     where w.state != 'Pending';
    a_objects := t_AllObjects;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end LicenseSearch;


  -----------------------------------------------------------------------------
  -- DistributorSearch
  --  Search for Distributors.
  -----------------------------------------------------------------------------
  procedure DistributorSearch(
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_IsActive                          char,
    a_IsDistributorLicense              char,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is

  begin

    -- Raise error if no search criteria is entered
    if a_Licensee is null
    and a_LicenseNumber is null then
    --and a_LicenseType is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    a_Objects := api.udt_ObjectList();
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');
    extension.pkg_cxproceduralsearch.SearchByIndex('LicenseNumber',a_LicenseNumber,a_LicenseNumber,true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Licensee', 'dup_FormattedName', a_Licensee, a_Licensee, true);
    extension.pkg_CxProceduralSearch.FilterObjects('IsActive','Y','Y',false);
    extension.pkg_CxProceduralSearch.FilterObjects('IsValidAsRegistrantDistributor','Y','Y',false);
    extension.pkg_cxproceduralsearch.PerformSearch(a_objects, 'and', 'none');

  end DistributorSearch;



  -----------------------------------------------------------------------------
  -- ProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure ProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is

    t_RegistrationDateFrom              date;
    t_RegistrationDateTo                date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_Count                             number;
    t_IsDashboardSearch                 boolean := true;
    t_ProductSearchRetValLimit          number;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);

  begin
    -- Raise error if no search criteria is entered
   if a_RegistrationNumber is null
    and a_Name is null
    and a_Type is null
    and a_RegistrationDateFrom is null
    and a_RegistrationDateTo is null
    and a_ExpirationDateFrom is null
    and a_ExpirationDateTo is null
    and a_Registrant is null
    and a_State is null
    and a_DistributorLicenseNumber is null
    and a_DistributorName is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- ProductSearchReturnValueLimit
        select o.ProductSearchReturnValueLimit
          into t_ProductSearchRetValLimit
          from query.o_SystemSettings o
         where o.ObjectDefTypeId = 1
           and rownum = 1;

    -- RegistrationDate
    if a_RegistrationDateFrom is not null or a_RegistrationDateTo is not null then
      if a_RegistrationDateFrom is null then
        t_RegistrationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_RegistrationDateFrom := to_date(to_char(a_RegistrationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_RegistrationDateTo is null then
        t_RegistrationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_RegistrationDateTo := to_date(to_char(a_RegistrationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- ExpirationDate
    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    a_Objects := api.udt_ObjectList();

    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Product');

    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber,true);
    extension.pkg_cxproceduralsearch.SearchByIndex('Name',a_Name,a_Name,true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductType','ObjectId',a_Type, a_Type, false);

    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationDate', t_RegistrationDateFrom,t_RegistrationDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('ExpirationDate', t_ExpirationDateFrom,t_ExpirationDateTo);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LERegistrant','dup_FormattedName',a_Registrant, a_Registrant, true);

    extension.pkg_cxproceduralsearch.SearchByIndex('State', a_State,a_State,false);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('DistributorLic','LicenseNumber',a_DistributorLicenseNumber, a_DistributorLicenseNumber, true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LEDistName','dup_FormattedName',a_DistributorName, a_DistributorName, true);

    extension.pkg_cxproceduralsearch.PerformSearch(a_objects, 'and', 'none');

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_objects.Count > t_ProductSearchRetValLimit and t_ExcelExtract = 'N' then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;
/*
    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if; */
  end ProductSearch;


  -----------------------------------------------------------------------------
  -- PublicProductSearch
  --  Search for Products on the Public website.
  -----------------------------------------------------------------------------
  procedure PublicProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_UserId                            number,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is

    t_Count                        number;
    t_IsDashboardSearch            boolean := true;
    t_Length                       number;
    t_ProductSearchRetValLimit     number;
    t_ProductIdList                udt_StringList;
    t_UnfilteredObjects               api.udt_ObjectList;

  begin

    -- Raise error if no search criteria is entered
   if a_RegistrationNumber is null
    and a_Name is null
    and a_Type is null
    and a_Registrant is null
    and a_State is null
    and a_DistributorLicenseNumber is null
    and a_DistributorName is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- ProductSearchReturnValueLimit
        select o.ProductSearchReturnValueLimit
          into t_ProductSearchRetValLimit
          from query.o_SystemSettings o
         where o.ObjectDefTypeId = 1
           and rownum = 1;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Product');

    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber, true);
    extension.pkg_cxproceduralsearch.SearchByIndex('Name',a_Name,a_Name,true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductType','ObjectId',a_Type, a_Type, false);
    extension.pkg_cxproceduralsearch.SearchByIndex('State', a_State,a_State,false);

    if a_Registrant is not null then
     t_Length := length(replace(a_Registrant, '%'));
      if t_Length < 3  or t_Length is null
        then raise_application_error(-20000, 'Please enter at least 3 characters of the Registrant Name.');
       else extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LERegistrant','dup_FormattedName',a_Registrant, a_Registrant, true);
      end if;
    end if;

    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('DistributorLic','LicenseNumber',a_DistributorLicenseNumber, a_DistributorLicenseNumber, true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LEDistName','dup_FormattedName',a_DistributorName, a_DistributorName, true);

    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');


    select api.udt_Object(ol.ObjectId) bulk collect into a_Objects from
   table(cast(t_UnfilteredObjects as api.udt_ObjectList)) ol
   join query.r_ABC_ProductRegistrant pr
   on pr.ProductObjectId = ol.ObjectId
   join query.r_ABC_UserLegalEntity ul
   on ul.LegalEntityObjectId = pr.RegistrantObjectId
   where ul.UserId = a_UserId;


    if a_Objects.Count > t_ProductSearchRetValLimit and
       a_ExcelExtract = 'N' then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search, or select "Save to Excel".');
    end if;

  end PublicProductSearch;

  -----------------------------------------------------------------------------
  -- FilteredProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure FilteredActiveLicenseLookup(
    a_LicenseNumber                varchar2,
    a_Objects                      out nocopy api.udt_ObjectList
  ) is
    t_UnfilteredObjects            api.udt_objectlist;

  begin

    if a_LicenseNumber is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    -- Search for Permits by Permit Number
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');
    extension.pkg_cxproceduralsearch.SearchByIndex('LicenseNumber',a_LicenseNumber,a_LicenseNumber,true);
    extension.pkg_cxproceduralsearch.FilterObjects('IsActive', 'Y', 'Y', false);
    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    a_Objects := t_UnFilteredObjects;

  end FilteredActiveLicenseLookup;

  -----------------------------------------------------------------------------
  -- FilteredProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure FilteredActivePermitLookup(
    a_PermitNumber                 varchar2,
    a_Objects                      out nocopy api.udt_ObjectList
  ) is
    t_UnfilteredObjects            api.udt_objectlist;

  begin

    if a_PermitNumber is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    -- Search for Permits by Permit Number
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Permit');
    extension.pkg_cxproceduralsearch.SearchByIndex('PermitNumber',a_PermitNumber,a_PermitNumber,true);
    extension.pkg_cxproceduralsearch.FilterObjects('IsActive', 'Y', 'Y', false);
    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    a_Objects := t_UnFilteredObjects;

  end FilteredActivePermitLookup;

  -----------------------------------------------------------------------------
  -- FilteredRenewalProductSearch
  -- Search for Products registered to a legal entity in a Renewal Job.
  -----------------------------------------------------------------------------
  procedure FilteredRenewalProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is

    t_UnfilteredObjects                 api.udt_objectlist;
    t_ProductSearchRetValLimit          number;
    t_ProductType                       varchar2(100);

  begin
     -- Raise error if no search criteria is entered
   if a_SourceObjectId is null then
      raise_application_error(-20000, 'You must have an object ID for this search.');
    end if;

  if a_RegistrationNumber is null
    and a_Name is null
    and a_Type is null then
     raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

        -- ProductSearchReturnValueLimit
        select o.ProductSearchReturnValueLimit
          into t_ProductSearchRetValLimit
          from query.o_SystemSettings o
         where o.ObjectDefTypeId = 1
           and rownum = 1;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

     --Start the search
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_ProductXref');
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductRenewal', 'ObjectId', a_SourceObjectId, a_SourceObjectId, false);

    if a_RegistrationNumber is not null then
        extension.pkg_cxproceduralsearch.FilterObjects('dup_RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber, true);
    end if;

    if a_Name is not null then
        extension.pkg_cxproceduralsearch.FilterObjects('dup_Name',a_Name,a_Name, true);
    end if;

    if a_Type is not null then
        -- Get the name of this product type
        select a.Type
          into t_ProductType
          from query.o_ABC_ProductType a
          where a.ObjectId = a_Type;

        extension.pkg_cxproceduralsearch.FilterObjects('dup_NewType',t_ProductType,t_ProductType, false);
    end if;

    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and');

    if t_UnfilteredObjects.Count >  t_ProductSearchRetValLimit then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;

    a_Objects := t_UnFilteredObjects;
  end FilteredRenewalProductSearch;


  -----------------------------------------------------------------------------
  -- FilteredProductSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
   procedure FilteredProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is

    t_RegistrationDateFrom              date;
    t_RegistrationDateTo                date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_UnfilteredObjects                 api.udt_objectlist;
    t_LegalEntityName                   varchar2(400);
    t_ProductSearchRetValLimit          number;

  begin
    -- Raise error if no search criteria is entered
   if a_SourceObjectId is null then
      raise_application_error(-20000, 'You must have an object ID for this search.');
    end if;

  t_LegalEntityName := api.pkg_columnquery.Value(a_SourceObjectId,'dup_FormattedName');

  --api.pkg_errors.RaiseError(-20000, 'Source: ' || a_Type);

   if (a_RegistrationNumber is null)
    and a_Name is null
    and a_Type is null
    and a_RegistrationDateFrom is null
    and a_RegistrationDateTo is null
    and a_ExpirationDateFrom is null
    and a_ExpirationDateTo is null
    and a_State is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;


    -- ProductSearchReturnValueLimit
        select o.ProductSearchReturnValueLimit
          into t_ProductSearchRetValLimit
          from query.o_SystemSettings o
         where o.ObjectDefTypeId = 1
           and rownum = 1;

    /* Dates in Outrider come in as 0000h of the day
       so manipulate dates to give full search of a given day */
    -- RegistrationDate
    if a_RegistrationDateFrom is not null or a_RegistrationDateTo is not null then
      if a_RegistrationDateFrom is null then
        t_RegistrationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_RegistrationDateFrom := to_date(to_char(a_RegistrationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_RegistrationDateTo is null then
        t_RegistrationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_RegistrationDateTo := to_date(to_char(a_RegistrationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- ExpirationDate
    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;


    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();


    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Product');
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LERegistrant','ObjectId',a_SourceObjectId, a_SourceObjectId, true);
    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber,true);
    extension.pkg_cxproceduralsearch.SearchByIndex('Name',a_Name,a_Name,true);

    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductType','ObjectId',a_Type, a_Type, false);
    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationDate', t_RegistrationDateFrom,t_RegistrationDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('ExpirationDate', t_ExpirationDateFrom,t_ExpirationDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('State', a_State,a_State,false);

    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    if t_UnfilteredObjects.Count > t_ProductSearchRetValLimit then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;

    a_Objects := t_UnFilteredObjects;
  end FilteredProductSearch;

-----------------------------------------------------------------------------
  -- FilteredDistributorSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
   procedure FilteredDistributorSearch(
    a_DistributorName                   varchar2,
    a_LicenseNumber                     varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is

    t_UnfilteredObjects                 api.udt_objectlist;
    t_RegistrationNumber                number;
    t_DistributorName                   varchar2(400);
    t_LicenseNumber                     varchar2(50);
    t_ProductSearchRetValLimit          number;

  begin

    -- Raise error if no search criteria is entered
   if a_SourceObjectId is null then
      raise_application_error(-20000, 'You must have an object ID for this search.');
    end if;

  t_RegistrationNumber := api.pkg_columnquery.Value(a_SourceObjectId,'RegistrationNumber');

   if (a_DistributorName is null)
    and a_LicenseNumber is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;


    -- ProductSearchReturnValueLimit
        select o.ProductSearchReturnValueLimit
          into t_ProductSearchRetValLimit
          from query.o_SystemSettings o
         where o.ObjectDefTypeId = 1
           and rownum = 1;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');

    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('DistributorProduct','RegistrationNumber',t_RegistrationNumber, t_RegistrationNumber, true);

    if a_DistributorName is not null then
       extension.pkg_cxproceduralsearch.SearchByRelatedIndex('Licensee', 'dup_Licensee', a_DistributorName, a_DistributorName, true);
    end if;

    if a_LicenseNumber is not null then
       extension.pkg_cxproceduralsearch.SearchByIndex('LicenseNumber',a_LicenseNumber,a_LicenseNumber,true);
    end if;

    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    if t_UnfilteredObjects.Count > t_ProductSearchRetValLimit then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;

    a_Objects := t_UnfilteredObjects;
  end FilteredDistributorSearch;


  -----------------------------------------------------------------------------
  -- FilteredLicenseProductSearch
  --  Search for Products on a License.
  -----------------------------------------------------------------------------
   procedure FilteredLicenseProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is

    t_RegistrationDateFrom              date;
    t_RegistrationDateTo                date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_UnfilteredObjects                 api.udt_objectlist;
    t_UnfilteredObjects2                api.udt_objectlist;
    t_LicenseeName                      varchar2(400);
    t_ProductSearchRetValLimit          number;
    t_Active                            varchar2(20);

  begin
    -- Raise error if no search criteria is entered
   if a_SourceObjectId is null then
      raise_application_error(-20000, 'You must have an object ID for this search.');
    end if;

  select api.pkg_columnquery.Value(r.LegalEntityObjectId,'dup_Licensee')
    into t_LicenseeName
    from query.r_Abc_Licenselicensee r
   where r.LicenseObjectId = a_SourceObjectId;
  t_Active := api.pkg_columnquery.Value(a_SourceObjectId,'State');

   if (a_RegistrationNumber is null)
    and a_Name is null
    and a_Type is null
    and a_RegistrationDateFrom is null
    and a_RegistrationDateTo is null
    and a_ExpirationDateFrom is null
    and a_ExpirationDateTo is null
    and a_State is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;


    -- ProductSearchReturnValueLimit
        select o.ProductSearchReturnValueLimit
          into t_ProductSearchRetValLimit
          from query.o_SystemSettings o
         where o.ObjectDefTypeId = 1
           and rownum = 1;

    /* Dates in Outrider come in as 0000h of the day
       so manipulate dates to give full search of a given day */
    -- RegistrationDate
    if a_RegistrationDateFrom is not null or a_RegistrationDateTo is not null then
      if a_RegistrationDateFrom is null then
        t_RegistrationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_RegistrationDateFrom := to_date(to_char(a_RegistrationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_RegistrationDateTo is null then
        t_RegistrationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_RegistrationDateTo := to_date(to_char(a_RegistrationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- ExpirationDate
    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;


    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();


      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Product');

    if t_Active = 'Expired' then
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('HistoricProducts','dup_Licensee',t_LicenseeName, t_LicenseeName, true);

    else
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('DistributorLic','dup_Licensee',t_LicenseeName, t_LicenseeName, true);

    end if;
      extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber,true);
      extension.pkg_cxproceduralsearch.SearchByIndex('Name',a_Name,a_Name,true);

      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductType','ObjectId',a_Type, a_Type, false);
      extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationDate', t_RegistrationDateFrom,t_RegistrationDateTo);
      extension.pkg_cxproceduralsearch.SearchByIndex('ExpirationDate', t_ExpirationDateFrom,t_ExpirationDateTo);
      extension.pkg_cxproceduralsearch.SearchByIndex('State', a_State,a_State,false);

       extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    if t_UnfilteredObjects.Count > t_ProductSearchRetValLimit then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;

    a_Objects := t_UnFilteredObjects;
  end FilteredLicenseProductSearch;

  -----------------------------------------------------------------------------
  -- FilteredLicenseProductCurrent
  --   Search for Products on a License, different that FilteredLicenseproductSearch
  --   In that it returns the product list based on the master license
  --   Programmer: Elijah Rust
  --   Date:       11/04/2015
  -----------------------------------------------------------------------------
  procedure FilteredLicenseProductCurrent(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
    i                                   integer;
    t_Index                             pls_integer := 0;

    t_RegistrationNumber                varchar2(4000);
    t_Name                              varchar2(4000);
    t_Type                              varchar2(4000);
    t_State                             varchar2(4000);
    t_RegistrationDateFrom              date;
    t_RegistrationDateTo                date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;

    t_ProductSearchRetValLimit          number;

    t_MasterLicenseId                   number;
    t_LicenseIds                        udt_IdList;
    t_LicenseIdsTable                   api.udt_ObjectList;
    t_Products                          udt_IdList;
    t_ProductIds                        udt_IdList;
    t_ProductIdsTable                   api.udt_ObjectList;
  begin
    -- Raise error if no search criteria is entered
    if a_SourceObjectId is null then
      raise_application_error(-20000, 'You must have an object ID for this search.');
    end if;

    t_MasterLicenseId := api.pkg_columnquery.Value(a_SourceObjectId,'MasterLicenseObjectId');

    if (a_RegistrationNumber is null)
      and a_Name is null
      and a_Type is null
      and a_RegistrationDateFrom is null
      and a_RegistrationDateTo is null
      and a_ExpirationDateFrom is null
      and a_ExpirationDateTo is null
      and a_State is null then
        raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- ProductSearchReturnValueLimit
    select o.ProductSearchReturnValueLimit
      into t_ProductSearchRetValLimit
      from query.o_SystemSettings o
     where o.ObjectDefTypeId = 1
       and rownum = 1;

    -- Registration Number
    if a_RegistrationNumber is not null then
      t_RegistrationNumber := a_RegistrationNumber || '%';
    else
      t_RegistrationNumber := '%';
    end if;

    -- Name
    if a_Name is not null then
      if length(a_Name) > 2 then
        t_Name := '%' || a_Name || '%';
      else
        t_Name := a_Name || '%';
      end if;
    else
      t_Name := '%';
    end if;

    -- Product Type
    if a_Type is not null then
      t_Type := api.pkg_columnquery.Value(a_Type, 'Type');
    else
      t_Type := '%';
    end if;

    -- State
    if a_State is not null then
      t_State := a_State;
    else
      t_State := '%';
    end if;

    /* Dates in Outrider come in as 0000h of the day
       so manipulate dates to give full search of a given day */
    -- RegistrationDate
    if a_RegistrationDateFrom is null then
      t_RegistrationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    else
      t_RegistrationDateFrom := to_date(to_char(a_RegistrationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    if a_RegistrationDateTo is null then
      t_RegistrationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
    else
      t_RegistrationDateTo := to_date(to_char(a_RegistrationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;

    -- ExpirationDate
    if a_ExpirationDateFrom is null then
      t_ExpirationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    else
      t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    if a_ExpirationDateTo is null then
      t_ExpirationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
    else
      t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;

    -- Get Licenses associated with the Master License
    select l.objectid
      bulk collect into t_LicenseIds
      from query.o_abc_license l
     where l.MasterLicenseObjectId = t_MasterLicenseId;

    -- Convert Id list into a table
    t_LicenseIdsTable := api.udt_ObjectList();
    if t_LicenseIds.count() > 0 then
      t_LicenseIdsTable.extend(t_LicenseIds.count());
      for i in 1..t_LicenseIds.count() loop
        t_Index := t_Index + 1;
        t_LicenseIdsTable(t_Index) := api.udt_Object(t_LicenseIds(i));
      end loop;
    end if;

    -- Get the Current Products for which the Licenses are Distributors
    select ProductId
      bulk collect into t_ProductIds
      from(
        select pd.ProductId
        from
          table(cast(t_LicenseIdsTable as api.udt_ObjectList)) l
          join query.r_abcproduct_distributor pd
            on pd.LicenseId = l.ObjectId
          );

    -- Convert Id list into a table
    t_Index := 0;
    t_ProductIdsTable := api.udt_ObjectList();
    if t_ProductIds.count() > 0 then
      t_ProductIdsTable.extend(t_ProductIds.count());
      for i in 1..t_ProductIds.count() loop
        t_Index := t_Index + 1;
        t_ProductIdsTable(t_Index) := api.udt_Object(t_ProductIds(i));
      end loop;
    end if;

    -- Filter Down Product list
    select pr.objectid
      bulk collect into t_Products
      from
        table(cast(t_ProductIdsTable as api.udt_ObjectList)) p
        join query.o_Abc_Product pr
          on pr.ObjectId = p.ObjectId
        join table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_abc_product', 'dup_type', t_Type) as api.udt_ObjectList)) ss
          on pr.ObjectId = ss.objectid
       where pr.RegistrationNumber like t_RegistrationNumber
         and (pr.RegistrationDate between t_RegistrationDateFrom and t_RegistrationDateTo)
         and (pr.ExpirationDate   between t_ExpirationDateFrom   and t_ExpirationDateTo)
         and (pr.State like t_State)
         and (lower(pr.Name) like lower(t_Name))
         and rownum <= t_ProductSearchRetValLimit + 1;

    if t_Products.Count > t_ProductSearchRetValLimit then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;

    a_Objects := extension.pkg_utils.ConvertToObjectList(t_Products);
  end FilteredLicenseProductCurrent;

  -----------------------------------------------------------------------------
  -- FilteredLicenseProductHistoric
  --   Search for Products on a License, different that FilteredLicenseproductSearch
  --   In that it returns the product list based on the master license
  --   Programmer: Elijah Rust
  --   Date:       11/04/2015
  -----------------------------------------------------------------------------
  procedure FilteredLicenseProductHistoric(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              number,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    number,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
    i                                   integer;
    t_Index                             pls_integer := 0;

    t_RegistrationNumber                varchar2(4000);
    t_Name                              varchar2(4000);
    t_Type                              varchar2(4000);
    t_State                             varchar2(4000);
    t_RegistrationDateFrom              date;
    t_RegistrationDateTo                date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;

    t_ProductSearchRetValLimit          number;

    t_MasterLicenseId                   number;
    t_LicenseIds                        udt_IdList;
    t_LicenseIdsTable                   api.udt_ObjectList;
    t_Products                          udt_IdList;
    t_ProductIds                        udt_IdList;
    t_ProductIdsTable                   api.udt_ObjectList;
  begin
    -- Raise error if no search criteria is entered
    if a_SourceObjectId is null then
      raise_application_error(-20000, 'You must have an object ID for this search.');
    end if;

    t_MasterLicenseId := api.pkg_columnquery.Value(a_SourceObjectId,'MasterLicenseObjectId');

    if (a_RegistrationNumber is null)
      and a_Name is null
      and a_Type is null
      and a_RegistrationDateFrom is null
      and a_RegistrationDateTo is null
      and a_ExpirationDateFrom is null
      and a_ExpirationDateTo is null
      and a_State is null then
        raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- ProductSearchReturnValueLimit
    select o.ProductSearchReturnValueLimit
      into t_ProductSearchRetValLimit
      from query.o_SystemSettings o
     where o.ObjectDefTypeId = 1
       and rownum = 1;

    -- Registration Number
    if a_RegistrationNumber is not null then
      t_RegistrationNumber := a_RegistrationNumber || '%';
    else
      t_RegistrationNumber := '%';
    end if;

    -- Name
    if a_Name is not null then
      if length(a_Name) > 2 then
        t_Name := '%' || a_Name || '%';
      else
        t_Name := a_Name || '%';
      end if;
    else
      t_Name := '%';
    end if;

    -- Product Type
    if a_Type is not null then
      t_Type := api.pkg_columnquery.Value(a_Type, 'Type');
    else
      t_Type := '%';
    end if;

    -- State
    if a_State is not null then
      t_State := a_State;
    else
      t_State := '%';
    end if;

    /* Dates in Outrider come in as 0000h of the day
       so manipulate dates to give full search of a given day */
    -- RegistrationDate
    if a_RegistrationDateFrom is null then
      t_RegistrationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    else
      t_RegistrationDateFrom := to_date(to_char(a_RegistrationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    if a_RegistrationDateTo is null then
      t_RegistrationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
    else
      t_RegistrationDateTo := to_date(to_char(a_RegistrationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;

    -- ExpirationDate
    if a_ExpirationDateFrom is null then
      t_ExpirationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    else
      t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    end if;
    if a_ExpirationDateTo is null then
      t_ExpirationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
    else
      t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    end if;

    -- Get Licenses associated with the Master License
    select l.objectid
      bulk collect into t_LicenseIds
      from query.o_abc_license l
     where l.MasterLicenseObjectId = t_MasterLicenseId;

    -- Convert Id list into a table
    t_LicenseIdsTable := api.udt_ObjectList();
    if t_LicenseIds.count() > 0 then
      t_LicenseIdsTable.extend(t_LicenseIds.count());
      for i in 1..t_LicenseIds.count() loop
        t_Index := t_Index + 1;
        t_LicenseIdsTable(t_Index) := api.udt_Object(t_LicenseIds(i));
      end loop;
    end if;

    -- Get the Historic Products for which the Licenses are Distributors
    select ProductId
      bulk collect into t_ProductIds
      from(
        select hd.ProductId
        from
          table(cast(t_LicenseIdsTable as api.udt_ObjectList)) l
          join query.r_ABC_Product_Historic_Dist hd
            on hd.LicenseId = l.ObjectId
            );

    -- Convert Id list into a table
    t_Index := 0;
    t_ProductIdsTable := api.udt_ObjectList();
    if t_ProductIds.count() > 0 then
      t_ProductIdsTable.extend(t_ProductIds.count());
      for i in 1..t_ProductIds.count() loop
        t_Index := t_Index + 1;
        t_ProductIdsTable(t_Index) := api.udt_Object(t_ProductIds(i));
      end loop;
    end if;

    -- Filter Down Product list
    select pr.objectid
      bulk collect into t_Products
      from
        table(cast(t_ProductIdsTable as api.udt_ObjectList)) p
        join query.o_Abc_Product pr
          on pr.ObjectId = p.ObjectId
        join table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_abc_product', 'dup_type', t_Type) as api.udt_ObjectList)) ss
          on pr.ObjectId = ss.objectid
       where pr.RegistrationNumber like t_RegistrationNumber
         and (pr.RegistrationDate between t_RegistrationDateFrom and t_RegistrationDateTo)
         and (pr.ExpirationDate   between t_ExpirationDateFrom   and t_ExpirationDateTo)
         and (pr.State like t_State)
         and (lower(pr.Name) like lower(t_Name))
         and rownum <= t_ProductSearchRetValLimit + 1;

    if t_Products.Count > t_ProductSearchRetValLimit then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;

    a_Objects := extension.pkg_utils.ConvertToObjectList(t_Products);
  end FilteredLicenseProductHistoric;

  ---------------------------------------------------------------------------------
  -- FilteredLicenseLookup_Permit
  --  Search for Licenses, filter by License Type selected on the given Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredLicenseLookup_Permit(
    a_PermitTypeObjectId              number,
    a_LicenseNumber                   varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  ) is
    t_LicenseLength                   number;
    t_UnfilteredObjects               api.udt_objectlist;

    cursor c_LicenseTypes is
      select r.ToObjectId LicenseTypeObjectId
        from api.relationships r
       where r.EndPointId = api.pkg_configquery.EndPointIdForName('o_ABC_PermitType','LicenseType')
         and r.FromObjectId = a_PermitTypeObjectId;

  begin

    if a_LicenseNumber is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    t_LicenseLength := length(a_LicenseNumber);

    if t_LicenseLength < 3 then
      raise_application_error(-20000, 'Please enter at least 3 digits of the License Number.');
    end if;


    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    -- Search for Licenses by License Number
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');
    extension.pkg_cxproceduralsearch.SearchByIndex('LicenseNumber',a_LicenseNumber,a_LicenseNumber,true);
    extension.pkg_cxproceduralsearch.FilterObjects('IsActive', 'Y', 'Y', false);

    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    for lt in c_LicenseTypes loop
      -- Ensure License returned has the selected License Type
      for lo in 1..t_UnfilteredObjects.Count loop
        if api.pkg_columnquery.Value(t_UnfilteredObjects(lo).ObjectId, 'LicenseTypeObjectId') = lt.licensetypeobjectid then
          a_Objects.Extend(1);
          -- add license to output
          a_Objects(a_Objects.count) := t_UnfilteredObjects(lo);
        end if;
      end loop;
    end loop;

  end FilteredLicenseLookup_Permit;

  ---------------------------------------------------------------------------------
  -- FilteredPermitLookup_Permit
  --  Search for Permits, filter by Permit Type selected on the given Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredPermitLookup_Permit(
    a_PermitTypeObjectId              number,
    a_PermitNumber                    varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  ) is
    t_PermitNumberLength              number;
    t_UnfilteredObjects               api.udt_objectlist;

    cursor c_PermitTypes is
      select r.ToObjectId PermitTypeObjectId
        from api.relationships r
       where r.EndPointId = api.pkg_configquery.EndPointIdForName('o_ABC_PermitType','ApplicablePermitType')
         and r.FromObjectId = a_PermitTypeObjectId;

  begin

    if a_PermitNumber is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    /*
    -- Removed for now because our permit numbers are so low...should be re-introduces once we have a
    -- permit number pattern from the client

    t_PermitNumberLength := length(a_PermitNumber);

    if t_PermitNumberLength < 3 then
      raise_application_error(-20000, 'Please enter at least 3 digits of the Permit Number.');
    end if;
    */

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    -- Search for Permits by Permit Number
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Permit');
    /** Wildcard will return 2 and 20 when supplied with 2 we should eventually change this... **/
    extension.pkg_cxproceduralsearch.SearchByIndex('PermitNumber',a_PermitNumber,a_PermitNumber,true);
    extension.pkg_cxproceduralsearch.FilterObjects('IsActive', 'Y', 'Y', false);
    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    for pt in c_PermitTypes loop
      -- Ensure Permits returned have the indicated Permit Type (related to the "Admin" permit type)
      for po in 1..t_UnfilteredObjects.Count loop
        if api.pkg_columnquery.Value(t_UnfilteredObjects(po).ObjectId, 'PermitTypeObjectId') = pt.PermitTypeObjectId then
          a_Objects.Extend(1);
          -- add license to output
          a_Objects(a_Objects.count) := t_UnfilteredObjects(po);
        end if;
      end loop;
    end loop;

  end FilteredPermitLookup_Permit;

  -----------------------------------------------------------------------------
  -- EstablishmentSearch
  --  Search for Establishments.
  -----------------------------------------------------------------------------
  procedure EstablishmentSearch(
    a_EstablishmentType                 varchar2,
    a_DoingBusinessAs                   varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_Operator                          varchar2,
    a_IncludeHistory                    char,
    a_Objects                        out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_MasterList                        api.udt_ObjectList;
    t_UnfilteredObjects                 api.udt_ObjectList;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
  begin
     if a_EstablishmentType is null
    and a_DoingBusinessAs is null
    and a_MailingAddress is null
    and a_PhysicalAddress is null
    and a_Operator is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();
    t_MasterList := api.udt_ObjectList();

    if a_EstablishmentType is not null
    or a_MailingAddress is not null
    or a_PhysicalAddress is not null
    or a_Operator is not null then
      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Establishment');
      if a_EstablishmentType is not null then
         extension.pkg_cxProceduralSearch.SearchByRelatedIndex('EstablishmentType', 'ObjectId', a_EstablishmentType, a_EstablishmentType, true);
      end if;
      if a_MailingAddress is not null then
  --    extension.pkg_cxProceduralSearch.SearchByIndex('DoingBusinessAs', a_DoingBusinessAs, a_DoingBusinessAs, true);
        extension.pkg_cxProceduralSearch.SearchByRelatedIndex('MailingAddress', 'dup_FormattedDisplay', a_MailingAddress, a_MailingAddress, true);
      end if;
      if a_PhysicalAddress is not null then
         extension.pkg_cxProceduralSearch.SearchByRelatedIndex('PhysicalAddress', 'dup_FormattedDisplay', a_PhysicalAddress, a_PhysicalAddress, true);
      end if;
      if a_Operator is not null then
         extension.pkg_cxProceduralSearch.SearchByRelatedIndex('Operator', 'dup_FormattedName', a_Operator, a_Operator, true);
      end if;

      extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');
      extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);
      t_UnfilteredObjects.delete;
    end if;

    if a_DoingBusinessAs is not null then
      --Search again to also search by the DBA information fields
      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Establishment');
      extension.pkg_cxProceduralSearch.SearchByIndex('AdditionalDBANames', a_DoingBusinessAs, a_DoingBusinessAs, true);
      extension.pkg_cxProceduralSearch.SearchByIndex('DoingBusinessAs', a_DoingBusinessAs, a_DoingBusinessAs, true);
      extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'or');
      if t_MasterList.Count > 0 then
        extension.pkg_CollectionUtils.Join(t_MasterList, t_UnfilteredObjects);
        t_UnfilteredObjects.delete;
      else
        extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);
        t_UnfilteredObjects.delete;
      end if;
    end if;

    -- Searching related Establishment History objects if the Include History checkbox is selected
    if a_IncludeHistory = 'Y' then
      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Establishment');

      if a_DoingBusinessAs <> '%' then
        extension.pkg_cxProceduralSearch.SearchByRelatedIndex('EstablishmentHistory', 'DoingBusinessAs', a_DoingBusinessAs, a_DoingBusinessAs, true);
        extension.pkg_cxProceduralSearch.SearchByRelatedIndex('EstablishmentHistory', 'AdditionalDBANames', a_DoingBusinessAs, a_DoingBusinessAs, true);
      end if;

      extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'or');
      extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);
      t_UnfilteredObjects.delete;
    end if;
    a_Objects := t_MasterList;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end EstablishmentSearch;


  -----------------------------------------------------------------------------
  -- LegalEntitySearch
  --  Search for Legal Entities.
  -----------------------------------------------------------------------------
  procedure LegalEntitySearch(
    a_LegalName                         varchar2,
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_LegalEntityTypeObjectId           udt_id,
    a_CorporationNumber                 varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_IncludeHistory                    char,
    a_IsBrandRegistrant                 char,
    a_IsPermittee                       char,
    a_SSNTIN                            varchar2,
    a_DriversLicense                    varchar2,
    a_ContactEmail                      varchar2,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_LegalEntityTypeObjectId           udt_id :=
      api.pkg_simplesearch.ObjectByIndex('o_ABC_LegalEntityType', 'Name', 'Individual');
    t_MasterList                        api.udt_ObjectList;
    t_UnfilteredObjects                 api.udt_ObjectList;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
    t_SSNEncrypted                      varchar2(4000);
    t_DriversLicenseEncrypted           varchar2(4000);

  begin
    -- Raise error if no search criteria is entered
    if  a_LegalName is null
      and a_LastName is null
      and a_FirstName is null
      and a_LegalEntityTypeObjectId is null
      and a_CorporationNumber is null
      and a_MailingAddress is null
      and a_PhysicalAddress is null
      and a_SSNTIN is null
      and a_DriversLicense is null
      and a_ContactEmail is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- Raise error if criteria is entered for an individual and a non-individual entity type
    if (a_LegalName is not null
       or a_CorporationNumber is not null
       or a_LegalEntityTypeObjectId <> t_LegalEntityTypeObjectId)
     and (a_LastName is not null
       or a_FirstName is not null
       or a_LegalEntityTypeObjectId = t_LegalEntityTypeObjectId) then
        raise_application_error(-20000, 'If you are searching for an Individual please do
          not enter criteria for Legal Name, Doing Business As or Corporation Number. Otherwise, please do not
          enter criteria for Last Name or First Name.');
    end if;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();
    t_MasterList := api.udt_ObjectList();

    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_LegalEntity');

     extension.pkg_cxProceduralSearch.SearchByIndex('LegalName', a_LegalName, a_LegalName, true);
     extension.pkg_cxProceduralSearch.SearchByIndex('LastName', a_LastName, a_LastName, true);
     extension.pkg_cxProceduralSearch.SearchByIndex('FirstName', a_FirstName, a_FirstName, true);
     extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityType', 'ObjectId', a_LegalEntityTypeObjectId, a_LegalEntityTypeObjectId, true);
     extension.pkg_cxProceduralSearch.SearchByIndex('CorporationNumber', a_CorporationNumber, a_CorporationNumber, true);
     extension.pkg_cxProceduralSearch.SearchByRelatedIndex('MailingAddress', 'dup_FormattedDisplay', a_MailingAddress, a_MailingAddress, true);
     extension.pkg_cxProceduralSearch.SearchByRelatedIndex('PhysicalAddress', 'dup_FormattedDisplay', a_PhysicalAddress, a_PhysicalAddress, true);
     extension.pkg_cxProceduralSearch.SearchByIndex('ContactEmailAddress', a_ContactEmail, a_ContactEmail, true);
     if a_SSNTIN is not null then
       if length(a_SSNTIN) = 9 and regexp_replace(a_SSNTIN, '[0-9]*') is null then
         t_SSNEncrypted := abc.pkg_abc_Encryption.Encrypt(a_SSNTIN);
         extension.pkg_cxProceduralSearch.SearchByIndex('SSNTINEncrypted', t_SSNEncrypted, t_SSNEncrypted, false);
       else
         raise_application_error(-20000, 'If searching by SSN/TIN/EIN you must enter exactly 9 digits.');
       end if;
     end if;
     if a_DriversLicense is not null then
        t_DriversLicenseEncrypted := abc.pkg_abc_Encryption.Encrypt(a_DriversLicense);
        extension.pkg_cxProceduralSearch.SearchByIndex('DriversLicenseEncrypted', t_DriversLicenseEncrypted, t_DriversLicenseEncrypted, false);
     end if;


     if a_IncludeHistory = 'N' or
        a_IncludeHistory is null then
        extension.pkg_CxProceduralSearch.FilterObjects ('Active', 'Y', null, False);
     end if;

   extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');

   extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);
   t_UnfilteredObjects.delete;

   -- Searching related Legal Entity History objects if the Include History checkbox is selected
   if a_IncludeHistory = 'Y' then
     extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_LegalEntity');

     if a_LegalName <> '%' then
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'LegalName', a_LegalName, a_LegalName, true);
     end if;
     if a_LastName <> '%' then
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'LastName', a_LastName, a_LastName, true);
     end if;
     if a_FirstName <> '%' then
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'FirstName', a_FirstName, a_FirstName, true);
     end if;
     if a_CorporationNumber <> '%' then
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'CorporationNumber', a_CorporationNumber, a_CorporationNumber, true);
     end if;
     if a_SSNTIN is not null then
       t_SSNEncrypted := abc.pkg_abc_Encryption.Encrypt(a_SSNTIN);
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'SSNTINEncrypted', t_SSNEncrypted, t_SSNEncrypted, false);
     end if;

       extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');
   end if;

   extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);
   t_UnfilteredObjects.delete;

   a_Objects := t_MasterList;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end LegalEntitySearch;


  -----------------------------------------------------------------------------
  -- JobSearch
  --  Search for Jobs.
  -----------------------------------------------------------------------------
  procedure JobSearch(
    a_JobNumber                         varchar2,
    a_JobType                           varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_LicenseNumber                     varchar2,
    a_PermitNumber                      varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_CreatedByUserFormattedName        varchar2,
    a_AutoCancelled                     varchar2 default 'N',
    a_ObjectList                        out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_JobNumber                         varchar2(41) := a_JobNumber;
    t_JobStatusId                       udt_Id;
    t_ResultInvalid                     boolean;
    t_MasterList                        api.udt_ObjectList;
    t_MasterListIND                     api.udt_ObjectList;
    t_UnfilteredObjects                 api.udt_ObjectList;
    t_UnfilteredObjectsIND              api.udt_ObjectList;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
    t_CreatedDateFrom                   date;
    t_CreatedDateTo                     date;
    t_CompletedDateFrom                 date;
    t_CompletedDateTo                   date;

  begin
    if a_JobNumber is null
     and a_LicenseNumber is null
     and a_PermitNumber is null
     and a_JobStatus is null
     and a_CreatedDateFrom is null
     and a_CreatedDateTo is null
     and a_CompletedDateFrom is null
     and a_CompletedDateTo is null
     and a_Region is null
     and a_Office is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    if t_JobNumber is not null then
      t_JobNumber := t_JobNumber || '%';
    end if;

    a_ObjectList           := api.udt_ObjectList();
    t_UnfilteredObjects    := api.udt_ObjectList();
    t_MasterList           := api.udt_ObjectList();
    t_UnfilteredObjectsIND := api.udt_ObjectList();
    t_MasterListIND        := api.udt_ObjectList();

    /* Dates in Outrider come in as 0000h of the day
       so manipulate dates to give full search of a given day */
    -- CreatedDate
    if a_CreatedDateFrom is not null or a_CreatedDateTo is not null then
      if a_CreatedDateFrom is null then
        t_CreatedDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_CreatedDateFrom := to_date(to_char(a_CreatedDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_CreatedDateTo is null then
        t_CreatedDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_CreatedDateTo := to_date(to_char(a_CreatedDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;
    -- CompletedDate
    if a_CompletedDateFrom is not null or a_CompletedDateTo is not null then
      if a_CompletedDateFrom is null then
        t_CompletedDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_CompletedDateFrom := to_date(to_char(a_CompletedDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_CompletedDateTo is null then
        t_CompletedDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_CompletedDateTo := to_date(to_char(a_CompletedDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- search by Job Type
    for cv_jobtype in (
        select  jt.name, jt.jobtypeid
          from  api.jobtypes jt
          where jt.name in ( -- Job types
                           'j_ABC_NewApplication',
                           'j_ABC_BatchRenewalNotification',
                           'j_ABC_AmendmentApplication',
                           'j_ABC_Expiration',
                           'j_ABC_RenewalApplication',
                           'j_ABC_Reinstatement',
                           'j_ABC_Inspection',
                           'j_ABC_Complaint',
                           'j_ABC_Accusation',
                           'j_ABC_RequestForCertification',
                           'j_ABC_PRApplication',
                           'j_ABC_PRNonRenewal',
                           'j_ABC_PRRenewal',
                           'j_ABC_PRAmendment',
                           'j_ABC_Petition',
                           'j_ABC_PetitionAmendment',
                           'j_ABC_MiscellaneousRevenue',
                           'j_ABC_DailyDeposit',
                           'j_ABC_Appeal',
                           'j_ABC_PermitApplication',
                           'j_ABC_PermitRenewal',
                           'j_ABC_CPLSubmissions',
                           'j_ABC_BatchRenewalNotifRequest',
                           'j_ABC_1218WarningBatch',
                           'j_ABC_1239WarningBatch'
                           )
--                           )

           -- replace variable with appropriate job description
            and jt.Description = NVL(a_JobType, jt.Description))
            loop

      t_ResultInvalid := false;
      extension.pkg_cxProceduralSearch.InitializeSearch(cv_jobtype.name);

    -- search by job statuses
      if a_JobStatus is not null then
        begin
          select s.StatusId into t_JobStatusId from api.statuses s
           join api.jobstatuses js on js.StatusId=s.StatusId
           where s.description = a_JobStatus
           and js.JobTypeId=cv_jobtype.jobtypeid;
        exception when no_data_found then
          t_ResultInvalid := true;
        end;
      end if;

      -- Only search if the selected status is valid for the current (or selected) job type
      if not t_ResultInvalid then

        extension.pkg_cxproceduralsearch.SearchBySystemColumn('CreatedDate', t_CreatedDateFrom, t_CreatedDateTo);

        if a_AutoCancelled = 'Y' then
          extension.pkg_cxproceduralsearch.SearchByIndex('Autocancelled', a_AutoCancelled, a_AutoCancelled);
        end if;
        if t_JobNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByIndex('ExternalFileNum', t_JobNumber, t_JobNumber);
        end if;
        if t_JobStatusId is not null then
          extension.pkg_cxproceduralsearch.SearchBySystemColumn('StatusId', t_JobStatusId, t_JobStatusId, false);
        end if;
        if t_CompletedDateFrom is not null or t_CompletedDateTo is not null then
          extension.pkg_cxproceduralsearch.SearchBySystemColumn('CompletedDate', t_CompletedDateFrom, t_CompletedDateTo);
        end if;

        if a_LicenseNumber is not null then
          if api.pkg_configquery.EndPointIdForName(cv_jobtype.name, 'License') is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('License', 'LicenseNumber', a_LicenseNumber, a_LicenseNumber, true);
          else
            t_ResultInvalid := true;
          end if;
        end if;

        if a_PermitNumber is not null then
          if api.pkg_configquery.EndPointIdForName(cv_jobtype.name, 'Permit') is not null then
             if cv_jobtype.name = 'j_ABC_PermitApplication' then
                extension.pkg_cxProceduralSearch.SearchByRelatedIndex ('AppToPermit', 'PermitNumber', a_PermitNumber, a_PermitNumber, true);
             elsif cv_jobtype.name = 'j_ABC_PermitRenewal' then
                extension.pkg_cxProceduralSearch.SearchByRelatedIndex ('RenewToPermit', 'PermitNumber', a_PermitNumber, a_PermitNumber, true);
             else
                extension.pkg_cxProceduralSearch.SearchByRelatedIndex ('Permit', 'PermitNumber', a_PermitNumber, a_PermitNumber, true);
             end if;
          else
            t_ResultInvalid := true;
          end if;
        end if;

        if a_Region is not null then
          if (api.pkg_configquery.EndPointIdForName(cv_jobtype.name, 'Region')) is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('Region', 'ObjectId', a_Region, a_Region, true);
          else
            t_ResultInvalid := true;
          end if;
        end if;

        if a_Office is not null then
          if (api.pkg_configquery.EndPointIdForName(cv_jobtype.name, 'Office')) is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('Office', 'ObjectId', a_Office, a_Office, true);
          else
            t_ResultInvalid := true;
          end if;
        end if;

        if a_CreatedByUserFormattedName is not null then
          extension.pkg_cxProceduralSearch.FilterObjects('CreatedByUserFormattedName', a_CreatedByUserFormattedName, a_CreatedByUserFormattedName, true);
        end if;

        if not t_ResultInvalid then
          extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');
        end if;

        -- Append objects from the UnfilteredObjects variable (the ojects collected in the loop)
        -- to the Master List and delete the objects in Unfiltered Objects so it is ready for another loop.

        extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);
        t_UnfilteredObjects.delete;

      end if;
    end loop;

    /*Only perform this search if the region is not null.*/
    if a_Region is not null then
      -- search by only j_ABC_Inspection Job Type, this is to search by the regions coming from the license
      for cv_jobtype in (
          select  jt.name, jt.jobtypeid
            from  api.jobtypes jt
            where jt.name = 'j_ABC_Inspection'

             -- replace variable with appropriate job description
              and jt.Description = NVL(a_JobType, jt.Description))
              loop

        t_ResultInvalid := false;
        extension.pkg_cxProceduralSearch.InitializeSearch(cv_jobtype.name);

        -- search by job statuses
        if a_JobStatus is not null then
          begin
            select s.StatusId into t_JobStatusId from api.statuses s
             join api.jobstatuses js on js.StatusId=s.StatusId
             where s.description = a_JobStatus
             and js.JobTypeId=cv_jobtype.jobtypeid;
          exception when no_data_found then
            t_ResultInvalid := true;
          end;
        end if;

        -- Only search if the selected status is valid for the current (or selected) job type
        if not t_ResultInvalid then

          extension.pkg_cxproceduralsearch.SearchBySystemColumn('CreatedDate', t_CreatedDateFrom, t_CreatedDateTo);
          if t_JobNumber is not null then
            extension.pkg_cxproceduralsearch.SearchByIndex('ExternalFileNum', t_JobNumber, t_JobNumber);
          end if;
          if t_JobStatusId is not null then
            extension.pkg_cxproceduralsearch.SearchBySystemColumn('StatusId', t_JobStatusId, t_JobStatusId, false);
          end if;
          if t_CompletedDateFrom is not null or t_CompletedDateTo is not null then
            extension.pkg_cxproceduralsearch.SearchBySystemColumn('CompletedDate', t_CompletedDateFrom, t_CompletedDateTo);
          end if;

          if a_LicenseNumber is not null then
            if api.pkg_configquery.EndPointIdForName(cv_jobtype.name, 'License') is not null then
              extension.pkg_cxProceduralSearch.SearchByRelatedIndex('License', 'LicenseNumber', a_LicenseNumber, a_LicenseNumber, true);
            else
              t_ResultInvalid := true;
            end if;
          end if;

          if a_PermitNumber is not null then
            if api.pkg_configquery.EndPointIdForName(cv_jobtype.name, 'Permit') is not null then
               if cv_jobtype.name = 'j_ABC_PermitApplication' then
                  extension.pkg_cxProceduralSearch.SearchByRelatedIndex ('AppToPermit', 'PermitNumber', a_PermitNumber, a_PermitNumber, true);
               elsif cv_jobtype.name = 'j_ABC_PermitRenewal' then
                  extension.pkg_cxProceduralSearch.SearchByRelatedIndex ('RenewToPermit', 'PermitNumber', a_PermitNumber, a_PermitNumber, true);
               else
                  extension.pkg_cxProceduralSearch.SearchByRelatedIndex ('Permit', 'PermitNumber', a_PermitNumber, a_PermitNumber, true);
               end if;
            else
              t_ResultInvalid := true;
            end if;
          end if;

          if a_Region is not null then
            if (api.pkg_configquery.EndPointIdForName(cv_jobtype.name, 'RegionIND')) is not null then
              extension.pkg_cxProceduralSearch.SearchByRelatedIndex('RegionIND', 'ObjectId', a_Region, a_Region, true);
            else
              t_ResultInvalid := true;
            end if;
          end if;

          if a_Office is not null then
            if (api.pkg_configquery.EndPointIdForName(cv_jobtype.name, 'Office')) is not null then
              extension.pkg_cxProceduralSearch.SearchByRelatedIndex('Office', 'ObjectId', a_Office, a_Office, true);
            else
              t_ResultInvalid := true;
            end if;
          end if;

          if a_CreatedByUserFormattedName is not null then
            extension.pkg_cxProceduralSearch.FilterObjects('CreatedByUserFormattedName', a_CreatedByUserFormattedName, a_CreatedByUserFormattedName, true);
          end if;

          if not t_ResultInvalid then
            extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjectsIND, 'and');
          end if;

          -- Append objects from the UnfilteredObjects variable (the ojects collected in the loop)
          -- to the Master List and delete the objects in Unfiltered Objects so it is ready for another loop.

          extension.pkg_CollectionUtils.Append(t_MasterListIND, t_UnfilteredObjectsIND);
          t_UnfilteredObjectsIND.delete;

        end if;
      end loop;
    end if;

    /*Since the second list will bring only the records that are not in the first list we can use append.*/
    extension.pkg_CollectionUtils.Append(t_MasterList,t_MasterListIND);

   a_ObjectList := t_MasterList;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_ObjectList.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end JobSearch;


  -----------------------------------------------------------------------------
  -- AccusationSearch
  --  Search for Accusation jobs.
  -- Programmer: StanH
  -- Date:       20Sep2012
  -- Notes:      Based on JobSearch.
  -----------------------------------------------------------------------------
  procedure AccusationSearch(
    a_JobNumber                         varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_LicenseNumber                     varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_CreatedByUserFormattedName        varchar2,
    a_Violations                        varchar2,
    a_LicenseType                       varchar2,
    a_Decision                          varchar2,
    a_ObjectList                        out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_JobNumber                         varchar2(41) := a_JobNumber;
    t_JobStatusId                       udt_Id;
    t_JobTypeName                       varchar2(30) := 'j_ABC_Accusation';
    t_JobTypeId                         udt_Id       := api.pkg_configquery.ObjectDefIdForName(t_JobTypeName);
    t_ResultInvalid                     boolean;
    t_MasterList                        api.udt_ObjectList;
    t_UnfilteredObjects                 api.udt_ObjectList;
    t_ViolationTypeEPs                  udt_StringList;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);

  begin
    if a_JobNumber is null
     and a_LicenseNumber is null
     and a_JobStatus is null
     and a_CreatedDateFrom is null
     and a_CreatedDateTo is null
     and a_CompletedDateFrom is null
     and a_CompletedDateTo is null
     and a_Region is null
     and a_Violations is null
     and a_LicenseType is null
     and a_Decision is null
     and a_Office is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    if t_JobNumber is not null then
      t_JobNumber := t_JobNumber || '%';
    end if;

    if a_Violations is null then
      t_ViolationTypeEPs(1) := 'none';
    else
      t_ViolationTypeEPs := extension.pkg_utils.Split('ViolationType,ViolationTypeCr,ViolationTypeCrBy',',');
    end if;

    a_ObjectList           := api.udt_ObjectList();
    t_UnfilteredObjects    := api.udt_ObjectList();
    t_MasterList           := api.udt_ObjectList();

    for i in 1..t_ViolationTypeEPs.count loop
      -- initialize search for Accusation jobs
      t_ResultInvalid := false;
        extension.pkg_cxProceduralSearch.InitializeSearch(t_JobTypeName);

    -- search by job statuses
      if a_JobStatus is not null then
        begin
          select s.StatusId into t_JobStatusId from api.statuses s
           join api.jobstatuses js on js.StatusId=s.StatusId
           where s.description = a_JobStatus
            and js.JobTypeId = t_JobTypeId;
        exception when no_data_found then
          t_ResultInvalid := true;
        end;
      end if;

      -- Only search if the selected status is valid for the current (or selected) job type
      if not t_ResultInvalid then

        extension.pkg_cxproceduralsearch.SearchBySystemColumn('CreatedDate', a_CreatedDateFrom, a_CreatedDateTo);
        if t_JobNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByIndex('ExternalFileNum', t_JobNumber, t_JobNumber);
        end if;

        if t_JobStatusId is not null then
          extension.pkg_cxproceduralsearch.SearchBySystemColumn('StatusId', t_JobStatusId, t_JobStatusId, false);
        end if;

        if a_CompletedDateFrom is not null or a_CompletedDateTo is not null then
          extension.pkg_cxproceduralsearch.SearchBySystemColumn('CompletedDate', a_CompletedDateFrom, a_CompletedDateTo);
        end if;

        if a_LicenseNumber is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('License', 'LicenseNumber', a_LicenseNumber, a_LicenseNumber, true);
        end if;

        if a_Region is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('Region', 'ObjectId', a_Region, a_Region, true);
        end if;

        if a_Office is not null then
          extension.pkg_cxProceduralSearch.SearchByRelatedIndex('Office', 'ObjectId', a_Office, a_Office, true);
        end if;

        if a_CreatedByUserFormattedName is not null then
          extension.pkg_cxProceduralSearch.FilterObjects('CreatedByUserFormattedName', a_CreatedByUserFormattedName, a_CreatedByUserFormattedName, true);
        end if;

        if a_Violations is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex(t_ViolationTypeEPs(i), 'ObjectId', a_Violations, a_Violations, true);
        end if;

        if a_LicenseType is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LicenseType', 'ObjectId', a_LicenseType, a_LicenseType, true);
        end if;

        if a_Decision is not null then
            extension.pkg_cxProceduralSearch.SearchByIndex('AccusationDecision', a_Decision, a_Decision, true);
        end if;

        if not t_ResultInvalid then
          extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');
        end if;

        -- Append objects from the UnfilteredObjects variable (the objects collected in the loop)
        -- to the Master List and delete the objects in Unfiltered Objects so it is ready for another loop.

        extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);
        t_UnfilteredObjects.delete;

        end if;
      end loop;
   a_ObjectList := t_MasterList;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_ObjectList.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end AccusationSearch;


  -----------------------------------------------------------------------------
  -- InspectionSearch
  --  Search for Inspection jobs.
  -- Programmer: StanH
  -- Date:       20Sep2012
  -----------------------------------------------------------------------------
  procedure InspectionSearch(
    a_JobNumber                         varchar2,
    a_LicenseNumber                     varchar2,
    a_JobStatus                         varchar2,
    a_InspectionType                    varchar2,
    a_InspectionDateFrom                date,
    a_InspectionDateTo                  date,
    a_InspectedBy                       varchar2,
    a_InspectionResult                  varchar2,
    a_ViolationType                     varchar2,
    a_LicenseType                       varchar2,
    a_Region                            varchar2,
    a_ObjectList                        out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_InspectionDateFrom                date;
    t_InspectionDateTo                  date;
    t_JobNumber                         varchar2(41) := a_JobNumber;
    t_JobStatusId                       udt_Id;
    t_JobTypeName                       varchar2(30) := 'j_ABC_Inspection';
    t_JobTypeId                         udt_Id       := api.pkg_configquery.ObjectDefIdForName(t_JobTypeName);
    t_ResultInvalid                     boolean;
    t_MasterList                        api.udt_ObjectList;
    t_MasterListIND                     api.udt_ObjectList;
    t_UnfilteredObjects                 api.udt_ObjectList;
    t_UnfilteredObjectsIND              api.udt_ObjectList;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);

  begin
    if a_JobNumber is null
     and a_LicenseNumber is null
     and a_JobStatus is null
     and a_InspectionType is null
     and a_InspectionDateFrom is null
     and a_InspectionDateTo is null
     and a_InspectedBy is null
     and a_InspectionResult is null
     and a_ViolationType is null
     and a_LicenseType is null
     and a_Region is null then
       raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    /* Dates in Outrider come in as 0000h of the day
       so manipulate dates to give full search of a given day */
    if a_InspectionDateFrom is not null or a_InspectionDateTo is not null then
      if a_InspectionDateFrom is null then
        t_InspectionDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_InspectionDateFrom := to_date(to_char(a_InspectionDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_InspectionDateTo is null then
        t_InspectionDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_InspectionDateTo := to_date(to_char(a_InspectionDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

   if t_JobNumber is not null then
      t_JobNumber := t_JobNumber || '%';
    end if;

    a_ObjectList           := api.udt_ObjectList();
    t_UnfilteredObjects    := api.udt_ObjectList();
    t_MasterList           := api.udt_ObjectList();
    t_UnfilteredObjectsIND := api.udt_ObjectList();
    t_MasterListIND        := api.udt_ObjectList();

    -- initialize search for Inspection jobs
      t_ResultInvalid := false;
      extension.pkg_cxProceduralSearch.InitializeSearch(t_JobTypeName);

    -- search by job statuses
      if a_JobStatus is not null then
        begin
          select s.StatusId into t_JobStatusId from api.statuses s
           join api.jobstatuses js on js.StatusId=s.StatusId
           where s.description = a_JobStatus
           and js.JobTypeId = t_JobTypeId;
        exception when no_data_found then
          t_ResultInvalid := true;
        end;
      end if;

      -- search Only if the selected status is valid for the current (or selected) job type
      if not t_ResultInvalid then

        if t_JobNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByIndex('ExternalFileNum', t_JobNumber, t_JobNumber);
        end if;

        if a_LicenseNumber is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('License', 'LicenseNumber', a_LicenseNumber, a_LicenseNumber, true);
        end if;

        if t_JobStatusId is not null then
          extension.pkg_cxproceduralsearch.SearchBySystemColumn('StatusId', t_JobStatusId, t_JobStatusId, false);
        end if;

        if a_InspectionType is not null then
          extension.pkg_cxProceduralSearch.SearchByRelatedIndex('InspectionType', 'ObjectId', a_InspectionType, a_InspectionType, true);
        end if;

        if t_InspectionDateFrom is not null or t_InspectionDateTo is not null then
          extension.pkg_cxproceduralsearch.SearchByIndex('InspectionDate', t_InspectionDateFrom, t_InspectionDateTo);
        end if;

        if a_InspectedBy is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('DefaultInspector', 'dup_UserNameFormat', a_InspectedBy, a_InspectedBy, true);
        end if;

        if a_InspectionResult is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('InspectionResultType', 'ObjectId', a_InspectionResult, a_InspectionResult, true);
        end if;

        if a_ViolationType is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('ViolationType', 'ObjectId', a_ViolationType, a_ViolationType, true);
        end if;

        if a_LicenseType is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LicenseType', 'ObjectId', a_LicenseType, a_LicenseType, true);
        end if;

        if a_Region is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('Region', 'ObjectId', a_Region, a_Region, true);
        end if;

        if not t_ResultInvalid then
          extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');
        end if;

        -- Append objects from the UnfilteredObjects variable
        -- to the Master List and delete the objects in Unfiltered Objects so it is ready for another search.

        extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);

      end if;

    -- perform this search Only if the region is not null.
    if a_Region is not null then

    -- initialize search for Inspection jobs
      t_ResultInvalid := false;
      extension.pkg_cxProceduralSearch.InitializeSearch(t_JobTypeName);

    -- search by job statuses
      if a_JobStatus is not null then
        begin
          select s.StatusId into t_JobStatusId from api.statuses s
           join api.jobstatuses js on js.StatusId=s.StatusId
           where s.description = a_JobStatus
           and js.JobTypeId = t_JobTypeId;
        exception when no_data_found then
          t_ResultInvalid := true;
        end;
      end if;

        -- search Only if the selected status is valid for the current (or selected) job type
      if not t_ResultInvalid then

        if t_JobNumber is not null then
          extension.pkg_cxproceduralsearch.SearchByIndex('ExternalFileNum', t_JobNumber, t_JobNumber);
        end if;

        if a_LicenseNumber is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('License', 'LicenseNumber', a_LicenseNumber, a_LicenseNumber, true);
        end if;

        if t_JobStatusId is not null then
          extension.pkg_cxproceduralsearch.SearchBySystemColumn('StatusId', t_JobStatusId, t_JobStatusId, false);
        end if;

        if a_InspectionType is not null then
          extension.pkg_cxProceduralSearch.SearchByRelatedIndex('InspectionType', 'ObjectId', a_InspectionType, a_InspectionType, true);
        end if;

        if t_InspectionDateFrom is not null or t_InspectionDateTo is not null then
          extension.pkg_cxproceduralsearch.SearchByIndex('InspectionDate', t_InspectionDateFrom, t_InspectionDateTo);
        end if;

        if a_InspectedBy is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('DefaultInspector', 'dup_UserNameFormat', a_InspectedBy, a_InspectedBy, true);
        end if;

        if a_InspectionResult is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('InspectionResultType', 'ObjectId', a_InspectionResult, a_InspectionResult, true);
        end if;

        if a_ViolationType is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('ViolationType', 'ObjectId', a_ViolationType, a_ViolationType, true);
        end if;

        if a_LicenseType is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LicenseType', 'ObjectId', a_LicenseType, a_LicenseType, true);
        end if;

        if a_Region is not null then
            extension.pkg_cxProceduralSearch.SearchByRelatedIndex('RegionIND', 'ObjectId', a_Region, a_Region, true);
        end if;

        if not t_ResultInvalid then
          extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjectsIND, 'and');
        end if;

        -- Append objects from the UnfilteredObjects variable
        -- to the Master List and delete the objects in Unfiltered Objects
        -- (although no looping is needed in this implementation)
        extension.pkg_CollectionUtils.Append(t_MasterListIND, t_UnfilteredObjectsIND);
        t_UnfilteredObjectsIND.delete;

      end if;
    end if;

    /*Since the second list will bring only the records that are not in the first list we can use append.*/
    extension.pkg_CollectionUtils.Append(t_MasterList,t_MasterListIND);

   a_ObjectList := t_MasterList;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_ObjectList.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end InspectionSearch;

  -----------------------------------------------------------------------------
  -- CPLInternalLicenseSelect
  --  Look up Licenses based on their type state and whether they require a CPL Submission,
  --  this procedure is used for the internal CPL Submission job
  -----------------------------------------------------------------------------
  procedure CPLInternalLicenseSelect(
    a_LicenseNumber                    varchar2,
    a_Licensee                         varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
  ) is
      t_LicenseLength                  number;
      t_Licensees                      udt_ObjectList;
      t_SearchResults                  api.pkg_Definition.udt_SearchResults;
      t_FinalList                      udt_IdList;
    begin
      a_ObjectList := api.udt_ObjectList();

      if a_LicenseNumber is null and
        a_Licensee is null then
        api.pkg_errors.raiseerror(-20000, 'You must enter some search criteria.');
      end if;

      if a_LicenseNumber = '%' then
        api.pkg_errors.raiseerror(-20000, 'You must enter at least one character of your License Number.');
      elsif a_Licensee = '%' then
        api.pkg_errors.raiseerror(-20000, 'You must enter at least one character of your name.');
      end if;

      if a_LicenseNumber is not null then
        if not api.pkg_Search.IntersectByColumnContents('o_ABC_License', 'LicenseNumber', a_LicenseNumber, t_SearchResults) then
          return;
        end if;
      end if;

      if a_Licensee is not null then
        if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_License', 'Licensee', 'dup_FormattedName', nvl(a_Licensee, '%'), t_SearchResults) then
          return;
        end if;
      end if;

      if not api.pkg_Search.IntersectByColumnContents('o_ABC_License', 'State', 'Active', t_SearchResults) then
        return;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'LicenseType', 'Active', 'Y', t_SearchResults) then
        return;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'LicenseType', 'CPLSubmission', 'Y', t_SearchResults) then
        return;
      end if;

      t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);

      for a in 1..t_FinalList.Count loop
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_FinalList(a));
      end loop;

  end CPLInternalLicenseSelect;

  -----------------------------------------------------------------------------
  -- CPLInternalPermitSelect
  --  Look up Permits based on their type state and whether they require a CPL Submission,
  --  this procedure is used for the internal CPL Submission job
  -- Programmer : Elijah R.
  -- Date       : 2015 Dec 30
  -----------------------------------------------------------------------------
  procedure CPLInternalPermitSelect(
    a_PermitNumber                     varchar2,
    a_Permittee                        varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
  ) is
      t_SearchResults                  api.pkg_Definition.udt_SearchResults;
      t_FinalList                      udt_IdList;
    begin
      a_ObjectList := api.udt_ObjectList();

      if a_PermitNumber is null and
        a_Permittee is null then
        api.pkg_errors.raiseerror(-20000, 'You must enter some search criteria.');
      end if;

      if a_PermitNumber = '%' then
        api.pkg_errors.raiseerror(-20000, 'You must enter at least one character of the Permit Number.');
      elsif a_Permittee = '%' then
        api.pkg_errors.raiseerror(-20000, 'You must enter at least one character of the Permitee.');
      end if;

      if a_PermitNumber is not null then
        if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'PermitNumber', a_PermitNumber, t_SearchResults) then
          return;
        end if;
      end if;

      if a_Permittee is not null then
        if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_Permit', 'Permittee', 'dup_FormattedName', nvl(a_Permittee, '%'), t_SearchResults) then
          return;
        end if;
      end if;

      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'State', 'Active', t_SearchResults) then
        return;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_Permit', 'PermitType', 'Active', 'Y', t_SearchResults) then
        return;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_Permit', 'PermitType', 'Code', 'TAP', t_SearchResults) then
        return;
      end if;

      t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);

      for a in 1..t_FinalList.Count loop
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_FinalList(a));
      end loop;

  end CPLInternalPermitSelect;

  -----------------------------------------------------------------------------
  -- CPLPublicLicenseSelect
  --  Look up Licenses based on their type state and whether they require a CPL Submission,
  --  this procedure is used for the public CPL Submission job
  -----------------------------------------------------------------------------
  procedure CPLPublicLicenseSelect(
    a_LicenseNumber                    varchar2,
    a_Licensee                         varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
    ) is
      t_LicenseLength                  number;
      t_Licensees                      udt_ObjectList;
      t_SearchResults                  api.pkg_Definition.udt_SearchResults;
      t_FinalList                      udt_IdList;
    begin
      a_ObjectList := api.udt_ObjectList();

      if a_LicenseNumber is null and
        a_Licensee is null then
        api.pkg_errors.raiseerror(-20000, 'You must enter some search criteria.');
      end if;

      if a_LicenseNumber = '%' then
        api.pkg_errors.raiseerror(-20000, 'You must enter at least one character of your License Number.');
      elsif a_Licensee = '%' then
        api.pkg_errors.raiseerror(-20000, 'You must enter at least one character of your name.');
      end if;

      if a_LicenseNumber is not null then
        if not api.pkg_Search.IntersectByColumnContents('o_ABC_License', 'LicenseNumber', a_LicenseNumber, t_SearchResults) then
          return;
        end if;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'UserLicensee', 'ObjectId', api.pkg_SecurityQuery.EffectiveUserId, t_SearchResults) then
        return;
      end if;

      if a_Licensee is not null then
        if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_License', 'Licensee', 'dup_FormattedName', nvl(a_Licensee, '%'), t_SearchResults) then
          return;
        end if;
      end if;

      if not api.pkg_Search.IntersectByColumnContents('o_ABC_License', 'State', 'Active', t_SearchResults) then
        return;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'LicenseType', 'Active', 'Y', t_SearchResults) then
        return;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'LicenseType', 'CPLSubmission', 'Y', t_SearchResults) then
        return;
      end if;

      t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);

      for a in 1..t_FinalList.Count loop
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_FinalList(a));
      end loop;

  end CPLPublicLicenseSelect;

  -----------------------------------------------------------------------------
  -- CPLPublicPermitSelect
  --  Look up Permits that are avaiable to the user to be used in a CPL job
  -- Programmer: Elijah R.
  -- Date:       2015 Dec 30
  -----------------------------------------------------------------------------
  procedure CPLPublicPermitSelect(
    a_PermitNumber                     varchar2,
    a_Permittee                        varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
    ) is
      t_SearchResults                  api.pkg_Definition.udt_SearchResults;
      t_FinalList                      udt_IdList;
    begin
      a_ObjectList := api.udt_ObjectList();

      if a_PermitNumber is null and
        a_Permittee is null then
        api.pkg_errors.raiseerror(-20000, 'You must enter some search criteria.');
      end if;

      if a_PermitNumber = '%' then
        api.pkg_errors.raiseerror(-20000, 'You must enter at least one character of the Permit Number.');
      elsif a_Permittee = '%' then
        api.pkg_errors.raiseerror(-20000, 'You must enter at least one character of the Permittee.');
      end if;

      if a_PermitNumber is not null then
        if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'PermitNumber', a_PermitNumber, t_SearchResults) then
          return;
        end if;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_Permit', 'UserPermittee', 'ObjectId', api.pkg_SecurityQuery.EffectiveUserId, t_SearchResults) then
        return;
      end if;

      if a_Permittee is not null then
        if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_Permit', 'Permittee', 'dup_FormattedName', nvl(a_Permittee, '%'), t_SearchResults) then
          return;
        end if;
      end if;

      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'State', 'Active', t_SearchResults) then
        return;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_Permit', 'PermitType', 'Active', 'Y', t_SearchResults) then
        return;
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_Permit', 'PermitType', 'Code', 'TAP', t_SearchResults) then
        return;
      end if;

      t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);

      for a in 1..t_FinalList.Count loop
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_FinalList(a));
      end loop;

  end CPLPublicPermitSelect;

  -----------------------------------------------------------------------------
  -- License Lookup
  --  Look up Licenses based on License Number, filtered by State
  -----------------------------------------------------------------------------
  procedure LicenseLookup(
    a_LicenseNumber                    varchar2,
    a_IsAmendable                      char,
    a_IsRenewable                      char,
    a_IsReinstatable                   char,
    a_IsInspectable                    char,
    a_IsAccusable                      char,
    a_ObjectList                       out nocopy udt_ObjectList
    ) is
      t_LicenseLength                  number;
    begin
      a_ObjectList := api.udt_ObjectList();

      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');

      t_LicenseLength := length(a_LicenseNumber);

      if t_LicenseLength < 3 then
         raise_application_error(-20000, 'Please enter at least 3 digits of the License Number');
      end if;
      extension.pkg_cxProceduralSearch.SearchByIndex('LicenseNumber', a_LicenseNumber, a_LicenseNumber, true);

      if a_IsAmendable = 'Y' then
        -- search for licenses with state Active and Suspended
        extension.pkg_cxProceduralSearch.SearchByIndex('dup_IsAmendable', a_IsAmendable, a_IsAmendable, false);
      end if;

      if a_IsRenewable = 'Y' then
        -- search for licenses with state Active, Expired, and Suspended
        extension.pkg_cxProceduralSearch.SearchByIndex('dup_IsRenewable', a_IsRenewable, a_IsRenewable, false);
      end if;

      if a_IsReinstatable = 'Y' then
        -- search for licenses with state Active, Expired, and Suspended
        extension.pkg_cxProceduralSearch.SearchByIndex('dup_IsReinstatable', a_IsReinstatable, a_IsReinstatable, false);
      end if;

      if a_IsInspectable = 'Y' then
        -- search for licenses with state Active
        extension.pkg_cxProceduralSearch.SearchByIndex('dup_IsInspectable', a_IsInspectable, a_IsInspectable, false);
      end if;

      if a_IsAccusable = 'Y' then
        -- search for licenses with state Active
        extension.pkg_cxProceduralSearch.SearchByIndex('dup_IsAccusable', a_IsAccusable, a_IsAccusable, false);
      end if;

      extension.pkg_cxProceduralSearch.PerformSearch(a_ObjectList, 'and');

  end LicenseLookup;

  -----------------------------------------------------------------------------
  -- LetterTemplateSearch
  --  Return Letter Templates.
  -----------------------------------------------------------------------------
  procedure LetterTemplateSearch (
    a_WordTemplateName                  varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is
    t_DocumentIdList                    udt_IdList;

  begin
    a_ObjectList := api.udt_ObjectList();

    select r.WordTemplateDocumentId
      bulk collect into t_DocumentIdList
      from query.r_SystemSettingsLetterTemplate r
        join query.d_WordInterfaceTemplate d
          on r.WordTemplateDocumentId = d.ObjectId
      where d.Name like a_WordTemplateName;

    if t_DocumentIdList.count > 0 then
      a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_DocumentIdList);
    end if;
  end LetterTemplateSearch;

   /*---------------------------------------------------------------------------
   * FindProcessEmail() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure FindProcessEmail (
    a_Description           varchar2,
    a_Results               out api.udt_ObjectList
  ) is
  begin
    a_Results := api.udt_ObjectList();

    extension.pkg_cxproceduralsearch.InitializeSearch('o_ProcessEmail');

    if a_Description is null
    or a_Description = '%'
      then
        extension.pkg_cxproceduralsearch.SearchByIndex('Name', 'p_ABC_%', null, True);
      else
        extension.pkg_cxproceduralsearch.SearchByIndex('Name', 'p_ABC_%', null, True);
        extension.pkg_cxproceduralsearch.SearchByIndex('Description', a_Description, a_Description, true);
    end if;

    extension.pkg_cxproceduralsearch.PerformSearch(a_Results, 'and');

  end;

  /*---------------------------------------------------------------------------
   * FindUserProcesses() -- PUBLIC
   *-------------------------------------------------------------------------*/

  procedure FindUserProcesses(
    a_User               varchar2,
    a_ProcessType        varchar2,
    a_JobExternalFileNum varchar2,
    a_DateCompletedFrom  date,
    a_DateCompletedTo    date,
    a_RelatedObjectIds   out nocopy api.udt_ObjectList
  ) is
    t_TodoListUserId         number(9);
    t_SearchingUserId        number(9);
    t_DateCompletedFrom      date := a_DateCompletedFrom;
    t_DateCompletedTo        date := a_DateCompletedTo;
    t_FinalUserList          udt_ObjectList;
    t_LoopCount              number;
    t_InvalidLoopCount       number;

    --set user list to search based on entered user criteria or on the current user when no user criteria is entered.
    cursor c_CurrentUser is
      select users.UserId
        from api.users users
       where (Lower(users.Name) like Lower(a_User || '%')
         and a_User is not null)
          or (users.userid = t_SearchingUserId
         and a_user is null);
  begin

    --Set SearchingUserId to that of the current user.
    t_SearchingUserId := api.pkg_SecurityQuery.EffectiveUserId();

    --Set date range when none is specified.
    if a_DateCompletedFrom is not null
    and a_DateCompletedTo is null then
      t_DateCompletedTo := sysdate;
    elsif a_DateCompletedFrom is null
    and a_DateCompletedTo is not null then
      t_DateCompletedFrom := to_date('01/01/1900', 'mm/dd/yyyy');
    end if;

    a_RelatedObjectIds := api.udt_objectlist();
    t_FinalUserList    := api.udt_ObjectList();

    --these details are later used to raise error
    t_LoopCount        := 0;
    t_InvalidLoopCount := 0;

    --loop through users from search criteria
    for r in c_CurrentUser loop
      t_TodoListUserId := r.userid;

      --Verify that user is in valid user list
--      for c3 in 1 .. t_FinalUserList.count loop
--        if t_TodoListUserId = t_FinalUserList(c3).objectid then
          --If searched todo list is okay for user to see, return todo list.
          for p in (select pr.processid
                      from api.incompleteprocessassignments pa
                      join api.processes pr on pr.processid = pa.processid
                      join api.processtypes pt on pt.processtypeid = pr.processtypeid
                      join api.jobs jobs on jobs.jobid = pr.jobid
                     where pa.AssignedTo = t_TodoListUserId
                       and (a_ProcessType is null
                        or (a_ProcessType is not null
                       and pt.Description = a_ProcessType)
                       and (jobs.ExternalFileNum like a_JobExternalFileNum || '%'
                        or a_JobExternalFileNum is null)
                       and (pr.DateCompleted >= t_DateCompletedFrom
                        or (pr.DateCompleted is null
                       and t_DateCompletedFrom is null))
                       and (pr.DateCompleted < t_DateCompletedTo + 1
                        or (pr.DateCompleted is null
                       and t_DateCompletedTo is null)))) loop
            a_RelatedObjectIds.extend(1);
            a_RelatedObjectIds(a_RelatedObjectIds.count) := api.udt_object(p.processid);
          end loop; --to get User's todo list

        t_LoopCount := t_LoopCount + 1; --Count each time the loop is done for all users from the criteria
      end loop;

    -- If valid users where entered in the search criteria (the loop was run) but the searching user was not allowed to see any of them, raise error.
    if t_LoopCount = t_InvalidLoopCount and t_LoopCount <> 0 then
      api.pkg_errors.RaiseError(-20000, 'You do not have security to view the To-Do List of the user(s) you have searched for.');
    end if;
  end FindUserProcesses;

  /*---------------------------------------------------------------------------
   * FindUserProcessesExtract() -- PUBLIC
   *-------------------------------------------------------------------------*/

  function FindUserProcessesExtract(
    a_User                               varchar2,
    a_ProcessType                        varchar2,
    a_JobExternalFileNum                 varchar2,
    a_DateCompletedFrom                  date,
    a_DateCompletedTo                    date
  ) return api.udt_ObjectList is
    t_RelatedObjectIds                   api.udt_ObjectList;
  begin
    t_RelatedObjectIds := api.udt_ObjectList();
    FindUserProcesses(a_User, a_ProcessType, a_JobExternalFileNum, a_DateCompletedFrom,
        a_DateCompletedTo, t_RelatedObjectIds);
    return t_RelatedObjectIds;
  end FindUserProcessesExtract;

  -----------------------------------------------------------------------------
  -- Legal Entity Lookup
  --  Look up LE's based on Name - only includes Active LE's
  -----------------------------------------------------------------------------
  procedure LegalEntityLookup(
    a_LegalEntityName                  varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
    ) is
      t_Length                  number;
    begin
      a_ObjectList := api.udt_ObjectList();

      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_LegalEntity');

     t_Length := length(replace(a_LegalEntityName, '%'));

      if t_Length < 2  or t_Length is null then
         raise_application_error(-20000, 'Please enter at least 2 characters of the Legal Entity Name.');
      end if;
      extension.pkg_cxProceduralSearch.SearchByIndex('dup_FormattedName', a_LegalEntityName, a_LegalEntityName, true);
      extension.pkg_cxproceduralsearch.FilterObjects('Active', 'Y', 'Y', false);

      extension.pkg_cxProceduralSearch.PerformSearch(a_ObjectList, 'and');

  end LegalEntityLookup;

  -----------------------------------------------------------------------------
  -- Establishment Lookup
  --  Look up Establishments based on Name - only includes Active Establishments
  -----------------------------------------------------------------------------
  procedure EstablishmentLookup(
    a_EstablishmentName                varchar2,
    a_ObjectList                       out nocopy udt_ObjectList
    ) is
      t_Length                  number;
      t_UnfilteredObjects       udt_ObjectList;
    begin
      a_ObjectList := api.udt_ObjectList();
      t_UnfilteredObjects := api.udt_ObjectList();

      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Establishment');

      t_Length := length(replace(a_EstablishmentName, '%'));

      if t_Length < 2  or t_Length is null then
         raise_application_error(-20000, 'Please enter at least 2 characters of the Establishment Name.');
      end if;
      extension.pkg_cxProceduralSearch.SearchByIndex('DoingBusinessAs', a_EstablishmentName, a_EstablishmentName, true);
      extension.pkg_cxproceduralsearch.FilterObjects('Active', 'Y', 'Y', false);
      extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');
      extension.pkg_CollectionUtils.Append(a_ObjectList, t_UnfilteredObjects);
      t_UnfilteredObjects.delete;

      --Search again by the larger DBA Name field
      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Establishment');
      extension.pkg_cxProceduralSearch.SearchByIndex('AdditionalDBANames', a_EstablishmentName, a_EstablishmentName, true);
      extension.pkg_cxproceduralsearch.FilterObjects('Active', 'Y', 'Y', false);
      extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');
      extension.pkg_CollectionUtils.Append(a_ObjectList, t_UnfilteredObjects);
      t_UnfilteredObjects.delete;

  end EstablishmentLookup;

  -----------------------------------------------------------------------------
  -- DashInspectionSearch
  -----------------------------------------------------------------------------
  procedure DashInspectionSearch(
    a_InspectionType                varchar2,
    a_InspectionResult              varchar2,
    a_Region                        varchar2,
    a_FromInspectionDate            date,
    a_ToInspectionDate              date,
    a_ObjectList                       out nocopy udt_ObjectList
    ) is
      t_Length                  number;
      t_FromDate                date;
      t_ToDate                  date;
    begin
      a_ObjectList := api.udt_ObjectList();

      --Set date range when none is specified.
      if a_FromInspectionDate is not null
      and a_ToInspectionDate is null then
        t_ToDate := sysdate;
      elsif a_FromInspectionDate is null
      and a_ToInspectionDate is not null then
        t_FromDate := to_date('01/01/1900', 'mm/dd/yyyy');
      end if;

      extension.pkg_cxProceduralSearch.InitializeSearch('j_ABC_Inspection');

      extension.pkg_cxproceduralsearch.SearchByIndex('InspectionDate', nvl(a_FromInspectionDate,t_FromDate), nvl(a_ToInspectionDate,t_ToDate));
      extension.pkg_cxProceduralSearch.SearchByRelatedIndex('InspectionType', 'InspectionType', a_InspectionType, a_InspectionType, false);
      extension.pkg_cxProceduralSearch.SearchByRelatedIndex('InspectionResultType', 'Name', a_InspectionResult, a_InspectionResult, false);

      extension.pkg_cxproceduralsearch.FilterObjects('LicenseRegionOrInspectRegion', a_Region, a_Region, false);

      extension.pkg_cxProceduralSearch.PerformSearch(a_ObjectList, 'and');

  end DashInspectionSearch;

  -----------------------------------------------------------------------------
  -- DocumentTypeJSON
  -- procedure for the registered statement GetDocumentTypeJSON
  -- for the dropdown on multi-file upload.
  -----------------------------------------------------------------------------
  function DocumentTypeJSON(
    Argument     varchar2
  ) return clob is
    t_ObjectIds  udt_IdList;
    t_Names      udt_StringList;
    t_JSON       clob;
    t_Items      number;
  begin
    select o.ObjectId, o.Name
     bulk collect into t_ObjectIds, t_Names
      from query.o_abc_documenttype o
       where o.ObjectDefTypeId = 1 and o.Active = 'Y'
        order by o.Name;
    t_JSON := '[';
    t_Items := t_ObjectIds.count;
    for i in 1..t_Items loop
      t_JSON := t_JSON || '{description: "' || t_Names(i) || '", value: ' || t_ObjectIds(i) || '}';
      if i < t_Items then
        t_JSON := t_JSON || ',';
      end if;
    end loop;
    return t_JSON || ']';

  end DocumentTypeJSON;

  -----------------------------------------------------------------------------
  -- LicenseSearchExtract
  -----------------------------------------------------------------------------
  function LicenseSearchExtract(
    a_LicenseNumber                     varchar2,
    a_LicenseType                       varchar2,
    a_State                             varchar2,
    a_OriginalIssueDateFrom             date,
    a_OriginalIssueDateTo               date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_Establishment                     varchar2,
    a_PhysicalAddress                   varchar2,
    a_Licensee                          varchar2,
    --a_Operator                          varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_Restriction                       varchar2,
    a_CriminalConviction                varchar2,
    a_ConflictOfInterest                varchar2
    --a_RiskFactor                        varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    LicenseSearch(a_LicenseNumber, a_LicenseType, a_State, a_OriginalIssueDateFrom, a_OriginalIssueDateTo, a_IssueDateFrom, a_IssueDateTo, a_EffectiveDateFrom,
                  a_EffectiveDateTo, a_ExpirationDateFrom, a_ExpirationDateTo, a_InactivityStartDateFrom, a_InactivityStartDateTo, a_Establishment,
                  a_PhysicalAddress, a_Licensee, a_Region, a_Office, a_Restriction, a_CriminalConviction, a_ConflictOfInterest, t_MasterList, 'Y');
    return t_MasterList;

  end LicenseSearchExtract;

  -----------------------------------------------------------------------------
  -- JobSearchExtract
  --  Search for Jobs for Save as Excel
  -----------------------------------------------------------------------------
  function JobSearchExtract(
    a_JobNumber                         varchar2,
    a_JobType                           varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_LicenseNumber                     varchar2,
    a_PermitNumber                      varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_CreatedByUserFormattedName        varchar2,
    a_AutoCancelled                     varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    JobSearch(a_JobNumber, a_JobType, a_JobStatus, a_CreatedDateFrom, a_CreatedDateTo, a_CompletedDateFrom, a_CompletedDateTo, a_LicenseNumber, a_PermitNumber, a_Region, a_Office, a_CreatedByUserFormattedName, a_AutoCancelled, t_MasterList, 'Y');
    return t_MasterList;

  end JobSearchExtract;

 -----------------------------------------------------------------------------
  -- ProductSearchExtract
  -----------------------------------------------------------------------------
  function ProductSearchExtract(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    --=api.pkg_errors.RaiseError(-20000, 'a_Type: ' || a_Type);
    ProductSearch(a_RegistrationNumber, a_Name, a_Type, a_RegistrationDateFrom, a_RegistrationDateTo, a_ExpirationDateFrom, a_ExpirationDateTo, a_Registrant, a_State, a_DistributorLicenseNumber, a_DistributorName, t_MasterList, 'Y');
    return t_MasterList;

  end ProductSearchExtract;

  -----------------------------------------------------------------------------
  -- ProductSearchAmendment
  -----------------------------------------------------------------------------
  procedure ProductSearchAmendment(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_RegistrantObjectId                number,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is
    t_ProductSearchRetValLimit     number;
    t_State                        varchar2(10) := 'Current';
    t_FromObjectId                 number;

  begin
    -- Raise error if no search criteria is entered
   if a_RegistrationNumber is null
    and a_Name is null
    and a_Type is null
    and a_Registrant is null
    and a_State is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- ProductSearchReturnValueLimit
        select o.ProductSearchReturnValueLimit
          into t_ProductSearchRetValLimit
          from query.o_SystemSettings o
         where o.ObjectDefTypeId = 1
           and rownum = 1;

        select j.OnlineUseLegalEntityObjectId
          into t_FromObjectId
          from query.j_abc_pramendment j
         where j.ObjectId = a_RegistrantObjectId;

    a_Objects := api.udt_ObjectList();

    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Product');

    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber,true);
    extension.pkg_cxproceduralsearch.SearchByIndex('Name',a_Name,a_Name,true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductType','ObjectId',a_Type, a_Type, false);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LERegistrant','dup_FormattedName',a_Registrant, a_Registrant, true);
    extension.pkg_cxproceduralsearch.SearchByIndex('State', t_State,t_State,false);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LERegistrant','ObjectId',t_FromObjectId, t_FromObjectId, true);
    extension.pkg_cxproceduralsearch.PerformSearch(a_objects, 'and', 'none');

    if a_objects.Count > t_ProductSearchRetValLimit then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;
  end ProductSearchAmendment;

-----------------------------------------------------------------------------
  -- AccusationSearchExtract
  --   Search for Accusations for Save as Excel
  -- Programmer: StanH
  -- Date:       24Sep2012
  -----------------------------------------------------------------------------
  function AccusationSearchExtract(
    a_JobNumber                         varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_LicenseNumber                     varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_CreatedByUserFormattedName        varchar2,
    a_Violations                        varchar2,
    a_LicenseType                       varchar2,
    a_Decision                          varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    AccusationSearch(a_JobNumber, a_JobStatus, a_CreatedDateFrom, a_CreatedDateTo, a_CompletedDateFrom, a_CompletedDateTo, a_LicenseNumber, a_Region, a_Office, a_CreatedByUserFormattedName, a_Violations, a_LicenseType, a_Decision, t_MasterList, 'Y');
    return t_MasterList;
  end AccusationSearchExtract;

-----------------------------------------------------------------------------
  -- InspectionSearchExtract
  --   Search for Inspections for Save as Excel
  -- Programmer: StanH
  -- Date:       20Sep2012
  -----------------------------------------------------------------------------
  function InspectionSearchExtract(
    a_JobNumber                         varchar2,
    a_LicenseNumber                     varchar2,
    a_JobStatus                         varchar2,
    a_InspectionType                    varchar2,
    a_InspectionDateFrom                date,
    a_InspectionDateTo                  date,
    a_InspectedBy                       varchar2,
    a_InspectionResult                  varchar2,
    a_ViolationType                     varchar2,
    a_LicenseType                       varchar2,
    a_Region                            varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    InspectionSearch(a_JobNumber, a_LicenseNumber, a_JobStatus, a_InspectionType, a_InspectionDateFrom, a_InspectionDateTo,
                     a_InspectedBy, a_InspectionResult, a_ViolationType, a_LicenseType, a_Region, t_MasterList, 'Y');
    return t_MasterList;

  end InspectionSearchExtract;


  -----------------------------------------------------------------------------
  -- EstablishmentSearchExtract
  --  Search for Establishments for Save as Excel.
  -----------------------------------------------------------------------------
  function EstablishmentSearchExtract(
    a_EstablishmentType                 varchar2,
    a_DoingBusinessAs                   varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_Operator                          varchar2,
    a_IncludeHistory                    char
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    EstablishmentSearch(a_EstablishmentType, a_DoingBusinessAs, a_MailingAddress, a_PhysicalAddress, a_Operator, a_IncludeHistory, t_MasterList, 'Y');
    return t_MasterList;

  end EstablishmentSearchExtract;

  -----------------------------------------------------------------------------
  -- LegalEntitySearch
  --  Search for Legal Entities for Save as Excel
  --  Created by Andy Patterson (CXUSA)
  --  Created on 2/19/2014
  -----------------------------------------------------------------------------
  function LegalEntitySearchExtract(
    a_LegalName                         varchar2,
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_LegalEntityTypeObjectId           udt_id,
    a_CorporationNumber                 varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_IncludeHistory                    char,
    a_IsBrandRegistrant                 char,
    a_IsPermittee                       char,
    a_SSNTIN                            varchar2,
    a_DriversLicense                    varchar2,
    a_ContactEmail                      varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    LegalEntitySearch(a_LegalName, a_LastName, a_FirstName, a_LegalEntityTypeObjectId, a_CorporationNumber, a_MailingAddress, a_PhysicalAddress, a_IncludeHistory, a_IsBrandRegistrant, a_IsPermittee, a_SSNTIN, a_DriversLicense, a_ContactEmail, t_MasterList, 'Y');
    return t_MasterList;

  end LegalEntitySearchExtract;

  -----------------------------------------------------------------------------
  -- EducationEventSearch
  --  Search for Education Events
  -----------------------------------------------------------------------------
  procedure EducationEventSearch(
    a_EducationEventType                varchar2,
    a_EventName                         varchar2,
    a_StartDateFrom                     date,
    a_StartDateTo                       date,
    a_Objects                        out nocopy api.udt_ObjectList
  ) is

  begin
    if a_EducationEventType is null and a_EventName is null and a_StartDateFrom is null and a_StartDateTo is null then
      api.pkg_Errors.RaiseError(-20000, 'Please select some search criteria.');
    end if;

    a_Objects := api.udt_ObjectList();

    extension.Pkg_Cxproceduralsearch.InitializeSearch('o_ABC_EducationEvent');
    extension.pkg_CXProceduralSearch.SearchByIndex('EducationEventType',
                                                   a_EducationEventType,
                                                   a_EducationEventType);
    extension.pkg_CXProceduralSearch.SearchByIndex('EventName',
                                                   a_EventName,
                                                   a_EventName,
                                                   True);
    extension.pkg_CXProceduralSearch.SearchByIndex('StartDate',
                                                   a_StartDateFrom,
                                                   a_StartDateTo);
    extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and');

  end EducationEventSearch;

  -----------------------------------------------------------------------------
  -- ClassesSearch
  --  Search for Education Classes
  -----------------------------------------------------------------------------
  procedure ClassesSearch(
    a_Course                varchar2,
    a_ClassDateFrom         date,
    a_ClassDateTo           date,
    a_Objects               out nocopy api.udt_ObjectList
  ) is

    t_CurrentUserId         udt_Id := api.pkg_SecurityQuery.EffectiveUserId;

  begin
    a_Objects := api.udt_ObjectList();

    extension.Pkg_Cxproceduralsearch.InitializeSearch('o_ABC_Class');
    extension.pkg_CXProceduralSearch.SearchByIndex('dup_Course',
                                                   a_Course,
                                                   a_Course,
                                                   True);
    extension.pkg_CXProceduralSearch.SearchByIndex('ClassDate',
                                                   a_ClassDateFrom,
                                                   a_ClassDateTo);
    extension.pkg_CXProceduralSearch.SearchByRelatedIndex('Instructor',
                                                          'ObjectId',
                                                          t_CurrentUserId,
                                                          t_CurrentUserId,
                                                          False);
    extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and');

  end ClassesSearch;

  -----------------------------------------------------------------------------
  -- CertificationRequestLicSearch
  --  On the request certification job, search for licenses related to the
  --  selected legal entity
  -----------------------------------------------------------------------------
  procedure CertificationRequestLicSearch(
    a_LicenseeObjectId                  number,
    a_LicenseNumber                     varchar2,
    a_EstablishmentName                 varchar2,
    a_PhysicalAddress                   varchar2,
    a_Objects               out nocopy api.udt_ObjectList
  ) is

    --t_CurrentUserId         udt_Id := api.pkg_SecurityQuery.EffectiveUserId;

  begin
    a_Objects := api.udt_ObjectList();
    -- Raise error if there is no criteria entered
    if a_LicenseNumber is null and
       a_EstablishmentName is null and
       a_PhysicalAddress is null then
       raise_application_error(-20000, 'You must enter some search criteria.');
    else
      extension.Pkg_Cxproceduralsearch.InitializeSearch('o_ABC_License');
      extension.pkg_CXProceduralSearch.SearchByIndex('LicenseNumber',
                                                     a_LicenseNumber,
                                                     a_LicenseNumber,
                                                     False);
      extension.pkg_CXProceduralSearch.SearchByRelatedIndex('Establishment',
                                                            'DoingBusinessAs',
                                                            a_EstablishmentName,
                                                            a_EstablishmentName,
                                                            True);
      extension.pkg_CXProceduralSearch.SearchByRelatedIndex('PhysicalAddress',
                                                            'dup_FormattedDisplay',
                                                            a_PhysicalAddress,
                                                            a_PhysicalAddress,
                                                            True);
      extension.pkg_CXProceduralSearch.SearchByRelatedIndex('Licensee',
                                                            'ObjectId',
                                                            a_LicenseeObjectId,
                                                            a_LicenseeObjectId,
                                                            True);
      extension.pkg_CXProceduralSearch.PerformSearch(a_Objects, 'and');
    end if;

  end CertificationRequestLicSearch;

  -----------------------------------------------------------------------------
  -- VintageLookup
  --  Look up Vintages from within system settings threshold
  -----------------------------------------------------------------------------
  procedure VintageLookup(
    a_VintageYear               varchar2,
    a_Objects                   out nocopy udt_ObjectList
    ) is
    t_MasterList                api.udt_ObjectList;
    begin

      a_Objects := api.udt_ObjectList();

      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Vintage');
      extension.pkg_cxProceduralSearch.SearchByIndex('Year', a_VintageYear, a_VintageYear, true);
      extension.pkg_cxproceduralsearch.SearchByIndex('dup_IsWithinThreshold', 'Y', 'Y', true);
      extension.pkg_cxproceduralsearch.FilterObjects('Active', 'Y', 'Y', false);
      extension.pkg_cxProceduralSearch.PerformSearch(a_Objects, 'and');

   end VintageLookup;

  -----------------------------------------------------------------------------
  -- VintageLookupWithCurrent
  --  Look up Vintages from within sys settings threshold including any from
  --  outside this range that have been related to the object
  -----------------------------------------------------------------------------
  procedure VintageLookupWithCurrent(
    a_VintageYear     varchar2,
    a_SourceObjectId  number,
    a_Objects         out nocopy udt_ObjectList
    ) is
    t_MasterList      api.udt_ObjectList;
    t_EndPoint        udt_Id;
    t_VintageIds      udt_IdList;
    t_VintageObjIds   api.udt_ObjectList;
    t_VintageCount    number;
    begin

      t_EndPoint := api.pkg_configquery.EndPointIdForName('o_ABC_Vintage','PRrenewal');

      -- initialize the lists
      a_Objects       := api.udt_ObjectList();
      t_VintageObjIds := api.udt_ObjectList();

      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Vintage');
      extension.pkg_cxProceduralSearch.SearchByIndex('Year', a_VintageYear, a_VintageYear, true);
      extension.pkg_cxproceduralsearch.FilterObjects('IsWithinThreshold','Y','Y', false);
      extension.pkg_cxproceduralsearch.FilterObjects('Active', 'Y', 'Y', false);
      extension.pkg_cxProceduralSearch.PerformSearch(a_Objects, 'and');

      -- get the vintages
      t_VintageIds := extension.pkg_objectquery.RelatedObjects(a_SourceObjectId,'Vintage');

      -- append the lists (removes duplicates)
      for a in 1..t_VintageIds.Count loop
        t_VintageObjIds.Extend();
        t_VintageObjIds(a) := api.udt_Object(t_VintageIds(a));
      end loop;

      extension.Pkg_Collectionutils.Append(a_Objects, t_VintageObjIds);

   end VintageLookupWithCurrent;

  -----------------------------------------------------------------------------
  -- VintageLookupAll
  --  Look up all Vintages
  -----------------------------------------------------------------------------
  procedure VintageLookupAll(
    a_VintageYear               varchar2,
    a_Objects                   out nocopy udt_ObjectList
    ) is
    t_MasterList                api.udt_ObjectList;
    begin

      a_Objects := api.udt_ObjectList();

      extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Vintage');
      extension.pkg_cxProceduralSearch.SearchByIndex('Year', a_VintageYear, a_VintageYear, true);
      extension.pkg_cxproceduralsearch.FilterObjects('Active', 'Y', 'Y', false);
      extension.pkg_cxProceduralSearch.PerformSearch(a_Objects, 'and');

   end VintageLookupAll;

  -----------------------------------------------------------------------------
  -- DailyDepositSearch
  --  Search for Daily Deposit Jobs
  -----------------------------------------------------------------------------
  procedure DailyDepositSearch(
    a_JobNumber                         varchar2,
    a_FromDepositDate                   date,
    a_ToDepositDate                     date,
    a_FromCreatedDate                   date,
    a_ToCreatedDate                     date,
    a_Status                            varchar2,
    a_CreatedBy                         varchar2,
    a_Objects                       out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_StatusId                          udt_Id;
    t_UserIdList                        api.pkg_Definition.udt_IdList;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
  begin
    if a_JobNumber is null
        and a_FromDepositDate is null
        and a_ToDepositDate is null
        and a_FromCreatedDate is null
        and a_ToCreatedDate is null
        and a_Status is null then
        if  a_CreatedBy is null then
            raise_application_error(-20000, 'You must enter some search criteria.');
        else
            raise_application_error(-20000, 'Please enter more search criteria.');
        end if;
    end if;

    a_Objects := api.udt_ObjectList();

    extension.pkg_CxProceduralSearch.InitializeSearch('j_ABC_DailyDeposit');
    extension.pkg_CxProceduralSearch.SearchByIndex('ExternalFileNum', a_JobNumber, null, true);
    extension.pkg_CxProceduralSearch.SearchByIndex('DepositDate', a_FromDepositDate, a_ToDepositDate);
    -- Because of the way dates are passed in for this search, dates on an end date
    -- will not show up if they take place after midnight on a particular date. We
    -- do this to get the entire day. Deposit Date does not share this, since the
    -- user input value defaults to midnight on the entered day.
    extension.pkg_CxProceduralSearch.SearchByIndex('CreatedDate', a_FromCreatedDate, a_ToCreatedDate + 86399/86400);

    -- Obtain Status Id for SearchBySystemColumn
    if a_Status is not null then
      select s.StatusId
        into t_StatusId
        from api.Statuses s
        join api.JobStatuses js
          on s.StatusId = js.StatusId
       where s.Description = a_Status
             and js.JobTypeId = api.pkg_ConfigQuery.ObjectDefIdForName('j_ABC_DailyDeposit');

      extension.pkg_CxProceduralSearch.SearchBySystemColumn('StatusId', t_StatusId, t_StatusId, false);
    end if;

    if a_Createdby is not null then
      extension.pkg_CxProceduralSearch.FilterObjects('CreatedByUserFormattedName', a_CreatedBy, a_CreatedBy, true);
    end if;

    -- Neither of the following work. Why?
    -- extension.pkg_CxProceduralSearch.SearchByIndex('StatusDescription', a_Status, null, false);
    -- extension.pkg_CxProceduralSearch.SearchBySystemColumn('CreatedByUserName', a_CreatedBy, null, true);

    extension.pkg_CxProceduralSearch.PerformSearch(a_Objects, 'and');

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end DailyDepositSearch;

  -----------------------------------------------------------------------------
  -- DailyDepositExtract
  --  Search for Daily Deposit Jobs to be exported to Excel
  -----------------------------------------------------------------------------
  function DailyDepositExtract(
    a_JobNumber                         varchar2,
    a_FromDepositDate                   date,
    a_ToDepositDate                     date,
    a_FromCreatedDate                   date,
    a_ToCreatedDate                     date,
    a_Status                            varchar2,
    a_CreatedBy                         varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_ObjectList();

    DailyDepositSearch(a_JobNumber, a_FromDepositDate, a_ToDepositDate,
        a_FromCreatedDate, a_ToCreatedDate, a_Status, a_CreatedBy, t_MasterList, 'Y');

    return t_MasterList;
  end DailyDepositExtract;

  -----------------------------------------------------------------------------
  --  PaymentSearch
  --  Search for Payments.
  -----------------------------------------------------------------------------
  procedure PaymentSearch(
    a_ReceiptNumber                     varchar2,
    a_PaymentMethod                     varchar2,
    a_PaymentDateFrom                   date,
    a_PaymentDateTo                     date,
    a_DepositDateFrom                   date,
    a_DepositDateTo                     date,
    a_GLAccount                         varchar2,
    a_Payor                             varchar2,
    a_PayerInternal                     varchar2,
    a_PayeeReference                    varchar2,
    a_IsRefund                          char,
    a_ReasonForRefund                   varchar2,
    a_ECommerceReferenceId              number default null,
    a_Results                           out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is

    t_idlist                            api.pkg_Definition.udt_idlist;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
    t_GLAccountSplit                    udt_StringList;

  begin
    a_Results := api.udt_objectlist();
    t_SearchResults := api.pkg_Search.NewSearchResults();

    if a_ReceiptNumber is null
        and a_PaymentMethod is null
        and a_PaymentDateFrom is null
        and a_PaymentDateTo is null
        and a_DepositDateFrom is null
        and a_DepositDateTo is null
        and a_GLAccount is null
        and a_Payor is null
        and a_PayeeReference is null
        and a_ECommerceReferenceId is null then
        if  a_PayerInternal is null then
            raise_application_error(-20000, 'You must enter some search criteria.');
        else
            raise_application_error(-20000, 'Please enter more search criteria.');
        end if;
    end if;

    if a_ReceiptNumber is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'ReceiptNumber', a_ReceiptNumber||
          '%', t_SearchResults) then
        return;
      end if;
    end if;

    if a_PaymentMethod is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('o_Payment', 'PaymentMethod', 'ObjectId',
          a_PaymentMethod, t_SearchResults) then
        return;
      end if;
    end if;

    -- if (a_PaymentDateFrom is not null or a_PaymentDateTo is not null) then
    --   if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'PaymentDate', a_PaymentDateTo,
    --       a_PaymentDateFrom+1, t_SearchResults) then
    --     return;
    --   end if;
    -- end if;

  if (a_DepositDateFrom is not null or a_DepositDateTo is not null) then
      if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'DepositDate', a_DepositDateto,
          a_DepositDateFrom, t_SearchResults) then
        return;
      end if;
    end if;

    if a_GLAccount is not null then
      t_GLAccountSplit := extension.pkg_Utils.Split(a_GLAccount, ', ');
      if not api.pkg_search.IntersectByRelColumnValue('o_Payment', 'Fee', 'GLAccountId',
          t_GLAccountSplit , t_SearchResults) then
        return;
      end if;
    end if;

    if a_Payor is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'PayorForSearch', a_Payor||'%',
            t_SearchResults) then
        return;
      end if;
    end if;

    if a_PayerInternal is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'CreatedByForSearch',
          a_PayerInternal||'%', t_SearchResults) then
        return;
      end if;
    end if;

    if a_PayeeReference is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'PayeeReference',
          a_PayeeReference||'%', t_SearchResults) then
        return;
      end if;
    end if;

    -- If we are coming from the refund search, filter out things that aren't refunds
    if a_IsRefund = 'Y' and not api.pkg_Search.IntersectByColumnValue('o_Payment', 'IsRefund',
        a_IsRefund, t_SearchResults) then
       return;
    end if;

    if a_ECommerceReferenceId is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('o_Payment', 'eCom', 'ObjectId',
          a_ECommerceReferenceId, t_SearchResults) then
        return;
      end if;
    end if;

--    if a_IsRefund is not null then
    if a_IsRefund = 'Y' then
--    Payment Date for Refunds is stored truncated
       if (a_PaymentDateFrom is not null or a_PaymentDateTo is not null) then
        if not api.pkg_Search.IntersectByRelColumnValue('o_Payment', 'FeeTransaction',
            'TransactionDate', a_PaymentDateTo, a_PaymentDateFrom, t_SearchResults) then
             return;
          end if;
       end if;
       if a_ReasonForRefund is not null then
        if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'ReasonForRefund',
            a_ReasonForRefund||'%', t_SearchResults) then
             return;
          end if;
       end if;
  else
--    Payment Date for non-Refunds is stored fully with time stamp. Need to add another day.
       if (a_PaymentDateFrom is not null or a_PaymentDateTo is not null) then
        if not api.pkg_Search.IntersectByRelColumnValue('o_Payment', 'FeeTransaction',
            'TransactionDate', a_PaymentDateTo, a_PaymentDateFrom+1, t_SearchResults) then
             return;
          end if;
       end if;
    end if;

    t_idlist := api.pkg_search.SearchResultsToList(t_SearchResults);
    for x in 1..t_idlist.count loop
      a_Results.extend;
      a_Results(x) := api.udt_Object(t_idlist(x));
    end loop;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Results.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end PaymentSearch;

 -----------------------------------------------------------------------------
  -- PaymentSearchExtract
  -----------------------------------------------------------------------------
  function PaymentSearchExtract(
    a_ReceiptNumber                     varchar2,
    a_PaymentMethod                     varchar2,
    a_PaymentDateFrom                   date,
    a_PaymentDateTo                     date,
    a_DepositDateFrom                   date,
    a_DepositDateTo                     date,
    a_GLAccount                         varchar2,
    a_Payor                             varchar2,
    a_PayerInternal                     varchar2,
    a_PayeeReference                    varchar2,
    a_IsRefund                          char,
    a_ReasonForRefund                   varchar2,
    a_ECommerceReferenceId              number default null
  )  return api.udt_ObjectList is
    t_Results                        api.udt_ObjectList;
  begin
    t_Results := api.udt_objectlist();
    PaymentSearch(a_ReceiptNumber, a_PaymentMethod, a_PaymentDateFrom, a_PaymentDateTo,
        a_DepositDateFrom, a_DepositDateTo, a_GLAccount, a_Payor, a_PayerInternal,
            a_PayeeReference, a_IsRefund, a_ReasonForRefund, a_ECommerceReferenceId, t_Results,
               'Y');
    return t_Results;

  end PaymentSearchExtract;

  -----------------------------------------------------------------------------
  -- ProductRegistrantSearch
  --  Search for Products.
  -----------------------------------------------------------------------------
  procedure ProductRegistrantSearch(
    a_DistributorObjectId               number,
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_RegistrationDateFrom              date,
    a_RegistrationDateTo                date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is

    t_RegistrationDateFrom              date;
    t_RegistrationDateTo                date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_ProductSearchRetValLimit          number;
    t_CurrentList                       udt_ObjectList;
    t_HistoricList                      udt_ObjectList;

  begin
    -- Raise error if no search criteria is entered
    if  a_DistributorObjectId is null then
      raise_application_error(-20000, 'A License or TAP permit must be entered before adding products.');
    elsif a_DistributorObjectId is null
    and a_RegistrationNumber is null
    and a_Name is null
    and a_Type is null
    and a_RegistrationDateFrom is null
    and a_RegistrationDateTo is null
    and a_ExpirationDateFrom is null
    and a_ExpirationDateTo is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- ProductSearchReturnValueLimit
    select o.ProductSearchReturnValueLimit
      into t_ProductSearchRetValLimit
      from query.o_SystemSettings o
     where o.ObjectDefTypeId = 1
       and rownum = 1;

    -- RegistrationDate
    if a_RegistrationDateFrom is not null or a_RegistrationDateTo is not null then
      if a_RegistrationDateFrom is null then
        t_RegistrationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_RegistrationDateFrom := to_date(to_char(a_RegistrationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_RegistrationDateTo is null then
        t_RegistrationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_RegistrationDateTo := to_date(to_char(a_RegistrationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- ExpirationDate
    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    a_Objects := api.udt_ObjectList();

    -- Search for products related to the License/TAP Permit Distributor
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Product');

    if api.pkg_columnquery.value(a_DistributorObjectId, 'ObjectDefName') = 'o_ABC_License' then
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('DistributorLic','ObjectId',a_DistributorObjectId,a_DistributorObjectId,true);
    elsif api.pkg_columnquery.value(a_DistributorObjectId, 'ObjectDefName') is not null /*= 'o_ABC_Permit'*/ then
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('TAPDistributor','ObjectId',a_DistributorObjectId,a_DistributorObjectId,true);
    else null;
    end if;
    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber,true);
    extension.pkg_cxproceduralsearch.SearchByIndex('Name',a_Name,a_Name,true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductType','ObjectId',a_Type, a_Type, false);
    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationDate', t_RegistrationDateFrom,t_RegistrationDateTo);
    extension.pkg_cxproceduralsearch.SearchByIndex('ExpirationDate', t_ExpirationDateFrom,t_ExpirationDateTo);

    extension.pkg_cxproceduralsearch.PerformSearch(a_Objects, 'and', 'none');


    -- If a License is the Distributor, search through all historical products and add them to the list.
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Product');

    if api.pkg_columnquery.value(a_DistributorObjectId, 'ObjectDefName') = 'o_ABC_License' then
      t_HistoricList := api.udt_ObjectList();
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('HistoricProducts','LicenseNumber',api.pkg_columnquery.value(a_DistributorObjectId,'LicenseNumber'),api.pkg_columnquery.value(a_DistributorObjectId,'LicenseNumber'),true);
      extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber,true);
      extension.pkg_cxproceduralsearch.SearchByIndex('Name',a_Name,a_Name,true);
      extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductType','ObjectId',a_Type, a_Type, false);
      extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationDate', t_RegistrationDateFrom,t_RegistrationDateTo);
      extension.pkg_cxproceduralsearch.SearchByIndex('ExpirationDate', t_ExpirationDateFrom,t_ExpirationDateTo);

      extension.pkg_cxproceduralsearch.PerformSearch(t_HistoricList, 'and', 'none');

      extension.pkg_collectionutils.Append(a_Objects, t_HistoricList);
    else null;
    end if;

    if a_objects.Count > t_ProductSearchRetValLimit then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search.');
    end if;

  end ProductRegistrantSearch;


  -----------------------------------------------------------------------------
  --  PaymentAdjustmentSearch
  --  Search for Payment Adjustment jobs.
  --  Created By: Andy Patterson (CXUSA)
  --  Created On: 7/31/2014
  -----------------------------------------------------------------------------
  procedure PaymentAdjustmentSearch(
    a_AdjustmentDateFrom                 date,
    a_AdjustmentDateTo                   date,
    a_DepositDateFrom                    date,
    a_DepositDateTo                      date,
    a_Results                            out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is

    t_SearchResults                      api.pkg_Definition.udt_SearchResults;
    t_AdjustmentDateTo                   date;
    t_DepositDateTo                      date;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);

  begin

    a_Results := api.udt_objectlist();
    t_SearchResults := api.pkg_Search.NewSearchResults();

    if a_AdjustmentDateFrom is not null or a_AdjustmentDateTo is not null then
      if  a_AdjustmentDateTo is not null then
        -- Ensure that adjustments made during the day are included since this is going against the CompletedDate which includes a timestamp
        t_AdjustmentDateTo := to_date(to_char(trunc(a_AdjustmentDateTo), 'MM/DD/YYYY') || ' 23:59:59', 'MM/DD/YYYY HH24:MI:SS');
      end if;

      if not api.pkg_Search.IntersectByColumnValue('j_ABC_PaymentAdjustment', 'CompletedDate', a_AdjustmentDateFrom, t_AdjustmentDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    if a_DepositDateFrom is not null or a_DepositDateTo is not null then
      if a_DepositDateTo is not null then
        -- Ensure that Deposits made during the day are included.
        t_DepositDateTo := to_date(to_char(trunc(a_DepositDateTo), 'MM/DD/YYYY') || ' 23:59:59', 'MM/DD/YYYY HH24:MI:SS');
      end if;

      if not api.pkg_Search.IntersectByRelColumnValue('j_ABC_PaymentAdjustment', 'Payment', 'DepositDate', a_DepositDateFrom, t_DepositDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    a_Results := SearchResultsToObjList(t_SearchResults);

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Results.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end PaymentAdjustmentSearch;


 -----------------------------------------------------------------------------
  -- PaymentAdjustmentSearchExtract
  -----------------------------------------------------------------------------
  function PaymentAdjustmentSearchExtract(
    a_AdjustmentDateFrom                 date,
    a_AdjustmentDateTo                   date,
    a_DepositDateFrom                    date,
    a_DepositDateTo                      date
  )  return api.udt_ObjectList is
    t_Results                        api.udt_ObjectList;
  begin
    t_Results := api.udt_objectlist();
    PaymentAdjustmentSearch(a_AdjustmentDateFrom, a_AdjustmentDateTo, a_DepositDateFrom, a_DepositDateTo, t_Results, 'Y');
    return t_Results;

  end PaymentAdjustmentSearchExtract;
  -----------------------------------------------------------------------------
  -- PetitionLicenseSearch
  --  Search for License valid for Petition jobs.
  --  Updated 4/8 - Paul O
  --    Modified to use api.Search, cleaner and improves performance. Issue 8094
  -----------------------------------------------------------------------------
  procedure PetitionLicenseSearch(
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_Objects                           out nocopy api.udt_ObjectList
  ) is

    t_LicenseNumberValue                udt_IdList;
    t_LicenseLength                     number;
    t_LicenseeLength                    number;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_TempList                          udt_Idlist;

  begin
    -- Raise error if no search criteria is entered

    if a_LicenseNumber is null
     and a_Licensee is null then
        raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    t_LicenseLength := length(a_LicenseNumber);

    if t_LicenseLength < 3 then
        raise_application_error(-20000, 'Please enter at least 3 digits of the License Number.');
      end if;

    t_LicenseeLength := length(a_Licensee);

    if t_LicenseeLength < 2 then
        raise_application_error(-20000, 'Please enter at least 2 characters for the Licensee.');
    end if;

    a_Objects := api.udt_ObjectList();
    --Start with all licenses where State is Active
    if not api.pkg_Search.IntersectByColumnValue('o_ABC_License', 'State', 'Active', t_SearchResults) then
        return;
    end if;
    --Search by search criteria
    if a_LicenseNumber is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_License', 'LicenseNumber', a_LicenseNumber||'%', t_SearchResults) then
          return;
      end if;
    end if;
    if a_Licensee is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'Licensee', 'dup_FormattedName', a_Licensee||'%', t_SearchResults) then
          return;
      end if;
    end if;
    --Only include Municipality Issued
    if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'LicenseType', 'IssuingAuthority', 'Municipality', t_SearchResults) then
        return;
    end if;

    t_TempList := api.pkg_Search.SearchResultsToList(t_SearchResults);

    for a in 1..t_TempList.Count loop
      a_Objects.Extend(1);
      a_Objects(a_Objects.Count) := api.udt_Object(t_TempList(a));
    end loop;

  end PetitionLicenseSearch;

  -----------------------------------------------------------------------------
  -- LegalJobSearch
  --  Search for Legal Jobs on Internal Site
  -----------------------------------------------------------------------------
  procedure LegalJobSearch(
    a_JobNumber                         varchar2,
    a_DocketAppealNumber                varchar2,
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_JobType                           varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_DateSendToOALFrom                 date,
    a_DateSendToOALTo                   date,
    a_DAG                               varchar2,
    a_AppealWentToOAL                   char,
    a_AppealResultFine                  char,
    a_AppealType                        varchar2,
    a_Objects                       out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_ResultsAppeal                     api.pkg_Search.udt_SearchResults;
    t_ResultsPetition                   api.pkg_Search.udt_SearchResults;
    t_ResultsPetitionAmend              api.pkg_Search.udt_SearchResults;
    t_SearchNotDone                     boolean := true;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
  begin
    -- Check to make sure at least some criteria has been entered.
    if a_JobNumber is null
        and a_DocketAppealNumber is null
        and a_LicenseNumber is null
        and a_Licensee is null
        and a_JobStatus is null
        and a_CreatedDateFrom is null
        and a_CreatedDateTo is null
        and a_CompletedDateFrom is null
        and a_CompletedDateTo is null
        and a_DateSendToOALFrom is null
        and a_DateSendToOALTo is null
        and a_DAG is null
        and a_AppealWentToOAL is null
        and a_AppealResultFine is null
        and a_AppealType is null
    then
      api.pkg_Errors.RaiseError(-20000, 'You must enter some search criteria.');
    end if;

    -- Search by Appeal.
    if a_JobType = 'Appeal' or a_JobType is null then
      t_ResultsAppeal := api.pkg_Search.NewSearchResults;

      -- Job Number
      if t_SearchNotDone and a_JobNumber is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Appeal',
            'ExternalFileNum', a_JobNumber || '%', t_ResultsAppeal);
      end if;

      -- Appeal Number
      if t_SearchNotDone and a_DocketAppealNumber is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Appeal',
            'AppealNumber', a_DocketAppealNumber || '%', t_ResultsAppeal);
      end if;

      -- License Number
      if t_SearchNotDone and a_LicenseNumber is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Appeal',
            'LicenseNumber', a_LicenseNumber || '%', t_ResultsAppeal);
      end if;

      -- Licensee
      if t_SearchNotDone and a_Licensee is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Appeal',
            'Licensee', a_Licensee || '%', t_ResultsAppeal);
      end if;

      -- Job Status
      if t_SearchNotDone and a_JobStatus is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnContents('j_ABC_Appeal',
            'StatusDescription', a_JobStatus, t_ResultsAppeal);
      end if;

      -- Created Date
      if t_SearchNotDone and (a_CreatedDateFrom is not null or a_CreatedDateTo is not null) then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Appeal',
            'CreatedDate', a_CreatedDateFrom, a_CreatedDateTo, t_ResultsAppeal);
      end if;

      -- Completed Date
      if t_SearchNotDone and (a_CompletedDateFrom is not null or a_CompletedDateTo is not null) then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Appeal',
            'CompletedDate', a_CompletedDateFrom, a_CompletedDateTo, t_ResultsAppeal);
      end if;

      -- Date Send to OAL
      if t_SearchNotDone and (a_DateSendToOALFrom is not null or a_DateSendToOALTo is not null) then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Appeal',
            'DateSentToOAL', a_DateSendToOALFrom, a_DateSendToOALTo, t_ResultsAppeal);
      end if;

      -- DAG
      if t_SearchNotDone and a_DAG is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Appeal',
            'DeputyAttorneyGeneral', a_DAG || '%', t_ResultsAppeal);
      end if;

      -- Appeal Type
      if t_SearchNotDone and a_AppealType is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnContents('j_ABC_Appeal',
            'AppealType', a_AppealType, t_ResultsAppeal);
      end if;

      -- Appeal Went to OAL?
      if t_SearchNotDone and a_AppealWentToOAL is not null then
        declare
          t_IdList                      udt_IdList;
        begin
          case a_AppealWentToOAL
            when 'Yes' then
              select a.ObjectId
                bulk collect into t_IdList
                from query.j_ABC_Appeal a
                join query.p_ABC_RecordOALDescision oal
                  on a.JobId = oal.JobId;
            when 'No' then
              select ObjectId
                bulk collect into t_IdList
                from query.j_ABC_Appeal
               where ObjectId not in (select a.ObjectId
                                        from query.j_ABC_Appeal a
                                        join query.p_ABC_RecordOALDescision oal
                                          on a.JobId = oal.JobId);
          end case;

          t_SearchNotDone := api.pkg_Search.IntersectList(t_IdList, t_ResultsAppeal);
        end;
      end if;

      -- Appeal Resulted in a fine?
      if t_SearchNotDone and a_AppealResultFine is not null then
        declare
          t_IdList                      udt_IdList;
        begin
          case a_AppealResultFine
            when 'Yes' then
              select a.ObjectId
                bulk collect into t_IdList
                from query.j_ABC_Appeal a
                join query.p_ABC_FineAssessment fa
                  on a.JobId = fa.JobId;
            when 'No' then
              select ObjectId
                bulk collect into t_IdList
                from query.j_ABC_Appeal
               where ObjectId not in (select a.ObjectId
                                        from query.j_ABC_Appeal a
                                        join query.p_ABC_FineAssessment fa
                                          on a.JobId = fa.JobId);
          end case;

          t_SearchNotDone := api.pkg_Search.IntersectList(t_IdList, t_ResultsAppeal);
        end;
      end if;
    end if;

    -- Search by Petition. We don't search by Petition if they have chosen an 'Appeal'
    -- option, because it doesn't make sense to return Petitions in this case.
    if (a_JobType = 'Petition' or a_JobType is null) and
        (a_AppealWentToOAL is null and a_AppealResultFine is null and a_AppealType is null) then
      t_SearchNotDone   := true;
      t_ResultsPetition := api.pkg_Search.NewSearchResults;

      -- Job Number
      if t_SearchNotDone and a_JobNumber is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Petition',
            'ExternalFileNum', a_JobNumber || '%', t_ResultsPetition);
      end if;

      -- Docket Number
      if t_SearchNotDone and a_DocketAppealNumber is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Petition',
            'DocketNumber', a_DocketAppealNumber || '%', t_ResultsPetition);
      end if;

      -- License Number
      if t_SearchNotDone and a_LicenseNumber is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Petition',
            'LicenseNumber', a_LicenseNumber || '%', t_ResultsPetition);
      end if;

      -- Licensee
      if t_SearchNotDone and a_Licensee is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Petition',
            'Licensee', a_Licensee || '%', t_ResultsPetition);
      end if;

      -- Job Status
      if t_SearchNotDone and a_JobStatus is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnContents('j_ABC_Petition',
            'StatusDescription', a_JobStatus, t_ResultsPetition);
      end if;

      -- Created Date
      if t_SearchNotDone and (a_CreatedDateFrom is not null or a_CreatedDateTo is not null) then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Petition',
            'CreatedDate', a_CreatedDateFrom, a_CreatedDateTo, t_ResultsPetition);
      end if;

      -- Completed Date
      if t_SearchNotDone and (a_CompletedDateFrom is not null or a_CompletedDateTo is not null) then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Petition',
            'CompletedDate', a_CompletedDateFrom, a_CompletedDateTo, t_ResultsPetition);
      end if;

      -- DAG
      if t_SearchNotDone and a_DAG is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_Petition',
            'DefaultAttorneyGeneral', a_DAG || '%', t_ResultsPetition);
      end if;
    end if;

    if (a_JobType = 'Petition Amendment' or a_JobType is null) and
        (a_AppealWentToOAL is null and a_AppealResultFine is null and a_AppealType is null) then
      t_SearchNotDone   := true;
      t_ResultsPetitionAmend := api.pkg_Search.NewSearchResults;

      -- Job Number
      if t_SearchNotDone and a_JobNumber is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_PetitionAmendment',
            'ExternalFileNum', a_JobNumber || '%', t_ResultsPetitionAmend);
      end if;

      -- License Number
      if t_SearchNotDone and a_LicenseNumber is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_PetitionAmendment',
            'LicenseNumber', a_LicenseNumber || '%', t_ResultsPetitionAmend);
      end if;

      -- Licensee
      if t_SearchNotDone and a_Licensee is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_PetitionAmendment',
            'Licensee', a_Licensee || '%', t_ResultsPetitionAmend);
      end if;

      -- Job Status
      if t_SearchNotDone and a_JobStatus is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnContents('j_ABC_PetitionAmendment',
            'StatusDescription', a_JobStatus, t_ResultsPetitionAmend);
      end if;

      -- Created Date
      if t_SearchNotDone and (a_CreatedDateFrom is not null or a_CreatedDateTo is not null) then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_PetitionAmendment',
            'CreatedDate', a_CreatedDateFrom, a_CreatedDateTo, t_ResultsPetitionAmend);
      end if;

      -- Completed Date
      if t_SearchNotDone and (a_CompletedDateFrom is not null or a_CompletedDateTo is not null) then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_PetitionAmendment',
            'CompletedDate', a_CompletedDateFrom, a_CompletedDateTo, t_ResultsPetitionAmend);
      end if;

      -- DAG
      if t_SearchNotDone and a_DAG is not null then
        t_SearchNotDone := api.pkg_Search.IntersectByColumnValue('j_ABC_PetitionAmendment',
            'DefaultAttorneyGeneral', a_DAG || '%', t_ResultsPetitionAmend);
      end if;
    end if;

    -- Union results
    api.pkg_Search.UnionSearchResults(t_ResultsPetition, t_ResultsAppeal);
    api.pkg_Search.UnionSearchResults(t_ResultsPetitionAmend, t_ResultsAppeal);

    -- Convert to ObjectList for Outrider
    declare
      t_IdList                          udt_IdList;
    begin
      t_IdList := api.pkg_Search.SearchResultsToList(t_ResultsAppeal);

      a_Objects := api.udt_ObjectList();
      for i in 1..t_IdList.count loop
        a_Objects.extend;
        a_Objects(i) := api.udt_Object(t_IdList(i));
      end loop;
    end;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end LegalJobSearch;

  -----------------------------------------------------------------------------
  -- LegalJobSearchExtract
  --  Extract Legal Jobs for Excel
  -----------------------------------------------------------------------------
  function LegalJobSearchExtract(
    a_JobNumber                         varchar2,
    a_DocketAppealNumber                varchar2,
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_JobType                           varchar2,
    a_JobStatus                         varchar2,
    a_CreatedDateFrom                   date,
    a_CreatedDateTo                     date,
    a_CompletedDateFrom                 date,
    a_CompletedDateTo                   date,
    a_DateSendToOALFrom                 date,
    a_DateSendToOALTo                   date,
    a_DAG                               varchar2,
    a_AppealWentToOAL                   char,
    a_AppealResultFine                  char,
    a_AppealType                        varchar2
  ) return api.udt_ObjectList is
    t_Objects                           api.udt_ObjectList;
  begin
    LegalJobSearch(a_JobNumber, a_DocketAppealNumber, a_LicenseNumber, a_Licensee, a_JobType,
        a_JobStatus, a_CreatedDateFrom, a_CreatedDateTo, a_CompletedDateFrom, a_CompletedDateTo,
        a_DateSendToOALFrom, a_DateSendToOALTo, a_DAG, a_AppealWentToOAL, a_AppealResultFine, a_AppealType, t_Objects, 'Y');

    return t_Objects;
  end LegalJobSearchExtract;

  /*---------------------------------------------------------------------------
   * PermitSearch() -- PUBLIC
   *   Search for Permits
   *-------------------------------------------------------------------------*/
  procedure PermitSearch (
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_State                             varchar2,
    a_OriginalIssueDateFrom             date,
    a_OriginalIssueDateTo               date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Permittee                         varchar2,
    a_Location                          varchar2,
    a_LocationDesc                      varchar2,
    a_County                            number,
    a_Municipality                      number,
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_LicenseType                       number,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_SolicitorName                     varchar2,
    a_Objects                out nocopy api.udt_Objectlist,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_OriginalIssueDateFrom             date;
    t_OriginalIssueDateTo               date;
    t_IssueDateFrom                     date;
    t_IssueDateTo                       date;
    t_EffectiveDateFrom                 date;
    t_EffectiveDateTo                   date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_EventDateFrom                     date;
    t_EventDateTo                       date;
    t_SearchLimit                       number;
    t_SearchResults                     udt_SearchResults;
    t_LicenseResults                    udt_SearchResults;
    t_PermitDefName                     varchar2(30) := 'o_ABC_Permit';
    t_BaseFromDate                      date
        := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                        date
        := to_date('31-DEC-3999 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    t_MuniIds                           udt_IdList;
    t_CountyIds                         udt_IdList;
    t_PermitTypeSplit                   udt_StringList;
    t_PermitIds                         udt_idlist;
    t_PermitIds1                        udt_idlist;
    t_AllObjects                        udt_ObjectList;
    t_PermitFound                       varchar2(1);
    t_FirstIssueDate                    date;
    t_Index                             number := 0;
    t_PermitNumber                      varchar2(100);
  begin

    -- Initialize the collection
    a_Objects := api.udt_ObjectList();

    -- Ensure that at least one Search Criteria is populated
    if a_PermitNumber is null
        and a_State is null
        and a_OriginalIssueDateFrom is null
        and a_OriginalIssueDateTo is null
        and a_IssueDateFrom is null
        and a_IssueDateTo is null
        and a_EffectiveDateFrom is null
        and a_EffectiveDateTo is null
        and a_ExpirationDateFrom is null
        and a_ExpirationDateTo is null
        and a_Permittee is null
        and a_Location is null
        and a_LocationDesc is null
        and a_County is null
        and a_Municipality is null
        and a_LicenseNumber is null
        and a_Licensee is null
        and a_EventDateFrom is null
        and a_EventDateTo is null
        and a_SolicitorName is null then
      if a_PermitType is null
          and a_LicenseType is null then
        raise_application_error(-20000,'You must enter some search criteria.');
      else
        raise_application_error(-20000,'You must enter more search criteria.');
      end if;
    end if;

    -- Set Begin Dates and End Dates if no date supplied and add timestamps to take
    -- the full day into account
    -- OriginalIssueDate
    if a_OriginalIssueDateFrom is not null or a_OriginalIssueDateTo is not null then
      if a_OriginalIssueDateFrom is null then
        t_OriginalIssueDateFrom := t_BaseFromDate;
      else
        t_OriginalIssueDateFrom := to_date(
            to_char(a_OriginalIssueDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_OriginalIssueDateTo is null then
        t_OriginalIssueDateTo := t_BaseToDate;
      else
        t_OriginalIssueDateTo := to_date(
            to_char(a_OriginalIssueDateTo, 'DD-MON-YYYY') || ' 23:59:59',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    if a_IssueDateFrom is not null or a_IssueDateTo is not null then
      if a_IssueDateFrom is null then
        t_IssueDateFrom := t_BaseFromDate;
      else
        t_IssueDateFrom := to_date(
            to_char(a_IssueDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_IssueDateTo is null then
        t_IssueDateTo := t_BaseToDate;
      else
        t_IssueDateTo := to_date(
            to_char(a_IssueDateTo, 'DD-MON-YYYY') || ' 23:59:59',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    if a_EffectiveDateFrom is not null or a_EffectiveDateTo is not null then
      if a_EffectiveDateFrom is null then
        t_EffectiveDateFrom := t_BaseFromDate;
      else
        t_EffectiveDateFrom := to_date(
            to_char(a_EffectiveDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EffectiveDateTo is null then
        t_EffectiveDateTo := t_BaseToDate;
      else
        t_EffectiveDateTo := to_date(
            to_char(a_EffectiveDateTo, 'DD-MON-YYYY') || ' 23:59:59',
              'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := t_BaseFromDate;
      else
        t_ExpirationDateFrom := to_date(
            to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := t_BaseToDate;
      else
        t_ExpirationDateTo := to_date(
            to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if a_EventDateFrom is null then
        t_EventDateFrom := t_BaseFromDate;
      else
        t_EventDateFrom := to_date(
            to_char(a_EventDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EventDateTo is null then
        t_EventDateTo := t_BaseToDate;
      else
        t_EventDateTo := to_date(
            to_char(a_EventDateTo, 'DD-MON-YYYY') || ' 23:59:59',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    -- Begin Search Procedures
    t_SearchResults := api.pkg_Search.NewSearchResults();

    -- Permit Number
    if a_PermitNumber is not null then
      -- Ensure that Permit Number does not end with wild card character ('%')
      t_PermitNumber := a_PermitNumber;
      if instr(t_PermitNumber, '%', 1) = length(t_PermitNumber) then
        t_PermitNumber := substr(a_PermitNumber, 1, length(a_PermitNumber)-1);
      end if;
      if not api.pkg_Search.IntersectByColumnContents(t_PermitDefName,
          'PermitNumber', t_PermitNumber, t_SearchResults) then
        return;
      end if;
    end if;

    -- Original Issue Date
    if t_OriginalIssueDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName,
          'OriginalIssueDate', t_OriginalIssueDateFrom, t_OriginalIssueDateTo,
          t_SearchResults) then
        return;
      end if;
    end if;

    -- Issue Date
    if t_IssueDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName,
          'IssueDate', t_IssueDateFrom, t_IssueDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    -- Effective Date
    if t_EffectiveDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName,
          'EffectiveDate', t_EffectiveDateFrom, t_EffectiveDateTo, t_SearchResults)
          then
        return;
      end if;
    end if;

    -- Expiration Date
    if t_ExpirationDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName,
          'ExpirationDate', t_ExpirationDateFrom, t_ExpirationDateTo,
          t_SearchResults) then
        return;
      end if;
    end if;

    -- Event Date
    if t_EventDateFrom is not null then
      if not api.pkg_Search.IntersectByRelColumnValue(t_PermitDefName, 'EventDate',
          'StartDate', t_EventDateFrom, t_EventDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    -- State
    if a_State is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'State',
          a_State, t_SearchResults) then
        return;
      end if;
    end if;

    -- Permitter
    if a_Permittee is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName,
          'dup_PermitteeDisplay_SQL', a_Permittee, t_SearchResults) then
        return;
      end if;
    end if;

    -- Location
    if a_Location is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'dup_Location',
          a_Location, t_SearchResults) then
        return;
      end if;
    end if;

    -- Location Description
    if a_LocationDesc is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName,
          'EventLocAddressDescription', a_LocationDesc, t_SearchResults) then
        return;
      end if;
    end if;

    -- County
    if a_County is not null then
      t_CountyIds(1) := a_County;
      if not api.pkg_Search.IntersectByRelObjectIds(t_PermitDefName, 'County',
          t_CountyIds, t_SearchResults) then
        return;
      end if;
    end if;

    -- Municipality
    if a_Municipality is not null then
      t_MuniIds(1) := a_Municipality;
      if not api.pkg_Search.IntersectByRelObjectIds(t_PermitDefName, 'Municipality',
          t_MuniIds, t_SearchResults) then
        return;
      end if;
    end if;

    -- License Number
    if a_LicenseNumber is not null then
      t_LicenseResults := api.pkg_Search.NewSearchResults();
      -- Get the results for both License relationships and union them together
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName, 'License',
          'LicenseNumber', a_LicenseNumber, t_LicenseResults);
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName, 'SellersLicense',
          'LicenseNumber', a_LicenseNumber, t_LicenseResults);
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName,
          'PermitGenerationalLicense', 'LicenseNumber', a_LicenseNumber,
          t_LicenseResults);

      -- Next, intersect those License Search results with the current search
      -- results
      if not api.pkg_Search.IntersectSearchResults(t_LicenseResults,
          t_SearchResults) then
        return;
      end if;
    end if; -- a_LicenseNumber

    -- Licensee
    if a_Licensee is not null then
      t_LicenseResults := api.pkg_Search.NewSearchResults();
      -- Get the results for both License relationships and union them together
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName, 'Licensee',
          'dup_Licensee', a_Licensee, t_LicenseResults);
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName, 'SellersLicensee',
          'dup_Licensee', a_Licensee, t_LicenseResults);
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName,
          'PermitGenerationalLicense', 'Licensee', a_Licensee, t_LicenseResults);

      -- Intersect those License Search results with the current search results
      if not api.pkg_Search.IntersectSearchResults(t_LicenseResults,
          t_SearchResults) then
        return;
      end if;
    end if; --a_Licensee

    -- License Type
    if a_LicenseType is not null then
      t_LicenseResults := api.pkg_Search.NewSearchResults();
      -- Get the results for both License relationships and union them together
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName, 'License',
          'LicenseTypeObjectId', a_LicenseType, t_LicenseResults);
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName, 'SellersLicense',
          'LicenseTypeObjectId', a_LicenseType, t_LicenseResults);
      api.pkg_Search.UnionByRelColumnValue(t_PermitDefName,
          'PermitGenerationalLicense', 'LicenseTypeObjectId', a_LicenseType,
          t_LicenseResults);

      -- Next, intersect those License Search results with the current search results
      if not api.pkg_Search.IntersectSearchResults(t_LicenseResults,
          t_SearchResults) then
        return;
      end if;
    end if; --a_LicenseType

    -- Permit Type
    if a_PermitType is not null then
      -- Get the different permit types into a table which will be passed into
      -- the search.
      t_PermitTypeSplit := extension.pkg_Utils.Split(a_PermitType,', ');

      if not api.pkg_Search.IntersectByRelColumnValue(t_PermitDefName, 'PermitType',
          'Name', t_PermitTypeSplit, t_SearchResults) then
        return;
      end if;
    end if;

    -- Solicitor Name
    if a_SolicitorName is not null then
      if not api.pkg_Search.IntersectByRelColumnContents(t_PermitDefName,
          'Solicitor','dup_FormattedName', a_SolicitorName, t_SearchResults) then
        return;
      end if;
    end if;

    -- Return (Insignia) Permits that were not canceled by the system
    if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'SystemCancelled',
        'N', t_SearchResults) then
       return;
    end if;

    -- Convert to udt_ObjectList
    a_Objects := extension.pkg_Utils.ConvertToObjectList(
        api.pkg_Search.SearchResultsToList(t_SearchResults));

    --Check Search Limit
    t_SearchLimit  := OutriderSearchLimit();
    if a_Objects.Count > t_SearchLimit and nvl(a_ExcelExtract,'N') = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end PermitSearch;

  /*---------------------------------------------------------------------------
   * PermitSearchExtract() -- PUBLIC
   *   Search for Permits to be exported to Excel.
   *-------------------------------------------------------------------------*/
  function PermitSearchExtract (
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_State                             varchar2,
    a_OriginalIssueDateFrom             date,
    a_OriginalIssueDateTo               date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_Permittee                         varchar2,
    a_Location                          varchar2,
    a_LocationDesc                      varchar2,
    a_County                            number,
    a_Municipality                      number,
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_LicenseType                       number,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_SolicitorName                     varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin

    t_MasterList := api.udt_objectlist();
    PermitSearch(a_PermitNumber, a_PermitType, a_State, a_OriginalIssueDateFrom,
        a_OriginalIssueDateTo, a_IssueDateFrom, a_IssueDateTo, a_EffectiveDateFrom,
        a_EffectiveDateTo, a_ExpirationDateFrom, a_ExpirationDateTo, a_Permittee,
        a_Location, a_LocationDesc, a_County, a_Municipality, a_LicenseNumber,
        a_Licensee, a_LicenseType, a_EventDateFrom, a_EventDateTo, a_SolicitorName,
        t_MasterList, 'Y');
    return t_MasterList;

  end PermitSearchExtract;

  -----------------------------------------------------------------------------
  -- License Lookup
  --  Look up Licenses based on entered criteria
  -----------------------------------------------------------------------------

  procedure LicenseLookup1(
    a_IsAmendable                       char,
    a_IsRenewable                       char,
    a_IsReinstatable                    char,
    a_IsInspectable                     char,
    a_IsAccusable                       char,
    a_LicenseNumber                     varchar2,
    a_LicenseType                       varchar2,
    a_State                             varchar2,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_Establishment                     varchar2,
    a_PhysicalAddress                   varchar2,
    a_Licensee                          varchar2,
    a_Region                            varchar2,
    a_Office                            varchar2,
    a_Restriction                       varchar2,
    a_CriminalConviction                varchar2,
    a_ConflictOfInterest                varchar2,
    a_ObjectList                        out nocopy api.udt_ObjectList
    ) is
      t_LicenseLength                  number;
    begin
      if a_LicenseNumber is null or
         length(a_LicenseNumber) < 3 then
         raise_application_error(-20000, 'Please enter at least 3 digits of the License Number');
      end if;

      LicenseSearch (a_LicenseNumber,
                     a_LicenseType,
                     a_State,
                     null,
                     null,
                     a_IssueDateFrom,
                     a_IssueDateTo,
                     a_EffectiveDateFrom,
                     a_EffectiveDateTo,
                     a_ExpirationDateFrom,
                     a_ExpirationDateTo,
                     a_InactivityStartDateFrom,
                     a_InactivityStartDateTo,
                     a_Establishment,
                     a_PhysicalAddress,
                     a_Licensee,
                     a_Region,
                     a_Office,
                     a_Restriction,
                     a_CriminalConviction,
                     a_ConflictOfInterest,
                     a_ObjectList,
                     'N',
                     a_IsAmendable,
                     a_IsRenewable,
                     a_IsReinstatable,
                     a_IsInspectable,
                     a_IsAccusable
                     );

  end LicenseLookup1;


  -----------------------------------------------------------------------------
  -- InactiveUserSearch
  --  Search for Inactive Users.
  -----------------------------------------------------------------------------
  procedure InactiveUserSearch(
    a_ToDate                            date,
    a_FromDate                          date,
    a_Objects                           out nocopy api.udt_ObjectList
  )is
    t_ObjectList                        udt_IdList;
  begin
    if a_ToDate is null or a_FromDate is null then
      raise_application_error(-20000, 'Please enter search criteria.');
    end if;

    if a_ToDate < a_FromDate then
      raise_application_error(-20000, 'Please ensure From date is prior to To date.');
    end if;

    a_Objects := api.udt_ObjectList();

    select    iu.userid bulk collect into t_ObjectList
    from      abc.internetusers iu
    join      query.u_users u on u.UserId = iu.userid
    where     iu.lastsuccessfullogin between trunc(a_FromDate) and trunc(a_ToDate+1)-interval '1' second
      and     u.Active = 'Y'
    and     u.UserType in ('Public', 'Municipal', 'Internal', 'Police');


    a_Objects.extend(t_ObjectList.count);
    for i in 1..t_ObjectList.count loop
        a_Objects(i) := api.udt_Object(t_ObjectList(i));
    end loop;


  end;
  ---------------------------------------------------------------------------------
  -- FilteredPermitLookup_TAP
  --  Search for Permits, filter by Permit Type selected on the given Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredPermitLookup_TAP(
    a_PermitNumber                    varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  ) is
    t_PermitTypeObjectId              number;
    t_PermitNumberLength              number;
    t_UnfilteredObjects               api.udt_objectlist;

  begin

    if a_PermitNumber is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    -- Get the objectid for PermitType TAP
    begin
      select objectid
        into t_PermitTypeObjectId
        from query.o_abc_permittype pt
       where pt.Code = 'TAP';
      exception
        when no_data_found then
           raise_application_error(-20000, 'No TAP Permit Type found. Contact your System Administrator.');
    end;
    /*
    -- Removed for now because our permit numbers are so low...should be re-introduces once we have a
    -- permit number pattern from the client

    t_PermitNumberLength := length(a_PermitNumber);

    if t_PermitNumberLength < 3 then
      raise_application_error(-20000, 'Please enter at least 3 digits of the Permit Number.');
    end if;
    */

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    -- Search for Permits by Permit Number
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Permit');
    /** Wildcard will return 2 and 20 when supplied with 2 we should eventually change this... **/
    extension.pkg_cxproceduralsearch.SearchByIndex('PermitNumber',a_PermitNumber,a_PermitNumber,true);
    extension.pkg_cxproceduralsearch.FilterObjects('IsActive', 'Y', 'Y', false);
    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

      -- Ensure Permits returned have the indicated Permit Type (related to the "Admin" permit type)
    for po in 1..t_UnfilteredObjects.Count loop
      if api.pkg_columnquery.Value(t_UnfilteredObjects(po).ObjectId, 'PermitTypeObjectId') = t_PermitTypeObjectId then
         a_Objects.Extend(1);
         -- add license to output
         a_Objects(a_Objects.count) := t_UnfilteredObjects(po);
      end if;
    end loop;

  end FilteredPermitLookup_TAP;

  ---------------------------------------------------------------------------------
  -- FilteredPermitLookup_Solicitor
  --  Search for Permits, filter by Permit Type = Solicitor and Active
  ---------------------------------------------------------------------------------
  procedure FilteredPermitLookup_Solicitor(
    a_PermitNumber                    varchar2,
    a_SolicitorName                   varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  ) is
    t_PermitTypeObjectId              number;
    t_PermitNumberLength              number;
    t_UnfilteredObjects               api.udt_objectlist;

  begin

    if a_PermitNumber is null and
       a_SolicitorName is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    -- Get the objectid for PermitType Solicitor
    begin
      select objectid
        into t_PermitTypeObjectId
        from query.o_abc_permittype pt
       where pt.Code = 'SOL';
      exception
        when no_data_found then
           raise_application_error(-20000, 'No Solicitor Permit Type found. Contact your System Administrator.');
    end;
    /*
    -- Removed for now because our permit numbers are so low...should be re-introduces once we have a
    -- permit number pattern from the client

    t_PermitNumberLength := length(a_PermitNumber);
    if t_PermitNumberLength < 3 then
      raise_application_error(-20000, 'Please enter at least 3 digits of the Permit Number.');
    end if;
    */

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    -- Search for Permits by Permit Number
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Permit');
    /** Wildcard will return 2 and 20 when supplied with 2 we should eventually change this... **/
    extension.pkg_cxproceduralsearch.SearchByIndex('PermitNumber',a_PermitNumber,a_PermitNumber,true);
    extension.pkg_CxProceduralSearch.SearchByRelatedIndex ('Solicitor', 'dup_FormattedName', a_SolicitorName, a_SolicitorName, true);
    extension.pkg_cxproceduralsearch.FilterObjects('IsActive', 'Y', 'Y', false);
    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');
      -- Ensure Permits returned have the indicated Permit Type (related to the "Admin" permit type)
    for po in 1..t_UnfilteredObjects.Count loop
      if api.pkg_columnquery.Value(t_UnfilteredObjects(po).ObjectId, 'PermitTypeObjectId') = t_PermitTypeObjectId then
         a_Objects.Extend(1);
         -- add license to output
         a_Objects(a_Objects.count) := t_UnfilteredObjects(po);
      end if;
    end loop;

  end FilteredPermitLookup_Solicitor;


  -------------------------------------
  -- Programmer : Michael F.
  -- Date       : 2015 Aug 07
  -- Purpose    : Search for Active CPL Data for Save as Excel.
  -- Updated    : Elijah R. 2015 Dec 30
  -- Notes      : Added search by Permit capability
  -------------------------------------
  function CPLDataSearchExtract(
    a_ProductName                      varchar2,
    a_ProductRegistrationNumber        varchar2,
    a_SKU                              varchar2,
    a_EffectiveFrom                    date,
    a_EffectiveTo                      date,
    a_LicenseNumber                    varchar2,
    a_Licensee                         varchar2,
    a_PermitNumber                     varchar2,
    a_Permittee                        varchar2,
    a_UnitVolume                       number,
    a_CloseOut                         varchar2,
    a_Combo                            varchar2,
    a_RIP                              varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    CPLDataSearch(
      a_ProductName,
      a_ProductRegistrationNumber,
      a_SKU,
      a_EffectiveFrom,
      a_EffectiveTo,
      a_LicenseNumber,
      a_Licensee,
      a_PermitNumber,
      a_Permittee,
      a_UnitVolume,
      a_CloseOut,
      a_Combo,
      a_RIP,
      t_MasterList,
      'Y' -- ExcelExtract
    );
    return t_MasterList;

  end CPLDataSearchExtract;

  -------------------------------------
  -- Programmer : Michael F.
  -- Date       : 2015 Aug 07
  -- Purpose    : Search for Active CPL Data
  -- Updated    : Elijah R. 2015 Dec 30
  -- Notes      : Added search by Permit capability
  -------------------------------------
  procedure CPLDataSearch(
    a_ProductName                      varchar2,
    a_ProductRegistrationNumber        varchar2,
    a_SKU                              varchar2,
    a_EffectiveFrom                    date,
    a_EffectiveTo                      date,
    a_LicenseNumber                    varchar2,
    a_Licensee                         varchar2,
    a_PermitNumber                     varchar2,
    a_Permittee                        varchar2,
    a_UnitVolume                       number,
    a_CloseOut                         varchar2,
    a_Combo                            varchar2,
    a_RIP                              varchar2,
    a_Objects                      out nocopy udt_ObjectList,
    a_ExcelExtract                     varchar2 default 'N'
  ) is
    t_SearchLimit                      number;
    t_UniquenessHelper                 extension.pkg_collectionutils.udt_ObjectTable;

    t_LicenseResults                   udt_ObjectList;
    t_LicenseCPLResults                udt_ObjectList;
    t_LicenseSearch                    boolean := a_Licensee is not null or a_LicenseNumber is not null;

    t_PermitResults                    udt_ObjectList;
    t_PermitCPLResults                 udt_ObjectList;
    t_PermitSearch                     boolean := a_Permittee is not null or a_PermitNumber is not null;
  begin
    select api.udt_Object(eo.objectid)
      bulk collect into a_Objects
      from abc.cpl_activedata_t cpl
      join api.registeredexternalobjects eo
        on eo.LinkValue = to_char(cpl.sequencenum)
      where (a_Combo is null or combopack = substr(a_Combo,1,1)) -- 'Y'es, or 'N'o
        and (a_RIP is null or rip = substr(a_RIP,1,1))
        and (a_Closeout is null or closeout = a_Closeout)
        and (a_SKU is null or cpl.sku = a_SKU)
        and (a_ProductRegistrationNumber is null or cpl.brandregistration = a_ProductRegistrationNumber)
        -- Only append a '%' to Product Name.  Performance is awful otherwise.
        --   Appending '%' is what seems to be expected... so that is good
        and (a_ProductName is null or lower(description) like lower(a_ProductName||'%'))
        and (a_EffectiveFrom is null or cpl.fromdate >= a_EffectiveFrom)
        and (a_EffectiveTo is null or cpl.todate <= a_EffectiveTo)
        and (a_UnitVolume is null or cpl.unitvolumeamount = a_UnitVolume)
        and eo.ObjectDefId = gc_CPLActiveDataObjectDef;

    pkg_debug.putsingleline(a_Objects.count || 'objects found');

    if t_LicenseSearch then
      -- License(e/Number) parameters supplied...
      -- Find all relevant licenses using supplied parameters
      abc.pkg_ABC_SearchProcedures.CPLInternalLicenseSelect(
        a_LicenseNumber => a_LicenseNumber,
        a_Licensee => a_Licensee,
        a_ObjectList => t_LicenseResults
      );
      pkg_debug.putsingleline(t_LicenseResults.count || 'License objects found');

      -- locate CPL submissions for the licenses identified
      select /*+cardinality(x, 50)*/
             api.udt_Object(eo.ObjectId)
        bulk collect into t_LicenseCPLResults
        from query.r_ABC_CPLLicense r
        join table(t_LicenseResults) x
          on x.objectid=r.LicenseObjectId
        join query.r_ABC_CPLSpreadsheet r2
          on r2.JobId = r.CPLJobId
        join abc.cpl_activedata_t cpl
          on cpl.cplsubmissiondocid = r2.DocumentId
        join api.registeredexternalobjects eo
          on eo.LinkValue = to_char(cpl.sequencenum);

      pkg_debug.putsingleline(t_LicenseCPLResults.count || 'CPL Data objects found');

      -- intersect found objects with those located by license
      extension.pkg_collectionutils.Join(a_Objects, t_LicenseCPLResults);
      pkg_debug.putsingleline(a_Objects.count || 'objects to return');

    end if; -- License(e/Number) search specifics

    if t_PermitSearch then
      -- Permitte(e/Number) parameters supplied...
      -- Find all relevant permits using supplied parameters
      abc.pkg_ABC_SearchProcedures.CPLInternalPermitSelect(
        a_PermitNumber => a_PermitNumber,
        a_Permittee => a_Permittee,
        a_ObjectList => t_PermitResults
      );
      pkg_debug.putsingleline(t_PermitResults.count || 'Permit objects found');

      -- locate CPL submissions for the permits identified
      select /*+cardinality(x, 50)*/
             api.udt_Object(eo.ObjectId)
        bulk collect into t_PermitCPLResults
        from query.r_ABC_CPLPermit r
        join table(t_PermitResults) x
          on x.objectid=r.PermitObjectId
        join query.r_ABC_CPLSpreadsheet r2
          on r2.JobId = r.CPLJobId
        join abc.cpl_activedata_t cpl
          on cpl.cplsubmissiondocid = r2.DocumentId
        join api.registeredexternalobjects eo
          on eo.LinkValue = to_char(cpl.sequencenum);

      pkg_debug.putsingleline(t_PermitCPLResults.count || 'CPL Data objects found');

      -- intersect found objects with those located by license
      extension.pkg_collectionutils.Join(a_Objects, t_PermitCPLResults);
      pkg_debug.putsingleline(a_Objects.count || 'objects to return');

    end if; -- Permitte(e/Number) search specifics

    --Check Search Limit
    t_SearchLimit  := OutriderSearchLimit();

    if a_Objects.Count > t_SearchLimit and nvl(a_ExcelExtract,'N') = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;
  end CPLDataSearch;

  -------------------------------------------
  -- Programmer : Iwasam Agube
  -- Date       : September-01-2015
  -- Purpose    : Search for Missed CPL Data
  -- NOTE       : SEARCH RESULTS LIMIT LEFT OUT OF THIS SEARCH AS RESULTS WILL NOT BE OVER 500
  -------------------------------------------
  procedure MissedCPLSearch(
    a_SubmissionPeriod             varchar2,
    a_Objects                      out nocopy udt_ObjectList,
    a_ExcelExtract                 varchar2 default 'N'
  ) is
    t_SearchLimit                  number;
    t_MasterLicenseIds             api.udt_ObjectList;
    t_LicenseIds                   api.udt_objectlist;
    t_ChildRecords                 api.udt_ObjectList;
    t_ExistingCPL                  api.udt_ObjectList;
    t_Objectids                    udt_ObjectList;

  begin
    pkg_debug.putsingleline('Missed CPL Search Entry');

    select api.udt_Object(y.ObjectId)
      bulk collect into t_MasterLicenseIds
      from (
      with HadCPLSubmission as (select distinct api.pkg_columnquery.value(l.LicenseObjectId, 'LicenseNumber') LicenseNumber
                                  from query.r_ABC_MasterLicenseLicense l
                                 where l.MasterLicenseObjectId = (select r2.MasterLicenseObjectId
                                                                    from query.r_Abc_Masterlicenselicense r2
                                                                   where r2.LicenseObjectId = l.LicenseObjectId)
                                   and exists (select 1
                                                 from query.r_ABC_CPLLicense r3
                                                where r3.LicenseObjectId = l.LicenseObjectId
                                                  and api.pkg_columnquery.value(r3.CPLJobId, 'PostingDateFor') = a_SubmissionPeriod))
      select distinct d.ObjectId
        from (select /*cardinality (lt, 1)*/ distinct l.MasterLicenseObjectId ObjectId
                from table(api.pkg_simplesearch.castableobjectsbyindex('o_ABC_LicenseType', 'CPLSubmission', 'Y')) lt
                join query.o_ABC_License l
                  on l.LicenseTypeObjectId = lt.objectid
                join table(api.pkg_simplesearch.castableobjectsbyindex('o_ABC_License', 'State', 'Active')) lt2  on lt2.objectid = l.ObjectId
               where l.CurrentCPLTypeIds is not null
               Minus
              select /*cardinality (sp, 1)*/ distinct api.pkg_columnquery.numericvalue(cp.LicenseObjectId, 'MasterLicenseObjectId') ObjectId
                from table(api.pkg_simplesearch.castableobjectsbyindex('o_ABC_CPLSubmissionPeriod', 'dup_SubmissionPeriod', a_SubmissionPeriod)) sp
                join query.r_ABC_CPLLicense cp on cp.CPLJobId = sp.objectid) d
       where api.pkg_ColumnQuery.Value(d.ObjectId, 'FirstLicenseNumber') not in (select LicenseNumber from HadCPLSubmission)) y;

     t_LicenseIds := api.udt_ObjectList();

     if t_MasterLicenseIds.Count() > 0 then
       for l in 1..t_MasterLicenseIds.Count() loop

         select api.udt_Object(r.LicenseObjectId)
           bulk collect into t_ChildRecords
           from query.r_Abc_Masterlicenselicense r
          where r.MasterLicenseObjectId = t_MasterLicenseIds(l).ObjectId;

          extension.pkg_CollectionUtils.Append(t_LicenseIds, t_ChildRecords);

       end loop;
     end if;

      a_objects := api.udt_objectlist();

      if t_LicenseIds.Count > 0 then
        for t in 1..t_LicenseIds.Count() loop
          if api.pkg_columnquery.value(t_LicenseIds(t).ObjectId, 'State') = 'Active' then
            a_objects.extend(1);
            a_Objects(a_Objects.Count()) := t_LicenseIds(t);
          end if;
        end loop;
      end if;

    pkg_debug.putsingleline(a_Objects.count || 'objects found');

  end MissedCPLSearch;

    ---------------------------------------------------------------------
  -- Programmer : Iwasam Agube
  -- Date       : September-02-2015
  -- Purpose    : Search for Licenses with Missed CPL for save as excel
  ---------------------------------------------------------------------
  function MissedCPLSearchExtract(
    a_SubmissionPeriod             varchar2
  ) return api.udt_ObjectList
  is

    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();



    MissedCPLSearch(
      a_SubmissionPeriod,
      t_MasterList,
      'Y' -- ExcelExtract
    );

    return t_MasterList;

  end MissedCPLSearchExtract;

 -----------------------------------------------------------------------------
  -- PermitRenewalLookup
  --  Lookup for Permits
  --  Updated by: Joshua Camps (CXUSA)
  --  Updated on: 09/15/15
  -----------------------------------------------------------------------------
  procedure PermitRenewalLookup(
    a_PermitNumber                varchar2,
    a_PermitType                  varchar2,
    a_State                       varchar2,
    a_IssueDateFrom               date,
    a_IssueDateTo                 date,
    a_EffectiveDateFrom           date,
    a_EffectiveDateTo             date,
    a_ExpirationDateFrom          date,
    a_ExpirationDateTo            date,
    a_Permittee                   varchar2,
    a_Objects                     out nocopy api.udt_Objectlist,
    a_ExcelExtract                varchar2 default 'N'
  ) is
    t_PermitNumber               varchar2(100);
    t_IssueDateFrom              date;
    t_IssueDateTo                date;
    t_EffectiveDateFrom          date;
    t_EffectiveDateTo            date;
    t_ExpirationDateFrom         date;
    t_ExpirationDateTo           date;
    t_EventDateFrom              date;
    t_EventDateTo                date;
    t_SearchLimit                number;
    t_SearchResults              udt_SearchResults;
    t_PermitDefName              varchar2(30) := 'o_ABC_Permit';
    t_BaseFromDate               date := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                 date := to_date('31-DEC-3999 23:59:59','DD-MON-YYYY HH24:MI:SS');
    t_PermitTypeSplit            udt_StringList;

  begin

    -- Initialize the collection
    a_Objects := api.udt_ObjectList();

    -- If no search values are entered, require some
    if a_PermitNumber is null
        and a_State is null
        and a_IssueDateFrom is null
        and a_IssueDateTo is null
        and a_EffectiveDateFrom is null
        and a_EffectiveDateTo is null
        and a_ExpirationDateFrom is null
        and a_ExpirationDateTo is null
        and a_Permittee is null then
      if a_PermitType is null then
        raise_application_error(-20000,'You must enter some search criteria.');
      else
        raise_application_error(-20000,'You must enter more search criteria.');
      end if;
    end if;

    --Set Begin Dates and End Dates if no date supplied and add timestamps to take the full day into account
    if a_IssueDateFrom is not null or a_IssueDateTo is not null then
      if a_IssueDateFrom is null then
        t_IssueDateFrom := t_BaseFromDate;
      else
        t_IssueDateFrom := to_date(to_char(a_IssueDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_IssueDateTo is null then
        t_IssueDateTo := t_BaseToDate;
      else
        t_IssueDateTo := to_date(to_char(a_IssueDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    if a_EffectiveDateFrom is not null or a_EffectiveDateTo is not null then
      if a_EffectiveDateFrom is null then
        t_EffectiveDateFrom := t_BaseFromDate;
      else
        t_EffectiveDateFrom := to_date(to_char(a_EffectiveDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EffectiveDateTo is null then
        t_EffectiveDateTo := t_BaseToDate;
      else
        t_EffectiveDateTo := to_date(to_char(a_EffectiveDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := t_BaseFromDate;
      else
        t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := t_BaseToDate;
      else
        t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    /* Begin Search Procedures */
    t_SearchResults := api.pkg_Search.NewSearchResults();

    -- Permit Number
    if a_PermitNumber is not null then
      if not api.pkg_Search.IntersectByColumnContents(t_PermitDefName, 'PermitNumber', a_PermitNumber, t_SearchResults) then
        return;
      end if;
    end if;

    -- Issue Date
    if t_IssueDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'IssueDate', t_IssueDateFrom, t_IssueDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    -- Effective Date
    if t_EffectiveDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'EffectiveDate', t_EffectiveDateFrom, t_EffectiveDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    -- Expiration Date
    if t_ExpirationDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'ExpirationDate', t_ExpirationDateFrom, t_ExpirationDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    -- State
    if a_State is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'State', a_State, t_SearchResults) then
        return;
      end if;
    end if;

    -- Permittee
    if a_Permittee is not null then
      if not api.pkg_Search.IntersectByColumnContents(t_PermitDefName, 'dup_PermitteeDisplay_SQL', a_Permittee, t_SearchResults) then
        return;
      end if;
    end if;

    -- Permit Type
    if a_PermitType is not null then

      -- Get the different permit types into a table which will be passed into the search.
      t_PermitTypeSplit := extension.pkg_Utils.Split(a_PermitType,', ');

      if not api.pkg_Search.IntersectByRelColumnValue(t_PermitDefName, 'PermitType', 'Name', t_PermitTypeSplit, t_SearchResults) then
        return;
      end if;
    end if;

    --Only return results that have a permit type that is Renewable
    if not api.Pkg_Search.IntersectByRelColumnValue(t_PermitDefName, 'PermitType', 'Renewable', 'Y', t_SearchResults) then
      return;
    end if;

    -- Filter out all results that are not renewable
    if not api.pkg_search.IntersectByColumnValue(t_PermitDefName, 'dup_IsRenewable', 'Y', t_SearchResults) then
      return;
    end if;

    -- Return (Insignia) Permits that were not cancelled by the system
    if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'SystemCancelled', 'N', t_SearchResults) then
       return;
    end if;

    -- Convert to udt_ObjectList
    a_Objects := extension.pkg_Utils.ConvertToObjectList(api.pkg_Search.SearchResultsToList(t_SearchResults));

    --Check Search Limit
    t_SearchLimit  := OutriderSearchLimit();
    if a_Objects.Count > t_SearchLimit and nvl(a_ExcelExtract,'N') = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end PermitRenewalLookup;
  
  /*---------------------------------------------------------------------------
   * PermitLookup() -- PUBLIC
   *   General Lookup script for permits with no conditions
   *-------------------------------------------------------------------------*/
  procedure PermitLookup(
    a_PermitNumber                varchar2,
    a_PermitType                  varchar2,
    a_State                       varchar2,
    a_IssueDateFrom               date,
    a_IssueDateTo                 date,
    a_EffectiveDateFrom           date,
    a_EffectiveDateTo             date,
    a_ExpirationDateFrom          date,
    a_ExpirationDateTo            date,
    a_Permittee                   varchar2,
    a_Objects                     out nocopy api.udt_Objectlist,
    a_TapOnly                     varchar2 default 'N',
    a_ExcelExtract                varchar2 default 'N'
  ) is
    t_PermitNumber               varchar2(100);
    t_IssueDateFrom              date;
    t_IssueDateTo                date;
    t_EffectiveDateFrom          date;
    t_EffectiveDateTo            date;
    t_ExpirationDateFrom         date;
    t_ExpirationDateTo           date;
    t_EventDateFrom              date;
    t_EventDateTo                date;
    t_SearchLimit                number;
    t_SearchResults              udt_SearchResults;
    t_PermitDefName              varchar2(30) := 'o_ABC_Permit';
    t_BaseFromDate               date := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                 date := to_date('31-DEC-3999 23:59:59','DD-MON-YYYY HH24:MI:SS');
    t_PermitTypeSplit            udt_StringList;

  begin

    -- Initialize the collection
    a_Objects := api.udt_ObjectList();

    -- If no search values are entered, require some
    if a_PermitNumber is null
        and a_State is null
        and a_IssueDateFrom is null
        and a_IssueDateTo is null
        and a_EffectiveDateFrom is null
        and a_EffectiveDateTo is null
        and a_ExpirationDateFrom is null
        and a_ExpirationDateTo is null
        and a_Permittee is null then
      if a_PermitType is null then
        raise_application_error(-20000,'You must enter some search criteria.');
      else
        raise_application_error(-20000,'You must enter more search criteria.');
      end if;
    end if;

    --Set Begin Dates and End Dates if no date supplied and add timestamps to take the full day into account
    if a_IssueDateFrom is not null or a_IssueDateTo is not null then
      if a_IssueDateFrom is null then
        t_IssueDateFrom := t_BaseFromDate;
      else
        t_IssueDateFrom := to_date(to_char(a_IssueDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_IssueDateTo is null then
        t_IssueDateTo := t_BaseToDate;
      else
        t_IssueDateTo := to_date(to_char(a_IssueDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    if a_EffectiveDateFrom is not null or a_EffectiveDateTo is not null then
      if a_EffectiveDateFrom is null then
        t_EffectiveDateFrom := t_BaseFromDate;
      else
        t_EffectiveDateFrom := to_date(to_char(a_EffectiveDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EffectiveDateTo is null then
        t_EffectiveDateTo := t_BaseToDate;
      else
        t_EffectiveDateTo := to_date(to_char(a_EffectiveDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if a_ExpirationDateFrom is null then
        t_ExpirationDateFrom := t_BaseFromDate;
      else
        t_ExpirationDateFrom := to_date(to_char(a_ExpirationDateFrom, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_ExpirationDateTo is null then
        t_ExpirationDateTo := t_BaseToDate;
      else
        t_ExpirationDateTo := to_date(to_char(a_ExpirationDateTo, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    /* Begin Search Procedures */
    t_SearchResults := api.pkg_Search.NewSearchResults();

    -- Permit Number
    if a_PermitNumber is not null then
      if not api.pkg_Search.IntersectByColumnContents(t_PermitDefName, 'PermitNumber', a_PermitNumber, t_SearchResults) then
        return;
      end if;
    end if;

    -- Issue Date
    if t_IssueDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'IssueDate', t_IssueDateFrom, t_IssueDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    -- Effective Date
    if t_EffectiveDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'EffectiveDate', t_EffectiveDateFrom, t_EffectiveDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    -- Expiration Date
    if t_ExpirationDateFrom is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'ExpirationDate', t_ExpirationDateFrom, t_ExpirationDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    -- State
    if a_State is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_PermitDefName, 'State', a_State, t_SearchResults) then
        return;
      end if;
    end if;

    -- Permittee
    if a_Permittee is not null then
      if not api.pkg_Search.IntersectByColumnContents(t_PermitDefName, 'dup_PermitteeDisplay_SQL', a_Permittee, t_SearchResults) then
        return;
      end if;
    end if;

    -- Permit Type
    if a_PermitType is not null then

      -- Get the different permit types into a table which will be passed into the search.
      t_PermitTypeSplit := extension.pkg_Utils.Split(a_PermitType,', ');

      if not api.pkg_Search.IntersectByRelColumnValue(t_PermitDefName, 'PermitType', 'Name', t_PermitTypeSplit, t_SearchResults) then
        return;
      end if;
    end if;
    
    if a_TapOnly = 'Y' then 
      if not api.pkg_Search.IntersectByRelColumnValue(t_PermitDefName, 'PermitType', 'Code', 'TAP', t_SearchResults) then
        return;
      end if;
    end if;

    -- Convert to udt_ObjectList
    a_Objects := extension.pkg_Utils.ConvertToObjectList(api.pkg_Search.SearchResultsToList(t_SearchResults));

    --Check Search Limit
    t_SearchLimit  := OutriderSearchLimit();
    if a_Objects.Count > t_SearchLimit and nvl(a_ExcelExtract,'N') = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end PermitLookup;
  
  /*---------------------------------------------------------------------------
   * PermitLookupTap() -- PUBLIC
   *   General Lookup script for Tap Permits
   *-------------------------------------------------------------------------*/
  procedure PermitLookupTap(
    a_PermitNumber                varchar2,
    a_PermitType                  varchar2,
    a_State                       varchar2,
    a_IssueDateFrom               date,
    a_IssueDateTo                 date,
    a_EffectiveDateFrom           date,
    a_EffectiveDateTo             date,
    a_ExpirationDateFrom          date,
    a_ExpirationDateTo            date,
    a_Permittee                   varchar2,
    a_Objects                     out nocopy api.udt_Objectlist
  ) is
  begin
    -- Initialize the collection
    a_Objects := api.udt_ObjectList();
    
    PermitLookup(a_PermitNumber, a_PermitType, a_State, a_IssueDateFrom, 
      a_IssueDateTo, a_EffectiveDateFrom, a_EffectiveDateTo, a_ExpirationDateFrom, 
      a_ExpirationDateTo, a_Permittee, a_Objects, 'Y', 'N');
      
  end PermitLookupTap;

  ---------------------------------------------------------------------------------
  -- FilteredRelatedLicenseLookup
  --  Search for Licenses, filter by License Type that allow Additional Warehouse
  ---------------------------------------------------------------------------------
  procedure FilteredRelatedLicenseLookup(
    a_LicenseNumber                   varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  ) is
    t_LicenseLength                   number;
    t_UnfilteredObjects               api.udt_objectlist;

    cursor c_LicenseTypes is
      select lt.objectid LicenseTypeObjectId
        from query.o_abc_licensetype lt
       where lt.ValidForAdditionalWarehouseLic = 'Y';

  begin

    if a_LicenseNumber is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    t_LicenseLength := length(a_LicenseNumber);

    if t_LicenseLength < 3 then
      raise_application_error(-20000, 'Please enter at least 3 digits of the License Number.');
    end if;


    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    -- Search for Licenses by License Number
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_License');
    extension.pkg_cxproceduralsearch.SearchByIndex('LicenseNumber',a_LicenseNumber,a_LicenseNumber,true);
    extension.pkg_cxproceduralsearch.FilterObjects('IsActive', 'Y', 'Y', false);
    extension.pkg_cxproceduralsearch.FilterObjects('ValidForAdditionalWarehouse', 'Y', 'Y', False);
    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    for lt in c_LicenseTypes loop
      -- Ensure License returned has the selected License Type
      for lo in 1..t_UnfilteredObjects.Count loop
        if api.pkg_columnquery.Value(t_UnfilteredObjects(lo).ObjectId, 'LicenseTypeObjectId') = lt.licensetypeobjectid then
          a_Objects.Extend(1);
          -- add license to output
          a_Objects(a_Objects.count) := t_UnfilteredObjects(lo);
        end if;
      end loop;
    end loop;

  end FilteredRelatedLicenseLookup;

  -----------------------------------------------------------------------------
  -- PublicDistributorProductSearch
  --  Search for Distributor Products on the Public website.
  -----------------------------------------------------------------------------
  procedure PublicDistributorProductSearch(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_UserId                            number,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is

    t_Count                        number;
    t_IsDashboardSearch            boolean := true;
    t_Length                       number;
    t_ProductSearchRetValLimit     number;
    t_ProductIdList                udt_StringList;
    t_UnfilteredObjects               api.udt_ObjectList;

  begin

    -- Raise error if no search criteria is entered
    if a_DistributorLicenseNumber is null then
       raise_application_error(-20000, 'You must enter a Distributor License Number.');
    end if;

    if a_RegistrationNumber is null
    and a_Name is null
    and a_Type is null
    and a_Registrant is null
    and a_State is null
    and a_DistributorLicenseNumber is null
    and a_DistributorName is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- ProductSearchReturnValueLimit
        select o.ProductSearchReturnValueLimit
          into t_ProductSearchRetValLimit
          from query.o_SystemSettings o
         where o.ObjectDefTypeId = 1
           and rownum = 1;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Product');

    extension.pkg_cxproceduralsearch.SearchByIndex('RegistrationNumber',a_RegistrationNumber,a_RegistrationNumber, true);
    extension.pkg_cxproceduralsearch.SearchByIndex('Name',a_Name,a_Name,true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('ProductType','ObjectId',a_Type, a_Type, false);
    extension.pkg_cxproceduralsearch.SearchByIndex('State', a_State,a_State,false);

    if a_Registrant is not null then
     t_Length := length(replace(a_Registrant, '%'));
      if t_Length < 3  or t_Length is null
        then raise_application_error(-20000, 'Please enter at least 3 characters of the Registrant Name.');
       else extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LERegistrant','dup_FormattedName',a_Registrant, a_Registrant, true);
      end if;
    end if;

    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('DistributorLic','LicenseNumber',a_DistributorLicenseNumber, a_DistributorLicenseNumber, true);
    extension.pkg_cxproceduralsearch.SearchByRelatedIndex('LEDistName','dup_FormattedName',a_DistributorName, a_DistributorName, true);

    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

    select api.udt_Object(ol.ObjectId) bulk collect into a_Objects
    from   table(cast(t_UnfilteredObjects as api.udt_ObjectList)) ol;

    if a_Objects.Count > t_ProductSearchRetValLimit and
       a_ExcelExtract = 'N' then
      raise_application_error(-20000, 'Your search returned too many results ( >' || t_ProductSearchRetValLimit || '). Please refine your search, or select "Save to Excel".');
    end if;

  end PublicDistributorProductSearch;

  ---------------------------------------------------------------------------------
  -- FilteredPermitLookup_Insignia
  --  Search for Insignia Permits, filter by Permit Type
  ---------------------------------------------------------------------------------
  procedure FilteredPermitLookup_Insignia(
    a_PermitTypeObjectId              number,
    a_PermitNumber                    varchar2,
    a_Objects                         out nocopy api.udt_ObjectList
  ) is
    t_PermitTypeObjectId              number;
    t_PermitNumberLength              number;
    t_UnfilteredObjects               api.udt_objectlist;

  begin

    t_PermitTypeObjectId := a_PermitTypeObjectId;

    /*
    -- Removed for now because our permit numbers are so low...should be re-introduces once we have a
    -- permit number pattern from the client

    t_PermitNumberLength := length(a_PermitNumber);

    if t_PermitNumberLength < 3 then
      raise_application_error(-20000, 'Please enter at least 3 digits of the Permit Number.');
    end if;
    */

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();

    -- Search for Permits by Permit Number
    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_Permit');
    /** Wildcard will return 2 and 20 when supplied with 2 we should eventually change this... **/
    extension.pkg_cxproceduralsearch.SearchByIndex('PermitNumber',a_PermitNumber,a_PermitNumber,true);
    extension.pkg_cxproceduralsearch.FilterObjects('IsActive', 'Y', 'Y', false);
    extension.pkg_cxproceduralsearch.PerformSearch(t_UnfilteredObjects, 'and', 'none');

      -- Ensure Permits returned have the indicated Permit Type
    for po in 1..t_UnfilteredObjects.Count loop
      if api.pkg_columnquery.Value(t_UnfilteredObjects(po).ObjectId, 'PermitTypeObjectId') = t_PermitTypeObjectId then
         a_Objects.Extend(1);
         -- add Permit to output
         a_Objects(a_Objects.count) := t_UnfilteredObjects(po);
      end if;
    end loop;

  end FilteredPermitLookup_Insignia;

  -----------------------------------------------------------------------------
  -- PublicProductSearchExtract
  --  Search for Products for Save as Excel
  -----------------------------------------------------------------------------
  function PublicProductSearchExtract(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_UserId                            number
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    PublicProductSearch(a_RegistrationNumber, a_Name, a_Type, a_Registrant, a_State, a_DistributorLicenseNumber, a_DistributorName, a_UserId, t_MasterList, 'Y');
    return t_MasterList;

  end PublicProductSearchExtract;

  -----------------------------------------------------------------------------
  -- PublicDistributorSearchExtract
  --  Search for Distributed Products for Save as Excel
  -----------------------------------------------------------------------------
  function PublicDistributorSearchExtract(
    a_RegistrationNumber                varchar2,
    a_Name                              varchar2,
    a_Type                              varchar2,
    a_Registrant                        varchar2,
    a_State                             varchar2,
    a_DistributorLicenseNumber          varchar2,
    a_DistributorName                   varchar2,
    a_UserId                            number
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    PublicDistributorProductSearch(a_RegistrationNumber, a_Name, a_Type, a_Registrant, a_State, a_DistributorLicenseNumber, a_DistributorName, a_UserId, t_MasterList, 'Y');
    return t_MasterList;

  end PublicDistributorSearchExtract;

  -----------------------------------------------------------------------------
  -- Legal Entity Search for Public User
  --  Search for Legal Entities to associate with a Public User
  -----------------------------------------------------------------------------
  procedure LegalEntitySearchPubUser(
    a_LegalName                         varchar2,
    a_LastName                          varchar2,
    a_FirstName                         varchar2,
    a_LegalEntityTypeObjectId           udt_id,
    a_CorporationNumber                 varchar2,
    a_MailingAddress                    varchar2,
    a_PhysicalAddress                   varchar2,
    a_IncludeHistory                    char,
    a_IsBrandRegistrant                 char,
    a_IsPermittee                       char,
    a_SSNTIN                            varchar2,
    a_DriversLicense                    varchar2,
    a_ContactEmail                      varchar2,
    a_PublicUserId                      udt_id,
    a_Objects                           out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_LegalEntityTypeObjectId           udt_id :=
      api.pkg_simplesearch.ObjectByIndex('o_ABC_LegalEntityType', 'Name', 'Individual');
    t_MasterList                        api.udt_ObjectList;
    t_UnfilteredObjects                 api.udt_ObjectList;
    t_AllObjects                        udt_IdList;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
    t_SSNEncrypted                      varchar2(4000);
    t_DriversLicenseEncrypted           varchar2(4000);
    t_Index                             number := 0;
    t                                   number;

  begin
    -- Raise error if no search criteria is entered
    if  a_LegalName is null
      and a_LastName is null
      and a_FirstName is null
      and a_LegalEntityTypeObjectId is null
      and a_CorporationNumber is null
      and a_MailingAddress is null
      and a_PhysicalAddress is null
      and a_SSNTIN is null
      and a_DriversLicense is null
      and a_ContactEmail is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    -- Raise error if criteria is entered for an individual and a non-individual entity type
    if (a_LegalName is not null
       or a_CorporationNumber is not null
       or a_LegalEntityTypeObjectId <> t_LegalEntityTypeObjectId)
     and (a_LastName is not null
       or a_FirstName is not null
       or a_LegalEntityTypeObjectId = t_LegalEntityTypeObjectId) then
        raise_application_error(-20000, 'If you are searching for an Individual please do
          not enter criteria for Legal Name, Doing Business As or Corporation Number. Otherwise, please do not
          enter criteria for Last Name or First Name.');
    end if;

    a_Objects := api.udt_ObjectList();
    t_UnfilteredObjects := api.udt_ObjectList();
    t_MasterList := api.udt_ObjectList();

    extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_LegalEntity');

     extension.pkg_cxProceduralSearch.SearchByIndex('LegalName', a_LegalName, a_LegalName, true);
     extension.pkg_cxProceduralSearch.SearchByIndex('LastName', a_LastName, a_LastName, true);
     extension.pkg_cxProceduralSearch.SearchByIndex('FirstName', a_FirstName, a_FirstName, true);
     extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityType', 'ObjectId', a_LegalEntityTypeObjectId, a_LegalEntityTypeObjectId, true);
     extension.pkg_cxProceduralSearch.SearchByIndex('CorporationNumber', a_CorporationNumber, a_CorporationNumber, true);
     extension.pkg_cxProceduralSearch.SearchByRelatedIndex('MailingAddress', 'dup_FormattedDisplay', a_MailingAddress, a_MailingAddress, true);
     extension.pkg_cxProceduralSearch.SearchByRelatedIndex('PhysicalAddress', 'dup_FormattedDisplay', a_PhysicalAddress, a_PhysicalAddress, true);
     extension.pkg_cxProceduralSearch.SearchByIndex('ContactEmailAddress', a_ContactEmail, a_ContactEmail, true);
     if a_SSNTIN is not null then
       if length(a_SSNTIN) = 9 and regexp_replace(a_SSNTIN, '[0-9]*') is null then
         t_SSNEncrypted := abc.pkg_abc_Encryption.Encrypt(a_SSNTIN);
         extension.pkg_cxProceduralSearch.SearchByIndex('SSNTINEncrypted', t_SSNEncrypted, t_SSNEncrypted, false);
       else
         raise_application_error(-20000, 'If searching by SSN/TIN/EIN you must enter exactly 9 digits.');
       end if;
     end if;
     if a_DriversLicense is not null then
        t_DriversLicenseEncrypted := abc.pkg_abc_Encryption.Encrypt(a_DriversLicense);
        extension.pkg_cxProceduralSearch.SearchByIndex('DriversLicenseEncrypted', t_DriversLicenseEncrypted, t_DriversLicenseEncrypted, false);
     end if;


     if a_IncludeHistory = 'N' or
        a_IncludeHistory is null then
        extension.pkg_CxProceduralSearch.FilterObjects ('Active', 'Y', null, False);
     end if;

   extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');

   extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);
   t_UnfilteredObjects.delete;

   -- Searching related Legal Entity History objects if the Include History checkbox is selected
   if a_IncludeHistory = 'Y' then
     extension.pkg_cxProceduralSearch.InitializeSearch('o_ABC_LegalEntity');

     if a_LegalName <> '%' then
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'LegalName', a_LegalName, a_LegalName, true);
     end if;
     if a_LastName <> '%' then
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'LastName', a_LastName, a_LastName, true);
     end if;
     if a_FirstName <> '%' then
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'FirstName', a_FirstName, a_FirstName, true);
     end if;
     if a_CorporationNumber <> '%' then
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'CorporationNumber', a_CorporationNumber, a_CorporationNumber, true);
     end if;
     if a_SSNTIN is not null then
       t_SSNEncrypted := abc.pkg_abc_Encryption.Encrypt(a_SSNTIN);
       extension.pkg_cxProceduralSearch.SearchByRelatedIndex('LegalEntityHistory', 'SSNTINEncrypted', t_SSNEncrypted, t_SSNEncrypted, false);
     end if;

       extension.pkg_cxProceduralSearch.PerformSearch(t_UnfilteredObjects, 'and');
   end if;

   extension.pkg_CollectionUtils.Append(t_MasterList, t_UnfilteredObjects);

   t_UnfilteredObjects.delete;
   t_UnfilteredObjects := api.udt_ObjectList();
   select *
   bulk   collect into t_allobjects
   from   table(cast(t_MasterList as api.udt_objectlist)) ob;
   for x in 1..t_AllObjects.count loop
      begin
         select 1
         into   t
         from   query.r_ABC_UserLegalEntity r
         where  r.UserId = a_PublicUserId
         and    r.LegalEntityObjectId = t_AllObjects(x);
      exception
         when no_data_found then
            t_UnfilteredObjects.Extend;
            t_index := t_Index + 1;
            t_UnfilteredObjects(t_Index) := api.udt_Object(t_AllObjects(x));
      end;
   end loop;

   a_Objects := t_UnfilteredObjects;
    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;
  end LegalEntitySearchPubUser;

  /*---------------------------------------------------------------------------
   * PoliceUserSearch() -- PUBLIC
   *   Search for Police Users
   *-------------------------------------------------------------------------*/
  procedure PoliceUserSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_EmailAddress                      varchar2,
    a_PhoneNumber                       varchar2,
    a_BusinessPhone                     varchar2,
    a_Municipality                      varchar2,
    a_UserType                          varchar2,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
  begin
    -- Check to make sure at least one criteria has been entered.
    if a_FirstName is null
        and a_LastName is null
        and a_EmailAddress is null
        and a_PhoneNumber is null
        and a_BusinessPhone is null
        and a_Municipality is null
    then
        api.pkg_Errors.RaiseError(-20000, 'You must enter some search criteria.');
    end if;

    UserExternalSearch(a_FirstName, a_LastName, a_EmailAddress, a_PhoneNumber,
        a_BusinessPhone, a_UserType, a_Municipality, null, null,
        null, a_Objects);

  end PoliceUserSearch;

  /*---------------------------------------------------------------------------
   * ActiveDistributorLicenses
   * Search for active Distributor Licenses, filtered by Licensee
   *-------------------------------------------------------------------------*/
  procedure ActiveDistributorLicenses(
    a_LicenseNumber                     varchar2,
    a_Licensee                          udt_id,
    a_Objects                           out nocopy api.udt_objectlist
  ) is
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_Length                            integer;

  begin
    t_Length := length(replace(a_LicenseNumber, '%'));
    if t_Length < 3  or t_Length is null then
      raise_application_error(-20000, 'Please enter at least 3 characters ' ||
         'of the License Number');
    end if;

    a_Objects := api.udt_objectlist();
    t_SearchResults := api.pkg_Search.NewSearchResults();
    if a_Licensee is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'Licensee',
          'ObjectId', a_Licensee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_LicenseNumber is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_License', 'LicenseNumber',
          a_LicenseNumber, t_SearchResults) then
        return;
      end if;
    end if;
    if not api.pkg_Search.IntersectByColumnValue('o_ABC_License',
          'IsValidAsRegistrantDistributor', 'Y', t_SearchResults) then
      return;
    end if;
    if not api.pkg_Search.IntersectByColumnValue('o_ABC_License', 'IsActive',
        'Y', t_SearchResults) then
      return;
    end if;
    a_Objects := SearchResultsToObjList(t_SearchResults);

  end ActiveDistributorLicenses;

  /*---------------------------------------------------------------------------
   * FilteredVehicleSearch()
   *   Used on the Vehicles tab on Legal Entity.
   * Search for vehicles related to the Legal Entity based on the criteria
   * entered on the Vehicles tab
   *-------------------------------------------------------------------------*/
  procedure FilteredVehicleSearch(
    a_MakeModelYear                     varchar2,
    a_StateRegistration                 varchar2,
    a_StateOfRegistration               varchar2,
    a_VIN                               varchar2,
    a_OwnedOrLeasedLimousine            varchar2,
    a_InsigniaNumber                    varchar2,
    a_PermitNumber                      varchar2,
    a_PermitTypeObjectId                udt_Id,
    a_PermitState                       varchar2,
    a_SourceObjectId                    udt_Id,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_FinalList                         udt_IdList;
  begin

    a_ObjectList := api.udt_ObjectList();
    if a_MakeModelYear is null
        and a_StateRegistration is null
        and a_StateOfRegistration is null
        and a_VIN is null
        and a_OwnedOrLeasedLimousine is null
        and a_InsigniaNumber is null
        and a_PermitNumber is null
        and a_PermitTypeObjectId is null
        and a_PermitState is null then
        api.pkg_errors.raiseerror(-20000, 'You must enter some search criteria.');
    end if;
    if a_SourceObjectId is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_Vehicle',
          'LegalEntity', 'ObjectId', a_SourceObjectId, t_SearchResults) then
        return;
      end if;
    end if;
    if a_MakeModelYear is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Vehicle',
          'MakeModelYear', a_MakeModelYear, t_SearchResults) then
        return;
      end if;
    end if;
    if a_StateRegistration is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Vehicle',
          'StateRegistration', a_StateRegistration, t_SearchResults) then
        return;
      end if;
    end if;
    if a_StateOfRegistration is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Vehicle',
          'StateOfRegistration', a_StateOfRegistration, t_SearchResults) then
        return;
      end if;
    end if;
    if a_VIN is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Vehicle',
          'VIN', a_VIN, t_SearchResults) then
        return;
      end if;
    end if;
    if a_OwnedOrLeasedLimousine is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Vehicle',
          'OwnedOrLeasedLimousine', a_OwnedOrLeasedLimousine, t_SearchResults) then
        return;
      end if;
    end if;
    if a_InsigniaNumber is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Vehicle',
          'InsigniaNumber', a_InsigniaNumber, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitNumber is not null then
      if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_Vehicle',
          'Permit', 'PermitNumber', a_PermitNumber, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitTypeObjectId is not null then
      if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_Vehicle',
          'Permit', 'PermitTypeObjectId', a_PermitTypeObjectId, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitState is not null then
      if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_Vehicle',
          'Permit', 'State', a_PermitState, t_SearchResults) then
        return;
      end if;
    end if;
    -- Ensure there are no results from system cancelled permits
    if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_Vehicle',
        'Permit', 'SystemCancelled', 'N', t_SearchResults) then
      return;
    end if;
    t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);

    -- Filter out vehicles based on permit state, only needed if permit state
    -- wasn't specified as criteria
    for a in 1..t_FinalList.Count loop
      if api.pkg_columnquery.value(t_FinalList(a), 'PermitState') in
          ('Active', 'Expired', 'Closed', 'Cancelled', 'Pending') then
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_FinalList(a));
      end if;
    end loop;

  end FilteredVehicleSearch;

  /*---------------------------------------------------------------------------
   * DocumentTypesForPublicMenu() -- PUBLIC
   *   Used on the Entry Point Document Templates, on the public site.
   *-------------------------------------------------------------------------*/
  procedure DocumentTypesForPublicMenu (
    a_ObjectList                        out nocopy udt_ObjectList
  ) is
    t_Objects                           udt_IdList;
    t_SearchResults                     api.pkg_Search.udt_SearchResults;
  begin

    a_ObjectList := api.udt_ObjectList();
    -- Retrieve all of the Document Types where AvailableOnline = 'Y'
        --and Active = 'Y'
    if not api.pkg_Search.IntersectByColumnValue('o_ABC_DocumentType', 'Active',
        'Y', t_SearchResults) then
      return;
    end if;
    if not api.pkg_Search.IntersectByColumnValue('o_ABC_DocumentType',
        'AvailableOnline', 'Y', t_SearchResults) then
      return;
    end if;

    t_Objects := api.pkg_Search.SearchResultsToList(t_SearchResults);

    for a in 1..t_Objects.count loop
      a_ObjectList.Extend(1);
      a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_Objects(a));
    end loop;

  end DocumentTypesForPublicMenu;

  /*---------------------------------------------------------------------------
   * LicenseJobSearch() -- PUBLIC
   *   Search for License Jobs only
   *-------------------------------------------------------------------------*/
  procedure LicenseJobSearch(
    a_ObjectList                        out nocopy api.udt_ObjectList
  ) is
    t_idlist                            api.pkg_Definition.udt_idlist;
  begin

    select j.ObjectId
    bulk collect into t_idlist
    from query.o_Jobtypes j
    where j.Name in ('j_ABC_NewApplication', 'j_ABC_RenewalApplication',
        'j_ABC_AmendmentApplication');
    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);

  end LicenseJobSearch;

  /*---------------------------------------------------------------------------
   * FindPermitAppsForPolice() -- PRIVATE
   *  Called by abc.pkg_ABC_SearchProcedures.PolicePermitAppSearch(). Search
   * for Permit Applications from the Police portal
   *-------------------------------------------------------------------------*/
  procedure FindPermitAppsForPolice (
    a_FileNumber                        varchar2,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_ApplicationType                   varchar2,
    a_Permittee                         varchar2,
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_TradeName                         varchar2,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_BaseFromDate                      date
        := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                        date
        := to_date('31-DEC-3999 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    t_EventDateFrom                     date;
    t_EventDateTo                       date;
    t_FileNum                           varchar2(4000) := a_FileNumber;
    t_FinalList                         udt_IdList;
    t_Licensee                          varchar2(4000) := a_Licensee;
    t_LicenseNumber                     varchar2(4000) := a_LicenseNumber;
    t_MuniIds                           udt_IdList;
    t_PermitNum                         varchar2(4000) := a_PermitNumber;
    t_PermitObjectIds                   udt_IdList;
    t_PermitSearchResults               api.pkg_Definition.udt_SearchResults;
    t_Permittee                         varchar2(4000) := a_Permittee;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_TradeName                         varchar2(4000) := a_TradeName;
    t_UserId                            udt_Id;
  begin

    a_Objects := api.udt_ObjectList();
    -- Find which municipalities the user is in and only get review processes
    --  for those municipalities
    t_UserId := api.pkg_SecurityQuery.EffectiveUserId;
    t_MuniIds := api.pkg_search.ObjectsByRelColumnValue('o_ABC_Office', 'PoliceUser',
        'ObjectId', t_UserId);
    if not api.pkg_search.IntersectByRelObjectIds('p_ABC_PoliceReview',
        'NewApplicationMunicipality', t_MuniIds, t_SearchResults) then
      return;
    end if;
    -- Search by the entered fields
    if a_FileNumber is not null then
      if substr(t_FileNum, Length(t_FileNum)) != '%' then
        t_FileNum := t_FileNum || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'PermitApplicationExt',
          'ExternalFileNum', t_FileNum, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitNumber is not null then
      if substr(t_PermitNum, Length(t_PermitNum)) != '%' then
        t_PermitNum := t_PermitNum || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'ApplicationPermit',
          'PermitNumber', t_PermitNum, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitType is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'ApplicationPermit',
          'PermitTypeObjectId', a_PermitType, t_SearchResults) then
        return;
      end if;
    end if;
    if a_ApplicationType is not null then
      if not api.pkg_Search.IntersectByColumnContents('p_ABC_PoliceReview',
          'JobTypeDescription', a_ApplicationType, t_SearchResults) then
        return;
      end if;
    end if;
    if a_Permittee is not null then
      if substr(t_Permittee, Length(t_Permittee)) != '%' then
        t_Permittee := t_Permittee || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'ApplicationPermit',
          'dup_PermitteeDisplay_SQL', t_Permittee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_Licensee is not null then
      if substr(t_Licensee, Length(t_Licensee)) != '%' then
        t_Licensee := t_Licensee || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'ApplicationLicensee',
          'dup_FormattedName', t_Licensee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_LicenseNumber is not null then
      if substr(t_LicenseNumber, Length(t_LicenseNumber)) != '%' then
        t_LicenseNumber := t_LicenseNumber || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'ApplicationLicense',
          'LicenseNumber', t_LicenseNumber, t_SearchResults) then
        return;
      end if;
    end if;
    if a_TradeName is not null then
      if substr(t_TradeName, Length(t_TradeName)) != '%' then
        t_TradeName := t_TradeName || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview',
          'ApplicationEstablishment', 'DoingBusinessAs', t_TradeName, t_SearchResults) then
        return;
      end if;
    end if;
    -- Search by event date
    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if a_EventDateFrom is null then
        t_EventDateFrom := t_BaseFromDate;
      else
        t_EventDateFrom := to_date(
            to_char(a_EventDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EventDateTo is null then
        t_EventDateTo := t_BaseToDate;
      else
        t_EventDateTo := to_date(
            to_char(a_EventDateTo, 'DD-MON-YYYY') || ' 23:59:59',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;
    -- Intersect permits that have an event date that falls within the bounds entered
    -- First, grab permits that have an event within the entered range
    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if api.pkg_search.IntersectByRelColumnValue('o_ABC_Permit', 'EventDate', 'StartDate',
          a_EventDateFrom, a_EventDateTo, t_PermitSearchResults) then
        t_PermitObjectIds := api.pkg_Search.SearchResultsToList(t_PermitSearchResults);
      else
        return;
      end if;
    end if;
    -- Search for Police Review processes related to the returned permit ids
    if t_PermitObjectIds.count != 0 then
      if not api.pkg_search.IntersectByRelObjectIds('p_ABC_PoliceReview', 'ApplicationPermit',
          t_PermitObjectIds, t_SearchResults) then
        return;
      end if;
    end if;
    t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);
    for a in 1..t_FinalList.Count loop
      a_Objects.Extend(1);
      a_Objects(a_Objects.Count) := api.udt_Object(t_FinalList(a));
    end loop;

  end FindPermitAppsForPolice;

  /*---------------------------------------------------------------------------
   * FindPermitRenewalsForPolice() -- PRIVATE
   *   Called by abc.pkg_ABC_SearchProcedures.PolicePermitAppSearch(). Search
   * for Permit Renewals from the Police Portal.
   *-------------------------------------------------------------------------*/
  procedure FindPermitRenewalsForPolice (
    a_FileNumber                        varchar2,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_ApplicationType                   varchar2,
    a_Permittee                         varchar2,
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_TradeName                         varchar2,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_BaseFromDate                      date
        := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                        date
        := to_date('31-DEC-3999 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    t_EventDateFrom                     date;
    t_EventDateTo                       date;
    t_FinalList                         udt_IdList;
    t_FileNum                           varchar2(4000) := a_FileNumber;
    t_Licensee                          varchar2(4000) := a_Licensee;
    t_LicenseNumber                     varchar2(4000) := a_LicenseNumber;
    t_MuniIds                           udt_IdList;
    t_PermitNum                         varchar2(4000) := a_PermitNumber;
    t_PermitObjectIds                   udt_IdList;
    t_PermitSearchResults               api.pkg_Definition.udt_SearchResults;
    t_Permittee                         varchar2(4000) := a_Permittee;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_TradeName                         varchar2(4000) := a_TradeName;
    t_UserId                            udt_Id;
  begin

    a_Objects := api.udt_ObjectList();
    -- Find which municipalities the user is in and only get review processes
    --  for those municipalities
    t_UserId := api.pkg_SecurityQuery.EffectiveUserId;
    t_MuniIds := api.pkg_search.ObjectsByRelColumnValue('o_ABC_Office', 'PoliceUser',
        'ObjectId', t_UserId);
    if not api.pkg_search.IntersectByRelObjectIds('p_ABC_PoliceReview',
        'RenewalApplicationMunicipality', t_MuniIds, t_SearchResults) then
      return;
    end if;
    -- Search by the entered fields
    if a_FileNumber is not null then
       if substr(t_FileNum, Length(t_FileNum)) != '%' then
        t_FileNum := t_FileNum || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'PermitRenewalExt',
          'ExternalFileNum', t_FileNum, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitNumber is not null then
      if substr(t_PermitNum, Length(t_PermitNum)) != '%' then
        t_PermitNum := t_PermitNum || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'RenewalPermit',
          'PermitNumber', t_PermitNum, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitType is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'RenewalPermit',
          'PermitTypeObjectId', a_PermitType, t_SearchResults) then
        return;
      end if;
    end if;
    if a_ApplicationType is not null then
      if not api.pkg_Search.IntersectByColumnContents('p_ABC_PoliceReview',
          'JobTypeDescription', a_ApplicationType, t_SearchResults) then
        return;
      end if;
    end if;
    if a_Permittee is not null then
      if substr(t_Permittee, Length(t_Permittee)) != '%' then
        t_Permittee := t_Permittee || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnContents('p_ABC_PoliceReview', 'RenewalPermit',
          'dup_PermitteeDisplay_SQL', t_Permittee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_Licensee is not null then
      if substr(t_Licensee, Length(t_Licensee)) != '%' then
        t_Licensee := t_Licensee || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'RenewalLicensee',
          'dup_FormattedName', t_Licensee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_LicenseNumber is not null then
      if substr(t_LicenseNumber, Length(t_LicenseNumber)) != '%' then
        t_LicenseNumber := t_LicenseNumber || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'RenewalLicense',
          'LicenseNumber', t_LicenseNumber, t_SearchResults) then
        return;
      end if;
    end if;
    if a_TradeName is not null then
      if substr(t_TradeName, Length(t_TradeName)) != '%' then
        t_TradeName := t_TradeName || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_PoliceReview', 'RenewalEstablishment',
          'DoingBusinessAs', t_TradeName, t_SearchResults) then
        return;
      end if;
    end if;
    -- Search by event date
    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if a_EventDateFrom is null then
        t_EventDateFrom := t_BaseFromDate;
      else
        t_EventDateFrom := to_date(
            to_char(a_EventDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EventDateTo is null then
        t_EventDateTo := t_BaseToDate;
      else
        t_EventDateTo := to_date(
            to_char(a_EventDateTo, 'DD-MON-YYYY') || ' 23:59:59',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;
    -- Intersect permits that have an event date that falls within the bounds entered
    -- First, grab permits that have an event within the entered range
    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if api.pkg_search.IntersectByRelColumnValue('o_ABC_Permit', 'EventDate', 'StartDate',
          a_EventDateFrom, a_EventDateTo, t_PermitSearchResults) then
        t_PermitObjectIds := api.pkg_Search.SearchResultsToList(t_PermitSearchResults);
      else
        return;
      end if;
    end if;
    -- Search for Police Review processes related to the returned permit ids
    if t_PermitObjectIds.count != 0 then
      if not api.pkg_search.IntersectByRelObjectIds('p_ABC_PoliceReview', 'RenewalPermit',
          t_PermitObjectIds, t_SearchResults) then
        return;
      end if;
    end if;
    t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);
    for a in 1..t_FinalList.Count loop
      a_Objects.Extend(1);
      a_Objects(a_Objects.Count) := api.udt_Object(t_FinalList(a));
    end loop;

  end FindPermitRenewalsForPolice;

  /*---------------------------------------------------------------------------
   * PolicePermitAppSearch() -- PUBLIC
   *   Search for Permit Applications from the Police portal
   *-------------------------------------------------------------------------*/
  procedure PolicePermitAppSearch (
    a_FileNumber                        varchar2,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_ApplicationType                   varchar2,
    a_Permittee                         varchar2,
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_TradeName                         varchar2,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_ApplicationList                   api.udt_ObjectList;
    t_ApplicationsExist                 boolean;
    t_BaseFromDate                      date
        := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                        date
        := to_date('31-DEC-3999 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    t_EventDateFrom                     date;
    t_EventDateTo                       date;
    t_FinalList                         udt_IdList;
    t_MuniIds                           udt_IdList;
    t_MuniIdsString                     udt_StringList;
    t_Objects                           api.udt_ObjectList;
    t_PermitObjectIds                   udt_IdList;
    t_PermitObjectIdsString             udt_StringList;
    t_RenewalList                       api.udt_ObjectList;
    t_UserId                            udt_Id;

  begin
    a_Objects := api.udt_ObjectList();
    t_Objects := api.udt_ObjectList();
    t_ApplicationList := api.udt_ObjectList();
    t_RenewalList := api.udt_ObjectList();
    -- Error if no criteria has been entered
    if a_FileNumber is null and a_PermitNumber is null and a_PermitType is null and
        a_ApplicationType is null and a_Permittee is null and a_Licensee is null and
        a_LicenseNumber is null and a_TradeName is null and a_EventDateFrom is null and
        a_EventDateTo is null then
      api.pkg_errors.raiseerror(-20000, 'You must enter some search criteria.');
    end if;
    if a_ApplicationType = 'Permit Application' or a_ApplicationType is null then
      FindPermitAppsForPolice(a_FileNumber, a_PermitNumber, a_PermitType,
          a_ApplicationType, a_Permittee, a_Licensee, a_LicenseNumber,
          a_TradeName, a_EventDateFrom, a_EventDateTo, t_ApplicationList);
    end if;
    if a_ApplicationType = 'Permit Renewal' or a_ApplicationType is null then
      FindPermitRenewalsForPolice(a_FileNumber, a_PermitNumber, a_PermitType,
          a_ApplicationType, a_Permittee, a_Licensee, a_LicenseNumber,
          a_TradeName, a_EventDateFrom, a_EventDateTo, t_RenewalList);
    end if;
    -- Combine the Application and Renewal results and convert to
    --  udt_ObjectList to show in the search results
    for a in 1..t_ApplicationList.Count loop
      t_Objects.Extend(1);
      t_Objects(t_Objects.Count) := t_ApplicationList(a);
    end loop;
    for a in 1..t_RenewalList.Count loop
      t_Objects.Extend(1);
      t_Objects(t_Objects.Count) := t_RenewalList(a);
    end loop;

    select api.udt_Object(max(x.objectid))
    bulk collect into a_Objects
    from table(cast(t_Objects as api.udt_objectlist)) x
        join api.processes p
            on p.processid = x.objectid
    group by p.jobid;

  end PolicePermitAppSearch;

  /*---------------------------------------------------------------------------
   * FindPermitAppsForMuni() -- PRIVATE
   *   Called by abc.pkg_ABC_SearchProcedures.MuniPermitAppSearch(). Search for
   * Permit Applications from the Muni Portal.
   *-------------------------------------------------------------------------*/
  procedure FindPermitAppsForMuni (
    a_FileNumber                        varchar2,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_ApplicationType                   varchar2,
    a_Permittee                         varchar2,
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_TradeName                         varchar2,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_BaseFromDate                      date
        := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                        date
        := to_date('31-DEC-3999 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    t_EventDateFrom                     date;
    t_EventDateTo                       date;
    t_FinalList                         udt_IdList;
    t_FileNum                           varchar2(4000) := a_FileNumber;
    t_Licensee                          varchar2(4000) := a_Licensee;
    t_LicenseNumber                     varchar2(4000) := a_LicenseNumber;
    t_MuniIds                           udt_IdList;
    t_PermitNum                         varchar2(4000) := a_PermitNumber;
    t_PermitObjectIds                   udt_IdList;
    t_PermitSearchResults               api.pkg_Definition.udt_SearchResults;
    t_Permittee                         varchar2(4000) := a_Permittee;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_TradeName                         varchar2(4000) := a_TradeName;
    t_UserId                            udt_Id;
  begin

    a_Objects := api.udt_ObjectList();
    -- Find which municipalities the user is in and only get review processes
    --  for those municipalities
    t_UserId := api.pkg_SecurityQuery.EffectiveUserId;
    t_MuniIds := api.pkg_search.ObjectsByRelColumnValue('o_ABC_Office', 'MunicipalUser',
        'ObjectId', t_UserId);
    if not api.pkg_search.IntersectByRelObjectIds('p_ABC_MunicipalityReview',
        'NewApplicationMunicipality', t_MuniIds, t_SearchResults) then
      return;
    end if;
    -- Search by the entered fields
    if a_FileNumber is not null then
      if substr(t_FileNum, Length(t_FileNum)) != '%' then
        t_FileNum := t_FileNum || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'PermitApplicationExt', 'ExternalFileNum', t_FileNum, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitNumber is not null then
      if substr(t_PermitNum, Length(t_PermitNum)) != '%' then
        t_PermitNum := t_PermitNum || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'ApplicationPermit','PermitNumber', t_PermitNum, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitType is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'ApplicationPermit', 'PermitTypeObjectId', a_PermitType, t_SearchResults) then
        return;
      end if;
    end if;
    if a_ApplicationType is not null then
      if not api.pkg_Search.IntersectByColumnContents('p_ABC_MunicipalityReview',
          'JobTypeDescription', a_ApplicationType, t_SearchResults) then
        return;
      end if;
    end if;
    if a_Permittee is not null then
      if substr(t_Permittee, Length(t_Permittee)) != '%' then
        t_Permittee := t_Permittee || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'ApplicationPermit', 'dup_PermitteeDisplay_SQL', t_Permittee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_Licensee is not null then
      if substr(t_Licensee, Length(t_Licensee)) != '%' then
        t_Licensee := t_Licensee || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'ApplicationLicensee', 'dup_FormattedName', t_Licensee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_LicenseNumber is not null then
      if substr(t_LicenseNumber, Length(t_LicenseNumber)) != '%' then
        t_LicenseNumber := t_LicenseNumber || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'ApplicationLicense', 'LicenseNumber', t_LicenseNumber, t_SearchResults) then
        return;
      end if;
    end if;
    if a_TradeName is not null then
      if substr(t_TradeName, Length(t_TradeName)) != '%' then
        t_TradeName := t_TradeName || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'ApplicationEstablishment', 'DoingBusinessAs', t_TradeName, t_SearchResults) then
        return;
      end if;
    end if;
    -- Search by event date
    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if a_EventDateFrom is null then
        t_EventDateFrom := t_BaseFromDate;
      else
        t_EventDateFrom := to_date(
            to_char(a_EventDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EventDateTo is null then
        t_EventDateTo := t_BaseToDate;
      else
        t_EventDateTo := to_date(
            to_char(a_EventDateTo, 'DD-MON-YYYY') || ' 23:59:59',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;
    -- Intersect permits that have an event date that falls within the bounds entered
    -- First, grab permits that have an event within the entered range
    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if api.pkg_search.IntersectByRelColumnValue('o_ABC_Permit', 'EventDate', 'StartDate',
          a_EventDateFrom, a_EventDateTo, t_PermitSearchResults) then
        t_PermitObjectIds := api.pkg_Search.SearchResultsToList(t_PermitSearchResults);
      else
        return;
      end if;
    end if;
    -- Search for Muni Review processes related to the returned permit ids
    if t_PermitObjectIds.count != 0 then
      if not api.pkg_search.IntersectByRelObjectIds('p_ABC_MunicipalityReview', 'ApplicationPermit',
          t_PermitObjectIds, t_SearchResults) then
        return;
      end if;
    end if;
    t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);
    for a in 1..t_FinalList.Count loop
      a_Objects.Extend(1);
      a_Objects(a_Objects.Count) := api.udt_Object(t_FinalList(a));
    end loop;

  end FindPermitAppsForMuni;

  /*---------------------------------------------------------------------------
   * FindPermitRenewalsForMuni() -- PRIVATE
   *   Called by abc.pkg_ABC_SearchProcedures.MuniPermitAppSearch(). Search for
   * Permit Renewals from the Muni portal
   *-------------------------------------------------------------------------*/
  procedure FindPermitRenewalsForMuni (
    a_FileNumber                        varchar2,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_ApplicationType                   varchar2,
    a_Permittee                         varchar2,
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_TradeName                         varchar2,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_BaseFromDate                      date
        := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                        date
        := to_date('31-DEC-3999 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    t_EventDateFrom                     date;
    t_EventDateTo                       date;
    t_FinalList                         udt_IdList;
    t_FileNum                           varchar2(4000) := a_FileNumber;
    t_Licensee                          varchar2(4000) := a_Licensee;
    t_LicenseNumber                     varchar2(4000) := a_LicenseNumber;
    t_MuniIds                           udt_IdList;
    t_PermitNum                         varchar2(4000) := a_PermitNumber;
    t_PermitObjectIds                   udt_IdList;
    t_PermitSearchResults               api.pkg_Definition.udt_SearchResults;
    t_Permittee                         varchar2(4000) := a_Permittee;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_TradeName                         varchar2(4000) := a_TradeName;
    t_UserId                            udt_Id;
  begin

    a_Objects := api.udt_ObjectList();
    -- Find which municipalities the user is in and only get review processes
    --  for those municipalities
    t_UserId := api.pkg_SecurityQuery.EffectiveUserId;
    t_MuniIds := api.pkg_search.ObjectsByRelColumnValue('o_ABC_Office', 'MunicipalUser',
        'ObjectId', t_UserId);
    if not api.pkg_search.IntersectByRelObjectIds('p_ABC_MunicipalityReview',
        'RenewalApplicationMunicipality', t_MuniIds, t_SearchResults) then
      return;
    end if;
    -- Search by the entered fields
    if a_FileNumber is not null then
      if substr(t_FileNum, Length(t_FileNum)) != '%' then
        t_FileNum := t_FileNum || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'PermitRenewalExt', 'ExternalFileNum', t_FileNum, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitNumber is not null then
      if substr(t_PermitNum, Length(t_PermitNum)) != '%' then
        t_PermitNum := t_PermitNum || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview', 'RenewalPermit',
          'PermitNumber', t_PermitNum, t_SearchResults) then
        return;
      end if;
    end if;
    if a_PermitType is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview', 'RenewalPermit',
          'PermitTypeObjectId', a_PermitType, t_SearchResults) then
        return;
      end if;
    end if;
    if a_ApplicationType is not null then
      if not api.pkg_Search.IntersectByColumnContents('p_ABC_MunicipalityReview',
          'JobTypeDescription', a_ApplicationType, t_SearchResults) then
        return;
      end if;
    end if;
    if a_Permittee is not null then
      if substr(t_Permittee, Length(t_Permittee)) != '%' then
        t_Permittee := t_Permittee || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnContents('p_ABC_MunicipalityReview',
          'RenewalPermit', 'dup_PermitteeDisplay_SQL', t_Permittee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_Licensee is not null then
      if substr(t_Licensee, Length(t_Licensee)) != '%' then
        t_Licensee := t_Licensee || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'RenewalLicensee', 'dup_FormattedName', t_Licensee, t_SearchResults) then
        return;
      end if;
    end if;
    if a_LicenseNumber is not null then
      if substr(t_LicenseNumber, Length(t_LicenseNumber)) != '%' then
        t_LicenseNumber := t_LicenseNumber || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview', 'RenewalLicense',
          'LicenseNumber', t_LicenseNumber, t_SearchResults) then
        return;
      end if;
    end if;
    if a_TradeName is not null then
      if substr(t_TradeName, Length(t_TradeName)) != '%' then
        t_TradeName := t_TradeName || '%';
      end if;
      if not api.pkg_Search.IntersectByRelColumnValue('p_ABC_MunicipalityReview',
          'RenewalEstablishment', 'DoingBusinessAs', t_TradeName, t_SearchResults) then
        return;
      end if;
    end if;
    -- Search by event date
    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if a_EventDateFrom is null then
        t_EventDateFrom := t_BaseFromDate;
      else
        t_EventDateFrom := to_date(
            to_char(a_EventDateFrom, 'DD-MON-YYYY') || ' 00:00:00',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
      if a_EventDateTo is null then
        t_EventDateTo := t_BaseToDate;
      else
        t_EventDateTo := to_date(
            to_char(a_EventDateTo, 'DD-MON-YYYY') || ' 23:59:59',
            'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;
    -- Intersect permits that have an event date that falls within the bounds entered
    -- First, grab permits that have an event within the entered range
    if a_EventDateFrom is not null or a_EventDateTo is not null then
      if api.pkg_search.IntersectByRelColumnValue('o_ABC_Permit', 'EventDate', 'StartDate',
          a_EventDateFrom, a_EventDateTo, t_PermitSearchResults) then
        t_PermitObjectIds := api.pkg_Search.SearchResultsToList(t_PermitSearchResults);
      else
        return;
      end if;
    end if;
    -- Search for Muni Review processes related to the returned permit ids
    if t_PermitObjectIds.count != 0 then
      if not api.pkg_search.IntersectByRelObjectIds('p_ABC_MunicipalityReview', 'RenewalPermit',
          t_PermitObjectIds, t_SearchResults) then
        return;
      end if;
    end if;
    t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);
    for a in 1..t_FinalList.Count loop
      a_Objects.Extend(1);
      a_Objects(a_Objects.Count) := api.udt_Object(t_FinalList(a));
    end loop;

  end FindPermitRenewalsForMuni;

  /*---------------------------------------------------------------------------
   * MuniPermitAppSearch() -- PUBLIC
   *   Search for Permit Applications from the Muni portal
   *-------------------------------------------------------------------------*/
  procedure MuniPermitAppSearch (
    a_FileNumber                        varchar2,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_ApplicationType                   varchar2,
    a_Permittee                         varchar2,
    a_Licensee                          varchar2,
    a_LicenseNumber                     varchar2,
    a_TradeName                         varchar2,
    a_EventDateFrom                     date,
    a_EventDateTo                       date,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_ApplicationList                   api.udt_ObjectList;
    t_ApplicationsExist                 boolean;
    t_BaseFromDate                      date
        := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                        date
        := to_date('31-DEC-3999 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    t_EventDateFrom                     date;
    t_EventDateTo                       date;
    t_FinalList                         udt_IdList;
    t_MuniIds                           udt_IdList;
    t_MuniIdsString                     udt_StringList;
    t_Objects                           api.udt_ObjectList;
    t_PermitObjectIds                   udt_IdList;
    t_PermitObjectIdsString             udt_StringList;
    t_RenewalList                       api.udt_ObjectList;
    t_UserId                            udt_Id;

  begin
    a_Objects := api.udt_ObjectList();
    t_Objects := api.udt_ObjectList();
    t_ApplicationList := api.udt_ObjectList();
    t_RenewalList := api.udt_ObjectList();
    -- Error if no criteria has been entered
    if a_FileNumber is null and a_PermitNumber is null and a_PermitType is null and
        a_ApplicationType is null and a_Permittee is null and a_Licensee is null and
        a_LicenseNumber is null and a_TradeName is null and a_EventDateFrom is null and
        a_EventDateTo is null then
      api.pkg_errors.raiseerror(-20000, 'You must enter some search criteria.');
    end if;
    if a_ApplicationType = 'Permit Application' or a_ApplicationType is null then
      FindPermitAppsForMuni(a_FileNumber, a_PermitNumber, a_PermitType,
          a_ApplicationType, a_Permittee, a_Licensee, a_LicenseNumber,
          a_TradeName, a_EventDateFrom, a_EventDateTo, t_ApplicationList);
    end if;
    if a_ApplicationType = 'Permit Renewal' or a_ApplicationType is null then
      FindPermitRenewalsForMuni(a_FileNumber, a_PermitNumber, a_PermitType,
          a_ApplicationType, a_Permittee, a_Licensee, a_LicenseNumber,
          a_TradeName, a_EventDateFrom, a_EventDateTo, t_RenewalList);
    end if;
    -- Combine the Application and Renewal results and convert to
    --  udt_ObjectList to show in the search results
    for a in 1..t_ApplicationList.Count loop
      t_Objects.Extend(1);
      t_Objects(t_Objects.Count) := t_ApplicationList(a);
    end loop;
    for a in 1..t_RenewalList.Count loop
      t_Objects.Extend(1);
      t_Objects(t_Objects.Count) := t_RenewalList(a);
    end loop;

    select api.udt_Object(max(x.objectid))
    bulk collect into a_Objects
    from table(cast(t_Objects as api.udt_objectlist)) x
         join api.processes p
             on p.processid = x.objectid
    group by p.jobid;

  end MuniPermitAppSearch;

  /*---------------------------------------------------------------------------
   * CPLSubmissionSearch() -- PUBLIC
   *   Search CPL Submissions from the Public portal
   *-------------------------------------------------------------------------*/
  procedure CPLSubmissionSearch (
    a_CPLSubmissionType                 varchar2,
    a_Wholesaler                        varchar2,
    a_SubmissionPeriodId                number,
    a_Objects                out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_Count                             number;
    t_CPLSubmissionDefName              varchar2(30) := 'j_ABC_CPLSubmissions';
    t_ExcelExtract                      varchar2(1);
    t_FinalList                         udt_IdList;
    t_JobIds                            udt_IdList;
    t_JobIds2                           udt_IdList;
    t_SearchLimit                       number;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_SubmissionPeriodIds               udt_IdList;
    t_Wholesaler                        varchar2(4000) := a_Wholesaler;
  begin

    a_Objects := api.udt_ObjectList();
     -- Search by wholesaler
    if a_Wholesaler is not null then
      if length(a_Wholesaler) > 1 then

        -- Get CPL Jobs where Wholesaler is a Licensee
        t_JobIds := api.pkg_search.ObjectsByRelColumnContents(t_CPLSubmissionDefName,
            'LegalEntityFromLicense', 'dup_FormattedName', t_Wholesaler);
        -- Get CPL Jobs where Wholesaler is a Permittee
        t_JobIds2 := api.pkg_search.ObjectsByRelColumnContents(t_CPLSubmissionDefName,
            'PermitteeLegalEntity', 'dup_FormattedName', t_Wholesaler);
        if t_JobIds.count() = 0 and t_JobIds2.count() = 0 then
          return;
        else
          if t_JobIds.count() > 0 then
            if not api.pkg_Search.IntersectList(t_JobIds, t_SearchResults) then
              return;
            end if;
          end if;
          if t_JobIds2.count() > 0 then
            if t_JobIds.count() > 0 then
              api.pkg_Search.UnionList(t_JobIds2, t_SearchResults);
            else
              if not api.pkg_Search.IntersectList(t_JobIds2, t_SearchResults) then
                return;
              end if;
            end if;
          end if;
        end if;
      else
        api.pkg_errors.raiseerror(-20092, 'You must enter at least 2 characters to complete a '
            || 'search.');
      end if;
    else
      api.pkg_errors.raiseerror(-20092, 'You must enter a value for Wholesaler.');
    end if;
    -- Search by submission type
    if a_CPLSubmissionType is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_CPLSubmissionDefName, 'SubmissionType',
          a_CPLSubmissionType, t_SearchResults) then
        return;
      end if;
    end if;

    -- Search by submission period id
    if a_SubmissionPeriodId is not null then
      if not api.pkg_Search.IntersectByRelColumnValue(t_CPLSubmissionDefName,
          'SubPeriod', 'ObjectId', a_SubmissionPeriodId, t_SearchResults) then
        return;
      end if;
    end if;
    if a_SubmissionPeriodId is null then
      select x.objectid
        bulk collect into t_SubmissionPeriodIds
        from (
        select p.PostingDate, p.objectid
          from query.o_abc_cplsubmissionperiod p
         where p.ObjectDefTypeId = 1
           and p.PostingDate <= sysdate
         order by p.postingdate desc
      ) x
       where rownum < 4;
      if not api.pkg_Search.IntersectByRelObjectIds(t_CPLSubmissionDefName,
          'SubPeriod', t_SubmissionPeriodIds, t_SearchResults) then
        return;
      end if;
    end if;
    -- Ensure we only return the most recent submission for this group of licenses
    if not api.pkg_Search.IntersectByColumnValue(t_CPLSubmissionDefName,
       'IsLatestSubmission', 'Y', t_SearchResults) then
      return;
    end if;

    -- Process Final List
    t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);
    for a in 1..t_FinalList.count() loop
      a_Objects.Extend(1);
      a_Objects(a_Objects.count()) := api.udt_Object(t_FinalList(a));
    end loop;

    -- Check search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract, 'N');

    select s.OutriderSearchLimit
    into t_SearchLimit
    from query.o_systemSettings s;
    if a_Objects.count() > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_errors.raiseerror(-20056, 'Over '|| t_SearchLimit ||' results were returned, please '
          || 'narrow your search criteria.');
    end if;

  end CPLSubmissionSearch;

  /*---------------------------------------------------------------------------
   * CPLSubmissionSearchExtract() -- PUBLIC
   *   Search CPL Submissions from the Public portal
   *-------------------------------------------------------------------------*/
  function CPLSubmissionSearchExtract (
    a_CPLSubmissionType                 varchar2,
    a_Wholesaler                        varchar2,
    a_SubmissionPeriodObjectId          number
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin

    t_MasterLIst := api.udt_ObjectList();
    CPLSubmissionSearch(a_CPLSubmissionType, a_Wholesaler, a_SubmissionPeriodObjectId,
        t_MasterList, 'Y');

    return t_MasterList;

  end;

  /*---------------------------------------------------------------------------
   * InternalCPLSubmissionSearch() -- PUBLIC
   *   Search CPL Submissions from the Public portal
   *-------------------------------------------------------------------------*/
  procedure InternalCPLSubmissionSearch (
    a_CPLSubmissionType                 varchar2,
    a_Wholesaler                        varchar2,
    a_PostingDateFrom                   varchar2,
    a_PostingDateTo                     varchar2,
    a_Objects                out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  )is
    t_BaseFromDate                     date
        := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
    t_BaseToDate                       date
        := to_date('31-DEC-3999 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
    t_PostingDateFrom                  date;
    t_PostingDateTo                    date;
    t_CPLSubmissionDefName             varchar2(30) := 'j_ABC_CPLSubmissions';
    t_SearchResults                    api.pkg_Definition.udt_SearchResults;
    t_SearchResults2                   api.pkg_Definition.udt_SearchResults;
    t_FinalList                        udt_IdList;
    t_Wholesaler                       varchar2(4000) := a_Wholesaler;
    t_JobIds                           udt_IdList;
    t_JobIds2                          udt_IdList;
    t_Count                            number;
    t_Objects                          api.udt_ObjectList;
    t_SearchLimit                      number;
    t_ExcelExtract                     varchar2(1);

  begin
    if a_CPLSubmissionType is null and
       a_Wholesaler is null and
       a_PostingDateFrom is null and
       a_PostingDateTo is null then
      api.pkg_errors.raiseerror(-20000, 'You must enter some search criteria.');
    end if;

    a_Objects := api.udt_ObjectList();
    t_Objects := api.udt_ObjectList();
     -- Search by wholesaler
    if a_Wholesaler is not null then
      t_JobIds := api.pkg_search.ObjectsByRelColumnContents(t_CPLSubmissionDefName,
          'LegalEntityFromLicense', 'dup_FormattedName', t_Wholesaler);
      t_JobIds2 := api.pkg_search.ObjectsByRelColumnContents(t_CPLSubmissionDefName,
          'PermitteeLegalEntity', 'dup_FormattedName', t_Wholesaler);
      if t_JobIds.count = 0 and t_JobIds2.count = 0 then
        return;
      else
        if t_JobIds.count > 0 then
          if not api.pkg_Search.IntersectList(t_JobIds, t_SearchResults) then
            return;
          end if;
        end if;
        if t_JobIds2.count > 0 then
          if t_JobIds.count > 0 then
            api.pkg_Search.UnionList(t_JobIds2, t_SearchResults);
          else
            if not api.pkg_Search.IntersectList(t_JobIds2, t_SearchResults) then
              return;
            end if;
          end if;
        end if;
      end if;
    end if;
    -- Search by submission type
    if a_CPLSubmissionType is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_CPLSubmissionDefName, 'SubmissionType',
          a_CPLSubmissionType, t_SearchResults) then
        return;
      end if;
    end if;
    -- Set posting dates
    if a_PostingDateFrom is null then
      t_PostingDateFrom := t_BaseFromDate;
    elsif regexp_substr(a_PostingDateFrom, '^(0[1-9]|1[012])[/]\d\d\d\d$') is not null then
      t_PostingDateFrom := to_date(a_PostingDateFrom, 'mm/yyyy');
    else
      api.pkg_errors.raiseerror(-20092, 'Please enter a valid date in the format of mm/yyyy');
    end if;
    if a_PostingDateTo is null then
      t_PostingDateTo := t_BaseToDate;
    elsif regexp_substr(a_PostingDateTo, '^(0[1-9]|1[012])[/]\d\d\d\d$') is not null then
      t_PostingDateTo := last_day(to_date(a_PostingDateTo, 'mm/yyyy'));
    else
      api.pkg_errors.raiseerror(-20092, 'Please enter a valid date in the format of mm/yyyy');
    end if;
    -- Search by posting date
    if a_PostingDateFrom is not null or a_PostingDateTo is not null then
      if not api.pkg_Search.IntersectByRelColumnValue(t_CPLSubmissionDefName, 'SubPeriod', 'PostingDate',
          t_PostingDateFrom, t_PostingDateTo, t_SearchResults) then
        return;
      end if;
    end if;
    -- Process Final List
    t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);
    for a in 1..t_FinalList.Count loop
      t_Objects.Extend(1);
      t_Objects(t_Objects.Count) := api.udt_Object(t_FinalList(a));
    end loop;
    --Ensure results have status of COMP or REJECT
    select api.udt_Object(x.objectid)
      bulk collect into a_Objects
      from table(cast(t_Objects as api.udt_objectlist)) x
           join api.jobs j
                on j.JobId = x.objectid
           where j.JobStatus in ('COMP', 'REJECT');

    -- Check search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract, 'N');

    select s.OutriderSearchLimit
    into t_SearchLimit
    from query.o_systemSettings s;
    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_errors.raiseerror(-20056, 'Over '|| t_SearchLimit ||' results were returned, please narrow your search'
          ||' criteria.');
    end if;

  end InternalCPLSubmissionSearch;

  /*---------------------------------------------------------------------------
   * CPLSubmissionSearchExtractInternal() -- PUBLIC
   *   Search CPL Submissions from the Public portal
   *-------------------------------------------------------------------------*/
  function CPLSubmissionSearchExtractInternal (
    a_CPLSubmissionType                varchar2,
    a_Wholesaler                       varchar2,
    a_PostingDateFrom                  varchar2,
    a_PostingDateTo                    varchar2
  ) return api.udt_ObjectList is
    t_MasterList                         api.udt_ObjectList;
  begin
    t_MasterLIst := api.udt_ObjectList();
    InternalCPLSubmissionSearch(a_CPLSubmissionType, a_Wholesaler, a_PostingDateFrom, a_PostingDateTo, t_MasterList, 'Y');
    return t_MasterList;
  end;

  /*---------------------------------------------------------------------------
   * UserPaymentHistorySearch() -- PUBLIC
   *   Given a User Id, return all payment transactions from the last 90 days.
   *-------------------------------------------------------------------------*/
  procedure UserPaymentHistorySearch (
    a_UserId                            udt_Id,
    a_PaymentDateFrom                   date,
    a_PaymentDateTo                     date,
    a_Objects                out nocopy api.udt_ObjectList
  ) is
    t_LegalEntityIds                    udt_IdList;
    t_OnlineUserLEObjects               api.udt_ObjectList;
    t_OnlineUserPayments                udt_IdList;
    t_PaymentIdList                     udt_IdList;
    t_OnlineUsersForLE                  udt_IdList;
    t_FilteredPayments                  udt_IdList;
    t_JobIds                            udt_IdList;
    t_PermitAppIds                      udt_IdList;
    t_JobPayments                       udt_IdList;
    t_JobDefNames                       udt_StringList;
    t_PaymentJobEPNames                 udt_StringList;
    t_MiscRevTransIds                   udt_IdList;
    t_MiscRevPaymentIdList              udt_IdList;
  begin
    if trunc(a_PaymentDateFrom) < trunc(sysdate) -90 then
      api.pkg_errors.raiseerror(-20095, 'You cannot search for payments processed before '
          || to_char(trunc(sysdate)-90, 'mm/dd/yyyy'));
    end if;
    -- Initialize the list of jobs for payments to be returned from
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_NewApplication';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'NewApplication';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_RenewalApplication';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'RenewalApplication';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_AmendmentApplication';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'AmendmentApplication';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_PermitApplication';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'PermitApplication';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_PermitRenewal';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'PermitRenewal';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_PRApplication';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'PRApplication';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_PRRenewal';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'PRRenewal';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_PRAmendment';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'PRAmendment';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_Accusation';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'Accusation';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_Reinstatement';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'Reinstatement';
    t_JobDefNames(t_JobDefNames.count+1) := 'j_ABC_Petition';
    t_PaymentJobEPNames(t_PaymentJobEPNames.count+1) := 'Petition';

    -- Get payments made by all users associated to the user's associated LEs
    t_LegalEntityIds := api.pkg_search.ObjectsByRelColumnValue('o_ABC_LegalEntity',
        'OnlineUser', 'ObjectId', a_UserId);
    t_OnlineUsersForLE := api.pkg_search.ObjectsByRelObjectIds('u_Users',
        'LegalEntity', t_LegalEntityIds);
    t_PaymentIdList := api.pkg_Search.ObjectsByRelObjectIds('o_Payment',
        'OnlineUser', t_OnlineUsersForLE);

    -- For each job def defined, get all payments over the corresponding end point
    for i in 1.. t_JobDefNames.count loop
      t_JobIds := api.pkg_search.ObjectsByRelColumnValue(t_JobDefNames(i),
          'PublicUser', 'ObjectId', a_UserId);
      t_JobPayments := api.pkg_search.ObjectsByRelObjectIds('o_Payment', t_PaymentJobEPNames(i), t_JobIds);
      extension.Pkg_Collectionutils.Append(t_PaymentIdList, t_JobPayments);
    end loop;

    -- Special Handling for the misc revenue transaction
    t_MiscRevTransIds := api.pkg_search.ObjectsByRelObjectIds('j_ABC_MiscellaneousRevenue',
        'LegalEntityProc', t_LegalEntityIds);
    t_MiscRevPaymentIdList := api.pkg_Search.ObjectsByRelObjectIds('o_Payment',
        'MiscRevenue', t_MiscRevTransIds);
    extension.pkg_collectionutils.Append(t_PaymentIdList, t_MiscRevPaymentIdList);

    -- Filter results to only within the entered criteria
    select column_value
      bulk collect into t_FilteredPayments
      from table(t_PaymentIdList)
     where api.pkg_columnquery.datevalue(column_value, 'PaymentDate')
         between (coalesce(a_PaymentDateFrom, trunc(sysdate-90)))
         and (coalesce(a_PaymentDateTo, trunc(sysdate))+(86399/86400));

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_FilteredPayments);

  end UserPaymentHistorySearch;

  /*---------------------------------------------------------------------------
   * JobTypeSearch() -- PUBLIC
   *   Given a Job Type name return all ABC Job Types.
   *-------------------------------------------------------------------------*/
  procedure JobTypeSearch (
    a_Description                       varchar2,
    a_Name                              varchar2,
    a_ObjectList                        out nocopy udt_ObjectList
  ) is

  t_IdList                              udt_IdList;

  begin


    if a_Description is null and a_Name is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;
    select jt.objectid
    bulk collect into t_IdList
    from query.o_jobtypes jt
    where (lower(jt.Description) like lower(a_description)||'%' or a_description is null)
      and (lower(jt.Name) like lower(a_name)||'%' or a_name is null)
      and jt.IsABCJob = 'Y';

    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);

  end JobTypeSearch;

  /*---------------------------------------------------------------------------
   * UserExternalSearch() -- PUBLIC
   *   Search for Municipal, Police, and Public Users on Internal Site
   *-------------------------------------------------------------------------*/
  procedure UserExternalSearch(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_EmailAddress                      varchar2,
    a_PhoneNumber                       varchar2,
    a_BusinessPhone                     varchar2,
    a_UserType                          varchar2,
    a_Municipality                      varchar2,
    a_LegalEntity                       varchar2,
    a_LicenseNumber                     varchar2,
    a_PermitNumber                      varchar2,
    a_Objects                       out api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_SearchResults                     api.pkg_Search.udt_SearchResults;
    t_MunicipalityResults               api.pkg_Search.udt_SearchResults;
    t_SearchNotDone                     boolean := true;
    t_SearchLimit                       number;
    t_ExcelExtract                      varchar2(1);
    t_UserTypes                         udt_StringList;
    t_ObjectDefName                     varchar2(10) := 'u_Users';
  begin
    -- Check to make sure at least some criteria has been entered.
    if a_FirstName is null
        and a_LastName is null
        and a_EmailAddress is null
        and a_PhoneNumber is null
        and a_BusinessPhone is null
        and a_Municipality is null
        and a_LegalEntity is null
        and a_LicenseNumber is null
        and a_PermitNumber is null
    then
      if a_UserType is not null then
        -- User Type alone is not enough to narrow search results
        api.pkg_Errors.RaiseError(-20000, 'Please enter more search criteria.');
      else
        api.pkg_Errors.RaiseError(-20000, 'You must enter some search criteria.');
      end if;
    end if;

    a_Objects := api.udt_objectlist();
    t_SearchResults := api.pkg_Search.NewSearchResults;

    -- Phone Number
    if a_PhoneNumber is not null then
      if not api.pkg_Search.IntersectByEditMask(t_ObjectDefName,
          'PhoneNumber', a_PhoneNumber, t_SearchResults) then
        return;
      end if;
    end if;

    -- Cell Number
    if a_BusinessPhone is not null then
      if not api.pkg_Search.IntersectByEditMask(t_ObjectDefName,
          'BusinessPhone', a_BusinessPhone, t_SearchResults) then
        return;
      end if;
    end if;

    -- Email Address
    if a_EmailAddress is not null then
      if not api.pkg_Search.IntersectByColumnContents(t_ObjectDefName, 'EmailAddress',
          a_EmailAddress, t_SearchResults) then
        return;
      end if;
    end if;

    -- Last Name
    if a_LastName is not null then
      if not api.pkg_Search.IntersectByColumnContents(t_ObjectDefName,
          'LastName', a_LastName, t_SearchResults) then
        return;
      end if;
    end if;

    -- First Name
    if a_FirstName is not null then
      if not api.pkg_Search.IntersectByColumnContents(t_ObjectDefName,
          'FirstName', a_FirstName, t_SearchResults) then
        return;
      end if;
    end if;

    -- Set User Type
    if a_UserType is not null then
      t_UserTypes := extension.pkg_Utils.Split(a_UserType, ', ');
    else
      t_UserTypes := extension.pkg_Utils.Split('Municipal, Public, Police', ', ');
    end if;

    -- User Type
    if t_UserTypes is not null then
      if not api.pkg_Search.IntersectByColumnValue(t_ObjectDefName,
          'UserType', t_UserTypes, t_SearchResults) then
        return;
      end if;
    end if;

    -- Municipality
    if a_Municipality is not null then
      t_MunicipalityResults := api.pkg_Search.NewSearchResults();
      api.pkg_Search.UnionByRelColumnContents(t_ObjectDefName,
        'Municipality', 'Name', a_Municipality, t_MunicipalityResults);
      api.pkg_Search.UnionByRelColumnContents(t_ObjectDefName,
        'MunicipalityPolice', 'Name', a_Municipality, t_MunicipalityResults);

      if not api.pkg_Search.IntersectSearchResults(t_MunicipalityResults,
          t_SearchResults) then
        return;
      end if;
    end if;

    -- Legal Entity
    if a_LegalEntity is not null then
      if not api.pkg_Search.IntersectByRelColumnContents(t_ObjectDefName,
        'LegalEntity', 'dup_FormattedName', a_LegalEntity, t_SearchResults) then
        return;
      end if;
    end if;

    -- License Number
    if a_LicenseNumber is not null then
      if not api.pkg_Search.IntersectByRelColumnContents(t_ObjectDefName,
        'UserLicenses', 'LicenseNumber', a_LicenseNumber, t_SearchResults) then
        return;
      end if;
    end if;

    -- Permit Number
    if a_PermitNumber is not null then
      if not api.pkg_Search.IntersectByRelColumnContents(t_ObjectDefName,
        'UserPermits', 'PermitNumber', a_PermitNumber, t_SearchResults) then
        return;
      end if;
    end if;

    -- Convert to ObjectList for Outrider
    declare
      t_IdList                          udt_IdList;
    begin
      t_IdList := api.pkg_Search.SearchResultsToList(t_SearchResults);

      a_Objects := api.udt_ObjectList();
      for i in 1..t_IdList.count loop
        a_Objects.extend;
        a_Objects(i) := api.udt_Object(t_IdList(i));
      end loop;
    end;

    -- Check the search limit and if this is an Excel Extract
    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if a_Objects.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

  end UserExternalSearch;

  /*---------------------------------------------------------------------------
   * UserExternalSearchExtract() -- PUBLIC
   *   Search Extract for Municipal, Police, and Public Users on Internal Site
   *-------------------------------------------------------------------------*/
  function UserExternalSearchExtract(
    a_FirstName                         varchar2,
    a_LastName                          varchar2,
    a_EmailAddress                      varchar2,
    a_PhoneNumber                       varchar2,
    a_BusinessPhone                     varchar2,
    a_UserType                          varchar2,
    a_Municipality                      varchar2,
    a_LegalEntity                       varchar2,
    a_LicenseNumber                     varchar2,
    a_PermitNumber                      varchar2
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    UserExternalSearch(a_FirstName, a_LastName, a_EmailAddress, a_PhoneNumber,
        a_BusinessPhone, a_UserType, a_Municipality, a_LegalEntity, a_LicenseNumber,
        a_PermitNumber, t_MasterList, 'Y');
    return t_MasterList;

  end UserExternalSearchExtract;

  /*---------------------------------------------------------------------------
   * EmailUsageReport() -- PUBLIC
   *   Search for emails. Used in search procedure s_EmailUsageReport
   *-------------------------------------------------------------------------*/
  Procedure EmailUsageReport(
    a_FromCreatedDate       date,
    a_ToCreatedDate         date,
    a_ObjectList            out nocopy api.udt_ObjectList
  ) is
    t_IdList                udt_IdList;
  begin
    select objectid
      bulk collect into t_IdList
      from extension.emails
    where createddate between trunc(nvl(a_FromCreatedDate, sysdate))
      and trunc(nvl(a_ToCreatedDate, sysdate)) + 86399 / 86400;

    if t_IdList.count > OutriderSearchLimit then
      raise_application_error(-20009, 'Your search returned '
        || t_IdList.count || ' rows. The current configured limit is '
        || OutriderSearchLimit
        || '. Please either refine your criteria or increase the limit.');
    end if;

    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);
  end EmailUsageReport;

  /*---------------------------------------------------------------------------
   * SMSUsageReport() -- PUBLIC
   *   Search for sms. Used in search procedure s_SMSUsageReport
   *-------------------------------------------------------------------------*/
  Procedure SMSUsageReport(
    a_FromCreatedDate       date,
    a_ToCreatedDate         date,
    a_ObjectList            out nocopy api.udt_ObjectList
  ) is
    t_IdList                udt_IdList;
  begin
    select objectid
      bulk collect into t_IdList
      from extension.sms
    where createddate between trunc(nvl(a_FromCreatedDate, sysdate))
      and trunc(nvl(a_ToCreatedDate, sysdate)) + 86399 / 86400;

    if t_IdList.count > OutriderSearchLimit then
      raise_application_error(-20009, 'Your search returned '
        || t_IdList.count || ' rows. The current configured limit is '
        || OutriderSearchLimit
        || '. Please either refine your criteria or increase the limit.');
    end if;

    for i in 1..t_IdList.count loop
      api.pkg_columnupdate.RemoveValue(t_idList(i), 'ResendStatusMessage');
    end loop;

    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_IdList);
  end SMSUsageReport;

  /*---------------------------------------------------------------------------
   * PublicPermitSearch() -- PUBLIC
   *   Search for permits on the public legal entity
   *-------------------------------------------------------------------------*/
  procedure PublicPermitSearch(
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_Permittee                         varchar2,
    a_State                             varchar2,
    a_UserId                            number,
    a_ObjectList                        out nocopy udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_EffectiveDateFrom                 date;
    t_EffectiveDateTo                   date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_IssueDateFrom                     date;
    t_IssueDateTo                       date;
    t_ExcelExtract                      varchar2(1);
    t_FinalList                         udt_IdList;
    t_IdList                            udt_IdList;
    t_SearchLimit                       number;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_Permittee                         varchar2(4000) := a_Permittee;
  begin

    -- Raise error if no search criteria is entered
    if a_PermitNumber is null
        and a_PermitType is null
        and a_Permittee is null
        and a_IssueDateFrom is null
        and a_IssueDateFrom is null
        and a_EffectiveDateFrom is null
        and a_EffectiveDateTo is null
        and a_ExpirationDateFrom is null
        and a_ExpirationDateTo is null
        and a_State is null then
      raise_application_error(-20000, 'You must enter some search criteria.');
    end if;

    t_EffectiveDateFrom := SetDateSearchCriteria(a_EffectiveDateFrom);
    t_EffectiveDateTo := SetDateSearchCriteria(a_EffectiveDateTo, false);
    t_ExpirationDateFrom := SetDateSearchCriteria(a_ExpirationDateFrom);
    t_ExpirationDateTo := SetDateSearchCriteria(a_ExpirationDateTo, false);
    t_IssueDateFrom := SetDateSearchCriteria(a_IssueDateFrom);
    t_IssueDateTo := SetDateSearchCriteria(a_IssueDateTo, false);

    t_Permittee := a_Permittee;

    --Start Search
    a_ObjectList := api.udt_ObjectList();
    t_SearchResults := api.pkg_Search.NewSearchResults();

    if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_Permit', 'User', 'ObjectId',
        a_UserID, t_SearchResults) then
      return;
    end if;

    if a_PermitNumber is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'PermitNumber',
          a_PermitNumber, t_SearchResults) then
        return;
      end if;
    end if;

    if a_PermitType is not null then
      if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_Permit', 'PermitType', 'Name',
          a_PermitType, t_SearchResults) then
        return;
      end if;
    end if;

    if a_State is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'State',
          a_State, t_SearchResults) then
        return;
      end if;
    end if;

    if a_Permittee is not null then
      if substr(t_Permittee, Length(t_Permittee)) != '%' then
        t_Permittee := t_Permittee || '%';
      end if;
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'dup_PermitteeDisplay_SQL',
          a_Permittee, t_SearchResults) then
        return;
      end if;
    end if;

    if a_IssueDateFrom is not null or a_IssueDateTo is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_Permit', 'IssueDate',
          t_IssueDateFrom, t_IssueDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    if a_EffectiveDateFrom is not null or a_EffectiveDateTo is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_Permit', 'EffectiveDate',
          t_EffectiveDateFrom, t_EffectiveDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_Permit', 'ExpirationDate',
          t_ExpirationDateFrom, t_ExpirationDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    t_FinalList := api.pkg_Search.SearchResultsToList(t_SearchResults);

    if t_FinalList.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

    for a in 1..t_FinalList.Count loop
      a_ObjectList.Extend(1);
      a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_FinalList(a));
    end loop;

  end PublicPermitSearch;

  /*---------------------------------------------------------------------------
   * PublicPermitSearchExtract() -- PUBLIC
   *   Provides Excel Extract for public pemrit search.
   *-------------------------------------------------------------------------*/
  function PublicPermitSearchExtract(
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_PermitNumber                      varchar2,
    a_PermitType                        varchar2,
    a_Permittee                         varchar2,
    a_State                             varchar2,
    a_UserID                            number
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin
    t_MasterList := api.udt_objectlist();
    PublicPermitSearch(a_EffectiveDateFrom, a_EffectiveDateTo, a_ExpirationDateFrom,
        a_ExpirationDateTo, a_IssueDateFrom, a_IssueDateTo, a_PermitNumber, a_PermitType,
        a_Permittee, a_State, a_UserID, t_MasterList, 'Y');
    return t_MasterList;

  end PublicPermitSearchExtract;

  /*---------------------------------------------------------------------------
   * SetDateSearchCriteria() -- PUBLIC
   *   Set From/To Date Search Criteria if no date supplied and add timestamps
   * to take the full day into account.
   *-------------------------------------------------------------------------*/
  function SetDateSearchCriteria (
    a_Date                              date,
    a_IsFromDate                        boolean default true
  ) return date is
    t_Date                              date;
  begin

    if a_IsFromDate then
      if a_Date is null then
        t_Date := to_date('01-JAN-1900 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      else
        t_Date := to_date(to_char(a_Date, 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    else
      if a_Date is null then
        t_Date := to_date('31-DEC-2999 23:59:59','DD-MON-YYYY HH24:MI:SS');
      else
        t_Date := to_date(to_char(a_Date, 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:MI:SS');
      end if;
    end if;

    return t_Date;

  end SetDateSearchCriteria;

  /*---------------------------------------------------------------------------
   * FilteredPermitSearch() -- PUBLIC
   *   Used on the Permits Tab of the Legal Entity to search for Permits
   * related to the Legal Entity based on the criteria entered on the Permits
   * tab.
   *-------------------------------------------------------------------------*/
  procedure FilteredPermitSearch (
    a_PermitNumber                      varchar2,
    a_PermitTypeObjectId                udt_Id,
    a_Permittee                         varchar2,
    a_IssueDateFrom                     date,
    a_IssueDateTo                       date,
    a_EffectiveDateFrom                 date,
    a_EffectiveDateTo                   date,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_State                             varchar2,
    a_SourceObjectId                    udt_Id,
    a_ObjectList             out nocopy api.udt_ObjectList
  ) is
    t_EffectiveDateFrom                 date;
    t_EffectiveDateTo                   date;
    t_ExpirationDateFrom                date;
    t_ExpirationDateTo                  date;
    t_IssueDateFrom                     date;
    t_IssueDateTo                       date;
    t_Permittee                         varchar2(4000) := a_Permittee;
    t_PermitIdList                      udt_IdList;
    t_SearchLimit                       number;
    t_SearchNotDone                     boolean := true;
    t_SearchResults                     udt_SearchResults;
    t_SolicitorSearchNotDone            boolean := true;
    t_SolicitorSearchResults            udt_SearchResults;
    t_SystemSettingsObjectId            udt_Id;
  begin

    if a_PermitNumber is null
        and a_PermitTypeObjectId is null
        and a_Permittee is null
        and a_IssueDateFrom is null
        and a_IssueDateTo is null
        and a_EffectiveDateFrom is null
        and a_EffectiveDateTo is null
        and a_ExpirationDateFrom is null
        and a_ExpirationDateTo is null
        and a_State is null then
      api.pkg_Errors.RaiseError(-20000, 'You must enter some search criteria.');
    end if;

    a_ObjectList := api.udt_objectlist();
    t_SearchResults := api.pkg_Search.NewSearchResults();
    t_SolicitorSearchResults := api.pkg_Search.NewSearchResults();

    t_EffectiveDateFrom := SetDateSearchCriteria(a_EffectiveDateFrom);
    t_EffectiveDateTo := SetDateSearchCriteria(a_EffectiveDateTo, false);
    t_ExpirationDateFrom := SetDateSearchCriteria(a_ExpirationDateFrom);
    t_ExpirationDateTo := SetDateSearchCriteria(a_ExpirationDateTo, false);
    t_IssueDateFrom := SetDateSearchCriteria(a_IssueDateFrom);
    t_IssueDateTo := SetDateSearchCriteria(a_IssueDateTo, false);
    t_SystemSettingsObjectId := api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
        'SystemSettingsNumber', 1);
    t_SearchLimit := api.pkg_ColumnQuery.NumericValue(t_SystemSettingsObjectId,
        'OutriderSearchLimit');

    if a_SourceObjectId is not null then
      t_SearchNotDone := api.pkg_Search.IntersectByRelColumnValue('o_ABC_Permit',
          'LegalEntity', 'ObjectId', a_SourceObjectId, t_SearchResults);
      t_SolicitorSearchNotDone := api.pkg_Search.IntersectByRelColumnValue('o_ABC_Permit',
          'Solicitor', 'ObjectId', a_SourceObjectId, t_SolicitorSearchResults);
      -- If neither searches succeed, return
      if not t_SearchNotDone and not t_SolicitorSearchNotDone then
        return;
      -- Otherwise, union the results
      else
        api.pkg_Search.UnionSearchResults(t_SolicitorSearchResults, t_SearchResults);
      end if;
    else
      raise_application_error(-20000, 'You must have an object ID for this search.');
    end if;

    if a_PermitNumber is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'PermitNumber',
          a_PermitNumber, t_SearchResults) then
        return;
      end if;
    end if;

    if a_PermitTypeObjectId is not null then
      if not api.pkg_Search.IntersectByRelColumnContents('o_ABC_Permit', 'PermitType', 'ObjectId',
          a_PermitTypeObjectId, t_SearchResults) then
        return;
      end if;
    end if;

    if t_Permittee is not null then
      if substr(t_Permittee, Length(t_Permittee)) != '%' then
        t_Permittee := t_Permittee || '%';
      end if;
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_Permit', 'dup_PermitteeDisplay_SQL',
          t_Permittee, t_SearchResults) then
        return;
      end if;
    end if;

    if a_IssueDateFrom is not null or a_IssueDateTo is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_Permit', 'IssueDate',
          t_IssueDateFrom, t_IssueDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    if a_EffectiveDateFrom is not null or a_EffectiveDateTo is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_Permit', 'EffectiveDate',
          t_EffectiveDateFrom, t_EffectiveDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_Permit', 'ExpirationDate',
          t_ExpirationDateFrom, t_ExpirationDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    if a_State is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit', 'State', a_State,
          t_SearchResults) then
        return;
      end if;
    end if;

    t_PermitIdList := api.pkg_Search.SearchResultsToList(t_SearchResults);

    if t_PermitIdList.count() > t_SearchLimit then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_PermitIdList);

  end FilteredPermitSearch;

  /*---------------------------------------------------------------------------
   * ApprovedTermsSearch() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ApprovedTermsSearch (
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_LicenseType                       varchar2,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_PetitionType                      varchar2,
    a_UserId                            udt_Id,
    a_ObjectList             out nocopy api.udt_ObjectList,
    a_ExcelExtract                      varchar2 default 'N'
  ) is
    t_ExcelExtract                      varchar2(1);
    t_HasResults                        boolean;
    t_LicenseIdList                     udt_IdList;
    t_SearchLimit                       number;
    t_SearchResults                     udt_SearchResults;
    t_StartingList                      api.udt_ObjectList;
    t_TempList                          udt_IdList;
  begin

    if a_LicenseNumber is null
        and a_Licensee is null
        and a_LicenseType is null
        and a_ExpirationDateFrom is null and a_ExpirationDateTo is null
        and a_InactivityStartDateFrom is null and a_InactivityStartDateTo is null
        and a_PetitionType is null
        and a_UserId is null then
      api.pkg_Errors.RaiseError(-20000, 'You must enter some criteria.');
    end if;

    if a_LicenseNumber is not null and length(a_LicenseNumber) < 3 then
      api.pkg_Errors.RaiseError(-20000, 'Please enter at least 3 digits of the License Number.');
    end if;

    if a_Licensee is not null and length(a_Licensee) < 3 then
      api.pkg_Errors.RaiseError(-20000, 'Please enter at least 3 characters of the Licensee name.');
    end if;

    if a_LicenseType is not null and length(a_LicenseType) < 3 then
      api.pkg_Errors.RaiseError(-2000, 'Please enter at least 3 characters of the License Type.');
    end if;

    a_ObjectList := api.udt_ObjectList();
    t_SearchResults := api.pkg_Search.NewSearchResults();

    -- Start with list of all licenses with approved terms associated to user
    abc.pkg_ABC_ProceduralRels.PublicLicensesWithAppTermsAll(a_UserId, 0, t_StartingList);

    t_HasResults := api.pkg_Search.IntersectObjectList(t_StartingList, t_SearchResults);

    if a_LicenseNumber is not null then
      if not api.pkg_Search.IntersectByColumnContents('o_ABC_License', 'LicenseNumber',
          a_LicenseNumber, t_SearchResults) then
        return;
      end if;
    end if;

    if a_Licensee is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'Licensee',
          'dup_FormattedName', a_Licensee||'%', t_SearchResults) then
        return;
      end if;
    end if;

    if a_LicenseType is not null then
      if not api.pkg_Search.IntersectByRelColumnValue('o_ABC_License', 'LicenseType',
          'Name', a_LicenseType, t_SearchResults) then
        return;
      end if;
    end if;

    if a_ExpirationDateFrom is not null or a_ExpirationDateTo is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_License', 'ExpirationDate',
          a_ExpirationDateFrom, a_ExpirationDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    if a_InactivityStartDateFrom is not null or a_InactivityStartDateTo is not null then
      if not api.pkg_Search.IntersectByColumnValue('o_ABC_License', 'InactivityStartDate',
          a_InactivityStartDateFrom, a_InactivityStartDateTo, t_SearchResults) then
        return;
      end if;
    end if;

    if a_PetitionType is not null then
      t_TempList := api.pkg_Search.ObjectsByRelColumnValue('o_ABC_License', 'PetitionTypeIND',
          'Name', a_PetitionType);

      if not api.pkg_Search.IntersectList(t_TempList, t_SearchResults) then
        return;
      end if;
    end if;

    t_LicenseIdList := api.pkg_Search.SearchResultsToList(t_SearchResults);

    t_ExcelExtract := nvl(a_ExcelExtract,'N');
    t_SearchLimit  := OutriderSearchLimit();

    if t_LicenseIdList.Count > t_SearchLimit and t_ExcelExtract = 'N' then
      api.pkg_Errors.RaiseError(-20000, OutriderSearchLimitError(t_SearchLimit));
    end if;

    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_LicenseIdList);

  end ApprovedTermsSearch;

  /*---------------------------------------------------------------------------
   * ApprovedTermsSearchExtract() -- PUBLIC
   *-------------------------------------------------------------------------*/
  function ApprovedTermsSearchExtract (
    a_LicenseNumber                     varchar2,
    a_Licensee                          varchar2,
    a_LicenseType                       varchar2,
    a_ExpirationDateFrom                date,
    a_ExpirationDateTo                  date,
    a_InactivityStartDateFrom           date,
    a_InactivityStartDateTo             date,
    a_PetitionType                      varchar2,
    a_UserId                            udt_Id
  ) return api.udt_ObjectList is
    t_MasterList                        api.udt_ObjectList;
  begin

    t_MasterList := api.udt_ObjectList();

    ApprovedTermsSearch(a_LicenseNumber, a_Licensee, a_LicenseType, a_ExpirationDateFrom, a_ExpirationDateTo,
      a_InactivityStartDateFrom, a_InactivityStartDateTo, a_PetitionType, a_UserId, t_MasterList, 'Y');
    return t_MasterList;

  end ApprovedTermsSearchExtract;

end pkg_ABC_SearchProcedures;
/
