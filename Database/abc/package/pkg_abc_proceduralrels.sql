create or replace package abc.pkg_ABC_ProceduralRels is

  /****************************************************************************
   * Public type declarations
   ***************************************************************************/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_ObjectList is api.udt_ObjectList;

  -------------------------------------
  -- Programmer : Bruce J
  -- Date       : 2011 Aug 31
  -- Purpose    : This gives the legal entity chosen by the user, if one was chosen.
  -------------------------------------
  procedure ChosenLegalEntity (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Scott M
  -- Date       : 2014 Aug 12
  -- Purpose    : This gives a list of legal entities already linked to the user
  --              where the Legal Entity has products
  -------------------------------------
  procedure ProductRenewalLegalEntities (
    a_ObjectId       udt_Id,
    a_EndPoint       udt_Id,
    a_ObjectList     out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Stan H
  -- Date       : 2011 May 23
  -- Purpose    : Create procedural rel to populate License tab on Legal Entity with licenses for which
  --              it is either an operator or a licensee.
  -------------------------------------
  procedure IssuedLicensesForLegalEntity (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Bruce J
  -- Date       : 2011 Aug 16
  -- Purpose    : This returns the latest payment against the object.
  -------------------------------------
  procedure LatestPayment (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Bruce J
  -- Date       : 2011 Aug 16
  -- Purpose    : This gives a list of legal entities already linked to the user.
  -------------------------------------
  procedure PreviousLegalEntities (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  procedure OnlineLicenseLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Modified By  : Joshua Lenon (CXUSA)
  -- Modified Date: 2015 July 31
  -- Purpose      : This returns CPL Submission jobs to the Public Dashboard
  -------------------------------------
  procedure OnlineSubmissionLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Modified By  : Joshua Lenon (CXUSA)
  -- Modified Date: 2015 May 18
  -- Purpose      : This returns CPL Submission jobs to the Public Dashboard
  -------------------------------------
  procedure RecentOnlineSubmissions (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Modified By  : Joshua Lenon (CXUSA)
  -- Modified Date: 2015 May 18
  -- Purpose      : This returns CPL Submission jobs to the Internal License
  -------------------------------------
  procedure LicenseCPLLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 22
  -- Purpose    : This returns the users selected as Online Application Screeners on System Settings object
  -------------------------------------
  procedure SelectedOnlineApplScreeners (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Scott M
  -- Date       : 2014 June 16
  -- Purpose    : This returns the user specified for Default Clerk on the Product Registration job
  -------------------------------------
  procedure SelectedDefaultClerk (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

/* -------------------------------------
  -- Programmer : Scott M
  -- Date       : 2014 June 16
  -- Purpose    : This returns the user specified for Default Revenue Clerk on the Product Registration job
  -------------------------------------
  procedure SelectedRevenueClerk (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );*/

  -------------------------------------
  -- Programmer : Scott M
  -- Date       : 2014 June 16
  -- Purpose    : This returns the user specified for Default Clerk on the Product Registration job
  -------------------------------------
  procedure SelectedDefaultCondProcessor (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 23
  -- Purpose    : This returns the conditions on a license that is related to the process
  --              that is creating the letter
  -------------------------------------
  procedure LicenseConditionsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2014 June 24
  -- Purpose    : Returns list of New Applications and Renewal Applications
  --              according to User Municipality.
  -------------------------------------
  procedure MunicipalApplications (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 23
  -- Purpose    : This returns the conditions on a license that is related to the process
  --              that is creating the letter
  -------------------------------------
  procedure EventDatesForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PetitionsForLetter() -- PUBLIC
   *   Returns all Petition Terms for a Letter
   *-------------------------------------------------------------------------*/
  procedure PetitionsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PetitionType1218ForLetter() -- PUBLIC
   *   Returns all 12.18 Petition Terms for a Letter
   *-------------------------------------------------------------------------*/
  procedure PetitionType1218ForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PetitionType1239ForLetter() -- PUBLIC
   *   Returns all 12.39 Petition Terms for a Letter
   *-------------------------------------------------------------------------*/
  procedure PetitionType1239ForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 28
  -- Purpose    : This returns the Pleadings on an accusation job that is related to the process
  --              that is creating the letter
  -------------------------------------
  procedure AccusationPleadingsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 28
  -- Purpose    : This returns the Violations on an accusation or inspection job that is related
  --              to the process that is creating the letter
  -------------------------------------
  procedure ViolationsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : John P
  -- Date       : 2012 Jan 11
  -- Purpose    : Returns the date range from the latest Suspend License Process
  -------------------------------------
  procedure DatesForAccusation (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * OutstandingFeesForOnlineUser()
   *   Returns the outstanding fees for an Online User
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeesForOnlineUser (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * OutstandingFeesForLegalEntity() -- PUBLIC
   *   Returns the outstanding fees for a Legal Entity
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeesForLegalEntity (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * OutstandingFeesForEComJob()
   *   Returns the outstanding fees for an eCom Job
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeesForEComJob (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * OutstandingFeesForJob() -- PUBLIC
   *   Returns the outstanding fees for a Job
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeesForJob (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) ;

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for New applications witht the same license number. (There shuold meb aonly one)
  -------------------------------------
   procedure AssociatedNewApplications (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Ammendments with this license number.
  -------------------------------------
   procedure AssociatedAmmendments (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Renewals with this license number.
  -------------------------------------
  procedure AssociatedRenewals (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Reinstatements with this license number.
  -------------------------------------
  procedure AssociatedReinstatetments (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Batch Renewals with this license number.
  -------------------------------------
   procedure AssociatedBatchRenewals (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Expirations with this license number.
  -------------------------------------
  procedure AssociatedExpirations (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Accusations with this license number.
  -------------------------------------
   procedure AssociatedAccusations (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Inspections with this license number.
  -------------------------------------
   procedure AssociatedInspections (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2013 Dec 03
  -- Purpose    : This returns License Letter Ids for Inspections with this license number (except this license).
  -------------------------------------
   procedure AssociatedLicenseLetters (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2013 Dec 03
  -- Purpose    : This returns Electronic Document Ids for Inspections with this license number (except this license).
  -------------------------------------
   procedure AssociatedElectronicDocuments (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Iwasam A
  -- Date       : 2013 Dec 03
  -- Purpose    : This returns Online DocumentIds for Inspections with this license number (except this license).
  -------------------------------------
   procedure AssociatedOnlineDocuments (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

 -------------------------------------
 -- Programmer : Iwasam A
 -- Date       : 2013 Dec 03
 -- Purpose    : This returns License ObjectIds for Inspections with this license number
 -------------------------------------
 procedure AssociatedLicenses (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2014 June 18
  -- Purpose    : Determine whether default clerk relationship should be obtained
  --              via License Type (State) or Municipality.
  -------------------------------------
  procedure DefaultClerk (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2014 July 1
  -- Purpose    : Gets list of fees for the Daily Deposit job.
  -------------------------------------
  procedure DailyDepositFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date := sysdate
  );

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2014 September 19
  -- Purpose    : Gets Access Groups for Instance Security on Product object.
  -------------------------------------
  procedure ProductToWWWAccessGroup (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2014 Sep 26
  -- Purpose    : This returns the Products on a Product Registration job (Application,
  --              Amendment, Renewal, Non-Renewal) that is related to the process that is creating the letter
  -------------------------------------
  procedure ProductsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2014 Sep 26
  -- Purpose    : This returns the Distributors on a Product Registration job (Application,
  --              Amendment, Renewal, Non-Renewal) that is related to the process that is creating the letter
  -------------------------------------
  procedure DistributorsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2014 Sep 26
  -- Purpose    : This returns the TAP Distributors on a Product Registration job (Application,
  --              Amendment) that is related to the process that is creating the letter
  -------------------------------------
  procedure TAPDistributorsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Paul Orstad
  -- Date       : 2019 Apr 2
  -- Purpose    : Get the Responsible Legal Entity for a Miscellaneous Revenue Job
  -------------------------------------
  procedure MiscTnsctnResponsibleParty (
    a_JobId                             udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );
  
  /*---------------------------------------------------------------------------
   * AppealResponsibleParty() -- PUBLIC
   *   Get the Responsible Legal Entity for an Appeal Job
   *-------------------------------------------------------------------------*/
  procedure AppealResponsibleParty (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Dalainya B
  -- Date       : 2015 Jan 27
  -- Purpose    : This is used to relate the user selected permittee over the licensee for permits
  -------------------------------------
  procedure PermitteeForPermit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Nathaniel vD
  -- Date       : 2015 Feb 3
  -- Purpose    : Create procedural rel to populate Permit tab on Legal Entity with issued
  -------------------------------------
  procedure PermitForPermitee (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Nathaniel vD
  -- Date       : 2015 Feb 3
  -- Purpose    : Create procedural rel to populate Permit tab on Legal Entity with issued
  -------------------------------------
  procedure IssuedPermitForPermitee (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Dalainya B
  -- Date       : 2015 Feb 10
  -- Purpose    : This returns the Permits where the individual is the Permittee (for Public site)
  --              returns all Permits
  -------------------------------------
  procedure OnlinePermitNoLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Dalainya B
  -- Date       : 2015 Feb 10
  -- Purpose    : This returns the Permits where the individual is the Permitee (for Public site)
  --              only return 10 at a time
  -------------------------------------
  procedure OnlinePermitLimit (
    a_UserId                            udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Shane Gilbert
  -- Date       : 2018 Apr 11
  -- Purpose    : This is used on a visibility expression within stage to improve performance
  -- If any count returns greater than 0 then do not continue checking the other counts
  -------------------------------------
  function HasOnlinePermits (
    a_UserId                            udt_Id
  ) return varchar ;

  -------------------------------------
  -- Programmer : Nathaniel vD
  -- Date       : 2015 Mar 04
  -- Purpose    : Returns all active permits related to the license being renewed
  -------------------------------------
  procedure ActivePermitsForRenewalLicense (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Nathaniel van Diepen
  -- Date       : 2015 March 09
  -- Purpose    : Returns the event dates on a permit for the permitletter
  -------------------------------------
  procedure EventDatesForPermitLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Nathaniel van Diepen
  -- Date       : 2015 March 09
  -- Purpose    : Returns the event dates on a permit for the permitletter
  -------------------------------------
  procedure RainDatesForPermitLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Keaton Leikam
  -- Date       : 2015 April 10
  -- Purpose    : Returns Vehicle Associated with the Permit for use with the Permit Letter
  -------------------------------------
  procedure VehiclesForPermitLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Nathaniel van Diepen
  -- Date       : 2015 March 24
  -- Purpose    : Returns responsible party for a new application
  -------------------------------------
  procedure NewAppResponsibleParty (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2015 Mar 27
  -- Purpose    : Returns Documents from the most recent Major application
  -------------------------------------
  procedure RecentMajorDocs (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  ------------------------------------------------------------------------------------
  -- Programmer : Joshua L
  -- Date       : 2015 March 12
  -- Purpose    : Assigns user to Administrative Review based on Conflict of interest
  ------------------------------------------------------------------------------------
  procedure AdministrativeReviewAssignment  (
    a_JobId                           udt_Id,
    a_Endpoint                        udt_Id,
    a_ObjectList                  out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Josh Camps
  -- Date       : 2015 Apr 15
  -- Purpose    : This gives the responsible legal entity from online or internal,
  --              for the Product Registration Jobs
  -------------------------------------
  procedure ResponsibleLegalEntity (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

 -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2015 July
  -- Purpose    : Returns all related License Renewals for a public user.
  -------------------------------------
  procedure PublicRenewalApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

 -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2015 July
  -- Purpose    : Returns all related users for a renewal app.
  -------------------------------------
  procedure RenewalAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related Product Registrations for a public user.
  -------------------------------------
  procedure PRPublicApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related users for  Product Registration App.
  -------------------------------------
  procedure PRAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related Product Registration Amendments for a public user.
  -------------------------------------
  procedure PRPublicAmendmentApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related users for  Product Registration Amendment app.
  -------------------------------------
  procedure PRAmendmentAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related  Product Registrations Renewal for a public user.
  -------------------------------------
  procedure PRPublicRenewalApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related users for a Product Registration Renewal.
  -------------------------------------
  procedure PRRenewalAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );
  -------------------------------------
  -- Programmer : Matthew J
  -- Date       : 2015 September
  -- Purpose    : Returns all related License Amendments for a public user.
  -------------------------------------
  procedure PublicAmendmentApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Matthew J
  -- Date       : 2015 September
  -- Purpose    : Returns all related users for an Amendment Application.
  -------------------------------------
  procedure AmendmentAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Matthew Johnson
  -- Date       : 2015 September
  -- Purpose    : Returns all related New Applications for a public user.
  -------------------------------------
  procedure PublicNewApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Matthew J
  -- Date       : 2015 September
  -- Purpose    : Returns all related users for a new application.
  -------------------------------------
  procedure NewAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Matthew Johnson
  -- Date       : 2015 September
  -- Purpose    : Returns all related Permit Applications for a public user.
  -------------------------------------
  procedure PublicPermitApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Matthew J
  -- Date       : 2015 September
  -- Purpose    : Returns all related users for a permit application.
  -------------------------------------
  procedure PermitAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * FeesFromePaymentMethod()
   *   Create procedural rel to populate Fees from ePayment Method
   *-------------------------------------------------------------------------*/
  procedure FeesFromePaymentMethod (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  procedure PermitAppResponsibleParty (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

 -------------------------------------
  -- Purpose    : Returns all related Permit Renewals for a public user.
  -------------------------------------
  procedure PublicPermitRenewal (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

 -------------------------------------
  -- Purpose    : Returns all related users for a Permit Renewal.
  -------------------------------------
  procedure PermitRenewalPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns JobIds for Permit Applications with the same Permit Number.
  -------------------------------------
   procedure AssociatedPermitApplications (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Programmer : Elijah R
  -- Date       : 11/09/2015
  -- Purpose    : This returns JobIds for Permit Batch Renewals with the same Permit Number.
  -------------------------------------
   procedure AssociatedPermitBatchRenewals (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns JobIds for Permit Renewals with the same Permit Number.
  -------------------------------------
   procedure AssociatedPermitRenewals (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Date       : 2016 Jan 26
  -- Purpose    : This returns JobIds for Appeal Jobs with this license number.
  -------------------------------------
   procedure AppealJobsForLicense (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Date       : 2016 Jan 26
  -- Purpose    : This returns JobIds for Petition Jobs with this license number.
  -------------------------------------
   procedure PetitionJobsForLicense (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns Responsible Party for Petition Job
  -------------------------------------
   procedure PetitionResponsibleParty (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : Returns all related users for a Petition.
  -------------------------------------
  procedure PetitionPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : Returns all Petitions for a public user.
  -------------------------------------
  procedure PublicPetition (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : Returns the clerk for an expiration job.
  -------------------------------------
  procedure ExpirationDefaultClerk (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : Returns the Additional Warehouse Licenses for a Parent Related License
  -------------------------------------
  procedure AdditionalWarehouses (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns the users selected as Default Clerk on Municipality or License Type
  --              depending on the Issuing Authority of the License
  --              This procedure is also called from the Online Application Screener (PRC) relationship
  --              on Amendment Job
  -------------------------------------
  procedure SelectedOnlineRenewalScreeners (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns the latest version of a License for a Permit.
  -------------------------------------
  procedure LatestLicenseForPermit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns all Vehicles with Permits that were NOT cancelled by the system
  -------------------------------------
  procedure VehiclesForLE (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns all CTW Permit Owners
  -------------------------------------
  procedure PermitOnlineOwner (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns all CTW Permit Solicitors
  -------------------------------------
  procedure PermitOnlineSolicitor (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  -------------------------------------
  -- Purpose    : This returns the person to whom the process should be assigned
  -------------------------------------
  procedure AssigneeNewInformation (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PoliceReviews()
   *   Returns list of Police Review Processes on New and Renewal Permit
   * Applications based on Police User Municipalities.
   *-------------------------------------------------------------------------*/
  procedure PoliceReviews (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * MunicipalReviews()
   *   Returns list of Municipal Review Processes on New and Renewal Permit
   * Applications based on the Municipality associated with the Municipal User.
   *-------------------------------------------------------------------------*/
  procedure MunicipalReviews (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * MunicipalPoliceReviewDocs()
   *   Returns all documents submitted by Municipal and Police Users through
   * the Municipal and Police Review processes.
   *-------------------------------------------------------------------------*/
  procedure MunicipalPoliceReviewDocs (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * MunicipalPoliceReviewResponses()
   *   Given a ProcessId or JobId, return all Municipal and Police Review
   * Processes related to the job.
   *-------------------------------------------------------------------------*/
  procedure MunicipalPoliceReviewResponses (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * GetUserToDoNotes()
   *   Given a UserId, return all To Do Notes and their associated incomplete
   * Processes
   *-------------------------------------------------------------------------*/
  procedure GetUserToDoNotes(
   a_ObjectId                      udt_Id,
   a_Endpoint                      udt_Id,
   a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PublicDeniedLicenses()
   *   Returns all Denied Applications related to a Public User.
   *-------------------------------------------------------------------------*/
  procedure PublicDeniedLicenses (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PublicDeniedPermits()
   *   Returns all Denied Permit Applications related to a Public User.
   *-------------------------------------------------------------------------*/
  procedure PublicDeniedPermits (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PublicDeniedProducts()
   *   Returns all Denied Product Registration Applications related to a Public
   * User.
   *-------------------------------------------------------------------------*/
  procedure PublicDeniedProducts (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PermitsByTypePerPermittee()
   *   Returns all Permits of a certain type that have been issued to the
   * Permittee for this calendar year. Excludes Cancelled and Revoked permits.
   *-------------------------------------------------------------------------*/
  procedure PermitsByTypePerPermittee (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * LicensesForPetition()
   *   Returns all valid Licenses for a Petition application based on the
   * selected Petition Type.
   *-------------------------------------------------------------------------*/
  procedure LicensesForPetition (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * LicenseReliefTerms()
   *   Returns all petition terms for the license on a Petition application
   * that match the application's selected petition type
   *-------------------------------------------------------------------------*/
  procedure LicenseReliefTerms (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PublicLicensesWithAppTermsAll()
   *   Returns all licenses that are associated wtih approved petitions
   *-------------------------------------------------------------------------*/
  procedure PublicLicensesWithAppTermsAll (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PublicLicensesWithAppTermsLimit()
   *   Returns a set number licenses that are associated wtih approved
   * petitions based on ShowMore value
   *-------------------------------------------------------------------------*/
  procedure PublicLicensesWithAppTermsLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * LicensesForPetitionAmendment()
   *   Returns all valid Licenses for a Petition amendment based on the
   * selected Petition Amendment Type.
   *-------------------------------------------------------------------------*/
  procedure LicensesForPetitionAmendment (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * LicenseUnapprovedTerms()
   *   Returns draft, pending, and rejected terms related to license.
   *-------------------------------------------------------------------------*/
  procedure LicenseUnapprovedTerms (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * PublicPetitionAmendment()
   *   Returns all petition amendments for a public user
   *-------------------------------------------------------------------------*/
  procedure PublicPetitionAmendment (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  );

end pkg_ABC_ProceduralRels;
/
create or replace package body pkg_ABC_ProceduralRels is

  g_OnlineGridLimit                     pls_integer := 10;

  -------------------------------------
  -- Programmer : Stan H
  -- Date       : 2011 May 23
  -- Purpose    : Create procedural rel to populate License tab on Legal Entity with licenses for which
  --              it is either an operator or a licensee.
  -------------------------------------
  procedure IssuedLicensesForLegalEntity (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select distinct LicenseObjectId
      bulk collect into t_ObjectIdList
      from (select LegalEntityObjectId, LicenseObjectId
              from query.r_ABC_LicenseOperator
              where LegalEntityObjectId = a_ObjectId
            union
            select LegalEntityObjectId, LicenseObjectId
              from query.r_ABC_LicenseLicenseeLE
              where LegalEntityObjectId = a_ObjectId)
      where api.pkg_ColumnQuery.Value(LicenseObjectId, 'dup_HasBeenIssued') = 'Y';

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end IssuedLicensesForLegalEntity;

  -------------------------------------
  -- Programmer : Bruce J
  -- Date       : 2011 Aug 16
  -- Purpose    : This gives a list of legal entities already linked to the user.
  -- Updated on 10/9 by Caleb T - Converted to use pkg_search and to only find active legal entities.
  -- Updated on 10/29/15 by Ryan den Otter - Performance Tuning.
  -------------------------------------
  procedure PreviousLegalEntities (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_FinalList                         udt_IdList;
    t_UserLegalEntityEPID               udt_id := api.pkg_configquery.EndPointIdForName('u_Users', 'LegalEntity');
    t_LegalEntityList                   udt_IdList;
    t_CurrentUser                       udt_id;
  begin
    a_ObjectList := api.udt_ObjectList();
    t_CurrentUser := api.pkg_SecurityQuery.EffectiveUserId;
    t_LegalEntityList := api.pkg_ObjectQuery.RelatedObjects(t_CurrentUser, t_UserLegalEntityEPID);

    for i in 1..t_LegalEntityList.Count loop
      if api.pkg_ColumnQuery.Value(t_LegalEntityList(i),'Active') = 'Y' then
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_LegalEntityList(i));
      end if;
    end loop;

  end PreviousLegalEntities;

 -------------------------------------
  -- Programmer : Scott M
  -- Date       : 2014 Aug 12
  -- Purpose    : This gives a list of legal entities already linked to the user
  --              where the Legal Entity has products
  -------------------------------------
  procedure ProductRenewalLegalEntities (
    a_ObjectId       udt_Id,
    a_EndPoint       udt_Id,
    a_ObjectList     out api.udt_ObjectList
  ) is
    t_ObjectIdList   udt_IdList;

  begin
    -- Limit Legal Entities to those that have Products
    select distinct r.LegalEntityObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_UserLegalEntity r
     where r.UserId = api.pkg_SecurityQuery.EffectiveUserId()
       and exists (select 1 from query.r_ABC_ProductRegistrant r1 where r1.RegistrantObjectId = r.LegalEntityObjectId);

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end ProductRenewalLegalEntities;

  -------------------------------------
  -- Programmer : Bruce J
  -- Date       : 2011 Aug 31
  -- Purpose    : This gives the legal entity chosen by the user, if one was chosen.
  -------------------------------------
  procedure ChosenLegalEntity (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineUseLegalEntityObjectId')
      bulk collect into t_ObjectIdList
      from dual;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end ChosenLegalEntity;

  -------------------------------------
  -- Programmer : Bruce J
  -- Date       : 2011 Aug 16
  -- Purpose    : This returns the latest payment against the object.
  -------------------------------------
  procedure LatestPayment (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select op.ObjectId
      bulk collect into t_ObjectIdList
      from api.fees f
        join api.FeeTransactions ft
          on f.FeeId = ft.FeeId
        join api.PaymentTransactions pt
          on ft.TransactionId = pt.TransactionId
        join api.RegisteredExternalObjects reo
          on reo.LinkValue = to_char(pt.PaymentId)
        join query.o_Payment op
          on reo.ObjectId = op.ObjectId
      where f.JobId = a_ObjectId
        and op.PaymentDate = (
          select max(op.PaymentDate)
            from api.fees f
              join api.FeeTransactions ft
                on f.FeeId = ft.FeeId
              join api.PaymentTransactions pt
                on ft.TransactionId = pt.TransactionId
              join api.RegisteredExternalObjects reo
                on reo.LinkValue = to_char(pt.PaymentId)
              join query.o_Payment op
                on reo.ObjectId = op.ObjectId
            where f.JobId = a_ObjectId);

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end LatestPayment;

  -------------------------------------
  -- Modified By  : Andy Patterson (CXUSA)
  -- Modified Date: 2015 May 18
  -- Purpose      : This returns License objects to the Public Dashboard
  -------------------------------------
  procedure OnlineLicenseLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is

  begin
    a_ObjectList := api.udt_ObjectList();

    for a in (select rr.LicenseObjectId, l.ShowOnline
                from query.r_Abc_Userlegalentity r
                join query.r_Abc_Licenselicenseele rr
                  on r.LegalEntityObjectId = rr.LegalEntityObjectId
                join query.o_Abc_License l
                  on l.ObjectId = rr.LicenseObjectId
               where r.UserId = a_ObjectId) loop

      if a.ShowOnline = 'Y' then
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(a.LicenseObjectId);
        if a_ObjectList.Count = 10 then
          exit;
        end if;
      end if;
    end loop;
  end;

  -------------------------------------
  -- Modified By  : Joshua Lenon (CXUSA)
  -- Modified Date: 2015 May 18
  -- Purpose      : This returns CPL Submission jobs to the Public Dashboard
  -------------------------------------
  procedure OnlineSubmissionLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_Final                             udt_IdList;
    t_ObjectIdList                      udt_IdList;
  begin
    --a_ObjectList := api.udt_ObjectList();

    select distinct clu.CPLSubmissionId
      bulk collect into t_ObjectIdList
      from query.r_ABC_OnlineCPLUser clu
     where clu.UserId = a_ObjectId
       and api.pkg_columnquery.value(clu.CPLSubmissionId, 'OnlineApproved') = 'Y'
       and api.pkg_columnquery.datevalue(clu.CPLSubmissionId, 'CreatedDate')
          between add_months(sysdate, -12) and sysdate;

    if t_ObjectIdList.count > 10 then
      for i in 1..10 loop
        t_Final(i) := t_ObjectIdList(i);
      end loop;
      a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_Final);
    else
      a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
    end if;
        --a_ObjectList.Extend(1);
        --a_ObjectList(a_ObjectList.Count) := api.udt_Object(a.CPLSubmissionId);

  end;

  -------------------------------------
  -- Modified By  : Joshua Lenon (CXUSA)
  -- Modified Date: 2015 May 18
  -- Purpose      : This returns CPL Submission jobs to the Public Dashboard
  -------------------------------------
  procedure RecentOnlineSubmissions (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_Final                             udt_IdList;
    t_ObjectIdList                      udt_IdList;
  begin
    --a_ObjectList := api.udt_ObjectList();

    select distinct clu.CPLSubmissionId
      bulk collect into t_ObjectIdList
      from query.r_ABC_OnlineCPLUser clu
     where clu.UserId = a_ObjectId
       and api.pkg_columnquery.value(clu.CPLSubmissionId, 'OnlineApproved') = 'Y'
       and api.pkg_columnquery.datevalue(clu.CPLSubmissionId, 'CreatedDate')
          between add_months(sysdate, -12) and sysdate;
dbms_output.put_line(t_ObjectIdList.count);

        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
        --a_ObjectList.Extend(1);
        --a_ObjectList(a_ObjectList.Count) := api.udt_Object(a.CPLSubmissionId);

  end;

  -------------------------------------
  -- Modified By  : Joshua Lenon (CXUSA)
  -- Modified Date: 2015 May 18
  -- Purpose      : This returns CPL Submission jobs to the Internal License
  -------------------------------------
  procedure LicenseCPLLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_Final                             udt_IdList;
    t_ObjectIdList                      udt_IdList;
  begin
    --a_ObjectList := api.udt_ObjectList();

    select cl.CPLJobId
      bulk collect into t_ObjectIdList
      from query.r_ABC_CPLLicense cl
     where cl.LicenseObjectId = a_ObjectId
       and api.pkg_columnquery.datevalue(cl.CPLJobId, 'CreatedDate')
          between add_months(sysdate, -12) and sysdate;

        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end;

 -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 22
  -- Purpose    : This returns the users selected as Online Application Screeners on System Settings object
  -------------------------------------
  procedure SelectedOnlineApplScreeners (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select r.UserId
      bulk collect into t_ObjectIdList
      from query.r_Onlineapplscreenersyssetting r;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end SelectedOnlineApplScreeners;

 -------------------------------------
  -- Programmer : Scott M
  -- Date       : 2014 June 16
  -- Purpose    : This returns the user specified for Default Clerk on the Product Registration job
  -------------------------------------
  procedure SelectedDefaultClerk (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    if api.pkg_columnquery.value(a_ObjectId,'DefaultClerk') is null then
      if api.pkg_columnquery.value(a_ObjectId,'EnteredOnline') = 'Y' then

        select r.UserId
          bulk collect into t_ObjectIdList
          from query.r_DefaultPRClerkSysSetting r;

      else

        select api.pkg_securityquery.EffectiveUserId()
          bulk collect into t_ObjectIdList
          from dual;

      end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
    end if;
  end SelectedDefaultClerk;

/* -------------------------------------
  -- Programmer : Scott M
  -- Date       : 2014 June 16
  -- Purpose    : This returns the user specified for Default Revenue Clerk on the Product Registration job
  -------------------------------------
  procedure SelectedRevenueClerk (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select r.UserId
      bulk collect into t_ObjectIdList
      from query.r_DefaultRevClerkSysSetting r;
    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end SelectedRevenueClerk;*/

   -------------------------------------
  -- Programmer : Scott M
  -- Date       : 2014 June 16
  -- Purpose    : This returns the user specified for Default Clerk on the Product Registration job
  -------------------------------------
  procedure SelectedDefaultCondProcessor (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin

    select r.UserId
      bulk collect into t_ObjectIdList
      from query.r_DefaultCondProcessSysSetting r;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end SelectedDefaultCondProcessor;

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 23
  -- Purpose    : This returns the conditions on a license that is related to the process
  --              that is creating the letter
  -------------------------------------
  procedure LicenseConditionsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    if api.pkg_columnquery.value(a_ObjectId,'DefaultRevenueClerk') is null then
    select lc.ConditionObjectId
    bulk collect into t_ObjectIdList
    from query.r_abc_licensecondition lc
    where lc.LicenseObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'LicenseObjectId');
    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
    end if;
  end LicenseConditionsForLetter;

-------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 August 25
  -- Purpose    : Returns list of New Applications and Renewal Applications
  --              according to User Municipality.
  -------------------------------------
  procedure MunicipalApplications (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_IdList                            udt_IdList;
    t_IdListFinal                       udt_IdList;
    t_MunicipalName                    varchar2(50) :=
        api.pkg_ColumnQuery.Value(a_ObjectId, 'MunicipalityName');
    t_MunicipalId                       udt_Id;
    t_ShowMore                          varchar2(1) :=
        api.pkg_ColumnQuery.Value(a_ObjectId, 'MunicipalApplicationsShowMore');
    t_StatusId                          udt_Id;
    t_ReliefStatusId                    udt_Id;
  begin

    select s.StatusId
      into t_StatusId
      from api.Statuses s
     where s.Tag = 'AWAIT';
    select s.StatusId
      into t_ReliefStatusId
      from api.Statuses s
     where s.Tag = 'RELIEF';

/*  Municipality Name is not unique enough, NJ has several municipalities with the same name.
    begin
      select o.objectid
        into t_MunicipalId
        from query.o_ABC_Office o
       where o.Name = t_MunicipalName;
    exception when no_data_found then
      api.pkg_Errors.RaiseError(-20000, 'A municipality is not related to your user please contact to POSSE ABC system administrator.');
    end;*/

    -- MunicipalityNameId is unique, so use this.
    if api.pkg_columnquery.NumericValue(a_Objectid,'MunicipalityNameId') is null then
       api.pkg_Errors.RaiseError(-20000, 'A municipality is not related to your user; please contact the POSSE ABC system administrator.');
    else
       t_MunicipalId := api.pkg_columnquery.NumericValue(a_Objectid,'MunicipalityNameId');
    end if;

    select ObjectId
      bulk collect into t_IdList
      from (select sr.ObjectId       ObjectId
                 , j.ExternalFileNum FileNum
              from query.p_ABC_SubmitResolution sr
              join api.Jobs j
                on sr.JobId = j.JobId
              join api.Statuses s
                on j.StatusId = s.StatusId
             where sr.ObjectDefTypeId = 3
                   and api.pkg_ColumnQuery.Value(j.JobId, 'OfficeObjectId') = t_MunicipalId
                   and sr.outcome is null
                   and s.StatusId = t_StatusId
         union
            select sr.ObjectId       ObjectId
                 , j.ExternalFileNum FileNum
              from query.p_ABC_ConfirmRelief sr
              join api.Jobs j
                on sr.JobId = j.JobId
              join api.Statuses s
                on j.StatusId = s.StatusId
             where sr.ObjectDefTypeId = 3
                   and api.pkg_ColumnQuery.Value(j.JobId, 'OfficeObjectId') = t_MunicipalId
                   and sr.outcome is null
                   and s.StatusId = t_ReliefStatusId
             order by FileNum);

    if t_ShowMore = 'Y' then
      t_IdListFinal := t_IdList;
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'MunicipalApplicationsShowMore', 'N');
    else
      -- make sure we don't try to access elements that don't exist
      -- we can just assign it in this case.
      if t_IdList.count < 10 then
        t_IdListFinal := t_IdList;
      else
        for i in 1..10 loop
          t_IdListFinal(i) := t_IdList(i);
        end loop;
      end if;
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_IdListFinal);
  end MunicipalApplications;

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 23
  -- Purpose    : This returns the conditions on a license that is related to the process
  --              that is creating the letter
  -------------------------------------
  procedure EventDatesForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select ev.EventDateObjectId
    bulk collect into t_ObjectIdList
    from query.r_abc_licenseeventdate ev
    where ev.LicenseObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'LicenseObjectId');

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end EventDatesForLetter;

  /*---------------------------------------------------------------------------
   * PetitionsForLetter() -- PUBLIC
   *   Returns all Petition Terms for a Letter
   *-------------------------------------------------------------------------*/
  procedure PetitionsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_JobDefName                        varchar2(30);
    t_JobId                             udt_Id;
    t_ObjectIdList                      udt_IdList;
  begin

    t_JobId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'RelatedJobId');
    t_JobDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    
    if t_JobDefName = 'j_ABC_Petition' then
      select r.PetitionTermObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_PetitionPetitionTerm r
      where r.PetitionObjectId = t_JobId
      order by lpad(api.pkg_columnquery.value(r.PetitionTermObjectId, 'PetitionType'), 10),
          lpad(api.pkg_columnquery.value(r.PetitionTermObjectId, 'TermStartYear'), 10);
    elsif t_JobDefName = 'j_ABC_PetitionAmendment' then
      select r.PetitionTermObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_PetitionAmendTerms r
      where r.PetitionAmendmentId = t_JobId
      order by lpad(api.pkg_columnquery.value(r.PetitionTermObjectId, 'PetitionType'), 10),
          lpad(api.pkg_columnquery.value(r.PetitionTermObjectId, 'TermStartYear'), 10);
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end PetitionsForLetter;

  /*---------------------------------------------------------------------------
   * PetitionType1218ForLetter() -- PUBLIC
   *   Returns all 12.18 Petition Terms for a Letter
   *-------------------------------------------------------------------------*/
  procedure PetitionType1218ForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_JobDefName                        varchar2(30);
    t_JobId                             udt_Id;
    t_ObjectIdList                      udt_IdList;
  begin

    t_JobId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'RelatedJobId');
    t_JobDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    
    if t_JobDefName = 'j_ABC_Petition' then
      select r.PetitionTermObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_PetitionPetitionTerm r
      where r.PetitionObjectId = t_JobId
        and api.pkg_ColumnQuery.Value(r.PetitionTermObjectId, 'PetitionType') = '12.18';
    elsif t_JobDefName = 'j_ABC_PetitionAmendment' then
      select r.PetitionTermObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_PetitionAmendTerms r
      where r.PetitionAmendmentId = t_JobId
        and api.pkg_ColumnQuery.Value(r.PetitionTermObjectId, 'PetitionType') = '12.18';
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end PetitionType1218ForLetter;

  /*---------------------------------------------------------------------------
   * PetitionType1239ForLetter() -- PUBLIC
   *   Returns all 12.39 Petition Terms for a Letter
   *-------------------------------------------------------------------------*/
  procedure PetitionType1239ForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_JobDefName                        varchar2(30);
    t_JobId                             udt_Id;
    t_ObjectIdList                      udt_IdList;
  begin

    t_JobId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'RelatedJobId');
    t_JobDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    
    if t_JobDefName = 'j_ABC_Petition' then
      select r.PetitionTermObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_PetitionPetitionTerm r
      where r.PetitionObjectId = t_JobId
        and api.pkg_ColumnQuery.Value(r.PetitionTermObjectId, 'PetitionType') = '12.39';
    elsif t_JobDefName = 'j_ABC_PetitionAmendment' then
      select r.PetitionTermObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_PetitionAmendTerms r
      where r.PetitionAmendmentId = t_JobId
        and api.pkg_ColumnQuery.Value(r.PetitionTermObjectId, 'PetitionType') = '12.39';
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end PetitionType1239ForLetter;

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 28
  -- Purpose    : This returns the Pleadings on an accusation job that is related to the process
  --              that is creating the letter
  -------------------------------------
  procedure AccusationPleadingsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select r.PleadingObjectId
    bulk collect into t_ObjectIdList
    from query.r_abc_accusationpleading r
    where r.AccusationObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'RelatedJobId');

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end AccusationPleadingsForLetter;

  -------------------------------------
  -- Programmer : Dalainya M
  -- Date       : 2011 Dec 28
  -- Purpose    : This returns the Violations on an accusation or inspection job that is related
  --              to the process that is creating the letter
  -------------------------------------
  procedure ViolationsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select r.ToObjectId
    bulk collect into t_ObjectIdList
    from api.relationships r
    join api.endpoints ep
    on ep.EndPointId = r.EndPointId
    where (r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_Accusation', 'Violation') or r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_Inspection', 'Violation'))
    and r.FromObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'RelatedJobId');

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end ViolationsForLetter;

  -------------------------------------
  -- Programmer : John P
  -- Date       : 2012 Jan 11
  -- Purpose    : Returns the date range from the latest Suspend License Process
  -------------------------------------
  procedure DatesForAccusation (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_SuspendLicenseId                  udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId, 'SuspendLicenseId');
    t_ObjectIdList                 udt_IdList := extension.pkg_ObjectQuery.RelatedObjects(t_SuspendLicenseId, 'SuspensionDate');
  begin
    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end DatesForAccusation;

  /*---------------------------------------------------------------------------
   * OutstandingFeesForOnlineUser() -- PUBLIC
   *   Returns the outstanding fees for an Online User
   *   Only includes fees from associated Legal Entities, which excludes jobs 
   *    where the user opted to create a new LE.
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeesForOnlineUser (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_LEObjectIdList                    udt_IdList;
    t_FeesList                          api.udt_ObjectList;
  begin
    a_ObjectList := api.udt_ObjectList();
    t_LEObjectIdList := api.pkg_Search.ObjectsByRelColumnValue('o_ABC_LegalEntity',
        'OnlineUser', 'ObjectId', a_ObjectId);
    for i in 1..t_LEObjectIdList.count() loop
      t_FeesList := api.udt_ObjectList();
      OutstandingFeesForLegalEntity(t_LEObjectIdList(i), null, t_FeesList);
      extension.pkg_collectionutils.Append(a_ObjectList, t_FeesList);
    end loop;

  end OutstandingFeesForOnlineUser;

  /*---------------------------------------------------------------------------
   * OutstandingFeesForLegalEntity() -- PUBLIC
   *   Returns the outstanding fees for a Legal Entity
   *   Called from: 
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeesForLegalEntity (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_FeeObjDefId                       udt_Id
        := api.pkg_ConfigQuery.ObjectDefIdForName('o_Fee');
  begin
    select api.udt_object(reo.objectid)
    bulk collect into a_ObjectList
      from api.fees f
      join api.registeredexternalobjects reo
          on reo.LinkValue = f.feeid
         and reo.ObjectDefId = t_FeeObjDefId
      join api.jobs j
          on f.JobId = j.JobId
      join api.jobstatuses js
          on j.StatusId = js.StatusId
         and j.JobTypeId = js.JobTypeId
      join api.jobtypes jt
          on jt.JobTypeId = j.JobTypeId
    where f.ResponsibleObjectId = a_ObjectId
      and (f.amount + f.PaidAmount + f.adjustedamount) > 0
      and (js.StatusType in ('O', 'C')
           or (js.StatusType = 'I'
               and (api.pkg_ColumnQuery.value(f.jobid, 'AddtoPayAllFees') = 'Y'
                    or nvl(api.pkg_ColumnQuery.value(f.jobid, 'EnteredOnline'), 'N') = 'N')))
      and jt.name in (
          'j_ABC_Accusation',
          'j_ABC_AmendmentApplication',
          'j_ABC_NewApplication',
          'j_ABC_Reinstatement',
          'j_ABC_RenewalApplication',
          'j_ABC_PRApplication',
          'j_ABC_PRRenewal',
          'j_ABC_PRAmendment',
          'j_ABC_PermitApplication',
          'j_ABC_PermitRenewal',
          'j_ABC_Petition',
          'j_ABC_PetitionAmendment',
          'j_ABC_Appeal');

  end OutstandingFeesForLegalEntity;

  /*---------------------------------------------------------------------------
   * OutstandingFeesForEComJob() -- PUBLIC
   *   Returns the outstanding fees for an eCom Job
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeesForEComJob (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_FeeList                           api.udt_ObjectList;
    t_SourceObjectId                    udt_Id;
    t_ePaymentMethodId                  udt_Id;
    t_IgnoreFeeList                     udt_IdList;
  begin
    a_ObjectList := api.udt_ObjectList();
    t_FeeList := api.udt_ObjectList();

    --Get the source object id
    t_SourceObjectId := api.pkg_columnquery.value(a_Objectid, 'SourceObjectId');
    if t_SourceObjectId is null then
      t_SourceObjectId := api.pkg_columnquery.value(a_Objectid, 'SourceObjectIdCalculated');
      if t_SourceObjectId is null then
        api.pkg_errors.RaiseError(-20000, 'Can''t get source object id');
      end if;
    end if;

    if api.pkg_ColumnQuery.Value(t_SourceObjectId, 'ObjectDefName') = 'o_ABC_LegalEntity' then
      OutstandingFeesForLegalEntity(t_SourceObjectId, null, t_FeeList);
    else
      OutstandingFeesForJob(t_SourceObjectId, null, t_FeeList);
    end if;
    
    t_ePaymentMethodId := api.pkg_Search.ObjectByColumnValue('o_ePaymentMethod', 'eCOMJobId', a_ObjectId);
    t_IgnoreFeeList := api.pkg_Search.ObjectsByRelColumnValue('o_Fee', 'IgnoreePayment', 'ObjectId', t_ePaymentMethodId);
    
    <<FeeListLoop>>
    for i in 1..t_FeeList.count() loop
      for c in 1..t_IgnoreFeeList.count() loop
        continue FeeListLoop when t_FeeList(i).ObjectId = t_IgnoreFeeList(c);
      end loop;
      -- If fee not in the ignore list, add it to our output
      a_ObjectList.extend(1);
      a_ObjectList(a_ObjectList.count()) := t_FeeList(i);
    end loop;
  end OutstandingFeesForEComJob;

  /*---------------------------------------------------------------------------
   * OutstandingFeesForJob() -- PUBLIC
   *   Returns the outstanding fees for a Job
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeesForJob (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_FeeObjDefId                       udt_Id
        := api.pkg_ConfigQuery.ObjectDefIdForName('o_Fee');
  begin
    -- Get all of the Fees related to the source object.
    select api.udt_object(reo.objectid)
      bulk collect into a_ObjectList
      from api.jobs j
      join api.fees f
          on f.JobId = j.JobId
      join api.registeredexternalobjects reo
          on reo.LinkValue = f.feeid
         and reo.ObjectDefId = t_FeeObjDefId
      join api.jobstatuses js
          on j.StatusId = js.StatusId
         and j.JobTypeId = js.JobTypeId
     where f.JobId = a_ObjectId
       and (f.amount + f.PaidAmount + f.adjustedamount) > 0
       and (js.StatusType in ('O', 'C')
            or (js.StatusType = 'I'));

  end OutstandingFeesForJob;

-------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for New applications with this license number. (There should be only one)
  -------------------------------------
   procedure AssociatedNewApplications (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.NewApplicationJobId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x

        join query.r_ABC_NewApplicationLicense r on r.LicenseObjectId = x.objectid
        union
        select r.NewApplicationJobId
          from query.r_abc_newapplicationlicense r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedNewApplications;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Ammendments with this license number.
  -------------------------------------
   procedure AssociatedAmmendments (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.AmendJobId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x

        join query.r_ABC_LicenseToAmendJobLicense r on r.LicenseObjectId = x.objectid
        union
        select r.AmendJobId
          from query.r_ABC_LicenseToAmendJobLicense r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedAmmendments;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Renewals with this license number.
  -------------------------------------
   procedure AssociatedRenewals (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.RenewJobId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x

        join query.r_ABC_LicenseToRenewJobLicense r on r.LicenseObjectId = x.objectid
        union
        select r.RenewJobId
          from query.r_ABC_LicenseToRenewJobLicense r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedRenewals;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Reinstatements with this license number.
  -------------------------------------
     procedure AssociatedReinstatetments (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.ReinstatementJobId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x

        join query.r_ABC_LicenseToReinstate r on r.LicenseObjectId = x.objectid
         union
        select r.ReinstatementJobId
          from query.r_ABC_LicenseToReinstate r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedReinstatetments;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Batch Renewals with this license number.
  -------------------------------------
   procedure AssociatedBatchRenewals (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.BatchRenewalNotificationJobId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x

        join query.r_ABC_BatchRenewalJobLicense r on r.LicenseObjectId = x.objectid
         union
        select r.BatchRenewalNotificationJobId
          from query.r_ABC_BatchRenewalJobLicense r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedBatchRenewals;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Expirations with this license number.
  -------------------------------------
   procedure AssociatedExpirations (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.ExpirationJobId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x

        join query.r_ABC_ExpirationLicense r on r.LicenseObjectId = x.objectid
         union
        select r.ExpirationJobId
          from query.r_ABC_ExpirationLicense r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedExpirations;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Accusations with this license number.
  -------------------------------------
   procedure AssociatedAccusations (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.AccusationJobId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x

        join query.r_ABC_AccusationLicense r on r.LicenseObjectId = x.objectid
         union
        select r.AccusationJobId
          from query.r_ABC_AccusationLicense r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedAccusations;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns JobIds for Inspections with this license number.
  -------------------------------------
   procedure AssociatedInspections (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.InspectionJobId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x

        join query.r_ABC_InspectionJobLicense r on r.LicenseObjectId = x.objectid
         union
        select r.InspectionJobId
          from query.r_ABC_InspectionJobLicense r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedInspections;

-------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns License Letter Ids for Inspections with this license number (except this license).
  -------------------------------------
   procedure AssociatedLicenseLetters (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.LicenseLetterObjectId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x
      join query.r_ABC_LicenseLetterLicense r on r.LicenseObjectId = x.objectid
        where x.objectid != a_ObjectId;

        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedLicenseLetters;

-------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns Electronic Document Ids for Inspections with this license number (except this license).
  -------------------------------------
   procedure AssociatedElectronicDocuments (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.DocumentId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x
      join query.r_ABC_ElectronicDocument r on r.LicenseObjectId = x.objectid

        where x.objectid != a_ObjectId;

        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedElectronicDocuments;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns Online DocumentIds for Inspections with this license number (except this license).
  -------------------------------------
   procedure AssociatedOnlineDocuments (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select r.DocumentId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x
        join query.r_ABC_OnlineDocument r on r.LicenseObjectId = x.objectid

        where x.objectid != a_ObjectId
         union
        select r.DocumentId
          from query.r_ABC_OnlineDocument r
        where r.LicenseObjectId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedOnlineDocuments;

  -------------------------------------
  -- Programmer : Iwasam A

  -- Date       : 2014 Jan 13
  -- Purpose    : This returns License ObjectIds for Inspections with this license number
                  --It is used to return notes, hence the where clause
  -------------------------------------
   procedure AssociatedLicenses (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

begin
    select x.objectid
       bulk collect into t_ObjectIdList
       from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'dup_LicenseNumberNoGeneration', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumberNoGeneration')) as api.udt_ObjectList)) x
       where api.pkg_columnquery.value(x.objectid, 'HasNotes')  = 'Y';
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
end AssociatedLicenses;

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2014 June 18
  -- Purpose    : Determine whether default clerk relationship should be obtained
  --              via License Type (State) or Municipality.
  -------------------------------------

  procedure DefaultClerk (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_IssuingAuthority                  varchar2(12);
  begin
    t_IssuingAuthority := api.pkg_ColumnQuery.Value(a_ObjectId, 'IssuingAuthority');

    if t_IssuingAuthority = 'State' then
      select dc.DefaultClerkId
        bulk collect into t_ObjectIdList
        from query.r_ABC_LicenseLicenseType lt
        join query.r_ABC_LicenseTypeDefaultClerk dc
          on lt.LicenseTypeObjectId = dc.LicenseTypeId
       where lt.LicenseObjectId = a_ObjectId;
    elsif t_IssuingAuthority = 'Municipality' then
      select c.ClerkObjectId
        bulk collect into t_ObjectIdList
        from query.r_ABC_LicenseOffice m
        join query.r_ClerkMunicipality c
          on m.OfficeObjectId = c.MunicipalityObjectId
       where m.LicenseObjectId = a_ObjectId;
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end DefaultClerk;

  ------------------------------------------------------------------------------------
  -- Programmer : Joshua L
  -- Date       : 2015 March 12
  -- Purpose    : Assigns user to Administrative Review based on Conflict of interest
  ------------------------------------------------------------------------------------
    procedure AdministrativeReviewAssignment (
      a_JobId                           udt_Id,
      a_Endpoint                        udt_Id,
      a_ObjectList                  out api.udt_ObjectList
    ) is
      t_ObjectIdList                    udt_IdList;
      t_JobDef                          varchar2(30) := api.pkg_columnquery.value(a_JobId, 'ObjectDefDescription');
      t_Conflict                        varchar2(10) := api.pkg_columnquery.value(a_JobId, 'IsConflictOfInterest');
      t_IssuingAuthority                varchar2(12) := api.pkg_ColumnQuery.Value(a_JobId, 'IssuingAuthority');

    begin

      if t_JobDef = 'New Application' then
        If t_IssuingAuthority = 'Municipality' and t_Conflict = 'Yes' then
          select s.DefaultSupervisorObjectId bulk collect
            into t_ObjectIdList
            from query.j_ABC_NewApplication s
           where s.ObjectId = a_JobId;
        else
          select s.DefaultClerkObjectId bulk collect
            into t_ObjectIdList
            from query.j_ABC_NewApplication s
           where s.ObjectId = a_JobId;
        end if;

      elsif t_JobDef = 'Renewal Application' then
        If t_IssuingAuthority = 'Municipality' and t_Conflict = 'Yes' then
          select s.DefaultSupervisorObjectId bulk collect
            into t_ObjectIdList
            from query.j_ABC_RenewalApplication s
           where s.ObjectId = a_JobId;
        else
          select s.DefaultClerkObjectId bulk collect
            into t_ObjectIdList
            from query.j_ABC_RenewalApplication s
           where s.ObjectId = a_JobId;
        end if;

      elsif t_JobDef = 'Amendment Application' then
        If t_IssuingAuthority = 'Municipality' and t_Conflict = 'Yes' then
          select s.DefaultSupervisorObjectId bulk collect
            into t_ObjectIdList
            from query.j_ABC_AmendmentApplication s
           where s.ObjectId = a_JobId;
        else
          select s.DefaultClerkObjectId bulk collect
            into t_ObjectIdList
            from query.j_ABC_AmendmentApplication s
           where s.ObjectId = a_JobId;
        end if;

      end if;

      a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

    end AdministrativeReviewAssignment;

  -------------------------------------
  -- Programmer : Caleb T
  -- Date       : 2015 May 26th
  -- Purpose    : Gets list of fees for the Daily Deposit job.
  -------------------------------------
    procedure DailyDepositFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date := sysdate
  ) is
    t_DepositDate                       date := api.pkg_ColumnQuery.DateValue(a_ObjectId, 'DepositDate');
    t_EndPointId                        udt_Id :=
      api.pkg_ConfigQuery.EndPointIdForName('j_ABC_DailyDeposit', 'NoDepositPayment');
    t_RelId                             udt_Id;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_Objects                           api.udt_Objectlist;
  begin

    t_SearchResults := api.pkg_Search.NewSearchResults();

    if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'IsRefund', 'N', t_SearchResults) then
      return;
    end if;

    if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'IsVoid', 'N', t_SearchResults) then
      return;
    end if;

    if not api.pkg_Search.IntersectByColumnValue('o_Payment', 'PaymentDate', to_date(null), t_DepositDate + 86399/86400, t_SearchResults) then
      return;
    end if;

    t_Objects := extension.pkg_Utils.ConvertToObjectList(api.pkg_Search.SearchResultsToList(t_SearchResults));

    for i in (select objectid
               from table(cast(t_Objects as api.udt_objectlist)))
    loop

      if api.pkg_columnquery.DateValue(i.objectid, 'DepositDate') is null then
      t_RelId := api.pkg_RelationshipUpdate.New(t_EndPointId, a_ObjectId, i.ObjectId);
       else null;
      end if;

    end loop;
  end DailyDepositFees;

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2014 September 19
  -- Purpose    : Gets Access Groups for Instance Security on Product object.
  -------------------------------------
  procedure ProductToWWWAccessGroup (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_UserId                            udt_IdList;
    t_AccessGroupList                   api.udt_ObjectList;

  begin
    -- Initialize to prevent error
    a_ObjectList := api.udt_ObjectList();
    t_AccessGroupList := api.udt_ObjectList();
    -- Determine if we are already related to a User through a
    -- Legal Entity, in this case we prefer these Access Groups
    select userid
    bulk collect into t_UserId
    from (
    select UserId
      from query.r_ABC_UserProduct
      where ProductId = a_ObjectId
     union
     select prou.UserId
        from query.r_ABC_ProdRegToProduct prtp
        join query.r_ABC_ProductRegOnlineUser prou
          on prtp.ProdRegJobId = prou.ProductRegistrationJobId
       where prtp.ProductObjectId = a_ObjectId
      union
     select ul.userid
       from query.r_ABC_ProductRegistrant pr
       join query.r_ABC_UserLegalEntity ul on ul.LegalEntityObjectId = pr.RegistrantObjectId
      where pr.ProductObjectId = a_ObjectId
      union
     select ul.userid
       from query.r_Legalentitydist ld
       join query.r_ABC_UserLegalEntity ul on ul.LegalEntityObjectId = ld.LegalEntityId
      where ld.ProdictId = a_ObjectId
      union
     select ul.UserId
       from query.r_ABC_ProdRegToProduct prtp
       join query.o_abc_legalentity le on le.objectid = api.pkg_ColumnQuery.Value(prtp.ProdRegJobId, 'OnlineUseLegalEntityObjectId')
       join query.r_ABC_UserLegalEntity ul on ul.LegalEntityObjectId = le.ObjectId
      where prtp.ProductObjectId = a_ObjectId );

    for i in 1..t_UserId.count loop
      select api.udt_Object(AccessGroupObjectId)
    bulk collect into t_AccessGroupList
        from query.r_UserWWWAccessGroup
       where UserId = t_UserId(i);

       extension.pkg_collectionutils.Append(a_ObjectList,t_AccessGroupList);

    end loop;
/*    -- If we don't find anything we try to get the Access Groups off of the User
    -- related to the Product Registration Job.
    if a_ObjectList.count <= 0 then
      select prou.UserId
        from query.r_ABC_ProdRegToProduct prtp
        join query.r_ABC_ProductRegOnlineUser prou
          on prtp.ProdRegJobId = prou.ProductRegistrationJobId
       where prtp.ProductObjectId = a_ObjectId;

      for i in 1..t_UserId.count loop
        select api.udt_Object(AccessGroupObjectId)
          bulk collect into a_ObjectList
          from query.r_UserWWWAccessGroup
         where UserId = t_UserId(i);
      end loop;

      select accessgroupid
      from query.r_ABC_UserProduct up
      join query
      productregcreatinguser
      productregistrantlegalentity

    end if;*/

  end ProductToWWWAccessGroup;

  -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2014 Sep 26
  -- Purpose    : This returns the Products on a Product Registration job (Application,
  --              Amendment, Renewal, Non-Renewal) that is related to the process that is creating the letter
  -------------------------------------
  procedure ProductsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select r.ToObjectId
    bulk collect into t_ObjectIdList
    from api.relationships r
    join api.endpoints ep
    on ep.EndPointId = r.EndPointId
    where (r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication', 'Product')
           or r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment', 'ProductEndPoint')
           or r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRNonRenewal', 'Product')           )
    and r.FromObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'RelatedJobId')

  union all

  select api.pkg_columnquery.NumericValue(r.ToObjectId, 'ProductObjectId')
    from api.relationships r
    join api.endpoints ep
    on ep.EndPointId = r.EndPointId
    where r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRRenewal', 'ProductXref')
    and r.FromObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'RelatedJobId');

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end ProductsForLetter;

  -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2014 Sep 26
  -- Purpose    : This returns the Distributors on a Product Registration job (Application,
  --              Amendment, Renewal, Non-Renewal) that is related to the process that is creating the letter
  -------------------------------------
  procedure DistributorsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select r.ToObjectId
    bulk collect into t_ObjectIdList
    from api.relationships r
    join api.endpoints ep
    on ep.EndPointId = r.EndPointId
    where (r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication', 'Distributor')
           or r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment', 'DistributorEndPoint') --EndPoint?
           )
    and r.FromObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'RelatedJobId');

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end DistributorsForLetter;

  -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2015 Oct 08
  -- Purpose    : This returns the TAP Distributors on a Product Registration job (Application,
  --              Amendment) that is related to the process that is creating the letter
  -------------------------------------
  procedure TAPDistributorsForLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select r.ToObjectId
    bulk collect into t_ObjectIdList
    from api.relationships r
    join api.endpoints ep
    on ep.EndPointId = r.EndPointId
    where (r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication', 'TAPDistributor')
           or r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment', 'TAPDistributor') --EndPoint?
           )
    and r.FromObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'RelatedJobId');

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end TAPDistributorsForLetter;

/*  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2015 Mar 27
  -- Purpose    : Given a License Id, determine the most recent Major Application
  -------------------------------------
  function FindMajorJob (
    a_LicenseId                         udt_Id
  ) return udt_Id is
    t_JobEPList                         api.pkg_Definition.udt_StringList;
    t_SendLicenseList                   udt_IdList;
    t_MaxProcId                         udt_Id := null;
  begin
    t_JobEPList := extension.pkg_Utils.Split(api.pkg_ColumnQuery.Value(a_LicenseId, 'DocJobEPs'), ',');

    for i in 1..t_JobEPList.count loop
      declare
        t_TmpSendLicenseList            udt_IdList;
      begin
        select p.ProcessId
          bulk collect into t_TmpSendLicenseList
          from api.Relationships r
          join api.EndPoints ep
            on r.EndPointId = ep.EndPointId
          join api.Processes p
            on r.ToObjectId = p.JobId
          join api.ProcessTypes pt
            on p.ProcessTypeId = pt.ProcessTypeId
         where r.FromObjectId = a_LicenseId
           and ep.Name = t_JobEPList(i)
           and pt.Name = 'p_ABC_SendLicense';

        extension.pkg_CollectionUtils.Append(t_SendLicenseList, t_TmpSendLicenseList);
      end;
    end loop;

    for i in 1..t_SendLicenseList.count loop
      if api.pkg_ColumnQuery.Value(t_SendLicenseList(i), 'DateCompleted') is not null then
        if t_MaxProcId is null or api.pkg_ColumnQuery.Value(t_SendLicenseList(i), 'DateCompleted') >
            api.pkg_ColumnQuery.Value(t_MaxProcId, 'DateCompleted')
        then
          if pkg_ABC_License.IsMajorJob(api.pkg_ColumnQuery.Value(t_SendLicenseList(i), 'JobId')) then
            t_MaxProcId := t_SendLicenseList(i);
          end if; -- Is Major Application?
        end if; -- Was Completed Later?
      end if; -- Was Completed?
    end loop;

    return api.pkg_ColumnQuery.Value(t_MaxProcId, 'JobId');
  end FindMajorJob;*/

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2015 Mar 27
  -- Purpose    : Given a Job Id, find all Online Documents, given an EndPoint Id
  -------------------------------------
  procedure GetMajorDocs (
    a_JobId                             udt_Id,
    a_LicenseId                         udt_Id,
    a_EndPointId                        udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_DocEPList                         api.pkg_Definition.udt_StringList;
    t_ToObjectDefId                     udt_Id;
  begin
    -- Find out what type of Documents we are concerned with.
    t_DocEPList  := extension.pkg_Utils.Split(api.pkg_ColumnQuery.Value(a_LicenseId, 'DocDocEPs'), ',');
    a_ObjectList := api.udt_ObjectList();

    -- Figure out what type of Document the relationship requires.
    select ep.ToObjectDefId
      into t_ToObjectDefId
      from api.EndPoints ep
     where ep.EndPointId = a_EndPointId;

    for i in 1..t_DocEPList.count loop
      declare
        t_TmpObjectList                 api.udt_ObjectList;
      begin
        select api.udt_Object(r.ToObjectId)
          bulk collect into t_TmpObjectList
          from api.Relationships r
          join api.EndPoints ep
            on r.EndPointId = ep.EndPointId
         where r.FromObjectId = a_JobId
           and ep.Name = t_DocEPList(i)
           and ep.ToObjectDefId = t_ToObjectDefId;
        for i in 1..t_TmpObjectList.count loop
          dbms_output.put_line(t_TmpObjectList(i).ObjectId);
        end loop;
        extension.pkg_CollectionUtils.Append(a_ObjectList, t_TmpObjectList);
      end;

    end loop;

  end GetMajorDocs;

  -------------------------------------
  -- Programmer : Grant M
  -- Date       : 2015 Mar 27
  -- Purpose    : Returns Documents from the most recent Major application
  -------------------------------------
  procedure RecentMajorDocs (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_JobId                             udt_Id;
    t_LicenseId                         udt_Id;
  begin
    a_ObjectList := api.udt_ObjectList();
    t_JobId      := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');

    select r.ToObjectId
      into t_LicenseId
      from api.Relationships r
      join api.EndPoints ep
        on r.EndPointId = ep.EndPointId
     where ep.Name = 'License'
       and r.FromObjectId = t_JobId;

    -- If the current job is a Major Application, do nothing, otherwise find
    -- the most recent Major Application and get Documents from it.
    if pkg_ABC_License.IsMajorJob(t_JobId) then
      return;
    else
      t_JobId := api.pkg_ColumnQuery.Value(api.pkg_ColumnQuery.Value(t_LicenseId
                                                                  , 'MajorSendLicProcessId')
                                        , 'JobId');
      GetMajorDocs(t_JobId, t_LicenseId, a_EndPoint, a_ObjectList);
    end if;
  end RecentMajorDocs;

  -------------------------------------
  -- Programmer : Paul Orstad
  -- Date       : 2019 Apr 2
  -- Purpose    : Get the Responsible Legal Entity for a Miscellaneous Revenue Job
  -------------------------------------
  procedure MiscTnsctnResponsibleParty (
    a_JobId                             udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ResponsibleRel                    udt_IdList;

  begin
    t_ResponsibleRel := api.pkg_search.ObjectsByRelColumnValue('o_ABC_LegalEntity',
        'MiscellaneousRevenue', 'ObjectId', a_JobId);
    if t_ResponsibleRel.count = 0 then
      t_ResponsibleRel := api.pkg_search.ObjectsByRelColumnValue('o_ABC_LegalEntity',
          'LicenseMiscRev', 'ObjectId', a_JobId);
    end if;
    if t_ResponsibleRel.count = 0 then
      t_ResponsibleRel := api.pkg_search.ObjectsByRelColumnValue('o_ABC_LegalEntity',
          'PermitMiscRev', 'ObjectId', a_JobId);
    end if;
    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ResponsibleRel);

  end MiscTnsctnResponsibleParty;
  
  /*---------------------------------------------------------------------------
   * AppealResponsibleParty() -- PUBLIC
   *   Get the Responsible Legal Entity for an Appeal Job
   *-------------------------------------------------------------------------*/
  procedure AppealResponsibleParty (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin

    select api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'FormattedLegalEntityObjectId')
        bulk collect into t_ObjectIdList
        from dual;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end AppealResponsibleParty;

  -------------------------------------
  -- Programmer : Dalainya B
  -- Date       : 2015 Jan 27
  -- Purpose    : This is used to relate the user selected permittee over the licensee for permits
  -------------------------------------
  procedure PermitteeForPermit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin

    select api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'FormattedPermitteeObjectId')
        bulk collect into t_ObjectIdList
        from dual;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PermitteeForPermit;

  -------------------------------------
  -- Programmer : Nathaniel vD
  -- Date       : 2015 Feb 3
  -- Purpose    : Create procedural rel to populate Permit tab on Legal Entity with issued
  -------------------------------------
  procedure PermitForPermitee (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is

    t_ObjectIdList                      udt_IdList;

  begin

      Select distinct PermitObjectId
       bulk collect into t_ObjectIdList
       from(SELECT PPE.PERMITOBJECTID,
                   NULL SHOWONLINE,
                   NULL LICENSEREQUIRED,
                   NULL PERMITTEEISLICENSEE,
                   NULL LICENSENUMBER,
                   API.PKG_COLUMNQUERY.VALUE(PPE.PERMITOBJECTID,'SystemCancelled') SYSTEMCANCELLED,
                   'Permiteee' TYPEOFPERMITEE
              FROM QUERY.R_ABC_PERMITPERMITTEE PPE
             WHERE PPE.PERMITTEEOBJECTID = a_ObjectId

            union all

           SELECT TPE.TAPPermitObjectId PERMITOBJECTID,
                  NULL SHOWONLINE,
                  NULL LICENSEREQUIRED,
                  NULL PERMITTEEISLICENSEE,
                  NULL LICENSENUMBER,
                  API.PKG_COLUMNQUERY.VALUE(TPE.TAPPERMITOBJECTID,'SystemCancelled') SYSTEMCANCELLED,
                  'TAPPermitee' TYPEOFPERMITEE
             FROM query.r_ABC_TAPPermit TPE
             Join query.r_ABC_PermitPermittee pp on pp.PermitObjectId = tpe.TAPPermitObjectId
            WHERE pp.PermitteeObjectId = a_ObjectId

        union all

           SELECT PEL.PERMITOBJECTID,
                  API.PKG_COLUMNQUERY.VALUE(PEL.PERMITOBJECTID,'ShowOnline') SHOWONLINE,
                  API.PKG_COLUMNQUERY.VALUE(PEL.PERMITOBJECTID,'LicenseRequired') LICENSEREQUIRED,
                  API.PKG_COLUMNQUERY.VALUE(PEL.PERMITOBJECTID,'PermitteeIsLicensee')PERMITTEEISLICENSEE,
                  API.PKG_COLUMNQUERY.VALUE(PEL.PERMITOBJECTID,'LicenseNumber') LICENSENUMBER,
                  API.PKG_COLUMNQUERY.VALUE(PEL.PERMITOBJECTID,'SystemCancelled') SYSTEMCANCELLED,
                  'PermitLicense' TYPEOFPERMITEE
             FROM QUERY.R_ABC_LICENSELICENSEELE LLE
             JOIN QUERY.R_PERMITLICENSE PEL ON PEL.LICENSEOBJECTID = LLE.LICENSEOBJECTID
            WHERE LLE.LEGALENTITYOBJECTID = a_ObjectId)

       WHERE ((TYPEOFPERMITEE = 'Permiteee') OR
              (TYPEOFPERMITEE = 'TAPPermitee') OR
              (TYPEOFPERMITEE = 'PermitLicense' AND PERMITTEEISLICENSEE = 'Y' AND LICENSEREQUIRED= 'Required') OR
              (TYPEOFPERMITEE = 'PermitLicense' AND PERMITTEEISLICENSEE = 'Y' AND LICENSEREQUIRED= 'Optional' AND LICENSENUMBER IS NOT NULL))
       AND    SYSTEMCANCELLED = 'N';

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PermitForPermitee;

  -------------------------------------
  -- Programmer : Nathaniel vD
  -- Date       : 2015 Feb 3
  -- Purpose    : Create procedural rel to populate Permit tab on Legal Entity with issued
  -------------------------------------
  procedure IssuedPermitForPermitee (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectList                      udt_ObjectList;

  begin
    a_ObjectList := api.udt_ObjectList();
    abc.pkg_abc_proceduralrels.PermitForPermitee(a_ObjectId,a_EndPoint,t_ObjectList);
    for i in 1..t_ObjectList.count loop
       if api.pkg_columnquery.Value(t_ObjectList(i).objectid,'dup_HasBeenIssued') = 'Y' then
         a_ObjectList.Extend(1);
         a_ObjectList(a_ObjectList.count) := t_ObjectList(i);
       end if;
    end loop;

  end IssuedPermitForPermitee;

  -------------------------------------
  -- Programmer : Dalainya B
  -- Date       : 2015 Feb 10
  -- Purpose    : This returns the Permits where the individual is the Permittee (for Public site)
  --              returns all Permits
  -------------------------------------
  procedure OnlinePermitNoLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_ShowAll                           varchar2(1) := api.pkg_columnQuery.Value(a_ObjectId, 'ShowMorePermits');
  begin

      select distinct PermitObjectId
        bulk collect into t_ObjectIdList
        from (select PermitObjectId from (select x.PermitObjectId,
                     api.pkg_columnQuery.Value(x.PermitObjectId, 'ShowOnline') ShowOnline,
                     api.pkg_columnquery.Value(x.PermitObjectId,'LicenseRequired') LicenseRequired,
                     api.pkg_columnquery.Value(x.PermitObjectId,'PermitteeIsLicensee')PermitteeIsLicensee,
                     api.pkg_columnquery.Value(x.PermitObjectId,'LicenseNumber') LicenseNumber,
                     api.pkg_columnquery.Value(x.PermitObjectId,'SystemCancelled') SystemCancelled,
                     x.TypeofPermitee from
                     (select ppe.PermitObjectId,
                             'Permitee' TypeofPermitee
                        from query.r_Abc_Userlegalentity r
                        join query.r_ABC_PermitPermittee ppe on ppe.PermitteeObjectId = r.LegalEntityObjectId
                       where r.UserId = a_ObjectId
                       union
                      select pel.PermitObjectId,
                             'PermitLicense'
                        from query.r_Abc_Userlegalentity r
                        join query.r_ABC_LicenseLicenseeLE lle on lle.LegalEntityObjectId = r.LegalEntityObjectId
                        join query.r_PermitLicense pel on pel.LicenseObjectId = lle.LicenseObjectId
                       where r.UserId = a_ObjectId
                       union
                      select tp.PermitObjectId,
                             'Permitee'
                        from query.r_ABC_UserLegalEntity r
                        join query.r_ABC_PermitPermittee ppe on ppe.PermitteeObjectId = r.LegalEntityObjectId
                        join query.r_ABC_TAPPermit tp on tp.TAPPermitObjectId = ppe.PermitObjectId
                       where r.UserId = a_ObjectId) x)
       where ShowOnline = 'Y'
         and ( (TypeofPermitee = 'Permitee') or
               (TypeofPermitee = 'PermitLicense' and PermitteeIsLicensee = 'Y' and LicenseRequired= 'Required') or
               (TypeofPermitee = 'PermitLicense' and PermitteeIsLicensee = 'Y' and LicenseRequired= 'Optional' and LicenseNumber is not null)
              )
         and  SystemCancelled = 'N'
      order by PermitObjectId desc);

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end OnlinePermitNoLimit;

  -------------------------------------
  -- Programmer : Dalainya B
  -- Date       : 2015 Feb 10
  -- Purpose    : This returns the Permits where the individual is the Permittee (for Public site)
  --              only return 10 at a time (unless show all is checked)
  -------------------------------------
  procedure OnlinePermitLimit (
    a_UserId                            udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_DaysToShowExpiredPermits          udt_Id;
    t_Sysdate                           Date;
    t_Count                             Number;
  begin

    select o.DaysToShowExpiredPermits, sysdate
      into t_DaysToShowExpiredPermits, t_Sysdate
      from query.o_SystemSettings o
     where ObjectDefTypeId = 1
       and rownum = 1;

    select count(pel.PermitObjectId)
      into t_Count
      from query.r_Abc_Userlegalentity r
      join query.r_ABC_LicenseLicenseeLE lle on lle.LegalEntityObjectId = r.LegalEntityObjectId
      join query.r_PermitLicense pel on pel.LicenseObjectId = lle.LicenseObjectId
     where r.UserId = a_UserId;

   if t_Count > 2500 then
     select PermitObjectId
     bulk collect into t_ObjectIdList
     from (
         select distinct PermitObjectId
         from (
             select PermitObjectId
             from (
                 select
                   x.PermitObjectId,
                   x.showonline,
                   api.pkg_columnquery.Value(x.PermitObjectId,'SystemCancelled') SystemCancelled
                 from (
                     select
                       ppe.permitobjectid,
                       api.pkg_columnquery.value(ppe.permitobjectid, 'showonline') showonline
                     from
                       query.r_abc_userlegalentity r
                       join query.r_abc_permitpermittee ppe
                           on ppe.permitteeobjectid = r.legalentityobjectid
                     where r.userid = a_UserId
                     union all
                     select
                       pel.PermitObjectId,
                       'Y' showonline
                     from
                        query.r_Abc_Userlegalentity r
                        join query.r_ABC_LicenseLicenseeLE lle
                            on lle.LegalEntityObjectId = r.LegalEntityObjectId
                        join query.r_PermitLicense pel
                            on pel.LicenseObjectId = lle.LicenseObjectId
                        join query.o_ABC_Permit p
                            on p.ObjectId = pel.PermitObjectId
                        join query.r_ABC_PermitPermitType pt
                            on pt.PermitId = pel.PermitObjectId
                        join query.o_Abc_Permittype pt
                            on pt.ObjectId = pt.PermitTypeId
                      where r.UserId = a_UserId
                        and pt.PermitteeIsLicensee = 'Y'
                        and pt.LicenseRequired in ('Required','Optional')
                        -- Handle the ShowOnline here
                        and ( p.State = 'Active' OR
                            ( p.State = 'Expired' AND trunc(t_Sysdate) - p.ExpirationDate < nvl( 10 , 0) ))
                     union all
                     select
                       tp.PermitObjectId,
                       api.pkg_columnquery.Value(tp.PermitObjectId,'Showonline') Showonline
                     from
                       query.r_abc_userlegalentity r
                       join query.r_ABC_PermitPermittee ppe
                           on ppe.PermitteeObjectId = r.LegalEntityObjectId
                       join query.r_ABC_TAPPermit tp
                           on tp.TAPPermitObjectId = ppe.PermitObjectId
                     where r.UserId = a_UserId) x
                 where showonline = 'Y')
             where SystemCancelled = 'N')
         order by PermitObjectId desc)
        where rownum <=10;
     else

      select PermitObjectId
        bulk collect into t_ObjectIdList
        from
            ( select distinct PermitObjectId from (select PermitObjectId
                from (select x.PermitObjectId,
                                   api.pkg_columnquery.Value(x.PermitObjectId,'Showonline') Showonline,
                                   api.pkg_columnquery.Value(x.PermitObjectId,'LicenseRequired') LicenseRequired,
                                   api.pkg_columnquery.Value(x.PermitObjectId,'PermitteeIsLicensee')PermitteeIsLicensee,
                                   api.pkg_columnquery.Value(x.PermitObjectId,'LicenseNumber') LicenseNumber,
                                   api.pkg_columnquery.Value(x.PermitObjectId,'SystemCancelled') SystemCancelled,
                                   x.TypeofPermitee
                        from
                             (
                              select ppe.PermitObjectId,
                                    'Permitee' TypeofPermitee
                                from query.r_Abc_Userlegalentity r
                                join query.r_ABC_PermitPermittee ppe on ppe.PermitteeObjectId = r.LegalEntityObjectId
                               where r.UserId = a_UserId
                               union
                              select pel.PermitObjectId,
                                     'PermitLicense' TypeofPermitee
                                from query.r_Abc_Userlegalentity r
                                join query.r_ABC_LicenseLicenseeLE lle on lle.LegalEntityObjectId = r.LegalEntityObjectId
                                join query.r_PermitLicense pel on pel.LicenseObjectId = lle.LicenseObjectId
                               where r.UserId = a_UserId
                               union
                              select tp.PermitObjectId,
                                     'Permitee'
                                from query.r_ABC_UserLegalEntity r
                                join query.r_ABC_PermitPermittee ppe on ppe.PermitteeObjectId = r.LegalEntityObjectId
                                join query.r_ABC_TAPPermit tp on tp.TAPPermitObjectId = ppe.PermitObjectId
                               where r.UserId = a_UserId
                               ) x)
               where ShowOnline = 'Y'
                 and ( (TypeofPermitee = 'Permitee') or
                       (TypeofPermitee = 'PermitLicense' and PermitteeIsLicensee = 'Y' and LicenseRequired= 'Required') or
                       (TypeofPermitee = 'PermitLicense' and PermitteeIsLicensee = 'Y' and LicenseRequired= 'Optional' and LicenseNumber is not null)
                     )
                 and SystemCancelled = 'N'
              )order by PermitObjectId desc)
       where rownum <=10;

     end if;

      a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end OnlinePermitLimit;

  -------------------------------------
  -- Programmer : Shane Gilbert
  -- Date       : 2018 Apr 11
  -- Purpose    : This is used on a visibility expression within stage to improve performance
  -- If any count returns greater than 0 then do not continue checking the other counts
  -------------------------------------
  function HasOnlinePermits (
    a_UserId                            udt_Id
  ) return varchar is
    t_ObjectIdList                      udt_IdList;
    t_DaysToShowExpiredPermits          udt_Id;
    t_Sysdate                           Date;
    t_Count                             Number;
  begin
    select count(t.PermitObjectId)
    into t_count
    from (
        select
          ppe.PermitObjectId,
          api.pkg_columnquery.value(ppe.PermitObjectId, 'Showonline') showonline,
          api.pkg_columnquery.value(ppe.PermitObjectId, 'SystemCancelled') cancelled
        from
          query.r_abc_userlegalentity r
          join query.r_abc_permitpermittee ppe
              on ppe.permitteeobjectid = r.legalentityobjectid
        where r.userid = a_UserId) t
    where showonline = 'Y'
      and cancelled = 'N'
      and rownum <= 1;

    if t_count = 0 then
      t_Sysdate := sysdate;

      select count(pel.PermitObjectId)
      into t_count
      from
         query.r_Abc_Userlegalentity r
         join query.r_ABC_LicenseLicenseeLE lle
             on lle.LegalEntityObjectId = r.LegalEntityObjectId
         join query.r_PermitLicense pel
             on pel.LicenseObjectId = lle.LicenseObjectId
         join query.o_ABC_Permit p
             on p.ObjectId = pel.PermitObjectId
         join query.r_ABC_PermitPermitType pt
             on pt.PermitId = pel.PermitObjectId
         join query.o_Abc_Permittype pt
             on pt.ObjectId = pt.PermitTypeId
       where r.UserId = a_UserId
         and pt.PermitteeIsLicensee = 'Y'
         and pt.LicenseRequired in ('Required','Optional')
         -- Handle the ShowOnline here
         and ( p.State = 'Active' OR
             ( p.State = 'Expired' AND trunc(t_Sysdate) - p.ExpirationDate < nvl( 10 , 0) ))
         and p.SystemCancelled = 'N'
         and rownum <= 1;

     end if;

     if t_count = 0 then

       select count(tp.PermitObjectId)
       into t_count
       from
         query.r_abc_userlegalentity r
         join query.r_ABC_PermitPermittee ppe
             on ppe.PermitteeObjectId = r.LegalEntityObjectId
         join query.r_ABC_TAPPermit tp
             on tp.TAPPermitObjectId = ppe.PermitObjectId
       where r.UserId = a_UserId
         and api.pkg_columnquery.Value(tp.PermitObjectId,'Showonline') = 'Y'
         and api.pkg_columnquery.Value(tp.PermitObjectId,'SystemCancelled') = 'N'
         and rownum <= 1;

     end if;

     return case when t_count > 0 then 'Y' else 'N' end;
  end HasOnlinePermits;

  -------------------------------------
  -- Programmer : Nathaniel vD
  -- Date       : 2015 Mar 04
  -- Purpose    : Returns all active permits related to the license being renewed
  -------------------------------------
  procedure ActivePermitsForRenewalLicense (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_RenewalLicenseId                  udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId,'RenewalLicenseId');
    t_PermitIdList                      udt_IdList;
    t_SellersPermitIdList               udt_IdList;
  begin
    t_PermitIdList := api.pkg_ObjectQuery.RelatedObjects(t_RenewalLicenseId,api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License','Permit'));
    t_SellersPermitIdList := api.pkg_ObjectQuery.RelatedObjects(t_RenewalLicenseId,api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License','SellersPermit'));
    a_ObjectList := api.udt_ObjectList();
    for i in 1..t_PermitIdList.Count loop
      if api.pkg_ColumnQuery.Value(t_PermitIdList(i),'State') = 'Active' then
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_PermitIdList(i));
      end if;
    end loop;
    for i in 1..t_SellersPermitIdList.Count loop
      if api.pkg_ColumnQuery.Value(t_SellersPermitIdList(i),'State') = 'Active' then
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := api.udt_Object(t_SellersPermitIdList(i));
      end if;
    end loop;
  end ActivePermitsForRenewalLicense;

  -------------------------------------
  -- Programmer : Nathaniel van Diepen
  -- Date       : 2015 March 09
  -- Purpose    : Returns the event dates on a permit for the permitletter
  -------------------------------------
  procedure EventDatesForPermitLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select ev.EventDateObjectId
    bulk collect into t_ObjectIdList
    from query.r_ABC_PermitEventDate ev
    where ev.PermitObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'PermitObjectId');

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end EventDatesForPermitLetter;

  -------------------------------------
  -- Programmer : Nathaniel van Diepen
  -- Date       : 2015 March 09
  -- Purpose    : Returns the event dates on a permit for the permitletter
  -------------------------------------
  procedure RainDatesForPermitLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
  begin
    select ev.RainDateObjectId
    bulk collect into t_ObjectIdList
    from query.r_ABC_PermitRainDate ev
    where ev.PermitObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'PermitObjectId');

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end RainDatesForPermitLetter;

    -------------------------------------
  -- Programmer : Keaton Leikam
  -- Date       : 2015 April 10
  -- Purpose    : Returns Vehicle Associated with the Permit for use with the Permit Letter
  -------------------------------------
  procedure VehiclesForPermitLetter (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
  begin
    select pv.VehicleObjectId
    bulk collect into t_ObjectIdList
    from query.r_ABC_PermitVehicle pv
    where pv.PermitObjectId = api.Pkg_Columnquery.NumericValue(a_ObjectId, 'PermitObjectId')
    order by lpad(api.pkg_columnquery.Value (pv.VehicleObjectId,'InsigniaNumber'),10);

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end VehiclesForPermitLetter;

  -------------------------------------
  -- Programmer : Nathaniel van Diepen
  -- Date       : 2015 March 24
  -- Purpose    : Returns responsible party for a new application
  -------------------------------------
  procedure NewAppResponsibleParty (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_col0 udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'LicenseeLegalEntityObjectId');
    t_col1 udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlineUseLegalEntityObjectId');
  begin
    a_ObjectList := api.udt_ObjectList();
    if t_col0 is not null then
      a_ObjectList.Extend(1);
      a_ObjectList(a_ObjectList.count) := api.udt_Object(t_col0);
    elsif t_col1 is not null then
      a_ObjectList.Extend(1);
      a_ObjectList(a_ObjectList.count) := api.udt_Object(t_col1);
    end if;
  end NewAppResponsibleParty;

  procedure PermitAppResponsibleParty (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_col0 udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'PermitteeLegalEntityObjectId');
    t_col1 udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlineResponsibleParty');
  begin
    a_ObjectList := api.udt_ObjectList();
    if t_col0 is not null then
      a_ObjectList.Extend(1);
      a_ObjectList(a_ObjectList.count) := api.udt_Object(t_col0);
    elsif t_col1 is not null then
      a_ObjectList.Extend(1);
      a_ObjectList(a_ObjectList.count) := api.udt_Object(t_col1);
    end if;
  end PermitAppResponsibleParty;

  -------------------------------------
  -- Programmer : Josh Camps
  -- Date       : 2015 Apr 15
  -- Purpose    : This gives the responsible legal entity from online or internal,
  --              for the Product Registration Jobs
  -------------------------------------
  procedure ResponsibleLegalEntity (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectDefName                     varchar2(100);
    t_ObjectIdList                      udt_IdList;

    begin
    --Get Job ObjectDefName different endpoint names were used for relationship
    --depinding on the type of job.
    t_ObjectDefName := api.pkg_columnquery.Value(a_ObjectId, 'ObjectDefName');

    --Check to see if the online relationship exists if 'Y' return the results from
    --the procedure relationship
    if extension.pkg_relutils.RelExists(a_ObjectId, 'LEChosenOnline') = 'Y' then
      select o.ObjectId
        bulk collect into t_ObjectIdList
        from query.o_ABC_LegalEntity o
       where o.ObjectId = api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineUseLegalEntityObjectId');
    end if;

    case t_ObjectDefName
      when 'j_ABC_PRRenewal' then
        if extension.pkg_relutils.RelExists(a_ObjectId, 'Registrant') = 'Y' then
          select r.ToObjectId
            bulk collect into t_ObjectIdList
            from api.relationships r
           where r.FromObjectId = a_ObjectId
             and r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PrRenewal', 'Registrant');
        end if;

      when 'j_ABC_PRApplication' then
        if extension.pkg_relutils.RelExists(a_ObjectId, 'Registrant') = 'Y' then
          select r.ToObjectId
            bulk collect into t_ObjectIdList
            from api.relationships r
           where r.FromObjectId = a_ObjectId
             and r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRApplication', 'Registrant');
        end if;

      when 'j_ABC_PRAmendment' then
        if extension.pkg_relutils.RelExists(a_ObjectId, 'RegistrantEndPoint') = 'Y' then
          select r.ToObjectId
            bulk collect into t_ObjectIdList
            from api.relationships r
           where r.FromObjectId = a_ObjectId
             and r.EndPointId = api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment', 'RegistrantEndPoint');
        end if;

      else
        null;

    end case;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end ResponsibleLegalEntity;

 -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2015 July
  -- Purpose    : Returns all related License Renewals for a public user.
  -------------------------------------
  procedure PublicRenewalApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select distinct jr.RenewalApplicationObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_UserLegalEntity r
      join query.r_ABC_LicenseeIND_RenewalApp jr on jr.LegalEntityObjectId = r.LegalEntityObjectId
     where r.UserId = a_ObjectId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PublicRenewalApps;

 -------------------------------------
  -- Programmer : Paul O
  -- Date       : 2015 July
  -- Purpose    : Returns all related users for a renewal app.
  -------------------------------------
  procedure RenewalAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select distinct r.UserId
      bulk collect into t_ObjectIdList
      from query.r_ABC_LicenseeIND_RenewalApp jr
      join query.r_ABC_UserLegalEntity r on r.LegalEntityObjectId = jr.LegalEntityObjectId
     where jr.RenewalApplicationObjectId = a_ObjectId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end RenewalAppPublicUsers;

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related Product Registrations for a public user.
  -------------------------------------
  procedure PRPublicApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select x.JobId
    bulk collect into t_ObjectIdList
    from( select jr.ProdRegJobId Jobid
      from query.r_ABC_UserLegalEntity r
      join query.r_ABC_ProdRegToRegistrant jr on jr.RegistrantObjectId = r.LegalEntityObjectId
     where r.UserId = a_ObjectId
     union
     select pro.ProductRegistrationJobId
       from query.r_ABC_ProductRegOnlineUser pro
      where pro.UserId = a_ObjectId) x;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PRPublicApps;

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related users for  Product Registration App.
  -------------------------------------
  procedure PRAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
     select x.Userid
     bulk collect into t_ObjectIdList
    from (select r.UserId
      from query.r_ABC_ProdRegToRegistrant jr
      join query.r_ABC_UserLegalEntity r on r.LegalEntityObjectId = jr.RegistrantObjectId
     where jr.ProdRegJobId = a_ObjectId
      union
     select pro.UserId
       from query.r_ABC_ProductRegOnlineUser pro
      where pro.ProductRegistrationJobId = a_ObjectId)x ;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PRAppPublicUsers;

   -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related License Amendments for a public user.
  -------------------------------------
  procedure PRPublicAmendmentApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select x.JobId
    bulk collect into t_ObjectIdList
    from (select  jr.ProdRegJobId JobId
      from query.r_ABC_UserLegalEntity r
      join query.r_ABC_PRAmendToRegistrant jr on jr.RegistrantObjectId = r.LegalEntityObjectId
     where r.UserId = a_ObjectId
     union
     select pro.PRAmendJobId
       from query.r_ABC_PRAmendOnlineUser pro
      where pro.UserId = a_ObjectId) x;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PRPublicAmendmentApps;

 -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related users for a amendment app.
  -------------------------------------
  procedure PRAmendmentAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
   select x.userid
     bulk collect into t_ObjectIdList
     from
    (select distinct r.UserId
      from query.r_ABC_PRAmendToRegistrant jr
      join query.r_ABC_UserLegalEntity r on r.LegalEntityObjectId = jr.RegistrantObjectId
     where jr.ProdRegJobId = a_ObjectId
     union
     select pro.UserId
       from query.r_ABC_PRAmendOnlineUser pro
      where pro.PRAmendJobId = a_ObjectId) x;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PRAmendmentAppPublicUsers;

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related  Product Registrations Renewal for a public user.
  -------------------------------------
  procedure PRPublicRenewalApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  )is
    t_ObjectIdList                      udt_IdList;

  begin
    select x.JobId
    bulk collect into t_ObjectIdList
    from (
    select  jr.ProdRegJobId JobId
      from query.r_ABC_UserLegalEntity r
      join query.r_ABC_ProdRenewalToRegistrant jr on jr.RegistrantObjectId = r.LegalEntityObjectId
     where r.UserId = a_ObjectId
     union
     select pro.ProductRegistrationJobId
       from query.r_ProductRenewalToUser pro
      where pro.UserId = a_ObjectId) x;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PRPublicRenewalApps;

  -------------------------------------
  -- Programmer : Keaton L
  -- Date       : 2015 Sept
  -- Purpose    : Returns all related users for a Product Registration Renewal.
  -------------------------------------
  procedure PRRenewalAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is

  t_ObjectIdList                      udt_IdList;

  begin
    select x.UserId
    bulk collect into t_ObjectIdList
    from (
    select distinct r.UserId
      from query.r_ABC_ProdRenewalToRegistrant jr
      join query.r_ABC_UserLegalEntity r on r.LegalEntityObjectId = jr.RegistrantObjectId
     where jr.ProdRegJobId = a_ObjectId
     union
     select pro.UserId
       from query.r_ProductRenewalToUser pro
      where pro.ProductRegistrationJobId = a_ObjectId) x;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PRRenewalAppPublicUsers;

  -------------------------------------
  -- Programmer : Matthew J
  -- Date       : 2015 September
  -- Purpose    : Returns all related License Amendments for a public user.
  -------------------------------------
  procedure PublicAmendmentApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select distinct jr.AmendmentApplicationObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_UserLegalEntity r
      join query.r_ABC_LicenseeIND_AmendmentApp jr on jr.LegalEntityObjectId = r.LegalEntityObjectId
     where r.UserId = a_ObjectId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PublicAmendmentApps;

  -------------------------------------
  -- Programmer : Matthew J
  -- Date       : 2015 September
  -- Purpose    : Returns all related users for an Amendment Application.
  -------------------------------------
  procedure AmendmentAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select distinct r.UserId
      bulk collect into t_ObjectIdList
      from query.r_ABC_LicenseeIND_AmendmentApp jr
      join query.r_ABC_UserLegalEntity r on r.LegalEntityObjectId = jr.LegalEntityObjectId
     where jr.AmendmentApplicationObjectId = a_ObjectId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end AmendmentAppPublicUsers;

  -------------------------------------
  -- Programmer : Matthew Johnson
  -- Date       : 2015 September
  -- Purpose    : Returns all related New Applications for a public user.
  -------------------------------------
  procedure PublicNewApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_SelectedList                      udt_IdList;

  begin
    select relData.NewAppObjectId
      bulk collect into t_ObjectIdList
      from (select jr.NewApplicationObjectId NewAppObjectId
              from query.r_ABC_UserLegalEntity r
              join query.r_ABC_NewAppJobLicensee jr on jr.LegalEntityObjectId = r.LegalEntityObjectId
             where r.UserId = a_ObjectId
             union
            select r.NewApplicationJobId NewAppObjectId
              from query.r_ABC_NewApplicationOnlineUser r
             where r.UserId = a_ObjectId
             union
            select /*+cardinality (x 1)*/ x.ObjectId NewAppObjectId
              from query.r_Abc_Userlegalentity r
              join table(cast(api.pkg_SimpleSearch.CastableObjectsByIndex('j_ABC_NewApplication', 'OnlineUseLegalEntityObjectId', r.LegalEntityObjectId) as api.udt_ObjectList)) x on 1=1
             where r.UserId = a_ObjectId) relData;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PublicNewApps;

  -------------------------------------
  -- Programmer : Matthew J
  -- Date       : 2015 September
  -- Purpose    : Returns all related users for a new application.
  -------------------------------------
  procedure NewAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select relData.UserId
      bulk collect into t_ObjectIdList
      from (select r.UserId
              from query.r_ABC_NewAppJobLicensee jr
              join query.r_ABC_UserLegalEntity r on r.LegalEntityObjectId = jr.LegalEntityObjectId
             where jr.NewApplicationObjectId = a_ObjectId
             union
            select r.UserId
              from query.r_ABC_NewApplicationOnlineUser r
             where r.NewApplicationJobId = a_ObjectId
             union
            select r.UserId
              from query.r_ABC_UserLegalEntity r
             where r.LegalEntityObjectId = api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'OnlineUseLegalEntityObjectId')) relData;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end NewAppPublicUsers;

  -------------------------------------
  -- Programmer : Matthew Johnson
  -- Date       : 2015 September
  -- Purpose    : Returns all related Permit Applications for a public user.
  -------------------------------------
  procedure PublicPermitApps (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_TempList                          udt_IdList;

  begin

    select d.PermitApplicationId
      bulk collect into t_ObjectIdList
      from (select x.PermitApplicationId PermitApplicationId
              from query.r_ABC_UserLegalEntity l
              join abc.lepermitappxref_t x on x.LegalEntityId = l.LegalEntityObjectId
             where l.UserId = a_ObjectId
             union
            select r.PermitApplicationObjectId PermitApplicationId
              from query.r_ABC_PermitAppOnlineUser r
             where r.UserObjectId = a_ObjectId
             union
            select /*+cardinality (x 1)*/ x.ObjectId PermitApplicationId
              from query.r_ABC_UserLegalEntity r
              join table(cast(api.pkg_SimpleSearch.CastableObjectsByIndex('j_ABC_PermitApplication', 'OnlineUseLegalEntityObjectId', r.LegalEntityObjectId) as api.udt_ObjectList)) x on 1=1
             where r.UserId = a_ObjectId
             union
            select al.PermitAppObjectId NewAppObjectId
              from query.r_Abc_Userlegalentity ul
              join query.r_abc_licenselicenseele ll on ll.LegalEntityObjectId = ul.LegalEntityObjectId
              join query.r_abc_permitapplicationlicense al on al.LicenseObjectId = ll.LicenseObjectId
              join query.r_abc_PermitAppPermitType papt on papt.PermitApplicationId = al.PermitAppObjectId
              join query.o_abc_PermitType pt on pt.ObjectId = papt.PermitTypeId
             where ul.UserId = a_ObjectId
               and pt.PermitteeIsLicensee= 'Y') d;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end PublicPermitApps;

  -------------------------------------
  -- Programmer : Matthew J
  -- Date       : 2015 September
  -- Purpose    : Returns all related users for a permit application.
  -------------------------------------
  procedure PermitAppPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_LicenseeIdList                    udt_IdList;

  begin

    select d.UserId
      bulk collect into t_ObjectIdList
      from (select l.UserId UserId
              from abc.lepermitappxref_t x
              join query.r_ABC_UserLegalEntity l on x.LegalEntityId = l.LegalEntityObjectId
             where x.PermitApplicationId = a_ObjectId
             union
            select r.UserObjectId UserId
              from query.r_ABC_PermitAppOnlineUser r
             where r.PermitApplicationObjectId = a_ObjectId
             union
            select r.UserId UserId
              from query.r_ABC_UserLegalEntity r
             where r.LegalEntityObjectId = api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'OnlineUseLegalEntityObjectId')) d;
   --If Permittee Is Licensee, get the Licensee from the License.
    if api.pkg_columnquery.value(api.pkg_columnquery.Value(a_ObjectId, 'PermitTypeObjectId'), 'PermitteeIsLicensee') = 'Y' then
      select ul.UserId
        bulk collect into t_LicenseeIdList
        from query.r_Abc_Userlegalentity ul
       where api.pkg_columnquery.value(a_ObjectId, 'PermiteeFromLicense') = ul.LegalEntityObjectId;
       extension.pkg_collectionutils.Append(t_ObjectIdList, t_LicenseeIdList);
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end PermitAppPublicUsers;

  /*---------------------------------------------------------------------------
   * FeesFromePaymentMethod() -- PUBLIC
   *   Create procedural rel to populate Fees from ePayment Method
   *-------------------------------------------------------------------------*/
  procedure FeesFromePaymentMethod (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_FeeList                           api.udt_ObjectList;
    t_SourceObjectId                    udt_Id;
    t_IgnoreFeeList                     udt_IdList;
  begin

    a_ObjectList := api.udt_ObjectList();
    t_FeeList := api.udt_ObjectList();

    t_SourceObjectId := api.pkg_columnquery.NumericValue(a_ObjectId, 'SourceObjectId');
    if t_SourceObjectId is null then
      api.pkg_errors.RaiseError(-20000, 'Can''t get source object id');
    end if;

    if api.pkg_ColumnQuery.Value(t_SourceObjectId, 'ObjectDefName') = 'o_ABC_LegalEntity' then
      abc.Pkg_Abc_Proceduralrels.OutstandingFeesForLegalEntity(t_SourceObjectId, null, t_FeeList);
    else
      abc.Pkg_Abc_Proceduralrels.OutstandingFeesForJob(t_SourceObjectId, null, t_FeeList);
    end if;
     
    t_IgnoreFeeList := api.pkg_Search.ObjectsByRelColumnValue('o_Fee', 'IgnoreePayment', 'ObjectId', a_ObjectID);
    
    <<FeeListLoop>>
    for i in 1..t_FeeList.count() loop
      for c in 1..t_IgnoreFeeList.count() loop
        continue FeeListLoop when t_FeeList(i).ObjectId = t_IgnoreFeeList(c);
      end loop;
      -- If fee not in the ignore list, add it to our output
      a_ObjectList.extend(1);
      a_ObjectList(a_ObjectList.count()) := t_FeeList(i);
    end loop;

  end FeesFromePaymentMethod;

 -------------------------------------
  -- Purpose    : Returns all related Permit Renewals for a public user.
  -------------------------------------
  procedure PublicPermitRenewal (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select d.PermitRenewalId
      bulk collect into t_ObjectIdList
      from (select /*+cardinality (x 1)*/ ptr.PermitRenewJobId PermitRenewalId
              from query.r_ABC_UserLegalEntity r
              join table(cast(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_Permit', 'dup_FormattedPermitteeObjectId', r.LegalEntityObjectId) as api.udt_ObjectList)) x on 1=1
              join query.r_ABC_PermitToRenewJobPermit ptr on ptr.PermitToRenewObjectId = x.ObjectId
             where r.UserId = a_ObjectId
             union
            select r.PermitRenewalObjectId PermitRenewalId
              from query.r_ABC_PermitRenewalOnlineUser r
             where r.UserObjectId = a_ObjectId) d;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PublicPermitRenewal;

 -------------------------------------
  -- Purpose    : Returns all related users for a Permit Renewal.
  -------------------------------------
  procedure PermitRenewalPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select d.UserId
      bulk collect into t_ObjectIdList
      from (select r.UserId UserId
              from query.r_ABC_PermitToRenewJobPermit ptr
              join query.o_ABC_Permit p on p.ObjectId = ptr.PermitToRenewObjectId
              join query.r_ABC_UserLegalEntity r on r.LegalEntityObjectId = p.dup_FormattedPermitteeObjectId
             where ptr.PermitRenewJobId = a_ObjectId
             union
            select r.UserObjectId UserId
              from query.r_ABC_PermitRenewalOnlineUser r
             where r.PermitRenewalObjectId = a_ObjectId) d;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PermitRenewalPublicUsers;

-------------------------------------
  -- Purpose    : This returns JobIds for Permit Applications with the same Permit Number.
  -------------------------------------
   procedure AssociatedPermitApplications (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_PermitNumber                      varchar2(50) := api.pkg_columnquery.value(a_ObjectId, 'PermitNumber');
    t_PermitTypeObjectId                udt_id := api.pkg_columnquery.Value(a_ObjectId, 'PermitTypeObjectId');

  begin
   select y.JobId
     bulk collect into t_ObjectIdList
     from (select /*+cardinality (x,1)*/ r.PermitApplicationObjectId JobId, api.pkg_columnquery.NumericValue(x.ObjectId,'PermitTypeObjectId') PermitTypeObjectId
             from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', t_PermitNumber)) x
             join query.r_ABC_PermitAppPermit r on r.PermitObjectId = x.objectid) y
    where y.PermitTypeObjectId = t_PermitTypeObjectId;

    if t_ObjectIdList.count() = 0 then
      select ap.PermitApplicationObjectId
        bulk collect into t_ObjectIdList
        from query.r_ABC_PermitAppPermit ap
       where ap.PermitObjectId = a_ObjectId;
    end if;

   a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end AssociatedPermitApplications;

  -------------------------------------
  -- Programmer : Elijah R
  -- Date       : 11/09/2015
  -- Purpose    : This returns JobIds for Permit Batch Renewals with the same Permit Number.
  -------------------------------------
   procedure AssociatedPermitBatchRenewals (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_PermitNumber                      varchar2(50) := api.pkg_columnquery.value(a_ObjectId, 'PermitNumber');
    t_PermitTypeObjectId                udt_id := api.pkg_columnquery.Value(a_ObjectId, 'PermitTypeObjectId');

  begin

    select y.JobId
      bulk collect into t_ObjectIdList
      from (select /*+cardinality (x,1)*/ r.BatchRenewalNotificationJobId JobId, api.pkg_columnquery.NumericValue(x.ObjectId,'PermitTypeObjectId') PermitTypeObjectId
              from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', t_PermitNumber)) x
              join query.r_ABC_BatchRenewalJobPerGrcPd r on r.PermitObjectId = x.objectid
             union
             select /*+cardinality (x,1)*/ r.BatchRenewalNotificationJobId JobId, api.pkg_columnquery.NumericValue(x.ObjectId,'PermitTypeObjectId') PermitTypeObjectId
              from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', t_PermitNumber)) x
              join query.r_ABC_BatchRenewalJobPermitExp r on r.PermitObjectId = x.objectid
            ) y
     where y.PermitTypeObjectId = t_PermitTypeObjectId;
    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end AssociatedPermitBatchRenewals;

-------------------------------------
  -- Purpose    : This returns JobIds for Permit Renewals with the same Permit Number.
  -------------------------------------
   procedure AssociatedPermitRenewals (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_PermitNumber                      varchar2(50) := api.pkg_columnquery.value(a_ObjectId, 'PermitNumber');
    t_PermitTypeObjectId                udt_id := api.pkg_columnquery.Value(a_ObjectId, 'PermitTypeObjectId');

  begin
   select y.JobId
     bulk collect into t_ObjectIdList
     from (select /*+cardinality (x,1)*/ r.PermitRenewJobId JobId, api.pkg_columnquery.NumericValue(x.ObjectId,'PermitTypeObjectId') PermitTypeObjectId
             from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_Permit', 'PermitNumber', t_PermitNumber)) x
             join query.r_ABC_PermitToRenewJobPermit r on r.PermitToRenewObjectId = x.objectid) y
    where y.PermitTypeObjectId = t_PermitTypeObjectId;

   a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end AssociatedPermitRenewals;

  -------------------------------------
  -- Date       : 2016 Jan 26
  -- Purpose    : This returns JobIds for Appeal Jobs with this license number.
  -------------------------------------
   procedure AppealJobsForLicense (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select al.AppealId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumber')) as api.udt_ObjectList)) x

        join query.r_ABC_AppealLicense al on al.LicenseId = x.objectid
         minus
        select al.AppealId
          from query.r_ABC_AppealLicense al
        where al.LicenseId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end AppealJobsForLicense;

  -------------------------------------
  -- Date       : 2016 Jan 26
  -- Purpose    : This returns JobIds for Petition Jobs with this license number.
  -------------------------------------
   procedure PetitionJobsForLicense (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select lp.PetitionId
        bulk collect into t_ObjectIdList
        from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'LicenseNumber', api.pkg_columnquery.value(a_ObjectId, 'LicenseNumber')) as api.udt_ObjectList)) x
        join query.r_ABC_LicensePetition lp on lp.LicenseId = x.objectid
         minus
        select lp.PetitionId
          from query.r_ABC_LicensePetition lp
        where lp.LicenseId = a_ObjectId;
        a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PetitionJobsForLicense;

  -------------------------------------
  -- Purpose    : This returns Responsible Party for Petition Job
  -------------------------------------
   procedure PetitionResponsibleParty (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_col0 udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlineUserObjectId');
    t_col1 udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'LicenseeObjectId');
  begin
    a_ObjectList := api.udt_ObjectList();
    if t_col0 is not null then
      a_ObjectList.Extend(1);
      a_ObjectList(a_ObjectList.count) := api.udt_Object(t_col0);
    elsif t_col1 is not null then
      a_ObjectList.Extend(1);
      a_ObjectList(a_ObjectList.count) := api.udt_Object(t_col1);
    end if;
  end PetitionResponsibleParty;

  -------------------------------------
  -- Purpose    : Returns all related users for a Petition.
  -------------------------------------
  procedure PetitionPublicUsers (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select relData.UserId
      bulk collect into t_ObjectIdList
      from (select r.UserId
              from query.r_ABC_PetitionOnlineUser r
             where r.PetitionJobId = a_ObjectId
             union
            select r2.UserId
              from query.r_ABC_PetitionAmendOnlineUser r2
             where r2.PetitionAmendmentId = a_ObjectId
             union
            select r1.UserId
              from query.r_ABC_UserLegalEntity r1
             where r1.LegalEntityObjectId = api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'LicenseeObjectId')) relData;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end PetitionPublicUsers;

 -------------------------------------
  -- Purpose    : Returns all Petitions for a public user.
  -------------------------------------
  procedure PublicPetition (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;

  begin
    select distinct jr.PetitionObjectId
      bulk collect into t_ObjectIdList
      from query.r_ABC_UserLegalEntity r
      join query.r_ABC_LicenseeIND_Petition jr on jr.LegalEntityObjectId = r.LegalEntityObjectId
     where r.UserId = a_ObjectId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PublicPetition;

 -------------------------------------
  -- Purpose    : Returns the clerk for an expiration job.
  -------------------------------------
  procedure ExpirationDefaultClerk (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_ObjectToExpireId                  udt_Id;
  begin
    t_ObjectToExpireId := coalesce(api.pkg_columnquery.value(a_ObjectId, 'LicenseObjectId'), api.pkg_columnquery.value(a_ObjectId, 'PermitObjectId'));
    --Find the correct clerk based on the License's County
    if api.pkg_columnquery.value(a_ObjectId, 'LicenseObjectId') is not null then
      if api.pkg_columnquery.Value(t_ObjectToExpireId, 'IssuingAuthority') = 'Municipality' then
        select df.ClerkObjectId
          bulk collect into t_ObjectIdList
          from query.r_abc_licenseoffice lo
          join query.r_ClerkMunicipality df on df.MunicipalityObjectId = lo.OfficeObjectId
         where lo.LicenseObjectId = t_ObjectToExpireId;
      else
        select df.DefaultClerkId
          bulk collect into t_ObjectIdList
          from query.r_abc_licenselicensetype lt
          join query.r_abc_licensetypedefaultclerk df on df.LicenseTypeId = lt.LicenseTypeObjectId
         where lt.LicenseObjectId = t_ObjectToExpireId;
      end if;
    end if;
    --Find the correct clerk based on the Permit Type
    if api.pkg_columnquery.value(a_ObjectId, 'PermitObjectId') is not null then
      select df.DefaultClerkId
        bulk collect into t_ObjectIdList
        from query.r_abc_permitpermittype pt
        join query.r_abc_permittypedefaultclerk df on df.PermitTypeId = pt.PermitTypeId
       where pt.PermitId = t_ObjectToExpireId;
    end if;
    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end ExpirationDefaultClerk;

 -------------------------------------
  -- Purpose    : Returns the Additional Warehouse Licenses for a Parent Related License
  -------------------------------------
  procedure AdditionalWarehouses (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
     t_ObjectIdList                   udt_IdList;
  begin
    Select x.LicenseObjectId
    bulk collect into t_ObjectIdList
    from
     (select mll3.LicenseObjectId, api.pkg_columnquery.value(mll3.LicenseObjectId, 'IslatestVersion') IslatestVersion
      from query.r_abc_masterlicenselicense mll
      join query.r_abc_masterlicenselicense mll2 on mll2.MasterLicenseObjectId = mll.MasterLicenseObjectId
      join query.r_ABC_AddtlWarehouseLicense awl on  awl.LicenseObjectId = mll.LicenseObjectId
      join query.r_abc_masterlicenselicense mll3 on mll3.LicenseObjectId = awl.AddtlWhseLicenseObjectId
     where mll.LicenseObjectId = a_ObjectId

    union

    select mll3.LicenseObjectId, api.pkg_columnquery.value(mll3.LicenseObjectId, 'IslatestVersion') IslatestVersion
      from query.r_abc_masterlicenselicense mll
      join query.r_abc_masterlicenselicense mll2 on mll2.MasterLicenseObjectId = mll.MasterLicenseObjectId
      join query.r_ABC_AddtlWarehousehisLicense awh on awh.LicenseHistObjectId = mll2.LicenseObjectId
      join query.r_abc_masterlicenselicense mll3 on mll3.LicenseObjectId = awh.AddtlWhseLicenseObjectId
     where mll.LicenseObjectId = a_ObjectId) x
    where x.IslatestVersion = 'Y';

     a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

   end AdditionalWarehouses;

  -------------------------------------
  -- Purpose    : This returns the users selected as Default Clerk on Municipality or License Type
  --              depending on the Issuing Authority of the License
  --              This procedure is also called from the Online Application Screener (PRC) relationship
  --              on Amendment Job
  -------------------------------------
  procedure SelectedOnlineRenewalScreeners (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_IsStateIssued                     varchar2(01) := api.pkg_columnquery.Value(a_ObjectId,'IsStateIssued');
    t_LicenseTypeObjectId               udt_id := api.pkg_columnquery.NumericValue(a_ObjectId,'LicenseTypeObjectId');
    t_MunicipalityId                    udt_id := api.pkg_columnquery.NumericValue(a_ObjectId,'OfficeObjectId');

  begin

    if t_IsStateIssued = 'Y' then
       select r.DefaultClerkId
       bulk   collect into t_ObjectIdList
       from   query.r_ABC_LicenseTypeDefaultClerk r
       where  r.LicenseTypeId = t_LicenseTypeObjectId;
    else
       select r.ClerkObjectId
       bulk   collect into t_ObjectIdList
       from   query.r_ClerkMunicipality r
       where  r.MunicipalityObjectId = t_MunicipalityId;
    end if;
    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end SelectedOnlineRenewalScreeners;

  -------------------------------------
  -- Purpose    : This returns the latest version of a License for a Permit.
  -------------------------------------
  procedure LatestLicenseForPermit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_LicenseObjectId                   udt_id := 0;
  begin

     for gl in (select pgl.LicenseObjectId
                from   query.r_abc_permitgenerationlicense pgl
                where  pgl.PermitObjectId = a_ObjectId
               ) loop
        if api.pkg_columnquery.Value(gl.LicenseObjectId,'IsLatestVersion') = 'Y'
           then t_LicenseObjectId := gl.licenseobjectid;
           exit;
        end if;
     end loop;
     -- No "Latest and Greatest" version found, get the current License
     if t_LicenseObjectId = 0 then
        begin
           select pl.LicenseObjectId
           into   t_LicenseObjectId
           from   query.r_permitlicense pl
           where  pl.PermitObjectId = a_ObjectId;
        exception
           when no_data_found then
                t_LicenseObjectId := 0;
        end;
     end if;

     t_ObjectIdList(1) := t_LicenseObjectId;
     a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end LatestLicenseForPermit;

  -------------------------------------
  -- Purpose    : This returns all Vehicles with Permits that were NOT cancelled by the system
  -------------------------------------
  procedure VehiclesForLE (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
  begin
    select pv.VehicleObjectId
    bulk   collect into t_ObjectIdList
    from   query.r_abc_permitvehicle pv
    join query.r_ABC_PermitPermittee pp on pp.PermitObjectId = pv.PermitObjectId
    where  pp.PermitteeObjectId = a_ObjectId
    and    api.pkg_columnquery.Value(pp.PermitObjectId, 'SystemCancelled') = 'N';

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end VehiclesForLE;

  -------------------------------------
  -- Purpose    : This returns all CTW Permit Owners
  -------------------------------------
  procedure PermitOnlineOwner (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
  begin
    select r1.OwnerObjectId
    bulk   collect into t_ObjectIdList
    from   query.r_ABC_RenewAppJobPermit r
    join   query.r_ABC_PermitRenewOwnerOnline r1 on r1.PermitRenewalObjectId = r.JobId
    where  r.PermitId = a_ObjectId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PermitOnlineOwner;

  -------------------------------------
  -- Purpose    : This returns all CTW Permit Solicitors
  -------------------------------------
  procedure PermitOnlineSolicitor (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
  begin
    select r1.SolicitorObjectId
    bulk   collect into t_ObjectIdList
    from   query.r_ABC_RenewAppJobPermit r
    join   query.r_Abc_PermitRenSolicitorOnline r1 on r1.PermitRenewalObjectId = r.JobId
    where  r.PermitId = a_ObjectId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PermitOnlineSolicitor;

  -------------------------------------
  -- Purpose    : This returns the person to whom the process should be assigned
  -------------------------------------
  procedure AssigneeNewInformation (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
  begin
    if api.pkg_columnquery.Value(a_ObjectId, 'EnteredOnline') = 'Y' then
      -- Return Public User
      select r.UserId
      bulk   collect into t_ObjectIdList
      from   query.r_Abc_Newapplicationonlineuser r
      where  r.NewApplicationJobId = a_ObjectId
      union
      select r.UserId
      from   query.r_ABC_AmendAppOnlineUser r
      where  r.AmendApplicationJobId = a_ObjectId;
    else
      -- Return Default Supervisor on job
      select j.DefaultSupervisorObjectId
      bulk   collect into t_ObjectIdList
      from   query.j_abc_newapplication j
      where  j.ObjectId = a_ObjectId
      union
      select j.DefaultSupervisorObjectId
      from   query.j_abc_amendmentapplication j
      where  j.ObjectId = a_ObjectId;
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end AssigneeNewInformation;

  /*---------------------------------------------------------------------------
   * PoliceReviews() -- PUBLIC
   *   Returns list of Police Review Processes on New and Renewal Permit
   * Applications based on Police User Municipalities.
   *-------------------------------------------------------------------------*/
  procedure PoliceReviews (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_AppToPermitEndpointId             udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PermitApplication', 'AppToPermit');
    t_MunicipalityIds                   api.udt_ObjectList;
    t_MuniHoldStatusId                  udt_Id;
    t_MuniPoliceEndpointId              udt_Id := api.pkg_ConfigQuery.EndPointIdForName(
        'u_Users', 'MunicipalityPolice');
    t_MuniReviewStatusId                udt_Id;
    t_PermitMuniEndpointId              udt_Id := api.pkg_ConfigQuery.EndPointIdForName(
        'o_ABC_Permit', 'Municipality');
    t_PoliceReviewIds                   udt_IdList;
    t_PoliceReviewIdsFinal              udt_IdList;
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'p_ABC_PoliceReview');
    t_RenewToPermitEndpointId           udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PermitRenewal', 'RenewToPermit');
    t_ShowMore                          varchar2(1) := api.pkg_ColumnQuery.Value(
        a_ObjectId, 'ShowMorePoliceReviews');
  begin

    select s.StatusId
    into t_MuniHoldStatusId
    from api.Statuses s
    where s.Tag = 'MHOLD';

    select s.StatusId
    into t_MuniReviewStatusId
    from api.Statuses s
    where s.Tag = 'MREV';

    select api.udt_Object(r.ToObjectId) ObjectId
    bulk collect into t_MunicipalityIds
    from api.relationships r
    where r.FromObjectId = a_ObjectId
      and r.EndpointId = t_MuniPoliceEndpointId;
    if t_MunicipalityIds.count() = 0 then
      api.pkg_Errors.RaiseError(-20000, 'A municipality is not related to your user;' ||
          'please contact the POSSE ABC system administrator.');
    end if;

    select p.ProcessId
    bulk collect into t_PoliceReviewIds
    from
      api.processes p
      join api.Jobs j
          on p.JobId = j.JobId
      join api.relationships r
          on j.JobId = r.FromObjectId
      join api.relationships rr
          on r.ToObjectId = rr.FromObjectId
      join table(t_MunicipalityIds) m
          on rr.ToObjectId = m.ObjectId
    where p.ProcessTypeId = t_ProcessTypeId
      and p.outcome is null
      and j.StatusId in (t_MuniHoldStatusId, t_MuniReviewStatusId)
      and r.EndpointId in (t_AppToPermitEndpointId, t_RenewToPermitEndpointId)
      and rr.EndpointId = t_PermitMuniEndpointId;

    if t_ShowMore = 'Y' then
      t_PoliceReviewIdsFinal := t_PoliceReviewIds;
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'ShowMorePoliceReviews', 'N');
    else
      if t_PoliceReviewIds.count() < 10 then
        t_PoliceReviewIdsFinal := t_PoliceReviewIds;
      else
        for i in 1..10 loop
          t_PoliceReviewIdsFinal(i) := t_PoliceReviewIds(i);
        end loop;
      end if;
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_PoliceReviewIdsFinal);

  end PoliceReviews;

  /*---------------------------------------------------------------------------
   * MunicipalReviews() -- PUBLIC
   *   Returns list of Municipal Review Processes on New and Renewal Permit
   * Applications based on the Municipality associated with the Municipal User.
   *-------------------------------------------------------------------------*/
  procedure MunicipalReviews (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_AppToPermitEndpointId             udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PermitApplication', 'AppToPermit');
    t_MunicipalityIds                   api.udt_ObjectList;
    t_MunicipalityEndpointId            udt_Id := api.pkg_ConfigQuery.EndPointIdForName(
        'u_Users', 'Municipality');
    t_MunicipalReviewIds                udt_IdList;
    t_MunicipalReviewIdsFinal           udt_IdList;
    t_MuniHoldStatusId                  udt_Id;
    t_MuniReviewStatusId                udt_Id;
    t_PermitMuniEndpointId              udt_Id := api.pkg_ConfigQuery.EndPointIdForName(
        'o_ABC_Permit', 'Municipality');
    t_ProcessTypeId                     udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'p_ABC_MunicipalityReview');
    t_RenewToPermitEndpointId           udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'j_ABC_PermitRenewal', 'RenewToPermit');
    t_ShowMore                          varchar2(1) := api.pkg_ColumnQuery.Value(
        a_ObjectId, 'ShowMoreMunicipalReviews');
  begin

    select s.StatusId
    into t_MuniHoldStatusId
    from api.Statuses s
    where s.Tag = 'MHOLD';

    select s.StatusId
    into t_MuniReviewStatusId
    from api.Statuses s
    where s.Tag = 'MREV';

    select api.udt_Object(r.ToObjectId) ObjectId
    bulk collect into t_MunicipalityIds
    from api.relationships r
    where r.FromObjectId = a_ObjectId
      and r.EndpointId = t_MunicipalityEndpointId;
    if t_MunicipalityIds.count() = 0 then
      api.pkg_Errors.RaiseError(-20000, 'A municipality is not related to your user;' ||
          'please contact the POSSE ABC system administrator.');
    end if;

    select p.ProcessId
    bulk collect into t_MunicipalReviewIds
    from
      api.processes p
      join api.Jobs j
          on p.JobId = j.JobId
      join api.relationships r
          on j.JobId = r.FromObjectId
      join api.relationships rr
          on r.ToObjectId = rr.FromObjectId
      join table(t_MunicipalityIds) m
          on rr.ToObjectId = m.ObjectId
    where p.outcome is null
      and p.ProcessTypeId = t_ProcessTypeId
      and j.StatusId in (t_MuniHoldStatusId, t_MuniReviewStatusId)
      and r.EndpointId in (t_AppToPermitEndpointId, t_RenewToPermitEndpointId)
      and rr.EndpointId = t_PermitMuniEndpointId;

    if t_ShowMore = 'Y' then
      t_MunicipalReviewIdsFinal := t_MunicipalReviewIds;
      api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'ShowMoreMunicipalReviews', 'N');
    else
      if t_MunicipalReviewIds.count() < 10 then
        t_MunicipalReviewIdsFinal := t_MunicipalReviewIds;
      else
        for i in 1..10 loop
          t_MunicipalReviewIdsFinal(i) := t_MunicipalReviewIds(i);
        end loop;
      end if;
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_MunicipalReviewIdsFinal);

  end MunicipalReviews;

  /*---------------------------------------------------------------------------
   * MunicipalPoliceReviewDocs() -- PUBLIC
   *   Returns all documents submitted by Municipal and Police Users through
   * the Municipal and Police Review processes.
   *-------------------------------------------------------------------------*/
  procedure MunicipalPoliceReviewDocs (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_DocumentIds                       udt_IdList;
    t_MuniReviewProcessTypeId           udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'p_ABC_MunicipalityReview');
    t_MuniRevToDocEndpointId            udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'p_ABC_MunicipalityReview', 'Documents');
    t_ObjectDefTypeId                   udt_Id;
    t_PoliceReviewProcessTypeId         udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'p_ABC_PoliceReview');
    t_PoliceRevToDocEndpointId          udt_Id := api.pkg_ConfigQuery.EndpointIdForName(
        'p_ABC_PoliceReview', 'Documents');
  begin

    select odt.ObjectDefTypeId
    into t_ObjectDefTypeId
    from
      api.objects o
      join api.ObjectDefTypes odt
          on o.ObjectDefTypeId = odt.ObjectDefTypeId
    where o.ObjectId = a_ObjectId;

    -- Job Types
    if t_ObjectDefTypeId = 2 then
      select r.ToObjectId
      bulk collect into t_DocumentIds
      from
        api.processes p
        join api.jobs j
            on p.JobId = j.JobId
        join api.relationships r
            on p.ProcessId = r.FromObjectId
      where p.ProcessTypeId in (t_MuniReviewProcessTypeId, t_PoliceReviewProcessTypeId)
        and p.Outcome is not null
        and j.JobId = a_ObjectId
        and r.EndpointId in (t_MuniRevToDocEndpointId, t_PoliceRevToDocEndpointId);

    -- Process Types
    elsif t_ObjectDefTypeId = 3 then
      select r.ToObjectId
      bulk collect into t_DocumentIds
      from
        api.processes p
        join api.jobs j
            on p.JobId = j.JobId
        join api.processes p2
            on j.JobId = p2.JobId
        join api.relationships r
            on p2.ProcessId = r.FromObjectId
      where p.ProcessId = a_ObjectId
        and p2.Outcome is not null
        and p2.ProcessTypeId in (t_MuniReviewProcessTypeId, t_PoliceReviewProcessTypeId)
        and r.EndpointId in (t_MuniRevToDocEndpointId, t_PoliceRevToDocEndpointId);
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_DocumentIds);

  end MunicipalPoliceReviewDocs;

  /*---------------------------------------------------------------------------
   * MunicipalPoliceReviewResponses() -- PUBLIC
   *   Given a ProcessId or JobId, return all Municipal and Police Review
   * Processes related to the job.
   *-------------------------------------------------------------------------*/
  procedure MunicipalPoliceReviewResponses (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_JobId                             udt_Id;
    t_MuniReviewProcessTypeId           udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'p_ABC_MunicipalityReview');
    t_ObjectDefTypeId                   udt_Id;
    t_PoliceReviewProcessTypeId         udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName(
        'p_ABC_PoliceReview');
    t_ResponseIds                       udt_IdList;
  begin

    -- Get ObjectDefType of a_Object
    select odt.ObjectDefTypeId
    into t_ObjectDefTypeId
    from
      api.objects o
      join api.ObjectDefTypes odt
          on o.ObjectDefTypeId = odt.ObjectDefTypeId
    where o.ObjectId = a_ObjectId;

    -- Job Types
    if t_ObjectDefTypeId = 2 then
      t_JobId := a_ObjectId;
    -- Get the JobId from the process
    elsif t_ObjectDefTypeId = 3 then
      t_JobId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'JobId');
    end if;

    -- Get Municipal and Police Review Processes related to the job
    select p.ProcessId
    bulk collect into t_ResponseIds
    from
      api.processes p
      join api.jobs j
          on p.JobId = j.JobId
    where p.ProcessTypeId in (t_MuniReviewProcessTypeId, t_PoliceReviewProcessTypeId)
      and j.JobId = t_JobId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ResponseIds);

  end MunicipalPoliceReviewResponses;

  /*---------------------------------------------------------------------------
   * GetUserToDoNotes() -- PUBLIC
   *   Given a UserId, return all To Do Notes and their associated incomplete
   * Processes
   *-------------------------------------------------------------------------*/
  procedure GetUserToDoNotes(

    a_ObjectId                          udt_Id,
    a_Endpoint                          udt_Id,
    a_ObjectList                        out api.udt_ObjectList
  ) is
    t_NoteIdList                        udt_IdList;
  begin

    -- Get To Do Note Data List
    select
      max(tn.objectid)
    bulk collect into
      t_NoteIdList
    from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_ToDoNote','UserID',a_ObjectId)) x
    join query.o_abc_todonote tn on tn.ObjectId = x.objectid
    join api.incompleteprocessassignments ip on ip.ProcessId = tn.ProcessId and ip.AssignedTo = a_ObjectId
    group by ip.processid;


    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_NoteIdList);

  end GetUserToDoNotes;

  /*---------------------------------------------------------------------------
   * PublicDeniedLicenses() -- PUBLIC
   *   Returns all Denied Applications related to a Public User.
   *-------------------------------------------------------------------------*/
  procedure PublicDeniedLicenses (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_DeniedJobIds                      udt_IdList;
    t_DeniedJobIdsFinal                 udt_IdList;
    t_JobIds                            udt_IdList;
    t_JobObjects                        api.udt_ObjectList;
    t_LegalEntityIds                    udt_IdList;
    t_LegalEntityObjects                api.udt_ObjectList;
    t_ShowMore                          varchar2(1);
  begin

    t_ShowMore := api.pkg_ColumnQuery.Value(a_ObjectId, 'ShowMoreDeniedLicenses');

    -- Get LE(s) related to Online User
    select ule.LegalEntityObjectId
    bulk collect into t_LegalEntityIds
    from query.r_ABC_UserLegalEntity ule
    where ule.userid = a_ObjectId;

    t_LegalEntityObjects := extension.pkg_utils.ConvertToObjectList(t_LegalEntityIds);

    -- Get Licensing Jobs related to Online User LE(s)
    -- Based on abc.pkg_ABC_ProceduralRels packages:
    --   PublicAmendmentApps(), PublicNewApps(), PublicRenewalApps()
    select jr1.AmendmentApplicationObjectId ObjectId
    bulk collect into t_JobIds
    from
      table(t_LegalEntityObjects) l
      join query.r_ABC_LicenseeIND_AmendmentApp jr1
          on jr1.LegalEntityObjectId = l.objectid
    union
    select jr2.NewApplicationObjectId ObjectId
    from
      table(t_LegalEntityObjects) l2
      join query.r_ABC_NewAppJobLicensee jr2
          on jr2.LegalEntityObjectId = l2.ObjectId
    union
    select r3.NewApplicationJobId ObjectId
    from query.r_ABC_NewApplicationOnlineUser r3
    where r3.UserId = a_ObjectId
    union
    select x.ObjectId --+cardinality (x 1)
    from
      table(t_LegalEntityObjects) r4
      join table(api.pkg_SimpleSearch.CastableObjectsByIndex(
          'j_ABC_NewApplication', 'OnlineUseLegalEntityObjectId', r4.ObjectId)) x
          on 1=1
    union
    select jr5.PetitionObjectId ObjectId
    from
      table(t_LegalEntityObjects) r5
      join query.r_ABC_LicenseeIND_Petition jr5
          on jr5.LegalEntityObjectId = r5.ObjectId
    union
    select jr6.RenewalApplicationObjectId ObjectId
    from
      table(t_LegalEntityObjects) r6
      join query.r_ABC_LicenseeIND_RenewalApp jr6
          on jr6.LegalEntityObjectId = r6.ObjectId;

    t_JobObjects := extension.pkg_utils.ConvertToObjectList(t_JobIds);

    -- Get denied licensing jobs, excluding Autocancelled Jobs
    select x.ObjectId
    bulk collect into t_DeniedJobIds
    from table(t_JobObjects) x
    where api.pkg_columnquery.value(x.objectid, 'OnlineDenied') = 'Y'
      and api.pkg_columnquery.value(x.objectid, 'Autocancelled') = 'N';

    if t_ShowMore = 'Y' then
      t_DeniedJobIdsFinal := t_DeniedJobIds;
    else
      if t_DeniedJobIds.count() < 10 then
        t_DeniedJobIdsFinal := t_DeniedJobIds;
      else
        for i in 1..10 loop
          t_DeniedJobIdsFinal(i) := t_DeniedJobIds(i);
        end loop;
      end if;
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_DeniedJobIdsFinal);

  end PublicDeniedLicenses;

  /*---------------------------------------------------------------------------
   * PublicDeniedPermits() -- PUBLIC
   *   Returns all Denied Permit Applications related to a Public User.
   *-------------------------------------------------------------------------*/
  procedure PublicDeniedPermits (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_DeniedJobIds                      udt_IdList;
    t_DeniedJobIdsFinal                 udt_IdList;
    t_JobIds                            udt_IdList;
    t_JobObjects                        api.udt_ObjectList;
    t_LegalEntityIds                    udt_IdList;
    t_LegalEntityObjects                api.udt_ObjectList;
    t_ShowMore                          varchar2(1);
  begin

    t_ShowMore := api.pkg_ColumnQuery.Value(a_ObjectId, 'ShowMoreDeniedPermits');

    -- Get LE(s) related to Online User
    select ule.LegalEntityObjectId
    bulk collect into t_LegalEntityIds
    from query.r_ABC_UserLegalEntity ule
    where ule.userid = a_ObjectId;

    t_LegalEntityObjects := extension.pkg_utils.ConvertToObjectList(t_LegalEntityIds);

    -- Get Permitting Jobs related to Online User LE(s)
    -- Based on abc.pkg_ABC_ProceduralRels packages:
    --   PublicPermitApps(), PublicPermitRenewal()
    select r1.PermitApplicationId ObjectId
    bulk collect into t_JobIds
    from
      table(t_LegalEntityObjects) l1
      join abc.lepermitappxref_t r1
          on r1.LegalEntityId = l1.ObjectId
    union
    select r2.PermitApplicationObjectId ObjectId
    from query.r_ABC_PermitAppOnlineUser r2
    where r2.UserObjectId = a_ObjectId
    union
    select x1.ObjectId ObjectId --+cardinality (x 1)
    from
      table(t_LegalEntityObjects) l2
      join table(api.pkg_SimpleSearch.CastableObjectsByIndex(
          'j_ABC_PermitApplication', 'OnlineUseLegalEntityObjectId', l2.ObjectId)) x1
          on 1=1
    union
    select r3.PermitAppObjectId ObjectId
    from
      table(t_LegalEntityObjects) l3
      join query.r_abc_licenselicenseele ll
          on ll.LegalEntityObjectId = l3.ObjectId
      join query.r_abc_permitapplicationlicense r3
          on r3.LicenseObjectId = ll.LicenseObjectId
      join query.r_abc_PermitAppPermitType r4
          on r4.PermitApplicationId = r3.PermitAppObjectId
      join query.o_abc_PermitType pt
          on pt.ObjectId = r4.PermitTypeId
    where pt.PermitteeIsLicensee= 'Y'
    union
    select r5.PermitRenewJobId ObjectId -- +cardinality (x 1)
    from
      table(t_LegalEntityObjects) l4
      join table(api.pkg_SimpleSearch.CastableObjectsByIndex(
          'o_ABC_Permit', 'dup_FormattedPermitteeObjectId', l4.ObjectId)) x2
          on 1=1
      join query.r_ABC_PermitToRenewJobPermit r5
          on r5.PermitToRenewObjectId = x2.ObjectId
    union
    select r6.PermitRenewalObjectId ObjectId
    from query.r_ABC_PermitRenewalOnlineUser r6
    where r6.UserObjectId = a_ObjectId;

    t_JobObjects := extension.pkg_utils.ConvertToObjectList(t_JobIds);

    -- Get denied permitting jobs
    select x.ObjectId
    bulk collect into t_DeniedJobIds
    from table(t_JobObjects) x
    where api.pkg_columnquery.value(x.ObjectId, 'OnlineDenied') = 'Y'
      and api.pkg_columnquery.value(x.objectid, 'Autocancelled') = 'N';

    if t_ShowMore = 'Y' then
      t_DeniedJobIdsFinal := t_DeniedJobIds;
    else
      if t_DeniedJobIds.count() < 10 then
        t_DeniedJobIdsFinal := t_DeniedJobIds;
      else
        for i in 1..10 loop
          t_DeniedJobIdsFinal(i) := t_DeniedJobIds(i);
        end loop;
      end if;
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_DeniedJobIdsFinal);

  end PublicDeniedPermits;

  /*---------------------------------------------------------------------------
   * PublicDeniedProducts() -- PUBLIC
   *   Returns all Denied Product Registration Applications related to a Public
   * User.
   *-------------------------------------------------------------------------*/
  procedure PublicDeniedProducts (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_DeniedJobIds                      udt_IdList;
    t_DeniedJobIdsFinal                 udt_IdList;
    t_JobIds                            udt_IdList;
    t_JobObjects                        api.udt_ObjectList;
    t_LegalEntityIds                    udt_IdList;
    t_LegalEntityObjects                api.udt_ObjectList;
    t_ShowMore                          varchar2(1);
  begin

    t_ShowMore := api.pkg_ColumnQuery.Value(a_ObjectId, 'ShowMoreDeniedProducts');

    -- Get LE(s) related to Online User
    select ule.LegalEntityObjectId
    bulk collect into t_LegalEntityIds
    from query.r_ABC_UserLegalEntity ule
    where ule.userid = a_ObjectId;

    t_LegalEntityObjects := extension.pkg_utils.ConvertToObjectList(t_LegalEntityIds);

    -- Get Product/Brand Registration Jobs related to Online User LE(s)
    -- Based on abc.pkg_ABC_ProceduralRels packages:
    --   PRPublicAmendmentApps(), PRPublicApps(), PRPublicRenewalApps()
    select r1.ProdRegJobId ObjectId
    bulk collect into t_JobIds
    from
      table(t_LegalEntityObjects) l1
      join query.r_ABC_PRAmendToRegistrant r1
          on r1.RegistrantObjectId = l1.ObjectId
    union
    select r2.PRAmendJobId ObjectId
    from query.r_ABC_PRAmendOnlineUser r2
    where r2.UserId = a_ObjectId
    union
    select r3.ProdRegJobId ObjectId
    from
      table(t_LegalEntityObjects) l2
      join query.r_ABC_ProdRegToRegistrant r3
          on r3.RegistrantObjectId = l2.ObjectId
    union
    select r4.ProductRegistrationJobId ObjectId
    from query.r_ABC_ProductRegOnlineUser r4
    where r4.UserId = a_ObjectId
    union
    select r5.ProdRegJobId ObjectId
    from
      table(t_LegalEntityObjects) l3
      join query.r_ABC_ProdRenewalToRegistrant r5
          on r5.RegistrantObjectId = l3.ObjectId
    union
    select r6.ProductRegistrationJobId ObjectId
    from query.r_ProductRenewalToUser r6
    where r6.UserId = a_ObjectId;

    t_JobObjects := extension.pkg_utils.ConvertToObjectList(t_JobIds);

    -- Get denied Product/Brand Registration jobs
    select x.ObjectId
    bulk collect into t_DeniedJobIds
    from table(t_JobObjects) x
    where api.pkg_columnquery.value(x.ObjectId, 'OnlineDenied') = 'Y'
      and api.pkg_columnquery.value(x.objectid, 'Autocancelled') = 'N';

    if t_ShowMore = 'Y' then
      t_DeniedJobIdsFinal := t_DeniedJobIds;
    else
      if t_DeniedJobIds.count() < 10 then
        t_DeniedJobIdsFinal := t_DeniedJobIds;
      else
        for i in 1..10 loop
          t_DeniedJobIdsFinal(i) := t_DeniedJobIds(i);
        end loop;
      end if;
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_DeniedJobIdsFinal);

  end PublicDeniedProducts;

  /*---------------------------------------------------------------------------
   * PermitsByTypePerPermittee() -- PUBLIC
   *   Returns all Permits of a certain type that have been issued to the
   * Permittee for this calendar year. Excludes Cancelled and Revoked permits.
   *-------------------------------------------------------------------------*/
  procedure PermitsByTypePerPermittee (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_AppToLicenseEndpointId            udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('j_ABC_PermitApplication', 'License');
    t_AppToPermitEndpointId             udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('j_ABC_PermitApplication', 'AppToPermit');
    t_AppToPermitTypeEndpointId         udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('j_ABC_PermitApplication', 'PermitType');
    t_AppToOnlineTAPEndpointId          udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('j_ABC_PermitApplication', 'OnlineTAPPermit');
    t_CalendarEnd                       date;
    t_CalendarStart                     date;
    t_LicenseToLicenseeEndpointId       udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_License', 'Licensee');
    t_PermitFromAppObjectId             udt_Id;
    t_PermitObjectIds                   udt_IdList;
    t_PermitteeIsLicensee               varchar2(1) := 'N';
    t_PermitteeObjectId                 udt_Id;
    t_PermitteeToPermitEndpointId       udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_LegalEntity', 'Permittee_Permit');
    t_PermitToPermitteeEndpointId       udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_Permit', 'Permittee');
    t_PermitToLicenseEndpointId         udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_Permit', 'License');
    t_PermitToTAPEndpointId             udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_Permit', 'TAPPermit');
    t_PermitToTypeEndpointId            udt_Id
        := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_Permit', 'PermitType');
    t_PermitTypeObjectId                udt_Id;
  begin

    t_CalendarStart := trunc(sysdate, 'YEAR');
    t_CalendarEnd := trunc(add_months(sysdate, 12), 'YEAR');
    a_ObjectList := api.udt_ObjectList();

    -- Get Permit Type
    begin
      -- Get the Permit Type from the application using the rels from the Application to the Permit
      -- and from the Permit to the Permit Type.
      select
        rr.ToObjectId,
        api.pkg_ColumnQuery.Value(rr.ToObjectId, 'PermitteeIsLicensee')
      into
        t_PermitTypeObjectId,
        t_PermitteeIsLicensee
      from
        api.relationships r -- from j_ABC_PermitApplication to o_ABC_Permit
        join api.relationships rr -- from o_ABC_Permit to o_ABC_PermitType
            on r.ToObjectId = rr.FromObjectId
      where r.FromObjectId = a_ObjectId
        and r.EndpointId = t_AppToPermitEndpointId
        and rr.EndpointId = t_PermitToTypeEndpointId;

    exception
      when no_data_found then
        -- The job is an unsubmitted Online Permit App, since the Internal wizard requires a Permit
        -- Type on the Permit before the Application can be assigned a JobId
        begin
          select
            r.ToObjectId,
            api.pkg_ColumnQuery.Value(r.ToObjectId, 'PermitteeIsLicensee')
          into
            t_PermitTypeObjectId,
            t_PermitteeIsLicensee
          from api.relationships r
          where r.FromObjectId = a_ObjectId
            and r.EndpointId = t_AppToPermitTypeEndpointId;

        exception
          when no_data_found then
            -- No type set on Online Permit Application
            null;
        end;
    end;

    -- Get Permit from Permit Application
    begin
      select rp.ToObjectId
      into t_PermitFromAppObjectId
      from api.relationships rp
      where rp.FromObjectId = a_ObjectId
        and rp.EndpointId = t_AppToPermitEndpointId;

    exception
      when no_data_found then
        -- Unsubmitted Online Permit Application
        begin
          select rr.ToObjectId
          into t_PermitteeObjectId
          from
            api.relationships r -- from j_ABC_PermitApplication to o_ABC_Permit/o_ABC_License
            join api.relationships rr -- from o_ABC_Permit/o_ABC_License to o_ABC_LegalEntity
                on r.ToObjectId = rr.FromObjectId
          where r.FromObjectId = a_ObjectId
            and r.EndpointId in (t_AppToLicenseEndpointId, t_AppToOnlineTAPEndpointId)
            and rr.EndpointId in (t_LicenseToLicenseeEndpointId, t_PermitToPermitteeEndpointId)
            and t_PermitteeIsLicensee = 'Y';

        exception
          when no_data_found then
            t_PermitteeObjectId := api.pkg_ColumnQuery.Value(a_ObjectId,
                'OnlineUseLegalEntityObjectId');

          when too_many_rows then
            -- License and TAP permit have been selected.
            null;
        end;
    end;

    -- Try getting Permittee from the License/TAP permit
    begin
      select rle.ToObjectId
      into t_PermitteeObjectId
      from
        api.relationships rpl -- from o_ABC_Permit to o_ABC_Permit/o_ABC_License
        join api.relationships rle -- from o_ABC_Permit/o_ABC_License to o_ABC_LegalEntity
            on rpl.ToObjectId = rle.FromObjectId
      where rpl.FromObjectId = t_PermitFromAppObjectId
        and rpl.EndpointId in (t_PermitToLicenseEndpointId, t_PermitToTAPEndpointId)
        and rle.EndpointId in (t_LicenseToLicenseeEndpointId, t_PermitToPermitteeEndpointId)
        and t_PermitteeIsLicensee = 'Y';

    exception
      when no_data_found then
        -- Try relationship from permit to permittee
        begin
          select rle.ToObjectId
          into t_PermitteeObjectId
          from api.relationships rle
          where rle.FromObjectId = t_PermitFromAppObjectId
            and rle.EndpointId = t_PermitToPermitteeEndpointId;

        exception
          when no_data_found then
            -- Permittee is unset.
            null;
        end;

      when too_many_rows then
        -- License and TAP permit have been selected.
        null;
    end;

    -- Get Permits based on Permit Type and LE
    begin
      select r.ToObjectId
      bulk collect into t_PermitObjectIds
      from
        api.relationships r
        join api.relationships rr
            on r.ToObjectId = rr.FromObjectId
      where r.FromObjectId = t_PermitteeObjectId
        and r.EndpointId = t_PermitteeToPermitEndpointId
        and rr.ToObjectId = t_PermitTypeObjectId
        and rr.EndpointId = t_PermitToTypeEndpointId
        and api.pkg_ColumnQuery.Value(r.ToObjectId, 'State') in ('Active', 'Closed', 'Expired')
        and api.pkg_ColumnQuery.DateValue(r.ToObjectId, 'IssueDate') >= t_CalendarStart
        and api.pkg_ColumnQuery.DateValue(r.ToObjectId, 'IssueDate') < t_CalendarEnd;

    exception
      when no_data_found then
        null;
    end;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_PermitObjectIds);

  end PermitsByTypePerPermittee;

  /*---------------------------------------------------------------------------
   * LicensesForPetition() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure LicensesForPetition (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_LELicenseEndPointId               udt_Id;
    t_LicenseWarningTypeId              udt_Id;
    t_PetitionTypeId                    udt_Id;
    t_UserId                            udt_Id;
  begin

    t_PetitionTypeId := api.pkg_columnquery.NumericValue(a_ObjectId, 'PetitionTypeId');

    if t_PetitionTypeId is null then
      a_ObjectList := api.udt_ObjectList();
      return;
    end if;

    t_UserId := api.pkg_securityquery.EffectiveUserId();
    t_LicenseWarningTypeId := api.pkg_columnquery.NumericValue(t_PetitionTypeId, 'LicenseWarningTypeId');

    if t_LicenseWarningTypeId is not null then
      select api.udt_Object(l.ObjectId)
      bulk collect into a_ObjectList
      from 
        query.r_ABC_UserLegalEntity rle
        join query.r_ABC_LicenseLicenseeLE rlel
            on rlel.LegalEntityObjectId = rle.LegalEntityObjectId
        join query.r_WarningLicense rwl
            on rwl.LicenseId = rlel.LicenseObjectId
        join query.r_LicenseWarningType rlwt
            on rlwt.LicenseWarningId = rwl.LicenseWarningId
        join query.o_ABC_License l
            on l.ObjectId = rlel.LicenseObjectId
        join query.r_ABC_LicenseLicenseType rllt
            on rllt.LicenseObjectId = l.ObjectId
        join query.o_ABC_LicenseType lt
            on lt.ObjectId = rllt.LicenseTypeObjectId
      where rle.UserId = t_UserId
        and rlwt.LicenseWarningTypeId = t_LicenseWarningTypeId
        and l.State = 'Active'
        and lt.IssuingAuthority = 'Municipality';
    else
      select api.udt_Object(l.ObjectId)
      bulk collect into a_ObjectList
      from 
        query.r_ABC_UserLegalEntity rle
        join query.r_ABC_LicenseLicenseeLE rlel
            on rlel.LegalEntityObjectId = rle.LegalEntityObjectId
        join query.o_ABC_License l
            on l.ObjectId = rlel.LicenseObjectId
        join query.r_ABC_LicenseLicenseType rllt
            on rllt.LicenseObjectId = l.ObjectId
        join query.o_ABC_LicenseType lt
            on lt.ObjectId = rllt.LicenseTypeObjectId
      where rle.UserId = t_UserId
        and l.State = 'Active'
        and lt.IssuingAuthority = 'Municipality';
    end if;

  end LicensesForPetition;

  /*---------------------------------------------------------------------------
   * PetitionReliefTerms() -- PRIVATE
   *   Returns list of petition term object ids of given petition type on
   * petition jobs related to a given license type
   *-------------------------------------------------------------------------*/
  function PetitionReliefTerms (
    a_LicenseId                         udt_Id,
    a_PetitionTypeId                    udt_Id
  ) return udt_IdList is
    t_AllTerms                          udt_IdList;
    t_TempTerms                         udt_IdList;
  begin

    -- first, get all terms directly related to license, excluding cancelled
    select pt.ObjectId
    bulk collect into t_AllTerms
    from 
      query.r_ABC_LicensePetitionTerm r
      join query.o_ABC_PetitionTerm pt
          on pt.ObjectId = r.PetitionTermObjectId
    where r.LicenseObjectId = a_LicenseId
      and pt.State != 'Cancelled'
      and pt.PetitionTypeObjectId = a_PetitionTypeId;

    -- get all terms on in-progress petition jobs related to license (terms on approved jobs
    -- will already be included from above query and we do not want denied or cancelled jobs)
    for p in (
        select lp.PetitionId
        from 
          query.r_abc_LicensePetition lp
          join query.r_ABC_PetitionPetitionType ppt
              on ppt.PetitionId = lp.PetitionId
          join query.j_ABC_Petition p
              on p.Objectid = ppt.PetitionId
        where lp.LicenseId = a_LicenseId
          and ppt.PetitionTypeId = a_PetitionTypeId
          and p.StatusName in ('NEW', 'REVIEW')
        ) loop
      if api.pkg_ColumnQuery.Value(p.PetitionId, 'EnteredOnline') = 'N'
          or api.pkg_ColumnQuery.Value(p.PetitionId, 'SubmitApplication') = 'Y' then
        select pt.ObjectId
        bulk collect into t_TempTerms
        from 
          query.r_ABC_PetitionPetitionTerm r
          join query.o_ABC_PetitionTerm pt
              on pt.ObjectId = r.PetitionTermObjectId
        where r.PetitionObjectId = p.PetitionId
          and pt.State in ('Pending', 'Draft');
      else
        -- terms on this rel will always be 'Draft' so no need to
        -- filter 
        select PetitionTermObjectId
        bulk collect into t_TempTerms
        from query.r_ABC_PetitionOnlineReqTerm
        where PetitionObjectId = p.PetitionId;
      end if;

      extension.pkg_CollectionUtils.Append(t_AllTerms, t_TempTerms);
    end loop;

    return t_AllTerms;

  end PetitionReliefTerms;

  /*---------------------------------------------------------------------------
   * LicenseReliefTerms() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure LicenseReliefTerms (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_AmendmentTypeId                   udt_Id;
    t_JobDefName                        varchar2(30);
    t_LicenseId                         udt_Id;
    t_PetitionTypeId                    udt_Id;
    t_TempTerms                         udt_IdList;
    t_Terms                             udt_IdList;
  begin

    a_ObjectList := api.udt_ObjectList();
    t_JobDefName := api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectDefName');
    t_LicenseId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'LicenseObjectId');

    if t_LicenseId is null then
      return;
    end if;

    if t_JobDefName = 'j_ABC_Petition' then
      t_PetitionTypeId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'PetitionTypeId');

      if t_PetitionTypeId is null then
        return;
      end if;

      t_Terms := PetitionReliefTerms(t_LicenseId, t_PetitionTypeId);
    elsif t_JobDefName = 'j_ABC_PetitionAmendment' then
      t_AmendmentTypeId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'PetitionAmendmentTypeId');
      
      if t_AmendmentTypeId is null then
        return;
      end if;

      -- get terms of all petition types, even if only a subset of petition types are valid for the amendment
      for pt in (
          select ObjectId
          from query.o_ABC_PetitionType
          where Active = 'Y'
            and ObjectDefTypeId = 1
          ) loop
        t_TempTerms := PetitionReliefTerms(t_LicenseId, pt.ObjectId);
        extension.pkg_CollectionUtils.Append(t_Terms, t_TempTerms);
      end loop;
    end if;

    a_ObjectList := extension.pkg_utils.ConvertToObjectList(t_Terms);

  end LicenseReliefTerms;

  /*---------------------------------------------------------------------------
   * PublicLicensesWithAppTerms() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PublicLicensesWithAppTermsAll (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_LicenseIds                        udt_IdList;
  begin

    select distinct l.ObjectId
    bulk collect into t_LicenseIds
    from 
      query.r_ABC_UserLegalEntity ule
      join query.r_ABC_LicenseLicenseeLE lle
          on lle.LegalEntityObjectId = ule.LegalEntityObjectId
      join query.o_ABC_License l
          on l.ObjectId = lle.LicenseObjectId
      join query.r_ABC_LicensePetitionTerm lpt
          on lpt.LicenseObjectId = l.ObjectId
      join query.o_ABC_PetitionTerm pt
          on pt.ObjectId = lpt.PetitionTermObjectId
    where ule.UserId = a_ObjectId
      and l.State = 'Active'
      and pt.State = 'Granted';

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_LicenseIds);

  end PublicLicensesWithAppTermsAll;

  /*---------------------------------------------------------------------------
   * PublicLicensesWithAppTermsLimit() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PublicLicensesWithAppTermsLimit (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_Licenses                          api.udt_ObjectList;
  begin

    a_ObjectList := api.udt_ObjectList();
    PublicLicensesWithAppTermsAll(a_ObjectId, a_Endpoint, t_Licenses);

    if t_Licenses.count <= g_OnlineGridLimit then
      a_ObjectList := t_Licenses;
    else
      for i in 1..g_OnlineGridLimit loop
        a_ObjectList.Extend(1);
        a_ObjectList(a_ObjectList.Count) := t_Licenses(i);
      end loop;
    end if;

  end PublicLicensesWithAppTermsLimit;

  /*---------------------------------------------------------------------------
   * LicensesForPetitionAmendment() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure LicensesForPetitionAmendment (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_AmendmentTypeId                   udt_Id;
    t_LicenseIds                        udt_IdList;
    t_UserId                            udt_Id;
  begin

    t_AmendmentTypeId := api.pkg_columnquery.Value(a_ObjectId, 'PetitionAmendmentTypeId');

    if t_AmendmentTypeId is null then
      a_ObjectList := api.udt_ObjectList();
      return;
    end if;

    t_UserId := api.pkg_securityquery.EffectiveUserId();

    if api.pkg_columnquery.Value(t_AmendmentTypeId, 'AllOrSomePetitionTypes') = 'Some' then
      select distinct l.ObjectId
      bulk collect into t_LicenseIds
      from 
        query.r_ABC_UserLegalEntity rle
        join query.r_ABC_LicenseLicenseeLE rlel
            on rlel.LegalEntityObjectId = rle.LegalEntityObjectId
        join query.r_ABC_LicensePetitionTerm lpt
            on lpt.LicenseObjectId = rlel.LicenseObjectId
        join query.r_ABC_PetitionTermPetitionType ptpt
            on ptpt.PetitionTermObjectId = lpt.PetitionTermObjectId
        join query.r_ABC_PetitionTypePetAmendType ptat
            on ptat.PetitionTypeId = ptpt.PetitionTypeObjectId
        join query.o_ABC_PetitionTerm pt
            on pt.ObjectId = lpt.PetitionTermObjectId
        join query.o_ABC_License l
            on l.ObjectId = lpt.LicenseObjectId
        join query.r_ABC_LicenseLicenseType rllt
            on rllt.LicenseObjectId = l.ObjectId
        join query.o_ABC_LicenseType lt
            on lt.ObjectId = rllt.LicenseTypeObjectId
      where ptat.PetitionAmendmentTypeId = t_AmendmentTypeId
        and rle.UserId = t_UserId
        and pt.State = 'Granted'
        and l.State = 'Active'
        and lt.IssuingAuthority = 'Municipality';
    else
      select distinct l.ObjectId
      bulk collect into t_LicenseIds
      from 
        query.r_ABC_UserLegalEntity rle
        join query.r_ABC_LicenseLicenseeLE rlel
            on rlel.LegalEntityObjectId = rle.LegalEntityObjectId
        join query.r_ABC_LicensePetitionTerm lpt
            on lpt.LicenseObjectId = rlel.LicenseObjectId
        join query.o_ABC_PetitionTerm pt
            on pt.ObjectId = lpt.PetitionTermObjectId
        join query.o_ABC_License l
            on l.ObjectId = lpt.LicenseObjectId
        join query.r_ABC_LicenseLicenseType rllt
            on rllt.LicenseObjectId = l.ObjectId
        join query.o_ABC_LicenseType lt
            on lt.ObjectId = rllt.LicenseTypeObjectId
      where rle.UserId = t_UserId
        and pt.State = 'Granted'
        and l.State = 'Active'
        and lt.IssuingAuthority = 'Municipality';
    end if;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_LicenseIds);

  end LicensesForPetitionAmendment;

  /*---------------------------------------------------------------------------
   * LicenseUnapprovedTerms() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure LicenseUnapprovedTerms (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_PetitionTerms                     udt_IdList;
    t_TempTerms                         udt_IdList;
  begin

    -- Draft, pending, and rejected terms on petition applications
    for x in (
        select j.ObjectId, j.EnteredOnline, j.SubmitApplication
        from 
          query.r_abc_LicensePetition r
          join query.j_ABC_Petition j
              on j.ObjectId = r.PetitionId
        where r.LicenseId = a_ObjectId
          and j.StatusName in ('DENY', 'REVIEW', 'NEW')
        ) loop
      if x.EnteredOnline = 'N' or x.SubmitApplication = 'Y' then
        select r.PetitionTermObjectId
        bulk collect into t_TempTerms
        from query.r_ABC_PetitionPetitionTerm r
        where r.PetitionObjectId = x.ObjectId;
      else
        select PetitionTermObjectId
        bulk collect into t_TempTerms
        from query.r_ABC_PetitionOnlineReqTerm
        where PetitionObjectId = x.ObjectId;
      end if;
      extension.pkg_CollectionUtils.Append(t_PetitionTerms, t_TempTerms);
    end loop;


    -- Draft, pending, and rejected terms on petition amendments
    for x in (
        select j.ObjectId, j.EnteredOnline, j.SubmitApplication
        from 
          query.r_ABC_LicensePetitionAmendment r
          join query.j_ABC_PetitionAmendment j
              on j.ObjectId = r.PetitionAmendmentId
        where r.LicenseId = a_ObjectId
          and j.StatusName in ('DENY', 'REVIEW', 'NEW')
        ) loop
      if x.EnteredOnline = 'N' or x.SubmitApplication = 'Y' then
        select pt.ObjectId
        bulk collect into t_TempTerms
        from 
          query.r_ABC_PetitionAmendTerms r
          join query.o_ABC_PetitionTerm pt
              on pt.ObjectId = r.PetitionTermObjectId
        where r.PetitionAmendmentId = x.ObjectId
          and pt.IsChanged = 'Y'
          and pt.State != 'Cancelled';
      else
        select pt.ObjectId
        bulk collect into t_TempTerms
        from 
          query.r_ABC_PetitionAmendOnlineTerms r
          join query.o_ABC_PetitionTerm pt
              on pt.ObjectId = r.PetitionTermObjectId
        where r.PetitionAmendmentId = x.ObjectId
          and pt.IsChanged = 'Y'
          and pt.State != 'Cancelled';
      end if;
      extension.pkg_CollectionUtils.Append(t_PetitionTerms, t_TempTerms);
    end loop;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_PetitionTerms);

  end LicenseUnapprovedTerms;

  /*---------------------------------------------------------------------------
   * PublicPetitionAmendment() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PublicPetitionAmendment (
    a_ObjectId                          udt_Id,
    a_EndPoint                          udt_Id,
    a_ObjectList                    out api.udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
  begin

    select distinct jr.PetitionAmendmentId
    bulk collect into t_ObjectIdList
    from 
      query.r_ABC_UserLegalEntity r
      join query.r_ABC_Licensee_PetitionAmend jr 
          on jr.LegalEntityObjectId = r.LegalEntityObjectId
    where r.UserId = a_ObjectId;

    a_ObjectList := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end PublicPetitionAmendment;

end pkg_ABC_ProceduralRels;
/
