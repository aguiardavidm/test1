create or replace package     pkg_ABC_Petition is

  subtype udt_id is api.pkg_definition.udt_id;
  subtype udt_idlist is api.pkg_definition.udt_idlist;     

  /*---------------------------------------------------------------------------
   * PostVerify
   *   This is the post verify process for the Petition job.
   *-------------------------------------------------------------------------*/ 
  procedure PostVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );
  
  /*---------------------------------------------------------------------------
   * ManagePetitionVerify
   *   Post-verify activities for the petition job
   *-------------------------------------------------------------------------*/
  procedure ManagePetitionVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * PetitionConstructor
   *   Constructor activities for the petition job
   *-------------------------------------------------------------------------*/
  procedure PetitionConstructor (
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );
  
  /*---------------------------------------------------------------------------
   * PetitionJobs
   *   Find related Petition Jobs that are Approved or In Review for any 
   *   versions of the License
   *-------------------------------------------------------------------------*/
  function PetitionJobs(
    a_LicenseObjectId                   udt_id,
    a_StatusName                        varchar2
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * ApprovalActivities
   *   Activities to complete when a job is approved
   *-------------------------------------------------------------------------*/
  procedure ApprovalActivities (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_JobId                             udt_Id
  );

  /*---------------------------------------------------------------------------
   * CancelPetitionTerms
   *   Change the state on any related petition terms to Cancelled when job
   * is cancelled.
   *-------------------------------------------------------------------------*/
  procedure CancelPetitionTerms (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * DenialActivities
   *   Activities to complete when a job is denied
   *-------------------------------------------------------------------------*/
  procedure DenialActivities (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_JobId                             udt_Id
  );

  /*---------------------------------------------------------------------------
   * GenerateTermsForAmendment
   *   Based on selected the license and petition amendment type, 
   * create petition terms for amendment. Runs on constructor of license and
   * petition amendment type rels
   *-------------------------------------------------------------------------*/
  procedure GenerateTermsForAmendment (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * PaymentSucceeded
   *   Activites for when payment succeeds
   *-------------------------------------------------------------------------*/
  procedure PaymentSucceeded (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * RegenerateFeeOnRelChange
   *   Sets the Regenerate Fee detail on rel change
   *-------------------------------------------------------------------------*/
  procedure RegenerateFeeOnRelChange (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SetDefaultAssignments
   *   Sets the default assignments as configured in admin settings
   *-------------------------------------------------------------------------*/
  procedure SetDefaultAssignments (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SubmissionActivities
   *   Activities to run when a petition is submitted
   *-------------------------------------------------------------------------*/
  procedure SubmissionActivities (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_Petition;

/

create or replace package body     pkg_ABC_Petition is

  /*---------------------------------------------------------------------------
   * ClonePetitionTerm -- PRIVATE
   *   Clone petition term and return object id of new petition term
   *-------------------------------------------------------------------------*/
  function ClonePetitionTerm (
    a_SourceTermObj                     udt_Id
  ) return udt_Id is
    t_PetitionTermDefId                 udt_Id;
    t_PetitionTermId                    udt_Id;
    t_PetitionTypeEndPointId            udt_Id;
    t_RelationshipId                    udt_Id;
  begin

    t_PetitionTermDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_PetitionTerm');
    t_PetitionTermId := api.pkg_objectupdate.New(t_PetitionTermDefId);
    t_PetitionTypeEndPointId := api.pkg_configquery.EndPointIdForName(t_PetitionTermDefId, 'PetitionType');

    t_RelationshipId := api.pkg_relationshipupdate.New(t_PetitionTypeEndPointId, t_PetitionTermId,
        api.pkg_columnquery.value(a_SourceTermObj, 'PetitionTypeObjectId'));
    api.pkg_columnupdate.SetValue(t_PetitionTermId, 'TermStartYear',
        api.pkg_columnquery.value(a_SourceTermObj, 'TermStartYear'));
    api.pkg_columnupdate.SetValue(t_PetitionTermId, 'State',
        api.pkg_columnquery.value(a_SourceTermObj, 'State'));

    return t_PetitionTermId;

  end ClonePetitionTerm;

  /*---------------------------------------------------------------------------
   * CopyPetitionAmendTypeQuestions() -- PRIVATE
   *   For each  question on the Petition Amendment Type admin object,
   * create a Response object to display on the amendment job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPetitionAmendTypeQuestions (
    a_JobId                             udt_Id,
    a_PetitionAmendTypeId               udt_Id
  ) is
    t_DocTypeId                         udt_Id;
    t_ParentResponseEndPointId          udt_Id;
    t_RelationshipIds                   udt_IdList;
    t_RelId                             udt_Id;
    t_ResponseDefId                     udt_Id;
    t_ResponseId                        udt_Id;
  begin

    t_ParentResponseEndPointId := api.pkg_configquery.EndpointIdForName(
        'o_ABC_QAResponse', 'ParentResponse');
    t_ResponseDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');

    --Grab the rel id of any questions on the job
    select r.RelationshipId
    bulk collect into t_RelationshipIds
    from query.r_ABC_PetitionAmendQAResponse r
    where r.PetitionAmendmentId = a_JobId;

    -- If no questions are currently on the job then generate them from the list
    -- on the admin site
    if t_RelationshipIds.count = 0 then
      for i in (select r.QuestionId
                from query.r_ABC_PetitionAmndTypeQuestion r
                where r.PetitionAmendTypeObjectId = a_PetitionAmendTypeId
               ) loop
        -- Create Response
        t_ResponseId := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (select
                    api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                    name columnname
                  from api.columndefs cd
                  where cd.objectdefid = t_ResponseDefId
                    and name in ('SortOrder', 'Question','AcceptableAnswer',
                        'NonacceptableText','NonacceptableResponseType')
                 ) loop
          api.pkg_columnupdate.SetValue(t_ResponseId, k.columnname, k.columnvalue);
        end loop;

        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
            'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
          t_RelId := api.pkg_RelationshipUpdate.New(
              api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
              t_ResponseId, t_DocTypeId);
        end if;
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
            t_ResponseId, i.questionid);
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'PetitionAmendment'),
            t_ResponseId, a_JobId);
      end loop;
      
      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_PetitionAmendQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_PetitionAmendQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.PetitionAmendmentId = a_JobId
               and pr.PetitionAmendmentId = a_JobId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(t_ParentResponseEndPointId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;

    -- Copy any documents for specific Petition Type to the Application job
    select r.RelationshipId
    bulk collect into t_RelationshipIds
    from query.r_ABC_PetAmendOnlineDocType r
    where r.PetitionAmendmentId = a_JobId;

    -- If no documents are currently on the job then generate them from the list
    -- on the admin site
    if t_RelationshipIds.count = 0 then
      for d in (select
                  r.DocumentTypeObjectId,
                  r.Mandatory
                from query.r_PetitionAmendTypeDocType r
                where r.PetitionAmendTypeObjectId = a_PetitionAmendTypeId
                  and r.DocumentTypeObjectId not in
                      (select r1.DocTypeId
                       from query.r_ABC_PetitionAmendRespDocType r1
                       where r1.PetitionAmendmentId = a_JobId)
               ) loop
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('j_ABC_PetitionAmendment',
            'PetitionDocumentType'), a_JobId, d.documenttypeobjectid);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      end loop;
    end if;

  end CopyPetitionAmendTypeQuestions;

  /*---------------------------------------------------------------------------
   * CopyPetitionTerms -- PRIVATE
   *   Copy petition terms to license
   *-------------------------------------------------------------------------*/
  procedure CopyPetitionTerms (
    a_JobId                             udt_Id,
    a_LicenseId                         udt_Id
  ) is
    t_EndPointId                        udt_Id;
    t_RelationshipId                    udt_Id;
  begin

    t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'PetitionTerm');

    for pt in (
        select PetitionTermObjectId
        from query.r_ABC_PetitionPetitionTerm
        where PetitionObjectId = a_JobId
        ) loop
      api.pkg_columnupdate.SetValue(pt.PetitionTermObjectId, 'State', 'Granted');
      t_RelationshipId :=
          api.pkg_relationshipupdate.New(t_EndPointId, a_LicenseId, pt.PetitionTermObjectId);
    end loop;

  end CopyPetitionTerms;

  /*---------------------------------------------------------------------------
   * CopyPetitionTypeQuestions() -- PRIVATE
   *   For each petition question on the Petition Type admin object,
   * create a Response object to display on the petition job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPetitionTypeQuestions (
    a_JobId                             udt_Id,
    a_PetitionTypeId                    udt_Id
  ) is
    t_DocTypeId                         udt_Id;
    t_ParentResponseEndPointId          udt_Id;
    t_RelationshipIds                   udt_IdList;
    t_RelId                             udt_Id;
    t_ResponseDefId                     udt_Id;
    t_ResponseId                        udt_Id;
  begin

    t_ParentResponseEndPointId := api.pkg_configquery.EndpointIdForName(
        'o_ABC_QAResponse', 'ParentResponse');
    t_ResponseDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');

    --Grab the rel id of any questions on the job
    select r.RelationshipId
    bulk collect into t_RelationshipIds
    from query.r_ABC_PetitionAppQAResponse r
    where r.PetitionObjectId = a_JobId;

    -- If no questions are currently on the job then generate them from the list
    -- on the admin site
    if t_RelationshipIds.count = 0 then
      for i in (select r.QuestionId
                from query.r_ABC_PetitionTypeQuestion r
                where r.PetitionTypeObjectId = a_PetitionTypeId
               ) loop
        -- Create Response
        t_ResponseId := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (select
                    api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                    name columnname
                  from api.columndefs cd
                  where cd.objectdefid = t_ResponseDefId
                    and name in ('SortOrder', 'Question','AcceptableAnswer',
                        'NonacceptableText','NonacceptableResponseType')
                 ) loop
          api.pkg_columnupdate.SetValue(t_ResponseId, k.columnname, k.columnvalue);
        end loop;

        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
            'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
          t_RelId := api.pkg_RelationshipUpdate.New(
              api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
              t_ResponseId, t_DocTypeId);
        end if;
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
            t_ResponseId, i.questionid);
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Petition'),
            t_ResponseId, a_JobId);
      end loop;
      
      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_PetitionAppQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_PetitionAppQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.PetitionObjectId = a_JobId
               and pr.PetitionObjectId = a_JobId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(t_ParentResponseEndPointId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;

    -- Copy any documents for specific Petition Type to the Application job
    select r.RelationshipId
    bulk collect into t_RelationshipIds
    from query.r_ABC_PetitionAppDocumentType r
    where r.PetitionId = a_JobId;

    -- If no documents are currently on the job then generate them from the list
    -- on the admin site
    if t_RelationshipIds.count = 0 then
      for d in (select
                  r.DocumentTypeObjectId,
                  r.Mandatory
                from query.r_PetitionTypeOnlineDocType r
                where r.PetitionTypeObjectId = a_PetitionTypeId
                  and r.DocumentTypeObjectId not in
                      (select r1.DocTypeId
                       from query.r_ABC_PetitionResponseDocType r1
                       where r1.PetitionId = a_JobId)
               ) loop
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('j_ABC_Petition',
            'PetitionDocumentType'), a_JobId, d.documenttypeobjectid);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      end loop;
    end if;

  end CopyPetitionTypeQuestions;

/*---------------------------------------------------------------------------
   * CopyRequestedTerms -- PRIVATE
   *   Copy requested petition terms to the appropriate rel
   *-------------------------------------------------------------------------*/
  procedure CopyRequestedTerms (
    a_JobId                             udt_Id
  ) is
    t_FromEndPointId                    udt_Id;
    t_ObjectDefId                       udt_Id;
    t_PetitionTerm                      udt_Id;
    t_RelationshipId                    udt_Id;
    t_ToEndPointId                      udt_Id;
  begin

    t_ObjectDefId := api.pkg_columnquery.NumericValue(a_JobId, 'ObjectDefId');
    t_FromEndPointId := api.pkg_configquery.EndPointIdForName(t_ObjectDefId, 'OnlineRequestedTerm');
    t_ToEndPointId := api.pkg_configquery.EndPointIdForName(t_ObjectDefId, 'PetitionTerm');

    for t in (
        select ToObjectId TermId
        from api.relationships
        where FromObjectid = a_JobId
          and EndPointId = t_FromEndPointId
        ) loop
      t_PetitionTerm := ClonePetitionTerm(t.TermId);
      if api.pkg_columnquery.Value(t_PetitionTerm, 'State') = 'Draft' then
        api.pkg_columnupdate.SetValue(t_PetitionTerm, 'State', 'Pending');
      end if;
      t_RelationshipId := api.pkg_relationshipupdate.New(t_ToEndPointId, a_JobId, t_PetitionTerm);
    end loop;

  end CopyRequestedTerms;

  /*---------------------------------------------------------------------------
   * CreateResponseDocuments() -- PRIVATE
   *   Create Document relationship based on questions that require a document
   * response
   *-------------------------------------------------------------------------*/
  procedure CreateResponseDocuments (
    a_JobId                             udt_Id
  ) is
    t_DocTypeEndPointId                 udt_Id;
    t_QARel                             udt_Id;
    t_QuestionDocCount                  number := 0;
  begin

    t_DocTypeEndPointId := api.pkg_configquery.EndPointIdForName(
        'j_ABC_Petition', 'DocumentType');

    for e in (
        select r.RelationshipId, r.DocTypeId
        from query.r_ABC_PetitionResponseDocType r
        where r.PetitionId = a_JobId
        ) loop
      api.pkg_relationshipupdate.Remove(e.relationshipid);
    end loop;
    
    for d in (
        select r.RelationshipId, r.Mandatory
        from query.r_ABC_PetitionAppDocumentType r
        where r.PetitionId = a_JobId
        ) loop
      api.pkg_columnupdate.SetValue(d.RelationshipId, 'QuestionMandatory', d.Mandatory);
    end loop;
    
    for c in (
        select distinct api.pkg_columnQuery.value(qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
        from query.r_ABC_PetitionAppQAResponse qa
        where qa.PetitionObjectId = a_JobId
          and (api.pkg_columnquery.value(qa.ResponseId, 'AcceptableAnswer') <>
                  api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob')
                  or api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
          and api.pkg_columnquery.value(qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
          and api.pkg_columnquery.value(qa.ResponseId, 'ShowQuestionSQL') = 'Y'
        ) loop
      select count(*)
      into t_QuestionDocCount
      from api.relationships r
      where r.FromObjectId = a_JobId
        and r.ToObjectId = c.DocumentTypeId;

      if t_QuestionDocCount = 0 then
        t_QaRel := api.pkg_relationshipupdate.New(t_DocTypeEndPointId, a_JobId,
            c.DocumentTypeId);
        api.pkg_columnupdate.SetValue(t_QARel, 'Mandatory','Y');
      elsif t_QuestionDocCount > 0 then
        for j in (
            select RelationshipId
            from api.relationships
            where FromObjectId = a_JobId
              and ToObjectId = c.documenttypeid
            ) loop
          api.pkg_columnupdate.SetValue(j.RelationshipId, 'QuestionMandatory', 'Y');
        end loop;
      end if;
    end loop;

  end CreateResponseDocuments;

  /*---------------------------------------------------------------------------
   * CreateResponseDocumentsAmend() -- PRIVATE
   *   Create Document relationship based on questions that require a document
   * response for petition amendmends
   *-------------------------------------------------------------------------*/
  procedure CreateResponseDocumentsAmend (
    a_JobId                             udt_Id
  ) is
    t_DocTypeEndPointId                 udt_Id;
    t_QARel                             udt_Id;
    t_QuestionDocCount                  number := 0;
  begin

    t_DocTypeEndPointId := api.pkg_configquery.EndPointIdForName(
        'j_ABC_PetitionAmendment', 'DocumentType');

    for e in (
        select r.RelationshipId, r.DocTypeId
        from query.r_ABC_PetitionAmendRespDocType r
        where r.PetitionAmendmentId = a_JobId
        ) loop
      api.pkg_relationshipupdate.Remove(e.relationshipid);
    end loop;
    
    for d in (
        select r.RelationshipId, r.Mandatory
        from query.r_ABC_PetAmendOnlineDocType r
        where r.PetitionAmendmentId = a_JobId
        ) loop
      api.pkg_columnupdate.SetValue(d.RelationshipId, 'QuestionMandatory', d.Mandatory);
    end loop;
    
    for c in (
        select distinct api.pkg_columnQuery.value(qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
        from query.r_ABC_PetitionAmendQAResponse qa
        where qa.PetitionAmendmentId = a_JobId
          and (api.pkg_columnquery.value(qa.ResponseId, 'AcceptableAnswer') <>
                  api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob')
                  or api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
          and api.pkg_columnquery.value(qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
          and api.pkg_columnquery.value(qa.ResponseId, 'ShowQuestionSQL') = 'Y'
        ) loop
      select count(*)
      into t_QuestionDocCount
      from api.relationships r
      where r.FromObjectId = a_JobId
        and r.ToObjectId = c.DocumentTypeId;

      if t_QuestionDocCount = 0 then
        t_QaRel := api.pkg_relationshipupdate.New(t_DocTypeEndPointId, a_JobId,
            c.DocumentTypeId);
        api.pkg_columnupdate.SetValue(t_QARel, 'Mandatory','Y');
      elsif t_QuestionDocCount > 0 then
        for j in (
            select RelationshipId
            from api.relationships
            where FromObjectId = a_JobId
              and ToObjectId = c.documenttypeid
            ) loop
          api.pkg_columnupdate.SetValue(j.RelationshipId, 'QuestionMandatory', 'Y');
        end loop;
      end if;
    end loop;

  end CreateResponseDocumentsAmend;

  /*---------------------------------------------------------------------------
   * SetDefaultUser -- PRIVATE
   *   Sets the default user based on petition type
   *-------------------------------------------------------------------------*/
  procedure SetDefaultUser (
    a_JobId                           udt_Id,
    a_JobDefId                        udt_Id,
    a_JobUserEndPointName             varchar2,
    a_PetitionTypeId                  udt_Id,
    a_PetitionTypeUserColName         varchar2,
    a_Replace                         boolean default false
  ) is
    t_EndPointId                      udt_Id;
    t_RelationshipId                  udt_Id;
    t_UserId                          udt_Id;
  begin

    t_UserId := api.pkg_ColumnQuery.Value(a_PetitionTypeId, a_PetitionTypeUserColName);

    if t_UserId is not null then
      t_EndPointId := api.pkg_configquery.EndPointIdForName(a_JobDefId, a_JobUserEndPointName);

      begin
        select RelationshipId
        into t_RelationshipId
        from api.Relationships
        where FromObjectid = a_JobId
          and EndPointId = t_EndPointId;
      exception
        when no_data_found then
          null;
      end;

      if t_RelationshipId is not null then
        if a_Replace then
          api.pkg_relationshipupdate.Remove(t_RelationshipId);
        else
          return;
        end if;
      end if;

      t_RelationshipId := api.pkg_relationshipupdate.New(t_EndPointId, a_JobId, t_UserId);
    end if;

  end SetDefaultUser;

  /*---------------------------------------------------------------------------
   * SetDocketNumber -- PRIVATE
   *   Sets the docket number
   *-------------------------------------------------------------------------*/
  procedure SetDocketNumber (
    a_ObjectId                          udt_Id
  ) is
    t_Prefix                            varchar2(5);
    t_SequenceId                        udt_Id;
    t_SequenceValue                     api.pkg_definition.udt_SequenceValue;
  begin

    select SequenceId
    into t_SequenceId
    from api.Sequences
    where Description = 'Petition Docket Number';

    t_Prefix := to_char(api.pkg_columnquery.DateValue(a_ObjectId, 'CreatedDate'), 'mm-yy');

    if api.pkg_columnquery.IsNull(a_ObjectId, 'DocketNumber') then
      api.pkg_columnupdate.SetValue(a_ObjectId, 'DocketNumber',
          api.pkg_Utils.GenerateExternalFileNum(t_Prefix || '-[9999999]', t_SequenceId));
    end if;

  end SetDocketNumber;

  /*---------------------------------------------------------------------------
   * SetOriginalLicense -- PRIVATE
   *   Sets the original license rel to the license at submission time
   *-------------------------------------------------------------------------*/
  procedure SetOriginalLicense (
    a_ObjectId                          udt_Id
  ) is
    t_Count                             pls_integer := 0;
    t_EndPointId                        udt_Id;
    t_ObjectDefId                       udt_Id;
    t_RelationshipId                    udt_Id;
  begin

    if api.pkg_columnquery.value(a_ObjectId, 'EnteredOnline') = 'N' then
      return;
    end if;

    t_ObjectDefId := api.pkg_columnquery.NumericValue(a_ObjectId, 'ObjectDefId');
    t_EndPointId := api.pkg_configquery.EndPointIdForName(t_ObjectDefId, 'OriginalLicense');
    
    select count(RelationshipId)
    into t_Count
    from api.relationships
    where EndPointId = t_EndPointId
      and FromObjectId = a_ObjectId;

    if t_Count = 0 then
      t_RelationshipId := api.pkg_relationshipupdate.New(t_EndPointId, a_ObjectId,
          api.pkg_columnquery.NumericValue(a_ObjectId, 'LicenseObjectId'));
    end if;

  end SetOriginalLicense;

  /*---------------------------------------------------------------------------
   * UpdateJobTermStates -- PRIVATE
   *   Sets all terms on a given job to the passed in state
   *-------------------------------------------------------------------------*/
  procedure UpdateJobTermStates (
    a_JobId                           udt_Id,
    a_State                           varchar2,
    a_UpdateOnlineTerms               boolean default false
  ) is
    t_JobDefName                      varchar2(30);
    t_OnlineTerms                     udt_IdList;
    t_Terms                           udt_IdList;
  begin

    t_JobDefName := api.pkg_columnquery.Value(a_JobId, 'ObjectDefName');

    if t_JobDefName = 'j_ABC_Petition' then
      select PetitionTermObjectId
      bulk collect into t_Terms
      from query.r_ABC_PetitionPetitionTerm
      where PetitionObjectId = a_JobId;
      if a_UpdateOnlineTerms then
        select PetitionTermObjectId
        bulk collect into t_OnlineTerms
        from query.r_ABC_PetitionOnlineReqTerm
        where PetitionObjectId = a_JobId;
      end if;
    elsif t_JobDefName = 'j_ABC_PetitionAmendment' then
      select PetitionTermObjectId
      bulk collect into t_Terms
      from query.r_ABC_PetitionAmendTerms
      where PetitionAmendmentId = a_JobId;
      if a_UpdateOnlineTerms then
        select PetitionTermObjectId
        bulk collect into t_OnlineTerms
        from query.r_ABC_PetitionAmendOnlineTerms
        where PetitionAmendmentId = a_JobId;
      end if;
    end if;
    
    for i in 1..t_Terms.count loop
      api.pkg_columnupdate.SetValue(t_Terms(i), 'State', a_State);
    end loop;

    for i in 1..t_OnlineTerms.count loop
      api.pkg_columnupdate.SetValue(t_OnlineTerms(i), 'State', a_State);
    end loop;

  end UpdateJobTermStates;

  /*---------------------------------------------------------------------------
   * UpdateLicenseWarnings -- PRIVATE
   *   Updates the license warnings related to the license
   *-------------------------------------------------------------------------*/
  procedure UpdateLicenseWarnings (
    a_LicenseId                         udt_Id
  ) is
    t_LicenseWarningTypeId              udt_Id;
    t_NewStartDate                      date;
  begin

    -- find latest granted term on license and update warning start date
    for x in (
        select 
          pt.PetitionTypeObjectId, 
          to_char(max(coalesce(extract(year from pt.EndDate), pt.TermStartYear + 1))) LastReliefTermYear
        from 
          query.r_ABC_LicensePetitionTerm lpt
          join query.o_ABC_PetitionTerm pt
              on pt.ObjectId = lpt.PetitionTermObjectId
          where lpt.LicenseObjectId = a_LicenseId
            and pt.State = 'Granted'
          group by pt.PetitionTypeObjectId, pt.PetitionType
        ) loop
      t_LicenseWarningTypeId := api.pkg_columnquery.NumericValue(x.PetitionTypeObjectId, 'LicenseWarningTypeId');

      if t_LicenseWarningTypeId is null then
        continue;
      end if;

      for w in (
          select lw.ObjectId, lw.StartDate 
          from 
            query.r_WarningLicense rwl 
            join query.r_LicenseWarningType rlwt
                on rlwt.LicenseWarningId = rwl.LicenseWarningId
            join query.o_ABC_LicenseWarning lw
                on lw.ObjectId = rwl.LicenseWarningId
          where rwl.LicenseId = a_LicenseId
            and rlwt.LicenseWarningTypeId = t_LicenseWarningTypeId
          ) loop
        t_NewStartDate := to_date(to_char(w.StartDate, 'dd-mm-') || x.LastReliefTermYear, 'dd-mm-yyyy');
        api.pkg_columnupdate.SetValue(w.ObjectId, 'StartDate', t_NewStartDate);
      end loop;
    end loop;

  end UpdateLicenseWarnings;

  /*---------------------------------------------------------------------------
   * UpdatePetitionTerms -- PRIVATE
   *   Replaces petition terms on license if they have been amended and adds
   * any new terms that were added as part of the petition amendment
   *-------------------------------------------------------------------------*/
  procedure UpdatePetitionTerms (
    a_JobId                           udt_Id,
    a_LicenseId                       udt_Id
  ) is
    t_EndPointId                      udt_Id;
    t_OriginalTermEndPointId          udt_Id;
    t_OriginalTermId                  udt_Id;
    t_RelationshipId                  udt_Id;
  begin

    t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'PetitionTerm');
    t_OriginalTermEndPointId := api.pkg_configquery.EndPointIdForName(
        'o_ABC_PetitionTerm', 'OriginalPetitionTerm');

    for pt in (
        select pt.ObjectId 
        from 
          query.r_ABC_PetitionAmendTerms r
          join query.o_ABC_PetitionTerm pt
              on pt.ObjectId = r.PetitionTermObjectId
        where r.PetitionAmendmentId = a_JobId
          and pt.IsChanged = 'Y'
        ) loop
      if api.pkg_columnquery.Value(pt.ObjectId, 'State') in ('Draft', 'Pending') then
        api.pkg_columnupdate.SetValue(pt.ObjectId, 'State', 'Granted');
      end if;

      -- check if this term is associated with an existing term on the license
      -- if so, then remove the rel to the old term
      begin
        select ToObjectId
        into t_OriginalTermId
        from api.relationships
        where FromObjectId = pt.ObjectId
          and EndPointId = t_OriginalTermEndPointId;
      exception
        when no_data_found then
          -- no original term found, i.e. new term added as part of amendment
          null;
      end;

      if t_OriginalTermId is not null then
        begin
          select RelationshipId
          into t_RelationshipId
          from query.r_ABC_LicensePetitionTerm
          where LicenseObjectId = a_LicenseId
            and PetitionTermObjectId = t_OriginalTermId;
        exception
          when no_data_found then
            -- may have been removed by another amendment
            null;
        end;
        api.pkg_RelationshipUpdate.Remove(t_RelationshipId);
      end if;

      if api.pkg_columnquery.Value(pt.ObjectId, 'State') != 'Cancelled' then
        t_RelationshipId := api.pkg_relationshipupdate.New(
            t_EndPointId, a_LicenseId, pt.ObjectId);
      end if;
    end loop;

  end UpdatePetitionTerms;
  
  /*---------------------------------------------------------------------------
   * PostVerify -- PUBLIC
   *-------------------------------------------------------------------------*/ 
  procedure PostVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is 
    t_AmendmentTypeId                   udt_Id;
    t_JobDefId                          udt_Id;
    t_PetitionTypeId                    udt_Id;
  begin

    t_JobDefId := api.pkg_columnquery.NumericValue(a_ObjectId, 'ObjectDefId');
    t_PetitionTypeId := api.pkg_columnquery.NumericValue(a_ObjectId, 'PetitionTypeId');
    t_AmendmentTypeId := api.pkg_columnquery.NumericValue(a_ObjectId, 'PetitionAmendmentTypeId');

    -- Set default users for assignment based on petition type if none are set
    SetDefaultUser(a_ObjectId, t_JobDefId, 'DefaultProcessor', t_PetitionTypeId, 'DefaultClerkId');
    SetDefaultUser(a_ObjectId, t_JobDefId, 'AttorneyGeneral', t_PetitionTypeId, 'DefaultDeputyAttorneyGeneralId');
    SetDefaultUser(a_ObjectId, t_JobDefId, 'DefaultSupervisor', t_PetitionTypeId, 'DefaultSupervisorId');

    if api.pkg_columnquery.Value(a_ObjectId, 'GenerateQuestions') = 'Y' then
      CopyPetitionTypeQuestions(a_ObjectId, t_PetitionTypeId);
      CopyPetitionAmendTypeQuestions(a_ObjectId, t_AmendmentTypeId);
      api.pkg_columnupdate.SetValue(a_ObjectId, 'GenerateQuestions', 'N');
    end if;

    if api.pkg_columnquery.Value(a_ObjectId, 'AddDocuments') = 'Y' then
      CreateResponseDocuments(a_ObjectId);
      CreateResponseDocumentsAmend(a_ObjectId);
      api.pkg_columnupdate.SetValue(a_ObjectId, 'AddDocuments', 'N');
    end if;

  end PostVerify;
  
  /*---------------------------------------------------------------------------
   * ManagePetitionVerify -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ManagePetitionVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is 
  begin

    if extension.pkg_relutils.RelExists(
        api.pkg_columnquery.Value(a_objectId, 'JobId'), 'PetitionTerm') = 'N' then
      api.pkg_errors.RaiseError(-20000, 'There must be at least 1 Petition term added.');
    end if;

  end ManagePetitionVerify;
  
  /*---------------------------------------------------------------------------
   * PetitionConstructor -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure PetitionConstructor (
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
  begin
     
    null;

  end PetitionConstructor;
  
---------------------------------------------------------------------------------
--PetitionJobs
--Find related Petition Jobs that are Approved or In Review for any versions of the License
---------------------------------------------------------------------------------
    
  function PetitionJobs(
    a_LicenseObjectId                   udt_id,
    a_StatusName                        varchar2
  ) return varchar2 is
    t_Output                            varchar2(4000);

  begin
    
    if a_StatusName = 'APP' then
       for p in (select nvl2(api.pkg_columnquery.DateValue(r.PetitionTermObjectId,'StartDate'),to_char(api.pkg_columnquery.DateValue(r.PetitionTermObjectId,'StartDate'),'yyyy') || '-' || to_char(api.pkg_columnquery.DateValue(r.PetitionTermObjectId,'EndDate'),'yyyy'),p.Petition1239StartYear || '-' || p.Petition1239EndYear) PetitionYears
                   from query.r_abc_masterlicenselicense ml --go to our master license
                   join query.r_abc_masterlicenselicense cl on cl.MasterLicenseObjectId = ml.MasterLicenseObjectId --get all of the child licenses, including ourself
                   join query.r_abc_LicensePetition j on j.LicenseId = cl.LicenseObjectId --get petitions on each child license
                   join query.r_ABC_PetitionPetitionTerm r on r.PetitionObjectId = j.PetitionId
                   join query.j_abc_petition p on p.jobid = r.PetitionObjectId
                   left outer join dataconv.j_abc_petition d on d.objectid = j.PetitionId
                  where ml.LicenseObjectId = a_LicenseObjectId
                    and (api.pkg_columnquery.Value(j.PetitionId, 'StatusName') = a_StatusName or d.statustag = 'COMP')
                    and api.pkg_columnquery.Value(r.PetitionTermObjectId, 'PetitionType') = '12.39'
                  order by PetitionYears
                ) loop
          if t_Output is null then
             t_Output := p.petitionyears;
          else
             t_Output := t_Output || chr(10) || p.petitionyears;
          end if;
       end loop;
    else
       for p in (select nvl2(api.pkg_columnquery.DateValue(r.PetitionTermObjectId,'StartDate'),to_char(api.pkg_columnquery.DateValue(r.PetitionTermObjectId,'StartDate'),'yyyy') || '-' || to_char(api.pkg_columnquery.DateValue(r.PetitionTermObjectId,'EndDate'),'yyyy'),p.Petition1239StartYear || '-' || p.Petition1239EndYear) PetitionYears
                   from query.r_abc_masterlicenselicense ml --go to our master license
                   join query.r_abc_masterlicenselicense cl on cl.MasterLicenseObjectId = ml.MasterLicenseObjectId --get all of the child licenses, including ourself
                   join query.r_abc_LicensePetition j on j.LicenseId = cl.LicenseObjectId --get petitions on each child license
                   join query.r_ABC_PetitionPetitionTerm r on r.PetitionObjectId = j.PetitionId
                   join query.j_abc_petition p on p.jobid = r.PetitionObjectId
                  where ml.LicenseObjectId = a_LicenseObjectId
                    and api.pkg_columnquery.Value(j.PetitionId, 'StatusName') = a_StatusName
                    and api.pkg_columnquery.Value(r.PetitionTermObjectId, 'PetitionType') = '12.39'
                  order by PetitionYears
                ) loop
          if t_Output is null then
             t_Output := p.petitionyears;
          else
             t_Output := t_Output || chr(10) || p.petitionyears;
          end if;
       end loop;
    end if;
    
    return t_Output;
  end PetitionJobs;

  /*---------------------------------------------------------------------------
   * ApprovalActivities -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure ApprovalActivities (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_JobId                             udt_Id
  ) is
    t_JobDefName                        varchar2(60);
    t_LicenseId                         udt_Id;
  begin

    t_JobDefName := api.pkg_columnquery.Value(a_JobId, 'ObjectDefName');
    t_LicenseId := api.pkg_columnquery.NumericValue(a_JobId, 'LicenseObjectId');

    if t_JobDefName = 'j_ABC_Petition' then
      CopyPetitionTerms(a_JobId, t_LicenseId);
    elsif t_JobDefName = 'j_ABC_PetitionAmendment' then
      UpdatePetitionTerms(a_JobId, t_LicenseId);
    end if;

    UpdateLicenseWarnings(t_LicenseId);

  end ApprovalActivities;

  /*---------------------------------------------------------------------------
   * CancelPetitionTerms -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure CancelPetitionTerms (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is
  begin

    if api.pkg_columnquery.Value(a_JobId, 'CancelJob') = 'Y' then
      UpdateJobTermStates(a_JobId, 'Cancelled', true);
    end if;

  end CancelPetitionTerms;


  /*---------------------------------------------------------------------------
   * DenialActivities -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure DenialActivities (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date,
    a_JobId                             udt_Id
  ) is
  begin

    UpdateJobTermStates(a_JobId, 'Rejected');

  end DenialActivities;

  /*---------------------------------------------------------------------------
   * GenerateTermsForAmendment -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure GenerateTermsForAmendment (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_AmendmentTypeId                   udt_Id;
    t_JobId                             udt_Id;
    t_JobTermEndPointId                 udt_Id;
    t_LicenseId                         udt_Id;
    t_PetitionAmendJobDefId             udt_Id;
    t_RelationshipId                    udt_Id;
    t_TermId                            udt_Id;
    t_TermTermEndPointId                udt_Id;
    t_Terms                             udt_IdList;
  begin

    t_PetitionAmendJobDefId := api.pkg_configquery.ObjectDefIdForName('j_ABC_PetitionAmendment');

    select r.FromObjectId
    into t_JobId
    from 
      api.Relationships r
      join api.EndPoints ep
          on ep.EndPointId = r.EndPointId
    where r.RelationshipId = a_RelationshipId
      and ep.FromObjectDefId = t_PetitionAmendJobDefId;

    -- Clear existing terms as there has been a change so the terms there now are no longer valid
    select PetitionTermObjectId
    bulk collect into t_Terms
    from query.r_ABC_PetitionAmendTerms
    where PetitionAmendmentId = t_JobId;

    for i in 1..t_Terms.count loop
      api.pkg_objectupdate.Remove(t_Terms(i));
    end loop;

    -- Create new terms based on license and amendment type
    t_AmendmentTypeId := api.pkg_columnquery.Value(t_JobId, 'PetitionAmendmentTypeId');
    t_LicenseId := api.pkg_columnquery.Value(t_JobId, 'LicenseObjectId');

    if api.pkg_columnquery.Value(t_AmendmentTypeId, 'AllOrSomePetitionTypes') = 'Some' then
      select pt.ObjectId
      bulk collect into t_Terms
      from 
        query.r_ABC_LicensePetitionTerm lpt
        join query.r_ABC_PetitionTermPetitionType ptpt
            on ptpt.PetitionTermObjectId = lpt.PetitionTermObjectId
        join query.r_ABC_PetitionTypePetAmendType ptat
            on ptat.PetitionTypeId = ptpt.PetitionTypeObjectId
        join query.o_ABC_PetitionTerm pt
            on pt.ObjectId = lpt.PetitionTermObjectId
      where lpt.LicenseObjectId = t_LicenseId
        and ptat.PetitionAmendmentTypeId = t_AmendmentTypeId
        and pt.State = 'Granted';
    else
      select pt.ObjectId
      bulk collect into t_Terms
      from 
        query.r_ABC_LicensePetitionTerm lpt
        join query.o_ABC_PetitionTerm pt
            on pt.ObjectId = lpt.PetitionTermObjectId
      where lpt.LicenseObjectId = t_LicenseId
        and pt.State = 'Granted';
    end if;

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'N' then
      t_JobTermEndPointId := api.pkg_configquery.EndPointIdForName(
          t_PetitionAmendJobDefId, 'PetitionTerm');
    else
      t_JobTermEndPointId := api.pkg_configquery.EndPointIdForName(
          t_PetitionAmendJobDefId, 'OnlineRequestedTerm');
    end if;
    t_TermTermEndPointId := api.pkg_configquery.EndPointIdForName(
        'o_ABC_PetitionTerm', 'OriginalPetitionTerm');

    for i in 1..t_Terms.count loop
      t_TermId := ClonePetitionTerm(t_Terms(i));
      t_RelationshipId := api.pkg_relationshipupdate.New(
          t_TermTermEndPointId, t_TermId, t_Terms(i));
      t_RelationshipId := api.pkg_relationshipupdate.New(
          t_JobTermEndPointId, t_JobId, t_TermId);
    end loop;

  end GenerateTermsForAmendment;

  /*---------------------------------------------------------------------------
   * PaymentSucceeded
   *   Activites for when payment succeeds
   *-------------------------------------------------------------------------*/
  procedure PaymentSucceeded (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is
  begin

    if api.pkg_columnquery.Value(a_JobId, 'PaymentSucceeded') = 'Y' then
      api.pkg_columnupdate.SetValue(a_JobId, 'ReceivedDate', sysdate);
      api.pkg_columnupdate.SetValue(a_JobId, 'SubmitApplication', 'Y');
    end if;

  end PaymentSucceeded;

  /*---------------------------------------------------------------------------
   * RegenerateFeeOnRelChange -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RegenerateFeeOnRelChange (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PetitionJobDefId                  udt_Id;
    t_PetitionAmendJobDefId             udt_Id;
  begin

    t_PetitionJobDefId := api.pkg_configquery.ObjectDefIdForName('j_ABC_Petition');
    t_PetitionAmendJobDefId := api.pkg_configquery.ObjectDefIdForName('j_ABC_PetitionAmendment');

    select r.FromObjectId
    into t_JobId
    from 
      api.Relationships r
      join api.EndPoints ep
          on ep.EndPointId = r.EndPointId
    where r.RelationshipId = a_RelationshipId
      and ep.FromObjectDefId in (t_PetitionJobDefId, t_PetitionAmendJobDefId);

    api.pkg_columnupdate.SetValue(t_JobId, 'ReGenerateFees', 'Y');

  end RegenerateFeeOnRelChange;

  /*---------------------------------------------------------------------------
   * SetDefaultAssignments -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure SetDefaultAssignments (
    a_RelationshipId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobDefId                          udt_Id;
    t_JobId                             udt_Id;
    t_PetitionAmendDefId                udt_Id;
    t_PetitionTypeDefId                 udt_Id;
    t_PetitionTypeId                    udt_Id;
  begin

    t_PetitionTypeDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_PetitionType');
    t_PetitionAmendDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_PetitionAmendmentType');

    select
      ep.FromObjectDefId,
      r.FromObjectId,
      r.ToObjectId 
    into
      t_JobDefId,
      t_JobId,
      t_PetitionTypeId
    from 
      api.Relationships r
      join api.EndPoints ep
          on ep.EndPointId = r.EndPointId
    where r.RelationshipId = a_RelationshipId
      and ep.ToObjectDefId in (t_PetitionTypeDefId, t_PetitionAmendDefId);

    -- Set/Replace default users for assignment when petition type/petition amendment type changes
    SetDefaultUser(t_JobId, t_JobDefId, 'DefaultProcessor', t_PetitionTypeId, 'DefaultClerkId', true);
    SetDefaultUser(t_JobId, t_JobDefId, 'AttorneyGeneral', t_PetitionTypeId, 'DefaultDeputyAttorneyGeneralId', true);
    SetDefaultUser(t_JobId, t_JobDefId, 'DefaultSupervisor', t_PetitionTypeId, 'DefaultSupervisorId', true);

  end SetDefaultAssignments;

  /*---------------------------------------------------------------------------
   * SubmissionActivities
   *   Activities to run when a petition is submitted
   *-------------------------------------------------------------------------*/
  procedure SubmissionActivities (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_SystemSettingsId                  udt_Id;
    t_TypeId                            udt_Id;
  begin

    if api.pkg_columnquery.Value(a_ObjectId, 'SubmitApplication') = 'N' then
      return;
    end if;

    SetOriginalLicense(a_ObjectId);

    if api.pkg_columnquery.NumericValue(a_ObjectId, 'OnlineNumRequestedTerms') > 0 then
      CopyRequestedTerms(a_ObjectId);
    end if;

    if api.pkg_columnquery.IsNull(a_ObjectId, 'ApplicationReceivedDate') then
      api.pkg_columnupdate.SetValue(a_ObjectId, 'ApplicationReceivedDate', sysdate);
    end if;

    t_SystemSettingsId := api.pkg_SimpleSearch.ObjectByIndex(
        'o_SystemSettings', 'SystemSettingsNumber', 1);
    api.pkg_columnupdate.SetValue(a_ObjectId, 'LegalDisclaimer',
        api.pkg_columnquery.Value(t_SystemSettingsId, 'PetitionDisclaimerText'));

    -- job type specific activities
    if api.pkg_columnquery.Value(a_ObjectId, 'ObjectDefName') = 'j_ABC_Petition' then
      SetDocketNumber(a_ObjectId);
      t_TypeId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'PetitionTypeId');
      api.pkg_columnupdate.SetValue(a_ObjectId, 'PetitionTypeDisclaimer',
        api.pkg_columnquery.Value(t_TypeId, 'InstrEndorsementDisclaimerText'));
    else
      t_TypeId := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'PetitionAmendmentTypeId');
      api.pkg_columnupdate.SetValue(a_ObjectId, 'AmendmentTypeDisclaimer',
        api.pkg_columnquery.Value(t_TypeId, 'InstrEndorsementDisclaimerText'));
    end if;

  end SubmissionActivities;
  
end pkg_ABC_Petition;
/
