create or replace package abc.pkg_ABC_Vehicle is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * ReGenerateFees()
   *   Called on change of VesselLength to trigger re-calculation of Fees
   *-------------------------------------------------------------------------*/
  procedure ReGenerateFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_Vehicle;
/
create or replace package body abc.pkg_ABC_Vehicle is

  /*---------------------------------------------------------------------------
   * ReGenerateFees()
   *   Called on change of VesselLength to trigger re-calculation of Fees
   *-------------------------------------------------------------------------*/
  procedure ReGenerateFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_LicenseId                         udt_id;
  begin

    begin
      select lv.LicenseId
      into t_LicenseId
      from query.r_abc_licensevehicle lv
      where lv.VehicleId = a_ObjectId;
      -- Force re-generate of Fee.
      abc.Pkg_Abc_Workflow.SetRegenerateFees(t_LicenseId, trunc(sysdate));
    exception
      when no_data_found then
        null;
    end;

  end ReGenerateFees;

end pkg_ABC_Vehicle;
/
