create or replace package pkg_ABC_Workflow is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;

  -----------------------------------------------------------------------------
  -- CreateLicensingNotifications
  --  Runs on Send outcome of Send License
  -----------------------------------------------------------------------------
  procedure CreateLicensingNotifications (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  -- CreateEnforcementNotifications
  -----------------------------------------------------------------------------
  procedure CreateEnforcementNotifications (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  --  AmendmentApplicationNumberChange
  --  Modifies the LicenseNumber on creation of the Amendment Application
  -----------------------------------------------------------------------------
  procedure AmendApplicationNumberChange (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  --  RenewalApplicationNumberChange
  --  Modifies the LicenseNumber on creation of the ARenewal Application
  --  if certain criterial are met.
  -----------------------------------------------------------------------------
  procedure RenewApplicationNumberChange (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  /*---------------------------------------------------------------------------
   * ApproveLicense
   *  This is triggered on the Approve outcome of the Approve/Reject Application
   * process it will set the License State to Active and generate a License Number
   *-------------------------------------------------------------------------*/
  procedure ApproveLicense (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  ---------------------------------------------------------------------------------------------
  -- RejectLicense
  --  this is triggered on the following outcomes:
     -- Reject outcome of the Approve/Reject Application process
     -- Reject outcome of the Review Application process
     -- Close outcome of the Monitor Hold process
  --  it will set the License State to "Not Approved"
  ---------------------------------------------------------------------------------------------
  procedure RejectLicense (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  /*---------------------------------------------------------------------------
   * CopyLicense()
   *   This is triggered on "SubmitApplication" boolean column on the Amendment
   * and Renewal jobs
   *-------------------------------------------------------------------------*/
  procedure CopyLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * OnlineSubmissionActivities()
   *   This is triggered on completion of New Application from Public site.
   * Create a new License object from temporary data details on the job, set
   * default values and copy documents to new rel on license specifically for
   * online objects. Call this procedure before other app completion procedures,
   * so that secondary licenses can be created as normal
   *-------------------------------------------------------------------------*/
  procedure OnlineSubmissionActivities (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * InternalSubmitActivities()
   *   This is triggered on completion of New Application from the internal
   * Site and sets details on the license.
   *-------------------------------------------------------------------------*/
  procedure InternalSubmitActivities (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CancelJob() -- PUBLIC
   *   Run on post verify of most job types when the "Cancel Job" detail
   * is checked.
   *-------------------------------------------------------------------------*/
  procedure CancelJob (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  -- Enforce Resolution Document
  -----------------------------------------------------------------------------
  procedure EnforceResolutionDocument (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  /*---------------------------------------------------------------------------
   * CopyNewApplicationQuestions()
   *   For each application question on the License Type admin object, create
   * a Response object to display on the application job. Also copy any applicable
   * documents.
   *-------------------------------------------------------------------------*/
  procedure CopyNewApplicationQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopyPRApplicationQuestions() -- PUBLIC
   *   Get the Questions and DocumentTypes for a Product Application
   *-------------------------------------------------------------------------*/
  procedure CopyPRApplicationQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopyPRRenewalQuestions() -- PUBLIC
   *   Get the Questions and DocumentTypes for a Product Renewal
   *-------------------------------------------------------------------------*/
  procedure CopyPRRenewalQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopyPoliceReviewQuestions() -- PUBLIC
   *   For each permit application question on the Permit Type admin object,
   * create a Response object to display on the application job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPoliceReviewQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopySubmitResponseQuestions() -- PUBLIC
   *   For each permit application question on the Permit Type admin object,
   * create a Response object to display on the application job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopySubmitResolutionQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopySubmitResponseQuestions() -- PUBLIC
   *   For each permit application question on the Permit Type admin object,
   * create a Response object to display on the application job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyMunicipalReviewQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CreateResponseDocuments()
   *   Create Document relationship based on questions that require a document
   * response
   *-------------------------------------------------------------------------*/
  procedure CreateResponseDocuments(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
   );

  /*---------------------------------------------------------------------------
   * CopyRenewalQuestions()
   *   For each renewal question on the License Type admin object, create
   * a Response object to display on the application job. Also copy any applicable
   * documents.
   *-------------------------------------------------------------------------*/
  procedure CopyRenewalQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopyAmendmentQuestions()
   *   For each amendment question on the Amendment Type admin object, create
   * a Response object to display on the amendment job. Also copy any applicable
   * documents.
   *-------------------------------------------------------------------------*/
  procedure CopyAmendmentQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopyPRAmendmentQuestions()
   *   For each PR amendment question on the Product Amendment Type admin object, create
   * a Response object to display on the PR amendment job. Also copy any applicable
   * documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPRAmendmentQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopyPermitApplicationQuestions()
   *   For each permit application question on the Permit Type admin object,
   * create a Response object to display on the application job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPermitApplicationQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * CopyPermitRenewalQuestions()
   *   For each permit renewal question on the Permit Type admin object,
   * create a Response object to display on the application job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPermitRenewalQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
    CopyDailyDeposit
  *-------------------------------------------------------------------------*/
  procedure CopyDailyDeposit(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * GetVehicles()
   *   Upon creation of Renewal Application from the Public Site, create a new
   * vehicle and related address record, then relate the new vehicle record to
   * the renewal application. This provides an audit trail of changes made to
   * the Vehicle and/or Address record.
   *-------------------------------------------------------------------------*/
  procedure GetVehicles (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
    SetRegenerateFees
  *-------------------------------------------------------------------------*/
  procedure SetRegenerateFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*-----------------------------------------------------------------------------
  -Set Default Condition Processor
  -----------------------------------------------------------------------------*/
  procedure SetDefaultConditionProcessor (
  a_ObjectId            udt_Id,
  a_AsOfDate            date
  ) ;

  /*---------------------------------------------------------------------------
    Generate License
  *-------------------------------------------------------------------------*/
  procedure GenerateLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * RenewalLicenseExpirationDate()
   *-------------------------------------------------------------------------*/
  procedure RenewalLicenseExpirationDate (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
    SetDocumentId
  *-------------------------------------------------------------------------*/
  procedure SetDocumentId (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  ----------------------------------------------------------------------------
  --  RemoveFees() -- PUBLIC
  --  Removes all the fees on a job - Called when the job moves into the
  --  cancelled status
  ----------------------------------------------------------------------------
  procedure RemoveFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  ----------------------------------------------------------------------------
  --  RemoveFeesOnWithdrawal
  --  Removes all the fees on a job - Called when the job moves into the
  --  Withdrawn status
  ----------------------------------------------------------------------------
  procedure RemoveFeesOnWithdrawal (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  ----------------------------------------------------------------------------
  --  SetSendLicProcessId()
  ----------------------------------------------------------------------------
  procedure SetSendLicProcessId (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  ----------------------------------------------------------------------------
  --  JobAppSummary()
  ----------------------------------------------------------------------------
  procedure JobAppSummary (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  ----------------------------------------------------------------------------
  --  SubJobAppSummary()
  ----------------------------------------------------------------------------
  procedure SubJobAppSummary (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  -- RegistrationVideoConstraint
  -- Ensure that only one tutorial video can be marked as 'Y'
  -----------------------------------------------------------------------------
  procedure RegistrationVideoConstraint (
    a_ObjectId                           udt_Id,
    a_AsOfDate                           date
  );

  /*---------------------------------------------------------------------------
    GetPermitRenewalData
  *-------------------------------------------------------------------------*/
  procedure GetPermitRenewalData(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
    Generate Batch Renewal Notification Document
  *-------------------------------------------------------------------------*/
  procedure GenerateBatchRenewalDocument (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_BatchType                         varchar2
  );

  ---------------------------------------------------------------------------------------------
  -- CopyPermit
  ---------------------------------------------------------------------------------------------
  procedure CopyPermit (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  ---------------------------------------------------------------------------------------------
  -- ApprovePermit
  ---------------------------------------------------------------------------------------------
  procedure ApprovePermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * PermitRenewalPostVerify()
  *-------------------------------------------------------------------------*/
  procedure PermitRenewalPostVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * DenyPermit()
  *  Runs Denial actions
  *  Run from workflow on the Permit Renewal job
  *-------------------------------------------------------------------------*/
  procedure DenyPermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * WithdrawPermit()
  *  Runs Denial actions
  *  Run from workflow on the Permit Renewal job
  *-------------------------------------------------------------------------*/
  procedure WithdrawPermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

 /*---------------------------------------------------------------------------
  * RevokePermit()
  *  Runs Denial actions
  *  Run from workflow on the Permit Renewal job
  *-------------------------------------------------------------------------*/
  procedure RevokePermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
    SetRegenerateFeesPermit
  *-------------------------------------------------------------------------*/
  procedure SetRegenerateFeesPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
    GetPermitId
  *-------------------------------------------------------------------------*/
  procedure GetPermitId (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
    CopyPermitteeToOnlineUserExt
  *-------------------------------------------------------------------------*/
  procedure CopyPermitteeToOnlineUserExt (
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  --  TransferDistributorLicense
  --  Transfer Distributor License on Renewal/Amendment to new license.
  -----------------------------------------------------------------------------
  procedure TransferDistributorLicense (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  -----------------------------------------------------------------------------
  --  CheckActiveJobs
  --  Check any active Renewal/Amendment jobs for the License or Permit
  -----------------------------------------------------------------------------
  procedure CheckActiveJobs (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  procedure RenewalConstructor (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

-----------------------------------------------------------------------------
  -- GetLatestLicense
  --   Get the License version which is the latest version
  -----------------------------------------------------------------------------
  function GetLatestLicense(
    a_LicenseId                         udt_id,
    a_PermitId                          udt_id
  ) return number;

-----------------------------------------------------------------------------
  -- GetLatestPermit
  --   Get the Associated Permit which is the latest Active version
  -----------------------------------------------------------------------------
  function GetLatestPermit(
    a_PermitId                          udt_id,
    a_AssociatedPermitId                udt_id
  ) return number;

  /*---------------------------------------------------------------------------
    SubmitAdditionalInfo
  *-------------------------------------------------------------------------*/
  procedure SubmitAdditionalInfo(
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*---------------------------------------------------------------------------
   * GetLatestSolicitorPermit()
   *   Get the Solicitor Permit which is the latest Active version (if any)
   *-------------------------------------------------------------------------*/
  function GetLatestSolicitorPermit(
    a_PermitObjectId                    udt_id,
    a_SolicitorObjectId                 udt_id
  ) return number;

  /*---------------------------------------------------------------------------
    Cancel Application / Amendment Job
  *-------------------------------------------------------------------------*/
  procedure CancelLicenseJob(
    a_ObjectId udt_Id,
    a_AsOfDate date
  );

  /*---------------------------------------------------------------------------
    Submit Review Validation
  *-------------------------------------------------------------------------*/
  procedure SubmitReviewValidation(
    a_ObjectId udt_Id,
    a_AsOfDate date,
    a_SpecialConditions varchar,
    a_Outcome varchar
  );

  /*---------------------------------------------------------------------------
  * MonitorReviewProcedure
  *   Complete the Monitor Review Process for Permit Applications and Renewals
  *--------------------------------------------------------------------------*/
  procedure MonitorReviewProcedure(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * ResetLicense
   *   Updates current pending license state to Superseded and generates a
   * new pending license object based on the latest license version
   *--------------------------------------------------------------------------*/
  procedure ResetLicense(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_Workflow;
/
create or replace package body pkg_ABC_Workflow is

  g_ParentResponseEPId                  udt_Id :=
      api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'ParentResponse');

  -----------------------------------------------------------------------------
  -- CopyColumns() -- PRIVATE
  -----------------------------------------------------------------------------
  procedure CopyColumns (
    a_SourceObjectId                    pls_integer,
    a_TargetObjectId                    pls_integer
  ) is
    cursor c_ColumnDefs is
      select
        cd.ColumnDefId,
        cd.Name,
        api.pkg_ColumnQuery.Value(o.ObjectId, cd.Name) Value
      from
        api.Objects o
        join api.ColumnDefs cd
            on cd.ObjectDefId = o.ObjectDefId
      where o.ObjectId = a_SourceObjectId
        and cd.Stored = 'Y'
        and cd.Active = 'Y'
        and lower(cd.Name) <> 'remove'
        and lower(cd.Name) not like 'dup/_%' escape '/'
        and lower(cd.Name) not like 'zzz%'
        and lower(cd.Name) not like 'donotrunpostverifyprocedures'
        and lower(cd.Name) not in ('issuedate', 'effectivedate', 'expirationdate', 'expirationdateoverridden')
        and lower(cd.Name) not like 'amendmentnumber'
        and lower(cd.Name) not like 'renewalnumber'
        and lower(cd.Name) not like 'reinstatenumber'
        and lower(cd.Name) not like 'createinspectionjob'
        and lower(cd.Name) <> 'additionalwarehousesalesroomfe'
        ;

  begin
    for cd in c_ColumnDefs loop
      if cd.Value is not null then
        api.pkg_ColumnUpdate.SetValue(a_TargetObjectId, cd.ColumnDefId,
            cd.Value);
      end if;
    end loop;
  end CopyColumns;

  -----------------------------------------------------------------------------
  -- CopyRels() -- PRIVATE
  -----------------------------------------------------------------------------
  procedure CopyRels (
    a_SourceObjectId                    pls_integer,
    a_TargetObjectId                    pls_integer
  ) is
    t_RelId                             number;
    t_OfficeCount                       number;
    t_LicenseTypeCode                   varchar2(100);

  begin

    for i in (select r.FromObjectId, r.ToObjectId, rd.ToEndPointId, rd.ToEndPointName, r.RelationshipId
                from api.relationships r
                join api.relationshipdefs rd on rd.RelationshipDefId = r.RelationshipDefId
               where r.FromObjectId = a_SourceObjectId
                 and rd.ToEndPointName in ('Office'
                                         , 'Master'
                                         , 'Region'
                                         , 'Establishment'
                                         , 'Licensee'
                                         , 'LicenseType'
                                         , 'RiskFactor'
                                         , 'EventType'
                                         , 'ConditionType'
                                         , 'SelectSecondaryLicType'
                                         , 'EventLocationAddress'
                                         , 'LicenseWarning'
                                         , 'PetitionType'
                                         )) loop

      --if Office, we need to check that it still belongs to the Region of the License
      if i.toendpointname = 'Office' then
        select count(1)
          into t_OfficeCount
          from query.r_ABC_RegionOffice r1
         where r1.OfficeObjectId = i.toobjectid
           and r1.RegionObjectId = api.pkg_columnquery.NumericValue(a_SourceObjectId, 'RegionObjectId');

         --we only copy the Office over if it does in fact belong to the Region of the License
         if t_OfficeCount > 0 then
           t_RelId := api.pkg_relationshipupdate.New(i.toendpointid, a_TargetObjectId, i.toobjectid);
         end if;
      else
        if i.toendpointname = 'UniqueLicense' then

           --t_RelId := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseHistory'), i.FromObjectId ,i.ToObjectId);
           api.pkg_relationshipupdate.Remove(i.Relationshipid);

        end if;
        t_RelId := api.pkg_relationshipupdate.New(i.toendpointid, a_TargetObjectId, i.toobjectid);
      end if;

    end loop;
  end CopyRels;

  -----------------------------------------------------------------------------
  -- CloneObject() -- PRIVATE
  -----------------------------------------------------------------------------
  procedure CloneObject (
    a_SourceObjectId                    pls_integer,
    a_TargetObjectId                    pls_integer,
    a_EndpointName                      varchar,
    a_ObjectDefName                     varchar
  ) is
    t_RelId                             number;
    t_NewObjectId                       number;

  begin
    for i in (select r.ToObjectId, rd.ToEndPointId
                from api.relationships r
                join api.relationshipdefs rd on rd.RelationshipDefId = r.RelationshipDefId
               where r.FromObjectId = a_SourceObjectId
                 and rd.ToEndPointName = a_EndpointName) loop

      t_NewObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName(a_ObjectDefName));
      CopyColumns(i.toobjectid, t_NewObjectId);
      CopyRels(i.toobjectid, t_NewObjectId);
      api.pkg_columnupdate.SetValue(t_NewObjectId, 'SourceObjectId', i.toobjectid);

      t_RelId := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName('o_ABC_License', a_EndpointName), a_TargetObjectId, t_NewObjectId);
    end loop;
  end CloneObject;

  -----------------------------------------------------------------------------
  -- CopyPermitteeToOnlineUser()
  --
  -- Description: Copy Permittee rel to online user for use in future applications.
  --   Includes all checks to see if a copy is necessary.
  -----------------------------------------------------------------------------
  procedure CopyPermitteeToOnlineUser (
    a_JobId                             pls_integer
  ) is
    t_Permittee                         number;
    t_OnlineUser                        number;
    t_ExistingRelCount                  number;
    t_JobDef                            varchar2(30) := api.pkg_columnquery.Value(a_JobId, 'ObjectDefName');

  begin
    if t_JobDef = 'j_ABC_PermitApplication' then
      if  api.pkg_ColumnQuery.Value(a_JobId,'EnteredOnline') = 'Y' then
        select api.pkg_ColumnQuery.NumericValue(j.PermitObjectId, 'FormattedPermitteeObjectId'), j.OnlineUserObjectId
          into t_Permittee, t_OnlineUser
          from query.j_ABC_PermitApplication j
         where j.JobId = a_JobId;
      end if;

      if t_OnlineUser is not null then -- no copy for guest users and users that already have this licensee
        select count(1)
          into t_ExistingRelCount
          from query.r_ABC_UserLegalEntity rul
         where rul.UserId = t_OnlineUser
           and rul.LegalEntityObjectId = t_Permittee;
        if t_ExistingRelCount = 0 then
          extension.pkg_RelationshipUpdate.New(
            t_OnlineUser,
            t_Permittee,
            'LegalEntity');
        end if;
      end if;
    end if;
  end CopyPermitteeToOnlineUser;

  -----------------------------------------------------------------------------
  -- CopyLicenseeToOnlineUser() -- PRIVATE
  --
  -- Created: 01Sep2011, Stan H
  -- Description: Copy Licensee rel to online user for use in future applications.
  --   Includes all checks to see if a copy is necessary.
  -----------------------------------------------------------------------------
  procedure CopyLicenseeToOnlineUser (
    a_JobId                             pls_integer
  ) is
    t_Licensee                          number;
    t_OnlineUser                        number;
    t_ExistingRelCount                  number;
    t_JobDef                            varchar2(30) := api.pkg_columnquery.Value(a_JobId, 'ObjectDefName');

  begin
    if t_JobDef = 'j_ABC_NewApplication' then
      if  api.pkg_ColumnQuery.Value(a_JobId,'EnteredOnline') = 'Y' then
        select api.pkg_ColumnQuery.NumericValue(j.LicenseObjectId, 'LicenseeObjectId'), j.OnlineUserObjectId
          into t_Licensee, t_OnlineUser
          from query.j_ABC_NewApplication j
         where j.JobId = a_JobId;
      end if;

      if t_OnlineUser is not null then -- no copy for guest users and users that already have this licensee
        select count(1)
          into t_ExistingRelCount
          from query.r_ABC_UserLegalEntity rul
         where rul.UserId = t_OnlineUser
           and rul.LegalEntityObjectId = t_Licensee;
        if t_ExistingRelCount = 0 then
          extension.pkg_RelationshipUpdate.New(
            t_OnlineUser,
            t_Licensee,
            'LegalEntity');
        end if;
      end if;
    end if;
  end CopyLicenseeToOnlineUser;

  -----------------------------------------------------------------------------
  -- CreateNotifications
  --  Runs on Send outcome of Send License
  -----------------------------------------------------------------------------
  procedure CreateNotifications (
    a_ObjectId            udt_Id,
    a_AccessGroup         varchar2
  ) is
    t_JobId               udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'JobId');
    t_ProcessTypeId       udt_Id := api.pkg_ConfigQuery.ObjectDefIdForName('p_ABC_Notification');
    t_NewProcessId        udt_Id;

  begin
    for i in (
        select agu.UserId
          from api.AccessGroups ag
            join api.AccessGroupUsers agu
              on agu.AccessGroupId = ag.AccessGroupId
          where ag.Description = a_AccessGroup
            and api.pkg_columnquery.Value(agu.UserId, 'Active') = 'Y') loop

      t_NewProcessId := api.pkg_ProcessUpdate.New(t_JobId, t_ProcessTypeId, 'Notification', sysdate, null, null);
      api.pkg_ProcessUpdate.Assign(t_NewProcessId, i.userid);
      api.pkg_ColumnUpdate.SetValue(t_NewProcessId, 'Notification', api.pkg_ColumnQuery.Value(a_ObjectId, 'TextForNotification'));
    end loop;
  end CreateNotifications;

  -----------------------------------------------------------------------------
  -- CreateLicensingNotifications
  --  Runs on Send outcome of Send License
  -----------------------------------------------------------------------------
  procedure CreateLicensingNotifications (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
  begin
    CreateNotifications(a_ObjectId, 'Licensing Notification');
  end CreateLicensingNotifications;

  -----------------------------------------------------------------------------
  -- CreateEnforcementNotifications
  --  Runs on Send outcome of Send License
  -----------------------------------------------------------------------------
  procedure CreateEnforcementNotifications (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
  begin
    CreateNotifications(a_ObjectId, 'Enforcement Notification');
  end CreateEnforcementNotifications;

  -----------------------------------------------------------------------------
  --  AmendmentApplicationNumberChange
  --  Modifies the LicenseNumber on creation of the Amendment Application
  -----------------------------------------------------------------------------
  procedure AmendApplicationNumberChange(
    a_ObjectId                    udt_Id,
    a_AsOfDate                    date
  ) is

    t_JobDef                      varchar2(30);
    t_JobId                       udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'JobId');
    t_GenerationNumber            number;
    t_PrimaryLicenseObjectId      udt_Id;
    t_LicenseId                   udt_id;
    t_SourceLicenseObjectId       udt_Id;
    t_IncrementLicenseNumber      varchar(1);
    t_LicenseNumber               varchar2(20);

  begin
    t_JobDef := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_PrimaryLicenseObjectId := api.pkg_ColumnQuery.Value(t_JobId, 'LicenseObjectId');
    t_SourceLicenseObjectId := api.pkg_ColumnQuery.NumericValue(t_PrimaryLicenseObjectId, 'SourceObjectId');
    t_LicenseNumber := api.pkg_columnquery.Value(t_SourceLicenseObjectId, 'LicenseNumber');
    t_LicenseId := api.pkg_ColumnQuery.Value(t_JobId, 'LicenseObjectId');
    t_GenerationNumber := substr(t_LicenseNumber, 13,3); --Grab the Last Three Digits of the License Number to get the generation Number
    select api.pkg_columnquery.value(r.AmendmentTypeId,'IncrementLicenseNumber')
      into t_IncrementLicenseNumber
      from query.r_ABC_AmendJobLicenseAmendType r
      where r.AmendJobLicenseId = t_JobId
      and rownum < 2;

    if t_JobDef = 'j_ABC_AmendmentApplication' then
      if t_GenerationNumber is null then
        t_GenerationNumber := 1;
      elsif t_IncrementLicenseNumber = 'Y' then
        t_GenerationNumber := t_GenerationNumber + 1;
        t_LicenseNumber := substr(t_LicenseNumber, 1, 12) || lpad(to_char(t_GenerationNumber), 3, '0');
      end if;
      api.pkg_ColumnUpdate.SetValue(t_LicenseId, 'GenerationNumber', t_GenerationNumber);
      api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'LicenseNumber', t_LicenseNumber);
    end if;

  end AmendApplicationNumberChange;

  -----------------------------------------------------------------------------
  --  RenewApplicationNumberChange
  --  Sets the LicenseNumber on creation of the renewal Application
  --  In New Jersey ABC License Numbers never change on a Renewal
  -----------------------------------------------------------------------------
  procedure RenewApplicationNumberChange(
    a_ObjectId                   udt_Id,
    a_AsOfDate                   date
    ) is

    t_JobDef                      varchar2(30);
    t_JobId                       udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'JobId');
    t_PrimaryLicenseObjectId      udt_Id;
    t_LicenseId                   udt_id;
    t_SourceLicenseObjectId       udt_Id;
    t_LicenseNumber               varchar2(20);

  begin
    t_JobDef := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_PrimaryLicenseObjectId := api.pkg_ColumnQuery.Value(t_JobId, 'LicenseObjectId');
    t_SourceLicenseObjectId := api.pkg_ColumnQuery.NumericValue(t_PrimaryLicenseObjectId, 'SourceObjectId');
    t_LicenseNumber := api.pkg_columnquery.Value(t_SourceLicenseObjectId, 'LicenseNumber');

    if t_JobDef = 'j_ABC_RenewalApplication' then
      api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'LicenseNumber', t_LicenseNumber);
    end if;

  end RenewApplicationNumberChange;

  /*---------------------------------------------------------------------------
   * ApproveLicense() -- PUBLIC
   *  This is triggered on the Approve outcome of the Approve/Reject Application
   * process it will set the License State to Active and generate a License Number
   *-------------------------------------------------------------------------*/
  procedure ApproveLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_EndpointId                        udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'Secondary');
    t_JobDef                            varchar2(30);
    t_JobId                             udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'JobId');
    t_PrimaryLicenseObjectId            udt_Id;
    t_RelId                             udt_Id;
    t_SecSourceLicenseObjectId          udt_Id;
    t_SecSourceLicenseState             varchar2(20);
    t_SourceLicenseObjectId             udt_Id;
    t_SourceLicenseState                varchar2(20);
    t_MunicipalityOrderOfIssuance       number;
    t_IssuingAuthority                  varchar2(20);
    t_GenerationNumber                  number;
    t_CountyCode                        varchar2(20);
    t_LicenseTypeCode                   varchar2(20);
    t_MunicipalityCode                  varchar2(20);
    t_MunicipalityCountyCode            varchar2(20);
    t_StateOrderOfIssuance              number;
    t_StateCode                         varchar2(20);
    t_OfficeId                          udt_Id := api.pkg_columnquery.numericValue(t_JobId, 'OfficeObjectId');
    t_RegionId                          number;
    t_LicenseId                         udt_id;
    t_SystemSettingsId                  number;
    t_StartInactivity                   varchar2(01);
    t_SeqExists                         varchar2(1);
    t_EndInactivity                     varchar2(1);
    t_PrevLicenseId                     udt_id;
    t_PermitCount                       number;
    t_GenerationalLicenseEPId           udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'GenerationalLicensePermit');
    t_PermitLicenseEPId                 udt_id := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit', 'License');
    t_HistoricalCoOpEPId                udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'HistoricalCoOpLicense_Permit');
    t_CoOpLicenseEPId                   udt_Id := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_License', 'CoOpLicense_Permit');
    t_LicenseTypeObjectId               udt_id;
    t_EffectiveDate                     date;
    t_ExpirationDate                    date;
    t_MasterLicenseObjectId             udt_Id := api.pkg_ColumnQuery.NumericValue(t_JobId, 'MasterLicenseObjectId');
    t_RelatedLicenseObjectId            udt_Id;
    t_AdditionalWarehouseCount          number;
    t_LicHistoryEndpointId              number := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseHistory');
    t_UniqueLicenseEndpointId           number := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'UniqueLicense');
    t_OriginalIssueDate                 date;
  begin

    t_JobDef := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_PrimaryLicenseObjectId := api.pkg_ColumnQuery.Value(t_JobId, 'LicenseObjectId');
    t_SourceLicenseObjectId := api.pkg_ColumnQuery.NumericValue(t_PrimaryLicenseObjectId, 'SourceObjectId');
    t_SourceLicenseState := api.pkg_ColumnQuery.Value(t_SourceLicenseObjectId, 'State');
    t_IssuingAuthority := api.pkg_ColumnQuery.Value(t_JobId, 'IssuingAuthority');

    t_LicenseId := api.pkg_ColumnQuery.Value(t_JobId, 'LicenseObjectId');
    t_GenerationNumber := api.pkg_columnquery.value(t_LicenseId, 'GenerationNumber');

    t_RegionId := api.pkg_columnquery.Value(t_JobId, 'RegionObjectId');
    t_CountyCode := api.pkg_columnquery.value(t_RegionId, 'CountyCode');

    t_LicenseTypeCode := api.pkg_columnquery.value(t_JobId,'LicenseTypeCode');
    t_LicenseTypeObjectId := api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'LicenseTypeObjectId');

    if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'LicenseTypeCode') = '24' then
       t_RelatedLicenseObjectId := api.pkg_ColumnQuery.NumericValue(t_PrimaryLicenseObjectId, 'RelatedLicenseObjectId');
       if t_RelatedLicenseObjectId is null then
          api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must select an active Related License.');
       end if;
       if api.pkg_columnquery.Value(t_RelatedLicenseObjectId, 'State') != 'Active' then
          api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must select an active Related License.');
       end if;
    end if;

   --     api.pkg_errors.raiseerror(-20000, 'SS ID: ' || t_SystemSettingsId || ' State Code: ' || t_StateCode || ' License ID: ' || t_PrimaryLicenseObjectId || ' License Code: ' || api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'LicenseTypeCode'));
    -- Check License Specific Details
      if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'LicenseTypeCode')   = '13' then
         if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'RetailTransitType') = 'Limousine' then
            if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'VehiclesEntered') is null then
               api.pkg_errors.RaiseError(-20000, 'At least one Vehicle is required on the License.');
            end if;
            for v in (select vehicleid
                        from query.r_Abc_Licensevehicle
                       where licenseid = t_PrimaryLicenseObjectId) loop
                       if api.pkg_columnquery.Value(v.vehicleid, 'InsigniaNumber') is null then
                          api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an InsigniaNumber for each Limo on the License.');
                          exit;
                       end if;
            end loop;
         end if;
         if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'RetailTransitType') = 'Boat' then
            if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'VesselsEntered') is null then
               api.pkg_errors.RaiseError(-20000, 'At least one Vessel is required on the License.');
            end if;
         end if;
      end if;

    --If Expiration Method = 'Seasonal' pre-fill the Effective Date
      if api.pkg_ColumnQuery.Value(t_LicenseTypeObjectId, 'ExpirationMethod') = 'Seasonal' then
         if api.pkg_ColumnQuery.DateValue(t_PrimaryLicenseObjectId, 'EffectiveDate') is null then
            t_EffectiveDate := to_Date(api.pkg_columnquery.Value(t_LicenseTypeObjectId, 'ExpirationStartMonth') || '/' || api.pkg_columnquery.Value(t_LicenseTypeObjectId, 'ExpirationStartDay'), 'MM/DD');
            if trunc(sysdate) > t_EffectiveDate then
               t_EffectiveDate := add_months (t_EffectiveDate, 12);
            end if;
            api.pkg_columnupdate.SetValue(t_PrimaryLicenseObjectId, 'EffectiveDate', t_EffectiveDate);
         end if;
      end if;
    --first check for mandatories on the Primary License (unless the expiration method is Never)
      if api.pkg_ColumnQuery.DateValue(t_PrimaryLicenseObjectId, 'EffectiveDate') is null then
        api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Effective Date for the License.');
      end if;
    if api.pkg_ColumnQuery.Value(t_PrimaryLicenseObjectId, 'ExpirationMethod') != 'Never' then
      if api.pkg_ColumnQuery.DateValue(t_PrimaryLicenseObjectId, 'CalculatedExpirationDate') is null 
          or (api.pkg_ColumnQuery.Value(t_PrimaryLicenseObjectId, 'ExpirationDateOverridden') = 'Y'
              and api.pkg_ColumnQuery.DateValue(t_PrimaryLicenseObjectId, 'ExpirationDate') is null) then
        api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Expiration Date for the License.');
      end if;
    end if;

    -- Check to see if there are any other Renewal jobs in Awaiting status
    --  with an expiration date prior to this one
    if t_JobDef = 'j_ABC_RenewalApplication' then
       begin
         select max(api.pkg_columnquery.datevalue(ral.RenewAppJobId, 'CalculatedExpirationDate'))
           into t_ExpirationDate
           from query.r_ABC_RenewAppJobLicense ral
           join query.r_ABC_MasterLicenseLicense mll
               on mll.LicenseObjectId = ral.LicenseObjectId
          where mll.MasterLicenseObjectId = t_MasterLicenseObjectId
            and api.pkg_columnquery.value(ral.RenewAppJobId, 'StatusDescription') in
                ('Awaiting Resolution', 'Awaiting Relief');
       exception
          when no_data_found then
             t_ExpirationDate := null;
       end;
       if t_ExpirationDate is not null then
          if api.pkg_ColumnQuery.DateValue(t_PrimaryLicenseObjectId, 'CalculatedExpirationDate') > t_ExpirationDate then
             api.pkg_errors.RaiseError(-20000, 'Before approving this License, you must first finish the Renewal with a term prior to this.');
          end if;
       end if;
    end if;

    -- Check if all fees are paid
    if api.pkg_columnquery.Value(t_JobId, 'HasNonZeroBalances') = 'Y' then
       api.pkg_errors.RaiseError(-20000, 'Before you go on, ensure all fees are paid.');
    end if;

    --get the StartInactivity flag to be used later for validation and deletion of existing date if flag is off
    if t_JobDef = 'j_ABC_AmendmentApplication' then
       select api.pkg_columnquery.Value(am.AmendmentTypeId, 'StartInactivity')
         into t_StartInactivity
        from query.r_ABC_AmendJobLicenseAmendType am
       where am.AmendJobLicenseId = t_JobId;

    --check if the Inactivity Date is mandatory and if it is, check that it is assigned.
    if  t_StartInactivity = 'Y' then
      if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'InactivityStartDate') is null then
      api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Inactivity Start Date for the License.');
      end if;
    end if;
  end if;

    --if the License is currently suspended, we need to make sure the new one remains Suspended
    if t_SourceLicenseState = 'Suspended' then
      api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'State', 'Suspended');
      --find all related Accusation jobs and relate the new License to them
      for a in (select al.RelationshipId, al.AccusationJobId
                  from query.r_ABC_AccusationLicense al
                  where al.LicenseObjectId = t_SourceLicenseObjectId) loop

        api.pkg_RelationshipUpdate.Remove(a.relationshipid);
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_configquery.EndPointIdForName('j_ABC_Accusation', 'License'), a.accusationjobid, t_PrimaryLicenseObjectId);
      end loop;
    else
      api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'State', 'Active');
    end if;

    api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'IsLatestVersion', 'Y');
    if t_JobDef != 'j_ABC_Reinstatement' then
       api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'IssueDate', trunc(sysdate));
       if api.pkg_ColumnQuery.Value(t_PrimaryLicenseObjectId, 'ExpirationDateOverridden') != 'Y' then
         api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'ExpirationDate', api.pkg_columnquery.DateValue(t_PrimaryLicenseObjectId, 'CalculatedExpirationDate'));
       end if;
    end if;

   --Generate the license number for a new application if the issuing authority is municipality.
    if t_JobDef = 'j_ABC_NewApplication' then
      if t_IssuingAuthority = 'Municipality' then
        t_MunicipalityCode := api.pkg_columnquery.value(t_OfficeId, 'MunicipalityCode');
        t_MunicipalitycountyCode := api.pkg_columnquery.value(t_OfficeId, 'MunicipalityCountyCode');
        begin
          execute immediate 'select abc.muniorderissue' || t_MunicipalitycountyCode || '_seq.nextval from dual'
          into t_MunicipalityOrderOfIssuance;
        exception when OTHERS then
          api.pkg_errors.raiseerror(-20100, 'Order of Issuance sequence does not exist for Municipality ' || t_MunicipalitycountyCode);
        end;
        api.pkg_ColumnUpdate.SetValue(t_OfficeId, 'OrderOfIssuance', t_MunicipalityOrderOfIssuance);
        t_GenerationNumber := 1;
        --api.pkg_ColumnUpdate.SetValue(t_LicenseId, 'GenerationNumber', t_GenerationNumber);
        api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'LicenseNumber', lpad(t_CountyCode, 2, '00') || lpad(t_MunicipalityCode, 2, '00') || '-' || lpad(t_LicenseTypeCode, 2, '00') || '-' || lpad(t_MunicipalityOrderOfIssuance, 3, '000') || '-' || lpad(t_GenerationNumber, 3, '000'));
     --Generate the license number for a new application if the issuing authority is state.
      elsif t_IssuingAuthority = 'State' then
        select o.ObjectId
        into t_SystemSettingsId
        from query.o_systemsettings o
        where rownum < 2;
        begin
          t_StateOrderOfIssuance := abc.stateorderissue_seq.nextval;
        exception when others then
          api.pkg_errors.RaiseError(-20101, 'Order of Issuance sequence does not exist for State Issued');
        end;
        t_StateCode := api.pkg_columnquery.numericvalue(t_SystemSettingsId, 'StateCode');
        if t_StateOrderOfIssuance = '1' then
          api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'StateCode', t_StateCode + 1);
        end if;
        api.pkg_ColumnUpdate.SetValue(t_SystemSettingsId, 'OrderOfIssuance', t_StateOrderOfIssuance);
        t_GenerationNumber := 1;
        --api.pkg_ColumnUpdate.SetValue(t_LicenseId, 'GenerationNumber', t_GenerationNumber);
        api.pkg_ColumnUpdate.SetValue(t_PrimaryLicenseObjectId, 'LicenseNumber', lpad(t_StateCode, 4, '0000') || '-' || lpad(t_LicenseTypeCode, 2, '00') || '-' || lpad(t_StateOrderOfIssuance, 3, '000') || '-' || lpad(t_GenerationNumber, 3, '000'));
      end if;
    end if;

    if t_JobDef = 'j_ABC_AmendmentApplication' then
      --check if the End Inactivity is checked for the amendment type and clear it if it is
       select api.pkg_columnquery.Value(am.AmendmentTypeId, 'EndInactivity')
         into t_EndInactivity
        from query.r_ABC_AmendJobLicenseAmendType am
       where am.AmendJobLicenseId = t_JobId;

       if  t_EndInactivity = 'Y' then
           if api.pkg_columnquery.Value(t_PrimaryLicenseObjectId, 'InactivityStartDate') is not null then
             api.pkg_columnupdate.RemoveValue(t_LicenseId, 'InactivityStartDate');
           end if;
       end if;
    end if;

   if t_JobDef in ('j_ABC_AmendmentApplication', 'j_ABC_RenewalApplication', 'j_ABC_Reinstatement') then
      --if this is an Amendment or Renewal or Reinstatement, we need to close the source License
      --if the source License was Revoked or Expired, we leave it in that state, otherwise we Close it
      if t_SourceLicenseState not in ('Revoked', 'Expired') then
        api.pkg_ColumnUpdate.SetValue(t_SourceLicenseObjectId, 'State', 'Closed');
      end if;
      --this License is no longer the latest version
      api.pkg_ColumnUpdate.SetValue(t_SourceLicenseObjectId, 'IsLatestVersion', 'N');

  --Set the Original Issue Date from the original License
       if api.pkg_columnquery.DateValue(t_PrimaryLicenseObjectId, 'OriginalIssueDate') is null then
          t_OriginalIssueDate := trunc(api.pkg_columnquery.DateValue(t_SourceLicenseObjectId, 'OriginalIssueDate'));
          api.pkg_columnupdate.SetValue(t_PrimaryLicenseObjectId, 'OriginalIssueDate', t_OriginalIssueDate);
       end if;

      --If the License is a related License move the 24 license to the new version
      begin
        select count(*)
          into t_AdditionalWarehouseCount
          from query.r_ABC_AddtlWarehouseLicense awl
         where awl.LicenseObjectId = t_SourceLicenseObjectId;

         if t_AdditionalWarehouseCount > 0 then
         for c in (select awl.AddtlWhseLicenseObjectId AddLic,
                          awl.RelationshipId RelIdToRemove,
                          api.pkg_columnquery.value(awl.AddtlWhseLicenseObjectId,'State')  State
                     from query.r_ABC_AddtlWarehouseLicense awl
                   where awl.LicenseObjectId = t_SourceLicenseObjectId) loop
           if c.State = 'Active' then
           t_RelId := api.pkg_relationshipupdate.New(t_LicHistoryEndpointId, t_SourceLicenseObjectId, c.AddLic);
           api.pkg_relationshipupdate.Remove(c.RelIdToRemove);
           t_RelId := api.pkg_relationshipupdate.New(t_UniqueLicenseEndpointId, t_PrimaryLicenseObjectId, c.AddLic);
           end if;
         end loop;
       end if;

        exception when no_data_found then
          --do nothing we are not a related license
        null;

      end;

/*-- either set or clear the Inactivity Date based on the Amendment Type
      if t_StartInactivity = 'N' then
         api.pkg_columnupdate.RemoveValue(t_LicenseId, 'InactivityStartDate');
      end if;*/


      --we also need to close any other Licenses that have been issued through concurrent amendments, renewals, re-instatements
      for j in (select r2.LicenseObjectId
                  from query.r_ABC_MasterLicenseLicense r1
                    join query.r_ABC_MasterLicenseLicense r2
                      on r2.MasterLicenseObjectId = r1.MasterLicenseObjectId
                    join query.o_abc_License l
                      on l.objectid = r2.LicenseObjectId
                  where r1.LicenseObjectId = t_PrimaryLicenseObjectId
                    and r2.LicenseObjectId not in (t_SourceLicenseObjectId, t_PrimaryLicenseObjectId)
                    and l.State = 'Active') loop
        api.pkg_ColumnUpdate.SetValue(j.LicenseObjectId, 'State', 'Closed');
        api.pkg_ColumnUpdate.SetValue(j.LicenseObjectId, 'IsLatestVersion', 'N');
      end loop;

      --if this is a secondary license, ensure the new version replaces the old on the primary license
      for l in (select r.RelationshipId, r.PrimaryLicenseObjectId
                  from query.r_ABC_PrimarySecondaryLicense r
                  where r.SecondaryLicenseObjectId = t_SourceLicenseObjectId) loop
        api.pkg_RelationshipUpdate.Remove(l.RelationshipId);
        t_RelId := api.pkg_RelationshipUpdate.New(t_EndpointId, l.PrimaryLicenseObjectId, t_PrimaryLicenseObjectId);
      end loop;
      --find the License that is being amended/renewed/reinstated. Need this to transfer the Permits to the new License.
      begin
        select ri.LicenseToReinstateObjectId
          into t_PrevLicenseId
          from query.j_abc_reinstatement ri
         where ri.ObjectId = t_JobId
        union
        select ra.LicenseToRenewObjectId
          from query.j_abc_renewalapplication ra
         where ra.ObjectId = t_JobId
        union
        select aa.LicenseToAmendObjectId
          from query.j_abc_amendmentapplication aa
         where aa.ObjectId = t_JobId;
      exception
        when no_data_found then
          t_PrevLicenseId := 0;
      end;
      if t_PrevLicenseId > 0 then
        if (t_JobDef in ('j_ABC_RenewalApplication', 'j_ABC_Reinstatement')) or
            (t_JobDef = 'j_ABC_AmendmentApplication' and
            api.pkg_columnquery.Value(
                api.pkg_columnquery.Value(t_JobId,'AmendmentTypeObjectId'),
                'TransferPermits'
            ) = 'Y') then
          -- loop through all r_PermitLicense and r_ABC_PermitGenerationLicense with
          -- previous license and create r_ABC_PermitGenerationLicense with new license
          for p in (
              select pl.PermitObjectId
              from query.r_ABC_PermitGenerationLicense pl
              where pl.LicenseObjectId = t_PrevLicenseId
              ) loop
            -- Determine if a Generational rel between the Primary License and
            -- the Permit already exists
            select count(1)
            into t_PermitCount
            from api.relationships r
            where r.FromObjectId = t_PrimaryLicenseObjectId
              and r.ToObjectId = p.PermitObjectId
              and r.EndpointId = t_GenerationalLicenseEPId;
            -- If there are no related permits, create the Generational Rel
            if t_PermitCount = 0 then
              t_RelId := api.pkg_relationshipupdate.New(t_GenerationalLicenseEPId,
                  t_PrimaryLicenseObjectId, p.Permitobjectid);
            end if;
          end loop;
          for p in (
              select pl.PermitObjectId, pl.RelationshipId
              from query.r_Permitlicense pl
              where pl.LicenseObjectId = t_PrevLicenseId
              ) loop
            api.pkg_relationshipupdate.Remove(p.RelationshipId);
            t_RelId := api.pkg_relationshipupdate.New(t_PermitLicenseEPId,
                  p.permitobjectid, t_PrimaryLicenseObjectId);

            -- Determine if a generational rel between the previous license and
            -- the Permit already exists
            select count(1)
            into t_PermitCount
            from api.relationships r
            where r.FromObjectId = t_PrevLicenseId
              and r.ToObjectId = p.PermitObjectId
              and r.EndpointId = t_GenerationalLicenseEPId;
            -- If there are no related permits, create the Generational Rel
            if t_PermitCount = 0 then
              t_RelId := api.pkg_relationshipupdate.New(t_GenerationalLicenseEPId,
                  t_PrevLicenseId, p.Permitobjectid);
            end if;
          end loop;
        end if;
         if (t_JobDef in ('j_ABC_RenewalApplication', 'j_ABC_Reinstatement')) or
            (t_JobDef = 'j_ABC_AmendmentApplication' and api.pkg_columnquery.Value(api.pkg_columnquery.Value(t_JobId,'AmendmentTypeObjectId'),'TransferCoOpMemberships') = 'Y') then
            --loop through all r_ABC_PermitCoOpLicenses with previous license and create r_ABC_PermitHistorCoOpLicenses with previous license,
            --create r_ABC_PermitCoOpLicenses with new license, and delete from r_ABC_PermitCoOpLicenses with previous license
            for p in (select pl.RelationshipId, pl.PermitObjectId, pl.CoOpMemberEffectiveDate
                        from query.r_ABC_PermitCoOpLicenses pl
                       where pl.CoOpMemberObjectId = t_PrevLicenseId
                     ) loop
               t_RelId := api.pkg_relationshipupdate.New(t_HistoricalCoOpEPId, t_PrevLicenseId, p.Permitobjectid);
               api.pkg_columnupdate.SetValue(t_RelId, 'CoOpMemberEffectiveDate', p.CoOpMemberEffectiveDate);
               t_RelId := api.pkg_relationshipupdate.New(t_CoOpLicenseEPId, t_PrimaryLicenseObjectId, p.Permitobjectid);
               api.pkg_columnupdate.SetValue(t_RelId, 'CoOpMemberEffectiveDate', p.CoOpMemberEffectiveDate);
               api.pkg_RelationshipUpdate.Remove(p.RelationshipId);
            end loop;
         end if;
      end if;
    elsif t_JobDef = 'j_ABC_NewApplication' then
  --Set the Original Issue Date using today's date
       api.pkg_columnupdate.SetValue(t_PrimaryLicenseObjectId, 'OriginalIssueDate', trunc(sysdate));
    end if;
    --now the Secondaries...
    for i in (select r.SecondaryLicenseObjectId
                from query.r_ABC_PrimarySecondaryLicense r
                where r.PrimaryLicenseObjectId = t_PrimaryLicenseObjectId) loop
      t_SecSourceLicenseObjectId := api.pkg_ColumnQuery.NumericValue(i.SecondaryLicenseObjectId, 'SourceObjectId');
      t_SecSourceLicenseState := api.pkg_ColumnQuery.Value(t_SecSourceLicenseObjectId, 'State');

      --first check for mandatories on Secondary Licenses
        if api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'EffectiveDate') is null then
          api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Effective Date for all the Secondary Licenses.');
        end if;

      if api.pkg_ColumnQuery.Value(i.SecondaryLicenseObjectId, 'ExpirationMethod') != 'Never' then
        if api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'CalculatedExpirationDate') is null
          or (api.pkg_ColumnQuery.Value(t_PrimaryLicenseObjectId, 'ExpirationDateOverridden') = 'Y'
              and api.pkg_ColumnQuery.DateValue(t_PrimaryLicenseObjectId, 'ExpirationDate') is null) then
          api.pkg_errors.RaiseError(-20000, 'Before approving the License, you must enter an Expiration Date for all the Secondary Licenses.');
        end if;
      end if;

      --if the License is suspended, we need to make sure the new one remains Suspended
      if t_SecSourceLicenseState = 'Suspended' then
        api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Suspended');
        --find all related Accusation jobs and relate the new License to them
        for a in (select al.RelationshipId, al.AccusationJobId
                    from query.r_ABC_AccusationLicense al
                    where al.LicenseObjectId = t_SecSourceLicenseObjectId) loop

          api.pkg_relationshipupdate.Remove(a.RelationshipId);
          t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndPointIdForName('j_ABC_Accusation', 'License'), a.AccusationJobId, i.SecondaryLicenseObjectId);
        end loop;
      else
        api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'State', 'Active');
      end if;

      api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'IsLatestVersion', 'Y');
  
      if t_JobDef != 'j_ABC_Reinstatement' then
        api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'IssueDate', trunc(sysdate));
        if api.pkg_ColumnQuery.Value(i.SecondaryLicenseObjectId, 'ExpirationDateOverridden') != 'Y' then
          api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'ExpirationDate', api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'CalculatedExpirationDate'));
        end if;
      end if;

      -- Set Original Issue Date
      if t_JobDef = 'j_ABC_NewApplication' then
         api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'OriginalIssueDate', trunc(sysdate));
      else
         api.pkg_ColumnUpdate.SetValue(i.SecondaryLicenseObjectId, 'OriginalIssueDate', api.pkg_ColumnQuery.DateValue(i.SecondaryLicenseObjectId, 'OriginalIssueDate'));
      end if;

      -- DM 2011-12-30 New Application or an amendment with a secondary license with a pending state
      if (t_JobDef = 'j_ABC_NewApplication' or (t_JobDef = 'j_ABC_AmendmentApplication' and t_SecSourceLicenseState = 'Pending')) then
        toolbox.pkg_counter.GenerateCounter(i.SecondaryLicenseObjectId, sysdate, '[9999999]', 'ABC License Number', 'LicenseNumber');
      elsif t_JobDef in ('j_ABC_AmendmentApplication', 'j_ABC_RenewalApplication', 'j_ABC_Reinstatement') then
        --if this is an Amendment or Renewal or Reinstatement, we need to close the source License
        --if the source License was Revoked or Expired, we leave it in that state, otherwise we Close it
        if t_SecSourceLicenseState not in ('Revoked', 'Expired') then
          api.pkg_ColumnUpdate.SetValue(t_SecSourceLicenseObjectId, 'State', 'Closed');
        end if;
        --this License is no longer the latest version
        api.pkg_ColumnUpdate.SetValue(t_SecSourceLicenseObjectId, 'IsLatestVersion', 'N');

        --we also need to close any other Licenses that have been issued through concurrent amendments, renewals, re-instatements
        for js in (select r2.LicenseObjectId
                    from query.r_ABC_MasterLicenseLicense r1
                      join query.r_ABC_MasterLicenseLicense r2
                        on r2.MasterLicenseObjectId = r1.MasterLicenseObjectId
                      join query.o_abc_License l
                        on l.objectid = r2.LicenseObjectId
                    where r1.LicenseObjectId = i.SecondaryLicenseObjectId
                      and r2.LicenseObjectId not in (t_SecSourceLicenseObjectId, i.SecondaryLicenseObjectId)
                      and l.State = 'Active') loop

          api.pkg_ColumnUpdate.SetValue(js.licenseobjectid, 'State', 'Closed');
        end loop;
      end if;
    end loop;
    CopyLicenseeToOnlineUser(t_JobId);

  end ApproveLicense;

  ---------------------------------------------------------------------------------------------
  -- RejectLicense
  --  this is triggered on the following outcomes:
     -- Reject outcome of the Approve/Reject Application process
     -- Reject outcome of the Review Application process
     -- Close outcome of the Monitor Hold process
  --  it will set the License State to "Not Approved"
  --
  --  Change History:
  --  01Sep2011, StanH, Copy Licensee rel to online user for use in future applications.
  ---------------------------------------------------------------------------------------------
  procedure RejectLicense (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_PrimaryLicenseObjectId  number;
    t_JobId                   number := api.pkg_columnquery.NumericValue(a_ObjectId, 'JobId');
  begin
    t_PrimaryLicenseObjectId := api.pkg_columnquery.Value(t_JobId, 'LicenseObjectId');

    api.pkg_columnupdate.SetValue(t_PrimaryLicenseObjectId, 'State', 'Not Approved');

    for i in (select r.SecondaryLicenseObjectId
                from query.r_ABC_PrimarySecondaryLicense r
               where r.PrimaryLicenseObjectId = t_PrimaryLicenseObjectId) loop

      api.pkg_columnupdate.SetValue(i.secondarylicenseobjectid, 'State', 'Not Approved');
    end loop;

    CopyLicenseeToOnlineUser(t_JobId);

  end RejectLicense;

  /*---------------------------------------------------------------------------
   * CopyLicense() -- PUBLIC
   *   This is triggered on "SubmitApplication" boolean column on the Amendment
   * and Renewal jobs
   *-------------------------------------------------------------------------*/
  procedure CopyLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_PrimaryLicenseObjectId            number;
    t_LicenseToCopy                     number;
    t_NewLicense                        number;
    t_NewSecLicense                     number;
    t_RelId                             number;
    t_MasterLicense                     number;
    t_JobId                             udt_id;
    t_JobDef                            varchar2(30);
    t_LicenseToCopyColName              varchar2(30);
    t_NumberColName                     varchar2(30);
    t_MaxColName                        varchar2(30);
    t_VehicleIds                        udt_IdList;
    t_NewVehicleId                      udt_id;
    t_InternalAddressObjectIds          udt_IdList;
    t_NewAddressId                      udt_id;
    t_WarehouseAddressId                udt_id;
    t_LicenseTypeObjectId               udt_id;
    t_EndpointId                        udt_id;
    t_AdditionalWarehouseCount          number :=0;
  begin

    t_JobId := a_ObjectId;
    t_JobDef := api.pkg_columnquery.Value(t_JobId,'ObjectDefName');

    -- Determining if the job is an amendment job or a renewal job, and set variables accordingly
    case
      when t_JobDef = 'j_ABC_AmendmentApplication' then
        t_LicenseToCopyColName := 'LicenseToAmendObjectId';
        t_NumberColName := 'AmendNumber';
        t_MaxColName := 'MaxAmendmentNumber';
      when t_JobDef = 'j_ABC_RenewalApplication' then
        t_LicenseToCopyColName := 'LicenseToRenewObjectId';
        t_NumberColName := 'RenewalNumber';
        t_MaxColName := 'MaxRenewalNumber';
      when t_JobDef = 'j_ABC_Reinstatement' then
        t_LicenseToCopyColName := 'LicenseToReinstateObjectId';
        t_NumberColName := 'ReinstateNumber';
        t_MaxColName := 'MaxReinstatementNumber';
    end case;

    -- Determine the license to copy and create a new license to store the copy
    t_LicenseToCopy := api.pkg_columnquery.Value(a_ObjectId, t_LicenseToCopyColName);
    t_MasterLicense := api.pkg_columnquery.Value(t_LicenseToCopy, 'MasterLicenseObjectId');
    t_NewLicense := api.pkg_objectupdate.New(
        api.pkg_configquery.ObjectDefIdForName('o_ABC_License'));
    api.pkg_columnupdate.SetValue(t_NewLicense, 'DoNotRunPostVerifyProcedures', 'Y');

    -- Relate new Primary License to job
    t_RelId := api.pkg_relationshipupdate.New(
        api.pkg_configquery.EndPointIdForName(t_JobDef, 'License'), a_ObjectId, t_NewLicense);
    if api.pkg_columnquery.Value (t_LicenseToCopy, 'LicenseTypeCode') = '24' then
      t_EndpointId := api.pkg_configquery.EndPointIdForName('o_ABC_License',
          'AddtlWarehouseLicense');
      t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_NewLicense,
          api.pkg_columnquery.NumericValue(t_LicenseToCopy, 'RelatedLicenseObjectId'));
    end if;
    CopyColumns(t_LicenseToCopy, t_NewLicense);
    CopyRels(t_LicenseToCopy, t_NewLicense);
    CloneObject(t_LicenseToCopy, t_NewLicense, 'Condition', 'o_ABC_Condition');
    CloneObject(t_LicenseToCopy, t_NewLicense, 'EventDate', 'o_ABC_EventDate');
    CloneObject(t_LicenseToCopy, t_NewLicense, 'PetitionTerm', 'o_ABC_PetitionTerm');

    -- Determine if this is an Amendment Job for a Seasonal License
    t_LicenseTypeObjectId := api.pkg_columnquery.Value(t_NewLicense, 'LicenseTypeObjectId');
    if t_JobDef = 'j_ABC_AmendmentApplication' and
        api.pkg_ColumnQuery.Value(t_LicenseTypeObjectId, 'ExpirationMethod') = 'Seasonal' then
      -- Copy EffectiveDate from the Original License
      api.Pkg_Columnupdate.SetValue(t_NewLicense, 'EffectiveDate',
          api.Pkg_Columnquery.DateValue(t_LicenseToCopy, 'EffectiveDate'));
    end if;

    -- Determine if this is a Reinstatement Job
    if t_JobDef = 'j_ABC_Reinstatement' then
      api.Pkg_Columnupdate.SetValue(t_NewLicense, 'EffectiveDate',
          api.Pkg_Columnquery.DateValue(t_LicenseToCopy, 'EffectiveDate'));
      api.Pkg_Columnupdate.SetValue(t_NewLicense, 'ExpirationDate',
          api.Pkg_Columnquery.DateValue(t_LicenseToCopy, 'ExpirationDate'));
       api.Pkg_Columnupdate.SetValue(t_NewLicense, 'IssueDate',
          api.Pkg_Columnquery.DateValue(t_LicenseToCopy, 'IssueDate'));
      api.Pkg_Columnupdate.SetValue(t_NewLicense, 'LicenseCertificateDocId_New',
          api.Pkg_Columnquery.NumericValue(t_LicenseToCopy, 'LicenseCertificateDocId_New'));
    end if;

    -- Set Issue Date from Original License
    api.Pkg_Columnupdate.SetValue(t_NewLicense, 'OriginalIssueDate',
        api.Pkg_Columnquery.DateValue(t_LicenseToCopy, 'OriginalIssueDate'));
    -- Set the Amend or Renewal or Reinstatement number
    api.pkg_columnupdate.RemoveValue(t_NewLicense, 'AmendNumber');
    api.pkg_columnupdate.RemoveValue(t_NewLicense, 'RenewalNumber');
    api.pkg_columnupdate.RemoveValue(t_NewLicense, 'ReinstateNumber');
    api.pkg_columnupdate.SetValue(t_NewLicense, t_NumberColName,
        nvl(api.pkg_columnquery.Value(t_MasterLicense, t_MaxColName), 0) + 1);
    -- Set the SourceObjectId
    api.pkg_columnupdate.SetValue(t_NewLicense, 'SourceObjectId', t_LicenseToCopy);

    -- Secondary Licenses
    for i in (
        select r.SecondaryLicenseObjectId
        from query.r_ABC_PrimarySecondaryLicense r
        where r.PrimaryLicenseObjectId = t_LicenseToCopy
        ) loop
      t_MasterLicense := api.pkg_columnquery.Value(i.secondarylicenseobjectid,
          'MasterLicenseObjectId');
      t_NewSecLicense := api.pkg_objectupdate.New(
        api.pkg_configquery.ObjectDefIdForName('o_ABC_License'));

      -- Relate new Secondary License to new Primary
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Secondary'), t_NewLicense,
          t_NewSecLicense);
      CopyColumns(i.secondarylicenseobjectid, t_NewSecLicense);
      CopyRels(i.secondarylicenseobjectid, t_NewSecLicense);
      CloneObject(i.secondarylicenseobjectid, t_NewSecLicense, 'Condition', 'o_ABC_Condition');
      CloneObject(i.secondarylicenseobjectid, t_NewSecLicense, 'EventDate', 'o_ABC_EventDate');

    -- Determine if this is an amendment job for a Seasonal License
      t_LicenseTypeObjectId := api.pkg_columnquery.Value(t_NewSecLicense, 'LicenseTypeObjectId');
      if t_JobDef = 'j_ABC_AmendmentApplication' and
          api.pkg_ColumnQuery.Value(t_LicenseTypeObjectId, 'ExpirationMethod') = 'Seasonal' then
        api.pkg_ColumnUpdate.SetValue(t_NewSecLicense, 'EffectiveDate',
            api.Pkg_Columnquery.DateValue(i.secondarylicenseobjectid, 'EffectiveDate'));
      end if;

    -- Determine if this is a reinstatement job
      if t_JobDef = 'j_ABC_Reinstatement' then
        api.pkg_ColumnUpdate.SetValue(t_NewSecLicense, 'EffectiveDate',
            api.pkg_ColumnQuery.DateValue(i.secondarylicenseobjectid, 'EffectiveDate'));
        api.pkg_ColumnUpdate.SetValue(t_NewSecLicense, 'ExpirationDate',
            api.pkg_ColumnQuery.DateValue(i.secondarylicenseobjectid, 'ExpirationDate'));
        api.pkg_ColumnUpdate.SetValue(t_NewSecLicense, 'IssueDate',
            api.pkg_ColumnQuery.DateValue(i.secondarylicenseobjectid, 'IssueDate'));
        api.pkg_ColumnUpdate.SetValue(t_NewSecLicense, 'LicenseCertificateDocId_New',
            api.pkg_ColumnQuery.NumericValue(i.secondarylicenseobjectid,
            'LicenseCertificateDocId_New'));
      end if;

      -- Set Issue Date from Original License
      api.pkg_ColumnUpdate.SetValue(t_NewSecLicense, 'OriginalIssueDate',
          api.Pkg_ColumnQuery.DateValue(i.secondarylicenseobjectid, 'OriginalIssueDate'));
      -- Set the Amend or Renewal number
      api.pkg_columnupdate.SetValue(t_NewSecLicense, t_NumberColName,
          nvl(api.pkg_columnquery.Value(t_MasterLicense, t_MaxColName), 0) + 1);
      -- Set the SourceObjectId
      api.pkg_columnupdate.SetValue(t_NewSecLicense, 'SourceObjectId',
          i.secondarylicenseobjectid);
    end loop;

   -- Set the License Type specific details from the renewal to the license
    if api.pkg_columnQuery.Value(t_JobId,'EnteredOnline') = 'Y'
        and t_JobDef = 'j_ABC_RenewalApplication' then
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'RestrictedBreweryProduction',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineRstrctdBreweryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'LimitedBreweryProduction',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLimitedBreweryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'SupplementaryLimitedDistillery',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineSupplementLmtdDistillery'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'PlenaryWineryProduction',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePlenaryWineryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'PlenaryWineryWholesalePrivileg',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePlenaryWineryWholesalePr'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'FarmWineryProduction',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineFarmWineryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'FarmWineryWholesalePrivilege',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineFarmWineryWholesalePriv'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'OutOfStateWineryProduction',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineOutStateWineryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'OutOfStateWineryWholesalePrivi',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineOutOfStateWineryWholesal'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'RetailTransitType',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineRetailTransitType'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'SportingFacilityCapacity',
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineSportingFacilityCapacity'));
    else
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'RestrictedBreweryProduction',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'RestrictedBreweryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'LimitedBreweryProduction',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'LimitedBreweryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'SupplementaryLimitedDistillery',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'SupplementaryLimitedDistillery'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'PlenaryWineryProduction',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'PlenaryWineryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'PlenaryWineryWholesalePrivileg',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'PlenaryWineryWholesalePrivileg'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'FarmWineryProduction',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'FarmWineryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'FarmWineryWholesalePrivilege',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'FarmWineryWholesalePrivilege'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'OutOfStateWineryProduction',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'OutOfStateWineryProduction'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'OutOfStateWineryWholesalePrivi',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'OutOfStateWineryWholesalePrivi'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'RetailTransitType',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'RetailTransitType'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'SportingFacilityCapacity',
          api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'SportingFacilityCapacity'));
    end if;

    -- Copy the Warehouse details
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'WarehouseName',
        api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'WarehouseName'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'WarehousePhoneNumber',
        api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'WarehousePhoneNumber'));
    begin
      select WarehouseAddressId
      into t_WarehouseAddressId
      from query.r_ABC_LicenseWarehouseAddress
      where LicenseId = t_LicenseToCopy;
    exception
      when no_data_found then
        t_WarehouseAddressId := null;
    end;
    if t_WarehouseAddressId is not null then
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('o_ABC_License', 'WarehouseAddress'),
          t_NewLicense, t_WarehouseAddressId);
    end if;

    -- Set License Type specific details from the question response for renew
    for i in (
        select
          qr.QuestionId,
          qa.ResponseId
        from
          query.r_ABC_RenewalQAResponse qa
          join query.r_ABC_QuestionResponse qr
              on qa.ResponseId = qr.ResponseId
        where qa.RenewalId = t_JobId
        ) loop
      if api.pkg_columnquery.value(i.Questionid, 'ConflictOfInterest') = 'Y' then
        api.pkg_columnupdate.SetValue(t_NewLicense, 'ConflictOfInterest',
            api.pkg_ColumnQuery.Value(i.responseid, 'AnswerOnJob'));
      end if;
      if api.pkg_columnquery.value(i.questionid, 'CriminalConviction') = 'Y' then
        api.pkg_columnupdate.SetValue(t_NewLicense, 'CriminalConviction',
            api.pkg_ColumnQuery.Value(i.responseid, 'AnswerOnJob'));
      end if;
    end loop;

    -- Set License Type specific details from the question response for amend
    for i in (
        select
          qr.QuestionId,
          qa.ResponseId
        from
          query.r_ABC_AmendmentQAResponse qa
          join query.r_ABC_QuestionResponse qr
              on qa.ResponseId = qr.ResponseId
        where qa.AmendmentJobId = t_JobId
        ) loop
      if api.pkg_columnquery.value(i.Questionid, 'ConflictOfInterest') = 'Y' then
        api.pkg_columnupdate.SetValue(t_NewLicense, 'ConflictOfInterest',
            api.pkg_ColumnQuery.Value(i.responseid, 'AnswerOnJob'));
      end if;
    end loop;

    -- Determine if the license has RetailTransitType of Limousine or Boat
    if api.pkg_ColumnQuery.Value(t_LicenseToCopy, 'RetailTransitType') in ('Limousine', 'Boat')
        then
      -- Determine if the job is Renewal Job submitted from the Public Site
      if t_JobDef = 'j_ABC_RenewalApplication' and
          api.pkg_columnQuery.Value(t_JobId,'EnteredOnline') = 'Y' then
        -- Copy the vehicles from the job and not the license
        api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'VerifyLicense', 'N');

        select
          rv.vehicleid,
          api.pkg_columnquery.Value(rv.VehicleId,'InternalStorageAddressObjectId')
        bulk collect into t_VehicleIds, t_InternalAddressObjectIds
        from query.r_ABC_RenewalVehicle rv
        where rv.RenewalApplicationId = t_JobId;

        for v in 1..t_VehicleIds.count loop

          extension.pkg_objectupdate.CopyObject(t_VehicleIds(v), t_NewVehicleId);
          t_RelId := api.pkg_relationshipupdate.New(
              api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Vehicle'), t_NewLicense,
              t_NewVehicleId);

          if t_InternalAddressObjectIds(v) is not null then
            t_RelId := api.pkg_relationshipupdate.New(
                api.pkg_configquery.EndPointIdForName('o_ABC_Vehicle', 'Address'), t_NewVehicleId,
                t_InternalAddressObjectIds(v));
          end if;
          api.pkg_ColumnUpdate.SetValue(t_NewVehicleId, 'VehicleType',
              api.pkg_ColumnQuery.Value(t_JobId, 'OnlineRetailTransitType'));
          api.pkg_columnupdate.SetValue(t_NewVehicleId, 'EnteredOnline',
              api.pkg_columnquery.Value(t_JobId,'EnteredOnline'));
        end loop;
      -- Otherwise, copy Vehicles from the license
      else
        select
          lv.vehicleid,
          api.pkg_columnquery.Value(lv.VehicleId,'InternalStorageAddressObjectId')
        bulk collect into t_VehicleIds, t_InternalAddressObjectIds
        from query.r_abc_licensevehicle lv
        where lv.LicenseId = t_LicenseToCopy;

        for v in 1..t_VehicleIds.count loop
          extension.pkg_objectupdate.CopyObject(t_VehicleIds(v), t_NewVehicleId);
          t_RelId := api.pkg_relationshipupdate.New(
              api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Vehicle'), t_NewLicense,
              t_NewVehicleId);

          if t_InternalAddressObjectIds(v) is not null then
            t_RelId := api.pkg_relationshipupdate.New(
                api.pkg_configquery.EndPointIdForName('o_ABC_Vehicle', 'Address'), t_NewVehicleId,
                t_InternalAddressObjectIds(v));
          end if;
          api.pkg_ColumnUpdate.SetValue(t_NewVehicleId, 'VehicleType',
              api.pkg_ColumnQuery.Value(t_NewLicense, 'RetailTransitType'));
          api.pkg_columnupdate.SetValue(t_NewVehicleId,'EnteredOnline',
              api.pkg_columnquery.Value(t_JobId,'EnteredOnline'));
        end loop;
      end if;
    end if;

    -- If the job is an Amendment, copy over the Batch Renewal Notification rels to the new License
    if t_JobDef = 'j_ABC_AmendmentApplication' then
      for br in (
          select r.BatchRenewalNotificationJobId ObjectId
          from query.r_ABC_BatchRenewalJobLicGrcPd r
          where r.LicenseObjectId = t_LicenseToCopy
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification',
            'LicenseGrcPd'), br.ObjectId, t_NewLicense);
      end loop;
      for br in (
          select r.BatchRenewalNotificationJobId ObjectId
          from query.r_ABC_BatchRenewalJobLicense r
          where r.LicenseObjectId = t_LicenseToCopy
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndPointIdForName('j_ABC_BatchRenewalNotification', 'License'),
            br.ObjectId, t_NewLicense);
      end loop;
    end if;
    api.pkg_columnupdate.SetValue(t_NewLicense, 'DoNotRunPostVerifyProcedures', 'N');

  end CopyLicense;

  /*---------------------------------------------------------------------------
   * OnlineSubmissionActivities() -- PUBLIC
   *   This is triggered on completion of New Application from Public site.
   * Create a new License object from temporary data details on the job, set
   * default values and copy documents to new rel on license specifically for
   * online objects. Call this procedure before other app completion procedures,
   * so that secondary licenses can be created as normal
   *-------------------------------------------------------------------------*/
  procedure OnlineSubmissionActivities (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobDef                            varchar2(30)
        := api.pkg_columnquery.Value(a_JobId, 'ObjectDefName');
    t_NewLicense                        udt_id;
    t_NewSecLicense                     udt_id;
    t_LicenseeObjectId                  udt_id;
    t_ProcessId                         udt_id;
    t_RelId                             number;
    t_NewVehicleId                      udt_id;
    t_NewAddressId                      udt_id;
    t_CountyObjectId                    udt_id;
    t_MunicipalityObjectId              udt_id;
    t_CountyEndPointId                  udt_id
        := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Region');
    t_MunicipalityEndPointId            udt_id
        := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Office');
    t_MuniRelId                         udt_id;
    t_CountyRelId                       udt_id;
    t_EndpointId                        udt_id;
  begin

    -- Set up New License
    if api.pkg_columnquery.Value(a_JobId, 'ApplicationReceivedDate') is null then
      api.pkg_ColumnUpdate.SetValue(a_JobId, 'ApplicationReceivedDate', sysdate);
    end if;
    api.pkg_ColumnUpdate.RemoveValue(a_JobId, 'DraftPaneName');
    t_NewLicense := api.pkg_objectupdate.New(
        api.pkg_configquery.ObjectDefIdForName('o_ABC_License'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'DoNotRunPostVerifyProcedures', 'Y');
    extension.pkg_relationshipupdate.New(a_JobId, t_NewLicense, 'License');

    -- Copy data from job
    if api.pkg_columnquery.Value (a_JobId, 'OnlineLicenseTypeCode') != '24' then
      t_LicenseeObjectId := api.pkg_ColumnQuery.NumericValue(a_JobId,
          'OnlineUseLegalEntityObjectId');
      if t_LicenseeObjectId is not null then
        extension.pkg_RelationshipUpdate.New(t_NewLicense, t_LicenseeObjectId, 'Licensee');
      end if;
    end if;
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'EventContacts',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineEVTContactInformation'));

    for i in (
        select
          r.ToObjectId,
          rd.ToEndPointName
        from
          api.relationships r
          join api.relationshipdefs rd
              on rd.RelationshipDefId = r.RelationshipDefId
        where r.FromObjectId = a_JobId
          -- All endpoint names must begin with 'OL' and match counterpart on License
          and rd.ToEndPointName in (
              'OLRegion',
              'OLLicenseType',
              'OLEventType',
              'OLSelectSecondaryLicType'
          )
        ) loop
      extension.pkg_RelationshipUpdate.New(t_NewLicense, i.ToObjectId,
          substr(i.ToEndpointName, 3));
    end loop;

    -- OLDocument rels do not map to Document rels on the license
    for i in (
        select
          r.ToObjectId,
          rd.ToEndPointName
        from
          api.relationships r
          join api.relationshipdefs rd
              on rd.RelationshipDefId = r.RelationshipDefId
        where r.FromObjectId = a_JobId
          and rd.ToEndPointName in ('OLDocument')
        ) loop
      extension.pkg_RelationshipUpdate.New(t_NewLicense, i.ToObjectId, 'OLDocument');
    end loop;

    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'DoNotRunPostVerifyProcedures', 'N');

    if api.pkg_columnquery.Value (a_JobId, 'OnlineLicenseTypeCode') = '24' then
      t_EndpointId := api.pkg_configquery.EndPointIdForName('o_ABC_License',
          'AddtlWarehouseLicense');
      t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_NewLicense,
          api.pkg_columnquery.NumericValue(a_JobId, 'OnlineRelatedLicenseObjectId'));
    end if;

   -- Set the License Type specific details from the application to the license
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'RestrictedBreweryProduction',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineRstrctdBreweryProduction'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'LimitedBreweryProduction',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineLimitedBreweryProduction'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'SupplementaryLimitedDistillery',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineSupplementLmtdDistillery'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'PlenaryWineryProduction',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePlenaryWineryProduction'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'PlenaryWineryWholesalePrivileg',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlinePlenaryWineryWholesalePr'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'FarmWineryProduction',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineFarmWineryProduction'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'FarmWineryWholesalePrivilege',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineFarmWineryWholesalePriv'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'OutOfStateWineryProduction',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineOutStateWineryProduction'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'OutOfStateWineryWholesalePrivi',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineOutOfStateWineryWholesal'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'RetailTransitType',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineRetailTransitType'));
    api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'SportingFacilityCapacity',
        api.pkg_ColumnQuery.Value(a_JobId, 'OnlineSportingFacilityCapacity'));

    -- Determine whether warehouse data entry is allowed
    if api.pkg_ColumnQuery.Value(a_JobId, 'EnableWarehouseAddressEntry') = 'Y' then
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'WarehouseName',
          api.pkg_ColumnQuery.Value(a_JobId, 'OnlineWarehouseName'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'WarehousePhoneNumber',
          api.pkg_ColumnQuery.Value(a_JobId, 'OnlineWarehousePhoneNumber'));
      api.pkg_ColumnUpdate.SetValue(t_NewLicense, 'EnableWarehouseDataEntryStored',
          api.pkg_ColumnQuery.Value(a_JobId, 'EnableWarehouseAddressEntry'));
    end if;

   -- Determine if the license has RetailTransitType of Limousine or Boat
    if api.pkg_ColumnQuery.Value(a_JobId, 'OnlineRetailTransitType') in ('Limousine', 'Boat') then
      -- Copy the Vehicle Object, then relate the new Vehicle to the new license
      for v in (
          select r.VehicleId
          from
            query.r_ABC_ApplicationVehicle r
            join query.o_ABC_Vehicle av
                on av.ObjectId = r.VehicleId
          where r.NewApplicationId = a_JobId
          ) loop
        extension.pkg_objectupdate.CopyObject(v.Vehicleid, t_NewVehicleId);
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Vehicle'), t_NewLicense,
            t_NewVehicleId);
        api.pkg_ColumnUpdate.SetValue(t_NewVehicleId, 'VehicleType',
            api.pkg_ColumnQuery.Value(a_JobId, 'OnlineRetailTransitType'));
        api.pkg_columnupdate.SetValue(t_NewVehicleId, 'EnteredOnline',
            api.pkg_columnquery.Value(a_JobId,'EnteredOnline'));
      end loop;
    end if;

    -- Determine the issuing authority of the license type attached to the job
    if api.pkg_columnquery.Value(a_JobId, 'OnlineLicenseIssuingAuthority') = 'Municipality' then
      t_CountyObjectId := api.pkg_columnquery.NumericValue(a_JobId, 'CountyObjectId');
      t_MunicipalityObjectId := api.pkg_columnquery.NumericValue(a_JobId, 'MunicipalityObjectId');
      t_CountyRelId := api.pkg_relationshipupdate.New(t_CountyEndPointId, t_NewLicense,
          t_CountyObjectId);
      t_MuniRelId := api.pkg_relationshipupdate.New(t_MunicipalityEndPointId, t_NewLicense,
          t_MunicipalityObjectId);
    -- Issuing authority is 'state', so look up the wholesale default county and municipality
    -- and attach to license.
    else
      select r.objectid
      into t_CountyObjectId
      from query.o_abc_region r
      where r.WholesaleDefault = 'Y';
      if t_CountyObjectId is not null then
        t_CountyRelId := api.pkg_relationshipupdate.New(t_CountyEndPointId, t_NewLicense,
            t_CountyObjectId);
      end if;

      select o.ObjectId
      into t_MunicipalityObjectId
      from query.o_abc_office o
      where o.WholesaleDefault = 'Y';
      if t_MunicipalityObjectId is not null then
        t_MuniRelId := api.pkg_relationshipupdate.New(t_MunicipalityEndPointId, t_NewLicense,
            t_MunicipalityObjectId);
      end if;
    end if;

  end OnlineSubmissionActivities;

  /*---------------------------------------------------------------------------
   * InternalSubmitActivities() -- PUBLIC
   *   This is triggered on completion of New Application from the internal
   * Site and sets details on the license.
   *-------------------------------------------------------------------------*/
  procedure InternalSubmitActivities (
    a_JobId                             udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobDefId                          udt_Id;
    t_LicenseEndpointId                 udt_id;
    t_LicenseIds                        udt_IdList;
    t_VehicleEndPointId                 udt_Id;
    t_VehicleIds                        udt_IdList;
  begin

    t_JobDefId := api.pkg_ColumnQuery.Value(a_JobId, 'ObjectDefId');
    t_LicenseEndpointId := api.pkg_ConfigQuery.EndPointIdForName(t_JobDefId, 'License');
    t_VehicleEndPointId := api.pkg_ConfigQuery.EndpointIdForName('o_ABC_License', 'Vehicle');

    select l.ObjectId LicenseId
    bulk collect into t_LicenseIds
    from
      query.o_ABC_License l
      join api.relationships r
          on l.ObjectId = r.ToObjectId
    where r.EndpointId = t_LicenseEndpointId
      and r.FromObjectId = a_JobId;

    -- Find the License related to the Job
    for i in 1..t_LicenseIds.count() loop
      -- Update the EnableWarehouseDataEntryStored detail to enable Warehouse details from now on.
      api.pkg_ColumnUpdate.SetValue(t_LicenseIds(i), 'EnableWarehouseDataEntryStored',
          api.pkg_ColumnQuery.Value(t_LicenseIds(i), 'EnableWarehouseDataEntry'));

      -- Get all vehicles related to this license
      select v.ObjectId
      bulk collect into t_VehicleIds
      from
        query.o_ABC_Vehicle v
        join api.relationships r
            on v.ObjectId = r.ToObjectId
      where r.EndpointId = t_VehicleEndpointId
        and r.FromObjectId = t_LicenseIds(i);

      -- Loop through vehicles and set VehicleType detail
      for j in 1..t_VehicleIds.count() loop
        api.pkg_ColumnUpdate.SetValue(t_VehicleIds(j), 'VehicleType',
            api.pkg_ColumnQuery.Value(t_LicenseIds(i), 'RetailTransitType'));
      end loop;
    end loop;

  end InternalSubmitActivities;

  -----------------------------------------------------------------------------
  -- Enforce Resolution Document
  -----------------------------------------------------------------------------
 procedure EnforceResolutionDocument (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
  t_RequiresResolution    varchar2(1);
  t_JobId                 udt_Id := api.pkg_columnquery.Value(a_ObjectId, 'jobid');
  t_JobDef                varchar2(30);
  begin

    t_JobDef := api.pkg_columnquery.value(t_JobId, 'ObjectDefName');

    if t_jobdef = 'j_ABC_AmendmentApplication' then

      select api.pkg_columnquery.Value(alt.AmendmentTypeId, 'RequiresResolution')
      into t_RequiresResolution
      from query.r_abc_amendjoblicenseamendtype alt
      where alt.AmendJobLicenseId = t_JobId;

      if api.pkg_columnquery.value(a_ObjectId, 'EnforceDocumentUpload') is null
        and api.pkg_columnquery.value(a_ObjectId, 'Outcome') not in ('Cancel','Awaiting Relief')
        and t_RequiresResolution = 'Y' then
        api.pkg_errors.raiseerror(-20000, 'Please upload resolution prior to submitting to A.B.C.');
      end if;

    else

        if api.pkg_columnquery.value(a_ObjectId, 'EnforceDocumentUpload') is null
          and api.pkg_columnquery.value(a_ObjectId, 'Outcome') not in ('Cancel','Awaiting Relief') then
          api.pkg_errors.raiseerror(-20000, 'Please upload resolution prior to submitting to A.B.C.');
        end if;

    end if;

  end EnforceResolutionDocument;

  /*---------------------------------------------------------------------------
   * CancelJob() -- PUBLIC
   *   Run on post verify of most job types when the "Cancel Job" detail
   * is checked.
   *-------------------------------------------------------------------------*/
  procedure CancelJob (
    a_ObjectId                                udt_id,
    a_AsOfDate                                date
  ) is
    t_ProcessTypeId                           udt_id
        := api.pkg_ConfigQuery.ObjectDefIdForName('p_ChangeStatus');
    t_NewProcessId                            udt_id;
    t_UserId                                  udt_id;
    t_FeesPaidCount                           number;
    t_FeeToEcomEP                             udt_id
        := api.pkg_ConfigQuery.EndPointIdForName('o_Fee', 'eComEndPoint');

  begin
    -- Check if the user should be allowed to cancel the job
    t_UserId := api.pkg_SecurityQuery.EffectiveUserId();
    if api.pkg_ColumnQuery.value(t_UserId, 'UserType') = 'Public' then
      if api.pkg_ColumnQuery.value(a_ObjectId, 'StatusName') != 'NEW' then
        api.pkg_Errors.RaiseError(-20001,'You cannot cancel an Application ' ||
            'that has already been Submitted. Please return to the Main Menu.');
      end if;
      if api.pkg_ColumnQuery.value(a_ObjectId, 'AddToPayAllFees') = 'Y' then
        api.pkg_Errors.RaiseError(-20001,'You cannot cancel an Application ' ||
            'that has been Added to Pay All Fees.');
      end if;
      -- Check if fees have already been paid for this job
      select count(1)
        into t_FeesPaidCount
        from api.fees f
       where f.jobid = a_ObjectId
         and f.PaidAmount < 0;
      if t_FeesPaidCount > 0 then
        api.pkg_Errors.RaiseError(-20001, 'You cannot cancel an application that has a Payment.' ||
            ' Please contact NJ ABC for further assistance.');
      end if;
      -- Check if there are any pending payments for this job
      for i in (
          select api.pkg_ColumnQuery.Value(r.ToObjectId, 'StatusName') Status
            from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_Fee', 'JobId', a_ObjectId)) f
            join api.relationships r
                on r.FromObjectId = f.objectid
                and r.EndPointId = t_FeeToEcomEP
          ) loop
        if i.Status = 'NEW' then
          api.pkg_Errors.RaiseError(-20001, 'You cannot cancel an Application ' ||
              'that has a Pending Payment. Please contact NJ ABC for further assistance');
        end if;
      end loop;
    end if;

    -- Perform the cancellation
    t_NewProcessId := api.pkg_ProcessUpdate.New(a_ObjectId, t_ProcessTypeId,
        'Cancel Job', sysdate, null, null);
    api.pkg_columnupdate.SetValue(t_NewProcessId, 'ReasonForChange',
        'Job cancelled internally by ' || api.pkg_securityquery.EffectiveUser);
    api.pkg_ProcessUpdate.Complete(t_NewProcessId, 'Cancelled');

    -- If we're cancelling the job, be sure to unassign any processes
    --  (otherwise they will remain on someone's To Do List forever)
    for i in (select pa.ProcessId, pa.AssignedTo
                from api.incompleteprocessassignments pa
                join api.processes p on p.ProcessId = pa.ProcessId
               where p.JobId = a_ObjectId) loop
      api.pkg_processupdate.Unassign(i.ProcessId, i.AssignedTo);
    end loop;

  end CancelJob;

  /*---------------------------------------------------------------------------
   * CopyNewApplicationQuestions() -- PUBLIC
   *   For each application question on the License Type admin object, create
   * a Response object to display on the application job. Also copy any applicable
   * documents.
   *-------------------------------------------------------------------------*/
  procedure CopyNewApplicationQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_LicenseTypeId                     number :=
        api.pkg_columnquery.NumericValue(a_ObjectId, 'OnlineLicenseTypeObjectId');
    t_NewResponse                       number;
    t_RelId                             number;
    t_ResponseDefId                     number(9)
        := api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
    t_DocTypeId                         number;
    t_QARel                             udt_IdList;

  begin

    -- Grab the rel id of any questions on the job
    select qa.relationshipid
    bulk collect into t_QARel
    from query.r_ABC_NewAppQAResponse qa
    where qa.NewApplicationId = a_ObjectId;

    -- If no questions are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
      for i in (select r.QuestionId
                from query.r_abc_lictypeappquestion r
                where r.LicenseTypeId = t_LicenseTypeId
               ) loop
       -- Create Response
        t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (select
                    api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                    name columnname
                  from api.columndefs cd
                  where cd.objectdefid = t_ResponseDefId
                    and name in ('SortOrder', 'Question','AcceptableAnswer',
                        'NonacceptableText','NonacceptableResponseType')
                 ) loop
          api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
        end loop;

        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
            'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
          t_RelId := api.pkg_RelationshipUpdate.New(
              api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse',
              'DocumentType'), t_NewResponse, t_DocTypeId);
        end if;
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
            t_NewResponse, i.questionid);
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'NewApplication'),
            t_NewResponse, a_ObjectId);
      end loop;

      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_NewAppQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_NewAppQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.NewApplicationId = a_ObjectId
               and pr.NewApplicationId = a_ObjectId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(g_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;

    -- Copy any documents for specific License Type to the Application job
    select r.RelationshipId
    bulk collect into t_QARel
    from query.r_ABC_NewAppDocumentType r
    where r.NewAppId = a_ObjectId;

    -- If no documents are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
      for d in (select
                  r.DocumentTypeObjectId,
                  r.Mandatory
                from query.r_OLLicenseTypeDocumentType r
                where r.LicenseTypeObjectId = t_LicenseTypeId
                  and r.NewApplication = 'Y'
               ) loop
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('j_ABC_NewApplication',
            'OnlineDocumentType'), a_ObjectId, d.documenttypeobjectid);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      end loop;
    end if;

  end CopyNewApplicationQuestions;

  /*---------------------------------------------------------------------------
   * CopyPRApplicationQuestions() -- PUBLIC
   *   Get the Questions and DocumentTypes for a Product Application
   *-------------------------------------------------------------------------*/
  procedure CopyPRApplicationQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_DocTypeId                         number;
    t_JobTypeID                         udt_Id;
    t_NewResponse                       number;
    t_QARel                             udt_IdList;
    t_RelId                             number;
    t_ResponseDefId                     number(9);
  begin
    t_JobTypeId:= api.pkg_SimpleSearch.ObjectByIndex('o_JobTypes', 'Name', 'j_ABC_PRApplication');
    t_ResponseDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');

    -- Grab the rel id of any questions on the job
    select qa.relationshipid
    bulk collect into t_QARel
    from query.r_ABC_PRApplicationQAResponse qa
    where qa.PRApplicationId = a_ObjectId;

    -- If no questions are currently on the job then generate them from the list on the admin site
    if t_QARel.count = 0 then
      for i in (
          select r.QuestionId
          from query.r_ABC_JobTypeQuestion r
          where r.JobTypeID = t_JobTypeId
          ) loop
        -- Create Response
        t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (
            select
              api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
              name columnname
            from api.columndefs cd
            where cd.objectdefid = t_ResponseDefId
              and name in ('SortOrder', 'Question','AcceptableAnswer','NonacceptableText',
                'NonacceptableResponseType')
            ) loop
          api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
        end loop;

        -- Relate any required Documents
        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
          t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndpointIdForName(
              'o_ABC_QAResponse','DocumentType'), t_NewResponse, t_DocTypeId);
        end if;
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndpointIdForName(
            'o_ABC_QAResponse', 'Question'), t_NewResponse, i.questionid);
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndpointIdForName(
            'o_ABC_QAResponse', 'PRApplication'), t_NewResponse, a_ObjectId);
      end loop;

      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_PRApplicationQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_PRApplicationQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.PRApplicationId = a_ObjectId
               and pr.PRApplicationId = a_ObjectId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(g_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;

    -- Copy any documents for a specific Job Type
    select r.RelationshipId
    bulk collect into t_QARel
    from query.r_ABC_OLPRAppDocumentType r
    where r.PRApplicationId = a_ObjectId;

    -- If no documents exist generate them from the admin site
    if t_QARel.count() = 0 then
      for j in (
          select
            r.DocumentTypeObjectId,
            r.Mandatory
          from query.r_JobTypeDocumentType r
          where r.JobTypeObjectId = t_JobTypeId
            and r.DocumentTypeObjectId not in (
                select r1.DocumentTypeId
                from query.r_ABC_OLPRAppDocumentType r1
                where r1.PRApplicationId = a_ObjectID)
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_configquery.EndpointIdForName(
          'j_ABC_PRApplication','OnlineDocumentType'), a_objectid, j.DocumentTypeObjectId);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', j.Mandatory);
      end loop;
    end if;
  end CopyPRApplicationQuestions;

  /*---------------------------------------------------------------------------
   * CopyPRRenewalQuestions() -- PUBLIC
   *   Get the Questions and DocumentTypes for a Product Renewal
   *-------------------------------------------------------------------------*/
  procedure CopyPRRenewalQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_DocTypeId                         number;
    t_JobTypeID                         udt_Id;
    t_NewResponse                       number;
    t_QARel                             udt_IdList;
    t_RelId                             number;
    t_ResponseDefId                     number(9);
  begin
    t_JobTypeId:= api.pkg_SimpleSearch.ObjectByIndex('o_JobTypes', 'Name', 'j_ABC_PRRenewal');
    t_ResponseDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');

    -- Grab the rel id of any questions on the job
    select qa.relationshipid
    bulk collect into t_QARel
    from query.r_ABC_PRRenewalQAResponse qa
    where qa.PRRenewalId = a_ObjectId;

    -- If no questions are currently on the job then generate them from the list on the admin site
    if t_QARel.count = 0 then
      for i in (
          select r.QuestionId
          from query.r_ABC_JobTypeQuestion r
          where r.JobTypeID = t_JobTypeId
          ) loop
        -- Create Response
        t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (
            select
              api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
              name columnname
            from api.columndefs cd
            where cd.objectdefid = t_ResponseDefId
              and name in ('SortOrder', 'Question','AcceptableAnswer','NonacceptableText',
                'NonacceptableResponseType')
            ) loop
          api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
        end loop;

        -- Relate any required Documents
        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
          t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndpointIdForName(
              'o_ABC_QAResponse','DocumentType'), t_NewResponse, t_DocTypeId);
        end if;
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndpointIdForName(
            'o_ABC_QAResponse', 'Question'), t_NewResponse, i.questionid);
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_ConfigQuery.EndpointIdForName(
            'o_ABC_QAResponse', 'PRRenewal'), t_NewResponse, a_ObjectId);
      end loop;

      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_PRRenewalQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_PRRenewalQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.PRRenewalId = a_ObjectId
               and pr.PRRenewalId = a_ObjectId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(g_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;

    -- Copy any documents for a specific Job Type
    select r.RelationshipId
    bulk collect into t_QARel
    from query.r_ABC_PRRenewalDocumentType r
    where r.PRRenewalId = a_ObjectId;

    -- If no documents exist generate them from the admin site
    if t_QARel.count() = 0 then
      for j in (
          select
            r.DocumentTypeObjectId,
            r.Mandatory
          from query.r_JobTypeDocumentType r
          where r.JobTypeObjectId = t_JobTypeId
            and r.DocumentTypeObjectId not in (
                select r1.DocumentTypeId
                from query.r_ABC_PRRenewalDocumentType r1
                where r1.PRRenewalId = a_ObjectID)
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(api.pkg_configquery.EndpointIdForName(
          'j_ABC_PRRenewal','OnlineDocumentType'), a_objectid, j.DocumentTypeObjectId);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', j.Mandatory);
      end loop;
    end if;
  end CopyPRRenewalQuestions;
  
  /*---------------------------------------------------------------------------
   * CopyPoliceReviewQuestions() -- PUBLIC
   *   For each permit application question on the Permit Type admin object,
   * create a Response object to display on the application job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPoliceReviewQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_PermitTypeId                     number :=
        api.pkg_columnquery.NumericValue(a_ObjectId, 'PermitTypeId');
    t_NewResponse                       number;
    t_RelId                             number;
    t_ResponseDefId                     number(9) :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
    t_DocTypeId                         number;
    t_QARel                             udt_IdList;
    t_ParentResponseEPId                udt_Id :=
        api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'ParentResponse');
    t_JobType                           varchar2(4000) :=
        api.pkg_columnquery.Value(a_ObjectId, 'JobName');
  begin

    for i in (select r.QuestionId
              from query.r_ABC_PermitTypePoliceQuestion r
              where r.PermitTypeId = t_PermitTypeId
              and ((t_JobType = 'j_ABC_PermitApplication' and r.Application = 'Y')
              or (t_JobType = 'j_ABC_PermitRenewal' and r.Renewal = 'Y'))
             ) loop
      -- Create Response
      t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
      for k in (select
                  api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                  name columnname
                from api.columndefs cd
                where cd.objectdefid = t_ResponseDefId
                  and name in ('SortOrder', 'Question','AcceptableAnswer',
                      'NonacceptableText','NonacceptableResponseType')
               ) loop
        api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
      end loop;

      t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
          'DocumentTypeObjectId'), 0);
      if t_DocTypeId > 0 then
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
            t_NewResponse, t_DocTypeId);
      end if;
      t_RelId := api.pkg_RelationshipUpdate.New(
          api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
          t_NewResponse, i.questionid);
      t_RelId := api.pkg_RelationshipUpdate.New(
          api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'PoliceReview'),
          t_NewResponse, a_ObjectId);
    end loop;
      
    -- Relate each response to its Parent response
    for i in (
        select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
            from query.r_ABC_PoliceReviewQAResponse r
            join query.r_ABC_QuestionResponse qr
                on qr.ResponseId = r.ResponseId
            join query.r_ABC_QuestionResponse pq
                on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
            join query.r_ABC_PoliceReviewQAResponse pr
                on pr.ResponseId = pq.ResponseId
           where r.PoliceReviewId = a_ObjectId
             and pr.PoliceReviewId = a_ObjectId
        ) loop
      t_RelId := api.pkg_RelationshipUpdate.New(t_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
    end loop;
    
    -- Generate Documents
    for d in (select
                r.DocumentTypeObjectId,
                (case 
                   when t_JobType = 'j_ABC_PermitApplication' and r.PoliceAppMandatory = 'Y' then 'Y'
                   when t_JobType = 'j_ABC_PermitRenewal' and r.PoliceRenewMandatory = 'Y' then 'Y'
                   else 'N' 
                end) Mandatory,
                r.SpecialConditionsMandatory
              from query.r_ABC_PermitTypeDocumentType r
              where r.PermitTypeObjectId = t_PermitTypeId
                and (r.PoliceApp = 'Y'
                or r.PoliceRenew = 'Y')
                and r.DocumentTypeObjectId not in
                    (select r1.DocumentTypeId
                     from query.r_ABC_PoliceReviewRespDocType r1
                     where r1.PoliceReviewId = a_ObjectId)
             ) loop
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('p_ABC_PoliceReview',
          'ResponseDocumentType'), a_ObjectId, d.documenttypeobjectid);
      api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      api.pkg_columnupdate.SetValue(t_RelId, 'SpecialConditionsMandatory', d.SpecialConditionsMandatory);
    end loop;

  end CopyPoliceReviewQuestions;
  
  /*---------------------------------------------------------------------------
   * CopySubmitResponseQuestions() -- PUBLIC
   *   This generates questions on submit resolution constructor
   * This will fail if run twice or on a process that has questions.
   *-------------------------------------------------------------------------*/
  procedure CopySubmitResolutionQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_LicenseTypeId                     number :=
        api.pkg_columnquery.NumericValue(a_ObjectId, 'LicenseTypeId');
    t_NewResponse                       number;
    t_RelId                             number;
    t_ResponseDefId                     number(9) :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
    t_DocTypeId                         number;
    t_QARel                             udt_IdList;
    t_ParentResponseEPId                udt_Id :=
        api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'ParentResponse');
    t_JobType                           varchar2(4000) :=
        api.pkg_columnquery.Value(a_ObjectId, 'JobName');
  begin
    
    for i in (select r.QuestionId
              from query.r_ABC_LicTypeMuniQuestion r
              where r.LicenseTypeId = t_LicenseTypeId
              and ((t_JobType = 'j_ABC_NewApplication' and Application = 'Y')
               or (t_JobType = 'j_ABC_AmendmentApplication' and Amendment = 'Y')
               or (t_JobType = 'j_ABC_RenewalApplication' and Renewal = 'Y'))
             ) loop
      -- Create Response
      t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
      for k in (select
                  api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                  name columnname
                from api.columndefs cd
                where cd.objectdefid = t_ResponseDefId
                  and name in ('SortOrder', 'Question','AcceptableAnswer',
                      'NonacceptableText','NonacceptableResponseType')
               ) loop
        api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
      end loop;

      t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
          'DocumentTypeObjectId'), 0);
      if t_DocTypeId > 0 then
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
            t_NewResponse, t_DocTypeId);
      end if;
      t_RelId := api.pkg_RelationshipUpdate.New(
          api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
          t_NewResponse, i.questionid);
      t_RelId := api.pkg_RelationshipUpdate.New(
          api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'SubmitResolution'),
          t_NewResponse, a_ObjectId);
    end loop;
      
    -- Relate each response to its Parent response
    for i in (
        select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
            from query.r_ABC_MuniQAResponse r
            join query.r_ABC_QuestionResponse qr
                on qr.ResponseId = r.ResponseId
            join query.r_ABC_QuestionResponse pq
                on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
            join query.r_ABC_MuniQAResponse pr
                on pr.ResponseId = pq.ResponseId
           where r.SubmitResolutionID = a_ObjectId
             and pr.SubmitResolutionID = a_ObjectId
        ) loop
      t_RelId := api.pkg_RelationshipUpdate.New(t_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
    end loop;

    -- Generate Documents 
    for d in (select
                r.DocumentTypeObjectId,
                (case 
                   when t_JobType = 'j_ABC_NewApplication' and r.MunicipalAppMandatory = 'Y' then 'Y'
                   when t_JobType = 'j_ABC_AmendmentApplication' and r.MunicipalAmendmentMandatory = 'Y' then 'Y'
                   when t_JobType = 'j_ABC_RenewalApplication' and r.MunicipalRenewMandatory = 'Y' then 'Y'
                   else 'N' 
                end) Mandatory
              from query.r_OLLicenseTypeDocumentType r
              where r.LicenseTypeObjectId = t_LicenseTypeId
                and (r.MunicipalApp = 'Y'
                or r.MunicipalAmendment = 'Y'
                or r.MunicipalRenew = 'Y')
                and r.DocumentTypeObjectId not in
                    (select r1.DocumentTypeId
                     from query.r_ABC_SubmitResRespDocType r1
                     where r1.SubmitResolutionId = a_ObjectId)
             ) loop
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('p_ABC_SubmitResolution',
          'ResponseDocumentType'), a_ObjectId, d.documenttypeobjectid);
      api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
    end loop;

  end CopySubmitResolutionQuestions;
  
  /*---------------------------------------------------------------------------
   * CopyMunicipalReviewQuestions() -- PUBLIC
   *   This generates questions on submit resolution constructor
   * This will fail if run twice or on a process that has questions.
   *-------------------------------------------------------------------------*/
  procedure CopyMunicipalReviewQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_PermitTypeId                      number :=
        api.pkg_columnquery.NumericValue(a_ObjectId, 'PermitTypeId');
    t_NewResponse                       number;
    t_RelId                             number;
    t_ResponseDefId                     number(9) :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
    t_DocTypeId                         number;
    t_QARel                             udt_IdList;
    t_ParentResponseEPId                udt_Id :=
        api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'ParentResponse');
    t_JobType                           varchar2(4000) :=
        api.pkg_columnquery.Value(a_ObjectId, 'JobName');
  begin
    
    for i in (select r.QuestionId
              from query.r_ABC_PermitTypeMuniQuestion r
              where r.PermitTypeId = t_PermitTypeId
              and ((t_JobType = 'j_ABC_PermitApplication' and r.Application = 'Y')
              or (t_JobType = 'j_ABC_PermitRenewal' and r.Renewal = 'Y'))
             ) loop
      -- Create Response
      t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
      for k in (select
                  api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                  name columnname
                from api.columndefs cd
                where cd.objectdefid = t_ResponseDefId
                  and name in ('SortOrder', 'Question','AcceptableAnswer',
                      'NonacceptableText','NonacceptableResponseType')
               ) loop
        api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
      end loop;

      t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
          'DocumentTypeObjectId'), 0);
      if t_DocTypeId > 0 then
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
            t_NewResponse, t_DocTypeId);
      end if;
      t_RelId := api.pkg_RelationshipUpdate.New(
          api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
          t_NewResponse, i.questionid);
      t_RelId := api.pkg_RelationshipUpdate.New(
          api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'MunicipalReview'),
          t_NewResponse, a_ObjectId);
    end loop;
      
    -- Relate each response to its Parent response
    for i in (
        select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
            from query.r_ABC_MunicipalReviewQARespons r
            join query.r_ABC_QuestionResponse qr
                on qr.ResponseId = r.ResponseId
            join query.r_ABC_QuestionResponse pq
                on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
            join query.r_ABC_MunicipalReviewQARespons pr
                on pr.ResponseId = pq.ResponseId
           where r.MunicipalReviewID = a_ObjectId
             and pr.MunicipalReviewID = a_ObjectId
        ) loop
      t_RelId := api.pkg_RelationshipUpdate.New(t_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
    end loop;

    -- Generate Documents 
    for d in (select
                r.DocumentTypeObjectId,
                (case 
                   when t_JobType = 'j_ABC_PermitApplication' and r.MunicipalAppMandatory = 'Y' then 'Y'
                   when t_JobType = 'j_ABC_PermitRenewal' and r.MunicipalRenewMandatory = 'Y' then 'Y'
                   else 'N' 
                end) Mandatory,
                r.SpecialConditionsMandatory
              from query.r_ABC_PermitTypeDocumentType r
              where r.PermitTypeObjectId = t_PermitTypeId
                and (r.MunicipalApp = 'Y'
                or r.MunicipalRenew = 'Y')
                and r.DocumentTypeObjectId not in
                    (select r1.DocumentTypeId
                     from query.r_ABC_MuniReviewRespDocType r1
                     where r1.MunicipalReviewId = a_ObjectId)
             ) loop
      t_RelId := api.pkg_relationshipupdate.New(
          api.pkg_configquery.EndPointIdForName('p_ABC_MunicipalityReview',
          'ResponseDocumentType'), a_ObjectId, d.documenttypeobjectid);
      api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      api.pkg_columnupdate.SetValue(t_RelId, 'SpecialConditionsMandatory', d.SpecialConditionsMandatory);
    end loop;

  end CopyMunicipalReviewQuestions;

  /*---------------------------------------------------------------------------
   * CreateResponseDocuments()
   *   Create Document relationship based on questions that require a document
   * response
   *-------------------------------------------------------------------------*/
  procedure CreateResponseDocuments(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
   ) is
    t_ObjectDef                         varchar2(40);
    t_QARel                             udt_Id;
    t_PermitTypeId                      udt_Id;
    t_LicenseTypeId                     udt_Id;
    t_DTEndpoint                        udt_Id;
    t_AddDocuments                      varchar2(1);
    t_QuestionsCount                    number;
    t_ResponseDocCount                  number;
    t_Mandatory                         varchar2(1);
    t_QuestionDocCount                  integer;
  begin

    t_ObjectDef := api.pkg_columnquery.value(a_ObjectId, 'ObjectDefName');
    t_PermitTypeId := nvl(api.pkg_columnquery.value(a_ObjectId, 'OnlinePermitTypeObjectId'),
        api.pkg_columnquery.value(a_ObjectId, 'PermitTypeObjectId'));
    t_LicenseTypeId := api.pkg_columnquery.value(a_ObjectId, 'OnlineLicenseTypeObjectId');
    t_DTEndpoint := api.pkg_configquery.EndPointIdForName(t_ObjectDef,'DocumentType');
    t_AddDocuments := api.pkg_columnquery.value(a_ObjectId, 'AddDocuments');

    if t_ObjectDef = 'j_ABC_NewApplication' then
      -- New Application
      for e in (select r.relationshipid, r.DocTypeId
                from query.r_ABC_AppResponseDocType r
                where r.NewAppId = a_ObjectId
               )loop
        api.pkg_relationshipupdate.remove(e.relationshipid);
      end loop;
      for d in (select na.relationshipid, na.Mandatory
                from query.r_ABC_NewAppDocumentType na
                where na.NewAppId = a_Objectid) loop
        api.pkg_columnupdate.SetValue(d.relationshipid, 'QuestionMandatory', d.mandatory);
      end loop;
      if t_AddDocuments = 'Y' then
        for c in (select
                    distinct qa.NewApplicationId,
                    api.pkg_columnQuery.value(
                        qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
                  from query.r_ABC_NewAppQAResponse qa
                  where qa.NewApplicationId = a_ObjectId
                    and (api.pkg_columnquery.value(
                        qa.ResponseId, 'AcceptableAnswer') <>
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') or
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'ShowQuestionSQL') = 'Y'
                 ) loop

          select count(*)
          into t_QuestionDocCount
          from api.relationships r
          where r.FromObjectId = a_objectid
            and r.toobjectid = c.documenttypeid;
          if t_QuestionDocCount = 0 then
            t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, c.newapplicationid,
                c.documenttypeid);
            api.pkg_columnupdate.SetValue(t_QARel, 'Mandatory','Y');
          elsif t_QuestionDocCount > 0 then
            for j in (select relationshipid
                      from api.relationships
                      where fromobjectid = a_ObjectId
                        and toobjectid = c.documenttypeid) loop
              api.pkg_columnupdate.SetValue(j.relationshipid, 'QuestionMandatory', 'Y');
            end loop;
          end if;
        end loop;
      end if;
    elsif t_ObjectDef = 'j_ABC_RenewalApplication' then
      -- Renewal Application
      for a in (select r.relationshipid
                from query.r_ABC_RenewResponseDocType r
                where r.RenewalAppId = a_ObjectId
               ) loop
        api.pkg_relationshipupdate.remove(a.relationshipid);
      end loop;
      for f in (select aa.relationshipid, aa.Mandatory
                from query.r_ABC_RenewAppDocumentType aa
                where aa.RenewAppId = a_Objectid) loop
        api.pkg_columnupdate.SetValue(f.relationshipid, 'QuestionMandatory', f.mandatory);
      end loop;
      if t_AddDocuments = 'Y' then
        for l in (select
                    distinct qa.RenewalId,
                    api.pkg_columnQuery.value(
                        qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
                  from query.r_ABC_RenewalQAResponse qa
                  where qa.RenewalId = a_ObjectId
                    and (api.pkg_columnquery.value(
                        qa.ResponseId, 'AcceptableAnswer') <>
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') or
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'ShowQuestionSQL') = 'Y'
                 ) loop

          select count(*)
          into t_QuestionDocCount
          from api.relationships r
          where r.FromObjectId = a_objectid
            and r.toobjectid = l.documenttypeid;
          if t_QuestionDocCount = 0 then
            t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, l.RenewalId,
                l.documenttypeid);
            api.pkg_columnupdate.setvalue(t_QARel, 'Mandatory', 'Y');
          elsif t_QuestionDocCount > 0 then
            for j in (select relationshipid
                      from api.relationships
                      where fromobjectid = a_ObjectId
                        and toobjectid = l.documenttypeid) loop
              api.pkg_columnupdate.SetValue(j.relationshipid, 'QuestionMandatory', 'Y');
            end loop;
          end if;
        end loop;
      end if;
    elsif t_ObjectDef = 'j_ABC_AmendmentApplication' then
      -- Amendment Application
      for a in (select r.relationshipid
                from query.r_ABC_AmendResponseDocType r
                where r.AmendAppId = a_ObjectId
               ) loop
        api.pkg_relationshipupdate.remove(a.relationshipid);
      end loop;
      for f in (select aa.relationshipid, aa.Mandatory
                from query.r_ABC_AmendAppDocumentType aa
                where aa.AmendAppId = a_Objectid) loop
        api.pkg_columnupdate.SetValue(f.relationshipid, 'QuestionMandatory', f.mandatory);
      end loop;
      if t_AddDocuments = 'Y' then
        for l in (select
                    distinct qa.AmendmentJobId,
                    api.pkg_columnQuery.value(
                        qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
                  from query.r_ABC_AmendmentQAResponse qa
                  where qa.AmendmentJobId = a_ObjectId
                    and (api.pkg_columnquery.value(
                        qa.ResponseId, 'AcceptableAnswer') <>
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') or
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'ShowQuestionSQL') = 'Y'
                 ) loop
          select count(*)
          into t_QuestionDocCount
          from api.relationships r
          where r.FromObjectId = a_objectid
            and r.toobjectid = l.documenttypeid;
          if t_QuestionDocCount = 0 then
            t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, l.amendmentjobid,
                l.documenttypeid);
            api.pkg_columnupdate.setvalue(t_QARel, 'Mandatory', 'Y');
          elsif t_QuestionDocCount > 0 then
            for j in (select relationshipid
                      from api.relationships
                      where fromobjectid = a_ObjectId
                        and toobjectid = l.documenttypeid) loop
              api.pkg_columnupdate.SetValue(j.relationshipid, 'QuestionMandatory', 'Y');
            end loop;
          end if;
        end loop;
      end if;
    elsif t_ObjectDef = 'j_ABC_PRApplication' then
      -- PR Application
      for e in (select r.relationshipid, r.DocTypeId
                from query.r_ABC_PRAppResponseDocType r
                where r.PRApplicationId = a_ObjectId
               )loop
        api.pkg_relationshipupdate.remove(e.relationshipid);
      end loop;
      for d in (select na.relationshipid, na.Mandatory
                from query.r_ABC_OLPRAppDocumentType na
                where na.PRApplicationId = a_Objectid) loop
        api.pkg_columnupdate.SetValue(d.relationshipid, 'QuestionMandatory', d.mandatory);
      end loop;
      if t_AddDocuments = 'Y' then
        for c in (select
                    distinct qa.PRApplicationId,
                    api.pkg_columnQuery.value(
                        qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
                  from query.r_ABC_PRApplicationQAResponse qa
                  where qa.PRApplicationId = a_ObjectId
                    and (api.pkg_columnquery.value(
                        qa.ResponseId, 'AcceptableAnswer') <>
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') or
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'ShowQuestionSQL') = 'Y'
                 ) loop

          select count(*)
          into t_QuestionDocCount
          from api.relationships r
          where r.FromObjectId = a_objectid
            and r.toobjectid = c.documenttypeid;
          if t_QuestionDocCount = 0 then
            t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, c.PRApplicationId,
                c.documenttypeid);
            api.pkg_columnupdate.SetValue(t_QARel, 'Mandatory','Y');
          elsif t_QuestionDocCount > 0 then
            for j in (select relationshipid
                      from api.relationships
                      where fromobjectid = a_ObjectId
                        and toobjectid = c.documenttypeid) loop
              api.pkg_columnupdate.SetValue(j.relationshipid, 'QuestionMandatory', 'Y');
            end loop;
          end if;
        end loop;
      end if;
    elsif t_ObjectDef = 'j_ABC_PRRenewal' then
      -- PR Renewal
      for e in (select r.relationshipid, r.DocTypeId
                from query.r_ABC_PRRenewalResponseDocType r
                where r.PRRenewalId = a_ObjectId
               )loop
        api.pkg_relationshipupdate.remove(e.relationshipid);
      end loop;
      for d in (select na.relationshipid, na.Mandatory
                from query.r_ABC_PRRenewalDocumentType na
                where na.PRRenewalId = a_Objectid) loop
        api.pkg_columnupdate.SetValue(d.relationshipid, 'QuestionMandatory', d.mandatory);
      end loop;
      if t_AddDocuments = 'Y' then
        for c in (select
                    distinct qa.PRRenewalId,
                    api.pkg_columnQuery.value(
                        qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
                  from query.r_ABC_PRRenewalQAResponse qa
                  where qa.PRRenewalId = a_ObjectId
                    and (api.pkg_columnquery.value(
                        qa.ResponseId, 'AcceptableAnswer') <>
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') or
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'ShowQuestionSQL') = 'Y'
                 ) loop

          select count(*)
          into t_QuestionDocCount
          from api.relationships r
          where r.FromObjectId = a_objectid
            and r.toobjectid = c.documenttypeid;
          if t_QuestionDocCount = 0 then
            t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, c.PRRenewalId,
                c.documenttypeid);
            api.pkg_columnupdate.SetValue(t_QARel, 'Mandatory','Y');
          elsif t_QuestionDocCount > 0 then
            for j in (select relationshipid
                      from api.relationships
                      where fromobjectid = a_ObjectId
                        and toobjectid = c.documenttypeid) loop
              api.pkg_columnupdate.SetValue(j.relationshipid, 'QuestionMandatory', 'Y');
            end loop;
          end if;
        end loop;
      end if;
    elsif t_ObjectDef = 'j_ABC_PRAmendment' then
      -- PR Amendment Application
      for a in (select r.relationshipid
                from query.r_ABC_PRAmendResponseDocType r
                where r.PRAmendAppId = a_ObjectId
               ) loop
        api.pkg_relationshipupdate.remove(a.relationshipid);
      end loop;
      for f in (select aa.relationshipid, aa.Mandatory
                from query.r_ABC_PRAmendAppDocumentType aa
                where aa.PRAmendAppId = a_Objectid) loop
        api.pkg_columnupdate.SetValue(f.relationshipid, 'QuestionMandatory', f.mandatory);
      end loop;
      if t_AddDocuments = 'Y' then
        for l in (select
                    distinct qa.PRAmendmentId,
                    api.pkg_columnQuery.value(
                        qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
                  from query.r_ABC_PRAmendQAResponse qa
                  where qa.PRAmendmentId = a_ObjectId
                    and (api.pkg_columnquery.value(
                        qa.ResponseId, 'AcceptableAnswer') <>
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') or
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'ShowQuestionSQL') = 'Y'
                 ) loop
          select count(*)
          into t_QuestionDocCount
          from api.relationships r
          where r.FromObjectId = a_objectid
            and r.toobjectid = l.documenttypeid;
          if t_QuestionDocCount = 0 then
            t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, l.pramendmentid,
                l.documenttypeid);
            api.pkg_columnupdate.setvalue(t_QARel, 'Mandatory', 'Y');
          elsif t_QuestionDocCount > 0 then
            for j in (select relationshipid
                      from api.relationships
                      where fromobjectid = a_ObjectId
                        and toobjectid = l.documenttypeid) loop
              api.pkg_columnupdate.SetValue(j.relationshipid, 'QuestionMandatory', 'Y');
            end loop;
          end if;
        end loop;
      end if;
    elsif t_ObjectDef = 'j_ABC_PermitApplication' then
      -- Permit Application
      for a in(select r.relationshipid
               from query.r_ABC_PermitAppResponseDocType r
               where r.PermitAppId = a_ObjectId
              ) loop
        api.pkg_relationshipupdate.remove(a.relationshipid);
      end loop;
      for b in (select pa.RelationshipId, pa.Mandatory
                from query.r_ABC_PermitAppDocumentType pa
                where pa.PermitAppId = a_Objectid) loop

        api.pkg_columnupdate.SetValue(b.relationshipid, 'QuestionMandatory', b.mandatory);
      end loop;
      if t_AddDocuments = 'Y' then
        for l in (select
                    distinct qa.PermitApplicationId,
                    api.pkg_columnQuery.value(
                        qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
                  from query.r_ABC_PermitAppQAResponse qa
                  where qa.PermitApplicationId = a_ObjectId
                    and (api.pkg_columnquery.value(
                        qa.ResponseId, 'AcceptableAnswer') <>
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') or
                        api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
                    and api.pkg_columnquery.value(
                        qa.ResponseId, 'ShowQuestionSQL') = 'Y'
                 ) loop

          select count(*)
          into t_QuestionDocCount
          from api.relationships r
          where r.FromObjectId = a_objectid
            and r.toobjectid = l.documenttypeid;
          if t_QuestionDocCount = 0 then
            t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, l.permitapplicationid,
                l.documenttypeid);
            api.pkg_columnupdate.setvalue(t_QARel, 'Mandatory', 'Y');
          elsif t_QuestionDocCount > 0 then
            for e in (select relationshipid
                      from api.relationships
                      where fromobjectid = a_ObjectId
                        and toobjectid = l.documenttypeid) loop
              api.pkg_columnupdate.SetValue(e.relationshipid, 'QuestionMandatory', 'Y');
            end loop;
          end if;
        end loop;
      end if;
    elsif t_ObjectDef = 'j_ABC_PermitRenewal' then
      -- Permit Renewal
      for a in (select r.relationshipid
                from query.r_ABC_PermitRnwResponseDocType r
                where r.PermitRnwId = a_ObjectId
               ) loop
        api.pkg_relationshipupdate.remove(a.relationshipid);
      end loop;
      for g in (select pr.RelationshipId, pr.Mandatory
                from query.r_ABC_PermitRenewDocumentType pr
                where pr.PermitRenewId = a_Objectid) loop
        api.pkg_columnupdate.SetValue(g.relationshipid, 'QuestionMandatory', g.mandatory);
      end loop;
      if t_AddDocuments = 'Y' then
        for l in (select
                    distinct qa.PermitRenewalId,
                    api.pkg_columnQuery.value(
                        qa.ResponseId, 'DocumentTypeObjectId') DocumentTypeId
                  from query.r_ABC_PermitRnwQAResponse qa
                   where qa.PermitRenewalId = a_ObjectId
                     and (api.pkg_columnquery.value(
                         qa.ResponseId, 'AcceptableAnswer') <>
                         api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') or
                         api.pkg_columnquery.value(qa.ResponseId, 'AnswerOnJob') is null)
                     and api.pkg_columnquery.value(
                         qa.ResponseId, 'NonAcceptableResponseType') = 'Document'
                     and api.pkg_columnquery.value(
                        qa.ResponseId, 'ShowQuestionSQL') = 'Y'
                 ) loop
          select count(*)
          into t_QuestionDocCount
          from api.relationships r
          where r.FromObjectId = a_objectid
            and r.toobjectid = l.documenttypeid;
          if t_QuestionDocCount = 0 then
          t_QaRel := api.pkg_relationshipupdate.New(t_DTEndpoint, l.PermitRenewalId,
              l.documenttypeid);
          api.pkg_columnupdate.setvalue(t_QARel, 'Mandatory', 'Y');
          elsif t_QuestionDocCount > 0 then
            for e in (select relationshipid
                      from api.relationships
                      where fromobjectid = a_ObjectId
                        and toobjectid = l.documenttypeid) loop
              api.pkg_columnupdate.SetValue(e.relationshipid, 'QuestionMandatory', 'Y');
            end loop;
          end if;
        end loop;
      end if;
    end if;

  end CreateResponseDocuments;

  /*---------------------------------------------------------------------------
   * CopyRenewalQuestions() -- PUBLIC
   *   For each renewal question on the License Type admin object, create
   * a Response object to display on the application job. Also copy any applicable
   * documents.
   *-------------------------------------------------------------------------*/
  procedure CopyRenewalQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_LicenseTypeId                     udt_Id :=
        api.pkg_columnquery.NumericValue(a_ObjectId, 'RenewalLicenseTypeId');
    t_NewResponse                       udt_Id;
    t_RelId                             udt_Id;
    t_ResponseDefId                     udt_Id :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
    t_DocTypeId                         udt_Id;
    t_QARel                             udt_idList;
  begin

    -- Grab the rel id of any questions on the job
    select qa.relationshipid
    bulk collect into t_QARel
    from query.r_ABC_RenewalQAResponse qa
    where qa.RenewalId = a_ObjectId;

    -- If no questions are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
    for i in (select r.QuestionId
              from query.r_abc_lictyperenewquestion r
              where r.LicenseTypeId = t_LicenseTypeId
             ) loop
      -- Create Response
      t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
      for k in (select
                  api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                  name columnname
                from api.columndefs cd
                where objectdefid = t_ResponseDefId
                  and name in ('SortOrder', 'Question','AcceptableAnswer',
                      'NonacceptableText','NonacceptableResponseType')
               ) loop
        -- For each column on the Question object, copy the value
        -- into the corresponding column on the Response object
        api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
      end loop;

      t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
          'DocumentTypeObjectId'), 0);
      if t_DocTypeId > 0 then
        -- Create rel from new Response object to Document Type
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
            t_NewResponse, t_DocTypeId);
      end if;
      -- Create rel from new response to the question object
      t_RelId := api.pkg_RelationshipUpdate.New(
          api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
          t_NewResponse, i.questionid);
      -- Create rel from new response object to renewal job
      t_RelId := api.pkg_RelationshipUpdate.New(
          api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Renewal'),
          t_NewResponse, a_ObjectId);
    end loop;

      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_RenewalQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_RenewalQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.RenewalId = a_ObjectId
               and pr.RenewalId = a_ObjectId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(g_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;
    -- Copy any documents for specific License Type to the Renewal job
    select r.RelationshipId
    bulk collect into t_QARel
    from query.r_ABC_RenewAppDocumentType r
    where r.RenewAppId = a_ObjectId;

    -- If no documents are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
      for d in (select
                  r.DocumentTypeObjectId,
                  r.RenewalMandatory
                from query.r_OLLicenseTypeDocumentType r
                where r.LicenseTypeObjectId = t_LicenseTypeId
                  and r.RenewalApplication = 'Y'
               ) loop
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('j_ABC_RenewalApplication',
            'OnlineDocumentType'), a_ObjectId, d.documenttypeobjectid);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.RenewalMandatory);
      end loop;
    end if;

  end CopyRenewalQuestions;

  /*---------------------------------------------------------------------------
   * CopyAmendmentQuestions() -- PUBLIC
   *   For each amendment question on the Amendment Type admin object, create
   * a Response object to display on the amendment job. Also copy any applicable
   * documents.
   *-------------------------------------------------------------------------*/
  procedure CopyAmendmentQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_AmendmentTypeId                   udt_Id :=
        api.pkg_columnquery.NumericValue(a_ObjectId, 'OnlineAmendmentTypeObjectId');
    t_NewResponse                       udt_Id;
    t_RelId                             udt_Id;
    t_ResponseDefId                     udt_Id :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
    t_DocTypeId                         udt_Id;
    t_QuestionGenerated                 varchar2(1);
    t_QARel                             udt_IdList;
  begin

    -- Grab the rel id of any questions on the job
    select qa.relationshipid
    bulk collect into t_QARel
    from query.r_ABC_AmendmentQAResponse qa
    where qa.AmendmentJobId = a_ObjectId;

    -- If no questions are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
    for i in (select r.QuestionId
              from query.r_ABC_AmendTypeQuestion r
              where r.AmendmentTypeId = t_AmendmentTypeId
             ) loop
      -- Create Response
      t_QuestionGenerated := 'N';
      if api.pkg_ColumnQuery.Value (i.QuestionId,'AllOrSomeLicTypes') = 'All' then
      -- Generate Question for all License Types
        t_QuestionGenerated := 'Y';
        t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (select
                    api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                    name columnname
                  from api.columndefs cd
                  where objectdefid = t_ResponseDefId
                    and name in ('SortOrder', 'Question','AcceptableAnswer',
                        'NonacceptableText','NonacceptableResponseType')
                 ) loop
          -- For each column on the Question object, copy the value into the
          -- corresponding column on the Response object
          api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
        end loop;
      else
        if instr(api.pkg_columnquery.Value(i.questionid,'AmendmentLicenseTypeObjectId'),
              api.pkg_columnquery.Value(api.pkg_columnquery.Value(a_ObjectId,
              'LicenseToAmendObjectId'),'LicenseTypeObjectId')) > 0 then
        -- Generate Question only for this particular License Type
          t_QuestionGenerated := 'Y';
          t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
          for k in (select
                      api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                      name columnname
                      from api.columndefs cd
                      where objectdefid = t_ResponseDefId
                        and name in ('SortOrder', 'Question','AcceptableAnswer',
                            'NonacceptableText','NonacceptableResponseType')
                     ) loop
            -- For each column on the Question object, copy the value into the
            -- corresponding column on the Response object
            api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
          end loop;
        end if;
      end if;

      if t_QuestionGenerated = 'Y' then
        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
            'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
        -- Create rel from new Response object to Document Type
          t_RelId := api.pkg_RelationshipUpdate.New(
              api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
              t_NewResponse, t_DocTypeId);
        end if;
        -- Create rel from new response to the question object
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
            t_NewResponse, i.questionid);
        -- Create rel from new response object to renewal job
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Amendment'),
            t_NewResponse, a_ObjectId);
      end if;
    end loop;

      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_AmendmentQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_AmendmentQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.AmendmentJobId = a_ObjectId
               and pr.AmendmentJobId = a_ObjectId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(g_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;
    -- Copy any documents for specific Amendment Type to the Amendment job
    select r.RelationshipId
    bulk collect into t_QARel
    from query.r_ABC_AmendAppDocumentType r
    where r.AmendAppId = a_ObjectId;

    -- If no documents are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
      for d in (select
                  r.DocumentTypeObjectId,
                  r.Mandatory
                from query.r_OLAmendmentTypeDocumentType r
                where r.AmendmentTypeObjectId = t_AmendmentTypeId
                  and r.DocumentTypeObjectId not in
                      (select r1.DocTypeId
                       from query.r_ABC_AmendResponseDocType r1
                       where r1.AmendAppId = a_ObjectId)
               ) loop
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('j_ABC_AmendmentApplication',
            'OnlineDocumentType'), a_ObjectId, d.documenttypeobjectid);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      end loop;
    end if;

  end CopyAmendmentQuestions;

  /*---------------------------------------------------------------------------
   * CopyPRAmendmentQuestions() -- PUBLIC
   *   For each amendment question on the Product Amendment Type admin object, create
   * a Response object to display on the PR amendment job. Also copy any applicable
   * documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPRAmendmentQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_ProductAmendTypeId                udt_Id;
    t_ProdAmdTypeQuesEndPId             udt_Id;
    t_PRAmendQARespEndPId               udt_Id;
    t_NewResponse                       udt_Id;
    t_RelId                             udt_Id;
    t_ResponseDefId                     udt_Id;
    t_DocTypeId                         udt_Id;
    t_QuestionGenerated                 varchar2(1);
    t_OLDocTypeExist                    number;
    t_QuesIdList                        udt_IdList;
    t_QARelExist                        number;
  begin
    select count(1) into t_QARelExist
    from query.r_ABC_PRAmendQAResponse r
    where r.PRAmendmentId = a_ObjectId;

    -- if no question on job then copy
    if t_QARelExist = 0 then
      t_ProductAmendTypeId := api.pkg_columnquery.NumericValue(a_ObjectId, 'ProductAmendmentTypeObjectId');
      t_ResponseDefId := api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
      t_ProdAmdTypeQuesEndPId := api.pkg_configquery.EndPointIdForName('o_ABC_ProductAmendmentType', 'Question');
      t_QuesIdList := api.pkg_objectquery.RelatedObjects(t_ProductAmendTypeId, t_ProdAmdTypeQuesEndPId);

      for i in 1 .. t_QuesIdList.count loop
        t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (select
                    api.pkg_columnquery.Value(t_QuesIdList(i), cd.name) columnvalue,
                    name columnname
                    from api.columndefs cd
                    where objectdefid = t_ResponseDefId
                      and name in ('SortOrder', 'Question','AcceptableAnswer',
                          'NonacceptableText','NonacceptableResponseType')
                   ) loop
          -- For each column on the Question object, copy the value into the
          -- corresponding column on the Response object
          api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
        end loop;

        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(t_QuesIdList(i),
            'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
        -- Create rel from new Response object to Document Type
          t_RelId := api.pkg_RelationshipUpdate.New(
              api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
              t_NewResponse, t_DocTypeId);
        end if;
        -- Create rel from new response to the question object
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
            t_NewResponse, t_QuesIdList(i));
        -- Create rel from new response object to renewal job
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'PRAmendment'),
            t_NewResponse, a_ObjectId);
      end loop;

      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_PRAmendQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_PRAmendQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.PRAmendmentId = a_ObjectId
               and pr.PRAmendmentId = a_ObjectId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(g_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;

    -- If no document types are currently on the job then generate them from the list
    -- on the admin site
    select count(1) into t_OLDocTypeExist
    from query.r_ABC_PRAmendAppDocumentType r
    where r.PRAmendAppId = a_ObjectId;

    if t_OLDocTypeExist = 0 then
      for d in (select
                  r.DocumentTypeObjectId,
                  r.Mandatory
                from query.r_PRAmendmentTypeDocumentType r
                where r.ProductAmendmentTypeObjectId = t_ProductAmendTypeId
                  and r.DocumentTypeObjectId not in
                      (select r1.DocTypeId
                       from query.r_ABC_PRAmendResponseDocType r1
                       where r1.PRAmendAppId = a_ObjectId)
               ) loop
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('j_ABC_PRAmendment',
            'OnlineDocumentType'), a_ObjectId, d.documenttypeobjectid);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      end loop;
    end if;

  end CopyPRAmendmentQuestions;

  /*---------------------------------------------------------------------------
   * CopyPermitApplicationQuestions() -- PUBLIC
   *   For each permit application question on the Permit Type admin object,
   * create a Response object to display on the application job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPermitApplicationQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_PermitTypeId                      number :=
        api.pkg_columnquery.NumericValue(a_ObjectId, 'OnlinePermitTypeObjectId');
    t_NewResponse                       number;
    t_RelId                             number;
    t_ResponseDefId                     number(9) :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
    t_DocTypeId                         number;
    t_QARel                             udt_IdList;
  begin

    --Grab the rel id of any questions on the job
    select qa.relationshipid
    bulk collect into t_QARel
    from query.r_ABC_PermitAppQAResponse qa
    where qa.PermitApplicationId = a_ObjectId;

    -- If no questions are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
      for i in (select r.QuestionId
                from query.r_ABC_PermitTypeAppQuestion r
                where r.PermitTypeId = t_PermitTypeId
               ) loop
        -- Create Response
        t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (select
                    api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                    name columnname
                  from api.columndefs cd
                  where cd.objectdefid = t_ResponseDefId
                    and name in ('SortOrder', 'Question','AcceptableAnswer',
                        'NonacceptableText','NonacceptableResponseType')
                 ) loop
          api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
        end loop;

        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
            'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
          t_RelId := api.pkg_RelationshipUpdate.New(
              api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
              t_NewResponse, t_DocTypeId);
        end if;
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
            t_NewResponse, i.questionid);
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'PermitApplication'),
            t_NewResponse, a_ObjectId);
      end loop;
      
      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_PermitAppQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_PermitAppQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.PermitApplicationId = a_ObjectId
               and pr.PermitApplicationId = a_ObjectId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(g_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;

    -- Copy any documents for specific Permit Type to the Application job
    select r.RelationshipId
    bulk collect into t_QARel
    from query.r_ABC_PermitAppDocumentType r
    where r.PermitAppId = a_ObjectId;

    -- If no documents are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
      for d in (select
                  r.DocumentTypeObjectId,
                  r.Mandatory
                from query.r_ABC_PermitTypeDocumentType r
                where r.PermitTypeObjectId = t_PermitTypeId
                  and r.NewApplication = 'Y'
                  and r.DocumentTypeObjectId not in
                      (select r1.DocTypeId
                       from query.r_ABC_PermitAppResponseDocType r1
                       where r1.PermitAppId = a_ObjectId)
               ) loop
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication',
            'OnlineDocumentType'), a_ObjectId, d.documenttypeobjectid);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.mandatory);
      end loop;
    end if;

  end CopyPermitApplicationQuestions;

  /*---------------------------------------------------------------------------
   * CopyPermitRenewalQuestions() -- PUBLIC
   *   For each permit renewal question on the Permit Type admin object,
   * create a Response object to display on the application job. Also copy any
   * applicable documents.
   *-------------------------------------------------------------------------*/
  procedure CopyPermitRenewalQuestions(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_PermitTypeId                      number :=
        api.pkg_columnquery.NumericValue(a_ObjectId, 'PermitTypeObjectId');
    t_NewResponse                       number;
    t_RelId                             number;
    t_ResponseDefId                     number(9) :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_QAResponse');
    t_DocTypeId                         number;
    t_QARel                             udt_IdList;
  begin

    --Grab the rel id of any questions on the job
    select qa.relationshipid
    bulk collect into t_QARel
    from query.r_ABC_PermitRnwQAResponse qa
    where qa.PermitRenewalId = a_ObjectId;
    -- If no questions are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
      for i in (select r.QuestionId
                from query.r_ABC_PermitTypeRenewQuestion r
                where r.PermitTypeId = t_PermitTypeId
               ) loop
        -- Create Response
        t_NewResponse := api.pkg_objectupdate.New(t_ResponseDefId);
        for k in (select
                    api.pkg_columnquery.Value(i.questionid, cd.name) columnvalue,
                    name columnname
                  from api.columndefs cd
                  where cd.objectdefid = t_ResponseDefId
                    and name in ('SortOrder', 'Question','AcceptableAnswer',
                        'NonacceptableText','NonacceptableResponseType')
                 ) loop
          api.pkg_columnupdate.SetValue(t_NewResponse, k.columnname, k.columnvalue);
        end loop;

        t_DocTypeId := nvl(api.pkg_columnquery.NumericValue(i.questionid,
            'DocumentTypeObjectId'), 0);
        if t_DocTypeId > 0 then
          t_RelId := api.pkg_RelationshipUpdate.New(
              api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'DocumentType'),
              t_NewResponse, t_DocTypeId);
        end if;
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'Question'),
            t_NewResponse, i.questionid);
        t_RelId := api.pkg_RelationshipUpdate.New(
            api.pkg_ConfigQuery.EndpointIdForName('o_ABC_QAResponse', 'PermitRenewal'),
            t_NewResponse, a_ObjectId);
      end loop;

      -- Relate each response to its Parent response
      for i in (
          select r.ResponseId ChildResponse, pr.ResponseId ParentResponse
              from query.r_ABC_PermitRnwQAResponse r
              join query.r_ABC_QuestionResponse qr
                  on qr.ResponseId = r.ResponseId
              join query.r_ABC_QuestionResponse pq
                  on pq.QuestionId = api.pkg_ColumnQuery.value(qr.QuestionId, 'ParentQuestionObjectId')
              join query.r_ABC_PermitRnwQAResponse pr
                  on pr.ResponseId = pq.ResponseId
             where r.PermitRenewalId = a_ObjectId
               and pr.PermitRenewalId = a_ObjectId
          ) loop
        t_RelId := api.pkg_RelationshipUpdate.New(g_ParentResponseEPId, i.ChildResponse, i.ParentResponse);
      end loop;
    end if;

    -- Copy any documents for specific Permit Type to the Renewal job
    select r.RelationshipId
    bulk collect into t_QARel
    from query.r_ABC_PermitRenewDocumentType r
    where r.PermitRenewId = a_ObjectId;

    -- If no documents are currently on the job then generate them from the list
    -- on the admin site
    if t_QARel.count = 0 then
      for d in (select
                  r.DocumentTypeObjectId,
                  r.RenewalMandatory
                from query.r_ABC_PermitTypeDocumentType r
                where r.PermitTypeObjectId = t_PermitTypeId
                  and r.RenewalApplication = 'Y'
                  and r.DocumentTypeObjectId not in
                      (select r1.DocTypeId
                       from query.r_ABC_PermitRnwResponseDocType r1
                       where r1.PermitRnwId = a_ObjectId)
               ) loop
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('j_ABC_PermitRenewal',
            'OnlineDocumentType'), a_ObjectId, d.documenttypeobjectid);
        api.pkg_columnupdate.SetValue(t_RelId, 'Mandatory', d.Renewalmandatory);
      end loop;
    end if;

  end CopyPermitRenewalQuestions;

  /*---------------------------------------------------------------------------
    CopyDailyDeposit
  *-------------------------------------------------------------------------*/
  procedure CopyDailyDeposit(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_DepositDate                       date :=
      api.pkg_ColumnQuery.DateValue(a_ObjectId, 'DepositDate');
  begin

    for i in (select nd.PaymentId ObjectId
                from query.r_ABC_DailyDepositNoDeposit nd
               where nd.DailyDepositId = a_ObjectId) loop
      api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'DepositDate', t_DepositDate);
    end loop;

  end CopyDailyDeposit;

  /*---------------------------------------------------------------------------
   * GetVehicles() -- PUBLIC
   *   Upon creation of Renewal Application from the Public Site, create a new
   * vehicle and related address record, then relate the new vehicle record to
   * the renewal application. This provides an audit trail of changes made to
   * the Vehicle and/or Address record.
   *-------------------------------------------------------------------------*/
  procedure GetVehicles (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_id;
    t_RelId                             number;
    t_VehicleObjectId                   udt_id;
    t_VehicleIds                        udt_IdList;
    t_InternalAddressObjectIds          udt_IdList;
  begin

    select RenewJobId
    into t_JobId
    from query.r_ABC_LicenseToRenewJobLicense
    where RelationshipId = a_ObjectId;

    if api.Pkg_Columnquery.Value(t_JobId, 'LicenseTypeCode') = '13' and
        api.Pkg_Columnquery.Value(t_JobId, 'OnlineRetailTransitType') in ('Limousine', 'Boat') then
      select
        lv.VehicleId,
        av.InternalStorageAddressObjectId
      bulk collect into t_VehicleIds, t_InternalAddressObjectIds
      from
        query.r_abc_licensevehicle lv
        join query.o_ABC_Vehicle av
            on av.ObjectId = lv.VehicleId
      where lv.LicenseId = api.pkg_ColumnQuery.Value(t_JobId, 'LicenseToRenewObjectId');

      for v in 1..t_VehicleIds.count loop
        -- Copy, then relate Vehicle Object to the Renewal Application.
        extension.pkg_objectupdate.CopyObject(t_VehicleIds(v), t_VehicleObjectId);
        t_RelId := api.pkg_relationshipupdate.New(
            api.pkg_configquery.EndPointIdForName('o_ABC_Vehicle', 'RenewalApplication'),
            t_VehicleObjectId, t_JobId);

        if t_InternalAddressObjectIds(v) is not null then
          -- Relate Internal Storage Address to current Internal Address
          t_RelId := api.pkg_relationshipupdate.New(
              api.pkg_configquery.EndPointIdForName('o_ABC_Vehicle', 'Address'),
              t_VehicleObjectId, t_InternalAddressObjectIds(v));
          api.pkg_ColumnUpdate.SetValue(t_VehicleObjectId, 'OnlineStorageAddress',
              api.pkg_ColumnQuery.Value(t_InternalAddressObjectIds(v), 'FormattedDisplay'));
        end if;
      end loop;
    end if;

  end GetVehicles;

  /*---------------------------------------------------------------------------
    SetRegenerateFees
  -- Description:
  *-------------------------------------------------------------------------*/
  procedure SetRegenerateFees (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_JobId udt_id;
    t_AppRel udt_id;
    t_RenRel udt_id;
    t_AmendRel udt_id;
    t_State  varchar2(60);
  begin
    t_AppRel := api.pkg_configquery.EndPointIdForName('o_ABC_License','NewApplication');
    t_RenRel := api.pkg_configquery.EndPointIdForName('o_ABC_License','Renewal');
    t_AmendRel := api.pkg_configquery.EndPointIdForName('o_ABC_License','AmendApplication');

    begin
        select r.ToObjectId
        into t_JobId
        from api.relationships r
        where r.fromobjectid = a_ObjectId
        and r.endpointid = t_RenRel
        union
        select r.ToObjectId
        from api.relationships r
        where r.fromobjectid = a_ObjectId
        and r.endpointid = t_AppRel
        union
        select r.ToObjectId
        from api.relationships r
        where r.fromobjectid = a_ObjectId
        and r.endpointid = t_AmendRel;
      exception when no_data_found then
        t_JobId := null;
      end;

    t_State := api.pkg_columnquery.Value(a_ObjectId, 'State');

    if t_JobId is not null and t_State = 'Pending' then
       api.pkg_columnupdate.setvalue(t_JobId,'GenerateFees','Y');
    end if;
  end SetRegenerateFees;

  /*-----------------------------------------------------------------------------
  -Set Default Condition Processor
  -----------------------------------------------------------------------------*/
  procedure SetDefaultConditionProcessor (
  a_ObjectId            udt_Id,
  a_AsOfDate            date
  ) is

  t_ConditionProcessor                   udt_Id;
  t_ConditionProcessorRel                udt_Id;
  t_EndPointId                           udt_Id;
  t_JobId                                udt_Id;

  begin

    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');

    select dcp.UserId
      into t_ConditionProcessor
      from query.r_DefaultCondProcessSysSetting dcp
     where SystemSettingsObjectId = (select objectid
                                            from query.o_systemsettings);

    t_EndPointId := api.pkg_configquery.EndPointIdForName(api.pkg_columnquery.value(t_JobId, 'ObjectDefName')
                                                          , 'DefaultConditionsProcessor');

    t_ConditionProcessorRel := api.pkg_relationshipupdate.New(t_EndPointId, t_JobId, t_ConditionProcessor);

  exception when no_data_found then
    raise_application_error(-20000, 'Default Conditions Processor is not avaialable - please contact your system administrator.');

  end SetDefaultConditionProcessor;

  /*---------------------------------------------------------------------------
    Generate License
  *-------------------------------------------------------------------------*/
  procedure GenerateLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is

    t_ReportId                          udt_Id;

  begin

    t_ReportId := extension.pkg_processserver.ScheduleRTFReport('LicenseCertificate', a_ObjectId, a_ObjectId);

  end GenerateLicense;

  /*---------------------------------------------------------------------------
   * RenewalLicenseExpirationDate() -- PUBLIC
   *-------------------------------------------------------------------------*/
  procedure RenewalLicenseExpirationDate (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_LicenseId                         udt_Id;
    t_JobId                             udt_Id;
    t_ExpirationDate                    date;
    t_StartDate                         date;
    t_EndDate                           date;
    t_LicenseTypeObjectId               udt_Id;
    t_ExpirationYear                    number;
    t_LicenseNumber                     varchar2(11);
    t_Objects                           udt_IdList;
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_RenewalDate                       date;
    t_PendingLicenseId                  udt_Id;
  begin

    t_JobId := api.pkg_columnquery.Value(a_ObjectId, 'JobId');
    t_LicenseId := api.pkg_columnquery.Value(t_JobId, 'LicenseObjectId');
    t_LicenseTypeObjectId := api.pkg_columnquery.Value(
        t_LicenseId, 'LicenseTypeObjectId');
    t_LicenseNumber := api.pkg_columnquery.Value(t_LicenseId,
        'LicenseNumberNoGeneration');

    if not api.pkg_search.IntersectByRelColumnValue('j_ABC_RenewalApplication',
        'LicenseToRenew', 'LicenseNumber', t_LicenseNumber || '%', t_SearchResults) then
      null;
    end if;
    t_Objects := api.pkg_Search.SearchResultsToList(t_SearchResults);
    for r in 1..t_Objects.Count loop
      t_PendingLicenseId := api.pkg_columnquery.Value(t_Objects(r), 'LicenseObjectId');
      if api.pkg_columnquery.Value(t_Objects(r), 'StatusName') != 'CANCEL' and
         api.pkg_columnquery.Value(t_PendingLicenseId, 'State') = 'Pending' then
        t_RenewalDate := to_date(api.pkg_columnquery.DateValue(t_Objects(r),
            'CalculatedExpirationDate'));
        if t_ExpirationDate is null then
          t_ExpirationDate := t_RenewalDate;
        else
          if t_RenewalDate > t_ExpirationDate then
            t_ExpirationDate := t_RenewalDate;
          end if;
        end if;
      end if;
    end loop;

    if t_ExpirationDate is null then
       t_ExpirationDate := api.pkg_columnquery.DateValue(
           api.pkg_columnquery.Value(t_JobId, 'LicenseToRenewObjectId'),
               'ExpirationDate');
    end if;

    t_ExpirationYear := to_char(t_ExpirationDate,'yyyy');
    if api.pkg_columnquery.DateValue(t_LicenseId, 'EffectiveDate') is null then
      if api.pkg_columnquery.Value(t_LicenseTypeObjectId,'ExpirationMethod')
          = 'Seasonal' then
        if to_Date(api.pkg_columnquery.Value(
            t_LicenseTypeObjectId, 'ExpirationEndMonth') || '/' ||
            api.pkg_columnquery.Value(t_LicenseTypeObjectId, 'ExpirationEndDay'),
            'MM/DD') >
           to_Date(api.pkg_columnquery.Value(
            t_LicenseTypeObjectId, 'ExpirationStartMonth') || '/' ||
            api.pkg_columnquery.Value(t_LicenseTypeObjectId, 'ExpirationStartDay'),
            'MM/DD') then
          t_ExpirationYear := t_ExpirationYear + 1;
        end if;
        t_StartDate := to_Date(api.pkg_columnquery.Value(
            t_LicenseTypeObjectId, 'ExpirationStartMonth') || '/' ||
            api.pkg_columnquery.Value(t_LicenseTypeObjectId, 'ExpirationStartDay')
            || '/' || t_ExpirationYear, 'MM/DD/YYYY');
        api.pkg_columnupdate.SetValue(t_LicenseId, 'EffectiveDate', t_StartDate);
      else
        api.pkg_columnupdate.SetValue(
            t_LicenseId, 'EffectiveDate', t_ExpirationDate + 1);
      end if;
    end if;

    api.pkg_columnupdate.SetValue(t_LicenseId, 'ExpirationDate',
        api.pkg_columnquery.DateValue(t_LicenseId, 'CalculatedExpirationDate'));

  end RenewalLicenseExpirationDate;

  /*---------------------------------------------------------------------------
    SetDocumentId
  *-------------------------------------------------------------------------*/
  procedure SetDocumentId (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is

    t_ProcessName                   varchar2(100);
    t_JobId                         udt_Id;
    t_LicenseId                     udt_Id;
    t_documentId                    udt_Id;

  begin
  --Select Process Name and JobId for use later
  select pt.Name, p.JobId
    into t_ProcessName, t_JobId
    from api.processes p
    join api.processtypes pt
      on p.ProcessTypeId = pt.ProcessTypeId
   where p.ProcessId = a_ObjectId;

  --Check to see which rel is present in order to determine which select statment to use
  --in order to obtain the license Certificate Document Id
  if extension.pkg_relutils.RelExists(a_ObjectId, 'LicCertificate') = 'Y' then
    --Use the process name to determine which process created the document and get
    --the document Id. The document uses the 'LicCertificate' end point for the
    --generate license process and the send license process
    --else use the Indirect relationship from the send license process.
    if t_ProcessName = 'p_ABC_SendLicense' then
      select sl.LicenseCertificateDocumentLook
        into t_documentId
        from query.p_abc_sendLicense sl
       where sl.ObjectId = a_ObjectId;
    else
      select gl.LicenseCertificateDocumentId
        into t_documentId
        from query.p_ABC_GenerateLicense gl
       where gl.ObjectId = a_ObjectId;
    end if;
  elsif extension.pkg_relutils.RelExists(a_ObjectId, 'LicCertificateIND') = 'Y' then
      select gl.LicenseCertificateDocumentId
        into t_documentId
        from query.p_ABC_GenerateLicense gl
       where gl.jobId = t_JobId;
  end if;

  --get the licenseId that will be the active license
  t_LicenseId := api.pkg_columnquery.Value(t_JobId, 'LicenseObjectId');

  --Set the documentId on the new license
  api.pkg_columnupdate.SetValue(t_LicenseId, 'LicenseCertificateDocId_New', t_DocumentId);

  end SetDocumentId;

  ----------------------------------------------------------------------------
  --  RemoveFees() -- PUBLIC
  --  Removes all the fees on a job - Called when the job moves into the
  --  cancelled status
  ----------------------------------------------------------------------------
  procedure RemoveFees (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_TransactionId                     udt_Id;
  begin
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');

    -- Loop through all system generated fees on the job that have not been paid, adjusted, etc.
    -- and adjust them down to zero
    if api.pkg_ColumnQuery.Value(t_JobId, 'HasNonZeroBalances') = 'Y' then
      for c in (select a.FeeId,
                       a.Amount + a.AdjustedAmount + a.PaidAmount NetAmount
                  from api.Fees a
                  where a.JobId = t_JobId
                    and a.SystemGenerated = 'Y'
                    and a.PostedDate is null) loop
        t_TransactionId := api.pkg_FeeUpdate.Adjust(c.FeeId, -c.NetAmount, 'Adjusted by system due to cancellation of job', sysdate);
       end loop;
     end if;

  end RemoveFees;

  ----------------------------------------------------------------------------
  --  RemoveFeesOnWithdrawal
  --  Removes all the fees on a job - Called when the job moves into the
  --  Withdrawn status
  ----------------------------------------------------------------------------
  procedure RemoveFeesOnWithdrawal (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_TransactionId                     udt_Id;
  begin
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');

    -- Loop through all system generated fees on the job that have not been paid, adjusted, etc.
    -- and adjust them down to zero
    if api.pkg_ColumnQuery.Value(t_JobId, 'HasNonZeroBalances') = 'Y' then
      for c in (select a.FeeId,
                       a.Amount + a.AdjustedAmount NetAmount
                  from api.Fees a
                  where a.JobId = t_JobId
                    and a.SystemGenerated = 'Y'
                    and a.PostedDate is null
                    and (select count(*)
                          from api.FeeTransactions b
                         where b.FeeId = a.FeeId
                           and b.TransactionType = 'Pay') = 0) loop
        t_TransactionId := api.pkg_FeeUpdate.Adjust(c.FeeId, -c.NetAmount, 'Adjusted by system due to withdrawal of job', sysdate);
       end loop;
     end if;

  end RemoveFeesOnWithdrawal;

  ----------------------------------------------------------------------------
  --  SetSendLicProcessId() -- PUBLIC
  --  Set the Send License ProcessId on the license object
  --  to allow relating the documents from a major application.
  ----------------------------------------------------------------------------
  procedure SetSendLicProcessId (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_CurrentJobId                      udt_Id;
    t_LicenseId                         udt_Id;
    t_CurrentObjectDefname              varchar2(100);

  begin
    --Get Current Job information
    t_CurrentJobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    t_CurrentObjectDefName := api.pkg_ColumnQuery.Value(t_CurrentJobId, 'ObjectDefName');
    t_LicenseId := api.pkg_ColumnQuery.Value(t_CurrentJobId, 'LicenseObjectId');

    --If currect job is amendment check to see what the previous job was,
    --If it was an amendment set the MajorSendLicProcessId to the last amendment
    if t_CurrentObjectDefName = 'j_ABC_AmendmentApplication' then
      if api.Pkg_Columnquery.Value(t_CurrentJobId, 'MajorAmendment') = 'Y' then
        api.pkg_columnupdate.SetValue(t_LicenseId, 'MajorSendLicProcessId', a_ObjectId);
      end if;
    end if;

    --If the Current job is not an amendment it is a major job set MajorSendLicProcessId
    if t_CurrentObjectDefName != 'j_ABC_AmendmentApplication' then
       api.pkg_columnupdate.SetValue(t_LicenseId, 'MajorSendLicProcessId', a_ObjectId);
    end if;

    --Set Most Recent Send License Process Id
    api.pkg_columnupdate.SetValue(t_LicenseID, 'RecentSendLicProcessID', a_ObjectId);

  end SetSendLicProcessId;

  ----------------------------------------------------------------------------
  --  JobAppSummary() -- PUBLIC
  --  Create relationship between the app summary report
  --  and the job that created it.
  ----------------------------------------------------------------------------
  procedure JobAppSummary (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_ProcessID                         udt_Id;
    t_JobId                             udt_Id;
    t_AppSummaryId                      udt_Id;
    t_EndPointId                        udt_Id;
    t_JobObjectDefName                  varchar2(100);
    t_RelId                             udt_Id;
  begin

     select ProcessId, DocumentId
      into t_ProcessId, t_AppSummaryId
      from query.r_Renwalappsummary
     where RelationshipId = a_ObjectId;

    t_JobId := api.pkg_ColumnQuery.Value(t_ProcessId, 'JobId');
    t_JobObjectDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_EndPointId := api.pkg_configquery.EndPointIdForName(t_JobObjectDefName, 'AppSummaryReport');

    t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_JobId, t_AppSummaryId);

  end JobAppSummary;

    ----------------------------------------------------------------------------
  --  SubJobAppSummary() -- PUBLIC
  --  Create relationship between the app summary report
  --  and the job that created it.
  ----------------------------------------------------------------------------
  procedure SubJobAppSummary (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_ProcessID                         udt_Id;
    t_JobId                             udt_Id;
    t_AppSummaryId                      udt_Id;
    t_EndPointId                        udt_Id;
    t_JobObjectDefName                  varchar2(100);
    t_RelId                             udt_Id;
  begin

     select ProcessId, DocumentId
      into t_ProcessId, t_AppSummaryId
      from query.r_subapplicationsummary
     where RelationshipId = a_ObjectId;

    t_JobId := api.pkg_ColumnQuery.Value(t_ProcessId, 'JobId');
    t_JobObjectDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_EndPointId := api.pkg_configquery.EndPointIdForName(t_JobObjectDefName, 'AppSummaryReport');

    t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_JobId, t_AppSummaryId);

  end SubJobAppSummary;

  -----------------------------------------------------------------------------
  -- RegistrationVideoConstraint
  -- Ensure that only one tutorial video can be marked as 'Y'
  -----------------------------------------------------------------------------
  procedure RegistrationVideoConstraint (
    a_ObjectId                           udt_Id,
    a_AsOfDate                           date
  ) is
    t_RegistrationVideo                  varchar2(1) := api.pkg_columnquery.Value(a_ObjectId, 'IsRegistrationVideo');

  begin
    for c in (select v.objectid
                from query.o_ABC_Video v
               where v.objectid != a_ObjectId
                 and t_RegistrationVideo = 'Y') loop

        api.pkg_columnupdate.SetValue(c.objectid, 'IsRegistrationVideo', 'N');

    end loop;

  end RegistrationVideoConstraint;

  /*---------------------------------------------------------------------------
    GetPermitVehicles
  -- Description: This is triggered on creation of Permit Renewal Application from
  -- the Public site. Its purpose is to create new vehicle records and relate the
  -- new vehicle record to the renewal. In this case we can keep an audit trail of any
  -- changes that are done to the Vehicle record.
  *-------------------------------------------------------------------------*/
  procedure GetPermitVehicles(
    a_JobId                             udt_Id
  ) is
    t_RelId                             number;
    t_VehicleObjectId                   udt_id;
  begin
    for v in (select av.ObjectId, av.VIN, av.InsigniaNumber, av.MakeModelYear, av.OwnedOrLeasedLimousine, av.StateRegistration, av.StateOfRegistration
                from query.r_ABC_PermitVehicle pv
                join query.o_ABC_Vehicle av on av.ObjectId = pv.VehicleObjectId
               where pv.PermitObjectId = api.Pkg_Columnquery.Value(a_JobId, 'PermitToRenewObjectId')
             ) loop
--    Copy Vehicle Object, then relate it to the Renewal Application
             extension.pkg_objectupdate.CopyObject(v.ObjectId, t_VehicleObjectId);
             t_RelId := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName('o_ABC_Vehicle', 'PermitRenewal'), t_VehicleObjectId, a_JobId);
             api.pkg_columnupdate.SetValue (t_VehicleObjectId,'VIN', v.VIN);
             api.pkg_columnupdate.SetValue (t_VehicleObjectId,'InsigniaNumber', v.InsigniaNumber);
             api.pkg_columnupdate.SetValue (t_VehicleObjectId,'MakeModelYear', v.MakeModelYear);
             api.pkg_columnupdate.SetValue (t_VehicleObjectId,'OwnedOrLeasedLimousine', v.OwnedOrLeasedLimousine);
             api.pkg_columnupdate.SetValue (t_VehicleObjectId,'StateRegistration', v.StateRegistration);
             api.pkg_columnupdate.SetValue (t_VehicleObjectId,'StateOfRegistration', v.StateOfRegistration);
       end loop;

  end GetPermitVehicles;

  /*---------------------------------------------------------------------------
    GetPermitCoOpMembers
  -- Description: This is triggered on creation of Permit Renewal Application from
  -- the Public site. Its purpose is to create new CoOp Member records and relate the
  -- new Member record to the Renewal. In this case we can keep an audit trail of any
  -- changes that are done to the Member record.
  *-------------------------------------------------------------------------*/
  procedure GetPermitCoOpMembers(
    a_JobId                             udt_Id
  ) is
    t_RelId                             number;
    t_MemberObjectId                    udt_id;
    t_EndPointId                        udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_CoOpMember', 'PermitRenewal');
    t_MemberDefId                       udt_id := api.pkg_configquery.ObjectDefIdForName('o_ABC_CoOpMember');
  begin
    for m in (select l.LicenseeObjectId, l.LicenseNumber, l.Licensee, l.ObjectId, pl.CoOpMemberEffectiveDate
                from query.r_ABC_PermitCoOpLicenses pl
                join query.o_ABC_License l on l.ObjectId = pl.CoOpMemberObjectId
               where pl.PermitObjectId = api.Pkg_Columnquery.Value(a_JobId, 'PermitToRenewObjectId')
             ) loop
--    Copy Member Object, then relate it to the Renewal Job
             t_MemberObjectId := api.pkg_objectupdate.New(t_MemberDefId);
             t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_MemberObjectId, a_JobId);
             api.pkg_columnupdate.SetValue (t_MemberObjectId,'LicenseNumber', REPLACE(m.LicenseNumber, '-'));
             api.pkg_columnupdate.SetValue (t_MemberObjectId,'Licensee', m.licensee);
             api.pkg_ColumnUpdate.SetValue (t_RelId, 'MemeberEffectiveDate', m.coopmembereffectivedate);
       end loop;

  end GetPermitCoOpMembers;

  /*---------------------------------------------------------------------------
    GetPermitRepresentatives
  -- Description: This is triggered on creation of Permit Renewal Application from
  -- the Public site.
  *-------------------------------------------------------------------------*/
  procedure GetPermitRepresentatives(
    a_JobId                             udt_Id
  ) is
    t_RelId                             number;
    t_CTSRepObjectId                    udt_id;
    t_EndPointId                        udt_id := api.pkg_configquery.EndPointIdForName('o_abc_ctsrepresentatives', 'PermitRenewal');
    t_CTSRepDefId                       udt_id := api.pkg_configquery.ObjectDefIdForName('o_abc_ctsrepresentatives');
  begin
    for i in (select ctsr.Name
                from query.r_ABC_CTSRepPermit cts
                join query.o_abc_ctsrepresentatives ctsr
                  on cts.CTSRepresentativeObjectId = ctsr.ObjectId
               where cts.PermitObjectId = api.Pkg_Columnquery.Value(a_JobId, 'PermitToRenewObjectId')
             ) loop
--    Copy Member Object, then relate it to the Renewal Job
             t_CTSRepObjectId := api.pkg_objectupdate.New(t_CTSRepDefId);
             t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_CTSRepObjectId, a_JobId);
             api.pkg_columnupdate.SetValue (t_CTSRepObjectId, 'Name', i.name);
       end loop;

  end GetPermitRepresentatives;

  /*---------------------------------------------------------------------------
    GetPermitProducts
  -- Description: This is triggered on creation of Permit Renewal Application from
  -- the Public site.
  *-------------------------------------------------------------------------*/
  procedure GetPermitProducts(
    a_JobId                             udt_Id
  ) is
    t_RelId                             number;
    t_ProdObjectId                    udt_id;
    t_EndPointId                        udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_ProductOnline', 'PermitRenewal');
    t_ProdDefId                       udt_id := api.pkg_configquery.ObjectDefIdForName('o_ABC_ProductOnline');
  begin
    for i in (select prod.Name, prod.RegistrationNumber
                from query.r_ABC_PermitProduct pr
                join query.o_abc_product prod
                  on pr.ProductObjectId = prod.ObjectId
               where pr.PermitObjectId = api.Pkg_Columnquery.Value(a_JobId, 'PermitToRenewObjectId')
             ) loop
--    Copy Member Object, then relate it to the Renewal Job
             t_ProdObjectId := api.pkg_objectupdate.New(t_ProdDefId);
             t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_ProdObjectId, a_JobId);
             api.pkg_columnupdate.SetValue (t_RelId, 'ProductDescription', i.name);
             api.pkg_ColumnUpdate.SetValue (t_RelId, 'ProductNumber', i.registrationnumber);
       end loop;

  end GetPermitProducts;

/*---------------------------------------------------------------------------
    GetPermitSolicitors
  -- Description: This is triggered on creation of Permit Renewal Application from
  -- the Public site for CTW Permits
  *-------------------------------------------------------------------------*/
  procedure GetPermitSolicitors(
    a_JobId                             udt_Id
  ) is
    t_RelId                             number;
    t_PermitToRenewObjId                udt_id := api.Pkg_Columnquery.Value(a_JobId, 'PermitToRenewObjectId');
    t_CTWOwnerEndPointId                udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_LegalEntity', 'PermitRenewal');
    t_CTWSolicitorEndPointId            udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitRenewalJob');
    t_SolicitorPermitId                 udt_id;
  begin
--  Copy any Solicitors to the Renewal job
    for i in (select p1.ObjectId, p1.SolicitorObjectId
                from query.r_ABC_SolicitorPermit r
                join query.o_abc_Permit p1
                  on p1.objectid = r.SolicitorPermitObjectId
               where r.PermitObjectId = t_PermitToRenewObjId
             ) loop
       t_SolicitorPermitId := GetLatestSolicitorPermit (i.Objectid, i.SolicitorObjectId);
       if t_SolicitorPermitId > 0 then
         t_RelId := api.pkg_relationshipupdate.New(t_CTWSolicitorEndPointId, t_SolicitorPermitId, a_JobId);
       end if;
    end loop;

--  Copy any Owners to the Renewal job
    for i in (select le.ObjectId, le.FormattedName Name
                from query.r_ABC_PermitOwner r
                join query.o_abc_legalentity le
                  on le.objectid = r.OwnerLEObjectId
               where r.PermitObjectId = t_PermitToRenewObjId
             ) loop
       t_RelId := api.pkg_relationshipupdate.New(t_CTWOwnerEndPointId, i.Objectid, a_JobId);
    end loop;

  end GetPermitSolicitors;

  /*---------------------------------------------------------------------------
    GetPermitRenewalData
  -- Description: This is triggered on creation of Permit Renewal Application from
  -- the Public site. Its purpose is to create new CoOp Member or Vehicle records
  -- and relate the new records to the Renewal. In this case we can keep an audit
  -- trail of any changes that are done to the new records.
  *-------------------------------------------------------------------------*/
  procedure GetPermitRenewalData(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_id;
  begin
    select PermitRenewJobId
      into t_JobId
      from query.r_ABC_PermitToRenewJobPermit
     where RelationshipId = a_ObjectId;
    if api.Pkg_Columnquery.Value(t_JobId, 'PermitTypeRequiresVehicle') = 'Y' then
       GetPermitVehicles(t_JobId);
    end if;
    if api.Pkg_Columnquery.Value(t_JobId, 'PermitTypeCode') = 'COOP' then
       GetPermitCoOpMembers(t_JobId);
    end if;
    if api.Pkg_Columnquery.Value(t_JobId, 'PermitTypeCode') = 'CTS' then
       GetPermitRepresentatives(t_JobId);
    end if;
    if api.Pkg_Columnquery.Value(t_JobId, 'PermitTypeRequiresProducts') = 'Y' then
       GetPermitProducts(t_JobId);
    end if;
    if api.Pkg_Columnquery.Value(t_JobId, 'PermitTypeCode') = 'CTW' then
       GetPermitSolicitors(t_JobId);
    end if;

  end GetPermitRenewalData;

  /*---------------------------------------------------------------------------
    Generate Batch Renewal Notification
  *-------------------------------------------------------------------------*/
  procedure GenerateBatchRenewalDocument (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_BatchType                         varchar2
  ) is

    t_ReportId                          udt_Id;
    t_JobId                             udt_Id;

  begin

    --Get jobid
    t_JobId := api.pkg_columnquery.Value(a_ObjectId, 'JobId');

    if a_BatchType = 'License - Municipality Issued' or a_BatchType = 'License - State Issued' then
      t_ReportId := extension.pkg_processserver.ScheduleRTFReport('LicenseRenewalNotification', t_JobId, t_JobId);
    elsif a_BatchType = 'Permit' then
      t_ReportId := extension.pkg_processserver.ScheduleRTFReport('PermitRenewalNotification', t_JobId, t_JobId);
    else
      t_ReportId := extension.pkg_processserver.ScheduleRTFReport('ProductRenewalNotification', t_JobId, t_JobId);
    end if;

  end GenerateBatchRenewalDocument;

  ---------------------------------------------------------------------------------------------
  -- CopyPermit
  --  this is triggered on "SubmitApplication" and "PaymentSucceeded" boolean column on the Permit Renewal Job
  --  Created by Josh Camps 09/15/2015
  ---------------------------------------------------------------------------------------------
  procedure CopyPermit (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
  t_JobId                 udt_Id;
  t_PermitToCopy          udt_Id;
  t_NewPermit             udt_Id;
  t_LicenseId             udt_id;
  t_LicenseEndPointId     udt_id := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit', 'License');
  t_PermitDefId           udt_Id := api.pkg_configquery.ObjectDefIdForName('o_ABC_Permit');
  t_PermitSpecialCondId   udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitSpecialConditions');
  t_PermitCondTypeId      udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_PermitSpecialConditions', 'ConditionType');
  t_GenerationalPermitEP  udt_id := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit', 'AssociatedGenerationalPermit');
  t_PrimaryPermitEP       udt_id := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit', 'AssociatedPermit');
  t_AssociatedPermit      udt_id;
  t_RelId                 udt_Id;
  t_StoredValue           varchar2(4000);
  t_JobDef                varchar2(200);
  t_OldExpirationDate     date;
  t_EffectiveDate         date;
  t_ColumnDefs            udt_StringList;
  t_EndpointId            udt_Id;
  t_PermitTypeCode        varchar2(20);
  t_ObjectId              udt_Id;
  t_SpecialConditionsId   udt_id;
  t_InsigniaNumber        integer;

  begin

    -- Set object id coming in to t_JobId to make this easier to read.
    t_JobId := a_ObjectId;
    -- Get the job def for creation of needed rels
    t_JobDef := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');

    --Get the permit type needed for copy of details later.
    t_PermitTypeCode := api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeCode');

    --set the application recived date on the job if it is null.
    if api.pkg_ColumnQuery.Value(t_Jobid, 'ApplicationReceivedDate') is null then
      api.pkg_ColumnUpdate.SetValue(t_JobId, 'ApplicationReceivedDate', sysdate);
    end if;

    -- Get the object Id for the License that is being copied (Old License)
    t_PermitToCopy := api.pkg_ColumnQuery.Value(t_JobId, 'PermitToRenewObjectId');

    -- Create the new permit to copy all the information to
    t_NewPermit := api.pkg_objectupdate.New(t_PermitDefId);

    -- Relate new Permit to Job
    t_RelId := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName(t_JobDef, 'RenewToPermit'), t_Jobid, t_NewPermit);

    -- Copy all the stored details from the old permit to the new permit.
    -- Exclude any dup_, boolean, zzz columns and the state.
    for i in (select cd.name
                from api.columndefs cd
               where lower(cd.ObjectDefName) = 'o_abc_permit'
                 and columndeftypename = 'Stored'
                 and lower(cd.name) not like '%dup_%'
                 and lower(cd.name) not like '%zzz%'
                 and lower(cd.name) != 'state'
                 and lower(cd.name) != 'startinginsignianumber'
                 and cd.FormattedDataType != 'boolean'
                 and cd.FormattedDataType != 'date') loop

      t_StoredValue := api.pkg_ColumnQuery.Value(t_PermitToCopy, i.name);
      api.pkg_ColumnUpdate.SetValue(t_NewPermit, i.name, t_StoredValue);

    end loop;

    -- Set the state of the new permit
    api.pkg_ColumnUpdate.SetValue(t_NewPermit, 'State', 'Pending');

    -- Copy the stored rels from old permit that are needed.
    for i in (select r.ToObjectId, rd.ToEndPointId, rd.ToEndPointName
                  from api.relationships r
                  join api.relationshipdefs rd on rd.RelationshipDefId = r.RelationshipDefId
                 where r.FromObjectId = t_PermitToCopy
                   and rd.ToEndPointName in ('CoOpOfficer'
                                           , 'CoOpLicense'
                                           , 'County'
--                                           , 'License'
                                           , 'Municipality'
--                                           , 'PermitSpecialConditions'
                                           , 'SelectSecondaryLicType'
                                           , 'EventLocationAddress'
                                           , 'TAPPermit'
                                           , 'PermitType'
                                           , 'CTSRepresentative'
                                           , 'Permittee'
                                           , 'Product'
                                           , 'PromoterAddress'
                                           , 'SellersLicense'
                                           , 'BuyersPermit'
                                           , 'Solicitor'
                                           , 'TAPPermittee'
                                           , 'HistoricalCoOpLicense'
                                           )) loop

      t_RelId := api.pkg_relationshipupdate.New(i.toendpointid, t_NewPermit, i.toobjectid);
      if i.ToEndPointName in ('CoOpLicense', 'HistoricalCoOpLicense') then
        for co in (select coop.CoOpMemberEffectiveDate
                    from query.r_ABC_PermitCoOpLicenses coop
                   where coop.CoOpMemberObjectId = i.toobjectid
                     and coop.PermitObjectId = t_PermitToCopy) loop

          api.pkg_ColumnUpdate.SetValue(t_RelId, 'CoOpMemberEffectiveDate', co.coopmembereffectivedate);

        end loop;
        for co in (select coop.CoOpMemberEffectiveDate
                    from query.r_ABC_PermitHistorCoOpLicenses coop
                   where coop.CoOpMemberObjectId = i.toobjectid
                     and coop.PermitObjectId = t_PermitToCopy) loop
          api.pkg_ColumnUpdate.SetValue(t_RelId, 'CoOpMemberEffectiveDate', co.coopmembereffectivedate);
        end loop;
      end if;
    end loop;

    --Get the Latest and Greatest version of the related License
    if api.pkg_columnquery.value(t_PermitToCopy, 'LicenseObjectId') is not null then
       t_LicenseId := api.pkg_columnquery.value(t_PermitToCopy, 'LicenseObjectId');
       --t_LicenseId := GetLatestLicense (api.pkg_columnquery.Value(t_PermitToCopy, 'LicenseObjectId'), t_PermitToCopy);
       t_RelId := api.pkg_relationshipupdate.New(t_LicenseEndPointId, t_NewPermit, t_LicenseId);
    end if;

    --Copy Special Conditions (if any)
    --To prevent default conditions created.
    api.pkg_ColumnUpdate.SetValue(t_NewPermit, 'DefaultCondCreated' ,'Y');
    for sc in (select psc.SpecialConditionsId
                from query.r_Abc_Permitspecialconditions psc
               where psc.PermitId = t_PermitToCopy
             )
    loop
       t_SpecialConditionsId := api.Pkg_Objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ABC_PermitSpecialConditions'));
       api.pkg_columnupdate.SetValue(t_SpecialConditionsId, 'SortOrder', api.pkg_columnquery.NumericValue(sc.specialconditionsid,'SortOrder'));
       api.pkg_columnupdate.SetValue(t_SpecialConditionsId, 'ConditionDescription', api.pkg_columnquery.Value(sc.specialconditionsid,'ConditionDescription'));
       t_RelId := api.pkg_relationshipupdate.New(t_PermitSpecialCondId, t_NewPermit, t_SpecialConditionsId);
       for ct in (select pct.ConditionTypeObjectId
                  from   query.r_ABC_PermitCondConditionType pct
                  where  pct.PermitSpecialConditionObjectId = sc.specialconditionsid
                 )
       loop
          t_RelId := api.pkg_relationshipupdate.New(t_PermitCondTypeId, t_SpecialConditionsId, ct.conditiontypeobjectid);
       end loop;
     end loop;

    --If the renewal was processed through the public site copy the needed details from the job.
    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      case t_PermitTypeCode
        when 'BE' then
          t_ColumnDefs(1) := 'OnlineNumberOfIndividuals';
          api.pkg_ColumnUpdate.SetValue(t_NewPermit,
                                    'NumberOfIndividuals',
                                    api.pkg_ColumnQuery.Value(t_JobId, 'OnlineNumberOfIndividuals'));
        when 'FP' then
          api.pkg_ColumnUpdate.SetValue(t_NewPermit,
                                    'FPNumberOfGallons',
                                    api.pkg_ColumnQuery.Value(t_JobId, 'OnlineFPNumberOfGallons'));
        else
          null;
      end case;
    end if;

    --Associated with Insignia Permits
   if api.pkg_columnquery.Value(t_NewPermit, 'PermitTypeCode') = 'LTP' then
      for ip in (select *
                 from   query.r_ABC_AssociatedPermit r
                 where  r.AsscPermitObjectId = t_PermitToCopy
                ) loop
         if api.pkg_columnquery.Value(ip.primarypermitobjectid, 'State') = 'Active' then
            api.pkg_relationshipupdate.Remove(ip.relationshipid);
            t_RelId := api.pkg_relationshipupdate.New(t_GenerationalPermitEP, ip.Primarypermitobjectid, t_PermitToCopy);
            t_RelId := api.pkg_relationshipupdate.New(t_PrimaryPermitEP, ip.Primarypermitobjectid, t_NewPermit);
         end if;
      end loop;
    end if;
    if api.pkg_columnquery.Value(t_NewPermit, 'RequiresVehicle') = 'Y' then
       begin
          select r.AsscPermitObjectId
          into   t_AssociatedPermit
          from   query.r_ABC_AssociatedPermit r
          where  r.PrimaryPermitObjectId = t_PermitToCopy;
          t_RelId := api.pkg_relationshipupdate.New(t_PrimaryPermitEP, t_NewPermit, t_AssociatedPermit);
       exception
          when no_data_found then
             null;
       end;
    end if;

    --Copy Vehicle
    for v in (select ve.VehicleObjectId
                from query.r_ABC_PermitRenewalVehicle ve
               where ve.PermitRenewalObjectId = t_JobId) loop

     t_InsigniaNumber := api.pkg_columnquery.NumericValue(v.vehicleobjectid, 'InsigniaNumber');
     t_ObjectId := api.Pkg_Objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ABC_Vehicle'));

     for i in (select cd.name
                 from api.columndefs cd
                where lower(cd.ObjectDefName) = 'o_abc_vehicle'
                  and columndeftypename = 'Stored') loop

        t_StoredValue := api.pkg_ColumnQuery.Value(v.vehicleobjectid, i.name);
        api.pkg_ColumnUpdate.SetValue(t_ObjectId, i.name, t_StoredValue);

      end loop;
      api.pkg_columnupdate.SetValue (t_Objectid, 'PreviousInsigniaNumber', t_InsigniaNumber);
      api.pkg_columnupdate.RemoveValue(t_ObjectId, 'InsigniaNumber');
      t_RelId := api.pkg_relationshipupdate.New(api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Vehicle')
                                    , t_NewPermit
                                    , t_ObjectId);

    end loop;

    -- Copy existing (non deleted) Solicitor and/or Owner relationships from Renewal Job to new Permit.
    -- Any Solicitors and/or Owners that were added by the Public User should be added manually later.
    t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'SolicitorPermit');
    for sp in (select r.PermitId
               from   query.r_ABC_PermitRenewalSolicitor r
               where  r.PermitRenewalObjectId = t_JobId
              ) loop
       t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_NewPermit, sp.permitid);
    end loop;
    t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'LegalEntityOwner');
    for o in (select r.OwnerObjectId
               from   query.r_ABC_PermitRenewalOwner r
               where  r.PermitRenewalObjectId = t_JobId
              ) loop
       t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_NewPermit, o.ownerobjectid);
    end loop;

    --Copy the Stored rels from the job that are needed.
    for i in (select r.ToObjectId, rd.ToEndPointId, rd.ToEndPointName
                  from api.relationships r
                  join api.relationshipdefs rd on rd.RelationshipDefId = r.RelationshipDefId
                 where r.FromObjectId = t_JobId
                   and rd.ToEndPointName in ('OnlineEventDate'
                                           , 'OnlineRainDate'
                                           , 'CTSRepOnline'
                                           )) loop

            select ep.EndPointId
              into t_EndpointId
              from api.endpoints ep
             where ep.FromObjectDefId = api.pkg_ColumnQuery.Value(i.ToObjectId, 'ObjectDefId')
               and ep.ToObjectDefId = api.pkg_configquery.ObjectDefIdForName('o_ABC_Permit')
               and ep.Name like
                  (case i.toendpointname
                     when 'OnlineEventDate' then 'EventDate_Permit'
                     when 'OnlineRainDate' then 'RainDate_Permit'
                     else '%%'
                   end);

      if api.pkg_ColumnQuery.Value(i.ToObjectId, 'ObjectDefName') = 'o_ABC_EventDate' then

        t_ObjectId := api.pkg_objectupdate.New(api.pkg_configquery.ObjectDefIdForName('o_ABC_EventDate'));

        for e in (select cd.name
                 from api.columndefs cd
                where cd.ObjectDefName = 'o_ABC_EventDate'
                  and columndeftypename = 'Stored') loop
          t_StoredValue := api.pkg_ColumnQuery.Value(i.toobjectid, e.name);
          api.pkg_ColumnUpdate.SetValue(t_ObjectId, e.name, t_StoredValue);
        end loop;

        t_RelId := api.pkg_relationshipupdate.New(t_EndpointId, t_ObjectId, t_NewPermit);
      else
        t_RelId := api.pkg_relationshipupdate.New(t_EndpointId, i.toobjectid, t_NewPermit);
      end if;

    end loop;
  end CopyPermit;

  ---------------------------------------------------------------------------------
  -- ApprovePermit
  --  this is triggered on status change to approved on the Permit Renewal Job
  --  Created by Josh Camps 09/15/2015
  ---------------------------------------------------------------------------------
  procedure ApprovePermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_NewPermitId                       udt_Id;
    t_OldPermitId                       udt_Id;
    t_LicenseId                         udt_id;
    t_RelationshipId                    udt_id;
    t_IssueDate                         date;
    t_OriginalIssueDate                 date;
    t_EffectiveDate                     date;
    t_ExpirationDate                    date;
    t_EndPointId                        udt_id :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'License');
    t_GenerationalPermitEP              udt_id :=
        api.pkg_configquery.EndPointIdForName ('o_ABC_Permit',
            'AssociatedGenerationalPermit');
    t_PrimaryPermitEP                   udt_id :=
        api.pkg_configquery.EndPointIdForName ('o_ABC_Permit', 'AssociatedPermit');
    t_LicenseNumber                     varchar2(50);
    t_AssociatedPermitId                udt_id;
    t_InsigniaNumberList                udt_IdList;
    t_Vehicles                          number;
    t_ToEndPointId                      udt_id;
    t_RelId                             udt_id;
    t_VehicleId                         udt_id;
    t_VehicleDefId                      udt_id :=
        api.pkg_configquery.ObjectDefIdForName ('o_ABC_Vehicle');
    t_InsigniaPermitId                  udt_id;
    t_ObjectDefId                       integer :=
        api.pkg_configquery.ObjectDefIdForName('o_ABC_PermitCancellation');
    t_Reason                            varchar2(400);
    t_ObjectId                          udt_id;
    t_RenewEndpointId                   integer :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitRenewal');
    t_CancelEndpointId                  integer :=
        api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'PermitCancellation');
    t_PermitTypeId                      udt_id;
    t_SearchRes                         api.pkg_definition.udt_idlist;
    t_vin                               varchar2(25);
    t_CreateNewVehicle                  varchar2(1);
    t_TAPObjectId                       udt_id;
    t_ConsolidatePermits                varchar2(1);
    t_PermitNumber                      varchar2(50);
    t_ActivePermits                     udt_idList;
    t_ConsolidatedPermitId              udt_id;
    t_RenewalCount                      integer;
  begin

    -- Get the job id from the process that this is running from
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    -- Get the permit object Id's from the job
    t_NewPermitId := api.pkg_ColumnQuery.Value(t_JobId, 'PermitObjectId');
    t_PermitNumber := api.pkg_columnquery.Value(t_NewPermitId, 'PermitNumber');
    t_OldPermitId := api.pkg_ColumnQuery.value(t_JobId, 'PermitToRenewObjectId');
    t_LicenseId := api.pkg_columnquery.value(t_NewPermitId, 'LicenseObjectId');
    t_LicenseNumber := api.pkg_columnquery.Value(t_NewPermitId, 'LicenseNumber');
    t_PermitTypeId := api.pkg_columnquery.value(t_NewPermitId, 'PermitTypeObjectId');
    -- Check if all fees are paid
    if api.pkg_columnquery.Value(t_JobId, 'HasNonZeroBalances') = 'Y' then
       api.pkg_errors.RaiseError(-20000, 'Before you go on, ensure all fees are paid.');
    end if;

    --Get the active version of the license
    if t_LicenseNumber is not null then
    -- Set Original License Number if null
      if api.pkg_columnquery.Value(t_NewPermitId, 'OriginalLicenseNumber') is null then
        api.pkg_columnupdate.SetValue(t_NewPermitId, 'OriginalLicenseNumber',
            nvl(api.pkg_columnquery.Value(t_OldPermitId, 'LicenseNumber'),
                 api.pkg_columnquery.Value(t_NewPermitId, 'LicenseNumber')));
      end if;
      select pl.RelationshipId
      into t_RelationshipId
      from query.r_PermitLicense pl
      where pl.PermitObjectId = t_NewPermitId
      and pl.LicenseObjectId = t_LicenseId;
      api.pkg_relationshipupdate.Remove(t_relationshipId);
      t_LicenseId := GetLatestLicense (t_LicenseId, t_NewPermitId);
      if t_LicenseId = 0 then
        api.pkg_errors.RaiseError(-20000, 'No Active License to associate');
      end if;
      t_RelationshipId := api.pkg_relationshipupdate.New(t_EndPointId, t_NewPermitId,
          t_LicenseId);
    end if;

    --Get the latest version of the TAP
    t_TAPObjectId := api.pkg_columnquery.Value(t_NewPermitId, 'PermitObjectId_TAP');
    if t_TAPObjectId is not null
        and api.pkg_columnquery.Value(t_TAPObjectId,'State') = 'Active' then
      t_TAPObjectId := api.pkg_columnquery.Value(t_NewPermitId, 'PermitObjectId_TAP');
      for p in (
                select r.PermitObjectId
                from query.r_Abc_Tappermit r
                where r.TAPPermitObjectId = t_TAPObjectId
               ) loop
        if api.pkg_columnquery.Value(p.permitobjectid, 'State') = 'Active' and
            api.pkg_columnquery.Value(p.permitobjectid, 'PermitTypeObjectId') =
                t_PermitTypeId then
          t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
              'InsigniaPermit');
          t_RelId := api.pkg_relationshipupdate.New(t_ToEndPointId, t_NewPermitId,
              p.permitobjectid);
          api.pkg_columnupdate.SetValue(t_NewPermitId, 'ExpirationDate',
              api.pkg_columnquery.DateValue(p.permitobjectid, 'ExpirationDate'));
          exit;
        end if;
      end loop;
    elsif t_TAPObjectId is not null then
      api.pkg_errors.RaiseError(-20000, 'No Active Temporary Authorization to'
          || ' Operate permit to associate.');
    end if;

    -- Get the latest version of the Associated Permit
    t_AssociatedPermitId := api.pkg_columnquery.Value(t_NewPermitId,
        'AssociatedPermitObjectId');
    if t_AssociatedPermitId is not null and
       api.pkg_columnquery.Value(t_AssociatedPermitId, 'IsActive') != 'Y' then
      t_EndPointId := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit',
          'AssociatedPermit');
       select pl.RelationshipId
       into t_RelationshipId
       from query.r_ABC_AssociatedPermit pl
       where pl.PrimaryPermitObjectId = t_NewPermitId
       and pl.AsscPermitObjectId = t_AssociatedPermitId;
       api.pkg_relationshipupdate.Remove(t_relationshipId);
       t_AssociatedPermitId := GetLatestPermit (t_NewPermitId, t_AssociatedPermitId);
       if t_AssociatedPermitId = 0 then
          api.pkg_errors.RaiseError(-20000, 'NO Active permit to associate.');
       else
        t_RelationshipId := api.pkg_relationshipupdate.New (t_EndPointId, t_NewPermitId,
            t_AssociatedPermitId);
       end if;
    end if;

    -- If it is the Associated Permit that was renewed create new relationship(s)
    -- Associated with Insignia Permits
    if api.pkg_columnquery.Value(t_NewPermitId, 'PermitTypeCode') = 'LTP' or
       api.pkg_columnquery.Value(t_NewPermitId, 'RequiresVehicle') = 'Y' then
      null;
    else
      t_EndPointId := api.pkg_configquery.EndPointIdForName ('o_ABC_Permit',
          'AssociatedPermit');
      for p in (
                select
                  ap.PrimaryPermitObjectId,
                  ap.RelationshipId
                from
                  query.r_ABC_RenewAppJobPermit r
                  join query.r_ABC_PermitToRenewJobPermit r2
                      on r2.PermitRenewJobId = r.JobId
                  join query.r_abc_associatedpermit ap
                      on ap.AsscPermitObjectId = r2.PermitToRenewObjectId
                where  r.PermitId = t_NewPermitId
               ) loop
      api.pkg_relationshipupdate.Remove(p.relationshipid);
      t_RelationshipId := api.pkg_relationshipupdate.New (t_EndPointId,
          p.primarypermitobjectid, t_NewPermitId);
      end loop;
    end if;
    -- Vehicles tab
    if api.pkg_columnQuery.VALUE (t_NewPermitId, 'RequiresVehicle') = 'Y' then
      if api.pkg_columnQuery.NumericValue(t_NewPermitId, 'TotalNumberofVehicles') < 1
          then
        api.pkg_errors.RaiseError(-20000,  'At least one Vehicle is required.');
      else
        -- Find Vehicle Insignia Numbers already on this Permit
        select v.InsigniaNumber
        bulk collect into t_InsigniaNumberList
        from
          query.o_abc_vehicle v
          join query.r_Abc_Permitvehicle pv
            on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_NewPermitId
        order by v.InsigniaNumber;
        for i in 2..t_InsigniaNumberList.Count loop
          if t_InsigniaNumberList(i - 1) = t_InsigniaNumberList(i) then
            api.pkg_errors.RaiseError(-20000, 'No duplicate Insignia Numbers are'
                || ' allowed on the Permit.');
          end if;
        end loop;
        -- All detail fields are mandatory
        -- Find Vehicles without Make/Model
        select count(*)
        into t_Vehicles
        from
          query.o_abc_vehicle v
          join query.r_Abc_Permitvehicle pv
            on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_NewPermitId
            and v.MakeModelYear is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'Make/Model/Year information is required.');
        end if;
        -- Find Vehicles without License Plate
        select count(*)
        into t_Vehicles
        from
          query.o_abc_vehicle v
          join query.r_Abc_Permitvehicle pv
            on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_NewPermitId
            and v.StateRegistration is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'License Plate information is required.');
        end if;
        -- Find Vehicles without State
        select count(*)
        into t_Vehicles
        from
          query.o_abc_vehicle v
          join query.r_Abc_Permitvehicle pv
            on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_NewPermitId
            and v.StateOfRegistration is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'State of Registration is required.');
        end if;
        -- Find Vehicles without VIN
        select count(*)
        into t_Vehicles
        from
          query.o_abc_vehicle v
          join query.r_Abc_Permitvehicle pv
              on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_NewPermitId
            and v.vin is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'VIN is required.');
        end if;
        -- Find Vehicles without Owned/Leased information
        select count(*)
        into t_Vehicles
        from
          query.o_abc_vehicle v
          join query.r_Abc_Permitvehicle pv
              on v.ObjectId = pv.VehicleObjectId
        where pv.PermitObjectId = t_NewPermitId
            and v.OwnedOrLeasedLimousine is null;
        if t_Vehicles > 0 then
          api.pkg_errors.RaiseError(-20000,  'Owned/Leased information is required.');
        end if;
      end if;
      -- Check if we need to consolidate permits
      t_ExpirationDate := api.pkg_columnquery.datevalue(t_NewPermitId, 'CalculatedExpirationDate');
      if t_AssociatedPermitId is not null then
        select r.PrimaryPermitObjectId
        bulk collect into t_ActivePermits
        from query.r_ABC_AssociatedPermit r
        where r.AsscPermitObjectId = t_AssociatedPermitId
            and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'State') = 'Active'
            and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitNumber')
                != t_PermitNumber
            and api.pkg_columnquery.Value(r.PrimaryPermitObjectId, 'PermitTypeObjectId') =
                t_PermitTypeId;
      elsif t_LicenseId is not null then
        select pl.PermitObjectId
        bulk collect into t_ActivePermits
        from query.r_PermitLicense pl
        where pl.LicenseObjectId = t_LicenseId
            and api.pkg_columnquery.Value(pl.PermitObjectId, 'State') = 'Active'
            and api.pkg_columnquery.Value(pl.PermitObjectId, 'PermitNumber')
                != t_PermitNumber
            and api.pkg_columnquery.Value( pl.PermitObjectId, 'PermitTypeObjectId') =
                t_PermitTypeId;
      elsif t_TAPObjectId is not null then
        select tap.PermitObjectId
        bulk collect into t_ActivePermits
        from query.r_Abc_Tappermit tap
        where tap.TAPPermitObjectId = t_TAPObjectId
            and api.pkg_columnquery.Value(tap.PermitObjectId, 'State') = 'Active'
            and api.pkg_columnquery.Value(tap.PermitObjectId, 'PermitNumber')
                != t_PermitNumber
            and api.pkg_columnquery.Value(tap.PermitObjectId, 'PermitTypeObjectId') =
                t_PermitTypeId;
      end if;
      if t_ActivePermits.count > 0 then
        t_EffectiveDate := nvl(api.pkg_columnquery.datevalue(t_OldPermitId,
            'ExpirationDate') + 1, trunc(sysdate));
        api.pkg_columnupdate.SetValue(t_NewPermitId, 'EffectiveDate', t_EffectiveDate);
        t_ExpirationDate := api.pkg_columnquery.DateValue(t_NewPermitId,
            'CalculatedExpirationDate');
        for c in 1..t_ActivePermits.count loop
          -- Check if the Other Active Permit has an expiration date greater than the new
          -- expiration date. If not, then don't consolidate.
          select count(*)
          into t_RenewalCount
          from
            api.relationships r
            join api.jobs j
                on j.JobId = r.ToObjectId
          where r.FromObjectId = t_ActivePermits(c)
              and r.EndPointId = t_RenewEndpointId
              and j.JobStatus in ('NEW','REVIEW');
          if api.pkg_columnquery.Datevalue(t_ActivePermits(c), 'ExpirationDate') =
               t_ExpirationDate and t_RenewalCount = 0 then
            t_ConsolidatePermits := 'Y';
            t_ConsolidatedPermitId := t_ActivePermits(c);
            exit;
          end if;
        end loop;
      end if;
    end if;
    if t_ConsolidatePermits = 'Y' then
      --Copy Vehicles to Active Permit to Consolidate the Vehicles
      t_ToEndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Permit',
          'Vehicle');
      for i in (select *
                from query.r_abc_permitvehicle rpv
                where rpv.PermitObjectId = t_NewPermitId) loop
        t_VehicleId := api.pkg_objectupdate.New(t_VehicleDefId);
        t_RelId := api.Pkg_Relationshipupdate.New(t_ToEndPointId,
            t_ConsolidatedPermitId, t_VehicleId);
        api.pkg_columnupdate.setvalue(t_VehicleId, 'InsigniaNumber',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'InsigniaNumber'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'PreviousInsigniaNumber',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'PreviousInsigniaNumber'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'MakeModelYear',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'makemodelyear'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'OwnedOrLeasedLimousine',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'ownedorleasedlimousine'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'VIN',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'vin'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateregistration',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateregistration'));
        api.pkg_columnupdate.setvalue(t_VehicleId, 'Stateofregistration',
            api.pkg_columnquery.Value(i.vehicleobjectid, 'Stateofregistration'));
      end loop;
      -- Close the old permit and set the new permit as Cancelled
      api.pkg_columnupdate.SetValue(t_OldPermitId, 'State', 'Closed');
      api.pkg_columnupdate.SetValue(t_NewPermitId, 'State', 'Cancelled');
      api.pkg_columnupdate.SetValue(t_NewPermitId, 'Edit', 'N');
      api.pkg_columnupdate.RemoveValue(t_NewPermitId, 'EffectiveDate');
      api.pkg_columnupdate.RemoveValue(t_NewPermitId, 'ExpirationDate');
      api.pkg_columnupdate.SetValue(t_NewPermitId, 'EffectiveDate', trunc(sysdate));
      api.pkg_columnupdate.SetValue(t_NewPermitId, 'ExpirationDate', trunc(sysdate));
      api.pkg_columnupdate.setValue(t_NewPermitId, 'SystemCancelled', 'Y');
      t_ObjectId := api.pkg_objectupdate.New (t_ObjectDefId , sysdate);
      t_Reason := 'Consolidated with Permit ' ||  api.pkg_columnquery.Value(
          t_ConsolidatedPermitId, 'PermitNumber');
      api.pkg_columnupdate.SetValue(t_ObjectId, 'CancellationType', 'Cancellation',
          sysdate);
      api.pkg_columnupdate.SetValue(t_ObjectId, 'Reason', t_Reason, sysdate);
      t_RelId := api.pkg_relationshipupdate.New (t_CancelEndpointId, t_NewPermitId,
          t_ObjectId, sysdate);
      api.pkg_columnupdate.SetValue(t_ConsolidatedPermitId, 'Edit', 'N');
      api.pkg_columnupdate.SetValue(t_ConsolidatedPermitId, 'ForceStateReadOnly', 'N');
      t_ConsolidatePermits := 'N';
    else
      -- Close the old permit and set the new permit as active
      api.pkg_ColumnUpdate.SetValue(t_OldPermitId, 'State', 'Closed');
      api.pkg_columnupdate.SetValue(t_NewPermitId, 'State', 'Active');
      -- Set Issue Date
      t_IssueDate := trunc(sysdate);
      api.pkg_columnupdate.SetValue(t_NewPermitId, 'IssueDate', t_IssueDate);
      -- Set Original Issue Date
      t_OriginalIssueDate := nvl(api.pkg_columnquery.datevalue(t_OldPermitId,
          'OriginalIssueDate'), trunc(sysdate));
      api.pkg_columnupdate.SetValue(t_NewPermitId, 'OriginalIssueDate',
          t_OriginalIssueDate);
      -- Set Effective Date
      t_EffectiveDate := nvl(api.pkg_columnquery.datevalue(t_OldPermitId,
          'ExpirationDate') + 1, trunc(sysdate));
     api.pkg_columnupdate.SetValue(t_NewPermitId, 'EffectiveDate', t_EffectiveDate);
     -- Set Expiration Date
     t_ExpirationDate := api.pkg_columnquery.DateValue(t_NewPermitId,
         'CalculatedExpirationDate');
     api.pkg_columnupdate.SetValue(t_NewPermitId, 'ExpirationDate', t_ExpirationDate);
     api.pkg_columnupdate.SetValue(t_NewPermitId, 'Edit', 'N');
     api.pkg_columnupdate.SetValue(t_NewPermitId, 'ForceStateReadOnly', 'N');
   end if;
   -- See if Public User needs to be associated with new Permittee
   CopyPermitteeToOnlineUser(t_JobId);

  end ApprovePermit;

 /*---------------------------------------------------------------------------
  * PermitRenewalPostVerify()
  *-------------------------------------------------------------------------*/
  procedure PermitRenewalPostVerify(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_DefaultClerkId                    udt_Id;
    t_DefaultSupervisorId               udt_Id;
    t_PermitEPId                        udt_Id := api.pkg_configquery.EndPointIdForName('j_ABC_PermitRenewal', 'PermitToRenew');
    t_DefaultClerkEPId                  udt_Id := api.pkg_configquery.EndPointIdForName('j_ABC_PermitRenewal', 'DefaultClerk');
    t_DefaultSupervisorEPId             udt_Id := api.pkg_configquery.EndPointIdForName('j_ABC_PermitRenewal', 'DefaultSupervisor');
    t_RelId                             udt_Id;
  begin
    --Set Default users if not already selected
    if api.pkg_objectquery.RelatedObjects(a_ObjectId, t_PermitEPId).count > 0 then
      if api.pkg_objectquery.RelatedObjects(a_ObjectId, t_DefaultClerkEPId).count = 0 then
        select r2.DefaultClerkId
          into t_DefaultClerkId
          from query.r_ABC_PermitToRenewJobPermit p
          join query.r_ABC_PermitPermitType pt
            on p.PermitToRenewObjectId = pt.PermitId
          join query.r_abc_permittypedefaultclerk r2
            on r2.PermitTypeId = pt.PermitTypeId
         where p.PermitRenewJobId = a_ObjectId;
        t_RelId := api.pkg_relationshipupdate.New(t_DefaultClerkEPId, a_ObjectId, t_DefaultclerkId);
      end if;
      if api.pkg_objectquery.RelatedObjects(a_ObjectId, t_DefaultSupervisorEPId).count = 0 then
        select r2.PermittingSupervisorId
          into t_DefaultSupervisorId
          from query.r_ABC_PermitToRenewJobPermit p
          join query.r_ABC_PermitPermitType pt
            on p.PermitToRenewObjectId = pt.PermitId
          join query.r_SupervisorPermitType r2
            on r2.PermitTypeId = pt.PermitTypeId
         where p.PermitRenewJobId = a_ObjectId;
        t_RelId := api.pkg_relationshipupdate.New(t_DefaultSupervisorEPId, a_ObjectId, t_DefaultSupervisorId);
      end if;
    end if;
  end PermitRenewalPostVerify;

 /*---------------------------------------------------------------------------
  * DenyPermit()
  *  Runs Denial actions
  *  Run from workflow on the Permit Renewal job
  *-------------------------------------------------------------------------*/
  procedure DenyPermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PermitId                          udt_id;

  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_PermitId := api.pkg_columnquery.value(t_JobId, 'PermitObjectId');
    api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Not Approved');
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'N');
  end DenyPermit;

 /*---------------------------------------------------------------------------
  * WithdrawPermit()
  *  Runs Denial actions
  *  Run from workflow on the Permit Renewal job
  *-------------------------------------------------------------------------*/
  procedure WithdrawPermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PermitId                          udt_id;

  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_PermitId := api.pkg_columnquery.value(t_JobId, 'PermitObjectId');
    api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Withdrawn');
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'N');
  end WithdrawPermit;

 /*---------------------------------------------------------------------------
  * RevokePermit()
  *  Runs Denial actions
  *  Run from workflow on the Permit Renewal job
  *-------------------------------------------------------------------------*/
  procedure RevokePermit(
    a_ObjectId                          udt_id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_PermitId                          udt_id;

  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_PermitId := api.pkg_columnquery.value(t_JobId, 'PermitObjectId');
    api.pkg_columnupdate.SetValue(t_PermitId, 'State', 'Revoked');
    api.pkg_columnupdate.SetValue(t_PermitId, 'Edit', 'N');
  end RevokePermit;

  /*---------------------------------------------------------------------------
    SetRegenerateFeesPermit
  -- Description:
  *-------------------------------------------------------------------------*/
  procedure SetRegenerateFeesPermit (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_JobId udt_id;
    t_AppRel udt_id;
    t_RenRel udt_id;
    t_State  varchar2(60);
  begin
    t_AppRel := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','PermitToApp');
    t_RenRel := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','PermitToRenew');
    begin
      select max (ToObjectId)
      into   t_JobId
      from (
        select r.ToObjectId
        from api.relationships r
        where r.fromobjectid = a_ObjectId
        and r.endpointid = t_RenRel
        union
        select r.ToObjectId
        from api.relationships r
        where r.fromobjectid = a_ObjectId
        and r.endpointid = t_AppRel);
    exception when no_data_found then
      t_JobId := null;
    end;
    t_State := api.pkg_columnquery.Value(a_ObjectId, 'State');
    if t_JobId is not null and t_State = 'Pending' then
       api.pkg_columnupdate.setvalue(t_JobId,'GenerateFees','Y');
    end if;
  end SetRegenerateFeesPermit;

  /*---------------------------------------------------------------------------
    GetPermitId
  -- Description:
  *-------------------------------------------------------------------------*/
  procedure GetPermitId (
     a_ObjectId               udt_id,
     a_AsOfDate               date
   ) is
     t_PermitId               udt_id;
     t_CoOpEndPointId         udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','CoOpLicense');
     t_CTSEndPointId          udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','CTSRepresentative');
     t_EventDateEndPointId    udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','EventDate');
     t_SolicitorEndPointId    udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','SolicitorPermit');
     t_ProductEndPointId      udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','Product');
     t_VehicleEndPointId      udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit','Vehicle');
  begin
    -- CoOp License
    begin
      select r.FromObjectId
        into t_PermitId
        from api.relationships r
       where r.RelationshipId = a_ObjectId
         and r.EndPointId = t_CoOpEndPointId
       union
    -- CTS Representative
      select r.FromObjectId
        from api.relationships r
       where r.RelationshipId = a_ObjectId
         and r.EndPointId = t_CTSEndPointId
       union
    -- Event Dates
      select r.FromObjectId
        from api.relationships r
       where r.RelationshipId = a_ObjectId
         and r.EndPointId = t_EventDateEndPointId
       union
    -- CTW Solicitors
      select r.FromObjectId
        from api.relationships r
       where r.RelationshipId = a_ObjectId
         and r.EndPointId = t_SolicitorEndPointId
       union
    -- Product
      select r.FromObjectId
        from api.relationships r
       where r.RelationshipId = a_ObjectId
         and r.EndPointId = t_ProductEndPointId
       union
    -- Vehicle
      select r.FromObjectId
        from api.relationships r
       where r.RelationshipId = a_ObjectId
         and r.EndPointId = t_VehicleEndPointId;
    exception when no_data_found then
        t_PermitId := null;
    end;

    if t_PermitId is not null then
       SetRegenerateFeesPermit(t_PermitId,sysdate);
       return;
    end if;

  end GetPermitId;

  procedure CopyPermitteeToOnlineUserExt (
     a_ObjectId               udt_id,
     a_AsOfDate               date
   ) is
     t_JobId                  udt_id;
  begin
   -- Get JobId from ObjectId
   select p.JobId
     into t_JobId
     from api.processes p
    where p.ProcessId = a_ObjectId;
   -- See if Public User needs to be associated with new Permittee
   CopyPermitteeToOnlineUser(t_JobId);
  end CopyPermitteeToOnlineUserExt;

  -----------------------------------------------------------------------------
  --  TransferDistributorLicense
  --  Transfer Distributor License on Renewal/Amendment to new license.
  -----------------------------------------------------------------------------
  procedure TransferDistributorLicense(
    a_ObjectId                    udt_Id,
    a_AsOfDate                    date
  ) is

    t_JobDef                      varchar2(30);
    t_JobId                       udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'JobId');
    t_OldLicense                  udt_id;
    t_NewLicense                  udt_id;
    t_TransferDistributorLicense  varchar2(01);
    t_ProductDistributorEPId      udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'DistributorLic');
    t_ProductHistDistributorEPId  udt_Id := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'HistoricProducts');
    t_RelId                       udt_Id;

  begin
    t_JobDef := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_OldLicense := nvl(api.pkg_columnquery.NumericValue(t_JobId,'LicenseToRenewObjectId'),api.pkg_columnquery.NumericValue(t_JobId,'LicenseToAmendObjectId'));
    t_NewLicense := api.pkg_columnquery.NumericValue(t_JobId,'LicenseObjectId');
    t_TransferDistributorLicense := api.pkg_columnquery.Value(api.pkg_columnquery.NumericValue(t_JobId,'AmendmentTypeObjectId'),'TransferDistributorLicense');

    for l in (select pd.RelationshipId, pd.ProductId, pd.DistributionStatus
                from query.r_Abcproduct_Distributor pd
               where pd.LicenseId = t_OldLicense) loop
       if (t_JobDef = 'j_ABC_AmendmentApplication' and t_TransferDistributorLicense = 'Y') or
           t_JobDef = 'j_ABC_RenewalApplication' then
       -- Create Product Distributor relationship for new license
          t_RelId := api.pkg_relationshipupdate.New(t_ProductDistributorEPId, l.ProductId, t_NewLicense);
       else
       -- Create Product Historic Distributor relationship for old license
          t_RelId := api.pkg_relationshipupdate.New(t_ProductHistDistributorEPId, l.ProductId, t_OldLicense);
       end if;
       -- Remove Product Distributor relationship for old license
       api.pkg_relationshipupdate.Remove(l.RelationshipId);
    end loop;

  end TransferDistributorLicense;

  -----------------------------------------------------------------------------
  --  CheckActiveJobs
  --  Check any active Renewal/Amendment jobs for the License or Permit
  -----------------------------------------------------------------------------
  procedure CheckActiveJobs (
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_CurrLicenseId          udt_id;
--    t_ActiveJobs             number := 0;
    t_CurrJobDef             varchar2(30);
    t_RenewalExists          varchar2 (01) := 'N';
    t_RenewalAmendmentExists varchar2 (01) := 'N';

  begin

      t_CurrLicenseId := coalesce(api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'LicenseToRenewObjectIdStored'),
                                  api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'LicenseToAmendObjectIdStored'),
                                  api.Pkg_Columnquery.NumericValue(a_ObjectId, 'PermitToRenewObjectIdStored'));
      t_CurrJobDef := api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectDefName');

      if t_CurrJobDef = 'j_ABC_PermitRenewal' then
         for j in (select p.ObjectId, p.StatusName
                   from   query.j_abc_permitrenewal p
                   where  p.PermitToRenewObjectId = t_CurrLicenseId
                   and    p.ObjectDefTypeId = 2
                  )
         loop
            if j.StatusName in ('NEW', 'REVIEW', 'AWAIT', 'RELIEF') then
               t_RenewalExists := 'Y';
               exit;
            end if;
         end loop;
      elsif t_CurrJobDef = 'j_ABC_RenewalApplication' then
         for j in (select r.ObjectId, r.StatusName
                   from   query.j_abc_renewalapplication r
                   where  r.LicenseToRenewObjectId = t_CurrLicenseId
                   and    r.ObjectDefTypeId = 2
                  )
         loop
            if j.StatusName in ('NEW', 'REVIEW', 'AWAIT', 'RELIEF') then
               t_RenewalExists := 'Y';
               exit;
            end if;
         end loop;
      elsif t_CurrJobDef = 'j_ABC_AmendmentApplication' then
         for j in (select r.ObjectId, r.StatusName
                   from   query.j_abc_renewalapplication r
                   where  r.LicenseToRenewObjectId = t_CurrLicenseId
                   and    r.ObjectDefTypeId = 2
                   union
                   select a.ObjectId, a.StatusName
                   from   query.j_abc_amendmentapplication a
                   where  a.LicenseToAmendObjectId = t_CurrLicenseId
                   and    a.ObjectDefTypeId = 2
                  )
         loop
            if j.StatusName in ('NEW', 'REVIEW', 'AWAIT', 'RELIEF') then
               t_RenewalAmendmentExists := 'Y';
               exit;
            end if;
         end loop;
      end if;
      if t_RenewalExists = 'Y' then
         if t_CurrJobDef = 'j_ABC_PermitRenewal' then
            api.pkg_Errors.RaiseError(-20000, 'An active Renewal job already exists for this Permit. Please ensure the other job is finished before initiating a new one.');
         else
           --This is to disable stacked renewals
           --api.pkg_Errors.RaiseError(-20000, 'An active Renewal job already exists for this License. Please ensure the other job is finished before initiating a new one.');
           null;
         end if;
      elsif t_RenewalAmendmentExists = 'Y' then
         api.pkg_Errors.RaiseError(-20000, 'An active Amendment or Renewal job already exists for this License. Please ensure the other job is finished before initiating a new one.');
      end if;

  end CheckActiveJobs;

  procedure RenewalConstructor (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_CurrUserId          udt_id;
    t_ActiveJobs          number := 0;
    t_LicToRenew          varchar2(4000);
    t_count               integer;
    type stringlist       is table of varchar2(50);
    v_LicNumber           dbms_utility.lname_array;

  begin

      t_CurrUserId := api.pkg_columnquery.NumericValue(a_ObjectId, 'CreatedByUserId');
      t_LicToRenew := api.pkg_columnquery.Value(a_ObjectId, 'LicenseNumber');
      dbms_utility.comma_to_table(api.pkg_columnquery.Value(t_CurrUserId, 'InProgressRenewalPermitNum'),
                                  t_count,
                                  v_LicNumber);
      if v_LicNumber is null then
         api.pkg_errors.RaiseError(-20000,'Empty');
      end if;
--      v_LicNumber  := api.pkg_columnquery.Value(t_CurrUserId, 'InProgressRenewalLicNum');
      api.pkg_errors.RaiseError(-20000,'Licenses ' || v_LicNumber(1));
--      if t_LicToRenew member of v_LicNumber then
--      if t_ActiveJobs > 0 then
--         raise_application_error (-20000,'An active Renewal job already exists. Please wait until the other job is finished.');
--      end if;

  end RenewalConstructor;

  -----------------------------------------------------------------------------
  -- GetLatestLicense
  --   Get the License version which is the latest version
  -----------------------------------------------------------------------------
  function GetLatestLicense(
    a_LicenseID                         udt_id, -- current license id
    a_PermitId                          udt_id  -- current permit id
  ) return number as
    t_LicenseObjectId                   udt_id := 0;
  begin

    -- Find the latest version of the license.
    if api.pkg_columnquery.Value(a_LicenseID, 'State') = 'Active' then
      return a_LicenseID;
    end if;

    begin
    select api.pkg_columnquery.Value(ml2.LicenseObjectId, 'ObjectId') ObjectId
    into t_LicenseObjectId
      from query.r_abc_masterlicenselicense ml
      join query.r_abc_masterlicenselicense ml2
          on ml2.MasterLicenseObjectId = ml.MasterLicenseObjectId
    where ml.LicenseObjectId = a_LicenseId
        and api.pkg_columnquery.Value(ml2.LicenseObjectId, 'State') = 'Active';
    exception
      when no_data_found then
        t_LicenseObjectId := 0;
    end;
    return t_LicenseObjectId;

  end GetLatestLicense;

  -----------------------------------------------------------------------------
  -- GetLatestPermit
  --   Get the Associated Permit which is the latest Active version
  -----------------------------------------------------------------------------
  function GetLatestPermit(
    a_PermitId                          udt_id, -- current Permit Id
    a_AssociatedPermitId                udt_id  -- current Associated Permit Id
  ) return number as
    t_PermitObjectId                    udt_id :=
        a_AssociatedPermitId; -- Active Associated Permit Id
    t_PermitNumber                      varchar2(100) :=
        api.pkg_columnquery.Value(a_AssociatedPermitId, 'PermitNumber');
    t_SearchResults                     api.pkg_definition.udt_SearchResults;
    t_Objects                           udt_idList;
    t_PermitObjects                     udt_idList;
  begin

    if api.pkg_columnquery.Value(t_PermitObjectId, 'IsActive') = 'Y' then
       return t_PermitObjectId;
    end if;
    t_PermitObjects := api.pkg_simplesearch.ObjectsByIndex('o_abc_permit', 'PermitNumber',
        t_PermitNumber, t_PermitNumber);

    select pn.objectid
    bulk collect into t_Objects
    from table(cast(api.pkg_simplesearch.CastableObjectsByIndex('o_abc_permit',
        'PermitNumber', t_PermitNumber) as api.udt_Objectlist)) pn
    where api.pkg_columnquery.value(pn.objectid, 'IsActive') = 'Y';

    if t_Objects.count = 0 then
      t_PermitObjectId := 0;
    end if;

    if t_Objects.count > 0 then
      t_PermitObjectId := t_Objects(1);
      return t_PermitObjectid;
    end if;
    return t_PermitObjectId;

  end GetLatestPermit;

  /*---------------------------------------------------------------------------
    SubmitAdditionalInfo - PUBLIC
  *-------------------------------------------------------------------------*/
  procedure SubmitAdditionalInfo(
    a_ObjectId            udt_Id,
    a_AsOfDate            date
    )is

    t_ProcessDefId         udt_Id;
    t_ProcessId            udt_Id;
    t_RequestedInfo        varchar2(4000);
    t_InfoProvided         varchar2(4000);
    t_DocEndPoint          udt_Id;
    t_RelId                udt_Id;
    t_JobDefName           varchar2(100);

  begin

   t_JobDefName := api.pkg_columnquery.Value(a_ObjectId, 'ObjectDefName');

   if api.pkg_columnquery.value(a_objectid, 'CreateEnterAddInfoProcess') = 'Y' then
    --Create a new Enter Additional Information procedure
    t_ProcessDefId := api.pkg_configquery.ObjectDefIdForName('p_ABC_ReceiptNewInformation');
    t_ProcessId := api.pkg_processupdate.New(a_ObjectId, t_ProcessDefId, null, null, null, null);

    --Set the outcome of the process to Complete to allow the job to move onto the next process.
    api.pkg_processupdate.Complete(t_ProcessId, 'Complete');

    --Get the information from the New/Amendment Application job and copy that information to the process.
    t_RequestedInfo := api.pkg_columnquery.Value(a_ObjectId, 'InfoRequested');
    t_InfoProvided  := api.pkg_columnquery.value(a_objectid, 'OnlineInfoProvided');

    api.pkg_columnupdate.SetValue(t_ProcessId, 'InfoRequested', t_RequestedInfo);
    api.pkg_columnupdate.setvalue(t_ProcessId, 'InfoProvided', t_InfoProvided);

    --Also pass that information to the 'Online Application Details' tab on the New / Amendment Application
    api.pkg_columnupdate.setvalue(a_ObjectId, 'OnlineInfoProvided', t_InfoProvided);

    --Get the endpointid of the document
    t_DocEndPoint := api.pkg_configquery.EndPointIdForName('p_ABC_ReceiptNewInformation', 'ElectronicDoc');

    --Find the documents that were uploaded from the public site and copy them to the process
    --We want to query different tables based on the JobDef

    --Licenseing Jobs
    if t_JobDefName = 'j_ABC_NewApplication' then
      for i in (select r.ElectronicDocId, r.RelationshipId
                  from query.r_NewAppAdditionalInfoDocs r
                 where r.NewApplicationId = a_objectid) loop
         --Relate the document to the process.
         if api.pkg_columnquery.Value(i.electronicdocid, 'DocumentSubmitted') = 'N' then
           t_RelId := api.pkg_relationshipupdate.new(t_DocEndPoint, t_ProcessId, i.ElectronicDocId);
           api.pkg_columnupdate.SetValue(i.electronicdocid, 'DocumentSubmitted', 'Y');
         end if;
       end loop;
     end if;

     if t_JobDefName = 'j_ABC_AmendmentApplication' then
      for i in (select r.ElectronicDocId, r.RelationshipId
                  from query.r_AmendmentAdditionalInfoDocs r
                 where r.AmendmentId = a_objectid) loop
         --Relate the document to the process.
         if api.pkg_columnquery.Value(i.electronicdocid, 'DocumentSubmitted') = 'N' then
           t_RelId := api.pkg_relationshipupdate.new(t_DocEndPoint, t_ProcessId, i.ElectronicDocId);
           api.pkg_columnupdate.SetValue(i.electronicdocid, 'DocumentSubmitted', 'Y');
         end if;
       end loop;
     end if;

    --Set the detail that indicates if the 'Online Information Requests' tab will be displayed on the New Application.
    api.pkg_columnupdate.setvalue(t_processId, 'OnlineInfoHasBeenSubmitted', 'Y');
    api.pkg_columnupdate.setvalue(a_objectId, 'OnlineInfoHasBeenSubmitted', 'Y');
    api.pkg_columnupdate.setvalue(a_objectId, 'PublicUserInfoRequired', 'N');
    api.pkg_columnupdate.setvalue(a_objectid, 'CreateEnterAddInfoProcess', 'N');
  end if;

 end SubmitAdditionalInfo;

  /*---------------------------------------------------------------------------
   * GetLatestSolicitorPermit() -- PUBLIC
   *   Get the Solicitor Permit which is the latest Active version (if any)
   *-------------------------------------------------------------------------*/
  function GetLatestSolicitorPermit(
    a_PermitObjectId                    udt_id, -- current Permit
    a_SolicitorObjectId                 udt_id  -- current Solicitor Id
  ) return number as
    t_PermitObjectId                    udt_id := 0;
    t_PermitNumber                      varchar2(100) :=
        api.pkg_columnquery.Value(a_PermitObjectId, 'PermitNumber');
    t_SearchResults                     api.pkg_Definition.udt_SearchResults;
    t_Objects                           udt_IdList;
  begin

    if api.pkg_columnquery.Value(a_PermitObjectId, 'IsActive') = 'Y' then
      return a_PermitObjectId;
    end if;

    if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit',
          'PermitNumber', t_PermitNumber, t_SearchResults) then
      return t_PermitObjectId;
    end if;
    if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit',
          'State', 'Active', t_SearchResults) then
      return t_PermitObjectId;
    end if;
    if not api.pkg_Search.IntersectByColumnContents('o_ABC_Permit',
          'SolicitorObjectId', a_SolicitorObjectId, t_SearchResults) then
      return t_PermitObjectId;
    end if;
    t_Objects := api.pkg_Search.SearchResultsToList(t_SearchResults);
    t_PermitObjectId := t_Objects(1);

    return t_PermitObjectId;

  end GetLatestSolicitorPermit;

  /*---------------------------------------------------------------------------
    Cancel Application / Amendment Job
  *-------------------------------------------------------------------------*/
  procedure CancelLicenseJob(
    a_ObjectId udt_Id,
    a_AsOfDate date
    )is
    t_JobId                udt_id;

  begin
    select n.NewApplicationId
    into   t_JobId
    from   query.r_abc_newappcancellation n
    where  n.RelationshipId = a_ObjectId
    union
    select a.AmendmentApplicationId
    from   query.r_ABC_AmendmentAppCancellation a
    where  a.relationshipid = a_ObjectId;

    api.pkg_columnupdate.SetValue(t_JobId, 'CancelJob', 'Y');
    api.pkg_columnupdate.RemoveValue(t_JobId, 'PublicUserInfoRequired');

  end CancelLicenseJob;

  /*---------------------------------------------------------------------------
    Submit Review Validation
  *-------------------------------------------------------------------------*/
  procedure SubmitReviewValidation(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_SpecialConditions                 varchar,
    a_Outcome                           varchar
  ) is
   t_Comments                           varchar(4000);
   t_HasConditionsDoc                   number;
   t_JobId                              udt_Id;
   t_SpecialConditionsDoc               udt_Id;
  begin

    select NVL(api.pkg_columnquery.Value(a_ObjectId, 'PoliceComments'),
        api.pkg_columnquery.Value(a_ObjectId, 'MunicipalComments'))
    into t_Comments
    from dual;

    t_JobId := api.pkg_columnquery.Value(a_ObjectId, 'JobId');

    --These are validation errors for public and internal site. 
    --There is some additional error checking in a python script on RaiseReqdApplicationDocError for public
    if a_Outcome = 'Approve' and a_SpecialConditions = 'Yes' 
        and api.pkg_columnquery.Value(a_ObjectId, 'MissingSpecialConditionsDoc') is not null then
      api.pkg_errors.RaiseError(-20075, 'Please upload a '
        || api.pkg_columnquery.Value(api.pkg_columnquery.Value(a_ObjectId, 'MissingSpecialConditionsDoc'), 'Name')
        ||' document prior to endorsing.');
    end if;

    if  a_Outcome = 'Deny' and t_Comments is Null then
      api.pkg_errors.RaiseError(-20076, 'Please provide a detailed reason when not endorsing. ' || t_Comments);
    end if;

    if a_Outcome = 'Hold' and t_Comments is Null then
      api.pkg_errors.RaiseError(-20077, 'Please provide a detailed reason for placing the '
          || 'application on hold.');
    end if;

    if  a_Outcome = 'Approve' and api.pkg_columnquery.Value(t_JobId, 'ReviewDenied') = 'Y' then
      api.pkg_errors.RaiseError(-20078, 'This application has an existing response of Do Not Endorse, please navigate'
        || ' to the "RESPONSES" section to see the reason. For more information'
        || ' regarding that response, please contact the identified responder. Contact the Division'
        || ' if the town reevaluates the original Do Not Endorse response and will be Endorsing the application.');
    end if;

    if a_Outcome = 'Deny' and a_SpecialConditions = 'Yes' then
      api.pkg_errors.RaiseError(-20079, 'Special conditions are not allowed for the response Do Not Endorse');
    end if;

    if a_Outcome = 'Hold' and a_SpecialConditions = 'Yes' then
      api.pkg_errors.RaiseError(-20080, 'Special conditions are not allowed when placing the '
          || 'application on hold.');
    end if;

  end SubmitReviewValidation;

  /*---------------------------------------------------------------------------
  * MonitorReviewProcedure
  *   Complete the Monitor Review Process for Permit Applications and Renewals
  *--------------------------------------------------------------------------*/
  procedure MonitorReviewProcedure(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_MonitorProcessTypeId              udt_Id;
    t_MuniReviewType                    udt_Id;
    t_PoliceReviewType                  udt_Id;
    t_MonitorProcessId                  udt_Id;
    t_MuniReviewRequired                varchar2(1);
    t_PoliceReviewRequired              varchar2(1);
    t_MuniCount                         integer;
    t_PoliceCount                       integer;
    t_MuniHold                          varchar2(1);
    t_PoliceHold                        varchar2(1);
    t_MuniDone                          varchar2(1);
    t_PoliceDone                          varchar2(1);
    t_JobStatus                         varchar2(60);

  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_JobStatus := api.pkg_columnquery.Value(t_JobId, 'StatusDescription');
    t_MuniReviewRequired := api.pkg_columnquery.Value(t_JobId, 'MuniReviewRequired');
    t_PoliceReviewRequired := api.pkg_columnquery.Value(t_JobId, 'PoliceReviewRequired');
    t_MuniHold := api.pkg_columnquery.Value(t_JobId, 'IsMuniHold');
    t_PoliceHold := api.pkg_columnquery.Value(t_JobId, 'IsPoliceHold');
    t_MonitorProcessTypeId := api.pkg_configquery.ObjectDefIdForName('p_ABC_MonitorReview');
    t_MuniReviewType := api.pkg_configquery.ObjectDefIdForName('p_ABC_MunicipalityReview');
    t_PoliceReviewType := api.pkg_configquery.ObjectDefIdForName('p_ABC_PoliceReview');

    -- Finds the most recent monitor process id that is not complete.
    select max(p.processid)
    into t_monitorprocessid
    from api.processes p
    where p.processtypeid = t_MonitorProcessTypeId
      and p.jobid = t_JobId
      and p.outcome is null;
    -- If there isn't a monitor process, then we are done here.
    if t_MonitorProcessId is null then
      return;
    end if;

    -- Checks if the Municipal review is required, and if it is completed.
    if t_MuniReviewRequired = 'Y' then
      select count(pr.processid)
      into t_MuniCount
      from api.processes pr
      where pr.JobId = t_JobId
        and pr.ProcessTypeId = t_MuniReviewType
        and pr.outcome is null;
      if t_MuniCount = 0 then
        t_MuniDone := 'Y';
      end if;
    else
      t_MuniDone := 'Y';
    end if;

    -- Checks if the Police review is required, and if it is completed.
    if t_PoliceReviewRequired = 'Y' then
      select count(po.ProcessId)
      into t_PoliceCount
      from api.processes po
      where po.JobId = t_JobId
        and po.ProcessTypeId = t_PoliceReviewType
        and po.outcome is null;
      if t_PoliceCount = 0 then
        t_PoliceDone := 'Y';
      end if;
    else
      t_PoliceDone := 'Y';
    end if;

    -- If both review processes are completed or not required, then complete the monitor process.
    if t_MuniDone = 'Y' and t_PoliceDone = 'Y' then
      api.pkg_processupdate.Complete(t_MonitorProcessId, 'Completed', sysdate);
      return;
    end if;

    -- If either of the review process are still open, then we check to see if the job should
    -- stay in the Hold Status.
    if t_JobStatus = 'Municipal Hold' and t_MuniHold = 'N' and t_PoliceHold = 'N' then
      api.pkg_processupdate.Complete(t_MonitorProcessId, 'To Review', sysdate);
    end if;

  end MonitorReviewProcedure;

  /*---------------------------------------------------------------------------
   * ResetLicense -- PUBLIC
   *--------------------------------------------------------------------------*/
  procedure ResetLicense(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_LicenseEndPointId                 udt_Id;
    t_LicenseToCopyColName              varchar2(30);
    t_MasterLicense                     udt_Id;
    t_NewReferencedLicense              udt_Id;
    t_ObjectDefName                     varchar2(30);
    t_OldPendingLicense                 udt_Id;
    t_ReferencedLicense                 udt_Id;
    t_RefLicenseEndPointId              udt_Id;
    t_RefLicenseEndPointName            varchar2(30);
    t_RelationshipId                    udt_Id;
    t_SupersededEndPointId              udt_Id;
  begin

    if api.pkg_columnquery.Value(a_ObjectId, 'ResetLicense') = 'N' then
      return;
    end if;

    t_ObjectDefName := api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectDefName');
    t_OldPendingLicense := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'LicenseObjectId');
    t_LicenseEndPointId := api.pkg_ConfigQuery.EndpointIdForName(t_ObjectDefName, 'License');
    t_SupersededEndPointId := api.pkg_ConfigQuery.EndpointIdForName(t_ObjectDefName, 'SupersededLicense');

    case t_ObjectDefName
      when 'j_ABC_AmendmentApplication' then
        t_LicenseToCopyColName := 'LicenseToAmendObjectId';
        t_RefLicenseEndPointName := 'LicenseToAmend';
      when 'j_ABC_RenewalApplication' then
        t_LicenseToCopyColName := 'LicenseToRenewObjectId';
        t_RefLicenseEndPointName := 'LicenseToRenew';
    end case;

    -- Set pending license to superseded and move it to the superseded rel
    api.pkg_ColumnUpdate.SetValue(t_OldPendingLicense, 'State', 'Superseded');
    api.pkg_ColumnUpdate.SetValue(t_OldPendingLicense, 'SupersededReason',
        api.pkg_ColumnQuery.Value(a_ObjectId, 'ResetLicenseReason'));

    select RelationshipId
    into t_RelationshipId
    from api.relationships
    where FromObjectId = a_ObjectId
      and EndPointId = t_LicenseEndPointId
      and ToObjectId = t_OldPendingLicense;

    api.pkg_RelationshipUpdate.Remove(t_RelationshipId);
    t_RelationshipId := api.pkg_RelationshipUpdate.New(
        t_SupersededEndPointId, a_ObjectId, t_OldPendingLicense);

    t_MasterLicense := api.pkg_ColumnQuery.NumericValue(t_OldPendingLicense, 'MasterLicenseObjectId');

    select Relationshipid
    into t_RelationshipId
    from query.r_ABC_MasterLicenseLicense
    where MasterLicenseObjectId = t_MasterLicense
      and LicenseObjectId = t_OldPendingLicense;

    api.pkg_RelationshipUpdate.Remove(t_RelationshipId);

    -- Use existing referenced to find the latest license
    t_ReferencedLicense := api.pkg_ColumnQuery.NumericValue(a_ObjectId, t_LicenseToCopyColName);
    t_MasterLicense := api.pkg_ColumnQuery.NumericValue(t_ReferencedLicense, 'MasterLicenseObjectId');

    begin
      select l.ObjectId
      into t_NewReferencedLicense
      from 
        query.r_ABC_MasterLicenseLicense r
        join query.o_ABC_License l
            on l.ObjectId = r.LicenseObjectId
      where r.MasterLicenseObjectId = t_MasterLicense
        and l.IsLatestVersion = 'Y'
        and l.State = 'Active';
    exception
      when no_data_found then
        api.pkg_Errors.QueueBusinessError('Unable to get latest version of referenced license.');
      when too_many_rows then
        api.pkg_Errors.QueueBusinessError('Unable to get latest version of referenced license.');
    end;

    if t_NewReferencedLicense is not null then
      t_RefLicenseEndPointId := api.pkg_ConfigQuery.EndPointIdForName(
          t_ObjectDefName, t_RefLicenseEndPointName);

      select RelationshipId
      into t_RelationshipId
      from api.relationships
      where FromObjectId = a_ObjectId
        and EndPointId = t_RefLicenseEndPointId
        and ToObjectId = t_ReferencedLicense;

      api.pkg_RelationshipUpdate.Remove(t_RelationshipId);
      t_RelationshipId := api.pkg_RelationshipUpdate.New(
          t_RefLicenseEndPointId, a_ObjectId, t_NewReferencedLicense);
    end if;

    -- Create a new pending license based on the new referenced license
    CopyLicense(a_ObjectId, a_AsOfDate);

    api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'ResetLicenseReason');
    api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'ResetLicense', 'N');

  end ResetLicense;

end pkg_ABC_Workflow;
/
