create or replace package pkg_ABC_municipality is

  /*---------------------------------------------------------------------------
  * DESCRIPTION
  *   Routines used to manage municipalities.  Creation and assignment of a
  * municipality-specific instance security accessgroup is managed here.
  *-------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------
  * Types
  *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;

  /*---------------------------------------------------------------------------
   * AssignAccessGroup() -- PUBLIC
   * Intended to run on constructor
   *-------------------------------------------------------------------------*/
  procedure AssignAccessGroup(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*--------------------------------------------------------------------------
  * AddStaff() - Add municipality access group to a user
  * Run on Constructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure AddStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * AddPoliceStaff() - Add police municipality access group to a user
  * Run on Constructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure AddPoliceStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * RemoveStaff() - Add municipality access group to a user
  * Run on Deleting event of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure RemoveStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*--------------------------------------------------------------------------
  * RemovePoliceStaff() - Remove police  municipality access group from a user
  * Run on Destructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure RemovePoliceStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  );

  /*---------------------------------------------------------------------------
   * CreateOrderIssuanceSeq() -- PUBLIC
   * Run on constructor of the Municipality (o_ABC_Office)
   *-------------------------------------------------------------------------*/
  procedure CreateOrderIssuanceSeq(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_MunicipalityCountyCode            number
  );

  /*---------------------------------------------------------------------------
   * DeleteOrderIssuanceSeq() -- PUBLIC
   * Run on destructor of the Municipality (o_ABC_Office)
   *-------------------------------------------------------------------------*/
  procedure DeleteOrderIssuanceSeq(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * ValidateWholesaleDefaultFlag()
   *   Run on post verify of the Municipality (o_ABC_Office)
   *-------------------------------------------------------------------------*/
  procedure ValidateWholesaleDefaultFlag (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * MuniEmailForPermit()
   *   Send notification Email to Municipality / Applicant on Permit
   * Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure MuniEmailForPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * MuniEmailForLicense()
   *   Send notification Email to Municipality / Applicant on License
   * Application/Renewal/Amendment. Note that Documents will not be attached
   * to the email.
   *-------------------------------------------------------------------------*/
  procedure MuniEmailForLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * PoliceEmailForPermit()
   *   Send notification Email to Police Users / Applicant on Permit
   * Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure PoliceEmailForPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * PoliceEmailForLicense()
   *   Send notification Email to Police / Applicant on License
   * Application/Renewal/Amendment. Note that Documents will not be attached
   * to the email.
   *-------------------------------------------------------------------------*/
  procedure PoliceEmailForLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * ReviewEmailReminder() -- PUBLIC
   *   Send reminder Email to Municipal Users / Applicant on Permit Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure ReviewEmailReminder(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_municipality;
/

create or replace package body pkg_ABC_municipality is

  /*---------------------------------------------------------------------------
   * AssignAccessGroup() -- PUBLIC
   * Intended to run on constructor
   *-------------------------------------------------------------------------*/
  procedure AssignAccessGroup(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_AccessGroupId                     udt_Id;
    t_AccessGroupObjectId               udt_Id;
    t_MunicipalityToAccessGroup                 udt_Id :=
        api.pkg_ConfigQuery.EndPointIdForName('o_ABC_Office', 'WWWAccessGroup');
    t_PoliceAccessGroup                 udt_id :=
        api.pkg_ConfigQuery.EndpointIdForName('o_ABC_Office', 'PoliceWWWAccessGroup');
    t_RelId                             udt_Id;
  begin
    IF api.pkg_ColumnQuery.Value(a_ObjectId, 'WWWAccessGroupName') is null THEN
      -- Create a new Access Group.
      t_AccessGroupId := abc.pkg_InstanceSecurity.NewAccessGroup;

      -- Access Group is already registered... locate its ObjectId
      t_AccessGroupObjectId := api.pkg_simplesearch.ObjectByIndex('o_AccessGroup', 'AccessgroupId', t_AccessGroupId);

      -- Relate Access Group and Municipality
      t_RelId := api.pkg_RelationshipUpdate.New(t_MunicipalityToAccessGroup,
          a_ObjectId, t_AccessGroupObjectId);

  /*    -- Adjust security appropriately.
      abc.pkg_InstanceSecurity.AdjustByColumn(a_User, sysdate,
          api.pkg_ColumnQuery.Value(a_User, 'ReadAccessGroups'),
          api.pkg_ColumnQuery.Value(a_User, 'ModifyAccessGroups'));
  */
   END IF;
   IF api.pkg_ColumnQuery.Value(a_ObjectId, 'PoliceWWWAccessGroupName') is null THEN
     -- Create a new Police Access Group.
     t_AccessGroupId := abc.pkg_InstanceSecurity.NewAccessGroup;

     --Police Access Group is already registered... locate its ObjectId
     t_AccessGroupObjectId := api.pkg_simplesearch.ObjectByIndex('o_AccessGroup', 'AccessgroupId', t_AccessGroupId);

     -- Relate Police Access Group and Municipality
      t_RelId := api.pkg_RelationshipUpdate.New(t_PoliceAccessGroup,
          a_ObjectId, t_AccessGroupObjectId);
    END IF;
  end AssignAccessGroup;

  /*--------------------------------------------------------------------------
  * AddStaff() - Add municipality access group to a user
  * Run on Constructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure AddStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  ) is
    t_user              udt_Id;
    t_municipality      udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    select FromObjectId, ToObjectId into t_user, t_municipality
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;
    t_GroupId := api.pkg_columnquery.value(t_municipality, 'AccessGroupId');
    select count(*) into t_exists
      from api.accessGroupUsers
      where userId = t_user
        and accessGroupId = t_GroupId;
    if t_exists < 1 then
      api.pkg_UserUpdate.AddToAccessGroup(t_user, t_groupid);
    end if;
  end AddStaff;

  /*--------------------------------------------------------------------------
  * AddPoliceStaff() - Add police municipality access group to a user
  * Run on Constructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure AddPoliceStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  ) is
    t_user              udt_Id;
    t_municipality      udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    select FromObjectId, ToObjectId into t_user, t_municipality
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;
    t_GroupId := api.pkg_columnquery.value(t_municipality, 'PoliceAccessGroupId');
    select count(*) into t_exists
      from api.accessGroupUsers
      where userId = t_user
        and accessGroupId = t_GroupId;
    if t_exists < 1 then
      api.pkg_UserUpdate.AddToAccessGroup(t_user, t_groupid);
    end if;
  end AddPoliceStaff;


  /*--------------------------------------------------------------------------
  * RemoveStaff() - Add municipality access group to a user
  * Run on Destructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure RemoveStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  ) is
    t_user              udt_Id;
    t_municipality      udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    --Obtain UserId for outgoing staff relationship
    select FromObjectId, ToObjectId into t_user, t_municipality
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;
    t_GroupId := api.pkg_columnquery.value(t_municipality, 'AccessGroupId');
    select count(*) into t_exists
      from api.accessGroupUsers
      where userId = t_user
        and accessGroupId = t_GroupId;
    if t_exists > 0 then
      api.pkg_UserUpdate.RemoveFromAccessGroup(t_user, t_groupid);
    end if;
  end RemoveStaff;

  /*--------------------------------------------------------------------------
  * RemovePoliceStaff() - Remove police  municipality access group from a user
  * Run on Destructor of the relationship between User and Municipality
  *------------------------------------------------------------------------*/
  procedure RemovePoliceStaff(
    a_RelationshipId udt_Id,
    a_AsOfDate date
  ) is
    t_user              udt_Id;
    t_municipality      udt_Id;
    t_groupId           udt_Id;
    t_exists            pls_integer;
  begin
    --Obtain UserId for outgoing staff relationship
    select FromObjectId, ToObjectId into t_user, t_municipality
      from api.relationships r
      join api.users u on r.FromObjectId = u.userId
     where relationshipid = a_relationshipid;
    t_GroupId := api.pkg_columnquery.value(t_municipality, 'PoliceAccessGroupId');
    select count(*) into t_exists
      from api.accessGroupUsers
      where userId = t_user
        and accessGroupId = t_GroupId;
    if t_exists > 0 then
      api.pkg_UserUpdate.RemoveFromAccessGroup(t_user, t_groupid);
    end if;
  end RemovePoliceStaff;

  /*--------------------------------------------------------------------------
  * CreateOrderIssuanceSeq() - Create a sequence to be used for the
  *   Municipality's Order of Issuance.
  * Run on Constructor of the Municipality
  *------------------------------------------------------------------------*/
  procedure CreateOrderIssuanceSeq(
    a_ObjectId udt_Id,
    a_AsOfDate date,
    a_MunicipalityCountyCode number
  ) is
--  t_MuniCode number(4);
  t_SeqName  varchar2(40);
  t_sql      varchar2(4000);
  PRAGMA AUTONOMOUS_TRANSACTION;
  begin
    t_Seqname := 'abc.muniorderissue' || lpad(a_MunicipalityCountyCode, 4, 0) || '_seq';
    t_sql := 'create sequence ' ||
             t_SeqName ||
             ' increment by 1 start with ' ||
             (case
                when api.pkg_columnquery.value(a_ObjectId, 'OrderofIssuance') = 0
                  or api.pkg_columnquery.value(a_ObjectId, 'OrderofIssuance') is null
                  then 1
                else api.pkg_columnquery.value(a_ObjectId, 'OrderofIssuance')
              end) ||
             ' maxvalue 999 nocycle nocache';
  begin
    execute immediate t_sql;
  exception when others then
    api.pkg_errors.RaiseError(-20001, 'Cannot create sequence: ' ||  t_SeqName || '. ' || sqlerrm);
  end;
  end CreateOrderIssuanceSeq;

  /*--------------------------------------------------------------------------
  * DeleteOrderIssuanceSeq() - Delete the sequence used for the
  *   Municipality's Order of Issuance.
  * Run on Destructor of the Municipality
  *------------------------------------------------------------------------*/
  procedure DeleteOrderIssuanceSeq(
    a_ObjectId udt_Id,
    a_AsOfDate date
  ) is
  t_MuniCode number(4);
  t_SeqName  varchar2(40);
  t_sql      varchar2(4000);
  PRAGMA AUTONOMOUS_TRANSACTION;
  begin
    t_MuniCode := api.pkg_columnquery.value(a_objectid, 'MunicipalityCountyCode');
    t_Seqname := 'abc.muniorderissue' || lpad(t_MuniCode, 4, 0) || '_seq';
    t_sql := 'drop sequence ' || t_SeqName;
    execute immediate t_sql;
  end DeleteOrderIssuanceSeq;

  /*---------------------------------------------------------------------------
   * ValidateWholesaleDefaultFlag() -- PUBLIC
   *   Run on post verify of the Municipality (o_ABC_Office)
   *-------------------------------------------------------------------------*/
  procedure ValidateWholesaleDefaultFlag (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_CountyIsActive                    varchar2(1);
    t_CountyWholesaleDefaultFlag        varchar2(1);
    t_MunicipalityIsActive              varchar2(1);
    t_UserCount                         number(4);
    t_WholesaleDefaultFlag              varchar2(1);
    t_WholesaleDefaultMuniCount         number(4);
  begin

    select muni.WholesaleDefault
    into t_WholesaleDefaultFlag
    from query.o_abc_office muni
    where muni.ObjectId = a_ObjectId;

    --check if any other municipalities are marked as a wholesale default
    if t_WholesaleDefaultFlag = 'Y' then
      select count(*)
      into t_WholesaleDefaultMuniCount
      from query.o_abc_office o
      where o.WholesaleDefault = 'Y';

      if t_WholesaleDefaultMuniCount > 1 then
        api.pkg_errors.RaiseError(-20000, 'Only one municipality is allowed to be marked as'
            || ' Wholesale Default.');
      end if;

      --check if the associated county is also marked as wholesale default also
      select r.WholesaleDefault
      into t_CountyWholesaleDefaultFlag
      from
        query.r_abc_regionoffice ro
        join query.o_abc_region r
            on r.ObjectId = ro.RegionObjectId
      where ro.OfficeObjectId = a_ObjectId;

      if t_CountyWholesaleDefaultFlag = 'N' then
        api.pkg_errors.RaiseError(-20000, 'The county must also be marked as Wholesale Default.');
      end if;
    end if;

    select api.pkg_columnquery.Value(o.CountyObjectId, 'Active')
    into t_CountyIsActive
    from query.o_abc_office o
    where o.ObjectId = a_ObjectId;

    select count(distinct u.ObjectId)
    into t_UserCount
    from
      api.objects o
      join api.relationships r
          on r.FromObjectId = o.ObjectId
      join query.u_users u
          on r.ToObjectId = u.ObjectId
    where o.ObjectId = a_ObjectId
      and u.UserType in ('Municipal', 'Police')
      and u.Active = 'Y';

    if t_CountyIsActive = 'N' then
      api.pkg_errors.RaiseError(-20009, 'Before you proceed to activate the Municipality, you must'
          || ' choose an active County.');
    end if;

    t_MunicipalityIsActive := api.pkg_columnquery.Value(a_ObjectId, 'Active');
    if t_UserCount > 0 and t_MunicipalityIsActive = 'N' then
      api.pkg_errors.RaiseError(-20009, 'You cannot deactivate the Municipality when there are'
          || ' Municipal or Police Users associated.');
    end if;

  end ValidateWholesaleDefaultFlag;

  /*---------------------------------------------------------------------------
   * MuniEmailForPermit() -- PUBLIC
   *   Send notification Email to Municipality / Applicant on Permit
   * Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure MuniEmailForPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_MunicipalityObjectId              udt_id;
    t_JobId                             integer;
    t_PermitObjectId                    integer;
    t_PermitteeObjectId                 integer;
    t_NotifyMunicipality                varchar2(1);
    t_MunicipalityRequired              varchar2(1);
    t_From                              varchar2(4000);
    t_Subject                           varchar2(4000);
    t_IsProduction                      varchar2(1);
    t_To                                varchar2(4000);
    t_HeaderImageName                   varchar2(4000);
    t_HeaderText                        varchar2(4000);
    t_FooterText                        varchar2(4000);
    t_baseURL                           varchar2(4000);
    t_JobDefName                        varchar2(200);
    t_PermitType                        varchar2(200);
    t_PermitTypeCode                    varchar2(20);
    t_JobNumber                         varchar2(200);
    t_PermitteeName                     varchar2(200);
    t_LicenseNumber                     varchar2(50);
    t_MailingAddress                    varchar2(4000);
    t_PhysicalAddress                   varchar2(4000);
    t_ContactName                       varchar2(250);
    t_ContactPhoneNumber                varchar2(50);
    t_County                            varchar2(200);
    t_Municipality                      varchar2(200);
    t_AdditionalPermitInfo              varchar2(4000);
    t_SAQuestion                        varchar2(500);
    t_EventLocationAddress              varchar2(4000);
    t_EventLocAddressDescription        varchar2(4000);
    t_EventDetails                      varchar2(4000);
    t_AnswerOnJob                       varchar2(2000);
    t_AcceptableAnswer                  varchar2(2000);
    t_RegistrantEmail                   varchar2(4000);
    t_OnlineUserId                      integer;
    t_EventDatesCLOB                    clob;
    t_RainDatesCLOB                     clob;
    t_QuestionsCLOB                     clob;
    t_DocumentListCLOB                  clob;
    t_Body                              clob;
    t_Body1                             clob;
    t_HeaderTextCLOB                    clob;
    t_FooterTextCLOB                    clob;
    t_Body2                             clob;
    t_PermitTypeCLOB                    clob;
    t_Body3                             clob;
    t_JobNumberCLOB                     clob;
    t_Body4                             clob;
    t_PermitteeNameCLOB                 clob;
    t_Body12                            clob;
    t_LicenseNumberCLOB                 clob;
    t_Body5                             clob;
    t_MailingAddressCLOB                clob;
    t_Body6                             clob;
    t_PhysicalAddressCLOB               clob;
    t_Body7                             clob;
    t_ContactNameCLOB                   clob;
    t_Body8                             clob;
    t_ContactPhoneNumberCLOB            clob;
    t_Body8a                            clob;
    t_RegistrantEmailCLOB               clob;
    t_Body9                             clob;
    t_AdditionalPermitInfoCLOB          clob;
    t_Body10                            clob;
    t_BodyEND                           clob;
    t_EventLocationCLOB                 clob;
    t_EventDetailsCLOB                  clob;
    t_Body11                            clob;
    t_MunicipalEmailsCLOB               clob;
  begin

    -- Collect all data needed for the email.  The email will be generated after all data is
    -- collected. This will be run on completion of a process which means we will need to get
    -- the JobId
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    -- Need the Job def name to determine what rels to use to get the required information
    t_JobDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_PermitType := api.pkg_ColumnQuery.Value(t_JobId, 'PermitType');
    -- Check to see if the permit type requires the muni or police to be notified.
    t_NotifyMunicipality := api.pkg_ColumnQuery.Value(
        api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeObjectId'), 'NotifyMunicipality');
    -- Check to see if the permit type requires muni or police review
    t_MunicipalityRequired := api.pkg_ColumnQuery.Value(
        api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeObjectId'), 'MunicipalityRequired');

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      -- Get the information needed that can be found through api.pkg_ColumnQuery
      t_OnlineUserId := api.pkg_ColumnQuery.NumericValue(t_JobId, 'OnlineUserObjectId');
      t_RegistrantEmail := api.pkg_columnquery.value(t_OnlineUserId, 'EmailAddress');
      t_JobNumber := api.pkg_ColumnQuery.Value(t_JobId, 'ExternalFileNum');
      t_PermitObjectId := api.pkg_ColumnQuery.Value(t_JobId, 'PermitObjectId');
      t_PermitTypeCode := api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeCode');
      t_LicenseNumber := coalesce(api.pkg_ColumnQuery.Value(t_PermitObjectid, 'LicenseNumber'),
            'N/A');
      t_PermitteeObjectId := api.pkg_ColumnQuery.Value(t_PermitObjectId, 'PermitteeObjectId');
      t_PermitteeName := nvl(api.pkg_ColumnQuery.Value(t_PermitObjectId, 'PermitteeName'),
          api.Pkg_Columnquery.Value(t_PermitObjectId, 'OnlineLEFormattedName'));
      t_MailingAddress := replace(nvl(
          api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'MailingAddress'),
          api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineLEMailingAddress')),
          chr(13) || chr(10), '<br>');
      if api.pkg_columnquery.Value(t_JobId,'OnlineLESameAsMailing') = 'Y' then
         t_PhysicalAddress := t_MailingAddress;
      else
        t_PhysicalAddress := replace(nvl(
            api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'PhysicalAddress'),
            api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePhysicalAddress')),
            chr(13)||chr(10), '<br>');
      end if;
      t_ContactName := coalesce(api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'ContactName'),
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactName'), t_PermitteeName);
      t_ContactPhoneNumber := REGEXP_REPLACE(nvl(
          api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'ContactPhoneNumber'),
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactPhone')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');
      t_County := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'County');
      t_Municipality := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'Municipality');
      t_MunicipalityObjectId := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'MunicipalityObjectId');
      t_AdditionalPermitInfo := api.pkg_ColumnQuery.Value(t_PermitObjectId,
          'AdditionalPermitInformation');
      t_EventLocationAddress := replace(nvl(
          nvl(api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineEventLocationAddress'),
          api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineEventLocationAddress')),
          api.Pkg_Columnquery.Value(t_PermitObjectId, 'EventLocationAddress')),
          chr(13)||chr(10), '<br>');
      t_EventLocAddressDescription := api.pkg_ColumnQuery.Value(t_PermitObjectId,
          'EventLocAddressDescription');
      t_EventDetails := api.pkg_ColumnQuery.Value(t_PermitObjectId, 'EventDetails');

      -- If permit is a Social Affair permit get the answer to the SA Question.
      if lower(t_PermitTypeCode) = 'sa' then
        t_SAQuestion := 'Was the Non-Profit Group/Organization formed as a ' ||
            'Religious, Civic or Educational Entity?<br>' ||
            nvl(api.pkg_ColumnQuery.Value(t_JobId,'OnlineCivicReligOrEduc'),
            api.pkg_ColumnQuery.Value(t_JobId, 'CivicReligOrEduc'));
      end if;

      -- Get the event dates and convert to clob with HTML formatting for the body of the email.
      for e in (
          select ed.EventDateObjectId
          from query.r_Abc_Permiteventdate ed
          where ed.PermitObjectId = t_PermitObjectId
          ) loop
        t_EventDatesCLOB := t_EventDatesCLOB || to_clob(to_char(
            api.pkg_ColumnQuery.DateValue(e.eventdateobjectid, 'StartDate'), 'Mon DD, YYYY'));
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'StartTimeHours') is not null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || ' from ' ||
              api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'FormattedStartTime'));
        end if;
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'EndTimeHours') is not null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || ' to ' ||
              api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'FormattedEndTime') || '<br>');
        end if;
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'EndTimeHours') is null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || '<br>');
        end if;
      end loop;
      -- Add Event Dates header to t_EventDatesCLOB if there were any event dates.
      if t_EventDatesCLOB is not null then
        t_EventDatesCLOB := '<br><div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"> <u>Event Dates</u><br>' || t_EventDatesCLOB ||
            '</div>';
      end if;

      -- Get the Rain Dates and convert to clob with HTML formatting for the body of the email.
      for r in (
          select rd.RainDateObjectId
          from query.r_ABC_PermitRainDate rd
          where rd.PermitObjectId = t_PermitObjectId
          ) loop
        t_RainDatesCLOB := t_RainDatesCLOB || to_clob(to_char(
            api.pkg_ColumnQuery.DateValue(r.RainDateObjectId, 'StartDate'), 'Mon DD, YYYY'));
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'StartTimeHours') is not null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || ' from ' ||
              api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'FormattedStartTime'));
        end if;
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'EndTimeHours') is not null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || ' to ' ||
              api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'FormattedEndTime') || '<br>');
        end if;
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'EndTimeHours') is null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || '<br>');
        end if;
      end loop;
      -- Add Rain Dates header to t_RainDatesCLOB if there were any rain dates.
      if t_RainDatesCLOB is not null then
        t_RainDatesCLOB := '<br><u>Rain Dates</u><br>' || t_RainDatesCLOB;
      end if;

      -- Get the information from the Permit Application.
      if t_JobDefName = 'j_ABC_PermitApplication' then
        -- If there is a rel to questions start creation of the HTML table
        if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
          -- Table header
          t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br><u>Application Questions</u><br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;">');
           -- Loop through all QAResponses
          for q in (
               select qa.ResponseId
               from query.r_ABC_PermitAppQAResponse qa
               where qa.PermitApplicationId = t_JobId
               order by api.pkg_columnquery.NumericValue(qa.ResponseId, 'SortOrder')
               ) loop
            t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Question ' ||
                api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') || ':</td>' ||
                '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'Question') || '</td></tr>');
            if nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A' then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                  "vertical-align:top; text-align:left">Yes / No Response:</td>' ||
                  '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
            end if;
            t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.responseid, 'AnswerOnJob');
            t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.responseid, 'AcceptableAnswer');
            if nvl(t_AnswerOnJob, 'N') != nvl(t_AcceptableAnswer, 'Y')
                and t_AcceptableAnswer is not null then
              if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                  then
                if nvl(t_AcceptableAnswer, 'N') != 'N/A' then
                  t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                      "width: 250px; vertical-align:top; text-align:left"></td>'
                      || '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText')
                      || '</td></tr>');
                end if;
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "vertical-align:top; text-align:left">Response:</td><td>'
                    || nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                    to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId,
                    'ResponseDate'), 'Mon DD, YYYY')) || '</td></tr>');
               end if;
            end if;
            t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
          end loop;
          -- Close the table
          t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
        end if;

        -- If there is a rel to documents create table for the documents
        if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
          -- Table header for documents
          t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
              Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Documents</u><br>
              See below for a list of documents attached to this email.<br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><tr><th align="left">Document Type</th>
              <th align="left">File Name</th><th align="left">Upload Date</th>
              </tr>');

          -- Loop through Online Uploads
          for d2 in (
              select
                api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
                dr.FileName,
                lt.CreatedDate
              from query.r_OLNewPermitAppElectronicDoc AppDoc
              join api.documentrevisions dr
                  on AppDoc.OLDocumentId = dr.DocumentId
              join query.r_ElectronicDocDocType dt
                  on dr.DocumentId = dt.DocumentId
              join api.logicaltransactions lt
                  on dr.LogicalTransactionId = lt.LogicalTransactionId
              where AppDoc.OLNewPermitAppJobId = t_JobId
              ) loop
            t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td>' || d2.doctype ||
                '</td><td>' || d2.filename || '</td><td>' || to_char(d2.Createddate, 'Mon DD, YYYY')
                || '</td></tr>');
          end loop;
          -- Close the table
          t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div>';
        end if;
      end if;

      -- Get the information from the Permit Renewal.
      if t_JobDefName = 'j_ABC_PermitRenewal' then
        -- If there is a rel to questions start creation of the HTML table
        if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
          -- Table header
          t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Application Questions
              </u><br><table style="width:100%; font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;">');
          -- Loop through all QAResponses
          for q in (
              select qa.ResponseId
              from query.r_ABC_PermitRnwQAResponse qa
              where qa.PermitRenewalId = t_JobId
              order by api.pkg_columnquery.NumericValue(qa.ResponseId, 'SortOrder')
              ) loop
            t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Question ' ||
                api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') ||
                ':</td><td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'Question')
                || '</td></tr>');
            if nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A' then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                  "vertical-align:top; text-align:left">Yes / No Response:</td><td>'
                  || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
            end if;
            t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.responseid, 'AnswerOnJob');
            t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.responseid, 'AcceptableAnswer');
            if nvl(t_AnswerOnJob, 'N') != nvl(t_AcceptableAnswer, 'Y')
                and t_AcceptableAnswer is not null then
              if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                  then
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "vertical-align:top; text-align:left">Response:</td><td>'
                    || nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                    to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId, 'ResponseDate'),
                    'Mon DD, YYYY')) || '</td></tr>');
                if nvl(t_AcceptableAnswer, 'N') != 'N/A' then
                  t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td
                      style="width: 250px; vertical-align:top; text-align:left">
                      Non-Acceptable Response Text:</td><td>' ||
                      api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText') || '</td></tr>');
                end if;
              end if;
            end if;
            t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
          end loop;
          -- Close the table
          t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
        end if;

        -- If there is a rel to documents creat table for the documents
        if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
          -- Table header for documents
          t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
              Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Documents</u><br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><tr><th align="left">Document Type</th>
              <th align="left">File Name</th><th align="left">Upload Date</th>
              </tr>');
          for d2 in (
              select
                api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
                dr.FileName,
                lt.CreatedDate
              from
                query.r_OLPermitRenewElectronicDoc AppDoc
                join api.documentrevisions dr
                    on AppDoc.OLDocumentId = dr.DocumentId
                join query.r_ElectronicDocDocType dt
                    on dr.DocumentId = dt.DocumentId
                join api.logicaltransactions lt
                    on dr.LogicalTransactionId = lt.LogicalTransactionId
              where AppDoc.OLPermitRenewJobId = t_JobId
              ) loop
            t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td>' || d2.doctype ||
                '</td><td>' || d2.filename || '</td><td>' || to_char(d2.Createddate, 'Mon DD, YYYY')
                || '</td></tr>');
          end loop;
          --Close the table
          t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div>';
        end if;
      end if;

      -- Retrieve active Municipal and Police Users for Municipality. Emails are sorted
      -- alphabetically
      for i in (
          select EmailAddress
          from (
              select api.pkg_ColumnQuery.Value(mu.MunicipalUserObjectId, 'EmailAddress')
                  EmailAddress
              from
                query.o_ABC_Office m
                join query.r_MunicipalUserMunicipality mu
                    on m.objectid = mu.MunicipalityObjectId
              where m.ObjectId = t_MunicipalityObjectId
                and api.pkg_columnquery.Value(mu.MunicipalUserObjectId, 'Active') = 'Y'
                and t_NotifyMunicipality = 'Y'
              union
              select t_RegistrantEmail
              from dual
              )
          order by lower(EmailAddress)
          ) loop
            t_MunicipalEmailsCLOB := t_MunicipalEmailsCLOB || to_clob('- ' || i.EmailAddress ||
                '<br>');
      end loop;

      -- Get the details needed from the system settings
      select
        ss.FromEmailAddress,
        ss.IsProduction,
        ss.ToEmailAddressNonProd,
        ss.MunicipalWebsiteBaseUrl
      into
        t_From,
        t_IsProduction,
        t_To,
        t_BaseURL
      from query.o_systemsettings ss
      where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
          'SystemSettingsNumber', 1);

      -- Conditionally get Endorsement/Standard details from system settings. When review is
      -- required, populate the email w/ Endorsement details
      if t_MunicipalityRequired = 'Y' then
        select
          ss.MuniEndEmailSubject,
          ss.MuniEndEmailHeaderPhotoURL,
          ss.MuniEndEmailBodyHeader,
          ss.MuniEndEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      -- Otherwise, send the standard email.
      else
        select
          ss.MuniStdEmailSubject,
          ss.MuniStdEmailHeaderPhotoURL,
          ss.MuniStdEmailBodyHeader,
          ss.MuniStdEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      end if;

      -- Create the body of the email in a clob. Breaking this into several clobs to concat
      -- together in hopes of making this easier to read.
      t_Body1 := '<table cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="100%"><tbody><tr><td style="padding: 20px;"><table
          border="0" cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="600"style="margin: 0px auto; border-radius: 10px;
          background-color: rgb(255, 255, 255);"><tbody><tr><td colspan="2">
          </td></tr><tr><td width="600"></td></tr><tr><td colspan="2" height="24">
          <img alt="New Jersey ABC" src="' || t_HeaderImageName || '" border="0"
          style="display: block; margin: 0px; padding-bottom: 3px;"></td></tr>
          <tr><td colspan="2"><table border="0" cellpadding="0" cellspacing="0"
          width="600"><tbody><tr><td align="left"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><br><br>';
      t_HeaderTextCLOB := to_clob(t_HeaderText);

      t_Body11 := to_clob('<br>This notification is being sent to the
          following email addresses:<br>');

      t_Body2 := to_clob('</div><div style="font-family: Calibri, Arial,
          Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
          line-height: 1.4em;"><br><table style="width:100%; font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
          rgb(0, 0, 0); line-height: 1.4em;"><tr><td>Permit Type:</td><td>');
      t_PermitTypeCLOB := to_clob(t_PermitType || '</td>');

      t_Body3 := '</tr><tr><td style="width: 250px;">File Number:</td><td>';
      t_JobNumberCLOB := to_clob(t_JobNumber || '</td>');

      t_Body4 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Permittee:</td>
          <td>');
      t_PermitteeNameCLOB := to_clob(t_PermitteeName || '</td>');

      t_Body12 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>License Number:</td>
          <td>');
      t_LicenseNumberCLOB := to_clob(t_LicenseNumber || '</td>');

      t_Body5 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Mailing Address:</td><td>');
      t_MailingAddressCLOB := to_clob(t_MailingAddress || '</td>');

      t_Body6 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Physical Address:</td><td>');
      t_PhysicalAddressCLOB := to_clob(t_PhysicalAddress || '</td>');

      t_Body7 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Contact:</td><td>');
      t_ContactNameCLOB := to_clob(t_ContactName || '</td>');

      t_Body8 := to_clob('</tr><tr><td></td><td>');
      t_ContactPhoneNumberCLOB := to_clob(t_ContactPhoneNumber || '</td><br>');

      t_Body8a := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Applicant Email:</td><td>');
      t_RegistrantEmailCLOB := to_clob(t_RegistrantEmail || '</td>');

      t_Body9 := to_clob('</tr>');
      t_FooterTextCLOB := to_clob(t_FooterText);

      -- If any type of additional information is needed create the header for the Additional info
      -- section.
      if t_County is not null or t_Municipality is not null or t_AdditionalPermitInfo is not null
          or lower(t_PermitTypeCode) = 'sa' then
        t_AdditionalPermitInfoCLOB := to_clob('<tr><td><u>Additional Permit Information</u></td></tr>');
        if t_County is not null then
          t_AdditionalPermitInfoCLOB :=  t_AdditionalPermitInfoCLOB ||
              to_clob('<tr><td>County:</td><td>' || t_County || '</td></tr>');
        end if;
        if t_Municipality is not null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_Clob('<tr><td>Municipality:</td><td>' || t_Municipality ||
              '</td></tr>');
        end if;
        if t_AdditionalPermitInfo is not null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('</table></div><div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br>' || t_AdditionalPermitInfo || '</div>');
        end if;
        if t_AdditionalPermitInfo is null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('</table></div>');
        end if;
        if lower(t_PermitTypeCode) = 'sa' then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('<div style="font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br>' || t_SAQuestion || '<br></div>');
        end if;
      end if;

      if t_AdditionalPermitInfoCLOB is null then
        t_Body10 := to_clob('</table></div>');
      end if;

      if t_EventLocationAddress is not null or t_EventLocAddressDescription is not null then
        t_EventLocationCLOB := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Location</u><br>');
        if t_EventLocAddressDescription is not null then
          t_EventLocationCLOB := t_EventLocationCLOB ||
              to_clob('Location Description:<br>' || t_EventLocAddressDescription || '<br>');
        end if;
        if t_EventLocationAddress is not null then
          t_EventLocationCLOB := t_EventLocationCLOB || to_clob('<br>Address:<br>'
              || t_EventLocationAddress || '<br>');
        end if;
        t_EventLocationCLOB := t_EventLocationCLOB || to_clob('</div>');
      end if;

      if t_EventDetails is not null then
        t_EventDetailsCLOB := to_clob('<div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><u>Event Details</u><br>What is the specific
            event being held?<br>' || t_EventDetails || '</div>');
      end if;

      t_BodyEND := to_clob('</td></tr></tbody></table></tbody></table><br></td>
          </tr></tbody></table>');
      t_Body := t_body1 || t_HeaderTextCLOB || t_Body11 || t_MunicipalEmailsCLOB ||
          t_Body2 || t_PermitTypeCLOB || t_Body3 || t_JobNumberCLOB ||
          t_body4 || t_PermitteeNameCLOB || t_Body12 || t_LicenseNumberCLOB ||
          t_Body5 || t_MailingAddressCLOB || t_Body6 || t_PhysicalAddressCLOB ||
          t_Body7 || t_ContactNameCLOB || t_Body8 || t_ContactPhoneNumberCLOB ||
          t_Body8a || t_RegistrantEmailCLOB || t_Body9 || t_AdditionalPermitInfoCLOB ||
          t_Body10 || t_EventLocationCLOB || t_EventDetailsCLOB || t_EventDatesCLOB ||
          t_RainDatesCLOB || t_QuestionsCLOB || t_DocumentListCLOB || t_FooterTextClob ||
          t_BodyEnd;

      -- Get the users that the email should be sent to
      if t_IsProduction = 'Y' then
        -- If the system is a production system send it to the user who applied
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_RegistrantEmail, null,
            null, t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
        -- If the system is a production system send it to the clerks related to the municipality.
        if t_NotifyMunicipality = 'Y' then
          for i in (
              select
                mu.MunicipalUserObjectId,
                api.pkg_ColumnQuery.Value(mu.MunicipalUserObjectId, 'EmailAddress') ToAddress
              from
                query.o_ABC_Office m
                join query.r_MunicipalUserMunicipality mu
                    on m.objectid = mu.MunicipalityObjectId
              where m.ObjectId = t_MunicipalityObjectId
              ) loop
            if api.pkg_columnquery.Value(i.MunicipalUserObjectId, 'Active') = 'Y' then
              extension.pkg_sendmail.SendEmail(t_JobId, t_From, i.toaddress, null,
                  null, t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
            end if;
          end loop;
        end if;
      else
        -- If the system is a test system send it to the test email on the config site
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_To, null, null,
            t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
      end if;
    end if;

  end MuniEmailForPermit;

  /*---------------------------------------------------------------------------
   * MuniEmailForLicense() -- PUBLIC
   *   Send notification Email to Municipality / Applicant on License
   * Application/Renewal/Amendment. Note that Documents will not be attached
   * to the email.
   *-------------------------------------------------------------------------*/
  procedure MuniEmailForLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_AcceptableAnswer                  varchar2(2000);
    t_AnswerOnJob                       varchar2(2000);
    t_baseURL                           varchar2(4000);
    t_Body                              clob;
    t_Body1                             clob;
    t_Body2                             clob;
    t_Body3                             clob;
    t_Body4                             clob;
    t_Body5                             clob;
    t_Body6                             clob;
    t_Body7                             clob;
    t_Body8                             clob;
    t_Body9                             clob;
    t_Body10                            clob;
    t_Body11                            clob;
    t_Body12                            clob;
    t_Body13                            clob;
    t_Body14                            clob;
    t_Body15                            clob;
    t_Body16                            clob;
    t_Body17                            clob;
    t_Body18                            clob;
    t_Body19                            clob;
    t_Body20                            clob;
    t_Body21                            clob;
    t_Body22                            clob;
    t_Body23                            clob;
    t_Body24                            clob;
    t_Body25                            clob;
    t_Body26                            clob;
    t_BodyEND                           clob;
    t_ContactAltPhoneNumber             varchar2(50);
    t_ContactAltPhoneNumberCLOB         clob;
    t_ContactEmailAddress               varchar2(100);
    t_ContactEmailAddressCLOB           clob;
    t_ContactFaxNumber                  varchar2(50);
    t_ContactFaxNumberCLOB              clob;
    t_ContactName                       varchar2(250);
    t_ContactNameCLOB                   clob;
    t_ContactNameLE                     varchar2(100);
    t_ContactPhoneNumber                varchar2(50);
    t_ContactPhoneNumberCLOB            clob;
    t_ContactPhoneNumberLE              varchar2(50);
    t_CorporationNumber                 varchar2(30);
    t_CorporationNumberCLOB             clob;
    t_County                            varchar2(200);
    t_DocumentListCLOB                  clob;
    t_DoingBusinessAs                   varchar2(400);
    t_DoingBusinessAsCLOB               clob;
    t_EmailsCLOB                        clob;
    t_EndTableRowDiv                    clob;
    t_EnteredOnline                     varchar2(1);
    t_EstablishmentObjectId             udt_Id;
    t_EstablishmentType                 varchar2(50);
    t_EstablishmentTypeCLOB             clob;
    t_FarmWineryProduction              varchar2(4000);
    t_FarmWineryWholesalePrivilege      varchar2(5);
    t_FeeAmount                         varchar2(20);
    t_FeeAmountCLOB                     clob;
    t_FooterText                        varchar2(4000);
    t_FooterTextCLOB                    clob;
    t_From                              varchar2(4000);
    t_HeaderImageName                   varchar2(4000);
    t_HeaderText                        varchar2(4000);
    t_HeaderTextCLOB                    clob;
    t_IncorporationDate                 date;
    t_IncorporationDateCLOB             clob;
    t_IsProduction                      varchar2(1);
    t_JobId                             udt_Id;
    t_JobNumber                         varchar2(200);
    t_JobNumberCLOB                     clob;
    t_JobType                           varchar2(60);
    t_JobTypeCLOB                       clob;
    t_LegalEntityName                   varchar2(400);
    t_LegalEntityObjectId               udt_Id;
    t_LicenseDetailsCLOB                clob;
    t_LicenseeName                      varchar2(200);
    t_LicenseeNameCLOB                  clob;
    t_LicenseNumber                     varchar2(50);
    t_LicenseNumberCLOB                 clob;
    t_LicenseObjectId                   udt_Id;
    t_LicenseType                       varchar2(200);
    t_LicenseTypeCLOB                   clob;
    t_LicenseTypeCode                   varchar2(20);
    t_LicenseTypeObjectId               udt_Id;
    t_LimitedBreweryProduction          varchar2(4000);
    t_MailingAddress                    varchar2(4000);
    t_MailingAddressCLOB                clob;
    t_MailingAddressEST                 varchar2(4000);
    t_MailingAddressESTCLOB             clob;
    t_MailingAddressLE                  varchar2(4000);
    t_MunicipalityObjectId              udt_Id;
    t_NJTaxAuthNumber                   varchar2(20);
    t_NJTaxAuthNumberCLOB               clob;
    t_NotifyMunicipality                varchar2(1);
    t_NotifyPolice                      varchar2(1);
    t_OnlineLECorporationNumber         varchar2(30);
    t_OnlineLEFormattedName             varchar2(400);
    t_OnlineLEIncorporationDate         date;
    t_OnlineLELegalName                 varchar2(100);
    t_OnlineLEMailingAddress            varchar2(4000);
    t_OnlineLEMailingAddressNewApp      varchar2(4000);
    t_OnlineLEPhysicalAddress           varchar2(4000);
    t_OnlineLEPreferredContactMethod    varchar2(4000);
    t_OnlineSecondaryLicenseTypes       varchar2(4000);
    t_OnlineSecondaryLicenseTypesCLOB   clob;
    t_OnlineUserId                      udt_Id;
    t_OnlineWarehouseAddress            varchar2(4000);
    t_Operator                          varchar2(400);
    t_OperatorCLOB                      clob;
    t_OutOfStateWineryProduction        varchar2(4000);
    t_OutOfStateWineryWholesalePrivi    varchar2(5);
    t_PhysicalAddress                   varchar2(4000);
    t_PhysicalAddressCLOB               clob;
    t_PhysicalAddressEST                varchar2(4000);
    t_PhysicalAddressESTCLOB            clob;
    t_PhysicalAddressLE                 varchar2(4000);
    t_PlenaryWineryProduction           varchar2(4000);
    t_PlenaryWineryWholesalePrivileg    varchar2(5);
    t_PreferredContactMethod            varchar2(4000);
    t_PreferredContactMethodCLOB        clob;
    t_QuestionsCLOB                     clob;
    t_ReceiptNumber                     varchar2(100);
    t_ReceiptNumberCLOB                 clob;
    t_RegistrantEmail                   varchar2(4000);
    t_RegistrantEmailCLOB               clob;
    t_RestrictedBreweryProduction       varchar2(4000);
    t_RetailTransitType                 varchar2(4000);
    t_SAQuestion                        varchar2(500);
    t_SportingFacilityCapacity          varchar2(4000);
    t_SubHeader1                        clob;
    t_SubHeader2                        clob;
    t_SubHeader3                        clob;
    t_SubHeader4                        clob;
    t_Subject                           varchar2(4000);
    t_SubmittedDateCLOB                 clob;
    t_SupplementaryLimitedDistillery    varchar2(4000);
    t_To                                varchar2(4000);
    t_WarehouseName                     varchar2(100);
    t_WarehousePhoneNumber              varchar2(50);
  begin

    -- Collect all data needed for the email.  The email will be generated after all data is
    -- collected. This will be run on completion of a process which means we will need to get
    -- the JobId
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    -- Need the Job def name to determine what rels to use to get the required information
    t_JobType := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefDescription');

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      -- Get the information needed that can be found through api.pkg_ColumnQuery
      t_OnlineUserId := api.pkg_ColumnQuery.NumericValue(t_JobId, 'OnlineUserObjectId');
      t_JobNumber := api.pkg_ColumnQuery.Value(t_JobId, 'ExternalFileNum');
      t_LicenseObjectId := api.pkg_ColumnQuery.NumericValue(t_JobId, 'LicenseObjectId');
      t_OnlineSecondaryLicenseTypes := api.pkg_ColumnQuery.Value(t_JobId,
          'OnlineSecondaryLicenseTypes');
      t_ReceiptNumber := api.pkg_ColumnQuery.Value(t_JobId, 'LatestReceiptNumber');
      t_FeeAmount := to_char(api.pkg_ColumnQuery.NumericValue(t_JobId, 'TotalFeeAmount'),
          '$999,990.00');
      t_RegistrantEmail := api.pkg_ColumnQuery.Value(t_OnlineUserId, 'EmailAddress');

      -- Details from License
      if t_LicenseObjectId is not null then
        select
          EstablishmentObjectId,
          LicenseTypeObjectId,
          LicenseNumber,
          OfficeObjectId,
          OnlineLEFormattedName,
          OnlineLEMailingAddress,
          RestrictedBreweryProduction,
          LimitedBreweryProduction,
          SupplementaryLimitedDistillery,
          PlenaryWineryProduction,
          PlenaryWineryWholesalePrivileg,
          FarmWineryProduction,
          FarmWineryWholesalePrivilege,
          OutOfStateWineryProduction,
          OutOfStateWineryWholesalePrivi,
          RetailTransitType,
          WarehouseName,
          WarehousePhoneNumber,
          OnlineWarehouseAddress,
          SportingFacilityCapacity
        into
          t_EstablishmentObjectId,
          t_LicenseTypeObjectId,
          t_LicenseNumber,
          t_MunicipalityObjectId,
          t_OnlineLEFormattedName,
          t_OnlineLEMailingAddress,
          t_RestrictedBreweryProduction,
          t_LimitedBreweryProduction,
          t_SupplementaryLimitedDistillery,
          t_PlenaryWineryProduction,
          t_PlenaryWineryWholesalePrivileg,
          t_FarmWineryProduction,
          t_FarmWineryWholesalePrivilege,
          t_OutOfStateWineryProduction,
          t_OutOfStateWineryWholesalePrivi,
          t_RetailTransitType,
          t_WarehouseName,
          t_WarehousePhoneNumber,
          t_OnlineWarehouseAddress,
          t_SportingFacilityCapacity
        from query.o_ABC_License
        where ObjectId = t_LicenseObjectId;
      end if;

      -- Details from License Type
      if t_LicenseTypeObjectId is not null then
        select
          Name,
          Code,
          NotifyMunicipality,
          NotifyPolice
        into
          t_LicenseType,
          t_LicenseTypeCode,
          t_NotifyMunicipality,
          t_NotifyPolice
        from query.o_ABC_LicenseType
        where ObjectId = t_LicenseTypeObjectId;
      end if;

      -- Select Legal Entity associated to Online User
      if t_OnlineUserId is not null then
        begin
          select
            r.ToObjectId
          into
            t_LegalEntityObjectId
          from api.Relationships r
            join api.RelationshipDefs rd
                on rd.RelationshipDefId = r.RelationshipDefId
          where r.FromObjectId = t_LicenseObjectId
          and rd.ToEndPointName = 'Licensee';
        exception when no_data_found then
          null;
        end;
      end if;

      -- Details from Legal Entity
      if t_LegalEntityObjectId is not null then
        select
          ContactName,
          ContactPhoneNumber,
          ContactAltPhoneNumber,
          ContactFaxNumber,
          ContactEmailAddress,
          CorporationNumber,
          dup_FormattedName,
          IncorporationDate,
          MailingAddress,
          NJTaxAuthNumber,
          PhysicalAddress,
          PreferredContactMethod
        into
          t_ContactNameLE,
          t_ContactPhoneNumberLE,
          t_ContactAltPhoneNumber,
          t_ContactFaxNumber,
          t_ContactEmailAddress,
          t_CorporationNumber,
          t_LegalEntityName,
          t_IncorporationDate,
          t_MailingAddressLE,
          t_NJTaxAuthNumber,
          t_PhysicalAddressLE,
          t_PreferredContactMethod
        from query.o_ABC_LegalEntity
        where ObjectId = t_LegalEntityObjectId;
      end if;

      -- Details from New Application if applicable
      if t_JobType = 'New Application' then
        select
          OnlineLECorporationNumber,
          OnlineLELegalName,
          OnlineLEIncorporationDate,
          OnlineLEMailingAddress,
          OnlineLEPhysicalAddress,
          OnlineLEPreferredContactMethod
        into
          t_OnlineLECorporationNumber,
          t_OnlineLELegalName,
          t_OnlineLEIncorporationDate,
          t_OnlineLEMailingAddressNewApp,
          t_OnlineLEPhysicalAddress,
          t_OnlineLEPreferredContactMethod
        from query.j_ABC_NewApplication
        where ObjectId = t_JobId;
      end if;

      -- Details from Establishment
      if t_EstablishmentObjectId is not null then
        select
          DoingBusinessAs,
          EstablishmentType,
          MailingAddress,
          Operator,
          PhysicalAddress
        into
          t_DoingBusinessAs,
          t_EstablishmentType,
          t_MailingAddressEST,
          t_Operator,
          t_PhysicalAddressEST
        from query.o_ABC_Establishment
        where ObjectId = t_EstablishmentObjectId;
      end if;

      t_LicenseeName := coalesce(t_LegalEntityName, t_OnlineLEFormattedName, t_OnlineLELegalName);
      t_MailingAddress := replace(coalesce(t_MailingAddressLE, t_OnlineLEMailingAddress,
        t_OnlineLEMailingAddressNewApp), chr(13) || chr(10), '<br>');

      if api.pkg_ColumnQuery.Value(t_JobId,'OnlineLESameAsMailing') = 'Y' then
         t_PhysicalAddress := t_MailingAddress;
      else
        t_PhysicalAddress := replace(coalesce(t_PhysicalAddressLE,
            api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePhysicalAddress'), t_OnlineLEPhysicalAddress),
            chr(13)||chr(10), '<br>');
      end if;

      t_CorporationNumber := coalesce(t_CorporationNumber, t_OnlineLECorporationNumber);
      t_IncorporationDate := coalesce(t_IncorporationDate, t_OnlineLEIncorporationDate);

      -- Contact Information
      t_ContactName := coalesce(t_ContactNameLE,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactName'), t_LicenseeName);

      t_ContactPhoneNumber := REGEXP_REPLACE(coalesce(t_ContactPhoneNumberLE,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactPhone')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');

      t_ContactAltPhoneNumber := REGEXP_REPLACE(coalesce(t_ContactAltPhoneNumber,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEAltPhone')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');

      t_ContactFaxNumber := REGEXP_REPLACE(coalesce(t_ContactFaxNumber,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEFax')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');

      t_ContactEmailAddress := coalesce(t_ContactEmailAddress,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEEmail'));

      t_PreferredContactMethod := coalesce(t_PreferredContactMethod,
          t_OnlineLEPreferredContactMethod);

      -- Establishment
      t_DoingBusinessAs := coalesce(t_DoingBusinessAs,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineESDoingBusinessAs'));
      t_EstablishmentType := coalesce(t_EstablishmentType,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineESEstablishmentType'));
      t_MailingAddressEST := coalesce(t_MailingAddressEST,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineMailingAddressES'));
      t_Operator := coalesce(t_Operator,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineESOperator'));
      t_PhysicalAddressEST := coalesce(t_PhysicalAddressEST,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePremiseAddressES'));

      t_MailingAddressEST := replace(t_MailingAddressEST,chr(13) || chr(10), '<br>');

      if api.pkg_ColumnQuery.Value(t_JobId, 'OnlineESSameAsMailing') = 'Y' then
        t_PhysicalAddressEST := t_MailingAddressEST;
      else
        t_PhysicalAddressEST := to_clob(replace(t_PhysicalAddressEST,chr(13) || chr(10), '<br>') ||
            '</td>');
      end if;

      -- QA
      if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
        -- Table header
        t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><br><u>Application Questions</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;">');
        -- Loop through all QAResponses
        for q in (
            select r.ToObjectId ResponseId
            from api.Relationships r
              join api.RelationshipDefs rd
                  on rd.RelationshipDefId = r.RelationshipDefId
            where r.FromObjectId = t_JobId
              and rd.ToEndPointName = 'Response'
            order by api.pkg_columnquery.NumericValue(ResponseId, 'SortOrder')
            ) loop
          t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style="width: 150px;
              vertical-align:top; text-align:left">Question ' ||
              api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') || ':</td><td>' ||
              api.pkg_ColumnQuery.Value(q.ResponseId, 'Question') || '</td></tr>');
          if coalesce(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A'
            then t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Yes / No Response:</td><td>'
                || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
          end if;
          t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob');
          t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer');
          if coalesce(t_AnswerOnJob, 'N') != coalesce(t_AcceptableAnswer, 'Y')
              and t_AcceptableAnswer is not null then
            if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style="width: 150px;
                  vertical-align:top; text-align:left">Response:</td><td>'
                  || coalesce(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                  to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId, 'ResponseDate'),
                  'Mon DD, YYYY')) || '</td></tr>');
              if coalesce(t_AcceptableAnswer, 'N') != 'N/A' then
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "width: 250px; vertical-align:top; text-align:left">
                    Non-Acceptable Response Text:</td><td>' ||
                    api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText') || '</td></tr>');
              end if;
            end if;
          end if;
          t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
        end loop;
        -- Close the table
        t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
      end if;

      -- If there is a rel to documents create table for the documents
      if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
        -- Table header for documents
        t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Application Documents</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;">');
        -- Loop through Online Uploads
        for d2 in (
            select
              api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
              dr.FileName,
              lt.CreatedDate
            from api.Relationships r
              join api.RelationshipDefs rd
                  on rd.RelationshipDefId = r.RelationshipDefId
              join api.documentrevisions dr
                  on r.ToObjectId = dr.DocumentId
              join query.r_ElectronicDocDocType dt
                  on dr.DocumentId = dt.DocumentId
              join api.logicaltransactions lt
                  on dr.LogicalTransactionId = lt.LogicalTransactionId
            where r.FromObjectId = t_JobId
              and rd.ToEndPointName = 'OLDocument'
            ) loop
          t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td style="width: 250px;
              ">Document Name:</td><td>' || d2.FileName || '</td>' || '</tr><tr><td style="width:
              250px; ">Document Type:</td><td>' || d2.DocType || '</td>' || '</tr><tr><td style=
              "width: 250px; ">Upload Date:</td><td>' || d2.CreatedDate || '</td></tr>');
        end loop;
        -- Close the table
        t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div><br>';
      end if;

      -- Retrieve active Municipal Users for Municipality and Applicant / Legal Entity.
      -- Emails sorted alphabetically
      if t_NotifyMunicipality = 'Y' then
        for i in (
            select EmailAddress
            from (
                select
                  distinct EmailAddress
                from query.o_ABC_Office m
                  join query.r_MunicipalUserMunicipality mu
                      on m.objectid = mu.MunicipalityObjectId
                  join query.u_Users u
                      on u.ObjectId = mu.MunicipalUserObjectId
                where m.ObjectId = t_MunicipalityObjectId
                  and u.Active = 'Y'
                union
                select
                  distinct u.EmailAddress
                from api.Relationships r
                  join api.RelationshipDefs rd
                      on rd.RelationshipDefId = r.RelationshipDefId
                  join query.r_ABC_LicenseLicenseeLE lic
                      on lic.LicenseObjectId = r.ToObjectId
                  join query.r_ABC_UserLegalEntity le
                      on le.LegalEntityObjectId = lic.LegalEntityObjectId
                  join query.u_Users u
                      on u.ObjectId = le.UserId
                where r.FromObjectId = t_JobId
                  and rd.ToEndPointName = 'License'
                union
                select t_RegistrantEmail
                from dual
                )
            order by lower(EmailAddress)
            ) loop
          t_EmailsCLOB := t_EmailsCLOB || to_clob('- ' || i.EmailAddress ||
              '<br>');
        end loop;
      -- Retrieve only Applicant / Legal Entity. Email sorted alphabetically
      elsif t_NotifyMunicipality = 'N' and t_NotifyPolice = 'N' then
        for i in (
            select EmailAddress
            from (
                select
                  distinct u.EmailAddress
                from api.Relationships r
                  join api.RelationshipDefs rd
                      on rd.RelationshipDefId = r.RelationshipDefId
                  join query.r_ABC_LicenseLicenseeLE lic
                      on lic.LicenseObjectId = r.ToObjectId
                  join query.r_ABC_UserLegalEntity le
                      on le.LegalEntityObjectId = lic.LegalEntityObjectId
                  join query.u_Users u
                      on u.ObjectId = le.UserId
                where r.FromObjectId = t_JobId
                  and rd.ToEndPointName = 'License'
                union
                select t_RegistrantEmail
                from dual
                )
            order by lower(EmailAddress)
            ) loop
          t_EmailsCLOB := t_EmailsCLOB || to_clob('- ' || i.EmailAddress ||
              '<br>');
        end loop;
      end if;

      -- Get the details needed from the system settings
      select
        ss.FromEmailAddress,
        ss.IsProduction,
        ss.ToEmailAddressNonProd,
        ss.MunicipalWebsiteBaseUrl
      into
        t_From,
        t_IsProduction,
        t_To,
        t_BaseURL
      from query.o_systemsettings ss
      where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
          'SystemSettingsNumber', 1);

      -- Conditionally get Resolution/Standard details from system settings.
      -- If NotifyMunicipality populate the email w/ Resolution details
      if t_NotifyMunicipality = 'Y' then
        select
          ss.MuniLicResEmailSubject,
          ss.MuniLicResEmailHeaderPhotoURL,
          ss.MuniLicResEmailBodyHeader,
          ss.MuniLicResEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      -- If neither NotifyMunicipality nor NotifyPolice populate the email w/ Standard details
      elsif t_NotifyMunicipality = 'N' and t_NotifyPolice = 'N' then
        select
          ss.MuniLicStdEmailSubject,
          ss.MuniLicStdEmailHeaderPhotoURL,
          ss.MuniLicStdEmailBodyHeader,
          ss.MuniLicStdEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      end if;

      -- Create the body of the email in a clob. Breaking this into several clobs to concat
      -- together in hopes of making this easier to read.
      t_Body1 := '<table cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="100%"><tbody><tr><td style="padding: 20px;"><table
          border="0" cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="600" style="margin: 0px auto; border-radius: 10px;
          background-color: rgb(255, 255, 255);"><tbody><tr><td colspan="2">
          </td></tr><tr><td width="600"></td></tr><tr><td colspan="2" height="24">
          <img alt="New Jersey ABC" src="' || t_HeaderImageName || '" border="0"
          style="display: block; margin: 0px; padding-bottom: 3px;"></td></tr>
          <tr><td colspan="2"><table border="0" cellpadding="0" cellspacing="0"
          width="600"><tbody><tr><td align="left"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><br><br>';
      t_HeaderTextCLOB := to_clob(t_HeaderText);

      t_Body2 := to_clob('<br><br>This notification is being sent to the
          following email addresses:<br>');

      t_Body3 := to_clob('</div><div style="font-family: Calibri, Arial,
          Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
          line-height: 1.4em;"><br><table style="width:100%; font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
          rgb(0, 0, 0); line-height: 1.4em;"><tr><td>License Type:</td><td>');

      t_LicenseTypeCLOB := to_clob(t_LicenseType || '</td>');

      t_Body4 := '</tr><tr><td style="width: 250px;">Type of Job:</td><td>';
      t_JobTypeCLOB := to_clob(t_JobType || '</td>');

      t_Body5 := '</tr><tr><td style="width: 250px;">File Number:</td><td>';
      t_JobNumberCLOB := to_clob(t_JobNumber || '</td>');

      t_Body26 := '</tr><tr><td style="width: 250px;">License Number:</td><td>';
      t_LicenseNumber := coalesce(t_LicenseNumber, 'N/A');
      t_LicenseNumberCLOB:= to_clob(t_LicenseNumber || '</td>');

      t_Body6 := '</tr><tr><td style="width: 250px;">Submitted Date:</td><td>';
      t_SubmittedDateCLOB := to_clob(to_char(
          api.pkg_ColumnQuery.DateValue(a_ObjectId, 'DateCompleted'), 'Mon DD, YYYY') || '</td>');

      t_Body7 := '</tr><tr><td style="width: 250px;">Applicant Email:</td><td>';
      t_RegistrantEmailCLOB := to_clob(t_RegistrantEmail || '</td>');

      t_Body8 := '</tr><tr><td style="width: 250px;">Receipt Number:</td><td>';
      t_ReceiptNumberCLOB := to_clob(t_ReceiptNumber || '</td>');

      t_Body9 := '</tr><tr><td style="width: 250px;">Fee Amount:</td><td>';
      t_FeeAmountCLOB := to_clob(t_FeeAmount || '</td>');

      t_SubHeader1 := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Licensee Information</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><tr><td>Licensee Applicant:</td><td>');
      t_LicenseeNameCLOB := to_clob(t_LicenseeName || '</td>');

      t_Body10 := '</tr><tr><td style="width: 250px;">Corporation Number:</td><td>';
      t_CorporationNumberCLOB := to_clob(t_CorporationNumber || '</td>');

      t_Body11 := '</tr><tr><td style="width: 250px;">Incorporation Date:</td><td>';
      t_IncorporationDateCLOB := to_clob(to_char(t_IncorporationDate, 'Mon DD, YYYY') || '</td>');

      t_Body12 := '</tr><tr><td style="width: 250px;">NJ Tax Auth Number:</td><td>';
      t_NJTaxAuthNumberCLOB := to_clob(t_NJTaxAuthNumber || '</td>');

      t_Body13 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Mailing Address:</td><td>');
      t_MailingAddressCLOB := to_clob(t_MailingAddress || '</td>');

      t_Body14 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Physical Address:</td><td>');
      t_PhysicalAddressCLOB := to_clob(t_PhysicalAddress || '</td>');

      t_SubHeader2 := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Contact Information</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><tr><td>Contact Name:</td><td>');
      t_ContactNameCLOB := to_clob(t_ContactName || '</td>');

      t_Body15 := '</tr><tr><td style="width: 250px;">Business Number:</td><td>';
      t_ContactPhoneNumberCLOB := to_clob(t_ContactPhoneNumber || '</td>');

      t_Body16 := '</tr><tr><td style="width: 250px;">Home Number:</td><td>';
      t_ContactAltPhoneNumberCLOB := to_clob(t_ContactAltPhoneNumber || '</td>');

      t_Body17 := '</tr><tr><td style="width: 250px;">Mobile Number:</td><td>';
      t_ContactFaxNumberCLOB := to_clob(t_ContactFaxNumber || '</td>');

      t_Body18 := '</tr><tr><td style="width: 250px;">Contact Email:</td><td>';
      t_ContactEmailAddressCLOB := to_clob(t_ContactEmailAddress || '</td>');

      t_Body19 := '</tr><tr><td style="width: 250px;">Preferred Contact Method:</td><td>';
      t_PreferredContactMethodCLOB := to_clob(t_PreferredContactMethod || '</td>');

      t_SubHeader3 := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Establishment Information</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><tr><td>Type:</td><td>');
      t_EstablishmentTypeCLOB := to_clob(t_EstablishmentType || '</td>');

      t_Body20 := '</tr><tr><td style="width: 250px;">DBA / Names:</td><td>';
      t_DoingBusinessAsCLOB := to_clob(t_DoingBusinessAs || '</td>');

      t_Body21 := '</tr><tr><td style="width: 250px;">Operator:</td><td>';
      t_OperatorCLOB := to_clob(t_Operator || '</td>');

      t_Body22 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Mailing Address:</td><td>');
      t_MailingAddressESTCLOB := to_clob(t_MailingAddressEST || '</td>');

      t_Body23 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Premises Address:</td><td>');
      t_PhysicalAddressESTCLOB := to_clob(t_PhysicalAddressEST || '</td>');

      t_SubHeader4 := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>License Type Details</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><tr><td>License Type:</td><td>');

      if t_RestrictedBreweryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Restricted Brewery Production:</td><td>' || t_RestrictedBreweryProduction || '</td>');
      elsif t_LimitedBreweryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Limited Brewery Production:</td><td>' || t_LimitedBreweryProduction || '</td>');
      elsif t_SupplementaryLimitedDistillery is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Supplementary Limited Distillery Production:</td><td>' ||
            t_SupplementaryLimitedDistillery || '</td>');
      elsif t_PlenaryWineryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Plenary Winery Production:</td><td>' || t_PlenaryWineryProduction || '</td>');

        if t_PlenaryWineryWholesalePrivileg = 'Y' then
          t_PlenaryWineryWholesalePrivileg := 'Yes';
        else
          t_PlenaryWineryWholesalePrivileg := 'No';
        end if;

        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="
            vertical-align:top; text-align:left">Exercise the wholesale privilege of <br> selling
            to New Jersey retail licensees:</td><td style="vertical-align:top; text-align:left">' ||
            t_PlenaryWineryWholesalePrivileg || '</td>');
      elsif t_FarmWineryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Farm Winery Production:</td><td>' || t_FarmWineryProduction || '</td>');

        if t_FarmWineryWholesalePrivilege = 'Y' then
          t_FarmWineryWholesalePrivilege := 'Yes';
        else
          t_FarmWineryWholesalePrivilege := 'No';
        end if;

        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="
            vertical-align:top; text-align:left">Exercise the wholesale privilege of <br> selling
            to New Jersey retail licensees:</td><td style="vertical-align:top; text-align:left">' ||
            t_FarmWineryWholesalePrivilege || '</td>');
      elsif t_OutOfStateWineryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Out of State Winery Production:</td><td>' || t_OutOfStateWineryProduction || '</td>');

        if t_OutOfStateWineryWholesalePrivi = 'Y' then
          t_OutOfStateWineryWholesalePrivi := 'Yes';
        else
          t_OutOfStateWineryWholesalePrivi := 'No';
        end if;

        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="
            vertical-align:top; text-align:left">Exercise the wholesale privilege of <br> selling
            to New Jersey retail licensees:</td><td style="vertical-align:top; text-align:left">' ||
            t_OutOfStateWineryWholesalePrivi || '</td>');
      elsif t_RetailTransitType is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Retail Transit Type:</td><td>' || t_RetailTransitType || '</td>');
      elsif t_SportingFacilityCapacity is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Capacity:</td><td>' || t_SportingFacilityCapacity || '</td>');
      end if;

      if t_WarehouseName is not null then
        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="width: 250px;">
            Warehouse Name:</td><td>' || t_WarehouseName || '</td>');
        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="width: 250px;">
            Warehouse Phone Number:</td><td>' || REGEXP_REPLACE(t_WarehousePhoneNumber,
            '([0-9]{3})([0-9]{3})', '(\1) \2-') || '</td>');
        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="
          vertical-align:top; text-align:left">Warehouse Address:</td><td>' ||
          replace(t_OnlineWarehouseAddress,chr(13) || chr(10), '<br>') || '</td>');
      end if;

      t_Body25 := '</tr><tr><td style="width: 250px;">Secondary License Types:</td><td>';
      t_OnlineSecondaryLicenseTypesCLOB := to_clob(t_OnlineSecondaryLicenseTypes || '</td>');

      t_EndTableRowDiv := to_clob('</tr></table></div>');
      t_FooterTextCLOB := to_clob('</div><div style="font-family: Calibri, Arial, Helvetica,
          Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0); line-height: 1.4em;">
          <table style="width:100%; font-family: Calibri, Arial, Helvetica, Verdana, sans-serif;
          font-size: 14px; color: rgb(0, 0, 0); line-height: 1.4em;"><tr><td>' || t_FooterText ||
          '</td></tr></table></div>');
      t_BodyEnd := to_clob('</td></tr></tbody></table></td></tr></tbody>
          </table><br></td></tr></tbody></table>');

      t_Body := t_body1 || t_HeaderTextCLOB || t_Body2 || t_EmailsCLOB||
          t_Body3 || t_LicenseTypeCLOB || t_Body4 || t_JobTypeCLOB ||
          t_Body5 || t_JobNumberCLOB || t_Body26 || t_LicenseNumberCLOB || t_Body6 ||
          t_SubmittedDateCLOB || t_Body7 || t_RegistrantEmailCLOB || t_Body8 ||
          t_ReceiptNumberCLOB || t_Body9 || t_FeeAmountCLOB || t_EndTableRowDiv || t_SubHeader1 ||
          t_LicenseeNameCLOB || t_Body10 || t_CorporationNumberCLOB || t_Body11 ||
          t_IncorporationDateCLOB || t_Body12 || t_NJTaxAuthNumberCLOB || t_Body13 ||
          t_MailingAddressCLOB || t_Body14 || t_PhysicalAddressCLOB || t_EndTableRowDiv ||
          t_SubHeader2 || t_ContactNameCLOB || t_Body15 || t_ContactPhoneNumberCLOB ||
          t_Body16 || t_ContactAltPhoneNumberCLOB || t_Body17 || t_ContactFaxNumberCLOB ||
          t_Body18 || t_ContactEmailAddressCLOB || t_Body19 || t_PreferredContactMethodCLOB ||
          t_EndTableRowDiv || t_SubHeader3 || t_EstablishmentTypeCLOB || t_Body20 ||
          t_DoingBusinessAsCLOB || t_Body21 || t_OperatorCLOB || t_Body22 ||
          t_MailingAddressESTCLOB || t_Body23 || t_PhysicalAddressESTCLOB || t_EndTableRowDiv ||
          t_SubHeader4 || t_LicenseTypeCLOB || t_LicenseDetailsCLOB || t_Body25 ||
          t_OnlineSecondaryLicenseTypesCLOB || t_EndTableRowDiv || t_QuestionsCLOB ||
          t_DocumentListCLOB || t_FooterTextClob || t_BodyEnd;

      -- Get the users that the email should be sent to
      if t_IsProduction = 'Y' then
        -- If the system is a production system send it to all users registered with the Licensee
        for i in (
            select
              distinct u.EmailAddress
            from api.Relationships r
              join api.RelationshipDefs rd
                  on rd.RelationshipDefId = r.RelationshipDefId
              join query.r_ABC_LicenseLicenseeLE lic
                  on lic.LicenseObjectId = r.ToObjectId
              join query.r_ABC_UserLegalEntity le
                  on le.LegalEntityObjectId = lic.LegalEntityObjectId
              join query.u_Users u
                  on u.ObjectId = le.UserId
            where r.FromObjectId = t_JobId
              and rd.ToEndPointName = 'License'
            union
            select t_RegistrantEmail
            from dual
            ) loop
          extension.pkg_sendmail.SendEmail(t_JobId, t_From, i.EmailAddress, null,
              null, t_Subject, t_Body, null, 'OLDocument', 'Y');
        end loop;

        -- If the system is a production system and the license type is set to Notify Municipality
        -- send it to the clerks related to the municipality.
        if t_NotifyMunicipality = 'Y' then
          for i in (
              select
                distinct EmailAddress
              from query.o_ABC_Office m
                join query.r_MunicipalUserMunicipality mu
                    on m.objectid = mu.MunicipalityObjectId
                join query.u_Users u
                    on u.ObjectId = mu.MunicipalUserObjectId
              where m.ObjectId = t_MunicipalityObjectId
                and u.Active = 'Y'
              ) loop
            extension.pkg_sendmail.SendEmail(t_JobId, t_From, i.EmailAddress, null,
                null, t_Subject, t_Body, null, 'OLDocument', 'Y');
          end loop;
        end if;
      else
        -- If the system is a test system send it to the test email on the config site
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_To, null, null,
            t_Subject, t_Body, null, 'OLDocument', 'Y');
      end if;
    end if;

  end MuniEmailForLicense;

  /*---------------------------------------------------------------------------
   * PoliceEmailForPermit() -- PUBLIC
   *   Send notification Email to Police Users / Applicant on Permit
   * Application/Renewal
   *-------------------------------------------------------------------------*/
  procedure PoliceEmailForPermit (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_MunicipalityObjectId              udt_id;
    t_JobId                             integer;
    t_PermitObjectId                    integer;
    t_PermitteeObjectId                 integer;
    t_NotifyPolice                      varchar2(1);
    t_PoliceRequired                    varchar2(1);
    t_From                              varchar2(4000);
    t_Subject                           varchar2(4000);
    t_IsProduction                      varchar2(1);
    t_To                                varchar2(4000);
    t_HeaderImageName                   varchar2(4000);
    t_HeaderText                        varchar2(4000);
    t_FooterText                        varchar2(4000);
    t_ConfigText                        varchar2(4000);
    t_baseURL                           varchar2(4000);
    t_JobDefName                        varchar2(200);
    t_PermitType                        varchar2(200);
    t_PermitTypeCode                    varchar2(20);
    t_JobNumber                         varchar2(200);
    t_PermitteeName                     varchar2(200);
    t_LicenseNumber                     varchar2(50);
    t_MailingAddress                    varchar2(4000);
    t_PhysicalAddress                   varchar2(4000);
    t_ContactName                       varchar2(250);
    t_ContactPhoneNumber                varchar2(50);
    t_County                            varchar2(200);
    t_Municipality                      varchar2(200);
    t_AdditionalPermitInfo              varchar2(4000);
    t_SAQuestion                        varchar2(500);
    t_EventLocationAddress              varchar2(4000);
    t_EventLocAddressDescription        varchar2(4000);
    t_EventDetails                      varchar2(4000);
    t_AnswerOnJob                       varchar2(2000);
    t_AcceptableAnswer                  varchar2(2000);
    t_RegistrantEmail                   varchar2(4000);
    t_OnlineUserId                      integer;
    t_EventDatesCLOB                    clob;
    t_RainDatesCLOB                     clob;
    t_QuestionsCLOB                     clob;
    t_DocumentListCLOB                  clob;
    t_Body                              clob;
    t_Body1                             clob;
    t_HeaderTextCLOB                    clob;
    t_FooterTextCLOB                    clob;
    t_Body2                             clob;
    t_PermitTypeCLOB                    clob;
    t_Body3                             clob;
    t_JobNumberCLOB                     clob;
    t_Body4                             clob;
    t_PermitteeNameCLOB                 clob;
    t_Body12                            clob;
    t_LicenseNumberCLOB                 clob;
    t_Body5                             clob;
    t_MailingAddressCLOB                clob;
    t_Body6                             clob;
    t_PhysicalAddressCLOB               clob;
    t_Body7                             clob;
    t_ContactNameCLOB                   clob;
    t_Body8                             clob;
    t_ContactPhoneNumberCLOB            clob;
    t_Body8a                            clob;
    t_RegistrantEmailCLOB               clob;
    t_Body9                             clob;
    t_AdditionalPermitInfoCLOB          clob;
    t_Body10                            clob;
    t_BodyEND                           clob;
    t_EventLocationCLOB                 clob;
    t_EventDetailsCLOB                  clob;
    t_Body11                            clob;
    t_PoliceEmailsCLOB                  clob;
  begin

    -- Collect all data needed for the email.  The email will be generated after all data is
    -- collected. This will be run on completion of a process which means we will need to get
    -- the JobId
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    -- Need the Job def name to determine what rels to use to get the required information
    t_JobDefName := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefName');
    t_PermitType := api.pkg_ColumnQuery.Value(t_JobId, 'PermitType');
    -- Check to see if the permit type requires the muni or police to be notified.
    t_NotifyPolice := api.pkg_ColumnQuery.Value(
        api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeObjectId'), 'NotifyPolice');
    -- Check to see if the permit type requires muni or police review
    t_PoliceRequired := api.pkg_ColumnQuery.Value(
        api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeObjectId'), 'PoliceRequired');

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      -- Get the information needed that can be found through api.pkg_ColumnQuery
      t_OnlineUserId := api.pkg_ColumnQuery.NumericValue(t_JobId, 'OnlineUserObjectId');
      t_RegistrantEmail := api.pkg_columnquery.value(t_OnlineUserId, 'EmailAddress');
      t_JobNumber := api.pkg_ColumnQuery.Value(t_JobId, 'ExternalFileNum');
      t_PermitObjectId := api.pkg_ColumnQuery.Value(t_JobId, 'PermitObjectId');
      t_PermitTypeCode := api.pkg_ColumnQuery.Value(t_JobId, 'PermitTypeCode');
      t_LicenseNumber := coalesce(api.pkg_ColumnQuery.Value(t_PermitObjectid, 'LicenseNumber'),
            'N/A');
      t_PermitteeObjectId := api.pkg_ColumnQuery.Value(t_PermitObjectId, 'PermitteeObjectId');
      t_PermitteeName := nvl(api.pkg_ColumnQuery.Value(t_PermitObjectId, 'PermitteeName'),
          api.Pkg_Columnquery.Value(t_PermitObjectId, 'OnlineLEFormattedName'));
      t_MailingAddress := replace(nvl(
          api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'MailingAddress'),
          api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineLEMailingAddress')),
          chr(13) || chr(10), '<br>');
      if api.pkg_columnquery.Value(t_JobId,'OnlineLESameAsMailing') = 'Y' then
         t_PhysicalAddress := t_MailingAddress;
      else
        t_PhysicalAddress := replace(nvl(
            api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'PhysicalAddress'),
            api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePhysicalAddress')),
            chr(13)||chr(10), '<br>');
      end if;
      t_ContactName := coalesce(api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'ContactName'),
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactName'), t_PermitteeName);
      t_ContactPhoneNumber := REGEXP_REPLACE(nvl(
          api.pkg_ColumnQuery.Value(t_PermitteeObjectId, 'ContactPhoneNumber'),
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactPhone')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');
      t_County := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'County');
      t_Municipality := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'Municipality');
      t_MunicipalityObjectId := api.pkg_ColumnQuery.Value(t_PermitObjectid, 'MunicipalityObjectId');
      t_AdditionalPermitInfo := api.pkg_ColumnQuery.Value(t_PermitObjectId,
          'AdditionalPermitInformation');
      t_EventLocationAddress := replace(nvl(
          nvl(api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineEventLocationAddress'),
          api.pkg_ColumnQuery.Value(t_PermitObjectId, 'OnlineEventLocationAddress')),
          api.Pkg_Columnquery.Value(t_PermitObjectId, 'EventLocationAddress')),
          chr(13)||chr(10), '<br>');
      t_EventLocAddressDescription := api.pkg_ColumnQuery.Value(t_PermitObjectId,
          'EventLocAddressDescription');
      t_EventDetails := api.pkg_ColumnQuery.Value(t_PermitObjectId, 'EventDetails');

      -- If permit is a Social Affair permit get the answer to the SA Question.
      if lower(t_PermitTypeCode) = 'sa' then
        t_SAQuestion := 'Was the Non-Profit Group/Organization formed as a ' ||
            'Religious, Civic or Educational Entity?<br>' ||
            nvl(api.pkg_ColumnQuery.Value(t_JobId,'OnlineCivicReligOrEduc'),
            api.pkg_ColumnQuery.Value(t_JobId, 'CivicReligOrEduc'));
      end if;

      -- Get the event dates and convert to clob with HTML formatting for the body of the email.
      for e in (
          select ed.EventDateObjectId
          from query.r_Abc_Permiteventdate ed
          where ed.PermitObjectId = t_PermitObjectId
          ) loop
        t_EventDatesCLOB := t_EventDatesCLOB || to_clob(to_char(
            api.pkg_ColumnQuery.DateValue(e.eventdateobjectid, 'StartDate'), 'Mon DD, YYYY'));
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'StartTimeHours') is not null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || ' from ' ||
              api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'FormattedStartTime'));
        end if;
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'EndTimeHours') is not null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || ' to ' ||
              api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'FormattedEndTime') || '<br>');
        end if;
        if api.pkg_ColumnQuery.Value(e.eventdateobjectid, 'EndTimeHours') is null then
          t_EventDatesCLOB := to_clob(t_EventDatesCLOB || '<br>');
        end if;
      end loop;
      -- Add Event Dates header to t_EventDatesCLOB if there were any event dates.
      if t_EventDatesCLOB is not null then
        t_EventDatesCLOB := '<br><div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"> <u>Event Dates</u><br>' || t_EventDatesCLOB ||
            '</div>';
      end if;

      -- Get the Rain Dates and convert to clob with HTML formatting for the body of the email.
      for r in (
          select rd.RainDateObjectId
          from query.r_ABC_PermitRainDate rd
          where rd.PermitObjectId = t_PermitObjectId
          ) loop
        t_RainDatesCLOB := t_RainDatesCLOB || to_clob(to_char(
            api.pkg_ColumnQuery.DateValue(r.RainDateObjectId, 'StartDate'), 'Mon DD, YYYY'));
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'StartTimeHours') is not null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || ' from ' ||
              api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'FormattedStartTime'));
        end if;
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'EndTimeHours') is not null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || ' to ' ||
              api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'FormattedEndTime') || '<br>');
        end if;
        if api.pkg_ColumnQuery.Value(r.RainDateObjectId, 'EndTimeHours') is null then
          t_RainDatesCLOB := to_clob(t_RainDatesCLOB || '<br>');
        end if;
      end loop;
      -- Add Rain Dates header to t_RainDatesCLOB if there were any rain dates.
      if t_RainDatesCLOB is not null then
        t_RainDatesCLOB := '<br><u>Rain Dates</u><br>' || t_RainDatesCLOB;
      end if;

      -- Get the information from the Permit Application.
      if t_JobDefName = 'j_ABC_PermitApplication' then
        -- If there is a rel to questions start creation of the HTML table
        if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
          -- Table header
          t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br><u>Application Questions</u><br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;">');
           -- Loop through all QAResponses
          for q in (
               select qa.ResponseId
               from query.r_ABC_PermitAppQAResponse qa
               where qa.PermitApplicationId = t_JobId
               order by api.pkg_columnquery.NumericValue(qa.ResponseId, 'SortOrder')
               ) loop
            t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Question ' ||
                api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') || ':</td>' ||
                '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'Question') || '</td></tr>');
            if nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A' then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                  "vertical-align:top; text-align:left">Yes / No Response:</td>' ||
                  '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
            end if;
            t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.responseid, 'AnswerOnJob');
            t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.responseid, 'AcceptableAnswer');
            if nvl(t_AnswerOnJob, 'N') != nvl(t_AcceptableAnswer, 'Y')
                and t_AcceptableAnswer is not null then
              if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                  then
                if nvl(t_AcceptableAnswer, 'N') != 'N/A' then
                  t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                      "width: 250px; vertical-align:top; text-align:left"></td>'
                      || '<td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText')
                      || '</td></tr>');
                end if;
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "vertical-align:top; text-align:left">Response:</td><td>'
                    || nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                    to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId,
                    'ResponseDate'), 'Mon DD, YYYY')) || '</td></tr>');
               end if;
            end if;
            t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
          end loop;
          -- Close the table
          t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
        end if;

        -- If there is a rel to documents create table for the documents
        if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
          -- Table header for documents
          t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
              Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Documents</u><br>
              See below for a list of documents attached to this email.<br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><tr><th align="left">Document Type</th>
              <th align="left">File Name</th><th align="left">Upload Date</th>
              </tr>');

          -- Loop through Online Uploads
          for d2 in (
              select
                api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
                dr.FileName,
                lt.CreatedDate
              from query.r_OLNewPermitAppElectronicDoc AppDoc
              join api.documentrevisions dr
                  on AppDoc.OLDocumentId = dr.DocumentId
              join query.r_ElectronicDocDocType dt
                  on dr.DocumentId = dt.DocumentId
              join api.logicaltransactions lt
                  on dr.LogicalTransactionId = lt.LogicalTransactionId
              where AppDoc.OLNewPermitAppJobId = t_JobId
              ) loop
            t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td>' || d2.doctype ||
                '</td><td>' || d2.filename || '</td><td>' || to_char(d2.Createddate, 'Mon DD, YYYY')
                || '</td></tr>');
          end loop;
          -- Close the table
          t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div>';
        end if;
      end if;

      -- Get the information from the Permit Renewal.
      if t_JobDefName = 'j_ABC_PermitRenewal' then
        -- If there is a rel to questions start creation of the HTML table
        if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
          -- Table header
          t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Application Questions
              </u><br><table style="width:100%; font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;">');
          -- Loop through all QAResponses
          for q in (
              select qa.ResponseId
              from query.r_ABC_PermitRnwQAResponse qa
              where qa.PermitRenewalId = t_JobId
              order by api.pkg_columnquery.NumericValue(qa.ResponseId, 'SortOrder')
              ) loop
            t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Question ' ||
                api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') ||
                ':</td><td>' || api.pkg_ColumnQuery.Value(q.ResponseId, 'Question')
                || '</td></tr>');
            if nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A' then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                  "vertical-align:top; text-align:left">Yes / No Response:</td><td>'
                  || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
            end if;
            t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.responseid, 'AnswerOnJob');
            t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.responseid, 'AcceptableAnswer');
            if nvl(t_AnswerOnJob, 'N') != nvl(t_AcceptableAnswer, 'Y')
                and t_AcceptableAnswer is not null then
              if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                  then
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "vertical-align:top; text-align:left">Response:</td><td>'
                    || nvl(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                    to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId, 'ResponseDate'),
                    'Mon DD, YYYY')) || '</td></tr>');
                if nvl(t_AcceptableAnswer, 'N') != 'N/A' then
                  t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td
                      style="width: 250px; vertical-align:top; text-align:left">
                      Non-Acceptable Response Text:</td><td>' ||
                      api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText') || '</td></tr>');
                end if;
              end if;
            end if;
            t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
          end loop;
          -- Close the table
          t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
        end if;

        -- If there is a rel to documents creat table for the documents
        if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
          -- Table header for documents
          t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
              Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
              rgb(0, 0, 0); line-height: 1.4em;"><br><u>Documents</u><br>
              <table style="width:100%; font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><tr><th align="left">Document Type</th>
              <th align="left">File Name</th><th align="left">Upload Date</th>
              </tr>');
          for d2 in (
              select
                api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
                dr.FileName,
                lt.CreatedDate
              from
                query.r_OLPermitRenewElectronicDoc AppDoc
                join api.documentrevisions dr
                    on AppDoc.OLDocumentId = dr.DocumentId
                join query.r_ElectronicDocDocType dt
                    on dr.DocumentId = dt.DocumentId
                join api.logicaltransactions lt
                    on dr.LogicalTransactionId = lt.LogicalTransactionId
              where AppDoc.OLPermitRenewJobId = t_JobId
              ) loop
            t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td>' || d2.doctype ||
                '</td><td>' || d2.filename || '</td><td>' || to_char(d2.Createddate, 'Mon DD, YYYY')
                || '</td></tr>');
          end loop;
          --Close the table
          t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div>';
        end if;
      end if;

      -- Retrieve active Municipal and Police Users for Municipality. Emails are sorted
      -- alphabetically
      for i in (
          select EmailAddress
          from (
              select u.EmailAddress
              from query.u_users u
              join query.r_policeusermunicipality pu
                  on u.ObjectId = pu.PoliceUserObjectId
              where pu.MunicipalityObjectId = t_MunicipalityObjectId
                and u.Active = 'Y'
                and t_NotifyPolice = 'Y'
              union
              select
                t_RegistrantEmail
              from dual
              )
          order by lower(EmailAddress)
          ) loop
            t_PoliceEmailsCLOB := t_PoliceEmailsCLOB || to_clob('- ' || i.EmailAddress || '<br>');
      end loop;

      -- Get the details needed from the system settings
      select
        ss.FromEmailAddress,
        ss.IsProduction,
        ss.ToEmailAddressNonProd,
        ss.PoliceWebsiteBaseUrl
      into
        t_From,
        t_IsProduction,
        t_To,
        t_BaseURL
      from query.o_systemsettings ss
      where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
          'SystemSettingsNumber', 1);

      -- Conditionally get Endorsement/Standard details from system settings. When review is
      -- required, populate the email w/ Endorsement details
      if t_PoliceRequired = 'Y' then
        select
          ss.PoliceEndEmailSubject,
          ss.PoliceEndEmailHeaderPhotoURL,
          ss.PoliceEndEmailBodyHeader,
          ss.PoliceEndEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      -- Otherwise, send the standard email.
      else
        select
          ss.PoliceStdEmailSubject,
          ss.PoliceStdEmailHeaderPhotoURL,
          ss.PoliceStdEmailBodyHeader,
          ss.PoliceStdEmailBodyFooter
        into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      end if;

      -- Create the body of the email in a clob. Breaking this into several clobs to concat
      -- together in hopes of making this easier to read.
      t_Body1 := '<table cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="100%"><tbody><tr><td style="padding: 20px;"><table
          border="0" cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="600"style="margin: 0px auto; border-radius: 10px;
          background-color: rgb(255, 255, 255);"><tbody><tr><td colspan="2">
          </td></tr><tr><td width="600"></td></tr><tr><td colspan="2" height="24">
          <img alt="New Jersey ABC" src="' || t_HeaderImageName || '" border="0"
          style="display: block; margin: 0px; padding-bottom: 3px;"></td></tr>
          <tr><td colspan="2"><table border="0" cellpadding="0" cellspacing="0"
          width="600"><tbody><tr><td align="left"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><br><br>';
      t_HeaderTextCLOB := to_clob(t_HeaderText);

      t_Body11 := to_clob('<br>This notification is being sent to the
          following email addresses:<br>');

      t_Body2 := to_clob('</div><div style="font-family: Calibri, Arial,
          Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
          line-height: 1.4em;"><br><table style="width:100%; font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
          rgb(0, 0, 0); line-height: 1.4em;"><tr><td>Permit Type:</td><td>');
      t_PermitTypeCLOB := to_clob(t_PermitType || '</td>');

      t_Body3 := '</tr><tr><td style="width: 250px;">File Number:</td><td>';
      t_JobNumberCLOB := to_clob(t_JobNumber || '</td>');

      t_Body4 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Permittee:</td>
          <td>');
      t_PermitteeNameCLOB := to_clob(t_PermitteeName || '</td>');

      t_Body12 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>License Number:</td>
          <td>');
      t_LicenseNumberCLOB := to_clob(t_LicenseNumber || '</td>');

      t_Body5 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Mailing Address:</td><td>');
      t_MailingAddressCLOB := to_clob(t_MailingAddress || '</td>');

      t_Body6 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Physical Address:</td><td>');
      t_PhysicalAddressCLOB := to_clob(t_PhysicalAddress || '</td>');

      t_Body7 := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Contact:</td><td>');
      t_ContactNameCLOB := to_clob(t_ContactName || '</td>');

      t_Body8 := to_clob('</tr><tr><td></td><td>');
      t_ContactPhoneNumberCLOB := to_clob(t_ContactPhoneNumber || '</td><br>');

      t_Body8a := to_clob('</tr><tr><td></td><td></td></tr><tr><td>Applicant Email:</td><td>');
      t_RegistrantEmailCLOB := to_clob(t_RegistrantEmail || '</td>');

      t_Body9 := to_clob('</tr>');
      t_FooterTextCLOB := to_clob(t_FooterText);

      -- If any type of additional information is needed create the header for the Additional info
      -- section.
      if t_County is not null or t_Municipality is not null or t_AdditionalPermitInfo is not null
          or lower(t_PermitTypeCode) = 'sa' then
        t_AdditionalPermitInfoCLOB := to_clob('<tr><td><u>Additional Permit Information</u></td></tr>');
        if t_County is not null then
          t_AdditionalPermitInfoCLOB :=  t_AdditionalPermitInfoCLOB ||
              to_clob('<tr><td>County:</td><td>' || t_County || '</td></tr>');
        end if;
        if t_Municipality is not null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_Clob('<tr><td>Municipality:</td><td>' || t_Municipality ||
              '</td></tr>');
        end if;
        if t_AdditionalPermitInfo is not null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('</table></div><div style="font-family: Calibri, Arial,
              Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br>' || t_AdditionalPermitInfo || '</div>');
        end if;
        if t_AdditionalPermitInfo is null then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('</table></div>');
        end if;
        if lower(t_PermitTypeCode) = 'sa' then
          t_AdditionalPermitInfoCLOB := t_AdditionalPermitInfoCLOB ||
              to_clob('<div style="font-family: Calibri, Arial, Helvetica,
              Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
              line-height: 1.4em;"><br>' || t_SAQuestion || '<br></div>');
        end if;
      end if;

      if t_AdditionalPermitInfoCLOB is null then
        t_Body10 := to_clob('</table></div>');
      end if;

      if t_EventLocationAddress is not null or t_EventLocAddressDescription is not null then
        t_EventLocationCLOB := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Location</u><br>');
        if t_EventLocAddressDescription is not null then
          t_EventLocationCLOB := t_EventLocationCLOB ||
              to_clob('Location Description:<br>' || t_EventLocAddressDescription || '<br>');
        end if;
        if t_EventLocationAddress is not null then
          t_EventLocationCLOB := t_EventLocationCLOB || to_clob('<br>Address:<br>'
              || t_EventLocationAddress || '<br>');
        end if;
        t_EventLocationCLOB := t_EventLocationCLOB || to_clob('</div>');
      end if;

      if t_EventDetails is not null then
        t_EventDetailsCLOB := to_clob('<div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><u>Event Details</u><br>What is the specific
            event being held?<br>' || t_EventDetails || '</div>');
      end if;

      t_BodyEND := to_clob('</td></tr></tbody></table></tbody></table><br></td>
          </tr></tbody></table>');
      t_Body := t_body1 || t_HeaderTextCLOB || t_Body11 || t_PoliceEmailsCLOB ||
          t_Body2 || t_PermitTypeCLOB || t_Body3 || t_JobNumberCLOB ||
          t_body4 || t_PermitteeNameCLOB || t_Body12 || t_LicenseNumberCLOB ||
          t_Body5 || t_MailingAddressCLOB || t_Body6 || t_PhysicalAddressCLOB ||
          t_Body7 || t_ContactNameCLOB || t_Body8 || t_ContactPhoneNumberCLOB ||
          t_Body8a || t_RegistrantEmailCLOB || t_Body9 || t_AdditionalPermitInfoCLOB ||
          t_Body10 || t_EventLocationCLOB || t_EventDetailsCLOB || t_EventDatesCLOB ||
          t_RainDatesCLOB || t_QuestionsCLOB || t_DocumentListCLOB || t_FooterTextClob ||
          t_BodyEnd;

      -- Get the users that the email should be sent to
      if t_IsProduction = 'Y' then
        -- If the system is a production system send it to the user who applied
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_RegistrantEmail, null,
            null, t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
        -- If the system is a production system send it to the Police users related to the
        -- municipality.
        if t_NotifyPolice = 'Y' then
          for i in (
              select
                u.UserId,
                u.EmailAddress,
                u.Active
              from
                query.u_users u
                join query.r_policeusermunicipality pu
                    on u.ObjectId = pu.PoliceUserObjectId
                where pu.MunicipalityObjectId = t_MunicipalityObjectId
                ) loop
            if i.Active = 'Y' then
              extension.pkg_sendmail.SendEmail(t_JobId, t_From, i.EmailAddress, null,
                  null, t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
            end if;
          end loop;
        end if;
      else
        -- If the system is a test system send it to the test email on the config site
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_To, null, null,
            t_Subject, t_Body, null, 'OLDocument', 'Y', 'Y');
      end if;
    end if;

  end PoliceEmailForPermit;

  /*---------------------------------------------------------------------------
   * PoliceEmailForLicense() -- PUBLIC
   *   Send notification Email to Police / Applicant on License
   * Application/Renewal/Amendment. Note that Documents will not be attached
   * to the email.
   *-------------------------------------------------------------------------*/
  procedure PoliceEmailForLicense (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_AcceptableAnswer                  varchar2(2000);
    t_AnswerOnJob                       varchar2(2000);
    t_baseURL                           varchar2(4000);
    t_Body                              clob;
    t_Body1                             clob;
    t_Body2                             clob;
    t_Body3                             clob;
    t_Body4                             clob;
    t_Body5                             clob;
    t_Body6                             clob;
    t_Body7                             clob;
    t_Body8                             clob;
    t_Body9                             clob;
    t_Body10                            clob;
    t_Body11                            clob;
    t_Body12                            clob;
    t_Body13                            clob;
    t_Body14                            clob;
    t_Body15                            clob;
    t_Body16                            clob;
    t_Body17                            clob;
    t_Body18                            clob;
    t_Body19                            clob;
    t_Body20                            clob;
    t_Body21                            clob;
    t_Body22                            clob;
    t_Body23                            clob;
    t_Body24                            clob;
    t_Body25                            clob;
    t_Body26                            clob;
    t_BodyEND                           clob;
    t_ContactAltPhoneNumber             varchar2(50);
    t_ContactAltPhoneNumberCLOB         clob;
    t_ContactEmailAddress               varchar2(100);
    t_ContactEmailAddressCLOB           clob;
    t_ContactFaxNumber                  varchar2(50);
    t_ContactFaxNumberCLOB              clob;
    t_ContactName                       varchar2(250);
    t_ContactNameCLOB                   clob;
    t_ContactNameLE                     varchar2(100);
    t_ContactPhoneNumber                varchar2(50);
    t_ContactPhoneNumberCLOB            clob;
    t_ContactPhoneNumberLE              varchar2(50);
    t_CorporationNumber                 varchar2(30);
    t_CorporationNumberCLOB             clob;
    t_County                            varchar2(200);
    t_DocumentListCLOB                  clob;
    t_DoingBusinessAs                   varchar2(400);
    t_DoingBusinessAsCLOB               clob;
    t_EmailsCLOB                        clob;
    t_EndTableRowDiv                    clob;
    t_EnteredOnline                     varchar2(1);
    t_EstablishmentObjectId             udt_Id;
    t_EstablishmentType                 varchar2(50);
    t_EstablishmentTypeCLOB             clob;
    t_FarmWineryProduction              varchar2(4000);
    t_FarmWineryWholesalePrivilege      varchar2(5);
    t_FeeAmount                         varchar2(20);
    t_FeeAmountCLOB                     clob;
    t_FooterText                        varchar2(4000);
    t_FooterTextCLOB                    clob;
    t_From                              varchar2(4000);
    t_HeaderImageName                   varchar2(4000);
    t_HeaderText                        varchar2(4000);
    t_HeaderTextCLOB                    clob;
    t_IncorporationDate                 date;
    t_IncorporationDateCLOB             clob;
    t_IsProduction                      varchar2(1);
    t_JobId                             udt_Id;
    t_JobNumber                         varchar2(200);
    t_JobNumberCLOB                     clob;
    t_JobType                           varchar2(60);
    t_JobTypeCLOB                       clob;
    t_LegalEntityName                   varchar2(400);
    t_LegalEntityObjectId               udt_Id;
    t_LicenseDetailsCLOB                clob;
    t_LicenseeName                      varchar2(200);
    t_LicenseeNameCLOB                  clob;
    t_LicenseNumber                     varchar2(50);
    t_LicenseNumberCLOB                 clob;
    t_LicenseObjectId                   udt_Id;
    t_LicenseType                       varchar2(200);
    t_LicenseTypeCLOB                   clob;
    t_LicenseTypeCode                   varchar2(20);
    t_LicenseTypeObjectId               udt_Id;
    t_LimitedBreweryProduction          varchar2(4000);
    t_MailingAddress                    varchar2(4000);
    t_MailingAddressCLOB                clob;
    t_MailingAddressEST                 varchar2(4000);
    t_MailingAddressESTCLOB             clob;
    t_MailingAddressLE                  varchar2(4000);
    t_MunicipalityObjectId              udt_Id;
    t_NJTaxAuthNumber                   varchar2(20);
    t_NJTaxAuthNumberCLOB               clob;
    t_NotifyMunicipality                varchar2(1);
    t_NotifyPolice                      varchar2(1);
    t_OnlineLECorporationNumber         varchar2(30);
    t_OnlineLEFormattedName             varchar2(400);
    t_OnlineLEIncorporationDate         date;
    t_OnlineLELegalName                 varchar2(100);
    t_OnlineLEMailingAddress            varchar2(4000);
    t_OnlineLEMailingAddressNewApp      varchar2(4000);
    t_OnlineLEPhysicalAddress           varchar2(4000);
    t_OnlineLEPreferredContactMethod    varchar2(4000);
    t_OnlineSecondaryLicenseTypes       varchar2(4000);
    t_OnlineSecondaryLicenseTypesCLOB   clob;
    t_OnlineUserId                      udt_Id;
    t_OnlineWarehouseAddress            varchar2(4000);
    t_Operator                          varchar2(400);
    t_OperatorCLOB                      clob;
    t_OutOfStateWineryProduction        varchar2(4000);
    t_OutOfStateWineryWholesalePrivi    varchar2(5);
    t_PhysicalAddress                   varchar2(4000);
    t_PhysicalAddressCLOB               clob;
    t_PhysicalAddressEST                varchar2(4000);
    t_PhysicalAddressESTCLOB            clob;
    t_PhysicalAddressLE                 varchar2(4000);
    t_PlenaryWineryProduction           varchar2(4000);
    t_PlenaryWineryWholesalePrivileg    varchar2(5);
    t_PreferredContactMethod            varchar2(4000);
    t_PreferredContactMethodCLOB        clob;
    t_QuestionsCLOB                     clob;
    t_ReceiptNumber                     varchar2(100);
    t_ReceiptNumberCLOB                 clob;
    t_RegistrantEmail                   varchar2(4000);
    t_RegistrantEmailCLOB               clob;
    t_RestrictedBreweryProduction       varchar2(4000);
    t_RetailTransitType                 varchar2(4000);
    t_SAQuestion                        varchar2(500);
    t_SportingFacilityCapacity          varchar2(4000);
    t_SubHeader1                        clob;
    t_SubHeader2                        clob;
    t_SubHeader3                        clob;
    t_SubHeader4                        clob;
    t_Subject                           varchar2(4000);
    t_SubmittedDateCLOB                 clob;
    t_SupplementaryLimitedDistillery    varchar2(4000);
    t_To                                varchar2(4000);
    t_WarehouseName                     varchar2(100);
    t_WarehousePhoneNumber              varchar2(50);
  begin

    -- Collect all data needed for the email.  The email will be generated after all data is
    -- collected. This will be run on completion of a process which means we will need to get
    -- the JobId
    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    -- Need the Job def name to determine what rels to use to get the required information
    t_JobType := api.pkg_ColumnQuery.Value(t_JobId, 'ObjectDefDescription');

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      -- Get the information needed that can be found through api.pkg_ColumnQuery
      t_OnlineUserId := api.pkg_ColumnQuery.NumericValue(t_JobId, 'OnlineUserObjectId');
      t_JobNumber := api.pkg_ColumnQuery.Value(t_JobId, 'ExternalFileNum');
      t_LicenseObjectId := api.pkg_ColumnQuery.NumericValue(t_JobId, 'LicenseObjectId');
      t_OnlineSecondaryLicenseTypes := api.pkg_ColumnQuery.Value(t_JobId,
          'OnlineSecondaryLicenseTypes');
      t_ReceiptNumber := api.pkg_ColumnQuery.Value(t_JobId, 'LatestReceiptNumber');
      t_FeeAmount := to_char(api.pkg_ColumnQuery.NumericValue(t_JobId, 'TotalFeeAmount'),
          '$999,990.00');
      t_RegistrantEmail := api.pkg_ColumnQuery.Value(t_OnlineUserId, 'EmailAddress');

      -- Details from License
      if t_LicenseObjectId is not null then
        select
          EstablishmentObjectId,
          LicenseTypeObjectId,
          LicenseNumber,
          OfficeObjectId,
          OnlineLEFormattedName,
          OnlineLEMailingAddress,
          RestrictedBreweryProduction,
          LimitedBreweryProduction,
          SupplementaryLimitedDistillery,
          PlenaryWineryProduction,
          PlenaryWineryWholesalePrivileg,
          FarmWineryProduction,
          FarmWineryWholesalePrivilege,
          OutOfStateWineryProduction,
          OutOfStateWineryWholesalePrivi,
          RetailTransitType,
          WarehouseName,
          WarehousePhoneNumber,
          OnlineWarehouseAddress,
          SportingFacilityCapacity
        into
          t_EstablishmentObjectId,
          t_LicenseTypeObjectId,
          t_LicenseNumber,
          t_MunicipalityObjectId,
          t_OnlineLEFormattedName,
          t_OnlineLEMailingAddress,
          t_RestrictedBreweryProduction,
          t_LimitedBreweryProduction,
          t_SupplementaryLimitedDistillery,
          t_PlenaryWineryProduction,
          t_PlenaryWineryWholesalePrivileg,
          t_FarmWineryProduction,
          t_FarmWineryWholesalePrivilege,
          t_OutOfStateWineryProduction,
          t_OutOfStateWineryWholesalePrivi,
          t_RetailTransitType,
          t_WarehouseName,
          t_WarehousePhoneNumber,
          t_OnlineWarehouseAddress,
          t_SportingFacilityCapacity
        from query.o_ABC_License
        where ObjectId = t_LicenseObjectId;
      end if;

      -- Details from License Type
      if t_LicenseTypeObjectId is not null then
        select
          Name,
          Code,
          NotifyMunicipality,
          NotifyPolice
        into
          t_LicenseType,
          t_LicenseTypeCode,
          t_NotifyMunicipality,
          t_NotifyPolice
        from query.o_ABC_LicenseType
        where ObjectId = t_LicenseTypeObjectId;
      end if;

      -- Select Legal Entity associated to Online User
      if t_OnlineUserId is not null then
        begin
          select
            r.ToObjectId
          into
            t_LegalEntityObjectId
          from api.Relationships r
            join api.RelationshipDefs rd
                on rd.RelationshipDefId = r.RelationshipDefId
          where r.FromObjectId = t_LicenseObjectId
          and rd.ToEndPointName = 'Licensee';
        exception when no_data_found then
          null;
        end;
      end if;

      -- Details from Legal Entity
      if t_LegalEntityObjectId is not null then
        select
          ContactName,
          ContactPhoneNumber,
          ContactAltPhoneNumber,
          ContactFaxNumber,
          ContactEmailAddress,
          CorporationNumber,
          dup_FormattedName,
          IncorporationDate,
          MailingAddress,
          NJTaxAuthNumber,
          PhysicalAddress,
          PreferredContactMethod
        into
          t_ContactNameLE,
          t_ContactPhoneNumberLE,
          t_ContactAltPhoneNumber,
          t_ContactFaxNumber,
          t_ContactEmailAddress,
          t_CorporationNumber,
          t_LegalEntityName,
          t_IncorporationDate,
          t_MailingAddressLE,
          t_NJTaxAuthNumber,
          t_PhysicalAddressLE,
          t_PreferredContactMethod
        from query.o_ABC_LegalEntity
        where ObjectId = t_LegalEntityObjectId;
      end if;

      -- Details from New Application if applicable
      if t_JobType = 'New Application' then
        select
          OnlineLECorporationNumber,
          OnlineLELegalName,
          OnlineLEIncorporationDate,
          OnlineLEMailingAddress,
          OnlineLEPhysicalAddress,
          OnlineLEPreferredContactMethod
        into
          t_OnlineLECorporationNumber,
          t_OnlineLELegalName,
          t_OnlineLEIncorporationDate,
          t_OnlineLEMailingAddressNewApp,
          t_OnlineLEPhysicalAddress,
          t_OnlineLEPreferredContactMethod
        from query.j_ABC_NewApplication
        where ObjectId = t_JobId;
      end if;

      -- Details from Establishment
      if t_EstablishmentObjectId is not null then
        select
          DoingBusinessAs,
          EstablishmentType,
          MailingAddress,
          Operator,
          PhysicalAddress
        into
          t_DoingBusinessAs,
          t_EstablishmentType,
          t_MailingAddressEST,
          t_Operator,
          t_PhysicalAddressEST
        from query.o_ABC_Establishment
        where ObjectId = t_EstablishmentObjectId;
      end if;

      t_LicenseeName := coalesce(t_LegalEntityName, t_OnlineLEFormattedName, t_OnlineLELegalName);
      t_MailingAddress := replace(coalesce(t_MailingAddressLE, t_OnlineLEMailingAddress,
        t_OnlineLEMailingAddressNewApp), chr(13) || chr(10), '<br>');

      if api.pkg_ColumnQuery.Value(t_JobId,'OnlineLESameAsMailing') = 'Y' then
         t_PhysicalAddress := t_MailingAddress;
      else
        t_PhysicalAddress := replace(coalesce(t_PhysicalAddressLE,
            api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePhysicalAddress'), t_OnlineLEPhysicalAddress),
            chr(13)||chr(10), '<br>');
      end if;

      t_CorporationNumber := coalesce(t_CorporationNumber, t_OnlineLECorporationNumber);
      t_IncorporationDate := coalesce(t_IncorporationDate, t_OnlineLEIncorporationDate);

      -- Contact Information
      t_ContactName := coalesce(t_ContactNameLE,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactName'), t_LicenseeName);

      t_ContactPhoneNumber := REGEXP_REPLACE(coalesce(t_ContactPhoneNumberLE,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEContactPhone')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');

      t_ContactAltPhoneNumber := REGEXP_REPLACE(coalesce(t_ContactAltPhoneNumber,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEAltPhone')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');

      t_ContactFaxNumber := REGEXP_REPLACE(coalesce(t_ContactFaxNumber,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEFax')),
          '([0-9]{3})([0-9]{3})', '(\1) \2-');

      t_ContactEmailAddress := coalesce(t_ContactEmailAddress,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineLEEmail'));

      t_PreferredContactMethod := coalesce(t_PreferredContactMethod,
          t_OnlineLEPreferredContactMethod);

      -- Establishment
      t_DoingBusinessAs := coalesce(t_DoingBusinessAs,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineESDoingBusinessAs'));
      t_EstablishmentType := coalesce(t_EstablishmentType,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineESEstablishmentType'));
      t_MailingAddressEST := coalesce(t_MailingAddressEST,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineMailingAddressES'));
      t_Operator := coalesce(t_Operator,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlineESOperator'));
      t_PhysicalAddressEST := coalesce(t_PhysicalAddressEST,
          api.pkg_ColumnQuery.Value(t_JobId, 'OnlinePremiseAddressES'));

      t_MailingAddressEST := replace(t_MailingAddressEST,chr(13) || chr(10), '<br>');

      if api.pkg_ColumnQuery.Value(t_JobId, 'OnlineESSameAsMailing') = 'Y' then
        t_PhysicalAddressEST := t_MailingAddressEST;
      else
        t_PhysicalAddressEST := to_clob(replace(t_PhysicalAddressEST,chr(13) || chr(10), '<br>') ||
            '</td>');
      end if;

      -- QA
      if extension.pkg_relutils.RelExists(t_JobId, 'Response') = 'Y' then
        -- Table header
        t_QuestionsCLOB := to_clob('<div style="font-family: Calibri, Arial,
            Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><br><u>Application Questions</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;">');
        -- Loop through all QAResponses
        for q in (
            select r.ToObjectId ResponseId
            from api.Relationships r
              join api.RelationshipDefs rd
                  on rd.RelationshipDefId = r.RelationshipDefId
            where r.FromObjectId = t_JobId
              and rd.ToEndPointName = 'Response'
            order by api.pkg_columnquery.NumericValue(ResponseId, 'SortOrder')
            ) loop
          t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style="width: 150px;
              vertical-align:top; text-align:left">Question ' ||
              api.pkg_ColumnQuery.Value(q.ResponseId, 'SortOrder') || ':</td><td>' ||
              api.pkg_ColumnQuery.Value(q.ResponseId, 'Question') || '</td></tr>');
          if coalesce(api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer'), 'N') != 'N/A'
            then t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                "vertical-align:top; text-align:left">Yes / No Response:</td><td>'
                || api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob') || '</td></tr>');
          end if;
          t_AnswerOnJob := api.pkg_ColumnQuery.Value(q.ResponseId, 'AnswerOnJob');
          t_AcceptableAnswer := api.pkg_ColumnQuery.Value(q.ResponseId, 'AcceptableAnswer');
          if coalesce(t_AnswerOnJob, 'N') != coalesce(t_AcceptableAnswer, 'Y')
              and t_AcceptableAnswer is not null then
            if api.pkg_Columnquery.Value(q.ResponseId, 'NonacceptableResponseType') != 'Document'
                then
              t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style="width: 150px;
                  vertical-align:top; text-align:left">Response:</td><td>'
                  || coalesce(api.pkg_ColumnQuery.Value(q.ResponseId, 'ResponseText'),
                  to_char(api.pkg_ColumnQuery.DateValue(q.ResponseId, 'ResponseDate'),
                  'Mon DD, YYYY')) || '</td></tr>');
              if coalesce(t_AcceptableAnswer, 'N') != 'N/A' then
                t_QuestionsCLOB := t_QuestionsCLOB || to_clob('<tr><td style=
                    "width: 250px; vertical-align:top; text-align:left">
                    Non-Acceptable Response Text:</td><td>' ||
                    api.pkg_ColumnQuery.Value(q.ResponseId, 'NonAcceptableText') || '</td></tr>');
              end if;
            end if;
          end if;
          t_QuestionsCLOB := t_QuestionsCLOB || '<tr><td></td><td></td></tr>';
        end loop;
        -- Close the table
        t_QuestionsCLOB := t_QuestionsCLOB || '</table></div>';
      end if;

      -- If there is a rel to documents create table for the documents
      if extension.pkg_relutils.RelExists(t_JobId, 'OLDocument') = 'Y' then
        -- Table header for documents
        t_DocumentListCLOB := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Application Documents</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;">');
        -- Loop through Online Uploads
        for d2 in (
            select
              api.pkg_ColumnQuery.Value(dt.DocumentTypeObjectId, 'Name') DocType,
              dr.FileName,
              lt.CreatedDate
            from api.Relationships r
              join api.RelationshipDefs rd
                  on rd.RelationshipDefId = r.RelationshipDefId
              join api.documentrevisions dr
                  on r.ToObjectId = dr.DocumentId
              join query.r_ElectronicDocDocType dt
                  on dr.DocumentId = dt.DocumentId
              join api.logicaltransactions lt
                  on dr.LogicalTransactionId = lt.LogicalTransactionId
            where r.FromObjectId = t_JobId
              and rd.ToEndPointName = 'OLDocument'
            ) loop
          t_DocumentListCLOB := t_DocumentListCLOB || to_clob('<tr><td style="width: 250px;
              ">Document Name:</td><td>' || d2.FileName || '</td>' || '</tr><tr><td style="width:
              250px; ">Document Type:</td><td>' || d2.DocType || '</td>' || '</tr><tr><td style=
              "width: 250px; ">Upload Date:</td><td>' || d2.CreatedDate || '</td></tr>');
        end loop;
        -- Close the table
        t_DocumentListCLOB := t_DocumentListCLOB || '<tr><td></td></tr></table></div><br>';
      end if;

      -- Retrieve active Police Users for Municipality and Applicant / Legal Entity.
      -- Emails sorted alphabetically
      if t_NotifyPolice = 'Y' then
        for i in (
            select EmailAddress
            from (
                select
                  distinct u.EmailAddress
                from query.u_Users u
                  join query.r_PoliceUserMunicipality pu
                      on u.ObjectId = pu.PoliceUserObjectId
                where pu.MunicipalityObjectId = t_MunicipalityObjectId
                  and u.Active = 'Y'
                union
                select
                  distinct u.EmailAddress
                from api.Relationships r
                  join api.RelationshipDefs rd
                      on rd.RelationshipDefId = r.RelationshipDefId
                  join query.r_ABC_LicenseLicenseeLE lic
                      on lic.LicenseObjectId = r.ToObjectId
                  join query.r_ABC_UserLegalEntity le
                      on le.LegalEntityObjectId = lic.LegalEntityObjectId
                  join query.u_Users u
                      on u.ObjectId = le.UserId
                where r.FromObjectId = t_JobId
                  and rd.ToEndPointName = 'License'
                union
                select t_RegistrantEmail
                from dual
                )
            order by lower(EmailAddress)
            ) loop
          t_EmailsCLOB := t_EmailsCLOB || to_clob('- ' || i.EmailAddress ||
              '<br>');
        end loop;
      -- Retrieve only Applicant / Legal Entity. Email sorted alphabetically
      elsif t_NotifyMunicipality = 'N' and t_NotifyPolice = 'N' then
        for i in (
            select EmailAddress
            from (
                select
                  distinct u.EmailAddress
                from api.Relationships r
                  join api.RelationshipDefs rd
                      on rd.RelationshipDefId = r.RelationshipDefId
                  join query.r_ABC_LicenseLicenseeLE lic
                      on lic.LicenseObjectId = r.ToObjectId
                  join query.r_ABC_UserLegalEntity le
                      on le.LegalEntityObjectId = lic.LegalEntityObjectId
                  join query.u_Users u
                      on u.ObjectId = le.UserId
                where r.FromObjectId = t_JobId
                  and rd.ToEndPointName = 'License'
                union
                select t_RegistrantEmail
                from dual
                )
            order by lower(EmailAddress)
            ) loop
          t_EmailsCLOB := t_EmailsCLOB || to_clob('- ' || i.EmailAddress ||
              '<br>');
        end loop;
      end if;

      -- Get the details needed from the system settings
      select
        ss.FromEmailAddress,
        ss.IsProduction,
        ss.ToEmailAddressNonProd,
        ss.PoliceWebsiteBaseUrl
      into
        t_From,
        t_IsProduction,
        t_To,
        t_BaseURL
      from query.o_systemsettings ss
      where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
          'SystemSettingsNumber', 1);

      -- Conditionally get Resolution/Standard details from system settings.
      -- If NotifyPolice populate the email w/ Resolution details
      /* Left in place and commented out for future use for Resolution
      if t_NotifyPolice = 'Y' then */
      select
        ss.PoliLicResEmailSubject,
        ss.PoliLicResEmailHeaderPhotoURL,
        ss.PoliLicResEmailBodyHeader,
        ss.PoliLicResEmailBodyFooter
      into
        t_Subject,
        t_HeaderImageName,
        t_HeaderText,
        t_FooterText
      from query.o_systemsettings ss
      where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
          'SystemSettingsNumber', 1);
      -- Otherwise, send the standard email.
      /* else
        select
          ss.PoliLicStdEmailSubject,
          ss.PoliLicStdEmailHeaderPhotoURL,
          ss.PoliLicStdEmailBodyHeader,
          ss.PoliLicStdEmailBodyFooter
       into
          t_Subject,
          t_HeaderImageName,
          t_HeaderText,
          t_FooterText
        from query.o_systemsettings ss
        where ss.ObjectId = api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings',
            'SystemSettingsNumber', 1);
      end if; */

      -- Create the body of the email in a clob. Breaking this into several clobs to concat
      -- together in hopes of making this easier to read.
      t_Body1 := '<table cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="100%"><tbody><tr><td style="padding: 20px;"><table
          border="0" cellpadding="0" cellspacing="0" align="center" bgcolor=
          "#ffffff" width="600" style="margin: 0px auto; border-radius: 10px;
          background-color: rgb(255, 255, 255);"><tbody><tr><td colspan="2">
          </td></tr><tr><td width="600"></td></tr><tr><td colspan="2" height="24">
          <img alt="New Jersey ABC" src="' || t_HeaderImageName || '" border="0"
          style="display: block; margin: 0px; padding-bottom: 3px;"></td></tr>
          <tr><td colspan="2"><table border="0" cellpadding="0" cellspacing="0"
          width="600"><tbody><tr><td align="left"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><div style="font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px;
          color: rgb(0, 0, 0); line-height: 1.4em;"><br><br>';
      t_HeaderTextCLOB := to_clob(t_HeaderText);

      t_Body2 := to_clob('<br><br>This notification is being sent to the
          following email addresses:<br>');

      t_Body3 := to_clob('</div><div style="font-family: Calibri, Arial,
          Helvetica, Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
          line-height: 1.4em;"><br><table style="width:100%; font-family:
          Calibri, Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
          rgb(0, 0, 0); line-height: 1.4em;"><tr><td>License Type:</td><td>');

      t_LicenseTypeCLOB := to_clob(t_LicenseType || '</td>');

      t_Body4 := '</tr><tr><td style="width: 250px;">Type of Job:</td><td>';
      t_JobTypeCLOB := to_clob(t_JobType || '</td>');

      t_Body5 := '</tr><tr><td style="width: 250px;">File Number:</td><td>';
      t_JobNumberCLOB := to_clob(t_JobNumber || '</td>');

      t_Body26 := '</tr><tr><td style="width: 250px;">License Number:</td><td>';
      t_LicenseNumber := coalesce(t_LicenseNumber, 'N/A');
      t_LicenseNumberCLOB:= to_clob(t_LicenseNumber || '</td>');

      t_Body6 := '</tr><tr><td style="width: 250px;">Submitted Date:</td><td>';
      t_SubmittedDateCLOB := to_clob(to_char(
          api.pkg_ColumnQuery.DateValue(a_ObjectId, 'DateCompleted'), 'Mon DD, YYYY') || '</td>');

      t_Body7 := '</tr><tr><td style="width: 250px;">Applicant Email:</td><td>';
      t_RegistrantEmailCLOB := to_clob(t_RegistrantEmail || '</td>');

      t_Body8 := '</tr><tr><td style="width: 250px;">Receipt Number:</td><td>';
      t_ReceiptNumberCLOB := to_clob(t_ReceiptNumber || '</td>');

      t_Body9 := '</tr><tr><td style="width: 250px;">Fee Amount:</td><td>';
      t_FeeAmountCLOB := to_clob(t_FeeAmount || '</td>');

      t_SubHeader1 := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Licensee Information</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><tr><td>Licensee Applicant:</td><td>');
      t_LicenseeNameCLOB := to_clob(t_LicenseeName || '</td>');

      t_Body10 := '</tr><tr><td style="width: 250px;">Corporation Number:</td><td>';
      t_CorporationNumberCLOB := to_clob(t_CorporationNumber || '</td>');

      t_Body11 := '</tr><tr><td style="width: 250px;">Incorporation Date:</td><td>';
      t_IncorporationDateCLOB := to_clob(to_char(t_IncorporationDate, 'Mon DD, YYYY') || '</td>');

      t_Body12 := '</tr><tr><td style="width: 250px;">NJ Tax Auth Number:</td><td>';
      t_NJTaxAuthNumberCLOB := to_clob(t_NJTaxAuthNumber || '</td>');

      t_Body13 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Mailing Address:</td><td>');
      t_MailingAddressCLOB := to_clob(t_MailingAddress || '</td>');

      t_Body14 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Physical Address:</td><td>');
      t_PhysicalAddressCLOB := to_clob(t_PhysicalAddress || '</td>');

      t_SubHeader2 := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Contact Information</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><tr><td>Contact Name:</td><td>');
      t_ContactNameCLOB := to_clob(t_ContactName || '</td>');

      t_Body15 := '</tr><tr><td style="width: 250px;">Business Number:</td><td>';
      t_ContactPhoneNumberCLOB := to_clob(t_ContactPhoneNumber || '</td>');

      t_Body16 := '</tr><tr><td style="width: 250px;">Home Number:</td><td>';
      t_ContactAltPhoneNumberCLOB := to_clob(t_ContactAltPhoneNumber || '</td>');

      t_Body17 := '</tr><tr><td style="width: 250px;">Mobile Number:</td><td>';
      t_ContactFaxNumberCLOB := to_clob(t_ContactFaxNumber || '</td>');

      t_Body18 := '</tr><tr><td style="width: 250px;">Contact Email:</td><td>';
      t_ContactEmailAddressCLOB := to_clob(t_ContactEmailAddress || '</td>');

      t_Body19 := '</tr><tr><td style="width: 250px;">Preferred Contact Method:</td><td>';
      t_PreferredContactMethodCLOB := to_clob(t_PreferredContactMethod || '</td>');

      t_SubHeader3 := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>Establishment Information</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><tr><td>Type:</td><td>');
      t_EstablishmentTypeCLOB := to_clob(t_EstablishmentType || '</td>');

      t_Body20 := '</tr><tr><td style="width: 250px;">DBA / Names:</td><td>';
      t_DoingBusinessAsCLOB := to_clob(t_DoingBusinessAs || '</td>');

      t_Body21 := '</tr><tr><td style="width: 250px;">Operator:</td><td>';
      t_OperatorCLOB := to_clob(t_Operator || '</td>');

      t_Body22 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Mailing Address:</td><td>');
      t_MailingAddressESTCLOB := to_clob(t_MailingAddressEST || '</td>');

      t_Body23 := to_clob('</tr><tr><td style="vertical-align:top; text-align:
          left">Premises Address:</td><td>');
      t_PhysicalAddressESTCLOB := to_clob(t_PhysicalAddressEST || '</td>');

      t_SubHeader4 := to_clob('<div style="font-family: Calibri,
            Arial, Helvetica, Verdana, sans-serif; font-size: 14px; color:
            rgb(0, 0, 0); line-height: 1.4em;"><br><u>License Type Details</u><br>
            <table style="width:100%; font-family: Calibri, Arial, Helvetica,
            Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0);
            line-height: 1.4em;"><tr><td>License Type:</td><td>');

      if t_RestrictedBreweryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Restricted Brewery Production:</td><td>' || t_RestrictedBreweryProduction || '</td>');
      elsif t_LimitedBreweryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Limited Brewery Production:</td><td>' || t_LimitedBreweryProduction || '</td>');
      elsif t_SupplementaryLimitedDistillery is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Supplementary Limited Distillery Production:</td><td>' ||
            t_SupplementaryLimitedDistillery || '</td>');
      elsif t_PlenaryWineryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Plenary Winery Production:</td><td>' || t_PlenaryWineryProduction || '</td>');

        if t_PlenaryWineryWholesalePrivileg = 'Y' then
          t_PlenaryWineryWholesalePrivileg := 'Yes';
        else
          t_PlenaryWineryWholesalePrivileg := 'No';
        end if;

        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="
            vertical-align:top; text-align:left">Exercise the wholesale privilege of <br> selling
            to New Jersey retail licensees:</td><td style="vertical-align:top; text-align:left">' ||
            t_PlenaryWineryWholesalePrivileg || '</td>');
      elsif t_FarmWineryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Farm Winery Production:</td><td>' || t_FarmWineryProduction || '</td>');

        if t_FarmWineryWholesalePrivilege = 'Y' then
          t_FarmWineryWholesalePrivilege := 'Yes';
        else
          t_FarmWineryWholesalePrivilege := 'No';
        end if;

        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="
            vertical-align:top; text-align:left">Exercise the wholesale privilege of <br> selling
            to New Jersey retail licensees:</td><td style="vertical-align:top; text-align:left">' ||
            t_FarmWineryWholesalePrivilege || '</td>');
      elsif t_OutOfStateWineryProduction is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Out of State Winery Production:</td><td>' || t_OutOfStateWineryProduction || '</td>');

        if t_OutOfStateWineryWholesalePrivi = 'Y' then
          t_OutOfStateWineryWholesalePrivi := 'Yes';
        else
          t_OutOfStateWineryWholesalePrivi := 'No';
        end if;

        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="
            vertical-align:top; text-align:left">Exercise the wholesale privilege of <br> selling
            to New Jersey retail licensees:</td><td style="vertical-align:top; text-align:left">' ||
            t_OutOfStateWineryWholesalePrivi || '</td>');
      elsif t_RetailTransitType is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Retail Transit Type:</td><td>' || t_RetailTransitType || '</td>');
      elsif t_SportingFacilityCapacity is not null then
        t_LicenseDetailsCLOB := to_clob('</tr><tr><td style="width: 250px;">
            Capacity:</td><td>' || t_SportingFacilityCapacity || '</td>');
      end if;

      if t_WarehouseName is not null then
        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="width: 250px;">
            Warehouse Name:</td><td>' || t_WarehouseName || '</td>');
        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="width: 250px;">
            Warehouse Phone Number:</td><td>' || REGEXP_REPLACE(t_WarehousePhoneNumber,
            '([0-9]{3})([0-9]{3})', '(\1) \2-') || '</td>');
        t_LicenseDetailsCLOB := t_LicenseDetailsCLOB || to_clob('</tr><tr><td style="
          vertical-align:top; text-align:left">Warehouse Address:</td><td>' ||
          replace(t_OnlineWarehouseAddress,chr(13) || chr(10), '<br>') || '</td>');
      end if;

      t_Body25 := '</tr><tr><td style="width: 250px;">Secondary License Types:</td><td>';
      t_OnlineSecondaryLicenseTypesCLOB := to_clob(t_OnlineSecondaryLicenseTypes || '</td>');

      t_EndTableRowDiv := to_clob('</tr></table></div>');
      t_FooterTextCLOB := to_clob('</div><div style="font-family: Calibri, Arial, Helvetica,
          Verdana, sans-serif; font-size: 14px; color: rgb(0, 0, 0); line-height: 1.4em;">
          <table style="width:100%; font-family: Calibri, Arial, Helvetica, Verdana, sans-serif;
          font-size: 14px; color: rgb(0, 0, 0); line-height: 1.4em;"><tr><td>' || t_FooterText ||
          '</td></tr></table></div>');
      t_BodyEnd := to_clob('</td></tr></tbody></table></td></tr></tbody>
          </table><br></td></tr></tbody></table>');

      t_Body := t_body1 || t_HeaderTextCLOB || t_Body2 || t_EmailsCLOB||
          t_Body3 || t_LicenseTypeCLOB || t_Body4 || t_JobTypeCLOB ||
          t_Body5 || t_JobNumberCLOB || t_Body26 || t_LicenseNumberCLOB || t_Body6 ||
          t_SubmittedDateCLOB || t_Body7 || t_RegistrantEmailCLOB || t_Body8 ||
          t_ReceiptNumberCLOB || t_Body9 || t_FeeAmountCLOB || t_EndTableRowDiv || t_SubHeader1 ||
          t_LicenseeNameCLOB || t_Body10 || t_CorporationNumberCLOB || t_Body11 ||
          t_IncorporationDateCLOB || t_Body12 || t_NJTaxAuthNumberCLOB || t_Body13 ||
          t_MailingAddressCLOB || t_Body14 || t_PhysicalAddressCLOB || t_EndTableRowDiv ||
          t_SubHeader2 || t_ContactNameCLOB || t_Body15 || t_ContactPhoneNumberCLOB ||
          t_Body16 || t_ContactAltPhoneNumberCLOB || t_Body17 || t_ContactFaxNumberCLOB ||
          t_Body18 || t_ContactEmailAddressCLOB || t_Body19 || t_PreferredContactMethodCLOB ||
          t_EndTableRowDiv || t_SubHeader3 || t_EstablishmentTypeCLOB || t_Body20 ||
          t_DoingBusinessAsCLOB || t_Body21 || t_OperatorCLOB || t_Body22 ||
          t_MailingAddressESTCLOB || t_Body23 || t_PhysicalAddressESTCLOB || t_EndTableRowDiv ||
          t_SubHeader4 || t_LicenseTypeCLOB || t_LicenseDetailsCLOB || t_Body25 ||
          t_OnlineSecondaryLicenseTypesCLOB || t_EndTableRowDiv || t_QuestionsCLOB ||
          t_DocumentListCLOB || t_FooterTextClob || t_BodyEnd;

      -- Get the users that the email should be sent to
      if t_IsProduction = 'Y' then
        -- If the system is a production system send it to all users registered with the Licensee
        for i in (
            select
              distinct u.EmailAddress
            from api.Relationships r
              join api.RelationshipDefs rd
                  on rd.RelationshipDefId = r.RelationshipDefId
              join query.r_ABC_LicenseLicenseeLE lic
                  on lic.LicenseObjectId = r.ToObjectId
              join query.r_ABC_UserLegalEntity le
                  on le.LegalEntityObjectId = lic.LegalEntityObjectId
              join query.u_Users u
                  on u.ObjectId = le.UserId
            where r.FromObjectId = t_JobId
              and rd.ToEndPointName = 'License'
            union
            select t_RegistrantEmail
            from dual
            ) loop
          extension.pkg_sendmail.SendEmail(t_JobId, t_From, i.EmailAddress, null,
              null, t_Subject, t_Body, null, 'OLDocument', 'Y');
        end loop;

        -- If the system is a production system and the license type is set to Notify Police
        -- send it to the clerks related to the municipality.
        if t_NotifyPolice = 'Y' then
          for i in (
              select
                distinct u.EmailAddress
              from query.u_Users u
                join query.r_PoliceUserMunicipality pu
                    on u.ObjectId = pu.PoliceUserObjectId
              where pu.MunicipalityObjectId = t_MunicipalityObjectId
                and u.Active = 'Y'
              ) loop
            extension.pkg_sendmail.SendEmail(t_JobId, t_From, i.EmailAddress, null,
                null, t_Subject, t_Body, null, 'OLDocument', 'Y');
          end loop;
        end if;
      else
        -- If the system is a test system send it to the test email on the config site
        extension.pkg_sendmail.SendEmail(t_JobId, t_From, t_To, null, null,
            t_Subject, t_Body, null, 'OLDocument', 'Y');
      end if;
    end if;

  end PoliceEmailForLicense;

  /*---------------------------------------------------------------------------
  * ReviewEmailReminder() -- PUBLIC
  *   Send reminder Email to Municipal Users / Applicant on Permit Application/
  * Renewal
  *-------------------------------------------------------------------------*/
  procedure ReviewEmailReminder (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_JobId                             udt_Id;
    t_MunicipalityObjectId              udt_id;
    t_Body                              clob;
    t_FromEmailAddress                  varchar2(4000);
    t_IsProd                            varchar2(1);
    t_ProcessType                       varchar2(4000);
    t_Subject                           varchar2(4000);
    t_ToAddress                         varchar2(4000);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_MuniSubject                       varchar2(4000);
    t_PoliceSubject                     varchar2(4000);
    t_NoteDefDescription                varchar2(60);
    t_SystemSettingsObjId               udt_Id;
    t_MunicipalReviewType               udt_Id;
    t_PoliceReviewType                  udt_Id;
    t_NoteTag                           varchar2(10);
    t_IsProduction                      varchar2(1);
    t_UserIdList                        api.pkg_definition.udt_IdList;
  begin

    t_JobId := api.pkg_ColumnQuery.Value(a_ObjectId, 'JobId');
    t_ProcessType := api.pkg_columnquery.value(a_ObjectId, 'ObjectDefId');
    t_MunicipalReviewType := api.pkg_configquery.ObjectDefIdForName('p_ABC_MunicipalityReview');
    t_PoliceReviewType := api.pkg_configquery.ObjectDefIdForName('p_ABC_PoliceReview');
    t_MunicipalityObjectId := api.pkg_columnquery.value(a_ObjectId, 'MunicipalityObjectId');

    if api.pkg_ColumnQuery.Value(t_JobId, 'EnteredOnline') = 'Y' then
      -- Get the details needed from the system settings
      select ss.FromEmailAddress,
             ss.IsProduction,
             ss.ToEmailAddressNonProd,
             ss.MuniRevReminderEmailSubject,
             ss.PoliceRevReminderEmailSubject,
             ss.objectid
      into t_FromEmailAddress,
           t_IsProduction,
           t_ToEmailAddressNonProd,
           t_MuniSubject,
           t_PoliceSubject,
           t_SystemSettingsObjId
      from query.o_systemsettings ss;

      if t_ProcessType = t_MunicipalReviewType then
        t_NoteTag := 'MRREML';
        t_Subject := t_MuniSubject;
        select mu.MunicipalUserObjectId
        bulk collect
        into t_UserIdList
        from query.r_MunicipalUserMunicipality mu
        where mu.MunicipalityObjectId = t_MunicipalityObjectId;

      elsif t_ProcessType = t_PoliceReviewType then
        t_NoteTag := 'PRREML';
        t_Subject := t_PoliceSubject;
        select pu.PoliceUserObjectId
        bulk collect
        into t_UserIdList
        from query.r_policeusermunicipality pu
        where pu.MunicipalityObjectId = t_MunicipalityObjectId;
      else
        return;
      end if;

      select n.NoteText
      into t_Body
      from api.notes n
      join api.notedefs nd
        on nd.NoteDefId = n.NoteDefId
      where nd.tag = t_NoteTag
        and n.ObjectId = t_SystemSettingsObjId;

      -- Make substitutions
      t_Body := replace(t_Body, '{FirstName}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'FirstName'));
      t_Body := replace(t_Body, '{LastName}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'LastName'));
      t_Body := replace(t_Body, '{EmailAddress}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'EmailAddress'));
      t_Body := replace(t_Body, '{PhoneNumber}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'PhoneNumber'));
      t_Body := replace(t_Body, '{FormattedPhoneNumber}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'FormattedPhoneNumber'));
      t_Body := replace(t_Body, '{JobNumber}', api.pkg_ColumnQuery.Value
          (a_ObjectId, 'FileNumber'));

      -- Get the users that the email should be sent to
      if t_IsProduction = 'Y' then
        -- If the system is a production system send it to the clerks related
        -- to the municipality.
          for i in 1..t_UserIdList.count loop
            if api.pkg_columnquery.Value(t_UserIdlist(i), 'Active') = 'Y' then
              t_ToAddress := api.pkg_columnquery.Value(t_UserIdList(i),'EmailAddress');
              -- Verify that To Address is not null
              continue when t_ToAddress is not null;
              extension.pkg_sendmail.SendEmail(t_JobId, t_FromEmailAddress, t_ToAddress, null,
                  null, t_Subject,t_Body, null, null, 'Y', 'N');
            end if;
          end loop;
      else
        -- Verify that To Email Address Non Prod is not null
        if t_ToEmailAddressNonProd is null then
          return;
        end if;
        -- If the system is a test system send it to the test email on the config
        -- site
        extension.pkg_sendmail.SendEmail(t_JobId, t_FromEmailAddress, t_ToEmailAddressNonProd,
            null, null, t_Subject, t_Body, null, null, 'Y', 'N');
      end if;
    end if;
  end ReviewEmailReminder;

end pkg_ABC_municipality;
/

