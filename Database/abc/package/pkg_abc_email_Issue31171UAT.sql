create or replace package abc.pkg_ABC_Email is

  -- Author  : JOHN.PETKAU
  -- Created : 9/26/2011 9:56:26 AM
  -- Purpose : Used to call possesendmail from any object

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;
  -----------------------------------------------------------------------------
  -- SendTestNoteEmail()
  -----------------------------------------------------------------------------
  procedure SendTestNoteEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_TestType                          varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  );

  -----------------------------------------------------------------------------
  -- SendTestEmail()
  -----------------------------------------------------------------------------
  procedure SendTestEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_TestType                          varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  );

  /*---------------------------------------------------------------------------
   * SendRegistrationEmail()
   *   Send a registration email to a new external user from registration page
   * Run on Column Change of boolean SendRegistrationEmail column on j_Registration.
   *-------------------------------------------------------------------------*/
  procedure SendRegistrationEmail (
    a_RegistrationId                    udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SendPasswordResetEmail() -- PUBLIC
   *   Send a password reset email to a public user from the forgot password
   * page. Run on Column Change of boolean SendPasswordResetEmail column on
   * j_Registration.
   *-------------------------------------------------------------------------*/
  procedure SendPasswordResetEmail (
    a_RegistrationId                    udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SendLicensingEmail()
   *   Send an Approve/Reject email on ABC License Jobs
   *-------------------------------------------------------------------------*/
  procedure SendLicensingEmail (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );

  /*--------------------------------------------------------------------------
   * InsertLink() -- PUBLIC
   * Inserts a link for text enclosed in [[ ]] and preserves the text.
   *------------------------------------------------------------------------*/
  function InsertLink (
    a_Body varchar2,
    a_Link varchar2
  )return varchar2;

  /*--------------------------------------------------------------------------
   * PreviewLicensingEmail() -- PUBLIC
   * Returns the HTML from the Email template object specified - from NLC Issue 11356
   *------------------------------------------------------------------------*/
  function PreviewLicensingEmail (
    a_EmailTemplateObjectId          number
  ) return clob;

  /*---------------------------------------------------------------------------
   * SendLicensingEmailMunicipality()
   *   Send an email to the Municipal user upon issuance of the license.
   *-------------------------------------------------------------------------*/
  procedure SendLicensingEmailMunicipality (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SendPasswordResetEmailMuni()
   *   Send a password reset email to a municipal user from the forgot password
   * page. Run on Column Change of boolean SendPasswordResetEmailMuni column on
   * j_Registration.
   *-------------------------------------------------------------------------*/
  procedure SendPasswordResetEmailMuni (
    a_RegistrationId                    udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SendPRRenewalEmail()
   *   Send an email to the user when a Product Registration Renewal job has
   * completed xref generation
   *-------------------------------------------------------------------------*/
  procedure SendPRRenewalEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SendRequestAddtionalInfoEmail()
   *   Send a Notification email to applicatns that need to provide additional
   * information
   *-------------------------------------------------------------------------*/
  procedure SendRequestAdditionalInfoEmail (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SendPasswordResetEmailPolice()
   *   Send a password reset email to a municipal user from the forgot password
   * page. Run on Column Change of boolean SendPasswordResetEmailPolice column
   * on j_Registration.
   *-------------------------------------------------------------------------*/
  procedure SendPasswordResetEmailPolice (
    a_RegistrationId                    udt_Id,
    a_AsOfDate                          date
  );

  /*---------------------------------------------------------------------------
   * SendReassignmentEmail() -- PUBLIC
   *   Send a notification email to inform the user that a process has been
   *   reassigned to them.
   *-------------------------------------------------------------------------*/
  procedure SendReassignmentEmail (
    a_ProcessId                         udt_Id,
    a_UserId                            udt_Id
  );

 /*----------------------------------------------------------------------------
  * SendAutomaticCancellationEmail() -- PUBLIC
  *   Send an Automatic Cancellation email to an external user
  *--------------------------------------------------------------------------*/
  procedure SendAutomaticCancellationEmail (
    a_ObjectId                          udt_Id,
    a_AutomaticCancellationPeriod       varchar2,
    a_AsOfDate                          date
  );

end pkg_ABC_Email;
/
grant execute
on pkg_abc_email
to posseextensions;

create or replace package body pkg_ABC_Email is
 /*---------------------------------------------------------------------------
  * ParseEmailBody()
  *-------------------------------------------------------------------------*/
  function ParseEmailBody (
    a_ObjectId                          number,
    a_TextToParse                       varchar2
  ) return varchar2 is
    t_ColumnName                        varchar2(4000);
    t_StartIndex                        pls_integer;
    t_Length                            pls_integer;
    t_ColumnDataType                    varchar2(4000);
    t_ColumnValue                       varchar2(4000);

  begin
    -- find beginning of first column name
    t_StartIndex := instr(a_TextToParse, '{');
    t_Length := instr(a_TextToParse, '}', t_StartIndex) - (t_StartIndex + 1);

    -- No columns were found to be replaced
    if t_StartIndex = 0 or t_StartIndex is null or t_Length <= 0 or t_Length is null then
      return a_TextToParse;
    end if;

    t_ColumnName := substr(a_TextToParse, t_StartIndex + 1, t_Length);

    -- Format the date value
    loop
      begin
        select datatype
          into t_ColumnDataType
          from api.ColumnDefs cd
          join api.Objects ob
            on ob.ObjectDefId = cd.ObjectDefId
         where ob.ObjectId = a_ObjectId
           and lower(cd.name) = lower(t_ColumnName);

        if t_ColumnDataType = 'date' then
          t_ColumnValue := to_char(api.pkg_ColumnQuery.DateValue(a_ObjectId, t_ColumnName), 'FMMonth DD, YYYY');
        else
          t_ColumnValue := api.pkg_ColumnQuery.Value(a_ObjectId, t_ColumnName);
        end if;

        exit;
      exception when no_data_found then
        -- if the column is not a valid column on this object, skip it and go to the next column
        t_StartIndex := instr(a_TextToParse, '{', t_StartIndex + t_Length);
        t_Length := instr(a_TextToParse, '}', t_StartIndex) - (t_StartIndex + 1);

        -- No other columns were found in this string
        if t_StartIndex = 0 or t_StartIndex is null or t_Length <= 0 or t_Length is null then
          return a_TextToParse;
        end if;

        t_ColumnName := substr(a_TextToParse, t_StartIndex + 1, t_Length);
      end;
    end loop;

    return ParseEmailBody(a_ObjectId, replace(a_TextToParse, '{' || t_ColumnName || '}', t_ColumnValue));
  end ParseEmailBody;

  -----------------------------------------------------------------------------
  -- SendTestNoteEmail()
  -----------------------------------------------------------------------------
  procedure SendTestNoteEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_TestType                          varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  ) is
    t_Body                   varchar2(32767);

  begin

  /*Make sure To is not empty*/
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'DevelopmentEmailAddresses') is null
    then
       return;
    end if;
    if api.pkg_columnquery.Value(a_ObjectId, 'TriggerNoteEmail') = 'Y' then
      -- Get the Note Text
      begin
      select n.Text into t_Body from api.notes n
        join api.notedefs nd on n.NoteDefId = nd.NoteDefId
        where n.ObjectId = a_ObjectId and nd.Tag = a_TestType;
      exception
        when too_many_rows then
          api.pkg_errors.raiseerror(-20000, 'You have too many Editors.');
        when no_data_found then
          api.pkg_errors.raiseerror(-20000, 'Test email content not found.');
      end;
      -- Make substitutions
      t_body := replace(t_Body, '{EmailHeader}', api.pkg_ColumnQuery.Value(a_ObjectId, 'EmailHeader'));
      t_body := replace(t_Body, '{WelcomeText}', api.pkg_ColumnQuery.Value(a_ObjectId, 'WelcomeText'));

      -- Append a warning to the beginning of the body if this is a non-production environment.
      if api.pkg_columnquery.Value(a_ObjectId, 'IsProduction') = 'N' then
        if Length(t_Body) < 32300 then --leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This Test Email is from a non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; font-weight: bold; color: red;">This Test Email is from a non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;

      -- Sending the email.
      extension.pkg_sendmail.SendPosseEmail(a_ObjectId , a_AsOfDate, a_FromAddressColName
          ,a_ToAddressColName, '', '', a_SubjectColName,
          t_Body, '', '', a_BodyIsHtml);

    end if;
  end SendTestNoteEmail;

  -----------------------------------------------------------------------------
  -- SendTestEmail()
  -----------------------------------------------------------------------------
  procedure SendTestEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_FromAddressColName                varchar2,
    a_ToAddressColName                  varchar2,
    a_SubjectColName                    varchar2,
    a_TestType                          varchar2,
    a_BodyIsHtml                        varchar2 default 'Y'
  ) is
    t_Body                   varchar2(32767);
  begin

  /*Make sure To is not empty*/
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'DevelopmentEmailAddresses') is null
    then
       return;
    end if;
    if api.pkg_columnquery.Value(a_ObjectId, 'TriggerEmail') = 'Y' then

    if a_TestType = 'REGEML' then
      t_Body := api.pkg_columnquery.Value(a_ObjectId, 'RegEmailPreamble');
      t_Body := t_Body || api.pkg_columnquery.Value(a_ObjectId, 'RegEmailClosing');
    elsif a_TestType = 'PWREML' then
      t_Body := api.pkg_columnquery.Value(a_ObjectId, 'ResetEmailPreamble')
       || api.pkg_columnquery.Value(a_ObjectId, 'ResetEmailClosing');
    end if;
      -- Make substitutions
      t_Body := replace(t_Body, '{EmailHeader}', api.pkg_ColumnQuery.Value(a_ObjectId, 'EmailHeader'));
      t_body := replace(t_Body, '{WelcomeText}', api.pkg_ColumnQuery.Value(a_ObjectId, 'WelcomeText'));

      -- Sending the email.
      extension.pkg_sendmail.SendPosseEmail(a_ObjectId , a_AsOfDate, a_FromAddressColName
          ,a_ToAddressColName, '', '', a_SubjectColName,
          t_Body, '', '', a_BodyIsHtml);

    end if;
  end SendTestEmail;

  /*---------------------------------------------------------------------------
   * SendRegistrationEmail() -- PUBLIC
   *   Send a registration email to a new external user from registration page
   * Run on Column Change of boolean SendRegistrationEmail column on j_Registration.
   *-------------------------------------------------------------------------*/
  procedure SendRegistrationEmail (
    a_RegistrationId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_SysObjectId                       udt_Id;
    t_EmailType                         varchar2(6) :=
      api.pkg_columnquery.Value(a_RegistrationId, 'EmailType');
    t_Body                              varchar2(32767);
    t_IsProd                            varchar2(1);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_FromEmailAddress                  varchar2(4000);
    t_ToAddress                         varchar2(4000);
    t_Subject                           varchar2(4000);
  begin

    select
      o.ObjectId,
      o.IsProduction,
      o.ToEmailAddressNonProd,
      o.FromEmailAddress,
      o.RegEmailSubject
    into
      t_SysObjectId,
      t_IsProd,
      t_ToEmailAddressNonProd,
      t_FromEmailAddress,
      t_Subject
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    select n.Text
    into t_Body
    from api.notes n
    join api.notedefs nd
        on n.NoteDefId = nd.NoteDefId
    where n.ObjectId = t_SysObjectId
      and nd.Tag = t_EmailType;

    -- Make sure To is not empty
    t_ToAddress := api.pkg_ColumnQuery.Value(a_RegistrationId, 'EmailAddress');
    if t_ToAddress is null then
      return;
    end if;
    if api.pkg_columnquery.Value(a_RegistrationId, 'SendRegistrationEmail') = 'Y' then
      -- Make substitutions
      t_Body := replace(t_Body, '{FirstName}', api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'FirstName'));
      t_Body := replace(t_Body, '{LastName}', api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'LastName'));
      t_Body := replace(t_Body, '{EmailAddress}', api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'EmailAddress'));
      t_Body := replace(t_Body, '{PhoneNumber}', api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'PhoneNumber'));
      t_Body := replace(t_Body, '{FormattedPhoneNumber}', api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'FormattedPhoneNumber'));
      t_Body := replace(t_Body, '{SecurityQuestion}', api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'SecurityQuestion'));
      t_Body := replace(t_Body, '{SecurityAnswer}', api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'SecurityAnswer'));
      t_Body := replace(t_Body, '{RegistrationLink}', api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'RegistrationLink'));
      t_Body := InsertLink(t_Body, api.pkg_ColumnQuery.Value(
          a_RegistrationId, 'RegistrationLink'));
      -- Append a warning to the beginning of the body if this is a non-production
      -- environment.
      if t_IsProd = 'N' then
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromEmailAddress);
        if Length(t_Body) < 32300 then
          -- Leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;
      -- Sending the email.
      extension.pkg_SendMail.SendEmail(a_RegistrationId, t_FromEmailAddress,
          t_ToAddress, null, null, t_Subject, t_Body, null, 'Y', 'Y');
    end if;

  end SendRegistrationEmail;

  /*---------------------------------------------------------------------------
   * SendPasswordResetEmail() -- PUBLIC
   *   Send a password reset email to a public user from the forgot password
   * page. Run on Column Change of boolean SendPasswordResetEmail column on
   * j_Registration.
   *-------------------------------------------------------------------------*/
  procedure SendPasswordResetEmail (
    a_RegistrationId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_SysObjectId                       udt_Id;
    t_EmailType                         varchar2(6) := 'PWREML';
    t_Body                              varchar2(32767);
    t_IsProd                            varchar2(1);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_FromEmailAddress                  varchar2(4000);
    t_ToAddress                         varchar2(4000);
    t_Subject                           varchar2(4000);
  begin

    select
      o.ObjectId,
      o.IsProduction,
      o.ToEmailAddressNonProd,
      o.FromEmailAddress,
      o.ResetEmailSubject
    into
      t_SysObjectId,
      t_IsProd,
      t_ToEmailAddressNonProd,
      t_FromEmailAddress,
      t_Subject
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    select n.Text
    into t_Body
    from api.notes n
    join api.notedefs nd
        on n.NoteDefId = nd.NoteDefId
    where n.ObjectId = t_SysObjectId
      and nd.Tag = t_EmailType;

    -- Make sure To is not empty
    t_ToAddress := api.pkg_ColumnQuery.Value(a_RegistrationId, 'EmailAddress');
    if t_ToAddress is null then
      return;
    end if;

    if api.pkg_columnquery.Value(a_RegistrationId, 'SendPasswordResetEmail') = 'Y' then
      -- Make substitutions
      t_Body := replace(t_Body, '{FirstName}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'FirstName'));
      t_Body := replace(t_Body, '{LastName}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'LastName'));
      t_Body := replace(t_Body, '{EmailAddress}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'EmailAddress'));
      t_Body := replace(t_Body, '{PhoneNumber}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'PhoneNumber'));
      t_Body := replace(t_Body, '{FormattedPhoneNumber}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'FormattedPhoneNumber'));
      t_Body := replace(t_Body, '{SecurityQuestion}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'SecurityQuestion'));
      t_Body := replace(t_Body, '{SecurityAnswer}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'SecurityAnswer'));
      t_Body := replace(t_Body, '{RegistrationLink}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'RegistrationLink'));
      t_Body := InsertLink(t_Body, api.pkg_ColumnQuery.Value(a_RegistrationId,
          'RegistrationLink'));
      -- Append a warning to the beginning of the body if this is a non-production
      -- environment.
      if t_IsProd = 'N' then
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromEmailAddress);
        if Length(t_Body) < 32300 then
          -- Leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;
      -- Sending the email.
      extension.pkg_SendMail.SendEmail(a_RegistrationId, t_FromEmailAddress,
          t_ToAddress, null, null, t_Subject, t_Body, null, 'Y', 'Y');
    end if;

  end SendPasswordResetEmail;

  /*---------------------------------------------------------------------------
   * SendLicensingEmail()
   *   Send an Approve/Reject email on ABC License Jobs
   *-------------------------------------------------------------------------*/
  procedure SendLicensingEmail (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_SysObjectId                       udt_Id;
    t_Body                              varchar2(32767);
    t_IsProd                            varchar2(1);
    t_ExternalWebsiteURL                varchar2(4000);
    t_CertificateEndPointName           varchar2(4000);
    t_EmailTemplateObjectId             number :=
        api.pkg_ColumnQuery.Value(a_ProcessId, 'EmailTemplateObjectId');
    t_ToEmailAddressNonProd             varchar2(4000);
    t_ToEmailAddress                    varchar2(4000);
    t_CCEmailAddress                    varchar2(4000);
    t_FromEmail                         varchar2(200) :=
        api.pkg_ColumnQuery.Value(a_ProcessId,'FromEmailAddress');
    t_EmailSubject                      varchar2(4000) :=
        api.pkg_ColumnQuery.Value(a_ProcessId,'EmailSubject');
  begin
    -- Make sure To is not empty and email should be sent
    if api.pkg_ColumnQuery.Value(a_ProcessId, 'SendToEmailAddresses') is null or
       api.pkg_columnquery.Value(a_ProcessId, 'SendEmail') = 'N' then
      return;
    end if;

    -- Get System Settings details
    select
      o.ObjectId,
      o.IsProduction,
      o.ExternalWebsiteBaseURL,
      o.ToEmailAddressNonProd
    into
      t_SysObjectId,
      t_IsProd,
      t_ExternalWebsiteURL,
      t_ToEmailAddressNonProd
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    -- Get the Certificate End Point
    select
      case
        when AttachCertificate = 'Y' and EmailType = 'Approval' then
          case
            when extension.pkg_RelUtils.RelExists(a_ProcessID, 'LicCertificate') = 'Y' then
                 'LicCertificate'
            when extension.pkg_RelUtils.RelExists(a_ProcessID, 'LicCertificateIND') = 'Y' then
                 'LicCertificateIND'
          end
      end
    into t_CertificateEndPointName
    from query.o_ABC_EmailTemplate
    where ObjectId = t_EmailTemplateObjectId;

    -- Get Note Text (Email Body)
    select n.Text
    into t_Body
    from api.notes n
    where n.ObjectId = t_EmailTemplateObjectId;

    -- Make substitutions
    if api.pkg_ColumnQuery.Value(t_EmailTemplateObjectId, 'LicenseType') = 'Special Event' then
      t_Body := replace(t_Body, '{EstablishmentLocationAddress}', nvl(
          api.pkg_ColumnQuery.Value(a_ProcessId, 'EstablishmentLocationAddress'),
          api.pkg_ColumnQuery.Value(a_ProcessId, 'EventLocationAddress')));
    end if;
    t_Body := replace(t_Body, '{OnlineLink}', t_ExternalWebsiteURL);
    t_Body := ParseEmailBody(api.pkg_ColumnQuery.Value(
        a_ProcessId, 'LicenseObjectId'), t_Body);
    t_Body := ParseEmailBody(a_ProcessId, t_Body);

    -- Append a warning to the beginning of the body if this is a non-production
    -- environment.
    if t_IsProd = 'N' then
      t_ToEmailAddress := nvl(t_ToEmailAddressNonProd, t_FromEmail);
      t_CCEmailAddress := null;
      if Length(t_Body) < 32300 then
        -- Leave 200 for this html, and 200 for the email headers added later.
        t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
            'font-weight: bold; color: red;">This Test Email is from a ' ||
            'non-production system.</span><br><br>' || t_Body;
      else
        t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
            'font-weight: bold; color: red;">This Test Email is from a ' ||
            'non-production system.<br><br>Message too long... truncated.</span><br>';
      end if;
    else
      t_ToEmailAddress := api.pkg_ColumnQuery.Value(a_ProcessId, 'SendToEmailAddress');
      t_CCEmailAddress := api.pkg_ColumnQuery.Value(a_ProcessId, 'CCEmailAddress');
    end if;

    -- Send the email.
    extension.pkg_SendMail.SendEmail(a_ProcessId, t_FromEmail, t_ToEmailAddress,
        t_CCEmailAddress, null, t_EmailSubject, t_Body, null, t_CertificateEndPointName,
        'Y');

  end SendLicensingEmail;

  /*--------------------------------------------------------------------------
   * InsertLink() -- PUBLIC
   * Inserts a link for text enclosed in [[ ]] and preserves the text.
   *------------------------------------------------------------------------*/
  function InsertLink (
    a_Body varchar2,
    a_Link varchar2
  )
  return varchar2 is
    t_TextBeforeLink       varchar2(32767);
    t_TextAfterLink        varchar2(32767);
    t_LinkText             varchar2(32767);
    t_Final                varchar2(32767);
  begin
    if instr(a_Body, '[[') > 0 and instr(a_Body, ']]') > 0 then
      t_TextBeforeLink := substr(a_Body, 1, instr(a_Body, '[[') - 1);
      t_TextAfterLink := substr(a_Body, instr(a_Body, ']]') + 2);
      t_LinkText := substr(a_Body, instr(a_Body, '[[') + 2, instr(a_Body, ']]') - instr(a_Body, '[[') - 2);
      t_Final := t_TextBeforeLink || '<a href="' || a_Link || '">' || t_LinkText || '</a>' || t_TextAfterLink;
    return t_Final;
  end if;
  return a_Body;

  end InsertLink;

  /*--------------------------------------------------------------------------
   * PreviewLicensingEmail() -- PUBLIC
   * --------------------------------------------------------------------------*/
  function PreviewLicensingEmail (
      a_EmailTemplateObjectId     number
  ) return clob is
       t_Body                     varchar2(32767);
  begin
    select n.Text
      into t_Body
      from api.notes n
     where n.ObjectId = a_EmailTemplateObjectId;

    return t_Body;
  end PreviewLicensingEmail;

  /*---------------------------------------------------------------------------
   * SendLicensingEmailMunicipality()
   *   Send an email to the Municipal user upon issuance of the license.
   *-------------------------------------------------------------------------*/
  procedure SendLicensingEmailMunicipality (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_SysObjectId                       udt_Id;
    t_Body                              varchar2(32767);
    t_JobId                             number :=
        api.pkg_ColumnQuery.Value(a_ProcessId,'JobId');
    t_LicenseId                         number :=
        api.pkg_ColumnQuery.Value(t_JobId,'LicenseObjectId');
    t_Municipality                      varchar2(200) :=
        api.pkg_ColumnQuery.Value(t_LicenseId,'OfficeObjectId');
    t_MunicipalityName                  varchar2(200) :=
        api.pkg_ColumnQuery.Value(t_Municipality,'Name');
    t_MunicipalityEmail                 varchar2(4000);
    t_MunicipalEndPoint                 udt_Id :=
        api.pkg_configquery.EndPointIdForName('u_Users','Municipality');
    t_IsProd                            varchar2(1);
    t_ExternalWebsiteURL                varchar2(4000);
    t_EmailTemplateObjectId             number :=
        api.pkg_ColumnQuery.Value(a_ProcessId,'EmailTemplateObjectId');
    t_FromEmail                         varchar2(200) :=
        api.pkg_ColumnQuery.Value(a_ProcessId,'FromEmailAddress');
    t_EmailSubject                      varchar2(200);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_ToEmailAddress                    varchar2(4000);
  begin

    for i in (select api.pkg_columnquery.value(r.FromObjectId,'EmailAddress')
                  as EmailAddress
              from api.relationships r
              where r.ToObjectId = t_Municipality
                and r.EndPointId = t_MunicipalEndPoint
                and api.pkg_columnquery.value(r.FromObjectId,'Active') = 'Y'
                and api.pkg_columnquery.value(r.FromObjectId,'IsMunicipalUser') = 'Y'
                and api.pkg_columnquery.value(r.FromObjectId,'EmailAddress') is not null
             ) loop
      t_MunicipalityEmail := t_MunicipalityEmail || i.emailaddress || ', ';
    end loop;

    -- If the user email has not been generated for the job, return
    if t_MunicipalityEmail is null then
      return;
    end if;

    -- Get System Settings details
    select
      o.ObjectId,
      o.IsProduction,
      o.ExternalWebsiteBaseURL,
      o.MunicipalEmailSubject,
      o.ToEmailAddressNonProd
    into
      t_SysObjectId,
      t_IsProd,
      t_ExternalWebsiteURL,
      t_EmailSubject,
      t_ToEmailAddressNonProd
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    -- Get Note Text (Email Body)
    select n.Text
    into t_Body
    from api.notes n
    join api.notedefs nd
        on nd.NoteDefId = n.NoteDefId
    where nd.Tag = 'MUNIEM';

      -- Make substitutions
    t_Body := replace(t_Body,'{FirstName}',
        api.pkg_columnquery.value(a_ProcessId,'OnlineGTFirstName'));
    t_Body := replace(t_Body, '{RegistrationLink}',
        api.pkg_ColumnQuery.Value(a_ProcessId, 'RegistrationLink'));
    t_Body := replace(t_Body, '{Licensee}',
        api.pkg_ColumnQuery.Value(a_ProcessId, 'Licensee'));
    t_Body := replace(t_Body, '{LicenseNumber}',
        api.pkg_ColumnQuery.Value(a_ProcessId, 'LicenseNumber'));
    t_Body := replace(t_Body, '{LicenseType}',
        api.pkg_ColumnQuery.Value(a_ProcessId, 'LicenseType'));
    t_Body := InsertLink(t_Body,
        api.pkg_ColumnQuery.Value(a_ProcessId, 'RegistrationLink'));

    -- Append a warning to the beginning of the body if this is a non-production
    -- environment.
    if t_IsProd = 'N' then
      t_ToEmailAddress := nvl(t_ToEmailAddressNonProd, t_FromEmail);
      if Length(t_Body) < 32300 then
        -- Leave 200 for this html, and 200 for the email headers added later.
        t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
            'font-weight: bold; color: red;">This Test Email is from a ' ||
            'non-production system.</span><br><br>' || t_Body;
      else
        t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
            'font-weight: bold; color: red;">This Test Email is from a ' ||
            'non-production system.<br><br>Message too long... truncated.</span><br>';
      end if;
    else
      t_ToEmailAddress := t_MunicipalityEmail;
    end if;

    -- Send the email
    extension.pkg_SendMail.SendEmail(a_ProcessId, t_FromEmail, t_ToEmailAddress,
        null, null, t_EmailSubject, t_Body, null, null, 'Y');

  end SendLicensingEmailMunicipality;

  /*---------------------------------------------------------------------------
   * SendPasswordResetEmailMuni() -- PUBLIC
   *   Send a password reset email to a municipal user from the forgot password
   * page. Run on Column Change of boolean SendPasswordResetEmailMuni column on
   * j_Registration.
   *-------------------------------------------------------------------------*/
  procedure SendPasswordResetEmailMuni (
    a_RegistrationId                    udt_Id,
    a_AsOfDate                          date
  ) is
    t_SysObjectId                       udt_Id;
    t_EmailType                         varchar2(6) := 'MPWREM';
    t_Body                              varchar2(32767);
    t_IsProd                            varchar2(1);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_FromEmailAddress                  varchar2(4000);
    t_ToAddress                         varchar2(4000);
    t_Subject                           varchar2(4000);
  begin

    select
      o.ObjectId,
      o.IsProduction,
      o.ToEmailAddressNonProd,
      o.FromEmailAddress,
      o.ResetEmailSubject
    into
      t_SysObjectId,
      t_IsProd,
      t_ToEmailAddressNonProd,
      t_FromEmailAddress,
      t_Subject
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    select n.Text
    into t_Body
    from api.notes n
    join api.notedefs nd
        on n.NoteDefId = nd.NoteDefId
    where n.ObjectId = t_SysObjectId
      and nd.Tag = t_EmailType;

    -- Make sure To is not empty
    t_ToAddress := api.pkg_ColumnQuery.Value(a_RegistrationId, 'EmailAddress');
    if t_ToAddress is null then
      return;
    end if;

    if api.pkg_columnquery.Value(
        a_RegistrationId, 'SendPasswordResetEmailMuni') = 'Y' then
      -- Make substitutions
      t_Body := replace(t_Body, '{FirstName}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'UserFirstNameLookup'));
      t_Body := replace(t_Body, '{LastName}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'LastName'));
      t_Body := replace(t_Body, '{EmailAddress}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'EmailAddress'));
      t_Body := replace(t_Body, '{PhoneNumber}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'PhoneNumber'));
      t_Body := replace(t_Body, '{FormattedPhoneNumber}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'FormattedPhoneNumber'));
      t_Body := replace(t_Body, '{SecurityQuestion}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'SecurityQuestion'));
      t_Body := replace(t_Body, '{SecurityAnswer}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'SecurityAnswer'));
      t_Body := replace(t_Body, '{RegistrationLink}', api.pkg_ColumnQuery.Value
          (a_RegistrationId, 'PasswordResetLinkMuni'));
      t_Body := InsertLink(t_Body, api.pkg_ColumnQuery.Value(a_RegistrationId,
          'PasswordResetLinkMuni'));
      -- Append a warning to the beginning of the body if this is a non-production
      -- environment.
      if t_IsProd = 'N' then
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromEmailAddress);
        if Length(t_Body) < 32300 then
          -- Leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;
      -- Sending the email.
      extension.pkg_SendMail.SendEmail(a_RegistrationId, t_FromEmailAddress,
          t_ToAddress, null, null, t_Subject, t_Body, null, 'Y', 'Y');
    end if;

  end SendPasswordResetEmailMuni;

  /*---------------------------------------------------------------------------
   * SendPRRenewalEmail() -- PUBLIC
   *   Send an email to the user when a Product Registration Renewal job has
   * completed xref generation
   *-------------------------------------------------------------------------*/
  procedure SendPRRenewalEmail (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_SysObjectId                       udt_Id;
    t_Body                              varchar2(32767);
    t_IsProd                            varchar2(1);
    t_ExternalWebsiteURL                varchar2(4000);
    t_EmailTemplateObjectId             number :=
        api.pkg_ColumnQuery.Value(a_ObjectId,'EmailTemplateObjectId');
    t_UserEmail                         varchar2(100) :=
        api.pkg_ColumnQuery.Value(a_ObjectId,'OnlineGTEmail');
    t_FromEmail                         varchar2(100) :=
        api.pkg_ColumnQuery.Value(a_ObjectId,'FromEmailAddress');
    t_OnlineUser                        number :=
        api.pkg_ColumnQuery.Value(a_ObjectId,'OnlineUserObjectId');
    t_EmailSubject                      varchar2(200);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_ToAddress                         varchar2(4000);
  begin

    -- If the user email has not been generated for the job, return
    if t_UserEmail is null or
       api.pkg_ColumnQuery.Value(t_OnlineUser,'Active') = 'N' then
      return;
    end if;

    -- Get System Settings details
    select
      o.ObjectId,
      o.IsProduction,
      o.ExternalWebsiteBaseURL,
      o.RenewalEmailSubject,
      o.ToEmailAddressNonProd
    into
      t_SysObjectId,
      t_IsProd,
      t_ExternalWebsiteURL,
      t_EmailSubject,
      t_ToEmailAddressNonProd
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    -- Get Note Text (Email Body)
    select n.Text
    into t_Body
    from api.notes n
    join api.notedefs nd
        on nd.NoteDefId = n.NoteDefId
    where nd.Tag = 'RENPRC';

    -- Make substitutions
    t_Body := replace(t_Body,'{FirstName}', api.pkg_columnquery.value(
        a_ObjectId,'OnlineGTFirstName'));
    t_Body := replace(t_Body,'{RegistrationLink}', api.pkg_ColumnQuery.Value(
        a_ObjectId,'RegistrationLink'));
    t_Body := InsertLink(t_Body, api.pkg_ColumnQuery.Value(
        a_ObjectId, 'RegistrationLink'));

    -- Append a warning to the beginning of the body if this is a non-production
    -- environment.
    if t_IsProd = 'N' then
      t_UserEmail := nvl(t_ToEmailAddressNonProd, t_FromEmail);
      if Length(t_Body) < 32300 then
        -- Leave 200 for this html, and 200 for the email headers added later.
        t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
            'font-weight: bold; color: red;">This Test Email is from a ' ||
            'non-production system.</span><br><br>' || t_Body;
      else
        t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
            'font-weight: bold; color: red;">This Test Email is from a ' ||
            'non-production system.<br><br>Message too long... truncated.</span><br>';
      end if;
    end if;

    -- Send the email
    extension.pkg_SendMail.SendEmail(a_ObjectId, t_FromEmail, t_UserEmail, null,
        null, t_EmailSubject, t_Body, null, null, 'Y');

  end SendPRRenewalEmail;

  /*---------------------------------------------------------------------------
   * SendRequestAddtionalInfoEmail() -- PUBLIC
   *   Send a Notification email to applicatns that need to provide additional
   * information
   *-------------------------------------------------------------------------*/
  procedure SendRequestAdditionalInfoEmail (
    a_ProcessId                         udt_Id,
    a_AsOfDate                          date
  ) is
    t_SysObjectId                       udt_Id;
    t_ToEmailAddress                    varchar2(100);
    t_ToEmailAddressNonProd             varchar2(100);
    t_FromEmailAddress                  varchar2(100);
    t_Body                              varchar2(32767);
    t_BodyNoName                        varchar2(32767);
    t_EmailSubject                      varchar2(200);
    t_BodyTemplate                      varchar2(32767);
    t_IsProd                            varchar2(1);
    t_ExternalWebsiteURL                varchar2(4000);
    t_UserId                            number;
    t_JobId                             number;
    t_JobDefName                        varchar2(100);
    t_ContactName                       varchar2(500);
  begin

    if api.pkg_processquery.jobvalue(a_ProcessId, 'EnteredOnline') = 'Y' and
       api.pkg_columnquery.Value(api.pkg_processquery.jobvalue(
           a_ProcessId, 'CreatedByUserId'), 'IsABCWebGuest') = 'N'  then
      -- If the user is a guest, we don't want to send an email out, nor if there
      -- was no online submission
      -- Check if an email needs to be sent
      if api.pkg_columnquery.value(a_ProcessId, 'Comments') is not null then
        -- If an email does need to be sent, get the objectid of the job that
        -- the email is related to.
        -- As of July 2017, only the New Applcation and Amendment Application
        -- jobs were eligible to send out this email type.
        t_JobId := api.pkg_columnquery.value(a_ProcessId, 'JobId');
        t_JobDefName := api.pkg_columnquery.value(t_JobId, 'ObjectDefName');
        -- Get System Settings details
        select
          o.ObjectId,
          o.IsProduction,
          o.ExternalWebsiteBaseURL,
          o.FromEmailAddress,
          o.ReqAdditionalInfoEmailSubject,
          o.ToEmailAddressNonProd
        into
          t_SysObjectId,
          t_IsProd,
          t_ExternalWebsiteURL,
          t_FromEmailAddress,
          t_EmailSubject,
          t_ToEmailAddressNonProd
        from query.o_SystemSettings o
        where o.ObjectDefTypeId = 1
          and rownum = 1;

        -- Get Note Text (Email Body) for the Request Additional Information
        -- Email (RAIE).
        select n.Text
        into t_Body
        from api.notes n
        join api.notedefs nd
            on n.NoteDefId = nd.NoteDefId
        where n.ObjectId = t_SysObjectId
            and nd.Tag = 'RAIE';

        -- Get the object ID for the User that created the job
        -- Licensing Jobs
        if t_JobDefName = 'j_ABC_NewApplication' then
          select
            ou.UserId,
            api.pkg_columnquery.Value(ou.UserId, 'EmailAddress')
          into
            t_UserId,
            t_ToEmailAddress
          from query.r_ABC_NewApplicationOnlineUser ou
          where ou.NewApplicationJobId = t_JobId;
        end if;
        if t_JobDefName = 'j_ABC_AmendmentApplication' then
          select
            ou.UserId,
            api.pkg_columnquery.Value(ou.UserId, 'EmailAddress')
          into
            t_UserId,
            t_ToEmailAddress
          from query.r_ABC_AmendAppOnlineUser ou
          where ou.AmendApplicationJobId = t_JobId;
        end if;

        --Substituting variables into the email body
        t_Body := replace(t_Body, '{FirstName}', api.pkg_ColumnQuery.Value(
            t_UserId, 'FirstName'));
        t_Body := replace(t_Body, '{LastName}', api.pkg_ColumnQuery.Value(
            t_UserId, 'LastName'));
        t_Body := replace(t_Body, '{JobType}', api.pkg_ColumnQuery.Value(
            t_JobId, 'ObjectDefDescription'));
        t_Body := replace(t_Body, '{JobNumber}', api.pkg_ColumnQuery.Value(
            t_JobId, 'ExternalFileNum'));
        -- Used to carry over the returns from the information requested.
        t_Body := replace(t_Body, '{AdditionalInformation}', replace (
            api.pkg_ColumnQuery.Value(a_ProcessId, 'Comments'), chr(10), '<br>'));
        t_Body := replace(t_Body, '{NotificationLink}', api.pkg_ColumnQuery.Value(
            t_SysObjectId, 'ExternalWebsiteBaseURL'));
        t_Body := InsertLink(t_Body, api.pkg_ColumnQuery.Value(t_SysObjectId,
            'ExternalWebsiteBaseURL'));
        -- Append a warning to the beginning of the body if this is a
        -- non-production environment.
        if t_IsProd = 'N' then
          t_ToEmailAddress := nvl(t_ToEmailAddressNonProd, t_FromEmailAddress);
          if Length(t_Body) < 32300 then
            -- Leave 200 for this html, and 200 for the email headers added later.
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a ' ||
                'non-production system.</span><br><br>' || t_Body;
          else
            t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
                'font-weight: bold; color: red;">This Test Email is from a ' ||
                'non-production system.<br><br>Message too long... truncated.</span><br>';
          end if;
        end if;
        -- Send the email.
        extension.pkg_SendMail.SendEmail(t_JobId, t_FromEmailAddress, t_ToEmailAddress,
            null, null, t_EmailSubject, t_Body, null, 'Y', 'Y');
      end if;
    end if;

  end SendRequestAdditionalInfoEmail;

  /*---------------------------------------------------------------------------
   * SendPasswordResetEmailPolice() -- PUBLIC
   *   Send a password reset email to a municipal user from the forgot password
   * page. Run on Column Change of boolean SendPasswordResetEmailPolice column
   * on j_Registration.
   *-------------------------------------------------------------------------*/
  procedure SendPasswordResetEmailPolice (
    a_RegistrationId                    udt_Id,
    a_AsOfDate                          date
  ) is

    t_Body                              varchar2(32767);
    t_Email                             varchar2(4000);
    t_EmailType                         varchar2(6) := 'PPWREM';
    t_FirstName                         varchar2(4000);
    t_FormattedPhoneNumber              varchar2(4000);
    t_FromEmailAddress                  varchar2(4000);
    t_IsProd                            varchar2(1);
    t_LastName                          varchar2(4000);
    t_PasswordResetLink                 varchar2(4000);
    t_PhoneNumber                       varchar2(4000);
    t_SendPasswordResetEmail            varchar2(1);
    t_SecurityAnswer                    varchar2(4000);
    t_SecurityQuestion                  varchar2(4000);
    t_Subject                           varchar2(4000);
    t_SysObjectId                       udt_Id;
    t_ToAddress                         varchar2(4000);
    t_ToEmailAddressNonProd             varchar2(4000);
  begin

    select
      o.ObjectId,
      o.IsProduction,
      o.ToEmailAddressNonProd,
      o.FromEmailAddress,
      o.ResetEmailSubject
    into
      t_SysObjectId,
      t_IsProd,
      t_ToEmailAddressNonProd,
      t_FromEmailAddress,
      t_Subject
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    select n.Text
    into t_Body
    from
        api.notes n
        join api.notedefs nd
            on n.NoteDefId = nd.NoteDefId
    where n.ObjectId = t_SysObjectId
      and nd.Tag = t_EmailType;

    -- Make sure To is not empty
    t_ToAddress := api.pkg_ColumnQuery.Value(a_RegistrationId, 'EmailAddress');
    if t_ToAddress is null then
      return;
    end if;

    t_SendPasswordResetEmail := api.pkg_columnquery.Value(a_RegistrationId,
        'SendPasswordResetEmailPolice');
    if t_SendPasswordResetEmail = 'Y' then
      t_FirstName := api.pkg_ColumnQuery.Value(a_RegistrationId, 'UserFirstNameLookup');
      t_PasswordResetLink := api.pkg_ColumnQuery.Value(a_RegistrationId,
          'PasswordResetLinkPolice');
      t_LastName :=  api.pkg_ColumnQuery.Value(a_RegistrationId, 'LastName');
      t_Email := api.pkg_ColumnQuery.Value(a_RegistrationId, 'EmailAddress');
      t_PhoneNumber := api.pkg_ColumnQuery.Value(a_RegistrationId, 'PhoneNumber');
      t_FormattedPhoneNumber :=  api.pkg_ColumnQuery.Value(a_RegistrationId,
          'FormattedPhoneNumber');
      t_SecurityQuestion := api.pkg_ColumnQuery.Value(a_RegistrationId,
          'SecurityQuestion');
      t_SecurityAnswer := api.pkg_ColumnQuery.Value(a_RegistrationId, 'SecurityAnswer');
      -- Make substitutions
      t_Body := replace(t_Body, '{FirstName}', t_FirstName);
      t_Body := replace(t_Body, '{LastName}', t_LastName);
      t_Body := replace(t_Body, '{EmailAddress}', t_Email);
      t_Body := replace(t_Body, '{PhoneNumber}', t_PhoneNumber);
      t_Body := replace(t_Body, '{FormattedPhoneNumber}', t_FormattedPhoneNumber);
      t_Body := replace(t_Body, '{SecurityQuestion}', t_SecurityQuestion);
      t_Body := replace(t_Body, '{SecurityAnswer}', t_SecurityAnswer);
      t_Body := replace(t_Body, '{RegistrationLink}', t_PasswordResetLink);
      t_Body := InsertLink(t_Body, t_PasswordResetLink);
      -- Append a warning to the beginning of the body if this is a non-production
      -- environment.
      if t_IsProd = 'N' then
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromEmailAddress);
        if Length(t_Body) < 32300 then
          -- Leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;
      -- Sending the email.
      extension.pkg_SendMail.SendEmail(a_RegistrationId, t_FromEmailAddress,
          t_ToAddress, null, null, t_Subject, t_Body, null, 'Y', 'Y');
    end if;

  end SendPasswordResetEmailPolice;

  /*---------------------------------------------------------------------------
   * SendReassignmentEmail() -- PUBLIC
   *   Send a notification email to inform the user that a process has been
   *   reassigned to them.
   *-------------------------------------------------------------------------*/
  procedure SendReassignmentEmail (
    a_ProcessId                         udt_Id,
    a_UserId                            udt_Id
  ) is

    t_Body                              varchar2(32767);
    t_FirstName                         varchar2(4000);
    t_FromEmailAddress                  varchar2(4000);
    t_IsProd                            varchar2(1);
    t_JobNumber                         number;
    t_JobType                           varchar2(4000);
    t_PermitType                        varchar2(4000);
    t_ProcessType                       varchar2(4000);
    t_Subject                           varchar2(4000);
    t_ToAddress                         varchar2(4000);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_UserId                            udt_Id;
  begin

   select pt.Description
   into t_ProcessType
   from
     api.processes p
       join api.processtypes pt
         on pt.ProcessTypeId = p.ProcessTypeId
    where p.ProcessId = a_ProcessId;

    select
      o.IsProduction,
      o.ToEmailAddressNonProd,
      o.FromEmailAddress
    into
      t_IsProd,
      t_ToEmailAddressNonProd,
      t_FromEmailAddress
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    t_JobNumber := api.pkg_columnquery.Value(a_ProcessId, 'FileNumber');
    t_PermitType := api.pkg_columnquery.Value(a_ProcessId, 'PermitType');
    t_JobType := api.pkg_columnquery.Value(a_ProcessId, 'JobTypeDescription');
    t_ToAddress := api.pkg_ColumnQuery.Value(a_UserId, 'EmailAddress');

    -- Make sure To is not empty
    if t_ToAddress is null then
      return;
    end if;

      t_Subject := 'Reassignment Notification';
      t_FirstName := api.pkg_ColumnQuery.Value(a_UserId, 'FirstName');
      -- Form email body
      t_Body := 'Dear ' ||t_FirstName|| ',<br> The following ' ||t_ProcessType||
          ' has been reassigned to you. <br><br>' || '<b>Job Number:</b> ' ||t_JobNumber||
          '<br> <b>Job Type:</b> ' ||t_JobType|| '<br> <b>Permit Type:</b> '||t_PermitType||
          '<br><br> Please check your To Do List.';
      -- Append a warning to the beginning of the body if this is a non-production
      -- environment.
      if t_IsProd = 'N' then
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromEmailAddress);
        if Length(t_Body) < 32300 then
          -- Leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;
      -- Sending the email.
      extension.pkg_SendMail.SendEmail(a_UserId, t_FromEmailAddress,
          t_ToAddress, null, null, t_Subject, t_Body, null, 'Y', 'Y');

  end SendReassignmentEmail;

 /*----------------------------------------------------------------------------
  * SendAutomaticCancellationEmail() -- PUBLIC
  *   Send an Automatic Cancellation email to an external user
  *--------------------------------------------------------------------------*/

  procedure SendAutomaticCancellationEmail (
    a_ObjectId                          udt_Id,
    a_AutomaticCancellationPeriod       varchar2,
    a_AsOfDate                          date
  ) is
    t_Body                              varchar2(32767);
    t_EmailType                         varchar2(6) := 'ACEML';
    t_FromEmailAddress                  varchar2(4000);
    t_IsProd                            varchar2(1);
    t_JobTypeId                         udt_Id;
    t_JobTypeObjectId                   udt_Id;
    t_Subject                           varchar2(4000);
    t_SysObjectId                       udt_Id;
    t_ToAddress                         varchar2(4000);
    t_ToEmailAddressNonProd             varchar2(4000);
    t_UserObjectId                      udt_Id
        := api.pkg_ColumnQuery.Value(a_ObjectId, 'OnlineUserObjectId');

  begin

    select
      o.ObjectId,
      o.IsProduction,
      o.ToEmailAddressNonProd,
      o.FromEmailAddress,
      o.AutomaticCancellationEmailSubj
    into
      t_SysObjectId,
      t_IsProd,
      t_ToEmailAddressNonProd,
      t_FromEmailAddress,
      t_Subject
    from query.o_SystemSettings o
    where o.ObjectDefTypeId = 1
      and rownum = 1;

    select n.Text
    into t_Body
    from api.notes n
    join api.notedefs nd
        on n.NoteDefId = nd.NoteDefId
    where n.ObjectId = t_SysObjectId
      and nd.Tag = t_EmailType;

    -- Make sure To is not empty
    t_ToAddress := api.pkg_ColumnQuery.Value(t_UserObjectId, 'EmailAddress');
    if t_ToAddress is null then
      return;
    end if;
      -- Make substitutions
      t_Body := replace(t_Body, '{FirstName}',
          api.pkg_ColumnQuery.Value(t_UserObjectId, 'FirstName'));
      t_Body := replace(t_Body, '{EmailAddress}',
          api.pkg_ColumnQuery.Value(t_UserObjectId, 'EmailAddress'));
      t_Body := replace(t_Body, '{JobType}',
          api.pkg_ColumnQuery.Value(a_ObjectId, 'ObjectDefDescription'));
      t_Body := replace(t_Body, '{FileNumber}',
          api.pkg_ColumnQuery.Value(a_ObjectId, 'ExternalFileNum'));
      t_Body := replace(t_Body, '{CreatedDate}',
          to_char(api.pkg_ColumnQuery.DateValue(a_ObjectId, 'CreatedDate'), 'Mon DD, YYYY'));
      t_Body := replace(t_Body, '{AutoCancellationDate}',
          to_char(trunc((api.pkg_ColumnQuery.DateValue(a_ObjectId, 'CreatedDate'))) +
          nvl(a_AutomaticCancellationPeriod, 0), 'Mon DD, YYYY'));
      t_Body := replace(t_Body, '{Link}', api.pkg_ColumnQuery.Value(a_ObjectId, 'Link'));
      t_Body := InsertLink(t_Body, api.pkg_ColumnQuery.Value(
          a_ObjectId, 'Link'));
      -- Append a warning to the beginning of the body if this is a non-production
      -- environment.
      if t_IsProd = 'N' then
        t_ToAddress := nvl(t_ToEmailAddressNonProd, t_FromEmailAddress);
        if Length(t_Body) < 32300 then
          -- Leave 200 for this html, and 200 for the email headers added later.
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.</span><br><br>' || t_Body;
        else
          t_Body := '<span style="font-family: Helvetica,Arial,sans-serif; ' ||
              'font-weight: bold; color: red;">This Test Email is from a ' ||
              'non-production system.<br><br>Message too long... truncated.</span><br>';
        end if;
      end if;
      -- Sending the email.
      extension.pkg_SendMail.SendEmail(a_ObjectId, t_FromEmailAddress,
          t_ToAddress, null, null, t_Subject, t_Body, null, 'Y', 'Y');

  end SendAutomaticCancellationEmail;

end pkg_ABC_Email;
/

