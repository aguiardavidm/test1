create or replace package     pkg_ABC_Establishment is

  -- Author  : JOHN.PETKAU
  -- Created : 5/16/2011 9:47:48 AM
  -- Purpose : Create a snapshot of Establishment when it is edited

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  -----------------------------------------------------------------------------
  -- CreateSnapshot
  --   Creates a snapshot of Establishment object when it is edited
  -----------------------------------------------------------------------------
  procedure CreateSnapshot (
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date default sysdate
    );

end pkg_ABC_Establishment;

/

create or replace package body     pkg_ABC_Establishment is

  -----------------------------------------------------------------------------
  --   CreateSnapshot
  --   Creates an Establishment History object that duplicates the details on a_ObjectId,
  --   and relates the two.
  -----------------------------------------------------------------------------
  procedure CreateSnapshot (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date default sysdate
    ) is
    type NameArrayType is varray(9) of varchar2(30);
    ColumnsToCopy NameArrayType
      := NameArrayType('DoingBusinessAs','ContactName','ContactPhoneNumber','ContactAltPhoneNumber',
          'ContactFaxNumber','ContactEmailAddress','PreferredContactMethod','Active','AdditionalDBANames');
    t_LatestHistoryId                            udt_Id := null;
    t_NewIncludesRelId                           udt_Id := null;
    t_NewEstablishmentHistory                    udt_Id;
    begin
    if api.pkg_columnquery.value(a_ObjectId, 'Edit') = 'Y' then
      t_NewEstablishmentHistory
        := api.pkg_objectupdate.new( api.pkg_configquery.ObjectDefIdForName('o_ABC_EstablishmentHistory') );

      -- FromDate comes from the last EH related to this Establishment
      select max(EstabHistoryObjectId)
        into t_LatestHistoryId
        from query.r_ABC_EstablishmentEstHistory
       where EstablishmentObjectId = a_ObjectId;
      api.pkg_columnupdate.SetValue (t_NewEstablishmentHistory, 'FromDate', nvl(api.pkg_columnquery.value(t_LatestHistoryId, 'ToDate'), api.pkg_columnquery.value(a_ObjectId, 'CreatedDate')));
      api.pkg_columnupdate.SetValue (t_NewEstablishmentHistory, 'ToDate', sysdate);
      api.pkg_columnupdate.SetValue (t_NewEstablishmentHistory, 'CopiedOperator', api.pkg_columnquery.value(a_ObjectId, 'Operator'));

      -- copy details to new EH
      for i in 1..ColumnsToCopy.count loop
        api.pkg_ColumnUpdate.SetValue(t_NewEstablishmentHistory, ColumnsToCopy(i),
                                      api.pkg_columnquery.value(a_ObjectId,ColumnsToCopy(i)));
      end loop;

      -- copy rels to new EH
      for reltocopy in (select r.ToObjectId, rd.ToEndPointName
                                   from api.relationships r
                                   join api.relationshipdefs rd
                                     on r.EndPointId = rd.ToEndPointId
                                  where r.FromObjectId = a_ObjectId
                                    and rd.ToEndpointName in ('MailingAddress','PhysicalAddress','EstablishmentType')
                        ) loop
        extension.pkg_relationshipupdate.new(t_NewEstablishmentHistory, reltocopy.ToObjectId, reltocopy.ToEndpointName);
      end loop; -- related items

      -- relate EH (with all its rels) to passed-in Est.
      extension.pkg_relationshipupdate.new(a_ObjectId, t_NewEstablishmentHistory, 'EstablishmentHistory');

    end if;

  end CreateSnapshot;

end pkg_ABC_Establishment;

/

