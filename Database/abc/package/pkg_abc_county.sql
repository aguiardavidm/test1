create or replace package pkg_ABC_county is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  /*---------------------------------------------------------------------------
   * PostVerifyWrapper()
   *   Runs on Post-Verify of the ABC Region (County)
   *-------------------------------------------------------------------------*/
  procedure PostVerifyWrapper (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );


end pkg_ABC_county;


/
create or replace package body pkg_ABC_county is

  /*---------------------------------------------------------------------------
   * PostVerifyWrapper() -- PUBLIC
   *   Runs on Post-Verify of the ABC Region (County)
   *-------------------------------------------------------------------------*/
  procedure PostVerifyWrapper (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_ActiveMuniCount                   number(4);
    t_CountyIsActive                    varchar2(1);
    t_CountyWholesaleDefaultCount       number(4);
    t_MuniWholesaleDefaultCount         number(4);
  begin

    select count(*)
    into t_CountyWholesaleDefaultCount
    from query.o_abc_region r
    where r.WholesaleDefault = 'Y';

    select count(*)
    into t_MuniWholesaleDefaultCount
    from query.o_abc_office o
    where o.WholesaleDefault = 'Y';

    select count(o.Active)
    into t_ActiveMuniCount
    from query.o_abc_office o
    where o.CountyObjectId = a_ObjectId
      and o.Active = 'Y';

    t_CountyIsActive := api.pkg_columnquery.Value(a_ObjectId, 'Active');
    if t_CountyIsActive = 'N' and t_ActiveMuniCount > 0 then
      api.pkg_errors.RaiseError(-20008, 'You cannot deactivate the County when there are active'
          || ' Municipalities associated.');
    end if;

    if t_CountyWholesaleDefaultCount > 1 then
      api.pkg_errors.RaiseError(-20000, 'Only one County can be marked as Wholesale Default.');
    end if;

    if t_CountyWholesaleDefaultCount = 0 and t_MuniWholesaleDefaultCount > 0 then
      api.pkg_errors.RaiseError(-20000, 'A Municipality marked as Wholesale Default exists for'
          || ' this county, please remove Wholesale Default flag on the Municipality.');
    end if;

end PostVerifyWrapper;

end pkg_ABC_county;
/
