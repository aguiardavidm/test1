create or replace package abc.pkg_ABC_Permit is

  -- Author  : DALAINYA.BRUINSMA
  -- Created : 29/01/2015 5:03:54 PM
  -- Purpose : General procedures for the ABC Permit Object

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  -- Public function and procedure declarations

  -----------------------------------------------------------------------------
  -- GeneratePermitNumber
  --  Runs on Post-Verify of the ABC Permit.
  --  Purpose:
  --    This is intended to be controlled more by workflow, but for Phase1 we
  --    do not have the Permit Job available.
  --    This procedure will generate a Permit Number based on a POSSE sequence
  --    and assign it to the given Permit object (called from the Permit)
  -----------------------------------------------------------------------------
  procedure GeneratePermitNumber(
    a_ObjectId       udt_Id,
    a_AsAtDate       date
  );

  -----------------------------------------------------------------------------
  -- PopulateVehicleInsigniaNumbers
  --  Runs on change procedure on "PopulateInsigniaNumbers" on ABC Permit.
  --    Set by action on "PopulateInsigniaNumbers" button on vehicles tab.
  --  Purpose:
  --    For each related vehicle, set the insignia number starting with the
  --    provided number and incrementing by 1
  -----------------------------------------------------------------------------
  procedure PopulateVehicleInsigniaNumbers(
    a_ObjectId               udt_Id,
    a_AsAtDate               date,
    a_StartingInsigniaNumber number
  );

  /*--------------------------------------------------------------------------
  * CountApprovedPermits () - CountsApporvedPermits this is used on the Dashboard
  *------------------------------------------------------------------------*/
    function CountApprovedPermits(
    a_UserID                         udt_Id
    )  return number;

    /*--------------------------------------------------------------------------
  * MinEventStartDate () - Retrieves the minimum Start Date of a related Event Date
  *------------------------------------------------------------------------*/
    function MinEventStartDate(
      a_PermitObjectId    udt_Id
    )  return date;

 /*****************************************************************************************
  *  ResetLicenseAndPermit()
  *  This will clear out the selected license and permit if a new legal entity is
  *  chosen on an online Permit Application (modelled after product registration procedure)
  *****************************************************************************************/
  procedure ResetLicenseAndPermit(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  );

  /*****************************************************************************************
  * CopyOfficerObjectIdToJob
  *   Copies the current objectId (Legal Entity) onto the New Permit Application Job.
  *****************************************************************************************/
  procedure CopyOfficerObjectIdToJob (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  );

  -----------------------------------------------------------------------------
  -- ValidatePermitData
  --  Runs on Post-Verify of Permit object. Should trigger on:
  --    Clicking "Finish Editing" button.
  --    Change State to "Active" or any save in Active state.
  --  Purpose:
  --    Instead of using Pane Mandatory, validate the data entered when the user
  --    presses the Finish Editing button or updates an Active Permit.
  -----------------------------------------------------------------------------
  procedure ValidatePermitData(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -----------------------------------------------------------------------------
  -- ValidatePermitLetterType
  --  Runs on Post-Verify of PermitLetter object.
  --  Purpose:
  --    Verify a LetterType is given for a Letter.
  -----------------------------------------------------------------------------
  procedure ValidatePermitLetterType(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  -----------------------------------------------------------------------------
  -- ValidateSpecialConditions
  --  Runs on Post-Verify of PermitSpecialConditions object.
  --  Purpose:
  --    Verify all data is entered for a Special Condition.
  -----------------------------------------------------------------------------
  procedure ValidateSpecialConditions(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );


 function RaisePermitError(
      a_ObjectId    udt_Id
    )  return varchar2;

  /*---------------------------------------------------------------------------
   * PermitCancellation
   *   Runs as procedure on constructor of rel r_ABC_PermitCancellation.
   * Handles permit cancellation
   *-------------------------------------------------------------------------*/
  procedure PermitCancellation(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

  -----------------------------------------------------------------------------
  --  TAPClosure
  --  Runs on the TAP Closure object
  --    Closes a TAP permit and copies relationships to the specified License,
  --    or cancels products / permits under the TAP
  -- Updated By:   Elijah R.  2016 Jan 4
  -- Included CPL Submission copy
  -----------------------------------------------------------------------------
  procedure TAPClosure(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  );

  /*---------------------------------------------------------------------------
   * CreateDefaultConditions()
   *   Runs on Constructor of the ABC Permit
   *-------------------------------------------------------------------------*/
  procedure CreateDefaultConditions (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_DefaultCondCreated                char
  );

  /*---------------------------------------------------------------------------
   * CalculateDueDate()
   *  Takes the start date of a process and returns a due date based on the days
   * to respond set on the permit type
   *--------------------------------------------------------------------------*/
  procedure CalculateDueDate (
    a_ObjectId                          udt_id,
    a_AsOfDate                          date,
    a_StartDateColumn                   varchar2,
    a_DaysToAddColumn                   varchar2,
    a_DueDateColumn                     varchar2
  );

  /*---------------------------------------------------------------------------
   * SetMuniPoliceDocType()
   *  Takes the start date of a process and returns a due date based on the days
   * to respond set on the permit type
   *--------------------------------------------------------------------------*/
  procedure SetMuniPoliceDocType (
    a_ObjectId                          udt_id,
    a_AsOfDate                          date,
    a_DocTypeName                       varchar2
  );

  /*---------------------------------------------------------------------------
   * BuyerPermitRelPostVerifyWrapper()
   *  Post verify check on r_BuyersPermit
   *--------------------------------------------------------------------------*/
  procedure BuyerPermitRelPostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  );

  /*---------------------------------------------------------------------------
   * CopyDefaultAttachmentToPermitLetter()
   *   Copies system settings detail to license letter
   *-------------------------------------------------------------------------*/
  procedure CopyDefaultAttachmentToPermitLetter(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_Permit;
/
create or replace package body abc.pkg_ABC_Permit is

  -----------------------------------------------------------------------------
  -- GeneratePermitNumber
  --  Runs on Post-Verify of the ABC Permit.
  --  Purpose:
  --    This is intended to be controlled more by workflow, but for Phase1 we
  --    do not have the Permit Job available.
  --    This procedure will generate a Permit Number based on a POSSE sequence
  --    and assign it to the given Permit object (called from the Permit)
  -----------------------------------------------------------------------------
  procedure GeneratePermitNumber(
    a_ObjectId       udt_Id,
    a_AsAtDate       date
  ) is

  t_SequenceId       udt_Id;
  t_PermitNumber     varchar2(50);

  begin
    -- Determine the sequence
    begin
      select SequenceId
        into t_SequenceId
        from api.Sequences
       where Description = 'ABC Permit Number';

    exception when no_data_found then
      api.pkg_errors.RaiseError(-20000,'Cannot get sequence id for "ABC Permit Number" sequence.');
    end;

    -- Only set the Permit Number if it has not been set already (Set Permit Number is set to Y whenever
    -- the State changes to Active)
    if api.pkg_columnQuery.Value(a_ObjectId, 'PermitNumber', a_AsAtDate) is null then
      -- Find the next number in the sequence
      t_PermitNumber := api.pkg_Utils.GenerateExternalFileNum('[9999999]', t_SequenceId);

      api.pkg_columnUpdate.SetValue(a_ObjectId, 'PermitNumber', t_PermitNumber);
    end if;

  end GeneratePermitNumber;

  -----------------------------------------------------------------------------
  -- PopulateVehicleInsigniaNumbers
  --  Runs on change procedure on "PopulateInsigniaNumbers" on ABC Permit.
  --    Set by action on "PopulateInsigniaNumbers" button on vehicles tab.
  --  Purpose:
  --    For each related vehicle that doesn't already have an insignia number,
  --    set the insignia number starting with the provided number and incrementing
  --    by 1.  If there is no VIN number, raise an error.
  -- Re-written on 3/22/2016  KL
  -----------------------------------------------------------------------------
  procedure PopulateVehicleInsigniaNumbers(
    a_ObjectId               udt_Id,
    a_AsAtDate               date,
    a_StartingInsigniaNumber number
  ) is
    t_NextInsigniaNumber     number := a_StartingInsigniaNumber;
    type numberlist          is table of number;
    type stringlist          is table of varchar2(60);
    t_InsigniaNumberList     numberlist;
    t_VehicleIdList          numberlist;
    t_VinList                stringlist;
    t_Index                  number := 1;
    t_EndPointId             udt_id := api.pkg_configquery.EndPointIdForName('o_ABC_Permit', 'Vehicle');
    t_UniqueInsigNumber      boolean := False;


  begin
    -- Find Vehicle Insignia Numbers already on this Permit
    select distinct x.InsigNumber,
                    nvl(x.VIN, '@#$'), --Hard Coded @#$ to use in member of comparision to see if VIN is null characters are not valid in VINs
                    x.vehicleobjectid
     bulk collect into t_InsigniaNumberList,
                    t_VinList,
                    t_VehicleIdList
    from (select api.pkg_columnquery.Value(v.VehicleObjectId, 'InsigniaNumber') InsigNumber,
               api.pkg_columnquery.Value(v.VehicleObjectId, 'VIN') VIN,
         v.vehicleobjectid
      from query.r_ABC_PermitVehicle v
     where v.PermitObjectId = a_ObjectId) x
    order by 3 asc;  --Important ordering for preservation of on screen funcitionality and sort order opertations

    if '@#$' member of t_VinList then --Usesing Member of against this unique charaterset checks for nulls and only compares once
       api.pkg_errors.RaiseError(-20000, 'Please ensure all vehicles have a VIN specified before continuing.');
    else

    for i in 1..t_VehicleIdList.Count loop
        -- Find an unused Insignia Number
    if t_InsigniaNumberList(i) is null then --If we do not have an insig number than set values and advance loop
      --for t_Index in 1..t_InsigniaNumberList.Count loop
      while t_UniqueInsigNumber = False loop
        if t_NextInsigniaNumber member of t_InsigniaNumberList then--t_InsigniaNumberList(t_Index) = t_NextInsigniaNumber then
           t_NextInsigniaNumber := t_NextInsigniaNumber+1;
          else
           t_UniqueInsigNumber := True;
           continue;  --Exit this iterration of the loop we have found a unique insignia number
          end if;
      end loop;
      t_UniqueInsigNumber := False;
      api.pkg_columnupdate.SetValue(t_VehicleIdList(i), 'InsigniaNumber', t_NextInsigniaNumber);
      t_NextInsigniaNumber := t_NextInsigniaNumber+1;--Increment Insignia number so that the next value is unique
      end if;
    end loop;
        end if;
  end PopulateVehicleInsigniaNumbers;


  /*--------------------------------------------------------------------------
  * CountApprovedPermits () - CountsApporvedPermits this is used on the Dashboard
  *------------------------------------------------------------------------*/
  function CountApprovedPermits (
    a_UserID                         udt_Id
  ) return number is
  t_count number;
  t_DaysToShowExpiredPermits number;
  begin

    Select o.DaysToShowExpiredPermits
      into t_DaysToShowExpiredPermits
      from query.o_SystemSettings o
     where ObjectDefTypeId = 1
       and rownum = 1;


    Select count(PermitObjectId)
    into t_count
    from (
        select
          ppe.PermitObjectId,
          -- left in place because it performed better on this SQL Statement
          api.pkg_columnquery.Value(ppe.PermitObjectId,'ShowOnline') ShowOnline
        from
          query.r_Abc_Userlegalentity r
          join query.r_ABC_PermitPermittee ppe
              on ppe.PermitteeObjectId = r.LegalEntityObjectId
        where r.USerId = a_UserId
        union all
        select
          pel.PermitObjectId,
          'Y' showonline
        from
           query.r_Abc_Userlegalentity r
           join query.r_ABC_LicenseLicenseeLE lle
               on lle.LegalEntityObjectId = r.LegalEntityObjectId
           join query.r_PermitLicense pel
               on pel.LicenseObjectId = lle.LicenseObjectId
           join query.o_ABC_Permit p
               on p.ObjectId = pel.PermitObjectId
           join query.r_ABC_PermitPermitType pt
               on pt.PermitId = pel.PermitObjectId
           join query.o_Abc_Permittype pt
               on pt.ObjectId = pt.PermitTypeId
         where r.UserId = a_UserId
           and pt.PermitteeIsLicensee = 'Y'
           and pt.LicenseRequired in ('Required','Optional')
           -- Handle the ShowOnline here
           and ( p.State = 'Active' OR
               ( p.State = 'Expired' AND trunc(sysdate) - p.ExpirationDate < nvl(t_DaysToShowExpiredPermits , 0) )))
    where showonline = 'Y';

     return t_count;

  end CountApprovedPermits;

 /*--------------------------------------------------------------------------
  * MinEventStartDate () - Retrieves the minimum Start Date of a related Event Date
  *------------------------------------------------------------------------*/
    function MinEventStartDate(
      a_PermitObjectId    udt_Id
    )  return date is
    t_MinDate             date;

    begin
      --loop through event dates on this permit(2 possible paths)
      for x in (select r.EventDateObjectId objectid
               from query.r_ABC_PermitEventDate r
              where r.PermitObjectId = a_PermitObjectId
              union
             select r1.RainDateObjectId objectid
               from query.r_ABC_PermitRainDate r1
              where r1.PermitObjectId = a_PermitObjectId) loop
        if t_MinDate is null then
          t_MinDate := api.pkg_columnquery.DateValue(x.objectid,'StartDate');
        elsif t_MinDate > api.pkg_columnquery.DateValue(x.objectid,'StartDate') then
          t_MinDate := api.pkg_columnquery.DateValue(x.objectid,'StartDate');
        end if;
      end loop;

      -- now subtract the lead days
      t_MinDate := t_MinDate - api.pkg_columnquery.NumericValue(a_PermitObjectid,'EventPermitLeadDays');

      return t_MinDate;
    end;

  /*****************************************************************************************
  *   ResetLicenseAndPermit()
  *  This will clear out the selected license and permit if a new legal entity is
  *  chosen on an online Permit Application (modelled after product registration procedure)
  *****************************************************************************************/
  procedure ResetLicenseAndPermit(
    a_ObjectId         udt_Id,
    a_AsOfDate         date
  ) is
    t_PermitJobId      udt_Id;
    t_EndPoint         udt_Id;
    t_JobId            udt_Id;

  begin
    -- Get the job objectid as set on button click
    t_PermitJobId := api.pkg_columnquery.Value(a_ObjectId, 'TemporaryNewPermitAppJobId');
    dbms_output.put_line('********** Permit App Job Id: ' || t_PermitJobId);
    -- Related License
    t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication','License');

    for i in (select r.RelationshipId
                from api.relationships r
               where r.FromObjectId = t_PermitJobId
                 and r.EndPointId = t_EndPoint) loop

      -- Clear the rel
      api.pkg_relationshipupdate.Remove(i.relationshipid);
    end loop;

    -- Related Permit
    t_EndPoint := api.pkg_configquery.EndPointIdForName('j_ABC_PermitApplication','Permit');

    for i in (select r.RelationshipId
                from api.relationships r
               where r.FromObjectId = t_PermitJobId
                 and r.EndPointId = t_EndPoint) loop

      -- Clear the rel
      api.pkg_relationshipupdate.Remove(i.relationshipid);
    end loop;
  end ResetLicenseAndPermit;

  /*****************************************************************************************
  * CopyOfficerObjectIdToJob
  *   Copies the current objectId (Legal Entity) onto the New Permit Application Job.
  *****************************************************************************************/
  procedure CopyOfficerObjectIdToJob (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date
  ) is
    t_TargetObjectId                        udt_Id := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'TemporaryNewPermitAppJobId');
  begin
    --raise_application_error(-20000, 'Copy Officer to Job.  JobId: ' ||t_TargetObjectId ||' LE: '|| a_ObjectId);
    if t_TargetObjectId is not null then
      api.pkg_ColumnUpdate.SetValue(t_TargetObjectId, 'OnlineUseLEOfficerObjectId', a_ObjectId);
      api.pkg_ColumnUpdate.RemoveValue(a_ObjectId, 'TemporaryNewPermitAppJobId');
    end if;
  end CopyOfficerObjectIdToJob;

  -----------------------------------------------------------------------------
  -- ValidatePermitData
  --  Runs on Post-Verify of Permit object. Should trigger on:
  --    Clicking "Finish Editing" button.
  --    Change State to "Active" or any save in Active state.
  --  Purpose:
  --    Instead of using Pane Mandatory, validate the data entered when the user
  --    presses the Finish Editing button or updates an Active Permit.
  -----------------------------------------------------------------------------
  procedure ValidatePermitData(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_Error                  varchar2(4000);

  begin
    if api.Pkg_Columnquery.Value(a_ObjectId, 'IgnoreErrors') = 'Y' then
      return;
    end if;
    -- Only validate if Edit is 'N' and State is 'Active'
    if api.pkg_ColumnQuery.VALUE (a_ObjectId,'Edit') = 'N'
        and api.pkg_ColumnQuery.VALUE (a_ObjectId,'State') = 'Active' then
      t_Error := RaisePermitError (a_ObjectId);
      if t_Error is not null then
         api.pkg_errors.RaiseError (-20000,t_Error);
      end if;
    end if;

  end ValidatePermitData;

  -----------------------------------------------------------------------------
  -- ValidatePermitLetterType
  --  Runs on Post-Verify of PermitLetter object.
  --  Purpose:
  --    Verify a LetterType is given for a Letter.
  -----------------------------------------------------------------------------
  procedure ValidatePermitLetterType(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_ValidateData         varchar2(1) := 'N';
  begin
    if api.pkg_columnQuery.VALUE (a_ObjectId,'LetterType') is null then
       api.pkg_errors.RaiseError(-20000, 'Before you go on, you must enter a value for Letter Type.');
    end if;

  end ValidatePermitLetterType;

  -----------------------------------------------------------------------------
  -- ValidateSpecialConditions
  --  Runs on Post-Verify of PermitSpecialConditions object.
  --  Purpose:
  --    Verify all data is entered for a Special Condition.
  -----------------------------------------------------------------------------
  procedure ValidateSpecialConditions(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
  begin
    if api.pkg_columnQuery.VALUE (a_ObjectId,'SortOrder') is null then
       api.pkg_errors.RaiseError(-20000, 'Ensure all Order numbers are entered.');
    end if;
    if api.pkg_columnQuery.VALUE (a_ObjectId,'ConditionDescription') is null then
       api.pkg_errors.RaiseError(-20000, 'Ensure all Descriptions are entered.');
    end if;

  end ValidateSpecialConditions;

  -----------------------------------------------------------------------------
  -- RaisePermitError
  --  Check to see if Error needs to be raised on the Permit post-verify
  -----------------------------------------------------------------------------
  function RaisePermitError(
    a_ObjectId       udt_Id
  ) return varchar2 is
    t_Conditions             number;
    t_Solicitors             number;
    t_ApplicationJobId       udt_id;
    t_EnteredOnline          varchar2(1);
    t_HasReviewProcess       varchar2(1);
    t_StartTimeHours               number;
    t_StartTimeMin                 number;
    t_StartTimeAMPM                varchar2(2);
    t_EndTimeHours                 number;
    t_EndTimeMin                   number;
    t_EndTimeAMPM                  varchar2(2);
    t_StartTimeError               varchar2(1) := 'N';
    t_EndTimeError                 varchar2(1) := 'N';
    t_RainStartTimeError               varchar2(1) := 'N';
    t_RainEndTimeError                 varchar2(1) := 'N';

  begin
-- Details tab
    if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitType') is null then
       return 'Please select a Permit Type.';
    end if;

    if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') not in ('AI', 'BRW', 'WN', 'TE', 'TAP') then
/*      if api.pkg_columnQuery.Value(a_ObjectId, 'EffectiveDate') is null then
         api.pkg_errors.RaiseError(-20000, 'Please specify an Effective Date for the Permit.');
      end if;
*/      if api.pkg_columnQuery.Value(a_ObjectId, 'ExpirationMethod') = 'Manual' and
         api.pkg_columnQuery.Value(a_ObjectId, 'ExpirationDate') is null then
         return 'Please specify an Expiration Date.';
      end if;
    end if;

    if api.pkg_columnQuery.Value (a_ObjectId, 'MunicipalityRequired') = 'Y' then
       if api.pkg_columnQuery.Value (a_ObjectId, 'County') is null then
          return 'Please select a County.';
       end if;
       if api.pkg_columnQuery.Value (a_ObjectId, 'Municipality') is null then
          return 'Please select a Municipality.';
       end if;
    end if;
    if api.pkg_columnQuery.Value (a_ObjectId, 'County') is not null then
       if api.pkg_columnQuery.Value (a_ObjectId, 'CountyObjectId') in (api.pkg_columnQuery.Value (a_ObjectId, 'MunicipalityCountyObjectIds')) then
          null;
       else
          return 'The selected Municipality does not belong to the selected County. Please select a new Municipality.';
       end if;
    end if;
    if api.pkg_columnQuery.Value(a_ObjectId, 'RequiresVehicle') = 'Y' and
       api.pkg_columnquery.Value(a_ObjectId, 'InsigniaPermitNumber') is not null then
       null;
    else
-- Check TAP Permit if applicable
    if api.pkg_columnquery.Value(a_ObjectId, 'AllowTAPOverride') = 'Y' then
       if api.pkg_columnquery.Value(a_ObjectId, 'TAPPermitNumber') is not null and
          api.pkg_columnquery.Value(a_ObjectId, 'LicenseNumber')   is not null then
          return 'Please enter either a License or a TAP Number, not both.';
       end if;
       if api.pkg_columnquery.Value(a_ObjectId, 'TAPPermitNumber') is null and
          api.pkg_columnquery.Value(a_ObjectId, 'LicenseNumber')   is null then
          return 'Please enter either a License or a TAP Number.';
       end if;
    else
       if api.pkg_columnQuery.Value (a_ObjectId, 'LicenseRequired') = 'Required' then
          if api.pkg_columnquery.Value(a_ObjectId, 'DisableLicenseLookup') = 'N' then
             if api.pkg_columnQuery.Value (a_ObjectId, 'LicenseNumber') is null then
                return 'Please select a License.';
             end if;
          else
             if api.pkg_columnQuery.Value (a_ObjectId, 'LicenseNumberStored') is null then
                return 'Please enter a License.';
             end if;
          end if;
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitteeIsLicensee') = 'Y' then
             if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitteeLicenseeName') is null then
                return 'Please enter a Permittee.';
             end if;
          else
             if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitteeName') is null then
                return 'Please enter a Permittee.';
             end if;
          end if;
      elsif api.pkg_columnQuery.Value (a_ObjectId, 'LicenseRequired') = 'Optional' then
         if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitteeIsLicensee') = 'Y'    and
            api.pkg_columnQuery.Value (a_ObjectId, 'LicenseNumber') is not null then
            if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitteeLicenseeName') is null then
               return 'Please enter a Permittee.';
            end if;
         else
            if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitteeName') is null then
               return 'Please enter a Permittee.';
            end if;
         end if;
      else
         if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitteeName') is null then
            return 'Please enter a Permittee.';
         end if;
      end if;
   end if;

    if api.pkg_columnQuery.Value (a_ObjectId, 'PermitRequired') = 'Y' then
       if api.pkg_columnQuery.Value (a_ObjectId, 'AssociatedPermitNumber') is null then
          return 'Please select an Associated Permit.';
       end if;
    end if;
    end if;

    if api.pkg_columnQuery.Value (a_ObjectId, 'RequireEffectiveDateOfLicTrans') = 'Y' then
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'EffectiveDateOfLicenseTransfer') is null then
          return 'Please set the Effective Date of the License Transfer.';
       end if;
    end if;

    if api.pkg_columnQuery.VALUE (a_ObjectId, 'RequireSellerLicense') = 'Y' then
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'AllowSellerTapOverride') = 'Y' then
         if api.pkg_columnQuery.VALUE (a_ObjectId, 'SellersLicenseNumber') is null
             and api.pkg_columnQuery.VALUE (a_ObjectId, 'BuyersPermitNumber') is null then
           return 'Please enter a '||api.pkg_columnQuery.VALUE (a_ObjectId, 'LicenseTapInformationLabel')||'''s License or Permit.';
         end if;
       else
         if api.pkg_columnQuery.VALUE (a_ObjectId, 'SellersLicenseNumber') is null then
           return 'Please enter a '||api.pkg_columnQuery.VALUE (a_ObjectId, 'LicenseTapInformationLabel')||'''s License.';
         end if;
       end if;
    end if;

-- PermitType details
    if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') in ('RR','BE','BSP','CTW','FP','SA','AI','BRW','WN','TE','PC','PS','CTS','MA','MSO','SP') then
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'BE' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'NumberofIndividuals') is null then
             return 'Please enter a Number of Individuals associated with this Permit Type.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'BSP' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'PurchasingAlcoholicInventory') is null then
             return 'Please select if Purchasing Alcoholic Inventory.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'CTW' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'NumberOfSolicitors') = 0 and
             api.pkg_columnQuery.VALUE (a_ObjectId, 'NumberOfOwners') = 0 then
             return 'Please enter at least one Solicitor or one Owner.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'MA' then
          if api.pkg_columnquery.VALUE(a_ObjectId, 'PromoterCompanyName') is null then
            return 'Please enter the Company Name for the Promoter.';
          end if;
          if api.pkg_columnquery.VALUE(a_ObjectId, 'PromoterAddress') is null then
            return 'Please enter the Address for the Promoter.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'CTS' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'TotalNumberOfRepresentatives') = 0 then
             return 'Please enter at least one Representative associated with this Permit.';
          end if;
          select  count (*)
             into t_Solicitors
             from query.r_ABC_CTSRepPermit paco
            where paco.PermitObjectId = a_ObjectId
              and api.pkg_columnquery.Value (paco.CTSRepresentativeObjectId,'Name') is null;
          if t_Solicitors > 0 then
             return 'Representative Name is required.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'FP' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'FPNumberOfGallons') is null then
             return 'Please select a Gallonage.';
          end if;
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'FPProductDescription') is null then
             return 'Please enter a Product description.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'MSO' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'TTBPermitNumber') is null then
             return 'A Federal Basic Permit Number is required.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'SA' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'IsCivicReligousOrEducational') is null then
             return 'Please answer the question if the Organization is Religious, Civic or Educational.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') in ('AI','BRW','WN','TE') then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTermFrom') is null then
             return 'Please enter the From Date for the term of the Permit.';
          end if;
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTermTo') is null then
             return 'Please enter the To Date for the term of the Permit.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'PC' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'PersonalConsumptionProductDesc') is null then
             return 'Please enter the Product Description.';
          end if;
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'PersonalConsumptionProductDest') is null then
             return 'Please enter the Product Destination.';
          end if;
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'PersonalConsumptionProductOrig') is null then
             return 'Please enter the Product Origin.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'PS' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'SaleByJudgementBy') is null then
             return 'Please enter a value for Sale By.';
          end if;
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'SaleByJudgementAt') is null then
             return 'Please enter a value for Sale At.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'PermitTypeCode') = 'SP' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'NumberOfProducts') < 1 then
             return 'At least one Product is required.';
          end if;
       end if;
    end if;

-- Events tab
    if api.pkg_columnQuery.VALUE (a_ObjectId, 'RequiresEvents') = 'Y' then
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'RequiresLocation') = 'Y' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'EventLocationAddress') is null then
             return 'Please enter a Location Address for the Event.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'HasEventDateSQL') is null then
          return 'At least one Event Date must be entered.';
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'EventStartDate') is null then
          return 'Please enter a Date for the Event.';
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'HasRainDate') is not null then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'EarliestRainDate') is null then
             return 'Please enter a Date for the Rain Date.';
          end if;
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'RainDateAfterEventDateSQL') is null then
          return 'Rain date must be after the event date.';
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'EventDetails') is null then
          return 'Event Details are required before continuing.';
       end if;
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'EventCount') < 1 then
          return 'At least one Event Date must be entered.';
       end if;

       for i in (select r.EventDateObjectId
                   from query.r_ABC_PermitEventDate r
                  where r.PermitObjectId = a_ObjectId) loop
         select ed.StartTimeHours
               ,ed.StartTimeMinutes
               ,ed.StartTimeAMPM
               ,ed.EndTimeHours
               ,ed.EndTimeMinutes
               ,ed.EndTimeAMPM
           into t_StartTimeHours
               ,t_StartTimeMin
               ,t_StartTimeAMPM
               ,t_EndTimeHours
               ,t_EndTimeMin
               ,t_EndTimeAMPM
           from query.o_abc_eventdate ed
          where ed.ObjectId = i.eventdateobjectid;

          if t_StartTimeHours is null or
             t_StartTimeMin is null or
             t_StartTimeAMPM is null then

             t_StartTimeError := 'Y';

          end if;

          if t_EndTimeHours is null or
             t_EndTimeMin is null or
             t_EndTimeAMPM is null then

             t_EndTimeError := 'Y';

          end if;

       end loop;

       for i in (select r.RainDateObjectId
                   from query.r_ABC_PermitRainDate r
                  where r.PermitObjectId = a_ObjectId) loop
         select ed.StartTimeHours
               ,ed.StartTimeMinutes
               ,ed.StartTimeAMPM
               ,ed.EndTimeHours
               ,ed.EndTimeMinutes
               ,ed.EndTimeAMPM
           into t_StartTimeHours
               ,t_StartTimeMin
               ,t_StartTimeAMPM
               ,t_EndTimeHours
               ,t_EndTimeMin
               ,t_EndTimeAMPM
           from query.o_abc_eventdate ed
          where ed.ObjectId = i.RainDateObjectId;

          if t_StartTimeHours is null or
             t_StartTimeMin is null or
             t_StartTimeAMPM is null then

             t_RainStartTimeError := 'Y';

          end if;

          if t_EndTimeHours is null or
             t_EndTimeMin is null or
             t_EndTimeAMPM is null then

             t_RainEndTimeError := 'Y';

          end if;

       end loop;

       if t_StartTimeError = 'Y' then
          return 'Please enter hours, minutes and AM/PM for the event start time';
       end if;

       if t_EndTimeError = 'Y' then
          return 'Please enter hours, minutes and AM/PM for the event end time';
       end if;

       if t_RainStartTimeError = 'Y' then
          return 'Please enter hours, minutes and AM/PM for the rain start time';
       end if;

       if t_RainEndTimeError = 'Y' then
          return 'Please enter hours, minutes and AM/PM for the rain end time';
       end if;

    else
-- Locations tab
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'RequiresLocation') = 'Y' then
          if api.pkg_columnQuery.VALUE (a_ObjectId, 'EventLocationAddress') is null then
             return 'Please enter a Location Address.';
          end if;
       end if;
    end if;

-- Co-Op tab
    if api.pkg_columnQuery.VALUE (a_ObjectId, 'RequiresCoOp') = 'Y' then
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'TotalCoOpLicenses') = 0 then
          return 'Please enter at least one (1) Member.';
       end if;
    end if;

-- Solicitor tab
    if api.pkg_columnQuery.VALUE (a_ObjectId, 'RequiresSolicitor') = 'Y' then
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'SolicitorName') is null then
          return 'Please select a Solicitor.';
       end if;
    end if;

-- Products tab
    if api.pkg_columnQuery.VALUE (a_ObjectId, 'RequiresProducts') = 'Y' then
       if api.pkg_columnQuery.VALUE (a_ObjectId, 'NumberOfProducts') < 1 then
          return 'At least one Product is required.';
       end if;
    end if;

-- No errors
    return null;

  end RaisePermitError;

  /*---------------------------------------------------------------------------
   * PermitCancellation
   *   Runs as procedure on constructor of rel r_ABC_PermitCancellation.
   * Handles permit cancellation
   *-------------------------------------------------------------------------*/
  procedure PermitCancellation(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_PermitObjectId                    udt_Id;
    t_CancellationObjectId              udt_Id;
    t_PermitState                       varchar2(20);
    t_relid                             number;
    t_PermitApplicationJobId            udt_Id;
    t_ProcessDefId                      udt_Id;
    t_ProcessId                         udt_Id;
    t_CancelType                        varchar2(100);
    t_Outcome                           varchar2(100);
  begin

    select
      r.PermitObjectId,
      r.CancellationObjectId
    into
      t_PermitObjectId,
      t_CancellationObjectId
    from query.r_ABC_PermitCancellation r
    where r.RelationshipId = a_ObjectId;
    t_CancelType := api.pkg_columnquery.value(t_CancellationObjectId,
        'CancellationType');

    -- Get the State of the permit
    t_PermitState := api.pkg_columnquery.value(t_PermitObjectId,'State');

    -- Update the Permit StatePriorToCancellation with the State Value
    api.pkg_columnupdate.SetValue(t_PermitObjectId, 'StatePriorToCancellation',
        t_PermitState);

    -- Set State on Permit to Cancelled or Revoked
    if t_CancelType = 'Cancellation' then
      api.pkg_columnupdate.setvalue(t_PermitObjectId,'State','Cancelled');
    elsif t_CancelType = 'Revocation' then
      api.pkg_columnupdate.setvalue(t_PermitObjectId,'State','Revoked');
    end if;

    -- Set details on cancellation object
    api.pkg_columnupdate.SetValue(t_CancellationObjectId,'CancelledByUserName',
        api.pkg_columnquery.value(t_CancellationObjectId, 'UserFormattedName2'));
    api.pkg_columnupdate.SetValue(t_CancellationObjectId,'CancelledDate',
        api.pkg_columnquery.value(t_CancellationObjectId, 'CurrentDate'));

    -- If there is an application job, enter and complete the Cancellation process
    -- This loop is added since due to the consolidation of Insignia Permits as
    -- per issue 9442,  multiple application jobs can exist.
    for j in (select r.PermitApplicationObjectId
              from query.r_abc_permitapppermit r
              where r.PermitObjectId = t_PermitObjectId
             ) loop
      t_PermitApplicationJobId := j.permitapplicationobjectid;
      if t_PermitApplicationJobId is not null then
        if api.pkg_columnquery.value(t_PermitApplicationJobId, 'StatusName') = 'APP'
            then
          if t_CancelType = 'Cancellation' then
            t_ProcessDefId := api.pkg_configquery.objectdefidforname
                ('p_ABC_CancelPermit');
            t_Outcome := 'Cancelled';
          elsif t_CancelType = 'Revocation' then
            t_ProcessDefId := api.pkg_configquery.objectdefidforname
                ('p_ABC_RevokePermit');
            t_Outcome := 'Revoked';
          end if;
          t_ProcessId := api.pkg_processupdate.new(t_PermitApplicationJobId,
              t_ProcessDefId, null, null, null, null);
          api.pkg_processupdate.complete(t_ProcessId, t_Outcome);
        end if;
      end if;
    end loop;

  end PermitCancellation;

  -----------------------------------------------------------------------------
  --  TAPClosure
  --  Runs on the TAP Closure object
  --    Closes a TAP permit and copies relationships to the specified License,
  --    or cancels products / permits under the TAP
  -- Updated By:   Elijah R.  2016 Jan 4
  -- Included CPL Submission copy
  -----------------------------------------------------------------------------
  procedure TAPClosure(
    a_ObjectId               udt_Id,
    a_AsOfDate               date
  ) is
    t_TAPPermitId            udt_Id;
    t_EndPointId             udt_Id;
    t_LicenseId              udt_Id;
    t_RelId                  udt_Id;
    t_HistoricDistEPId       udt_Id;
    t_LicTypeIsValid         varchar2(1);
    t_LicensetypeId          udt_Id;
  begin
    if api.pkg_columnquery.value(a_ObjectId, 'TAPClosed') = 'Y' then
      --Do nothing, the tap has already been closed
      null;
    else
      t_TAPPermitId := api.pkg_columnquery.value(a_ObjectId, 'PermitIdStored');
      t_LicenseId := api.pkg_columnquery.Value(a_ObjectId, 'LicenseObjectId');
      api.pkg_columnupdate.SetValue(t_TAPPermitId, 'State', 'Closed');

--      api.pkg_errors.RaiseError(-20001, t_TAPPermitId || '. ' || t_LicenseId);
      if api.pkg_columnquery.value(a_ObjectId, 'LicenseIssuedOrDenied') = 'Issued' then
        if t_LicenseId is null then
          api.pkg_errors.RaiseError(-20001, 'You must specify a license number if the License has been issued.');
        end if;
        --Find related permits and move the relationship to the license instead.
        t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'Permit');
        for i in (select r.RelationshipId, r.PermitObjectId
                    from query.r_ABC_TAPPermit r
                   where r.TAPPermitObjectId = t_TAPPermitId) loop
          t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_LicenseId, i.permitobjectid);
          api.pkg_relationshipupdate.Remove(i.relationshipid);
        end loop;


        --Find related CPL Submissions and move the relationships to the license instead.
        t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'CPLSubmission');
        for i in (select r.RelationshipId, r.CPLJobId
                    from query.r_ABC_CPLPermit r
                   where r.PermitObjectId = t_TAPPermitId) loop
          --Check if selected License allows for CPL Submissions
          if api.pkg_columnquery.value(api.pkg_columnquery.Value(t_LicenseId, 'LicenseTypeObjectId'),'CPLSubmission') = 'N' then
            api.pkg_errors.RaiseError(-20001, 'The License selected does not allow CPL Submissions');
          end if;
          --Moves the Relationship to the License if allowed
          t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, t_LicenseId, i.CPLJobId);
          api.pkg_relationshipupdate.Remove(i.relationshipid);
        end loop;

        --Copy information for Products
        --Registrant
        t_LicenseTypeId := api.pkg_columnquery.value(t_LicenseId, 'LicenseTypeObjectId');
        t_LicTypeIsValid := api.pkg_columnquery.value(t_LicenseTypeId, 'IsValidAsRegistrantDistributor');
        t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'RegistrantLic');
        for i in (select r.RelationshipId, r.ProductId
                    from query.r_Abc_Producttapregistrant r
                   where r.PermitId = t_TAPPermitId) loop
          if t_LicTypeIsValid = 'Y' then
            api.pkg_relationshipupdate.Remove(i.relationshipid);
            t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, i.productid, t_LicenseId);
          else
            api.pkg_errors.RaiseError(-20002, 'License Type is not valid as a Registrant / Distributor');
          end if;
        end loop;
        --Distributors
        t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'DistributorLic');
        t_HistoricDistEPId := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'HistoricTAPDistributor');
        for i in (select r.RelationshipId, r.ProductId
                    from query.r_ABC_ProductTAPDistributor r
                   where r.PermitId = t_TAPPermitId) loop
          if t_LicTypeIsValid = 'Y' then
            t_RelId := api.pkg_relationshipupdate.New(t_EndPointId, i.productid, t_LicenseId);
            t_RelId := api.pkg_relationshipupdate.New(t_HistoricDistEPId, i.productid, t_TAPPermitId);
            api.pkg_relationshipupdate.Remove(i.relationshipid);
          else
            api.pkg_errors.RaiseError(-20002, 'License Type is not valid as a Registrant / Distributor');
          end if;
        end loop;
      end if; -- end permit is issued

      --Run Denial/Withdrawal actions
      if api.pkg_columnquery.value(a_ObjectId, 'LicenseIssuedOrDenied') = 'Denied / Withdrawn' then
        if t_LicenseId is not null then
          api.pkg_errors.RaiseError(-20001, 'You may not specify a license number if the License has been Denied / Withdrawn.');
        end if;
        --Find related permits and close them
        for i in (select r.RelationshipId, r.PermitObjectId
                    from query.r_ABC_TAPPermit r
                   where r.TAPPermitObjectId = t_TAPPermitId) loop
          api.pkg_columnupdate.SetValue(i.permitobjectid, 'State', 'Closed');
        end loop;

        --Cancel Products
        --As Registrant
        for i in (select r.RelationshipId, r.ProductId
                    from query.r_Abc_Producttapregistrant r
                   where r.PermitId = t_TAPPermitId) loop
          api.pkg_columnupdate.SetValue(i.ProductId, 'State', 'Historical');
        end loop;
        --Distributors
        t_EndPointId := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'DistributorLic');
        t_HistoricDistEPId := api.pkg_configquery.EndPointIdForName('o_ABC_Product', 'HistoricTAPDistributor');
        for i in (select r.RelationshipId, r.ProductId
                    from query.r_ABC_ProductTAPDistributor r
                   where r.PermitId = t_TAPPermitId) loop
          t_RelId := api.pkg_relationshipupdate.New(t_HistoricDistEPId, i.productid, t_TAPPermitId);
          api.pkg_relationshipupdate.Remove(i.relationshipid);
        end loop;
      end if;
      api.pkg_columnupdate.SetValue(a_ObjectId, 'TAPClosed', 'Y');
    end if; --end check for closed TAP
  end TAPClosure;

  /*---------------------------------------------------------------------------
   * CreateDefaultConditions() -- PUBLIC
   *   Runs on Constructor of the ABC Permit
   *-------------------------------------------------------------------------*/
  procedure CreateDefaultConditions (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date,
    a_DefaultCondCreated                char
  ) is
    t_ABCNewCondId                      number;
    t_CondObjDefId                      number
        := api.pkg_ConfigQuery.ObjectDefIdForName('o_ABC_PermitSpecialConditions');
    t_CondTypeCondEndPointId            number
        := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_PermitSpecialConditions', 'ConditionType');
    t_CondTypeCondRelId                 number;
    t_ExistingSortCount                 number;
    t_PermitCondEndPointId              number
        := api.pkg_ConfigQuery.EndPointIdForName('o_ABC_Permit', 'PermitSpecialConditions');
    t_PermitCondRelId                   number;
    t_SortCount                         number;
  begin

    --since this is on post verify of a permit, we don't want it to run every time
    if (a_DefaultCondCreated = 'N') then
      -- Just in case there are already existing conditions, want to start the Sort Order correctly
      select max(ps.SortOrder)
      into t_ExistingSortCount
      from
        query.o_abc_permitspecialconditions ps
        join api.relationships r
            on ps.ObjectId = r.ToObjectId
      where r.FromObjectId = a_ObjectId
        and r.EndpointId = t_PermitCondEndPointId;

      if t_ExistingSortCount is null then
        t_ExistingSortCount := 0;
      end if;
      t_SortCount := t_ExistingSortCount + 1;

      for i in (
          select
            ct.ObjectId ctObjId,
            ct.Code, ct.ConditionText
          from
            query.o_abc_permit p
            join query.r_abc_conditiontypepermittype r
                on r.PermitTypeObjectId = p.PermitTypeObjectId
            join query.o_abc_conditiontype ct
                on ct.ObjectId = r.ConditionTypeObjectId
          where p.ObjectId = a_ObjectId
            and r.DefaultCondition = 'Y'
          order by ct.SortOrder
          ) loop
        t_ABCNewCondId := api.pkg_objectupdate.New(t_CondObjDefId);
        api.pkg_columnupdate.SetValue(t_ABCNewCondId, 'SortOrder', t_SortCount);
        api.pkg_columnupdate.SetValue(t_ABCNewCondId, 'ConditionDescription', i.conditiontext);
        t_PermitCondRelId := api.pkg_relationshipupdate.New(t_PermitCondEndPointId, a_ObjectId, t_ABCNewCondId);
        t_CondTypeCondRelId := api.pkg_relationshipupdate.New(t_CondTypeCondEndPointId, t_ABCNewCondId,i.ctobjid);

        t_SortCount := t_SortCount + 1;
      end loop;
      api.pkg_columnupdate.SetValue(a_ObjectId, 'DefaultCondCreated', 'Y');
    end if;

  end CreateDefaultConditions;

  /*---------------------------------------------------------------------------
   * CalculateDueDate()
   *  Takes the start date of a process and returns a due date based on the days
   * to respond set on the permit type
   *--------------------------------------------------------------------------*/
  procedure CalculateDueDate (
    a_ObjectId                          udt_id,
    a_AsOfDate                          date,
    a_StartDateColumn                   varchar2,
    a_DaysToAddColumn                   varchar2,
    a_DueDateColumn                     varchar2
  ) is
  t_StartDate                           date;
  t_EndDate                             date;
  t_DaysToAdd                           integer;
  t_WorkWeeks                           integer;
  t_WorkDays                            integer;
  t_WeekendWrap                         char(1);
  t_JobId                               udt_id;
  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    t_StartDate := trunc(nvl(api.pkg_columnquery.DateValue(t_JobId, a_StartDateColumn), sysdate));
    t_DaysToAdd := api.pkg_columnquery.NumericValue(t_JobId, a_DaysToAddColumn);

    if t_StartDate is null or t_DaysToAdd is null then
      return;
    end if;
    t_EndDate := trunc(extension.pkg_utils.AddWorkingDays(t_StartDate, t_DaysToAdd));
    api.pkg_columnupdate.SetValue(a_ObjectId, a_DueDateColumn, t_EndDate);

  end CalculateDueDate;

  /*---------------------------------------------------------------------------
   * SetMuniPoliceDocType()
   *  Takes the start date of a process and returns a due date based on the days
   * to respond set on the permit type
   *--------------------------------------------------------------------------*/
  procedure SetMuniPoliceDocType (
    a_ObjectId                          udt_id,
    a_AsOfDate                          date,
    a_DocTypeName                       varchar2
  ) is

    t_DocObjTypeId                      udt_id;
    t_DocObjIdList                      udt_idList;
    t_DocTypeObjectId                   udt_id;
    t_EndpointId                        udt_id;
    t_MuniProcEndPoint                  udt_id;
    t_PoliceProcEndPoint                udt_id;
    t_RelId                             udt_id;

  begin

    t_EndpointId := api.pkg_configquery.EndPointIdForName('d_ElectronicDocument', 'DocumentType');
    t_MuniProcEndPoint := api.pkg_configquery.EndPointIdForName('p_ABC_MunicipalityReview',
        'Documents');
    t_PoliceProcEndPoint := api.pkg_configquery.EndPointIdForName('p_ABC_PoliceReview',
        'Documents');
    select distinct r.ToObjectId
    bulk collect into t_DocObjIdList
    from api.relationships r
    where r.EndPointId in (t_MuniProcEndPoint, t_PoliceProcEndPoint)
    and r.fromobjectid = a_ObjectId;

    select dt.ObjectId
    into t_DocTypeObjectId
    from query.o_abc_documenttype dt
    where dt.Name = a_DocTypeName;

    for i in 1..t_DocObjIdList.count loop
      begin
      select re.RelationshipId
      into t_RelId
      from api.relationships re
      where re.FromObjectId = t_DocObjIdList(i)
      and re.EndPointId = t_EndpointId;


      exception when no_data_found then null;
      end;

      if t_RelId is not null then
        api.pkg_relationshipupdate.Remove(t_RelId);
      end if;

      dbms_output.put_line( i);
         t_RelId := api.Pkg_Relationshipupdate.New(t_EndpointId, t_DocObjIdList(i), t_DocTypeObjectId);
    end loop;
  end SetMuniPoliceDocType;

  /*---------------------------------------------------------------------------
   * BuyerPermitRelPostVerifyWrapper()
   *  Post verify check on r_BuyersPermit
   *--------------------------------------------------------------------------*/
  procedure BuyerPermitRelPostVerifyWrapper (
    a_ObjectId            udt_Id,
    a_AsOfDate            date
  ) is
    t_PermitType          varchar2(20);
    t_PermitState         varchar2(20);
    t_LabelType           varchar2(4000);
  begin
    select api.pkg_columnquery.Value(bp.BuyersPermit, 'permittypecode'),
        api.pkg_columnquery.Value(bp.PermitToBuyers, 'state'),
        api.pkg_columnquery.Value(bp.BuyersPermit, 'LicenseTapInformationLabel')
    into t_PermitType, t_PermitState, t_LabelType
    from query.r_Buyerspermit bp
    where bp.RelationshipId = a_ObjectId;

    if t_PermitType = 'RR' and t_PermitState <> 'Active' then
      api.pkg_errors.RaiseError(-20000,'The selected '||t_LabelType||'''s permit must be active.');
    end if;

  end BuyerPermitRelPostVerifyWrapper;

  /*---------------------------------------------------------------------------
   * CopyDefaultAttachmentToLicenseLetter()
   *   Copies system settings detail to license letter
   *-------------------------------------------------------------------------*/
  procedure CopyDefaultAttachmentToPermitLetter(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  ) is
    t_DefaultAttach                     varchar2(1);
    t_PermitLetterId                    udt_Id;
  begin

    select d.DefaultAttachment, r.PermitLetterId
    into t_DefaultAttach, t_PermitLetterId
    from query.r_ABC_PermLetterWordTemplate r
    join query.d_wordinterfacetemplate d
      on r.WordMergeTemplateId = d.ObjectId
    where r.RelationshipId = a_ObjectId;

    api.pkg_ColumnUpdate.SetValue(t_PermitLetterId, 'AttachToEmail', t_DefaultAttach);

  end CopyDefaultAttachmentToPermitLetter;

end pkg_ABC_Permit;
/
