create or replace package abc.pkg_ABC_ProceduralLookup is

  -- Author  : JOHN.PETKAU
  -- Created : 4/14/2011 11:21:25 AM
  -- Purpose : Procedural Lookups for POSSE ABC

  -- Public type declarations
  subtype udt_ID is api.pkg_Definition.udt_ID;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_ObjectList is api.udt_ObjectList;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;

  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --<VariableName> <Datatype>;

  -- Public function and procedure declarations

  /**************************************************************************************************
  *  PrimaryLicenseTypes()
  Used on License to filter the drop-down for the Active and Primary License Types
  ***************************************************************************************************/
  procedure PrimaryLicenseTypes(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  PrimaryLicenseTypesWithState()
  *  Primary Licenses with the Issuing authority of State (Used for Wholesale Report)
  ***************************************************************************************************/
  procedure PrimaryLicenseTypesWithState (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  PrimaryLicenseTypesWithMuni()
  *  Primary Licenses with the Issuing authority of Municipality
  ***************************************************************************************************/
  procedure PrimaryLicenseTypesWithMuni (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  GLAccountCategories()
  ***************************************************************************************************/
  procedure GLAccountCategories (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

 /**************************************************************************************************
  *  LicensesForPublicPR()
  ***************************************************************************************************/
  procedure LicensesForPublicPR (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  LicensesForInternalPR()
  ***************************************************************************************************/
  procedure LicensesForInternalPR (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

 /**************************************************************************************************
  *  ActiveComplaintTypes()
  ***************************************************************************************************/
  procedure ActiveCOmplaintTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ActiveEstablishmentTypes()
  ***************************************************************************************************/
  procedure ActiveEstablishmentTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

   /**************************************************************************************************
  *  ActiveEventTypes()
  ***************************************************************************************************/
  procedure ActiveEventTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ActiveGLAccounts()
  ***************************************************************************************************/
  procedure ActiveGLAccounts(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

   /**************************************************************************************************
  *  ActiveInspectionTypes()
  ***************************************************************************************************/
  procedure ActiveInspectionTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

/**************************************************************************************************
  *  ActiveInspectionResultTypes()
  ***************************************************************************************************/
  procedure ActiveInspectionResultTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

 /**************************************************************************************************
  *  ActiveLegalEntityTypes()
  ***************************************************************************************************/
  procedure ActiveLegalEntityTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );
  
  /*---------------------------------------------------------------------------
   * ActiveAndNotOtherCountries() -- PUBLIC
   *   This gives a list of Countries that are active and do not have the other
   * check box.
   *-------------------------------------------------------------------------*/
  procedure ActiveAndNotOtherCountries (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

/**************************************************************************************************
  *  ActiveRiskFactors()
  ***************************************************************************************************/
  procedure ActiveRiskFactors (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

 /**************************************************************************************************
  *  ActiveViolations()
  ***************************************************************************************************/
  procedure ActiveViolations(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

 /**************************************************************************************************
  *  ActiveUsers()
  ***************************************************************************************************/
  procedure ActiveUsers (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ActiveProductTypes()
  ***************************************************************************************************/
  procedure ActiveProductTypes(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ActivePermitTypes()
  ***************************************************************************************************/
  procedure ActivePermitTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ABCCPLReviewer()
  ***************************************************************************************************/
  procedure ABCCPLReviewer(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ABCClerkRole()
  ***************************************************************************************************/
  procedure ABCClerkRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

/*  \**************************************************************************************************
  *  ABCRevenueClerkRole()
  ***************************************************************************************************\
  procedure ABCRevenueClerkRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );*/

  /**************************************************************************************************
  *  ABCEnforceClerkRole()
  ***************************************************************************************************/
  procedure ABCEnforceClerkRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ABCSupervisorRole()
  ***************************************************************************************************/
  procedure ABCSupervisorRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ABCEnforceSupervisorRole()
  ***************************************************************************************************/
  procedure ABCEnforceSupervisorRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ABCManagerRole()
  ***************************************************************************************************/
  procedure ABCManagerRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ABCLegalRole()
  ***************************************************************************************************/
  procedure ABCLegalRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  ABCInspectorRole()
  ***************************************************************************************************/
  procedure ABCInspectorRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  DefaultClerksForRegion()
  Used on Region to filter the drop-down for the Default Users
  ***************************************************************************************************/
  procedure DefaultClerksForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  DefaultEnforceClerksForRegion()
  Used on Region to filter the drop-down for the Default Users
  ***************************************************************************************************/
  procedure DefaultEnforceClerksForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  DefaultSupervisorsForRegion()
  Used on Region to filter the drop-down for the Default Users
  ***************************************************************************************************/
  procedure DefaultSupervisorsForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  DefaultEnforceSupervisorsForRegion()
  Used on Region to filter the drop-down for the Default Users
  ***************************************************************************************************/
  procedure DefaultEnfSupervisorsForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  DefaultManagersForRegion()
  Used on Region to filter the drop-down for the Default Users
  ***************************************************************************************************/
  procedure DefaultManagersForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  DefaultInspectorsForRegion()
  Used on Region to filter the drop-down for the Default Users
  ***************************************************************************************************/
  procedure DefaultInspectorsForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  DefaultLegalsForRegion()
  Used on Region to filter the drop-down for the Default Users
  ***************************************************************************************************/
  procedure DefaultLegalsForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  );

  /**************************************************************************************************
  *  NonSpecialEventLicenseTypes()
  *   Returns a list of License Types that are not special events.
  ***************************************************************************************************/
  procedure NonSpecialEventLicenseTypes(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                           out udt_ObjectList
  );

  /**************************************************************************************************
  *  SpecialEventLicenseTypes()
  *   Returns a list of License Types that are special events.
  ***************************************************************************************************/
  procedure SpecialEventLicenseTypes(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                           out udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * OutstandingFeeLegalEntities()
   *   This gives a list of legal entities with outstanding fees linked to the
   * user.
   *-------------------------------------------------------------------------*/
   procedure OutstandingFeeLegalEntities (
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  *  ActivePaymentMethod()
  *  This procedure looks at the active details in the name deatil for the o_paymentmethod object
  *  then fileters the objects based on if the detail is active.
  ***************************************************************************************************/
   procedure ActivePaymentMethod(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                           out udt_ObjectList
  );

  /**************************************************************************************************
  * GLAccountsForCategory()
  *  Finds License Types with one or more associated fees
  ***************************************************************************************************/
  procedure GLAccountsForCategory(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  * AccusationGLAccounts()
  *  Finds valid GL Accounts for use on Manual fee jobs associated with Accusations
  ***************************************************************************************************/
  procedure AccusationGLAccounts(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

    /**************************************************************************************************
  * SelectedCPLSubmissionPeriod()
  *  For use to retreive Submission Periods set up on the admin site that are active.
  ***************************************************************************************************/
  procedure SelectedCPLSubmissionPeriod(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  * LegalUsers()
  *  Finds Users with the Legal Role
  ***************************************************************************************************/
  procedure LegalUsers(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  *  ActiveLicenseWarnings()
  *  This procedure looks for active license warnings that are defined on the Admin site.
  ***************************************************************************************************/
   procedure ActiveLicenseWarnings(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                           out udt_ObjectList
  );

  /**************************************************************************************************
  * LegalAppealProcessor()
  *  Finds Users in the Appeal Processor Access Group
  ***************************************************************************************************/
  procedure LegalAppealProcessor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  * LegalAppealFineClerk()
  *  Finds Users in the Appeal Fine Clerk Access Group
  ***************************************************************************************************/
  procedure LegalAppealFineClerk(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  * LegalDeputyAttorneyGeneral()
  *  Finds Users in the Deputy Attorney General Access Group
  ***************************************************************************************************/
  procedure LegalDeputyAttorneyGeneral(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  * LegalPetitionProcessor()
  *  Finds Users in the Petition Processor Access Group
  ***************************************************************************************************/
  procedure LegalPetitionProcessor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  * LegalPetitionProcessor()
  *  Finds Users in the Petition Processor Access Group
  ***************************************************************************************************/
  procedure ConditionProcessor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  * LegalPetitionSupervisor()
  *  Finds Users in the Petition Supervisor Access Group
  ***************************************************************************************************/
  procedure LegalPetitionSupervisor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
  * LicensingAdminAccessGroup()
  *  Finds Users in the Licensing Administration Access Group
  *  Used in the Batch Renewal Notification Default Clerk on the Admin site
  ***************************************************************************************************/
  procedure LicensingAdminAccessGroup(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /*************************************************************************************
   * LicensesForOnlinePermitApp
   * Returns licenses available to the permit application based on chosen legal entity and
   * the available license types selected on the permit type
   *************************************************************************************/
  procedure LicensesForOnlinePermitApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /*************************************************************************************
   * PermitsForOnlinePermitApp
   * Returns permits available to the permit application based on chosen legal entity and
   * the available permit types selected on the permit type
   *************************************************************************************/
  procedure PermitsForOnlinePermitApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

   /*************************************************************************************
   * PermitsForPermitee
   * Returns the Permits for the Permitee Passed in. Runs the procedural Rel
   *************************************************************************************/

  function PermitsForPermitee
  (a_ObjectId    udt_Id,
   a_EndPoint     udt_Id) return  api.udt_ObjectList;

  /*************************************************************************************
   * TAPPermitsForOnlinePermitApp
   * Returns TAP permits available to the permit application based on chosen legal entity and
   * the available permit types selected on the permit type
   *************************************************************************************/
  procedure TAPPermitsForOnlinePermitApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /*************************************************************************************
   * PermitTypes
   * Returns active Permit Types available to the public permit application.
   *************************************************************************************/
  procedure PermitTypes(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /*************************************************************************************
   * TAPPermitsForOnlinePermitApp
   * Returns TAP permits available to the permit application based on chosen legal entity and
   * the available permit types selected on the permit type
   *************************************************************************************/
  procedure TAPPermitsForPublicPR(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /*---------------------------------------------------------------------------
   * ActiveAmendmentTypes()
   *   Returns active Amendment Types available to the public Amendment
   * application.
   *-------------------------------------------------------------------------*/
  procedure ActiveAmendmentTypes(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /*************************************************************************************
   * LicensesForOnlineLicenseApp
   * Returns licenses available to the license application based on chosen license type (24)
   * and if the the available license types for this users can apply for this type
   *************************************************************************************/
  procedure LicensesForOnlineLicenseApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /*************************************************************************************
   * InsigniaPermitForOnlPermitApp
   * Returns Insignia Permits available to the permit application based on chosen Permit
   * Type
   *************************************************************************************/
  procedure InsigniaPermitForOnlPermitApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

  /**************************************************************************************************
   * PetitionSupervisor()
   *  Finds Users in the Petition Supervisor Access Group
  ***************************************************************************************************/
  procedure PetitionSupervisor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  );

end pkg_ABC_ProceduralLookup;
/
create or replace package body abc.pkg_ABC_ProceduralLookup is

  /**************************************************************************************************
  *  PrimaryLicenseTypes()
  ***************************************************************************************************/
  procedure PrimaryLicenseTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_licensetype
     where ObjectDefTypeId = 1
       and Active = 'Y'
       and IsSecondaryOnly = 'N';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end PrimaryLicenseTypes;

  /**************************************************************************************************
  *  PrimaryLicenseTypesWithState()
  *  Primary Licenses with the Issuing authority of State
  ***************************************************************************************************/
  procedure PrimaryLicenseTypesWithState (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_licensetype
     where ObjectDefTypeId = 1
       and Active = 'Y'
       and IsSecondaryOnly = 'N'
       and IssuingAuthority = 'State';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end PrimaryLicenseTypesWithState;

  /**************************************************************************************************
  *  PrimaryLicenseTypesWithMuni()
  *  Primary Licenses with the Issuing authority of Municipality
  ***************************************************************************************************/
  procedure PrimaryLicenseTypesWithMuni (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_licensetype
     where ObjectDefTypeId = 1
       and Active = 'Y'
       and IsSecondaryOnly = 'N'
       and IssuingAuthority = 'Municipality';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end PrimaryLicenseTypesWithMuni;

  /**************************************************************************************************
  *  GLAccountCategories()
  ***************************************************************************************************/
  procedure GLAccountCategories (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin

    select t.ObjectId --+CARDINALITY(t 20)
     bulk collect into t_ObjectList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_ABC_GLAccountCategory', 'Active', 'Y')as api.udt_ObjectList)) t;

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end GLAccountCategories;

 /**************************************************************************************************
  *  LicensesForPublicPR()
  ***************************************************************************************************/
  procedure LicensesForPublicPR (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
    t_LegalEntityObjectId    udt_Id;
  begin

     begin
        select p.LegalEntityObjectId
          into t_LegalEntityObjectId
         from query.j_Abc_Prapplication p
        where p.ObjectId = a_ObjectId;

        select l.LicenseObjectId
          bulk collect into t_ObjectList
          from query.r_ABC_LicenseLicenseeLE l
         where l.LegalEntityObjectId = t_LegalEntityObjectId
           and api.pkg_columnquery.value(l.LicenseObjectId, 'State') = 'Active'
           and api.pkg_columnquery.value(l.LicenseObjectId, 'LicenseNumber') not like '99%'
           and api.pkg_columnquery.value(l.LicenseObjectId, 'IsValidAsNewRegistrant') = 'Y';

      exception when no_data_found then
         null;
       end;

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);

  end LicensesForPublicPR;

 /**************************************************************************************************
  *  LicensesForInternalPR()
  ***************************************************************************************************/
  procedure LicensesForInternalPR (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
    t_LegalEntityObjectId    udt_Id;

  begin

    begin
        select p.LegalEntityObjectId
          into t_LegalEntityObjectId
         from query.j_Abc_Prapplication p
        where p.ObjectId = a_ObjectId;

        select l.LicenseObjectId
          bulk collect into t_ObjectList
          from query.r_ABC_LicenseLicenseeLE l
         where l.LegalEntityObjectId = t_LegalEntityObjectId
           and api.pkg_columnquery.value(l.LicenseObjectId, 'State') = 'Active'
           and api.pkg_columnquery.value(l.LicenseObjectId, 'IsValidAsNewRegistrant') = 'Y';

     exception when no_data_found then
       null;
     end;

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);

  end LicensesForInternalPR;

 /**************************************************************************************************
  *  ActiveComplaintTypes()
  ***************************************************************************************************/
  procedure ActiveComplaintTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_complainttype
     where ObjectDefTypeId = 1
       and Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveComplaintTypes;

  /**************************************************************************************************
  *  ActiveEstablishmentTypes()
  ***************************************************************************************************/
  procedure ActiveEstablishmentTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_establishmenttype
     where ObjectDefTypeId = 1
       and Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveEstablishmentTypes;

/**************************************************************************************************
  *  ActiveEventTypes()
  ***************************************************************************************************/
  procedure ActiveEventTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_eventtype
     where ObjectDefTypeId = 1
       and Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
 end ActiveEventTypes;

  /**************************************************************************************************
  *  ActiveGLAccounts()
  ***************************************************************************************************/
  procedure ActiveGLAccounts(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin

   t_ObjectList := api.pkg_search.ObjectsByColumnValue('o_GLAccount','Active','Y');

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveGLAccounts;
 /**************************************************************************************************
  *  ActiveInspectionTypes()
  ***************************************************************************************************/
  procedure ActiveInspectionTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_InspectionType
     where ObjectDefTypeId = 1
       and Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveInspectionTypes;

 /**************************************************************************************************
  *  ActiveInspectionResultTypes()
  ***************************************************************************************************/
  procedure ActiveInspectionResultTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_InspectionResultType
     where ObjectDefTypeId = 1
       and Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveInspectionResultTypes;

 /**************************************************************************************************
  *  ActiveLegalEntityTypes()
  ***************************************************************************************************/
  procedure ActiveLegalEntityTypes (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_legalentitytype
     where ObjectDefTypeId = 1
       and Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveLegalEntityTypes;

  /*---------------------------------------------------------------------------
   * ActiveAndNotOtherCountries() -- PUBLIC
   *   This gives a list of Countries that are active and do not have the other
   * check box.
   *-------------------------------------------------------------------------*/
  procedure ActiveAndNotOtherCountries (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_country
     where ObjectDefTypeId = 1
       and Active = 'Y'
       and Other = 'N';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveAndNotOtherCountries;

  /**************************************************************************************************
  *  ActiveRiskFactors()
  ***************************************************************************************************/
  procedure ActiveRiskFactors (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_riskfactor
     where ObjectDefTypeId = 1
       and Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveRiskFactors;


   /**************************************************************************************************
  *  ActiveUsers()
  ***************************************************************************************************/
  procedure ActiveUsers (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select x.objectid bulk collect into t_ObjectList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('u_Users', 'UserType', 'Internal') as api.udt_ObjectList)) x
      join query.u_users u on u.ObjectId = x.objectid
     where u.Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveUsers;

/**************************************************************************************************
  *  ActiveViolations()
  ***************************************************************************************************/
  procedure ActiveViolations(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_ABC_ViolationType
     where Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveViolations;

  /**************************************************************************************************
  *  ActiveProductTypes()
  ***************************************************************************************************/
  procedure ActiveProductTypes(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_ABC_ProductType
     where Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActiveProductTypes;

  /**************************************************************************************************
  *  ActivePermitTypes()
  ***************************************************************************************************/
  procedure ActivePermitTypes(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_ObjectList             udt_IdList;
  begin
    select objectid bulk collect into t_ObjectList
      from query.o_abc_PermitType
     where ObjectDefTypeId = 1
       and Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end ActivePermitTypes;

  /**************************************************************************************************
  *  ABCCPLReviewer()
  ***************************************************************************************************/
  procedure ABCCPLReviewer(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
    t_AccessGroupId          number;
  begin

    begin
       select ag.AccessGroupId
         into t_AccessGroupId
         from api.accessgroups ag
        where ag.Description = 'CPL Reviewer';
    exception
       when no_data_found then
          api.pkg_errors.RaiseError(-20000,'No Access Group for CPL Reviewer has been set up. Please correct before continuing.');
    end;

    select /*+cardinality(x 1)*/distinct(agu.UserId) bulk collect into t_IdList
      from api.accessgroupusers agu
      join api.accessgroups ag on ag.AccessGroupId = agu.AccessGroupId
     where agu.AccessGroupId = t_AccessGroupId
       and api.pkg_columnquery.Value(agu.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCCPLReviewer;

  /**************************************************************************************************
  *  ABCClerkRole()
  ***************************************************************************************************/
  procedure ABCClerkRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCClerkRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur
        on ur.RoleObjectId = x.objectid
     where api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCClerkRole;


/*  \**************************************************************************************************
  *  ABCRevenueClerkRole()
  ***************************************************************************************************\
  procedure ABCRevenueClerkRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select \*+cardinality(x 1)*\distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCRevenueClerkRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur
        on ur.RoleObjectId = x.objectid
     where api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCRevenueClerkRole;*/


  /**************************************************************************************************
  *  ABCEnforceClerkRole()
  ***************************************************************************************************/
  procedure ABCEnforceClerkRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCEnforceClerkRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur
        on ur.RoleObjectId = x.objectid
     where api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCEnforceClerkRole;

  /**************************************************************************************************
  *  ABCSupervisorRole()
  ***************************************************************************************************/
  procedure ABCSupervisorRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCSupervisorRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur
        on ur.RoleObjectId = x.objectid
     where api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCSupervisorRole;

  /**************************************************************************************************
  *  ABCEnforceSupervisorRole()
  ***************************************************************************************************/
  procedure ABCEnforceSupervisorRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCEnforceSupervisorRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur
        on ur.RoleObjectId = x.objectid
     where api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCEnforceSupervisorRole;

  /**************************************************************************************************
  *  ABCManagerRole()
  ***************************************************************************************************/
  procedure ABCManagerRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCManagerRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur
        on ur.RoleObjectId = x.objectid
     where api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCManagerRole;

  /**************************************************************************************************
  *  ABCLegalRole()
  ***************************************************************************************************/
  procedure ABCLegalRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCLegalRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur
        on ur.RoleObjectId = x.objectid
     where api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCLegalRole;

  /**************************************************************************************************
  *  ABCInspectorRole()
  ***************************************************************************************************/
  procedure ABCInspectorRole(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCInspectorRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur
        on ur.RoleObjectId = x.objectid
     where api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end ABCInspectorRole;

  /**************************************************************************************************
  *  DefaultClerksForRegion()
  ***************************************************************************************************/
  procedure DefaultClerksForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCClerkRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur on ur.RoleObjectId = x.objectid
      join query.r_UserOffice o on o.UserObjectId = ur.UserId
      join query.r_Abc_Regionoffice ro on ro.OfficeObjectId = o.OfficeObjectId
    where ro.RegionObjectId = a_ObjectId
      and api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end DefaultClerksForRegion;

  /**************************************************************************************************
  *  DefaultEnforceClerksForRegion()
  ***************************************************************************************************/
  procedure DefaultEnforceClerksForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCEnforceClerkRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur on ur.RoleObjectId = x.objectid
      join query.r_UserOffice o on o.UserObjectId = ur.UserId
      join query.r_Abc_Regionoffice ro on ro.OfficeObjectId = o.OfficeObjectId
    where ro.RegionObjectId = a_ObjectId
      and api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end DefaultEnforceClerksForRegion;

  /**************************************************************************************************
  *  DefaultSupervisorsForRegion()
  ***************************************************************************************************/
  procedure DefaultSupervisorsForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCSupervisorRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur on ur.RoleObjectId = x.objectid
      join query.r_UserOffice o on o.UserObjectId = ur.UserId
      join query.r_Abc_Regionoffice ro on ro.OfficeObjectId = o.OfficeObjectId
    where ro.RegionObjectId = a_ObjectId
      and api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end DefaultSupervisorsForRegion;

  /**************************************************************************************************
  *  DefaultEnforceSupervisorsForRegion()
  ***************************************************************************************************/
  procedure DefaultEnfSupervisorsForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCEnforceSupervisorRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur on ur.RoleObjectId = x.objectid
      join query.r_UserOffice o on o.UserObjectId = ur.UserId
      join query.r_Abc_Regionoffice ro on ro.OfficeObjectId = o.OfficeObjectId
    where ro.RegionObjectId = a_ObjectId
      and api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end DefaultEnfSupervisorsForRegion;

  /**************************************************************************************************
  *  DefaultManagersForRegion()
  ***************************************************************************************************/
  procedure DefaultManagersForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCManagerRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur on ur.RoleObjectId = x.objectid
    where  api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end DefaultManagersForRegion;

  /**************************************************************************************************
  *  DefaultInspectorsForRegion()
  ***************************************************************************************************/
  procedure DefaultInspectorsForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCInspectorRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur on ur.RoleObjectId = x.objectid
      join query.r_UserOffice o on o.UserObjectId = ur.UserId
      join query.r_Abc_Regionoffice ro on ro.OfficeObjectId = o.OfficeObjectId
    where ro.RegionObjectId = a_ObjectId
      and api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end DefaultInspectorsForRegion;

  /**************************************************************************************************
  *  DefaultLegalsForRegion()
  ***************************************************************************************************/
  procedure DefaultLegalsForRegion(
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_IdList                 udt_IdList;
  begin
    select /*+cardinality(x 1)*/distinct(ur.UserId) bulk collect into t_IdList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_Role', 'dup_IsABCLegalRole', 'Y') as api.udt_ObjectList)) x
      join query.r_userrole ur on ur.RoleObjectId = x.objectid
      join query.r_UserOffice o on o.UserObjectId = ur.UserId
      join query.r_ABC_RegionOffice ro on ro.OfficeObjectId = o.OfficeObjectId
    where ro.RegionObjectId = a_ObjectId
      and api.pkg_columnquery.Value(ur.UserId, 'Active') = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end DefaultLegalsForRegion;


  /**************************************************************************************************
  *  LicenseTypes()
  *   Returns a list of License Types based on whether they are special events.
  ***************************************************************************************************/
  function LicenseTypes(
    a_IsSpecialEvent                    char
  ) return udt_ObjectList is
    t_IdList                            udt_IdList;

  begin
    select o.ObjectId
      bulk collect into t_IdList
      from query.o_ABC_LicenseType o
      where ObjectDefTypeId = 1
        and o.IsSpecialEventLicense = a_IsSpecialEvent
        and o.Active = 'Y'
        and IsSecondaryOnly = 'N'
        and AvailableOnline = 'Y';

    return extension.pkg_Utils.ConvertToObjectList(t_IdList);
  end LicenseTypes;


  /**************************************************************************************************
  *  NonSpecialEventLicenseTypes()
  *   Returns a list of License Types that are not special events.
  ***************************************************************************************************/
  procedure NonSpecialEventLicenseTypes(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                           out udt_ObjectList
  ) is
  begin
    a_Objects := LicenseTypes('N');
  end NonSpecialEventLicenseTypes;

  /*---------------------------------------------------------------------------
   * OutstandingFeeLegalEntities() -- PUBLIC
   *   This gives a list of legal entities with outstanding fees linked to the
   * user.
   *-------------------------------------------------------------------------*/
  procedure OutstandingFeeLegalEntities (
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_Balance                           udt_NumberList;
    t_EffectiveUserId                   udt_Id := api.pkg_SecurityQuery.EffectiveUserId();
    t_LEObjectIdList                    udt_IdList;
    t_FeeList                           api.udt_ObjectList;
  begin
    a_Objects := api.udt_ObjectList();
    t_FeeList := api.udt_ObjectList();
    
    t_LEObjectIdList := api.pkg_Search.ObjectsByRelColumnValue('o_ABC_LegalEntity',
        'OnlineUser', 'ObjectId', a_ObjectId);
    for i in 1..t_LEObjectIdList.count() loop
      abc.pkg_ABC_ProceduralRels.OutstandingFeesForLegalEntity(t_LEObjectIdList(i), null, t_FeeList);
      if t_FeeList.count > 0 then
        a_Objects.Extend(1);
        a_Objects(a_Objects.count()) := api.udt_Object(t_LEObjectIdList(i));
      end if;
    end loop;

  end OutstandingFeeLegalEntities;


  /**************************************************************************************************
  *  SpecialEventLicenseTypes()
  *   Returns a list of License Types that are special events.
  ***************************************************************************************************/
  procedure SpecialEventLicenseTypes(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                           out udt_ObjectList
  ) is
  begin
    a_Objects := LicenseTypes('Y');
  end SpecialEventLicenseTypes;

  /**************************************************************************************************
  *  ActivePaymentMethod()
  *  This procedure looks at the active details in the name deatil for the o_paymentmethod object
  *  then fileters the objects based on if the detail is active.
  ***************************************************************************************************/
   procedure ActivePaymentMethod(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                           out udt_ObjectList
  ) is
    t_ObjectNameList                    udt_IdList;
    begin

    select pm.objectid
      bulk collect into t_ObjectNameList
      from query.o_paymentmethod pm
     where pm.ActiveFlag = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectNameList);

end ActivePaymentMethod;

  /**************************************************************************************************
  * GLAccountsForCategory()
  *  Finds License Types with one or more associated fees
  ***************************************************************************************************/
  procedure GLAccountsForCategory(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_GLAccountCategories               varchar2(4000);
    t_GLAccountCategoryList             api.pkg_Definition.udt_StringList;
    t_GLAccountList                     udt_IdList;
    t_FullAccountList                   udt_IdList;
  begin
    -- Parse list
    t_GLAccountCategories := api.pkg_ColumnQuery.Value(a_ObjectId, 'GLAccountCategory');
    t_GLAccountCategoryList := extension.pkg_Utils.Split(t_GLAccountCategories, ', ');

    -- Obtain list of GLAccounts per GLAccountCategory
    a_Objects := api.udt_ObjectList();
    for i in 1..t_GLAccountCategoryList.count loop
      select gl.objectid
        bulk collect into t_GLAccountList
        from query.o_glaccount gl
        join api.relationships r
          on r.FromObjectId = gl.objectid
        join query.o_ABC_GLAccountCategory glc
          on r.ToObjectId = glc.objectid
       where glc.Category = t_GLAccountCategoryList(i);

       -- append the per category list to a final output list
       extension.pkg_collectionutils.Append(t_FullAccountList,t_GLAccountList);
    end loop;

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_FullAccountList);

  exception
    when no_data_found then
      -- We just ignore errors, it's null anyways.
      null;
  end GLAccountsForCategory;

    /**************************************************************************************************
  * AccusationGLAccounts()
  *  Finds valid GL Accounts for use on Manual fee jobs associated with Accusations
  ***************************************************************************************************/
  procedure AccusationGLAccounts(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_GLAccountList                  udt_IdList;

  begin
    select t.ObjectId --+CARDINALITY(t 20)
     bulk collect into t_GLAccountList
      from table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_GLAccount', 'Active', 'Y')as api.udt_ObjectList)) t
      join table(cast(api.pkg_simplesearch.castableobjectsbyindex('o_GLAccount', 'AvailableOnAccusationJob', 'Y')as api.udt_ObjectList)) a
        on t.objectid = a.objectid;

  a_Objects := extension.pkg_Utils.ConvertToObjectList(t_GLAccountList);

  end;

    /**************************************************************************************************
  * SelectedCPLSubmissionPeriod()
  *  For use to retreive Submission Periods set up on the admin site that are active.
  ***************************************************************************************************/
  procedure SelectedCPLSubmissionPeriod(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is

  begin


    for c in (select t.ObjectId, t.Active
      from query.o_Abc_Cplsubmissionperiod t) loop
         if c.Active = 'Y' then
           a_Objects.Extend();
           a_Objects(a_Objects.count):= api.udt_object(c.objectid);
         end if;
     end loop;

  end SelectedCPLSubmissionPeriod;

  /**************************************************************************************************
  * LegalUsers()
  *  Finds Users with the Legal Role
  ***************************************************************************************************/
  procedure LegalUsers(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
  begin
    select api.udt_Object(ur.UserId)
      bulk collect into a_Objects
      from query.r_UserRole ur
     where api.pkg_ColumnQuery.Value(ur.RoleObjectId, 'Name') = 'Legal';
  end;

  /**************************************************************************************************
  *  ActiveLicenseWarnings()
  *  This procedure looks for active license warnings that are defined on the Admin site.
  ***************************************************************************************************/
   procedure ActiveLicenseWarnings(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                           out udt_ObjectList
   ) is
    t_LicenseWarningType                udt_IdList;
    begin

    select lw.objectid
      bulk collect into t_LicenseWarningType
      from query.o_Abc_Licensewarningtype lw
     where lw.Active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_LicenseWarningType);

   end ActiveLicenseWarnings;

  /**************************************************************************************************
  * LegalAppealProcessor()
  *  Finds Users in the Appeal Processor Access Group
  ***************************************************************************************************/
  procedure LegalAppealProcessor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_AppealProcessor                   udt_IdList;
  begin
    select agu.UserId
      bulk collect into t_AppealProcessor
      from api.AccessGroupUsers agu
      join api.AccessGroups ag
        on agu.AccessGroupId = ag.AccessGroupId
     where ag.Description = 'Appeal Processor';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_AppealProcessor);

  end LegalAppealProcessor;

  /**************************************************************************************************
  * LegalAppealFineClerk()
  *  Finds Users in the Appeal Fine Clerk Access Group
  ***************************************************************************************************/
  procedure LegalAppealFineClerk(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
  begin
    select api.udt_Object(agu.UserId)
      bulk collect into a_Objects
      from api.AccessGroupUsers agu
      join api.AccessGroups ag
        on agu.AccessGroupId = ag.AccessGroupId
     where ag.Description = 'Appeal Fine Clerk';
  end LegalAppealFineClerk;

  /**************************************************************************************************
  * LegalDeputyAttorneyGeneral()
  *  Finds Users in the Deputy Attorney General Access Group
  ***************************************************************************************************/
  procedure LegalDeputyAttorneyGeneral(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
  begin

    select api.udt_Object(u.UserId)
      bulk collect into a_Objects
      from api.AccessGroupUsers agu
      join api.AccessGroups ag
        on agu.AccessGroupId = ag.AccessGroupId
      join api.Users u
        on u.UserId = agu.UserId
     where ag.Description = 'Deputy Attorney General'
       and u.Active = 'Y';

  end LegalDeputyAttorneyGeneral;

  /**************************************************************************************************
  * LegalPetitionProcessor()
  *  Finds Users in the Petition Processor Access Group
  ***************************************************************************************************/
  procedure LegalPetitionProcessor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
  begin

    select api.udt_Object(u.UserId)
      bulk collect into a_Objects
      from api.AccessGroupUsers agu
      join api.AccessGroups ag
        on agu.AccessGroupId = ag.AccessGroupId
      join api.Users u
        on u.UserId = agu.UserId
     where ag.Description = 'Petition Processor'
       and u.Active = 'Y';

  end LegalPetitionProcessor;

  /**************************************************************************************************
  * LegalDeputyAttorneyGeneral()
  *  Finds Users in the Deputy Attorney General Access Group
  ***************************************************************************************************/
  procedure ConditionProcessor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
  begin
    select api.udt_Object(agu.UserId)
      bulk collect into a_Objects
      from api.AccessGroupUsers agu
      join api.AccessGroups ag
        on agu.AccessGroupId = ag.AccessGroupId
     where ag.Description = 'Condition Processor';
  end ConditionProcessor;


  /**************************************************************************************************
  * LegalPetitionSupervisor()
  *  Finds Users in the Legal Supervisor Access Group
  ***************************************************************************************************/
  procedure LegalPetitionSupervisor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
  begin
    select api.udt_Object(agu.UserId)
      bulk collect into a_Objects
      from api.AccessGroupUsers agu
      join api.AccessGroups ag
        on agu.AccessGroupId = ag.AccessGroupId
     where ag.Description = 'Licensing Review';
  end LegalPetitionSupervisor;

    /**************************************************************************************************
  * LicensingAdminAccessGroup()
  *  Finds Users in the Licensing Administration Access Group
  *  Used in the Batch Renewal Notification Default Clerk on the Admin site
  ***************************************************************************************************/
  procedure LicensingAdminAccessGroup(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_ObjectList                        udt_IdList;
  begin
    select u.ObjectId
      bulk collect into t_ObjectList
      from api.AccessGroupUsers agu
      join api.AccessGroups ag
        on agu.AccessGroupId = ag.AccessGroupId
      join query.u_users u on agu.UserId = u.ObjectId
     where ag.Description = 'Licensing Administration'
       and u.active = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectList);
  end LicensingAdminAccessGroup;

  /*************************************************************************************
   * LicensesForOnlinePermitApp
   * Returns licenses available to the permit application based on chosen legal entity and
   * the available license types selected on the permit type
   *************************************************************************************/
  procedure LicensesForOnlinePermitApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_PermitTypeId                      udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlinePermitTypeObjectId');
    t_UseLEObjectId                     udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlineUseLegalEntityObjectId');
    t_ObjectIdList                      udt_IdList;
    t_OnlineUserId                      udt_ID := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'OnlineUserObjectId');
    --t_LicenseType                       varchar2(100);
    --t_LicenseTypeId                     number;
    t_LicenseTypeIds                    udt_IdList;
    type ApplicableLicenseType   is table of number(9);
    t_ApplicableLicenseTypes            ApplicableLicenseType;
  begin

    pkg_debug.putline('******** Licenses For Online Permit App: Start ********');
    pkg_debug.putline('******** Permit Application Job ID: ' || a_ObjectId || ' ********');
    pkg_debug.putline('******** Selected LE ObjectID: ' || t_UseLEObjectId || ' ********');
    pkg_debug.putline('******** Permit Type ID: ' || t_PermitTypeId || ' ********');


    select LicenseTypeId
     bulk collect into t_ApplicableLicenseTypes
     from query.r_ABC_PermitTypeLicenseType plt
    where plt.PermitTypeId = t_PermitTypeId;

    if t_ApplicableLicenseTypes.count > 0 then

    for c in (select ll.LicenseObjectId,
                     api.pkg_columnQuery.Value(ll.LicenseObjectId, 'IsActive') IsActive,
                     api.pkg_columnQuery.NumericValue(ll.LicenseObjectId, 'LicenseTypeObjectId') LicenseTypeId
              from query.r_Abc_Licenselicenseele ll
               join query.r_ABC_UserLegalEntity ule on ule.LegalEntityObjectId = ll.LegalEntityObjectId
             where ule.UserId = t_OnlineUserId) loop
       if c.IsActive = 'Y'
         and c.LicenseTypeId member of t_ApplicableLicenseTypes then
       t_ObjectIdList(t_ObjectIdList.Count()) := c.licenseobjectid;
       end if;
     end loop;
   end if;

    pkg_debug.putline('******** Objects Returned: ' || t_ObjectIdList.count || '********');
    pkg_debug.putline('******** Licenses For Online Permit App: End ********');
    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end LicensesForOnlinePermitApp;


  /*************************************************************************************
   * PermitsForOnlinePermitApp
   * Returns permits available to the permit application based on chosen legal entity and
   * the available permit types selected on the permit type
   *************************************************************************************/
  procedure PermitsForOnlinePermitApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_PermitTypeId                      udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlinePermitTypeObjectId');
    t_UseLEObjectId                     udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlineUseLegalEntityObjectId');
    t_UserId                            udt_Id := api.pkg_columnquery.Value(a_ObjectId,'OnlineUserObjectId');
  t_PermitEndPointId          udt_Id := api.pkg_configquery.EndPointIdForName('o_Abc_LegalEntity', 'Permit');
    t_ObjectIdList                      udt_IdList;
  t_PermitIdList                      udt_IdList;
    type ApplicablePermitType   is table of number(9);
    t_ApplicablePermitTypes            ApplicablePermitType;

  begin

    select ApplicablePermitTypeId
    bulk collect into t_ApplicablePermitTypes
    from query.r_ABC_PermitTypeAppPermitType
    where PermitTypeId = t_PermitTypeId;

  if t_ApplicablePermitTypes.count > 0 then

  for c in(select p.ObjectId PermitObjectId,
                  api.pkg_columnquery.value(p.objectid,'IsActive') Active,
                  pp.permittypeid
      from query.r_ABC_UserLegalEntity ule
      join table(PermitsForPermitee(ule.LegalEntityObjectId,
                                       t_PermitEndPointId)) p on 1=1
      join query.r_abc_permitpermittype pp on pp.PermitId = p.ObjectId
     where ule.UserId = t_UserId) loop
     if c.Active = 'Y' and
        c.permittypeid member of t_ApplicablePermitTypes then
       t_ObjectIdList(t_ObjectIdList.count):=c.PermitObjectId;
     end if;
   end loop;

  end if;

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end PermitsForOnlinePermitApp;

 /*************************************************************************************
   * PermitsForPermitee
   * Returns the Permits for the Permitee Passed in. Runs the procedural Rel
   *************************************************************************************/

  function PermitsForPermitee
    (a_ObjectId    udt_Id,
     a_EndPoint     udt_Id) return  api.udt_ObjectList
  is
  t_PermitIdList  api.udt_ObjectList;
  begin
  abc.pkg_abc_proceduralrels.PermitForPermitee(a_ObjectId,
                                               a_EndPoint,
                                               t_PermitIdList);
  return  t_PermitIdList;
  end PermitsForPermitee;



  /*************************************************************************************
   * TAPPermitsForOnlinePermitApp
   * Returns TAP permits available to the permit application based on chosen legal entity and
   * the available permit types selected on the permit type
   *************************************************************************************/
  procedure TAPPermitsForOnlinePermitApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_PermitTypeId                      udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlinePermitTypeObjectId');
    t_UseLEObjectId                     udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlineUseLegalEntityObjectId');
    t_UserId                            udt_Id := api.pkg_columnquery.Value(a_ObjectId,'OnlineUserObjectId');
    t_ObjectIdList                      udt_IdList;
  t_PermitTypeObjectId        udt_id;
  begin


   select api.pkg_simplesearch.ObjectByIndex('o_abc_permittype','Code','TAP')
     into  t_PermitTypeObjectId  from dual;

    for c in (select tpp.PermitObjectId, api.pkg_columnquery.value(tpp.PermitObjectId, 'IsActive') Active
    from query.r_ABC_UserLegalEntity ule
    join query.r_ABC_PermitPermittee tpp on tpp.PermitteeObjectId = ule.LegalEntityObjectId
    join query.r_abc_permitpermittype pp on pp.PermitId = tpp.PermitObjectId
   where ule.UserId = t_UserId
     and pp.PermitTypeId = t_PermitTypeObjectId) loop

     if c.Active = 'Y'then
       t_ObjectIdList(t_ObjectIdList.count):=c.PermitObjectId;
     end if;

  end loop;

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end TAPPermitsForOnlinePermitApp;

  /**************************************************************************************************
  *  PermitTypes()
  *   Returns a list of active Permit Types
  ***************************************************************************************************/
  procedure PermitTypes(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_ObjectIdList                            udt_IdList;

  begin
    select o.ObjectId
      bulk collect into t_ObjectIdList
      from query.o_ABC_PermitType o
      where ObjectDefTypeId = 1
        and o.Active = 'Y'
        and o.AvailableOnlineApplication = 'Y';

    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
  end PermitTypes;

 /**************************************************************************************************
  *  TAPPermitsForPublicPR()
  ***************************************************************************************************/
  procedure TAPPermitsForPublicPR (
    a_ObjectId               udt_Id,
    a_RelationshipDefId      udt_Id,
    a_EndPoint               varchar2,
    a_SearchString           varchar2,
    a_Objects                out udt_ObjectList
  ) is
    t_PermitTypeObjectId     udt_id := api.pkg_simplesearch.ObjectByIndex('o_abc_permittype','Code','TAP');
    t_LEObjectDefId          udt_id := api.pkg_configquery.ObjectDefIdForName('o_ABC_LegalEntity');
    t_ObjectIdList           udt_IdList;
  begin

    for c in (select tpp.PermitObjectId, api.pkg_columnquery.value(tpp.PermitObjectId, 'IsActive') Active, api.pkg_columnquery.value(tpp.PermitObjectId, 'PermitNumber') PermitNumber
                from query.j_Abc_Prapplication p
                join api.objects le on le.objectid = p.LegalEntityObjectId
                join query.r_ABC_PermitPermittee tpp on tpp.PermitteeObjectId = p.LegalEntityObjectId
                join query.r_abc_permitpermittype pp on pp.PermitId = tpp.PermitObjectId
              where le.objectdefid = t_LEObjectDefId
                and p.objectid = a_ObjectId
                and pp.PermitTypeId = t_PermitTypeObjectId) loop
       if c.Active = 'Y'then
          t_ObjectIdList(t_ObjectIdList.count):=c.PermitObjectId;
       end if;
    end loop;
    a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end TAPPermitsForPublicPR;

  /*---------------------------------------------------------------------------
   * ActiveAmendmentTypes()
   *   Returns active Amendment Types available to the public Amendment
   * application.
   *-------------------------------------------------------------------------*/
  procedure ActiveAmendmentTypes(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_LicenseTypeObjectId               udt_id :=
        api.pkg_columnquery.Value(a_ObjectId, 'LicenseTypeObjectId');
    t_Count                             integer;
  begin

    select o.ObjectId
    bulk collect into t_ObjectIdList
    from query.o_ABC_AmendmentType o
    where ObjectDefTypeId = 1
      and o.Active = 'Y'
      and o.AvailableOnline = 'Y';

    a_Objects := api.Udt_Objectlist();
    -- Select appropriate Amendment Types
    for a in 1..t_ObjectIdList.count loop
      if api.pkg_columnquery.Value(t_ObjectIdList(a), 'AllOrSomeLicTypes') = 'All' then
        a_Objects.Extend(1);
        a_Objects(a_Objects.Count) := api.udt_Object(t_ObjectIdList(a));
      else
        select count (*)
        into t_Count
        from query.r_abc_amendtypelicensetype r
        where r.AmendmentTypeId = t_ObjectIdList(a)
          and r.LicenseTypeId = t_LicenseTypeObjectId;
        if t_count > 0 then
          a_Objects.Extend(1);
          a_Objects(a_Objects.Count) := api.udt_Object(t_ObjectIdList(a));
        end if;
      end if;
    end loop;

  end ActiveAmendmentTypes;

  /*************************************************************************************
   * LicensesForOnlineLicenseApp
   * Returns licenses available to the license application based on chosen license type (24)
   * and if the the available license types for this users can apply for this type
   *************************************************************************************/
  procedure LicensesForOnlineLicenseApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_ObjectIdList                      udt_IdList;
    t_OnlineUserId                      udt_ID := api.pkg_ColumnQuery.NumericValue(a_ObjectId, 'OnlineUserObjectId');
    type ApplicableLicenseType   is table of number(9);
    t_ApplicableLicenseTypes            ApplicableLicenseType;
  begin

    select lt.ObjectId
     bulk collect into t_ApplicableLicenseTypes
     from query.o_abc_licensetype lt
    where lt.ValidForAdditionalWarehouseLic = 'Y';

    if t_ApplicableLicenseTypes.count > 0 then

    for c in (select ll.LicenseObjectId,
                     api.pkg_columnQuery.Value(ll.LicenseObjectId, 'IsActive') IsActive,
                     api.pkg_columnQuery.NumericValue(ll.LicenseObjectId, 'LicenseTypeObjectId') LicenseTypeId
              from query.r_Abc_Licenselicenseele ll
               join query.r_ABC_UserLegalEntity ule on ule.LegalEntityObjectId = ll.LegalEntityObjectId
             where ule.UserId = t_OnlineUserId) loop
       if c.IsActive = 'Y'
         and c.LicenseTypeId member of t_ApplicableLicenseTypes then
       t_ObjectIdList(t_ObjectIdList.Count()) := c.licenseobjectid;
       end if;
     end loop;
     a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);
   end if;

  end LicensesForOnlineLicenseApp;

  /*************************************************************************************
   * InsigniaPermitForOnlPermitApp
   * Returns Insignia Permits available to the permit application based on chosen Permit
   * Type
   *************************************************************************************/
  procedure InsigniaPermitForOnlPermitApp(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
    t_PermitTypeId                      udt_Id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlinePermitTypeObjectId');
    t_UserId                            udt_Id := api.pkg_columnquery.Value(a_ObjectId,'OnlineUserObjectId');
    t_LicenseId                         udt_id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlineLicenseObjectId');
    t_TAPPermitId                       udt_id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlineTAPPermitObjectId');
    t_PermitId                          udt_id := api.pkg_columnquery.NumericValue(a_ObjectId,'OnlinePermitObjectId');
    t_ObjectIdList                      udt_IdList;

  begin
     if t_LicenseId is not null then
     -- If a License is selected from the dropdown. Show Active Insignia Permits associated with License.
        for c in (
                  select r.PermitObjectId
                  from   query.r_PermitLicense r
                  where  r.LicenseObjectId = t_LicenseId
                 ) loop
           if api.pkg_columnquery.Value(c.PermitObjectId,'PermitTypeObjectId') = t_PermitTypeId and
              api.pkg_columnquery.Value(c.PermitObjectId, 'IsActive') = 'Y'then
              t_ObjectIdList(t_ObjectIdList.count) := c.PermitObjectId;
           end if;
        end loop;
     elsif
     -- If a TAP Permit is selected from the dropdown. Show Active Insignia Permits associated with Permittee.
        t_TAPPermitId is not null then
        for c in (
                  select r.PermitObjectId, r.TAPPermitObjectId
                  from   query.r_ABC_TAPPermit r
                  where  r.TAPPermitObjectId = t_TAPPermitId
                 ) loop
           if api.pkg_columnquery.Value(c.PermitObjectId,'PermitTypeObjectId') = t_PermitTypeId and
              api.pkg_columnquery.Value(c.TAPPermitObjectId, 'IsActiveTAP') = 'Y'then
              t_ObjectIdList(t_ObjectIdList.count) := c.PermitObjectId;
           end if;
        end loop;
     elsif
     -- If a Permit is selected from the dropdown. Show Active Insignia Permits associated with Permittee.
        t_PermitId is not null then
        for c in (
                  select r.PrimaryPermitObjectId
                  from   query.r_ABC_UserLegalEntity ule
                  join   query.r_ABC_PermitPermittee tpp on tpp.PermitteeObjectId = ule.LegalEntityObjectId
                  join   query.r_Abc_Associatedpermit r on r.PrimaryPermitObjectId = tpp.PermitObjectId
                  where  ule.UserId = t_UserId
                  and    r.AsscPermitObjectId = t_PermitId
                 ) loop
           if api.pkg_columnquery.Value(c.PrimaryPermitObjectId,'PermitTypeObjectId') = t_PermitTypeId and
              api.pkg_columnquery.Value(c.PrimaryPermitObjectId, 'IsActive') = 'Y' then
              t_ObjectIdList(t_ObjectIdList.count) := c.PrimaryPermitObjectId;
           end if;
        end loop;
     else
     -- If no License or TAP Permit is selected from the dropdown. Show Active Insignia Permits associated with
     -- all Legal Entities associated with Public user.
        for c in (
                  select tpp.PermitObjectId, api.pkg_columnquery.value(tpp.PermitObjectId, 'IsActive') Active
                  from   query.r_ABC_UserLegalEntity ule
                  join   query.r_ABC_PermitPermittee tpp on tpp.PermitteeObjectId = ule.LegalEntityObjectId
                  join   query.r_abc_permitpermittype pp on pp.PermitId = tpp.PermitObjectId
                  where  ule.UserId = t_UserId
                  and    pp.PermitTypeId = t_PermitTypeId
                 ) loop
           if c.Active = 'Y'then
              t_ObjectIdList(t_ObjectIdList.count) := c.PermitObjectId;
           end if;
        end loop;
     end if;
     a_Objects := extension.pkg_Utils.ConvertToObjectList(t_ObjectIdList);

  end InsigniaPermitForOnlPermitApp;

  /**************************************************************************************************
   * PetitionSupervisor() -- PUBLIC
   **************************************************************************************************/
  procedure PetitionSupervisor(
    a_ObjectId                          udt_Id,
    a_RelationshipDefId                 udt_Id,
    a_EndPoint                          varchar2,
    a_SearchString                      varchar2,
    a_Objects                       out udt_ObjectList
  ) is
  begin

    select api.udt_Object(u.UserId)
      bulk collect into a_Objects
      from api.AccessGroupUsers agu
      join api.AccessGroups ag
        on agu.AccessGroupId = ag.AccessGroupId
      join api.Users u
        on u.UserId = agu.UserId
     where ag.Description = 'Petition Supervisor'
       and u.Active = 'Y';

  end PetitionSupervisor;

end pkg_ABC_ProceduralLookup;
/
