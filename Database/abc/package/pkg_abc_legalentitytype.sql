create or replace package abc.pkg_ABC_LegalEntityType is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  -----------------------------------------------------------------------------
  -- PostVerifyWrapper
  --   Run any Post Verify validation for Legal Entity Type
  -----------------------------------------------------------------------------
  procedure PostVerifyWrapper (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date default sysdate
  );

 


/

grant execute
on abc.pkg_abc_legalentitytype
to posseextensions;

create or replace package body abc.pkg_ABC_LegalEntityType is

  -----------------------------------------------------------------------------
  -- PostVerifyWrapper
  --   Run any Post Verify validation for Legal Entity Type
  -----------------------------------------------------------------------------
  procedure PostVerifyWrapper (
    a_ObjectId                              udt_Id,
    a_AsOfDate                              date default sysdate
    ) is
    a_IsIndividualType                      varchar2(1)
    begin
      t_IsIndividualType := api.pkg_columnquery.Value(a_ObjectId, 'IsIndividualType');
      if t_IsIndividualType = 'Y' then
        if api.pkg_columnquery.Value(a_ObjectId, 'StateOfIncorporationMandatory') 'Y' then
          api.pkg_errors.RaiseError(-20001, 'State of Incorporation Mandatory may not be checked if Is Individual is checked.');
        end if;
      end if;
    end PostVerifyWrapper;
  
end pkg_ABC_LegalEntityType;


/
