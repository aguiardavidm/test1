create or replace package abc.pkg_ABC_WarningBatch is

  -- Public type declarations
  subtype udt_Id is api.pkg_definition.udt_Id;
  subtype udt_IdList is api.pkg_definition.udt_IdList;

  /*---------------------------------------------------------------------------
  * ReliefRequired1218
  *   Check if a License currently requires 12.18 Relief
  *--------------------------------------------------------------------------*/
  function ReliefRequired1218 (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date default sysdate
    ) return varchar2;

  /*---------------------------------------------------------------------------
  * WarningCreation1218
  *   Create 12.18 Warnings on Licenses that require them
  *  Run by workflow on the j_ABC_1218WarningBatch job
  *--------------------------------------------------------------------------*/
  procedure WarningCreation1218 (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date default sysdate
  );

  /*---------------------------------------------------------------------------
  * ReliefRequired1239
  *   Check if a License currently requires 12.39 Relief
  *--------------------------------------------------------------------------*/
  function ReliefRequired1239 (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date default sysdate
  ) return varchar2;

  /*---------------------------------------------------------------------------
  * WarningCreation1239
  *   Create 12.39 Warnings on Licenses that require them
  *  Run by workflow on the j_ABC_1239WarningBatch job
  *--------------------------------------------------------------------------*/
  procedure WarningCreation1239 (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date default sysdate
  );

end pkg_ABC_WarningBatch;
/
create or replace package body abc.pkg_ABC_WarningBatch is

  /*---------------------------------------------------------------------------
  * ReliefRequired1218
  *   Check if a License currently requires 12.18 Relief
  *--------------------------------------------------------------------------*/
  function ReliefRequired1218 (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date default sysdate
    ) return varchar2 is
    t_PetitionTypeId                    udt_Id;
    t_PetitionTermNumber                udt_Id;
    t_ReliefRequired                    varchar2(1) := 'N';
  begin
    t_PetitionTypeId     := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionType', 'Name', '12.18');
    t_PetitionTermNumber := coalesce(api.pkg_ColumnQuery.NumericValue(t_PetitionTypeId, 'PetitionTermStartYearNumber'),0);
    select coalesce(max('Y'), 'N')
      into t_ReliefRequired
      from dual
     where exists(-- Open 12.18 Warning
         select lw.RelationshipId
           from query.r_WarningLicense lw
          where lw.LicenseId = a_ObjectId
            and api.pkg_columnquery.Value(lw.LicenseWarningId, 'WarningTypeActive') = 'Y'
            and (a_AsOfDate >= api.pkg_columnquery.DateValue(lw.LicenseWarningId, 'StartDate'))
            and (a_AsOfDate < nvl(api.pkg_columnquery.DateValue(lw.LicenseWarningId, 'EndDate'), to_date(3999, 'yyyy')))
           )
        or (-- License valid for relief
            api.pkg_ColumnQuery.value(a_ObjectId, 'IssuingAuthority') = 'Municipality'
            and api.pkg_ColumnQuery.value(a_ObjectId, 'State') = 'Active'
            and add_months(api.pkg_columnquery.DateValue(a_ObjectId, 'ExpirationDate'), t_PetitionTermNumber*12) <
                (select add_months(s.WarningCreationDate1218, -12) from query.o_systemsettings s)
            and (not exists (-- Petition Term
                   select r.RelationshipId
                     from query.r_abc_licensepetitionterm r
                    where r.LicenseObjectId = a_ObjectId
                      and api.pkg_columnquery.Value(r.PetitionTermObjectId, 'State') = 'Granted'
                      and api.pkg_columnquery.Value(r.PetitionTermObjectId, 'PetitionType') = '12.18'
                      and (to_char(a_AsOfDate, 'yyyy') = api.pkg_columnquery.Value(r.PetitionTermObjectId, 'TermStartYear')
                          -- Check StartDate and EndDate for historic records
                          or a_AsOfDate between trunc(api.pkg_columnquery.DateValue(r.PetitionTermObjectId, 'StartDate'), 'yyyy')
                              and trunc(api.pkg_columnquery.DateValue(r.PetitionTermObjectId, 'EndDate'), 'yyyy')))));
    return t_ReliefRequired;
  end ReliefRequired1218;

  /*---------------------------------------------------------------------------
  * WarningCreation1218
  *   Check if a License currently requires 12.18 Relief
  *--------------------------------------------------------------------------*/
  procedure WarningCreation1218 (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date default sysdate
    ) is
    t_BatchDate                         date;
    t_JobId                             udt_Id;
    t_LicenseList                       udt_IdList;
    t_LicenseWarningDef                 udt_Id;
    t_LicLicWarningEP                   udt_Id;
    t_LicWarningId                      udt_Id;
    t_LicWarningType                    udt_Id;
    t_LicWarningTypeEP                  udt_Id;
    t_PetitionTypeId                    udt_Id;
    t_PetitionTermNumber                udt_Id;
    t_RelId                             udt_Id;
    t_SystemSettingsId                  udt_Id;
    t_WarningBatchLicEP                 udt_Id;
    t_WarningBatchWarnEP                udt_Id;
    t_WarningComment                    varchar2(4000);
  begin
    t_SystemSettingsId   := api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings', 'SystemSettingsNumber', 1);
    t_BatchDate          := api.pkg_columnquery.DateValue(t_SystemSettingsId, 'WarningCreationDate1218');
    t_JobId              := api.pkg_columnquery.NumericValue(a_ObjectId, 'JobId');
    t_LicenseWarningDef  := api.pkg_configquery.ObjectDefIdForName('o_ABC_LicenseWarning');
    t_LicLicWarningEP    := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseWarning');
    t_LicWarningType     := api.pkg_simplesearch.ObjectByIndex('o_ABC_LicenseWarningType', 'WarningType', 'Y');
    t_LicWarningTypeEP   := api.pkg_configquery.EndPointIdForName('o_ABC_LicenseWarning', 'LicenseWarning');
    t_PetitionTypeId     := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionType', 'Name', '12.18');
    t_PetitionTermNumber := coalesce(api.pkg_ColumnQuery.NumericValue(t_PetitionTypeId, 'PetitionTermStartYearNumber'),0);
    t_WarningBatchLicEP  := api.pkg_configquery.EndPointIdForName('j_ABC_1218WarningBatch', 'License');
    t_WarningBatchWarnEP := api.pkg_configquery.EndPointIdForName('j_ABC_1218WarningBatch', 'LicenseWarning');
    t_WarningComment     := '12.18 RULING REQUIRED FOR ' || (to_char(t_BatchDate, 'yyyy')) || '-' ||
        (to_char(t_BatchDate, 'yyyy')+1) || ' ON ' || to_char(t_BatchDate, 'MON dd, yyyy');
    

    -- Get all licenses that need a 12.18 warning
    select l.objectid
      bulk collect into t_LicenseList
      from table(api.pkg_simplesearch.CastableObjectsByIndex('o_ABC_License', 'State', 'Active')) l
     where trunc(add_months(api.pkg_columnquery.DateValue(l.objectid, 'ExpirationDate'),
         t_PetitionTermNumber*12)) < trunc(t_BatchDate)
       and api.pkg_columnquery.Value(l.objectid, 'IssuingAuthority') = 'Municipality'
       and not exists (-- Open Warnings
        select r.RelationshipId
          from query.r_WarningLicense r
         where r.LicenseId = l.objectid
           and api.pkg_columnquery.Value(r.LicenseWarningId, 'WarningTypeActive') = 'Y'
           and (t_BatchDate >= api.pkg_columnquery.DateValue(r.LicenseWarningId, 'StartDate'))
           and (t_BatchDate < nvl(api.pkg_columnquery.DateValue(r.LicenseWarningId, 'EndDate'), to_date(3999, 'yyyy')))
       )
       and not exists (-- Petition Terms
        select pt.RelationshipId
          from query.r_abc_licensepetitionterm pt
         where pt.LicenseObjectId = l.objectid
           and api.pkg_columnquery.Value(pt.PetitionTermObjectId, 'State') = 'Granted'
           and api.pkg_columnquery.Value(pt.PetitionTermObjectId, 'PetitionType') = '12.18'
           and (to_char(api.pkg_ColumnQuery.DateValue(pt.LicenseObjectId, 'ExpirationDate'), 'yyyy')
               = api.pkg_columnquery.Value(pt.PetitionTermObjectId, 'TermStartYear')
               -- Check StartDate and EndDate for historic records
               or api.pkg_ColumnQuery.DateValue(pt.LicenseObjectId, 'ExpirationDate')+1 
                   between trunc(api.pkg_columnquery.DateValue(pt.PetitionTermObjectId, 'StartDate'), 'yyyy')
                   and trunc(api.pkg_columnquery.DateValue(pt.PetitionTermObjectId, 'EndDate'), 'yyyy'))
       )
       and not exists (-- In Review Renewals
          select ra.RelationshipId
            from query.r_ABC_LicenseToRenewJobLicense ra
           where ra.LicenseObjectId = l.objectid
             and api.pkg_columnquery.Value(ra.RenewJobId, 'StatusName') not in ('NEW', 'CANCEL', 'REJECT')
             and api.pkg_columnquery.DateValue(ra.RenewJobId, 'CalculatedExpirationDate') > t_BatchDate
       );
    
    for i in 1 .. t_LicenseList.count loop
      dbms_output.put_line(t_LicenseList(i));
      t_RelId := api.pkg_relationshipupdate.New(t_WarningBatchLicEP, t_JobId, t_LicenseList(i));
      --Create Warning and relate to license and job
      t_LicWarningId := api.pkg_objectupdate.New(t_LicenseWarningDef);
      t_RelId := api.pkg_relationshipupdate.New(t_LicWarningTypeEP, t_LicWarningId, t_LicWarningType);
      t_RelId := api.pkg_relationshipupdate.New(t_WarningBatchWarnEP, t_JobId, t_LicWarningId);
      api.pkg_columnupdate.SetValue(t_LicWarningId, 'SystemGenerated', 'Y');
      api.pkg_columnupdate.SetValue(t_LicWarningId, 'StartDate', trunc(t_BatchDate));
      api.pkg_columnupdate.SetValue(t_LicWarningId, 'Comments', t_WarningComment);
      t_RelId := api.pkg_relationshipupdate.New(t_LicLicWarningEP, t_LicenseList(i), t_LicWarningId);
    end loop;
    
    --Set the batch creation date on system settings
    api.pkg_columnupdate.SetValue(t_SystemSettingsId, 'WarningCreationDate1218', add_months(t_BatchDate, 12));
  end WarningCreation1218;

  /*---------------------------------------------------------------------------
  * ReliefRequired1239
  *   Check if a License currently requires 12.39 Relief
  *--------------------------------------------------------------------------*/
  function ReliefRequired1239 (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date default sysdate
    ) return varchar2 is
    t_ExpirationDate                    date;
    t_LicenseType                       udt_Id;
    t_PetitionTypeId                    udt_Id;
    t_PetitionTermNumber                udt_Id;
    t_WarningStartDate                  date;
    t_ReliefRequired                    varchar2(1) := 'N';
  begin
    t_ExpirationDate     := trunc(api.pkg_columnquery.DateValue(a_ObjectId, 'CalculatedExpirationDate'));
    t_LicenseType        := api.pkg_columnquery.value(a_ObjectId, 'LicenseTypeObjectId');
    t_PetitionTypeId     := api.pkg_SimpleSearch.ObjectByIndex('o_ABC_PetitionType', 'Name', '12.39');
    t_PetitionTermNumber := coalesce(api.pkg_ColumnQuery.NumericValue(t_PetitionTypeId, 'PetitionTermStartYearNumber'),0);
    t_WarningStartDate   := trunc(api.pkg_columnquery.DateValue(a_ObjectId, 'WarningStartDate1239'));
    
    if t_ExpirationDate is not null then
      select coalesce(max('Y'), 'N')
        into t_ReliefRequired
        from dual
       where exists(-- Open 12.39 Warning
           select lw.RelationshipId
             from query.r_WarningLicense lw
            where lw.LicenseId = a_ObjectId
              and api.pkg_columnquery.Value(lw.LicenseWarningId, 'WarningType1239') = 'Y'
              and (a_AsOfDate >= api.pkg_columnquery.DateValue(lw.LicenseWarningId, 'StartDate'))
              and (a_AsOfDate < nvl(api.pkg_columnquery.DateValue(lw.LicenseWarningId, 'EndDate'), to_date(3999, 'yyyy')))
             )
          or (api.pkg_columnquery.DateValue(a_ObjectId, 'InactivityStartDate') is not null
             and t_WarningStartDate <= to_date(to_char(trunc(t_ExpirationDate+1), 'mmdd') || to_char(sysdate, 'yyyy'), 'mmddyyyy')
             and t_ExpirationDate <= trunc(a_AsOfDate) + api.pkg_columnquery.value(t_LicenseType, 'CanRenewDaysPrior')
             and api.pkg_columnquery.Value(a_ObjectId, 'IssuingAuthority') = 'Municipality'
             and not exists(-- Petition Term
                 select r.RelationshipId
                   from query.r_abc_licensepetitionterm r
                  where r.LicenseObjectId = a_ObjectId
                    and api.pkg_columnquery.Value(r.PetitionTermObjectId, 'State') = 'Granted'
                    and api.pkg_columnquery.Value(r.PetitionTermObjectId, 'PetitionType') = '12.39'
                    and (to_char(t_ExpirationDate, 'yyyy') = api.pkg_columnquery.Value(r.PetitionTermObjectId, 'TermStartYear')
                         -- check StartDate and EndDate for historic records
                         or t_ExpirationDate+1 between trunc(api.pkg_columnquery.DateValue(r.PetitionTermObjectId, 'StartDate'), 'yyyy')
                                                   and trunc(api.pkg_columnquery.DateValue(r.PetitionTermObjectId, 'EndDate'), 'yyyy')))
             );
    end if;
    return t_ReliefRequired;
  end ReliefRequired1239;

  /*---------------------------------------------------------------------------
  * WarningCreation1239
  *   Create 12.39 Warnings on Licenses that require them
  *  Run by workflow on the j_ABC_1239WarningBatch job
  *--------------------------------------------------------------------------*/
  procedure WarningCreation1239 (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date default sysdate
    ) is
    t_BatchDate                         date;
    t_JobId                             udt_Id;
    t_LicenseList                       udt_IdList;
    t_LicenseWarningDef                 udt_Id;
    t_LicLicWarningEP                   udt_Id;
    t_LicWarningId                      udt_Id;
    t_LicWarningType                    udt_Id;
    t_LicWarningTypeEP                  udt_Id;
    t_RelId                             udt_Id;
    t_SystemSettingsId                  udt_Id;
    t_WarningBatchLicEP                 udt_Id;
    t_WarningBatchWarnEP                udt_Id;
    t_WarningComment                    varchar2(4000);
    t_WarningStartDate                  date;
  begin
    t_SystemSettingsId   := api.pkg_SimpleSearch.ObjectByIndex('o_SystemSettings', 'SystemSettingsNumber', 1);
    t_BatchDate          := api.pkg_columnquery.DateValue(t_SystemSettingsId, 'WarningCreationDate1239');
    t_JobId              := api.pkg_columnquery.NumericValue(a_ObjectId, 'JobId');
    t_LicenseWarningDef  := api.pkg_configquery.ObjectDefIdForName('o_ABC_LicenseWarning');
    t_LicLicWarningEP    := api.pkg_configquery.EndPointIdForName('o_ABC_License', 'LicenseWarning'); 
    t_LicWarningType     := api.pkg_simplesearch.ObjectByIndex('o_ABC_LicenseWarningType', 'WarningType1239', 'Y');
    t_LicWarningTypeEP   := api.pkg_configquery.EndPointIdForName('o_ABC_LicenseWarning', 'LicenseWarning');
    t_WarningBatchLicEP  := api.pkg_configquery.EndPointIdForName('j_ABC_1239WarningBatch', 'License');
    t_WarningBatchWarnEP := api.pkg_configquery.EndPointIdForName('j_ABC_1239WarningBatch', 'LicenseWarning');
    -- Warnings are created early and post-dated to 7/1
    t_WarningStartDate   := to_date('07', 'mm');
    t_WarningComment := '12.39 RULING REQUIRED FOR ' || to_char(t_BatchDate, 'yyyy')
        || '-' || (to_char(t_BatchDate, 'yyyy')+1) || ' ' || 
        REGEXP_REPLACE(to_char(t_BatchDate, 'MONTH dd, yyyy'), '\s{2,}', ' ');

    -- Get all licenses where a 12.39 warning needs to be created
    select l.objectid
      bulk collect into t_LicenseList
      from table(api.pkg_SimpleSearch.CastableObjectsByIndex('o_ABC_License', 'State', 'Active')) l
     where not exists (-- Open Warnings
         select r.RelationshipId
           from query.r_WarningLicense r
          where r.LicenseId = l.objectid
            and api.pkg_columnquery.Value(r.LicenseWarningId, 'WarningType1239') = 'Y'
            and (api.pkg_columnquery.DateValue(l.objectid, 'WarningStartDate1239')
                < nvl(api.pkg_columnquery.DateValue(r.LicenseWarningId, 'EndDate'), to_date(3999, 'yyyy')))
        )
       and ReliefRequired1239(l.objectid, t_WarningStartDate) = 'Y';
       
    for i in 1 .. t_LicenseList.count loop
      t_RelId := api.pkg_relationshipupdate.New(t_WarningBatchLicEP, t_JobId, t_LicenseList(i));
      -- Create the Warning object
      t_LicWarningId := api.pkg_objectupdate.New(t_LicenseWarningDef);
      t_RelId := api.pkg_relationshipupdate.New(t_LicWarningTypeEP, t_LicWarningId, t_LicWarningType);
      t_RelId := api.pkg_relationshipupdate.New(t_WarningBatchWarnEP, t_JobId, t_LicWarningId);
      api.pkg_columnupdate.SetValue(t_LicWarningId, 'SystemGenerated', 'Y');
      api.pkg_columnupdate.SetValue(t_LicWarningId, 'StartDate', t_WarningStartDate);
      api.pkg_columnupdate.SetValue(t_LicWarningId, 'Comments', t_WarningComment);
      t_RelId := api.pkg_relationshipupdate.New(t_LicLicWarningEP, t_LicenseList(i), t_LicWarningId);
    end loop;
    
    --Set the batch creation date on system settings to next year
    api.pkg_columnupdate.SetValue(t_SystemSettingsId, 'WarningCreationDate1239', add_months(t_BatchDate, 12));
  end WarningCreation1239;

end pkg_ABC_WarningBatch;
/
