create or replace package pkg_ABC_CPL is

  -- Public type declarations
  subtype udt_Id         is api.pkg_definition.udt_Id;
  subtype udt_IdList     is api.pkg_Definition.udt_IdList;

  -----------------------------------------------------------------------------
  -- ValidateCPLDocument
  -- Created By: Michael Froese
  --  Runs on Post-Verify of d_CPLSpreadsheet
  --  Queues python script on process server if applicable which validates
  --    content of CPL spreadsheet
  -----------------------------------------------------------------------------
  procedure ValidateCPLDocument(
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  );

  -----------------------------------------------------------------------------
  -- ValidateAndProcessCPLDocuments
  -- Created By: Michael Froese
  --  Runs on ____event____ of j_ABC_CPLSubmissions
  --  Queues python script on process server if applicable which processes
  --    content of CPL spreadsheets
  -----------------------------------------------------------------------------
  procedure ValidateAndProcessCPLDocuments(
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  );

  -----------------------------------------------------------------------------------------------------------------------------------------\
  -- OnlineCPLDocumentTypes  -- PUBLIC
  -- Created By: Joshua Lenon
  -- Created Date: 8/13/15
  -- Use:  Relate Document Types set up on the system settings object to the online CPL Submission job
  ------------------------------------------------------------------------------------------------------------------------------------------
  procedure OnlineCPLDocumentTypes (
    a_ObjectId                     udt_Id,
    a_AsOfDate                     date
  );

  -----------------------------------------------------------------------------
  -- ArchiveOldCPLData
  -- Created By: Michael Froese
  --  Runs on daily schedule in process-server
  --  Archive CPL Data for submission periods which are no longer current
  -----------------------------------------------------------------------------
  procedure ArchiveOldCPLData;

  -----------------------------------------------------------------------------
  -- CPLSpreadsheetDeletingEvent
  -- Created By: Michael Froese
  --  If a CPL Spreadsheet document object is deleted, then delete any CPL Data
  --  Originating from this document
  -----------------------------------------------------------------------------
  procedure CPLSpreadsheetDeletingEvent(
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  );

  -----------------------------------------------------------------------------------------------------------------------------------------\
  -- Submit CPL Documents
  -- Created By: Iwasam Agube
  -- Created Date: August-28-15
  -- Use: Submit CPL documents on their submssion deadline.
  ------------------------------------------------------------------------------------------------------------------------------------------
  procedure SubmitCPLs;

  -----------------------------------------------------------------------------------
  --CheckForInitialSubmission
  --Created By: Iwasam Agube
  --Created Date: September-10-15
  --Is called to raise error if there is no previous initial submission
  -- Called from: j_abc_cplsubmissions -> Procedures -> Run procedures - verify -> Procedure:"ABC Raise error if there is no initial submission"
  -----------------------------------------------------------------------------------
  procedure CheckForInitialSubmission(
   a_ObjectId                             udt_Id,
   a_AsOfDate                             date
  );

  -----------------------------------------------------------------------------------
  -- DeleteCPLEntriesByDocId
  -- Created By: Iwasam Agube
  -- Created Date: September-11-15
  -- Delete CPL Entries given cpl submission document id
  -----------------------------------------------------------------------------------
  procedure DeleteCPLEntriesByDocId(
    a_DocumentId                              udt_Id
    );

  -----------------------------------------------------------------------------------
  -- DeleteFormerEntries
  -- Created By: Iwasam Agube
  -- Created Date: September-11-15
  -- Is called to remove older CPL entries
  -----------------------------------------------------------------------------------
  procedure DeleteFormerEntries(
   a_ObjectId                             udt_Id,
   a_AsOfDate                             date
  );

    -----------------------------------------------------------------------------------
    -- CPLDocumentInstanceSecurity
    -- Is called to instance secure CPL Templates
    -----------------------------------------------------------------------------------

    procedure CPLDocumentInstanceSecurity(
     a_ObjectId                             udt_Id,
     a_AsOfDate                             date
    );

    -----------------------------------------------------------------------------------
    -- CPLSubmissionPostVerify
    -- Is called on post-verify of CPLSubmissions job
    -----------------------------------------------------------------------------------
    procedure CPLSubmissionPostVerify(
     a_ObjectId                             udt_Id,
     a_AsOfDate                             date
    );

  -----------------------------------------------------------------------------------
  -- CPLSubmissionAccepted
  -- Is called from workflow of the CPLSubmission job, on Acceptance of the
  --  Review Submission process
  -----------------------------------------------------------------------------------
  procedure CPLSubmissionAccepted(
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  );

end pkg_ABC_CPL;
/
grant execute on abc.pkg_abc_cpl to posseextensions;

create or replace package body abc.pkg_ABC_CPL is

  procedure ValidateCPLDocument(
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  ) is
    t number;
    args varchar2(4000);
  begin
    if api.pkg_ColumnQuery.Value(a_ObjectId, 'Validated') = 'Y'  -- already been validated
      or api.pkg_ColumnQuery.Value(a_ObjectId, 'ValidationStatus') is not null -- has already been scheduled/run
      or api.pkg_ColumnQuery.Value(a_ObjectId, 'CPLType') != 'Wine / Spirits' -- isn't of the correct type
      or api.pkg_ColumnQuery.Value(a_ObjectId, 'CPLType') is null
    then
      return; -- nothing to do for non Wine / Spirits CPL docs
    end if;
    api.pkg_ProcessServer.AddArgument(args, a_ObjectId);
    t := api.pkg_ProcessServer.SchedulePythonScript(
           'ValidateCPL',
           'Validate CPL Data for DocumentId:'||a_ObjectId,
           args,
           sysdate
    );
    api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'ValidationStatus', 'Scheduled');
    api.pkg_ColumnUpdate.SetValue(a_ObjectId, 'ScheduleId', t);
  end;

  procedure ValidateAndProcessCPLDocuments(
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  ) is
    t_JobId                            udt_Id;
    t                                  number;
    args                               varchar2(4000);
  begin

    api.pkg_ProcessServer.AddArgument(args, a_ObjectId);
    t:= api.pkg_ProcessServer.SchedulePythonScript(
          'ValidateCPL',
          'Validate and Process CPL Documents for ProcessId:'||a_ObjectId,
          args,
          sysdate
    );
  end;




  -----------------------------------------------------------------------------------------------------------------------------------------\
  -- OnlineCPLDocumentTypes  -- PUBLIC
  -- Created By: Joshua Lenon
  -- Created Date: 8/13/15
  -- Use:  Relate Document Types set up on the system settings object to the online CPL Submission job
  ------------------------------------------------------------------------------------------------------------------------------------------
  procedure OnlineCPLDocumentTypes (
    a_ObjectId                     udt_Id,
    a_AsOfDate                     date
  ) is
    t_RelationshipCount            number;
    t_RelationshipId               udt_Id;

  begin
  -- Loop through document types on the system settings object


    if api.pkg_columnquery.value(a_ObjectId, 'CreateOLDocRels') = 'Y' then

      for c in (select d.DocumentTypeObjectId
                  from query.r_Abc_Cplonlinedocumenttype d
                 where not exists (select r.relationshipid
                                     from query.r_ABC_OnlineCPLDocumentType r
                                    where r.DocumentTypeObjectId = d.DocumentTypeObjectId
                                      and r.CPLSubmissionJobId = a_ObjectId))loop

           --relate document types to the online CPL job
           t_RelationshipId := api.pkg_relationshipupdate.new(api.pkg_configquery.EndPointIdForName('j_ABC_CPLSubmissions', 'OnlineDocumentType'), a_ObjectId, c.documenttypeobjectid);

      end loop;

    end if;

  end;

  procedure CPLSpreadsheetDeletingEvent(
    a_ObjectId                         udt_Id,
    a_AsOfDate                         date
  ) is
  begin
    delete from ABC.CPL_ActiveData_t where CPLSubmissionDocId = a_ObjectId;
    api.Pkg_Logicaltransactionupdate.EndTransaction;
    delete from ABC.CPL_ArchiveData_t where CPLSubmissionDocId = a_ObjectId;
  end;

  procedure ArchiveOldCPLData is
  /*
  Text from Gap Item Log
  12 months of data must be available for searching (i.e. the current month plus 11 previous).
  o        For example, if searching on 4/5/2014, the data returned will be from  May 2013 ?
           April 2014.  If searching on 4/22/2014 (i.e. after the posting date), the data
           returned will be from June 2013 ? May 2014.
  o        Older data will be archived on a regular basis and retrievable using queries, but
          not via the POSSE ABC front end.

  So... the PostingDate ends the submission period and warrants deletion of data from the
  submission period 12 prior and backward.

  E.g.  As of 4/19/2014, entries from May 2013 submission period and earlier may be archived
  */

  /*
  This is how we solve the problem...
  Assume submission periods occur monthly only

    1. Find Submission Periods ordered according to posting-date desc
    2. Restrict to submission periods with a posting-date prior to sysdate
    3. Via analytical row_number function, submission periods with row_number >= 13 are in-scope
    4. For each of those submission periods
       a. sanity check to ensure that the period is in fact 13 months ago
          i. this is to validate our assumption that periods are always monthly and prevents us
             from archiving data that is not old enough
       b. check if CPLDataPermanentlyArchived
          i. if True, skip
          ii if False, then archive CPL data for all CPL jobs related to the period
  */

  begin
    for i in (
            select x.objectid,
                   postingdate,
                   SubmissionPeriod,
                   CPLDataPermanentlyArchived,
                   row_number() over (partition by 1 order by postingdate desc) RN
              from query.o_abc_cplsubmissionperiod p
              join table(api.pkg_simplesearch.CastableObjectsByIndexRange(
                          'o_ABC_CPLSubmissionPeriod',
                          'PostingDate',
                          '1900-01-01 00:00:00',
                          to_char(trunc(sysdate), 'YYYY-MM-DD') || ' 00:00:00'
                          )
              ) X on x.objectid = p.objectid
    ) loop
      if i.rn >= 13 and
         -- not already archived... assumes the submission period can't be reused.
         -- to ensure this, we'll mark the submisison period as inactive in this code
         i.CPLDataPermanentlyArchived = 'N' and
         -- sanity check
         months_between(trunc(sysdate),i.postingdate)> 12 then

       --dbms_output.put_line(
        pkg_Debug.Putline(
          'Archiving Submission Period '||i.SubmissionPeriod);

        -- archive files per CPL Spreadsheet (Internal)
        for j in (
          select r2.DocumentId
            from query.r_abc_cplsubmissionperiods r
            join query.r_abc_cplspreadsheet r2
              on r2.JobId = r.CPLJobId
            where r.CPLObjectId = i.objectid
        ) loop
          --dbms_output.put_line(
          pkg_Debug.Putline(
            'Archiving CPL Submission Document '||j.DocumentId);

          insert /*+ APPEND */ into ABC.Cpl_Archivedata_t
            select *
              from ABC.CPL_ACTIVEDATA_T cpl_data
              where cpl_data.CPLSubmissionDocId = j.DocumentId;
          COMMIT;
          delete from ABC.CPL_ActiveData_t where CPLSubmissionDocId = j.DocumentId;
          api.pkg_logicaltransactionupdate.EndTransaction;
          COMMIT;
        end loop; --j

        -- and Online submissions too
        for j in (
          select r2.DocumentId
            from query.r_abc_cplsubmissionperiods r
            join query.r_abc_cplonlinespreadsheet r2
              on r2.JobId = r.CPLJobId
            where r.CPLObjectId = i.objectid
        ) loop
          --dbms_output.put_line(
          pkg_Debug.Putline(
          'Archiving Online CPL Submission Document '||j.DocumentId);

          insert /*+ APPEND */ into ABC.Cpl_Archivedata_t
            select *
              from ABC.CPL_ACTIVEDATA_T cpl_data
              where cpl_data.CPLSubmissionDocId = j.DocumentId;
          COMMIT;
          delete from ABC.CPL_ActiveData_t where CPLSubmissionDocId = j.DocumentId;
          api.pkg_logicaltransactionupdate.EndTransaction;
          COMMIT;
        end loop; --j
        -- inactivate the submission period
        api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'Active', 'N');
        -- note that submission period has already had its data archived
        api.pkg_ColumnUpdate.SetValue(i.ObjectId, 'CPLDataPermanentlyArchived', 'Y');
        api.pkg_logicaltransactionupdate.EndTransaction;
        COMMIT;
      end if;
    end loop; --i
  end ArchiveOldCPLData;

  -----------------------------------------------------------------------------------------------------------------------------------------\
  -- Submit CPL Documents
  -- Created By: Iwasam Agube
  -- Created Date: August-28-15
  -- Use: Submit CPL documents on their submssion deadline.
  ------------------------------------------------------------------------------------------------------------------------------------------
  procedure SubmitCPLs is
    begin
      for x in
         (
          select psc.objectid processid
               , jcs.ObjectId jobid
               , jcs.SubmissionDeadlineDate
               , jcs.SpreadsheetIsValid
          from query.p_abc_submitcpl psc
          join query.j_abc_cplsubmissions jcs
          on jcs.objectid = psc.JobId
          and psc.ObjectDefTypeId = 3
          and jcs.ObjectDefTypeId = 2
          and psc.DateCompleted is null
          )
      loop
        begin
         pkg_debug.putline('Submiting CPL(ProcessId:'||x.processid||', JobId:'||x.jobid||')');
         if trunc(sysdate) > trunc(x.submissiondeadlinedate) then
             api.pkg_processupdate.Complete(a_Process => x.processid, a_Outcome => 'Submitted');
             api.pkg_logicaltransactionupdate.EndTransaction;
             commit;
         end if;
         exception when others then
           rollback;
           api.pkg_logicaltransactionupdate.ResetTransaction;
           pkg_debug.putline('Error trying to submit CPL('||x.processid||', '||x.jobid||'): '||SQLERRM||':'||SQLCODE);
         end;
      end loop;
    end SubmitCPLs;

    -----------------------------------------------------------------------------------
    --CheckForInitialSubmission
    --Is called to raise error if there is no previous initial submission
    -- Called from: j_abc_cplsubmissions -> Procedures -> Run procedures - verify -> Procedure:"ABC Raise error if there is no initial submission"
    -----------------------------------------------------------------------------------
    procedure CheckForInitialSubmission(
     a_ObjectId                             udt_Id,
     a_AsOfDate                             date
    )
    is
    t_InitSubmExists boolean := False;

    begin
      null;
      for x in (
         select objectid
          from table(
               api.pkg_simplesearch
                .CastableObjectsByIndex('j_ABC_CPLSubmissions'
                                       ,'dup_LicenseSubmissionPeriod'
                                       ,api.pkg_columnquery.value(a_ObjectId,'dup_LicenseSubmissionPeriod')
                                       ))
        ) loop
        if api.pkg_columnquery.value(x.objectid,'SubmissionType') = 'Initial'
          then
            t_InitSubmExists := True;
        end if;
      end loop;

      if not t_InitSubmExists then
        api.pkg_errors.RaiseError(-20001,'There must first be an initial submission');
      end if;

    end CheckForInitialSubmission;

    -----------------------------------------------------------------------------------
    -- DeleteCPLEntriesByDocId
    -- Delete CPL Entries given cpl submission document id
    -----------------------------------------------------------------------------------
    procedure DeleteCPLEntriesByDocId(
      a_DocumentId                              udt_Id
      )
      is

      begin
        delete from abc.cpl_activedata_t o
        where o.cplsubmissiondocid = a_DocumentId
        ;

      end DeleteCPLEntriesByDocId
      ;

    -----------------------------------------------------------------------------------
    -- DeleteFormerEntries
    -- Is called to remove older CPL entries
    -----------------------------------------------------------------------------------
    procedure DeleteFormerEntries(
     a_ObjectId                             udt_Id,
     a_AsOfDate                             date
    )
    is

    t_PresentDocId api.pkg_definition.udt_id := nvl(api.pkg_columnquery.numericvalue(a_objectid,'InternalSpreadsheetObjectId'),
                                                    api.pkg_columnquery.numericvalue(a_objectid,'OnlineSpreadsheetObjectId'));
    t_FoundDocId  api.pkg_definition.udt_id;
    begin


      for x in (
          select objectid
          from table(
               api.pkg_simplesearch
                .CastableObjectsByIndex('j_ABC_CPLSubmissions'
                                       ,'dup_LicenseSubmissionPeriod'
                                       ,api.pkg_columnquery.value(a_ObjectId,'dup_LicenseSubmissionPeriod')
                                       ))
        ) loop
        t_FoundDocId := nvl(api.pkg_columnquery.numericvalue(x.objectid,'InternalSpreadsheetObjectId'),
               api.pkg_columnquery.numericvalue(x.objectid,'OnlineSpreadsheetObjectId'));
        if t_FoundDocId is not null and t_FoundDocId != t_PresentDocId then
          DeleteCPLEntriesByDocId(t_FoundDocId);
        end if;
      end loop;

    end DeleteFormerEntries;

    -----------------------------------------------------------------------------------
    -- CPLDocumentInstanceSecurity
    -- Is called to instance secure CPL Templates
    -----------------------------------------------------------------------------------

    procedure CPLDocumentInstanceSecurity(
     a_ObjectId                             udt_Id,
     a_AsOfDate                             date
    )
    is
    t_ReadGroups                            varchar2(4000);
    t_ModifyGroups                          varchar2(4000);
    t_DocId                                 udt_Id;
    t_PLSQL_Block                           varchar2(500);

    begin
      -- added dynamic SQL so I would not have to write multiple procedures
      -- all the views should have the correct columns named JobID and DocumentId for this
      -- to be generic.

      t_PLSQL_Block := 'select q.DocumentId ' ||
                       'from query.' || api.pkg_ColumnQuery.Value(a_Objectid, 'ObjectDefName') || ' q ' ||
                       'where q.RelationshipId = :a_ObjectId';

      begin
        EXECUTE IMMEDIATE t_PLSQL_Block
           into t_DocId
          using a_ObjectId;
      exception when no_data_found then
        t_DocId := null;
      end;
      t_ReadGroups := api.pkg_columnQuery.Value(t_DocId,'InstanceSecureReadGroups');
      api.pkg_columnupdate.SetValue(t_DocId,
                                    'InstanceSecureReadGroups',
                                    'ABC Public;' || t_ReadGroups);

    end CPLDocumentInstanceSecurity;

  /*---------------------------------------------------------------------------
   * CPLSubmissionPostVerify() -- PUBLIC
   *   Is called on post-verify of CPLSubmissions job
   *-------------------------------------------------------------------------*/
  procedure CPLSubmissionPostVerify (
    a_ObjectId                          udt_Id,
    a_AsOfDate                          date
  )
  is
    t_DefaultClerkId                    udt_Id;
    t_DefaultCPLReviewer                udt_Id;
    t_RelId                             udt_Id;
    t_StatusDescription                 varchar2(200);
  begin

    t_DefaultCPLReviewer := api.pkg_ConfigQuery.EndPointIdForName(
        'j_ABC_CPLSubmissions', 'CPLReviewer');
    if api.pkg_ObjectQuery.RelatedObjects(a_ObjectId, t_DefaultCPLReviewer).count() = 0 then
       begin

         select DefaultReviewerObjectId
         into t_DefaultClerkId
         from query.o_SystemSettings;
       exception
         when no_data_found then
            raise_application_error(-20000, 'CPL Reviewer is not avaialable - please contact' ||
                ' the NJ ABC Administrator.');

       end;
       t_RelId := api.pkg_RelationshipUpdate.New(t_DefaultCPLReviewer,
           a_ObjectId, t_DefaultClerkId);
    end if;
    -- If an amendment or resubmission is not allowed for the selected period
    --  then stop the user immediately.
    t_StatusDescription := api.pkg_ColumnQuery.Value(a_ObjectId, 'StatusDescription');
    if t_StatusDescription != 'Completed' and t_StatusDescription != 'Rejected' and t_StatusDescription != 'Cancelled' then
      if api.pkg_ColumnQuery.Value(a_ObjectId, 'SubmissionType') = 'Amendment' then
        if api.pkg_ColumnQuery.Value(a_ObjectId, 'AmendmentIsAllowed') = 'N' then
          api.pkg_Errors.RaiseError(-20000,
              'Amendments are not allowed before the Submission Deadline Date.');
        end if;
      elsif api.pkg_ColumnQuery.Value(a_ObjectId, 'SubmissionType') = 'Resubmission' then
        if api.pkg_ColumnQuery.Value(a_ObjectId, 'ResubmissionIsAllowed') = 'N' then
          api.pkg_Errors.RaiseError(-20000,
              'Resubmissions are not allowed after the Submission Deadline Date.');
        end if;
      end if;
    end if;

  end CPLSubmissionPostVerify;

  -----------------------------------------------------------------------------------
  -- CPLSubmissionAccepted
  -- Is called from workflow of the CPLSubmission job, on Acceptance of the
  --  Review Submission process.
  -----------------------------------------------------------------------------------
  procedure CPLSubmissionAccepted(
    a_ObjectId                             udt_Id,
    a_AsOfDate                             date
  )
  is
    t_JobId                                udt_Id;
    t_SubmissionIds                        udt_IdList;
    t_LicenseSubmissionPeriod              varchar2(4000);
  begin
    t_JobId := api.pkg_columnquery.value(a_ObjectId, 'JobId');
    -- Set IsLatestSubmission to N on previous jobs
    t_LicenseSubmissionPeriod := api.pkg_columnquery.value(t_JobId,
        'dup_LicenseSubmissionPeriod');
    t_SubmissionIds := api.pkg_simplesearch.ObjectsByIndex(
        'j_ABC_CPLSubmissions', 'dup_LicenseSubmissionPeriod',
        t_LicenseSubmissionPeriod);
    for i in 1 .. t_SubmissionIds.count loop
      if api.pkg_columnquery.Value(t_SubmissionIds(i),
          'IsLatestSubmission') = 'Y' then
        api.pkg_columnupdate.SetValue(t_SubmissionIds(i),
            'IsLatestSubmission', 'N');
      end if;
    end loop;
    -- Set this job as the Latest Submission
    api.pkg_columnupdate.SetValue(t_JobId, 'IsLatestSubmission', 'Y');
  end CPLSubmissionAccepted;

end pkg_ABC_CPL;
/
