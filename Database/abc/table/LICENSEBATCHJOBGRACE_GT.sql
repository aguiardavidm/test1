-- Create table
create global temporary table LICENSEBATCHJOBGRACE_GT
(
  licenseobjectid      NUMBER(9),
  licensetypeobjectid  NUMBER(9),
  licensetype          VARCHAR2(50),
  municipalityobjectid NUMBER(9),
  municipality         VARCHAR2(50),
  expirationdate       DATE,
  issuingauthority     VARCHAR2(20)
)
on commit delete rows;
-- Create/Recreate indexes 
create index LICENSEBATCHJOBGRACE_GT_IX1 on LICENSEBATCHJOBGRACE_GT (LICENSETYPE);
create index LICENSEBATCHJOBGRACE_GT_IX2 on LICENSEBATCHJOBGRACE_GT (MUNICIPALITY);
create index LICENSEBATCHJOBGRACE_GT_IX3 on LICENSEBATCHJOBGRACE_GT (ISSUINGAUTHORITY);
-- Grant/Revoke object privileges 
grant select, insert, update, delete, alter on LICENSEBATCHJOBGRACE_GT to POSSEEXTENSIONS;
