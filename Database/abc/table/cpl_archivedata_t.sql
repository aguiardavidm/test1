create table cpl_archivedata_t (
  sequencenum                     number not null,
  cplsubmissiondocid              number(9) not null,
  sku                             varchar2(50) not null,
  customfield2                    varchar2(50) not null,
  wholesaler                      varchar2(250) not null,
  submissiondate                  date not null,
  fromdate                        date not null,
  todate                          date not null,
  brandregistration               number not null,
  description                     varchar2(1000) not null,
  proof                           number not null,
  unittype                        varchar2(50) not null,
  unitquantity                    number not null,
  unitvolumetype                  varchar2(50) not null,
  unitvolumeamount                number not null,
  pack                            number not null,
  sleeve                          number not null,
  combopack                       char(1) not null,
  rip                             char(1) not null,
  listpricecase                   number(14, 2) not null,
  listpricebottle                 number(14, 2) not null,
  bestcase                        number(14, 2) not null,
  bestbottle                      number(14, 2) not null,
  splitchg                        number(14, 2) not null,
  level1qty                       number not null,
  level1disc                      number(14, 2) not null,
  level2qty                       number not null,
  level2disc                      number(14, 2) not null,
  level3qty                       number not null,
  level3disc                      number(14, 2) not null,
  level4qty                       number not null,
  level4disc                      number(14, 2) not null,
  level5qty                       number not null,
  level5disc                      number(14, 2) not null,
  level6qty                       number not null,
  level6disc                      number(14, 2) not null,
  closeout                        char(1)
);
grant
  delete,
  insert,
  select,
  update
on cpl_archivedata_t
to
  posseextensions;

