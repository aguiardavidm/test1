create global temporary table annualrefundcomparison_t (
  gldescription                   varchar2(60),
  prioryear                       number(15, 2),
  baseyear                        number(15, 2),
  difference                      number(15, 2)
) on commit preserve rows;

grant select
on annualrefundcomparison_t
to public;

