/*Script*/
alter table abc.cpl_activedata_t rename column PACK to NOTES;
alter table abc.cpl_archivedata_t rename column PACK to NOTES;

alter table abc.cpl_activedata_t modify NOTES NULL;
update abc.cpl_activedata_t set NOTES = '';
alter table abc.cpl_activedata_t modify NOTES varchar2(4000);

alter table abc.cpl_archivedata_t modify NOTES NULL;
update abc.cpl_archivedata_t set NOTES = '';
alter table abc.cpl_archivedata_t modify NOTES varchar2(4000);

create index cpl_activedata_ix10
on cpl_activedata_t (
  SKU
);
