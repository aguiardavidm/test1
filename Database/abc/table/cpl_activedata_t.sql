create table cpl_activedata_t (
  sequencenum                     number not null,
  cplsubmissiondocid              number(9) not null,
  sku                             varchar2(50) not null,
  customfield2                    varchar2(50) not null,
  wholesaler                      varchar2(250) not null,
  submissiondate                  date not null,
  fromdate                        date not null,
  todate                          date not null,
  brandregistration               number not null,
  description                     varchar2(1000) not null,
  proof                           number not null,
  unittype                        varchar2(50) not null,
  unitquantity                    number not null,
  unitvolumetype                  varchar2(50) not null,
  unitvolumeamount                number not null,
  pack                            number not null,
  sleeve                          number not null,
  combopack                       char(1) not null,
  rip                             char(1) not null,
  listpricecase                   number(14, 2) not null,
  listpricebottle                 number(14, 2) not null,
  bestcase                        number(14, 2) not null,
  bestbottle                      number(14, 2) not null,
  splitchg                        number(14, 2) not null,
  level1qty                       number not null,
  level1disc                      number(14, 2) not null,
  level2qty                       number not null,
  level2disc                      number(14, 2) not null,
  level3qty                       number not null,
  level3disc                      number(14, 2) not null,
  level4qty                       number not null,
  level4disc                      number(14, 2) not null,
  level5qty                       number not null,
  level5disc                      number(14, 2) not null,
  level6qty                       number not null,
  level6disc                      number(14, 2) not null,
  closeout                        char(1)
);

grant
  delete,
  insert,
  select,
  update
on cpl_activedata_t
to
  posseextensions,
  possesys;

alter table cpl_activedata_t
add constraint cpl_activedata_pk
primary key (
  sequencenum
);

create index cpl_activedata_ix1
on cpl_activedata_t (
  cplsubmissiondocid
);

create index cpl_activedata_ix2
on cpl_activedata_t (
  description
);

create index cpl_activedata_ix3
on cpl_activedata_t (
  brandregistration
);

create index cpl_activedata_ix4
on cpl_activedata_t (
  fromdate
);

create index cpl_activedata_ix5
on cpl_activedata_t (
  todate
);

create index cpl_activedata_ix6
on cpl_activedata_t (
  unitvolumeamount
);

create index cpl_activedata_ix7
on cpl_activedata_t (
  combopack
);

create index cpl_activedata_ix8
on cpl_activedata_t (
  rip
);

create index cpl_activedata_ix9
on cpl_activedata_t (
  closeout
);

create or replace trigger CPL_ACTIVEDATA_TG1
  before insert on CPL_ACTIVEDATA_T
  for each row
declare
  t_SequenceId number;
  t_LogicalTransactionId api.pkg_definition.udt_Id;
begin
  if :NEW.SequenceNum is null then
    select SequenceId
      into t_SequenceId
      from api.sequences s
      where description = 'ABC CPL Submission Row Number';
    :NEW.SequenceNum := api.pkg_sequenceupdate.NextValue(t_SequenceId);
    t_LogicalTransactionId := api.pkg_logicaltransactionupdate.CurrentTransaction();
    api.pkg_objectupdate.RegisterExternalObject('o_ABC_CPLActiveData', :NEW.SequenceNum );
  end if;
end CPL_ACTIVEDATA_TG1;
/

create or replace trigger CPL_ACTIVEDATA_TG2
  before delete on CPL_ACTIVEDATA_T
  for each row
declare
begin
  api.pkg_objectupdate.DeregisterExternalObject('o_ABC_CPLActiveData', :OLD.SequenceNum);
end CPL_ACTIVEDATA_TG2;
/

