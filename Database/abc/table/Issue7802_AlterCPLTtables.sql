  alter table abc.cpl_activedata_t
  add (
    brandregistration1  VARCHAR2(4000) null,
    proof1              VARCHAR2(4000) null,
    sleeve1             VARCHAR2(4000) null,
    closeout1           VARCHAR2(4000) null,
    level1qty1          VARCHAR2(4000) null,
    level2qty1          VARCHAR2(4000) null,
    level3qty1          VARCHAR2(4000) null,
    level4qty1          VARCHAR2(4000) null,
    level5qty1          VARCHAR2(4000) null,
    level6qty1          VARCHAR2(4000) null
  );          

  update abc.cpl_activedata_t set brandregistration1 = brandregistration,
                                  proof1             = proof,
                                  sleeve1            = sleeve,
                                  closeout1          = closeout,
                                  level1qty1         = level1qty,
                                  level2qty1         = level2qty,
                                  level3qty1         = level3qty,
                                  level4qty1         = level4qty,
                                  level5qty1         = level5qty,
                                  level6qty1         = level6qty;

  alter table abc.cpl_activedata_t
  drop (brandregistration, proof, sleeve, closeout, level1qty, level2qty, level3qty, level4qty, level5qty, level6qty);
  
  alter table abc.cpl_activedata_t
  rename column brandregistration1 to brandregistration;  
  
  alter table abc.cpl_activedata_t
  rename column proof1 to proof;  

  alter table abc.cpl_activedata_t
  rename column sleeve1 to sleeve;  

  alter table abc.cpl_activedata_t
  rename column closeout1 to closeout;  

  alter table abc.cpl_activedata_t
  rename column level1qty1 to level1qty;  

  alter table abc.cpl_activedata_t
  rename column level2qty1 to level2qty;  

  alter table abc.cpl_activedata_t
  rename column level3qty1 to level3qty;  

  alter table abc.cpl_activedata_t
  rename column level4qty1 to level4qty;  

  alter table abc.cpl_activedata_t
  rename column level5qty1 to level5qty;  

  alter table abc.cpl_activedata_t
  rename column level6qty1 to level6qty;  


  alter table abc.cpl_archivedata_t
  add (
    brandregistration1  VARCHAR2(4000) null,
    proof1              VARCHAR2(4000) null,
    sleeve1             VARCHAR2(4000) null,
    closeout1           VARCHAR2(4000) null,
    level1qty1          VARCHAR2(4000) null,
    level2qty1          VARCHAR2(4000) null,
    level3qty1          VARCHAR2(4000) null,
    level4qty1          VARCHAR2(4000) null,
    level5qty1          VARCHAR2(4000) null,
    level6qty1          VARCHAR2(4000) null
  );          

  update abc.cpl_archivedata_t set brandregistration1 = brandregistration,
                                   proof1             = proof,
                                   sleeve1            = sleeve,
                                   closeout1          = closeout,
                                   level1qty1         = level1qty,
                                   level2qty1         = level2qty,
                                   level3qty1         = level3qty,
                                   level4qty1         = level4qty,
                                   level5qty1         = level5qty,
                                   level6qty1         = level6qty;

  alter table abc.cpl_archivedata_t
  drop (brandregistration, proof, sleeve, closeout, level1qty, level2qty, level3qty, level4qty, level5qty, level6qty);
  
  alter table abc.cpl_archivedata_t
  rename column brandregistration1 to brandregistration;  
  
  alter table abc.cpl_archivedata_t
  rename column proof1 to proof;  

  alter table abc.cpl_archivedata_t
  rename column sleeve1 to sleeve;  

  alter table abc.cpl_archivedata_t
  rename column closeout1 to closeout;  

  alter table abc.cpl_archivedata_t
  rename column level1qty1 to level1qty;  

  alter table abc.cpl_archivedata_t
  rename column level2qty1 to level2qty;  

  alter table abc.cpl_archivedata_t
  rename column level3qty1 to level3qty;  

  alter table abc.cpl_archivedata_t
  rename column level4qty1 to level4qty;  

  alter table abc.cpl_archivedata_t
  rename column level5qty1 to level5qty;  

  alter table abc.cpl_archivedata_t
  rename column level6qty1 to level6qty;  

