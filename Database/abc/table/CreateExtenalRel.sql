create table abc.RegistrantNotifications_t 
     (NotificationJobId    number,
      RegistrantObjectid   number,
      RegistrantInitial    varchar2(1),
      RenewalCycleObjectId number );
      
-- Create/Recreate indexes 
create index ABC.NotificationJobId_IX1 on ABC.REGISTRANTNOTIFICATIONS_T (notificationjobid);
create index ABC.RegistrantObjectId_IX2 on ABC.REGISTRANTNOTIFICATIONS_T (registrantobjectid); 
create index ABC.RenewalCycleObjectId_IX3 on ABC.REGISTRANTNOTIFICATIONS_T (RenewalCycleObjectId); 

grant select, update, insert, delete on abc.RegistrantNotifications_t  to posseextensions;     
