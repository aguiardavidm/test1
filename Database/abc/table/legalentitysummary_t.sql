create global temporary table legalentitysummary_t (
  parentlegalentityid             number,
  legalentityid                   number,
  relationshipid                  number,
  active                          char(1),
  vlevel                          number,
  tag                             char(10)
) on commit delete rows;

grant alter
on legalentitysummary_t
to possedba;

grant delete
on legalentitysummary_t
to possedba;

grant index
on legalentitysummary_t
to possedba;

grant insert
on legalentitysummary_t
to possedba;

grant references
on legalentitysummary_t
to possedba;

grant select
on legalentitysummary_t
to possedba;

grant select
on legalentitysummary_t
to posseextensions;

grant select
on legalentitysummary_t
to public;

grant update
on legalentitysummary_t
to possedba;

