create global temporary table annualrevenuecomparison_t (
  gldescription                   varchar2(4000),
  licensetype                     varchar2(20),
  description                     varchar2(60),
  priorfiscalyear                 number(15, 2),
  basefiscalyear                  number(15, 2),
  difference                      number(15, 2)
) on commit preserve rows;

grant select
on annualrevenuecomparison_t
to public;

