--create table
CREATE TABLE abc.LoginAudit 
( 
  ObjectId      number,
  Event         varchar2(100),
  EventStatus   varchar(100),
  AuditDate     date,
  IPAddress     varchar2(60),
  HostName      varchar2(300),
  Account       varchar2(100),
  Site          varchar2(100)
) TABLESPACE LARGEDATA;
/