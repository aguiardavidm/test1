create table LEPERMITAPPXREF_T
(
  LEPermitAppXrefId   number not null,
  LegalEntityId       number not null,
  PermitApplicationId number not null
)
;

grant
  delete,
  insert,
  select,
  update
on ABC.LEPERMITAPPXREF_T 
to POSSEEXTENSIONS;

alter table LEPERMITAPPXREF_T
add constraint LEPERMITAPP_PK
primary key (
  LEPermitAppXrefId
);

create index LEPERMITAPP_IX_01 on LEPERMITAPPXREF_T (LegalEntityId);
create index LEPERMITAPP_IX_02 on LEPERMITAPPXREF_T (PermitApplicationId);