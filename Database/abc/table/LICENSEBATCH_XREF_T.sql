-- Create table
create table LICENSEBATCH_XREF_T
(
  renewalnotifbatchjobid NUMBER(9),
  licenseobjectid        NUMBER(9),
  licensetypeid          NUMBER(9)
)
tablespace LARGEDATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
-- Create/Recreate indexes 
create index LICENSEBATCH_XREF_IX1 on LICENSEBATCH_XREF_T (RENEWALNOTIFBATCHJOBID)
  tablespace LARGEDATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
create index LICENSEBATCH_XREF_IX2 on LICENSEBATCH_XREF_T (LICENSEOBJECTID)
  tablespace LARGEDATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
create index LICENSEBATCH_XREF_IX3 on LICENSEBATCH_XREF_T (LICENSETYPEID)
  tablespace LARGEDATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete, alter on LICENSEBATCH_XREF_T to POSSEEXTENSIONS;
