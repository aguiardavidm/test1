create global temporary table revenuereport_t (
  glaccountcategory               varchar2(4000),
  glaccountcatobjid               number(9),
  glaccountobjid                  number(9),
  revenuesum                      number(15, 2)
) on commit preserve rows;

grant select
on revenuereport_t
to public;

