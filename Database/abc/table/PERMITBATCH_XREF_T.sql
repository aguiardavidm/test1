-- Create table
create table PERMITBATCH_XREF_T
(
  renewalnotifbatchjobid NUMBER(9),
  permitobjectid         NUMBER(9),
  permittypeid           NUMBER(9)
)
tablespace LARGEDATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
-- Create/Recreate indexes 
create index PERMITBATCH_XREF_IX1 on PERMITBATCH_XREF_T (RENEWALNOTIFBATCHJOBID)
  tablespace LARGEDATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
create index PERMITBATCH_XREF_IX2 on PERMITBATCH_XREF_T (PERMITOBJECTID)
  tablespace LARGEDATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
create index PERMITBATCH_XREF_IX3 on PERMITBATCH_XREF_T (PERMITTYPEID)
  tablespace LARGEDATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 256K
    next 256K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
-- Grant/Revoke object privileges 
grant select, insert, update, delete, alter on PERMITBATCH_XREF_T to POSSEEXTENSIONS;
