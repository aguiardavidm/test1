-- Create table
create global temporary table PERMITBATCHJOB_GT
(
  permitobjectid         NUMBER(9),
  permittypeobjectid     NUMBER(9),
  permittype             VARCHAR2(50),
  expirationdate         DATE,
  relatedlicenseobjectid VARCHAR2(20)
)
on commit delete rows;
-- Create/Recreate indexes 
create index PERMITBATCHJOB_GT_IX1 on PERMITBATCHJOB_GT (PERMITOBJECTID);
create index PERMITBATCHJOB_GT_IX2 on PERMITBATCHJOB_GT (PERMITTYPEOBJECTID);
create index PERMITBATCHJOB_GT_IX3 on PERMITBATCHJOB_GT (RELATEDLICENSEOBJECTID);
-- Grant/Revoke object privileges 
grant select, insert, update, delete, alter on PERMITBATCHJOB_GT to POSSEEXTENSIONS;
