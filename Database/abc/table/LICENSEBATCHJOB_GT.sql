-- Create table
create global temporary table LICENSEBATCHJOB_GT
(
  licenseobjectid      NUMBER(9),
  licensetypeobjectid  NUMBER(9),
  licensetype          VARCHAR2(50),
  municipalityobjectid NUMBER(9),
  municipality         VARCHAR2(50),
  expirationdate       DATE,
  issuingauthority     VARCHAR2(20)
)
on commit delete rows;
-- Create/Recreate indexes 
create index LICENSEBATCHJOB_GT_IX1 on LICENSEBATCHJOB_GT (LICENSETYPE);
create index LICENSEBATCHJOB_GT_IX2 on LICENSEBATCHJOB_GT (MUNICIPALITY);
create index LICENSEBATCHJOB_GT_IX3 on LICENSEBATCHJOB_GT (ISSUINGAUTHORITY);
-- Grant/Revoke object privileges 
grant select, insert, update, delete, alter on LICENSEBATCHJOB_GT to POSSEEXTENSIONS;
