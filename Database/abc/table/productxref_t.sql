create table productxref_t (
  productxrefid                   number(9) not null,
  possexrefobjid                  number(9),
  renewaljobid                    number(9),
  productobjid                    number(9)
) tablespace largedata;

alter table productxref_t
add constraint sys_c0023992
primary key (
  productxrefid
) using index tablespace largedata;

