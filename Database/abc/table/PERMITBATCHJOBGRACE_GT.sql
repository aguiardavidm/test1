-- Create table
create global temporary table PERMITBATCHJOBGRACE_GT
(
  permitobjectid         NUMBER(9),
  permittypeobjectid     NUMBER(9),
  permittype             VARCHAR2(50),
  expirationdate         DATE,
  relatedlicenseobjectid VARCHAR2(20)
)
on commit delete rows;
-- Create/Recreate indexes 
create index PERMITBATCHJOBGRACE_GT_IX1 on PERMITBATCHJOBGRACE_GT (PERMITOBJECTID);
create index PERMITBATCHJOBGRACE_GT_IX2 on PERMITBATCHJOBGRACE_GT (PERMITTYPE);
create index PERMITBATCHJOBGRACE_GT_IX3 on PERMITBATCHJOBGRACE_GT (RELATEDLICENSEOBJECTID);
-- Grant/Revoke object privileges 
grant select, insert, update, delete, alter on PERMITBATCHJOBGRACE_GT to POSSEEXTENSIONS;
