create table onlineaccesscode (
  onlineaccesscode                number(8) not null
) tablespace mediumdata;

alter table onlineaccesscode
add constraint pk_onlineaccesscode
primary key (
  onlineaccesscode
) using index tablespace mediumindexes;

