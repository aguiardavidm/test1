create or replace TYPE             "UDT_OBJECT"                                          as object (
  ObjectId                              number(9)
);
/

grant execute
on udt_object
to public;

