create or replace package pkg_DashboardChartUtils is

  /*---------------------------------------------------------------------------
   * Constants - time period hack to get decent date for a demo
   *-------------------------------------------------------------------------*/
  gc_TimePeriodOffset constant number := 0;


  -- Author  : MICHAEL.FROESE
  -- Created : 7/21/2011 2:26:03 PM
  -- Purpose : Utils for Dashboard Charts

  subtype udt_Id                        is dashboard.pkg_DashboardDefinition.udt_Id;
  subtype udt_IdList                    is dashboard.pkg_DashboardDefinition.udt_IdList;
  subtype udt_StringList                is dashboard.pkg_DashboardDefinition.udt_StringList;
  subtype udt_NumberList                is dashboard.pkg_DashboardDefinition.udt_NumberList;
  subtype udt_ObjectList                is dashboard.udt_objectlist;
  subtype udt_DataMatrix                is dashboard.pkg_DashboardDefinition.udt_DataMatrix;
  subtype udt_UrlParameters             is dashboard.pkg_DashboardDefinition.udt_UrlParameters;
  subtype udt_Gauge                     is dashboard.pkg_DashboardDefinition.udt_Gauge;
  subtype udt_Data                      is dashboard.pkg_DashboardDefinition.udt_Data;
  subtype udt_DataList                  is dashboard.pkg_DashboardDefinition.udt_DataList;
  subtype udt_Chart                     is dashboard.pkg_DashboardDefinition.udt_Chart;
  subtype udt_NumberListByString        is dashboard.pkg_DashboardDefinition.udt_NumberListByString;

  gc_NullStringList			udt_StringList;

  /*---------------------------------------------------------------------------
   *   NewChart()
   *   Returns udt_Chart pre-set with details both from the o_DashboardChart
   *     corresponding to a_ChartId and the passed in Args.
   *-------------------------------------------------------------------------*/
  function NewChart(
    a_ChartId                           pls_integer,
    a_ChartArgs                         varchar2
  ) return udt_Chart;

  /*---------------------------------------------------------------------------
   * UserObjectList()
   * Returns a list of user objects.
   *-------------------------------------------------------------------------*/
  function UserObjectList(
    a_users                         varchar2
  ) return udt_ObjectList;

  /*---------------------------------------------------------------------------
   * GetUserDropDownListJSON()
   * Retuns users JSON result.
   *-------------------------------------------------------------------------*/
  function GetUserDropDownListJSON(
    a_AccessGroupName                  varchar2 default null,
    a_users                            varchar2 default null
    ) return clob;

  /*---------------------------------------------------------------------------
   *   toJSON()
   *   Returns a JSON object as a CLOB for the udt_Chart provided.
   *     Optionally specify true for the second argument to obtain a pretty-
   *     printed CLOB
   *-------------------------------------------------------------------------*/
  function ToJSON (
    a_Chart                             in udt_Chart,
    a_PrettyPrint                       boolean default false
  ) return clob;

  /*---------------------------------------------------------------------------
   *   ApplyDataMatrixToChart()
   *   Takes four arguments, a_Matrix (which is the result of
   *     pkg_DashboardUtils.Group%), a_Chart, a_xList and a_sgList.
   *     The procedure processes the matrix and autosets the fieldnames,
   *     serieslabels, and data with the contents of the matrix.
   *-------------------------------------------------------------------------*/
  procedure ApplyDataMatrixToChart(
    a_Matrix                            in udt_DataMatrix,
    a_Chart                             in out nocopy udt_Chart,
    a_xList                             in udt_NumberListByString,
    a_sgList                            in udt_NumberListByString
  );

  /*---------------------------------------------------------------------------
   * SetGaugeValues()
   *-------------------------------------------------------------------------*/
  procedure SetGaugeValues (
    a_Chart				                    in out nocopy udt_Chart,
    a_Value				                      number,
    a_MinValue				                  number default null,
    a_MaxValue				                  number default null,
    a_Threshold1                        number default null,
    a_Threshold2                        number default null
  );

  /*---------------------------------------------------------------------------
   * SetSingleValue()
   *-------------------------------------------------------------------------*/
  procedure SetSingleValue (
    a_Chart				in out nocopy udt_Chart,
    a_Value				number,
    a_xLabel                            varchar2
  );

  /*---------------------------------------------------------------------------
   * Split()
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                            varchar2,
    a_Separator                         varchar2
  ) return udt_StringList;

  /*---------------------------------------------------------------------------
   * DumpMatrix()
   *   For Testing
   *-------------------------------------------------------------------------*/
  procedure DumpMatrix( a_Matrix in udt_DataMatrix);

end pkg_DashboardChartUtils;

 
/

grant execute
on pkg_dashboardchartutils
to posseextensions;

create or replace package body pkg_DashboardChartUtils is

  /*---------------------------------------------------------------------------
   * Constants
   *-------------------------------------------------------------------------*/
  gc_Gauge				constant varchar2(10) := pkg_DashboardDefinition.gc_Gauge;

  /*---------------------------------------------------------------------------
   * ParseQueryString()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  function ParseQueryString (
    a_QueryString                       varchar2
  ) return udt_UrlParameters is
    t_UrlArgs                           udt_StringList;
    t_Args                              udt_UrlParameters;
    t_Name                              varchar2(4000);
    t_Value                             varchar2(4000);
    t_Index                             pls_integer;
  begin
    t_UrlArgs := Split(a_QueryString, '&');
    for i in 1..t_UrlArgs.count loop
      t_Index := instr(t_UrlArgs(i), '=');
      if t_Index > 1 then
        t_Name := substr(t_UrlArgs(i), 1, instr(t_UrlArgs(i), '=')-1);
        t_Value := utl_url.unescape(substr(t_UrlArgs(i), instr(t_UrlArgs(i), '=')+1));
        t_Args(upper(t_Name)) := t_Value;
      end if;
    end loop;
    return t_Args;
  end;

  /*---------------------------------------------------------------------------
   * CalculateTimePeriod() -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure CalculateTimePeriod(
    a_Chart                            in out nocopy udt_Chart
  ) is
  begin
    a_Chart.todate := trunc(sysdate);
    case
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Current week') or a_Chart.period = 'Current week' then
        a_Chart.fromdate := trunc(sysdate, 'IW');
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Current month') or a_Chart.period = 'Current month' then
        a_Chart.fromdate := trunc(sysdate, 'MM');
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Current quarter') or a_Chart.period = 'Current quarter' then
        a_Chart.fromdate := trunc(sysdate, 'Q');
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Current year') or a_Chart.period = 'Current year' then
        a_Chart.fromdate := trunc(sysdate, 'YY');
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Current year to date') or a_Chart.period = 'Current year to date' then
        a_Chart.fromdate := trunc(sysdate, 'YY');
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Previous week') or a_Chart.period = 'Previous week' then
        a_Chart.fromdate := trunc(sysdate - 7, 'IW');
        a_Chart.todate := a_Chart.fromdate + 6;
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Previous month') or a_Chart.period = 'Previous month' then
        a_Chart.fromdate := trunc(add_months(sysdate, -1), 'MM');
        a_Chart.todate := trunc(sysdate, 'MM') - 1;
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Previous quarter') or a_Chart.period = 'Previous quarter' then
        a_Chart.fromdate := trunc(add_months(sysdate,-3), 'Q');
        a_Chart.todate := trunc(sysdate, 'Q') - 1;
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Previous year') or a_Chart.period = 'Previous year' then
        a_Chart.fromdate := trunc(add_months(sysdate, -12), 'YY');
        a_Chart.todate := trunc(sysdate, 'YY') - 1;
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Last 3 months') or a_Chart.period = 'Last 3 months' then
        a_Chart.fromdate := trunc(add_months(sysdate,-3), 'MM');
        a_Chart.todate := trunc(sysdate, 'MM')-1;
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Last 6 months') or a_Chart.period = 'Last 6 months' then
        a_Chart.fromdate := trunc(add_months(sysdate,-6), 'MM');
        a_Chart.todate := trunc(sysdate, 'MM')-1;
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Last 12 months') or a_Chart.period = 'Last 12 months' then
        a_Chart.fromdate := trunc(add_months(sysdate,-12), 'MM');
        a_Chart.todate := trunc(sysdate, 'MM')-1;
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Previous fiscal year') or a_Chart.period = 'Previous fiscal year' then
        a_Chart.fromdate := trunc(add_months(a_Chart.FiscalYearEndDate, -11), 'MM');
        a_Chart.todate := a_Chart.FiscalYearEndDate;
      when (a_Chart.period is null and a_Chart.defaultperiod = 'Next fiscal year') or a_Chart.period = 'Next fiscal year' then
        a_Chart.fromdate := a_Chart.FiscalYearEndDate + 1;
        a_Chart.todate := trunc(add_months(a_Chart.FiscalYearEndDate, 12), 'MM');
      when a_Chart.period = 'Custom' then
        null; --this is already taken care of in ApplyArgs
      else
        null;
    end case;

    a_Chart.fromdate_ := to_char(a_Chart.fromdate, 'mm/dd/yyyy');
    a_Chart.todate_   := to_char(a_Chart.todate, 'mm/dd/yyyy');
  end;

  /*---------------------------------------------------------------------------
   * CalculateGaugeTimePeriod()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure CalculateGaugeTimePeriod(
    a_Chart                            in out nocopy udt_Chart
  )  is
    t_Delta                             pls_integer :=
        a_Chart.GaugeCalculationPrevXDays;
    t_PrevFromDate                      date;
    t_TruncBy                           varchar2(10);
  begin
    -- exit the procedure if the time period does not apply
    if a_Chart.Charttype = 'WarningGaugeCustom' then
      return;
    end if;

    t_PrevFromDate := sysdate;

    if a_Chart.GaugeCalculationTimePeriod = 'Day' then
      t_TruncBy := 'DD';
      t_PrevFromDate := t_PrevFromDate + (t_Delta * -1);
    elsif a_Chart.GaugeCalculationTimePeriod = 'Week' then
      t_TruncBy := 'IW';
      t_PrevFromDate := t_PrevFromDate + (t_Delta * -7);
    elsif a_Chart.GaugeCalculationTimePeriod = 'Month' then
      t_TruncBy := 'MM';
      t_PrevFromDate := add_months(t_PrevFromDate,(t_Delta * -1));
    elsif a_Chart.GaugeCalculationTimePeriod = 'Quarter' then
      t_TruncBy := 'Q';
      t_PrevFromDate := add_months(t_PrevFromDate,(t_Delta * -3));
    elsif a_Chart.GaugeCalculationTimePeriod = 'Year' then
      t_TruncBy := 'YYYY';
      t_PrevFromDate := add_months(t_PrevFromDate,(t_Delta * -12));
    end if;

    if a_Chart.GaugeCalculationCurrPrev = 'Current' then
      a_Chart.ToDate := trunc(sysdate);
      a_Chart.FromDate := trunc(sysdate, t_TruncBy);
    else
      a_Chart.ToDate := trunc(sysdate,t_TruncBy)-1;
      a_Chart.FromDate := trunc(t_PrevFromDate, t_TruncBy);
    end if;

    a_Chart.fromdate_ := to_char(a_Chart.fromdate, 'mm/dd/yyyy');
    a_Chart.todate_   := to_char(a_Chart.todate, 'mm/dd/yyyy');
  end;

  /*---------------------------------------------------------------------------
   * ApplyArgs()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure ApplyArgs(
    a_Chart                            in out nocopy udt_Chart,
    a_ChartArgs                        varchar2
  ) is
    t_Args                             udt_URLParameters;
    t_NotTopNList                      udt_StringList;
    t_Value                            varchar2(100);
    t_Count                            number := 0;
  begin
    t_Args := ParseQueryString(a_ChartArgs); 
    if t_Args.exists('NOTTOPNVALUES') then 
      a_Chart.nottopnvalues := replace(t_Args('NOTTOPNVALUES'), '+', ' ');
    end if;
    if t_Args.exists('GROUP1VALUE') then
         a_Chart.groupvalues(1) := replace(t_Args('GROUP1VALUE'), '+', ' ');
    end if;
    if t_Args.exists('GROUP2VALUE') then
      a_Chart.groupvalues(2) := replace(t_Args('GROUP2VALUE'), '+', ' ');
    end if;
    if t_Args.exists('GROUP3VALUE') then
      a_Chart.groupvalues(3) := replace(t_Args('GROUP3VALUE'), '+', ' ');
    end if;
    if t_Args.exists('GROUP4VALUE') then
      a_Chart.groupvalues(4) := replace(t_Args('GROUP4VALUE'), '+', ' ');
    end if;
    
    
    if t_Args.exists('GROUP1NOTTOPNVALUES') then
         a_Chart.groupnottopnvalues(1) := replace(t_Args('GROUP1NOTTOPNVALUES'), '+', ' ');
    end if;
    if t_Args.exists('GROUP2NOTTOPNVALUES') then
      a_Chart.groupnottopnvalues(2) := replace(t_Args('GROUP2NOTTOPNVALUES'), '+', ' ');
    end if;
    if t_Args.exists('GROUP3NOTTOPNVALUES') then
      a_Chart.groupnottopnvalues(3) := replace(t_Args('GROUP3NOTTOPNVALUES'), '+', ' ');
    end if;
    if t_Args.exists('GROUP4NOTTOPNVALUES') then
      a_Chart.groupnottopnvalues(4) := replace(t_Args('GROUP4NOTTOPNVALUES'), '+', ' ');
    end if;
       
    if t_Args.exists('PERIOD') then
      a_Chart.period := replace(t_Args('PERIOD'),'+', ' ');
    end if;

    if t_Args.exists('USERS') then
      a_Chart.users_ := t_Args('USERS');
    end if;

    if t_Args.exists('VIEW') then
      a_Chart.view_ := t_Args('VIEW');
    end if;
    
    if a_Chart.period = 'Custom' then
      if t_Args.exists('FROMDATE') then
        a_Chart.fromdate_ := t_Args('FROMDATE');
        a_Chart.fromdate := to_date(a_Chart.fromdate_, 'mm/dd/yyyy');
      end if;

      if t_Args.exists('TODATE') then
        a_Chart.todate_ := t_Args('TODATE');
        a_Chart.todate := to_date(a_Chart.todate_, 'mm/dd/yyyy');
      end if;

    elsif a_Chart.period = 'All' then
      null;
    elsif a_Chart.displaytype = gc_Gauge then
      CalculateGaugeTimePeriod( a_Chart );
    else
      CalculateTimePeriod( a_Chart );
    end if;
  end;

  /*---------------------------------------------------------------------------
   * Collapse()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  function collapse(
    a_StringList                       udt_StringList
  ) return clob is
    t_Clob                             clob;
    i                                  pls_integer;
  begin
    for i in 1..a_StringList.count loop
      if i = 1 then
        t_Clob := a_StringList(1);
      else
        t_Clob := t_Clob || ','||a_StringList(i);
      end if;
    end loop;
    return t_Clob;
  end collapse;

  /*---------------------------------------------------------------------------
   * NewChart()
   *-------------------------------------------------------------------------*/
  function NewChart(
    a_ChartId                           pls_integer,
    a_ChartArgs                         varchar2
  ) return udt_Chart is
    t_Chart                             udt_Chart;
  begin
    select
      ObjectId,
      AccessGroup,
      AlwaysShowGroup1Values,
      AlwaysShowGroup2Values,
      AlwaysShowGroup3Values,
      AlwaysShowGroup4Values,
      Calculation,
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,CalculationDetail),
      ChartTypeName,
      CorralObjectIdColumnName,
      CorralSchemaName,
      CorralTableName,
      DefaultTimePeriod,
      DisplayAsPercentage,
      DisplayTwoGroups,
      DisplayTypeName,
      DrillDownURL,
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,EndDateDetail),
      EndStatusNames,
      ExcludeStatusNames,
      GaugeMin,
      GaugeMax,
      GaugeCalculationCurrPrev,
      GaugeCalculationName,
      GaugeCalculationPrevXDays,
      GaugeCalculationTimePeriod,
      GaugeLevel1Threshold,
      GaugeLevel2Threshold,
      GaugeSql,
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,Group1Expression),
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,Group2Expression),
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,Group3Expression),
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,Group4Expression),
      Group1Label,
      Group2Label,
      Group3Label,
      Group4Label,
      IncludeCancelled,
      OnlyIncludeScheduled,
      ShowTwoGroups,
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,StartDateDetail),
      JobStatusNames,
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,GroupDetail),
      pkg_dashboardutils.GetExpressionSyntax(a_ChartId,TimePeriodDetail),
      Title,
      WorkingOrCalendarDays,
      XAxisLabel,
      YAxisLabel,
      ObjectTypeNames,
      DefaultTopNValues,
      DefaultOrderBy,
      ValueFormat,
      DecimalPlaces,
      OnlyJobTypeSelected
    into
      t_Chart.ChartId,
      t_Chart.AccessGroup,
      t_Chart.ShowGroupValues(1),
      t_Chart.ShowGroupValues(2),
      t_Chart.ShowGroupValues(3),
      t_Chart.ShowGroupValues(4),
      t_Chart.Calculation,
      t_Chart.CalculationDetail,
      t_Chart.ChartType,
      t_Chart.CorralObjectIdColumnName,
      t_Chart.CorralSchemaName,
      t_Chart.CorralTableName,
      t_Chart.DefaultPeriod,
      t_Chart.DisplayAsPercentage,
      t_Chart.DisplayTwoGroups,
      t_Chart.DisplayType,
      t_Chart.DrillDownURL,
      t_Chart.EndDateDetail,
      t_Chart.EndStatus,
      t_Chart.ExcludeStatus,
      t_Chart.Gauge.Min,
      t_Chart.Gauge.Max,
      t_Chart.GaugeCalculationCurrPrev,
      t_Chart.GaugeCalculationName,
      t_Chart.GaugeCalculationPrevXDays,
      t_Chart.GaugeCalculationTimePeriod,
      t_Chart.GaugeLevel1Threshold,
      t_Chart.GaugeLevel2Threshold,
      t_Chart.GaugeSql,
      t_Chart.GroupExpressions(1),
      t_Chart.GroupExpressions(2),
      t_Chart.GroupExpressions(3),
      t_Chart.GroupExpressions(4),
      t_Chart.GroupLabels(1),
      t_Chart.GroupLabels(2),
      t_Chart.GroupLabels(3),
      t_Chart.GroupLabels(4),
      t_Chart.IncludeCancelled,
      t_Chart.OnlyIncludeScheduled,
      t_Chart.ShowTwoGroups,
      t_Chart.StartDateDetail,
      t_Chart.StartStatus,
      t_Chart.SubGroup,
      t_Chart.TimePeriodDetail,
      t_Chart.Title,
      t_Chart.WorkingOrCalendarDays,
      t_Chart.XAxisLabel,
      t_Chart.YAxisLabel,
      t_Chart.ObjectTypes,
      t_Chart.DefaultTopNValues,
      t_Chart.DefaultOrderBy,
      t_Chart.ValueFormat,
      t_Chart.DecimalPlaces,
      t_Chart.OnlyJobTypeSelected
    from o_DashboardChart
    where ObjectId = a_ChartId;
    
    select value
      into t_Chart.FiscalYearEndDate
      from dashboard.Constants
     where name = 'FiscalYearEndDate';
      
    select od.ObjectDefId
      bulk collect into t_Chart.JobTypeIds
      from
        r_DashboardChartObjectType r
        join o_DashboardObjectType ot
          on ot.ObjectId = r.DashboardObjectTypeId
        join ObjectDefs od
          on od.ObjectDefId = ot.ExternalObjectDefId
      where r.DashboardChartId = a_ChartId
        and od.ObjectDefTypeId = 2;

    select od.ObjectDefId
      bulk collect into t_Chart.ProcessTypeIds
      from
        r_DashboardChartObjectType r
        join o_DashboardObjectType ot
          on ot.ObjectId = r.DashboardObjectTypeId
        join ObjectDefs od
          on od.ObjectDefId = ot.ExternalObjectDefId
      where r.DashboardChartId = a_ChartId
        and od.ObjectDefTypeId = 3;

    select s.StatusId
      bulk collect into t_Chart.StartStatusIds
      from
        r_DashboardChartChartStatus r
        join o_DashboardJobStatus cs
          on cs.ObjectId = r.DashboardJobStatusId
        join Statuses s
          on s.Tag = cs.Tag
      where r.DashboardChartId = a_ChartId;
    
    -- Grouping
    for i in 1..4 loop
      if t_Chart.GroupExpressions(i) is not null
          and t_Chart.GroupLabels(i) is null then
-- FIX ME - GroupLabels was previously set here - any extjs code that depends on no array element?
        raise_application_error(-20000,
            'A label must be configured from Group ' || i ||
            ' on Chart ' || a_ChartId);
      end if;
    end loop;

    -- Gauges
    t_Chart.MaxYValue := 0;
    if t_Chart.DisplayType = gc_Gauge then
-- FIX ME - Gauge.min/max were previously set here - any extjs code that depends on nulls??
      if t_Chart.DisplayAsPercentage = 'Y' then
        t_Chart.Gauge.pct := 'true';
      end if;
    end if;

    ApplyArgs(t_Chart, a_ChartArgs);

    /* Time period hack for demo */
/*    if t_Chart.FromDate is not null then
      t_Chart.FromDate := add_months(t_Chart.FromDate, gc_TimePeriodOffset);
    end if;
    if t_Chart.ToDate is not null then
      t_Chart.ToDate := add_months(t_Chart.ToDate, gc_TimePeriodOffset);
    end if;*/

    return t_Chart;
  end;

  /*---------------------------------------------------------------------------
   * UserObjectList()
   *-------------------------------------------------------------------------*/
  function UserObjectList(
    a_users                             varchar2
  ) return udt_ObjectList is
    t_ObjectList                        udt_ObjectList := dashboard.udt_objectlist();
    t_users                             udt_StringList;
  begin
    t_users := Split(a_users, ',');
    t_ObjectList.Extend(t_users.Count);
    for i in 1..t_users.count loop
      t_ObjectList(i) := dashboard.udt_object(t_users(i));
    end loop;
    return t_ObjectList;
  end;

  /*---------------------------------------------------------------------------
   * GetUserDropDownListJSON()
   *-------------------------------------------------------------------------*/
  function GetUserDropDownListJSON(
    a_AccessGroupName                   varchar2 default null,
    a_users                             varchar2 default null
  ) return clob is
    t_JSON                              clob;
    t_UserIds                           udt_IdList;
    t_Names                             udt_StringList;
  begin
    if a_AccessGroupName is null then
       select
         u.UserId,
         u.Name
       bulk collect into
         t_UserIds,
         t_Names
       from dashboard.users u
       where u.Active = 'Y'
         order by u.Name;
    else
       if a_users is not null then
         select
           u.UserId,
           u.Name
         bulk collect into
           t_UserIds,
           t_Names
         from dashboard.users u
           join table(pkg_DashboardChartUtils.UserObjectList(a_users)) argusers
             on argusers.ObjectId = u.UserId
           join dashboard.accessgroupusers agu
             on agu.UserId = argusers.ObjectId
            and u.Active = 'Y'
           join dashboard.accessgroups ag
             on ag.AccessGroupId = agu.AccessGroupId
          where upper(ag.Description) = upper(a_AccessGroupName)
          order by u.Name;
        else
         select
           u.UserId,
           u.Name
         bulk collect into
           t_UserIds,
           t_Names
         from dashboard.users u
           join dashboard.accessgroupusers agu
             on agu.UserId = u.UserId
            and u.Active = 'Y'
           join dashboard.accessgroups ag
             on ag.AccessGroupId = agu.AccessGroupId
          where upper(ag.Description) = upper(a_AccessGroupName)
          order by u.Name;
        end if;
     end if;

     t_JSON :=
       ' users: [';
       for i in 1..t_UserIds.count loop
          t_JSON := concat(t_JSON, '{id: ' || t_UserIds(i) || ', name: "' || t_Names(i) || '"},');

       end loop;

       if t_UserIds.count > 0 then
          t_JSON := concat(substr(t_JSON, 1, length(t_JSON)-1), ']');
       else
          t_JSON := concat(t_JSON,']');
       end if;

    return t_JSON;
  end;

  /*---------------------------------------------------------------------------
   * ToJSON()
   *-------------------------------------------------------------------------*/
  function EscapeString(str varchar2) return varchar2 as
    sb varchar2(32767) := '';
    buf varchar2(40);
    num number;
  begin
    if(str is null) then return ''; end if;
    for i in 1 .. length(str) loop
      buf := substr(str, i, 1);
      --backspace b = U+0008
      --formfeed  f = U+000C
      --newline   n = U+000A
      --carret    r = U+000D
      --tabulator t = U+0009
      case buf
      when chr( 8) then buf := '\b';
      when chr( 9) then buf := '\t';
      when chr(10) then buf := '\n';
      when chr(13) then buf := '\f';
      when chr(14) then buf := '\r';
      when chr(34) then buf := '\"';
      --when chr(47) then if(escape_solidus) then buf := '\/'; end if;
      when chr(92) then buf := '\\';
      else 
        if(ascii(buf) < 32) then
          buf := '\u'||replace(substr(to_char(ascii(buf), 'XXXX'),2,4), ' ', '0');
        else
          buf := replace(asciistr(buf), '\', '\u');
        end if;
      end case;      
      
      sb := sb || buf;
    end loop;
  
    return sb;
  end EscapeString;

  /*---------------------------------------------------------------------------
   * ToJSON()
   *-------------------------------------------------------------------------*/
  function ToJSON (
    a_Chart                             in udt_Chart,
    a_PrettyPrint                       boolean default false
  ) return clob is
    t_JSON                              clob;
    t_JSON_users                        clob;
    t_JSON_tmp                          varchar2(32766);
    t_CR                                varchar2(1) := chr(13);
    t_Tab                               varchar2(1) := chr(9);
    i                                   pls_integer;
    j                                   pls_integer;
    x                                   varchar2(100);
    t_userId                            udt_Id := dashboard.pkg_securityquery.EffectiveUserId();
    t_NotTopNList                       varchar2(1000);
    t_Value                             varchar2(100);
    t_exitFlag                          varchar2(1) := 'N';
  begin
    if not a_PrettyPrint then
      t_CR := '';
      t_Tab := '';
    end if;

    /*  Note, that || is a string operator and is limited to 32k ... so when we
     *  build the data, we will use a temp string and concat() it to the clob
     */
    --Build JSON clob
    t_JSON := '{'    ||t_CR;
    t_JSON := t_JSON || t_Tab || 'widgetid: '       || a_Chart.Chartid      ||','  || t_CR;
    t_JSON := t_JSON || t_Tab || 'widgettype: "'    || a_Chart.Charttype    ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'visible: '       || case when a_Chart.visible = 'N' then 'false' else 'true' end       ||',' || t_CR;
    t_JSON := t_JSON || t_Tab || 'category: "'      || a_Chart.category      ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'title: "'         || a_Chart.title         ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'displaytype: "'   || a_Chart.displaytype   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'accessgroup: "'   || a_Chart.accessgroup   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'xaxislabel: "'    || EscapeString(a_Chart.xaxislabel)    ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'yaxislabel: "'    || EscapeString(a_Chart.yaxislabel)    ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'defaultperiod: "' || a_Chart.defaultperiod ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'timeperioddetail: "' || a_Chart.timeperioddetail ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'period: "'        || a_Chart.period        ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'fromdate: "'      || a_Chart.fromdate_     ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'todate: "'        || a_Chart.todate_       ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'view: "'          || a_Chart.view_         ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'user: "'          || t_userId               ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'subgroup: "'      || a_Chart.subgroup      ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'maxyvalue: "'     || a_Chart.maxyvalue     ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'drilldownurl: "'  || a_Chart.drilldownurl  ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'fieldnames: "'    || collapse(a_Chart.fieldnames)   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'serieslabels: "'  || EscapeString(collapse(a_Chart.serieslabels)) ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'displaytwogroups: "'|| a_Chart.displaytwogroups     ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'fiscalyearenddate: "'|| a_Chart.fiscalyearenddate   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'defaulttopnvalues: "'|| a_Chart.defaulttopnvalues   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'defaultorderby: "'|| a_Chart.defaultorderby   ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'valueformat: "'|| nvl(a_Chart.valueformat,0) ||'",' || t_CR;
    t_JSON := t_JSON || t_Tab || 'decimalplaces: "'|| a_Chart.decimalplaces ||'",' || t_CR;
   
    t_Value := a_Chart.NotTopNList.first();
    while t_Value is not null loop
      if t_NotTopNList is not null then
        t_NotTopNList := t_NotTopNList ||',' ||t_Value;
      else
        t_NotTopNList := t_Value;
      end if;
      t_Value := a_Chart.NotTopNList.next(t_Value);
    end loop;
    
    t_JSON := t_JSON || t_Tab || 'nottopnvalues: "'|| EscapeString(t_NotTopNList)  ||'",' || t_CR;
    -- Group labels
    for i in 1..4 loop
      if a_Chart.grouplabels.exists(i) then
        t_JSON := t_JSON || t_Tab || 'group'||i||'label: "'  || EscapeString(a_Chart.grouplabels(i)) ||'",' || t_CR;
      end if;
    end loop;
    
    -- Group Values
    for i in 1..4 loop
      if a_Chart.groupvalues.exists(i) then
        t_JSON := t_JSON || t_Tab || 'group'||i||'value: "'  || EscapeString(a_Chart.groupvalues(i)) ||'",' || t_CR;
      end if;
    end loop;
    
    -- Group Not Top N Values
    for i in 1..4 loop
      if a_Chart.GroupNotTopNValues.exists(i) then
        t_JSON := t_JSON || t_Tab || 'group'||i||'nottopnvalues: "'  || EscapeString(a_Chart.GroupNotTopNValues(i)) ||'",' || t_CR;
      end if;
    end loop;

    --Gauge object
    t_JSON := t_JSON || t_Tab || 'gauge: '          || '{'                  || t_CR;
    t_JSON := t_JSON || t_Tab || t_Tab || 'min: '   || nvl(a_Chart.gauge.min,0)   ||','  || t_CR;
    t_JSON := t_JSON || t_Tab || t_Tab || 'max: '   || nvl(a_Chart.gauge.max,1)   ||','  || t_CR;
    t_JSON := t_JSON || t_Tab || t_Tab || 'alert: ' || a_Chart.gauge.alert ||','  || t_CR;
    t_JSON := t_JSON || t_Tab || t_Tab || 'pct: '   || a_Chart.gauge.pct   || t_CR;
    t_JSON := t_JSON || t_Tab || '},'  || t_CR;
    --Data object array
    t_JSON := t_JSON || t_Tab || 'data: ' || '[' || t_CR;
    for i in 1..a_Chart.data.count loop
      t_exitFlag := 'N';
      if a_Chart.data(i).x = 'zzzzz' and a_Chart.ChartType = 'ObjectSummary' then
         x := 'Other';
      elsif a_Chart.data(i).x = 'zzzzzz' and a_Chart.ChartType = 'ObjectSummary' then
         t_exitFlag := 'Y';
      else
        x := a_Chart.data(i).x;
      end if;
        if t_exitFlag = 'N' then
          t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab || '{' || t_CR;
          t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab || t_Tab|| 'x: "' || EscapeString(x) ||'",' || t_CR;
          for j in 1..a_Chart.data(i).y.count loop
            if j = a_Chart.data(i).y.count then
              if a_Chart.data(i).y(j) is not null then
                t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab || t_Tab|| 'y'||j||': ' || a_Chart.data(i).y(j) || t_CR;
              end if;
            else
              if a_Chart.data(i).y(j) is not null then
                --dbms_output.put_line(i||'-'||j||': '||a_Chart.data(i).y(j)||':::'||length(t_JSON_tmp));
                t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab || t_Tab|| 'y'||j||': ' || a_Chart.data(i).y(j) ||',' || t_CR;
              end if;
            end if;
          end loop;
          if i = a_Chart.data.count or (i = (a_Chart.data.count - 1) and a_Chart.data(i+1).x = 'zzzzzz') then
            t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab|| '}' || t_CR;
          else
            t_JSON_tmp := t_JSON_tmp || t_Tab || t_Tab|| '},'|| t_CR;
          end if;
          t_JSON := concat( t_JSON, t_JSON_tmp );
          t_JSON_tmp := '';
        end if;
    end loop;

    t_JSON := concat( t_JSON, t_Tab || ']');

    if a_Chart.Charttype = 'ToDoListSummary' then
       t_JSON_users := GetUserDropDownListJSON(a_Chart.accessgroup,a_Chart.users_);
       t_JSON := concat(t_JSON,','||t_JSON_users);
    end if;

    t_JSON := concat( t_JSON, t_CR || '}');

    return t_JSON;
  end;

  /*---------------------------------------------------------------------------
   * DumpMatrix()
   *-------------------------------------------------------------------------*/
  procedure DumpMatrix( a_Matrix in udt_DataMatrix) is
    t_x             varchar2(1000);
    t_y             varchar2(1000);
  begin
    t_x := a_Matrix.first();
    while t_x is not null loop
      dbms_output.put_line(t_x);
      t_y := a_Matrix(t_x).first();
      while t_y is not null loop
        dbms_output.put_line('  ' || t_y || ':' || a_Matrix(t_x)(t_y));
        t_y := a_Matrix(t_x).next(t_y);
      end loop;
      t_x := a_Matrix.next(t_x);
    end loop;
  end;

  /*---------------------------------------------------------------------------
   * AutoSetFieldNames()
   *   Auto sets the fieldnames attribute of a Chart based on how many
   *     serieslabels are present
   *-------------------------------------------------------------------------*/
  procedure AutoSetFieldNames(
    a_Chart                           in out nocopy udt_Chart
  ) is
    t_StringList                       udt_StringList;
    i                                  pls_integer;
  begin
    a_Chart.fieldnames(1) := 'x';
    if a_Chart.serieslabels.count = 0 then
      a_Chart.fieldnames(2) := 'y1';
    else
      for i in 1..a_Chart.serieslabels.count loop
        a_Chart.fieldnames(i+1) := 'y'||i;
      end loop;
    end if;
  end;

  /*---------------------------------------------------------------------------
   * ApplyDataMatrixToChart()
   *-------------------------------------------------------------------------*/
  procedure ApplyDataMatrixToChart(
    a_Matrix                            in udt_DataMatrix,
    a_Chart                            in out nocopy udt_Chart,
    a_xList                             in udt_NumberListByString,
    a_sgList                            in udt_NumberListByString
  ) is
    t_x             varchar2(100);
    t_sg            varchar2(100);
  begin
    -- Note that xList and sgList are sparse arrays indexed by the group or
    -- sub-group value with the seqnum being the data portion
    -- e.g. ['East' : 2, 'North' : 1, 'South' : 3, 'West' : 4]

    -- define the series labels on the Chart when more than one
    if a_sgList.count > 1 then
      t_sg := a_sgList.first();
      while t_sg is not null loop
        -- when done this array should be dense
        a_Chart.SeriesLabels(a_sgList(t_sg)) := t_sg;
        t_sg := a_sgList.next(t_sg);
      end loop;
      end if;
   
    -- load the Chart data structure from the Matrix using the Orderd x and sg
    -- lists to indicate the array positions
    t_x := a_xList.first();
      while t_x is not null loop
        t_sg := a_sgList.first();
        while t_sg is not null loop
          a_Chart.Data(a_xList(t_x)).x := t_x;
          a_Chart.Data(a_xList(t_x)).y(a_sgList(t_sg)) := a_Matrix(t_x)(t_sg);
          a_Chart.MaxYValue := greatest(a_Chart.MaxYValue, a_Matrix(t_x)(t_sg));
          t_sg := a_sgList.next(t_sg);
        end loop;
        t_x := a_xList.next(t_x);
      end loop;

    AutoSetFieldNames( a_Chart );

  end ApplyDataMatrixToChart;

  /*---------------------------------------------------------------------------
   * SetGaugeValues()
   *-------------------------------------------------------------------------*/
  procedure SetGaugeValues (
    a_Chart                             in out nocopy udt_Chart,
    a_Value                             number,
    a_MinValue                          number default null,
    a_MaxValue                          number default null,
    a_Threshold1                        number default null,
    a_Threshold2                        number default null
  ) is
    t_Threshold1                        number;
    t_Threshold2                        number;
    t_Value                             number;
    t_Alert				number := 0;
  begin
    t_Threshold1 := nvl(a_Threshold1, nvl(a_Chart.GaugeLevel1Threshold, 0));
    t_Threshold2 := nvl(a_Threshold2, nvl(a_Chart.GaugeLevel2Threshold, 0));

    a_Chart.fieldnames(1) := 'x';
    a_Chart.fieldnames(2) := 'y1';
    a_Chart.data(1).x := 'Needle';

    if a_Chart.gauge.pct = 'true' then
      a_Chart.gauge.min := 0;
      a_Chart.gauge.max := 100;
      if a_Value = 0 then
        a_Chart.data(1).y(1) := 0;
      else
        a_Chart.data(1).y(1) := round(
            ((a_Value - a_MinValue)/(a_MaxValue - a_MinValue))*100, 0);
      end if;
    else
      a_Chart.gauge.min := nvl(a_MinValue, a_Chart.gauge.min);
      a_Chart.gauge.max := nvl(a_MaxValue, a_Chart.gauge.max);
      a_Chart.gauge.max := greatest(a_Chart.gauge.max, a_Value);
      a_Chart.data(1).y(1) := a_Value;
    end if;
    t_Value := a_Chart.data(1).y(1);

    -- ensure the gauge max is at least 1
    a_Chart.gauge.max := greatest(a_Chart.gauge.max, 1);

    -- set Alert value
    if t_Threshold1 is not null and t_Threshold1 < t_Threshold2 then
      if t_Value < t_Threshold1 then
        t_Alert := 0;
      elsif t_Value < t_Threshold2 then
        t_Alert := 1;
      else
        t_Alert := 2;
      end if;
    elsif t_Threshold1 is not null then
      if t_Value < t_Threshold2 then
        t_Alert := 2;
      elsif t_Value < t_Threshold1 then
        t_Alert := 1;
      else
        t_Alert := 0;
      end if;
    end if;
    a_Chart.gauge.alert := t_Alert;
  end SetGaugeValues;

  /*---------------------------------------------------------------------------
   * SetSingleValue()
   *-------------------------------------------------------------------------*/
  procedure SetSingleValue (
    a_Chart				in out nocopy udt_Chart,
    a_Value				number,
    a_xLabel                            varchar2
  ) is
  begin
    a_Chart.Data(1).x := a_xLabel;
    a_Chart.Data(1).y(1) := a_Value;
    a_Chart.MaxYValue := a_Value;
    AutoSetFieldNames(a_Chart);
  end SetSingleValue;

  /*---------------------------------------------------------------------------
   * Split()
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                            varchar2,
    a_Separator                         varchar2
  ) return udt_StringList is
    t_List                              udt_StringList;
    t_Pos                               number;
    t_Index                             number;
    t_SepLen                            number;
  begin
    t_SepLen := length(a_Separator);
    t_Pos := 0;
    while t_Pos <= length(a_String) loop
      t_Index := instr(a_String, a_Separator, t_Pos+1);
      if t_Index > 0 then
        t_List(t_List.count+1) := substr(a_String, t_Pos+1, t_Index-t_Pos-1);
        t_Pos := t_Index + t_SepLen - 1;
      else
        t_List(t_List.count+1) := substr(a_String, t_Pos+1);
        t_Pos := length(a_String) + 1;
      end if;
    end loop;

    return t_List;
  end Split;

end pkg_DashboardChartUtils;

/

