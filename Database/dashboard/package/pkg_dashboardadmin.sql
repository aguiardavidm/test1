create or replace package pkg_DashboardAdmin is

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_IdList is pkg_DashboardDefinition.udt_IdList;
  subtype udt_Id is pkg_DashboardDefinition.udt_Id;

   /*---------------------------------------------------------------------------
   * VerifyAccessGroup()
   *-------------------------------------------------------------------------*/
  procedure VerifyAccessGroup ( a_ObjectId  udt_Id,
                                a_AsofDate  Date,
                                a_AccessGroupName varchar2);
                                
  procedure SecureDashboard (  a_ObjectId  udt_Id,
                                a_AsofDate  Date);

end pkg_DashboardAdmin;

 
/

grant execute
on pkg_dashboardadmin
to posseextensions;

create or replace package body pkg_DashboardAdmin is

  /*---------------------------------------------------------------------------
   * VerifyAccessGroup()
   *-------------------------------------------------------------------------*/
  procedure VerifyAccessGroup ( a_ObjectId  udt_Id,
                                a_AsofDate  Date,
                                a_AccessGroupName varchar2)
   is
  t_Count                   number;
  t_AccessGroupId           number;
  begin

  select count(ag.AccessGroupId)
   into t_Count
  from api.accessgroups ag
  where ag.Description = trim(a_AccessGroupName);

  if t_Count = 0 then
    api.pkg_errors.RaiseError(-20001, 'Access group "'||a_AccessGroupName||'" does not exist.');
  elsif t_Count = 1 then 
    select ag.AccessGroupId
       into t_AccessGroupId
    from api.accessgroups ag
    where ag.Description = trim(a_AccessGroupName);   
        api.pkg_columnupdate.SetValue(a_ObjectId,'AccessGroupID',t_AccessGroupId);
  else
    --this should never happen
    api.pkg_errors.RaiseError(-20001, 'More than one Security Group with the name "'||a_AccessGroupName||'" exists.');
  end if;

  end VerifyAccessGroup;
 /*---------------------------------------------------------------------------
   * SecureDashboard()
   *-------------------------------------------------------------------------*/
   
   procedure SecureDashboard (
    a_ObjectId                          udt_Id,
    a_AsofDate                          date
  ) is
    t_SecurityOverridden                char(1);
    t_SecurityGroupsCount               number;
    t_SuperUserAccessGp                 number;
    t_DashboardAdminAccessGp            number;
      
    cursor c_ObjectInstanceSecurity is
        select 'Read' Name, ag.AccessGroupId
      from api.relationships r
      join query.o_dashboardaccessgroup dag on r.ToObjectId = dag.ObjectId
      join api.accessgroups ag on ag.Description = dag.Description
      where r.FromObjectId = a_objectId;    

  begin
  
  t_SecurityOverridden := api.pkg_columnquery.Value(a_objectid, 'OverrideDefaultSecurity');
  api.pkg_ObjectUpdate.RevertToDefaultSecurity(a_ObjectId);
           
  
    if t_SecurityOverridden = 'Y' then
      --Grant Dashboard Admin Access
      for r in ( select a.AccessGroupId, o.Name
                       from api.ObjectSecurity o
                       inner join api.AccessGroups a
                        on a.AccessGroupId = o.AccessGroupId   
                        where o.ObjectId = a_ObjectId
                        and a.Description = 'Dashboard Admin'
                        and o.Name <> 'Insert') loop
                                                
           api.pkg_ObjectUpdate.GrantPrivilege(a_ObjectId, r.AccessGroupId, r.Name);             
       end loop;
      --Check to see if there are related dashboard access groups     
      select count(ag.AccessGroupId)
      into t_SecurityGroupsCount
      from api.relationships r
      join query.o_dashboardaccessgroup dag on r.ToObjectId = dag.ObjectId
      join api.accessgroups ag on ag.Description = dag.Description
      where r.FromObjectId = a_objectId;     
     
      if t_SecurityGroupsCount > 0 then          
        
        Select distinct a.accessgroupid
          into t_DashboardAdminAccessGp
          from api.accessgroups a
          where a.description like 'Dashboard Admin'; 
        /* Apply the security  if the access group is not Dashboard Admin*/
        for s in c_ObjectInstanceSecurity loop
          if s.accessgroupid <> t_DashboardAdminAccessGp then            
             api.pkg_ObjectUpdate.GrantPrivilege(a_ObjectId, s.AccessGroupId, s.Name); 
          end if;
        end loop; 
              
      else
        --Grant Dashboard Admin Access
      for r in ( select a.AccessGroupId, o.Name
                       from api.ObjectSecurity o
                       inner join api.AccessGroups a
                        on a.AccessGroupId = o.AccessGroupId   
                        where o.ObjectId = a_ObjectId
                        and a.Description = 'Dashboard Admin'
                        and o.Name <> 'Insert') loop
                                                
           api.pkg_ObjectUpdate.GrantPrivilege(a_ObjectId, r.AccessGroupId, r.Name);             
       end loop;
       
          Select distinct a.accessgroupid
          into t_SuperUserAccessGp
          from api.accessgroups a
          where a.description like 'Super Users';
      api.pkg_ObjectUpdate.GrantPrivilege(a_ObjectId,
                                           t_SuperUserAccessGp,
                                          'Read');
      end if;        
    end if;
  end SecureDashboard;
end pkg_DashboardAdmin;

/

