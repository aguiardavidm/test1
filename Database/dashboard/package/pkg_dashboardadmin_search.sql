create or replace package pkg_DashboardAdmin_Search is

  -- Author  : HUSSEIN.HUSSEIN
  -- Created : 5/11/2012 5:01:54 PM
  -- Purpose : Search procedures for Dashboard Admin Site

  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  subtype udt_ObjectList is api.udt_ObjectList;
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  
  -----------------------------------------------------------------------------
  -- ObjectTypeSearchSelect
  -- Search for Dashboard Object Types: 
  -- Object Types
  -- Job Types and
  -- Process Types and
  -- Document Types will be shown. 
  -- "zzz" types will not be shown 
  -----------------------------------------------------------------------------
  procedure ObjectTypeSearchSelect(
    a_Name                              varchar2,
    a_Description                       varchar2,
    a_IsJobType                         varchar2,
    a_IsProcessType                     varchar2,
    a_SelectedObjectIds                 varchar2,
    a_Objects                           out nocopy udt_ObjectList
  );
  
  -----------------------------------------------------------------------------
  -- JobTypeStatusSearchSelect
  -- Searches for Job Statuses given list of job types
  -----------------------------------------------------------------------------
  procedure JobTypeStatusSearchSelect(
    a_JobTypes                          varchar2,
    a_SelectedObjectIds                 varchar2,
    a_Objects                           out nocopy udt_ObjectList
  );
end pkg_DashboardAdmin_Search;

 
/

grant execute
on pkg_dashboardadmin_search
to posseextensions;

create or replace package body pkg_DashboardAdmin_Search is

  /*---------------------------------------------------------------------------
   * GetObjectTypeObjectId()
   *-------------------------------------------------------------------------*/ 
  function GetObjectTypeObjectId (
    a_ObjectDefId                       udt_Id
  ) return udt_Id is
    t_ObjectId                          udt_Id;
  begin
    t_ObjectId := api.pkg_SimpleSearch.ObjectByIndex('o_DashboardObjectType',
        'ExternalObjectDefId', a_ObjectDefId);
    if t_ObjectId is null then
      t_ObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
          'o_DashboardObjectType', a_ObjectDefId);
    end if;
    return t_ObjectId;
  end;

  /*---------------------------------------------------------------------------
   * GetJobStatusObjectId()
   *-------------------------------------------------------------------------*/ 
  function GetJobStatusObjectId (
    a_StatusId                          udt_Id
  ) return udt_Id is
    t_ObjectId                          udt_Id;
  begin
    t_ObjectId := api.Pkg_Simplesearch.ObjectByIndex('o_Dashboardjobstatus',
        'StatusId', a_StatusId);
    if t_ObjectId is null then
      t_ObjectId := api.pkg_ObjectUpdate.RegisterExternalObject(
          'o_Dashboardjobstatus', a_StatusId);
    end if;
    return t_ObjectId;
  end;

 -----------------------------------------------------------------------------
  -- ObjectTypeSearchSelect
  -- Search for Dashboard Object Types given.
  -----------------------------------------------------------------------------
  procedure ObjectTypeSearchSelect(
    a_Name                              varchar2,
    a_Description                       varchar2,
    a_IsJobType                         varchar2,
    a_IsProcessType                     varchar2,
    a_SelectedObjectIds                 varchar2,
    a_Objects                           out nocopy udt_ObjectList
  ) is
    t_ObjectDefIds                      udt_IdList;
    t_DashboardObjectTypeObjectIds      udt_IdList;
    t_SelectedObjectIdList              udt_StringList;
    t_SelectedObjectIds                 api.udt_ObjectList;
  begin
    a_Objects := api.udt_ObjectList();

    t_SelectedObjectIdList := dashboard.pkg_DashboardAdmin_Utils.Split(a_SelectedObjectIds, ',');
    t_SelectedObjectIds := api.udt_ObjectList();
    t_SelectedObjectIds.extend(t_SelectedObjectIdList.count);
    for i in 1..t_SelectedObjectIdList.count loop
      t_SelectedObjectIds(i) := api.udt_Object(to_number(t_SelectedObjectIdList(i)));
    end loop;
    
    if a_IsJobType = 'Y' then
      select od.ObjectDefId
      bulk collect into t_ObjectDefIds
      from api.ObjectDefs od
      where instr(upper(od.Name),upper('zzz')) = 0 and instr(upper(od.Description),upper('zzz')) = 0 
        and (od.Name is null or a_Name = '%' or upper(od.Name) like upper(a_Name) || '%')
        and (od.Description is null or a_Description = '%' or upper(od.Description) like upper(a_Description) || '%')
        and od.ObjectDefTypeId = 2;
    elsif a_IsProcessType = 'Y' then
      select od.ObjectDefId
      bulk collect into t_ObjectDefIds
      from api.ObjectDefs od
      where instr(upper(od.Name),upper('zzz')) = 0 and instr(upper(od.Description),upper('zzz')) = 0 
        and (od.Name is null or a_Name = '%' or upper(od.Name) like upper(a_Name) || '%')
        and (od.Description is null or a_Description = '%' or upper(od.Description) like upper(a_Description) || '%')
        and od.ObjectDefTypeId = 3;
     else
      select od.ObjectDefId
      bulk collect into t_ObjectDefIds
      from api.ObjectDefs od
      where instr(upper(od.Name),upper('zzz')) = 0 and instr(upper(od.Description),upper('zzz')) = 0 
        and (od.Name is null or a_Name = '%' or upper(od.Name) like upper(a_Name) || '%')
        and (od.Description is null or a_Description = '%' or upper(od.Description) like upper(a_Description) || '%')
        and od.ObjectDefTypeId in (1,2,3,5)
        and instr(upper(od.Name),upper('o_dashboard')) = 0
        and instr(upper(od.Name),upper('o_PosseUtilities')) = 0;
     end if;
     
     a_Objects.extend(t_ObjectDefIds.count);
     for i in 1..t_ObjectDefIds.count loop
       a_Objects(i) := api.udt_Object(GetObjectTypeObjectId(t_ObjectDefIds(i)));
     end loop;
     dashboard.pkg_DashboardAdmin_utils.Append(a_Objects, t_SelectedObjectIds);
    
  exception 
   when no_data_found then
    null;
  end ObjectTypeSearchSelect;
  
  -----------------------------------------------------------------------------
  -- JobTypeStatusSearchSelect()
  -- Search for Jobs Statuses given a comma seperated list of Job Types.
  -----------------------------------------------------------------------------
  procedure JobTypeStatusSearchSelect(
    a_JobTypes                          varchar2,
    a_SelectedObjectIds                 varchar2,
    a_Objects                           out nocopy udt_ObjectList
  ) is
    t_JobStatusIds                      udt_IdList;
    t_SelectedObjectIdList              udt_StringList;
    t_SelectedObjectIds                 api.udt_ObjectList;
  begin
    a_Objects := api.udt_ObjectList();
    
    t_SelectedObjectIdList := dashboard.pkg_DashboardAdmin_Utils.Split(a_SelectedObjectIds, ',');
    t_SelectedObjectIds := api.udt_ObjectList();
    t_SelectedObjectIds.extend(t_SelectedObjectIdList.count);
    for i in 1..t_SelectedObjectIdList.count loop
      t_SelectedObjectIds(i) := api.udt_Object(to_number(t_SelectedObjectIdList(i)));
    end loop;
    
    select djs.StatusId
      bulk collect into t_JobStatusIds
      from api.Statuses djs
      join api.JobStatuses js
        on js.StatusId = djs.StatusId
      join table(pkg_DashboardAdmin_utils.ObjectDefIdList(a_JobTypes)) o 
        on o.ObjectId = js.JobTypeId;
    
    a_Objects.extend(t_JobStatusIds.count);
    for i in 1..t_JobStatusIds.count loop
      a_Objects(i) := api.udt_Object(GetJobStatusObjectId(t_JobStatusIds(i)));
    end loop;
    dashboard.pkg_dashboardadmin_utils.Append(a_Objects, t_SelectedObjectIds);
  exception 
   when no_data_found then
    null;
  end JobTypeStatusSearchSelect;
end pkg_DashboardAdmin_Search;

/

