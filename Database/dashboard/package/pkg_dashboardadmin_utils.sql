create or replace package pkg_DashboardAdmin_utils as

  /*--------------------------------------------------------------------------
   * Types -- PUBLIC
   *------------------------------------------------------------------------*/
  subtype udt_StringList is api.pkg_Definition.udt_StringList;
  subtype udt_Id is api.pkg_Definition.udt_Id;
  subtype udt_IdList is api.pkg_Definition.udt_IdList;
  
  /*---------------------------------------------------------------------------
   * Split ()
   *  Return a list of the words in a_String that are separated by a_Separator.
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                    varchar2,
    a_Separator                 varchar2
  ) return udt_StringList;
  
  /*--------------------------------------------------------------------------
   * ConvertToObjectList()
   *   Convert an udt_IdList to an api.udt_ObjectList;
   *------------------------------------------------------------------------*/
  function ConvertToObjectList (
    a_IdList                udt_IdList
  ) return api.udt_ObjectList;
  
  /*--------------------------------------------------------------------------
   * Append() -- PUBLIC
   * Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy api.udt_ObjectList,
    a_SecondList                        api.udt_ObjectList
  );

/*---------------------------------------------------------------------------
   * FormatObjectDefIdList()
   *-------------------------------------------------------------------------*/
  function FormatObjectDefIdList(
    a_ObjectTypes                         varchar2
  ) return varchar2;
  
  /*---------------------------------------------------------------------------
   * ObjectDefIdList()
   * Returns a list of objects.
   *-------------------------------------------------------------------------*/
  function ObjectDefIdList(
    a_ObjectTypes                         varchar2
  ) return udt_ObjectList;
  
end pkg_DashboardAdmin_utils;

 
/

grant execute
on pkg_dashboardadmin_utils
to posseextensions;

create or replace package body pkg_DashboardAdmin_utils as

  /*--------------------------------------------------------------------------
   * Types -- PRIVATE
   *------------------------------------------------------------------------*/
  type udt_ObjectTable is table of varchar2(1) index by binary_integer;
  subtype udt_NumberList is api.pkg_Definition.udt_NumberList;
  
  /*---------------------------------------------------------------------------
   * Split ()
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                    varchar2,
    a_Separator                 varchar2
  ) return udt_StringList is
    t_List                      udt_StringList;
    t_Pos                       number;
    t_Index                     number;
    t_SepLen                    number;
  begin
    t_SepLen := length(a_Separator);
    t_Pos := 0;
    while t_Pos <= length(a_String) loop
      t_Index := instr(a_String, a_Separator, t_Pos+1);
      if t_Index > 0 then
        t_List(t_List.count+1) := substr(a_String, t_Pos+1, t_Index-t_Pos-1);
        t_Pos := t_Index + t_SepLen - 1;
      else
        t_List(t_List.count+1) := substr(a_String, t_Pos+1);
        t_Pos := length(a_String) + 1;
      end if;
    end loop;

    return t_List;
  end;
  
  /*--------------------------------------------------------------------------
   * ConvertToObjectList()
   *   Convert an udt_IdList to an api.pkg_ObjectList;
   *------------------------------------------------------------------------*/
  function ConvertToObjectList (
    a_IdList                udt_IdList
  ) return api.udt_ObjectList is
    t_Objects                api.udt_ObjectList;
    t_Index                pls_integer;
    t_Count                pls_integer;
  begin
    t_Objects := api.udt_ObjectList();
    t_Objects.extend(a_IdList.count);

    t_Count := 1;
    t_Index := a_IdList.first;
    while t_Index is not null loop
      t_Objects(t_Count) := api.udt_Object(a_IdList(t_Index));
      t_Count := t_Count + 1;
      t_Index := a_IdList.next(t_Index);
    end loop;

    return t_Objects;
  end;
  
  /*--------------------------------------------------------------------------
   * Append() -- PUBLIC
   * Adds all the values in a_SecondList to a_MainList
   *------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy api.udt_ObjectList,
    a_SecondList                        api.udt_ObjectList
  ) is
    t_Count                             pls_integer;
    t_Index                             number;
    t_TempList                          udt_ObjectTable;
  begin
    /* Load a_MainList into an Object Table */
    t_Count := a_MainList.count;
    for i in 1..t_Count loop
      t_TempList(a_MainList(i).ObjectId) := 'Y';
    end loop;

    /* Now append the second list */
    t_Count := a_SecondList.count;
    for i in 1..t_Count loop
      t_Index := a_SecondList(i).ObjectId;
      t_TempList(t_Index) := 'Y';
    end loop;

    a_MainList.delete;
    t_Index := t_TempList.first;
    while t_Index is not null loop
      a_MainList.extend(1);
      a_MainList(a_MainList.count) := api.udt_Object(t_Index);
      t_Index := t_TempList.next(t_Index);
    end loop;
    t_TempList.delete;
  end;

  /*---------------------------------------------------------------------------
   * FormatObjectDefIdList()
   *-------------------------------------------------------------------------*/ 
  function FormatObjectDefIdList (
     a_ObjectTypes                    varchar2
   ) return varchar2 is
     t_ObjectDefList                  udt_StringList;
     t_ObjectDefIds                   varchar2(4000);
   begin
     t_ObjectDefList := Split(a_ObjectTypes, ',');
     for i in 1..t_ObjectDefList.count loop
       pkg_DashboardUtils.AddSection(t_ObjectDefIds, '''' || t_ObjectDefList(i) || '''', ',');
     end loop;

     return t_ObjectDefIds;
   end;
   
  /*---------------------------------------------------------------------------
   * ObjectDefIdList()
   *-------------------------------------------------------------------------*/
  function ObjectDefIdList(
    a_ObjectTypes                       varchar2
  ) return udt_ObjectList is
    t_ObjectList                        udt_ObjectList := dashboard.udt_objectlist();
    t_ObjectDefIds                      udt_StringList;
  begin
    t_ObjectDefIds := Split(a_ObjectTypes, ',');
    t_ObjectList.Extend(t_ObjectDefIds.Count);
    for i in 1..t_ObjectDefIds.count loop
      t_ObjectList(i) := dashboard.udt_object(t_ObjectDefIds(i));
    end loop;
    return t_ObjectList;
  end;

end pkg_DashboardAdmin_utils;

/

