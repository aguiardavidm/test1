create or replace package pkg_DashboardDefinition is

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_NumberList is dbms_sql.Number_Table;
  subtype udt_Id is number(9);
  subtype udt_ObjectList is dashboard.udt_objectlist;

  type udt_DateList is table of date index by binary_integer;
  type udt_IdList is table of udt_Id index by binary_integer;
  type udt_StringList is table of varchar2(4000) index by binary_integer;

  type udt_CorralTable is record (
    schema                              varchar2(30),
    name                                varchar2(30),
    IdName                              varchar2(30)
  );

  type udt_Gauge is record (
    min                                number(12,1) := 0,
    max                                number(12,1) := 0,
    alert                              pls_integer  := 0, --{0,1,2}
    pct                                varchar2(5)  := 'false'
  );

  type udt_Data is record (
    x                                  varchar2(100),
    y                                  udt_NumberList
  );
  
  type udt_DataList                    is table of udt_Data index by binary_integer;

  type udt_UrlParameters               is table of varchar2(4000) index by varchar2(4000);

  type udt_NumberListByString is table of number index by varchar2(100);

  type udt_Chart is record (
    AccessGroup                         varchar2(200),
    Calculation                         varchar2(100),
    CalculationDetail                   varchar2(30),
    Category                            varchar2(60),
    ChartId                             pls_integer,
    ChartType                           varchar2(200),
    CorralObjectIdColumnName            varchar2(30),
    CorralSchemaName                    varchar2(30),
    CorralTableName                     varchar2(30),
    Data                                udt_DataList,
    DefaultPeriod                       varchar2(100),
    DisplayAsPercentage                 char(1),
    DisplayTwoGroups                    char(1),
    DisplayType                         varchar2(100),
    DrillDownUrl                        varchar2(4000),
    EndDateDetail                       varchar2(500),
    EndStatus                           varchar2(4000),
    ExcludeStatus                       varchar2(4000),
    FieldNames                          udt_StringList,
    FromDate_                           varchar2(10), --'mm/dd/yyyy'
    FromDate                            date,
    Gauge                               udt_Gauge,
    GaugeCalculationCurrPrev            varchar2(100),
    GaugeCalculationName                varchar2(60),
    GaugeCalculationPrevXDays           number,
    GaugeCalculationTimePeriod          varchar2(100),
    GaugeLevel1Threshold                number,
    GaugeLevel2Threshold                number,
    GaugeSql                            varchar2(4000),
    GroupLabels                         udt_StringList,
    GroupValues                         udt_StringList,
    GroupExpressions                    udt_StringList,
    IncludeCancelled                    char(1),
    JobTypeIds                          udt_IdList,
    MaxYValue                           number,
    OnlyIncludeScheduled                char(1),
    Period                              varchar2(100),
    ProcessTypeIds                      udt_IdList,
    SeriesLabels                        udt_StringList,
    ShowGroupValues                     udt_StringList,
    ShowTwoGroups                       char(1),
    StartDateDetail                     varchar2(500),
    StartStatus                         varchar2(4000),
    StartStatusIds                      udt_IdList,
    SubGroup                            varchar2(4000),
    TimePeriodDetail                    varchar2(100),
    Title                               varchar2(400),
    ToDate_                             varchar2(10), --'mm/dd/yyyy'
    ToDate                              date,
    Users_                              varchar2(4000),
    View_                               varchar2(30),
    Visible                             varchar2(1),
    WorkingOrCalendarDays               varchar2(100),
    XAxisLabel                          varchar2(100),
    YAxisLabel                          varchar2(100),
    ObjectTypes                         varchar2(4000),
    FiscalYearEndDate                   date,
    DefaultTopNValues                   number,
    DefaultOrderBy                      varchar2(100),
    NotTopNList                         udt_NumberListByString,
    NotTopNValues                       varchar2(4000),
    FilterExpression                    varchar2(4000),
    GroupNotTopNValues                  udt_StringList,
    ValueFormat                         varchar2(100),
    DecimalPlaces                       number,
    OnlyJobTypeSelected                 varchar2(1)                      
  );

  type udt_WarningLevel is record (
    key                                number,
    val                                varchar2(100)
  );

  type tbl_WarningLevel is table of udt_WarningLevel;

  type udt_DataMatrix is table of udt_NumberListByString index by varchar2(100);

  type udt_PeriodCalc is record (
    PeriodName                          varchar2(30),
    Value                               number
  );

  type udt_PeriodCalcList is table of udt_PeriodCalc index by binary_integer;

  /*--------------------------------------------------------------------------
   * Global Constants
   *------------------------------------------------------------------------*/
  -- value used to display null values
  gc_NullValue                          constant varchar2(10) := '*None*';  
  -- values for udt_Chart.displaytype
  gc_Timeline				                    constant varchar2(10) := 'Timeline';
  gc_BarGraph				                    constant varchar2(10) := 'Bar Graph';
  gc_Gauge				                      constant varchar2(10) := 'Gauge';

  -- values for udt_Chart.view_
  gc_Compare                            constant varchar2(10) := 'Compare';
  gc_Detailed                           constant varchar2(10) := 'Detailed';
  gc_Summary                            constant varchar2(10) := 'Summary';

  -- values for Function Name
  gc_Count                              constant varchar2(10) := 'Count';
  gc_Sum                                constant varchar2(10) := 'Sum';
  gc_Average                            constant varchar2(10) := 'Average';

end pkg_DashboardDefinition;

 
/

