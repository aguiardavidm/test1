create or replace package pkg_Dashboard is

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_IdList is pkg_DashboardDefinition.udt_IdList;
  subtype udt_Id is pkg_DashboardDefinition.udt_Id;
  subtype udt_Chart is pkg_DashboardChartUtils.udt_Chart;
  subtype udt_Data is pkg_DashboardChartUtils.udt_Data;
  subtype udt_DateList is pkg_DashboardDefinition.udt_DateList;
  subtype udt_StringList is pkg_DashboardDefinition.udt_StringList;
  subtype udt_NumberList is pkg_DashboardDefinition.udt_NumberList;
  subtype udt_DataMatrix is pkg_DashboardUtils.udt_DataMatrix;
  subtype udt_DataList is pkg_DashboardChartUtils.udt_DataList;
  subtype udt_CorralTable is pkg_DashboardDefinition.udt_CorralTable;
 /*--------------------------------------------------------------------------
  * Constants
  *------------------------------------------------------------------------*/
  -- constants for Function Name
  gc_NullValue    constant varchar2(10) := pkg_DashboardDefinition.gc_NullValue;

  g_NullNumberList                      udt_NumberList;
  g_NullStringList                      udt_StringList;

  /*---------------------------------------------------------------------------
   * BuiltInGauges()
   *-------------------------------------------------------------------------*/
  function BuiltInGauges (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * CustomGauge()
   *-------------------------------------------------------------------------*/
  function CustomGauge (
    a_ChartId                          udt_Id,
    a_ChartArgs                        varchar2
  ) return clob;
  
  /*---------------------------------------------------------------------------
   * ProcessInWarningState()
   *-------------------------------------------------------------------------*/
  function ProcessInWarningState(
    a_ChartId                         udt_Id,
    a_ChartArgs                       varchar2
  ) return clob ;

  /*---------------------------------------------------------------------------
   * ProcessCompletionWarningGauge()
   *-------------------------------------------------------------------------*/
  function ProcessCompletionWarningGauge(
    a_ChartId                          udt_Id,
    a_ChartArgs                        varchar2
  ) return clob;

 /*---------------------------------------------------------------------------
  * ToDoListSummary()
  *-------------------------------------------------------------------------*/
  function ToDoListSummary (
     a_ChartId                         udt_Id,
     a_ChartArgs                       varchar2
   ) return clob;

 /*---------------------------------------------------------------------------
  * JobsInStatus()
  *-------------------------------------------------------------------------*/
  function JobsInStatus (
    a_ChartId                         udt_Id,
    a_ChartArgs                       varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * DaysToCompleteByStaff()
   *-------------------------------------------------------------------------*/
  function DaysToCompleteByStaff (
    a_ChartId                          udt_Id,
    a_ChartArgs                        varchar2
  ) return clob;
  /*---------------------------------------------------------------------------
   * DaysToCompleteSummary()
   *-------------------------------------------------------------------------*/
  function DaysToCompleteSummary (
    a_ChartId                          udt_Id,
    a_ChartArgs                        varchar2
  ) return clob;
  /*---------------------------------------------------------------------------
   * ObjectSummary()
   *-------------------------------------------------------------------------*/
  function ObjectSummary (
    a_ChartId                         udt_Id,
    a_ChartArgs                       varchar2
  ) return clob ;
 
  /*---------------------------------------------------------------------------
   * ObjectIntakeRate()
   *-------------------------------------------------------------------------*/
  function ObjectIntakeRate (
    a_ChartId                          udt_Id,
    a_ChartArgs                        varchar2
  ) return clob;
end pkg_Dashboard;

 
/

grant execute
on pkg_dashboard
to posseextensions;

create or replace package body pkg_Dashboard as

  /*---------------------------------------------------------------------------
   * Subtypes
   *-------------------------------------------------------------------------*/
  subtype udt_ObjectList is dashboard.udt_ObjectList;
  subtype udt_BindVarList is pkg_DashboardSql.udt_BindVarList;
  subtype udt_ResultRecord is pkg_DashboardSql.udt_ResultRecord;

  /*---------------------------------------------------------------------------
   * Constants
   *-------------------------------------------------------------------------*/
  gc_Timeline                           constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Timeline;
  gc_BarGraph                           constant varchar2(10) :=
      pkg_DashboardDefinition.gc_BarGraph;
  gc_Gauge                              constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Gauge;
  gc_Compare                            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Compare;
  gc_Detailed                           constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Detailed;
  gc_Summary                            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Summary;
  gc_CRLF                               constant varchar2(2) :=
      chr(13) || chr(10);
  gc_Count                              constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Count;
  gc_Sum                                constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Sum;
  gc_Average                            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Average;
  gc_String                             constant varchar2(10) :=
      pkg_DashboardSql.gc_String;
  gc_Number                             constant varchar2(10) :=
      pkg_DashboardSql.gc_Number;
  gc_Date                               constant varchar2(10) :=
      pkg_DashboardSql.gc_Date;

  /*---------------------------------------------------------------------------
   * BuiltInGauges()
   *-------------------------------------------------------------------------*/
  function BuiltInGauges (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_Type                              varchar2(100);
  begin
    select GaugeCalculationName
      into t_Type
      from dashboard.o_DashboardChart
      where ObjectId = a_ChartId;

    case t_Type
      when 'ObjectIntakeRate' then
        return ObjectIntakeRate(a_ChartId, a_ChartArgs);
      when 'ObjectSummary' then
        return ObjectSummary(a_ChartId, a_ChartArgs);
      when 'DaysToCompleteSummary' then
        return DaysToCompleteSummary(a_ChartId, a_ChartArgs);
      when 'DaysToCompleteByStaff' then
        return DaysToCompleteByStaff(a_ChartId, a_ChartArgs);
      else
        pkg_DashboardUtils.RaiseError(-20000, 'Invalid Gauge Calculation "'
            || t_Type || '" for Chart id: ' || a_ChartId);
    end case;

  end BuiltInGauges;

  /*---------------------------------------------------------------------------
   * CustomGauge()
   *-------------------------------------------------------------------------*/
  function CustomGauge (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_Chart                             udt_Chart;
    t_Value                             number;
    t_Min                               number;
    t_Max                               number;
    t_Threshold1                        number;
    t_Threshold2                        number;
  begin
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId, a_ChartArgs);

    execute immediate t_Chart.GaugeSql
    into t_Value, t_Min, t_Max, t_Threshold1, t_Threshold2;

    pkg_DashboardChartUtils.SetGaugeValues(t_Chart, t_Value, t_Min, t_Max,
        t_Threshold1, t_Threshold2);
    return pkg_DashboardChartUtils.ToJSON(t_Chart);
  end CustomGauge;

  /*---------------------------------------------------------------------------
   * ProcessInWarningState()
   *-------------------------------------------------------------------------*/
  function ProcessInWarningState(
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_Chart                             udt_Chart;
    t_ProcessTypeIds                    udt_ObjectList;
    t_JobTypeIds                        udt_ObjectList;
    t_Group2                            varchar2(4000);
    t_Columns                           varchar2(4000);
    t_Sql                               varchar2(4000);
    t_WhereClause                       varchar2(4000);
    t_JobJoinClause                     varchar2(200);
    t_Data                              udt_StringList;
    t_GroupData                         udt_StringList;

  begin
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId,
      a_ChartArgs);

    t_ProcessTypeIds := pkg_DashboardUtils.ConvertToObjectList(
        t_Chart.ProcessTypeIds);
    t_JobTypeIds := pkg_DashboardUtils.ConvertToObjectList(t_Chart.JobTypeIds);

    if t_Chart.GroupExpressions(1) is null then
      pkg_DashboardUtils.RaiseError(-20000,
          'Group 1 Expression not configured for ' || t_Chart.Title);
    end if;

    if t_Chart.DisplayType <> gc_Gauge then
      t_Group2 := t_Chart.GroupExpressions(2);
    end if;

    t_Columns := t_Chart.GroupExpressions(1);
   -- t_WhereClause := 'and ' || t_Chart.GroupExpressions(1) || ' is not null' ||
   --     gc_CRLF;

    if t_Group2 is not null then
      t_Columns := t_Columns || ',' || gc_CRLF || t_Group2;
      --t_WhereClause := t_WhereClause ||
      --    'and ' || t_Group2 || ' is not null' || gc_CRLF;
    end if;

    if t_JobTypeIds.count > 0 then
      t_JobJoinClause :=
        '  join dashboard.Jobs job' || gc_CRLF ||
        '    on job.JobId = process.JobId' || gc_CRLF ||
        '  join table(:JobTypes) jt' || gc_CRLF ||
        '    on jt.ObjectId = job.JobTypeId' || gc_CRLF;
    end if;

-- FIX ME - may not have to join to the corral table if only dashboard
-- tables are referenced, but how to tell that is the case?
    t_Sql :=
      'select /*+ cardinality (pt 1) */ ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      '  from table(:ProcessTypes) pt' || gc_CRLF ||
      '  join dashboard.Processes process' || gc_CRLF ||
      '    on process.ProcessTypeId = pt.ObjectId' || gc_CRLF ||
      t_JobJoinClause ||
      pkg_DashboardUtils.FilterJoinClause(t_Chart, 'process.ProcessId', true) ||
      '  where 1 = 1' || gc_CRLF ||
      t_WhereClause;

    -- execute the sql with the correct arguments
    if t_JobJoinClause is null then
      if t_Group2 is null then
        execute immediate t_Sql
        bulk collect into t_Data
        using t_ProcessTypeIds;
      else
        execute immediate t_Sql
        bulk collect into t_Data, t_GroupData
        using t_ProcessTypeIds;
      end if;
    else
      if t_Group2 is null then
        execute immediate t_Sql
        bulk collect into t_Data
        using t_ProcessTypeIds, t_JobTypeIds;
      else
        execute immediate t_Sql
        bulk collect into t_Data, t_GroupData
        using t_ProcessTypeIds, t_JobTypeIds;
      end if;
    end if;

    if t_Chart.DisplayType = gc_Gauge then
      pkg_DashboardChartUtils.SetGaugeValues(t_Chart, t_Data.count);
    else
      pkg_DashboardUtils.GroupByValues(t_Chart, t_Data, t_GroupData);
    end if;

    return pkg_DashboardChartUtils.toJSON(t_Chart);
  end ProcessInWarningState;

  /*---------------------------------------------------------------------------
   * ProcessCompletionWarningGauge()
   *-------------------------------------------------------------------------*/
  function ProcessCompletionWarningGauge(
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_Chart                             udt_Chart;
    t_Count                             pls_integer;
    t_Sql                               varchar2(4000);
    t_ResultRecord                      udt_ResultRecord;
    t_ColumnTypes                       udt_StringList;
    t_BindVars                          udt_BindVarList;
  begin
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId, a_ChartArgs);

    t_Sql :=
        'select' || gc_CRLF ||
        '  count(*),' || gc_CRLF ||
        '  to_char(count(process.DateCompleted))' || gc_CRLF ||
        'from dashboard.Processes process' || gc_CRLF ||
        pkg_DashboardUtils.FilterJoinClause(t_Chart, 'process.ProcessId') ||
        'where process.ProcessTypeId = :ProcessTypeId';

    pkg_DashboardSql.AddColumnType(gc_Number, t_ColumnTypes);
    -- bit of a hack as pkg_DashboardSql only allows for a single number type
    pkg_DashboardSql.AddColumnType(gc_String, t_ColumnTypes);
    pkg_DashboardSql.AddBindVar('ProcessTypeId', t_Chart.ProcessTypeIds(1), t_BindVars);

    if t_Chart.OnlyIncludeScheduled = 'Y' then
      t_Sql := t_Sql || gc_CRLF ||
          '  and process.ScheduledStartDate >= :FromDate' || gc_CRLF ||
          '  and process.ScheduledStartDate < :ToDate + 1';
    else
      t_Sql := t_Sql || gc_CRLF ||
          '  and ((process.ScheduledStartDate >= :FromDate' || gc_CRLF ||
          '      and process.ScheduledStartDate < :ToDate + 1)' || gc_CRLF ||
          '      or (process.DateCompleted >= :FromDate' || gc_CRLF ||
          '      and process.DateCompleted < :ToDate + 1))';
    end if;
    pkg_DashboardSql.AddBindVar('FromDate', t_Chart.FromDate, t_BindVars);
    pkg_DashboardSql.AddBindVar('ToDate', t_Chart.ToDate, t_BindVars);

    pkg_DashboardSql.ExecuteInto(t_Sql, t_BindVars, t_ColumnTypes,
        t_ResultRecord);

    pkg_DashboardChartUtils.SetGaugeValues(t_Chart,
        t_ResultRecord.Strings1(1), 0, t_ResultRecord.Numbers(1));

    return pkg_DashboardChartUtils.toJSON(t_Chart);
  end ProcessCompletionWarningGauge;

  /*---------------------------------------------------------------------------
   * ToDoListSummary()
   *-------------------------------------------------------------------------*/
  function ToDoListSummary (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_Chart                             udt_Chart;
    t_UserId                            udt_Id;
    t_UserIdList                        udt_IdList;
    t_UserObjectList                    udt_ObjectList;
    t_Sql                               varchar2(4000);
    t_ProcessTypes                      udt_StringList;

  begin
-- FIX ME - grouped only by ProcessType regardless of JobType
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId,
        a_ChartArgs);

    -- get one or more UserIds to be passed into sql
    if t_Chart.AccessGroup is null then
      if t_Chart.users_ is not null then
        t_UserId := to_number(t_Chart.users_);
      else
        t_UserId := dashboard.pkg_securityquery.EffectiveUserId();
      end if;
    else
      if t_Chart.users_ is not null then
        t_UserObjectList := pkg_DashboardChartUtils.UserObjectList(
            t_Chart.users_);
      else
        select UserId
          bulk collect into t_UserIdList
          from
            dashboard.AccessGroups ag
            join dashboard.AccessGroupUsers agu
              on agu.AccessGroupId = ag.AccessGroupId
          where ag.Description = t_Chart.AccessGroup;

        t_UserObjectList := pkg_DashboardUtils.ConvertToObjectList(
            t_UserIdList);
      end if;

      if t_UserObjectList.count = 1 then
        t_UserId := t_UserObjectList(1).ObjectId;
      end if;
    end if;

    -- get the ProcessType descriptions
    if t_UserId is not null then
      t_Sql :=
          'select od.Description' || gc_CRLF ||
          'from' || gc_CRLF ||
          '  dashboard.IncompleteProcessAssignments ipa' || gc_CRLF ||
          '  join dashboard.Processes process' || gc_CRLF ||
          '    on process.ProcessId = ipa.ProcessId' || gc_CRLF ||
          '  join dashboard.ObjectDefs od' || gc_CRLF ||
          '    on od.ObjectDefId = process.ProcessTypeId' || gc_CRLF ||
          'where ipa.AssignedTo = :UserId';

      execute immediate t_Sql
      bulk collect into t_ProcessTypes
      using t_UserId;
    else
      t_Sql :=
          'select od.Description' || gc_CRLF ||
          'from' || gc_CRLF ||
          '  dashboard.Processes process' || gc_CRLF ||
          '  join dashboard.ObjectDefs od' || gc_CRLF ||
          '    on od.ObjectDefId = process.ProcessTypeId' || gc_CRLF ||
          'where process.ProcessId in (' || gc_CRLF ||
          '  select /*+ cardinality (u 2) */ distinct ipa.ProcessId' || gc_CRLF ||
          '  from' || gc_CRLF ||
          '    table(:UserObjectList) u' || gc_CRLF ||
          '    join dashboard.IncompleteProcessAssignments ipa' || gc_CRLF ||
          '      on ipa.AssignedTo = u.ObjectId)';

      execute immediate t_Sql
      bulk collect into t_ProcessTypes
      using t_UserObjectList;
    end if;

    pkg_DashboardUtils.GroupByValues(t_Chart, t_ProcessTypes);

    return pkg_DashboardChartUtils.toJSON(t_Chart);
  end;

  /*---------------------------------------------------------------------------
   * JobsInStatus()
   *-------------------------------------------------------------------------*/
  function JobsInStatus (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_JobTypeId				udt_Id;
    t_Chart                             udt_Chart;
    t_StatusDescriptions                udt_StringList;
    t_Sql                               varchar2(4000);
    t_StatusObjectList                  udt_ObjectList;

  begin
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId,
        a_ChartArgs);
    t_StatusObjectList := pkg_DashboardUtils.ConvertToObjectList(
        t_Chart.StartStatusIds);

    t_Sql :=
      'insert into DashboardObjects_gt' || gc_CRLF ||
      '(select job.JobId' || gc_CRLF ||
      '  from dashboard.Jobs job' || gc_CRLF ||
      '  join table(:1) s' || gc_CRLF ||
      '    on s.ObjectId = job.StatusId' || gc_CRLF ||
      pkg_DashboardUtils.FilterJoinClause(t_Chart, 'job.JobId') ||
      '  where job.JobTypeId = :2)';

    execute immediate t_Sql
    using t_StatusObjectList, t_Chart.JobTypeIds(1);

    select s.Tag
      bulk collect into t_StatusDescriptions
      from DashboardObjects_gt do
      join dashboard.Jobs job
        on job.JobId = do.ObjectId
      join dashboard.Statuses s
        on s.StatusId = job.StatusId;

    pkg_DashboardUtils.GroupByValues(t_Chart, t_StatusDescriptions);

    return pkg_DashboardChartUtils.toJSON(t_Chart);
  end JobsInStatus;

  /*---------------------------------------------------------------------------
   * DaysToCompleteByStaff()
   *-------------------------------------------------------------------------*/
  function DaysToCompleteByStaff (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
   ) return clob is
    t_PreviousYears                     pls_integer := 0;
    t_AccessGroupNameJoinClause         varchar2(1000);
    t_ProcessTypeWhereClause            varchar2(1000);
    t_Dates                             udt_DateList;
    t_TempDates                         udt_DateList;
    t_FromDate                          date;
    t_ToDate                            date;
    t_Chart                             udt_Chart;
    t_Sql                               varchar2(4000);
    t_Columns                           varchar2(4000);
    t_Data                              udt_NumberList;
    t_TempData                          udt_NumberList;
    t_GroupData                         udt_StringList;
    t_TempGroupData                     udt_StringList;
    t_AllData                           udt_NumberList;
    t_AllGroupData                      udt_StringList;
    t_AllDates                          udt_DateList;

  begin
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId, a_ChartArgs);

    if t_Chart.WorkingOrCalendarDays = 'Working Days' then
       t_Columns := 'case
                      when process.ScheduledStartDate is not null then
                        pkg_DashboardUtils.WorkingDaysBetween(process.ScheduledStartDate,process.DateCompleted)
                     else
                        pkg_DashboardUtils.WorkingDaysBetween(process.CreatedDate,process.DateCompleted)
                     end';
    else
       t_Columns := 'case
                      when process.ScheduledStartDate is not null then
                        trunc(process.DateCompleted) - trunc(process.ScheduledStartDate)
                     else
                        trunc(process.DateCompleted) - trunc(process.CreatedDate)
                     end';
    end if;

    t_Columns := t_Columns || ',' || gc_CRLF ||'process.DateCompleted' || ',' || gc_CRLF ||'u.Name';
    if t_Chart.AccessGroup is not null then
       t_AccessGroupNameJoinClause :=
          '  join dashboard.accessgroupusers agu' || gc_CRLF ||
          '    on agu.UserId = u.UserId' || gc_CRLF ||
          '  join dashboard.accessgroups ag' || gc_CRLF ||
          '    on ag.AccessGroupId = agu.AccessGroupId' || gc_CRLF ||
          '   and ag.Description = :1'|| gc_CRLF;
     end if;

    if t_Chart.ProcessTypeIds.Count > 0 then
       t_ProcessTypeWhereClause :=
         '    and process.ProcessTypeId = :4' || gc_CRLF;
    end if;

    if t_Chart.view_ = gc_Compare then
      if t_Chart.ToDate - t_Chart.FromDate > 365 then
        pkg_DashboardUtils.RaiseError(-20000,
            'Compare Previous Years date range must be a year or less.');
      end if;
      t_PreviousYears := 2;
    end if;

    t_Sql :=
      'select ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      '   from dashboard.Processes process' || gc_CRLF ||
      '   join dashboard.users u' || gc_CRLF ||
      '     on u.UserId = process.CompletedByUserId' || gc_CRLF ||
      t_AccessGroupNameJoinClause || gc_CRLF ||
      '  where process.DateCompleted >= :2' || gc_CRLF ||
      '    and process.DateCompleted < :3 + 1' || gc_CRLF ||
      t_ProcessTypeWhereClause ||
      '    and process.Outcome is not null'|| gc_CRLF ||
      pkg_DashboardUtils.FilterJoinClause(t_Chart, 'process.JobId') || gc_CRLF;
      
    for i in 0..t_PreviousYears loop
      t_FromDate := add_months(t_Chart.FromDate, i * -12);
      t_ToDate := add_months(t_Chart.ToDate, i * -12);

      if t_Chart.ProcessTypeIds.Count > 0 then
       for j in 1..t_Chart.ProcessTypeIds.Count loop
        if t_Chart.AccessGroup is not null then
            execute immediate t_Sql
            bulk collect into t_TempData, t_TempDates, t_TempGroupData
            using t_Chart.AccessGroup, t_FromDate, t_ToDate, t_Chart.ProcessTypeIds(j);
        else
            execute immediate t_Sql
            bulk collect into t_TempData, t_TempDates, t_TempGroupData
            using t_FromDate, t_ToDate, t_Chart.ProcessTypeIds(j);
        end if;
       pkg_DashboardUtils.Append(t_Data,t_TempData);
       pkg_DashboardUtils.Append(t_Dates,t_TempDates);
       pkg_DashboardUtils.Append(t_GroupData,t_TempGroupData);
       end loop;
      else
        if t_Chart.AccessGroup is not null then
            execute immediate t_Sql
            bulk collect into t_Data, t_Dates, t_GroupData
            using t_Chart.AccessGroup, t_FromDate, t_ToDate;
        else
            execute immediate t_Sql
            bulk collect into t_Data, t_Dates, t_GroupData
            using t_FromDate, t_ToDate;
        end if;
      end if;
      pkg_DashboardUtils.Append(t_AllData,t_Data);
      pkg_DashboardUtils.Append(t_AllDates,t_Dates);
      pkg_DashboardUtils.Append(t_AllGroupData,t_GroupData);
    end loop;

    -- creating Timeline graph
    if t_Chart.DisplayType = gc_Timeline and t_Chart.view_ in (gc_Detailed, gc_Compare) then
      pkg_DashboardUtils.GroupByTimeline(t_Chart, t_AllDates, t_AllGroupData, gc_Average,t_AllData);
    elsif t_Chart.DisplayType = gc_Timeline then
      t_AllGroupData.delete;
      pkg_DashboardUtils.GroupByTimeline(t_Chart, t_AllDates, t_AllGroupData, gc_Average,t_AllData);
    elsif t_Chart.DisplayType = gc_Gauge then
      pkg_DashboardChartUtils.SetGaugeValues(t_Chart, t_AllData.Count);
    end if;
    t_Dates.delete;
    t_Data.delete;
    t_GroupData.delete;
    t_AllDates.delete;
    t_AllData.delete;
    t_AllGroupData.delete;

    return pkg_DashboardChartUtils.ToJSON(t_Chart);

end DaysToCompleteByStaff;

  /*---------------------------------------------------------------------------
   * DaysToCompleteSummary()
   *-------------------------------------------------------------------------*/
  function DaysToCompleteSummary (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_PreviousYears                     pls_integer := 0;
    t_ExcludeCancelledJoinClause        varchar2(1000);
    t_FromDate                          date;
    t_ToDate                            date;
    t_JobTypeId                         udt_Id;
    t_Dates                             udt_DateList;
    t_Chart                             udt_Chart;
    t_Sql                               varchar2(4000);
    t_Group1                            varchar2(4000);
    t_Group2                            varchar2(4000);
    t_Columns                           varchar2(4000);
    t_WhereClause                       varchar2(4000);
    t_Data                              udt_NumberList;
    t_GroupData                         udt_StringList;
    t_ExcludeJoinClause                 varchar2(100);
    t_StatusChangeJoinClause            varchar2(500);
    t_StatusChangeGroupByClause         varchar2(500);
    t_AllData                           udt_NumberList;
    t_AllGroupData                      udt_StringList;
    t_AllDates                          udt_DateList;


    t_Jobs                              udt_IdList;
    t_StartDates                        udt_DateList;
    t_EndDates                          udt_DateList;
    t_DaysToComplete                    number;
    t_StartDate                         date;
    t_EndDate                           date;
    t_PrevEndDate                       date;
    cursor c_ExcludeStatuses(a_JobId number, a_StartDate date, a_EndDate date) is
      select
        EffectiveStartDate,
        EffectiveEndDate
      from dashboard.JobStatusChange
      where JobId = a_JobId
        and instr(t_Chart.ExcludeStatus, StatusName) > 0
        and EffectiveStartDate <= a_EndDate
        and EffectiveEndDate > a_StartDate
      order by EffectiveStartDate;

  begin
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId,
        a_ChartArgs);

    if t_Chart.DisplayType = gc_Gauge then
      t_Group1 := 'job.JobTypeId';
    else
      t_Group1 := t_Chart.GroupExpressions(1);
    end if;

    if t_Group1 is null then
      pkg_DashboardUtils.RaiseError(-20000,
          'Group 1 Expression not configured for ' || t_Chart.Title);
    end if;

    if t_Chart.StartStatus is null and t_Chart.StartDateDetail is null then
      pkg_DashboardUtils.RaiseError(-20000,
          'Start statuses or start date columns not configured for ' || t_Chart.Title);
    end if;
    if t_Chart.EndStatus is null and t_Chart.EndDateDetail is null then
      pkg_DashboardUtils.RaiseError(-20000,
          'End statuses or end date columns not configured for ' || t_Chart.Title);
    end if;
    if t_Chart.StartStatus is not null and t_Chart.StartDateDetail is not null then
      pkg_DashboardUtils.RaiseError(-20000,
          'Either start statuses or start date columns must be configured for ' || t_Chart.Title||' but not both');
    end if;
    if t_Chart.EndStatus is not null and t_Chart.EndDateDetail is not null then
      pkg_DashboardUtils.RaiseError(-20000,
          'Either end statuses or end date columns must be configured for ' || t_Chart.Title||' but not both');
    end if;

    if t_Chart.DisplayType <> gc_Gauge then
      t_Group2 := t_Chart.GroupExpressions(2);
    end if;

    t_Columns := 'job.JobId';
    if t_Chart.StartStatus is not null and t_Chart.EndStatus is not null then
      t_StatusChangeJoinClause :=
          '  join dashboard.jobstatuschange jsc' || gc_CRLF ||
          '    on jsc.JobId = job.JobId' || gc_CRLF ||
          '   and jsc.StatusName in (' || pkg_DashboardUtils.FormatStatusList(t_Chart.EndStatus) || ')' || gc_CRLF;
      t_StatusChangeGroupByClause := 'group by job.JobId';
      if t_Group1 is not null then
        t_StatusChangeGroupByClause := t_StatusChangeGroupByClause || ', '||t_Group1;
      end if;
      if t_Group2 is not null then
        t_StatusChangeGroupByClause := t_StatusChangeGroupByClause || ', '||t_Group2;
      end if;
      t_StatusChangeGroupByClause := t_StatusChangeGroupByClause || gc_CRLF ||
          'having max(jsc.effectivestartdate) >= trunc(:StartDate)' || gc_CRLF ||
          '  and max(jsc.effectivestartdate) < trunc(:EndDate) + 1' || gc_CRLF;
      t_Columns := t_Columns || ', ' || gc_CRLF ||
          '  (select min(jsc2.effectivestartdate)' || gc_CRLF ||
          'from dashboard.jobstatuschange jsc2' || gc_CRLF ||
          'where jsc2.statusname IN (' || pkg_DashboardUtils.FormatStatusList(t_Chart.StartStatus) || ')' || gc_CRLF ||
          '  and jsc2.jobid = job.jobid' || gc_CRLF ||
          ') StartDate,' || gc_CRLF ||
          'max(jsc.effectivestartdate) EndDate';
       t_WhereClause :=
           '  and exists (' || gc_CRLF ||
           '    select *' || gc_CRLF ||
           '    from dashboard.jobstatuschange jsc2' || gc_CRLF ||
           '    where jsc2.jobid = jsc.jobid' || gc_CRLF ||
           '      and jsc2.statusname in (' || pkg_DashboardUtils.FormatStatusList(t_Chart.StartStatus) || ')' || gc_CRLF ||
           '      and jsc2.effectivestartdate < jsc.effectivestartdate)' || gc_CRLF;
    elsif t_Chart.StartDateDetail is not null and t_Chart.EndDateDetail is not null then
      t_Columns := t_Columns || ', ' || gc_CRLF ||
          t_Chart.StartDateDetail || ' StartDate,' || gc_CRLF ||
          t_Chart.EndDateDetail || ' EndDate';
      t_WhereClause :=
          ' and ' || t_Chart.EndDateDetail || ' >= trunc(:StartDate)' || gc_CRLF ||
          ' and ' || t_Chart.EndDateDetail || ' < trunc(:EndDate) + 1' || gc_CRLF ||
          ' and ' || t_Chart.StartDateDetail || ' is not null' || gc_CRLF;
    else
      pkg_DashboardUtils.RaiseError(-20000,
          'Either start/end statuses or start/end date columns must be configured for ' || t_Chart.Title||' but not both');
    end if;

    if t_Group1 is not null then
      t_Columns := t_Columns || ',' || gc_CRLF || t_Group1;
    end if;

    if t_Group2 is not null then
      t_Columns := t_Columns || ',' || gc_CRLF || t_Group2;
    end if;

    if t_Chart.view_ = gc_Compare then
      if t_Chart.ToDate - t_Chart.FromDate > 365 then
        pkg_DashboardUtils.RaiseError(-20000,
            'Compare Previous Years date range must be a year or less.');
      end if;
      t_PreviousYears := 2;
    end if;

    if t_Chart.IncludeCancelled <> 'Y' then
      t_ExcludeCancelledJoinClause :=
          '  join dashboard.JobStatuses js' || gc_CRLF ||
          '    on js.JobTypeId = job.JobTypeId' || gc_CRLF ||
          '   and js.StatusId = job.StatusId' || gc_CRLF ||
          '   and js.StatusType <> ''X''' || gc_CRLF;
    end if;

     t_Sql :=
      'select ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      '  from dashboard.Jobs job' || gc_CRLF ||
      t_StatusChangeJoinClause ||
      t_ExcludeJoinClause ||
      t_ExcludeCancelledJoinClause ||
      pkg_DashboardUtils.FilterJoinClause(t_Chart, 'job.JobId', true) ||
      '  where job.JobTypeId = :ObjectDefId' || gc_CRLF ||
      t_WhereClause ||
      t_StatusChangeGroupByClause;

    for i in 0..t_PreviousYears loop
      t_FromDate := add_months(t_Chart.FromDate, i * -12);
      t_ToDate := add_months(t_Chart.ToDate, i * -12);

      execute immediate t_SQL
      bulk collect into t_Jobs, t_StartDates, t_EndDates, t_GroupData
      using t_Chart.JobTypeIds(1), t_FromDate, t_ToDate;

      /* Calculate the days to complete for each job */
      t_Data.delete;
      for j in 1..t_Jobs.count loop
        if t_Chart.WorkingOrCalendarDays = 'Working Days' then
          t_DaysToComplete := pkg_DashboardUtils.WorkingDaysBetween(t_StartDates(j), t_EndDates(j));
        else
          t_DaysToComplete := trunc(t_EndDates(j)) - trunc(t_StartDates(j));
        end if;

        /* Exclude any time periods in an excluded status */
        for s in c_ExcludeStatuses(t_Jobs(j), trunc(t_StartDates(j)), trunc(t_EndDates(j))) loop
          t_StartDate := trunc(greatest(s.EffectiveStartDate, t_StartDates(j)));
          t_EndDate := trunc(least(s.EffectiveEndDate, t_EndDates(j)));
          /* If the current status occurred immediately after the previous one, then ensure
           that the start date is not double counted */
          if t_StartDate = t_PrevEndDate then
            t_StartDate := t_StartDate + 1;
          end if;

          if t_Chart.WorkingOrCalendarDays = 'Working Days' then
            t_DaysToComplete := t_DaysToComplete - pkg_DashboardUtils.WorkingDaysBetween(t_StartDate, t_EndDate);
          else
            t_DaysToComplete := t_DaysToComplete - (trunc(t_EndDate) - trunc(t_StartDate));
          end if;
          t_PrevEndDate := t_EndDate;
        end loop;

        t_Data(j) := t_DaysToComplete;
      end loop;
       pkg_DashboardUtils.Append(t_AllData,t_Data);
       pkg_DashboardUtils.Append(t_AllDates,t_EndDates);
       pkg_DashboardUtils.Append(t_AllGroupData,t_GroupData);
    end loop;

    -- creating Timeline graph
    if t_Chart.DisplayType = gc_Timeline and t_Chart.view_ in (gc_Detailed, gc_Compare) then
      pkg_DashboardUtils.GroupByTimeline(t_Chart, t_AllDates, t_AllGroupData, gc_Average,t_AllData);
    elsif t_Chart.DisplayType = gc_Timeline then
      t_AllGroupData.delete;
      pkg_DashboardUtils.GroupByTimeline(t_Chart, t_AllDates, t_AllGroupData, gc_Average,t_AllData);
    -- creating Bar graph
    elsif t_Chart.DisplayType = gc_BarGraph then
      pkg_DashboardUtils.GroupByValues(t_Chart, t_AllGroupData,g_NullStringList,g_NullStringList,g_NullStringList, gc_Average, t_AllData);
    elsif t_Chart.DisplayType = gc_Gauge then
      pkg_DashboardUtils.GroupByValues(t_Chart, t_AllGroupData,g_NullStringList,g_NullStringList,g_NullStringList, gc_Average, t_AllData);
      pkg_DashboardChartUtils.SetGaugeValues(t_Chart, t_Chart.data(1).y(1));
    end if;
    t_Dates.delete;
    t_Data.delete;
    t_GroupData.delete;
    t_AllDates.delete;
    t_AllData.delete;
    t_AllGroupData.delete;

    return pkg_DashboardChartUtils.ToJSON(t_Chart);

end DaysToCompleteSummary;
/*---------------------------------------------------------------------------
   * ObjectSummary()
   *-------------------------------------------------------------------------*/
  function ObjectSummary (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_Chart                             udt_Chart;
    t_GroupCols                         udt_StringList;
    t_ShowValues                        udt_StringList;
    t_SubGroupShowValues                udt_StringList;
    t_GroupLevel                        pls_integer;
    t_Columns                           varchar2(4000);
    t_BindVars                          udt_BindVarList;
    t_ObjectDefIdsTable                 udt_BindVarList;
    t_Sql                               varchar2(4000);
    t_WhereClause                       varchar2(4000);
    t_StatusJoinClause                  varchar2(400);
    t_JobJoinClause                     varchar2(400);
    t_ProcessJoinClause                 varchar2(400);
    t_ObjectsJoinClause                 varchar2(4000);
    t_ObjectWhereClause                 varchar2(4000);
    t_FilterJoinClause                  varchar2(4000);
    t_ColumnCount                       pls_integer;
    t_BindName                          varchar2(30);
    t_ResultRecord                      udt_ResultRecord;
    t_ColumnTypes                       udt_StringList;
    t_Found                             boolean;
    t_NotTopNList                       udt_StringList;
    t_NotTopNValues                     varchar2(4000);
    t_JobCount                          number;
    t_ProcessCount                      number;
    t_ObjectCount                       number;
  begin
    -- Create Chart so we can use its preset attributes
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId,
      a_ChartArgs);
      
    Select count(*)
      into t_JobCount
      from api.Objectdefs od
      join table(pkg_DashboardUtils.ObjectDefIdList(t_Chart.ObjectTypes)) odl
        on odl.ObjectId = od.ObjectDefId
       and od.ObjectDefTypeId = 2;
    
    Select count(*)
      into t_ProcessCount
      from api.Objectdefs od
      join table(pkg_DashboardUtils.ObjectDefIdList(t_Chart.ObjectTypes)) odl
        on odl.ObjectId = od.ObjectDefId
       and od.ObjectDefTypeId = 3;
     
    Select count(*)
      into t_ObjectCount
      from api.Objectdefs od
      join table(pkg_DashboardUtils.ObjectDefIdList(t_Chart.ObjectTypes)) odl
        on odl.ObjectId = od.ObjectDefId
       and od.ObjectDefTypeId = 1;
       
    -- Handle Object Types
    if pkg_DashboardUtils.FormatObjectDefIdList(t_Chart.ObjectTypes) is not null then
      t_ObjectsJoinClause := '  from dashboard.Objects object' || gc_CRLF;
      t_ObjectWhereClause := ' where object.ObjectDefId in (' || pkg_DashboardUtils.FormatObjectDefIdList(t_Chart.ObjectTypes) || ')' || gc_CRLF ||
                             '   and object.ObjectDefTypeId in (1, 2, 3)' || gc_CRLF;
      if t_Chart.CorralSchemaName is not null and t_Chart.CorralTableName is not null and t_Chart.CorralObjectIdColumnName is not null then                       
         t_FilterJoinClause  := pkg_DashboardUtils.FilterJoinClause(t_Chart, 'object.ObjectId', true);
      end if;
    elsif t_Chart.CorralSchemaName is not null and t_Chart.CorralTableName is not null then
      pkg_DashboardUtils.VerifyObjectConfig(t_Chart);
      t_ObjectsJoinClause := '  from '|| t_Chart.CorralSchemaName || '.' || t_Chart.CorralTableName ||' corral' || gc_CRLF;
      t_ObjectWhereClause := ' where 1 = 1'|| gc_CRLF;
    end if;


    if t_Chart.FromDate is not null and t_Chart.TimePeriodDetail is not null then
      t_WhereClause := t_WhereClause || ' and ' || t_Chart.TimePeriodDetail ||
          ' >= :FromDate' || gc_CRLF;
      pkg_DashboardSql.AddBindVar('FromDate', t_Chart.FromDate, t_BindVars);
    end if;
    if t_Chart.ToDate is not null and t_Chart.TimePeriodDetail is not null then
      t_WhereClause := t_WhereClause || ' and '|| t_Chart.TimePeriodDetail ||
          ' < :ToDate + 1' || gc_CRLF;
      pkg_DashboardSql.AddBindVar('ToDate', t_Chart.ToDate, t_BindVars);
    end if;
 
    if t_Chart.DisplayType = gc_Gauge and pkg_DashboardUtils.FormatObjectDefIdList(t_Chart.ObjectTypes) is not null then
      t_GroupCols(1) := 'object.ObjectDefId';
      t_ShowValues(1) := null;
    else
      t_GroupCols := t_Chart.GroupExpressions;
      t_ShowValues := t_Chart.ShowGroupValues;
    end if;

    if t_GroupCols(1) is null then
      pkg_DashboardUtils.RaiseError(-20000,
          'Group 1 Expression not configured for ' || t_Chart.Title);
    end if;

    t_GroupLevel := t_Chart.GroupValues.count + 1;
    -- if a drill down is requested that goes beyond the configured group
    -- expressions all drill down values will be removed (better than an error)
    -- The front end should not make such a call but this is just in case.
    if t_GroupLevel > 4 or t_GroupCols(t_GroupLevel) is null then
      t_Chart.GroupValues.delete;
      t_GroupLevel := 0;
    end if;

    /* Only show two groups on the first level.  Each subsequent drill down is a single level */
    if t_Chart.ShowTwoGroups = 'Y' and t_GroupLevel = 1 then
      t_ColumnCount := 2;
    else
      t_ColumnCount := 1;
    end if;
    
    -- add sections to where clause for drill down (group) values passed in
    if t_Chart.DisplayType = gc_Gauge then
      t_ColumnCount := 1;
      t_GroupLevel := 1;
    else
      for i in 1..t_GroupLevel - 1 loop
        if (t_Chart.groupvalues(i) != 'Other' or
           t_Chart.DefaultTopNValues is null) and
           t_Chart.groupvalues(i) != gc_NullValue then
          t_BindName := ':Group' || to_char(i);
          t_WhereClause := t_WhereClause || ' and ' || t_GroupCols(i) ||
              ' = ' || t_BindName || gc_CRLF;
          pkg_DashboardSql.AddBindVar(t_BindName, t_Chart.GroupValues(i),
              t_BindVars);
        elsif t_Chart.groupvalues(i) = 'Other' and
              t_Chart.DefaultTopNValues is not null then   
          t_NotTopNList := pkg_dashboardUtils.Split(t_Chart.GroupNotTopNValues(i), ',');
          t_WhereClause := t_WhereClause || ' and ' || t_GroupCols(i) ||
              ' in (';
          for j in 1..t_NotTopNList.count loop
            t_BindName := ':Group' || to_char(i) || 'NotTopNValues'||to_char(j);
            if j > 1 then
              t_WhereClause := t_WhereClause || ',';
            end if;
            t_WhereClause := t_WhereClause || t_BindName;
            
            pkg_DashboardSql.AddBindVar(t_BindName, t_NotTopNList(j),
                t_BindVars);
          end loop;
          t_WhereClause := t_WhereClause || ')' || gc_CRLF;
        elsif t_Chart.groupvalues(i) = gc_NullValue then
          t_WhereClause := t_WhereClause || ' and ' || t_GroupCols(i) ||
              ' is null ' || gc_CRLF;
        end if;
      end loop;
    end if;

    -- add columns and where clause sections based on level of drill down
    for i in t_GroupLevel..t_GroupLevel + t_ColumnCount - 1 loop
      if t_GroupCols(i) is null then
        -- if ShowTwoGroups is true but only three group columns (expressions)
        -- are configured then there will be no sub-group for the drill down
        t_ColumnCount := t_ColumnCount - 1;
      else
        pkg_DashboardSql.AddColumnType(gc_String, t_ColumnTypes);
        pkg_DashboardUtils.AddSection(t_Columns, t_GroupCols(i), ',' || gc_CRLF);
      end if;
    end loop;

    -- add calculation expression if required
    if t_Chart.Calculation in (gc_Sum, gc_Average)
        and t_Chart.CalculationDetail is not null then
      pkg_DashboardSql.AddColumnType(gc_Number, t_ColumnTypes);
      pkg_DashboardUtils.AddSection(t_Columns, t_Chart.CalculationDetail,
          ',' || gc_CRLF);
    end if;
    
    if t_JobCount > 0 and t_ObjectCount = 0 then
      t_JobJoinClause := '  join dashboard.Jobs job'|| gc_CRLF ||
                         '    on job.JobId = object.ObjectId'|| gc_CRLF;
    elsif t_JobCount > 0 and t_ObjectCount > 0 then
      t_JobJoinClause := '  left outer join dashboard.Jobs job'|| gc_CRLF ||
                         '    on job.JobId = object.ObjectId'|| gc_CRLF;
    end if;
    
    if t_ProcessCount > 0 and t_ObjectCount = 0 then
      t_ProcessJoinClause := '  join dashboard.Processes process'|| gc_CRLF ||
                             '    on process.ProcessId = object.ObjectId'|| gc_CRLF;
    elsif t_ProcessCount > 0 and t_ObjectCount > 0 then  
      t_ProcessJoinClause := '  left outer join dashboard.Processes process'|| gc_CRLF ||
                             '    on process.ProcessId = object.ObjectId'|| gc_CRLF;        
    end if;
    
    -- limit by a list of status ids if any are configured
    -- this should be acceptable as it is expected to be a short list
    if t_Chart.StartStatusIds.count > 0 and pkg_DashboardUtils.FormatObjectDefIdList(t_Chart.ObjectTypes) is not null then
      t_WhereClause := t_WhereClause ||
          '    and job.StatusId in (';
      for i in 1..t_Chart.StartStatusIds.count loop
        if i > 1 then
          t_WhereClause := t_WhereClause || ', ';
        end if;
        t_WhereClause := t_WhereClause ||
            to_char(t_Chart.StartStatusIds(i));
      end loop;
      t_WhereClause := t_WhereClause || ')';
    end if;
    
    -- construct the sql statement
    t_Sql :=
      'select ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      t_ObjectsJoinClause ||
      t_JobJoinClause ||
      t_ProcessJoinClause ||
      t_FilterJoinClause ||
      t_ObjectWhereClause ||
      t_WhereClause;
   
    -- execute the query and get the rows back
    pkg_DashboardSql.ExecuteInto(t_Sql, t_BindVars, t_ColumnTypes,
        t_ResultRecord);

    if t_Chart.DisplayType = gc_Gauge then
      pkg_DashboardUtils.GroupByValues(t_Chart,
          t_ResultRecord.Strings1, t_ResultRecord.Strings2,
          pkg_DashboardUtils.Split(t_ShowValues(t_GroupLevel), ','),
          g_NullStringList,
          nvl(t_Chart.Calculation, gc_Count),
          t_ResultRecord.Numbers);
      pkg_DashboardChartUtils.SetGaugeValues(t_Chart, t_Chart.data(1).y(1));
    else
      if t_GroupLevel < 4 then
        t_SubGroupShowValues :=
            pkg_DashboardUtils.Split(t_ShowValues(t_GroupLevel + 1), ',');
      else
        t_SubGroupShowValues := g_NullStringList;
      end if;

      pkg_DashboardUtils.GroupByValues(t_Chart,
          t_ResultRecord.Strings1, t_ResultRecord.Strings2,
          pkg_DashboardUtils.Split(t_ShowValues(t_GroupLevel), ','),
          t_SubGroupShowValues,
          nvl(t_Chart.Calculation, gc_Count),
          t_ResultRecord.Numbers);
    end if;
    
    return pkg_DashboardChartUtils.toJSON(t_Chart);
  end ObjectSummary;
  /*---------------------------------------------------------------------------
   * ObjectIntakeRate()
   *-------------------------------------------------------------------------*/
  function ObjectIntakeRate (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_PreviousYears                     pls_integer := 0;
    t_ExcludeCancelledJoinClause        varchar2(1000);
    t_FromDate                          date;
    t_ToDate                            date;
    t_Count                             number;
    t_Dates                             udt_DateList;
    t_Chart                             udt_Chart;
    t_Sql                               varchar2(4000);
    t_Columns                           varchar2(4000);
    t_Data                              udt_NumberList;
    t_GroupData                         udt_StringList;
    t_AllData                           udt_NumberList;
    t_AllGroupData                      udt_StringList;
    t_AllDates                          udt_DateList;
    t_WhereClause                       varchar2(4000);
    t_ObjectDefIdList                   udt_ObjectList;
    t_JobCount                          number;
    t_ProcessCount                      number;
    t_ObjectCount                       number;
    t_JobJoinClause                     varchar2(400);
    t_ProcessJoinClause                 varchar2(400);
    t_ObjectsJoinClause                 varchar2(4000);
    t_ObjectWhereClause                 varchar2(4000);
    t_FilterJoinClause                  varchar2(4000);

  begin
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId, a_ChartArgs);
    
    Select count(*)
      into t_JobCount
      from api.Objectdefs od
      join table(pkg_DashboardUtils.ObjectDefIdList(t_Chart.ObjectTypes)) odl
        on odl.ObjectId = od.ObjectDefId
       and od.ObjectDefTypeId = 2;
    
    Select count(*)
      into t_ProcessCount
      from api.Objectdefs od
      join table(pkg_DashboardUtils.ObjectDefIdList(t_Chart.ObjectTypes)) odl
        on odl.ObjectId = od.ObjectDefId
       and od.ObjectDefTypeId = 3;
     
    Select count(*)
      into t_ObjectCount
      from api.Objectdefs od
      join table(pkg_DashboardUtils.ObjectDefIdList(t_Chart.ObjectTypes)) odl
        on odl.ObjectId = od.ObjectDefId
       and od.ObjectDefTypeId = 1;
    
    -- Handle Object Types
    if pkg_DashboardUtils.FormatObjectDefIdList(t_Chart.ObjectTypes) is not null then
      t_ObjectsJoinClause := '  from dashboard.Objects object' || gc_CRLF;
      t_ObjectWhereClause := ' where object.ObjectDefId in (' || pkg_DashboardUtils.FormatObjectDefIdList(t_Chart.ObjectTypes) || ')' || gc_CRLF ||
                             '   and object.ObjectDefTypeId in (1, 2, 3)' || gc_CRLF;
      if t_Chart.CorralSchemaName is not null and t_Chart.CorralTableName is not null and t_Chart.CorralObjectIdColumnName is not null then                       
         t_FilterJoinClause  := pkg_DashboardUtils.FilterJoinClause(t_Chart, 'object.ObjectId', true);
      end if;
    elsif t_Chart.CorralSchemaName is not null and t_Chart.CorralTableName is not null then
      pkg_DashboardUtils.VerifyObjectConfig(t_Chart);
      t_ObjectsJoinClause := '  from '|| t_Chart.CorralSchemaName || '.' || t_Chart.CorralTableName ||' corral' || gc_CRLF;
      t_ObjectWhereClause := ' where 1 = 1'|| gc_CRLF;
    end if;
    
    if t_JobCount > 0 and t_ObjectCount = 0 then
      t_JobJoinClause := '  join dashboard.Jobs job'|| gc_CRLF ||
                         '    on job.JobId = object.ObjectId'|| gc_CRLF;
    elsif t_JobCount > 0 and t_ObjectCount > 0 then
      t_JobJoinClause := '  left outer join dashboard.Jobs job'|| gc_CRLF ||
                         '    on job.JobId = object.ObjectId'|| gc_CRLF;
    end if;
    
    if t_ProcessCount > 0 and t_ObjectCount = 0 then
      t_ProcessJoinClause := '  join dashboard.Processes process'|| gc_CRLF ||
                             '    on process.ProcessId = object.ObjectId'|| gc_CRLF;
    elsif t_ProcessCount > 0 and t_ObjectCount > 0 then  
      t_ProcessJoinClause := '  left outer join dashboard.Processes process'|| gc_CRLF ||
                             '    on process.ProcessId = object.ObjectId'|| gc_CRLF;        
    end if;
  
    if t_Chart.view_ in (gc_Detailed) then
      if t_Chart.GroupExpressions(1) is null then
        pkg_DashboardUtils.RaiseError(-20000,
            'Group 1 Expression not configured for ' || t_Chart.Title);
      end if;
    end if;

    if t_Chart.view_ = gc_Compare then
      if t_Chart.ToDate - t_Chart.FromDate > 365 then
        pkg_DashboardUtils.RaiseError(-20000,
            'Compare Previous Years date range must be a year or less.');
      end if;
      t_PreviousYears := 2;
    end if;

    if t_Chart.IncludeCancelled <> 'Y' then
     if t_JobCount > 0 and t_ObjectCount = 0 then
      t_ExcludeCancelledJoinClause :=
          '  join dashboard.JobStatuses js' || gc_CRLF ||
          '    on js.JobTypeId = job.JobTypeId' || gc_CRLF ||
          '   and js.StatusId = job.StatusId' || gc_CRLF ||
          '   and js.StatusType <> ''X''' || gc_CRLF;
     elsif t_JobCount > 0 and t_ObjectCount > 0 then
      t_ExcludeCancelledJoinClause :=
          '  left outer join dashboard.JobStatuses js' || gc_CRLF ||
          '    on js.JobTypeId = job.JobTypeId' || gc_CRLF ||
          '   and js.StatusId = job.StatusId' || gc_CRLF ||
          '   and js.StatusType <> ''X''' || gc_CRLF;
     end if;
    end if;
    
  -- Handle time period criteria
    if t_Chart.TimePeriodDetail is not null and t_ObjectCount > 0 then
      t_Columns := 'object.ObjectId' || ',' || gc_CRLF ||t_Chart.TimePeriodDetail;
      t_WhereClause := 'and '||t_Chart.TimePeriodDetail||' >= :2' || gc_CRLF ||
                       'and '||t_Chart.TimePeriodDetail||' < :3 + 1' || gc_CRLF;
    elsif t_Chart.TimePeriodDetail is not null and t_Chart.CorralObjectIdColumnName is not null and t_ObjectCount = 0 then
      t_Columns := 'corral.'||t_Chart.CorralObjectIdColumnName || ',' || gc_CRLF ||t_Chart.TimePeriodDetail;
      t_WhereClause := 'and '||t_Chart.TimePeriodDetail||' >= :2' || gc_CRLF ||
                       'and '||t_Chart.TimePeriodDetail||' < :3 + 1' || gc_CRLF;
    elsif t_ObjectCount > 0 then
      t_Columns := 'object.ObjectId' || ',' || gc_CRLF ||'object.CreatedDate';
      t_WhereClause := 'and object.CreatedDate >= :2' || gc_CRLF ||
                       'and object.CreatedDate < :3 + 1';
    end if;

    if t_Chart.GroupExpressions(1) is not null and t_Chart.view_ in (gc_Detailed, gc_Compare) then
      t_Columns := t_Columns || ',' || gc_CRLF || t_Chart.GroupExpressions(1);
    end if;

-- FIX ME - replace all FilterJoinClause with sql expression
    t_Sql :=
      'select ' || gc_CRLF ||
      t_Columns || gc_CRLF ||
      t_ObjectsJoinClause ||
      t_JobJoinClause ||
      t_ProcessJoinClause ||
      t_FilterJoinClause ||
      t_ExcludeCancelledJoinClause ||
      t_ObjectWhereClause ||
      t_WhereClause ;
      

    for i in 0..t_PreviousYears loop
      t_FromDate := add_months(t_Chart.FromDate, i * -12);
      t_ToDate := add_months(t_Chart.ToDate, i * -12);

      if t_Chart.GroupExpressions(1) is not null and t_Chart.view_ in (gc_Detailed, gc_Compare) then
        execute immediate t_Sql
        bulk collect into t_Data, t_Dates, t_GroupData
        using t_FromDate, t_ToDate;
      else
        execute immediate t_Sql
        bulk collect into t_Data, t_Dates
        using t_FromDate, t_ToDate;
      end if;

      pkg_DashboardUtils.Append(t_AllData,t_Data);
      pkg_DashboardUtils.Append(t_AllDates,t_Dates);
      pkg_DashboardUtils.Append(t_AllGroupData,t_GroupData);
    end loop;

    -- creating Timeline graph
    if t_Chart.DisplayType = gc_Timeline then
      pkg_DashboardUtils.GroupByTimeline(t_Chart, t_AllDates, t_AllGroupData);

    -- creating Bar graph
    elsif t_Chart.DisplayType = gc_BarGraph then
      if t_Chart.View_ = gc_Compare then
        pkg_DashboardUtils.GroupByValues(t_Chart, t_AllGroupData, t_AllDates);
      else
        if t_AllGroupData.count > 0 then
          pkg_DashboardUtils.GroupByValues(t_Chart, t_AllGroupData);
        else
          -- a single group only so just use the count
          pkg_DashboardChartUtils.SetSingleValue(t_Chart,
              t_AllData.Count, 'All');
        end if;
      end if;

    -- creating Gauge graph
    elsif t_Chart.DisplayType = gc_Gauge then
      pkg_DashboardChartUtils.SetGaugeValues(t_Chart, t_AllData.Count);
    end if;
    t_Data.delete;
    t_Dates.delete;
    t_GroupData.delete;
    t_AllDates.delete;
    t_AllData.delete;
    t_AllGroupData.delete;

    return pkg_DashboardChartUtils.ToJSON(t_Chart);
  end ObjectIntakeRate;
end pkg_Dashboard;

/

