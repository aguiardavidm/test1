create or replace package pkg_DashboardSql as

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_DateList is pkg_DashboardDefinition.udt_DateList;
  subtype udt_NumberList is pkg_DashboardDefinition.udt_NumberList;
  subtype udt_StringList is pkg_DashboardDefinition.udt_StringList;

  type udt_BindVarRecord is record (
    Name                                varchar2(30),
    DateValue                           date,
    NumberValue                         number,
    StringValue                         varchar2(4000),
    NumberList                          udt_NumberList
  );

  type udt_BindVarList is table of udt_BindVarRecord index by pls_integer;

  type udt_ResultRecord is record (
    Strings1                            udt_StringList,
    Strings2                            udt_StringList,
    Numbers                             udt_NumberList,
    Dates                               udt_DateList
  );

  gc_ArraySize                          constant number := 2000;
  gc_String                             constant varchar2(10) := 'String';
  gc_Number                             constant varchar2(10) := 'Number';
  gc_Date                               constant varchar2(10) := 'Date';

  /*---------------------------------------------------------------------------
   * AddBindVar()
   *-------------------------------------------------------------------------*/
  procedure AddBindVar (
    a_Name                              varchar2,
    a_DateValue                         date,
    a_BindVars                          in out nocopy udt_BindVarList
  );

  /*---------------------------------------------------------------------------
   * AddBindVar()
   *-------------------------------------------------------------------------*/
  procedure AddBindVar (
    a_Name                              varchar2,
    a_NumberValue                       number,
    a_BindVars                          in out nocopy udt_BindVarList
  );

  /*---------------------------------------------------------------------------
   * AddBindVar()
   *-------------------------------------------------------------------------*/
  procedure AddBindVar (
    a_Name                              varchar2,
    a_StringValue                       varchar2,
    a_BindVars                          in out nocopy udt_BindVarList
  );

  /*---------------------------------------------------------------------------
   * AddColumnType()
   *-------------------------------------------------------------------------*/
  procedure AddColumnType (
    a_ColumnType                        varchar2,
    a_ColumnTypes                       in out udt_StringList
  );

  /*---------------------------------------------------------------------------
   * ExecuteInto()
   *-------------------------------------------------------------------------*/
  procedure ExecuteInto (
    a_Sql                               varchar2,
    a_BindVars                          udt_BindVarList,
    a_ColumnTypes                       udt_StringList,
    a_ResultRecord                      out nocopy udt_ResultRecord
  );

end pkg_DashboardSql;

 
/

grant execute
on pkg_dashboardsql
to posseextensions;

create or replace package body pkg_DashboardSql as

  /*---------------------------------------------------------------------------
   * BindAll()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure BindAll (
    a_CursorId                          number,
    a_BindVars                          udt_BindVarList
  ) is
  begin
    for i in 1..a_BindVars.count loop
      if a_BindVars(i).DateValue is not null then
        dbms_sql.bind_variable(a_CursorId, a_BindVars(i).Name,
            a_BindVars(i).DateValue);
      elsif a_BindVars(i).NumberValue is not null then
        dbms_sql.bind_variable(a_CursorId, a_BindVars(i).Name,
            a_BindVars(i).NumberValue);
      elsif a_BindVars(i).StringValue is not null then
        dbms_sql.bind_variable(a_CursorId, a_BindVars(i).Name,
            a_BindVars(i).StringValue);
      elsif a_BindVars(i).NumberList is not null then
        dbms_sql.bind_array(a_CursorId, a_BindVars(i).Name,
            a_BindVars(i).NumberList);
      else
        pkg_DashboardUtils.RaiseError(-20000, 'No value to bind for "' ||
            a_BindVars(i).Name || '".');
      end if;
    end loop;
  end BindAll;

  /*---------------------------------------------------------------------------
   * RaiseColumnError()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure RaiseColumnError (
    a_ColumnType                        varchar2,
    a_MaxCount                          pls_integer
  ) is
  begin
    pkg_DashboardUtils.RaiseError(-20000, 'ExecuteInto handles a maximum of ' ||
        a_MaxCount || ' ' || a_ColumnType || ' columns.');
  end RaiseColumnError;

  /*---------------------------------------------------------------------------
   * AddBindVar()
   *-------------------------------------------------------------------------*/
  procedure AddBindVar (
    a_Name                              varchar2,
    a_DateValue                         date,
    a_BindVars                          in out nocopy udt_BindVarList
  ) is
  begin
    a_BindVars(a_BindVars.count + 1).Name := a_Name;
    a_BindVars(a_BindVars.count).DateValue := a_DateValue;
  end AddBindVar;

  /*---------------------------------------------------------------------------
   * AddBindVar()
   *-------------------------------------------------------------------------*/
  procedure AddBindVar (
    a_Name                              varchar2,
    a_NumberValue                       number,
    a_BindVars                          in out nocopy udt_BindVarList
  ) is
  begin
    a_BindVars(a_BindVars.count + 1).Name := a_Name;
    a_BindVars(a_BindVars.count).NumberValue := a_NumberValue;
  end AddBindVar;

  /*---------------------------------------------------------------------------
   * AddBindVar()
   *-------------------------------------------------------------------------*/
  procedure AddBindVar (
    a_Name                              varchar2,
    a_StringValue                       varchar2,
    a_BindVars                          in out nocopy udt_BindVarList
  ) is
  begin
    a_BindVars(a_BindVars.count + 1).Name := a_Name;
    a_BindVars(a_BindVars.count).StringValue := a_StringValue;
  end AddBindVar;

  /*---------------------------------------------------------------------------
   * AddColumnType()
   *-------------------------------------------------------------------------*/
  procedure AddColumnType (
    a_ColumnType                        varchar2,
    a_ColumnTypes                       in out udt_StringList
  ) is
  begin
    if a_ColumnType not in (gc_String, gc_Number, gc_Date) then
      pkg_DashboardUtils.RaiseError(-20000, 'Invalid column type "' ||
          a_ColumnType || '".');
    end if;
    a_ColumnTypes(a_ColumnTypes.count + 1) := a_ColumnType;
  end AddColumnType;

  /*---------------------------------------------------------------------------
   * ExecuteInto()
   *   Note: The code used in this procedure is a bit ugly, but I wanted to
   * have all the dynamic sql processing done and the results passed back
   * in a single procedure call.  This is the only way I could think of.
   *-------------------------------------------------------------------------*/
  procedure ExecuteInto (
    a_Sql                               varchar2,
    a_BindVars                          udt_BindVarList,
    a_ColumnTypes                       udt_StringList,
    a_ResultRecord                      out nocopy udt_ResultRecord
  ) is
    t_CursorId                          number;
    t_StringList1                       dbms_sql.varchar2_table;
    t_StringList2                       dbms_sql.varchar2_table;
    t_DateList                          dbms_sql.date_table;
    t_String1Pos                        pls_integer;
    t_String2Pos                        pls_integer;
    t_NumberPos                         pls_integer;
    t_DatePos                           pls_integer;
    t_Return                            number;
    t_RowCount                          number;
  begin
    pkg_Debug.PutSingleLine(a_Sql);
    t_CursorId := dbms_sql.open_cursor;

    dbms_sql.parse(t_CursorId, a_Sql, dbms_sql.NATIVE);

    -- define arrays for the columns to be selected and record their positions
    for i in 1..a_ColumnTypes.count loop
      case a_ColumnTypes(i)
        when gc_String then
          if t_String1Pos is null then
            t_String1Pos := i;
            dbms_sql.define_array(t_CursorId, i, t_StringList1, gc_ArraySize,
                1);
          elsif t_String2Pos is null then
            t_String2Pos := i;
            dbms_sql.define_array(t_CursorId, i, t_StringList2, gc_ArraySize,
                1);
          else
            RaiseColumnError(gc_String, 2);
          end if;
        when gc_Number then
          if t_NumberPos is null then
            t_NumberPos := i;
            dbms_sql.define_array(t_CursorId, i, a_ResultRecord.Numbers,
                gc_ArraySize, 1);
          else
            RaiseColumnError(gc_Number, 1);
          end if;
        when gc_Date then
          if t_DatePos is null then
            t_DatePos := i;
            dbms_sql.define_array(t_CursorId, i, t_DateList, gc_ArraySize, 1);
          else
            RaiseColumnError(gc_Date, 1);
          end if;
        else
          pkg_DashboardUtils.RaiseError(-20000, 'Invalid column type "' ||
              a_ColumnTypes(i) || '".');
      end case;
    end loop;

    BindAll(t_CursorId, a_BindVars);

    t_Return := dbms_sql.execute(t_CursorId);

    -- fetch the rows from the query
    -- note: in 11g we could get rid of this and the following datatype
    -- conversion by using dbms_sql.to_RefCursor and then execute immediate
    loop
      t_RowCount := dbms_sql.fetch_rows(t_CursorId);

      if t_String1Pos is not null then
        dbms_sql.column_value(t_CursorId, t_String1Pos, t_StringList1);
      end if;

      if t_String2Pos is not null then
        dbms_sql.column_value(t_CursorId, t_String2Pos, t_StringList2);
      end if;

      if t_NumberPos is not null then
        dbms_sql.column_value(t_CursorId, t_NumberPos, a_ResultRecord.Numbers);
      end if;

      if t_DatePos is not null then
        dbms_sql.column_value(t_CursorId, t_DatePos, t_DateList);
      end if;

      exit when t_RowCount < gc_ArraySize;
    end loop;

    dbms_sql.close_cursor(t_CursorId);


    -- convert and load the values into the return record
    for i in 1..t_StringList1.count loop
      a_ResultRecord.Strings1(i) := t_StringList1(i);
    end loop;
    t_StringList1.delete;

    for i in 1..t_StringList2.count loop
      a_ResultRecord.Strings2(i) := t_StringList2(i);
    end loop;
    t_StringList2.delete;

    for i in 1..t_DateList.count loop
      a_ResultRecord.Dates(i) := t_DateList(i);
    end loop;
    t_DateList.delete;

  exception
  when others then
    if dbms_sql.is_open(t_CursorId) then
      dbms_sql.close_cursor(t_CursorId);
    end if;
    raise;
  end ExecuteInto;

end pkg_DashboardSql;

/

