create or replace package pkg_SecurityQuery as

  /*---------------------------------------------------------------------------
   * Types
   *-------------------------------------------------------------------------*/
  subtype udt_Id is pkg_DashboardDefinition.udt_Id;

  /*---------------------------------------------------------------------------
   * EffectiveUserId()
   *   Returns the user id of the user whose security scheme is currently
   * active.
   *-------------------------------------------------------------------------*/
  function EffectiveUserId
  return udt_Id;

end pkg_SecurityQuery;
 
/

create or replace package body pkg_SecurityQuery as

  /*---------------------------------------------------------------------------
   * EffectiveUserId()
   *-------------------------------------------------------------------------*/
  function EffectiveUserId
  return udt_Id is
  begin
    return api.pkg_SecurityQuery.EffectiveUserId();
  end;

end pkg_SecurityQuery;
/

