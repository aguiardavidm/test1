create or replace package pkg_DashboardUtils as

  /*--------------------------------------------------------------------------
   * Types
   *------------------------------------------------------------------------*/
  subtype udt_Id is pkg_DashboardDefinition.udt_Id;
  subtype udt_IdList is pkg_DashboardDefinition.udt_IdList;
  subtype udt_StringList is pkg_DashboardDefinition.udt_StringList;
  subtype udt_DateList is pkg_DashboardDefinition.udt_DateList;
  subtype udt_NumberList is pkg_DashboardDefinition.udt_NumberList;
  subtype udt_Chart is pkg_DashboardDefinition.udt_Chart;
  subtype udt_NumberListByString is
      pkg_DashboardDefinition.udt_NumberListByString;
  subtype udt_DataMatrix is pkg_DashboardDefinition.udt_DataMatrix;
  subtype udt_UrlParameters is pkg_DashboardDefinition.udt_UrlParameters;
  subtype udt_CorralTable is pkg_DashboardDefinition.udt_CorralTable;

  g_NullNumberList                      udt_NumberList;
  g_NullStringList                      udt_StringList;

  /*--------------------------------------------------------------------------
   * Constants
   *------------------------------------------------------------------------*/
  -- constants for Function Name
  gc_Count                              constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Count;
  gc_Sum                                constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Sum;
  gc_Average                            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Average;
  gc_NullValue                          constant varchar2(10) :=
      pkg_DashboardDefinition.gc_NullValue;

  /*---------------------------------------------------------------------------
   * AddSection()
   *   Add a section to a string, separating the section from the remainder
   * of the string, if necessary.
   *-------------------------------------------------------------------------*/
  procedure AddSection (
    a_String                            in out varchar2,
    a_Section                           varchar2,
    a_Separator                         varchar2
  );

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_StringList,
    a_SecondList                        udt_StringList
  );

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_DateList,
    a_SecondList                        udt_DateList
  );

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_NumberList,
    a_SecondList                        udt_NumberList
  );

  /*--------------------------------------------------------------------------
   * ConvertToObjectList()
   *   Convert an udt_IdList to a dashboard.pkg_ObjectList;
   *------------------------------------------------------------------------*/
  function ConvertToObjectList (
    a_IdList                            udt_IdList
  ) return dashboard.udt_ObjectList;

  /*---------------------------------------------------------------------------
   * GetChartJSON()
   *  Run the PL/SQL procedure for the Chart with the arguments and return
   * the JSON result.
   *-------------------------------------------------------------------------*/
   function GetChartJSON (
     a_ChartId                          udt_Id,
     a_ChartArgs                        varchar2
   ) return clob;

  /*---------------------------------------------------------------------------
   * GetChartInfo()
   *-------------------------------------------------------------------------*/
   function GetChartInfo (
     a_ChartId                          udt_Id
   ) return clob;

  /*---------------------------------------------------------------------------
   * GetDashboardInfo()
   *  For a given Dashboard, retrieve the information about the Charts.
   *-------------------------------------------------------------------------*/
  function GetDashboardInfo (
    a_DashboardName                     varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * SetDashboardInfo()
   *  Update the visible Charts for the current user for a given dashboard.
   *-------------------------------------------------------------------------*/
  procedure SetDashboardInfo (
    a_ChartObjectIds                varchar2,
    a_VisibleList                   varchar2
  );

  /*---------------------------------------------------------------------------
   * GetAccessGroupUsersJSON()
   *-------------------------------------------------------------------------*/
  function GetAccessGroupUsersJSON (
    a_AccessGroupName                  varchar2
  ) return clob;

  /*---------------------------------------------------------------------------
   * VerifyObjectConfig()
   *-------------------------------------------------------------------------*/
  procedure VerifyObjectConfig(
    a_Chart                             udt_Chart
  );
  /*---------------------------------------------------------------------------
   * FilterJoinClause()
   *-------------------------------------------------------------------------*/
  function FilterJoinClause(
    a_Chart                             udt_Chart,
    a_JoinColumn                        varchar2,
    a_AlwaysJoin                        boolean default false
  ) return varchar2;

  /*---------------------------------------------------------------------------
   * GroupByTimeline()
   *   GroupByPeriod should be one of 'month', 'week' or 'day'.
   *-------------------------------------------------------------------------*/
  procedure GroupByTimeline (
    a_Chart                             in out nocopy udt_Chart,
    a_Dates                             udt_DateList,
    a_GroupValues                       udt_StringList default g_NullStringList,
    a_FunctionName                      varchar2 default gc_Count,
    a_FunctionValues                    udt_NumberList default g_NullNumberList
  );

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Chart                             in out nocopy udt_Chart,
    a_xValues                           in out nocopy udt_StringList,
    a_sgValues                          udt_StringList default g_NullStringList,
    a_xDistinctValues                   udt_StringList default g_NullStringList,
    a_sgDistinctValues                  udt_StringList default g_NullStringList
  );

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Chart                             in out nocopy udt_Chart,
    a_xValues                           in out nocopy udt_StringList,
    a_sgValues                          udt_StringList,
    a_xDistinctValues                   udt_StringList,
    a_sgDistinctValues                  udt_StringList,
    a_FunctionName                      varchar2,
    a_FunctionValues                    udt_NumberList
  );

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *   This overload is for Compare Previous Years Bar graphs.
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Chart                             in out nocopy udt_Chart,
    a_xValues                           in out nocopy udt_StringList,
    a_Dates                             udt_DateList,
    a_xDistinctValues                   udt_StringList default g_NullStringList
  );

  /*---------------------------------------------------------------------------
   * RaiseError()
   *--------------------------------------------------------------------------*/
  procedure RaiseError (
    a_ErrorNumber                       number,
    a_Message                           varchar2
  );

  /*---------------------------------------------------------------------------
   * Split()
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                            varchar2,
    a_Separator                         varchar2
  ) return udt_StringList;

  /*---------------------------------------------------------------------------
   * WorkingDaysBetween()
   *--------------------------------------------------------------------------*/
  function WorkingDaysBetween (
    a_StartDate                         date,
    a_EndDate                           date
  ) return number;
  
  /*---------------------------------------------------------------------------
   * FormatStatusList()
   *-------------------------------------------------------------------------*/
  function FormatStatusList(
    a_Statuses                           varchar2
  ) return varchar2;
  
  /*---------------------------------------------------------------------------
   * FormatObjectDefIdList()
   *-------------------------------------------------------------------------*/
  function FormatObjectDefIdList(
    a_ObjectTypes                         varchar2
  ) return varchar2;
  
  /*---------------------------------------------------------------------------
   * ObjectDefIdList()
   * Returns a list of objects.
   *-------------------------------------------------------------------------*/
  function ObjectDefIdList(
    a_ObjectTypes                         varchar2
  ) return udt_ObjectList;
 
 /*---------------------------------------------------------------------------
  * GetExpressionSyntax() 
  * A utility function that will return the correct sytax into the dynamic SQL.
  *-------------------------------------------------------------------------*/
  function GetExpressionSyntax (
    a_ChartId                  udt_Id,
    a_ExpressionColumnName     varchar2
  ) return varchar2;
 
end pkg_DashboardUtils;

 
/

grant execute
on pkg_dashboardutils
to posseextensions;

create or replace package body pkg_DashboardUtils as

  /*--------------------------------------------------------------------------
   * Types - PRIVATE
   *------------------------------------------------------------------------*/
  type udt_Filter is record (
    ColumnName                  varchar2(30),
    ColumnDefId                 udt_Id,
    Stored                      varchar2(1),
    Indexed                     varchar2(1),
    LookupIndexed               varchar2(1),
    EndPointName                varchar2(30),
    LookupColumnName            varchar2(30),
    FromValue                   varchar2(4000),
    ToValue                     varchar2(4000)
  );
  type udt_FilterList is table of udt_Filter index by binary_integer;

  /*--------------------------------------------------------------------------
   * Constants - PRIVATE
   *------------------------------------------------------------------------*/
  -- constant for udt_Chart.view_
  gc_Compare				            constant varchar2(10) :=
      pkg_DashboardDefinition.gc_Compare;

  -- constant EOL characters
  gc_CRLF                               constant varchar2(2) :=
      chr(13) || chr(10);

  /*---------------------------------------------------------------------------
   * CorralColumnDataType()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  function CorralColumnDataType (
    a_Chart                             udt_Chart,
    a_ColumnName                        varchar2
  ) return varchar2 is
    t_DataType                          varchar2(110);
  begin
    begin
      select data_type
        into t_DataType
        from all_tab_columns
        where owner = upper(a_Chart.CorralSchemaName)
          and table_name = upper(a_Chart.CorralTableName)
          and column_name = upper(a_ColumnName);
    exception
    when no_data_found then
      null;
    end;

    if t_DataType is null then
      RaiseError(-20000, 'A "' || a_ColumnName ||
          '" column does not exist on "' || a_Chart.CorralSchemaName || '.' ||
          a_Chart.CorralTableName || '.');
    end if;

    return t_DataType;
  end CorralColumnDataType;

  /*---------------------------------------------------------------------------
   * GenerateDatePoints()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  function GenerateDatePoints (
    a_FromDate                          date,
    a_ToDate                            date,
    a_TruncBy                           varchar2,
    a_Format                            varchar2,
    a_GroupByPeriod                     varchar2
  ) return udt_StringList is
    t_Date                              date;
    t_DatePoints                        udt_StringList;
  begin
    t_Date := a_FromDate;
    while t_Date <= a_ToDate loop
      t_DatePoints(t_DatePoints.count + 1) :=
          to_char(trunc(t_Date, a_TruncBy), a_Format);

      -- NOTE: if this code is changed then the case statement in
      -- GroupByTimeline() must be modifed to match
      case a_GroupByPeriod
        when 'year' then
          t_Date := add_months(t_Date, 12);
        when 'month' then
          t_Date := add_months(t_Date, 1);
        when 'week' then
          t_Date := t_Date + 7;
        when 'day' then
          t_Date := t_Date + 1;
        else
          RaiseError(-20000,
              'Invalid GroupByPeriod: "' || a_GroupByPeriod || '".');
      end case;
    end loop;
    return t_DatePoints;
  end GenerateDatePoints;

  /*---------------------------------------------------------------------------
   * GroupParameters()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure GroupParameters (
    a_Chart                             udt_Chart,
    a_GroupByPeriod                     in out varchar2,
    a_TruncBy                           out varchar2,
    a_Format                            out varchar2,
    a_CompareYears                      boolean
  ) is
    t_DaysDiff                          number;
  begin

    if a_GroupByPeriod = 'auto' then
      t_DaysDiff := a_Chart.ToDate - a_Chart.FromDate;
      if t_DaysDiff <= 31 then
        a_GroupByPeriod := 'day';
      elsif t_DaysDiff <= 731 then
        a_GroupByPeriod := 'month';
      else
        a_GroupByPeriod := 'year';
      end if;
    end if;

    -- NOTE: if this code is changed then the case statement in
    -- GenerateDatePoints() must be modifed to match
    case a_GroupByPeriod
      when 'year' then
        a_TruncBy := 'YYYY';
        a_Format := 'YYYY';
      when 'month' then
        a_TruncBy := 'MM';
        a_Format := 'Mon YYYY';
      when 'week' then
        a_TruncBy := 'IW';
        a_Format := 'Mon DD YYYY';
      when 'day' then
        a_TruncBy := 'DD';
        a_Format := 'Mon DD YYYY';
      else
        RaiseError(-20000,
            'Invalid GroupByPeriod: "' || a_GroupByPeriod || '".');
    end case;

    -- drop the year from the x axis data when comparing years
    if a_CompareYears then
      a_Format := replace(a_Format, ' YYYY');
    end if;
  end GroupParameters;

  /*---------------------------------------------------------------------------
   * FormatString()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  function FormatString (
    a_Value                             varchar2,
    a_DataType                          varchar2
  ) return varchar2 is
  begin
    if a_DataType = 'NUMBER' then
      return a_Value;
    elsif a_DataType = 'VARCHAR2' then
      return '''' || replace(a_Value,  '''', '''''') || '''';
    else
      RaiseError(-20000,
          'Unsupported filter column data type: ' || a_DataType);
    end if;
  end FormatString;

  /*---------------------------------------------------------------------------
   * SparseDistinctList()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure SparseDistinctList (
    a_Values                            udt_StringList,
    a_DistinctValues                    udt_StringList,
    a_OutList                           in out nocopy udt_NumberListByString
  ) is
    t_ValueList                         udt_NumberListByString;
    t_Value                             varchar2(100);
  begin
    -- get list of distinct values from the data
    for i in 1..a_Values.count loop
      t_ValueList(nvl(a_Values(i),gc_NullValue)) := null;
    end loop;

    -- build a sparse list of values with the seqnum as the data
    -- start with the a_DistinctValues if populated
    for i in 1..a_DistinctValues.count loop
      a_OutList(a_DistinctValues(i)) := i;
    end loop;

    -- append any additional values from the data to the array
    -- this way the added values will be in alphabetical order
    t_Value := t_ValueList.first();
    while t_Value is not null loop
      if not a_OutList.exists(t_Value) then
        a_OutList(t_Value) := a_OutList.count + 1;
      end if;
      t_Value := t_ValueList.next(t_Value);
    end loop;
  end SparseDistinctList;

    /*---------------------------------------------------------------------------
   * SortArray()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure SortArray (
    a_SortedList                       out nocopy udt_StringList,
    a_OutList                          in out nocopy udt_NumberListByString
  ) is
    t_TempOutList                      udt_NumberListByString;
    t_OperationalList                  udt_NumberListByString;
    t_xValue                           varchar2(100);
    t_nextXValue                       varchar2(100);
    t_xValueCount                      number;
    t_Count                            number := 0;
    t_Value                            varchar2(4000);
  begin
    
    t_TempOutList := a_OutList;
    t_OperationalList := a_OutList;
    a_OutList.delete();             
    t_Value := t_TempOutList.first();
    while t_Value is not null loop
      t_xValue := t_OperationalList.first();
      t_xValueCount := t_OperationalList(t_xValue); 
      t_nextXValue := t_OperationalList.next(t_xValue);
      while t_nextXValue is not null loop
         if t_OperationalList(t_nextXValue) > t_xValueCount then
           t_xValueCount := t_OperationalList(t_nextXValue);
           t_xValue := t_nextXValue;
         end if;
         t_nextXValue := t_OperationalList.next(t_nextXValue);
      end loop;
      t_Count := t_Count + 1;
      a_SortedList(t_Count) := t_xValue;
      a_OutList(t_xValue) := t_xValueCount;
      t_OperationalList.delete(t_xValue);
      t_Value := t_TempOutList.next(t_Value);
    end loop;
    
  end SortArray;
  /*---------------------------------------------------------------------------
   * ApplyTopN()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure ApplyTopN (
    a_Values                           in out nocopy udt_StringList,
    a_Chart                            in out nocopy udt_Chart,
    a_OutList                          in out nocopy udt_NumberListByString
  ) is
    t_ValueList                        udt_NumberListByString;
    t_TopNList                         udt_NumberListByString;
    t_NotTopNList                      udt_NumberListByString;
    t_OutList                          udt_NumberListByString;
    t_SortedList                       udt_StringList;
    t_OtherList                        udt_StringList;
    t_Value                            varchar2(100);
    t_x                                varchar2(100);
    t_ListShow                         varchar2(4000);
    t_Count                            number := 0;
  begin
    -- build a array list of values with top N Values.
    for i in 1..a_Values.count loop
     if t_ValueList.exists(nvl(a_Values(i),gc_NullValue)) then
       t_ValueList(nvl(a_Values(i),gc_NullValue)) := t_ValueList(nvl(a_Values(i),gc_NullValue)) + 1;
     else
       t_ValueList(nvl(a_Values(i),gc_NullValue)) := 1;
     end if;
    end loop;
    
    t_x := a_OutList.first();
    while t_x is not null loop
      if not t_ValueList.exists(t_x) then
         t_ValueList(t_x) := t_ValueList.count + 1;
      end if;
      t_x := a_OutList.next(t_x);
    end loop;
    
    if a_Chart.DefaultOrderBy = 'Rank' then 
      SortArray(t_SortedList,t_ValueList);
      t_OutList := a_OutList;
      a_OutList.delete();
      for i in 1..t_SortedList.Count loop
        if i <= a_Chart.DefaultTopNValues then
           t_TopNList(t_SortedList(i)) := t_TopNList.count + 1;
           a_OutList(t_SortedList(i)) := a_OutList.count + 1;
        else
           t_NotTopNList(t_SortedList(i)) := t_NotTopNList.count + 1;
        end if;
      end loop;
      if t_TopNList.count < t_OutList.count then
          t_TopNList('zzzzz') := t_TopNList.count + 1;
          a_OutList('zzzzz') := a_OutList.count + 1; 
      elsif t_TopNList.count = t_OutList.count then
          t_TopNList('zzzzzz') := t_TopNList.count + 1;---I am going to eliminate this at the end..
          a_OutList('zzzzzz') := a_OutList.count + 1; 
      end if;
    else
      t_x := a_OutList.first();
      while t_x is not null loop
        if t_Count < a_Chart.DefaultTopNValues then
           t_Count := t_Count + 1;
           t_TopNList(t_x) := t_TopNList.count + 1;
        else
          t_NotTopNList(t_x) := t_NotTopNList.count + 1; 
        end if;
        t_x := a_OutList.next(t_x);
      end loop;
      
      if t_TopNList.count < a_OutList.Count then
         t_TopNList('zzzzz') := t_TopNList.count + 1;
      elsif t_TopNList.count = a_OutList.Count then
         t_TopNList('zzzzzz') := t_TopNList.count + 1;---I am going to eliminate this at the end..
      end if;
      -- create a new array with Top N Values and 'Other'
      a_OutList.delete();
      t_Value := t_TopNList.first();
      while t_Value is not null loop
        if not a_OutList.exists(t_Value) then
          a_OutList(t_Value) := a_OutList.count + 1;
        end if;
        t_Value := t_TopNList.next(t_Value);
      end loop;
    end if;
    
    if t_NotTopNList.count > 0 then
      a_Chart.NotTopNList := t_NotTopNList;
    end if;  
  
   -- modified all the a_Values not in Top N to be at the end
    for i in 1..a_Values.count loop
     if a_Values(i) is not null then
       if not t_TopNList.exists(nvl(a_Values(i),gc_NullValue)) and t_TopNList.exists('zzzzz') then
          a_Values(i) := 'zzzzz';
       elsif not t_TopNList.exists(nvl(a_Values(i),gc_NullValue)) and t_TopNList.exists('zzzzzz') then
          a_Values(i) := 'zzzzzz';---I am going to eliminate this at the end..
       end if;
     end if;
    end loop;
    
    
    
  end ApplyTopN;
  
  /*---------------------------------------------------------------------------
   * YearGroupValues()  -- PRIVATE
   *   Create a parallel array of year values from the date list.
   *-------------------------------------------------------------------------*/
  procedure YearGroupValues (
    a_Chart                             udt_Chart,
    a_Dates                             udt_DateList,
    a_yValues                           in out nocopy udt_StringList
  ) is
    t_yRanges                           udt_StringList;
    t_Date                              date;
  begin
    -- first, the easy way - date range within a single year
    if to_char(a_Chart.FromDate, 'YYYY') = to_char(a_Chart.ToDate, 'YYYY')
        then
      for i in 1..a_Dates.count loop
        a_yValues(i) := to_char(a_Dates(i), 'YYYY');
      end loop;

    -- the hard way - date range includes two years
    else
      t_yRanges(3) := to_char(a_Chart.FromDate, 'YYYY')
          || ' - ' || to_char(a_Chart.ToDate, 'YYYY');
      t_yRanges(2) := to_char(add_months(a_Chart.FromDate, -12), 'YYYY')
          || ' - ' || to_char(add_months(a_Chart.ToDate, -12), 'YYYY');
      t_yRanges(1) := to_char(add_months(a_Chart.FromDate, -24), 'YYYY')
          || ' - ' || to_char(add_months(a_Chart.ToDate, -24), 'YYYY');

      t_Date := add_months(a_Chart.ToDate, -24) + 1;
      for i in 1..a_Dates.count loop
        if a_Dates(i) >= a_Chart.FromDate then
          a_yValues(i) := t_yRanges(3);
        elsif a_Dates(i) < t_Date then
          a_yValues(i) := t_yRanges(1);
        else
          a_yValues(i) := t_yRanges(2);
        end if;
      end loop;
    end if;
  end YearGroupValues;

  /*---------------------------------------------------------------------------
   * AddSection()
   *-------------------------------------------------------------------------*/
  procedure AddSection (
    a_String                            in out varchar2,
    a_Section                           varchar2,
    a_Separator                         varchar2
  ) is
  begin
    if a_Section is not null then
      if a_String is null then
        a_String := a_Section;
      else
        a_String := a_String || a_Separator || a_Section;
      end if;
    end if;
  end AddSection;

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_StringList,
    a_SecondList                        udt_StringList
  ) is
  begin
    for i in 1..a_SecondList.count loop
      a_MainList(a_MainList.count + 1) := a_SecondList(i);
    end loop;
  end Append;

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_DateList,
    a_SecondList                        udt_DateList
  ) is
  begin
    for i in 1..a_SecondList.count loop
      a_MainList(a_MainList.count + 1) := a_SecondList(i);
    end loop;
  end Append;

  /*---------------------------------------------------------------------------
   * Append()
   *   Add all the values in a_SecondList to a_MainList.
   *-------------------------------------------------------------------------*/
  procedure Append (
    a_MainList                          in out nocopy udt_NumberList,
    a_SecondList                        udt_NumberList
  ) is
  begin
    for i in 1..a_SecondList.count loop
      a_MainList(a_MainList.count + 1) := a_SecondList(i);
    end loop;
  end Append;

  /*--------------------------------------------------------------------------
   * ConvertToObjectList()
   *   Convert an udt_IdList to an dashboard.pkg_ObjectList;
   *------------------------------------------------------------------------*/
  function ConvertToObjectList (
    a_IdList                udt_IdList
  ) return dashboard.udt_ObjectList is
    t_Objects              dashboard.udt_ObjectList;
    t_Index                pls_integer;
    t_Count                pls_integer;
  begin
    t_Objects := dashboard.udt_ObjectList();
    t_Objects.extend(a_IdList.count);

    t_Count := 1;
    t_Index := a_IdList.first;
    while t_Index is not null loop
      t_Objects(t_Count) := dashboard.udt_Object(a_IdList(t_Index));
      t_Count := t_Count + 1;
      t_Index := a_IdList.next(t_Index);
    end loop;

    return t_Objects;
  end;

  /*---------------------------------------------------------------------------
   * GetChartInfo()
   *-------------------------------------------------------------------------*/
  function GetChartInfo (
    a_ChartId                           udt_Id
  ) return clob is
    t_Chart                             udt_Chart;
  begin
    t_Chart := pkg_DashboardChartUtils.NewChart(a_ChartId, null);
    return pkg_DashboardChartUtils.ToJSON(t_Chart);
  end;

  /*---------------------------------------------------------------------------
   * GetChartJSON()
   *-------------------------------------------------------------------------*/
  function GetChartJSON (
    a_ChartId                           udt_Id,
    a_ChartArgs                         varchar2
  ) return clob is
    t_JSON                              clob;
    t_SQL                               varchar2(31767);
    t_ChartTypeId                       udt_Id;
    t_ProcedureName                     varchar2(100);
  begin
    select ct.ProcedureName
      into t_ProcedureName
      from
        r_DashboardChartChartType r
        join o_DashboardChartType ct
            on ct.ObjectId = r.DashboardChartTypeId
      where r.DashboardChartId = a_ChartId;

    if t_ProcedureName is not null or t_ProcedureName = 'null' then
      t_SQL := 'begin' || chr(10) || ' :p_RetVal := ' || t_ProcedureName ||
          '(:p_ChartId, :p_ChartArgs);' || chr(10) || 'end;';
      execute immediate t_SQL using out t_JSON, a_ChartId, utl_url.unescape(a_ChartArgs);
    else
      t_JSON := '{}';
    end if;

    rollback; -- clear global temp table
    return t_JSON;
  end;

  /*---------------------------------------------------------------------------
   * GetDashboardInfo()
   *-------------------------------------------------------------------------*/
  function GetDashboardInfo (
    a_DashboardName                     varchar2
  ) return clob is
    t_JSON                              clob;
    t_ChartJSON                         clob;
    t_Chart                             pkg_DashboardDefinition.udt_Chart;
    t_IncludeExclude                    varchar2(30);

-- FIX ME - the performance of this query may be poor because there is no
-- indexed access into DashboardName
    cursor c_Charts is
      select
        c.ObjectId ChartId,
        dc.Name Category,
        c.SeqNum,
        c.VisibleByDefault
      from
        dashboard.o_Dashboard d
        join dashboard.r_Dashboard_DashboardChart rx
            on rx.DashboardId = d.ObjectId
        join dashboard.o_DashboardChart c
            on c.ObjectId = rx.DashboardChartId
        left outer join dashboard.r_Dashboard_DashboardCategory rdc
            on rdc.DashboardChartId = c.ObjectId
        left outer join dashboard.o_DashboardCategory dc
            on dc.ObjectId = rdc.DashboardCategoryId
      where trim(upper(d.DashboardName)) = trim(upper(a_DashboardName))
      order by dc.SeqNum, dc.Name, c.SeqNum; -- c.Name is need when multiple categories have the same SeqNum
  begin
    t_JSON := '[';
    for c in c_Charts loop
      t_Chart := pkg_DashboardChartUtils.NewChart(c.ChartId, null);
      t_Chart.category := c.Category;

      begin
        select IncludeExclude
        into t_IncludeExclude
        from r_DashboardChartUser
        where DashboardChartId = c.ChartId
          and UserId = dashboard.pkg_SecurityQuery.EffectiveUserId();

        if t_IncludeExclude = 'Include' then
          t_Chart.visible := 'Y';
        else
          t_Chart.visible := 'N';
        end if;
      exception
      when no_data_found then
        t_Chart.visible := c.VisibleByDefault;
      end;

      t_ChartJSON := pkg_DashboardChartUtils.ToJSON(t_Chart);
      if t_JSON <> '[' then
        t_JSON := t_JSON || ',';
      end if;
      t_JSON := t_JSON || t_ChartJSON;
    end loop;

    return t_JSON || ']';
  end;

  /*---------------------------------------------------------------------------
   * SetDashboardInfo()
   *-------------------------------------------------------------------------*/
  procedure SetDashboardInfo (
    a_ChartObjectIds                varchar2,
    a_VisibleList                   varchar2
  ) is
    t_ChartList                         udt_StringList;
    t_VisibleList                       udt_StringList;
    t_VisibleByDefault                  varchar2(1);
    t_RelationshipId                    udt_Id;
    t_EndPointId                        udt_Id;
    t_EffectiveUserId                   udt_Id;
  begin
    t_ChartList := pkg_DashboardChartUtils.Split(a_ChartObjectIds, ',');
    t_VisibleList := pkg_DashboardChartUtils.Split(a_VisibleList, ',');
    t_EndPointId := api.pkg_ConfigQuery.EndPointIdForName(
        'o_DashboardChart', 'User');
    t_EffectiveUserId := dashboard.pkg_SecurityQuery.EffectiveUserId();

    for i in 1..t_ChartList.count loop
      begin
        select r.RelationshipId
        into t_RelationshipId
        from api.Relationships r
        where r.FromObjectId = t_ChartList(i)
          and r.EndPointId = t_EndPointId
          and r.ToObjectId = t_EffectiveUserId;
      exception
      when no_data_found then
        t_RelationshipId := null;
      end;

      t_VisibleByDefault := api.pkg_ColumnQuery.Value(t_ChartList(i), 'VisibleByDefault');
      if t_VisibleList(i) = t_VisibleByDefault and t_RelationshipId is not null then
        api.pkg_RelationshipUpdate.Remove(t_RelationshipId);
      elsif t_VisibleList(i) <> t_VisibleByDefault then
        if t_RelationshipId is null then
          t_RelationshipId := api.pkg_RelationshipUpdate.New(t_EndPointId,
              t_ChartList(i), t_EffectiveUserId);
        end if;

        if t_VisibleList(i) = 'Y' then
          api.pkg_ColumnUpdate.SetValue(t_RelationshipId, 'IncludeExclude', 'Include');
        else
          api.pkg_ColumnUpdate.SetValue(t_RelationshipId, 'IncludeExclude', 'Exclude');
        end if;
      end if;
    end loop;
  end;

  /*---------------------------------------------------------------------------
   * GetAccessGroupUsersJSON()
   *-------------------------------------------------------------------------*/
  function GetAccessGroupUsersJSON (
    a_AccessGroupName                   varchar2
  ) return clob is
    t_JSON                              clob;
    t_UserIds                           udt_IdList;
    t_Names                             udt_StringList;
  begin
    select
      u.UserId,
      u.Name
    bulk collect into
      t_UserIds,
      t_Names
    from
      dashboard.Users u
      join dashboard.AccessGroupUsers agu
          on agu.UserId = u.UserId
          and u.Active = 'Y'
      join dashboard.AccessGroups ag
          on ag.AccessGroupId = agu.AccessGroupId
    where upper(ag.Description) = upper(a_AccessGroupName)
    order by u.Name;

     t_JSON :=
       '{Users: [';
       for i in 1..t_UserIds.count loop
          t_JSON := concat(t_JSON, '{id: ' || t_UserIds(i) || ', name: "' ||
              t_Names(i) || '"},');
       end loop;

       if t_UserIds.count > 0 then
          t_JSON := concat(substr(t_JSON, 1, length(t_JSON)-1), ']}');
       else
          t_JSON := concat(t_JSON,']}');
       end if;

    return t_JSON;
  end;

  /*---------------------------------------------------------------------------
   * VerifyCorralConfig()  -- PRIVATE
   *-------------------------------------------------------------------------*/
  procedure VerifyCorralConfig (
    a_Chart                             udt_Chart
  ) is
  begin
    if a_Chart.CorralSchemaName is null then
      RaiseError(-20000,
          'Corral Table Schema not configured for "' || a_Chart.Title ||
          '" Chart.');
    elsif a_Chart.CorralTableName is null then
      RaiseError(-20000,
          'Corral Table Name not configured for "' || a_Chart.Title ||
          '" Chart.');
    elsif a_Chart.CorralObjectIdColumnName is null then
      RaiseError(-20000,
          'Corral Object Id Column Name not configured for "' ||
          a_Chart.Title || '" Chart.');
    end if;
  end VerifyCorralConfig;
  
  /*---------------------------------------------------------------------------
   * VerifyObjectConfig()
   *-------------------------------------------------------------------------*/
  procedure VerifyObjectConfig (
    a_Chart                             udt_Chart
  ) is
  begin
    if a_Chart.CorralSchemaName is null then
      RaiseError(-20000,
          'Corral Table Schema not configured for "' || a_Chart.Title ||
          '" Chart.');
    elsif a_Chart.CorralTableName is null then
      RaiseError(-20000,
          'Corral Table Name not configured for "' || a_Chart.Title ||
          '" Chart.');
    end if;
  end VerifyObjectConfig;

  /*---------------------------------------------------------------------------
   * FilterJoinClause()
   *-------------------------------------------------------------------------*/
  function FilterJoinClause(
    a_Chart                             udt_Chart,
    a_JoinColumn                        varchar2,
    a_AlwaysJoin                        boolean default false
  ) return varchar2 is
    t_FilterIds                         udt_IdList;
    t_ColumnNames                       udt_StringList;
    t_FromValues                        udt_StringList;
    t_ToValues                          udt_StringList;
    t_DataType                          varchar2(110);
    t_Sql                               varchar2(4000);
  begin
    
   if a_chart.FilterExpression is not null then  
      
      if  a_AlwaysJoin then
        VerifyCorralConfig(a_Chart);

        t_Sql :=
          '  join ' || a_Chart.CorralSchemaName || '.' || a_Chart.CorralTableName ||
              ' corral' || gc_CRLF ||
          '    on corral.' || a_Chart.CorralObjectIdColumnName || ' = ' || a_JoinColumn;
      end if;
    
   
      if t_Sql is not null then 
        t_Sql := t_Sql || gc_CRLF ||' and ' ||a_chart.FilterExpression;
      else 
        t_Sql := gc_CRLF ||' and ' ||a_chart.FilterExpression;
      end if; 
       
  else 

    if a_AlwaysJoin then
      VerifyCorralConfig(a_Chart);

      t_Sql :=
        '  join ' || a_Chart.CorralSchemaName || '.' || a_Chart.CorralTableName ||
            ' corral' || gc_CRLF ||
        '    on corral.' || a_Chart.CorralObjectIdColumnName || ' = ' || a_JoinColumn;
    end if;
    
 end if;
 
    if t_Sql is not null then
      t_Sql := t_Sql || gc_CRLF;
    end if;

    return t_Sql;
  end FilterJoinClause;

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Chart                             in out nocopy udt_Chart,
    a_xValues                           in out nocopy udt_StringList,
    a_sgValues                          udt_StringList default g_NullStringList,
    a_xDistinctValues                   udt_StringList default g_NullStringList,
    a_sgDistinctValues                  udt_StringList default g_NullStringList
  ) is
    t_NullNumberList                    udt_NumberList;
  begin
    GroupByValues(a_Chart, a_xValues, a_sgValues, a_xDistinctValues,
        a_sgDistinctValues, gc_Count, t_NullNumberList);
  end GroupByValues;

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Chart                             in out nocopy udt_Chart,
    a_xValues                           in out nocopy udt_StringList,
    a_sgValues                          udt_StringList,
    a_xDistinctValues                   udt_StringList,
    a_sgDistinctValues                  udt_StringList,
    a_FunctionName                      varchar2,
    a_FunctionValues                    udt_NumberList
  ) is
    t_xList                             udt_NumberListByString;
    t_sgList                            udt_NumberListByString;
    t_topNXValues                       udt_StringList;
    t_x                                 varchar2(10000);
    t_sg                                varchar2(10000);
    t_totalsg                           varchar2(10000);
    t_HasSubGroup                       boolean;
    t_Matrix                            udt_DataMatrix;
    t_CountMatrix                       udt_DataMatrix;
    c_Zero                              constant char(1) := '0';
  t_key  varchar2(100);
  t_AllDistinct udt_StringList;
  begin
    if a_FunctionName not in (gc_Count, gc_Sum, gc_Average) then
      RaiseError(-20000, 'Invalid function name "' ||
          a_FunctionName || '" passed in to GroupByValues.');
    end if;

    if a_sgValues.count > 0 and a_xValues.count <> a_sgValues.count then
      RaiseError(-20000, 'GroupByValues x and sub-group '
          || 'arrays are not the same length: ' || a_xValues.count || ' and '
          || a_sgValues.count);
    end if;

    if a_FunctionValues.count > 0
        and a_xValues.count <> a_FunctionValues.count then
      RaiseError(-20000, 'GroupByValues x and FunctionValue '
          || 'arrays are not the same length: ' || a_xValues.count || ' and '
          || a_FunctionValues.count);
    end if;

    -- build sparse lists of x and sub-group values with the seqnum as the data
    -- start with the a_xDistinctValues if populated
    SparseDistinctList(a_xValues, a_xDistinctValues, t_xList);
    if a_Chart.ChartType = 'ObjectSummary' then
      if a_Chart.DefaultTopNValues is not null and
         a_Chart.DefaultOrderBy is not null then
        ApplyTopN(a_xValues,a_Chart,t_xList);
      end if;
    end if;
    if a_sgValues.count > 0 then
      SparseDistinctList(a_sgValues, a_sgDistinctValues, t_sgList);
      t_HasSubGroup := true;
    else
      t_sgList(c_Zero) := 1;
      t_HasSubGroup := false;
    end if;
    
    -- create a matrix of all points with the value set to 0
    t_x := t_xList.first();
    while t_x is not null loop
      t_sg := t_sgList.first();
      while t_sg is not null loop
        t_Matrix(t_x)(t_sg) := 0;
        t_CountMatrix(t_x)(t_sg) := 0;
        t_sg := t_sgList.next(t_sg);
      end loop;
      t_x := t_xList.next(t_x);
    end loop;

    -- load the matrix with data
    t_sg := c_Zero;
    for i in 1..a_xValues.count loop
      t_x := nvl(a_xValues(i),gc_NullValue);
      if t_HasSubGroup then
        t_sg := a_sgValues(i);
      end if;
      if t_sg is not null then
        if a_FunctionName = gc_Count then
          t_Matrix(t_x)(t_sg) := t_Matrix(t_x)(t_sg) + 1;
        elsif a_FunctionValues(i) is not null then
          t_Matrix(t_x)(t_sg) := t_Matrix(t_x)(t_sg) + a_FunctionValues(i);
          t_CountMatrix(t_x)(t_sg) := t_CountMatrix(t_x)(t_sg) + 1;
        end if;
      end if;
    end loop;
          

    if a_FunctionName = gc_Average then
      -- calculate the average for each value in the matrix
      t_x := t_xList.first();
      while t_x is not null loop
        t_sg := t_sgList.first();
        while t_sg is not null loop
          if t_CountMatrix(t_x)(t_sg) <> 0 then
            t_Matrix(t_x)(t_sg) := round(t_Matrix(t_x)(t_sg)
                / t_CountMatrix(t_x)(t_sg));
          end if;
          t_sg := t_sgList.next(t_sg);
        end loop;
        t_x := t_xList.next(t_x);
      end loop;
    end if;
    
    
    t_CountMatrix.delete;

    pkg_DashboardChartUtils.ApplyDataMatrixToChart(t_Matrix,
        a_Chart, t_xList, t_sgList);
  end GroupByValues;

  /*---------------------------------------------------------------------------
   * GroupByValues()
   *   This overload is for Compare previous years Bar graphs.
   *-------------------------------------------------------------------------*/
  procedure GroupByValues (
    a_Chart                             in out nocopy udt_Chart,
    a_xValues                           in out nocopy udt_StringList,
    a_Dates                             udt_DateList,
    a_xDistinctValues                   udt_StringList default g_NullStringList
  ) is
    t_sgValues				udt_StringList;
  begin
    YearGroupValues(a_Chart, a_Dates, t_sgValues);
    GroupByValues(a_Chart, a_xValues, t_sgValues, a_xDistinctValues);
  end GroupByValues;

  /*---------------------------------------------------------------------------
   * GroupByTimeline()
   *-------------------------------------------------------------------------*/
  procedure GroupByTimeline (
    a_Chart                             in out nocopy udt_Chart,
    a_Dates                             udt_DateList,
    a_GroupValues                       udt_StringList default g_NullStringList,
    a_FunctionName                      varchar2 default gc_Count,
    a_FunctionValues                    udt_NumberList default g_NullNumberList
  ) is
    t_GroupByPeriod                     varchar2(10);
    t_TruncBy                           varchar2(4);
    t_Format                            varchar2(12);
    t_DatePoints                        udt_StringList;
    t_xValues                           udt_StringList;
    t_sgValues                          udt_StringList;
    t_CompareYears                      boolean := false;
    t_yRanges                           udt_StringList;
  begin
    if a_GroupValues.count > 0 and a_Dates.count <> a_GroupValues.count then
      RaiseError(-20000, 'GroupByTimeline Dates and Group '
          || 'arrays are not the same length: ' || a_Dates.count || ' and '
          || a_GroupValues.count);
    end if;

    if a_FunctionValues.count > 0 and a_Dates.count <> a_FunctionValues.count
        then
      RaiseError(-20000, 'GroupByTimeline Dates and Function '
          || 'arrays are not the same length: ' || a_Dates.count || ' and '
          || a_FunctionValues.count);
    end if;

    t_CompareYears := a_Chart.view_ = gc_Compare;
    t_GroupByPeriod := 'auto';
    GroupParameters(a_Chart, t_GroupByPeriod, t_TruncBy, t_Format,
        t_CompareYears);

    -- get a list of all the required date points
    t_DatePoints := GenerateDatePoints(a_Chart.FromDate, a_Chart.ToDate,
        t_TruncBy, t_Format, t_GroupByPeriod);

    -- convert the dates to the appropriate string value
    for i in 1..a_Dates.count loop
      t_xValues(i) := to_char(trunc(a_Dates(i), t_TruncBy), t_Format);
    end loop;

    if t_CompareYears then
      YearGroupValues(a_Chart, a_Dates, t_sgValues);
      GroupByValues(a_Chart, t_xValues, t_sgValues, t_DatePoints,
          g_NullStringList, a_FunctionName, a_FunctionValues);
    else
      GroupByValues(a_Chart, t_xValues, a_GroupValues, t_DatePoints,
          g_NullStringList, a_FunctionName, a_FunctionValues);
    end if;
  end GroupByTimeline;

  /*---------------------------------------------------------------------------
   * RaiseError()
   *--------------------------------------------------------------------------*/
  procedure RaiseError (
    a_ErrorNumber                       number,
    a_Message                           varchar2
  ) is
  begin
    raise_application_error(a_ErrorNumber, a_Message);
  end RaiseError;

  /*---------------------------------------------------------------------------
   * Split()
   *--------------------------------------------------------------------------*/
  function Split (
    a_String                            varchar2,
    a_Separator                         varchar2
  ) return udt_StringList is
  begin
    return pkg_DashboardChartUtils.Split(a_String, a_Separator);
  end;

  /*---------------------------------------------------------------------------
   * WorkingDaysBetween()
   *--------------------------------------------------------------------------*/
  function WorkingDaysBetween (
    a_StartDate                         date,
    a_EndDate                           date
  ) return number is
   t_StartDate                          date;
   t_EndDate                            date;
   t_Workdays                           pls_integer;
   t_NumHolidays                        pls_integer;
  begin

    /* Any null input will return a null output */
    if a_StartDate is null or a_EndDate is null then
      return null;
    end if;

    /* Initialization */
    t_StartDate := trunc(a_StartDate);
    t_EndDate := trunc(a_EndDate);

    /* Count Days */
    if t_EndDate >= t_StartDate then
      /* Date Range */
      t_workdays := t_EndDate - t_StartDate
            /* Subtracts the number of weekend days by determining the number
             of days, dividing by 7 to weeks and then multiplying by 2 to
             get the number of weekend days */
         - ((trunc(t_EndDate,'D') - trunc(t_StartDate,'D'))/7)*2 + 1;

      /* Adjust for ending date on a saturday */
      if to_char(t_EndDate,'D') = '7' then
        t_workdays := t_workdays - 1;
      end if;

      /* Adjust for starting date on a sunday */
      if to_char(t_StartDate,'D') = '1' then
        t_workdays := t_workdays - 1;
      end if;
    else
      t_workdays := 0;
    end if;

    /* Count Holidays */
    select Count(Holiday)
    into t_NumHolidays
    from dashboard.Holidays
    where to_char(Holiday, 'D') not in (1, 7)
      and holiday between t_StartDate and t_EndDate;


    /* Subtract the WorkDays from the Holidays */
    t_workdays := t_workdays - t_NumHolidays;

    return(t_workdays);
  end WorkingDaysBetween;
  /*---------------------------------------------------------------------------
   * FormatStatusList()
   *-------------------------------------------------------------------------*/
  function FormatStatusList (
     a_Statuses                    varchar2
   ) return varchar2 is
     t_StatusList                  udt_StringList;
     t_Statuses                    varchar2(4000);
   begin
     t_StatusList := pkg_DashboardUtils.Split(a_Statuses, ',');
     for i in 1..t_StatusList.count loop
       pkg_DashboardUtils.AddSection(t_Statuses, '''' || t_StatusList(i) || '''', ',');
     end loop;

     return t_Statuses;
   end;
  /*---------------------------------------------------------------------------
   * FormatObjectDefIdList()
   *-------------------------------------------------------------------------*/ 
  function FormatObjectDefIdList (
     a_ObjectTypes                    varchar2
   ) return varchar2 is
     t_ObjectDefList                  udt_StringList;
     t_ObjectDefIds                   varchar2(4000);
   begin
     t_ObjectDefList := pkg_DashboardUtils.Split(a_ObjectTypes, ',');
     for i in 1..t_ObjectDefList.count loop
       pkg_DashboardUtils.AddSection(t_ObjectDefIds, '''' || api.pkg_configquery.ObjectDefIdForName(t_ObjectDefList(i)) || '''', ',');
     end loop;

     return t_ObjectDefIds;
   end;
  
  /*---------------------------------------------------------------------------
   * ObjectDefIdList()
   *-------------------------------------------------------------------------*/
  function ObjectDefIdList(
    a_ObjectTypes                       varchar2
  ) return udt_ObjectList is
    t_ObjectList                        udt_ObjectList := dashboard.udt_objectlist();
    t_ObjectDefIds                      udt_StringList;
  begin
    t_ObjectDefIds := pkg_dashboardUtils.Split(a_ObjectTypes, ',');
    t_ObjectList.Extend(t_ObjectDefIds.Count);
    for i in 1..t_ObjectDefIds.count loop
      t_ObjectList(i) := dashboard.udt_object(api.pkg_configquery.ObjectDefIdForName(t_ObjectDefIds(i)));
    end loop;
    return t_ObjectList;
  end;
  
 /*---------------------------------------------------------------------------
  * GetExpressionSyntax() 
  *-------------------------------------------------------------------------*/
  function GetExpressionSyntax (
    a_ChartId                  udt_Id,
    a_ExpressionColumnName     varchar2
  ) return varchar2 is
  
  t_ExpressionColumnName        varchar2(4000);
  t_OnlyJobTypeSelected         varchar2(1);
  begin
    Select OnlyJobTypeSelected
      into t_OnlyJobTypeSelected
      from o_dashboardChart
     where objectid = a_ChartId;
     
    if a_ExpressionColumnName is not null then
      t_ExpressionColumnName := substr(a_ExpressionColumnName,instr(a_ExpressionColumnName,':',1,2) + 1,length(a_ExpressionColumnName));
      if instr(a_ExpressionColumnName,':') > 0 then
        if substr(a_ExpressionColumnName,1,6) = 'corral' then
          return 'corral.'||t_ExpressionColumnName;
        elsif substr(a_ExpressionColumnName,1,6) = 'system' then
          if t_ExpressionColumnName = 'createddate' then
             if t_OnlyJobTypeSelected = 'Y' then
                return 'job.'||t_ExpressionColumnName;
             else
                return 'object.'||t_ExpressionColumnName;
             end if;
          elsif t_ExpressionColumnName IN ('objectdefname','objectdefdescription','createdbyusername','lastupdatebyusername','lastupdatedate','externalfilenum') then
             return 'object.'||t_ExpressionColumnName;
          elsif t_ExpressionColumnName IN ('projectid','seqnum','externalid','statusname','statusdescription','issuedate','completeddate','description') then
             return 'job.'||t_ExpressionColumnName;
          elsif t_ExpressionColumnName IN ('datestarted','datecompleted','completedbyusername','outcome','processdescription') then 
             return 'process.'||t_ExpressionColumnName;
          end if;
        end if;
      else
        return a_ExpressionColumnName;
      end if;
    else
      return a_ExpressionColumnName;
    end if;
  end GetExpressionSyntax;

end pkg_DashboardUtils;

/

