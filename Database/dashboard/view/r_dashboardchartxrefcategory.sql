create or replace view r_dashboardchartxrefcategory as
select
  RelationshipId,
  DashboardChartXrefId,
  DashboardCategoryId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartXrefCategory;

