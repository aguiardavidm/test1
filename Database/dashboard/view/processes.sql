create or replace view processes as
select
  LogicalTransactionId,
  ProcessId,
  ExternalFileNum,
  JobId,
  SeqNum,
  ProcessTypeId,
  JobTypeProcessTypeId,
  ProcessStatus,
  Description ProcessDescription,
  ScheduledStartDate,
  ScheduledCompleteDate,
  DateStarted,
  DateCompleted,
  CompletedByUserId,
  CompletedBy,
  (select u.Name
   from api.Users u
   where u.UserId = p.CompletedByUserId) CompletedByUserName,
  Outcome,
  CreatedByUserId,
  CreatedBy,
  CreatedDate,
  AssignedStaff,
  ObjectMaintenanceBlockId,
  CompletedByLogicalTransId
from api.Processes p;

grant select
on processes
to posseextensions;

