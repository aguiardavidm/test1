create or replace view dash_openaccusations as
select j.ACCUSATIONJOBID JobId, trunc(j.CreatedDate) CTCreatedDate
from abcrw.accusationjob j
where j.JobStatus in ('In Progress', 'Decided');

