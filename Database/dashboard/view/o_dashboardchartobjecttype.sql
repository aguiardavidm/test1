create or replace view o_dashboardchartobjecttype as
select
  ObjectId,
  ObjectDefId,
  ObjectDefTypeId,
  LogicalTransactionId,
  CreatedLogicalTransactionId,
  ExternalFileNum,
  DefaultSecurityOverridden,
  Address,
  BitmapName,
  EffectiveEndDate,
  EffectiveStartDate,
  HouseNumber,
  HouseSuffix,
  LastUpdateByUserId,
  LastUpdateByUserName,
  LastUpdateByUserOracleLogon,
  ObjectDefDescription,
  ObjectDefName,
  StreetName,
  StreetNameId,
  Suite,
  DisplayFormat,
  Dashboard_ObjectDefName
from query.o_DashboardChartObjectType;

