create or replace view dash_license as
select l.ObjectId, l.LicenseNumber, l.EffectiveStartDate, l.EffectiveEndDate, l.ExpirationDate, l.state, l.IssueDate, lt.Name LicenseType, l.Establishment, l.PhysicalAddress
     , l.Licensee, r.Name Region, l.Office, l.RiskFactor, l.EventType, l.EventLocationAddress
from bcpcorral.license l
join abcrw.region r on r.REGIONID = l.RegionObjectId
join bcpcorral.licensetype lt on lt.LICTYPEOBJECTID = l.LicenseTypeObjectId;

