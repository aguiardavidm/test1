create or replace view dash_newapplicationsapproved as
select j.NEWAPPLICATIONJOBID JobId, trunc(j.CreatedDate) CreatedDate, trunc(j.ApplicationReceivedDate) ApplicationReceivedDate, trunc(j.completeddate) CompletedDate
from abcrw.newapplicationjob j
where j.ApplicationReceivedDate is not null and j.CompletedDate is not null
  and j.JobStatus in ('Distribute', 'Approved');

