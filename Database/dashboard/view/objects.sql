create or replace view objects as
select
 o.ObjectId,
 o.ObjectDefId,
 (select od.Name
  from api.ObjectDefs od
  where od.ObjectDefId = o.ObjectDefId) ObjectDefName,
 (select od.Description
  from api.ObjectDefs od
  where od.ObjectDefId = o.ObjectDefId) ObjectDefDescription,
 o.ObjectDefTypeId,
 o.ExternalFileNum,
 o.CreatedLogicalTransactionId,
 o.LogicalTransactionId,
 l.CreatedBy CreatedByUserId,
 (select u.Name
  from api.Users u
  where u.UserId = l.CreatedBy) CreatedByUserName,
 l.CreatedDate,
 l2.CreatedBy LastUpdateByUserId,
 (select u.Name
  from api.Users u
  where u.UserId = l2.CreatedBy) LastUpdateByUserName,
 l2.CreatedDate LastUpdateDate
from
  api.Objects o
  join api.LogicalTransactions l
      on l.LogicalTransactionId = o.CreatedLogicalTransactionId
  join api.LogicalTransactions l2
      on l2.LogicalTransactionId = o.LogicalTransactionId;

grant select
on objects
to posseextensions;

