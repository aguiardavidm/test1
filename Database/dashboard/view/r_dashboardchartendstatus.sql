create or replace view r_dashboardchartendstatus as
select
  RelationshipId,
  DashboardEndStatusId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartEndStatus;

