create or replace view o_dashboardjobstatus as
select
  ObjectId,
  ObjectDefId,
  ObjectDefTypeId,
  LogicalTransactionId,
  CreatedLogicalTransactionId,
  ExternalFileNum,
  DefaultSecurityOverridden,
  Address,
  BitmapName,
  EffectiveEndDate,
  EffectiveStartDate,
  HouseNumber,
  HouseSuffix,
  LastUpdateByUserId,
  LastUpdateByUserName,
  LastUpdateByUserOracleLogon,
  ObjectDefDescription,
  ObjectDefName,
  StreetName,
  StreetNameId,
  Suite,
  DisplayFormat,
  StatusId,
  Tag,
  Description,
  WebSearchSelected
from query.o_DashboardJobStatus;

