create or replace view constants as
select
  'FiscalYearEndDate' name,
  to_date(value,'mmdd') value
from api.Constants
where name = 'DASHBOARD_FISCAL_YEAR_END_DATE';

