create or replace view jobstatuses as
select
  LogicalTransactionId,
  JobTypeId,
  StatusId,
  StatusType
from api.JobStatuses;

