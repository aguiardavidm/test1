create or replace view r_dashboardchartjobstatus as
select
  RelationshipId,
  DashboardJobStatusId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartJobStatus;

