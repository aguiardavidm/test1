create or replace view r_dashboard_dashboardchart as
select
  RelationshipId,
  DashboardId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_Dashboard_DashboardChart;

