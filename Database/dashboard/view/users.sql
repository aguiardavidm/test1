create or replace view users as
select
  LogicalTransactionId,
  UserId,
  UserTypeId,
  OracleLogonId,
  ShortName,
  AuthenticationName,
  Name,
  IdleMinutes,
  CanModifyTime,
  Active,
  IsRangerUser
from api.Users;

