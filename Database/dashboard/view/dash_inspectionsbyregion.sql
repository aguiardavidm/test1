create or replace view dash_inspectionsbyregion as
select i.inspectionjobid JobId, i.JobStatus ctjobstatus, i.InspectionDate CTInspectionDate, nvl(r.name, 'None') CTRegion, ir.Name CTInspectionResult, it.inspectiontype CTInspectionType
from abcrw.inspectionjob i
join abcrw.inspectiontype it on it.INSPECTIONTYPEID = i.InspectionTypeId
left join bcpcorral.license l on l.ObjectId = i.LicenseToInspectId
left join abcrw.region r on r.REGIONID = nvl(l.RegionObjectId, i.RegionId)
left join abcrw.inspectionresulttype ir on ir.INSPECTIONRESULTTYPEID = i.InspectionResultTypeId;

