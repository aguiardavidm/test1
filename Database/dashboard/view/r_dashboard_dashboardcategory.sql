create or replace view r_dashboard_dashboardcategory as
select
  RelationshipId,
  DashboardCategoryId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_Dashboard_DashboardCategory;

