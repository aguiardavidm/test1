create or replace view o_dashboardcategory as
select
  ObjectId,
  ObjectDefId,
  ObjectDefTypeId,
  LogicalTransactionId,
  CreatedLogicalTransactionId,
  ExternalFileNum,
  DefaultSecurityOverridden,
  Address,
  BitmapName,
  EffectiveEndDate,
  EffectiveStartDate,
  HouseNumber,
  HouseSuffix,
  LastUpdateByUserId,
  LastUpdateByUserName,
  LastUpdateByUserOracleLogon,
  ObjectDefDescription,
  ObjectDefName,
  StreetName,
  StreetNameId,
  Suite,
  DisplayFormat,
  Name,
  SeqNum
from query.o_DashboardCategory;

