create or replace view r_dashboardchartxref as
select
  RelationshipId,
  DashboardChartXrefId,
  DashboardId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartXref;

