create or replace view dash_licensesales as
select l.ObjectId, j.NEWAPPLICATIONJOBID JobId, l.LicenseNumber, lt.Name CTLicenseType, (f.PaidAmount * -1) CTPaidAmount, l.IssueDate CTIssueDate, trunc(f.TransactionDate) CTTransactionDate
from bcpcorral.license l
join abcrw.newapplicationjob j on j.LicenseObjectId = l.ObjectId
join bcpcorral.licensetype lt on lt.LICTYPEOBJECTID = l.LicenseTypeObjectId
join api.fees f on f.JobId = j.NEWAPPLICATIONJOBID;

