create or replace view r_dashboardchartprocesstype as
select
  RelationshipId,
  DashboardChartObjectTypeId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartProcessType;

