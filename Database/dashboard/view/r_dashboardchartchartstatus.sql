create or replace view r_dashboardchartchartstatus as
select
  RelationshipId,
  DashboardJobStatusId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from dashboard.r_DashboardChartJobStatus
union all
select
  RelationshipId,
  DashboardEndStatusId DashboardJobStatusId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from dashboard.r_DashboardChartEndStatus
union all
select
  RelationshipId,
  DashboardExcludeStatusId DashboardJobStatusId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from dashboard.r_DashboardChartExludeStatus;

