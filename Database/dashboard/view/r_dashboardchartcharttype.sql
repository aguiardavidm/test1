create or replace view r_dashboardchartcharttype as
select
  RelationshipId,
  DashboardChartTypeId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartChartType;

