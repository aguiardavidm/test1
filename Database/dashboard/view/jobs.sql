create or replace view jobs as
select
  JobId,
  JobTypeId,
  ParentJobId,
  ProjectId,
  ExternalId,
  SeqNum,
  StatusId,
  JobStatus,
  JobStatus StatusName,
  (select s.Description
   from api.Statuses s
   where s.StatusId = j.StatusId) StatusDescription,
  CompletedDate,
  Description,
  IssueDate,
  JobLocation,
  ExternalFileNum,
  CreatedByUserId,
  CreatedBy,
  CreatedDate
from api.Jobs j;

grant select
on jobs
to posseextensions;

