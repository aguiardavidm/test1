create or replace view holidays as
select
  LogicalTransactionId,
  Holiday,
  Description
from api.Holidays;

