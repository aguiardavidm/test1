create or replace view o_dashboardchartstatus as
select
  ObjectId,
  ObjectDefId,
  ObjectDefTypeId,
  LogicalTransactionId,
  CreatedLogicalTransactionId,
  ExternalFileNum,
  DefaultSecurityOverridden,
  Address,
  BitmapName,
  EffectiveEndDate,
  EffectiveStartDate,
  HouseNumber,
  HouseSuffix,
  LastUpdateByUserId,
  LastUpdateByUserName,
  LastUpdateByUserOracleLogon,
  ObjectDefDescription,
  ObjectDefName,
  StreetName,
  StreetNameId,
  Suite,
  DisplayFormat,
  StatusName,
  ToStatusName
from query.o_DashboardChartStatus;

