create or replace view r_dashboardchartuser as
select
  RelationshipId,
  DashboardChartId,
  UserId,
  ObjectDefDescription,
  ObjectDefName,
  IncludeExclude
from query.r_DashboardChartUser;

