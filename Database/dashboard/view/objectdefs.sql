create or replace view objectdefs as
select
  LogicalTransactionId,
  ObjectDefId,
  ObjectDefTypeId,
  Name,
  Description,
  EffectiveDated,
  BitmapName,
  DisplayFormat,
  ExternalTableName,
  ExternalTableOwner,
  ExternalColumnNameForObjectId,
  ExternalFileNumMask,
  ExternalFileNumSequenceId,
  BusinessDescription
from api.ObjectDefs;

