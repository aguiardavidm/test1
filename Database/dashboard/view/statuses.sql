create or replace view statuses as
select
  LogicalTransactionId,
  StatusId,
  Tag,
  Description
from api.Statuses;

