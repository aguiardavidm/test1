create or replace view r_dashboardchartchartxref as
select
  RelationshipId,
  DashboardChartXrefId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartChartXref;

