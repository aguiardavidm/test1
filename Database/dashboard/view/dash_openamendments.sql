create or replace view dash_openamendments as
select j.AMENDMENTJOBID JobId, trunc(j.CreatedDate) CTCreatedDate
from abcrw.amendmentjob j
where j.JobStatus in ('In Review');

