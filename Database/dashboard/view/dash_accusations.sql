create or replace view dash_accusations as
select accusationjobid jobid
     , trunc(createddate) ctcreateddate
     , trunc(completeddate) ctcompleteddate
     , accusationdecision ctdecision
     , jobstatus ctjobstatus
from abcrw.accusationjob a;

