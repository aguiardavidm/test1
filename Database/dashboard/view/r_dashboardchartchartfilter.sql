create or replace view r_dashboardchartchartfilter as
select
  RelationshipId,
  DashboardChartFilterId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartChartFilter;

