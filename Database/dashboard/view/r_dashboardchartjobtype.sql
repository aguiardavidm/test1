create or replace view r_dashboardchartjobtype as
select
  RelationshipId,
  DashboardChartObjectTypeId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartJobType;

