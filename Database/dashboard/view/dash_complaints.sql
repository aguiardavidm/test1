create or replace view dash_complaints as
select c.COMPLAINTJOBID JobId, c.CompletedDate CTCompletedDate
from bcpcorral.complaintjob c
where c.CompletedDate is not null;

