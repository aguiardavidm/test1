create or replace view dash_openrenewals as
select j.RENEWALJOBID JobId, trunc(j.CreatedDate) CTCreatedDate
from abcrw.renewaljob j
where j.JobStatus in ('In Review');

