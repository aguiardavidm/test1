create or replace view dash_openinspections as
select j.INSPECTIONJOBID JobId, trunc(j.CreatedDate) CTCreatedDate
from abcrw.inspectionjob j
where j.JobStatus in ('In Review');

grant select
on dash_openinspections
to posseextensions;

