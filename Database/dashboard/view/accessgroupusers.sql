create or replace view accessgroupusers as
select
  LogicalTransactionId,
  AccessGroupId,
  UserId
from api.AccessGroupUsers;

