create or replace view r_dashboardchartobjecttype as
select
  RelationshipId,
  DashboardObjectTypeId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartObjectType;

