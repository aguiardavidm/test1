create or replace view dash_openapplications as
select newapplicationjobid JobId, trunc(j.CreatedDate) CTCreatedDate
from abcrw.newapplicationjob j
where j.JobStatus in ('In Review');

