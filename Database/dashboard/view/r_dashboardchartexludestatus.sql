create or replace view r_dashboardchartexludestatus as
select
  RelationshipId,
  DashboardExcludeStatusId,
  DashboardChartId,
  ObjectDefDescription,
  ObjectDefName
from query.r_DashboardChartExludeStatus;

