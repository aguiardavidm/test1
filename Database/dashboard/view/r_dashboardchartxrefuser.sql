create or replace view r_dashboardchartxrefuser as
select
  RelationshipId,
  DashboardChartXrefId,
  UserId,
  ObjectDefDescription,
  ObjectDefName,
  IncludeExclude
from query.r_DashboardChartXrefUser;

