create or replace view incompleteprocessassignments as
select
  ProcessId,
  AssignedTo
from api.IncompleteProcessAssignments;

