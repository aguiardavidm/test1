create or replace view o_dashboardchartfilter as
select
  ObjectId,
  ObjectDefId,
  ObjectDefTypeId,
  LogicalTransactionId,
  CreatedLogicalTransactionId,
  ExternalFileNum,
  DefaultSecurityOverridden,
  Address,
  BitmapName,
  EffectiveEndDate,
  EffectiveStartDate,
  HouseNumber,
  HouseSuffix,
  LastUpdateByUserId,
  LastUpdateByUserName,
  LastUpdateByUserOracleLogon,
  ObjectDefDescription,
  ObjectDefName,
  StreetName,
  StreetNameId,
  Suite,
  DisplayFormat,
  ColumnName,
  FromValue,
  ToValue
from query.o_DashboardChartFilter;

