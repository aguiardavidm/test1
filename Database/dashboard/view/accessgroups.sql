create or replace view accessgroups as
select
  LogicalTransactionId,
  AccessGroupId,
  Description
from api.AccessGroups;

