create table jobstatuschange_t (
  jobstatuschangeid               number(9) not null,
  jobid                           number(9),
  statusname                      varchar2(6),
  effectivestartdate              date,
  effectiveenddate                date,
  mostrecent                      char(1),
  statustype                      varchar2(1),
  effectiveuser                   varchar2(30),
  statusdate                      date
) tablespace largedata;

grant select
on jobstatuschange_t
to public;

alter table jobstatuschange_t
add constraint jobstatuschange_pk
primary key (
  jobstatuschangeid
) using index tablespace largeindexes;

create index jobstatuschange_ix_1
on jobstatuschange_t (
  jobid
) tablespace largeindexes;

