[Main]
select j.jobid Jobid,
       j.ExternalFileNum,
       j.JobDescription,
       to_char( j.CreatedDate, 'fmMonth DD, YYYY' ) CreatedDate,
       j.CreatedByUser
from query.j_abc_batchrenewalnotification j 
where  j.ObjectId = :PosseObjectId

[PermitContent]
select a.PermitObjectId,
       a.Permittee,
       a.PermitNumber,
       a.PermitType,
       a.ExpirationDate,
       a.Establishment,
       a.EstablishmentAddress,
       a.PermitteeObjectID,
       a.MailingAddress,
       a.AddDetails1,
       a.AddLabels1,
       a.AddDetails2
  from (
          select   p.ObjectId                                                                  PermitObjectId,
                   le.dup_FormattedName                                                        Permittee,
                   p.PermitNumber                                                              PermitNumber,
                   p.PermitType                                                                PermitType,
                   to_char(p.ExpirationDate,'fmMonth DD, YYYY')                                ExpirationDate,
                   p.LicenseEstablishment                                                      Establishment,
                   p.LicenseEstablishmentLocation                                              EstablishmentAddress,
                   p.dup_FormattedPermitteeObjectId                                            PermitteeObjectID,
                   le.MailingAddress                                                           MailingAddress,
                   (case
                      when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
                        p.EventLocAddressDescription
                      when p.PermitTypeCode = 'SOL' then
                        p.SolicitorName
                    end)                                                                       AddDetails1,
                   (case
                      when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
                        'Location:'
                      when p.PermitTypeCode = 'SOL' then
                        'Solicitor:'
                    end)                                                                       AddLabels1,
                   (case
                      when p.RequiresLocation = 'Y' and p.RequiresEvents = 'N' then
                        p.EventLocationAddress
                    end)                                                                       AddDetails2
          from     query.j_abc_batchrenewalnotification j
          join     abc.permitbatchgrace_xref_t rg on rg.renewalnotifbatchjobid = j.JobId
          join     query.o_abc_permit p on p.ObjectId = rg.permitobjectid
          join     query.o_abc_legalentity le on le.objectid = p.dup_FormattedPermitteeObjectId
          where    j.objectid = :[Main]Jobid
            and    (le.PreferredContactMethod is null or
                    le.PreferredContactMethod is not null and le.PreferredContactMethod <> 'Email')
          UNION ALL
          select   p2.ObjectId                                                                 PermitObjectId,
                   le.dup_FormattedName                                                        Permittee,
                   p2.PermitNumber                                                             PermitNumber,
                   p2.PermitType                                                               PermitType,
                   to_char(p2.ExpirationDate,'fmMonth DD, YYYY')                               ExpirationDate,
                   p2.LicenseEstablishment                                                     Establishment,
                   p2.LicenseEstablishmentLocation                                             EstablishmentAddress,
                   p2.dup_FormattedPermitteeObjectId                                           PermitteeObjectID,
                   le.MailingAddress                                                           MailingAddress,
                   (case
                      when p2.RequiresLocation = 'Y' and p2.RequiresEvents = 'N' then
                        p2.EventLocAddressDescription
                      when p2.PermitTypeCode = 'SOL' then
                        p2.SolicitorName
                    end)                                                                       AddDetails1,
                   (case
                      when p2.RequiresLocation = 'Y' and p2.RequiresEvents = 'N' then
                        'Location:'
                      when p2.PermitTypeCode = 'SOL' then
                        'Solicitor:'
                    end)                                                                       AddLabels1,
                   (case
                      when p2.RequiresLocation = 'Y' and p2.RequiresEvents = 'N' then
                        p2.EventLocationAddress
                    end)                                                                       AddDetails2
          from     query.j_abc_batchrenewalnotification j
          join     abc.permitbatch_xref_t rl on rl.renewalnotifbatchjobid = j.jobid
          join     query.o_abc_permit p2 on p2.objectid = rl.permitobjectid
          join     query.o_abc_legalentity le on le.objectid = p2.dup_FormattedPermitteeObjectId
          where    j.objectid = :[Main]Jobid
            and    (le.PreferredContactMethod is null or
                    le.PreferredContactMethod is not null and le.PreferredContactMethod <> 'Email')
       ) a
  order by 3

[NJABCAddress]
select    ss.DepartmentAddress Address
from      query.o_systemsettings ss

[PermitText]
select    s2.PermitLetterText NotifyText
from      query.o_systemsettings s2


[Document Info]
EndpointName=BatchRenewalLetter
DocumentTypeName=d_ElectronicDocument

[Document Bindings]
Description=Batch Renewal Notification - Permit