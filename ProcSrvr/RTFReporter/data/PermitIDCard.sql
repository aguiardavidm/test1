[Settings]
ApplyWatermark=N
mainobjectid={Main:PermitObjectId}
ApplyImageWatermark=Y
WatermarkImage=idwatermark.gif
WatermarkImageTransparency=95
watermarkImageHeight=825
watermarkImageWidth=580
WatermarkEscapement=0
PDFA=N

[MAIN]
select /*+ optimizer_features_enable('11.2.0.4') */ :PosseObjectId ProcessId
     , p.ObjectId PermitObjectId
     , (case when p.PermitTypeCode = 'MA2' then 'MARKETING AGENT' else upper(p.PermitType) end) PermitType
     , nvl(p.PermitNumber, '(PENDING)') PermitNumber
     , to_Char(p.ExpirationDate, 'FM MONTH dd, yyyy') ExpirationDate
     , (case when p.PermitTypeCode='SOL' then upper(p.SolicitorName)
         else upper(p.PermitteeDisplay_sql)
        end ) PermitteeName
     , upper((select le.MailingAddress 
                from query.o_abc_legalentity le 
     	       where le.ObjectId = (case when p.PermitTypeCode='SOL' then p.SolicitorObjectId else p.FormattedPermitteeObjectId end)
     	      )) PermitteeAddress
     , (case when p.PermitTypeCode='MA2' then 'Y' else 'N' end) flag_ShowMA
     , (case when p.PermitTypeCode='SOL' then 'Y' else 'N' end) flag_ShowSOL
	 , (case when p.TAPPermitNumber is null then 'N' else 'Y' end) flag_ShowTAP
  from query.o_ABC_Permit p
 where p.ObjectId = api.pkg_columnquery.numericvalue(:PosseObjectId, 'PermitId')
    or p.ObjectId = :PosseObjectId
 
[MALicense]
select upper(api.pkg_columnQuery.Value(p.LicenseObjectId, 'Licensee')) Licensee
  from query.r_PermitLicense p
  join query.r_ABC_AssociatedPermit ap on ap.AsscPermitObjectId = p.PermitObjectId
 where ap.PrimaryPermitObjectId = :[Main]PermitObjectId

[MAPermit]
select (case when api.pkg_ColumnQuery.Value(ap.PrimaryPermitObjectId, 'PermitTypeCode') = 'MA2' then 
	     upper(api.pkg_ColumnQuery.Value(ap.AsscPermitObjectId, 'PromoterCompanyName'))
	   else
	     upper(api.pkg_ColumnQuery.Value(ap.AsscPermitObjectId, 'PermitteeDisplay_SQL')) 
       end) Employer
  from query.r_ABC_AssociatedPermit ap
 where ap.PrimaryPermitObjectId = :[Main]PermitObjectId

[SOL]
select upper(api.pkg_ColumnQuery.Value(p.LicenseObjectId, 'Licensee')) Employer
     , api.pkg_ColumnQuery.Value(p.LicenseObjectId, 'LicenseNumber') LicenseNumber
  from query.r_PermitLicense p
 where p.PermitObjectId = :[Main]PermitObjectId

[TAPData]
select upper(api.pkg_ColumnQuery.Value(p.TAPPermitObjectId, 'PermitteeDisplay_SQL')) TAPEmployer
     , api.pkg_ColumnQuery.Value(p.TAPPermitObjectId, 'PermitNumber') TAPPermitNumber
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId
 
[Document Info]
EndpointName=PermitIDCard
DocumentTypeName=d_PosseReportLetter
 
[Document Bindings]
Description=Permit ID Card