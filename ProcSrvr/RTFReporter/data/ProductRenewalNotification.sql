[Main]
select j.jobid Jobid,
       j.ExternalFileNum,
       j.JobDescription,
       to_char( j.CreatedDate, 'fmMonth DD, YYYY' ) CreatedDate,
       j.CreatedByUser
from   query.j_abc_batchrenewalnotification j 
where  j.ObjectId = :PosseObjectId

[Registrant]
select ol.dup_FormattedName Registrant,
       ol.MailingAddress,
       ol.PhysicalAddress,
       oprc.RenewalCycle,
       r.ProductCount,
       to_char( r.EarliestProductExpirationDate, 'fmMonth DD, YYYY')  ExpirationDate
  from abc.RegistrantNotifications_t rt 
  join query.r_ABC_RegistrantProdRenewCycle r on r.ProductRenewalCycleObjectId = rt.renewalcycleobjectid 
                                              and r.RegistrantObjectId = rt.registrantobjectid
  join query.o_abc_productrenewalcycle oprc on oprc.ObjectId = rt.Renewalcycleobjectid
  join query.o_abc_legalentity ol on ol.ObjectId = rt.RegistrantObjectid  -- only need this if we are going to use RenewCycle
  where rt.notificationjobid = :[Main]JobId
    and ( ol.PreferredContactMethod is null or
          ol.PreferredContactMethod is not null and ol.PreferredContactMethod <> 'Email' )
  order by 1

[NJABCAddress]
select ss.DepartmentAddress Address
  from query.o_systemsettings ss
  where rownum = 1

[Product]
select s2.ProductLetterText NotifyText
  from query.o_systemsettings s2
  where rownum = 1

[Document Info]
EndpointName=BatchRenewalLetter
DocumentTypeName=d_ElectronicDocument

[Document Bindings]
Description=Batch Renewal Notification - Products
