[Settings]
ApplyWatermark=N
mainobjectid={Main:ObjectId}
ApplyImageWatermark=Y
WatermarkImage=watermark.gif
WatermarkImageTransparency=80
watermarkImageHeight=580
watermarkImageWidth=580
WatermarkEscapement=0
PDFA=N

[MAIN]
select /*+ optimizer_features_enable('11.2.0.4') */ p.ObjectId PermitObjectId
     , upper(p.PermitType) PermitType 
     , upper(to_Char(p.IssueDate, 'MM/DD/YYYY')) IssueDate
     , upper(to_Char(p.ExpirationDate, 'MM/DD/YYYY')) ExpirationDate
     , upper(p.PermitNumber) PermitNumber
     , p.PermitTypeObjectId
     , (select case when count(plt.LegalTextId) >0 then 'Y' else 'N' end
          from query.r_abc_permittypeLegalText plt 
         where plt.PermitTypeId = p.PermitTypeObjectId
       ) flag_ShowLegalText
     , p.RequiresEvents flag_ShowEvents
     , p.RequiresLocation flag_ShowEvtLocation
     , p.RequiresEvents flag_ShowEventDates
     , p.RequiresEvents flag_ShowRainDate
     , (case when p.RequiresLocation = 'Y' and p.RequiresEvents <> 'Y' then 'Y' else 'N' end) flag_ShowLocation
     , p.RequiresCoOp flag_ShowCoOp
     , (case when p.RequiresCoOp = 'Y' then 'N' else 'Y' end) flag_ShowPermit
     , p.RequiresSolicitor flag_ShowSolicitor
     , p.RequiresProducts flag_ShowProducts
     , (select (case when count(m.CoOpMemberObjectId)>0 and p.RequiresCoOp = 'Y' then 'Y' else 'N' end)
 	  from query.r_abc_permitcooplicenses m
         where m.PermitObjectId = p.ObjectId
       ) flag_ShowMembers
     , (case when p.AssociatedPermitObjectId is not null then 'Y' else 'N' end) flag_ShowAssociatedPermit
     , (case when p.LicenseObjectId is not null and p.requiresCoOp <> 'Y' then 'Y' else 'N' end) flag_ShowAssociatedLicense
     , p.RequireSellerLicense flag_ShowSellerLicenseInfo
     , (select api.pkg_columnquery.value(s.objectid,'DirectorName') as DirectorName
         from api.objects s
         where s.objectdefid = api.pkg_configquery.objectdefidforname('o_SystemSettings')
         and rownum < 2
       ) DirectorName
     , (select api.pkg_columnquery.value(s.objectid,'DirectorTitle') as DirectorTitle
         from api.objects s
         where s.objectdefid = api.pkg_configquery.objectdefidforname('o_SystemSettings')
         and rownum < 2
       ) DirectorTitle
     , p.LicenseObjectId
     , (case when api.pkg_columnQuery.Value(p.LicenseObjectId, 'IssuingAuthority') = 'Municipality' then 'Y' else 'N' end) flag_ShowMunicipality
     , (select pt.RequireEffectiveDateOfLicTrans
          from query.o_ABC_PermitType pt 
         where pt.ObjectId = p.PermitTypeObjectId
       ) flag_ShowTransferDate
     , (select pt.DisplayDetailedLicenseInfo
          from query.o_ABC_PermitType pt 
         where pt.ObjectId = p.PermitTypeObjectId
       ) flag_ShowLicenseDetails
     , (case when p.PermitTypeCode = 'FP' then 'Y' else 'N' end) flag_ShowFPInfo
     , (case when p.PermitTypeCode = 'PC' then 'Y' else 'N' end) flag_ShowImportProductInfo
     , (case when p.PermitTypeCode = 'PS' then 'Y' else 'N' end) flag_ShowSaleInfo
	 , (case when p.TAPPermitNumber is null then 'N' else 'Y' end) flag_ShowTAP
     , (select case when count(*) >0 then 'Y' else 'N' end
          from query.r_ABC_PermitSpecialConditions psc
         where psc.PermitId = p.ObjectId
       ) flag_ShowSpecialConditions
     , (select case when count(*) >0 then 'Y' else 'N' end
          from query.r_ABC_SolicitorPermit sp
         where sp.PermitObjectId = p.ObjectId
	   ) flag_ShowCTWSolicitor
     , (select case when count(*) >0 then 'Y' else 'N' end
          from query.r_ABC_PermitOwner po
         where po.PermitObjectId = p.ObjectId
	   ) flag_ShowCTWOwner
	 , (case when p.PermitTypeCode = 'MA' then 'Y' else 'N' end) flag_ShowMarketingAgent
	 , (case when p.PermitTypeCode = 'MSO' then 'Y' else 'N' end) flag_ShowTTBPermit
	 , (case when p.PermitTypeCode = 'SP' then 'Y' else 'N' end) flag_ShowSPPermit
     , 'Y' flag_ShowFee
  from query.o_ABC_Permit p
 where p.ObjectId = :PosseObjectId

[AssociatedPermit]
select upper(p.AssociatedPermitNumber) AssociatedPermitNumber
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId
 
[Co-Op]
select upper(api.pkg_columnquery.value(p.PermitteeObjectId, 'ContactName')) PermitteeContact
     , upper(api.pkg_columnquery.value(p.PermitteeObjectId, 'FormattedName')) PermitteeName
     , upper(api.pkg_columnquery.value(p.PermitteeObjectId, 'PhysicalAddress')) PermitteeAddress
  from query.o_Abc_Permit p
 where p.ObjectId = :[Main]PermitObjectId

[EventDetails]
 select upper((to_Char(ed.StartDate, 'MM/DD/YYYY')|| ' '|| ed.FormattedStartTime || ' to ' || ed.FormattedEndTime)) EventDateDisplay
 from query.o_ABC_EventDate ed
  join query.r_Abc_Permiteventdate ped
    on ped.EventDateObjectId=ed.objectId
 where ped.PermitObjectId = :[Main]PermitObjectId

[EventType]
select upper(p.EventDetails) EventType
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId
  
[Fees]
select (case when sum(api.pkg_columnQuery.NumericValue(rfp.JobId, 'FeeAmount')) > 0
        then trim(to_char(sum(api.pkg_columnQuery.NumericValue(rfp.JobId, 'FeeAmount')), '$9,999.99'))
        else 'N/A'
        end) TotalAmount
  from query.r_MiscRevenuePermit rfp
 where rfp.PermitId = :[Main]PermitObjectId

[FP]
select upper(FPProductDescription) Product
     , upper(FPNumberOfGallons) Gallons
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId

[Import]
select upper(PersonalConsumptionProductDesc) Description
     , upper(PersonalConsumptionProductOrig) Origin
     , upper(PersonalConsumptionProductDest) Destination
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId
 
[License]
select upper(p.LicenseNumber) LicenseNumber
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId

[LicenseDetails]
select (select upper(le.dup_FormattedName)
               from query.o_abc_LegalEntity le
			  where le.objectid = l.LicenseeObjectId) Licensee
     , (select upper(nvl(e.physicaladdress, e.mailingaddress))
               from query.o_abc_Establishment e
              where e.objectid = l.EstablishmentObjectId
       ) PremiseAddress
  from query.o_ABC_License l
 where l.ObjectId = :[Main]LicenseObjectId

[Location]
select upper(EventLocationAddress) Location, upper(p.EventLocAddressDescription) LocDescription
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId
   
[Members]
select upper(lic.dup_Licensee) MemberLicenseeName, upper(lic.LicenseNumber) LicenseNumber, upper(to_char(m.CoOpMemberEffectiveDate,'MM/DD/YYYY')) EffectiveDate
  from query.r_abc_permitcooplicenses m
  join query.o_abc_license lic on lic.ObjectId = m.CoOpMemberObjectId
 where m.PermitObjectId = :[Main]PermitObjectId
 order by lic.LicenseNumber

[Municipality]
select upper(l.Office) Office
  from query.o_ABC_License l
 where l.ObjectId = :[Main]LicenseObjectId
 
[Permit]
select upper(p.PermitteeDisplay_sql) PermitteeName
     , (select upper(le.PhysicalAddress) 
          from query.o_abc_legalentity le 
         where le.ObjectId = p.FormattedPermitteeObjectId
       ) PermitteeAddress
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId

[Products]
 select nvl(trim(to_Char(rppr.NumberOfCasesAffected)),'N/A') NumberOfCasesAffected
      , nvl(trim(to_Char(rppr.PricePerCase, '$9,999.99')),'N/A') PricePerCase
      , upper(api.pkg_columnQuery.Value(rppr.ProductObjectId, 'Name')) ProductName
      , api.pkg_columnQuery.Value(rppr.ProductObjectId, 'RegistrationNumber') RegistrationNumber
   from query.r_ABC_PermitProduct rppr 
  where rppr.PermitObjectId = :[Main]PermitObjectId
  order by api.pkg_columnQuery.Value(rppr.ProductObjectId, 'Name')

[PS]
-- Sale by Judgement, Creditor, Receiver, or Trustee
select upper(p.SaleByJudgementBy) SaleBy
     , upper(p.SaleByJudgementAt) SaleAt
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId
 
[RainDateDetails]
select upper((to_Char(rd.StartDate, 'MM/DD/YYYY') || ' ' || rd.FormattedStartTime || ' to ' || rd.FormattedEndTime)) RainDateDisplay
  from query.o_ABC_EventDate rd
  join query.r_abc_permitRainDate prd
    on prd.RainDateObjectId=rd.objectId
 where prd.PermitObjectId = :[Main]PermitObjectId

[Seller]
select rsp.LicenseObjectId,
       upper(l.Licensee) Licensee,
       (select upper(nvl(e.physicaladdress, e.mailingaddress))
          from query.o_abc_Establishment e
         where e.objectid = l.EstablishmentObjectId) SellerAddress,
       upper(l.LicenseNumber) LicenseNumber,
       (case
         when upper(l.IssuingAuthority) = 'MUNICIPALITY' then
          upper(l.Office)
         else
          upper(l.IssuingAuthority)
       end) IssuingAuthority,
       (case
         when api.pkg_columnquery.Value(:[Main]PermitObjectId, 'PermitTypeCode') = 'RR' then
           'PURCHASING LICENSEE:'
         else
           'FORMER LICENSEE:'
       end) LicenseeLabel
  from query.r_SellersLicense rsp
  join query.o_ABC_License l
    on l.ObjectId = rsp.LicenseObjectId
 where rsp.PermitObjectId = :[Main]PermitObjectId
 
[Solicitor]
select upper(p.SolicitorName) SolicitorName
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId

[TransferDate]
select upper(EffectiveDateOfLicenseTransfer) TransferDate
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId

[LegalText]
select UPPER(lt.LegalDescription) Text
  from query.r_abc_permittypeLegalText plt
  join query.o_abc_PermitLegalText lt on plt.LegalTextId = lt.ObjectId 
 where plt.PermitTypeId = :[Main]PermitTypeObjectId
 order by lt.SortOrder

[Signature]
select dr.Value,
       dr.Extension
from api.objects s
join api.documents d
     on d.DocumentId = api.pkg_columnquery.numericvalue(s.objectid,'DirectorSignatureObjectId')
join api.documentrevisions dr
     on dr.DocumentId = d.DocumentId
     and dr.RevisionNum = d.TailRevisionNum
where s.objectdefid = api.pkg_configquery.objectdefidforname('o_SystemSettings')
and rownum < 2

[NJAddress]
select /*+ CARDINALITY(s 1) optimizer_features_enable('11.2.0.4') */
       upper(api.pkg_columnQuery.Value(s.ObjectId, 'DepartmentNameAndAddress')) Address
  from api.objects s
 where s.objectdefid = api.pkg_configquery.objectdefidforname('o_SystemSettings')

[TAPData]
select upper (p.TAPPermitNumber) TAPPermitNumber,
	   upper (p.PermitteeName_TAP) TAPPermitteeName,
	   (select upper (nvl(le.PhysicalAddress,le.MailingAddress) )
         from query.o_abc_legalentity le 
         where le.ObjectId = p.PermitteeObjectId_TAP
       ) TAPPermitteeAddress,
       upper(to_Char(api.pkg_columnQuery.DateValue(p.TAPPermitObjectId, 'ExpirationDate'), 'MM/DD/YYYY')) TAPExpirationDate
  from query.o_ABC_Permit p
 where p.ObjectId = :[Main]PermitObjectId

[SpecialConditions]
select UPPER(ps.ConditionDescription) Text,
       ps.SortOrder SortOrder
  from query.o_abc_permitspecialconditions ps
  join query.r_ABC_PermitSpecialConditions psc on psc.SpecialConditionsId = ps.ObjectId
 where psc.PermitId = :[Main]PermitObjectId
 order by ps.SortOrder

[CTWSolicitor]
select upper (p.PermitNumber) SolicitorNumber,
	   upper (p.SolicitorName) SolicitorName
from query.r_ABC_SolicitorPermit sp
join query.o_Abc_Permit p on p.ObjectId = sp.SolicitorPermitObjectId
where sp.PermitObjectId = :[Main]PermitObjectId
order by lpad(p.PermitNumber,50)

[CTWOwner]
select upper (le.FormattedName) OwnerName
from query.r_ABC_PermitOwner po
join query.o_Abc_LegalEntity le on le.ObjectId = po.OwnerLEObjectId
where po.PermitObjectId = :[Main]PermitObjectId

[MarketingAgent]
select upper(p.PromoterCompanyName) PromoterCompanyName, 
	   upper(p.PromoterAddress) PromoterAddress
from query.o_abc_permit p where p.ObjectId = :[Main]PermitObjectId

[TTBPermit]
select upper(p.TTBPermitNumber) TTBPermitNumber
from query.o_abc_permit p where p.ObjectId = :[Main]PermitObjectId

[SP]
 select upper(api.pkg_columnQuery.Value(rppr.ProductObjectId, 'Name')) ProductName
      , api.pkg_columnQuery.Value(rppr.ProductObjectId, 'RegistrationNumber') RegistrationNumber
   from query.r_ABC_PermitProduct rppr 
  where rppr.PermitObjectId = :[Main]PermitObjectId
  order by api.pkg_columnQuery.Value(rppr.ProductObjectId, 'Name')

[Document Info]
EndpointName=PermitCertificate
DocumentTypeName=d_PosseReportLetter
 
[Document Bindings]
Description=Permit Certificate

[FeeAmount]
select /*+ optimizer_features_enable('11.2.0.4') */
case when (select sum(FeeAmount)
from   (select f.Amount+F.AdjustedAmount FeeAmount
        from   query.r_ABC_PermitAppPermit r1
        join   api.fees f on f.JobId = r1.PermitApplicationObjectId
        where  r1.PermitObjectId = :[Main]PermitObjectId
        union
        select f.Amount+f.AdjustedAmount FeeAmount
        from   query.r_ABC_RenewAppJobPermit r2
        join   api.fees f on f.JobId = r2.JobId
        where  r2.PermitId = :[Main]PermitObjectId
       )) > 0 then
      trim(to_char((
select sum(FeeAmount)
from   (select f.Amount+F.AdjustedAmount FeeAmount
        from   query.r_ABC_PermitAppPermit r1
        join   api.fees f on f.JobId = r1.PermitApplicationObjectId
        where  r1.PermitObjectId = :[Main]PermitObjectId
        union
        select f.Amount+f.AdjustedAmount FeeAmount
        from   query.r_ABC_RenewAppJobPermit r2
        join   api.fees f on f.JobId = r2.JobId
        where  r2.PermitId = :[Main]PermitObjectId
       )), '$99,999,999.99'))
	    else 'N/A'
end FeeAmount
from  query.o_abc_permit p
where p.ObjectId = :[Main]PermitObjectId
