The control file appends any records to the table, it does not replace existing records. You can change the name of the input file if you want to keep separate versions.
*** ENSURE THE CSV FILE IS PIPE DELIMITED, NOT COMMA!!!

Run the following script to load the Police User data into the NJ ABC database of each environment:
- sqlldr possedba@cxnjdev control=PolicePortal.ctl log=PolicePortal.txt -- For njdev you need to sign on to the database server Njorcw64-1 since SQLLoader is not installed locally.
- sqlldr possedba@njdelivery control=PolicePortal.ctl log=PolicePortal.txt
- sqlldr possedba@njuat control=PolicePortal.ctl log=PolicePortal.txt
- sqlldr possedba@njconv control=PolicePortal.ctl log=PolicePortal.txt
- sqlldr possedba@njprod control=PolicePortal.ctl log=PolicePortal.txt