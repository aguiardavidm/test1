options (skip=3, rows=10000, bindsize=80000000, readsize=80000000)
load data
 infile 'D:\scripts\SQLLDR\Issue32713-PolicePortal\PolicePortal.csv'
 append
 into table possedba.ISSUE32713_POLICE
 fields terminated by "|" optionally enclosed by '"'
(
RANKTITLE,
FIRSTNAME,
LASTNAME,
EMAILADDRESS,
BUSINESSPHONE,
PHONEEXTENSION,
CELLPHONE,
MUNICIPALITY
)